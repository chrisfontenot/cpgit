﻿namespace DebtPlus.UI.Common.Controls
{
    /// <summary>
    /// A class to generate the button image for the search function.
    /// </summary>
    public class SearchButton : DevExpress.XtraEditors.Controls.EditorButton
    {
        /// <summary>
        /// Graphic image for the search operation
        /// </summary>
        private static System.Drawing.Image buttonImage;

        /// <summary>
        /// When the class is started, find the graphic image.
        /// </summary>
        static SearchButton()
        {
            buttonImage = Images.Search;
        }

        /// <summary>
        /// Create a search button
        /// </summary>
        public SearchButton() : this(true)
        {
        }

        /// <summary>
        /// Create the search button
        /// </summary>
        /// <param name="isDefaultButton">TRUE if the search button is the default button</param>
        public SearchButton(bool isDefaultButton) : base()
        {
            this.IsDefaultButton = isDefaultButton;
            this.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
            this.Image = new System.Drawing.Bitmap(buttonImage); // wash the image through another bitmap to make it suitable for use. Don't just use the image!!
            this.Tag = "search";
            this.ToolTip = "Click here to perform the search operation.";
        }
    }
}
