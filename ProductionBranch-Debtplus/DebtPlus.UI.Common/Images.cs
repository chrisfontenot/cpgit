using System.Drawing;
using System.Reflection;
using System.IO;

namespace DebtPlus.UI.Common
{
    /// <summary>
    /// Class for the images
    /// </summary>
    public static class Images
    {
        /// <summary>
        /// Search button image
        /// </summary>
        public static System.Drawing.Image Search
        {
            get
            {
                System.Drawing.Image answer = null;

                // Load the search bitmap from the resource information
                Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
                using (System.IO.Stream ios = asm.GetManifestResourceStream("DebtPlus.UI.Common.Find.bmp"))
                {
                    if (ios != null)
                    {
                        try // Do not die if the image can not be loaded.
                        {
                            answer = new System.Drawing.Bitmap(ios);
                        }
                        catch { } // Handle the error conditions by ignoring them. The default is sufficient.
                        ios.Close();
                    }
                }

                // If there is no bitmap then create a blank bitmap of 1x1 in size
                if (answer == null)
                {
                    answer = new System.Drawing.Bitmap(1, 1);
                }
                return answer;
            }
        }
    }
}
