﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// This file should be deleted in the future once the update routines have been converted
// from using ADO.NET to LINQ for database accesses.

namespace DebtPlus.UI.Common.Templates.ADO.Net
{
    public class EditTemplateForm : DebtPlus.UI.Common.Templates.EditTemplateForm
    {
        public System.Data.DataRowView drv;
        public Context ctx;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public EditTemplateForm() : base() { }

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="drv"></param>
        public EditTemplateForm(Context ctx, System.Data.DataRowView drv)
            : base()
        {
            this.ctx = ctx;
            this.drv = drv;
        }
    }
}
