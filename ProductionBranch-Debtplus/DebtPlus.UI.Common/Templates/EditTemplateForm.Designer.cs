﻿namespace DebtPlus.UI.Common.Templates
{
    partial class EditTemplateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // SimpleButton_Cancel
            // 
            this.SimpleButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(208, 75);
            this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
            this.SimpleButton_Cancel.Size = new System.Drawing.Size(72, 26);
            this.SimpleButton_Cancel.TabIndex = 21;
            this.SimpleButton_Cancel.Text = "&Cancel";
            this.SimpleButton_Cancel.ToolTip = "Click here to cancel the dialog and return to the previous form.";
            this.SimpleButton_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // SimpleButton_OK
            // 
            this.SimpleButton_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SimpleButton_OK.Enabled = false;
            this.SimpleButton_OK.Location = new System.Drawing.Point(208, 43);
            this.SimpleButton_OK.Name = "SimpleButton_OK";
            this.SimpleButton_OK.Size = new System.Drawing.Size(72, 26);
            this.SimpleButton_OK.TabIndex = 20;
            this.SimpleButton_OK.Text = "&OK";
            this.SimpleButton_OK.ToolTip = "Click here to commit the changes to the database";
            this.SimpleButton_OK.ToolTipController = this.ToolTipController1;
            // 
            // EditTemplateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 140);
            this.Controls.Add(this.SimpleButton_Cancel);
            this.Controls.Add(this.SimpleButton_OK);
            this.Name = "EditTemplateForm";
            this.Text = "Edit Item";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
        public DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
    }
}