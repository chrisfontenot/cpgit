﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Common.Templates
{
    public partial class MainTemplateForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Row when the popup items are being processed.
        protected Int32 ControlRow = -1;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public MainTemplateForm() : base()
        {
            InitializeComponent();
            RegisterEvents();
        }

        /// <summary>
        /// Register for the events
        /// </summary>
        private void RegisterEvents()
        {
            SimpleButton_Cancel.Click += SimpleButton_Cancel_Click;
            SimpleButton_Edit.Click += SimpleButton_Edit_Click;
            SimpleButton_New.Click += SimpleButton_New_Click;
            SimpleButton_SaveChanges.Click += SimpleButton_SaveChanges_Click;

            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.MouseDown += GridView1_MouseDown;

            barButtonItem_Add.ItemClick += barButtonItem_Add_ItemClick;
            barButtonItem_Delete.ItemClick += barButtonItem_Delete_ItemClick;
            barButtonItem_Edit.ItemClick += barButtonItem_Edit_ItemClick;
        }

        /// <summary>
        /// Remove the event registrations
        /// </summary>
        private void UnRegisterEvents()
        {
            SimpleButton_Cancel.Click -= SimpleButton_Cancel_Click;
            SimpleButton_Edit.Click -= SimpleButton_Edit_Click;
            SimpleButton_New.Click -= SimpleButton_New_Click;
            SimpleButton_SaveChanges.Click -= SimpleButton_SaveChanges_Click;

            GridView1.DoubleClick -= GridView1_DoubleClick;
            GridView1.MouseDown -= new MouseEventHandler(GridView1_MouseDown);

            barButtonItem_Add.ItemClick -= new DevExpress.XtraBars.ItemClickEventHandler(barButtonItem_Add_ItemClick);
            barButtonItem_Delete.ItemClick -= new DevExpress.XtraBars.ItemClickEventHandler(barButtonItem_Delete_ItemClick);
            barButtonItem_Edit.ItemClick -= new DevExpress.XtraBars.ItemClickEventHandler(barButtonItem_Edit_ItemClick);
        }

        /// <summary>
        /// Process the MOUSE DOWN event on the grid control. Do the popup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                // Find the location of the click event
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo(GridControl1.PointToClient(MousePosition));
                if (hi.InRow)
                {
                    ControlRow = hi.RowHandle;
                    object obj = GridView1.GetRow(ControlRow);
                    barButtonItem_Edit.Enabled = EnableEdit(obj);
                    barButtonItem_Delete.Enabled = EnableDelete(obj);
                }
                else
                {
                    ControlRow = -1;
                    barButtonItem_Edit.Enabled = false;
                    barButtonItem_Delete.Enabled = false;
                }
                barButtonItem_Add.Enabled = true;

                // Display the popup menu at the current mouse position
                popupMenu1.ShowPopup(MousePosition);
            }
        }

        /// <summary>
        /// Should the edit be allowed from the popup list
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected virtual bool EnableEdit(object obj)
        {
            return obj != null;
        }

        /// <summary>
        /// Should the delete be allowed from the popup list?
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        protected virtual bool EnableDelete(object obj)
        {
            return obj != null;
        }

        /// <summary>
        /// Process the CLICK event on the EDIT button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void SimpleButton_Edit_Click(object sender, EventArgs e) // Handles SimpleButton_Edit.Click
        {
            System.Int32 rowHandle = GridView1.FocusedRowHandle;

            // Find the object for the edit operation
            object obj = null;
            if (rowHandle >= 0)
            {
                GridView1.FocusedRowHandle = rowHandle;
                obj = GridView1.GetRow(rowHandle);
            }

            // If there is a row then edit it
            if (obj != null)
            {
                OnEdit(obj);
            }
        }

        /// <summary>
        /// Process the CLICK event on the POPUP menu EDIT item
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private void barButtonItem_Edit_ItemClick(object obj, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            object objItem = GridView1.GetRow(ControlRow);
            if (objItem != null)
            {
                OnEdit(objItem);
            }
            ControlRow = -1;
        }

        /// <summary>
        /// Process the CLICK event on the POPUP menu CREATE item
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        protected virtual void barButtonItem_Add_ItemClick(object obj, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ControlRow = -1;
            OnCreate();
        }

        /// <summary>
        /// Process the CLICK event on the NEW button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void SimpleButton_New_Click(object sender, EventArgs e) // Handles SimpleButton_New.Click
        {
            ControlRow = -1;
            OnCreate();
        }

        /// <summary>
        /// Process the CLICK event on the POPUP menu DELETE item
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="e"></param>
        private void barButtonItem_Delete_ItemClick(object obj, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            object objItem = GridView1.GetRow(ControlRow);
            if (objItem != null)
            {
                OnDelete(objItem);
            }
            ControlRow = -1;
        }

        /// <summary>
        /// Process a CLICK event on the CANCEL button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void SimpleButton_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            if (!Modal)
            {
                Close();
            }
        }

        /// <summary>
        /// Enable or disable the EDIT button when the focused row changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SimpleButton_Edit.Enabled = e.FocusedRowHandle >= 0;
        }

        /// <summary>
        /// Process a DOUBLE-CLICK event on the grid to edit the row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)));
            System.Int32 rowHandle = hi.RowHandle;

            // Find the object for the edit operation
            object obj = null;
            if (rowHandle >= 0)
            {
                GridView1.FocusedRowHandle = rowHandle;
                obj = GridView1.GetRow(rowHandle);
            }

            // If there is a row then edit it
            if (obj != null)
            {
                OnEdit(obj);
            }
        }

        /// <summary>
        /// Handle the edit of the object row
        /// </summary>
        /// <param name="obj"></param>
        protected virtual void OnEdit(object obj)
        {
        }

        /// <summary>
        /// Handle the deletion of an object row
        /// </summary>
        /// <param name="obj"></param>
        protected virtual void OnDelete(object obj)
        {
        }

        /// <summary>
        /// Handle the creation of new row
        /// </summary>
        protected virtual void OnCreate()
        {
        }

        /// <summary>
        /// Process the CLICK event on the SAVE button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>This is an empty procedure and is meant to be overridden by the implementation.</remarks>
        protected virtual void SimpleButton_SaveChanges_Click(object sender, EventArgs e)
        {
        }
    }
}
