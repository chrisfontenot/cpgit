﻿using System;

namespace DebtPlus.UI.Common.Templates
{
    public partial class EditTemplateForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public EditTemplateForm() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            SimpleButton_OK.Click += SimpleButton_OK_Click;
            SimpleButton_Cancel.Click += SimpleButton_Cancel_Click;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            SimpleButton_OK.Click -= SimpleButton_OK_Click;
            SimpleButton_Cancel.Click -= SimpleButton_Cancel_Click;
        }

        /// <summary>
        /// Handle a click on the CANCEL button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void SimpleButton_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// Handle a click on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
