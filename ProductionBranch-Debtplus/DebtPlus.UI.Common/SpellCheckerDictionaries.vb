Imports System.IO
Imports System.Globalization
Imports System.Windows.Forms
Imports Microsoft.Win32
Imports DebtPlus.Utils
Imports System.Xml.Serialization
Imports System.Xml
Imports DevExpress.XtraSpellChecker

Public Class SpellCheckerDictionaries
    Private Sub New()
        ' can't create this static class
    End Sub

    ''' <summary>
    '''     Define the dictionaries for the current speller
    ''' </summary>
    Public Shared Sub SetSpellCheckerDictionaries(ByRef ctl As SharedDictionaryStorage)
        Try
            LoadCustomWords(ctl)
            LoadLanguageDictionaries(ctl)
        Catch ex As Exception
            DebtPlus.Data.Errors.ExceptionMessageBoxDialog(ex, "Error loading spelling dictionaries")
        End Try
    End Sub

    ''' <summary>
    '''     Define the custom word list dictionaries
    ''' </summary>
    Private Shared Sub LoadCustomWords(ByRef ctl As SharedDictionaryStorage)
        If Not LoadOfficeWords(ctl) Then
            LoadDefaultWords(ctl)
        End If
    End Sub

    ''' <summary>
    '''     Define the Microsoft Office custom word list dictionaries
    ''' </summary>
    Private Shared Function LoadOfficeWords(ByRef ctl As SharedDictionaryStorage) As Boolean
        Dim pathName As String
        Dim answer As Boolean = False

        '-- Use the registry location to try to find the custom dictionaries for "Microsoft Word". If you don't have
        '-- word installed, well too bad ....
        Dim reg As RegistryKey = Microsoft.Win32.Registry.CurrentUser
        Try
            reg = reg.OpenSubKey("Software\Microsoft\Shared Tools\Proofing Tools\Custom Dictionaries", False)
            If reg IsNot Nothing Then

                '-- We choose the first item. You can have more than one, but the first item is all that we use.
                Dim names() As String = reg.GetValueNames()
                For Each keyName As String In names
                    Dim fileName As String = Convert.ToString(reg.GetValue(keyName))

                    If Not Path.IsPathRooted(fileName) Then
                        Dim applicationDataFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
                        pathName = Path.Combine(applicationDataFolder, "Microsoft\Proof")
                        fileName = Path.Combine(pathName, fileName)
                    End If

                    LoadCustomWordList(ctl, fileName)
                    answer = True
                Next
            End If

        Finally
            If reg IsNot Nothing Then reg.Close()
        End Try

        Return answer
    End Function

    ''' <summary>
    '''     Define the custom word list dictionary in the Documents directory
    ''' </summary>
    Private Shared Sub LoadDefaultWords(ByRef ctl As SharedDictionaryStorage)

        '-- Create a path to the location documents directory
        Dim PathName As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim FileName As String = Path.Combine(PathName, "Custom Spelling Word List.txt")

        '-- Create the file if it does not exist
        If Not File.Exists(FileName) Then
            Dim sr As FileStream = Nothing
            Try
                sr = File.Create(FileName)

            Catch ex As IOException
                DebtPlus.Data.Errors.DebuggingException(ex)

            Finally
                If sr IsNot Nothing Then
                    sr.Close()
                    sr.Dispose()
                End If
            End Try
        End If

        '-- Create the dictionary against the file
        Dim CurrentCulture As CultureInfo = CultureInfo.CurrentCulture
        Dim Dictionary As New SpellCheckerCustomDictionary(FileName, CurrentCulture)
        ctl.Dictionaries.Add(Dictionary)
    End Sub

    ''' <summary>
    '''     Create dictionary object for a custom word list file
    ''' </summary>
    Private Shared Sub LoadCustomWordList(ByRef ctl As SharedDictionaryStorage, ByVal FileName As String)

        '-- Ensure that the path exists to the file
        Dim PathName As String = Path.GetDirectoryName(FileName)
        If Not Directory.Exists(PathName) Then
            Directory.CreateDirectory(PathName)
        End If

        '-- Create the file if it does not exist in the directory
        If Not File.Exists(FileName) Then
            Dim sr As FileStream = Nothing
            Try
                sr = File.Create(FileName)

            Catch ex As IOException
                DebtPlus.Data.Errors.DebuggingException(ex)

            Finally
                If sr IsNot Nothing Then
                    sr.Close()
                    sr.Dispose()
                End If
            End Try
        End If

        '-- Create the dictionary against the file
        Dim CurrentCulture As CultureInfo = CultureInfo.CurrentCulture
        Dim Dictionary As New SpellCheckerCustomDictionary(FileName, CurrentCulture)
        ctl.Dictionaries.Add(Dictionary)
    End Sub

    ''' <summary>
    '''     Load the standard list of dictionary words
    ''' </summary>
    Private Shared Sub LoadLanguageDictionaries(ByRef ctl As SharedDictionaryStorage)
        Dim CommonFolderName As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles)
        Dim PathName As String = Path.Combine(CommonFolderName, "DebtPlus\Dictionaries")
        If Directory.Exists(PathName) Then
            Dim Folders() As String = Directory.GetDirectories(PathName)
            For Each folder As String In Folders
                LoadLanguageDictionary(ctl, folder)
            Next
        End If
    End Sub

    ''' <summary>
    '''     Load a specific dictionary word list
    ''' </summary>
    Private Shared Sub LoadLanguageDictionary(ByRef ctl As SharedDictionaryStorage, ByVal FolderName As String)
        Dim Dict As Dictionary = ReadDictionaryObject(FolderName)
        If Dict.Enabled Then

            '-- For now, we only support Open Office formats
            If String.Compare(Dict.Format, "OpenOffice", True) <> 0 Then
                Throw New ApplicationException("Only OPENOFFICE dictionary formats are supported at this time")
            End If

            '-- Create the spelling dictionary object and set the culture
            Dim LanguageDictionary As New SpellCheckerOpenOfficeDictionary()
            LanguageDictionary.Culture = New CultureInfo(Dict.Culture)

            Dim dicStream As Stream = Nothing
            Dim affStream As Stream = Nothing
            Try
                dicStream = Dict.GetDICStream(FolderName)
                affStream = Dict.GetAFFStream(FolderName)
                LanguageDictionary.LoadFromStream(dicStream, affStream, Nothing)
                ctl.Dictionaries.Add(LanguageDictionary)

            Finally
                If affStream IsNot Nothing Then
                    affStream.Close()
                    affStream.Dispose()
                End If

                If dicStream IsNot Nothing Then
                    dicStream.Close()
                    dicStream.Dispose()
                End If
            End Try

        End If
    End Sub

    ''' <summary>
    '''     Read the Dictionary.xml file and generate a Dictionary object
    ''' </summary>
    Private Shared Function ReadDictionaryObject(ByVal PathName As String) As Dictionary
        Dim answer As Dictionary = Nothing
        Dim FileName As String = Path.Combine(PathName, "Dictionary.xml")

        If File.Exists(FileName) Then
            Using fs As New FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read, 4096)
                Try
                    Dim ser As New XmlSerializer(GetType(Dictionary()), String.Empty)
                    answer = CType(ser.Deserialize(fs), Dictionary)

                Catch ex As XmlException
                    DebtPlus.Data.Forms.MessageBox.Show(String.Format("The file {0} is not a valid XML document", FileName), "Error loading dictionary objects", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End Using
        End If

        If answer Is Nothing Then
            answer = DefaultDictionary(PathName)
        End If

        Return answer
    End Function

    ''' <summary>
    '''     Create a default dictionary object from the path files
    ''' </summary>
    Private Shared Function DefaultDictionary(ByVal PathName As String) As Dictionary
        Dim answer As New Dictionary()

        '-- Look for the files in the directory to fill in the defaults
        Dim Files() As String = Directory.GetFiles(PathName, "*.aff")
        If Files.GetUpperBound(0) >= 0 Then
            answer.aff = Path.GetFileName(Files(0))
        End If

        Files = Directory.GetFiles(PathName, "*.dic")
        If Files.GetUpperBound(0) >= 0 Then
            answer.dic = Path.GetFileName(Files(0))
        End If

        '-- If the files are present then enable the dictionary item
        answer.Enabled = answer.aff <> String.Empty AndAlso answer.dic <> String.Empty
        Return answer
    End Function

End Class
