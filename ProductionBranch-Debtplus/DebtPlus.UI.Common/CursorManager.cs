﻿using System;
using System.Windows.Forms;

namespace DebtPlus.UI.Common
{
    /// <summary>
    /// Class to implement the waiting cursor
    /// </summary>
    public class WaitCursor : CursorManager
    {
        public WaitCursor()
            : base(System.Windows.Forms.Cursors.WaitCursor)
        {
        }
    }

    /// <summary>
    /// Class to implement the application starting cursor
    /// </summary>
    public class AppStartingCursor : CursorManager
    {
        public AppStartingCursor()
            : base(System.Windows.Forms.Cursors.AppStarting)
        {
        }
    }

    /// <summary>
    /// Class for the cursor management
    /// </summary>
    public class CursorManager : IDisposable
    {
        private Cursor desiredCursor;
        private Cursor currentCursor;
        private bool cursorSet;

        /// <summary>
        /// Initialize the class. Set the cursor to the waiting state.
        /// </summary>
        public CursorManager()
        {
            desiredCursor = System.Windows.Forms.Cursors.WaitCursor;
            SetCursor();
        }

        /// <summary>
        /// Initialize the class. Set the cursor to the desired cursor.
        /// </summary>
        public CursorManager(System.Windows.Forms.Cursor DesiredCursor)
        {
			this.desiredCursor = DesiredCursor;
            SetCursor();
        }

        /// <summary>
        /// Reset the cursor without disposing of the class. It is used when the cursor
        /// is to be restored to the previous settings before the dispose event is tripped.
        /// </summary>
        public void ResetCursor()
        {
            if (cursorSet)
            {
                System.Windows.Forms.Cursor.Current = currentCursor;
                cursorSet = false;
            }
        }

        /// <summary>
        /// If the cursor has been reset then it may be set again by calling this routine.
        /// </summary>
        public void SetCursor()
        {
            if (!cursorSet)
            {
                currentCursor = System.Windows.Forms.Cursor.Current;
                System.Windows.Forms.Cursor.Current = desiredCursor;
                cursorSet = true;

                // Force the cursor to be displayed immediately
                try
                {
                    var handle = GetForegroundWindow();
                    SendMessage(handle, 0x20, handle, (IntPtr)1);
                }
                catch { }  // Ignore errors as they would be too low to bother with.
                           // so the cursor update does not happen immediately. It will soon enough.
            }
        }

        /// <summary>
        /// Prevent the dispose from being performed twice
        /// </summary>
        private bool disposed = false;

        /// <summary>
        /// Handle the Dispose and Finalize events
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                disposed = true;
                ResetCursor();
            }
        }

        /// <summary>
        /// Dispose of the storage for the class.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Support the Finalize event with this routine
        /// </summary>
        ~CursorManager()
        {
            Dispose(false);
        }

        // The SendMessage function comes from User32....
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);

        // Routine to find the foreground window also is in user32....
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
    }
}
