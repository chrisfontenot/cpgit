using DebtPlus.Repository;
using System;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes
{
    /// <summary>
    /// Class to handle the notes in the system
    /// </summary>
    public sealed partial class NoteClass
    {
        /// <summary>
        /// Client note processing
        /// </summary>
        public class ClientNote : BaseClass
        {
            // Use for logging purposes if needed
            private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            public bool IsArchivedNote { get; set; }

            // Dataset and table name
            protected const string TableName = "client_notes";

            /// <summary>
            /// Read the client note from the system into local storage.
            /// </summary>
            /// <param name="noteID">The ID of the creditor note</param>
            /// <returns>The note or nothing if the note is not found.</returns>
            /// <remarks></remarks>
            protected combined_note ReadClientNote(Int32 noteID)
            {
                if (IsArchivedNote)
                {
                    using (var ac = new ArchiveContext())
                    {
                        ac.DeferredLoadingEnabled = true;
                        return (from n in ac.client_notes where n.Id == noteID select new combined_note(n)).FirstOrDefault();
                    }
                }
                else
                {
                    return (from n in bc.client_notes where n.Id == noteID select new combined_note(n)).FirstOrDefault();
                }
            }

            /// <summary>
            /// Edit the client note
            /// </summary>
            /// <param name="drv">Pointer to the note to be used.</param>
            /// <param name="EnableEdit">Do we permit the note to be modified? TRUE for edit. FALSE for show.</param>
            /// <param name="EnableFlags">Do we want the flags (dont_edit/delete/print) to be enabled?</param>
            /// <returns></returns>
            protected virtual System.Windows.Forms.DialogResult EditClientNote(combined_note n, bool EnableEdit, bool EnableFlags)
            {
                // Perform the edit operation on the row
                using (var EditForm = new EditNoteForm(bc, EnableEdit, EnableFlags))
                {
                    EditForm.ValidTypes.Clear();
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent);
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Temporary);
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert);
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.HUD);

                    if (n.IsNew) // This is valid only on a newly created client note
                    {
                        EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement);
                    }

                    // Fill out the editing form with the note information
                    EditForm.Text = FormText();
                    EditForm.note = n;
                    n.IsDirty = false;

                    // if the document was not dirty then the request was canceled.
                    System.Windows.Forms.DialogResult Answer = EditForm.ShowDialog();
                    if (!n.IsDirty)
                    {
                        Answer = System.Windows.Forms.DialogResult.Cancel;
                    }
                    return Answer;
                }
            }

            /// <summary>
            /// Save the note to the database from the edit operation.
            /// </summary>
            /// <param name="note">Pointer to the note to be saved</param>
            /// <returns>TRUE if successful and the note was updated. FALSE otherwise.</returns>
            protected bool SaveClientNote(combined_note note)
            {
                try
                {
                    client_note q;
                    if (note.IsNew)
                    {
                        q = DebtPlus.LINQ.Factory.Manufacture_client_note(note.client.Value, note.type);
                        q.client_creditor = note.client_creditor;
                        bc.client_notes.InsertOnSubmit(q);
                    }
                    else
                    {
                        q = bc.client_notes.Where(n => n.Id == note.Id).FirstOrDefault();
                        if (q == null)
                        {
                            return false;
                        }
                    }

                    // Supply the values for the note.
                    q.type            = note.type;
                    q.subject         = note.subject;
                    q.dont_print      = note.dont_print;
                    q.dont_edit       = note.dont_edit;
                    q.dont_delete     = note.dont_delete;
                    q.note            = note.note;
                    q.expires         = note.expires;
                    q.client          = note.client.Value;
                    q.client_creditor = note.client_creditor;

                    // Do the changes and return success
                    bc.SubmitChanges();
                    return true;
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    log.Error("Error updating client_note", ex);
                    return false;
                }
            }

            /// <summary>
            /// Save the note to the disbursement note table
            /// </summary>
            /// <param name="note">Pointer to the note to be saved</param>
            /// <returns>TRUE if successful and the note was updated. FALSE otherwise.</returns>
            protected bool SaveDisbursementNote(combined_note note)
            {
                try
                {
                    disbursement_note q;
                    if (note.IsNew)
                    {
                        q = new disbursement_note();
                        bc.disbursement_notes.InsertOnSubmit(q);
                    }
                    else
                    {
                        q = bc.disbursement_notes.Where(n => n.Id == note.Id).FirstOrDefault();
                        if (q == null)
                        {
                            return false;
                        }
                    }

                    // Supply the values for the note.
                    q.type = note.type;
                    q.subject = note.subject;
                    q.dont_print = note.dont_print;
                    q.dont_edit = note.dont_edit;
                    q.dont_delete = note.dont_delete;
                    q.note = note.note;
                    q.client = note.client.Value;
                    q.disbursement_register = note.disbursement_register;
                    q.deposit_batch_id = note.deposit_batch_id;

                    // Do the changes and return success
                    bc.SubmitChanges();
                    return true;
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    log.Error("Error updating disbursement_note", ex);
                    return false;
                }
            }

            /// <summary>
            /// Text for the edit form. It needs to say Client Note.
            /// </summary>
            /// <returns></returns>
            protected virtual string FormText()
            {
                return "Client Note";
            }

            /// <summary>
            /// Display but do not allow edit for a client note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            public override bool Show(Int32 noteID)
            {
                combined_note note = ReadClientNote(noteID);
                if (note != null)
                {
                    EditClientNote(note, false, false);
                }

                return false; // this is always FALSE since we did not change the note.
            }

            /// <summary>
            /// Edit the indicated client note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Edit(Int32 noteID)
            {
                // Clear the current note from the dataset if present
                combined_note note = ReadClientNote(noteID);
                if (note != null)
                {
                    if (EditClientNote(note, note.EnableEdit(), note.EnableFlags()) == System.Windows.Forms.DialogResult.OK)
                    {
                        return SaveClientNote(note);
                    }
                }

                return false;
            }

            /// <summary>
            /// Create the client note
            /// </summary>
            /// <param name="Client">Client ID associated with this new note.</param>
            /// <returns></returns>
            public virtual bool Create(Int32 Client)
            {
                Int32? client_creditor = new Int32?();
                return CreateProcedure(Client, client_creditor);
            }

            /// <summary>
            /// Create the client note
            /// </summary>
            /// <param name="Client">Client ID associated with this new note.</param>
            /// <param name="ClientCreditor">Debt record ID associated with this new note.</param>
            /// <returns></returns>
            protected bool CreateProcedure(Int32 Client, Int32? ClientCreditor)
            {
                // Create the new note for this client/debt note
                combined_note n = new combined_note()
                {
                    client = Client,
                    client_creditor = ClientCreditor,
                    type = 1,
                    note = DefaultNote,
                    subject = DefaultSubject,
                    expires = DefaultExpires,
                    dont_delete = DefaultDontDelete,
                    dont_print = DefaultDontPrint,
                    dont_edit = DefaultDontEdit,
                    created_by = DefaultCreatedBy,
                    date_created = DefaultDateCreated,
                    IsDirty = false
                };

                if (EditClientNote(n, true, true) == System.Windows.Forms.DialogResult.OK)
                {
                    // Disbursement notes go to the disbursement notes table
                    if ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)n.type == DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement)
                    {
                        if (SaveDisbursementNote(n))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        // Others go to the client_notes table
                        if (SaveClientNote(n))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            /// <summary>
            /// Delete the indicated client note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Delete(Int32 noteID)
            {
                // Find the note in the table. Use only portions to find the note's ability to delete.
                var q = bc.client_notes.Where(n => n.Id == noteID).FirstOrDefault();
                if (q == null)
                {
                    return false;
                }

                // Determine if we can delete the note. If not authorized then complain.
                var note = new combined_note(q);
                if (!note.EnableDelete())
                {
                    DebtPlus.Data.Forms.MessageBox.Show(STR_YouAreNotAuthorizedToDeleteThisNoteAtThisTime, STR_NoteDeletionSecurityViolation, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return false;
                }

                // Confirm the deletion of the note.
                if (confirm_delete())
                {
                    bc.client_notes.DeleteOnSubmit(q);
                    bc.SubmitChanges();
                    return true;
                }

                // The note was not deleted.
                return false;
            }
        }
    }
}