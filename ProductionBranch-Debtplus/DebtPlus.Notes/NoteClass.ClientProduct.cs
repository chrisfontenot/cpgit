using DebtPlus.Repository;
using System;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes
{
    /// <summary>
    /// Class to handle the notes in the system
    /// </summary>
    public sealed partial class NoteClass
    {
        /// <summary>
        /// Client product note processing
        /// </summary>
        public class ClientProductNote : BaseClass
        {
            // Use for logging purposes if needed
            private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            public bool IsArchivedNote { get; set; }

            // Dataset and table name
            protected const string TableName = "client_product_notes";

            /// <summary>
            /// Read the client note from the system into local storage.
            /// </summary>
            /// <param name="noteID">The ID of the creditor note</param>
            /// <returns>The note or nothing if the note is not found.</returns>
            /// <remarks></remarks>
            protected combined_note ReadNote(Int32 noteID)
            {
                return (from n in bc.client_product_notes where n.Id == noteID select new combined_note(n)).FirstOrDefault();
            }

            /// <summary>
            /// Edit the client note
            /// </summary>
            /// <param name="drv">Pointer to the note to be used.</param>
            /// <param name="EnableEdit">Do we permit the note to be modified? TRUE for edit. FALSE for show.</param>
            /// <param name="EnableFlags">Do we want the flags (dont_edit/delete/print) to be enabled?</param>
            /// <returns></returns>
            protected virtual System.Windows.Forms.DialogResult EditNote(combined_note n, bool EnableEdit, bool EnableFlags)
            {
                // Perform the edit operation on the row
                using (var EditForm = new EditNoteForm(bc, EnableEdit, EnableFlags))
                {
                    EditForm.ValidTypes.Clear();
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent);

                    // Fill out the editing form with the note information
                    EditForm.Text = FormText();
                    EditForm.note = n;
                    n.IsDirty     = false;

                    // if the document was not dirty then the request was canceled.
                    System.Windows.Forms.DialogResult Answer = EditForm.ShowDialog();
                    if (!n.IsDirty)
                    {
                        Answer = System.Windows.Forms.DialogResult.Cancel;
                    }
                    return Answer;
                }
            }

            /// <summary>
            /// Save the note to the database from the edit operation.
            /// </summary>
            /// <param name="note">Pointer to the note to be saved</param>
            /// <returns>TRUE if successful and the note was updated. FALSE otherwise.</returns>
            protected bool SaveNote(combined_note note)
            {
                try
                {
                    client_product_note q;
                    if (note.IsNew)
                    {
                        q                = DebtPlus.LINQ.Factory.Manufacture_client_product_note();
                        q.client_product = note.client_product.Value;
                        bc.client_product_notes.InsertOnSubmit(q);
                    }
                    else
                    {
                        q = bc.client_product_notes.Where(n => n.Id == note.Id).FirstOrDefault();
                        if (q == null)
                        {
                            return false;
                        }
                    }

                    // Supply the values for the note.
                    q.type            = note.type;
                    q.subject         = note.subject;
                    q.dont_print      = note.dont_print;
                    q.dont_edit       = note.dont_edit;
                    q.dont_delete     = note.dont_delete;
                    q.note            = note.note;

                    // Do the changes and return success
                    bc.SubmitChanges();
                    return true;
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    log.Error("Error updating client_product_note", ex);
                    return false;
                }
            }

            /// <summary>
            /// Text for the edit form. It needs to say Client Note.
            /// </summary>
            /// <returns></returns>
            protected virtual string FormText()
            {
                return "Client Product Note";
            }

            /// <summary>
            /// Display but do not allow edit for a client note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            public override bool Show(Int32 noteID)
            {
                combined_note note = ReadNote(noteID);
                if (note != null)
                {
                    EditNote(note, false, false);
                }

                return false; // this is always FALSE since we did not change the note.
            }

            /// <summary>
            /// Edit the indicated client note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Edit(Int32 noteID)
            {
                // Clear the current note from the dataset if present
                combined_note note = ReadNote(noteID);
                if (note == null)
                {
                    return false;
                }

                if (EditNote(note, note.EnableEdit(), note.EnableFlags()) == System.Windows.Forms.DialogResult.OK)
                {
                    return SaveNote(note);
                }

                return false;
            }

            /// <summary>
            /// Create the client note
            /// </summary>
            /// <param name="ClientProductID">Client Product ID associated with this new note.</param>
            /// <returns></returns>
            public virtual bool Create(Int32 ClientProductID)
            {
                return CreateProcedure(ClientProductID);
            }

            /// <summary>
            /// Create the client note
            /// </summary>
            /// <param name="Client">Client ID associated with this new note.</param>
            /// <param name="ClientCreditor">Debt record ID associated with this new note.</param>
            /// <returns></returns>
            protected bool CreateProcedure(Int32 ClientProductID)
            {
                // Create the new note for this client/debt note
                combined_note n = new combined_note()
                {
                    client_product = ClientProductID,
                    type           = (System.Int32) DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent,
                    note           = DefaultNote,
                    subject        = DefaultSubject,
                    expires        = DefaultExpires,
                    dont_delete    = DefaultDontDelete,
                    dont_print     = DefaultDontPrint,
                    dont_edit      = DefaultDontEdit,
                    created_by     = DefaultCreatedBy,
                    date_created   = DefaultDateCreated,
                    IsDirty        = false
                };

                if (EditNote(n, true, true) == System.Windows.Forms.DialogResult.OK)
                {
                    // Others go to the client_notes table
                    if (SaveNote(n))
                    {
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// Delete the indicated client note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Delete(Int32 noteID)
            {
                // Find the note in the table. Use only portions to find the note's ability to delete.
                var q = bc.client_product_notes.Where(n => n.Id == noteID).FirstOrDefault();
                if (q == null)
                {
                    return false;
                }

                // Determine if we can delete the note. If not authorized then complain.
                combined_note note = new combined_note(q);
                if (!note.EnableDelete())
                {
                    DebtPlus.Data.Forms.MessageBox.Show(STR_YouAreNotAuthorizedToDeleteThisNoteAtThisTime, STR_NoteDeletionSecurityViolation, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return false;
                }

                // Confirm the deletion of the note.
                if (confirm_delete())
                {
                    bc.client_product_notes.DeleteOnSubmit(q);
                    bc.SubmitChanges();
                    return true;
                }

                // The note was not deleted.
                return false;
            }
        }
    }
}