using System;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes
{
    public sealed partial class NoteClass
    {
        /// <summary>
        /// Disbursement note processing
        /// </summary>
        public class DisbursementNote : ClientNote
        {
            // Use for logging purposes if needed
            private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            /// <summary>
            /// Create a new instance of our class
            /// </summary>
            public DisbursementNote()
                : base()
            {
            }

            /// <summary>
            /// Read the client note from the system into local storage.
            /// </summary>
            /// <param name="noteID">The ID of the disbursement note</param>
            /// <returns>The note or nothing if the note is not found.</returns>
            /// <remarks></remarks>
            protected combined_note ReadDisbursementNote(Int32 noteID)
            {
                return (from n in bc.disbursement_notes where n.Id == noteID select new combined_note(n)).FirstOrDefault();
            }

            /// <summary>
            /// Read the client note from the system into local storage.
            /// </summary>
            /// <param name="Client">The client ID desired</param>
            /// <param name="DepositBatchID">The deposit batch ID. A client has at most one note for each deposit batch.</param>
            /// <returns></returns>
            /// <remarks></remarks>
            protected combined_note ReadDisbursementNote(Int32 Client, Int32? DepositBatchID)
            {
                if (!DepositBatchID.HasValue)
                {
                    return null;
                }

                return (from n in bc.disbursement_notes where n.client == Client && n.deposit_batch_id == DepositBatchID.Value select new combined_note(n)).FirstOrDefault();
            }

            /// <summary>
            /// Edit the disbursement note record.
            /// </summary>
            /// <param name="drv">Pointer to the note to be used.</param>
            /// <param name="EnableEdit">Do we permit the note to be modified? TRUE for edit. FALSE for show.</param>
            /// <param name="EnableFlags">Do we want the flags (dont_edit/delete/print) to be enabled?</param>
            /// <returns></returns>
            /// <remarks></remarks>
            private System.Windows.Forms.DialogResult EditDisbursementNote(combined_note n, bool EnableEdit, bool EnableFlags)
            {
                // Perform the edit operation on the row
                using (var EditForm = new EditNoteForm(bc, EnableEdit, EnableFlags))
                {
                    // Only valid type is "disbursement"
                    EditForm.ValidTypes.Clear();
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement);

                    // Fill out the editing form with the note information
                    EditForm.Text = FormText();
                    EditForm.note = n;
                    n.IsDirty = false;

                    // if the document was not dirty then the request was canceled.
                    System.Windows.Forms.DialogResult Answer = EditForm.ShowDialog();
                    if (!n.IsDirty)
                    {
                        Answer = System.Windows.Forms.DialogResult.Cancel;
                    }
                    return Answer;
                }
            }

            /// <summary>
            /// Define the subject for new notes.
            /// </summary>
            protected override string DefaultSubject
            {
                get
                {
                    return "Disbursement Note";
                }
            }

            /// <summary>
            /// Edit the specific disbursement note.
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Edit(Int32 noteID)
            {
                combined_note n = ReadDisbursementNote(noteID);
                if (n != null)
                {
                    if (EditDisbursementNote(n, n.EnableEdit(), n.EnableFlags()) == System.Windows.Forms.DialogResult.OK)
                    {
                        return SaveDisbursementNote(n);
                    }
                }

                return false;
            }

            /// <summary>
            /// Display but do not allow the edit of the disbursement note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Show(Int32 noteID)
            {
                combined_note n = ReadDisbursementNote(noteID);
                if (n != null)
                {
                    EditDisbursementNote(n, false, false);
                }

                return false; // this is always FALSE since we did not change the note.
            }

            /// <summary>
            /// Create a new note for the client. No batch is mentioned.
            /// </summary>
            /// <param name="Client">Client ID associated with this new note.</param>
            /// <returns></returns>
            public override bool Create(Int32 Client)
            {
                Int32? DepositBatch = new Int32?();
                return Create(Client, DepositBatch);
            }

            /// <summary>
            /// Create a new note for the client. A deposit batch is mentioned. There can only be
            /// one disbursement note for a given client in a given deposit batch so we do an "edit"
            /// operation if the user attempts to create a second note.
            /// </summary>
            /// <param name="Client">Client ID associated with this new note.</param>
            /// <param name="DepositBatch">Batch ID of the deposit.</param>
            /// <returns></returns>
            public virtual bool Create(Int32 Client, Int32? DepositBatch)
            {
                // Retrieve the note if we have one. If so, do an edit.
                if (DepositBatch.HasValue)
                {
                    combined_note oldNote = ReadDisbursementNote(Client, DepositBatch);
                    if (oldNote != null)
                    {
                        if (EditDisbursementNote(oldNote, oldNote.EnableEdit(), oldNote.EnableFlags()) == System.Windows.Forms.DialogResult.OK)
                        {
                            return SaveDisbursementNote(oldNote);
                        }
                        return false;
                    }
                }

                // Do the create operation at this point.
                combined_note newNote = new combined_note()
                {
                    type = 5,
                    client = Client,
                    deposit_batch_id = DepositBatch,
                    date_created = DefaultDateCreated,
                    dont_delete = DefaultDontDelete,
                    dont_edit = DefaultDontEdit,
                    dont_print = DefaultDontPrint,
                    expires = DefaultExpires,
                    subject = DefaultSubject,
                    note = DefaultNote,
                    IsDirty = false
                };

                // Do the edit/create of the note
                if (EditDisbursementNote(newNote, newNote.EnableEdit(), newNote.EnableFlags()) == System.Windows.Forms.DialogResult.OK)
                {
                    return SaveDisbursementNote(newNote);
                }

                return false;
            }

            /// <summary>
            /// Delete the disbursement note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Delete(Int32 noteID)
            {
                // Find the note in the table. Use only portions to find the note's ability to delete.
                var q = bc.disbursement_notes.Where(n => n.Id == noteID).FirstOrDefault();
                if (q == null)
                {
                    return false;
                }

                // Determine if we can delete the note. If not authorized then complain.
                var note = new combined_note(q);
                if (!note.EnableDelete())
                {
                    DebtPlus.Data.Forms.MessageBox.Show(STR_YouAreNotAuthorizedToDeleteThisNoteAtThisTime, STR_NoteDeletionSecurityViolation, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return false;
                }

                // Confirm the deletion of the note.
                if (confirm_delete())
                {
                    bc.disbursement_notes.DeleteOnSubmit(q);
                    bc.SubmitChanges();
                    return true;
                }

                // The note was not deleted.
                return false;
            }
        }
    }
}