//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2010 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is Protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

using System;
using System.Collections.Generic;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes.AlertNotes
{
    /// <summary>
    /// Class to show the Disbursement alert notes
    /// </summary>
    public class Disbursement : Client, DebtPlus.Interfaces.Disbursement.IAlertNotes
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Create the note subject for the form's text property
        /// </summary>
        protected override string MakeText(NoteClass.combined_note n)
        {
            if (n.source == 2)
            {
                return @"Disbursement Note: " + n.subject;
            }
            return base.MakeText(n);
        }

        /// <summary>
        /// Background color for the dialog
        /// </summary>
        protected override System.Drawing.Color NoteBackgroundColor(NoteClass.combined_note n)
        {
            if (n.source == 2)
            {
                return System.Drawing.Color.PowderBlue;
            }
            return base.NoteBackgroundColor(n);
        }

        /// <summary>
        /// Display the alert notes
        /// </summary>
        /// <param name="ClientID"></param>
        /// <returns></returns>
        public new int ShowAlerts(int ClientID)
        {
            Int32? disbursement_register = new Int32?();
            return ShowAlerts(ClientID, disbursement_register, false);
        }

        /// <summary>
        /// Display the alert notes
        /// </summary>
        public override int ShowAlerts(int ClientID, Int32? DisbursementRegister)
        {
            return ShowAlerts(ClientID, DisbursementRegister, false);
        }

        /// <summary>
        /// Display the alert notes
        /// </summary>
        public int ShowAlerts(int ClientID, bool ShowOnlyNewNotes)
        {
            Int32? disbursement_register = new Int32?();
            return ShowAlerts(ClientID, disbursement_register, ShowOnlyNewNotes);
        }

        /// <summary>
        /// Retrieve the list of disbursement notes
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="DisbursementRegister"></param>
        /// <param name="ShowOnlyNewNotes"></param>
        /// <returns></returns>
        private List<NoteClass.combined_note> getDisbursementNotes(int ClientID, Int32? DisbursementRegister, bool ShowOnlyNewNotes)
        {
            // If there is no disbursement register then don't bother to look
            if (ClientID <= 0 || !DisbursementRegister.HasValue || DisbursementRegister.Value <= 0)
            {
                return null;
            }

            // Look in the database for a disbursement register
            if (ShowOnlyNewNotes)
            {
                // Show only notes that have not been assigned to a disbursement.
                return (from n in bc.disbursement_notes where n.client == ClientID && n.type == 5 && n.disbursement_register == null select new NoteClass.combined_note(n, DisbursementRegister)).ToList();
            }

            // If there is a disbursement register then look for notes that are on our disbursement.
            if (DisbursementRegister.HasValue && DisbursementRegister.Value > 0)
            {
                return (from n in bc.disbursement_notes where n.client == ClientID && n.type == 5 && (n.disbursement_register == null || n.disbursement_register == DisbursementRegister.Value) select new NoteClass.combined_note(n, DisbursementRegister)).ToList();
            }

            // The parameters are inconsistent. Just return the empty set.
            return null;
        }

        /// <summary>
        /// Display the alert notes
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="DisbursementRegister"></param>
        /// <param name="ShowOnlyNewNotes"></param>
        /// <returns></returns>
        public int ShowAlerts(int ClientID, Int32? DisbursementRegister, bool ShowOnlyNewNotes)
        {
            // Retrieve the notes from the system for each type. They go into two lists as that will set the note type correctly.
            lst = null;
            List<NoteClass.combined_note> lstClient = getClientNotes(ClientID, new Int32?());
            List<NoteClass.combined_note> lstDisbursement = getDisbursementNotes(ClientID, DisbursementRegister, ShowOnlyNewNotes);

            // If there are both lists then we need to combine them.
            if (lstClient != null && lstDisbursement != null)
            {
                lst = lstClient.Concat(lstDisbursement).OrderBy(f => f.date_created).ToList();
            }

            // If there is a list client then it is the result
            else if (lstClient != null)
            {
                lst = lstClient;
            }

            // If there is a list disbursement then it is the result
            else if (lstDisbursement != null)
            {
                lst = lstDisbursement;
            }

            // If there is no list or the list is empty then terminate early
            if (lst == null || lst.Count <= 0)
            {
                return 0;
            }

            NoteCount = 0;
            using (var frm = new DisbursementAlertNotesForm(bc))
            {
                frm.NextNote += NextNoteHandler;
                frm.FormatNote += FormatNoteHandler;

                frm.ShowDialog();

                frm.NextNote -= NextNoteHandler;
                frm.FormatNote -= FormatNoteHandler;
            }
            return NoteCount;
        }

        /// <summary>
        /// Format the note for the display. This replaces the client note formatter to allow for the "delete" button.
        /// </summary>
        /// <param name="sender">Pointer to the form</param>
        /// <param name="e">Empty arguments</param>
        protected override void FormatNoteHandler(object sender, EventArgs e)
        {
            NoteClass.combined_note n = lst[NoteCount];
            NotesAlertShowForm frm = (NotesAlertShowForm)sender;

            // Add the note to the save button so that the command may be executed
            frm.SaveButton(n);

            // Do the basic note formatting
            base.FormatNoteHandler(sender, e);

            try
            {
                frm.emptySpaceItem_Left.Width = (frm.emptySpaceItem_Left.Width + frm.emptySpaceItem_Right.Width) / 2;
            }
            catch (Exception ex)
            {
                log.Error("Error centering button locations", ex);
            }
        }

        // TO BE REMOVED LATER ONCE THE INTERFACE IS CHANED FROM int To Int32? //

        /// <summary>
        /// Display the alert notes
        /// </summary>
        public int ShowAlerts(int ClientID, int DisbursementRegister)
        {
            return ShowAlerts(ClientID, new Int32?(DisbursementRegister), false);
        }

        /// <summary>
        /// Display the alert notes
        /// </summary>
        public int ShowAlerts(int ClientID, int DisbursementRegister, bool ShowOnlyNewNotes)
        {
            return ShowAlerts(ClientID, new Int32?(DisbursementRegister), ShowOnlyNewNotes);
        }
    }
}
