﻿using System.Linq;
using DebtPlus.LINQ;
using System;

namespace DebtPlus.Notes.AlertNotes
{
    internal partial class DisbursementAlertNotesForm : NotesAlertShowForm
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal DisbursementAlertNotesForm()
            : base()
        {
            InitializeComponent();
        }

        internal DisbursementAlertNotesForm(DebtPlus.LINQ.BusinessContext bc)
            : base(bc)
        {
            InitializeComponent();
        }

        DebtPlus.Notes.NoteClass.combined_note CurrentNote;
        protected internal override void SaveButton(DebtPlus.Notes.NoteClass.combined_note Note)
        {
            // Save the note pointer should the OK button be pressed
            CurrentNote = Note;

            // Show the save button only on disbursement notes.
            simpleButton_Save.Visible = false;
            layoutControlItem_simpleButton_Save.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;

            // Display the SAVE button if this is a disbursement note. The save is the normal OK processing for a normal
            // alert note. The OK button is changed to delete the alert note if this is a disbursement note.
            if (Note.type == 5)
            {
                if (Note.new_disbursement_register.HasValue)
                {
                    if (!Note.disbursement_register.HasValue)
                    {
                        simpleButton_Save.Visible = true;
                        layoutControlItem_simpleButton_Save.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    }
                }
            }
        }

        /// <summary>
        /// Handle the OK button for disbursement alert notes
        /// </summary>
        protected override void simpleButton_OK_Click(Object Sender, System.EventArgs e)
        {
            // Find the note that is attached to the button. It there is a note then update the notes with the new
            // disbursement register. This will take the note out of the "new" class and put it into the "old, seen before" class.
            // The note is not deleted. It is simply marked as "shown".
            if (CurrentNote != null && CurrentNote.type == 5 && CurrentNote.new_disbursement_register.HasValue && !CurrentNote.disbursement_register.HasValue)
            {
                try
                {
                    disbursement_note q = bc.disbursement_notes.Where(notes => notes.Id == CurrentNote.Id && notes.disbursement_register == null && notes.type == 5).FirstOrDefault();
                    if (q != null)
                    {
                        q.disbursement_register = CurrentNote.new_disbursement_register;
                        bc.SubmitChanges();
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    log.Error("Error updating notes database", ex);
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating notes database");
                }
            }

            // Do the standard logic once we have disposed of the note.
            base.simpleButton_OK_Click(Sender, e);
        }
    }
}
