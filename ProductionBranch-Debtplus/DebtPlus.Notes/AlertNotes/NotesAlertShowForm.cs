﻿//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2010 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is Protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

using DebtPlus.Utils;
using DebtPlus.LINQ;
using DebtPlus.Notes;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;

namespace DebtPlus.Notes.AlertNotes
{
    internal partial class NotesAlertShowForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Dispose of local storage
        /// </summary>
        /// <param name="disposing"></param>
        private void NotesAlertShowForm_Dispose(bool disposing)
        {
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        // Event when we want to process the next note in the database
        public event CancelEventHandler NextNote;

        /// <summary>
        /// Raise the next note event and return the cancel status
        /// </summary>
        protected bool RaiseNextNote()
        {
            CancelEventArgs Args = new CancelEventArgs();
            OnNextNote(Args);
            return (Args.Cancel);
        }

        /// <summary>
        /// Raise the Next Note event
        /// </summary>
        protected virtual void OnNextNote(CancelEventArgs e)
        {
            RaiseNextNote(e);
        }

        /// <summary>
        /// Raise the next note event
        /// </summary>
        protected void RaiseNextNote(CancelEventArgs e)
        {
            var evt = NextNote;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        public event EventHandler FormatNote;
        /// <summary>
        /// Raise the format note event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnFormatNote(EventArgs e)
        {
            var evt = FormatNote;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the Format Note Event
        /// </summary>
        protected void RaiseFormatNote()
        {
            OnFormatNote(EventArgs.Empty);
        }

        /// <summary>
        /// Handle the click event on the save button
        /// </summary>
        /// <param name="DeleteCommmand"></param>
        protected internal virtual void SaveButton(NoteClass.combined_note Note)
        {
        }

        /// <summary>
        /// Initialize the form class
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        internal NotesAlertShowForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the form class
        /// </summary>
        internal NotesAlertShowForm(BusinessContext bc) : this()
        {
            this.bc = bc;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += Form_Load;
            layoutControl1.Resize += layoutControl1_Resize;
            simpleButton_OK.Click += simpleButton_OK_Click;
            simpleButton_Save.Click += simpleButton_Save_Click;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= Form_Load;
            layoutControl1.Resize -= layoutControl1_Resize;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            simpleButton_Save.Click -= simpleButton_Save_Click;
        }

        /// <summary>
        /// Handle the click event for the OK button
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected virtual void simpleButton_OK_Click(Object Sender, System.EventArgs e)
        {
            // Fetch the next note
            if (RaiseNextNote())
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                RaiseFormatNote();
                this.Refresh();
            }
        }

        /// <summary>
        /// Handle the click event for the OK button
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected virtual void simpleButton_Save_Click(Object Sender, System.EventArgs e)
        {
            // Fetch the next note
            if (RaiseNextNote())
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                RaiseFormatNote();
                this.Refresh();
            }
        }

        /// <summary>
        /// Process the form load event
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void Form_Load(Object Sender, System.EventArgs e)
        {
            CenterButtons();
            FormHelper.MakeTopMost(this);
            RaiseFormatNote();

            // Move the focus to the input control
            this.Focus();
            richEditControl1.Focus();
        }

        /// <summary>
        /// Process the resize event on the layout control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void layoutControl1_Resize(object sender, EventArgs e)
        {
            CenterButtons();
        }

        /// <summary>
        /// Try to center the one/two buttons on the form when it is loaded or resized.
        /// </summary>
        private void CenterButtons()
        {
            try
            {
                emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) >> 1;
            }
            catch (Exception ex)
            {
                log.Error("Error centering buttons", ex);
            }
        }
    }
}
