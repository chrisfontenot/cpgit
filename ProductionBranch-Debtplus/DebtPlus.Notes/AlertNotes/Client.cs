using DebtPlus.Interfaces.Client;
using System.Windows.Forms;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes.AlertNotes
{
    public class Client : Base, IAlertNotes
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Current client for the note
        protected Int32 ClientID;

        /// <summary>
        /// Create the instance of the class
        /// </summary>
        public Client()
        {
        }

        /// <summary>
        /// Display the alert notes for the current client
        /// </summary>
        /// <param name="ClientID">Client ID</param>
        /// <returns>The number of notes displayed.</returns>
        public virtual int ShowAlerts(int ClientID)
        {
            Int32? ClientCreditor = new Int32?();
            return ShowAlerts(ClientID, ClientCreditor);
        }

        /// <summary>
        /// DIsplay the alert notes for the current debt
        /// </summary>
        /// <param name="ClientID">Client ID</param>
        /// <param name="ClientCreditor">Debt Pointer</param>
        /// <returns>The number of notes displayed</returns>
        public virtual int ShowAlerts(int ClientID, Int32? ClientCreditor)
        {
            // Get the notes from the database
            lst = getClientNotes(ClientID, ClientCreditor);
            if (lst == null || lst.Count <= 0)
            {
                return 0;
            }

            // If there are notes then we need to show them now.
            NoteCount = 0;
            using (var frm = new AlertNotes.CreditorAlertNotesShowForm(bc))
            {
                frm.NextNote += NextNoteHandler;
                frm.FormatNote += FormatNoteHandler;

                frm.ShowDialog();

                frm.NextNote -= NextNoteHandler;
                frm.FormatNote -= FormatNoteHandler;
            }

            // The result is the number of notes displayed to the user.
            return NoteCount;
        }

        /// <summary>
        /// Get the list of client notes to be shown.
        /// </summary>
        /// <param name="ClientID"></param>
        /// <param name="ClientCreditor"></param>
        /// <returns></returns>
        protected System.Collections.Generic.List<NoteClass.combined_note> getClientNotes(Int32 ClientID, Int32? ClientCreditor)
        {
            // If there is no client then do nothing
            if (ClientID < 0)
            {
                return null;
            }

            try
            {
                // Generate the base note query to access the client notes table
                System.Linq.IQueryable<DebtPlus.LINQ.client_note> query = bc.client_notes.Where(n => n.type == 4 && n.client == ClientID);

                // If there is a debt reference then we need to include it
                if (ClientCreditor.HasValue && ClientCreditor.Value > 0)
                {
                    query = query.Where(n => n.client_creditor == ClientCreditor.Value);
                }
                else
                {
                    query = query.Where(n => n.client_creditor == null);
                }

                return query.Select(n => new DebtPlus.Notes.NoteClass.combined_note(n)).ToList();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                log.Error("Error reading alert notes", ex);
                return null;
            }
        }

        /// <summary>
        /// DIsplay the alert notes for the current debt
        /// </summary>
        /// <param name="ClientID">Client ID</param>
        /// <param name="ClientCreditor">Debt Pointer</param>
        /// <returns>The number of notes displayed</returns>
        public virtual int ShowAlerts(int ClientID, object ClientCreditor)
        {
            return ShowAlerts(ClientID, DebtPlus.Utils.Nulls.v_Int32(ClientCreditor));
        }
    }
}
