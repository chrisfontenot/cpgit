﻿namespace DebtPlus.Notes.AlertNotes
{
    partial class CreditorAlertNotesShowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_richEditControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(214, 263);
            // 
            // richEditControl1
            // 
            this.richEditControl1.Views.SimpleView.BackColor = System.Drawing.Color.LemonChiffon;
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.layoutControl1.Controls.SetChildIndex(this.richEditControl1, 0);
            this.layoutControl1.Controls.SetChildIndex(this.simpleButton_Save, 0);
            // 
            // simpleButton_Save
            // 
            this.simpleButton_Save.Location = new System.Drawing.Point(130, 263);
            // 
            // layoutControlItem_simpleButton_OK
            // 
            this.layoutControlItem_simpleButton_OK.Location = new System.Drawing.Point(202, 251);
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(286, 251);
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(118, 30);
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(118, 30);
            // 
            // layoutControlItem_simpleButton_Save
            // 
            this.layoutControlItem_simpleButton_Save.Location = new System.Drawing.Point(118, 251);
            // 
            // CreditorAlertNotesShowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 329);
            this.Name = "CreditorAlertNotesShowForm";
            this.Text = "Creditor Alert Note";
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_richEditControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion
    }
}