namespace DebtPlus.Notes.AlertNotes
{
    partial class NotesAlertShowForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                NotesAlertShowForm_Dispose(disposing);
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton_Save = new DevExpress.XtraEditors.SimpleButton();
            this.richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_richEditControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_simpleButton_OK = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_simpleButton_Save = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_richEditControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Save)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_OK.Location = new System.Drawing.Point(211, 263);
            this.simpleButton_OK.MaximumSize = new System.Drawing.Size(80, 26);
            this.simpleButton_OK.MinimumSize = new System.Drawing.Size(80, 26);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(80, 26);
            this.simpleButton_OK.StyleController = this.layoutControl1;
            this.simpleButton_OK.TabIndex = 2;
            this.simpleButton_OK.Text = "&OK";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton_Save);
            this.layoutControl1.Controls.Add(this.richEditControl1);
            this.layoutControl1.Controls.Add(this.simpleButton_OK);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(424, 301);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton_Save
            // 
            this.simpleButton_Save.Location = new System.Drawing.Point(127, 263);
            this.simpleButton_Save.MaximumSize = new System.Drawing.Size(80, 26);
            this.simpleButton_Save.MinimumSize = new System.Drawing.Size(80, 26);
            this.simpleButton_Save.Name = "simpleButton_Save";
            this.simpleButton_Save.Size = new System.Drawing.Size(80, 26);
            this.simpleButton_Save.StyleController = this.layoutControl1;
            this.simpleButton_Save.TabIndex = 1;
            this.simpleButton_Save.Text = "&Save";
            // 
            // richEditControl1
            // 
            this.richEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richEditControl1.Location = new System.Drawing.Point(12, 12);
            this.richEditControl1.MenuManager = this.barManager1;
            this.richEditControl1.Name = "richEditControl1";
            this.richEditControl1.ReadOnly = true;
            this.richEditControl1.ShowCaretInReadOnly = false;
            this.richEditControl1.Size = new System.Drawing.Size(400, 225);
            this.richEditControl1.TabIndex = 0;
            this.richEditControl1.Views.SimpleView.BackColor = System.Drawing.Color.LemonChiffon;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2});
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.barStaticItem1.Caption = "BarStaticItem_created_by";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem1.Width = 32;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "BarStaticItem_date_created";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(424, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 301);
            this.barDockControlBottom.Size = new System.Drawing.Size(424, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 301);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(424, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 301);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_richEditControl1,
            this.layoutControlItem_simpleButton_OK,
            this.emptySpaceItem_Right,
            this.emptySpaceItem_Left,
            this.layoutControlItem_simpleButton_Save,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(424, 301);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem_richEditControl1
            // 
            this.layoutControlItem_richEditControl1.Control = this.richEditControl1;
            this.layoutControlItem_richEditControl1.CustomizationFormText = "Message Window";
            this.layoutControlItem_richEditControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_richEditControl1.Name = "layoutControlItem_richEditControl1";
            this.layoutControlItem_richEditControl1.Size = new System.Drawing.Size(404, 229);
            this.layoutControlItem_richEditControl1.Text = "Message Window";
            this.layoutControlItem_richEditControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_richEditControl1.TextToControlDistance = 0;
            this.layoutControlItem_richEditControl1.TextVisible = false;
            // 
            // layoutControlItem_simpleButton_OK
            // 
            this.layoutControlItem_simpleButton_OK.Control = this.simpleButton_OK;
            this.layoutControlItem_simpleButton_OK.CustomizationFormText = "OK Button";
            this.layoutControlItem_simpleButton_OK.Location = new System.Drawing.Point(199, 251);
            this.layoutControlItem_simpleButton_OK.Name = "layoutControlItem_simpleButton_OK";
            this.layoutControlItem_simpleButton_OK.Size = new System.Drawing.Size(84, 30);
            this.layoutControlItem_simpleButton_OK.Text = "OK Button";
            this.layoutControlItem_simpleButton_OK.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_simpleButton_OK.TextToControlDistance = 0;
            this.layoutControlItem_simpleButton_OK.TextVisible = false;
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.AllowHotTrack = false;
            this.emptySpaceItem_Right.CustomizationFormText = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(283, 251);
            this.emptySpaceItem_Right.Name = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(121, 30);
            this.emptySpaceItem_Right.Text = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.AllowHotTrack = false;
            this.emptySpaceItem_Left.CustomizationFormText = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Location = new System.Drawing.Point(0, 251);
            this.emptySpaceItem_Left.Name = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(115, 30);
            this.emptySpaceItem_Left.Text = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_simpleButton_Save
            // 
            this.layoutControlItem_simpleButton_Save.Control = this.simpleButton_Save;
            this.layoutControlItem_simpleButton_Save.CustomizationFormText = "Save Button";
            this.layoutControlItem_simpleButton_Save.Location = new System.Drawing.Point(115, 251);
            this.layoutControlItem_simpleButton_Save.Name = "layoutControlItem_simpleButton_Save";
            this.layoutControlItem_simpleButton_Save.Size = new System.Drawing.Size(84, 30);
            this.layoutControlItem_simpleButton_Save.Text = "Save Button";
            this.layoutControlItem_simpleButton_Save.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_simpleButton_Save.TextToControlDistance = 0;
            this.layoutControlItem_simpleButton_Save.TextVisible = false;
            this.layoutControlItem_simpleButton_Save.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 229);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(0, 22);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(10, 22);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(404, 22);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // NotesAlertShowForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(424, 329);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "NotesAlertShowForm";
            this.Text = "Alert Notes";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_richEditControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_simpleButton_Save)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        protected internal DevExpress.XtraBars.BarManager barManager1;
        protected internal DevExpress.XtraBars.Bar bar3;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        protected internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        protected internal DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        protected internal DevExpress.XtraBars.BarStaticItem barStaticItem1;
        protected internal DevExpress.XtraBars.BarStaticItem barStaticItem2;
        protected internal DevExpress.XtraRichEdit.RichEditControl richEditControl1;
        protected internal DevExpress.XtraLayout.LayoutControl layoutControl1;
        protected internal DevExpress.XtraEditors.SimpleButton simpleButton_Save;
        protected internal DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        protected internal DevExpress.XtraLayout.LayoutControlItem layoutControlItem_richEditControl1;
        protected internal DevExpress.XtraLayout.LayoutControlItem layoutControlItem_simpleButton_OK;
        protected internal DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Right;
        protected internal DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Left;
        protected internal DevExpress.XtraLayout.LayoutControlItem layoutControlItem_simpleButton_Save;
        protected internal DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
    }
}

