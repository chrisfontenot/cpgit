//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2010 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is Protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes.AlertNotes
{
    public class Vendor : Base, DebtPlus.Interfaces.Creditor.IAlertNotes
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Display the alert notes given the vendor label
        /// </summary>
        /// <param name="CreditorLabel">String for the vendor label</param>
        /// <returns>The number of notes displayed.</returns>
        public int ShowAlerts(String VendorLabel)
        {
            if (string.IsNullOrWhiteSpace(VendorLabel))
            {
                return 0;
            }

            lst = (from n in bc.vendor_notes
                   join v in bc.vendors on n.vendor equals v.Id
                   where v.Label == VendorLabel && n.type == 4
                   select new DebtPlus.Notes.NoteClass.combined_note(n)).ToList();

            // If there are notes then we need to show them now.
            if (lst.Count < 1)
            {
                return 0;
            }

            NoteCount = 0;
            using (var frm = new AlertNotes.CreditorAlertNotesShowForm(bc))
            {
                frm.NextNote   += NextNoteHandler;
                frm.FormatNote += FormatNoteHandler;

                frm.ShowDialog();

                frm.NextNote   -= NextNoteHandler;
                frm.FormatNote -= FormatNoteHandler;
            }

            return NoteCount;
        }

        /// <summary>
        /// Display the vendor alert notes
        /// </summary>
        /// <param name="CreditorID">Pointer to the vendor record</param>
        /// <returns>The number of notes displayed.</returns>
        public int ShowAlerts(int VendorID)
        {
            if (VendorID <= 0)
            {
                return 0;
            }

            lst = bc.vendor_notes.Where(s => s.vendor == VendorID && s.type == 4).Select(s => new DebtPlus.Notes.NoteClass.combined_note(s)).ToList();

            // If there are notes then we need to show them now.
            if (lst.Count <= 0)
            {
                return 0;
            }

            NoteCount = 0;
            using (var frm = new AlertNotes.CreditorAlertNotesShowForm(bc))
            {
                frm.NextNote   += NextNoteHandler;
                frm.FormatNote += FormatNoteHandler;
                frm.Text        = "Vendor Alert Note";

                frm.ShowDialog();

                frm.NextNote   -= NextNoteHandler;
                frm.FormatNote -= FormatNoteHandler;
            }

            return NoteCount;
        }
    }
}
