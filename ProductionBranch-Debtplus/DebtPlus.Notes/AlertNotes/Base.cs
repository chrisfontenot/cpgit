﻿using System;
using System.Collections.Generic;
using System.Drawing;
using DebtPlus.Utils.Format;

namespace DebtPlus.Notes.AlertNotes
{
    public class Base : IDisposable
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected int NoteCount;
        protected List<NoteClass.combined_note> lst = null;
        protected DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Initialize the class structure
        /// </summary>
        internal Base()
        {
            bc = new LINQ.BusinessContext();
        }

        /// <summary>
        /// Allow the ancestor classes a chance of the Dispose function
        /// </summary>
        /// <param name="disposing">TRUE for Dispose. FALSE for Finalize.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (bc != null)
                {
                    bc.Dispose();
                }
            }
            bc = null;
        }

        /// <summary>
        /// Dispose of the local storage.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Finalize the removal of the class
        /// </summary>
        ~Base()
        {
            Dispose(false);
        }

        /// <summary>
        /// Color for the background color
        /// </summary>
        protected virtual Color NoteBackgroundColor(NoteClass.combined_note n)
        {
            return Color.LightYellow;
        }

        /// <summary>
        /// Generate the note title
        /// </summary>
        /// <param name="Subject">Current subject of the note</param>
        /// <returns></returns>
        protected virtual string MakeText(NoteClass.combined_note n)
        {
            return @"Alert Note: " + n.subject;
        }

        /// <summary>
        /// Read the next note from the input dataset. Cancel on the last record.
        /// </summary>
        /// <param name="sender">The display form</param>
        /// <param name="e">Cancel event arguments.</param>
        protected void NextNoteHandler(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = (NoteCount >= lst.Count);
        }

        /// <summary>
        /// Format the note from the dataset.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void FormatNoteHandler(Object sender, EventArgs e)
        {
            NotesAlertShowForm frm = (NotesAlertShowForm)sender;

            // Retrieve the current note and bump the count
            NoteClass.combined_note n = lst[NoteCount];
            NoteCount++;

            // Build the note format
            frm.Text = MakeText(n);
            frm.barStaticItem1.Caption = n.created_by;
            frm.barStaticItem2.Caption = n.date_created.Value.ToString("MM/dd/yyyy h:mm tt");

            // Ensure that the tag is null. It will be replaced by the Disbursement not formatter later if needed.
            frm.simpleButton_OK.Tag = null;

            frm.richEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            frm.richEditControl1.Views.SimpleView.BackColor = NoteBackgroundColor(n);

            // Try to load the text as RTF
            if (Strings.IsRTF(n.note))
            {
                try
                {
                    frm.richEditControl1.RtfText = n.note;
                    return;
                }
                catch (Exception ex)
                {
                    log.Error("Error setting RTF text into editing control", ex);
                }
            }

            // Try to load the note as HTML
            if (Strings.IsHTML(n.note))
            {
                try
                {
                    frm.richEditControl1.HtmlText = n.note;
                    return;
                }
                catch (Exception ex)
                {
                    log.Error("Error setting HTML text into editing control", ex);
                }
            }

            try
            {
                frm.richEditControl1.Text = n.note;
            }

            catch (Exception ex)
            {
                log.Error("Error setting TXT text into editing control", ex);
            }
        }
    }
}
