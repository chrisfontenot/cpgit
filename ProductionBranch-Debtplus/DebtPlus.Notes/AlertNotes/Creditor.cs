//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2010 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is Protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes.AlertNotes
{
    public class Creditor : Base, DebtPlus.Interfaces.Creditor.IAlertNotes
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Display the alert notes given the creditor label
        /// </summary>
        /// <param name="CreditorLabel">String for the creditor label</param>
        /// <returns>The number of notes displayed.</returns>
        public int ShowAlerts(String CreditorLabel)
        {
            if (CreditorLabel != String.Empty)
            {
                lst = bc.creditor_notes.Where(n => n.type == 4 && n.creditor == CreditorLabel).Select(n => new DebtPlus.Notes.NoteClass.combined_note(n)).ToList();

                // If there are notes then we need to show them now.
                if (lst.Count > 0)
                {
                    NoteCount = 0;
                    using (var frm = new AlertNotes.CreditorAlertNotesShowForm(bc))
                    {
                        frm.NextNote += NextNoteHandler;
                        frm.FormatNote += FormatNoteHandler;

                        frm.ShowDialog();

                        frm.NextNote -= NextNoteHandler;
                        frm.FormatNote -= FormatNoteHandler;
                    }
                }
            }

            return NoteCount;
        }

        /// <summary>
        /// Display the creditor alert notes
        /// </summary>
        /// <param name="CreditorID">Pointer to the creditor record</param>
        /// <returns>The number of notes displayed.</returns>
        public int ShowAlerts(int CreditorID)
        {
            if (CreditorID <= 0)
            {
                return 0;
            }

            lst = (from n in bc.creditor_notes
                   from cr in bc.creditors
                   where cr.creditor_id == CreditorID && n.type == 4 && n.creditor == cr.Id
                   select new DebtPlus.Notes.NoteClass.combined_note(n)).ToList();

            // If there are notes then we need to show them now.
            if (lst.Count <= 0)
            {
                return 0;
            }

            NoteCount = 0;
            using (var frm = new AlertNotes.CreditorAlertNotesShowForm(bc))
            {
                frm.NextNote += NextNoteHandler;
                frm.FormatNote += FormatNoteHandler;

                frm.ShowDialog();

                frm.NextNote -= NextNoteHandler;
                frm.FormatNote -= FormatNoteHandler;
            }

            return NoteCount;
        }
    }
}
