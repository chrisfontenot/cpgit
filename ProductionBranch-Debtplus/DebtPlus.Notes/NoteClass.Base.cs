﻿using System;
using DebtPlus.LINQ;

namespace DebtPlus.Notes
{
    /// <summary>
    /// Class to handle the notes in the system
    /// </summary>
    public sealed partial class NoteClass
    {
        /// <summary>
        /// This class represents the total combination of all of the note tables. It is used
        /// to render the notes similar to the editing procedure so that the notes will be
        /// updated in a common fashion.
        /// </summary>
        public sealed partial class combined_note
        {
            private bool p_IsDirty;
            public bool IsDirty
            {
                get
                {
                    return p_IsDirty;
                }
                set
                {
                    p_IsDirty = value;
                }
            }

            private Int32 p_source;
            public Int32 source
            {
                get
                {
                    return p_source;
                }
                set
                {
                    if (p_source != value)
                    {
                        p_source = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32 p_Id;
            public Int32 Id
            {
                get
                {
                    return p_Id;
                }
                set
                {
                    if (p_Id != value)
                    {
                        p_Id = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32 p_type;
            public Int32 type
            {
                get
                {
                    return p_type;
                }
                set
                {
                    if (p_type != value)
                    {
                        p_type = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32? p_disbursement_register;
            public Int32? disbursement_register
            {
                get
                {
                    return p_disbursement_register;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_disbursement_register, value) != 0)
                    {
                        p_disbursement_register = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32? p_client_product;
            public Int32? client_product
            {
                get
                {
                    return p_client_product;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_client_product, value) != 0)
                    {
                        p_client_product = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32? p_deposit_batch_id;
            public Int32? deposit_batch_id
            {
                get
                {
                    return p_deposit_batch_id;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_deposit_batch_id, value) != 0)
                    {
                        p_deposit_batch_id = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32? p_client_creditor;
            public Int32? client_creditor
            {
                get
                {
                    return p_client_creditor;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_client_creditor, value) != 0)
                    {
                        p_client_creditor = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32? p_client;
            public Int32? client
            {
                get
                {
                    return p_client;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_client, value) != 0)
                    {
                        p_client = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32? p_prior_note;
            public Int32? prior_note
            {
                get
                {
                    return p_prior_note;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_prior_note, value) != 0)
                    {
                        p_prior_note = value;
                        IsDirty = true;
                    }
                }
            }

            private Int32? p_new_disbursement_register;
            public Int32? new_disbursement_register
            {
                get
                {
                    return p_new_disbursement_register;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_new_disbursement_register, value) != 0)
                    {
                        p_new_disbursement_register = value;
                        IsDirty = true;
                    }
                }
            }

            private string p_creditor;
            public string creditor
            {
                get
                {
                    return p_creditor;
                }
                set
                {
                    if (p_creditor != value)
                    {
                        p_creditor = value;
                        IsDirty = true;
                    }
                }
            }
            private Int32? p_vendor;
            public Int32? vendor
            {
                get
                {
                    return p_vendor;
                }
                set
                {
                    if (System.Nullable.Compare<Int32>(p_vendor, value) != 0)
                    {
                        p_vendor = value;
                    }
                }
            }

            private string p_subject;
            public string subject
            {
                get
                {
                    return p_subject;
                }
                set
                {
                    if (p_subject != value)
                    {
                        p_subject = value;
                        IsDirty = true;
                    }
                }
            }

            private string p_note;
            public string note
            {
                get
                {
                    return p_note;
                }
                set
                {
                    if (p_note != value)
                    {
                        p_note = value;
                        IsDirty = true;
                    }
                }
            }

            private Boolean p_dont_edit;
            public Boolean dont_edit
            {
                get
                {
                    return p_dont_edit;
                }
                set
                {
                    if (p_dont_edit != value)
                    {
                        p_dont_edit = value;
                        IsDirty = true;
                    }
                }
            }

            private Boolean p_dont_print;
            public Boolean dont_print
            {
                get
                {
                    return p_dont_print;
                }
                set
                {
                    if (p_dont_print != value)
                    {
                        p_dont_print = value;
                        IsDirty = true;
                    }
                }
            }

            private Boolean p_dont_delete;
            public Boolean dont_delete
            {
                get
                {
                    return p_dont_delete;
                }
                set
                {
                    if (p_dont_delete != value)
                    {
                        p_dont_delete = value;
                        IsDirty = true;
                    }
                }
            }

            private DateTime? p_date_created;
            public DateTime? date_created
            {
                get
                {
                    return p_date_created;
                }
                set
                {
                    if (System.Nullable.Compare<DateTime>(p_date_created, value) != 0)
                    {
                        p_date_created = value;
                        IsDirty = true;
                    }
                }
            }

            private DateTime? p_expires;
            public DateTime? expires
            {
                get
                {
                    return p_expires;
                }
                set
                {
                    if (System.Nullable.Compare<DateTime>(p_expires, value) != 0)
                    {
                        p_expires = value;
                        IsDirty = true;
                    }
                }
            }

            /// <summary>
            /// Is the user the admin user?
            /// </summary>
            public bool adminUser
            {
                get
                {
                    string currentUser = BusinessContext.suser_sname();
                    return string.Compare(currentUser, "sa", true) == 0;
                }
            }

            /// <summary>
            /// Was the note created by the same user who is using the system now?
            /// </summary>
            public bool sameUser
            {
                get
                {
                    string currentUser = BusinessContext.suser_sname();

                    // Compare the ids of the person who created the note with the ID of the current user
                    // Use "m_created_by" and not "created_by" since we want the original/full name, not the edited/partial value.
                    return string.Compare(currentUser, p_created_by, true) == 0;
                }
            }

            private string p_created_by;
            public string created_by
            {
                get
                {
                    Int32 iPos = p_created_by.LastIndexOfAny(new char[] { '/', '\\' });
                    if (iPos >= 0 && iPos < p_created_by.Length)
                    {
                        return p_created_by.Substring(iPos + 1);
                    }
                    return p_created_by;
                }
                set
                {
                    p_created_by = value;
                }
            }

            internal combined_note()
            {
                dont_edit      = false;
                dont_print     = false;
                dont_delete    = false;
                source         = 0;
                Id             = default(Int32);
                vendor         = null;
                client_product = null;
                IsDirty        = false;
            }

            /// <summary>
            /// Is the note newly created and must be inserted rather than updated?
            /// </summary>
            public bool IsNew
            {
                get
                {
                    return Id <= 0;
                }
            }

            /// <summary>
            /// Load the class with the contents of a client or debt note
            /// </summary>
            /// <param name="Note">The note object</param>
            internal combined_note(client_note Note) : this()
            {
                source          = 1;
                Id              = Note.Id;
                client          = Note.client;
                client_creditor = Note.client_creditor;
                created_by      = Note.created_by;
                date_created    = Note.date_created;
                dont_delete     = Note.dont_delete;
                dont_edit       = Note.dont_edit;
                dont_print      = Note.dont_print;
                expires         = Note.expires;
                note            = Note.note;
                prior_note      = Note.prior_note;
                subject         = Note.subject;
                type            = Note.type;
                IsDirty         = false;
            }

            /// <summary>
            /// Load the class with the contents of a disbursement note
            /// </summary>
            /// <param name="Note">The note object</param>
            internal combined_note(disbursement_note Note) : this()
            {
                source                = 2;
                Id                    = Note.Id;
                client                = Note.client;
                created_by            = Note.created_by;
                date_created          = Note.date_created;
                dont_delete           = Note.dont_delete;
                dont_edit             = Note.dont_edit;
                dont_print            = Note.dont_print;
                note                  = Note.note;
                prior_note            = Note.prior_note;
                subject               = Note.subject;
                type                  = Note.type;
                deposit_batch_id      = Note.deposit_batch_id;
                disbursement_register = Note.disbursement_register;
                IsDirty               = false;
            }

            /// <summary>
            /// Load the class with the contents of a disbursement note
            /// </summary>
            /// <param name="Note">The note object</param>
            /// <param name="DisbursementRegister">The current disbursement register for setting new notes</param>
            internal combined_note(disbursement_note Note, Int32? DisbursementRegister) : this(Note)
            {
                new_disbursement_register = DisbursementRegister;
                IsDirty = false;
            }

            /// <summary>
            /// Load the class with the contents of a disbursement note
            /// </summary>
            /// <param name="Note">The note object</param>
            internal combined_note(creditor_note Note) : this()
            {
                source       = 3;
                Id           = Note.Id;
                creditor     = Note.creditor;
                created_by   = Note.created_by;
                date_created = Note.date_created;
                dont_delete  = Note.dont_delete;
                dont_edit    = Note.dont_edit;
                dont_print   = Note.dont_print;
                note         = Note.note;
                prior_note   = Note.prior_note;
                subject      = Note.subject;
                type         = Note.type;
                IsDirty      = false;
            }

            /// <summary>
            /// Load the class with the contents of a vendor note
            /// </summary>
            /// <param name="Note">The note object</param>
            internal combined_note(vendor_note Note) : this()
            {
                source       = 3;
                Id           = Note.Id;
                vendor       = Note.vendor;
                created_by   = Note.created_by;
                date_created = Note.date_created;
                dont_delete  = Note.dont_delete;
                dont_edit    = Note.dont_edit;
                dont_print   = Note.dont_print;
                note         = Note.note;
                subject      = Note.subject;
                type         = Note.type;
                expires = Note.expires;
                IsDirty      = false;
            }

            /// <summary>
            /// Load the class with the contents of a client_product note
            /// </summary>
            /// <param name="Note">The note object</param>
            internal combined_note(client_product_note Note) : this()
            {
                source         = 4;
                Id             = Note.Id;
                client_product = Note.client_product;
                created_by     = Note.created_by;
                date_created   = Note.date_created;
                dont_delete    = Note.dont_delete;
                dont_edit      = Note.dont_edit;
                dont_print     = Note.dont_print;
                note           = Note.note;
                subject        = Note.subject;
                type           = Note.type;
                IsDirty        = false;
            }

            /// <summary>
            /// Should we enable the edit operation for the note?
            /// </summary>
            /// <returns>TRUE if edit operation is allowed. FALSE otherwise.</returns>
            /// <remarks></remarks>
            public bool EnableEdit()
            {
                // Created notes should not even be going through here. But, just in case ....
                if (IsNew)
                {
                    return true;
                }

                // System notes are never editable
                if ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)type == DebtPlus.LINQ.InMemory.Notes.NoteTypes.System)
                {
                    return false;
                }

                // Disbursement notes attached to a disbursement batch may not be edited
                // This prevents the counselor from revising the instructions after the disbursement and then accusing the disbursement people of not following his instructions when they were written in a disbursement note.
                if ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)type == DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement)
                {
                    if (disbursement_register != null)
                    {
                        return false;
                    }
                }

                // If this is the admin user then editing is allowed.
                if (adminUser)
                {
                    return true;
                }

                // Look to see if the user is the same. If so, allow the edit operation if permitted.
                if (sameUser)
                {
                    return DebtPlus.Configuration.Config.EditExistingNoteSameUser;
                }

                // This is not the same user. If we deny permissions on different users, deny this permission too.
                if (!DebtPlus.Configuration.Config.EditExistingNoteOtherUser)
                {
                    return false;
                }

                // finally, look at the dont_edit flag
                return !dont_edit;
            }

            /// <summary>
            /// Should we enable the editing of the flag settings on the note?
            /// </summary>
            /// <returns>TRUE if edit operation is allowed. FALSE otherwise.</returns>
            /// <remarks></remarks>
            public bool EnableFlags()
            {
                // if the not is not editable then the note is not editable
                if (!EnableEdit())
                {
                    return false;
                }

                // Created notes should not even be going through here. But, just in case ....
                if (IsNew)
                {
                    return true;
                }

                // System notes are never editable
                if ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)type == DebtPlus.LINQ.InMemory.Notes.NoteTypes.System)
                {
                    return false;
                }

                // Look to see if the user is the same. if so, allow the edit operation.
                return adminUser || sameUser;
            }

            /// <summary>
            /// Should we allow (as a system) the ability to delete this note?
            /// </summary>
            /// <returns>TRUE if the note may be deleted. FALSE otherwise.</returns>
            /// <remarks></remarks>
            public bool EnableDelete()
            {
                // Notes that have not been filed may be deleted. (This is extremely rare....)
                if (IsNew)
                {
                    return true;
                }

                // System notes are not deletable at all
                if ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)type == DebtPlus.LINQ.InMemory.Notes.NoteTypes.System)
                {
                    return false;
                }

                // Disbursement notes attached to a disbursement batch may not be deleted
                // This prevents the counselor from revising the instructions after the disbursement and then accusing the disbursement people of not following his instructions when they were written in a disbursement note.
                if ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)type == DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement && disbursement_register != null)
                {
                    return false;
                }

                // Admin users may delete a note without explicit permission.
                if (adminUser)
                {
                    return true;
                }

                // if the user does not have the authority to delete then reject the deletion.
                if (!DebtPlus.Configuration.Config.DeleteNotes)
                {
                    return false;
                }

                // finally look for the same user and allow the deletion to occur
                if (sameUser)
                {
                    return true;
                }

                // Otherwise, follow the delete flag
                return !dont_delete;
            }
        }

        /// <summary>
        /// Common class for the processing of notes in general
        /// </summary>
        public abstract class BaseClass : IDisposable
        {
            // Some other global string values
            protected const string STR_YouAreNotAuthorizedToDeleteThisNoteAtThisTime = "You are not authorized to delete this note at this time.";
            protected const string STR_NoteDeletionSecurityViolation = "Note Deletion Security Violation";

            protected DebtPlus.LINQ.BusinessContext bc = null;

            /// <summary>
            /// Create a new instance of our class
            /// </summary>
            internal BaseClass()
            {
                bc = new DebtPlus.LINQ.BusinessContext();
            }

            /// <summary>
            /// Delete the indicated note by its id
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public abstract bool Delete(Int32 noteID);

            /// <summary>
            /// Edit the indicated note by its id
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public abstract bool Edit(Int32 noteID);

            /// <summary>
            /// Display but do not allow editing of the note.
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public abstract bool Show(Int32 noteID);

            /// <summary>
            /// Default note text for new notes.
            /// </summary>
            protected virtual string DefaultNote
            {
                get
                {
                    return DebtPlus.LINQ.InMemory.Notes.Default_Note;
                }
            }

            /// <summary>
            /// Return the default value for the subject field on a note
            /// </summary>
            protected virtual string DefaultSubject
            {
                get
                {
                    return string.Empty;
                }
            }

            /// <summary>
            /// Return the default value for the created_by field on a note
            /// </summary>
            protected virtual string DefaultCreatedBy
            {
                get
                {
                    return "Me";
                }
            }

            /// <summary>
            /// Return the default value for the date_created field on a note
            /// </summary>
            protected virtual DateTime DefaultDateCreated
            {
                get
                {
                    return DateTime.Now;
                }
            }

            /// <summary>
            /// Return the default value for the dont_edit field on a note
            /// </summary>
            protected virtual bool DefaultDontEdit
            {
                get
                {
                    return false;
                }
            }

            /// <summary>
            /// Return the default value for the dont_delete field on a note
            /// </summary>
            protected virtual bool DefaultDontDelete
            {
                get
                {
                    return false;
                }
            }

            /// <summary>
            /// Return the default value for the dont_print field on a note
            /// </summary>
            protected virtual bool DefaultDontPrint
            {
                get
                {
                    return false;
                }
            }

            /// <summary>
            /// Return the default value for the Expires field on a note
            /// </summary>
            protected virtual DateTime DefaultExpires
            {
                get
                {
                    return DateTime.Now.Date.AddMonths(3);
                }
            }

            /// <summary>
            /// Confirm the note deletion
            /// </summary>
            /// <returns></returns>
            protected static bool confirm_delete()
            {
                bool Ask = false;

                // Confirm the deletion of the notes.
                Microsoft.Win32.RegistryKey RegKey = Microsoft.Win32.Registry.LocalMachine;

                try
                {
                    RegKey = RegKey.OpenSubKey(DebtPlus.Configuration.Constants.InstallationRegistryKey + @"\confirmation", false);
                    if (RegKey != null)
                    {
                        Ask = Convert.ToBoolean(RegKey.GetValue("delete note", "false"));
                    }
                }
                catch { }
                finally
                {
                    if (RegKey != null)
                    {
                        RegKey.Close();
                    }
                }

                // Ask the user if the delete is desired
                if (!Ask)
                {
                    Ask = DebtPlus.Data.Forms.MessageBox.Show("Are you sure that you wish to delete this note?", "Delete note", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes;
                }

                // TRUE if the note is to be deleted, FALSE if not.
                return Ask;
            }

            private bool disposedValue = false;

            /// <summary>
            /// Process the dispose / finalize request
            /// </summary>
            /// <param name="disposing">TRUE if Dispose is called. FALSE for Finalize.</param>
            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    disposedValue = true;
                    if (disposing)
                    {
                        if (bc != null)
                        {
                            bc.Dispose();
                        }
                        bc = null;
                    }
                }
            }

            /// <summary>
            /// Dispose of the note object
            /// </summary>
            void IDisposable.Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Finalize the destruction of this class.
            /// </summary>
            ~BaseClass()
            {
                Dispose(false);
            }
        }
    }
}
