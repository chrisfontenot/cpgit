﻿namespace DebtPlus.Notes
{
    partial class EditNoteForm
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                EditNoteForm_Dispose(disposing);
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditNoteForm));
            DevExpress.XtraSpellChecker.OptionsSpelling optionsSpelling1 = new DevExpress.XtraSpellChecker.OptionsSpelling();
            DevExpress.XtraRichEdit.Model.BorderInfo borderInfo1 = new DevExpress.XtraRichEdit.Model.BorderInfo();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.Bar_MainMenu = new DevExpress.XtraBars.Bar();
            this.BarSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem_Edit = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_View = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BarToolbarsListItem2 = new DevExpress.XtraBars.BarToolbarsListItem();
            this.BarSubItem_view_zoom = new DevExpress.XtraBars.BarSubItem();
            this.View_Zoom_025 = new DevExpress.XtraBars.BarCheckItem();
            this.View_Zoom_050 = new DevExpress.XtraBars.BarCheckItem();
            this.View_Zoom_075 = new DevExpress.XtraBars.BarCheckItem();
            this.View_Zoom_100 = new DevExpress.XtraBars.BarCheckItem();
            this.View_Zoom_150 = new DevExpress.XtraBars.BarCheckItem();
            this.View_Zoom_200 = new DevExpress.XtraBars.BarCheckItem();
            this.View_Zoom_300 = new DevExpress.XtraBars.BarCheckItem();
            this.BarSubItem_Insert = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_Insert_File = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem_Format = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.BarSubItem_Tools = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_Tools_Spell = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Tools_SpellingOptions = new DevExpress.XtraBars.BarButtonItem();
            this.Bar_Status = new DevExpress.XtraBars.Bar();
            this.BarStaticItem_Line = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_Column = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_Page = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_Zoom = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_CAPS = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_NUM = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_SCR = new DevExpress.XtraBars.BarStaticItem();
            this.Bar1 = new DevExpress.XtraBars.Bar();
            this.CommonBar1 = new DevExpress.XtraRichEdit.UI.CommonBar();
            this.FileNewItem1 = new DevExpress.XtraRichEdit.UI.FileNewItem();
            this.FileOpenItem1 = new DevExpress.XtraRichEdit.UI.FileOpenItem();
            this.FileSaveItem1 = new DevExpress.XtraRichEdit.UI.FileSaveItem();
            this.FileSaveAsItem1 = new DevExpress.XtraRichEdit.UI.FileSaveAsItem();
            this.QuickPrintItem1 = new DevExpress.XtraRichEdit.UI.QuickPrintItem();
            this.PrintItem1 = new DevExpress.XtraRichEdit.UI.PrintItem();
            this.PrintPreviewItem1 = new DevExpress.XtraRichEdit.UI.PrintPreviewItem();
            this.UndoItem1 = new DevExpress.XtraRichEdit.UI.UndoItem();
            this.RedoItem1 = new DevExpress.XtraRichEdit.UI.RedoItem();
            this.ClipboardBar1 = new DevExpress.XtraRichEdit.UI.ClipboardBar();
            this.CutItem1 = new DevExpress.XtraRichEdit.UI.CutItem();
            this.CopyItem1 = new DevExpress.XtraRichEdit.UI.CopyItem();
            this.PasteItem1 = new DevExpress.XtraRichEdit.UI.PasteItem();
            this.FontBar1 = new DevExpress.XtraRichEdit.UI.FontBar();
            this.ChangeFontNameItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontNameItem();
            this.RepositoryItemFontEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.ChangeFontSizeItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontSizeItem();
            this.RepositoryItemRichEditFontSizeEdit2 = new DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit();
            this.RichEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            this.SpellChecker1 = new DevExpress.XtraSpellChecker.SpellChecker();
            this.ChangeFontColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontColorItem();
            this.ChangeFontBackColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontBackColorItem();
            this.ToggleFontBoldItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontBoldItem();
            this.ToggleFontItalicItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontItalicItem();
            this.ToggleFontUnderlineItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem();
            this.ToggleFontDoubleUnderlineItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem();
            this.ToggleFontStrikeoutItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem();
            this.ToggleFontDoubleStrikeoutItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem();
            this.ToggleFontSuperscriptItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem();
            this.ToggleFontSubscriptItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem();
            this.ChangeTextCaseItem1 = new DevExpress.XtraRichEdit.UI.ChangeTextCaseItem();
            this.MakeTextUpperCaseItem1 = new DevExpress.XtraRichEdit.UI.MakeTextUpperCaseItem();
            this.MakeTextLowerCaseItem1 = new DevExpress.XtraRichEdit.UI.MakeTextLowerCaseItem();
            this.ToggleTextCaseItem1 = new DevExpress.XtraRichEdit.UI.ToggleTextCaseItem();
            this.FontSizeIncreaseItem1 = new DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem();
            this.FontSizeDecreaseItem1 = new DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem();
            this.ClearFormattingItem1 = new DevExpress.XtraRichEdit.UI.ClearFormattingItem();
            this.ShowFontFormItem1 = new DevExpress.XtraRichEdit.UI.ShowFontFormItem();
            this.ParagraphBar1 = new DevExpress.XtraRichEdit.UI.ParagraphBar();
            this.ToggleParagraphAlignmentLeftItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentLeftItem();
            this.ToggleParagraphAlignmentCenterItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentCenterItem();
            this.ToggleParagraphAlignmentRightItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentRightItem();
            this.ToggleParagraphAlignmentJustifyItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyItem();
            this.ChangeParagraphLineSpacingItem1 = new DevExpress.XtraRichEdit.UI.ChangeParagraphLineSpacingItem();
            this.SetSingleParagraphSpacingItem1 = new DevExpress.XtraRichEdit.UI.SetSingleParagraphSpacingItem();
            this.SetSesquialteralParagraphSpacingItem1 = new DevExpress.XtraRichEdit.UI.SetSesquialteralParagraphSpacingItem();
            this.SetDoubleParagraphSpacingItem1 = new DevExpress.XtraRichEdit.UI.SetDoubleParagraphSpacingItem();
            this.ShowLineSpacingFormItem1 = new DevExpress.XtraRichEdit.UI.ShowLineSpacingFormItem();
            this.AddSpacingBeforeParagraphItem1 = new DevExpress.XtraRichEdit.UI.AddSpacingBeforeParagraphItem();
            this.RemoveSpacingBeforeParagraphItem1 = new DevExpress.XtraRichEdit.UI.RemoveSpacingBeforeParagraphItem();
            this.AddSpacingAfterParagraphItem1 = new DevExpress.XtraRichEdit.UI.AddSpacingAfterParagraphItem();
            this.RemoveSpacingAfterParagraphItem1 = new DevExpress.XtraRichEdit.UI.RemoveSpacingAfterParagraphItem();
            this.ToggleBulletedListItem1 = new DevExpress.XtraRichEdit.UI.ToggleBulletedListItem();
            this.ToggleNumberingListItem1 = new DevExpress.XtraRichEdit.UI.ToggleNumberingListItem();
            this.ToggleMultiLevelListItem1 = new DevExpress.XtraRichEdit.UI.ToggleMultiLevelListItem();
            this.DecreaseIndentItem1 = new DevExpress.XtraRichEdit.UI.DecreaseIndentItem();
            this.IncreaseIndentItem1 = new DevExpress.XtraRichEdit.UI.IncreaseIndentItem();
            this.ToggleShowWhitespaceItem1 = new DevExpress.XtraRichEdit.UI.ToggleShowWhitespaceItem();
            this.ShowParagraphFormItem1 = new DevExpress.XtraRichEdit.UI.ShowParagraphFormItem();
            this.StylesBar1 = new DevExpress.XtraRichEdit.UI.StylesBar();
            this.ChangeStyleItem1 = new DevExpress.XtraRichEdit.UI.ChangeStyleItem();
            this.RepositoryItemRichEditStyleEdit2 = new DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit();
            this.EditingBar1 = new DevExpress.XtraRichEdit.UI.EditingBar();
            this.FindItem1 = new DevExpress.XtraRichEdit.UI.FindItem();
            this.ReplaceItem1 = new DevExpress.XtraRichEdit.UI.ReplaceItem();
            this.PagesBar1 = new DevExpress.XtraRichEdit.UI.PagesBar();
            this.InsertPageBreakItem1 = new DevExpress.XtraRichEdit.UI.InsertPageBreakItem();
            this.TablesBar1 = new DevExpress.XtraRichEdit.UI.TablesBar();
            this.InsertTableItem1 = new DevExpress.XtraRichEdit.UI.InsertTableItem();
            this.IllustrationsBar1 = new DevExpress.XtraRichEdit.UI.IllustrationsBar();
            this.InsertPictureItem1 = new DevExpress.XtraRichEdit.UI.InsertPictureItem();
            this.LinksBar1 = new DevExpress.XtraRichEdit.UI.LinksBar();
            this.InsertBookmarkItem1 = new DevExpress.XtraRichEdit.UI.InsertBookmarkItem();
            this.InsertHyperlinkItem1 = new DevExpress.XtraRichEdit.UI.InsertHyperlinkItem();
            this.HeaderFooterBar1 = new DevExpress.XtraRichEdit.UI.HeaderFooterBar();
            this.EditPageHeaderItem1 = new DevExpress.XtraRichEdit.UI.EditPageHeaderItem();
            this.EditPageFooterItem1 = new DevExpress.XtraRichEdit.UI.EditPageFooterItem();
            this.InsertPageNumberItem1 = new DevExpress.XtraRichEdit.UI.InsertPageNumberItem();
            this.InsertPageCountItem1 = new DevExpress.XtraRichEdit.UI.InsertPageCountItem();
            this.SymbolsBar1 = new DevExpress.XtraRichEdit.UI.SymbolsBar();
            this.InsertSymbolItem1 = new DevExpress.XtraRichEdit.UI.InsertSymbolItem();
            this.PageSetupBar1 = new DevExpress.XtraRichEdit.UI.PageSetupBar();
            this.ChangeSectionPageMarginsItem1 = new DevExpress.XtraRichEdit.UI.ChangeSectionPageMarginsItem();
            this.SetNormalSectionPageMarginsItem1 = new DevExpress.XtraRichEdit.UI.SetNormalSectionPageMarginsItem();
            this.SetNarrowSectionPageMarginsItem1 = new DevExpress.XtraRichEdit.UI.SetNarrowSectionPageMarginsItem();
            this.SetModerateSectionPageMarginsItem1 = new DevExpress.XtraRichEdit.UI.SetModerateSectionPageMarginsItem();
            this.SetWideSectionPageMarginsItem1 = new DevExpress.XtraRichEdit.UI.SetWideSectionPageMarginsItem();
            this.ChangeSectionPageOrientationItem1 = new DevExpress.XtraRichEdit.UI.ChangeSectionPageOrientationItem();
            this.SetPortraitPageOrientationItem1 = new DevExpress.XtraRichEdit.UI.SetPortraitPageOrientationItem();
            this.SetLandscapePageOrientationItem1 = new DevExpress.XtraRichEdit.UI.SetLandscapePageOrientationItem();
            this.ChangeSectionColumnsItem1 = new DevExpress.XtraRichEdit.UI.ChangeSectionColumnsItem();
            this.SetSectionOneColumnItem1 = new DevExpress.XtraRichEdit.UI.SetSectionOneColumnItem();
            this.SetSectionTwoColumnsItem1 = new DevExpress.XtraRichEdit.UI.SetSectionTwoColumnsItem();
            this.SetSectionThreeColumnsItem1 = new DevExpress.XtraRichEdit.UI.SetSectionThreeColumnsItem();
            this.MailMergeBar1 = new DevExpress.XtraRichEdit.UI.MailMergeBar();
            this.InsertMergeFieldItem1 = new DevExpress.XtraRichEdit.UI.InsertMergeFieldItem();
            this.ShowAllFieldCodesItem1 = new DevExpress.XtraRichEdit.UI.ShowAllFieldCodesItem();
            this.ShowAllFieldResultsItem1 = new DevExpress.XtraRichEdit.UI.ShowAllFieldResultsItem();
            this.ToggleViewMergedDataItem1 = new DevExpress.XtraRichEdit.UI.ToggleViewMergedDataItem();
            this.DocumentViewsBar1 = new DevExpress.XtraRichEdit.UI.DocumentViewsBar();
            this.SwitchToSimpleViewItem1 = new DevExpress.XtraRichEdit.UI.SwitchToSimpleViewItem();
            this.SwitchToDraftViewItem1 = new DevExpress.XtraRichEdit.UI.SwitchToDraftViewItem();
            this.SwitchToPrintLayoutViewItem1 = new DevExpress.XtraRichEdit.UI.SwitchToPrintLayoutViewItem();
            this.ShowBar1 = new DevExpress.XtraRichEdit.UI.ShowBar();
            this.ToggleShowHorizontalRulerItem1 = new DevExpress.XtraRichEdit.UI.ToggleShowHorizontalRulerItem();
            this.ToggleShowVerticalRulerItem1 = new DevExpress.XtraRichEdit.UI.ToggleShowVerticalRulerItem();
            this.ZoomBar1 = new DevExpress.XtraRichEdit.UI.ZoomBar();
            this.ZoomOutItem1 = new DevExpress.XtraRichEdit.UI.ZoomOutItem();
            this.ZoomInItem1 = new DevExpress.XtraRichEdit.UI.ZoomInItem();
            this.DocumentProtectionBar1 = new DevExpress.XtraRichEdit.UI.DocumentProtectionBar();
            this.ProtectDocumentItem1 = new DevExpress.XtraRichEdit.UI.ProtectDocumentItem();
            this.ChangeRangeEditingPermissionsItem1 = new DevExpress.XtraRichEdit.UI.ChangeRangeEditingPermissionsItem();
            this.UnprotectDocumentItem1 = new DevExpress.XtraRichEdit.UI.UnprotectDocumentItem();
            this.TableStylesBar1 = new DevExpress.XtraRichEdit.UI.TableStylesBar();
            this.ChangeTableCellsShadingItem1 = new DevExpress.XtraRichEdit.UI.ChangeTableCellsShadingItem();
            this.ChangeTableBordersItem1 = new DevExpress.XtraRichEdit.UI.ChangeTableBordersItem();
            this.ToggleTableCellsBottomBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomBorderItem();
            this.ToggleTableCellsTopBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsTopBorderItem();
            this.ToggleTableCellsLeftBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsLeftBorderItem();
            this.ToggleTableCellsRightBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsRightBorderItem();
            this.ResetTableCellsAllBordersItem1 = new DevExpress.XtraRichEdit.UI.ResetTableCellsAllBordersItem();
            this.ToggleTableCellsAllBordersItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsAllBordersItem();
            this.ToggleTableCellsOutsideBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsOutsideBorderItem();
            this.ToggleTableCellsInsideBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsInsideBorderItem();
            this.ToggleTableCellsInsideHorizontalBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsInsideHorizontalBorderItem();
            this.ToggleTableCellsInsideVerticalBorderItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsInsideVerticalBorderItem();
            this.ToggleShowTableGridLinesItem1 = new DevExpress.XtraRichEdit.UI.ToggleShowTableGridLinesItem();
            this.TableDrawBordersBar1 = new DevExpress.XtraRichEdit.UI.TableDrawBordersBar();
            this.ChangeTableBorderLineStyleItem1 = new DevExpress.XtraRichEdit.UI.ChangeTableBorderLineStyleItem();
            this.RepositoryItemBorderLineStyle1 = new DevExpress.XtraRichEdit.Forms.Design.RepositoryItemBorderLineStyle();
            this.ChangeTableBorderLineWeightItem1 = new DevExpress.XtraRichEdit.UI.ChangeTableBorderLineWeightItem();
            this.RepositoryItemBorderLineWeight1 = new DevExpress.XtraRichEdit.Forms.Design.RepositoryItemBorderLineWeight();
            this.ChangeTableBorderColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeTableBorderColorItem();
            this.TableTableBar1 = new DevExpress.XtraRichEdit.UI.TableTableBar();
            this.SelectTableElementsItem1 = new DevExpress.XtraRichEdit.UI.SelectTableElementsItem();
            this.SelectTableCellItem1 = new DevExpress.XtraRichEdit.UI.SelectTableCellItem();
            this.SelectTableColumnItem1 = new DevExpress.XtraRichEdit.UI.SelectTableColumnItem();
            this.SelectTableRowItem1 = new DevExpress.XtraRichEdit.UI.SelectTableRowItem();
            this.SelectTableItem1 = new DevExpress.XtraRichEdit.UI.SelectTableItem();
            this.TableRowsAndColumnsBar1 = new DevExpress.XtraRichEdit.UI.TableRowsAndColumnsBar();
            this.DeleteTableElementsItem1 = new DevExpress.XtraRichEdit.UI.DeleteTableElementsItem();
            this.ShowDeleteTableCellsFormItem1 = new DevExpress.XtraRichEdit.UI.ShowDeleteTableCellsFormItem();
            this.DeleteTableColumnsItem1 = new DevExpress.XtraRichEdit.UI.DeleteTableColumnsItem();
            this.DeleteTableRowsItem1 = new DevExpress.XtraRichEdit.UI.DeleteTableRowsItem();
            this.DeleteTableItem1 = new DevExpress.XtraRichEdit.UI.DeleteTableItem();
            this.InsertTableRowAboveItem1 = new DevExpress.XtraRichEdit.UI.InsertTableRowAboveItem();
            this.InsertTableRowBelowItem1 = new DevExpress.XtraRichEdit.UI.InsertTableRowBelowItem();
            this.InsertTableColumnToLeftItem1 = new DevExpress.XtraRichEdit.UI.InsertTableColumnToLeftItem();
            this.InsertTableColumnToRightItem1 = new DevExpress.XtraRichEdit.UI.InsertTableColumnToRightItem();
            this.ShowInsertTableCellsFormItem1 = new DevExpress.XtraRichEdit.UI.ShowInsertTableCellsFormItem();
            this.TableMergeBar1 = new DevExpress.XtraRichEdit.UI.TableMergeBar();
            this.MergeTableCellsItem1 = new DevExpress.XtraRichEdit.UI.MergeTableCellsItem();
            this.ShowSplitTableCellsForm1 = new DevExpress.XtraRichEdit.UI.ShowSplitTableCellsForm();
            this.SplitTableItem1 = new DevExpress.XtraRichEdit.UI.SplitTableItem();
            this.TableAlignmentBar1 = new DevExpress.XtraRichEdit.UI.TableAlignmentBar();
            this.ToggleTableCellsTopLeftAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsTopLeftAlignmentItem();
            this.ToggleTableCellsTopCenterAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsTopCenterAlignmentItem();
            this.ToggleTableCellsTopRightAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsTopRightAlignmentItem();
            this.ToggleTableCellsMiddleLeftAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsMiddleLeftAlignmentItem();
            this.ToggleTableCellsMiddleCenterAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsMiddleCenterAlignmentItem();
            this.ToggleTableCellsMiddleRightAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsMiddleRightAlignmentItem();
            this.ToggleTableCellsBottomLeftAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomLeftAlignmentItem();
            this.ToggleTableCellsBottomCenterAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomCenterAlignmentItem();
            this.ToggleTableCellsBottomRightAlignmentItem1 = new DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomRightAlignmentItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.ImageList16x16 = new System.Windows.Forms.ImageList();
            this.BarSubItem_View_toolbars = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.BarToolbarsListItem1 = new DevExpress.XtraBars.BarToolbarsListItem();
            this.ImageList32x32 = new System.Windows.Forms.ImageList();
            this.RepositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            this.RepositoryItemRichEditFontSizeEdit1 = new DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit();
            this.RepositoryItemRichEditStyleEdit1 = new DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit();
            this.LayoutControl01 = new DevExpress.XtraLayout.LayoutControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEdit_DontDelete = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_DontPrint = new DevExpress.XtraEditors.CheckEdit();
            this.CheckEdit_DontEdit = new DevExpress.XtraEditors.CheckEdit();
            this.DateEdit_Expires = new DevExpress.XtraEditors.DateEdit();
            this.ComboBoxEdit_Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ComboBoxEdit_Subjects = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LayoutControlGroup_EditingControls = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup_Checkboxes = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem_CheckEdit_DontDelete = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem_CheckEdit_DontPrint = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem_CheckEdit_DontEdit = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup_Type_Expires = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem_Expires = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem_Combobox_Type = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem_Subjects = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem_RichEditControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.sharedDictionaryStorage1 = new DevExpress.XtraSpellChecker.SharedDictionaryStorage();
            this.RichEditBarController1 = new DevExpress.XtraRichEdit.UI.RichEditBarController();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemFontEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditFontSizeEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditStyleEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemBorderLineStyle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemBorderLineWeight1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemFontEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditFontSizeEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditStyleEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl01)).BeginInit();
            this.LayoutControl01.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DontDelete.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DontPrint.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DontEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Expires.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Expires.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Subjects.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup_EditingControls)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup_Checkboxes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_CheckEdit_DontDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_CheckEdit_DontPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_CheckEdit_DontEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup_Type_Expires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_Expires)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_Combobox_Type)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_Subjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_RichEditControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RichEditBarController1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.Bar_MainMenu,
            this.Bar_Status,
            this.Bar1,
            this.CommonBar1,
            this.ClipboardBar1,
            this.FontBar1,
            this.ParagraphBar1,
            this.StylesBar1,
            this.EditingBar1,
            this.PagesBar1,
            this.TablesBar1,
            this.IllustrationsBar1,
            this.LinksBar1,
            this.HeaderFooterBar1,
            this.SymbolsBar1,
            this.PageSetupBar1,
            this.MailMergeBar1,
            this.DocumentViewsBar1,
            this.ShowBar1,
            this.ZoomBar1,
            this.DocumentProtectionBar1,
            this.TableStylesBar1,
            this.TableDrawBordersBar1,
            this.TableTableBar1,
            this.TableRowsAndColumnsBar1,
            this.TableMergeBar1,
            this.TableAlignmentBar1});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Images = this.ImageList16x16;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.BarStaticItem_Line,
            this.BarStaticItem_Column,
            this.BarStaticItem_Page,
            this.BarStaticItem_Zoom,
            this.BarStaticItem_CAPS,
            this.BarStaticItem_NUM,
            this.BarStaticItem_SCR,
            this.BarSubItem_File,
            this.BarSubItem_Edit,
            this.BarSubItem_View,
            this.BarButtonItem_File_Exit,
            this.BarSubItem_View_toolbars,
            this.BarSubItem_view_zoom,
            this.BarSubItem_Format,
            this.BarSubItem_Tools,
            this.BarButtonItem_Tools_Spell,
            this.View_Zoom_025,
            this.View_Zoom_050,
            this.View_Zoom_075,
            this.View_Zoom_100,
            this.View_Zoom_150,
            this.View_Zoom_200,
            this.View_Zoom_300,
            this.BarSubItem_Insert,
            this.BarButtonItem_Insert_File,
            this.BarButtonItem1,
            this.BarToolbarsListItem1,
            this.BarButtonItem_Tools_SpellingOptions,
            this.BarSubItem1,
            this.BarToolbarsListItem2,
            this.BarSubItem2,
            this.BarSubItem3,
            this.BarSubItem4,
            this.FileNewItem1,
            this.FileOpenItem1,
            this.FileSaveItem1,
            this.FileSaveAsItem1,
            this.QuickPrintItem1,
            this.PrintItem1,
            this.PrintPreviewItem1,
            this.UndoItem1,
            this.RedoItem1,
            this.CutItem1,
            this.CopyItem1,
            this.PasteItem1,
            this.ChangeFontNameItem1,
            this.ChangeFontSizeItem1,
            this.ChangeFontColorItem1,
            this.ChangeFontBackColorItem1,
            this.ToggleFontBoldItem1,
            this.ToggleFontItalicItem1,
            this.ToggleFontUnderlineItem1,
            this.ToggleFontDoubleUnderlineItem1,
            this.ToggleFontStrikeoutItem1,
            this.ToggleFontDoubleStrikeoutItem1,
            this.ToggleFontSuperscriptItem1,
            this.ToggleFontSubscriptItem1,
            this.ChangeTextCaseItem1,
            this.MakeTextUpperCaseItem1,
            this.MakeTextLowerCaseItem1,
            this.ToggleTextCaseItem1,
            this.FontSizeIncreaseItem1,
            this.FontSizeDecreaseItem1,
            this.ClearFormattingItem1,
            this.ShowFontFormItem1,
            this.ToggleParagraphAlignmentLeftItem1,
            this.ToggleParagraphAlignmentCenterItem1,
            this.ToggleParagraphAlignmentRightItem1,
            this.ToggleParagraphAlignmentJustifyItem1,
            this.ChangeParagraphLineSpacingItem1,
            this.SetSingleParagraphSpacingItem1,
            this.SetSesquialteralParagraphSpacingItem1,
            this.SetDoubleParagraphSpacingItem1,
            this.ShowLineSpacingFormItem1,
            this.AddSpacingBeforeParagraphItem1,
            this.RemoveSpacingBeforeParagraphItem1,
            this.AddSpacingAfterParagraphItem1,
            this.RemoveSpacingAfterParagraphItem1,
            this.ToggleBulletedListItem1,
            this.ToggleNumberingListItem1,
            this.ToggleMultiLevelListItem1,
            this.DecreaseIndentItem1,
            this.IncreaseIndentItem1,
            this.ToggleShowWhitespaceItem1,
            this.ShowParagraphFormItem1,
            this.ChangeStyleItem1,
            this.FindItem1,
            this.ReplaceItem1,
            this.InsertPageBreakItem1,
            this.InsertTableItem1,
            this.InsertPictureItem1,
            this.InsertBookmarkItem1,
            this.InsertHyperlinkItem1,
            this.EditPageHeaderItem1,
            this.EditPageFooterItem1,
            this.InsertPageNumberItem1,
            this.InsertPageCountItem1,
            this.InsertSymbolItem1,
            this.ChangeSectionPageMarginsItem1,
            this.SetNormalSectionPageMarginsItem1,
            this.SetNarrowSectionPageMarginsItem1,
            this.SetModerateSectionPageMarginsItem1,
            this.SetWideSectionPageMarginsItem1,
            this.ChangeSectionPageOrientationItem1,
            this.SetPortraitPageOrientationItem1,
            this.SetLandscapePageOrientationItem1,
            this.ChangeSectionColumnsItem1,
            this.SetSectionOneColumnItem1,
            this.SetSectionTwoColumnsItem1,
            this.SetSectionThreeColumnsItem1,
            this.InsertMergeFieldItem1,
            this.ShowAllFieldCodesItem1,
            this.ShowAllFieldResultsItem1,
            this.ToggleViewMergedDataItem1,
            this.SwitchToSimpleViewItem1,
            this.SwitchToDraftViewItem1,
            this.SwitchToPrintLayoutViewItem1,
            this.ToggleShowHorizontalRulerItem1,
            this.ToggleShowVerticalRulerItem1,
            this.ZoomOutItem1,
            this.ZoomInItem1,
            this.ProtectDocumentItem1,
            this.ChangeRangeEditingPermissionsItem1,
            this.UnprotectDocumentItem1,
            this.ChangeTableCellsShadingItem1,
            this.ChangeTableBordersItem1,
            this.ToggleTableCellsBottomBorderItem1,
            this.ToggleTableCellsTopBorderItem1,
            this.ToggleTableCellsLeftBorderItem1,
            this.ToggleTableCellsRightBorderItem1,
            this.ResetTableCellsAllBordersItem1,
            this.ToggleTableCellsAllBordersItem1,
            this.ToggleTableCellsOutsideBorderItem1,
            this.ToggleTableCellsInsideBorderItem1,
            this.ToggleTableCellsInsideHorizontalBorderItem1,
            this.ToggleTableCellsInsideVerticalBorderItem1,
            this.ToggleShowTableGridLinesItem1,
            this.ChangeTableBorderLineStyleItem1,
            this.ChangeTableBorderLineWeightItem1,
            this.ChangeTableBorderColorItem1,
            this.SelectTableElementsItem1,
            this.SelectTableCellItem1,
            this.SelectTableColumnItem1,
            this.SelectTableRowItem1,
            this.SelectTableItem1,
            this.DeleteTableElementsItem1,
            this.ShowDeleteTableCellsFormItem1,
            this.DeleteTableColumnsItem1,
            this.DeleteTableRowsItem1,
            this.DeleteTableItem1,
            this.InsertTableRowAboveItem1,
            this.InsertTableRowBelowItem1,
            this.InsertTableColumnToLeftItem1,
            this.InsertTableColumnToRightItem1,
            this.ShowInsertTableCellsFormItem1,
            this.MergeTableCellsItem1,
            this.ShowSplitTableCellsForm1,
            this.SplitTableItem1,
            this.ToggleTableCellsTopLeftAlignmentItem1,
            this.ToggleTableCellsTopCenterAlignmentItem1,
            this.ToggleTableCellsTopRightAlignmentItem1,
            this.ToggleTableCellsMiddleLeftAlignmentItem1,
            this.ToggleTableCellsMiddleCenterAlignmentItem1,
            this.ToggleTableCellsMiddleRightAlignmentItem1,
            this.ToggleTableCellsBottomLeftAlignmentItem1,
            this.ToggleTableCellsBottomCenterAlignmentItem1,
            this.ToggleTableCellsBottomRightAlignmentItem1});
            this.barManager1.LargeImages = this.ImageList32x32;
            this.barManager1.MainMenu = this.Bar_MainMenu;
            this.barManager1.MaxItemId = 318;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.RepositoryItemFontEdit1,
            this.RepositoryItemRichEditFontSizeEdit1,
            this.RepositoryItemRichEditStyleEdit1,
            this.RepositoryItemFontEdit2,
            this.RepositoryItemRichEditFontSizeEdit2,
            this.RepositoryItemRichEditStyleEdit2,
            this.RepositoryItemBorderLineStyle1,
            this.RepositoryItemBorderLineWeight1});
            this.barManager1.StatusBar = this.Bar_Status;
            // 
            // Bar_MainMenu
            // 
            this.Bar_MainMenu.BarName = "Bar_MainMenu";
            this.Bar_MainMenu.DockCol = 0;
            this.Bar_MainMenu.DockRow = 0;
            this.Bar_MainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar_MainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_File),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Edit),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_View),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Insert),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Format),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_Tools)});
            this.Bar_MainMenu.OptionsBar.MultiLine = true;
            this.Bar_MainMenu.OptionsBar.UseWholeRow = true;
            this.Bar_MainMenu.Text = "Main menu";
            // 
            // BarSubItem_File
            // 
            this.BarSubItem_File.Caption = "&File";
            this.BarSubItem_File.Id = 51;
            this.BarSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem4, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit, true)});
            this.BarSubItem_File.Name = "BarSubItem_File";
            // 
            // BarSubItem4
            // 
            this.BarSubItem4.Caption = "Layout";
            this.BarSubItem4.Id = 175;
            this.BarSubItem4.Name = "BarSubItem4";
            // 
            // BarButtonItem_File_Exit
            // 
            this.BarButtonItem_File_Exit.Caption = "&Exit";
            this.BarButtonItem_File_Exit.Id = 69;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            // 
            // BarSubItem_Edit
            // 
            this.BarSubItem_Edit.Caption = "&Edit";
            this.BarSubItem_Edit.Id = 52;
            this.BarSubItem_Edit.Name = "BarSubItem_Edit";
            // 
            // BarSubItem_View
            // 
            this.BarSubItem_View.Caption = "&View";
            this.BarSubItem_View.Id = 53;
            this.BarSubItem_View.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_view_zoom, true)});
            this.BarSubItem_View.Name = "BarSubItem_View";
            // 
            // BarSubItem1
            // 
            this.BarSubItem1.Caption = "ToolBars";
            this.BarSubItem1.Id = 171;
            this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarToolbarsListItem2)});
            this.BarSubItem1.Name = "BarSubItem1";
            // 
            // BarToolbarsListItem2
            // 
            this.BarToolbarsListItem2.Caption = "BarToolbarsListItem2";
            this.BarToolbarsListItem2.Id = 172;
            this.BarToolbarsListItem2.Name = "BarToolbarsListItem2";
            // 
            // BarSubItem_view_zoom
            // 
            this.BarSubItem_view_zoom.Caption = "Zoom";
            this.BarSubItem_view_zoom.Id = 72;
            this.BarSubItem_view_zoom.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.View_Zoom_025, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.View_Zoom_050),
            new DevExpress.XtraBars.LinkPersistInfo(this.View_Zoom_075),
            new DevExpress.XtraBars.LinkPersistInfo(this.View_Zoom_100),
            new DevExpress.XtraBars.LinkPersistInfo(this.View_Zoom_150),
            new DevExpress.XtraBars.LinkPersistInfo(this.View_Zoom_200),
            new DevExpress.XtraBars.LinkPersistInfo(this.View_Zoom_300)});
            this.BarSubItem_view_zoom.Name = "BarSubItem_view_zoom";
            // 
            // View_Zoom_025
            // 
            this.View_Zoom_025.Caption = "25%";
            this.View_Zoom_025.Id = 102;
            this.View_Zoom_025.Name = "View_Zoom_025";
            // 
            // View_Zoom_050
            // 
            this.View_Zoom_050.Caption = "50%";
            this.View_Zoom_050.Id = 103;
            this.View_Zoom_050.Name = "View_Zoom_050";
            // 
            // View_Zoom_075
            // 
            this.View_Zoom_075.Caption = "75%";
            this.View_Zoom_075.Id = 104;
            this.View_Zoom_075.Name = "View_Zoom_075";
            // 
            // View_Zoom_100
            // 
            this.View_Zoom_100.Caption = "100%";
            this.View_Zoom_100.Checked = true;
            this.View_Zoom_100.Id = 105;
            this.View_Zoom_100.Name = "View_Zoom_100";
            // 
            // View_Zoom_150
            // 
            this.View_Zoom_150.Caption = "150%";
            this.View_Zoom_150.Id = 106;
            this.View_Zoom_150.Name = "View_Zoom_150";
            // 
            // View_Zoom_200
            // 
            this.View_Zoom_200.Caption = "200%";
            this.View_Zoom_200.Id = 107;
            this.View_Zoom_200.Name = "View_Zoom_200";
            // 
            // View_Zoom_300
            // 
            this.View_Zoom_300.Caption = "300%";
            this.View_Zoom_300.Id = 108;
            this.View_Zoom_300.Name = "View_Zoom_300";
            // 
            // BarSubItem_Insert
            // 
            this.BarSubItem_Insert.Caption = "Insert";
            this.BarSubItem_Insert.Id = 117;
            this.BarSubItem_Insert.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Insert_File)});
            this.BarSubItem_Insert.Name = "BarSubItem_Insert";
            // 
            // BarButtonItem_Insert_File
            // 
            this.BarButtonItem_Insert_File.Caption = "File...";
            this.BarButtonItem_Insert_File.Id = 118;
            this.BarButtonItem_Insert_File.Name = "BarButtonItem_Insert_File";
            // 
            // BarSubItem_Format
            // 
            this.BarSubItem_Format.Caption = "Format";
            this.BarSubItem_Format.Id = 73;
            this.BarSubItem_Format.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem2, true)});
            this.BarSubItem_Format.Name = "BarSubItem_Format";
            // 
            // BarSubItem2
            // 
            this.BarSubItem2.Caption = "Headers And Footers";
            this.BarSubItem2.Id = 173;
            this.BarSubItem2.Name = "BarSubItem2";
            // 
            // BarSubItem3
            // 
            this.BarSubItem3.Caption = "Table";
            this.BarSubItem3.Id = 174;
            this.BarSubItem3.Name = "BarSubItem3";
            // 
            // BarSubItem_Tools
            // 
            this.BarSubItem_Tools.Caption = "Tools";
            this.BarSubItem_Tools.Id = 83;
            this.BarSubItem_Tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Tools_Spell),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Tools_SpellingOptions)});
            this.BarSubItem_Tools.Name = "BarSubItem_Tools";
            // 
            // BarButtonItem_Tools_Spell
            // 
            this.BarButtonItem_Tools_Spell.Caption = "Spell...";
            this.BarButtonItem_Tools_Spell.Id = 84;
            this.BarButtonItem_Tools_Spell.ImageIndex = 39;
            this.BarButtonItem_Tools_Spell.LargeImageIndex = 39;
            this.BarButtonItem_Tools_Spell.Name = "BarButtonItem_Tools_Spell";
            // 
            // BarButtonItem_Tools_SpellingOptions
            // 
            this.BarButtonItem_Tools_SpellingOptions.Caption = "Spelling &Options...";
            this.BarButtonItem_Tools_SpellingOptions.Id = 138;
            this.BarButtonItem_Tools_SpellingOptions.Name = "BarButtonItem_Tools_SpellingOptions";
            this.BarButtonItem_Tools_SpellingOptions.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // Bar_Status
            // 
            this.Bar_Status.BarName = "Status bar";
            this.Bar_Status.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.Bar_Status.DockCol = 0;
            this.Bar_Status.DockRow = 0;
            this.Bar_Status.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.Bar_Status.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Line),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Column),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Page),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_Zoom),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_CAPS),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_NUM),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_SCR)});
            this.Bar_Status.OptionsBar.AllowQuickCustomization = false;
            this.Bar_Status.OptionsBar.DrawDragBorder = false;
            this.Bar_Status.OptionsBar.UseWholeRow = true;
            this.Bar_Status.Text = "Status bar";
            this.Bar_Status.Visible = false;
            // 
            // BarStaticItem_Line
            // 
            this.BarStaticItem_Line.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.BarStaticItem_Line.Caption = "1";
            this.BarStaticItem_Line.Description = "Line number for the editor";
            this.BarStaticItem_Line.Id = 44;
            this.BarStaticItem_Line.Name = "BarStaticItem_Line";
            this.BarStaticItem_Line.TextAlignment = System.Drawing.StringAlignment.Center;
            this.BarStaticItem_Line.Width = 32;
            // 
            // BarStaticItem_Column
            // 
            this.BarStaticItem_Column.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.BarStaticItem_Column.Caption = "1";
            this.BarStaticItem_Column.Description = "Column number for the editor";
            this.BarStaticItem_Column.Id = 45;
            this.BarStaticItem_Column.Name = "BarStaticItem_Column";
            this.BarStaticItem_Column.TextAlignment = System.Drawing.StringAlignment.Center;
            this.BarStaticItem_Column.Width = 32;
            // 
            // BarStaticItem_Page
            // 
            this.BarStaticItem_Page.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.BarStaticItem_Page.Caption = "1";
            this.BarStaticItem_Page.Description = "Page number for the editor";
            this.BarStaticItem_Page.Id = 46;
            this.BarStaticItem_Page.Name = "BarStaticItem_Page";
            this.BarStaticItem_Page.TextAlignment = System.Drawing.StringAlignment.Center;
            this.BarStaticItem_Page.Width = 32;
            // 
            // BarStaticItem_Zoom
            // 
            this.BarStaticItem_Zoom.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.BarStaticItem_Zoom.Caption = "100%";
            this.BarStaticItem_Zoom.Description = "Zoom factor for the editor";
            this.BarStaticItem_Zoom.Id = 47;
            this.BarStaticItem_Zoom.Name = "BarStaticItem_Zoom";
            this.BarStaticItem_Zoom.TextAlignment = System.Drawing.StringAlignment.Center;
            this.BarStaticItem_Zoom.Width = 32;
            // 
            // BarStaticItem_CAPS
            // 
            this.BarStaticItem_CAPS.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.BarStaticItem_CAPS.Description = "Capital Letters Lock status";
            this.BarStaticItem_CAPS.Id = 48;
            this.BarStaticItem_CAPS.Name = "BarStaticItem_CAPS";
            this.BarStaticItem_CAPS.TextAlignment = System.Drawing.StringAlignment.Center;
            this.BarStaticItem_CAPS.Width = 32;
            // 
            // BarStaticItem_NUM
            // 
            this.BarStaticItem_NUM.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.BarStaticItem_NUM.Description = "Numeric lock status for the editor";
            this.BarStaticItem_NUM.Id = 49;
            this.BarStaticItem_NUM.Name = "BarStaticItem_NUM";
            this.BarStaticItem_NUM.TextAlignment = System.Drawing.StringAlignment.Center;
            this.BarStaticItem_NUM.Width = 32;
            // 
            // BarStaticItem_SCR
            // 
            this.BarStaticItem_SCR.AutoSize = DevExpress.XtraBars.BarStaticItemSize.None;
            this.BarStaticItem_SCR.Description = "Scroll Lock for the editor";
            this.BarStaticItem_SCR.Id = 50;
            this.BarStaticItem_SCR.Name = "BarStaticItem_SCR";
            this.BarStaticItem_SCR.TextAlignment = System.Drawing.StringAlignment.Center;
            this.BarStaticItem_SCR.Width = 32;
            // 
            // Bar1
            // 
            this.Bar1.BarName = "Spell";
            this.Bar1.DockCol = 21;
            this.Bar1.DockRow = 2;
            this.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Tools_Spell)});
            this.Bar1.Offset = 630;
            this.Bar1.OptionsBar.AllowRename = true;
            this.Bar1.Text = "Spell";
            this.Bar1.Visible = false;
            // 
            // CommonBar1
            // 
            this.CommonBar1.DockCol = 2;
            this.CommonBar1.DockRow = 1;
            this.CommonBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.CommonBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.FileNewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.FileOpenItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.FileSaveItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.FileSaveAsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.QuickPrintItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.UndoItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.RedoItem1)});
            this.CommonBar1.Offset = 618;
            this.CommonBar1.Visible = false;
            // 
            // FileNewItem1
            // 
            this.FileNewItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("FileNewItem1.Glyph")));
            this.FileNewItem1.Id = 184;
            this.FileNewItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("FileNewItem1.LargeGlyph")));
            this.FileNewItem1.Name = "FileNewItem1";
            // 
            // FileOpenItem1
            // 
            this.FileOpenItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("FileOpenItem1.Glyph")));
            this.FileOpenItem1.Id = 185;
            this.FileOpenItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("FileOpenItem1.LargeGlyph")));
            this.FileOpenItem1.Name = "FileOpenItem1";
            // 
            // FileSaveItem1
            // 
            this.FileSaveItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("FileSaveItem1.Glyph")));
            this.FileSaveItem1.Id = 186;
            this.FileSaveItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("FileSaveItem1.LargeGlyph")));
            this.FileSaveItem1.Name = "FileSaveItem1";
            // 
            // FileSaveAsItem1
            // 
            this.FileSaveAsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("FileSaveAsItem1.Glyph")));
            this.FileSaveAsItem1.Id = 187;
            this.FileSaveAsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("FileSaveAsItem1.LargeGlyph")));
            this.FileSaveAsItem1.Name = "FileSaveAsItem1";
            // 
            // QuickPrintItem1
            // 
            this.QuickPrintItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("QuickPrintItem1.Glyph")));
            this.QuickPrintItem1.Id = 188;
            this.QuickPrintItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("QuickPrintItem1.LargeGlyph")));
            this.QuickPrintItem1.Name = "QuickPrintItem1";
            // 
            // PrintItem1
            // 
            this.PrintItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("PrintItem1.Glyph")));
            this.PrintItem1.Id = 189;
            this.PrintItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("PrintItem1.LargeGlyph")));
            this.PrintItem1.Name = "PrintItem1";
            // 
            // PrintPreviewItem1
            // 
            this.PrintPreviewItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("PrintPreviewItem1.Glyph")));
            this.PrintPreviewItem1.Id = 190;
            this.PrintPreviewItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("PrintPreviewItem1.LargeGlyph")));
            this.PrintPreviewItem1.Name = "PrintPreviewItem1";
            // 
            // UndoItem1
            // 
            this.UndoItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("UndoItem1.Glyph")));
            this.UndoItem1.Id = 191;
            this.UndoItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("UndoItem1.LargeGlyph")));
            this.UndoItem1.Name = "UndoItem1";
            // 
            // RedoItem1
            // 
            this.RedoItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("RedoItem1.Glyph")));
            this.RedoItem1.Id = 192;
            this.RedoItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("RedoItem1.LargeGlyph")));
            this.RedoItem1.Name = "RedoItem1";
            // 
            // ClipboardBar1
            // 
            this.ClipboardBar1.DockCol = 8;
            this.ClipboardBar1.DockRow = 2;
            this.ClipboardBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.ClipboardBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.CutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.CopyItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.PasteItem1)});
            this.ClipboardBar1.Offset = 558;
            this.ClipboardBar1.Visible = false;
            // 
            // CutItem1
            // 
            this.CutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("CutItem1.Glyph")));
            this.CutItem1.Id = 193;
            this.CutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("CutItem1.LargeGlyph")));
            this.CutItem1.Name = "CutItem1";
            // 
            // CopyItem1
            // 
            this.CopyItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("CopyItem1.Glyph")));
            this.CopyItem1.Id = 194;
            this.CopyItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("CopyItem1.LargeGlyph")));
            this.CopyItem1.Name = "CopyItem1";
            // 
            // PasteItem1
            // 
            this.PasteItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("PasteItem1.Glyph")));
            this.PasteItem1.Id = 195;
            this.PasteItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("PasteItem1.LargeGlyph")));
            this.PasteItem1.Name = "PasteItem1";
            // 
            // FontBar1
            // 
            this.FontBar1.DockCol = 0;
            this.FontBar1.DockRow = 1;
            this.FontBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.FontBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeFontNameItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeFontSizeItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeFontColorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeFontBackColorItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontBoldItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontItalicItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontUnderlineItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontDoubleUnderlineItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontStrikeoutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontDoubleStrikeoutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontSuperscriptItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleFontSubscriptItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeTextCaseItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.FontSizeIncreaseItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.FontSizeDecreaseItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ClearFormattingItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowFontFormItem1)});
            // 
            // ChangeFontNameItem1
            // 
            this.ChangeFontNameItem1.Edit = this.RepositoryItemFontEdit2;
            this.ChangeFontNameItem1.Id = 196;
            this.ChangeFontNameItem1.Name = "ChangeFontNameItem1";
            // 
            // RepositoryItemFontEdit2
            // 
            this.RepositoryItemFontEdit2.AutoHeight = false;
            this.RepositoryItemFontEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemFontEdit2.Name = "RepositoryItemFontEdit2";
            // 
            // ChangeFontSizeItem1
            // 
            this.ChangeFontSizeItem1.Edit = this.RepositoryItemRichEditFontSizeEdit2;
            this.ChangeFontSizeItem1.Id = 197;
            this.ChangeFontSizeItem1.Name = "ChangeFontSizeItem1";
            // 
            // RepositoryItemRichEditFontSizeEdit2
            // 
            this.RepositoryItemRichEditFontSizeEdit2.AutoHeight = false;
            this.RepositoryItemRichEditFontSizeEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemRichEditFontSizeEdit2.Control = this.RichEditControl1;
            this.RepositoryItemRichEditFontSizeEdit2.Name = "RepositoryItemRichEditFontSizeEdit2";
            // 
            // RichEditControl1
            // 
            this.RichEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.RichEditControl1.Location = new System.Drawing.Point(12, 36);
            this.RichEditControl1.MenuManager = this.barManager1;
            this.RichEditControl1.Name = "RichEditControl1";
            this.SpellChecker1.SetShowSpellCheckMenu(this.RichEditControl1, false);
            this.RichEditControl1.Size = new System.Drawing.Size(666, 225);
            this.RichEditControl1.SpellChecker = this.SpellChecker1;
            this.SpellChecker1.SetSpellCheckerOptions(this.RichEditControl1, optionsSpelling1);
            this.RichEditControl1.TabIndex = 4;
            // 
            // SpellChecker1
            // 
            this.SpellChecker1.Culture = new System.Globalization.CultureInfo("en-US");
            this.SpellChecker1.OptionsSpelling.CheckFromCursorPos = DevExpress.Utils.DefaultBoolean.False;
            this.SpellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.False;
            this.SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.Utils.DefaultBoolean.True;
            this.SpellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.SpellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.Utils.DefaultBoolean.False;
            this.SpellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.Utils.DefaultBoolean.False;
            this.SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.Utils.DefaultBoolean.True;
            this.SpellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.Utils.DefaultBoolean.True;
            this.SpellChecker1.ParentContainer = null;
            // 
            // ChangeFontColorItem1
            // 
            this.ChangeFontColorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeFontColorItem1.Glyph")));
            this.ChangeFontColorItem1.Id = 198;
            this.ChangeFontColorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeFontColorItem1.LargeGlyph")));
            this.ChangeFontColorItem1.Name = "ChangeFontColorItem1";
            // 
            // ChangeFontBackColorItem1
            // 
            this.ChangeFontBackColorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeFontBackColorItem1.Glyph")));
            this.ChangeFontBackColorItem1.Id = 199;
            this.ChangeFontBackColorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeFontBackColorItem1.LargeGlyph")));
            this.ChangeFontBackColorItem1.Name = "ChangeFontBackColorItem1";
            // 
            // ToggleFontBoldItem1
            // 
            this.ToggleFontBoldItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontBoldItem1.Glyph")));
            this.ToggleFontBoldItem1.Id = 200;
            this.ToggleFontBoldItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontBoldItem1.LargeGlyph")));
            this.ToggleFontBoldItem1.Name = "ToggleFontBoldItem1";
            // 
            // ToggleFontItalicItem1
            // 
            this.ToggleFontItalicItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontItalicItem1.Glyph")));
            this.ToggleFontItalicItem1.Id = 201;
            this.ToggleFontItalicItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontItalicItem1.LargeGlyph")));
            this.ToggleFontItalicItem1.Name = "ToggleFontItalicItem1";
            // 
            // ToggleFontUnderlineItem1
            // 
            this.ToggleFontUnderlineItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontUnderlineItem1.Glyph")));
            this.ToggleFontUnderlineItem1.Id = 202;
            this.ToggleFontUnderlineItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontUnderlineItem1.LargeGlyph")));
            this.ToggleFontUnderlineItem1.Name = "ToggleFontUnderlineItem1";
            // 
            // ToggleFontDoubleUnderlineItem1
            // 
            this.ToggleFontDoubleUnderlineItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontDoubleUnderlineItem1.Glyph")));
            this.ToggleFontDoubleUnderlineItem1.Id = 203;
            this.ToggleFontDoubleUnderlineItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontDoubleUnderlineItem1.LargeGlyph")));
            this.ToggleFontDoubleUnderlineItem1.Name = "ToggleFontDoubleUnderlineItem1";
            // 
            // ToggleFontStrikeoutItem1
            // 
            this.ToggleFontStrikeoutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontStrikeoutItem1.Glyph")));
            this.ToggleFontStrikeoutItem1.Id = 204;
            this.ToggleFontStrikeoutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontStrikeoutItem1.LargeGlyph")));
            this.ToggleFontStrikeoutItem1.Name = "ToggleFontStrikeoutItem1";
            // 
            // ToggleFontDoubleStrikeoutItem1
            // 
            this.ToggleFontDoubleStrikeoutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontDoubleStrikeoutItem1.Glyph")));
            this.ToggleFontDoubleStrikeoutItem1.Id = 205;
            this.ToggleFontDoubleStrikeoutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontDoubleStrikeoutItem1.LargeGlyph")));
            this.ToggleFontDoubleStrikeoutItem1.Name = "ToggleFontDoubleStrikeoutItem1";
            // 
            // ToggleFontSuperscriptItem1
            // 
            this.ToggleFontSuperscriptItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontSuperscriptItem1.Glyph")));
            this.ToggleFontSuperscriptItem1.Id = 206;
            this.ToggleFontSuperscriptItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontSuperscriptItem1.LargeGlyph")));
            this.ToggleFontSuperscriptItem1.Name = "ToggleFontSuperscriptItem1";
            // 
            // ToggleFontSubscriptItem1
            // 
            this.ToggleFontSubscriptItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontSubscriptItem1.Glyph")));
            this.ToggleFontSubscriptItem1.Id = 207;
            this.ToggleFontSubscriptItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleFontSubscriptItem1.LargeGlyph")));
            this.ToggleFontSubscriptItem1.Name = "ToggleFontSubscriptItem1";
            // 
            // ChangeTextCaseItem1
            // 
            this.ChangeTextCaseItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeTextCaseItem1.Glyph")));
            this.ChangeTextCaseItem1.Id = 208;
            this.ChangeTextCaseItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeTextCaseItem1.LargeGlyph")));
            this.ChangeTextCaseItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.MakeTextUpperCaseItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.MakeTextLowerCaseItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTextCaseItem1)});
            this.ChangeTextCaseItem1.Name = "ChangeTextCaseItem1";
            // 
            // MakeTextUpperCaseItem1
            // 
            this.MakeTextUpperCaseItem1.Id = 209;
            this.MakeTextUpperCaseItem1.Name = "MakeTextUpperCaseItem1";
            // 
            // MakeTextLowerCaseItem1
            // 
            this.MakeTextLowerCaseItem1.Id = 210;
            this.MakeTextLowerCaseItem1.Name = "MakeTextLowerCaseItem1";
            // 
            // ToggleTextCaseItem1
            // 
            this.ToggleTextCaseItem1.Id = 211;
            this.ToggleTextCaseItem1.Name = "ToggleTextCaseItem1";
            // 
            // FontSizeIncreaseItem1
            // 
            this.FontSizeIncreaseItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("FontSizeIncreaseItem1.Glyph")));
            this.FontSizeIncreaseItem1.Id = 212;
            this.FontSizeIncreaseItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("FontSizeIncreaseItem1.LargeGlyph")));
            this.FontSizeIncreaseItem1.Name = "FontSizeIncreaseItem1";
            // 
            // FontSizeDecreaseItem1
            // 
            this.FontSizeDecreaseItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("FontSizeDecreaseItem1.Glyph")));
            this.FontSizeDecreaseItem1.Id = 213;
            this.FontSizeDecreaseItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("FontSizeDecreaseItem1.LargeGlyph")));
            this.FontSizeDecreaseItem1.Name = "FontSizeDecreaseItem1";
            // 
            // ClearFormattingItem1
            // 
            this.ClearFormattingItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ClearFormattingItem1.Glyph")));
            this.ClearFormattingItem1.Id = 214;
            this.ClearFormattingItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ClearFormattingItem1.LargeGlyph")));
            this.ClearFormattingItem1.Name = "ClearFormattingItem1";
            // 
            // ShowFontFormItem1
            // 
            this.ShowFontFormItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowFontFormItem1.Glyph")));
            this.ShowFontFormItem1.Id = 215;
            this.ShowFontFormItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowFontFormItem1.LargeGlyph")));
            this.ShowFontFormItem1.Name = "ShowFontFormItem1";
            // 
            // ParagraphBar1
            // 
            this.ParagraphBar1.DockCol = 1;
            this.ParagraphBar1.DockRow = 1;
            this.ParagraphBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.ParagraphBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleParagraphAlignmentLeftItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleParagraphAlignmentCenterItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleParagraphAlignmentRightItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleParagraphAlignmentJustifyItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeParagraphLineSpacingItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleBulletedListItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleNumberingListItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleMultiLevelListItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.DecreaseIndentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.IncreaseIndentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleShowWhitespaceItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowParagraphFormItem1)});
            this.ParagraphBar1.Offset = 492;
            // 
            // ToggleParagraphAlignmentLeftItem1
            // 
            this.ToggleParagraphAlignmentLeftItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentLeftItem1.Glyph")));
            this.ToggleParagraphAlignmentLeftItem1.Id = 216;
            this.ToggleParagraphAlignmentLeftItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentLeftItem1.LargeGlyph")));
            this.ToggleParagraphAlignmentLeftItem1.Name = "ToggleParagraphAlignmentLeftItem1";
            // 
            // ToggleParagraphAlignmentCenterItem1
            // 
            this.ToggleParagraphAlignmentCenterItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentCenterItem1.Glyph")));
            this.ToggleParagraphAlignmentCenterItem1.Id = 217;
            this.ToggleParagraphAlignmentCenterItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentCenterItem1.LargeGlyph")));
            this.ToggleParagraphAlignmentCenterItem1.Name = "ToggleParagraphAlignmentCenterItem1";
            // 
            // ToggleParagraphAlignmentRightItem1
            // 
            this.ToggleParagraphAlignmentRightItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentRightItem1.Glyph")));
            this.ToggleParagraphAlignmentRightItem1.Id = 218;
            this.ToggleParagraphAlignmentRightItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentRightItem1.LargeGlyph")));
            this.ToggleParagraphAlignmentRightItem1.Name = "ToggleParagraphAlignmentRightItem1";
            // 
            // ToggleParagraphAlignmentJustifyItem1
            // 
            this.ToggleParagraphAlignmentJustifyItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentJustifyItem1.Glyph")));
            this.ToggleParagraphAlignmentJustifyItem1.Id = 219;
            this.ToggleParagraphAlignmentJustifyItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleParagraphAlignmentJustifyItem1.LargeGlyph")));
            this.ToggleParagraphAlignmentJustifyItem1.Name = "ToggleParagraphAlignmentJustifyItem1";
            // 
            // ChangeParagraphLineSpacingItem1
            // 
            this.ChangeParagraphLineSpacingItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeParagraphLineSpacingItem1.Glyph")));
            this.ChangeParagraphLineSpacingItem1.Id = 220;
            this.ChangeParagraphLineSpacingItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeParagraphLineSpacingItem1.LargeGlyph")));
            this.ChangeParagraphLineSpacingItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.SetSingleParagraphSpacingItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetSesquialteralParagraphSpacingItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetDoubleParagraphSpacingItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowLineSpacingFormItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.AddSpacingBeforeParagraphItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.RemoveSpacingBeforeParagraphItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.AddSpacingAfterParagraphItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.RemoveSpacingAfterParagraphItem1)});
            this.ChangeParagraphLineSpacingItem1.Name = "ChangeParagraphLineSpacingItem1";
            // 
            // SetSingleParagraphSpacingItem1
            // 
            this.SetSingleParagraphSpacingItem1.Id = 221;
            this.SetSingleParagraphSpacingItem1.Name = "SetSingleParagraphSpacingItem1";
            // 
            // SetSesquialteralParagraphSpacingItem1
            // 
            this.SetSesquialteralParagraphSpacingItem1.Id = 222;
            this.SetSesquialteralParagraphSpacingItem1.Name = "SetSesquialteralParagraphSpacingItem1";
            // 
            // SetDoubleParagraphSpacingItem1
            // 
            this.SetDoubleParagraphSpacingItem1.Id = 223;
            this.SetDoubleParagraphSpacingItem1.Name = "SetDoubleParagraphSpacingItem1";
            // 
            // ShowLineSpacingFormItem1
            // 
            this.ShowLineSpacingFormItem1.Id = 224;
            this.ShowLineSpacingFormItem1.Name = "ShowLineSpacingFormItem1";
            // 
            // AddSpacingBeforeParagraphItem1
            // 
            this.AddSpacingBeforeParagraphItem1.Id = 225;
            this.AddSpacingBeforeParagraphItem1.Name = "AddSpacingBeforeParagraphItem1";
            // 
            // RemoveSpacingBeforeParagraphItem1
            // 
            this.RemoveSpacingBeforeParagraphItem1.Id = 226;
            this.RemoveSpacingBeforeParagraphItem1.Name = "RemoveSpacingBeforeParagraphItem1";
            // 
            // AddSpacingAfterParagraphItem1
            // 
            this.AddSpacingAfterParagraphItem1.Id = 227;
            this.AddSpacingAfterParagraphItem1.Name = "AddSpacingAfterParagraphItem1";
            // 
            // RemoveSpacingAfterParagraphItem1
            // 
            this.RemoveSpacingAfterParagraphItem1.Id = 228;
            this.RemoveSpacingAfterParagraphItem1.Name = "RemoveSpacingAfterParagraphItem1";
            // 
            // ToggleBulletedListItem1
            // 
            this.ToggleBulletedListItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleBulletedListItem1.Glyph")));
            this.ToggleBulletedListItem1.Id = 229;
            this.ToggleBulletedListItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleBulletedListItem1.LargeGlyph")));
            this.ToggleBulletedListItem1.Name = "ToggleBulletedListItem1";
            // 
            // ToggleNumberingListItem1
            // 
            this.ToggleNumberingListItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleNumberingListItem1.Glyph")));
            this.ToggleNumberingListItem1.Id = 230;
            this.ToggleNumberingListItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleNumberingListItem1.LargeGlyph")));
            this.ToggleNumberingListItem1.Name = "ToggleNumberingListItem1";
            // 
            // ToggleMultiLevelListItem1
            // 
            this.ToggleMultiLevelListItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleMultiLevelListItem1.Glyph")));
            this.ToggleMultiLevelListItem1.Id = 231;
            this.ToggleMultiLevelListItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleMultiLevelListItem1.LargeGlyph")));
            this.ToggleMultiLevelListItem1.Name = "ToggleMultiLevelListItem1";
            // 
            // DecreaseIndentItem1
            // 
            this.DecreaseIndentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("DecreaseIndentItem1.Glyph")));
            this.DecreaseIndentItem1.Id = 232;
            this.DecreaseIndentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("DecreaseIndentItem1.LargeGlyph")));
            this.DecreaseIndentItem1.Name = "DecreaseIndentItem1";
            // 
            // IncreaseIndentItem1
            // 
            this.IncreaseIndentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("IncreaseIndentItem1.Glyph")));
            this.IncreaseIndentItem1.Id = 233;
            this.IncreaseIndentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("IncreaseIndentItem1.LargeGlyph")));
            this.IncreaseIndentItem1.Name = "IncreaseIndentItem1";
            // 
            // ToggleShowWhitespaceItem1
            // 
            this.ToggleShowWhitespaceItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowWhitespaceItem1.Glyph")));
            this.ToggleShowWhitespaceItem1.Id = 234;
            this.ToggleShowWhitespaceItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowWhitespaceItem1.LargeGlyph")));
            this.ToggleShowWhitespaceItem1.Name = "ToggleShowWhitespaceItem1";
            // 
            // ShowParagraphFormItem1
            // 
            this.ShowParagraphFormItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowParagraphFormItem1.Glyph")));
            this.ShowParagraphFormItem1.Id = 235;
            this.ShowParagraphFormItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowParagraphFormItem1.LargeGlyph")));
            this.ShowParagraphFormItem1.Name = "ShowParagraphFormItem1";
            // 
            // StylesBar1
            // 
            this.StylesBar1.DockCol = 20;
            this.StylesBar1.DockRow = 2;
            this.StylesBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.StylesBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeStyleItem1)});
            this.StylesBar1.Visible = false;
            // 
            // ChangeStyleItem1
            // 
            this.ChangeStyleItem1.Edit = this.RepositoryItemRichEditStyleEdit2;
            this.ChangeStyleItem1.Id = 236;
            this.ChangeStyleItem1.Name = "ChangeStyleItem1";
            // 
            // RepositoryItemRichEditStyleEdit2
            // 
            this.RepositoryItemRichEditStyleEdit2.AutoHeight = false;
            this.RepositoryItemRichEditStyleEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemRichEditStyleEdit2.Control = this.RichEditControl1;
            this.RepositoryItemRichEditStyleEdit2.Name = "RepositoryItemRichEditStyleEdit2";
            // 
            // EditingBar1
            // 
            this.EditingBar1.DockCol = 19;
            this.EditingBar1.DockRow = 2;
            this.EditingBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.EditingBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.FindItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ReplaceItem1)});
            this.EditingBar1.Visible = false;
            // 
            // FindItem1
            // 
            this.FindItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("FindItem1.Glyph")));
            this.FindItem1.Id = 237;
            this.FindItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("FindItem1.LargeGlyph")));
            this.FindItem1.Name = "FindItem1";
            // 
            // ReplaceItem1
            // 
            this.ReplaceItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ReplaceItem1.Glyph")));
            this.ReplaceItem1.Id = 238;
            this.ReplaceItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ReplaceItem1.LargeGlyph")));
            this.ReplaceItem1.Name = "ReplaceItem1";
            // 
            // PagesBar1
            // 
            this.PagesBar1.DockCol = 18;
            this.PagesBar1.DockRow = 2;
            this.PagesBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.PagesBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertPageBreakItem1)});
            this.PagesBar1.Visible = false;
            // 
            // InsertPageBreakItem1
            // 
            this.InsertPageBreakItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertPageBreakItem1.Glyph")));
            this.InsertPageBreakItem1.Id = 239;
            this.InsertPageBreakItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertPageBreakItem1.LargeGlyph")));
            this.InsertPageBreakItem1.Name = "InsertPageBreakItem1";
            // 
            // TablesBar1
            // 
            this.TablesBar1.DockCol = 17;
            this.TablesBar1.DockRow = 2;
            this.TablesBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.TablesBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertTableItem1)});
            this.TablesBar1.Visible = false;
            // 
            // InsertTableItem1
            // 
            this.InsertTableItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertTableItem1.Glyph")));
            this.InsertTableItem1.Id = 240;
            this.InsertTableItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertTableItem1.LargeGlyph")));
            this.InsertTableItem1.Name = "InsertTableItem1";
            // 
            // IllustrationsBar1
            // 
            this.IllustrationsBar1.DockCol = 16;
            this.IllustrationsBar1.DockRow = 2;
            this.IllustrationsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.IllustrationsBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertPictureItem1)});
            this.IllustrationsBar1.Visible = false;
            // 
            // InsertPictureItem1
            // 
            this.InsertPictureItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertPictureItem1.Glyph")));
            this.InsertPictureItem1.Id = 241;
            this.InsertPictureItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertPictureItem1.LargeGlyph")));
            this.InsertPictureItem1.Name = "InsertPictureItem1";
            // 
            // LinksBar1
            // 
            this.LinksBar1.DockCol = 15;
            this.LinksBar1.DockRow = 2;
            this.LinksBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.LinksBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertBookmarkItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertHyperlinkItem1)});
            this.LinksBar1.Visible = false;
            // 
            // InsertBookmarkItem1
            // 
            this.InsertBookmarkItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertBookmarkItem1.Glyph")));
            this.InsertBookmarkItem1.Id = 242;
            this.InsertBookmarkItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertBookmarkItem1.LargeGlyph")));
            this.InsertBookmarkItem1.Name = "InsertBookmarkItem1";
            // 
            // InsertHyperlinkItem1
            // 
            this.InsertHyperlinkItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertHyperlinkItem1.Glyph")));
            this.InsertHyperlinkItem1.Id = 243;
            this.InsertHyperlinkItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertHyperlinkItem1.LargeGlyph")));
            this.InsertHyperlinkItem1.Name = "InsertHyperlinkItem1";
            // 
            // HeaderFooterBar1
            // 
            this.HeaderFooterBar1.DockCol = 14;
            this.HeaderFooterBar1.DockRow = 2;
            this.HeaderFooterBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.HeaderFooterBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.EditPageHeaderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.EditPageFooterItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertPageNumberItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertPageCountItem1)});
            this.HeaderFooterBar1.Visible = false;
            // 
            // EditPageHeaderItem1
            // 
            this.EditPageHeaderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("EditPageHeaderItem1.Glyph")));
            this.EditPageHeaderItem1.Id = 244;
            this.EditPageHeaderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("EditPageHeaderItem1.LargeGlyph")));
            this.EditPageHeaderItem1.Name = "EditPageHeaderItem1";
            // 
            // EditPageFooterItem1
            // 
            this.EditPageFooterItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("EditPageFooterItem1.Glyph")));
            this.EditPageFooterItem1.Id = 245;
            this.EditPageFooterItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("EditPageFooterItem1.LargeGlyph")));
            this.EditPageFooterItem1.Name = "EditPageFooterItem1";
            // 
            // InsertPageNumberItem1
            // 
            this.InsertPageNumberItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertPageNumberItem1.Glyph")));
            this.InsertPageNumberItem1.Id = 246;
            this.InsertPageNumberItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertPageNumberItem1.LargeGlyph")));
            this.InsertPageNumberItem1.Name = "InsertPageNumberItem1";
            // 
            // InsertPageCountItem1
            // 
            this.InsertPageCountItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertPageCountItem1.Glyph")));
            this.InsertPageCountItem1.Id = 247;
            this.InsertPageCountItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertPageCountItem1.LargeGlyph")));
            this.InsertPageCountItem1.Name = "InsertPageCountItem1";
            // 
            // SymbolsBar1
            // 
            this.SymbolsBar1.DockCol = 13;
            this.SymbolsBar1.DockRow = 2;
            this.SymbolsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.SymbolsBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertSymbolItem1)});
            this.SymbolsBar1.Visible = false;
            // 
            // InsertSymbolItem1
            // 
            this.InsertSymbolItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertSymbolItem1.Glyph")));
            this.InsertSymbolItem1.Id = 248;
            this.InsertSymbolItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertSymbolItem1.LargeGlyph")));
            this.InsertSymbolItem1.Name = "InsertSymbolItem1";
            // 
            // PageSetupBar1
            // 
            this.PageSetupBar1.DockCol = 12;
            this.PageSetupBar1.DockRow = 2;
            this.PageSetupBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.PageSetupBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeSectionPageMarginsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeSectionPageOrientationItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeSectionColumnsItem1)});
            this.PageSetupBar1.Visible = false;
            // 
            // ChangeSectionPageMarginsItem1
            // 
            this.ChangeSectionPageMarginsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeSectionPageMarginsItem1.Glyph")));
            this.ChangeSectionPageMarginsItem1.Id = 249;
            this.ChangeSectionPageMarginsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeSectionPageMarginsItem1.LargeGlyph")));
            this.ChangeSectionPageMarginsItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.SetNormalSectionPageMarginsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetNarrowSectionPageMarginsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetModerateSectionPageMarginsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetWideSectionPageMarginsItem1)});
            this.ChangeSectionPageMarginsItem1.Name = "ChangeSectionPageMarginsItem1";
            // 
            // SetNormalSectionPageMarginsItem1
            // 
            this.SetNormalSectionPageMarginsItem1.Id = 250;
            this.SetNormalSectionPageMarginsItem1.Name = "SetNormalSectionPageMarginsItem1";
            // 
            // SetNarrowSectionPageMarginsItem1
            // 
            this.SetNarrowSectionPageMarginsItem1.Id = 251;
            this.SetNarrowSectionPageMarginsItem1.Name = "SetNarrowSectionPageMarginsItem1";
            // 
            // SetModerateSectionPageMarginsItem1
            // 
            this.SetModerateSectionPageMarginsItem1.Id = 252;
            this.SetModerateSectionPageMarginsItem1.Name = "SetModerateSectionPageMarginsItem1";
            // 
            // SetWideSectionPageMarginsItem1
            // 
            this.SetWideSectionPageMarginsItem1.Id = 253;
            this.SetWideSectionPageMarginsItem1.Name = "SetWideSectionPageMarginsItem1";
            // 
            // ChangeSectionPageOrientationItem1
            // 
            this.ChangeSectionPageOrientationItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeSectionPageOrientationItem1.Glyph")));
            this.ChangeSectionPageOrientationItem1.Id = 254;
            this.ChangeSectionPageOrientationItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeSectionPageOrientationItem1.LargeGlyph")));
            this.ChangeSectionPageOrientationItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.SetPortraitPageOrientationItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetLandscapePageOrientationItem1)});
            this.ChangeSectionPageOrientationItem1.Name = "ChangeSectionPageOrientationItem1";
            // 
            // SetPortraitPageOrientationItem1
            // 
            this.SetPortraitPageOrientationItem1.Id = 255;
            this.SetPortraitPageOrientationItem1.Name = "SetPortraitPageOrientationItem1";
            // 
            // SetLandscapePageOrientationItem1
            // 
            this.SetLandscapePageOrientationItem1.Id = 256;
            this.SetLandscapePageOrientationItem1.Name = "SetLandscapePageOrientationItem1";
            // 
            // ChangeSectionColumnsItem1
            // 
            this.ChangeSectionColumnsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeSectionColumnsItem1.Glyph")));
            this.ChangeSectionColumnsItem1.Id = 257;
            this.ChangeSectionColumnsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeSectionColumnsItem1.LargeGlyph")));
            this.ChangeSectionColumnsItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.SetSectionOneColumnItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetSectionTwoColumnsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SetSectionThreeColumnsItem1)});
            this.ChangeSectionColumnsItem1.Name = "ChangeSectionColumnsItem1";
            // 
            // SetSectionOneColumnItem1
            // 
            this.SetSectionOneColumnItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SetSectionOneColumnItem1.Glyph")));
            this.SetSectionOneColumnItem1.Id = 258;
            this.SetSectionOneColumnItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SetSectionOneColumnItem1.LargeGlyph")));
            this.SetSectionOneColumnItem1.Name = "SetSectionOneColumnItem1";
            // 
            // SetSectionTwoColumnsItem1
            // 
            this.SetSectionTwoColumnsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SetSectionTwoColumnsItem1.Glyph")));
            this.SetSectionTwoColumnsItem1.Id = 259;
            this.SetSectionTwoColumnsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SetSectionTwoColumnsItem1.LargeGlyph")));
            this.SetSectionTwoColumnsItem1.Name = "SetSectionTwoColumnsItem1";
            // 
            // SetSectionThreeColumnsItem1
            // 
            this.SetSectionThreeColumnsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SetSectionThreeColumnsItem1.Glyph")));
            this.SetSectionThreeColumnsItem1.Id = 260;
            this.SetSectionThreeColumnsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SetSectionThreeColumnsItem1.LargeGlyph")));
            this.SetSectionThreeColumnsItem1.Name = "SetSectionThreeColumnsItem1";
            // 
            // MailMergeBar1
            // 
            this.MailMergeBar1.DockCol = 11;
            this.MailMergeBar1.DockRow = 2;
            this.MailMergeBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.MailMergeBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertMergeFieldItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowAllFieldCodesItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowAllFieldResultsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleViewMergedDataItem1)});
            this.MailMergeBar1.Visible = false;
            // 
            // InsertMergeFieldItem1
            // 
            this.InsertMergeFieldItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertMergeFieldItem1.Glyph")));
            this.InsertMergeFieldItem1.Id = 261;
            this.InsertMergeFieldItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertMergeFieldItem1.LargeGlyph")));
            this.InsertMergeFieldItem1.Name = "InsertMergeFieldItem1";
            // 
            // ShowAllFieldCodesItem1
            // 
            this.ShowAllFieldCodesItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowAllFieldCodesItem1.Glyph")));
            this.ShowAllFieldCodesItem1.Id = 262;
            this.ShowAllFieldCodesItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowAllFieldCodesItem1.LargeGlyph")));
            this.ShowAllFieldCodesItem1.Name = "ShowAllFieldCodesItem1";
            // 
            // ShowAllFieldResultsItem1
            // 
            this.ShowAllFieldResultsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowAllFieldResultsItem1.Glyph")));
            this.ShowAllFieldResultsItem1.Id = 263;
            this.ShowAllFieldResultsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowAllFieldResultsItem1.LargeGlyph")));
            this.ShowAllFieldResultsItem1.Name = "ShowAllFieldResultsItem1";
            // 
            // ToggleViewMergedDataItem1
            // 
            this.ToggleViewMergedDataItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleViewMergedDataItem1.Glyph")));
            this.ToggleViewMergedDataItem1.Id = 264;
            this.ToggleViewMergedDataItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleViewMergedDataItem1.LargeGlyph")));
            this.ToggleViewMergedDataItem1.Name = "ToggleViewMergedDataItem1";
            // 
            // DocumentViewsBar1
            // 
            this.DocumentViewsBar1.DockCol = 10;
            this.DocumentViewsBar1.DockRow = 2;
            this.DocumentViewsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.DocumentViewsBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.SwitchToSimpleViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SwitchToDraftViewItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SwitchToPrintLayoutViewItem1)});
            this.DocumentViewsBar1.Visible = false;
            // 
            // SwitchToSimpleViewItem1
            // 
            this.SwitchToSimpleViewItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SwitchToSimpleViewItem1.Glyph")));
            this.SwitchToSimpleViewItem1.Id = 265;
            this.SwitchToSimpleViewItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SwitchToSimpleViewItem1.LargeGlyph")));
            this.SwitchToSimpleViewItem1.Name = "SwitchToSimpleViewItem1";
            // 
            // SwitchToDraftViewItem1
            // 
            this.SwitchToDraftViewItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SwitchToDraftViewItem1.Glyph")));
            this.SwitchToDraftViewItem1.Id = 266;
            this.SwitchToDraftViewItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SwitchToDraftViewItem1.LargeGlyph")));
            this.SwitchToDraftViewItem1.Name = "SwitchToDraftViewItem1";
            // 
            // SwitchToPrintLayoutViewItem1
            // 
            this.SwitchToPrintLayoutViewItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SwitchToPrintLayoutViewItem1.Glyph")));
            this.SwitchToPrintLayoutViewItem1.Id = 267;
            this.SwitchToPrintLayoutViewItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SwitchToPrintLayoutViewItem1.LargeGlyph")));
            this.SwitchToPrintLayoutViewItem1.Name = "SwitchToPrintLayoutViewItem1";
            // 
            // ShowBar1
            // 
            this.ShowBar1.DockCol = 9;
            this.ShowBar1.DockRow = 2;
            this.ShowBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.ShowBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleShowHorizontalRulerItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleShowVerticalRulerItem1)});
            this.ShowBar1.Visible = false;
            // 
            // ToggleShowHorizontalRulerItem1
            // 
            this.ToggleShowHorizontalRulerItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowHorizontalRulerItem1.Glyph")));
            this.ToggleShowHorizontalRulerItem1.Id = 268;
            this.ToggleShowHorizontalRulerItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowHorizontalRulerItem1.LargeGlyph")));
            this.ToggleShowHorizontalRulerItem1.Name = "ToggleShowHorizontalRulerItem1";
            // 
            // ToggleShowVerticalRulerItem1
            // 
            this.ToggleShowVerticalRulerItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowVerticalRulerItem1.Glyph")));
            this.ToggleShowVerticalRulerItem1.Id = 269;
            this.ToggleShowVerticalRulerItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowVerticalRulerItem1.LargeGlyph")));
            this.ToggleShowVerticalRulerItem1.Name = "ToggleShowVerticalRulerItem1";
            // 
            // ZoomBar1
            // 
            this.ZoomBar1.DockCol = 7;
            this.ZoomBar1.DockRow = 2;
            this.ZoomBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.ZoomBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ZoomOutItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ZoomInItem1)});
            this.ZoomBar1.Visible = false;
            // 
            // ZoomOutItem1
            // 
            this.ZoomOutItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ZoomOutItem1.Glyph")));
            this.ZoomOutItem1.Id = 270;
            this.ZoomOutItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ZoomOutItem1.LargeGlyph")));
            this.ZoomOutItem1.Name = "ZoomOutItem1";
            // 
            // ZoomInItem1
            // 
            this.ZoomInItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ZoomInItem1.Glyph")));
            this.ZoomInItem1.Id = 271;
            this.ZoomInItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ZoomInItem1.LargeGlyph")));
            this.ZoomInItem1.Name = "ZoomInItem1";
            // 
            // DocumentProtectionBar1
            // 
            this.DocumentProtectionBar1.DockCol = 6;
            this.DocumentProtectionBar1.DockRow = 2;
            this.DocumentProtectionBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.DocumentProtectionBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ProtectDocumentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeRangeEditingPermissionsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.UnprotectDocumentItem1)});
            this.DocumentProtectionBar1.Visible = false;
            // 
            // ProtectDocumentItem1
            // 
            this.ProtectDocumentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ProtectDocumentItem1.Glyph")));
            this.ProtectDocumentItem1.Id = 272;
            this.ProtectDocumentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ProtectDocumentItem1.LargeGlyph")));
            this.ProtectDocumentItem1.Name = "ProtectDocumentItem1";
            // 
            // ChangeRangeEditingPermissionsItem1
            // 
            this.ChangeRangeEditingPermissionsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeRangeEditingPermissionsItem1.Glyph")));
            this.ChangeRangeEditingPermissionsItem1.Id = 273;
            this.ChangeRangeEditingPermissionsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeRangeEditingPermissionsItem1.LargeGlyph")));
            this.ChangeRangeEditingPermissionsItem1.Name = "ChangeRangeEditingPermissionsItem1";
            // 
            // UnprotectDocumentItem1
            // 
            this.UnprotectDocumentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("UnprotectDocumentItem1.Glyph")));
            this.UnprotectDocumentItem1.Id = 274;
            this.UnprotectDocumentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("UnprotectDocumentItem1.LargeGlyph")));
            this.UnprotectDocumentItem1.Name = "UnprotectDocumentItem1";
            // 
            // TableStylesBar1
            // 
            this.TableStylesBar1.DockCol = 5;
            this.TableStylesBar1.DockRow = 2;
            this.TableStylesBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.TableStylesBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeTableCellsShadingItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeTableBordersItem1)});
            this.TableStylesBar1.Visible = false;
            // 
            // ChangeTableCellsShadingItem1
            // 
            this.ChangeTableCellsShadingItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeTableCellsShadingItem1.Glyph")));
            this.ChangeTableCellsShadingItem1.Id = 275;
            this.ChangeTableCellsShadingItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeTableCellsShadingItem1.LargeGlyph")));
            this.ChangeTableCellsShadingItem1.Name = "ChangeTableCellsShadingItem1";
            // 
            // ChangeTableBordersItem1
            // 
            this.ChangeTableBordersItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeTableBordersItem1.Glyph")));
            this.ChangeTableBordersItem1.Id = 276;
            this.ChangeTableBordersItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeTableBordersItem1.LargeGlyph")));
            this.ChangeTableBordersItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsBottomBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsTopBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsLeftBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsRightBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ResetTableCellsAllBordersItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsAllBordersItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsOutsideBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsInsideBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsInsideHorizontalBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsInsideVerticalBorderItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleShowTableGridLinesItem1)});
            this.ChangeTableBordersItem1.Name = "ChangeTableBordersItem1";
            // 
            // ToggleTableCellsBottomBorderItem1
            // 
            this.ToggleTableCellsBottomBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomBorderItem1.Glyph")));
            this.ToggleTableCellsBottomBorderItem1.Id = 277;
            this.ToggleTableCellsBottomBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomBorderItem1.LargeGlyph")));
            this.ToggleTableCellsBottomBorderItem1.Name = "ToggleTableCellsBottomBorderItem1";
            // 
            // ToggleTableCellsTopBorderItem1
            // 
            this.ToggleTableCellsTopBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopBorderItem1.Glyph")));
            this.ToggleTableCellsTopBorderItem1.Id = 278;
            this.ToggleTableCellsTopBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopBorderItem1.LargeGlyph")));
            this.ToggleTableCellsTopBorderItem1.Name = "ToggleTableCellsTopBorderItem1";
            // 
            // ToggleTableCellsLeftBorderItem1
            // 
            this.ToggleTableCellsLeftBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsLeftBorderItem1.Glyph")));
            this.ToggleTableCellsLeftBorderItem1.Id = 279;
            this.ToggleTableCellsLeftBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsLeftBorderItem1.LargeGlyph")));
            this.ToggleTableCellsLeftBorderItem1.Name = "ToggleTableCellsLeftBorderItem1";
            // 
            // ToggleTableCellsRightBorderItem1
            // 
            this.ToggleTableCellsRightBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsRightBorderItem1.Glyph")));
            this.ToggleTableCellsRightBorderItem1.Id = 280;
            this.ToggleTableCellsRightBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsRightBorderItem1.LargeGlyph")));
            this.ToggleTableCellsRightBorderItem1.Name = "ToggleTableCellsRightBorderItem1";
            // 
            // ResetTableCellsAllBordersItem1
            // 
            this.ResetTableCellsAllBordersItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ResetTableCellsAllBordersItem1.Glyph")));
            this.ResetTableCellsAllBordersItem1.Id = 281;
            this.ResetTableCellsAllBordersItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ResetTableCellsAllBordersItem1.LargeGlyph")));
            this.ResetTableCellsAllBordersItem1.Name = "ResetTableCellsAllBordersItem1";
            // 
            // ToggleTableCellsAllBordersItem1
            // 
            this.ToggleTableCellsAllBordersItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsAllBordersItem1.Glyph")));
            this.ToggleTableCellsAllBordersItem1.Id = 282;
            this.ToggleTableCellsAllBordersItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsAllBordersItem1.LargeGlyph")));
            this.ToggleTableCellsAllBordersItem1.Name = "ToggleTableCellsAllBordersItem1";
            // 
            // ToggleTableCellsOutsideBorderItem1
            // 
            this.ToggleTableCellsOutsideBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsOutsideBorderItem1.Glyph")));
            this.ToggleTableCellsOutsideBorderItem1.Id = 283;
            this.ToggleTableCellsOutsideBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsOutsideBorderItem1.LargeGlyph")));
            this.ToggleTableCellsOutsideBorderItem1.Name = "ToggleTableCellsOutsideBorderItem1";
            // 
            // ToggleTableCellsInsideBorderItem1
            // 
            this.ToggleTableCellsInsideBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsInsideBorderItem1.Glyph")));
            this.ToggleTableCellsInsideBorderItem1.Id = 284;
            this.ToggleTableCellsInsideBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsInsideBorderItem1.LargeGlyph")));
            this.ToggleTableCellsInsideBorderItem1.Name = "ToggleTableCellsInsideBorderItem1";
            // 
            // ToggleTableCellsInsideHorizontalBorderItem1
            // 
            this.ToggleTableCellsInsideHorizontalBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsInsideHorizontalBorderItem1.Glyph")));
            this.ToggleTableCellsInsideHorizontalBorderItem1.Id = 285;
            this.ToggleTableCellsInsideHorizontalBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsInsideHorizontalBorderItem1.LargeGlyph")));
            this.ToggleTableCellsInsideHorizontalBorderItem1.Name = "ToggleTableCellsInsideHorizontalBorderItem1";
            // 
            // ToggleTableCellsInsideVerticalBorderItem1
            // 
            this.ToggleTableCellsInsideVerticalBorderItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsInsideVerticalBorderItem1.Glyph")));
            this.ToggleTableCellsInsideVerticalBorderItem1.Id = 286;
            this.ToggleTableCellsInsideVerticalBorderItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsInsideVerticalBorderItem1.LargeGlyph")));
            this.ToggleTableCellsInsideVerticalBorderItem1.Name = "ToggleTableCellsInsideVerticalBorderItem1";
            // 
            // ToggleShowTableGridLinesItem1
            // 
            this.ToggleShowTableGridLinesItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowTableGridLinesItem1.Glyph")));
            this.ToggleShowTableGridLinesItem1.Id = 287;
            this.ToggleShowTableGridLinesItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleShowTableGridLinesItem1.LargeGlyph")));
            this.ToggleShowTableGridLinesItem1.Name = "ToggleShowTableGridLinesItem1";
            // 
            // TableDrawBordersBar1
            // 
            this.TableDrawBordersBar1.DockCol = 4;
            this.TableDrawBordersBar1.DockRow = 2;
            this.TableDrawBordersBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.TableDrawBordersBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeTableBorderLineStyleItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeTableBorderLineWeightItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ChangeTableBorderColorItem1)});
            this.TableDrawBordersBar1.Visible = false;
            // 
            // ChangeTableBorderLineStyleItem1
            // 
            this.ChangeTableBorderLineStyleItem1.Edit = this.RepositoryItemBorderLineStyle1;
            borderInfo1.Color = System.Drawing.Color.Black;
            borderInfo1.Frame = false;
            borderInfo1.Offset = 0;
            borderInfo1.Shadow = false;
            borderInfo1.Style = DevExpress.XtraRichEdit.Model.BorderLineStyle.Single;
            borderInfo1.Width = 10;
            this.ChangeTableBorderLineStyleItem1.EditValue = borderInfo1;
            this.ChangeTableBorderLineStyleItem1.Id = 288;
            this.ChangeTableBorderLineStyleItem1.Name = "ChangeTableBorderLineStyleItem1";
            // 
            // RepositoryItemBorderLineStyle1
            // 
            this.RepositoryItemBorderLineStyle1.AutoHeight = false;
            this.RepositoryItemBorderLineStyle1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemBorderLineStyle1.Control = this.RichEditControl1;
            this.RepositoryItemBorderLineStyle1.Name = "RepositoryItemBorderLineStyle1";
            // 
            // ChangeTableBorderLineWeightItem1
            // 
            this.ChangeTableBorderLineWeightItem1.Edit = this.RepositoryItemBorderLineWeight1;
            this.ChangeTableBorderLineWeightItem1.EditValue = 20;
            this.ChangeTableBorderLineWeightItem1.Id = 289;
            this.ChangeTableBorderLineWeightItem1.Name = "ChangeTableBorderLineWeightItem1";
            // 
            // RepositoryItemBorderLineWeight1
            // 
            this.RepositoryItemBorderLineWeight1.AutoHeight = false;
            this.RepositoryItemBorderLineWeight1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemBorderLineWeight1.Control = this.RichEditControl1;
            this.RepositoryItemBorderLineWeight1.Name = "RepositoryItemBorderLineWeight1";
            // 
            // ChangeTableBorderColorItem1
            // 
            this.ChangeTableBorderColorItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ChangeTableBorderColorItem1.Glyph")));
            this.ChangeTableBorderColorItem1.Id = 290;
            this.ChangeTableBorderColorItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ChangeTableBorderColorItem1.LargeGlyph")));
            this.ChangeTableBorderColorItem1.Name = "ChangeTableBorderColorItem1";
            // 
            // TableTableBar1
            // 
            this.TableTableBar1.DockCol = 3;
            this.TableTableBar1.DockRow = 2;
            this.TableTableBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.TableTableBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.SelectTableElementsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleShowTableGridLinesItem1)});
            this.TableTableBar1.Visible = false;
            // 
            // SelectTableElementsItem1
            // 
            this.SelectTableElementsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SelectTableElementsItem1.Glyph")));
            this.SelectTableElementsItem1.Id = 291;
            this.SelectTableElementsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SelectTableElementsItem1.LargeGlyph")));
            this.SelectTableElementsItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.SelectTableCellItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SelectTableColumnItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SelectTableRowItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SelectTableItem1)});
            this.SelectTableElementsItem1.Name = "SelectTableElementsItem1";
            // 
            // SelectTableCellItem1
            // 
            this.SelectTableCellItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SelectTableCellItem1.Glyph")));
            this.SelectTableCellItem1.Id = 292;
            this.SelectTableCellItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SelectTableCellItem1.LargeGlyph")));
            this.SelectTableCellItem1.Name = "SelectTableCellItem1";
            // 
            // SelectTableColumnItem1
            // 
            this.SelectTableColumnItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SelectTableColumnItem1.Glyph")));
            this.SelectTableColumnItem1.Id = 293;
            this.SelectTableColumnItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SelectTableColumnItem1.LargeGlyph")));
            this.SelectTableColumnItem1.Name = "SelectTableColumnItem1";
            // 
            // SelectTableRowItem1
            // 
            this.SelectTableRowItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SelectTableRowItem1.Glyph")));
            this.SelectTableRowItem1.Id = 294;
            this.SelectTableRowItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SelectTableRowItem1.LargeGlyph")));
            this.SelectTableRowItem1.Name = "SelectTableRowItem1";
            // 
            // SelectTableItem1
            // 
            this.SelectTableItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SelectTableItem1.Glyph")));
            this.SelectTableItem1.Id = 295;
            this.SelectTableItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SelectTableItem1.LargeGlyph")));
            this.SelectTableItem1.Name = "SelectTableItem1";
            // 
            // TableRowsAndColumnsBar1
            // 
            this.TableRowsAndColumnsBar1.DockCol = 2;
            this.TableRowsAndColumnsBar1.DockRow = 2;
            this.TableRowsAndColumnsBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.TableRowsAndColumnsBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.DeleteTableElementsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertTableRowAboveItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertTableRowBelowItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertTableColumnToLeftItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.InsertTableColumnToRightItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowInsertTableCellsFormItem1)});
            this.TableRowsAndColumnsBar1.Offset = 4;
            this.TableRowsAndColumnsBar1.Visible = false;
            // 
            // DeleteTableElementsItem1
            // 
            this.DeleteTableElementsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableElementsItem1.Glyph")));
            this.DeleteTableElementsItem1.Id = 296;
            this.DeleteTableElementsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableElementsItem1.LargeGlyph")));
            this.DeleteTableElementsItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowDeleteTableCellsFormItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.DeleteTableColumnsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.DeleteTableRowsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.DeleteTableItem1)});
            this.DeleteTableElementsItem1.Name = "DeleteTableElementsItem1";
            // 
            // ShowDeleteTableCellsFormItem1
            // 
            this.ShowDeleteTableCellsFormItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowDeleteTableCellsFormItem1.Glyph")));
            this.ShowDeleteTableCellsFormItem1.Id = 297;
            this.ShowDeleteTableCellsFormItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowDeleteTableCellsFormItem1.LargeGlyph")));
            this.ShowDeleteTableCellsFormItem1.Name = "ShowDeleteTableCellsFormItem1";
            // 
            // DeleteTableColumnsItem1
            // 
            this.DeleteTableColumnsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableColumnsItem1.Glyph")));
            this.DeleteTableColumnsItem1.Id = 298;
            this.DeleteTableColumnsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableColumnsItem1.LargeGlyph")));
            this.DeleteTableColumnsItem1.Name = "DeleteTableColumnsItem1";
            // 
            // DeleteTableRowsItem1
            // 
            this.DeleteTableRowsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableRowsItem1.Glyph")));
            this.DeleteTableRowsItem1.Id = 299;
            this.DeleteTableRowsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableRowsItem1.LargeGlyph")));
            this.DeleteTableRowsItem1.Name = "DeleteTableRowsItem1";
            // 
            // DeleteTableItem1
            // 
            this.DeleteTableItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableItem1.Glyph")));
            this.DeleteTableItem1.Id = 300;
            this.DeleteTableItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("DeleteTableItem1.LargeGlyph")));
            this.DeleteTableItem1.Name = "DeleteTableItem1";
            // 
            // InsertTableRowAboveItem1
            // 
            this.InsertTableRowAboveItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertTableRowAboveItem1.Glyph")));
            this.InsertTableRowAboveItem1.Id = 301;
            this.InsertTableRowAboveItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertTableRowAboveItem1.LargeGlyph")));
            this.InsertTableRowAboveItem1.Name = "InsertTableRowAboveItem1";
            // 
            // InsertTableRowBelowItem1
            // 
            this.InsertTableRowBelowItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertTableRowBelowItem1.Glyph")));
            this.InsertTableRowBelowItem1.Id = 302;
            this.InsertTableRowBelowItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertTableRowBelowItem1.LargeGlyph")));
            this.InsertTableRowBelowItem1.Name = "InsertTableRowBelowItem1";
            // 
            // InsertTableColumnToLeftItem1
            // 
            this.InsertTableColumnToLeftItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertTableColumnToLeftItem1.Glyph")));
            this.InsertTableColumnToLeftItem1.Id = 303;
            this.InsertTableColumnToLeftItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertTableColumnToLeftItem1.LargeGlyph")));
            this.InsertTableColumnToLeftItem1.Name = "InsertTableColumnToLeftItem1";
            // 
            // InsertTableColumnToRightItem1
            // 
            this.InsertTableColumnToRightItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("InsertTableColumnToRightItem1.Glyph")));
            this.InsertTableColumnToRightItem1.Id = 304;
            this.InsertTableColumnToRightItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("InsertTableColumnToRightItem1.LargeGlyph")));
            this.InsertTableColumnToRightItem1.Name = "InsertTableColumnToRightItem1";
            // 
            // ShowInsertTableCellsFormItem1
            // 
            this.ShowInsertTableCellsFormItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowInsertTableCellsFormItem1.Glyph")));
            this.ShowInsertTableCellsFormItem1.Id = 305;
            this.ShowInsertTableCellsFormItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowInsertTableCellsFormItem1.LargeGlyph")));
            this.ShowInsertTableCellsFormItem1.Name = "ShowInsertTableCellsFormItem1";
            // 
            // TableMergeBar1
            // 
            this.TableMergeBar1.DockCol = 1;
            this.TableMergeBar1.DockRow = 2;
            this.TableMergeBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.TableMergeBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.MergeTableCellsItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ShowSplitTableCellsForm1),
            new DevExpress.XtraBars.LinkPersistInfo(this.SplitTableItem1)});
            this.TableMergeBar1.Offset = 5;
            this.TableMergeBar1.Visible = false;
            // 
            // MergeTableCellsItem1
            // 
            this.MergeTableCellsItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("MergeTableCellsItem1.Glyph")));
            this.MergeTableCellsItem1.Id = 306;
            this.MergeTableCellsItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("MergeTableCellsItem1.LargeGlyph")));
            this.MergeTableCellsItem1.Name = "MergeTableCellsItem1";
            // 
            // ShowSplitTableCellsForm1
            // 
            this.ShowSplitTableCellsForm1.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowSplitTableCellsForm1.Glyph")));
            this.ShowSplitTableCellsForm1.Id = 307;
            this.ShowSplitTableCellsForm1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowSplitTableCellsForm1.LargeGlyph")));
            this.ShowSplitTableCellsForm1.Name = "ShowSplitTableCellsForm1";
            // 
            // SplitTableItem1
            // 
            this.SplitTableItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("SplitTableItem1.Glyph")));
            this.SplitTableItem1.Id = 308;
            this.SplitTableItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("SplitTableItem1.LargeGlyph")));
            this.SplitTableItem1.Name = "SplitTableItem1";
            // 
            // TableAlignmentBar1
            // 
            this.TableAlignmentBar1.DockCol = 0;
            this.TableAlignmentBar1.DockRow = 2;
            this.TableAlignmentBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.TableAlignmentBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsTopLeftAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsTopCenterAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsTopRightAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsMiddleLeftAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsMiddleCenterAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsMiddleRightAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsBottomLeftAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsBottomCenterAlignmentItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.ToggleTableCellsBottomRightAlignmentItem1)});
            this.TableAlignmentBar1.Offset = 2;
            this.TableAlignmentBar1.Visible = false;
            // 
            // ToggleTableCellsTopLeftAlignmentItem1
            // 
            this.ToggleTableCellsTopLeftAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopLeftAlignmentItem1.Glyph")));
            this.ToggleTableCellsTopLeftAlignmentItem1.Id = 309;
            this.ToggleTableCellsTopLeftAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopLeftAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsTopLeftAlignmentItem1.Name = "ToggleTableCellsTopLeftAlignmentItem1";
            // 
            // ToggleTableCellsTopCenterAlignmentItem1
            // 
            this.ToggleTableCellsTopCenterAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopCenterAlignmentItem1.Glyph")));
            this.ToggleTableCellsTopCenterAlignmentItem1.Id = 310;
            this.ToggleTableCellsTopCenterAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopCenterAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsTopCenterAlignmentItem1.Name = "ToggleTableCellsTopCenterAlignmentItem1";
            // 
            // ToggleTableCellsTopRightAlignmentItem1
            // 
            this.ToggleTableCellsTopRightAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopRightAlignmentItem1.Glyph")));
            this.ToggleTableCellsTopRightAlignmentItem1.Id = 311;
            this.ToggleTableCellsTopRightAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsTopRightAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsTopRightAlignmentItem1.Name = "ToggleTableCellsTopRightAlignmentItem1";
            // 
            // ToggleTableCellsMiddleLeftAlignmentItem1
            // 
            this.ToggleTableCellsMiddleLeftAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsMiddleLeftAlignmentItem1.Glyph")));
            this.ToggleTableCellsMiddleLeftAlignmentItem1.Id = 312;
            this.ToggleTableCellsMiddleLeftAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsMiddleLeftAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsMiddleLeftAlignmentItem1.Name = "ToggleTableCellsMiddleLeftAlignmentItem1";
            // 
            // ToggleTableCellsMiddleCenterAlignmentItem1
            // 
            this.ToggleTableCellsMiddleCenterAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsMiddleCenterAlignmentItem1.Glyph")));
            this.ToggleTableCellsMiddleCenterAlignmentItem1.Id = 313;
            this.ToggleTableCellsMiddleCenterAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsMiddleCenterAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsMiddleCenterAlignmentItem1.Name = "ToggleTableCellsMiddleCenterAlignmentItem1";
            // 
            // ToggleTableCellsMiddleRightAlignmentItem1
            // 
            this.ToggleTableCellsMiddleRightAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsMiddleRightAlignmentItem1.Glyph")));
            this.ToggleTableCellsMiddleRightAlignmentItem1.Id = 314;
            this.ToggleTableCellsMiddleRightAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsMiddleRightAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsMiddleRightAlignmentItem1.Name = "ToggleTableCellsMiddleRightAlignmentItem1";
            // 
            // ToggleTableCellsBottomLeftAlignmentItem1
            // 
            this.ToggleTableCellsBottomLeftAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomLeftAlignmentItem1.Glyph")));
            this.ToggleTableCellsBottomLeftAlignmentItem1.Id = 315;
            this.ToggleTableCellsBottomLeftAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomLeftAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsBottomLeftAlignmentItem1.Name = "ToggleTableCellsBottomLeftAlignmentItem1";
            // 
            // ToggleTableCellsBottomCenterAlignmentItem1
            // 
            this.ToggleTableCellsBottomCenterAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomCenterAlignmentItem1.Glyph")));
            this.ToggleTableCellsBottomCenterAlignmentItem1.Id = 316;
            this.ToggleTableCellsBottomCenterAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomCenterAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsBottomCenterAlignmentItem1.Name = "ToggleTableCellsBottomCenterAlignmentItem1";
            // 
            // ToggleTableCellsBottomRightAlignmentItem1
            // 
            this.ToggleTableCellsBottomRightAlignmentItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomRightAlignmentItem1.Glyph")));
            this.ToggleTableCellsBottomRightAlignmentItem1.Id = 317;
            this.ToggleTableCellsBottomRightAlignmentItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ToggleTableCellsBottomRightAlignmentItem1.LargeGlyph")));
            this.ToggleTableCellsBottomRightAlignmentItem1.Name = "ToggleTableCellsBottomRightAlignmentItem1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(690, 77);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 474);
            this.barDockControlBottom.Size = new System.Drawing.Size(690, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 77);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 397);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(690, 77);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 397);
            // 
            // ImageList16x16
            // 
            this.ImageList16x16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList16x16.ImageStream")));
            this.ImageList16x16.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList16x16.Images.SetKeyName(0, "ZoomOut_16x16.png");
            this.ImageList16x16.Images.SetKeyName(1, "AlignCenter_16x16.png");
            this.ImageList16x16.Images.SetKeyName(2, "AlignJustify_16x16.png");
            this.ImageList16x16.Images.SetKeyName(3, "AlignLeft_16x16.png");
            this.ImageList16x16.Images.SetKeyName(4, "AlignRight_16x16.png");
            this.ImageList16x16.Images.SetKeyName(5, "Background_16x16.png");
            this.ImageList16x16.Images.SetKeyName(6, "Bold_16x16.png");
            this.ImageList16x16.Images.SetKeyName(7, "Columns_16x16.png");
            this.ImageList16x16.Images.SetKeyName(8, "Copy_16x16.png");
            this.ImageList16x16.Images.SetKeyName(9, "Cut_16x16.png");
            this.ImageList16x16.Images.SetKeyName(10, "Delete_16x16.png");
            this.ImageList16x16.Images.SetKeyName(11, "Find_16x16.png");
            this.ImageList16x16.Images.SetKeyName(12, "Font_16x16.png");
            this.ImageList16x16.Images.SetKeyName(13, "FontColor_16x16.png");
            this.ImageList16x16.Images.SetKeyName(14, "FontSize_16x16.png");
            this.ImageList16x16.Images.SetKeyName(15, "FontSizeDecrease_16x16.png");
            this.ImageList16x16.Images.SetKeyName(16, "FontSizeIncrease_16x16.png");
            this.ImageList16x16.Images.SetKeyName(17, "Highlight_16x16.png");
            this.ImageList16x16.Images.SetKeyName(18, "IndentDecrease_16x16.png");
            this.ImageList16x16.Images.SetKeyName(19, "IndentIncrease_16x16.png");
            this.ImageList16x16.Images.SetKeyName(20, "InsertImage_16x16.png");
            this.ImageList16x16.Images.SetKeyName(21, "InsertPageBreak_16x16.png");
            this.ImageList16x16.Images.SetKeyName(22, "Italic_16x16.png");
            this.ImageList16x16.Images.SetKeyName(23, "ListBullets_16x16.png");
            this.ImageList16x16.Images.SetKeyName(24, "ListMultilevel_16x16.png");
            this.ImageList16x16.Images.SetKeyName(25, "ListNumbers_16x16.png");
            this.ImageList16x16.Images.SetKeyName(26, "New_16x16.png");
            this.ImageList16x16.Images.SetKeyName(27, "Open_16x16.png");
            this.ImageList16x16.Images.SetKeyName(28, "Paragraph_16x16.png");
            this.ImageList16x16.Images.SetKeyName(29, "Paste_16x16.png");
            this.ImageList16x16.Images.SetKeyName(30, "Preview_16x16.png");
            this.ImageList16x16.Images.SetKeyName(31, "Print_16x16.png");
            this.ImageList16x16.Images.SetKeyName(32, "PrintDialog_16x16.png");
            this.ImageList16x16.Images.SetKeyName(33, "Redo_16x16.png");
            this.ImageList16x16.Images.SetKeyName(34, "Replace_16x16.png");
            this.ImageList16x16.Images.SetKeyName(35, "Save_16x16.png");
            this.ImageList16x16.Images.SetKeyName(36, "SaveAll_16x16.png");
            this.ImageList16x16.Images.SetKeyName(37, "SaveAs_16x16.png");
            this.ImageList16x16.Images.SetKeyName(38, "ShowHidden_16x16.png");
            this.ImageList16x16.Images.SetKeyName(39, "SpellCheck_16x16.png");
            this.ImageList16x16.Images.SetKeyName(40, "Strikeout_16x16.png");
            this.ImageList16x16.Images.SetKeyName(41, "StrikeoutDouble_16x16.png");
            this.ImageList16x16.Images.SetKeyName(42, "Subscript_16x16.png");
            this.ImageList16x16.Images.SetKeyName(43, "Superscript_16x16.png");
            this.ImageList16x16.Images.SetKeyName(44, "Symbol_16x16.png");
            this.ImageList16x16.Images.SetKeyName(45, "Underline_16x16.png");
            this.ImageList16x16.Images.SetKeyName(46, "UnderlineDouble_16x16.png");
            this.ImageList16x16.Images.SetKeyName(47, "UnderlineWord_16x16.png");
            this.ImageList16x16.Images.SetKeyName(48, "Undo_16x16.png");
            this.ImageList16x16.Images.SetKeyName(49, "ZoomIn_16x16.png");
            // 
            // BarSubItem_View_toolbars
            // 
            this.BarSubItem_View_toolbars.Caption = "Toolbars";
            this.BarSubItem_View_toolbars.Id = 70;
            this.BarSubItem_View_toolbars.Name = "BarSubItem_View_toolbars";
            // 
            // BarButtonItem1
            // 
            this.BarButtonItem1.Caption = "BarButtonItem1";
            this.BarButtonItem1.Id = 136;
            this.BarButtonItem1.Name = "BarButtonItem1";
            // 
            // BarToolbarsListItem1
            // 
            this.BarToolbarsListItem1.Caption = "Toolbars";
            this.BarToolbarsListItem1.Id = 137;
            this.BarToolbarsListItem1.Name = "BarToolbarsListItem1";
            this.BarToolbarsListItem1.ShowCustomizationItem = false;
            // 
            // ImageList32x32
            // 
            this.ImageList32x32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList32x32.ImageStream")));
            this.ImageList32x32.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList32x32.Images.SetKeyName(0, "ZoomOut_32x32.png");
            this.ImageList32x32.Images.SetKeyName(1, "AlignCenter_32x32.png");
            this.ImageList32x32.Images.SetKeyName(2, "AlignJustify_32x32.png");
            this.ImageList32x32.Images.SetKeyName(3, "AlignLeft_32x32.png");
            this.ImageList32x32.Images.SetKeyName(4, "AlignRight_32x32.png");
            this.ImageList32x32.Images.SetKeyName(5, "Background_32x32.png");
            this.ImageList32x32.Images.SetKeyName(6, "Bold_32x32.png");
            this.ImageList32x32.Images.SetKeyName(7, "Columns_32x32.png");
            this.ImageList32x32.Images.SetKeyName(8, "Copy_32x32.png");
            this.ImageList32x32.Images.SetKeyName(9, "Cut_32x32.png");
            this.ImageList32x32.Images.SetKeyName(10, "Delete_32x32.png");
            this.ImageList32x32.Images.SetKeyName(11, "Find_32x32.png");
            this.ImageList32x32.Images.SetKeyName(12, "Font_32x32.png");
            this.ImageList32x32.Images.SetKeyName(13, "FontColor_32x32.png");
            this.ImageList32x32.Images.SetKeyName(14, "FontSize_32x32.png");
            this.ImageList32x32.Images.SetKeyName(15, "FontSizeDecrease_32x32.png");
            this.ImageList32x32.Images.SetKeyName(16, "FontSizeIncrease_32x32.png");
            this.ImageList32x32.Images.SetKeyName(17, "Highlight_32x32.png");
            this.ImageList32x32.Images.SetKeyName(18, "IndentDecrease_32x32.png");
            this.ImageList32x32.Images.SetKeyName(19, "IndentIncrease_32x32.png");
            this.ImageList32x32.Images.SetKeyName(20, "InsertImage_32x32.png");
            this.ImageList32x32.Images.SetKeyName(21, "InsertPageBreak_32x32.png");
            this.ImageList32x32.Images.SetKeyName(22, "Italic_32x32.png");
            this.ImageList32x32.Images.SetKeyName(23, "ListBullets_32x32.png");
            this.ImageList32x32.Images.SetKeyName(24, "ListMultilevel_32x32.png");
            this.ImageList32x32.Images.SetKeyName(25, "ListNumbers_32x32.png");
            this.ImageList32x32.Images.SetKeyName(26, "New_32x32.png");
            this.ImageList32x32.Images.SetKeyName(27, "Open_32x32.png");
            this.ImageList32x32.Images.SetKeyName(28, "Paragraph_32x32.png");
            this.ImageList32x32.Images.SetKeyName(29, "Paste_32x32.png");
            this.ImageList32x32.Images.SetKeyName(30, "Preview_32x32.png");
            this.ImageList32x32.Images.SetKeyName(31, "Print_32x32.png");
            this.ImageList32x32.Images.SetKeyName(32, "PrintDialog_32x32.png");
            this.ImageList32x32.Images.SetKeyName(33, "Redo_32x32.png");
            this.ImageList32x32.Images.SetKeyName(34, "Replace_32x32.png");
            this.ImageList32x32.Images.SetKeyName(35, "Save_32x32.png");
            this.ImageList32x32.Images.SetKeyName(36, "SaveAll_32x32.png");
            this.ImageList32x32.Images.SetKeyName(37, "SaveAs_32x32.png");
            this.ImageList32x32.Images.SetKeyName(38, "ShowHidden_32x32.png");
            this.ImageList32x32.Images.SetKeyName(39, "SpellCheck_32x32.png");
            this.ImageList32x32.Images.SetKeyName(40, "Strikeout_32x32.png");
            this.ImageList32x32.Images.SetKeyName(41, "StrikeoutDouble_32x32.png");
            this.ImageList32x32.Images.SetKeyName(42, "Subscript_32x32.png");
            this.ImageList32x32.Images.SetKeyName(43, "Superscript_32x32.png");
            this.ImageList32x32.Images.SetKeyName(44, "Symbol_32x32.png");
            this.ImageList32x32.Images.SetKeyName(45, "Underline_32x32.png");
            this.ImageList32x32.Images.SetKeyName(46, "UnderlineDouble_32x32.png");
            this.ImageList32x32.Images.SetKeyName(47, "UnderlineWord_32x32.png");
            this.ImageList32x32.Images.SetKeyName(48, "Undo_32x32.png");
            this.ImageList32x32.Images.SetKeyName(49, "ZoomIn_32x32.png");
            // 
            // RepositoryItemFontEdit1
            // 
            this.RepositoryItemFontEdit1.AutoHeight = false;
            this.RepositoryItemFontEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemFontEdit1.Name = "RepositoryItemFontEdit1";
            // 
            // RepositoryItemRichEditFontSizeEdit1
            // 
            this.RepositoryItemRichEditFontSizeEdit1.AutoHeight = false;
            this.RepositoryItemRichEditFontSizeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemRichEditFontSizeEdit1.Control = this.RichEditControl1;
            this.RepositoryItemRichEditFontSizeEdit1.Name = "RepositoryItemRichEditFontSizeEdit1";
            // 
            // RepositoryItemRichEditStyleEdit1
            // 
            this.RepositoryItemRichEditStyleEdit1.AutoHeight = false;
            this.RepositoryItemRichEditStyleEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RepositoryItemRichEditStyleEdit1.Control = this.RichEditControl1;
            this.RepositoryItemRichEditStyleEdit1.Name = "RepositoryItemRichEditStyleEdit1";
            // 
            // LayoutControl01
            // 
            this.LayoutControl01.Controls.Add(this.Button_Cancel);
            this.LayoutControl01.Controls.Add(this.Button_OK);
            this.LayoutControl01.Controls.Add(this.CheckEdit_DontDelete);
            this.LayoutControl01.Controls.Add(this.CheckEdit_DontPrint);
            this.LayoutControl01.Controls.Add(this.CheckEdit_DontEdit);
            this.LayoutControl01.Controls.Add(this.DateEdit_Expires);
            this.LayoutControl01.Controls.Add(this.ComboBoxEdit_Type);
            this.LayoutControl01.Controls.Add(this.ComboBoxEdit_Subjects);
            this.LayoutControl01.Controls.Add(this.RichEditControl1);
            this.LayoutControl01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl01.Location = new System.Drawing.Point(0, 77);
            this.LayoutControl01.Name = "LayoutControl01";
            this.LayoutControl01.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(274, 21, 250, 350);
            this.LayoutControl01.Root = this.LayoutControlGroup_EditingControls;
            this.LayoutControl01.Size = new System.Drawing.Size(690, 397);
            this.LayoutControl01.TabIndex = 4;
            this.LayoutControl01.Text = "LayoutControl01";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(319, 361);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 24);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 24);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 24);
            this.Button_Cancel.StyleController = this.LayoutControl01;
            this.Button_Cancel.TabIndex = 12;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_OK
            // 
            this.Button_OK.Location = new System.Drawing.Point(240, 361);
            this.Button_OK.MaximumSize = new System.Drawing.Size(75, 24);
            this.Button_OK.MinimumSize = new System.Drawing.Size(75, 24);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 24);
            this.Button_OK.StyleController = this.LayoutControl01;
            this.Button_OK.TabIndex = 11;
            this.Button_OK.Text = "&OK";
            // 
            // CheckEdit_DontDelete
            // 
            this.CheckEdit_DontDelete.Location = new System.Drawing.Point(331, 325);
            this.CheckEdit_DontDelete.MenuManager = this.barManager1;
            this.CheckEdit_DontDelete.Name = "CheckEdit_DontDelete";
            this.CheckEdit_DontDelete.Properties.Caption = "Don\'t Permit Deleting this Note";
            this.CheckEdit_DontDelete.Size = new System.Drawing.Size(335, 20);
            this.CheckEdit_DontDelete.StyleController = this.LayoutControl01;
            this.CheckEdit_DontDelete.TabIndex = 10;
            // 
            // CheckEdit_DontPrint
            // 
            this.CheckEdit_DontPrint.Location = new System.Drawing.Point(331, 301);
            this.CheckEdit_DontPrint.MenuManager = this.barManager1;
            this.CheckEdit_DontPrint.Name = "CheckEdit_DontPrint";
            this.CheckEdit_DontPrint.Properties.Caption = "Don\'t Permit Printing this Note";
            this.CheckEdit_DontPrint.Size = new System.Drawing.Size(335, 20);
            this.CheckEdit_DontPrint.StyleController = this.LayoutControl01;
            this.CheckEdit_DontPrint.TabIndex = 9;
            // 
            // CheckEdit_DontEdit
            // 
            this.CheckEdit_DontEdit.Location = new System.Drawing.Point(331, 277);
            this.CheckEdit_DontEdit.MenuManager = this.barManager1;
            this.CheckEdit_DontEdit.Name = "CheckEdit_DontEdit";
            this.CheckEdit_DontEdit.Properties.Caption = "Don\'t Permit editing this note";
            this.CheckEdit_DontEdit.Size = new System.Drawing.Size(335, 20);
            this.CheckEdit_DontEdit.StyleController = this.LayoutControl01;
            this.CheckEdit_DontEdit.TabIndex = 8;
            // 
            // DateEdit_Expires
            // 
            this.DateEdit_Expires.EditValue = null;
            this.DateEdit_Expires.Location = new System.Drawing.Point(63, 301);
            this.DateEdit_Expires.MenuManager = this.barManager1;
            this.DateEdit_Expires.Name = "DateEdit_Expires";
            this.DateEdit_Expires.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DateEdit_Expires.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.DateEdit_Expires.Size = new System.Drawing.Size(240, 20);
            this.DateEdit_Expires.StyleController = this.LayoutControl01;
            this.DateEdit_Expires.TabIndex = 7;
            // 
            // ComboBoxEdit_Type
            // 
            this.ComboBoxEdit_Type.Location = new System.Drawing.Point(63, 277);
            this.ComboBoxEdit_Type.MenuManager = this.barManager1;
            this.ComboBoxEdit_Type.Name = "ComboBoxEdit_Type";
            this.ComboBoxEdit_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_Type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ComboBoxEdit_Type.Size = new System.Drawing.Size(240, 20);
            this.ComboBoxEdit_Type.StyleController = this.LayoutControl01;
            this.ComboBoxEdit_Type.TabIndex = 6;
            // 
            // ComboBoxEdit_Subjects
            // 
            this.ComboBoxEdit_Subjects.Location = new System.Drawing.Point(51, 12);
            this.ComboBoxEdit_Subjects.MenuManager = this.barManager1;
            this.ComboBoxEdit_Subjects.Name = "ComboBoxEdit_Subjects";
            this.ComboBoxEdit_Subjects.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboBoxEdit_Subjects.Size = new System.Drawing.Size(627, 20);
            this.ComboBoxEdit_Subjects.StyleController = this.LayoutControl01;
            this.ComboBoxEdit_Subjects.TabIndex = 5;
            // 
            // LayoutControlGroup_EditingControls
            // 
            this.LayoutControlGroup_EditingControls.CustomizationFormText = "Editing Controls";
            this.LayoutControlGroup_EditingControls.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup_Checkboxes,
            this.LayoutControlGroup_Type_Expires,
            this.LayoutControlItem_Subjects,
            this.LayoutControlItem_RichEditControl1,
            this.EmptySpaceItem1,
            this.LayoutControlItem1,
            this.LayoutControlItem2,
            this.EmptySpaceItem2});
            this.LayoutControlGroup_EditingControls.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup_EditingControls.Name = "LayoutControlGroup_EditingControls";
            this.LayoutControlGroup_EditingControls.Size = new System.Drawing.Size(690, 397);
            this.LayoutControlGroup_EditingControls.Text = "Editing Controls";
            this.LayoutControlGroup_EditingControls.TextVisible = false;
            // 
            // LayoutControlGroup_Checkboxes
            // 
            this.LayoutControlGroup_Checkboxes.CustomizationFormText = "Permission Checkboxes";
            this.LayoutControlGroup_Checkboxes.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem_CheckEdit_DontDelete,
            this.LayoutControlItem_CheckEdit_DontPrint,
            this.LayoutControlItem_CheckEdit_DontEdit});
            this.LayoutControlGroup_Checkboxes.Location = new System.Drawing.Point(307, 253);
            this.LayoutControlGroup_Checkboxes.Name = "LayoutControlGroup_Checkboxes";
            this.LayoutControlGroup_Checkboxes.Size = new System.Drawing.Size(363, 96);
            this.LayoutControlGroup_Checkboxes.Text = "Permissions";
            this.LayoutControlGroup_Checkboxes.TextVisible = false;
            // 
            // LayoutControlItem_CheckEdit_DontDelete
            // 
            this.LayoutControlItem_CheckEdit_DontDelete.Control = this.CheckEdit_DontDelete;
            this.LayoutControlItem_CheckEdit_DontDelete.CustomizationFormText = "Dont Delete Checkbox";
            this.LayoutControlItem_CheckEdit_DontDelete.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem_CheckEdit_DontDelete.Name = "LayoutControlItem_CheckEdit_DontDelete";
            this.LayoutControlItem_CheckEdit_DontDelete.Size = new System.Drawing.Size(339, 24);
            this.LayoutControlItem_CheckEdit_DontDelete.Text = "Dont Delete";
            this.LayoutControlItem_CheckEdit_DontDelete.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem_CheckEdit_DontDelete.TextToControlDistance = 0;
            this.LayoutControlItem_CheckEdit_DontDelete.TextVisible = false;
            // 
            // LayoutControlItem_CheckEdit_DontPrint
            // 
            this.LayoutControlItem_CheckEdit_DontPrint.Control = this.CheckEdit_DontPrint;
            this.LayoutControlItem_CheckEdit_DontPrint.CustomizationFormText = "Dont Print Checkbox";
            this.LayoutControlItem_CheckEdit_DontPrint.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem_CheckEdit_DontPrint.Name = "LayoutControlItem_CheckEdit_DontPrint";
            this.LayoutControlItem_CheckEdit_DontPrint.Size = new System.Drawing.Size(339, 24);
            this.LayoutControlItem_CheckEdit_DontPrint.Text = "Dont Print";
            this.LayoutControlItem_CheckEdit_DontPrint.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem_CheckEdit_DontPrint.TextToControlDistance = 0;
            this.LayoutControlItem_CheckEdit_DontPrint.TextVisible = false;
            // 
            // LayoutControlItem_CheckEdit_DontEdit
            // 
            this.LayoutControlItem_CheckEdit_DontEdit.Control = this.CheckEdit_DontEdit;
            this.LayoutControlItem_CheckEdit_DontEdit.CustomizationFormText = "Dont Edit Checkbox";
            this.LayoutControlItem_CheckEdit_DontEdit.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem_CheckEdit_DontEdit.Name = "LayoutControlItem_CheckEdit_DontEdit";
            this.LayoutControlItem_CheckEdit_DontEdit.Size = new System.Drawing.Size(339, 24);
            this.LayoutControlItem_CheckEdit_DontEdit.Text = "Dont Edit";
            this.LayoutControlItem_CheckEdit_DontEdit.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem_CheckEdit_DontEdit.TextToControlDistance = 0;
            this.LayoutControlItem_CheckEdit_DontEdit.TextVisible = false;
            // 
            // LayoutControlGroup_Type_Expires
            // 
            this.LayoutControlGroup_Type_Expires.CustomizationFormText = "Note type and expiration";
            this.LayoutControlGroup_Type_Expires.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem_Expires,
            this.LayoutControlItem_Combobox_Type});
            this.LayoutControlGroup_Type_Expires.Location = new System.Drawing.Point(0, 253);
            this.LayoutControlGroup_Type_Expires.Name = "LayoutControlGroup_Type_Expires";
            this.LayoutControlGroup_Type_Expires.Size = new System.Drawing.Size(307, 96);
            this.LayoutControlGroup_Type_Expires.Text = "Note Type";
            this.LayoutControlGroup_Type_Expires.TextVisible = false;
            // 
            // LayoutControlItem_Expires
            // 
            this.LayoutControlItem_Expires.Control = this.DateEdit_Expires;
            this.LayoutControlItem_Expires.CustomizationFormText = "Expiration Date";
            this.LayoutControlItem_Expires.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem_Expires.Name = "LayoutControlItem_Expires";
            this.LayoutControlItem_Expires.Size = new System.Drawing.Size(283, 48);
            this.LayoutControlItem_Expires.Text = "Expires";
            this.LayoutControlItem_Expires.TextSize = new System.Drawing.Size(36, 13);
            // 
            // LayoutControlItem_Combobox_Type
            // 
            this.LayoutControlItem_Combobox_Type.Control = this.ComboBoxEdit_Type;
            this.LayoutControlItem_Combobox_Type.CustomizationFormText = "Note Type";
            this.LayoutControlItem_Combobox_Type.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem_Combobox_Type.Name = "LayoutControlItem_Combobox_Type";
            this.LayoutControlItem_Combobox_Type.Size = new System.Drawing.Size(283, 24);
            this.LayoutControlItem_Combobox_Type.Text = "Type";
            this.LayoutControlItem_Combobox_Type.TextSize = new System.Drawing.Size(36, 13);
            // 
            // LayoutControlItem_Subjects
            // 
            this.LayoutControlItem_Subjects.Control = this.ComboBoxEdit_Subjects;
            this.LayoutControlItem_Subjects.CustomizationFormText = "Note subject";
            this.LayoutControlItem_Subjects.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem_Subjects.Name = "LayoutControlItem_Subjects";
            this.LayoutControlItem_Subjects.Size = new System.Drawing.Size(670, 24);
            this.LayoutControlItem_Subjects.Text = "Subject";
            this.LayoutControlItem_Subjects.TextSize = new System.Drawing.Size(36, 13);
            // 
            // LayoutControlItem_RichEditControl1
            // 
            this.LayoutControlItem_RichEditControl1.Control = this.RichEditControl1;
            this.LayoutControlItem_RichEditControl1.CustomizationFormText = "Text Editing window";
            this.LayoutControlItem_RichEditControl1.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem_RichEditControl1.Name = "LayoutControlItem_RichEditControl1";
            this.LayoutControlItem_RichEditControl1.Size = new System.Drawing.Size(670, 229);
            this.LayoutControlItem_RichEditControl1.Text = "Note Text";
            this.LayoutControlItem_RichEditControl1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem_RichEditControl1.TextToControlDistance = 0;
            this.LayoutControlItem_RichEditControl1.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 349);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(228, 28);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.Button_OK;
            this.LayoutControlItem1.CustomizationFormText = "OK Button";
            this.LayoutControlItem1.Location = new System.Drawing.Point(228, 349);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(79, 28);
            this.LayoutControlItem1.Text = "OK Button";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.Button_Cancel;
            this.LayoutControlItem2.CustomizationFormText = "CANCEL Button";
            this.LayoutControlItem2.Location = new System.Drawing.Point(307, 349);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(79, 28);
            this.LayoutControlItem2.Text = "CANCEL Button";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(386, 349);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(284, 28);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // RichEditBarController1
            // 
            this.RichEditBarController1.BarItems.Add(this.FileNewItem1);
            this.RichEditBarController1.BarItems.Add(this.FileOpenItem1);
            this.RichEditBarController1.BarItems.Add(this.FileSaveItem1);
            this.RichEditBarController1.BarItems.Add(this.FileSaveAsItem1);
            this.RichEditBarController1.BarItems.Add(this.QuickPrintItem1);
            this.RichEditBarController1.BarItems.Add(this.PrintItem1);
            this.RichEditBarController1.BarItems.Add(this.PrintPreviewItem1);
            this.RichEditBarController1.BarItems.Add(this.UndoItem1);
            this.RichEditBarController1.BarItems.Add(this.RedoItem1);
            this.RichEditBarController1.BarItems.Add(this.CutItem1);
            this.RichEditBarController1.BarItems.Add(this.CopyItem1);
            this.RichEditBarController1.BarItems.Add(this.PasteItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeFontNameItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeFontSizeItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeFontColorItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeFontBackColorItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontBoldItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontItalicItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontUnderlineItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontDoubleUnderlineItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontStrikeoutItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontDoubleStrikeoutItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontSuperscriptItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleFontSubscriptItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeTextCaseItem1);
            this.RichEditBarController1.BarItems.Add(this.MakeTextUpperCaseItem1);
            this.RichEditBarController1.BarItems.Add(this.MakeTextLowerCaseItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTextCaseItem1);
            this.RichEditBarController1.BarItems.Add(this.FontSizeIncreaseItem1);
            this.RichEditBarController1.BarItems.Add(this.FontSizeDecreaseItem1);
            this.RichEditBarController1.BarItems.Add(this.ClearFormattingItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowFontFormItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleParagraphAlignmentLeftItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleParagraphAlignmentCenterItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleParagraphAlignmentRightItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleParagraphAlignmentJustifyItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeParagraphLineSpacingItem1);
            this.RichEditBarController1.BarItems.Add(this.SetSingleParagraphSpacingItem1);
            this.RichEditBarController1.BarItems.Add(this.SetSesquialteralParagraphSpacingItem1);
            this.RichEditBarController1.BarItems.Add(this.SetDoubleParagraphSpacingItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowLineSpacingFormItem1);
            this.RichEditBarController1.BarItems.Add(this.AddSpacingBeforeParagraphItem1);
            this.RichEditBarController1.BarItems.Add(this.RemoveSpacingBeforeParagraphItem1);
            this.RichEditBarController1.BarItems.Add(this.AddSpacingAfterParagraphItem1);
            this.RichEditBarController1.BarItems.Add(this.RemoveSpacingAfterParagraphItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleBulletedListItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleNumberingListItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleMultiLevelListItem1);
            this.RichEditBarController1.BarItems.Add(this.DecreaseIndentItem1);
            this.RichEditBarController1.BarItems.Add(this.IncreaseIndentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleShowWhitespaceItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowParagraphFormItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeStyleItem1);
            this.RichEditBarController1.BarItems.Add(this.FindItem1);
            this.RichEditBarController1.BarItems.Add(this.ReplaceItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertPageBreakItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertTableItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertPictureItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertBookmarkItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertHyperlinkItem1);
            this.RichEditBarController1.BarItems.Add(this.EditPageHeaderItem1);
            this.RichEditBarController1.BarItems.Add(this.EditPageFooterItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertPageNumberItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertPageCountItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertSymbolItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeSectionPageMarginsItem1);
            this.RichEditBarController1.BarItems.Add(this.SetNormalSectionPageMarginsItem1);
            this.RichEditBarController1.BarItems.Add(this.SetNarrowSectionPageMarginsItem1);
            this.RichEditBarController1.BarItems.Add(this.SetModerateSectionPageMarginsItem1);
            this.RichEditBarController1.BarItems.Add(this.SetWideSectionPageMarginsItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeSectionPageOrientationItem1);
            this.RichEditBarController1.BarItems.Add(this.SetPortraitPageOrientationItem1);
            this.RichEditBarController1.BarItems.Add(this.SetLandscapePageOrientationItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeSectionColumnsItem1);
            this.RichEditBarController1.BarItems.Add(this.SetSectionOneColumnItem1);
            this.RichEditBarController1.BarItems.Add(this.SetSectionTwoColumnsItem1);
            this.RichEditBarController1.BarItems.Add(this.SetSectionThreeColumnsItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertMergeFieldItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowAllFieldCodesItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowAllFieldResultsItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleViewMergedDataItem1);
            this.RichEditBarController1.BarItems.Add(this.SwitchToSimpleViewItem1);
            this.RichEditBarController1.BarItems.Add(this.SwitchToDraftViewItem1);
            this.RichEditBarController1.BarItems.Add(this.SwitchToPrintLayoutViewItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleShowHorizontalRulerItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleShowVerticalRulerItem1);
            this.RichEditBarController1.BarItems.Add(this.ZoomOutItem1);
            this.RichEditBarController1.BarItems.Add(this.ZoomInItem1);
            this.RichEditBarController1.BarItems.Add(this.ProtectDocumentItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeRangeEditingPermissionsItem1);
            this.RichEditBarController1.BarItems.Add(this.UnprotectDocumentItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeTableCellsShadingItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeTableBordersItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsBottomBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsTopBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsLeftBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsRightBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ResetTableCellsAllBordersItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsAllBordersItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsOutsideBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsInsideBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsInsideHorizontalBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsInsideVerticalBorderItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleShowTableGridLinesItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeTableBorderLineStyleItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeTableBorderLineWeightItem1);
            this.RichEditBarController1.BarItems.Add(this.ChangeTableBorderColorItem1);
            this.RichEditBarController1.BarItems.Add(this.SelectTableElementsItem1);
            this.RichEditBarController1.BarItems.Add(this.SelectTableCellItem1);
            this.RichEditBarController1.BarItems.Add(this.SelectTableColumnItem1);
            this.RichEditBarController1.BarItems.Add(this.SelectTableRowItem1);
            this.RichEditBarController1.BarItems.Add(this.SelectTableItem1);
            this.RichEditBarController1.BarItems.Add(this.DeleteTableElementsItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowDeleteTableCellsFormItem1);
            this.RichEditBarController1.BarItems.Add(this.DeleteTableColumnsItem1);
            this.RichEditBarController1.BarItems.Add(this.DeleteTableRowsItem1);
            this.RichEditBarController1.BarItems.Add(this.DeleteTableItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertTableRowAboveItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertTableRowBelowItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertTableColumnToLeftItem1);
            this.RichEditBarController1.BarItems.Add(this.InsertTableColumnToRightItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowInsertTableCellsFormItem1);
            this.RichEditBarController1.BarItems.Add(this.MergeTableCellsItem1);
            this.RichEditBarController1.BarItems.Add(this.ShowSplitTableCellsForm1);
            this.RichEditBarController1.BarItems.Add(this.SplitTableItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsTopLeftAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsTopCenterAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsTopRightAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsMiddleLeftAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsMiddleCenterAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsMiddleRightAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsBottomLeftAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsBottomCenterAlignmentItem1);
            this.RichEditBarController1.BarItems.Add(this.ToggleTableCellsBottomRightAlignmentItem1);
            this.RichEditBarController1.RichEditControl = this.RichEditControl1;
            // 
            // EditNoteForm
            // 
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(690, 502);
            this.CloseBox = false;
            this.Controls.Add(this.LayoutControl01);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "EditNoteForm";
            this.Text = "Note";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemFontEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditFontSizeEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditStyleEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemBorderLineStyle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemBorderLineWeight1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemFontEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditFontSizeEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemRichEditStyleEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl01)).EndInit();
            this.LayoutControl01.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DontDelete.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DontPrint.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEdit_DontEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Expires.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateEdit_Expires.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboBoxEdit_Subjects.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup_EditingControls)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup_Checkboxes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_CheckEdit_DontDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_CheckEdit_DontPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_CheckEdit_DontEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup_Type_Expires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_Expires)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_Combobox_Type)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_Subjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem_RichEditControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RichEditBarController1)).EndInit();
            this.ResumeLayout(false);

        }

        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar Bar_MainMenu;
        internal DevExpress.XtraBars.Bar Bar_Status;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraEditors.Repository.RepositoryItemFontEdit RepositoryItemFontEdit1;
        internal DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit RepositoryItemRichEditFontSizeEdit1;
        internal DevExpress.XtraRichEdit.RichEditControl RichEditControl1;
        internal DevExpress.XtraSpellChecker.SpellChecker SpellChecker1;
        internal DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit RepositoryItemRichEditStyleEdit1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl01;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup_EditingControls;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem_RichEditControl1;
        internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_Subjects;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem_Subjects;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_Line;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_Column;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_Page;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_Zoom;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_CAPS;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_NUM;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_SCR;
        internal DevExpress.XtraEditors.DateEdit DateEdit_Expires;
        internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_Type;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup_Type_Expires;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem_Expires;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem_Combobox_Type;
        internal DevExpress.XtraEditors.CheckEdit CheckEdit_DontDelete;
        internal DevExpress.XtraEditors.CheckEdit CheckEdit_DontPrint;
        internal DevExpress.XtraEditors.CheckEdit CheckEdit_DontEdit;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup_Checkboxes;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem_CheckEdit_DontDelete;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem_CheckEdit_DontPrint;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem_CheckEdit_DontEdit;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_File;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_Edit;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_View;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_View_toolbars;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_view_zoom;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_Format;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_Tools;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Tools_Spell;
        internal DevExpress.XtraBars.BarCheckItem View_Zoom_025;
        internal DevExpress.XtraBars.BarCheckItem View_Zoom_050;
        internal DevExpress.XtraBars.BarCheckItem View_Zoom_075;
        internal DevExpress.XtraBars.BarCheckItem View_Zoom_100;
        internal DevExpress.XtraBars.BarCheckItem View_Zoom_150;
        internal DevExpress.XtraBars.BarCheckItem View_Zoom_200;
        internal DevExpress.XtraBars.BarCheckItem View_Zoom_300;
        internal System.Windows.Forms.ImageList ImageList16x16;
        internal System.Windows.Forms.ImageList ImageList32x32;
        internal DevExpress.XtraBars.BarSubItem BarSubItem_Insert;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Insert_File;
        internal DevExpress.XtraBars.BarToolbarsListItem BarToolbarsListItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem1;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        internal DevExpress.XtraSpellChecker.SharedDictionaryStorage sharedDictionaryStorage1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Tools_SpellingOptions;
        internal DevExpress.XtraBars.BarSubItem BarSubItem1;
        internal DevExpress.XtraBars.BarToolbarsListItem BarToolbarsListItem2;
        internal DevExpress.XtraBars.BarSubItem BarSubItem2;
        internal DevExpress.XtraBars.BarSubItem BarSubItem3;
        internal DevExpress.XtraBars.BarSubItem BarSubItem4;
        internal DevExpress.XtraBars.Bar Bar1;
        internal DevExpress.XtraRichEdit.UI.CommonBar CommonBar1;
        internal DevExpress.XtraRichEdit.UI.FileNewItem FileNewItem1;
        internal DevExpress.XtraRichEdit.UI.FileOpenItem FileOpenItem1;
        internal DevExpress.XtraRichEdit.UI.FileSaveItem FileSaveItem1;
        internal DevExpress.XtraRichEdit.UI.FileSaveAsItem FileSaveAsItem1;
        internal DevExpress.XtraRichEdit.UI.QuickPrintItem QuickPrintItem1;
        internal DevExpress.XtraRichEdit.UI.PrintItem PrintItem1;
        internal DevExpress.XtraRichEdit.UI.PrintPreviewItem PrintPreviewItem1;
        internal DevExpress.XtraRichEdit.UI.UndoItem UndoItem1;
        internal DevExpress.XtraRichEdit.UI.RedoItem RedoItem1;
        internal DevExpress.XtraRichEdit.UI.ClipboardBar ClipboardBar1;
        internal DevExpress.XtraRichEdit.UI.CutItem CutItem1;
        internal DevExpress.XtraRichEdit.UI.CopyItem CopyItem1;
        internal DevExpress.XtraRichEdit.UI.PasteItem PasteItem1;
        internal DevExpress.XtraRichEdit.UI.FontBar FontBar1;
        internal DevExpress.XtraRichEdit.UI.ChangeFontNameItem ChangeFontNameItem1;
        internal DevExpress.XtraEditors.Repository.RepositoryItemFontEdit RepositoryItemFontEdit2;
        internal DevExpress.XtraRichEdit.UI.ChangeFontSizeItem ChangeFontSizeItem1;
        internal DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit RepositoryItemRichEditFontSizeEdit2;
        internal DevExpress.XtraRichEdit.UI.ChangeFontColorItem ChangeFontColorItem1;
        internal DevExpress.XtraRichEdit.UI.ChangeFontBackColorItem ChangeFontBackColorItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontBoldItem ToggleFontBoldItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontItalicItem ToggleFontItalicItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem ToggleFontUnderlineItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem ToggleFontDoubleUnderlineItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem ToggleFontStrikeoutItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem ToggleFontDoubleStrikeoutItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem ToggleFontSuperscriptItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem ToggleFontSubscriptItem1;
        internal DevExpress.XtraRichEdit.UI.ChangeTextCaseItem ChangeTextCaseItem1;
        internal DevExpress.XtraRichEdit.UI.MakeTextUpperCaseItem MakeTextUpperCaseItem1;
        internal DevExpress.XtraRichEdit.UI.MakeTextLowerCaseItem MakeTextLowerCaseItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTextCaseItem ToggleTextCaseItem1;
        internal DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem FontSizeIncreaseItem1;
        internal DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem FontSizeDecreaseItem1;
        internal DevExpress.XtraRichEdit.UI.ClearFormattingItem ClearFormattingItem1;
        internal DevExpress.XtraRichEdit.UI.ShowFontFormItem ShowFontFormItem1;
        internal DevExpress.XtraRichEdit.UI.ParagraphBar ParagraphBar1;
        internal DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentLeftItem ToggleParagraphAlignmentLeftItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentCenterItem ToggleParagraphAlignmentCenterItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentRightItem ToggleParagraphAlignmentRightItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyItem ToggleParagraphAlignmentJustifyItem1;
        internal DevExpress.XtraRichEdit.UI.ChangeParagraphLineSpacingItem ChangeParagraphLineSpacingItem1;
        internal DevExpress.XtraRichEdit.UI.SetSingleParagraphSpacingItem SetSingleParagraphSpacingItem1;
        internal DevExpress.XtraRichEdit.UI.SetSesquialteralParagraphSpacingItem SetSesquialteralParagraphSpacingItem1;
        internal DevExpress.XtraRichEdit.UI.SetDoubleParagraphSpacingItem SetDoubleParagraphSpacingItem1;
        internal DevExpress.XtraRichEdit.UI.ShowLineSpacingFormItem ShowLineSpacingFormItem1;
        internal DevExpress.XtraRichEdit.UI.AddSpacingBeforeParagraphItem AddSpacingBeforeParagraphItem1;
        internal DevExpress.XtraRichEdit.UI.RemoveSpacingBeforeParagraphItem RemoveSpacingBeforeParagraphItem1;
        internal DevExpress.XtraRichEdit.UI.AddSpacingAfterParagraphItem AddSpacingAfterParagraphItem1;
        internal DevExpress.XtraRichEdit.UI.RemoveSpacingAfterParagraphItem RemoveSpacingAfterParagraphItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleBulletedListItem ToggleBulletedListItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleNumberingListItem ToggleNumberingListItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleMultiLevelListItem ToggleMultiLevelListItem1;
        internal DevExpress.XtraRichEdit.UI.DecreaseIndentItem DecreaseIndentItem1;
        internal DevExpress.XtraRichEdit.UI.IncreaseIndentItem IncreaseIndentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleShowWhitespaceItem ToggleShowWhitespaceItem1;
        internal DevExpress.XtraRichEdit.UI.ShowParagraphFormItem ShowParagraphFormItem1;
        internal DevExpress.XtraRichEdit.UI.StylesBar StylesBar1;
        internal DevExpress.XtraRichEdit.UI.ChangeStyleItem ChangeStyleItem1;
        internal DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit RepositoryItemRichEditStyleEdit2;
        internal DevExpress.XtraRichEdit.UI.EditingBar EditingBar1;
        internal DevExpress.XtraRichEdit.UI.FindItem FindItem1;
        internal DevExpress.XtraRichEdit.UI.ReplaceItem ReplaceItem1;
        internal DevExpress.XtraRichEdit.UI.PagesBar PagesBar1;
        internal DevExpress.XtraRichEdit.UI.InsertPageBreakItem InsertPageBreakItem1;
        internal DevExpress.XtraRichEdit.UI.TablesBar TablesBar1;
        internal DevExpress.XtraRichEdit.UI.InsertTableItem InsertTableItem1;
        internal DevExpress.XtraRichEdit.UI.IllustrationsBar IllustrationsBar1;
        internal DevExpress.XtraRichEdit.UI.InsertPictureItem InsertPictureItem1;
        internal DevExpress.XtraRichEdit.UI.LinksBar LinksBar1;
        internal DevExpress.XtraRichEdit.UI.InsertBookmarkItem InsertBookmarkItem1;
        internal DevExpress.XtraRichEdit.UI.InsertHyperlinkItem InsertHyperlinkItem1;
        internal DevExpress.XtraRichEdit.UI.HeaderFooterBar HeaderFooterBar1;
        internal DevExpress.XtraRichEdit.UI.EditPageHeaderItem EditPageHeaderItem1;
        internal DevExpress.XtraRichEdit.UI.EditPageFooterItem EditPageFooterItem1;
        internal DevExpress.XtraRichEdit.UI.InsertPageNumberItem InsertPageNumberItem1;
        internal DevExpress.XtraRichEdit.UI.InsertPageCountItem InsertPageCountItem1;
        internal DevExpress.XtraRichEdit.UI.SymbolsBar SymbolsBar1;
        internal DevExpress.XtraRichEdit.UI.InsertSymbolItem InsertSymbolItem1;
        internal DevExpress.XtraRichEdit.UI.PageSetupBar PageSetupBar1;
        internal DevExpress.XtraRichEdit.UI.ChangeSectionPageMarginsItem ChangeSectionPageMarginsItem1;
        internal DevExpress.XtraRichEdit.UI.SetNormalSectionPageMarginsItem SetNormalSectionPageMarginsItem1;
        internal DevExpress.XtraRichEdit.UI.SetNarrowSectionPageMarginsItem SetNarrowSectionPageMarginsItem1;
        internal DevExpress.XtraRichEdit.UI.SetModerateSectionPageMarginsItem SetModerateSectionPageMarginsItem1;
        internal DevExpress.XtraRichEdit.UI.SetWideSectionPageMarginsItem SetWideSectionPageMarginsItem1;
        internal DevExpress.XtraRichEdit.UI.ChangeSectionPageOrientationItem ChangeSectionPageOrientationItem1;
        internal DevExpress.XtraRichEdit.UI.SetPortraitPageOrientationItem SetPortraitPageOrientationItem1;
        internal DevExpress.XtraRichEdit.UI.SetLandscapePageOrientationItem SetLandscapePageOrientationItem1;
        internal DevExpress.XtraRichEdit.UI.ChangeSectionColumnsItem ChangeSectionColumnsItem1;
        internal DevExpress.XtraRichEdit.UI.SetSectionOneColumnItem SetSectionOneColumnItem1;
        internal DevExpress.XtraRichEdit.UI.SetSectionTwoColumnsItem SetSectionTwoColumnsItem1;
        internal DevExpress.XtraRichEdit.UI.SetSectionThreeColumnsItem SetSectionThreeColumnsItem1;
        internal DevExpress.XtraRichEdit.UI.MailMergeBar MailMergeBar1;
        internal DevExpress.XtraRichEdit.UI.InsertMergeFieldItem InsertMergeFieldItem1;
        internal DevExpress.XtraRichEdit.UI.ShowAllFieldCodesItem ShowAllFieldCodesItem1;
        internal DevExpress.XtraRichEdit.UI.ShowAllFieldResultsItem ShowAllFieldResultsItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleViewMergedDataItem ToggleViewMergedDataItem1;
        internal DevExpress.XtraRichEdit.UI.DocumentViewsBar DocumentViewsBar1;
        internal DevExpress.XtraRichEdit.UI.SwitchToSimpleViewItem SwitchToSimpleViewItem1;
        internal DevExpress.XtraRichEdit.UI.SwitchToDraftViewItem SwitchToDraftViewItem1;
        internal DevExpress.XtraRichEdit.UI.SwitchToPrintLayoutViewItem SwitchToPrintLayoutViewItem1;
        internal DevExpress.XtraRichEdit.UI.ShowBar ShowBar1;
        internal DevExpress.XtraRichEdit.UI.ToggleShowHorizontalRulerItem ToggleShowHorizontalRulerItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleShowVerticalRulerItem ToggleShowVerticalRulerItem1;
        internal DevExpress.XtraRichEdit.UI.ZoomBar ZoomBar1;
        internal DevExpress.XtraRichEdit.UI.ZoomOutItem ZoomOutItem1;
        internal DevExpress.XtraRichEdit.UI.ZoomInItem ZoomInItem1;
        internal DevExpress.XtraRichEdit.UI.DocumentProtectionBar DocumentProtectionBar1;
        internal DevExpress.XtraRichEdit.UI.ProtectDocumentItem ProtectDocumentItem1;
        internal DevExpress.XtraRichEdit.UI.ChangeRangeEditingPermissionsItem ChangeRangeEditingPermissionsItem1;
        internal DevExpress.XtraRichEdit.UI.UnprotectDocumentItem UnprotectDocumentItem1;
        internal DevExpress.XtraRichEdit.UI.TableStylesBar TableStylesBar1;
        internal DevExpress.XtraRichEdit.UI.ChangeTableCellsShadingItem ChangeTableCellsShadingItem1;
        internal DevExpress.XtraRichEdit.UI.ChangeTableBordersItem ChangeTableBordersItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomBorderItem ToggleTableCellsBottomBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsTopBorderItem ToggleTableCellsTopBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsLeftBorderItem ToggleTableCellsLeftBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsRightBorderItem ToggleTableCellsRightBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ResetTableCellsAllBordersItem ResetTableCellsAllBordersItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsAllBordersItem ToggleTableCellsAllBordersItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsOutsideBorderItem ToggleTableCellsOutsideBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsInsideBorderItem ToggleTableCellsInsideBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsInsideHorizontalBorderItem ToggleTableCellsInsideHorizontalBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsInsideVerticalBorderItem ToggleTableCellsInsideVerticalBorderItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleShowTableGridLinesItem ToggleShowTableGridLinesItem1;
        internal DevExpress.XtraRichEdit.UI.TableDrawBordersBar TableDrawBordersBar1;
        internal DevExpress.XtraRichEdit.UI.ChangeTableBorderLineStyleItem ChangeTableBorderLineStyleItem1;
        internal DevExpress.XtraRichEdit.Forms.Design.RepositoryItemBorderLineStyle RepositoryItemBorderLineStyle1;
        internal DevExpress.XtraRichEdit.UI.ChangeTableBorderLineWeightItem ChangeTableBorderLineWeightItem1;
        internal DevExpress.XtraRichEdit.Forms.Design.RepositoryItemBorderLineWeight RepositoryItemBorderLineWeight1;
        internal DevExpress.XtraRichEdit.UI.ChangeTableBorderColorItem ChangeTableBorderColorItem1;
        internal DevExpress.XtraRichEdit.UI.TableTableBar TableTableBar1;
        internal DevExpress.XtraRichEdit.UI.SelectTableElementsItem SelectTableElementsItem1;
        internal DevExpress.XtraRichEdit.UI.SelectTableCellItem SelectTableCellItem1;
        internal DevExpress.XtraRichEdit.UI.SelectTableColumnItem SelectTableColumnItem1;
        internal DevExpress.XtraRichEdit.UI.SelectTableRowItem SelectTableRowItem1;
        internal DevExpress.XtraRichEdit.UI.SelectTableItem SelectTableItem1;
        internal DevExpress.XtraRichEdit.UI.TableRowsAndColumnsBar TableRowsAndColumnsBar1;
        internal DevExpress.XtraRichEdit.UI.DeleteTableElementsItem DeleteTableElementsItem1;
        internal DevExpress.XtraRichEdit.UI.ShowDeleteTableCellsFormItem ShowDeleteTableCellsFormItem1;
        internal DevExpress.XtraRichEdit.UI.DeleteTableColumnsItem DeleteTableColumnsItem1;
        internal DevExpress.XtraRichEdit.UI.DeleteTableRowsItem DeleteTableRowsItem1;
        internal DevExpress.XtraRichEdit.UI.DeleteTableItem DeleteTableItem1;
        internal DevExpress.XtraRichEdit.UI.InsertTableRowAboveItem InsertTableRowAboveItem1;
        internal DevExpress.XtraRichEdit.UI.InsertTableRowBelowItem InsertTableRowBelowItem1;
        internal DevExpress.XtraRichEdit.UI.InsertTableColumnToLeftItem InsertTableColumnToLeftItem1;
        internal DevExpress.XtraRichEdit.UI.InsertTableColumnToRightItem InsertTableColumnToRightItem1;
        internal DevExpress.XtraRichEdit.UI.ShowInsertTableCellsFormItem ShowInsertTableCellsFormItem1;
        internal DevExpress.XtraRichEdit.UI.TableMergeBar TableMergeBar1;
        internal DevExpress.XtraRichEdit.UI.MergeTableCellsItem MergeTableCellsItem1;
        internal DevExpress.XtraRichEdit.UI.ShowSplitTableCellsForm ShowSplitTableCellsForm1;
        internal DevExpress.XtraRichEdit.UI.SplitTableItem SplitTableItem1;
        internal DevExpress.XtraRichEdit.UI.TableAlignmentBar TableAlignmentBar1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsTopLeftAlignmentItem ToggleTableCellsTopLeftAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsTopCenterAlignmentItem ToggleTableCellsTopCenterAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsTopRightAlignmentItem ToggleTableCellsTopRightAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsMiddleLeftAlignmentItem ToggleTableCellsMiddleLeftAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsMiddleCenterAlignmentItem ToggleTableCellsMiddleCenterAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsMiddleRightAlignmentItem ToggleTableCellsMiddleRightAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomLeftAlignmentItem ToggleTableCellsBottomLeftAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomCenterAlignmentItem ToggleTableCellsBottomCenterAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.ToggleTableCellsBottomRightAlignmentItem ToggleTableCellsBottomRightAlignmentItem1;
        internal DevExpress.XtraRichEdit.UI.RichEditBarController RichEditBarController1;
    }
}