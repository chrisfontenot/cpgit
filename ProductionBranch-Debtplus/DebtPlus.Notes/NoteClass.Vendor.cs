﻿using DebtPlus.Svc.Notes;
using System;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Notes
{
    public sealed partial class NoteClass
    {
        /// <summary>
        /// Creditor note processing class
        /// </summary>
        public class VendorNote : BaseClass
        {
            /// <summary>
            /// Create a new instance of our class
            /// </summary>
            public VendorNote()
                : base()
            {
            }

            // Dataset and table name
            private const string TableName = "vendor_notes";

            /// <summary>
            /// Read the creditor note from the system into local storage.
            /// </summary>
            /// <param name="noteID">The ID of the creditor note</param>
            /// <returns>The note or null if the note is not found.</returns>
            /// <remarks></remarks>
            protected combined_note ReadNote(Int32 noteID)
            {
                return (from n in bc.vendor_notes where n.Id == noteID select new combined_note(n)).FirstOrDefault();
            }

            /// <summary>
            /// Edit the vendor note
            /// </summary>
            /// <param name="drv">Pointer to the note to be used.</param>
            /// <param name="EnableEdit">Do we permit the note to be modified? TRUE for edit. FALSE for show.</param>
            /// <param name="EnableFlags">Do we want the flags (dont_edit/delete/print) to be enabled?</param>
            /// <returns></returns>
            protected System.Windows.Forms.DialogResult EditNote(combined_note n, bool EnableEdit, bool EnableFlags)
            {
                // Perform the edit operation on the row
                using (var EditForm = new EditNoteForm(bc, EnableEdit, EnableFlags))
                {
                    EditForm.ValidTypes.Clear();
                    EditForm.ValidTypes.Add(LINQ.InMemory.Notes.NoteTypes.Permanent);
                    //EditForm.ValidTypes.Add(LINQ.InMemory.Notes.NoteTypes.Temporary);
                    EditForm.ValidTypes.Add(LINQ.InMemory.Notes.NoteTypes.Alert);
                    //EditForm.ValidTypes.Add(LINQ.InMemory.Notes.NoteTypes.Research);

                    // Fill out the editing form with the note information
                    EditForm.Text = FormText();
                    EditForm.note = n;
                    n.IsDirty = false;

                    // if the document was not dirty then the request was canceled.
                    System.Windows.Forms.DialogResult Answer = EditForm.ShowDialog();
                    if (Answer == System.Windows.Forms.DialogResult.OK && !n.IsDirty)
                    {
                        return System.Windows.Forms.DialogResult.Cancel;
                    }

                    return Answer;
                }
            }

            /// <summary>
            /// Save the note to the database from the edit operation.
            /// </summary>
            /// <returns>TRUE if successful and the note was updated. FALSE otherwise.</returns>
            private bool SaveNote(combined_note n)
            {
                try
                {
                    vendor_note q = null;
                    if (n.IsNew)
                    {
                        q        = DebtPlus.LINQ.Factory.Manufacture_vendor_note(n.vendor.Value, n.type);
                        q.vendor = n.vendor.Value;
                        bc.vendor_notes.InsertOnSubmit(q);
                    }
                    else
                    {
                        q = bc.vendor_notes.Where(note => note.Id == n.Id).FirstOrDefault();
                        if (q == null)
                        {
                            return false;
                        }
                    }

                    // Supply the values for the note.
                    q.type        = n.type;
                    q.subject     = n.subject;
                    q.dont_print  = n.dont_print;
                    q.dont_edit   = n.dont_edit;
                    q.dont_delete = n.dont_delete;
                    q.note        = n.note;
                    q.expires     = n.expires;

                    // Do the changes and return success
                    bc.SubmitChanges();
                    return true;
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                    return false;
                }
            }

            /// <summary>
            /// Subject for the edit form needs to say Vendor Note.
            /// </summary>
            protected virtual string FormText()
            {
                return "Vendor Note";
            }

            /// <summary>
            /// Display but do not allow editing of the Vendor note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Show(Int32 noteID)
            {
                // Read the note and display it but do not allow it to be modified.
                combined_note n = ReadNote(noteID);
                if (n != null)
                {
                    EditNote(n, false, false);
                }

                return false; // this is always FALSE since we did not change the note.
            }

            /// <summary>
            /// Edit the creditor note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Edit(Int32 noteID)
            {
                // Read the note. Edit the note based upon the indicators.
                combined_note n = ReadNote(noteID);
                if (n != null)
                {
                    if (EditNote(n, n.EnableEdit(), n.EnableFlags()) == System.Windows.Forms.DialogResult.OK)
                    {
                        return SaveNote(n);
                    }
                }

                return false;
            }

            /// <summary>
            /// Create the creditor note
            /// </summary>
            /// <param name="Creditor">ID associated with the creditor for this new note.</param>
            /// <returns></returns>
            public bool Create(int Vendor)
            {
                combined_note n = new combined_note()
                {
                    vendor = Vendor,
                    type = 1,
                    created_by = DefaultCreatedBy,
                    date_created = DefaultDateCreated,
                    dont_delete = DefaultDontDelete,
                    dont_edit = DefaultDontEdit,
                    dont_print = DefaultDontPrint,
                    expires = DefaultExpires,
                    note = DefaultNote,
                    subject = DefaultSubject,
                    IsDirty = false
                };

                if (EditNote(n, true, true) == System.Windows.Forms.DialogResult.OK)
                {
                    if (SaveNote(n))
                    {
                        return true;
                    }
                }

                return false;
            }

            /// <summary>
            /// Delete the creditor note
            /// </summary>
            /// <param name="noteID">ID of the note to be used. This is the primary key to the table.</param>
            /// <returns></returns>
            public override bool Delete(Int32 noteID)
            {
                // Find the note in the table. Use only portions to find the note's ability to delete.
                var q = bc.vendor_notes.Where(n => n.Id == noteID).FirstOrDefault();
                if (q == null)
                {
                    return false;
                }

                // Determine if we can delete the note. If not authorized then complain.
                var note = new combined_note(q);
                if (!note.EnableDelete())
                {
                    DebtPlus.Data.Forms.MessageBox.Show(STR_YouAreNotAuthorizedToDeleteThisNoteAtThisTime, STR_NoteDeletionSecurityViolation, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Exclamation);
                    return false;
                }

                // Confirm the deletion of the note.
                if (confirm_delete())
                {
                    bc.vendor_notes.DeleteOnSubmit(q);
                    bc.SubmitChanges();
                    return true;
                }

                // The note was not deleted.
                return false;
            }
        }
    }
}