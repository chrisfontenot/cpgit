using System;
using System.Data;

namespace DebtPlus.Notes
{
    /// <summary>
    /// Class to handle the notes in the system
    /// </summary>
    public sealed partial class NoteClass
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Debt Note processing
        /// </summary>
        public class DebtNote : ClientNote
        {
            /// <summary>
            /// Create a new instance of our class
            /// </summary>
            public DebtNote()
                : base()
            {
            }

            /// <summary>
            /// Define the text banner for the edit form. It needs to say Debt Note and not Client Note.
            /// </summary>
            /// <returns></returns>
            protected override string FormText()
            {
                return "Debt Note";
            }

            /// <summary>
            /// Create the debt note
            /// </summary>
            /// <param name="Client">Client ID associated with this new note.</param>
            /// <param name="ClientCreditor">Debt record ID associated with this new note.</param>
            /// <returns></returns>
            public bool Create(Int32 Client, Int32 ClientCreditor)
            {
                return CreateProcedure(Client, ClientCreditor);
            }

            /// <summary>
            /// Edit the debt Note
            /// </summary>
            /// <param name="drv">Pointer to the note to be used.</param>
            /// <param name="EnableEdit">Do we permit the note to be modified? TRUE for edit. FALSE for show.</param>
            /// <param name="EnableFlags">Do we want the flags (dont_edit/delete/print) to be enabled?</param>
            /// <returns></returns>
            protected override System.Windows.Forms.DialogResult EditClientNote(combined_note n, bool EnableEdit, bool EnableFlags)
            {
                using (var EditForm = new EditNoteForm(bc, EnableEdit, EnableFlags))
                {
                    EditForm.ValidTypes.Clear();
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent);
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Temporary);
                    EditForm.ValidTypes.Add(DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert);

                    // Fill out the editing form with the note information
                    EditForm.Text = FormText();

                    // Ask the user to change the form information
                    EditForm.note = n;
                    n.IsDirty = false;

                    // if the document was not dirty then the request was canceled.
                    System.Windows.Forms.DialogResult Answer = EditForm.ShowDialog();
                    if (Answer == System.Windows.Forms.DialogResult.OK && !n.IsDirty)
                    {
                        return System.Windows.Forms.DialogResult.Cancel;
                    }
                    return Answer;
                }
            }
        }
    }
}