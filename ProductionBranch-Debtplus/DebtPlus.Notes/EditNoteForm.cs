using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.UI.Common;

namespace DebtPlus.Notes
{
    public partial class EditNoteForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Use for logging purposes if needed
        private static log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Dispose of locally allocated storage
        /// </summary>
        /// <param name="disposing"></param>
        private void EditNoteForm_Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!DesignMode)
                {
                    UnRegisterHandlers();
                }
            }
        }

        // TRUE if the form is editable. FALSE for read-only.
        protected bool EnableEdit { get; private set; }

        // TRUE if the flags are editable. FALSE if read-only.
        protected bool EnableFlags { get; private set; }

        // Note pointer to be edited
        public NoteClass.combined_note note { get; set; }

        protected DebtPlus.LINQ.BusinessContext bc = null;

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        private EditNoteForm() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="DatabaseInfo">Pointer to the database connection object</param>
        /// <param name="EnableEdit">Should the form allow editing?</param>
        /// <param name="EnableFlags">Should the edit flags be enabled?</param>
        public EditNoteForm(DebtPlus.LINQ.BusinessContext bc, bool EnableEdit, bool EnableFlags)
            : this()
        {
            // Save the flag settings
            this.bc          = bc;
            this.EnableEdit  = EnableEdit;
            this.EnableFlags = EnableFlags;

            DevExpress.XtraSpellChecker.Native.SpellCheckTextControllersManager.Default.RegisterClass(typeof(DevExpress.XtraEditors.ComboBoxEdit), typeof(DevExpress.XtraSpellChecker.Native.SimpleTextEditTextController));
            DevExpress.XtraSpellChecker.Native.SpellCheckTextControllersManager.Default.RegisterClass(typeof(DevExpress.XtraRichEdit.RichEditControl), typeof(DevExpress.XtraRichEdit.SpellChecker.RichEditSpellCheckController));
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load                                     += EditNoteForm_Load;
            RichEditControl1.PopupMenuShowing             += RichEditControl1_PopupMenuShowing;
            RichEditControl1.SelectionChanged             += RichEditControl1_SelectionChanged;
            RichEditControl1.ZoomChanged                  += RichEditControl1_ZoomChanged;
            Button_OK.Click                               += Button_OK_Click;
            BarButtonItem_File_Exit.ItemClick             += BarButtonItem_File_Exit_ItemClick;
            View_Zoom_025.ItemClick                       += View_Zoom_025_CheckedChanged;
            View_Zoom_050.ItemClick                       += View_Zoom_050_CheckedChanged;
            View_Zoom_075.ItemClick                       += View_Zoom_075_CheckedChanged;
            View_Zoom_100.ItemClick                       += View_Zoom_100_CheckedChanged;
            View_Zoom_150.ItemClick                       += View_Zoom_150_CheckedChanged;
            View_Zoom_200.ItemClick                       += View_Zoom_200_CheckedChanged;
            View_Zoom_300.ItemClick                       += View_Zoom_300_CheckedChanged;
            BarButtonItem_Insert_File.ItemClick           += BarButtonItem_Insert_File_ItemClick;
            BarButtonItem_Tools_Spell.ItemClick           += BarButtonItem_Tools_Spell_ItemClick;
            BarSubItem_view_zoom.Popup                    += BarSubItem_view_zoom_Popup;
            RichEditControl1.KeyUp                        += RichEditControl1_KeyUp;
            BarButtonItem_Tools_SpellingOptions.ItemClick += BarButtonItem_Tools_SpellingOptions_ItemClick;
            SpellChecker1.CheckCompleteFormShowing        += SpellChecker1_CheckCompleteFormShowing;
            LayoutControl01.HideCustomization             += LayoutControl01_HideCustomization;
            barManager1.EndCustomization                  += BarManager1_EndCustomization;
            ComboBoxEdit_Type.SelectedIndexChanged        += ComboBoxEdit_Type_SelectedIndexChanged;

            // Track changes to the viewed toolbars
            barManager1.EndCustomization     += BarManager1_EndCustomization;
            ClipboardBar1.VisibleChanged     += BarManager1_EndCustomization;
            CommonBar1.VisibleChanged        += BarManager1_EndCustomization;
            FontBar1.VisibleChanged          += BarManager1_EndCustomization;
            EditingBar1.VisibleChanged       += BarManager1_EndCustomization;
            IllustrationsBar1.VisibleChanged += BarManager1_EndCustomization;
            StylesBar1.VisibleChanged        += BarManager1_EndCustomization;
            ZoomBar1.VisibleChanged          += BarManager1_EndCustomization;
            ParagraphBar1.VisibleChanged     += BarManager1_EndCustomization;
            Bar_Status.VisibleChanged        += BarManager1_EndCustomization;

            LayoutControl01.Resize           += LayoutControl01_Resize;
        }

        /// <summary>
        /// Remove the event registration from the form
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load                                     -= EditNoteForm_Load;
            RichEditControl1.PopupMenuShowing             -= RichEditControl1_PopupMenuShowing;
            RichEditControl1.SelectionChanged             -= RichEditControl1_SelectionChanged;
            RichEditControl1.ZoomChanged                  -= RichEditControl1_ZoomChanged;
            Button_OK.Click                               -= Button_OK_Click;
            BarButtonItem_File_Exit.ItemClick             -= BarButtonItem_File_Exit_ItemClick;
            View_Zoom_025.ItemClick                       -= View_Zoom_025_CheckedChanged;
            View_Zoom_050.ItemClick                       -= View_Zoom_050_CheckedChanged;
            View_Zoom_075.ItemClick                       -= View_Zoom_075_CheckedChanged;
            View_Zoom_100.ItemClick                       -= View_Zoom_100_CheckedChanged;
            View_Zoom_150.ItemClick                       -= View_Zoom_150_CheckedChanged;
            View_Zoom_200.ItemClick                       -= View_Zoom_200_CheckedChanged;
            View_Zoom_300.ItemClick                       -= View_Zoom_300_CheckedChanged;
            BarButtonItem_Insert_File.ItemClick           -= BarButtonItem_Insert_File_ItemClick;
            BarButtonItem_Tools_Spell.ItemClick           -= BarButtonItem_Tools_Spell_ItemClick;
            BarSubItem_view_zoom.Popup                    -= BarSubItem_view_zoom_Popup;
            RichEditControl1.KeyUp                        -= RichEditControl1_KeyUp;
            BarButtonItem_Tools_SpellingOptions.ItemClick -= BarButtonItem_Tools_SpellingOptions_ItemClick;
            SpellChecker1.CheckCompleteFormShowing        -= SpellChecker1_CheckCompleteFormShowing;
            LayoutControl01.HideCustomization             -= LayoutControl01_HideCustomization;
            barManager1.EndCustomization                  -= BarManager1_EndCustomization;
            ComboBoxEdit_Type.SelectedIndexChanged        -= ComboBoxEdit_Type_SelectedIndexChanged;

            // Track changes to the viewed toolbars
            barManager1.EndCustomization     -= BarManager1_EndCustomization;
            ClipboardBar1.VisibleChanged     -= BarManager1_EndCustomization;
            CommonBar1.VisibleChanged        -= BarManager1_EndCustomization;
            FontBar1.VisibleChanged          -= BarManager1_EndCustomization;
            EditingBar1.VisibleChanged       -= BarManager1_EndCustomization;
            IllustrationsBar1.VisibleChanged -= BarManager1_EndCustomization;
            StylesBar1.VisibleChanged        -= BarManager1_EndCustomization;
            ZoomBar1.VisibleChanged          -= BarManager1_EndCustomization;
            ParagraphBar1.VisibleChanged     -= BarManager1_EndCustomization;
            Bar_Status.VisibleChanged        -= BarManager1_EndCustomization;

            LayoutControl01.Resize           -= LayoutControl01_Resize;
        }

        /// <summary>
        /// This is a list of the valid types that may be represented by this note.
        /// </summary>
        internal System.Collections.Generic.List<DebtPlus.LINQ.InMemory.Notes.NoteTypes> privateValidTypes = new System.Collections.Generic.List<DebtPlus.LINQ.InMemory.Notes.NoteTypes>();
        internal System.Collections.Generic.List<DebtPlus.LINQ.InMemory.Notes.NoteTypes> ValidTypes
        {
            get
            {
                return privateValidTypes;
            }
        }

        /// <summary>
        /// Handle the condition where the layout has been resized.
        /// </summary>
        private void LayoutControl01_Resize(object ender, EventArgs e)
        {
            // Center the OK and CANCEL buttons
            try
            {
                EmptySpaceItem1.Width = (EmptySpaceItem1.Width + EmptySpaceItem2.Width) / 2;
            }
            catch (System.Exception ex)
            {
                log.Error("Error with LayoutControl01_Resize", ex);
            }
        }

        /// <summary>
        /// Handle the before-popup event for the document.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RichEditControl1_PopupMenuShowing(object sender, DevExpress.XtraRichEdit.PopupMenuShowingEventArgs e)
        {
            var item = new DevExpress.Utils.Menu.DXMenuItem("Spell Document...", new EventHandler(SpellPopup));
            item.BeginGroup = true;
            e.Menu.Items.Add(item);
        }

        /// <summary>
        /// Process the spelling popup. Spell the document.
        /// </summary>
        private void SpellPopup(object sender, EventArgs e)
        {
            SpellDocument();
        }

        /// <summary>
        /// Handle the cursor moving through the document
        /// </summary>
        private void RichEditControl1_SelectionChanged(object sender, EventArgs e)
        {
            Int32 index = RichEditControl1.Views.DraftView.CurrentPageIndex;
            BarStaticItem_Page.Caption = string.Format("{0:f0}", index);
        }

        /// <summary>
        /// Handle the change in the zoom factor from the control
        /// </summary>
        private void RichEditControl1_ZoomChanged(object sender, EventArgs e)
        {
            BarStaticItem_Zoom.Caption = string.Format("{0:f0}%", Convert.ToInt32(RichEditControl1.ActiveView.ZoomFactor * 100F));
        }

        private bool IsValidType(DebtPlus.LINQ.InMemory.Notes.NoteTypes newType)
        {
            return ValidTypes != null && ValidTypes.Any(t => t == newType);
        }

        /// <summary>
        /// Process the form load event for the notes form
        /// </summary>
        private void EditNoteForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                base.LoadPlacement("Notes");

                // Load the standard list of subjects
                ComboBoxEdit_Subjects.Properties.Items.Clear();
                ComboBoxEdit_Subjects.SelectedIndex = -1;
                foreach (DebtPlus.LINQ.subject s in DebtPlus.LINQ.Cache.subject.getList().OrderBy(s => s.description))
                {
                    DebtPlus.Data.Controls.ComboboxItem item = new DebtPlus.Data.Controls.ComboboxItem(s.description);
                    Int32 ItemNUmber = ComboBoxEdit_Subjects.Properties.Items.Add(item);
                    if (string.Compare(item.description, ComboBoxEdit_Subjects.Text, true) == 0)
                    {
                        ComboBoxEdit_Subjects.SelectedIndex = ItemNUmber;
                    }
                }

                // Set the subject text
                if (ComboBoxEdit_Subjects.SelectedIndex < 0)
                {
                    ComboBoxEdit_Subjects.Text = note.subject;
                }
                ComboBoxEdit_Subjects.Properties.ReadOnly = !EnableEdit;

                // Set the standard controls
                CheckEdit_DontDelete.Checked = note.dont_delete;
                CheckEdit_DontEdit.Checked   = note.dont_edit;
                CheckEdit_DontPrint.Checked  = note.dont_print;
                DateEdit_Expires.EditValue   = note.expires;

                // Define the read-only status appropriately
                CheckEdit_DontEdit.Properties.ReadOnly   = !EnableFlags;
                CheckEdit_DontDelete.Properties.ReadOnly = !EnableFlags;
                CheckEdit_DontPrint.Properties.ReadOnly  = !EnableFlags;
                DateEdit_Expires.Properties.ReadOnly     = !EnableEdit;

                // Define the types of permissible notes
                var NoteType = (DebtPlus.LINQ.InMemory.Notes.NoteTypes)note.type;
                for (Int32 note_item_Index = (Int32)DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent; note_item_Index < (Int32)DebtPlus.LINQ.InMemory.Notes.NoteTypes.HUD; ++note_item_Index)
                {
                    var note_item_type = (DebtPlus.LINQ.InMemory.Notes.NoteTypes)note_item_Index;
                    if (NoteType == note_item_type || IsValidType(note_item_type))
                    {
                        DebtPlus.Data.Controls.ComboboxItem newItem = null;

                        // construct the new item for the combobox list item
                        switch (note_item_type)
                        {
                            case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent:
                                newItem = new DebtPlus.Data.Controls.ComboboxItem("Permanent", DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent);
                                break;

                            case DebtPlus.LINQ.InMemory.Notes.NoteTypes.System:
                                newItem = new DebtPlus.Data.Controls.ComboboxItem("System", DebtPlus.LINQ.InMemory.Notes.NoteTypes.System);
                                break;

                            case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert:
                                newItem = new DebtPlus.Data.Controls.ComboboxItem("Alert", DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert);
                                break;

                            case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement:
                                newItem = new DebtPlus.Data.Controls.ComboboxItem("Disbursement", DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement);
                                break;

                            case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Research:
                                newItem = new DebtPlus.Data.Controls.ComboboxItem("Research", DebtPlus.LINQ.InMemory.Notes.NoteTypes.Research);
                                break;

                            case DebtPlus.LINQ.InMemory.Notes.NoteTypes.HUD:
                                newItem = new DebtPlus.Data.Controls.ComboboxItem("Housing", DebtPlus.LINQ.InMemory.Notes.NoteTypes.HUD);
                                break;

                            default:
                                break;
                        }

                        // if( there is an item, add it
                        if (newItem != null)
                        {
                            ComboBoxEdit_Type.Properties.Items.Add(newItem);
                        }

                        // if the item is the default note type, select it.
                        if (NoteType == note_item_type)
                        {
                            ComboBoxEdit_Type.SelectedItem = newItem;
                        }
                    }
                }

                // Indicate that we want the items sorted in the list
                ComboBoxEdit_Type.Properties.Sorted = true;

                // Set the first item if there is no selection
                if (ComboBoxEdit_Type.SelectedIndex < 0)
                {
                    ComboBoxEdit_Type.SelectedIndex = 0;
                }

                // if this is RTF text then try to load the RTF code. Otherwise, try ASCII text.
                bool NoteLoaded = false;

                // Start the update events
                RichEditControl1.BeginUpdate();

                // First, try RTF as the note format
                if (DebtPlus.Utils.Format.Strings.IsRTF(note.note))
                {
                    try
                    {
                        RichEditControl1.RtfText = note.note;
                        NoteLoaded = true;
                    }
                    catch (System.Exception ex)
                    {
                        log.Error("Error loading RTF editing control", ex);
                    }
                }

                // try { to use HTML as the note format if possible
                if ((!NoteLoaded) && DebtPlus.Utils.Format.Strings.IsHTML(note.note))
                {
                    try
                    {
                        RichEditControl1.HtmlText = note.note;
                        NoteLoaded = true;
                    }
                    catch (System.Exception ex)
                    {
                        log.Error("Error loading HTML editing control", ex);
                    }
                }

                // Load the note as ASCII text if the note was not loaded as RTF
                if (!NoteLoaded)
                {
                    try
                    {
                        RichEditControl1.Text = note.note;
                    }
                    catch (System.Exception ex)
                    {
                        log.Error("Error loading TXT editing control", ex);
                    }
                }
                RichEditControl1.ReadOnly = !EnableEdit;

                RichEditControl1.EndUpdate();

                // Disable the type if the control is readonly.
                ComboBoxEdit_Type.Properties.ReadOnly = (!EnableEdit) || ((NoteType == DebtPlus.LINQ.InMemory.Notes.NoteTypes.System) || (NoteType == DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement));

                // Restore the layout of the form
                // Load the layout from the saved file
                string Filename = System.IO.Path.Combine(XMLDirectoryName(), STR_LayoutXML);

                try
                {
                    if (System.IO.File.Exists(Filename))
                    {
                        LayoutControl01.RestoreLayoutFromXml(Filename);
                        DisplayExpireDate(); // re-show or re-hide the expiration date correctly
                    }
                }

                catch (System.IO.DirectoryNotFoundException ex)
                {
                    log.ErrorFormat("Error reading '{0}' file: {1}", Filename, ex.Message);
                }
                catch (System.IO.FileNotFoundException ex)
                {
                    log.ErrorFormat("Error reading '{0}' file: {1}", Filename, ex.Message);
                }
                catch (System.Xml.XmlException ex)
                {
                    log.ErrorFormat("Error reading '{0}' file: {1}", Filename, ex.Message);
                }

                // Load the layout from the saved file
                Filename = System.IO.Path.Combine(XMLDirectoryName(), STR_BarsXML);
                try
                {
                    if (System.IO.File.Exists(Filename))
                    {
                        barManager1.RestoreLayoutFromXml(Filename);
                    }
                }

                catch (System.IO.DirectoryNotFoundException ex)
                {
                    log.ErrorFormat("Error reading '{0}' file: {1}", Filename, ex.Message);
                }
                catch (System.IO.FileNotFoundException ex)
                {
                    log.ErrorFormat("Error reading '{0}' file: {1}", Filename, ex.Message);
                }
                catch (System.Xml.XmlException ex)
                {
                    log.ErrorFormat("Error reading '{0}' file: {1}", Filename, ex.Message);
                }
            }

            finally
            {
                RegisterHandlers();
            }

            // Enable or disable the expiration date based upon the note type.
            DisplayExpireDate();
        }

        /// <summary>
        /// Process a change in the type of the note
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboBoxEdit_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            DisplayExpireDate();
        }

        private void DisplayExpireDate()
        {
            // Find the selected note type
            DebtPlus.LINQ.InMemory.Notes.NoteTypes SelectedNoteType;
            if (ComboBoxEdit_Type.SelectedIndex >= 0)
            {
                SelectedNoteType = (DebtPlus.LINQ.InMemory.Notes.NoteTypes)((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_Type.SelectedItem).value;
            }
            else
            {
                SelectedNoteType = DebtPlus.LINQ.InMemory.Notes.NoteTypes.Permanent;
            }

            // Enable or disable the expiration date based upon the note type.
            ComboBoxEdit_Type.Properties.ReadOnly = (!EnableEdit) || ((SelectedNoteType == DebtPlus.LINQ.InMemory.Notes.NoteTypes.System) || (SelectedNoteType == DebtPlus.LINQ.InMemory.Notes.NoteTypes.Disbursement));

            UnRegisterHandlers();  // Do not trip the save function just because we are hiding the expiration date
            try
            {
                switch (SelectedNoteType)
                {
                    case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert:
                    case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Temporary:
                        DateEdit_Expires.Properties.ReadOnly = false;
                        LayoutControlItem_Expires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                        break;

                    default:
                        DateEdit_Expires.Properties.ReadOnly = true;
                        LayoutControlItem_Expires.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
                        break;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the CLICK even on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(object sender, EventArgs e)
        {
#if true    // USE RTF as the default note format text
            note.note = RichEditControl1.RtfText;
#else       // USE HTML as the default note format text
            note.note = RichEditControl1.HtmlText;
#endif
            note.type = Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_Type.SelectedItem).value);
            note.subject = ComboBoxEdit_Subjects.Text.Trim();

            note.dont_delete = CheckEdit_DontDelete.Checked;
            note.dont_edit = CheckEdit_DontEdit.Checked;
            note.dont_print = CheckEdit_DontPrint.Checked;

            switch ((DebtPlus.LINQ.InMemory.Notes.NoteTypes)note.type)
            {
                case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Alert:
                case DebtPlus.LINQ.InMemory.Notes.NoteTypes.Temporary:
                    note.expires = DebtPlus.Utils.Nulls.v_DateTime(DateEdit_Expires.EditValue);
                    break;

                default:
                    note.expires = new DateTime?();
                    break;
            }

            // Complete the dialog normally
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        #region File menu
        private void BarButtonItem_File_Print_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraRichEdit.Commands.QuickPrintCommand cmd = new DevExpress.XtraRichEdit.Commands.QuickPrintCommand(RichEditControl1);
            cmd.Execute();
        }

        private void BarButtonItem_File_PrintDialog_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraRichEdit.Commands.PrintCommand cmd = new DevExpress.XtraRichEdit.Commands.PrintCommand(RichEditControl1);
            cmd.Execute();
        }

        private void BarButtonItem_File_PrintPreview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraRichEdit.Commands.PrintPreviewCommand cmd = new DevExpress.XtraRichEdit.Commands.PrintPreviewCommand(RichEditControl1);
            cmd.Execute();
        }

        private void BarButtonItem_File_SaveAs_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraRichEdit.Commands.SaveDocumentAsCommand cmd = new DevExpress.XtraRichEdit.Commands.SaveDocumentAsCommand(RichEditControl1);
            cmd.Execute();
        }

        private void BarButtonItem_File_Save_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraRichEdit.Commands.SaveDocumentCommand cmd = new DevExpress.XtraRichEdit.Commands.SaveDocumentCommand(RichEditControl1);
            cmd.Execute();
        }

        private void SaveAs()
        {
            System.Windows.Forms.DialogResult Answer = System.Windows.Forms.DialogResult.Cancel;
            string FileName = string.Empty;

            // Ask the user for the filename
            using (var dlg = new System.Windows.Forms.SaveFileDialog())
            {
                dlg.AddExtension = true;
                dlg.CheckPathExists = true;
                dlg.DefaultExt = ".rtf";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                dlg.FileName = "*.rtf";
                dlg.OverwritePrompt = true;
                dlg.RestoreDirectory = true;
                dlg.Title = "Save Note Text";
                dlg.Filter = "RTF Text (*.rtf)|*.rtf|HTML Text (*.htm, *.html)|*.htm;*.html|Text (*.txt)|*.txt|All Files (*.*)|*.*";
                Answer = dlg.ShowDialog();
                if (Answer == System.Windows.Forms.DialogResult.OK)
                {
                    FileName = dlg.FileName;
                }
            }

            // if( successful then try to save the file according to the format
            if (Answer == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(FileName))
            {
                Save(FileName);
            }
        }

        private string SavedFilename = string.Empty;
        private void Save(string FileName)
        {
            try
            {
                if (string.Compare(System.IO.Path.GetExtension(FileName), ".rtf", true) == 0)
                {
                    RichEditControl1.Document.SaveDocument(FileName, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
                }
                else
                {
                    RichEditControl1.Document.SaveDocument(FileName, DevExpress.XtraRichEdit.DocumentFormat.PlainText);
                }
                SavedFilename = FileName;
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error saving file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        private void BarButtonItem_File_Exit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        #endregion File menu

        /// <summary>
        /// Process the SELECT ALL event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarButtonItem_Edit_SelectAll_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.XtraRichEdit.Commands.SelectAllCommand cmd = new DevExpress.XtraRichEdit.Commands.SelectAllCommand(RichEditControl1);
            cmd.Execute();
        }

        #region View menu
        private void View_Zoom_025_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RichEditControl1.ActiveView.ZoomFactor = 0.25F;
        }

        private void View_Zoom_050_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RichEditControl1.ActiveView.ZoomFactor = 0.5F;
        }

        private void View_Zoom_075_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RichEditControl1.ActiveView.ZoomFactor = 0.75F;
        }

        private void View_Zoom_100_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RichEditControl1.ActiveView.ZoomFactor = 1.0F;
        }

        private void View_Zoom_150_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RichEditControl1.ActiveView.ZoomFactor = 1.5F;
        }

        private void View_Zoom_200_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RichEditControl1.ActiveView.ZoomFactor = 2.0F;
        }

        private void View_Zoom_300_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RichEditControl1.ActiveView.ZoomFactor = 3.0F;
        }

        #endregion View menu

        #region Insert menu
        private void BarButtonItem_Insert_File_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string FileName = string.Empty;
            System.Windows.Forms.DialogResult Answer;
            using (var dlg = new System.Windows.Forms.OpenFileDialog())
            {
                dlg.AddExtension = true;
                dlg.AutoUpgradeEnabled = true;
                dlg.CheckFileExists = true;
                dlg.CheckPathExists = true;
                dlg.DefaultExt = ".txt";
                dlg.FileName = "*.txt";
                dlg.Filter = "Text Files (*.txt)|*.txt|RTF Files (*.rtf)|*.rtf|HTML Text (*.htm, *.html)|*.htm;*.html|All Files (*.*)|*.*";
                dlg.FilterIndex = 0;
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                dlg.Multiselect = false;
                dlg.ReadOnlyChecked = true;
                dlg.RestoreDirectory = true;
                dlg.Title = "Insert file";
                Answer = dlg.ShowDialog();
                FileName = dlg.FileName;
            }

            // if( there is a file then attempt to open it
            if (Answer == System.Windows.Forms.DialogResult.OK && !string.IsNullOrEmpty(FileName))
            {
                string strContents = string.Empty;
                try
                {
                    using (var txFS = new System.IO.StreamReader(FileName))
                    {
                        strContents = txFS.ReadToEnd().Trim();
                        txFS.Close();
                    }
                }
                catch (System.IO.FileNotFoundException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading include file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
                catch (System.IO.DirectoryNotFoundException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading include file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
                catch (System.IO.PathTooLongException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading include file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }

                // Attempt to determine the file format
                if (!string.IsNullOrEmpty(strContents))
                {
                    try
                    {
                        switch (System.IO.Path.GetExtension(FileName).ToLower())
                        {
                            case ".rtf":
                                RichEditControl1.Document.InsertRtfText(RichEditControl1.Document.CaretPosition, strContents);
                                break;
                            case ".htm":
                                RichEditControl1.Document.InsertHtmlText(RichEditControl1.Document.CaretPosition, strContents);
                                break;
                            case ".html":
                                RichEditControl1.Document.InsertHtmlText(RichEditControl1.Document.CaretPosition, strContents);
                                break;
                            default:
                                RichEditControl1.Document.InsertText(RichEditControl1.Document.CaretPosition, strContents);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error inserting file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                }
            }
        }

        #endregion Insert menu

        #region Tools menu
        private void BarButtonItem_Tools_Spell_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SpellDocument();
        }

        #endregion Tools menu

        /// <summary>
        /// Check the appropriate zoom level that reflects the values in the editor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarSubItem_view_zoom_Popup(object sender, EventArgs e)
        {
            // Convert the percentage to one of the known categories. Choose the closest one.
            Int32 Pct = Convert.ToInt32(RichEditControl1.ActiveView.ZoomFactor * 100.0);
            if (Pct >= 300)
                Pct = 300;
            else if (Pct >= 200)
                Pct = 200;
            else if (Pct >= 150)
                Pct = 150;
            else if (Pct >= 100)
                Pct = 100;
            else if (Pct >= 75)
                Pct = 75;
            else if (Pct >= 50)
                Pct = 50;
            else
                Pct = 25;

            // Check the appropriate box on the zoom factors.
            View_Zoom_300.Checked = Pct == 300;
            View_Zoom_200.Checked = Pct == 200;
            View_Zoom_150.Checked = Pct == 150;
            View_Zoom_100.Checked = Pct == 100;
            View_Zoom_075.Checked = Pct == 75;
            View_Zoom_050.Checked = Pct == 50;
            View_Zoom_025.Checked = Pct == 25;
        }

        #region Status Bar
        /// <summary>
        /// Handle the key-up event from the control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void RichEditControl1_KeyUp(Object sender, System.Windows.Forms.KeyEventArgs e)
        {
            if (Bar_Status.Visible)
            {
                BarStaticItem_CAPS.Caption = (System.Windows.Forms.Control.IsKeyLocked(System.Windows.Forms.Keys.CapsLock) ? "CAPS" : string.Empty);
                BarStaticItem_NUM.Caption = (System.Windows.Forms.Control.IsKeyLocked(System.Windows.Forms.Keys.NumLock) ? "NUM" : string.Empty);
                BarStaticItem_SCR.Caption = (System.Windows.Forms.Control.IsKeyLocked(System.Windows.Forms.Keys.Scroll) ? "SCR" : string.Empty);
            }
        }

        #endregion Status Bar

        #region Spell Checker

        /// <summary>
        /// Spell the document
        /// </summary>
        private void SpellDocument()
        {
            // Restore the option settings for the speller
            ReadSpellingOptions();

            // Set our spelling options for the new processing
            SpellChecker1.OptionsSpelling.CheckFromCursorPos = DevExpress.Utils.DefaultBoolean.False;
            SpellChecker1.OptionsSpelling.CheckSelectedTextFirst = DevExpress.Utils.DefaultBoolean.False;

            // Do the actual spelling of the note
            SpellChecker1.Check(RichEditControl1);           // check the note text
            SpellChecker1.Check(ComboBoxEdit_Subjects);      // and the subject text

            // Save the spell checker options should they have been shown
            SaveSpellCheckerOptions();

            // Display the completion event.
            DebtPlus.Data.Forms.MessageBox.Show("The spelling check is complete.", System.Windows.Forms.Application.ProductName, System.Windows.Forms.MessageBoxButtons.OK);
        }

        /// <summary>
        /// Allow the spelling options to be changed
        /// </summary>
        private void BarButtonItem_Tools_SpellingOptions_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // Restore the option settings for the speller
            ReadSpellingOptions();

            if (SpellChecker1.FormsManager.ShowOptionsForm() == System.Windows.Forms.DialogResult.OK)
            {
                SpellChecker1.OptionsSpelling.IgnoreEmails = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreEmails);
                SpellChecker1.OptionsSpelling.IgnoreMixedCaseWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreMixedCaseWords);
                SpellChecker1.OptionsSpelling.IgnoreRepeatedWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreRepeatedWords);
                SpellChecker1.OptionsSpelling.IgnoreUpperCaseWords = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreUpperCase);
                SpellChecker1.OptionsSpelling.IgnoreUrls = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreUrls);
                SpellChecker1.OptionsSpelling.IgnoreWordsWithNumbers = DevExpress.SpellChecker.Native.BoolDefaultBooleanConverter.ConvertBoolToDefaultBoolean(SpellChecker1.FormsManager.OptionsForm.IgnoreWordsWithDigits);

                // Save the spelling culture
                SpellChecker1.Culture = SpellChecker1.FormsManager.OptionsForm.Culture;

                // Save the options
                SaveSpellCheckerOptions();
            }
        }

        /// <summary>
        /// Set the spelling options into the spelling control
        /// </summary>
        private bool FirstSpelling = true;
        private void ReadSpellingOptions()
        {
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                // Load the dictionaries on the first time
                if (sharedDictionaryStorage1.Dictionaries.Count == 0)
                {
                    SpellCheckerDictionaries.SetSpellCheckerDictionaries(ref sharedDictionaryStorage1);
                }

                // if( this is the first time then load the options from the saved storage
                if (FirstSpelling)
                {
                    FirstSpelling = false;

                    string PathName = string.Format("{0}{1}DebtPlus{1}Spelling", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar);
                    string FileName = System.IO.Path.Combine(PathName, "Options.xml");

                    if (System.IO.File.Exists(FileName))
                    {
                        SpellChecker1.RestoreFromXML(FileName);
                    }
                }
            }
            catch (Exception ex)  // Don't trap if we have an error. Just ignore it.
            {
                log.Error("Error reading spelling options", ex);
            }

            finally
            {
                Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Save the spell checking options to the disk system for next time
        /// </summary>
        private void SaveSpellCheckerOptions()
        {
            try
            {
                string PathName = string.Format("{0}{1}DebtPlus{1}Spelling", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar);
                string FileName = System.IO.Path.Combine(PathName, "Options.xml");

                if (!System.IO.Directory.Exists(PathName))
                {
                    System.IO.Directory.CreateDirectory(PathName);
                }
                SpellChecker1.SaveToXML(FileName);
            }
            catch (Exception ex)   // Don't trap if we have an error. Just ignore it.
            {
                log.Error("Error saving spelling options", ex);
            }
        }

        /// <summary>
        /// Callback from the speller to indicate that the operation is complete
        /// </summary>
        private void SpellChecker1_CheckCompleteFormShowing(object sender, DevExpress.XtraSpellChecker.FormShowingEventArgs e)
        {
            e.Handled = true;
        }

        #endregion Spell Checker

        #region Layout
        protected const string STR_LayoutXML = "Layout.xml";
        protected const string STR_BarsXML = "Bars.xml";
        /// <summary>
        /// Return the directory for saving / restoring the configuration information
        /// </summary>
        /// <returns></returns>
        protected internal static string XMLDirectoryName()
        {
            return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Notes");
        }

        /// <summary>
        /// Called when the form customization is complete. Save the definition for the next time.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LayoutControl01_HideCustomization(object sender, EventArgs e)
        {
            string PathName = XMLDirectoryName();
            string Filename = System.IO.Path.Combine(PathName, STR_LayoutXML);
            bool Success = false;

            // try to save the file directly. This is the normal case once there is a file to be saved.
            try
            {
                if (System.IO.Directory.Exists(PathName))
                {
                    LayoutControl01.SaveLayoutToXml(Filename);
                    Success = true;
                }

            }
#pragma warning disable 168 // Yes, I know that "ex" is not used.
            catch (System.IO.FileLoadException ex) { }
            catch (System.IO.DirectoryNotFoundException ex) { }
#pragma warning restore 168 // Yes, I know that "ex" is not used.

            // try to create the base directory if the item does not exist
            if (!Success)
            {
                try
                {
                    System.IO.Directory.CreateDirectory(PathName);
                    if (System.IO.Directory.Exists(PathName))
                    {
                        LayoutControl01.SaveLayoutToXml(Filename);
                        Success = true;
                    }

                }
#pragma warning disable 168 // Yes, I know that "ex" is not used.
                catch (System.IO.FileLoadException ex) { }
                catch (System.IO.DirectoryNotFoundException ex) { }
#pragma warning restore 168 // Yes, I know that "ex" is not used.
            }
        }

        /// <summary>
        /// Called when the menu bar customization is complete. This will save the menu definitions for the next time.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BarManager1_EndCustomization(object sender, EventArgs e)
        {
            string PathName = XMLDirectoryName();
            string Filename = System.IO.Path.Combine(PathName, STR_BarsXML);
            bool Success = false;

            // try to save the file directly. This is the normal case once there is a file to be saved.
            try
            {
                if (System.IO.Directory.Exists(PathName))
                {
                    barManager1.SaveLayoutToXml(Filename);
                    Success = true;
                }

            }
#pragma warning disable 168 // Yes, I know that "ex" is not used.
            catch (System.IO.FileLoadException ex) { }
            catch (System.IO.DirectoryNotFoundException ex) { }
#pragma warning restore 168 // Yes, I know that "ex" is not used.

            // try to create the base directory if the item does not exist
            if (!Success)
            {
                try
                {
                    System.IO.Directory.CreateDirectory(PathName);
                    if (System.IO.Directory.Exists(PathName))
                    {
                        barManager1.SaveLayoutToXml(Filename);
                        Success = true;
                    }
                }
#pragma warning disable 168 // Yes, I know that "ex" is not used.
                catch (System.IO.FileLoadException ex) { }
                catch (System.IO.DirectoryNotFoundException ex) { }
#pragma warning restore 168 // Yes, I know that "ex" is not used.
            }
        }

        #endregion Layout
    }
}
