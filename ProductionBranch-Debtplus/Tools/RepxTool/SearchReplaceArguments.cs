﻿namespace RepxTool
{
    public class SearchReplaceArguments
    {
        public string SearchString { get; set; }
        public string ReplaceString { get; set; }
    }
}
