﻿using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace RepxTool
{
    public partial class MainWindow : Window
    {
        public Reports Reports { get; set; }

        public string[] References { get; set; }  // part of Validate(), which is not working yet


        public MainWindow()
        {
            InitializeComponent();
            ReportsDirectoryTb.Text = DebtPlus.Utils.Configuration.ReportsDirectory;
            //References = Directory.GetFiles("D:\\v4\\DebtPlus.Desktop\\bin\\Debug", "*.dll");
            if (ReportsDirectoryTb.Text == string.Empty)
            {
                System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
                ReportsDirectoryTb.Text = asm.Location;
            }
        }

        #region Clearing/Resetting
        private void Reset()
        {
            ClearMainGrid();
            InitReports();
            ClearProgressBar();
            ClearStatusMessage();
        }

        private void ClearMainGrid()
        {
            ReportsLb.Items.Clear();
        }

        private void ClearProgressBar()
        {
            if (Reports == null) return;
            SetProgress(0, Reports.Count, 0);
        }

        private void SetProgress(int min, int max, int current)
        {
            progressBar1.Minimum = min;
            progressBar1.Maximum = max;
            progressBar1.Value = current;                        
        }

        private void ClearStatusMessage()
        {
            StatusMessage.Text = string.Empty;
        }

        private void InitReports()
        {
            if (Reports != null) Reports.Dispose();
            Reports = new Reports(ReportsDirectoryTb.Text);
            Reports.ScanReportNames();
        }
        #endregion Clearing/Resetting


        #region UI utils
        private void AddReportToDisplayList(Report rpt)
        {
            ReportsLb.Items.Add(rpt);
        }
        #endregion UI utils


        #region Scan Reports
        private void ScanButtonClick(object sender, RoutedEventArgs e)
        {
            Reset();
            StatusMessage.Text = "Scanning...";
            var bw = new BackgroundWorker();
            bw.DoWork += ScanReportsDoWork;
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
            bw.ProgressChanged += ScanReportsProgressChanged;
            bw.RunWorkerCompleted += ScanReportsCompleted;
            bw.RunWorkerAsync();
        }

        private void ScanReportsDoWork(object sender, DoWorkEventArgs e)
        {
            var bw = sender as BackgroundWorker;
            if (bw == null) return;

            var index = 0;
            var numReports = Reports.Count;
            foreach (var report in Reports)
            {
                report.LoadReport();
                index++;
                var progressArgs = new ReportProgressChangeArguments
                    {
                        CurrentReport = report,
                        ProgressMinimum = 0,
                        ProgressMaximum = numReports,
                        ProgressValue = index,
                    };
                bw.ReportProgress(index, progressArgs);
            }
        }

        private void ScanReportsProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var progressArgs = e.UserState as ReportProgressChangeArguments;
            if (progressArgs == null) return;

            var rpt = progressArgs.CurrentReport;
            if (rpt == null) return;

            SetProgress(progressArgs.ProgressMinimum, progressArgs.ProgressMaximum, progressArgs.ProgressValue);
            StatusMessage.Text = "Scanning " + rpt.FileName;

            AddReportToDisplayList(rpt);
        }

        private void ScanReportsCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ClearProgressBar();
            StatusMessage.Text = "Finished scanning " + Reports.Count + " reports";
        }

        #endregion Scan Reports


        #region Search & Clear Search
        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SearchTextTb.Text)) return;

            ClearMainGrid();
            foreach (var rpt in (from r in Reports where r.Search(SearchTextTb.Text) select r))
                AddReportToDisplayList(rpt);
        }

        private void ClearSearchButtonClick(object sender, RoutedEventArgs e)
        {
            ClearMainGrid();
            foreach (var rpt in (from r in Reports select r))
                AddReportToDisplayList(rpt);
        }
        #endregion Search & Clear Search


        #region Replace
        private void ReplaceButtonClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SearchTextTb.Text)) return;
            if (string.IsNullOrWhiteSpace(ReplaceTextTb.Text)) return;  // this might be intended, but don't allow for now.

            ClearProgressBar();
            StatusMessage.Text = "Replacing...";
            var bw = new BackgroundWorker();
            bw.DoWork += ReplaceDoWork;
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
            bw.ProgressChanged += ReplaceProgressChanged;
            bw.RunWorkerCompleted += ReplaceCompleted;

            var srargs = new SearchReplaceArguments {SearchString = SearchTextTb.Text, ReplaceString = ReplaceTextTb.Text};
            bw.RunWorkerAsync(srargs);
        }

        void ReplaceDoWork(object sender, DoWorkEventArgs e)
        {
            var bw = sender as BackgroundWorker;
            if (bw == null) return;

            var srargs = e.Argument as SearchReplaceArguments;
            if (srargs == null) return;

            var reportsWithSearchString = (from r in Reports where r.Search(srargs.SearchString) select r).ToList();
            var index = 0;
            var numReports = reportsWithSearchString.Count();
            foreach (var report in reportsWithSearchString)
            {
                report.Replace(srargs.SearchString, srargs.ReplaceString);
                index++;
                var progressArgs = new ReportProgressChangeArguments
                {
                    CurrentReport = report,
                    ProgressMinimum = 0,
                    ProgressMaximum = numReports,
                    ProgressValue = index,
                };
                bw.ReportProgress(index, progressArgs);
            }
        }

        void ReplaceProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var progressArgs = e.UserState as ReportProgressChangeArguments;
            if (progressArgs == null) return;

            var rpt = progressArgs.CurrentReport;
            if (rpt == null) return;

            SetProgress(progressArgs.ProgressMinimum, progressArgs.ProgressMaximum, progressArgs.ProgressValue);

            StatusMessage.Text = "Replaced " + rpt.FileName;
        }

        void ReplaceCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ClearProgressBar();
            StatusMessage.Text = "Finished replacing strings";
        }
        #endregion Replace


        #region Save

        private void SaveAllButtonClick(object sender, RoutedEventArgs e)
        {
            ClearProgressBar();
            StatusMessage.Text = "Saving...";
            var bw = new BackgroundWorker();
            bw.DoWork += SaveDoWork;
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
            bw.ProgressChanged += SaveProgressChanged;
            bw.RunWorkerCompleted += SaveCompleted;
            bw.RunWorkerAsync();
        }

        void SaveDoWork(object sender, DoWorkEventArgs e)
        {
            var bw = sender as BackgroundWorker;
            if (bw == null) return;

            var reportsToSave = (from r in Reports where r.HasChanged select r).ToList();
            var index = 0;
            var numReports = reportsToSave.Count();
            foreach (var report in reportsToSave)
            {
                report.Save();
                index++;
                var progressArgs = new ReportProgressChangeArguments
                {
                    CurrentReport = report,
                    ProgressMinimum = 0,
                    ProgressMaximum = numReports,
                    ProgressValue = index,
                };
                bw.ReportProgress(index, progressArgs);
            }
        }

        void SaveProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var progressArgs = e.UserState as ReportProgressChangeArguments;
            if (progressArgs == null) return;

            var rpt = progressArgs.CurrentReport;
            if (rpt == null) return;

            SetProgress(progressArgs.ProgressMinimum, progressArgs.ProgressMaximum, progressArgs.ProgressValue);

            StatusMessage.Text = "Saved " + rpt.FileName;
        }

        void SaveCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ClearProgressBar();
            StatusMessage.Text = "Finished saving reports";
        }

        #endregion Save


        #region Validate
        // currently inoperative - the XtraReport.Validate method is ornery

        private void ValidateAllButtonClick(object sender, RoutedEventArgs e)
        {
            ClearProgressBar();
            StatusMessage.Text = "Validating...";
            var bw = new BackgroundWorker();
            bw.DoWork += ValidateDoWork;
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
            bw.ProgressChanged += ValidateProgressChanged;
            bw.RunWorkerCompleted += ValidateCompleted;
            bw.RunWorkerAsync();
        }

        void ValidateDoWork(object sender, DoWorkEventArgs e)
        {
            var bw = sender as BackgroundWorker;
            if (bw == null) return;

            var index = 0;
            var numReports = Reports.Count();
            foreach (var report in Reports)
            {
                report.Validate(References);
                index++;
                var progressArgs = new ReportProgressChangeArguments
                {
                    CurrentReport = report,
                    ProgressMinimum = 0,
                    ProgressMaximum = numReports,
                    ProgressValue = index,
                };
                bw.ReportProgress(index, progressArgs);
            }
        }

        void ValidateProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var progressArgs = e.UserState as ReportProgressChangeArguments;
            if (progressArgs == null) return;

            var rpt = progressArgs.CurrentReport;
            if (rpt == null) return;

            SetProgress(progressArgs.ProgressMinimum, progressArgs.ProgressMaximum, progressArgs.ProgressValue);

            StatusMessage.Text = (rpt.ValidationErrors.Count <= 0 ? "Validated " : "INVALID ") + rpt.FileName;
        }

        void ValidateCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ClearProgressBar();
            StatusMessage.Text = "Finished saving reports";
        }

        #endregion Validate



    }
}
