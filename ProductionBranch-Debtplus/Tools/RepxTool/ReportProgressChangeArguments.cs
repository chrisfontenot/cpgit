﻿namespace RepxTool
{
    public class ReportProgressChangeArguments
    {
        public Report CurrentReport { get; set; }

        public int ProgressMinimum { get; set; }
        public int ProgressMaximum { get; set; }
        public int ProgressValue { get; set; }
    }
}
