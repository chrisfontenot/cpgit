﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RepxTool
{
    public class Reports : List<Report>, IDisposable
    {
        public string ReportsDirectory { get; set; }

        public Reports(string reportsDirectory)
        {
            ReportsDirectory = reportsDirectory;
        }

        public void ScanReportNames()
        {
            if (string.IsNullOrWhiteSpace(ReportsDirectory)) return;

            GetReportNames(ReportsDirectory);
        }

        private void GetReportNames(string targetDirectory)
        {
            if (!Directory.Exists(targetDirectory)) return;

            var fileEntries = Directory.GetFiles(targetDirectory, "*.repx");
            foreach (var fileName in fileEntries)
                Add(new Report{ FileName = fileName });
            
            // recurse
            var subdirectoryEntries = Directory.GetDirectories(targetDirectory);
            foreach (var subdirectory in from subdirectory in subdirectoryEntries let fileName = Path.GetFileName(subdirectory) where fileName != null && !fileName.StartsWith(".") select subdirectory)
                GetReportNames(subdirectory);
        }

        public void Dispose()
        {
            foreach (var item in this) item.Dispose();
        }
    }

}
