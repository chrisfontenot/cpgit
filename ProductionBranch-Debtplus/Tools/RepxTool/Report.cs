﻿using System;
using System.CodeDom.Compiler;
using DevExpress.XtraReports.UI;

namespace RepxTool
{
    public class Report: IDisposable
    {
        public string FileName { get; set; }
        public string Script
        {
            get { return XtraReport == null ? "" : XtraReport.ScriptsSource; }
            set { if (XtraReport != null) XtraReport.ScriptsSource = value; HasChanged = true; }
        }

        public XtraReport XtraReport { get; set; }
        public bool FailedToLoad { get; set; }
        public bool HasChanged { get; set; }
        public CompilerErrorCollection ValidationErrors { get; set; }  // doesn't work yet

        public Report()
        {
            FailedToLoad = false;
            HasChanged = false;
        }

        public bool LoadReport()
        {
            try
            {
                XtraReport = new XtraReport();
                XtraReport.LoadLayout(FileName);
            }
            catch (Exception)
            {
                XtraReport = null;
                FailedToLoad = true;
            }
            return FailedToLoad;
        }

        public bool Search(string searchString)
        {
            if (string.IsNullOrWhiteSpace(searchString)) return false;
            if (XtraReport == null || FailedToLoad) return false;

            return XtraReport.ScriptsSource.IndexOf(searchString) >= 0;
        }

        public bool Replace(string searchString, string replaceString)
        {
            if (string.IsNullOrWhiteSpace(searchString)) return false;
            if (XtraReport == null || FailedToLoad) return false;

            XtraReport.ScriptsSource = XtraReport.ScriptsSource.Replace(searchString, replaceString);
            HasChanged = true;
            return true;
        }

        public bool Validate(string[] references)
        {
            XtraReport.ScriptReferences = references;
            ValidationErrors = XtraReport.ValidateScripts();
            return ValidationErrors.Count > 0;
        }

        public void Save()
        {
            XtraReport.SaveLayout(FileName);
            HasChanged = false;
        }

        public void Dispose()
        {
            if (XtraReport != null) XtraReport.Dispose();
        }
    }
}
