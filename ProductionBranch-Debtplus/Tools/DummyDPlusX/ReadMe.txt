﻿This is a dummy app intended to be used as a testing tool.  It merely puts up a form saying "Dummy DPlusX".

The Desktop needs to be able to launch the older Crystal Reports, which is in an app called VB6/dplusx.exe.  

The VB6/dplusx.exe is relative to the root directory of the Desktop app.  During development, the root
directory of the Desktop is typically DebtPlus.Desktop/bin/Debug.  So to test whether the Desktop can
launch the Crystal Reports, just copy the dplusx.exe from this project into a directory called VB6 in
the root of the Desktop:

Copy:
	DummyDPlusX/bin/Debug/dplusx.exe
To:
	DebtPlus.Desktop/bin/Debug/VB6/dplusx.exe

We could make a post-build routine to do this automatically, but that might clobber any real dplusx.exe
that might have already been put in that location.
