﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace VersionStamp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            (new Executeable()).Run();
        }
    }

    class Executeable
    {
        System.Text.StringBuilder sb;
        String Major;
        String Minor;
        String Modification;
        String Build;

        public void Run()
        {
            using (Form1 frm = new Form1())
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return;
                }
                Major = frm.Major;
                Minor = frm.Minor;
                Modification = frm.Modification;
                Build = frm.Build;
            }

            // Process the WIX file
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
            string path = System.IO.Path.GetDirectoryName(asm.Location);

            // Merge the path to the installation folder and the WIX files
            string fname = System.IO.Path.Combine(path, "DebtPlus.Packaging/Version.WXI");
            ProcessWIX(path, "DebtPlus.Packaging/Version.WXI", 0);

            // Process the AssemblyInfo files
            ProcessDirectory(path);

            System.Windows.Forms.MessageBox.Show("Completed.", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ProcessWIX(string path, string fname, int level)
        {
            // Read the file contents
            string combinedFname = System.IO.Path.GetFullPath(System.IO.Path.Combine(path, fname));
            try
            {
                using (System.IO.StreamReader txt = new System.IO.StreamReader(combinedFname))
                {
                    sb = new System.Text.StringBuilder(txt.ReadToEnd());
                    txt.Close();
                }
            }

#pragma warning disable 168 // I know that "ex" is not used.
            catch (System.IO.DirectoryNotFoundException ex)
            {
                if (level == 8)
                {
                    throw;
                }
                ProcessWIX(path + "/..", fname, level + 1);
                return;
            }

            catch (System.IO.FileNotFoundException ex)
            {
                if (level == 8)
                {
                    throw;
                }
                ProcessWIX(path + "/..", fname, level + 1);
                return;
            }
#pragma warning restore 168

            ReplaceString("\\<\\?define\\s+VERSION\\s*=\\s*\"([^\"]*)\"\\?\\>", Major);
            ReplaceString("\\<\\?define\\s+REVISION\\s*=\\s*\"([^\"]*)\"\\?\\>", Minor);
            ReplaceString("\\<\\?define\\s+MODIFICATION\\s*=\\s*\"([^\"]*)\"\\?\\>", Modification);
            ReplaceString("\\<\\?define\\s+BUILD\\s*=\\s*\"([^\"]*)\"\\?\\>", Build);
            ReplaceString("\\<\\?define\\s+PRODUCTCODE\\s*=\\s*\"([^\"]*)\"\\?\\>", newGuid());

            // Generate the output file and leave
            string newFileName = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(combinedFname), "StampedVersion.wxi");
            ReplaceFile(newFileName, sb.ToString());

            // Hide the file. It does not need to show in the directory list.
            System.IO.File.SetAttributes(newFileName, System.IO.File.GetAttributes(newFileName) | System.IO.FileAttributes.Hidden);
        }

        private void ProcessDirectory(string dirName)
        {
            // Recurse to the directory
            foreach (string directory in System.IO.Directory.GetDirectories(dirName))
            {
                if (!System.IO.Path.GetFileName(directory).StartsWith("."))
                {
                    ProcessDirectory(directory);
                }
            }

            foreach (string filenmame in System.IO.Directory.GetFiles(dirName, "AssemblyInfo.cs"))
            {
                ProcessCSharp(filenmame);
            }

            foreach (string filenmame in System.IO.Directory.GetFiles(dirName, "AssemblyInfo.vb"))
            {
                ProcessVBNet(filenmame);
            }
        }

        private string newGuid()
        {
            string strValue = System.Text.RegularExpressions.Regex.Replace(System.Guid.NewGuid().ToString().ToUpper(), @"[{}]", "");
            return strValue.PadLeft(36, '0');
        }

        private void ProcessCSharp(string filename)
        {
            bool NeedsReplacement = false;

            // Read the file contents
            using (System.IO.StreamReader txt = new System.IO.StreamReader(filename))
            {
                sb = new System.Text.StringBuilder(txt.ReadToEnd());
                txt.Close();
            }

            NeedsReplacement = ReplaceString(@"^\s*\[assembly\: AssemblyVersion\(\""([^""]*)\""\)\]", string.Format("{0:f0}.{1:f0}.{2:f0}.{3:f0}", Major, Minor, Modification, Build)) |
                               ReplaceString(@"^\s*\[assembly\: AssemblyFileVersion\(\""([^""]*)\""\)\]", string.Format("{0:f0}.{1:f0}.{2:f0}.{3:f0}", Major, Minor, Modification, Build)) |
                               ReplaceString(@"^\s*\[assembly\: AssemblyInformationalVersion\(\""([^""]*)\""\)\]", string.Format("{0:f0}.{1:f0}.{2:f0}.{3:f0}", Major, Minor, Modification, Build));

            if (NeedsReplacement)
            {
                ReplaceFile(filename, sb.ToString());
            }
        }

        private void ProcessVBNet(string filename)
        {
            bool NeedsReplacement = false;

            // Read the file contents
            using (System.IO.StreamReader txt = new System.IO.StreamReader(filename))
            {
                sb = new System.Text.StringBuilder(txt.ReadToEnd());
                txt.Close();
            }

            NeedsReplacement = ReplaceString(@"^\s*\<Assembly\: AssemblyVersion\(""([^""]*)""\)\>", string.Format("{0:f0}.{1:f0}.{2:f0}.{3:f0}", Major, Minor, Modification, Build)) |
                               ReplaceString(@"^\s*\<Assembly\: AssemblyFileVersion\(""([^""]*)""\)\>", string.Format("{0:f0}.{1:f0}.{2:f0}.{3:f0}", Major, Minor, Modification, Build)) |
                               ReplaceString(@"^\s*\<Assembly\: AssemblyInformationalVersion\(""([^""]*)""\)\>", string.Format("{0:f0}.{1:f0}.{2:f0}.{3:f0}", Major, Minor, Modification, Build));

            if (NeedsReplacement)
            {
                ReplaceFile(filename, sb.ToString());
            }
        }

        private Boolean ReplaceString(string RegexString, string VersionString)
        {
            string testingString = sb.ToString();
            System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(sb.ToString(), RegexString, System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Multiline);
            if (match.Success)
            {
                System.Text.RegularExpressions.Group item = match.Groups[1];
                for (Int32 indx = item.Captures.Count - 1; indx >= 0; --indx)
                {
                    System.Text.RegularExpressions.Capture capture = item.Captures[indx];
                    if (capture.Length > 0)
                    {
                        sb.Remove(capture.Index, capture.Length);
                    }
                    sb.Insert(capture.Index, VersionString);
                }
            }
            return match.Success;
        }

        private void ReplaceFile(string Fname, string Contents)
        {
            string tmp = System.IO.Path.ChangeExtension(Fname, ".tmp");
            string bak = System.IO.Path.ChangeExtension(Fname, ".bak");

            if (System.IO.File.Exists(tmp))
            {
                System.IO.File.Delete(tmp);
            }

            using (System.IO.StreamWriter txt = new System.IO.StreamWriter(tmp))
            {
                txt.Write(Contents);
                txt.Flush();
                txt.Close();
            }

            if (System.IO.File.Exists(bak))
            {
                System.IO.File.Delete(bak);
            }

            if (System.IO.File.Exists(Fname))
            {
                System.IO.File.Move(Fname, bak);
            }

            System.IO.File.Move(tmp, Fname);
        }
    }
}
