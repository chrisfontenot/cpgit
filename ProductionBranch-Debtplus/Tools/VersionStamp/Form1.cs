﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace VersionStamp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            textBox1.KeyPress += textBox_KeyPress;
            textBox2.KeyPress += textBox_KeyPress;
            textBox3.KeyPress += textBox_KeyPress;
            textBox4.KeyPress += textBox_KeyPress;

            textBox1.TextChanged += EnableOK;
            textBox2.TextChanged += EnableOK;
            textBox3.TextChanged += EnableOK;
            textBox4.TextChanged += EnableOK;

            // Set the default values for the current date and time into the control boxes.
            // Allow the user to override them before the program is executed.
            var currentTime = GetCurrentESTTime();
            textBox1.Text = (currentTime.Year % 100).ToString();
            textBox2.Text = currentTime.Month.ToString();
            textBox3.Text = currentTime.Day.ToString();
            textBox4.Text = string.Format("{0:00}{1:00}", currentTime.Hour, currentTime.Minute);

            button1.Enabled = !HasErrors();
        }

        private DateTime GetCurrentESTTime()
        {
            var tzZulu = DateTime.UtcNow;
            var tzEST = tzZulu.AddHours(-5);            // Eastern Standard Time is UTC - 5 hours
            if (isDST(tzEST))
            {
                tzEST = tzEST.AddHours(1);              // EDT is + 1 hours
            }
            return tzEST;
        }

        private bool isDST(DateTime currentStandardTime)
        {
            if (currentStandardTime < StartingDST(currentStandardTime.Year))
            {
                return false;
            }

            if (currentStandardTime >= EndingDST(currentStandardTime.Year))
            {
                return false;
            }

            return true;
        }

        private DateTime StartingDST(Int32 currentYear)
        {
            // Find the first Sunday in April for the year
            var tryTime = new DateTime(currentYear, 4, 1);
            Int32 Days = (int) tryTime.DayOfWeek;
            if (Days != 7)
            {
                tryTime = tryTime.AddDays(7 - Days);
            }
            return tryTime.Add(new TimeSpan(2, 0, 0));      // The switch is at 0200 local time (standard time)
        }

        private DateTime EndingDST(Int32 currentYear)
        {
            // Find the first Sunday in November for the year
            var tryTime = new DateTime(currentYear, 11, 1);
            Int32 Days = (int)tryTime.DayOfWeek;
            if (Days != 7)
            {
                tryTime = tryTime.AddDays(7 - Days);
            }
            return tryTime.Add(new TimeSpan(1, 0, 0));      // The switch is at 0100 local time (standard time)
        }

        private void EnableOK(object sender, EventArgs e)
        {
            button1.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            Int32 testValue;
            if (Int32.TryParse(textBox1.Text, out testValue) && testValue >= 0)
            {
                if (Int32.TryParse(textBox2.Text, out testValue) && testValue >= 0)
                {
                    if (Int32.TryParse(textBox3.Text, out testValue) && testValue >= 0)
                    {
                        if (Int32.TryParse(textBox4.Text, out testValue) && testValue >= 0)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar))
            {
                if (!char.IsDigit(e.KeyChar))
                {
                    e.Handled = true;
                }
            }
        }

        public string Modification
        {
            get
            {
                return textBox3.Text.Trim();
            }
        }

        public string Build
        {
            get
            {
                return textBox4.Text.Trim();
            }
        }

        public string Major
        {
            get
            {
                return textBox1.Text.Trim();
            }
        }

        public string Minor
        {
            get
            {
                return textBox2.Text.Trim();
            }
        }
    }
}
