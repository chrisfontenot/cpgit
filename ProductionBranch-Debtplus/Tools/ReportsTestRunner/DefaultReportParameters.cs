﻿namespace ReportsTestRunner
{
    public class DefaultReportParameters
    {
        public int ClientId { get; set; }
        public string CreditorId { get; set; }
    }
}
