﻿using System.Collections.Generic;
using DebtPlus.Reports;

namespace ReportsTestRunner
{
    public class Reports : List<Report>
    {
        public Reports()
        {
#if false  // TODO:: FINISH THIS OFF !!!!
            var rpts = LoadReportShim.GetAllReports();
            foreach (var rpt in rpts)
            {
                Add(new Report
                    {
                        ReportName = rpt.Key,
                        ReportType = rpt.Value,
                        TestStatus = TestRunStatus.NotRun
                    });
            }
#endif
        }
    }
}
