﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.Interfaces.Client;
using DebtPlus.Interfaces.Creditor;

namespace ReportsTestRunner
{
    public class Report
    {
        public string ReportName { get; set; }
        public Type ReportType { get; set; }
        public TestRunStatus TestStatus { get; set; }
        public Dictionary<string, string> TestParameters { get; set; }

        public bool IsClientReport()
        {
            if (ReportType == null) return false;
            return (from i in ReportType.GetInterfaces() where i.Name.Contains("IClient")  select i).FirstOrDefault() != null;
        }
        public bool IsCreditorReport()
        {
            if (ReportType == null) return false;
            return (from i in ReportType.GetInterfaces() where i.Name.Contains("ICreditor") select i).FirstOrDefault() != null;
        }
        public bool IsAppointmentReport()
        {
            if (ReportType == null) return false;
            return ReportName.Contains("Appointment");
        }

        public bool Run(DefaultReportParameters defaults)
        {
            try
            {
                var rpt = DebtPlus.Reports.ReportLoader.LoadReport(ReportName);

                rpt.AllowParameterChangesByUser = true;

                var clientInterface = rpt as IClient;
                if (clientInterface != null)
                    clientInterface.ClientId = defaults.ClientId;

                var creditorInterface = rpt as ICreditor;
                if (creditorInterface != null)
                    creditorInterface.Creditor = defaults.CreditorId;

                if (rpt.RequestReportParameters() != DialogResult.OK) return false;

                TestParameters = rpt.ParametersDictionary();

                rpt.PrepareReport();
                rpt.ShowReport();
            }
            catch
            {
                TestStatus = TestRunStatus.Failure;
                return false;
            }
            TestStatus = TestRunStatus.Success;
            return true;
        }

    }
}
