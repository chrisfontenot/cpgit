﻿namespace ReportsTestRunner
{
    public enum TestRunStatus
    {
        NotRun = 0,
        Instantiating, 
        SettingParameters,
        Success,
        Failure,
    }
}
