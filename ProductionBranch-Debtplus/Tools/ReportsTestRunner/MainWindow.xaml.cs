﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ReportsTestRunner
{
    /*
     *  Objective:
     *  The intent of this little utility is to become an automated report testing tool.  It needs about a week of effort to complete it,
     *  but once complete it should save hundreds of hours each year from manually testing each report.
     * 
     *  Theory of operation/notes:
     *      1) Get list of runnable reports from the LoadReportShim
     *              Currently, some reports aren't in the returned list because the LoadReportShim uses reflection to identify reports.
     *              Reflection does a good job but misses several reports - namely the ones that use the kickers called Client.Packets.PacketReport 
     *              and Documents.ReportDocument.  Most reports are launched by <name>, so reflection is fine, but these are launched by <kickername, reportname>
     *              so reflection is insufficient.  To fix: don't add the fixup here, it belongs in LoadReportShim.  And, LoadReportShim might want to call
     *              the repository to get the missing <reportname> elements in building the list.  Again, do not add the fixup to here, it would be bad design.
     *      2) Display all reports and allow individual reports to be tested, or a automatically cycle through some or all reports.
     *      3) Test a report
     *              The Report class is very lightweight and should stay that way.  I've extended the IReports interface to make it easy to get a dictionary of
     *              report parameters used in a report after the user has entered these values.  These values can then be saved in some fashion to a flatfile and
     *              retrieved on subsequent runs of this app.
     *      4) Saving state
     *              Currently this app does not save it's state, but it should.  Specifically, it should save the test status of reports and each reports params.
     *              This will allow loading and continuing from where testing left off during the previous session.
     * 
     */ 

    public partial class MainWindow : Window
    {
        public Reports Reports { get; set; }
        public DefaultReportParameters DefaultParams { get; set; }
        
        public MainWindow()
        {
            InitializeComponent();
            SetDefaultState();
            InitReports();
        }

        #region Clearing/Resetting
        private void SetDefaultState()
        {
            // set parameter defaults
            DefaultClientIdTb.Text = (from m in (new DebtPlus.Data.MRU("Clients", "Recent Clients")).GetList select m).FirstOrDefault();
            DefaultCreditorIdTb.Text = (from m in (new DebtPlus.Data.MRU("Creditors", "Recent Creditors")).GetList select m).FirstOrDefault();

            // set control defaults
            AllReportsFilterRb.IsChecked = true;
        }

        private void Reset()
        {
            ClearMainGrid();
            InitReports();
            ClearProgressBar();
            ClearStatusMessage();
        }

        private void ClearMainGrid()
        {
            ReportsGd.ItemsSource = null;
        }

        private void UpdateReportsList()
        {
            ClearMainGrid();

            if (AllReportsFilterRb.IsChecked != null && AllReportsFilterRb.IsChecked == true)
                ReportsGd.ItemsSource = Reports;

            else if (ClientReportsFilterRb.IsChecked != null && ClientReportsFilterRb.IsChecked == true)
                ReportsGd.ItemsSource = (from r in Reports where r.IsClientReport() select r).ToList();

            else if (CreditorReportsFilterRb.IsChecked != null && CreditorReportsFilterRb.IsChecked == true)
                ReportsGd.ItemsSource = (from r in Reports where r.IsCreditorReport() select r).ToList();

            else if (AppointmentReportsFilterRb.IsChecked != null && AppointmentReportsFilterRb.IsChecked == true)
                ReportsGd.ItemsSource = (from r in Reports where r.IsAppointmentReport() select r).ToList();

            SelectedReportsCountTb.Text = (ReportsGd.Items.Count - 1).ToString(CultureInfo.InvariantCulture) + "/" + Reports.Count.ToString(CultureInfo.InvariantCulture);
        }

        private void ClearProgressBar()
        {
            if (Reports == null) return;
            SetProgress(0, 100, 0);
        }

        private void SetProgress(int min, int max, int current)
        {
            progressBar1.Minimum = min;
            progressBar1.Maximum = max;
            progressBar1.Value = current;
        }

        private void ClearStatusMessage()
        {
            StatusMessage.Text = string.Empty;
        }

        private void InitReports()
        {
            Reports = new Reports();
            UpdateReportsList();
        }
        #endregion Clearing/Resetting

        #region Convenience functions
        private static Report ReportFromContextMenuSender(object sender)
        {
            var menuItem = sender as MenuItem;
            if (menuItem == null) return null;

            var contextMenu = menuItem.Parent as ContextMenu;
            if (contextMenu == null) return null;

            var item = contextMenu.PlacementTarget as DataGrid;
            if (item == null) return null;
            

            var rpt = item.SelectedCells[0].Item as Report;
            return rpt;
        }

        private void SnapshotTheDefaultParams()
        {
            var clientIdStr = DefaultClientIdTb.Text;
            int clientId;
            try
            {
                clientId = Convert.ToInt32(clientIdStr);
            }
            catch
            {
                // todo: issue a popup about bad params
                return;
            }

            DefaultParams = new DefaultReportParameters
                {
                    ClientId = clientId,
                    CreditorId = DefaultCreditorIdTb.Text,
                };
        }

        private void ResetResults()
        {
            foreach (var rpt in ReportsGd.Items.OfType<Report>())
                rpt.TestStatus = TestRunStatus.NotRun;
            ReportsGd.Items.Refresh();
        }

        #endregion Convenience functions

        private void ContextMenuRunReportClick(object sender, RoutedEventArgs e)
        {
            var rpt = ReportFromContextMenuSender(sender);
            if (rpt == null) return;
            SnapshotTheDefaultParams();
            rpt.Run(DefaultParams);

            ReportsGd.Items.Refresh();
        }

        private void ClearResultsButtonClick(object sender, RoutedEventArgs e)
        {
            ResetResults();
        }
        
        #region Run Tests

        private void RunTestsButtonClick(object sender, RoutedEventArgs e)
        {
            ResetResults();
            SnapshotTheDefaultParams();
            StatusMessage.Text = "Running tests...";
            var bw = new BackgroundWorker();
            bw.DoWork += RunTestsDoWork;
            bw.WorkerSupportsCancellation = true;
            bw.WorkerReportsProgress = true;
            bw.ProgressChanged += RunTestsProgressChanged;
            bw.RunWorkerCompleted += RunTestsCompleted;
            bw.RunWorkerAsync();
        }

        private void RunTestsDoWork(object sender, DoWorkEventArgs e)
        {
            var bw = sender as BackgroundWorker;
            if (bw == null) return;

            var index = 0;
            var numReports = ReportsGd.Items.Count;
            foreach (var rpt in ReportsGd.Items.OfType<Report>())
            {
                rpt.Run(DefaultParams);
                index++;
                var progressArgs = new ReportProgressChangeArguments
                {
                    CurrentReport = rpt,
                    ProgressMinimum = 0,
                    ProgressMaximum = numReports,
                    ProgressValue = index,
                };
                bw.ReportProgress(index, progressArgs);
            }
        }

        private void RunTestsProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var progressArgs = e.UserState as ReportProgressChangeArguments;
            if (progressArgs == null) return;

            var rpt = progressArgs.CurrentReport;
            if (rpt == null) return;

            SetProgress(progressArgs.ProgressMinimum, progressArgs.ProgressMaximum, progressArgs.ProgressValue);

            StatusMessage.Text = "Testing  " + rpt.ReportName;
            ReportsGd.Items.Refresh();
        }

        private void RunTestsCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ClearProgressBar();
            StatusMessage.Text = "Finished testing " + ReportsGd.Items.Count + " reports";
        }

        #endregion Run Tests

        #region Report List Filters
        private void FilterButtonClick(object sender, RoutedEventArgs e)
        {
            UpdateReportsList();
        }
        #endregion Report List Filters





    }
}
