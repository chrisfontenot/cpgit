﻿Imports DebtPlus.Reports

Public Class ComboBoxReportsForm

    Public ReportsList As Dictionary(Of String, Type)
    Private Const ListStart As String = "Select a report..."

    Sub New()
        InitializeComponent()
        ClearStatus()

        ' TODO ::: FINISh THIS OFF !!!!!
        'ReportsList = GetAllReports()

        'Dim nameList As List(Of String) = (From r In ReportsList Select r.Key).ToList()

        'nameList.Insert(0, ListStart)

        'ComboBox1.DataSource = nameList
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        ClearStatus()
        If sender Is Nothing Then Return

        Dim combobox As ComboBox = CType(sender, ComboBox)
        If combobox Is Nothing Then Return

        Dim reportDefinition = combobox.SelectedItem
        If String.IsNullOrWhiteSpace(reportDefinition) Then Return
        If reportDefinition = ListStart Then Return

        Dim rpt As IReports = DebtPlus.Reports.ReportLoader.LoadReport(reportDefinition)
        If rpt Is Nothing Then Return

        rpt.AllowParameterChangesByUser = True
        rpt.RunReportInSeparateThread()
    End Sub

    Private Sub SmokeTest_Click(sender As System.Object, e As System.EventArgs) Handles SmokeTest.Click
        Dim frm As SmokeTestForm = New SmokeTestForm(Me)
        frm.Show()
    End Sub

    Private Sub ClearStatus()
        ToolStripStatusLabel1.Text = String.Empty
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim frm As RunReportTestForm = New RunReportTestForm(Me)
        frm.Show()
    End Sub
End Class