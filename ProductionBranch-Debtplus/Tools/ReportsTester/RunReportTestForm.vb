Imports DebtPlus.Interfaces.Client

Public Class RunReportTestForm

    Private _parent As ComboBoxReportsForm
    Private _running As Boolean = False

    Public Sub New(ByVal parent As ComboBoxReportsForm)
        InitializeComponent()

        _parent = parent

        DefaultClientId.Text = GetDefaultClientId()

        ClearListView()
    End Sub

    Private Function GetDefaultClientId() As String
        Return (From m In (New DebtPlus.Data.MRU("Clients", "Recent Clients")).GetList Select m).FirstOrDefault()
    End Function

    Private Sub ClearListView()
        With ListView1
            .Clear()
            .Columns.Add("Report Class", 600, HorizontalAlignment.Left)
            .Columns.Add("Run Report Result", 100, HorizontalAlignment.Left)
            .Refresh()
        End With
    End Sub

    Private Sub ToggleRunTestsButtonText()
        Button1.Text = If(_running, "Stop Tests", "Run Tests")
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If Not _running Then
            _running = True
            ToggleRunTestsButtonText()
            ClearListView()
            RunTests()
        End If
        _running = False
        ToggleRunTestsButtonText()
    End Sub

    Private Sub RunTests()
        For Each r In _parent.ReportsList
            Application.DoEvents()

            If Not _running Then Return

            Dim row(2) As String
            row(0) = r.Key
            row(1) = "Running..."
            Dim lvItem As New ListViewItem(row)
            ListView1.Items.Add(lvItem)
            ListView1.Refresh()

            Dim success = True
            Try
                Dim rpt = DebtPlus.Reports.ReportLoader.LoadReport(r.Key)
                rpt.AllowParameterChangesByUser = True

                Dim paramType As IClient = TryCast(rpt, IClient)
                If paramType Is Nothing AndAlso OnlyClientReportsChkBx.Checked Then
                    lvItem.SubItems(1).Text = "Skipping"
                    ListView1.EnsureVisible(ListView1.Items.Count - 1)
                    ListView1.Refresh()
                    Continue For
                End If
                
                If paramType IsNot Nothing Then
                    paramType.ClientId = DefaultClientId.Text
                End If

                rpt.RequestReportParameters()
                rpt.RunReport()
            Catch
                success = False
            End Try

            lvItem.SubItems(1).Text = (If(success, "Success", "Test Failed"))
            ListView1.EnsureVisible(ListView1.Items.Count - 1)
            ListView1.Refresh()
        Next
    End Sub

End Class
