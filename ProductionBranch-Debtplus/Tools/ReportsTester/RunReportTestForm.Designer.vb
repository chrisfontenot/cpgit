﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RunReportTestForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DefaultClientId = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.OnlyClientReportsChkBx = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'ListView1
        '
        Me.ListView1.Location = New System.Drawing.Point(12, 121)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(791, 593)
        Me.ListView1.TabIndex = 0
        Me.ListView1.UseCompatibleStateImageBehavior = False
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(13, 13)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(106, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Run Tests"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(125, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(394, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "This test wll instantiate each report, prompt you for parameters, then run the re" & _
    "port."
        '
        'DefaultClientId
        '
        Me.DefaultClientId.Location = New System.Drawing.Point(112, 57)
        Me.DefaultClientId.Name = "DefaultClientId"
        Me.DefaultClientId.Size = New System.Drawing.Size(121, 20)
        Me.DefaultClientId.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(21, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Default Client ID"
        '
        'OnlyClientReportsChkBx
        '
        Me.OnlyClientReportsChkBx.AutoSize = True
        Me.OnlyClientReportsChkBx.Location = New System.Drawing.Point(251, 57)
        Me.OnlyClientReportsChkBx.Name = "OnlyClientReportsChkBx"
        Me.OnlyClientReportsChkBx.Size = New System.Drawing.Size(128, 17)
        Me.OnlyClientReportsChkBx.TabIndex = 5
        Me.OnlyClientReportsChkBx.Text = "Only run client reports"
        Me.OnlyClientReportsChkBx.UseVisualStyleBackColor = True
        '
        'RunReportTestForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(815, 726)
        Me.Controls.Add(Me.OnlyClientReportsChkBx)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DefaultClientId)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListView1)
        Me.Name = "RunReportTestForm"
        Me.Text = "Reports Run Tests"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DefaultClientId As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents OnlyClientReportsChkBx As System.Windows.Forms.CheckBox
End Class
