Imports DebtPlus.Reports

Public Class MainForm

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub ButtonClick(sender As System.Object, e As EventArgs) Handles Button1.Click, Button2.Click,
        Button3.Click, Button4.Click, Button5.Click, Button6.Click, Button7.Click, Button8.Click, Button9.Click,
        Button10.Click, Button11.Click, Button12.Click, Button13.Click, Button14.Click, Button15.Click, Button16.Click,
        Button17.Click, Button18.Click, Button19.Click, Button20.Click, Button21.Click, Button22.Click, Button23.Click, Button24.Click,
        Button25.Click, Button26.Click, Button27.Click, Button28.Click, Button29.Click, Button30.Click, Button31.Click,
        Button32.Click, Button33.Click, Button34.Click, Button35.Click, Button36.Click, Button37.Click, Button38.Click,
        Button39.Click, Button40.Click, Button41.Click, Button42.Click, Button43.Click, Button44.Click, Button45.Click,
        Button46.Click, Button47.Click, Button48.Click, Button49.Click, Button50.Click, Button51.Click, Button52.Click,
        Button53.Click, Button54.Click, Button55.Click, Button56.Click, Button57.Click, Button58.Click, Button59.Click,
        Button60.Click, Button61.Click, Button62.Click, Button63.Click, Button64.Click, Button65.Click, Button66.Click,
        Button67.Click, Button68.Click, Button69.Click, Button70.Click, Button71.Click

        ' the button control has a user-defined 'tag' property that is set to be the name of the report to launch
        ' we need to get this tag and validate it
        If sender Is Nothing Then Return
        Dim buttonTag = sender.Tag
        If buttonTag Is Nothing Then Return
        Dim reportDefinition = Convert.ToString(buttonTag)
        If String.IsNullOrWhiteSpace(reportDefinition) Then Return

        ' we have a valid-looking report definition, so make a loader call the report handler
        Dim rpt As IReports = DebtPlus.Reports.ReportLoader.LoadReport(reportDefinition)
        If rpt Is Nothing Then Return

        rpt.AllowParameterChangesByUser = True
        rpt.RunReportInSeparateThread()
    End Sub

End Class
