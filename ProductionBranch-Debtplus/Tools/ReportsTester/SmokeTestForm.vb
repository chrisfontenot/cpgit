Public Class SmokeTestForm

    Private _parent As ComboBoxReportsForm
    Private _running As Boolean = False

    Public Sub New(ByVal parent As ComboBoxReportsForm)
        InitializeComponent()

        _parent = parent
        ClearListView()
    End Sub

    Private Sub ClearListView()
        With ListView1
            .Clear()
            .Columns.Add("Report Class", 600, HorizontalAlignment.Left)
            .Columns.Add("Smoke Test Result", 100, HorizontalAlignment.Left)
            .Refresh()
        End With
    End Sub

    Private Sub ToggleSmokeTestButtonText()
        Button1.Text = If(_running, "Stop Tests", "Run Smoke Tests")
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        If Not _running Then
            _running = True
            ToggleSmokeTestButtonText()
            ClearListView()
            RunSmokeTests()
        End If
        _running = False
        ToggleSmokeTestButtonText()
    End Sub

    Private Sub RunSmokeTests()
        For Each r In _parent.ReportsList
            Application.DoEvents()

            If Not _running Then Return

            Dim row(2) As String
            row(0) = r.Key
            row(1) = "Running..."
            Dim lvItem As New ListViewItem(row)
            ListView1.Items.Add(lvItem)
            ListView1.Refresh()

            Dim success = True
            Try
                DebtPlus.Reports.ReportLoader.LoadReport(r.Key)
            Catch
                success = False
            End Try

            lvItem.SubItems(1).Text = (If(success, "Success", "Smoke Test Failed"))
            ListView1.EnsureVisible(ListView1.Items.Count - 1)
            ListView1.Refresh()
        Next
    End Sub

End Class
