﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button70 = New System.Windows.Forms.Button()
        Me.Button69 = New System.Windows.Forms.Button()
        Me.Button68 = New System.Windows.Forms.Button()
        Me.Button66 = New System.Windows.Forms.Button()
        Me.Button65 = New System.Windows.Forms.Button()
        Me.Button64 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Button63 = New System.Windows.Forms.Button()
        Me.Button62 = New System.Windows.Forms.Button()
        Me.Button61 = New System.Windows.Forms.Button()
        Me.Button60 = New System.Windows.Forms.Button()
        Me.Button59 = New System.Windows.Forms.Button()
        Me.Button58 = New System.Windows.Forms.Button()
        Me.Button57 = New System.Windows.Forms.Button()
        Me.Button56 = New System.Windows.Forms.Button()
        Me.Button55 = New System.Windows.Forms.Button()
        Me.Button54 = New System.Windows.Forms.Button()
        Me.Button53 = New System.Windows.Forms.Button()
        Me.Button52 = New System.Windows.Forms.Button()
        Me.Button51 = New System.Windows.Forms.Button()
        Me.Button50 = New System.Windows.Forms.Button()
        Me.Button49 = New System.Windows.Forms.Button()
        Me.Button48 = New System.Windows.Forms.Button()
        Me.Button47 = New System.Windows.Forms.Button()
        Me.Button46 = New System.Windows.Forms.Button()
        Me.Button45 = New System.Windows.Forms.Button()
        Me.Button44 = New System.Windows.Forms.Button()
        Me.Button43 = New System.Windows.Forms.Button()
        Me.Button42 = New System.Windows.Forms.Button()
        Me.Button41 = New System.Windows.Forms.Button()
        Me.Button40 = New System.Windows.Forms.Button()
        Me.Button39 = New System.Windows.Forms.Button()
        Me.Button38 = New System.Windows.Forms.Button()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.Button36 = New System.Windows.Forms.Button()
        Me.Button35 = New System.Windows.Forms.Button()
        Me.Button34 = New System.Windows.Forms.Button()
        Me.Button33 = New System.Windows.Forms.Button()
        Me.Button32 = New System.Windows.Forms.Button()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.Button67 = New System.Windows.Forms.Button()
        Me.Button71 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Tag = "DebtPlus.Reports.ACH.Balancing"
        Me.Button1.Text = "Balancing"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button20)
        Me.GroupBox1.Controls.Add(Me.Button19)
        Me.GroupBox1.Controls.Add(Me.Button18)
        Me.GroupBox1.Controls.Add(Me.Button17)
        Me.GroupBox1.Controls.Add(Me.Button16)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 133)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "ACH"
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(87, 48)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(82, 23)
        Me.Button20.TabIndex = 5
        Me.Button20.Tag = "DebtPlus.Reports.ACH.Transactions"
        Me.Button20.Text = "Transactions"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(87, 19)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(82, 23)
        Me.Button19.TabIndex = 4
        Me.Button19.Tag = "DebtPlus.Reports.ACH.Changes"
        Me.Button19.Text = "Responses"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(6, 104)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(75, 23)
        Me.Button18.TabIndex = 3
        Me.Button18.Tag = "DebtPlus.Reports.ACH.ClientList"
        Me.Button18.Text = "Client List"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(6, 77)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(75, 23)
        Me.Button17.TabIndex = 2
        Me.Button17.Tag = "DebtPlus.Reports.ACH.Changes"
        Me.Button17.Text = "Changes"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(6, 48)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(75, 23)
        Me.Button16.TabIndex = 1
        Me.Button16.Tag = "DebtPlus.Reports.Tables.ACH.Batches"
        Me.Button16.Text = "Batches"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button8)
        Me.GroupBox2.Controls.Add(Me.Button3)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Location = New System.Drawing.Point(425, 119)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 116)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "RPPS"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(6, 77)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(120, 23)
        Me.Button8.TabIndex = 2
        Me.Button8.Tag = "DebtPlus.Reports.RPPS.Responses"
        Me.Button8.Text = "Responses"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(6, 48)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(120, 23)
        Me.Button3.TabIndex = 1
        Me.Button3.Tag = "DebtPlus.Reports.RPPS.InvalidAccounts"
        Me.Button3.Text = "Invalid Accounts"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(6, 19)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(120, 23)
        Me.Button2.TabIndex = 0
        Me.Button2.Tag = "DebtPlus.Reports.RPPS.Dropped.Proposals"
        Me.Button2.Text = "Dropped Proposals"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button5)
        Me.GroupBox3.Controls.Add(Me.Button4)
        Me.GroupBox3.Location = New System.Drawing.Point(219, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 100)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Proposals"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(6, 19)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(75, 23)
        Me.Button5.TabIndex = 1
        Me.Button5.Tag = "DebtPlus.Reports.Proposals.Full"
        Me.Button5.Text = "Full"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(6, 48)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 0
        Me.Button4.Tag = "DebtPlus.Reports.Proposals.Standard"
        Me.Button4.Text = "Standard"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button71)
        Me.GroupBox4.Controls.Add(Me.Button70)
        Me.GroupBox4.Controls.Add(Me.Button69)
        Me.GroupBox4.Controls.Add(Me.Button68)
        Me.GroupBox4.Controls.Add(Me.Button66)
        Me.GroupBox4.Controls.Add(Me.Button65)
        Me.GroupBox4.Controls.Add(Me.Button64)
        Me.GroupBox4.Controls.Add(Me.Button7)
        Me.GroupBox4.Controls.Add(Me.Button6)
        Me.GroupBox4.Location = New System.Drawing.Point(631, 13)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(200, 328)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Trust"
        '
        'Button70
        '
        Me.Button70.Location = New System.Drawing.Point(6, 249)
        Me.Button70.Name = "Button70"
        Me.Button70.Size = New System.Drawing.Size(133, 23)
        Me.Button70.TabIndex = 9
        Me.Button70.Tag = "DebtPlus.Reports.Trust.ReservedInTrust"
        Me.Button70.Text = "Reserved In Trust"
        Me.Button70.UseVisualStyleBackColor = True
        '
        'Button69
        '
        Me.Button69.Location = New System.Drawing.Point(6, 220)
        Me.Button69.Name = "Button69"
        Me.Button69.Size = New System.Drawing.Size(133, 23)
        Me.Button69.TabIndex = 8
        Me.Button69.Tag = "DebtPlus.Reports.Trust.DailyDeposits"
        Me.Button69.Text = "Daily Deposits"
        Me.Button69.UseVisualStyleBackColor = True
        '
        'Button68
        '
        Me.Button68.Location = New System.Drawing.Point(6, 191)
        Me.Button68.Name = "Button68"
        Me.Button68.Size = New System.Drawing.Size(133, 23)
        Me.Button68.TabIndex = 7
        Me.Button68.Tag = "DebtPlus.Reports.Trust.DailyDeposits.ByBatch"
        Me.Button68.Text = "Daily Deposit by Batch"
        Me.Button68.UseVisualStyleBackColor = True
        '
        'Button66
        '
        Me.Button66.Location = New System.Drawing.Point(6, 162)
        Me.Button66.Name = "Button66"
        Me.Button66.Size = New System.Drawing.Size(133, 23)
        Me.Button66.TabIndex = 6
        Me.Button66.Tag = "DebtPlus.Reports.Trust.CheckRegister.Detail"
        Me.Button66.Text = "Check Register Detail"
        Me.Button66.UseVisualStyleBackColor = True
        '
        'Button65
        '
        Me.Button65.Location = New System.Drawing.Point(6, 133)
        Me.Button65.Name = "Button65"
        Me.Button65.Size = New System.Drawing.Size(120, 23)
        Me.Button65.TabIndex = 5
        Me.Button65.Tag = "DebtPlus.Reports.Trust.CheckRegister"
        Me.Button65.Text = "Check Register"
        Me.Button65.UseVisualStyleBackColor = True
        '
        'Button64
        '
        Me.Button64.Location = New System.Drawing.Point(6, 104)
        Me.Button64.Name = "Button64"
        Me.Button64.Size = New System.Drawing.Size(120, 23)
        Me.Button64.TabIndex = 4
        Me.Button64.Tag = "DebtPlus.Reports.Trust.Register"
        Me.Button64.Text = "Register"
        Me.Button64.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(6, 75)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(120, 23)
        Me.Button7.TabIndex = 1
        Me.Button7.Tag = "DebtPlus.Reports.Trust.Balance"
        Me.Button7.Text = "Balance"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(6, 19)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(120, 23)
        Me.Button6.TabIndex = 0
        Me.Button6.Tag = "DebtPlus.Reports.Trust.AccountBalance"
        Me.Button6.Text = "Account Balance"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Button9)
        Me.GroupBox5.Location = New System.Drawing.Point(425, 13)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(200, 100)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Daily Summary"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(6, 19)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(106, 23)
        Me.Button9.TabIndex = 0
        Me.Button9.Tag = "DebtPlus.Reports.DailySummary.Refunds.Creditor"
        Me.Button9.Text = "Creditor Refunds"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Button13)
        Me.GroupBox6.Controls.Add(Me.Button11)
        Me.GroupBox6.Controls.Add(Me.Button10)
        Me.GroupBox6.Location = New System.Drawing.Point(13, 152)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(200, 100)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Creditor"
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(119, 19)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(75, 23)
        Me.Button13.TabIndex = 2
        Me.Button13.Tag = "DebtPlus.Reports.Creditor.Invoice"
        Me.Button13.Text = "Invoice"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(6, 48)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(98, 23)
        Me.Button11.TabIndex = 1
        Me.Button11.Tag = "DebtPlus.Reports.Creditor.Invoice.Aging"
        Me.Button11.Text = "Invoice Aging"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(6, 19)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(75, 23)
        Me.Button10.TabIndex = 0
        Me.Button10.Tag = "DebtPlus.Reports.Creditor.ClientList"
        Me.Button10.Text = "Client List"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.Button30)
        Me.GroupBox7.Controls.Add(Me.Button29)
        Me.GroupBox7.Controls.Add(Me.Button28)
        Me.GroupBox7.Controls.Add(Me.Button27)
        Me.GroupBox7.Controls.Add(Me.Button26)
        Me.GroupBox7.Controls.Add(Me.Button25)
        Me.GroupBox7.Controls.Add(Me.Button24)
        Me.GroupBox7.Controls.Add(Me.Button23)
        Me.GroupBox7.Controls.Add(Me.Button22)
        Me.GroupBox7.Controls.Add(Me.Button21)
        Me.GroupBox7.Controls.Add(Me.Button12)
        Me.GroupBox7.Location = New System.Drawing.Point(423, 241)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(200, 206)
        Me.GroupBox7.TabIndex = 2
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Appointments"
        '
        'Button30
        '
        Me.Button30.Location = New System.Drawing.Point(6, 135)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(75, 23)
        Me.Button30.TabIndex = 10
        Me.Button30.Tag = "DebtPlus.Reports.Appointments.Template"
        Me.Button30.Text = "Template"
        Me.Button30.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.Location = New System.Drawing.Point(6, 106)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(75, 23)
        Me.Button29.TabIndex = 9
        Me.Button29.Tag = "DebtPlus.Reports.Appointments.Schedule"
        Me.Button29.Text = "Schedule"
        Me.Button29.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.Location = New System.Drawing.Point(87, 164)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(102, 23)
        Me.Button28.TabIndex = 8
        Me.Button28.Tag = "DebtPlus.Reports.Appointments.Schedule.Grid"
        Me.Button28.Text = "Schedule Grid"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.Location = New System.Drawing.Point(87, 135)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(102, 23)
        Me.Button27.TabIndex = 7
        Me.Button27.Tag = "DebtPlus.Reports.Appointments.Schedule.Analysis"
        Me.Button27.Text = "Schedule Analysis"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.Location = New System.Drawing.Point(87, 106)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(102, 23)
        Me.Button26.TabIndex = 6
        Me.Button26.Tag = "DebtPlus.Reports.Appointments.Results.ByOffice"
        Me.Button26.Text = "Results By Office"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.Location = New System.Drawing.Point(87, 77)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(102, 23)
        Me.Button25.TabIndex = 5
        Me.Button25.Tag = "DebtPlus.Reports.Appointments.Missed.Detail"
        Me.Button25.Text = "Missed Details"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.Location = New System.Drawing.Point(87, 48)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(102, 23)
        Me.Button24.TabIndex = 4
        Me.Button24.Tag = "DebtPlus.Reports.Appointments.ByCounselor"
        Me.Button24.Text = "By Counselor"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.Location = New System.Drawing.Point(6, 77)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(75, 23)
        Me.Button23.TabIndex = 3
        Me.Button23.Tag = "DebtPlus.Reports.Appointments.Booking"
        Me.Button23.Text = "Booking"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.Location = New System.Drawing.Point(87, 19)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(102, 23)
        Me.Button22.TabIndex = 2
        Me.Button22.Tag = "DebtPlus.Reports.Appointments.Available.Analysis"
        Me.Button22.Text = "Available Analysis"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.Location = New System.Drawing.Point(6, 48)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(75, 23)
        Me.Button21.TabIndex = 1
        Me.Button21.Tag = "DebtPlus.Reports.Appointments.Activity"
        Me.Button21.Text = "Activity"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(6, 19)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(75, 23)
        Me.Button12.TabIndex = 0
        Me.Button12.Tag = "DebtPlus.Reports.Appointments.ByOffice"
        Me.Button12.Text = "By Office"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Button63)
        Me.GroupBox8.Controls.Add(Me.Button62)
        Me.GroupBox8.Controls.Add(Me.Button61)
        Me.GroupBox8.Controls.Add(Me.Button60)
        Me.GroupBox8.Controls.Add(Me.Button59)
        Me.GroupBox8.Controls.Add(Me.Button58)
        Me.GroupBox8.Controls.Add(Me.Button57)
        Me.GroupBox8.Controls.Add(Me.Button56)
        Me.GroupBox8.Controls.Add(Me.Button55)
        Me.GroupBox8.Controls.Add(Me.Button54)
        Me.GroupBox8.Controls.Add(Me.Button53)
        Me.GroupBox8.Controls.Add(Me.Button52)
        Me.GroupBox8.Controls.Add(Me.Button51)
        Me.GroupBox8.Controls.Add(Me.Button50)
        Me.GroupBox8.Controls.Add(Me.Button49)
        Me.GroupBox8.Controls.Add(Me.Button48)
        Me.GroupBox8.Controls.Add(Me.Button47)
        Me.GroupBox8.Controls.Add(Me.Button46)
        Me.GroupBox8.Controls.Add(Me.Button45)
        Me.GroupBox8.Controls.Add(Me.Button44)
        Me.GroupBox8.Controls.Add(Me.Button43)
        Me.GroupBox8.Controls.Add(Me.Button42)
        Me.GroupBox8.Controls.Add(Me.Button41)
        Me.GroupBox8.Controls.Add(Me.Button40)
        Me.GroupBox8.Controls.Add(Me.Button39)
        Me.GroupBox8.Controls.Add(Me.Button38)
        Me.GroupBox8.Controls.Add(Me.Button37)
        Me.GroupBox8.Controls.Add(Me.Button36)
        Me.GroupBox8.Controls.Add(Me.Button35)
        Me.GroupBox8.Controls.Add(Me.Button34)
        Me.GroupBox8.Controls.Add(Me.Button33)
        Me.GroupBox8.Controls.Add(Me.Button32)
        Me.GroupBox8.Controls.Add(Me.Button31)
        Me.GroupBox8.Controls.Add(Me.Button15)
        Me.GroupBox8.Controls.Add(Me.Button14)
        Me.GroupBox8.Location = New System.Drawing.Point(13, 331)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(355, 394)
        Me.GroupBox8.TabIndex = 3
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Tag = ""
        Me.GroupBox8.Text = "Client"
        '
        'Button63
        '
        Me.Button63.Location = New System.Drawing.Point(202, 19)
        Me.Button63.Name = "Button63"
        Me.Button63.Size = New System.Drawing.Size(143, 23)
        Me.Button63.TabIndex = 34
        Me.Button63.Tag = "DebtPlus.Reports.Client.PacketDMP.TestingFullReport"
        Me.Button63.Text = "DMP Test Full Report"
        Me.Button63.UseVisualStyleBackColor = True
        '
        'Button62
        '
        Me.Button62.Location = New System.Drawing.Point(202, 365)
        Me.Button62.Name = "Button62"
        Me.Button62.Size = New System.Drawing.Size(143, 23)
        Me.Button62.TabIndex = 33
        Me.Button62.Tag = "DebtPlus.Reports.Client.PacketDMP.PacketReport"
        Me.Button62.Text = "DMP Packet Report"
        Me.Button62.UseVisualStyleBackColor = True
        '
        'Button61
        '
        Me.Button61.Location = New System.Drawing.Point(202, 338)
        Me.Button61.Name = "Button61"
        Me.Button61.Size = New System.Drawing.Size(143, 23)
        Me.Button61.TabIndex = 32
        Me.Button61.Tag = "DebtPlus.Reports.Client.PacketDMP.SurplusDeficit"
        Me.Button61.Text = "DMP Surplus Deficit"
        Me.Button61.UseVisualStyleBackColor = True
        '
        'Button60
        '
        Me.Button60.Location = New System.Drawing.Point(202, 309)
        Me.Button60.Name = "Button60"
        Me.Button60.Size = New System.Drawing.Size(143, 23)
        Me.Button60.TabIndex = 31
        Me.Button60.Tag = "DebtPlus.Reports.Client.PacketDMP.DMPdebt.DMPDebtOverview"
        Me.Button60.Text = "DMP Debt Overview"
        Me.Button60.UseVisualStyleBackColor = True
        '
        'Button59
        '
        Me.Button59.Location = New System.Drawing.Point(202, 280)
        Me.Button59.Name = "Button59"
        Me.Button59.Size = New System.Drawing.Size(143, 23)
        Me.Button59.TabIndex = 30
        Me.Button59.Tag = "DebtPlus.Reports.Client.PacketDMP.Disclosures.Survey"
        Me.Button59.Text = "DMP Survey"
        Me.Button59.UseVisualStyleBackColor = True
        '
        'Button58
        '
        Me.Button58.Location = New System.Drawing.Point(202, 251)
        Me.Button58.Name = "Button58"
        Me.Button58.Size = New System.Drawing.Size(143, 23)
        Me.Button58.TabIndex = 29
        Me.Button58.Tag = "DebtPlus.Reports.Client.PacketDMP.Disclosures.MonthlyStatements"
        Me.Button58.Text = "DMP Monthly Stmts"
        Me.Button58.UseVisualStyleBackColor = True
        '
        'Button57
        '
        Me.Button57.Location = New System.Drawing.Point(202, 222)
        Me.Button57.Name = "Button57"
        Me.Button57.Size = New System.Drawing.Size(143, 23)
        Me.Button57.TabIndex = 28
        Me.Button57.Tag = "DebtPlus.Reports.Client.PacketDMP.Disclosures.MakingAppropriateChoices"
        Me.Button57.Text = "DMP Making Choices"
        Me.Button57.UseVisualStyleBackColor = True
        '
        'Button56
        '
        Me.Button56.Location = New System.Drawing.Point(202, 193)
        Me.Button56.Name = "Button56"
        Me.Button56.Size = New System.Drawing.Size(143, 23)
        Me.Button56.TabIndex = 27
        Me.Button56.Tag = "DebtPlus.Reports.Client.PacketDMP.Disclosures.GettingStartedWithACH"
        Me.Button56.Text = "DMP Getting Started ACH"
        Me.Button56.UseVisualStyleBackColor = True
        '
        'Button55
        '
        Me.Button55.Location = New System.Drawing.Point(202, 164)
        Me.Button55.Name = "Button55"
        Me.Button55.Size = New System.Drawing.Size(123, 23)
        Me.Button55.TabIndex = 26
        Me.Button55.Tag = "DebtPlus.Reports.Client.PacketDMP.Disclosures.ClientGrievanceProcedures"
        Me.Button55.Text = "DMP Grievance Proc."
        Me.Button55.UseVisualStyleBackColor = True
        '
        'Button54
        '
        Me.Button54.Location = New System.Drawing.Point(202, 135)
        Me.Button54.Name = "Button54"
        Me.Button54.Size = New System.Drawing.Size(110, 23)
        Me.Button54.TabIndex = 25
        Me.Button54.Tag = "DebtPlus.Reports.Client.PacketDMP.Disclosures.Checklist"
        Me.Button54.Text = "DMP Checklist"
        Me.Button54.UseVisualStyleBackColor = True
        '
        'Button53
        '
        Me.Button53.Location = New System.Drawing.Point(202, 106)
        Me.Button53.Name = "Button53"
        Me.Button53.Size = New System.Drawing.Size(110, 23)
        Me.Button53.TabIndex = 24
        Me.Button53.Tag = "DebtPlus.Reports.Client.PacketDMP.CoverLetter"
        Me.Button53.Text = "DMP Cover Letter"
        Me.Button53.UseVisualStyleBackColor = True
        '
        'Button52
        '
        Me.Button52.Location = New System.Drawing.Point(202, 77)
        Me.Button52.Name = "Button52"
        Me.Button52.Size = New System.Drawing.Size(110, 23)
        Me.Button52.TabIndex = 23
        Me.Button52.Tag = "DebtPlus.Reports.Client.PacketDMP.Budget.BudgetOverview"
        Me.Button52.Text = "DMP Budget"
        Me.Button52.UseVisualStyleBackColor = True
        '
        'Button51
        '
        Me.Button51.Location = New System.Drawing.Point(202, 48)
        Me.Button51.Name = "Button51"
        Me.Button51.Size = New System.Drawing.Size(110, 23)
        Me.Button51.TabIndex = 22
        Me.Button51.Tag = "DebtPlus.Reports.Client.PacketDMP.ActionPlan"
        Me.Button51.Text = "DMP ActionPlan"
        Me.Button51.UseVisualStyleBackColor = True
        '
        'Button50
        '
        Me.Button50.Location = New System.Drawing.Point(96, 365)
        Me.Button50.Name = "Button50"
        Me.Button50.Size = New System.Drawing.Size(110, 23)
        Me.Button50.TabIndex = 21
        Me.Button50.Tag = "DebtPlus.Reports.Client.PayoutSummary"
        Me.Button50.Text = "Payout Summary"
        Me.Button50.UseVisualStyleBackColor = True
        '
        'Button49
        '
        Me.Button49.Location = New System.Drawing.Point(6, 367)
        Me.Button49.Name = "Button49"
        Me.Button49.Size = New System.Drawing.Size(85, 23)
        Me.Button49.TabIndex = 20
        Me.Button49.Tag = "DebtPlus.Reports.Client.Phone.ByDMPStatusDate"
        Me.Button49.Text = "Phone List"
        Me.Button49.UseVisualStyleBackColor = True
        '
        'Button48
        '
        Me.Button48.Location = New System.Drawing.Point(98, 338)
        Me.Button48.Name = "Button48"
        Me.Button48.Size = New System.Drawing.Size(85, 23)
        Me.Button48.TabIndex = 19
        Me.Button48.Tag = "DebtPlus.Reports.Client.StartDateByCounselor"
        Me.Button48.Text = "Start Date"
        Me.Button48.UseVisualStyleBackColor = True
        '
        'Button47
        '
        Me.Button47.Location = New System.Drawing.Point(6, 338)
        Me.Button47.Name = "Button47"
        Me.Button47.Size = New System.Drawing.Size(85, 23)
        Me.Button47.TabIndex = 18
        Me.Button47.Tag = "DebtPlus.Reports.Client.Statement.Monthly"
        Me.Button47.Text = "Monthly Stmt"
        Me.Button47.UseVisualStyleBackColor = True
        '
        'Button46
        '
        Me.Button46.Location = New System.Drawing.Point(97, 309)
        Me.Button46.Name = "Button46"
        Me.Button46.Size = New System.Drawing.Size(91, 23)
        Me.Button46.TabIndex = 17
        Me.Button46.Tag = "DebtPlus.Reports.Client.Statement.OtherDebts"
        Me.Button46.Text = "Other Debt Stmt"
        Me.Button46.UseVisualStyleBackColor = True
        '
        'Button45
        '
        Me.Button45.Location = New System.Drawing.Point(6, 309)
        Me.Button45.Name = "Button45"
        Me.Button45.Size = New System.Drawing.Size(85, 23)
        Me.Button45.TabIndex = 16
        Me.Button45.Tag = "DebtPlus.Reports.Client.Statement.Quarterly"
        Me.Button45.Text = "Quarterly Stmt"
        Me.Button45.UseVisualStyleBackColor = True
        '
        'Button44
        '
        Me.Button44.Location = New System.Drawing.Point(90, 280)
        Me.Button44.Name = "Button44"
        Me.Button44.Size = New System.Drawing.Size(78, 23)
        Me.Button44.TabIndex = 15
        Me.Button44.Tag = "DebtPlus.Reports.Client.Status"
        Me.Button44.Text = "Status"
        Me.Button44.UseVisualStyleBackColor = True
        '
        'Button43
        '
        Me.Button43.Location = New System.Drawing.Point(6, 280)
        Me.Button43.Name = "Button43"
        Me.Button43.Size = New System.Drawing.Size(78, 23)
        Me.Button43.TabIndex = 14
        Me.Button43.Tag = "DebtPlus.Reports.Client.TotalDebt"
        Me.Button43.Text = "Total Debt"
        Me.Button43.UseVisualStyleBackColor = True
        '
        'Button42
        '
        Me.Button42.Location = New System.Drawing.Point(87, 251)
        Me.Button42.Name = "Button42"
        Me.Button42.Size = New System.Drawing.Size(101, 23)
        Me.Button42.TabIndex = 13
        Me.Button42.Tag = "DebtPlus.Reports.Client.TrustActivity"
        Me.Button42.Text = "Trust Activity"
        Me.Button42.UseVisualStyleBackColor = True
        '
        'Button41
        '
        Me.Button41.Location = New System.Drawing.Point(6, 251)
        Me.Button41.Name = "Button41"
        Me.Button41.Size = New System.Drawing.Size(78, 23)
        Me.Button41.TabIndex = 12
        Me.Button41.Tag = "DebtPlus.Reports.Client.Notes"
        Me.Button41.Text = "Notes"
        Me.Button41.UseVisualStyleBackColor = True
        '
        'Button40
        '
        Me.Button40.Location = New System.Drawing.Point(110, 222)
        Me.Button40.Name = "Button40"
        Me.Button40.Size = New System.Drawing.Size(78, 23)
        Me.Button40.TabIndex = 11
        Me.Button40.Tag = "DebtPlus.Reports.Client.Labels"
        Me.Button40.Text = "Labels"
        Me.Button40.UseVisualStyleBackColor = True
        '
        'Button39
        '
        Me.Button39.Location = New System.Drawing.Point(6, 222)
        Me.Button39.Name = "Button39"
        Me.Button39.Size = New System.Drawing.Size(98, 23)
        Me.Button39.TabIndex = 10
        Me.Button39.Tag = "DebtPlus.Reports.Client.Deposit.Coupon"
        Me.Button39.Text = "Deposit Coupon"
        Me.Button39.UseVisualStyleBackColor = True
        '
        'Button38
        '
        Me.Button38.Location = New System.Drawing.Point(6, 193)
        Me.Button38.Name = "Button38"
        Me.Button38.Size = New System.Drawing.Size(177, 23)
        Me.Button38.TabIndex = 9
        Me.Button38.Tag = "DebtPlus.Reports.Client.CloseToPayoff"
        Me.Button38.Text = "Close To Payoff"
        Me.Button38.UseVisualStyleBackColor = True
        '
        'Button37
        '
        Me.Button37.Location = New System.Drawing.Point(6, 164)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(177, 23)
        Me.Button37.TabIndex = 8
        Me.Button37.Tag = "DebtPlus.Reports.Client.CloseToPayoff.Detail"
        Me.Button37.Text = "Close To Payoff by Detail"
        Me.Button37.UseVisualStyleBackColor = True
        '
        'Button36
        '
        Me.Button36.Location = New System.Drawing.Point(6, 135)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(177, 23)
        Me.Button36.TabIndex = 7
        Me.Button36.Tag = "DebtPlus.Reports.Client.CloseToPayoff.ByDebt"
        Me.Button36.Text = "Close To Payoff by Debt"
        Me.Button36.UseVisualStyleBackColor = True
        '
        'Button35
        '
        Me.Button35.Location = New System.Drawing.Point(97, 106)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(86, 23)
        Me.Button35.TabIndex = 6
        Me.Button35.Tag = "DebtPlus.Reports.Client.Budget"
        Me.Button35.Text = "Budget"
        Me.Button35.UseVisualStyleBackColor = True
        '
        'Button34
        '
        Me.Button34.Location = New System.Drawing.Point(6, 106)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(85, 23)
        Me.Button34.TabIndex = 5
        Me.Button34.Tag = "DebtPlus.Reports.Client.Appointments"
        Me.Button34.Text = "Appointments"
        Me.Button34.UseVisualStyleBackColor = True
        '
        'Button33
        '
        Me.Button33.Location = New System.Drawing.Point(87, 77)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(86, 23)
        Me.Button33.TabIndex = 4
        Me.Button33.Tag = "DebtPlus.Reports.Client.ActiveStatus"
        Me.Button33.Text = "Active Status"
        Me.Button33.UseVisualStyleBackColor = True
        '
        'Button32
        '
        Me.Button32.Location = New System.Drawing.Point(87, 19)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(75, 23)
        Me.Button32.TabIndex = 3
        Me.Button32.Tag = "DebtPlus.Reports.Bankruptcy.PreBankruptcy.ByAppt"
        Me.Button32.Text = "Bankruptcy"
        Me.Button32.UseVisualStyleBackColor = True
        '
        'Button31
        '
        Me.Button31.Location = New System.Drawing.Point(6, 77)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(75, 23)
        Me.Button31.TabIndex = 2
        Me.Button31.Tag = "DebtPlus.Reports.Client.ActionPlan"
        Me.Button31.Text = "Action Plan"
        Me.Button31.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(6, 48)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(123, 23)
        Me.Button15.TabIndex = 1
        Me.Button15.Tag = "DebtPlus.Reports.Housing.Transactions"
        Me.Button15.Text = "Housing Transactions"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(6, 19)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(75, 23)
        Me.Button14.TabIndex = 0
        Me.Button14.Tag = "DebtPlus.Reports.Client.Transactions"
        Me.Button14.Text = "Transactions"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.Button67)
        Me.GroupBox9.Location = New System.Drawing.Point(423, 453)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(200, 100)
        Me.GroupBox9.TabIndex = 4
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Disbursement"
        '
        'Button67
        '
        Me.Button67.Location = New System.Drawing.Point(6, 19)
        Me.Button67.Name = "Button67"
        Me.Button67.Size = New System.Drawing.Size(106, 23)
        Me.Button67.TabIndex = 0
        Me.Button67.Tag = "DebtPlus.Reports.Disbursement.DetailInformation.DetailInformationReport"
        Me.Button67.Text = "Detail Information"
        Me.Button67.UseVisualStyleBackColor = True
        '
        'Button71
        '
        Me.Button71.Location = New System.Drawing.Point(6, 47)
        Me.Button71.Name = "Button71"
        Me.Button71.Size = New System.Drawing.Size(142, 23)
        Me.Button71.TabIndex = 10
        Me.Button71.Tag = "DebtPlus.Reports.Trust.AccountBalance.ByState"
        Me.Button71.Text = "Account Balance by State"
        Me.Button71.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1000, 737)
        Me.Controls.Add(Me.GroupBox9)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "MainForm"
        Me.Text = "DebtPlus Reports Launcher"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Button29 As System.Windows.Forms.Button
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents Button32 As System.Windows.Forms.Button
    Friend WithEvents Button33 As System.Windows.Forms.Button
    Friend WithEvents Button34 As System.Windows.Forms.Button
    Friend WithEvents Button35 As System.Windows.Forms.Button
    Friend WithEvents Button36 As System.Windows.Forms.Button
    Friend WithEvents Button37 As System.Windows.Forms.Button
    Friend WithEvents Button38 As System.Windows.Forms.Button
    Friend WithEvents Button39 As System.Windows.Forms.Button
    Friend WithEvents Button40 As System.Windows.Forms.Button
    Friend WithEvents Button41 As System.Windows.Forms.Button
    Friend WithEvents Button42 As System.Windows.Forms.Button
    Friend WithEvents Button43 As System.Windows.Forms.Button
    Friend WithEvents Button44 As System.Windows.Forms.Button
    Friend WithEvents Button45 As System.Windows.Forms.Button
    Friend WithEvents Button46 As System.Windows.Forms.Button
    Friend WithEvents Button47 As System.Windows.Forms.Button
    Friend WithEvents Button48 As System.Windows.Forms.Button
    Friend WithEvents Button49 As System.Windows.Forms.Button
    Friend WithEvents Button50 As System.Windows.Forms.Button
    Friend WithEvents Button51 As System.Windows.Forms.Button
    Friend WithEvents Button52 As System.Windows.Forms.Button
    Friend WithEvents Button53 As System.Windows.Forms.Button
    Friend WithEvents Button54 As System.Windows.Forms.Button
    Friend WithEvents Button55 As System.Windows.Forms.Button
    Friend WithEvents Button56 As System.Windows.Forms.Button
    Friend WithEvents Button57 As System.Windows.Forms.Button
    Friend WithEvents Button58 As System.Windows.Forms.Button
    Friend WithEvents Button59 As System.Windows.Forms.Button
    Friend WithEvents Button60 As System.Windows.Forms.Button
    Friend WithEvents Button61 As System.Windows.Forms.Button
    Friend WithEvents Button62 As System.Windows.Forms.Button
    Friend WithEvents Button63 As System.Windows.Forms.Button
    Friend WithEvents Button64 As System.Windows.Forms.Button
    Friend WithEvents Button65 As System.Windows.Forms.Button
    Friend WithEvents Button66 As System.Windows.Forms.Button
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents Button67 As System.Windows.Forms.Button
    Friend WithEvents Button68 As System.Windows.Forms.Button
    Friend WithEvents Button69 As System.Windows.Forms.Button
    Friend WithEvents Button70 As System.Windows.Forms.Button
    Friend WithEvents Button71 As System.Windows.Forms.Button
End Class
