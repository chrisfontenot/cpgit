﻿using DebtPlus.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DebtPlus.Repository.Test
{
    [TestClass]
    public class LookupsTests
    {
        [TestInitialize]
        public void Initialize()
        {
            Setup.ConnectionString = Configuration.GetDatabaseInformation().ConnectionString;
        }

        [TestMethod]
        public void GetCompanyNameByConfiguration()
        {
            Assert.AreEqual(
                Lookups.GetCompanyName(),
                "ClearPoint Financial Solutions");
        }

        [TestMethod]
        public void GetMenusByConfiguration()
        {
            Assert.AreNotEqual(
                Lookups.GetMenus(),
                null);
        }
    }
}