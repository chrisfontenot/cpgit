﻿
namespace DebtPlus.Svc.Notes
{
    /// <summary>
    /// Values used in the Type field in the notes table.
    /// </summary>
    public enum NoteTypes
    {
        Permanent = 1,              // Permanent note
        Temporary = 2,              // Temporary, expires, note
        System = 3,                 // System note
        Alert = 4,                  // Alert note
        Disbursement = 5,           // Used for disbursements
        Research = 6,               // Used for creditor research
        HUD = 7                     // Client housing notes
    }
}
