using System;
using System.Data;
using System.IO;
using DebtPlus.Interfaces.Vendor;
using DebtPlus.Reports.CS.Template.Forms;
using DevExpress.XtraReports.UI;

namespace DebtPlus.Reports.CS.Vendor.ClientList
{
    public class ClientsByVendorListReport : DebtPlus.Reports.CS.Template.TemplateXtraReportClass, IVendor
    {
        private static readonly string namespaceValue = "DebtPlus.Reports.CS.Vendor.ClientList";
        public ClientsByVendorListReport() : base()
        {
            InitializeComponent();

            // Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();

            // Copy the script references to the subreports as well
            foreach (DevExpress.XtraReports.UI.Band Band in Bands)
            {
                foreach (var ctl in Band.Controls)
                {
                    var rpt = ctl as XRSubreport;
                    if (rpt != null)
                    {
                        var SubRpt = rpt.ReportSource as XtraReport;
                        if (SubRpt != null)
                        {
                            SubRpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();
                        }
                    }
                }
            }

            // Remove the parameter values
            Parameters["ParameterVendor"].Value = string.Empty;

            // Enable the select expert
            ReportFilter.IsEnabled = true;
        }

        private void InitializeComponent()
        {
            const string ReportName = "DebtPlus.Reports.Vendor.ClientList.repx";

            // See if there is a report reference that we can use
            string Fname = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName);
            bool UseDefault = true;
            try
            {
                if ((!DebtPlus.Configuration.Config.LocalReportsOnly) && System.IO.File.Exists(Fname))
                {
                    LoadLayout(Fname);
                    UseDefault = false;
                }
            }
            catch
            {
            }

            // Load the standard report definition if we need a new item
            if (UseDefault)
            {
                var asm = System.Reflection.Assembly.GetExecutingAssembly();
                string manifestName = string.Format("{0}.{1}", namespaceValue, ReportName);
                using (Stream ios = asm.GetManifestResourceStream(manifestName))
                {
                    if (ios != null)
                    {
                        LoadLayout(ios);
                    }
                }
            }
        }

        /// <summary>
        ///     Report title information
        /// </summary>
        public override string ReportTitle
        {
            get
            {
                return "Clients By Vendor";
            }
        }

        /// <summary>
        ///     Report subtitle information
        /// </summary>
        public override string ReportSubTitle
        {
            get
            {
                // No vendor is no name
                if (!Vendor.HasValue)
                {
                    return string.Empty;
                }

                // Look for the vendor in the cached name list
                var vue = DataSource as System.Data.DataView;
                if (vue != null)
                {
                    var ds = (System.Data.DataSet)vue.Table.DataSet;
                    var tbl = ds.Tables["vendors"];
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        System.Data.DataRow row = tbl.Rows[0];
                        string VendorName = (DebtPlus.Utils.Nulls.v_String(row["Name"]) ?? string.Empty).Trim();
                        string vendorID = (DebtPlus.Utils.Nulls.v_String(row["vendor"]) ?? string.Empty).Trim();
                        if (!string.IsNullOrWhiteSpace(VendorName) && !string.IsNullOrWhiteSpace(vendorID))
                        {
                            return string.Format("[{0}] {1}", vendorID, VendorName);
                        }
                    }
                }

                // Use the vendor ID key as the name
                return Vendor.Value.ToString();
            }
        }

        /// <summary>
        /// Vendor ID
        /// </summary>
        public System.Nullable<int> Vendor
        {
            get
            {
                return Parameter_Vendor;
            }
            set
            {
                Parameter_Vendor = value;
            }
        }

        /// <summary>
        ///     Parameter for the vendor ID
        /// </summary>
        public System.Nullable<int> Parameter_Vendor
        {
            get
            {
                // Find the vendor parameter. If none then there is no value.
                var parm = FindParameter("ParameterVendor");
                if (parm == null)
                {
                    return (System.Nullable<int>)null;
                }

                // Look for the marker indicating that the parameter is not given
                int vendorID = System.Convert.ToInt32(parm.Value);
                if (vendorID < 1)
                {
                    return (System.Nullable<int>)null;
                }

                // Otherwise, use the entered value.
                return vendorID;
            }
            set
            {
                SetParameter("ParameterVendor", typeof(int), value.Value, "Vendor ID", false);
            }
        }

        /// <summary>
        ///     Do we need to request parameters?
        /// </summary>
        public override bool NeedParameters()
        {
            return base.NeedParameters() || Parameter_Vendor < 0;
        }

        /// <summary>
        ///     Change the parameter value for this report
        /// </summary>
        public override void SetReportParameter(string Parameter, object Value)
        {
            switch (Parameter)
            {
                case "Vendor":
                    Parameter_Vendor = System.Convert.ToInt32(Value);
                    break;

                default:
                    base.SetReportParameter(Parameter, Value);
                    break;
            }
        }

        /// <summary>
        ///     Request the parameters for the report from the user
        /// </summary>
        public override System.Windows.Forms.DialogResult RequestReportParameters()
        {
            var Answer = System.Windows.Forms.DialogResult.OK;
            if (NeedParameters())
            {
                using (var frm = new DebtPlus.Reports.CS.Template.Forms.VendorParametersForm())
                {
                    Answer = frm.ShowDialog();
                    Parameter_Vendor = frm.Parameter_Vendor;
                }
            }
            return Answer;
        }
    }
}
