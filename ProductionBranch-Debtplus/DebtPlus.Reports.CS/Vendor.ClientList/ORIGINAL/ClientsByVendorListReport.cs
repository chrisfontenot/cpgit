namespace DebtPlus.Reports.CS.Vendor.ClientList
{
    public partial class ClientsByVendorListReport : DebtPlus.Reports.CS.Template.TemplateXtraReportClass, DebtPlus.Interfaces.Vendor.IVendor
    {
        /// <summary>
        /// Create an instance of our report
        /// </summary>
        public ClientsByVendorListReport() : base()
        {
            InitializeComponent();

            // Add handler routines
            BeforePrint += ClientsByVendorListReport_BeforePrint;

            // Default the parameters
            ParameterVendor.Value = string.Empty;

            // Enable the select expert
            ReportFilter.IsEnabled = true;
        }

        /// <summary>
        /// Report title information
        /// </summary>
        public override string ReportTitle
        {
            get
            {
                return "Clients By Vendor";
            }
        }

        /// <summary>
        /// Report subtitle information
        /// </summary>
        public override string ReportSubTitle
        {
            get
            {
                // No vendor is no name
                if (!Vendor.HasValue)
                {
                    return string.Empty;
                }

                // Look for the vendor in the cached name list
                var vue = DataSource as System.Data.DataView;
                if (vue != null)
                {
                    var ds = (System.Data.DataSet)vue.Table.DataSet;
                    var tbl = ds.Tables["vendors"];
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        System.Data.DataRow row = tbl.Rows[0];
                        string VendorName = (DebtPlus.Utils.Nulls.v_String(row["Name"]) ?? string.Empty).Trim();
                        string vendorID = (DebtPlus.Utils.Nulls.v_String(row["vendor"]) ?? string.Empty).Trim();
                        if (!string.IsNullOrWhiteSpace(VendorName) && !string.IsNullOrWhiteSpace(vendorID))
                        {
                            return string.Format("[{0}] {1}", vendorID, VendorName);
                        }
                    }
                }

                // Use the vendor ID key as the name
                return Vendor.Value.ToString();
            }
        }

        /// <summary>
        /// Vendor ID
        /// </summary>
        public System.Nullable<int> Vendor
        {
            get
            {
                return Parameter_Vendor;
            }
            set
            {
                Parameter_Vendor = value;
            }
        }

        /// <summary>
        /// Parameter for the vendor ID
        /// </summary>
        public System.Nullable<int> Parameter_Vendor
        {
            get
            {
                // Find the vendor parameter. If none then there is no value.
                var parm = FindParameter("ParameterVendor");
                if (parm == null)
                {
                    return (System.Nullable<int>)null;
                }

                // Look for the marker indicating that the parameter is not given
                int vendorID = System.Convert.ToInt32(parm.Value);
                if (vendorID < 1)
                {
                    return (System.Nullable<int>)null;
                }

                // Otherwise, use the entered value.
                return vendorID;
            }
            set
            {
                SetParameter("ParameterVendor", typeof(int), value.Value, "Vendor ID", false);
            }
        }

        /// <summary>
        /// Do we need to request parameters?
        /// </summary>
        public override bool NeedParameters()
        {
            return base.NeedParameters() || Parameter_Vendor < 0;
        }

        /// <summary>
        /// Change the parameter value for this report
        /// </summary>
        public override void SetReportParameter(string Parameter, object Value)
        {
            switch (Parameter)
            {
                case "Vendor":
                    Parameter_Vendor = System.Convert.ToInt32(Value);
                    break;

                default:
                    base.SetReportParameter(Parameter, Value);
                    break;
            }
        }

        /// <summary>
        /// Request the parameters for the report from the user
        /// </summary>
        public override System.Windows.Forms.DialogResult RequestReportParameters()
        {
            var Answer = System.Windows.Forms.DialogResult.OK;
            if (NeedParameters())
            {
                using (var frm = new DebtPlus.Reports.CS.Template.Forms.VendorParametersForm())
                {
                    Answer = frm.ShowDialog();
                    Parameter_Vendor = frm.Parameter_Vendor;
                }
            }
            return Answer;
        }

        /// <summary>
        /// Build the report dataset
        /// </summary>
        System.Data.DataSet ds = new System.Data.DataSet("ds");
        private void ClientsByVendorListReport_BeforePrint(object sender, System.EventArgs e)
        {
            const string TableName = "view_clients_by_vendor";
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)sender;
            System.Data.DataTable tbl = ds.Tables[TableName];

            if (tbl == null)
            {
                var sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                try
                {
                    using (var cn = new System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString))
                    {
                        cn.Open();

                        using (var cmd = new System.Data.SqlClient.SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT [label] as vendor, name FROM vendors WHERE oID=@vendor";
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.Parameters.Add("@vendor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters["ParameterVendor"].Value;

                            using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, "vendors");
                            }
                        }

                        using (var cmd = new System.Data.SqlClient.SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT [last_payment_date],[last_payment_amount],[client],[name],[status],[product],[orig_balance],[principle_payments],[current_balance] FROM view_clients_by_vendor WITH (NOLOCK) WHERE [vendor]=@vendor ORDER BY [client]";
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.Parameters.Add("@vendor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters["ParameterVendor"].Value;
                            cmd.CommandTimeout = 0;

                            using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, TableName);
                                tbl = ds.Tables[TableName];
                            }
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading information for report");
                }
            }

            if (tbl != null)
            {
                rpt.DataSource = new System.Data.DataView(tbl, ((DebtPlus.Interfaces.Reports.IReportFilter)rpt).ReportFilter.ViewFilter, string.Empty, System.Data.DataViewRowState.CurrentRows);
            }
        }
    }
}
