using System;
using DevExpress.XtraBars;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.Parameters;

namespace DebtPlus.Reports.CS.Template
{
    public partial class PrintPreviewForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Name of the key in the registry for saving the location/size of the form
        /// </summary>
        private const string ReportPlacementName = "Reports";

        /// <summary>
        /// Current printing subsystem
        /// </summary>
        private PrintingSystemBase PrintingSystem;

        bool DocumentCreated;

        /// <summary>
        /// Pointer to the report currently loaded
        /// </summary>
        protected BaseXtraReportClass Report { get; set; }

        /// <summary>
        /// Return the status for the report filter
        /// </summary>
        private bool SupportFilter
        {
            get
            {
                if (Report == null)
                {
                    return false;
                }

                if (!(Report is DebtPlus.Interfaces.Reports.IReportFilter))
                {
                    return false;
                }

                return ((DebtPlus.Interfaces.Reports.IReportFilter)Report).ReportFilter.IsEnabled;
            }
        }

        /// <summary>
        /// Event to process the Customize Report function
        /// </summary>
        public event EventHandler CustomizeReport;
        protected virtual void OnCustomizeReport(EventArgs e)
        {
            if (CustomizeReport != null)
            {
                CustomizeReport(this, e);
            }
        }

        /// <summary>
        /// Raise the CustomizeReport event
        /// </summary>
        protected void RaiseCustomizeReport()
        {
            OnCustomizeReport(EventArgs.Empty);
        }

        public PrintPreviewForm(BaseXtraReportClass rpt)
            : base()
        {
            Report = rpt;
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Timer1.Tick                            += Timer1_Tick;
            Load                                   += Form_Load;
            BarButtonItemCustomizeReport.ItemClick += BarButtonItemCustomizeReport_ItemClick;
            BarButtonItem_Filter.ItemClick         += BarButtonItem_Filter_ItemClick;
            BarButtonItem2.ItemClick               += BarButtonItem2_ItemClick;

            if (PrintingSystem != null)
            {
                PrintingSystem.AfterBuildPages     += PrintingSystem_AfterBuildPages;
                PrintingSystem.BeforeBuildPages    += PrintingSystem_BeforeBuildPages;
            }
        }

        private void UnRegisterHandlers()
        {
            Timer1.Tick                            -= Timer1_Tick;
            Load                                   -= Form_Load;
            BarButtonItemCustomizeReport.ItemClick -= BarButtonItemCustomizeReport_ItemClick;
            BarButtonItem_Filter.ItemClick         -= BarButtonItem_Filter_ItemClick;
            BarButtonItem2.ItemClick               -= BarButtonItem2_ItemClick;

            if (PrintingSystem != null)
            {
                PrintingSystem.AfterBuildPages     -= PrintingSystem_AfterBuildPages;
                PrintingSystem.BeforeBuildPages    -= PrintingSystem_BeforeBuildPages;
            }
        }

        /// <summary>
        /// Handle the tick of the timer
        /// </summary>
        private void Timer1_Tick(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Do this the first time that the report is being displayed.
                if (!DocumentCreated)
                {
                    DocumentCreated = true;
                    Report.CreateDocument(true);
                    Report.PrintingSystem.ShowMarginsWarning = false;
                }

                Timer1.Enabled = false;
            }

            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating report document");
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the load event for the preview form
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                // Do not embed PDF fonts
                //
                // We need to set this before we show the PDF dialog so do it now. If you really want to
                // override this and export the PDF document yourself, then just set the NeverEmbeddedFonts
                // string to an empty string.
                Report.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList();
            }

            catch
            {
            }

            try
            {
                // Restore the form placement
                LoadPlacement(ReportPlacementName);

                // Use it for our text as well
                if (Report.ReportTitle != string.Empty)
                {
                    Text = string.Format("Preview - {0}", Report.ReportTitle);
                }
                else
                {
                    Text = "Preview - Report";
                }

                // Connect to the report
                PrintingSystem = Report.PrintingSystem;
                PrintControl1.PrintingSystem = PrintingSystem;

                // The report should not show the parameter form unless requested by the report.
                if (!Report.RequestParameters)
                {
                    for (Int32 ItemNo = Report.Parameters.Count - 1; ItemNo >= 0; ItemNo += -1)
                    {
                        Parameter parm = Report.Parameters[ItemNo];
                        parm.Visible = false;
                    }
                }

                // Enable the select expert if the report supports the filtering
                BarButtonItem_Filter.Enabled = SupportFilter;
            }

            finally
            {
                RegisterHandlers();
            }

            // Start a timer to trip a callback when the form is processed and painted
            if (Report.PrintingSystem == null || Report.PrintingSystem.Document.PageCount == 0)
            {
                Timer1.Interval = 10;
                Timer1.Enabled = true;
            }
        }

        /// <summary>
        /// Process the click on the Customize button
        /// </summary>
        private void BarButtonItemCustomizeReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            RaiseCustomizeReport();
        }

        /// <summary>
        /// Handle the condition once the report has been built
        /// </summary>

        private void PrintingSystem_AfterBuildPages(object sender, EventArgs e)
        {
            // Hide the accessing information
            BarStaticItemAccessingDatabase.Visibility = BarItemVisibility.Never;

            // Set the name of the document for export
            if (Report.ReportTitle != string.Empty)
            {
                PrintingSystem.Document.Name = Report.ReportTitle;
            }
            else
            {
                PrintingSystem.Document.Name = "DebtPlus Report";
            }
        }

        /// <summary>
        /// Handle the condition before building the report
        /// </summary>
        private void PrintingSystem_BeforeBuildPages(object sender, EventArgs e)
        {
            BarStaticItemAccessingDatabase.Visibility = BarItemVisibility.Always;
        }

        /// <summary>
        /// Process the click event on the Select Expert button
        /// </summary>
        private void BarButtonItem_Filter_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!SupportFilter)
            {
                return;
            }

            UnRegisterHandlers();
            try
            {
                using (var frm = new SelectExpertForm(Report, ((DebtPlus.Interfaces.Reports.IReportFilter)Report).ReportFilter.CriteriaFilter))
                {
                    if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        ((DebtPlus.Interfaces.Reports.IReportFilter)Report).ReportFilter.CriteriaFilter = frm.CriteriaFilter;
                        Report.CreateDocument();
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the menu pick to upload the file to an FTP server
        /// </summary>
        private void BarButtonItem2_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var parm = new DebtPlus.Reports.CS.Template.Forms.SFTPUloadParameers();

            // Read the current structure from the saved parameter file
            string savePath = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData), "DebtPlus");
            string saveFile = "DebtPlus.Reports.Template.PrintPreviewForm.FTP.xml";
            string fname = System.IO.Path.Combine(savePath, saveFile);

            try
            {
                if (System.IO.File.Exists(fname))
                {
                    using (var fs = System.IO.File.OpenRead(fname))
                    {
                        var ser = new System.Xml.Serialization.XmlSerializer(typeof(DebtPlus.Reports.CS.Template.Forms.SFTPUloadParameers), string.Empty);
                        parm = ser.Deserialize(fs) as DebtPlus.Reports.CS.Template.Forms.SFTPUloadParameers;
                    }
                }
            }
            catch
            {
            }

            // The file name is the only real variable here. So ensure that it is consistent.
            parm.FileName = string.Empty;

            // Request the parameters for the upload event
            try
            {
                using (DebtPlus.Reports.CS.Template.Forms.SFTPUploadForm frmUpload = new DebtPlus.Reports.CS.Template.Forms.SFTPUploadForm(parm))
                {
                    frmUpload.PerformUpload += sftpUpload;
                    frmUpload.ShowDialog();
                    frmUpload.PerformUpload -= sftpUpload;
                }

                // Save the parameters that were given to the resource for the next operation
                if (!System.IO.Directory.Exists(savePath))
                {
                    System.IO.Directory.CreateDirectory(savePath);
                }

                using (var fs = System.IO.File.OpenWrite(fname))
                {
                    var ser = new System.Xml.Serialization.XmlSerializer(typeof(DebtPlus.Reports.CS.Template.Forms.SFTPUloadParameers), string.Empty);
                    ser.Serialize(fs, parm);
                }
            }

            catch (System.Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error doing the operation request");
            }
        }

        /// <summary>
        /// Upload the file
        /// </summary>
        private void sftpUpload(object sender, DebtPlus.Reports.CS.Template.Forms.PerformUploadEventArgs e)
        {
            // Render the document as a PDF file
            string fileName = System.IO.Path.GetTempFileName();
            try
            {
                using (System.IO.FileStream outStream = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    Report.CreatePdfDocument(outStream);
                    outStream.Flush();
                    outStream.Close();
                }

                // Perform the upload operation
                e.Cancel = (bool)uploadFTP(e.Parameters, fileName);
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error sending data to FTP site");
                e.Cancel = true;
            }
            finally
            {
                DeleteTempoaryFile(fileName);
            }
        }

        /// <summary>
        /// Delete the temporary file from the system. Since it is temporary and we are on the way
        /// out, we don't really care about errors at this point. Just ignore them.
        /// </summary>
        private void DeleteTempoaryFile(string FileName)
        {
            try
            {
                // Remove the temporary file from the system. Ignore errors.
                System.IO.File.Delete(FileName);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Upload the file with SFTP protocol
        /// </summary>
        private object uploadSFTP(DebtPlus.Reports.CS.Template.Forms.SFTPUloadParameers param, string fileName)
        {
            Int32 port = 0;
            string host = string.Empty;

            // Split the port from the server designation
            System.Text.RegularExpressions.Regex exp = new System.Text.RegularExpressions.Regex("^([^:]+):(\\d+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
            System.Text.RegularExpressions.Match m   = exp.Match(param.Site);
            if (m.Success)
            {
                string pStr = m.Groups[2].Captures[0].Value;
                port        = System.Int32.Parse(pStr);
                host        = m.Groups[1].Captures[0].Value;
            }
            else
            {
                port = 22;
                host = param.Site;
            }

            // Create the FTP client to the host
            var ftpClient = new Tamir.SharpSsh.Sftp(host, param.UserName, param.Password);

            // Send the file to the remote host
            ftpClient.Connect(port);
            string targetName = System.IO.Path.Combine(param.Directory.TrimEnd('/') + "/", param.FileName);
            ftpClient.Put(fileName, targetName);

            // Return the valid status code.
            return false;
        }

        /// <summary>
        /// Upload the file with FTP protocol
        /// </summary>
        private object uploadFTP(DebtPlus.Reports.CS.Template.Forms.SFTPUloadParameers param, string fileName)
        {
            Int32 port = 0;
            string host = string.Empty;

            // Split the port from the server designation
            System.Text.RegularExpressions.Regex exp = new System.Text.RegularExpressions.Regex("^([^:]+):(\\d+)$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
            System.Text.RegularExpressions.Match m   = exp.Match(param.Site);
            if (m.Success)
            {
                string pStr = m.Groups[2].Captures[0].Value;
                port        = System.Int32.Parse(pStr);
                host        = m.Groups[1].Captures[0].Value;
            }
            else
            {
                port = 21;
                host = param.Site;
            }

            // Regenerate the base string to include the fields that we need to specify
            string newFileName = string.Format("{0}{1}:{2:f0}/{3}", System.Uri.UriSchemeFtp, host, port, param.Directory.TrimEnd('/') + "/" + param.FileName);

            // Get the object used to communicate with the server.
            System.Net.FtpWebRequest request = (System.Net.FtpWebRequest)System.Net.WebRequest.Create(newFileName);
            request.Method                   = System.Net.WebRequestMethods.Ftp.UploadFile;
            request.EnableSsl                = false;
            request.UseBinary                = true;
            request.UsePassive               = true;

            // Use the credentials if they are given. If not, use anonymous.
            if (!string.IsNullOrWhiteSpace(param.UserName))
            {
                request.Credentials = new System.Net.NetworkCredential(param.UserName, param.Password);
            }
            else
            {
                request.Credentials = new System.Net.NetworkCredential("anonymous", "DoNotReply@clearpointccs.org");
            }

            // Transmit the binary data to the FTP server
            byte[] fileContents = null;
            using (System.IO.StreamReader ifs = new System.IO.StreamReader(fileName, System.Text.Encoding.UTF8))
            {
                fileContents = System.Text.Encoding.UTF8.GetBytes(ifs.ReadToEnd());
            }

            request.ContentLength          = fileContents.Length;
            System.IO.Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            // Obtain the response from the server to determine the validity.
            System.Net.FtpWebResponse response = (System.Net.FtpWebResponse)request.GetResponse();

            // If successful, return the valid status to complete the operation.
            if (response.StatusCode == System.Net.FtpStatusCode.CommandOK)
            {
                return false;
            }

            // Otherwise display the transfer error condition
            if (!string.IsNullOrWhiteSpace(response.StatusDescription))
            {
                DebtPlus.Data.Forms.MessageBox.Show(response.StatusDescription, "Transfer Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return true;
            }

            // Something strange happened
            DebtPlus.Data.Forms.MessageBox.Show(string.Format("Transfer failed with response code: {0:f0}", response.StatusCode), "Transfer Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            return true;
        }
    }
}
