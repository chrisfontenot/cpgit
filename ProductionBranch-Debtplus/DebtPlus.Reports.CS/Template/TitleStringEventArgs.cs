using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Reports.CS.Template
{
	/// <summary>
	/// Class to handle the event arguments for reading the title and subtitle
	/// </summary>
	public sealed class TitleStringEventArgs : EventArgs
	{
		/// <summary>
		/// The string associated with the title.
		/// </summary>
		public string Title { get; set; }
	}
}
