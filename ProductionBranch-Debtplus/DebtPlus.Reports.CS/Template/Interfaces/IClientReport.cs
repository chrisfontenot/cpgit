using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.LINQ;
using System.Linq;
using DebtPlus.UI.Common;
using DebtPlus.Data.Forms;
using DevExpress.XtraEditors;
using DebtPlus.UI.Client.Widgets.Controls;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DebtPlus.Utils.Format;

namespace DebtPlus.Reports.CS.Template.Interfaces
{
	public interface IClientReport
	{
		Int32 Client { get; set; }
	}
}
