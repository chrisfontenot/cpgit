using System;
using System.ComponentModel;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Threading;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;

namespace DebtPlus.Reports.CS.Template
{
    /// <summary>
    /// A base class for all report objects.
    /// </summary>
    public partial class BaseXtraReportClass : DevExpress.XtraReports.UI.XtraReport, DebtPlus.Interfaces.Reports.IReportFilter, DebtPlus.Interfaces.Reports.IReports
    {
        public BaseXtraReportClass()
            : base()
        {
            ReportFilter = new DebtPlus.Interfaces.Reports.ReportFilterItems();
            AllowParameterChangesByUser = false;

            InitializeComponent();
            Margins = new System.Drawing.Printing.Margins(25, 25, 25, 25);
        }

        public string ReportName = "No Name Yet";

        public bool UsingDefaultLayout = true;

        protected TopMarginBand TopMarginBand1;
        protected DetailBand Detail;

        protected BottomMarginBand BottomMarginBand1;

        private void InitializeComponent()
        {
            TopMarginBand1 = new TopMarginBand();
            Detail = new DetailBand();
            BottomMarginBand1 = new BottomMarginBand();
            ((ISupportInitialize)this).BeginInit();
            //
            //TopMarginBand1
            //
            TopMarginBand1.HeightF = 25f;
            TopMarginBand1.Name = "TopMarginBand1";
            //
            //Detail
            //
            Detail.Name = "Detail";
            //
            //BottomMarginBand1
            //
            BottomMarginBand1.HeightF = 25f;
            BottomMarginBand1.Name = "BottomMarginBand1";
            //
            //BaseXtraReportClass
            //
            Bands.AddRange(new Band[] {
				TopMarginBand1,
				Detail,
				BottomMarginBand1
			});
            Margins = new Margins(25, 25, 25, 25);
            Version = "11.2";
            ((ISupportInitialize)this).EndInit();
        }

        /// <summary>
        /// Setup the report
        /// </summary>
        private void InitializeReport()
        {
            // Ensure that the parameters are all hidden.
            foreach (DevExpress.XtraReports.Parameters.Parameter parm in this.Parameters)
            {
                parm.Visible = false;
            }

            PrintingSystem.Document.Name = string.IsNullOrEmpty(ReportTitle) ? "DebtPlus Report" : ReportTitle;
        }

        /// <summary>
        /// Hook into the routine called before the report is generated. Do any initialization
        /// that is needed for the report at this time.
        /// </summary>
        protected override void BeforeReportPrint()
        {
            InitializeReport();
            base.BeforeReportPrint();
        }

        /// <summary>
        /// Do we need to show the parameters dialog?
        /// </summary>
        /// <returns>TRUE if the dialog should be shown</returns>
        public virtual bool NeedParameters()
        {
            return false;
        }

        /// <summary>
        /// Locate the parameter in the list by name
        /// </summary>
        protected Parameter FindParameter(string ParameterName)
        {
            return Parameters.AsQueryable().OfType<DevExpress.XtraReports.Parameters.Parameter>().Where(s => string.Compare(s.Name, ParameterName) == 0).FirstOrDefault();
        }

        /// <summary>
        /// Set the parameter for the report
        /// </summary>
        protected void SetParameter(string pname, System.Type type, object value, string description, bool canSee)
        {
            if (string.IsNullOrWhiteSpace(pname))
            {
                throw new ArgumentNullException();
            }

            if (Parameters == null)
            {
                throw new System.ArgumentException("Parameters must not be null");
            }

            // Find the parameter in the list of parameters
            var param = FindParameter(pname);

            // If the parameter is not found, add it
            if (param == null)
            {
                param = new Parameter();
                param.Name = pname;
                param.Type = type;
                param.Description = description;
                param.Visible = canSee;
                Parameters.Add(param);
            }

            // Update the value for the parameter
            param.Value = value;
        }

        /// <summary>
        /// Handle the customize report event from the preview
        /// </summary>
        protected void CustomizeReportEvent(object Sender, System.EventArgs e)
        {
            try
            {
                ShowDesignerDialog();
            }
            catch (System.Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error customizing report");
            }
        }

        /// <summary>
        /// Event raised when the report wants the title. This is not cached. It is raised
        /// with every request for the title string.
        /// </summary>
        public event TitleStringEventHandler GetTitleString;

        /// <summary>
        /// Raise the GetTitleString event
        /// </summary>
        /// <param name="e">parameters to the event</param>
        protected void RaiseReportTitle(TitleStringEventArgs e)
        {
            if (GetTitleString != null)
            {
                GetTitleString(this, e);
            }
        }

        /// <summary>
        /// Linkage to the GetTitleString event. This may be overridden.
        /// </summary>
        /// <param name="e">parameters to the event</param>
        protected virtual void OnReportTitle(TitleStringEventArgs e)
        {
            RaiseReportTitle(e);
        }

        /// <summary>
        /// Title associated with the report. This is on the report, the PreviewDialog, and the report printer document name
        /// </summary>
        public virtual string ReportTitle
        {
            get
            {
                var e = new TitleStringEventArgs();
                OnReportTitle(e);
                return e.Title;
            }
        }

        /// <summary>
        /// Event raised when the report wants the title. This is not cached. It is raised
        /// with every request for the title string.
        /// </summary>
        public event TitleStringEventHandler GetSubTitleString;

        /// <summary>
        /// Raise the GetSubTitleString event
        /// </summary>
        /// <param name="e">parameters to the event</param>
        protected void RaiseReportSubTitle(TitleStringEventArgs e)
        {
            if (GetSubTitleString != null)
            {
                GetSubTitleString(this, e);
            }
        }

        /// <summary>
        /// Linkage to the GetSubTitleString event. This may be overridden.
        /// </summary>
        /// <param name="e">parameters to the event</param>
        protected virtual void OnReportSubTitle(TitleStringEventArgs e)
        {
            RaiseReportSubTitle(e);
        }

        /// <summary>
        /// SubTitle associated with the report. This is on the report, the PreviewDialog, and the report printer document name
        /// </summary>
        public virtual string ReportSubTitle
        {
            get
            {
                var e = new TitleStringEventArgs();
                OnReportSubTitle(e);
                return e.Title;
            }
        }

        /// <summary>
        /// Do we allow the parameters to be changed by the user or not?
        /// </summary>
        public bool AllowParameterChangesByUser { get; set; }

        /// <summary>
        /// Routine to externally set a report parameter by name
        /// </summary>
        /// <param name="parameterName">Name of the parameter to be modified</param>
        /// <param name="value">Value to be used for the parameter</param>
        /// <remarks></remarks>
        public virtual void SetReportParameter(string parameterName, object value)
        {
            // Do nothing
        }

        /// <summary>
        /// Request the parameters for the report, should always be called by report requesting code
        /// </summary>
        public virtual System.Windows.Forms.DialogResult RequestReportParameters()
        {
            return System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Return the dictionary of the current parameter arguments.
        /// </summary>
        /// <returns></returns>
        public virtual System.Collections.Generic.Dictionary<string, string> ParametersDictionary()
        {
            return Parameters.AsQueryable().OfType<DevExpress.XtraReports.Parameters.Parameter>().ToDictionary(p => p.Name, p => p.Value.ToString());
        }

        /// <summary>
        /// Thread procedure to run the report.
        /// </summary>
        public void RunReport()
        {
            InitializeReport();

            // Ask for the parameters and if OK, show the preview dialog.
            if (RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
            {
                DisplayPreviewDialog();
            }
        }

        /// <summary>
        /// Start a separate thread to run the report.
        /// </summary>
        public void RunReportInSeparateThread()
        {
            var thrd = new Thread(RunReport);
            thrd.SetApartmentState(ApartmentState.STA);
            thrd.IsBackground = true;
            thrd.Name = Guid.NewGuid().ToString();
            thrd.Start();
        }

        /// <summary>
        /// Display the PageSetup dialog.
        /// </summary>
        public System.Windows.Forms.DialogResult DisplayPageSetupDialog()
        {
            return this.ShowPageSetupDialog();
        }

        /// <summary>
        /// Show the preview of the report
        /// </summary>
        public virtual void DisplayPreviewDialog()
        {
            using (var frm = new PrintPreviewForm(this))
            {
                frm.CustomizeReport += CustomizeReportEvent;
                frm.ShowDialog();
                frm.CustomizeReport -= CustomizeReportEvent;
            }
        }

        /// <summary>
        /// Show the preview of the report
        /// </summary>
        public virtual void DisplayPreviewDialog(DevExpress.LookAndFeel.UserLookAndFeel lookAndFeel)
        {
            using (var frm = new PrintPreviewForm(this))
            {
                frm.LookAndFeel.Assign(lookAndFeel);
                frm.CustomizeReport += CustomizeReportEvent;
                frm.ShowDialog();
                frm.CustomizeReport -= CustomizeReportEvent;
            }
        }

        /// <summary>
        /// Print the report document.
        /// </summary>
        public void PrintDocument()
        {
            this.Print();
        }

        /// <summary>
        /// Print the report document.
        /// </summary>
        public void PrintDocument(string printerName)
        {
            this.Print(printerName);
        }

        /// <summary>
        /// Create an RTF output file to a disk file
        /// </summary>
        public void CreateRtfDocument(string Path)
        {
            this.ExportToRtf(Path);
        }

        /// <summary>
        /// Create an RTF output file to a stream
        /// </summary>
        public void CreateRtfDocument(Stream Stream)
        {
            this.ExportToRtf(Stream);
        }

        /// <summary>
        /// Create an PDF output file to a disk file
        /// </summary>
        public void CreatePdfDocument(string Path)
        {
            this.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList();
            this.ExportToPdf(Path);
        }

        /// <summary>
        /// Create an PDF output file to a stream
        /// </summary>
        public void CreatePdfDocument(Stream Stream)
        {
            this.ExportOptions.Pdf.NeverEmbeddedFonts = DebtPlus.Data.Fonts.GetExcludeList();
            this.ExportToPdf(Stream);
        }

        /// <summary>
        /// Dispose of the current report object
        /// </summary>

        protected override void OnDisposing()
        {
            try
            {
                // Clean up the report filter item storage
                if (ReportFilter != null)
                {
                    ReportFilter.Dispose();
                    ReportFilter = null;
                }
            }
            finally
            {
                // Do the standard cleanup on the report at this point.
                base.OnDisposing();
            }
        }

        public virtual DebtPlus.Interfaces.Reports.ReportFilterItems ReportFilter { get; set; }

    }
}