using System;
using DebtPlus.Reports.CS.Template.Forms;

namespace DebtPlus.Reports.CS.Template.Packets
{
    public partial class DatedTemplateXtraReportClass : TemplateXtraReportClass
    {
        protected DebtPlus.Utils.DateRange ReportDateRange = DefaultDateRange;
        protected bool FromDateDefined = false;

        protected bool ToDateDefined = false;

        /// <summary>
        /// Default to be used for the date range if one is not specified
        /// </summary>
        protected static DebtPlus.Utils.DateRange DefaultDateRange
        {
            get { return DebtPlus.Utils.DateRange.Today; }
        }

        public DatedTemplateXtraReportClass(DebtPlus.Utils.DateRange dateRange)
            : this()
        {
            ReportDateRange = dateRange;
        }

        public DatedTemplateXtraReportClass()
            : base()
        {
            InitializeComponent();

            // Initialize the date as not having been set
            ReportDateRange = DefaultDateRange;
            Parameter_FromDate = DateTime.Now.Date;
            Parameter_ToDate = Parameter_FromDate;
            FromDateDefined = false;
            ToDateDefined = false;
        }

        /// <summary>
        /// Define the starting date for the date range
        /// </summary>
        protected internal DateTime Parameter_FromDate
        {
            get
            {
                DevExpress.XtraReports.Parameters.Parameter parm = FindParameter("ParameterFromDate");
                if (parm == null || parm.Value == null)
                    return DateTime.MinValue;
                return Convert.ToDateTime(parm.Value);
            }
            set
            {
                SetParameter("ParameterFromDate", typeof(DateTime), value, "Starting Date", false);
                FromDateDefined = true;
            }
        }

        /// <summary>
        /// Define the ending date for the date range
        /// </summary>
        protected DateTime Parameter_ToDate
        {
            get
            {
                DevExpress.XtraReports.Parameters.Parameter parm = FindParameter("ParameterToDate");
                if (parm == null)
                    return DateTime.MaxValue;
                return Convert.ToDateTime(parm.Value);
            }
            set
            {
                SetParameter("ParameterToDate", typeof(DateTime), value, "Starting Date", false);
                ToDateDefined = true;
            }
        }

        /// <summary>
        /// Are parameters required to be asked?
        /// </summary>
        protected bool DatedReport_NeedParameters()
        {
            return base.NeedParameters() || !ToDateDefined || !FromDateDefined;
        }

        public override bool NeedParameters()
        {
            return DatedReport_NeedParameters();
        }

        #region " Component Designer generated code "

        //Component overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Component Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Component Designer
        //It can be modified using the Component Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            //
            //XrLabel_Title
            //
            this.XrLabel_Title.StylePriority.UseFont = false;
            //
            //XrPageInfo_PageNumber
            //
            this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
            //
            //XRLabel_Agency_Name
            //
            this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address3
            //
            this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address1
            //
            this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Phone
            //
            this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
            this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address2
            //
            this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
            //
            //DatedTemplateXtraReportClass
            //
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
				this.Detail,
				this.PageHeader,
				this.PageFooter,
				this.TopMarginBand1,
				this.BottomMarginBand1
			});
            this.RequestParameters = false;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[]
            {
				this.XrControlStyle_Header,
				this.XrControlStyle_Totals,
				this.XrControlStyle_HeaderPannel,
				this.XrControlStyle_GroupHeader
			});
            this.Version = "10.1";
            ((System.ComponentModel.ISupportInitialize)this).EndInit();
        }

        #endregion " Component Designer generated code "

        /// <summary>
        /// Define the subtitle for the report
        /// </summary>
        protected string DatedReport_ReportSubtitle()
        {
            // Special logic for the all dates range
            if (Parameter_ToDate == new DateTime(2099, 12, 31) && Parameter_FromDate == new DateTime(1900, 1, 1))
            {
                return "This report covers all dates";
            }

            if (Parameter_FromDate.Date == DateTime.Now.Date && Parameter_ToDate.Date == DateTime.Now.Date)
            {
                return "This report is for today";
            }

            return string.Format("This report covers dates from {0:d} through {1:d}", Parameter_FromDate, Parameter_ToDate);
        }

        public override string ReportSubTitle
        {
            get
            {
                return DatedReport_ReportSubtitle();
            }
        }

        /// <summary>
        /// Request the report parameters
        /// </summary>
        protected System.Windows.Forms.DialogResult DatedReport_RequestParameters()
        {
            var answer = System.Windows.Forms.DialogResult.OK;

            // If we need parameters then ask for them
            if (NeedParameters())
            {
                // Ask for the date range from the user.
                using (var frm = new DateReportParametersForm(ReportDateRange))
                {
                    answer = frm.ShowDialog();
                    Parameter_FromDate = frm.Parameter_FromDate;
                    Parameter_ToDate = frm.Parameter_ToDate;
                }
            }

            // The last answer that we received is our response.
            return answer;
        }

        public override System.Windows.Forms.DialogResult RequestReportParameters()
        {
            return DatedReport_RequestParameters();
        }
    }
}