using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Reports.CS.Template
{
	/// <summary>
	/// Delegate to handle the title and subtitle string
	/// </summary>
	public delegate void TitleStringEventHandler(object Sender, TitleStringEventArgs e);
}
