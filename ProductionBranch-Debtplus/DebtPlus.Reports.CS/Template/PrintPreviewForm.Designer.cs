﻿namespace DebtPlus.Reports.CS.Template
{
    partial class PrintPreviewForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintPreviewForm));
            this.PrintBarManager1 = new DevExpress.XtraPrinting.Preview.PrintBarManager(this.components);
            this.PreviewBar1 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            this.PrintPreviewBarItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.ZoomBarEditItem1 = new DevExpress.XtraPrinting.Preview.ZoomBarEditItem();
            this.PrintPreviewRepositoryItemComboBox1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox();
            this.PrintPreviewBarItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem18 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem20 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem21 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem22 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem23 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem24 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem25 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem26 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PreviewBar2 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            this.BarStaticItemAccessingDatabase = new DevExpress.XtraBars.BarStaticItem();
            this.PrintPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.BarStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.ProgressBarEditItem1 = new DevExpress.XtraPrinting.Preview.ProgressBarEditItem();
            this.RepositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.PrintPreviewBarItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.BarButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.PrintPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.ZoomTrackBarEditItem1 = new DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem();
            this.RepositoryItemZoomTrackBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar();
            this.PreviewBar3 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            this.PrintPreviewSubItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.BarButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Filter = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItemCustomizeReport = new DevExpress.XtraBars.BarButtonItem();
            this.PrintPreviewSubItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.PrintPreviewSubItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.PrintPreviewBarItem27 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.PrintPreviewBarItem28 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.BarToolbarsListItem1 = new DevExpress.XtraBars.BarToolbarsListItem();
            this.PrintPreviewSubItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.PrintPreviewBarCheckItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintPreviewBarCheckItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.PrintControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintBarManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintPreviewRepositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemZoomTrackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // PrintBarManager1
            // 
            this.PrintBarManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.PreviewBar1,
            this.PreviewBar2,
            this.PreviewBar3});
            this.PrintBarManager1.DockControls.Add(this.barDockControlTop);
            this.PrintBarManager1.DockControls.Add(this.barDockControlBottom);
            this.PrintBarManager1.DockControls.Add(this.barDockControlLeft);
            this.PrintBarManager1.DockControls.Add(this.barDockControlRight);
            this.PrintBarManager1.Form = this;
            this.PrintBarManager1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("PrintBarManager1.ImageStream")));
            this.PrintBarManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.PrintPreviewStaticItem1,
            this.BarStaticItem1,
            this.ProgressBarEditItem1,
            this.PrintPreviewBarItem1,
            this.BarButtonItem1,
            this.PrintPreviewStaticItem2,
            this.ZoomTrackBarEditItem1,
            this.PrintPreviewBarItem2,
            this.PrintPreviewBarItem3,
            this.PrintPreviewBarItem4,
            this.PrintPreviewBarItem5,
            this.PrintPreviewBarItem6,
            this.PrintPreviewBarItem7,
            this.PrintPreviewBarItem8,
            this.PrintPreviewBarItem9,
            this.PrintPreviewBarItem10,
            this.PrintPreviewBarItem11,
            this.PrintPreviewBarItem12,
            this.PrintPreviewBarItem13,
            this.PrintPreviewBarItem14,
            this.PrintPreviewBarItem15,
            this.ZoomBarEditItem1,
            this.PrintPreviewBarItem16,
            this.PrintPreviewBarItem17,
            this.PrintPreviewBarItem18,
            this.PrintPreviewBarItem19,
            this.PrintPreviewBarItem20,
            this.PrintPreviewBarItem21,
            this.PrintPreviewBarItem22,
            this.PrintPreviewBarItem23,
            this.PrintPreviewBarItem24,
            this.PrintPreviewBarItem25,
            this.PrintPreviewBarItem26,
            this.PrintPreviewSubItem1,
            this.PrintPreviewSubItem2,
            this.PrintPreviewSubItem3,
            this.PrintPreviewSubItem4,
            this.PrintPreviewBarItem27,
            this.PrintPreviewBarItem28,
            this.BarToolbarsListItem1,
            this.PrintPreviewBarCheckItem1,
            this.PrintPreviewBarCheckItem2,
            this.PrintPreviewBarCheckItem3,
            this.PrintPreviewBarCheckItem4,
            this.PrintPreviewBarCheckItem5,
            this.PrintPreviewBarCheckItem6,
            this.PrintPreviewBarCheckItem7,
            this.PrintPreviewBarCheckItem8,
            this.PrintPreviewBarCheckItem9,
            this.PrintPreviewBarCheckItem10,
            this.PrintPreviewBarCheckItem11,
            this.PrintPreviewBarCheckItem12,
            this.PrintPreviewBarCheckItem13,
            this.PrintPreviewBarCheckItem14,
            this.PrintPreviewBarCheckItem15,
            this.PrintPreviewBarCheckItem16,
            this.PrintPreviewBarCheckItem17,
            this.BarButtonItemCustomizeReport,
            this.BarButtonItem_Filter,
            this.BarStaticItemAccessingDatabase,
            this.BarButtonItem2});
            this.PrintBarManager1.MainMenu = this.PreviewBar3;
            this.PrintBarManager1.MaxItemId = 61;
            this.PrintBarManager1.PreviewBar = this.PreviewBar1;
            this.PrintBarManager1.PrintControl = this.PrintControl1;
            this.PrintBarManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.RepositoryItemProgressBar1,
            this.RepositoryItemZoomTrackBar1,
            this.PrintPreviewRepositoryItemComboBox1});
            this.PrintBarManager1.StatusBar = this.PreviewBar2;
            this.PrintBarManager1.TransparentEditors = true;
            // 
            // PreviewBar1
            // 
            this.PreviewBar1.BarName = "Toolbar";
            this.PreviewBar1.DockCol = 0;
            this.PreviewBar1.DockRow = 1;
            this.PreviewBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.PreviewBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem5, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem6, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem8, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem11),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem12),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem13, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem14),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem15, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.ZoomBarEditItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem16),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem17, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem19),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem20),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem21, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem22),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem24, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem25),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem26, true)});
            this.PreviewBar1.Text = "Toolbar";
            // 
            // PrintPreviewBarItem2
            // 
            this.PrintPreviewBarItem2.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem2.Caption = "Document Map";
            this.PrintPreviewBarItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap;
            this.PrintPreviewBarItem2.Enabled = false;
            this.PrintPreviewBarItem2.Hint = "Document Map";
            this.PrintPreviewBarItem2.Id = 7;
            this.PrintPreviewBarItem2.ImageIndex = 19;
            this.PrintPreviewBarItem2.Name = "PrintPreviewBarItem2";
            // 
            // PrintPreviewBarItem3
            // 
            this.PrintPreviewBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem3.Caption = "Parameters";
            this.PrintPreviewBarItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Parameters;
            this.PrintPreviewBarItem3.Enabled = false;
            this.PrintPreviewBarItem3.Hint = "Parameters";
            this.PrintPreviewBarItem3.Id = 8;
            this.PrintPreviewBarItem3.ImageIndex = 22;
            this.PrintPreviewBarItem3.Name = "PrintPreviewBarItem3";
            // 
            // PrintPreviewBarItem4
            // 
            this.PrintPreviewBarItem4.Caption = "Search";
            this.PrintPreviewBarItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find;
            this.PrintPreviewBarItem4.Enabled = false;
            this.PrintPreviewBarItem4.Hint = "Search";
            this.PrintPreviewBarItem4.Id = 9;
            this.PrintPreviewBarItem4.ImageIndex = 20;
            this.PrintPreviewBarItem4.Name = "PrintPreviewBarItem4";
            // 
            // PrintPreviewBarItem5
            // 
            this.PrintPreviewBarItem5.Caption = "Customize";
            this.PrintPreviewBarItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize;
            this.PrintPreviewBarItem5.Enabled = false;
            this.PrintPreviewBarItem5.Hint = "Customize";
            this.PrintPreviewBarItem5.Id = 10;
            this.PrintPreviewBarItem5.ImageIndex = 14;
            this.PrintPreviewBarItem5.Name = "PrintPreviewBarItem5";
            // 
            // PrintPreviewBarItem6
            // 
            this.PrintPreviewBarItem6.Caption = "Open";
            this.PrintPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Open;
            this.PrintPreviewBarItem6.Enabled = false;
            this.PrintPreviewBarItem6.Hint = "Open a document";
            this.PrintPreviewBarItem6.Id = 11;
            this.PrintPreviewBarItem6.ImageIndex = 23;
            this.PrintPreviewBarItem6.Name = "PrintPreviewBarItem6";
            // 
            // PrintPreviewBarItem7
            // 
            this.PrintPreviewBarItem7.Caption = "Save";
            this.PrintPreviewBarItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Save;
            this.PrintPreviewBarItem7.Enabled = false;
            this.PrintPreviewBarItem7.Hint = "Save the document";
            this.PrintPreviewBarItem7.Id = 12;
            this.PrintPreviewBarItem7.ImageIndex = 24;
            this.PrintPreviewBarItem7.Name = "PrintPreviewBarItem7";
            // 
            // PrintPreviewBarItem8
            // 
            this.PrintPreviewBarItem8.Caption = "&Print...";
            this.PrintPreviewBarItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print;
            this.PrintPreviewBarItem8.Enabled = false;
            this.PrintPreviewBarItem8.Hint = "Print";
            this.PrintPreviewBarItem8.Id = 13;
            this.PrintPreviewBarItem8.ImageIndex = 0;
            this.PrintPreviewBarItem8.Name = "PrintPreviewBarItem8";
            // 
            // PrintPreviewBarItem9
            // 
            this.PrintPreviewBarItem9.Caption = "P&rint";
            this.PrintPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect;
            this.PrintPreviewBarItem9.Enabled = false;
            this.PrintPreviewBarItem9.Hint = "Quick Print";
            this.PrintPreviewBarItem9.Id = 14;
            this.PrintPreviewBarItem9.ImageIndex = 1;
            this.PrintPreviewBarItem9.Name = "PrintPreviewBarItem9";
            // 
            // PrintPreviewBarItem10
            // 
            this.PrintPreviewBarItem10.Caption = "Page Set&up...";
            this.PrintPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.PrintPreviewBarItem10.Enabled = false;
            this.PrintPreviewBarItem10.Hint = "Page Setup";
            this.PrintPreviewBarItem10.Id = 15;
            this.PrintPreviewBarItem10.ImageIndex = 2;
            this.PrintPreviewBarItem10.Name = "PrintPreviewBarItem10";
            // 
            // PrintPreviewBarItem11
            // 
            this.PrintPreviewBarItem11.Caption = "Header And Footer";
            this.PrintPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF;
            this.PrintPreviewBarItem11.Enabled = false;
            this.PrintPreviewBarItem11.Hint = "Header And Footer";
            this.PrintPreviewBarItem11.Id = 16;
            this.PrintPreviewBarItem11.ImageIndex = 15;
            this.PrintPreviewBarItem11.Name = "PrintPreviewBarItem11";
            // 
            // PrintPreviewBarItem12
            // 
            this.PrintPreviewBarItem12.ActAsDropDown = true;
            this.PrintPreviewBarItem12.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem12.Caption = "Scale";
            this.PrintPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.PrintPreviewBarItem12.Enabled = false;
            this.PrintPreviewBarItem12.Hint = "Scale";
            this.PrintPreviewBarItem12.Id = 17;
            this.PrintPreviewBarItem12.ImageIndex = 25;
            this.PrintPreviewBarItem12.Name = "PrintPreviewBarItem12";
            // 
            // PrintPreviewBarItem13
            // 
            this.PrintPreviewBarItem13.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem13.Caption = "Hand Tool";
            this.PrintPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.PrintPreviewBarItem13.Enabled = false;
            this.PrintPreviewBarItem13.Hint = "Hand Tool";
            this.PrintPreviewBarItem13.Id = 18;
            this.PrintPreviewBarItem13.ImageIndex = 16;
            this.PrintPreviewBarItem13.Name = "PrintPreviewBarItem13";
            // 
            // PrintPreviewBarItem14
            // 
            this.PrintPreviewBarItem14.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem14.Caption = "Magnifier";
            this.PrintPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.PrintPreviewBarItem14.Enabled = false;
            this.PrintPreviewBarItem14.Hint = "Magnifier";
            this.PrintPreviewBarItem14.Id = 19;
            this.PrintPreviewBarItem14.ImageIndex = 3;
            this.PrintPreviewBarItem14.Name = "PrintPreviewBarItem14";
            // 
            // PrintPreviewBarItem15
            // 
            this.PrintPreviewBarItem15.Caption = "Zoom Out";
            this.PrintPreviewBarItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.PrintPreviewBarItem15.Enabled = false;
            this.PrintPreviewBarItem15.Hint = "Zoom Out";
            this.PrintPreviewBarItem15.Id = 20;
            this.PrintPreviewBarItem15.ImageIndex = 5;
            this.PrintPreviewBarItem15.Name = "PrintPreviewBarItem15";
            // 
            // ZoomBarEditItem1
            // 
            this.ZoomBarEditItem1.Caption = "Zoom";
            this.ZoomBarEditItem1.Edit = this.PrintPreviewRepositoryItemComboBox1;
            this.ZoomBarEditItem1.EditValue = "100%";
            this.ZoomBarEditItem1.Enabled = false;
            this.ZoomBarEditItem1.Hint = "Zoom";
            this.ZoomBarEditItem1.Id = 21;
            this.ZoomBarEditItem1.Name = "ZoomBarEditItem1";
            this.ZoomBarEditItem1.Width = 70;
            // 
            // PrintPreviewRepositoryItemComboBox1
            // 
            this.PrintPreviewRepositoryItemComboBox1.AutoComplete = false;
            this.PrintPreviewRepositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PrintPreviewRepositoryItemComboBox1.DropDownRows = 11;
            this.PrintPreviewRepositoryItemComboBox1.Name = "PrintPreviewRepositoryItemComboBox1";
            this.PrintPreviewRepositoryItemComboBox1.UseParentBackground = true;
            // 
            // PrintPreviewBarItem16
            // 
            this.PrintPreviewBarItem16.Caption = "Zoom In";
            this.PrintPreviewBarItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.PrintPreviewBarItem16.Enabled = false;
            this.PrintPreviewBarItem16.Hint = "Zoom In";
            this.PrintPreviewBarItem16.Id = 22;
            this.PrintPreviewBarItem16.ImageIndex = 4;
            this.PrintPreviewBarItem16.Name = "PrintPreviewBarItem16";
            // 
            // PrintPreviewBarItem17
            // 
            this.PrintPreviewBarItem17.Caption = "First Page";
            this.PrintPreviewBarItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage;
            this.PrintPreviewBarItem17.Enabled = false;
            this.PrintPreviewBarItem17.Hint = "First Page";
            this.PrintPreviewBarItem17.Id = 23;
            this.PrintPreviewBarItem17.ImageIndex = 7;
            this.PrintPreviewBarItem17.Name = "PrintPreviewBarItem17";
            // 
            // PrintPreviewBarItem18
            // 
            this.PrintPreviewBarItem18.Caption = "Previous Page";
            this.PrintPreviewBarItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage;
            this.PrintPreviewBarItem18.Enabled = false;
            this.PrintPreviewBarItem18.Hint = "Previous Page";
            this.PrintPreviewBarItem18.Id = 24;
            this.PrintPreviewBarItem18.ImageIndex = 8;
            this.PrintPreviewBarItem18.Name = "PrintPreviewBarItem18";
            // 
            // PrintPreviewBarItem19
            // 
            this.PrintPreviewBarItem19.Caption = "Next Page";
            this.PrintPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage;
            this.PrintPreviewBarItem19.Enabled = false;
            this.PrintPreviewBarItem19.Hint = "Next Page";
            this.PrintPreviewBarItem19.Id = 25;
            this.PrintPreviewBarItem19.ImageIndex = 9;
            this.PrintPreviewBarItem19.Name = "PrintPreviewBarItem19";
            // 
            // PrintPreviewBarItem20
            // 
            this.PrintPreviewBarItem20.Caption = "Last Page";
            this.PrintPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage;
            this.PrintPreviewBarItem20.Enabled = false;
            this.PrintPreviewBarItem20.Hint = "Last Page";
            this.PrintPreviewBarItem20.Id = 26;
            this.PrintPreviewBarItem20.ImageIndex = 10;
            this.PrintPreviewBarItem20.Name = "PrintPreviewBarItem20";
            // 
            // PrintPreviewBarItem21
            // 
            this.PrintPreviewBarItem21.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem21.Caption = "Multiple Pages";
            this.PrintPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.PrintPreviewBarItem21.Enabled = false;
            this.PrintPreviewBarItem21.Hint = "Multiple Pages";
            this.PrintPreviewBarItem21.Id = 27;
            this.PrintPreviewBarItem21.ImageIndex = 11;
            this.PrintPreviewBarItem21.Name = "PrintPreviewBarItem21";
            // 
            // PrintPreviewBarItem22
            // 
            this.PrintPreviewBarItem22.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem22.Caption = "&Color...";
            this.PrintPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.PrintPreviewBarItem22.Enabled = false;
            this.PrintPreviewBarItem22.Hint = "Background";
            this.PrintPreviewBarItem22.Id = 28;
            this.PrintPreviewBarItem22.ImageIndex = 12;
            this.PrintPreviewBarItem22.Name = "PrintPreviewBarItem22";
            // 
            // PrintPreviewBarItem23
            // 
            this.PrintPreviewBarItem23.Caption = "&Watermark...";
            this.PrintPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.PrintPreviewBarItem23.Enabled = false;
            this.PrintPreviewBarItem23.Hint = "Watermark";
            this.PrintPreviewBarItem23.Id = 29;
            this.PrintPreviewBarItem23.ImageIndex = 21;
            this.PrintPreviewBarItem23.Name = "PrintPreviewBarItem23";
            // 
            // PrintPreviewBarItem24
            // 
            this.PrintPreviewBarItem24.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem24.Caption = "Export Document...";
            this.PrintPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.PrintPreviewBarItem24.Enabled = false;
            this.PrintPreviewBarItem24.Hint = "Export Document...";
            this.PrintPreviewBarItem24.Id = 30;
            this.PrintPreviewBarItem24.ImageIndex = 18;
            this.PrintPreviewBarItem24.Name = "PrintPreviewBarItem24";
            // 
            // PrintPreviewBarItem25
            // 
            this.PrintPreviewBarItem25.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.PrintPreviewBarItem25.Caption = "Send via E-Mail...";
            this.PrintPreviewBarItem25.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.PrintPreviewBarItem25.Enabled = false;
            this.PrintPreviewBarItem25.Hint = "Send via E-Mail...";
            this.PrintPreviewBarItem25.Id = 31;
            this.PrintPreviewBarItem25.ImageIndex = 17;
            this.PrintPreviewBarItem25.Name = "PrintPreviewBarItem25";
            // 
            // PrintPreviewBarItem26
            // 
            this.PrintPreviewBarItem26.Caption = "E&xit";
            this.PrintPreviewBarItem26.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.PrintPreviewBarItem26.Enabled = false;
            this.PrintPreviewBarItem26.Hint = "Close Preview";
            this.PrintPreviewBarItem26.Id = 32;
            this.PrintPreviewBarItem26.ImageIndex = 13;
            this.PrintPreviewBarItem26.Name = "PrintPreviewBarItem26";
            // 
            // PreviewBar2
            // 
            this.PreviewBar2.BarName = "Status Bar";
            this.PreviewBar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.PreviewBar2.DockCol = 0;
            this.PreviewBar2.DockRow = 0;
            this.PreviewBar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.PreviewBar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItemAccessingDatabase),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.ProgressBarEditItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewStaticItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.ZoomTrackBarEditItem1)});
            this.PreviewBar2.OptionsBar.AllowQuickCustomization = false;
            this.PreviewBar2.OptionsBar.DrawDragBorder = false;
            this.PreviewBar2.OptionsBar.UseWholeRow = true;
            this.PreviewBar2.Text = "Status Bar";
            // 
            // BarStaticItemAccessingDatabase
            // 
            this.BarStaticItemAccessingDatabase.Caption = "Accessing Database";
            this.BarStaticItemAccessingDatabase.Id = 59;
            this.BarStaticItemAccessingDatabase.Name = "BarStaticItemAccessingDatabase";
            this.BarStaticItemAccessingDatabase.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // PrintPreviewStaticItem1
            // 
            this.PrintPreviewStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PrintPreviewStaticItem1.Caption = "Nothing";
            this.PrintPreviewStaticItem1.Id = 0;
            this.PrintPreviewStaticItem1.LeftIndent = 1;
            this.PrintPreviewStaticItem1.Name = "PrintPreviewStaticItem1";
            this.PrintPreviewStaticItem1.RightIndent = 1;
            this.PrintPreviewStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.PrintPreviewStaticItem1.Type = "PageOfPages";
            // 
            // BarStaticItem1
            // 
            this.BarStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.BarStaticItem1.Id = 1;
            this.BarStaticItem1.Name = "BarStaticItem1";
            this.BarStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.BarStaticItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInRuntime;
            // 
            // ProgressBarEditItem1
            // 
            this.ProgressBarEditItem1.Edit = this.RepositoryItemProgressBar1;
            this.ProgressBarEditItem1.EditHeight = 12;
            this.ProgressBarEditItem1.Id = 2;
            this.ProgressBarEditItem1.Name = "ProgressBarEditItem1";
            this.ProgressBarEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.ProgressBarEditItem1.Width = 150;
            // 
            // RepositoryItemProgressBar1
            // 
            this.RepositoryItemProgressBar1.Name = "RepositoryItemProgressBar1";
            this.RepositoryItemProgressBar1.UseParentBackground = true;
            // 
            // PrintPreviewBarItem1
            // 
            this.PrintPreviewBarItem1.Caption = "Stop";
            this.PrintPreviewBarItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.StopPageBuilding;
            this.PrintPreviewBarItem1.Enabled = false;
            this.PrintPreviewBarItem1.Hint = "Stop";
            this.PrintPreviewBarItem1.Id = 3;
            this.PrintPreviewBarItem1.Name = "PrintPreviewBarItem1";
            this.PrintPreviewBarItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarButtonItem1
            // 
            this.BarButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.BarButtonItem1.Enabled = false;
            this.BarButtonItem1.Id = 4;
            this.BarButtonItem1.Name = "BarButtonItem1";
            this.BarButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.OnlyInRuntime;
            // 
            // PrintPreviewStaticItem2
            // 
            this.PrintPreviewStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.PrintPreviewStaticItem2.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PrintPreviewStaticItem2.Caption = "100%";
            this.PrintPreviewStaticItem2.Id = 5;
            this.PrintPreviewStaticItem2.Name = "PrintPreviewStaticItem2";
            this.PrintPreviewStaticItem2.TextAlignment = System.Drawing.StringAlignment.Far;
            this.PrintPreviewStaticItem2.Type = "ZoomFactor";
            this.PrintPreviewStaticItem2.Width = 40;
            // 
            // ZoomTrackBarEditItem1
            // 
            this.ZoomTrackBarEditItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.ZoomTrackBarEditItem1.Edit = this.RepositoryItemZoomTrackBar1;
            this.ZoomTrackBarEditItem1.EditValue = 90;
            this.ZoomTrackBarEditItem1.Enabled = false;
            this.ZoomTrackBarEditItem1.Id = 6;
            this.ZoomTrackBarEditItem1.Name = "ZoomTrackBarEditItem1";
            this.ZoomTrackBarEditItem1.Range = new int[] {
        10,
        500};
            this.ZoomTrackBarEditItem1.Width = 140;
            // 
            // RepositoryItemZoomTrackBar1
            // 
            this.RepositoryItemZoomTrackBar1.Alignment = DevExpress.Utils.VertAlignment.Center;
            this.RepositoryItemZoomTrackBar1.AllowFocused = false;
            this.RepositoryItemZoomTrackBar1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.RepositoryItemZoomTrackBar1.Maximum = 180;
            this.RepositoryItemZoomTrackBar1.Name = "RepositoryItemZoomTrackBar1";
            this.RepositoryItemZoomTrackBar1.ScrollThumbStyle = DevExpress.XtraEditors.Repository.ScrollThumbStyle.ArrowDownRight;
            this.RepositoryItemZoomTrackBar1.UseParentBackground = true;
            // 
            // PreviewBar3
            // 
            this.PreviewBar3.BarName = "Main Menu";
            this.PreviewBar3.DockCol = 0;
            this.PreviewBar3.DockRow = 0;
            this.PreviewBar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.PreviewBar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewSubItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewSubItem3)});
            this.PreviewBar3.OptionsBar.MultiLine = true;
            this.PreviewBar3.OptionsBar.UseWholeRow = true;
            this.PreviewBar3.Text = "Main Menu";
            // 
            // PrintPreviewSubItem1
            // 
            this.PrintPreviewSubItem1.Caption = "&File";
            this.PrintPreviewSubItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.File;
            this.PrintPreviewSubItem1.Id = 33;
            this.PrintPreviewSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem9),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem2, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem24),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem25),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Filter, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItemCustomizeReport),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem26, true)});
            this.PrintPreviewSubItem1.Name = "PrintPreviewSubItem1";
            // 
            // BarButtonItem2
            // 
            this.BarButtonItem2.Caption = "Upload PDF to SFTP site...";
            this.BarButtonItem2.Id = 60;
            this.BarButtonItem2.Name = "BarButtonItem2";
            // 
            // BarButtonItem_Filter
            // 
            this.BarButtonItem_Filter.Caption = "&Select Expert...";
            this.BarButtonItem_Filter.Id = 58;
            this.BarButtonItem_Filter.Name = "BarButtonItem_Filter";
            // 
            // BarButtonItemCustomizeReport
            // 
            this.BarButtonItemCustomizeReport.Caption = "&Customize...";
            this.BarButtonItemCustomizeReport.Id = 57;
            this.BarButtonItemCustomizeReport.Name = "BarButtonItemCustomizeReport";
            // 
            // PrintPreviewSubItem2
            // 
            this.PrintPreviewSubItem2.Caption = "&View";
            this.PrintPreviewSubItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.View;
            this.PrintPreviewSubItem2.Id = 34;
            this.PrintPreviewSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewSubItem4, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.BarToolbarsListItem1, true)});
            this.PrintPreviewSubItem2.Name = "PrintPreviewSubItem2";
            // 
            // PrintPreviewSubItem4
            // 
            this.PrintPreviewSubItem4.Caption = "&Page Layout";
            this.PrintPreviewSubItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayout;
            this.PrintPreviewSubItem4.Id = 36;
            this.PrintPreviewSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem27),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem28)});
            this.PrintPreviewSubItem4.Name = "PrintPreviewSubItem4";
            // 
            // PrintPreviewBarItem27
            // 
            this.PrintPreviewBarItem27.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem27.Caption = "&Facing";
            this.PrintPreviewBarItem27.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutFacing;
            this.PrintPreviewBarItem27.Enabled = false;
            this.PrintPreviewBarItem27.GroupIndex = 100;
            this.PrintPreviewBarItem27.Id = 37;
            this.PrintPreviewBarItem27.Name = "PrintPreviewBarItem27";
            // 
            // PrintPreviewBarItem28
            // 
            this.PrintPreviewBarItem28.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.PrintPreviewBarItem28.Caption = "&Continuous";
            this.PrintPreviewBarItem28.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutContinuous;
            this.PrintPreviewBarItem28.Enabled = false;
            this.PrintPreviewBarItem28.GroupIndex = 100;
            this.PrintPreviewBarItem28.Id = 38;
            this.PrintPreviewBarItem28.Name = "PrintPreviewBarItem28";
            // 
            // BarToolbarsListItem1
            // 
            this.BarToolbarsListItem1.Caption = "Bars";
            this.BarToolbarsListItem1.Id = 39;
            this.BarToolbarsListItem1.Name = "BarToolbarsListItem1";
            // 
            // PrintPreviewSubItem3
            // 
            this.PrintPreviewSubItem3.Caption = "&Background";
            this.PrintPreviewSubItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Background;
            this.PrintPreviewSubItem3.Id = 35;
            this.PrintPreviewSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem22),
            new DevExpress.XtraBars.LinkPersistInfo(this.PrintPreviewBarItem23)});
            this.PrintPreviewSubItem3.Name = "PrintPreviewSubItem3";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(486, 51);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 234);
            this.barDockControlBottom.Size = new System.Drawing.Size(486, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 51);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 183);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(486, 51);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 183);
            // 
            // PrintPreviewBarCheckItem1
            // 
            this.PrintPreviewBarCheckItem1.Caption = "PDF File";
            this.PrintPreviewBarCheckItem1.Checked = true;
            this.PrintPreviewBarCheckItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.PrintPreviewBarCheckItem1.Enabled = false;
            this.PrintPreviewBarCheckItem1.GroupIndex = 2;
            this.PrintPreviewBarCheckItem1.Hint = "PDF File";
            this.PrintPreviewBarCheckItem1.Id = 40;
            this.PrintPreviewBarCheckItem1.Name = "PrintPreviewBarCheckItem1";
            // 
            // PrintPreviewBarCheckItem2
            // 
            this.PrintPreviewBarCheckItem2.Caption = "HTML File";
            this.PrintPreviewBarCheckItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.PrintPreviewBarCheckItem2.Enabled = false;
            this.PrintPreviewBarCheckItem2.GroupIndex = 2;
            this.PrintPreviewBarCheckItem2.Hint = "HTML File";
            this.PrintPreviewBarCheckItem2.Id = 41;
            this.PrintPreviewBarCheckItem2.Name = "PrintPreviewBarCheckItem2";
            // 
            // PrintPreviewBarCheckItem3
            // 
            this.PrintPreviewBarCheckItem3.Caption = "MHT File";
            this.PrintPreviewBarCheckItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.PrintPreviewBarCheckItem3.Enabled = false;
            this.PrintPreviewBarCheckItem3.GroupIndex = 2;
            this.PrintPreviewBarCheckItem3.Hint = "MHT File";
            this.PrintPreviewBarCheckItem3.Id = 42;
            this.PrintPreviewBarCheckItem3.Name = "PrintPreviewBarCheckItem3";
            // 
            // PrintPreviewBarCheckItem4
            // 
            this.PrintPreviewBarCheckItem4.Caption = "RTF File";
            this.PrintPreviewBarCheckItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.PrintPreviewBarCheckItem4.Enabled = false;
            this.PrintPreviewBarCheckItem4.GroupIndex = 2;
            this.PrintPreviewBarCheckItem4.Hint = "RTF File";
            this.PrintPreviewBarCheckItem4.Id = 43;
            this.PrintPreviewBarCheckItem4.Name = "PrintPreviewBarCheckItem4";
            // 
            // PrintPreviewBarCheckItem5
            // 
            this.PrintPreviewBarCheckItem5.Caption = "XLS File";
            this.PrintPreviewBarCheckItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.PrintPreviewBarCheckItem5.Enabled = false;
            this.PrintPreviewBarCheckItem5.GroupIndex = 2;
            this.PrintPreviewBarCheckItem5.Hint = "XLS File";
            this.PrintPreviewBarCheckItem5.Id = 44;
            this.PrintPreviewBarCheckItem5.Name = "PrintPreviewBarCheckItem5";
            // 
            // PrintPreviewBarCheckItem6
            // 
            this.PrintPreviewBarCheckItem6.Caption = "XLSX File";
            this.PrintPreviewBarCheckItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXlsx;
            this.PrintPreviewBarCheckItem6.Enabled = false;
            this.PrintPreviewBarCheckItem6.GroupIndex = 2;
            this.PrintPreviewBarCheckItem6.Hint = "XLSX File";
            this.PrintPreviewBarCheckItem6.Id = 45;
            this.PrintPreviewBarCheckItem6.Name = "PrintPreviewBarCheckItem6";
            // 
            // PrintPreviewBarCheckItem7
            // 
            this.PrintPreviewBarCheckItem7.Caption = "CSV File";
            this.PrintPreviewBarCheckItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.PrintPreviewBarCheckItem7.Enabled = false;
            this.PrintPreviewBarCheckItem7.GroupIndex = 2;
            this.PrintPreviewBarCheckItem7.Hint = "CSV File";
            this.PrintPreviewBarCheckItem7.Id = 46;
            this.PrintPreviewBarCheckItem7.Name = "PrintPreviewBarCheckItem7";
            // 
            // PrintPreviewBarCheckItem8
            // 
            this.PrintPreviewBarCheckItem8.Caption = "Text File";
            this.PrintPreviewBarCheckItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.PrintPreviewBarCheckItem8.Enabled = false;
            this.PrintPreviewBarCheckItem8.GroupIndex = 2;
            this.PrintPreviewBarCheckItem8.Hint = "Text File";
            this.PrintPreviewBarCheckItem8.Id = 47;
            this.PrintPreviewBarCheckItem8.Name = "PrintPreviewBarCheckItem8";
            // 
            // PrintPreviewBarCheckItem9
            // 
            this.PrintPreviewBarCheckItem9.Caption = "Image File";
            this.PrintPreviewBarCheckItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.PrintPreviewBarCheckItem9.Enabled = false;
            this.PrintPreviewBarCheckItem9.GroupIndex = 2;
            this.PrintPreviewBarCheckItem9.Hint = "Image File";
            this.PrintPreviewBarCheckItem9.Id = 48;
            this.PrintPreviewBarCheckItem9.Name = "PrintPreviewBarCheckItem9";
            // 
            // PrintPreviewBarCheckItem10
            // 
            this.PrintPreviewBarCheckItem10.Caption = "PDF File";
            this.PrintPreviewBarCheckItem10.Checked = true;
            this.PrintPreviewBarCheckItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.PrintPreviewBarCheckItem10.Enabled = false;
            this.PrintPreviewBarCheckItem10.GroupIndex = 1;
            this.PrintPreviewBarCheckItem10.Hint = "PDF File";
            this.PrintPreviewBarCheckItem10.Id = 49;
            this.PrintPreviewBarCheckItem10.Name = "PrintPreviewBarCheckItem10";
            // 
            // PrintPreviewBarCheckItem11
            // 
            this.PrintPreviewBarCheckItem11.Caption = "MHT File";
            this.PrintPreviewBarCheckItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.PrintPreviewBarCheckItem11.Enabled = false;
            this.PrintPreviewBarCheckItem11.GroupIndex = 1;
            this.PrintPreviewBarCheckItem11.Hint = "MHT File";
            this.PrintPreviewBarCheckItem11.Id = 50;
            this.PrintPreviewBarCheckItem11.Name = "PrintPreviewBarCheckItem11";
            // 
            // PrintPreviewBarCheckItem12
            // 
            this.PrintPreviewBarCheckItem12.Caption = "RTF File";
            this.PrintPreviewBarCheckItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.PrintPreviewBarCheckItem12.Enabled = false;
            this.PrintPreviewBarCheckItem12.GroupIndex = 1;
            this.PrintPreviewBarCheckItem12.Hint = "RTF File";
            this.PrintPreviewBarCheckItem12.Id = 51;
            this.PrintPreviewBarCheckItem12.Name = "PrintPreviewBarCheckItem12";
            // 
            // PrintPreviewBarCheckItem13
            // 
            this.PrintPreviewBarCheckItem13.Caption = "XLS File";
            this.PrintPreviewBarCheckItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.PrintPreviewBarCheckItem13.Enabled = false;
            this.PrintPreviewBarCheckItem13.GroupIndex = 1;
            this.PrintPreviewBarCheckItem13.Hint = "XLS File";
            this.PrintPreviewBarCheckItem13.Id = 52;
            this.PrintPreviewBarCheckItem13.Name = "PrintPreviewBarCheckItem13";
            // 
            // PrintPreviewBarCheckItem14
            // 
            this.PrintPreviewBarCheckItem14.Caption = "XLSX File";
            this.PrintPreviewBarCheckItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXlsx;
            this.PrintPreviewBarCheckItem14.Enabled = false;
            this.PrintPreviewBarCheckItem14.GroupIndex = 1;
            this.PrintPreviewBarCheckItem14.Hint = "XLSX File";
            this.PrintPreviewBarCheckItem14.Id = 53;
            this.PrintPreviewBarCheckItem14.Name = "PrintPreviewBarCheckItem14";
            // 
            // PrintPreviewBarCheckItem15
            // 
            this.PrintPreviewBarCheckItem15.Caption = "CSV File";
            this.PrintPreviewBarCheckItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.PrintPreviewBarCheckItem15.Enabled = false;
            this.PrintPreviewBarCheckItem15.GroupIndex = 1;
            this.PrintPreviewBarCheckItem15.Hint = "CSV File";
            this.PrintPreviewBarCheckItem15.Id = 54;
            this.PrintPreviewBarCheckItem15.Name = "PrintPreviewBarCheckItem15";
            // 
            // PrintPreviewBarCheckItem16
            // 
            this.PrintPreviewBarCheckItem16.Caption = "Text File";
            this.PrintPreviewBarCheckItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.PrintPreviewBarCheckItem16.Enabled = false;
            this.PrintPreviewBarCheckItem16.GroupIndex = 1;
            this.PrintPreviewBarCheckItem16.Hint = "Text File";
            this.PrintPreviewBarCheckItem16.Id = 55;
            this.PrintPreviewBarCheckItem16.Name = "PrintPreviewBarCheckItem16";
            // 
            // PrintPreviewBarCheckItem17
            // 
            this.PrintPreviewBarCheckItem17.Caption = "Image File";
            this.PrintPreviewBarCheckItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.PrintPreviewBarCheckItem17.Enabled = false;
            this.PrintPreviewBarCheckItem17.GroupIndex = 1;
            this.PrintPreviewBarCheckItem17.Hint = "Image File";
            this.PrintPreviewBarCheckItem17.Id = 56;
            this.PrintPreviewBarCheckItem17.Name = "PrintPreviewBarCheckItem17";
            // 
            // PrintControl1
            // 
            this.PrintControl1.BackColor = System.Drawing.Color.Empty;
            this.PrintControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PrintControl1.ForeColor = System.Drawing.Color.Empty;
            this.PrintControl1.IsMetric = false;
            this.PrintControl1.Location = new System.Drawing.Point(0, 51);
            this.PrintControl1.Name = "PrintControl1";
            this.PrintControl1.Size = new System.Drawing.Size(486, 183);
            this.PrintControl1.TabIndex = 4;
            this.PrintControl1.TooltipFont = new System.Drawing.Font("Tahoma", 8.25F);
            // 
            // PrintPreviewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 262);
            this.Controls.Add(this.PrintControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "PrintPreviewForm";
            this.Text = "Report";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintBarManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintPreviewRepositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RepositoryItemZoomTrackBar1)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraPrinting.Preview.PrintBarManager PrintBarManager1;
        private DevExpress.XtraPrinting.Preview.PreviewBar PreviewBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem15;
        private DevExpress.XtraPrinting.Preview.ZoomBarEditItem ZoomBarEditItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox PrintPreviewRepositoryItemComboBox1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem17;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem18;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem19;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem20;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem21;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem22;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem23;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem24;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem25;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem26;
        private DevExpress.XtraPrinting.Preview.PreviewBar PreviewBar2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem PrintPreviewStaticItem1;
        private DevExpress.XtraBars.BarStaticItem BarStaticItem1;
        private DevExpress.XtraPrinting.Preview.ProgressBarEditItem ProgressBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar RepositoryItemProgressBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem1;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem PrintPreviewStaticItem2;
        private DevExpress.XtraPrinting.Preview.ZoomTrackBarEditItem ZoomTrackBarEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemZoomTrackBar RepositoryItemZoomTrackBar1;
        private DevExpress.XtraPrinting.Preview.PreviewBar PreviewBar3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem PrintPreviewSubItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem PrintPreviewSubItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem PrintPreviewSubItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem27;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem PrintPreviewBarItem28;
        private DevExpress.XtraBars.BarToolbarsListItem BarToolbarsListItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem PrintPreviewSubItem3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraPrinting.Control.PrintControl PrintControl1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem15;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem PrintPreviewBarCheckItem17;
        private DevExpress.XtraBars.BarButtonItem BarButtonItemCustomizeReport;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem_Filter;
        private System.Windows.Forms.Timer Timer1;
        private DevExpress.XtraBars.BarStaticItem BarStaticItemAccessingDatabase;
        private DevExpress.XtraBars.BarButtonItem BarButtonItem2;
    }
}
