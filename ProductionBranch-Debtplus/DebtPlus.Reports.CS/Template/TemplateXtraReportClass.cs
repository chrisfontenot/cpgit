using System;
using System.Drawing.Printing;
using System.Linq;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace DebtPlus.Reports.CS.Template
{
    // Do not make this MustInherit !!
    public partial class TemplateXtraReportClass : BaseXtraReportClass
    {
        public TemplateXtraReportClass()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            XrLabel_Title.BeforePrint += XrLabel_Title_BeforePrint;
            PrintingSystem.PageSettingsChanged += HandlePageSettingsChanged;
            PrintingSystem.AfterMarginsChange += HandleAfterMarginsChange;
            XrLabel_Subtitle.BeforePrint += XrLabel_Subtitle_BeforePrint;
            XrPanel_AgencyAddress.BeforePrint += XrPanel_AgencyAddress_BeforePrint;
        }

        private void UnRegisterHandlers()
        {
            XrLabel_Title.BeforePrint -= XrLabel_Title_BeforePrint;
            PrintingSystem.PageSettingsChanged -= HandlePageSettingsChanged;
            PrintingSystem.AfterMarginsChange -= HandleAfterMarginsChange;
            XrLabel_Subtitle.BeforePrint -= XrLabel_Subtitle_BeforePrint;
            XrPanel_AgencyAddress.BeforePrint -= XrPanel_AgencyAddress_BeforePrint;
        }

        //Component overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Component Designer
        private System.ComponentModel.IContainer components = null;

        protected DevExpress.XtraReports.UI.PageHeaderBand PageHeader;

        protected DevExpress.XtraReports.UI.PageFooterBand PageFooter;

        //NOTE: The following procedure is required by the Component Designer
        //It can be modified using the Component Designer.
        //Do not modify it using the code editor.
        protected DevExpress.XtraReports.UI.XRLabel XrLabel_Title;

        protected DevExpress.XtraReports.UI.XRLabel XrLabel_Subtitle;
        protected DevExpress.XtraReports.UI.XRPageInfo XrPageInfo_PageNumber;
        protected DevExpress.XtraReports.UI.XRPageInfo XrPageInfo1;
        protected DevExpress.XtraReports.UI.XRPanel XrPanel_AgencyAddress;
        protected DevExpress.XtraReports.UI.XRLabel XRLabel_Agency_Name;
        protected DevExpress.XtraReports.UI.XRLine XrLine_Title;
        protected DevExpress.XtraReports.UI.XRLabel XrLabel_Agency_Address3;
        protected DevExpress.XtraReports.UI.XRLabel XrLabel_Agency_Address1;
        protected DevExpress.XtraReports.UI.XRLabel XrLabel_Agency_Phone;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_Header;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_Totals;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_HeaderPannel;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_GroupHeader;

        protected DevExpress.XtraReports.UI.XRLabel XrLabel_Agency_Address2;

        private void InitializeComponent()
        {
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.XrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.XrLabel_Subtitle = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLine_Title = new DevExpress.XtraReports.UI.XRLine();
            this.XrLabel_Title = new DevExpress.XtraReports.UI.XRLabel();
            this.XrPanel_AgencyAddress = new DevExpress.XtraReports.UI.XRPanel();
            this.XrLabel_Agency_Address3 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_Agency_Address1 = new DevExpress.XtraReports.UI.XRLabel();
            this.XRLabel_Agency_Name = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_Agency_Phone = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_Agency_Address2 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.XrPageInfo_PageNumber = new DevExpress.XtraReports.UI.XRPageInfo();
            this.XrControlStyle_Header = new DevExpress.XtraReports.UI.XRControlStyle();
            this.XrControlStyle_Totals = new DevExpress.XtraReports.UI.XRControlStyle();
            this.XrControlStyle_HeaderPannel = new DevExpress.XtraReports.UI.XRControlStyle();
            this.XrControlStyle_GroupHeader = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            //
            //Detail
            //
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //PageHeader
            //
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrPageInfo1,
				this.XrLabel_Subtitle,
				this.XrLine_Title,
				this.XrLabel_Title,
				this.XrPanel_AgencyAddress
			});
            this.PageHeader.HeightF = 108f;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //XrPageInfo1
            //
            this.XrPageInfo1.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrPageInfo1.ForeColor = System.Drawing.Color.Teal;
            this.XrPageInfo1.Format = "{0:MMMM dd, yyyy          h:mm tt}";
            this.XrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(8f, 67f);
            this.XrPageInfo1.Name = "XrPageInfo1";
            this.XrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.XrPageInfo1.SizeF = new System.Drawing.SizeF(217f, 17f);
            this.XrPageInfo1.StyleName = "XrControlStyle_Header";
            this.XrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //XrLabel_Subtitle
            //
            this.XrLabel_Subtitle.CanShrink = true;
            this.XrLabel_Subtitle.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrLabel_Subtitle.ForeColor = System.Drawing.Color.Teal;
            this.XrLabel_Subtitle.LocationFloat = new DevExpress.Utils.PointFloat(8f, 85.00001f);
            this.XrLabel_Subtitle.Multiline = true;
            this.XrLabel_Subtitle.Name = "XrLabel_Subtitle";
            this.XrLabel_Subtitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_Subtitle.SizeF = new System.Drawing.SizeF(734f, 17f);
            this.XrLabel_Subtitle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //XrLine_Title
            //
            this.XrLine_Title.ForeColor = System.Drawing.Color.Teal;
            this.XrLine_Title.LineWidth = 2;
            this.XrLine_Title.LocationFloat = new DevExpress.Utils.PointFloat(8f, 50f);
            this.XrLine_Title.Name = "XrLine_Title";
            this.XrLine_Title.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrLine_Title.SizeF = new System.Drawing.SizeF(425f, 8f);
            //
            //XrLabel_Title
            //
            this.XrLabel_Title.CanShrink = true;
            this.XrLabel_Title.Font = new System.Drawing.Font("Comic Sans MS", 20f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrLabel_Title.ForeColor = System.Drawing.Color.Teal;
            this.XrLabel_Title.LocationFloat = new DevExpress.Utils.PointFloat(8f, 8f);
            this.XrLabel_Title.Name = "XrLabel_Title";
            this.XrLabel_Title.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_Title.SizeF = new System.Drawing.SizeF(425f, 42f);
            this.XrLabel_Title.StyleName = "XrControlStyle_Header";
            this.XrLabel_Title.StylePriority.UseFont = false;
            this.XrLabel_Title.Text = "Template Report Title";
            this.XrLabel_Title.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //XrPanel_AgencyAddress
            //
            this.XrPanel_AgencyAddress.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrLabel_Agency_Address3,
				this.XrLabel_Agency_Address1,
				this.XRLabel_Agency_Name,
				this.XrLabel_Agency_Phone,
				this.XrLabel_Agency_Address2
			});
            this.XrPanel_AgencyAddress.LocationFloat = new DevExpress.Utils.PointFloat(442f, 0f);
            this.XrPanel_AgencyAddress.Name = "XrPanel_AgencyAddress";
            this.XrPanel_AgencyAddress.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrPanel_AgencyAddress.SizeF = new System.Drawing.SizeF(308f, 85.00001f);
            //
            //XrLabel_Agency_Address3
            //
            this.XrLabel_Agency_Address3.CanShrink = true;
            this.XrLabel_Agency_Address3.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrLabel_Agency_Address3.ForeColor = System.Drawing.Color.Teal;
            this.XrLabel_Agency_Address3.LocationFloat = new DevExpress.Utils.PointFloat(0f, 51f);
            this.XrLabel_Agency_Address3.Name = "XrLabel_Agency_Address3";
            this.XrLabel_Agency_Address3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_Agency_Address3.SizeF = new System.Drawing.SizeF(300f, 17f);
            this.XrLabel_Agency_Address3.StyleName = "XrControlStyle_Header";
            this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
            this.XrLabel_Agency_Address3.Text = "Address3";
            this.XrLabel_Agency_Address3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_Agency_Address1
            //
            this.XrLabel_Agency_Address1.CanShrink = true;
            this.XrLabel_Agency_Address1.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrLabel_Agency_Address1.ForeColor = System.Drawing.Color.Teal;
            this.XrLabel_Agency_Address1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 17f);
            this.XrLabel_Agency_Address1.Name = "XrLabel_Agency_Address1";
            this.XrLabel_Agency_Address1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_Agency_Address1.SizeF = new System.Drawing.SizeF(300f, 17f);
            this.XrLabel_Agency_Address1.StyleName = "XrControlStyle_Header";
            this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
            this.XrLabel_Agency_Address1.Text = "Address1";
            this.XrLabel_Agency_Address1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XRLabel_Agency_Name
            //
            this.XRLabel_Agency_Name.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XRLabel_Agency_Name.ForeColor = System.Drawing.Color.Teal;
            this.XRLabel_Agency_Name.LocationFloat = new DevExpress.Utils.PointFloat(0f, 0f);
            this.XRLabel_Agency_Name.Name = "XRLabel_Agency_Name";
            this.XRLabel_Agency_Name.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XRLabel_Agency_Name.SizeF = new System.Drawing.SizeF(300f, 17f);
            this.XRLabel_Agency_Name.StyleName = "XrControlStyle_Header";
            this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
            this.XRLabel_Agency_Name.Text = "AgencyName";
            this.XRLabel_Agency_Name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_Agency_Phone
            //
            this.XrLabel_Agency_Phone.CanShrink = true;
            this.XrLabel_Agency_Phone.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrLabel_Agency_Phone.ForeColor = System.Drawing.Color.Teal;
            this.XrLabel_Agency_Phone.LocationFloat = new DevExpress.Utils.PointFloat(0f, 68f);
            this.XrLabel_Agency_Phone.Name = "XrLabel_Agency_Phone";
            this.XrLabel_Agency_Phone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_Agency_Phone.SizeF = new System.Drawing.SizeF(300f, 17f);
            this.XrLabel_Agency_Phone.StyleName = "XrControlStyle_Header";
            this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
            this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
            this.XrLabel_Agency_Phone.Text = "Phone: (000) 555-1212";
            this.XrLabel_Agency_Phone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_Agency_Address2
            //
            this.XrLabel_Agency_Address2.CanShrink = true;
            this.XrLabel_Agency_Address2.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrLabel_Agency_Address2.ForeColor = System.Drawing.Color.Teal;
            this.XrLabel_Agency_Address2.LocationFloat = new DevExpress.Utils.PointFloat(0f, 34f);
            this.XrLabel_Agency_Address2.Name = "XrLabel_Agency_Address2";
            this.XrLabel_Agency_Address2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_Agency_Address2.SizeF = new System.Drawing.SizeF(300f, 17f);
            this.XrLabel_Agency_Address2.StyleName = "XrControlStyle_Header";
            this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
            this.XrLabel_Agency_Address2.Text = "Address2";
            this.XrLabel_Agency_Address2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //PageFooter
            //
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] { this.XrPageInfo_PageNumber });
            this.PageFooter.HeightF = 30f;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //XrPageInfo_PageNumber
            //
            this.XrPageInfo_PageNumber.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrPageInfo_PageNumber.ForeColor = System.Drawing.Color.Teal;
            this.XrPageInfo_PageNumber.Format = "{0:Page #,#}";
            this.XrPageInfo_PageNumber.LocationFloat = new DevExpress.Utils.PointFloat(608f, 8f);
            this.XrPageInfo_PageNumber.Name = "XrPageInfo_PageNumber";
            this.XrPageInfo_PageNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrPageInfo_PageNumber.PageInfo = DevExpress.XtraPrinting.PageInfo.Number;
            this.XrPageInfo_PageNumber.SizeF = new System.Drawing.SizeF(133f, 17f);
            this.XrPageInfo_PageNumber.StyleName = "XrControlStyle_Header";
            this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
            this.XrPageInfo_PageNumber.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrControlStyle_Header
            //
            this.XrControlStyle_Header.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal;
            this.XrControlStyle_Header.Name = "XrControlStyle_Header";
            this.XrControlStyle_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //XrControlStyle_Totals
            //
            this.XrControlStyle_Totals.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_Totals.Name = "XrControlStyle_Totals";
            this.XrControlStyle_Totals.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrControlStyle_HeaderPannel
            //
            this.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal;
            this.XrControlStyle_HeaderPannel.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White;
            this.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel";
            this.XrControlStyle_HeaderPannel.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrControlStyle_GroupHeader
            //
            this.XrControlStyle_GroupHeader.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon;
            this.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader";
            this.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //TemplateXtraReportClass
            //
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
				this.PageHeader,
				this.PageFooter
			});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
				this.XrControlStyle_Header,
				this.XrControlStyle_Totals,
				this.XrControlStyle_HeaderPannel,
				this.XrControlStyle_GroupHeader
			});
            this.Version = "9.3";
            ((System.ComponentModel.ISupportInitialize)this).EndInit();
        }

        public override System.Windows.Forms.DialogResult RequestReportParameters()
        {
            return System.Windows.Forms.DialogResult.OK;
        }

        private bool firstTime = true;

        private void XrPanel_AgencyAddress_BeforePrint(object sender, PrintEventArgs e)
        {
            // Do this logic once and once only. The agency name/address never changes from one page to another
            if (firstTime)
            {
                firstTime = false;

                // Find the agency information from the database via the procedure xpr_OrganizationAddress
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    // Get the organization address from the system tables.
                    var query = (from sp in bc.xpr_OrganizationAddress() select sp).Single();

                    // Load the items into the controls
                    if (query != null)
                    {
                        XRLabel_Agency_Name.Text = (query.name ?? string.Empty).Trim();
                        XrLabel_Agency_Address1.Text = (query.addr1 ?? string.Empty).ToString().Trim();
                        XrLabel_Agency_Address2.Text = (query.addr2 ?? string.Empty).ToString().Trim();
                        XrLabel_Agency_Address3.Text = (query.addr3 ?? string.Empty).ToString().Trim();

                        // Include the phone number if supplied
                        if (!string.IsNullOrEmpty(query.telephone))
                        {
                            XrLabel_Agency_Phone.Text = string.Format("Phone: {0}", query.telephone);
                        }
                    }
                }
            }
        }

        protected virtual void XrLabel_Title_BeforePrint(object sender, PrintEventArgs e)
        {
            ((XRLabel)sender).Text = ReportTitle;
        }

        protected virtual void XrLabel_Subtitle_BeforePrint(object sender, PrintEventArgs e)
        {
            ((XRLabel)sender).Text = ReportSubTitle;
        }

        /// <summary>
        /// Should the page layout change, recalculate the locations
        /// </summary>
        protected void HandlePageSettingsChanged(object sender, EventArgs e)
        {
            RightAlignFields(sender);
        }

        protected void HandleAfterMarginsChange(object sender, MarginsChangeEventArgs e)
        {
            RightAlignFields(sender);
        }

        protected void RightAlignFields(object sender)
        {
            UnRegisterHandlers();
            try
            {
                PrintingSystemBase ps = sender as PrintingSystemBase;
                Int32 newPageWidth = ps.PageBounds.Width - ps.PageMargins.Left - ps.PageMargins.Right;
                Int32 currentPageWidth = PageWidth - Margins.Left - Margins.Right;
                Int32 shift = currentPageWidth - newPageWidth;

                // Align the title box to the right edge
                if (shift != 0)
                {
                    XrPanel_AgencyAddress.Location = new System.Drawing.Point(newPageWidth - XrPanel_AgencyAddress.Width, XrPanel_AgencyAddress.Location.Y);
                    XrPageInfo_PageNumber.Location = new System.Drawing.Point(newPageWidth - XrPageInfo_PageNumber.Width, XrPageInfo_PageNumber.Location.Y);

                    // Adjust the margins
                    Margins.Top = ps.PageMargins.Top;
                    Margins.Bottom = ps.PageMargins.Bottom;
                    Margins.Left = ps.PageMargins.Left;
                    Margins.Right = ps.PageMargins.Right;

                    // Adjust the paper settings
                    PaperKind = ps.PageSettings.PaperKind;
                    PaperName = ps.PageSettings.PaperName;
                    Landscape = ps.PageSettings.Landscape;

                    // Recreate the document
                    CreateDocument();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}