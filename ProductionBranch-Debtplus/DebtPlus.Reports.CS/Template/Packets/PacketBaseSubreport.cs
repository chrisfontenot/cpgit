using System;

namespace DebtPlus.Reports.CS.Template.Packets
{
    public partial class PacketBaseSubreport : DebtPlus.Interfaces.Client.IClient
    {
        public PacketBaseSubreport()
            : base()
        {
            this.InitializeComponent();
        }

        [System.Xml.Serialization.XmlIgnore(), System.ComponentModel.Browsable(false)]
        public Int32 ClientId
        {
            get
            {
                var rpt = MasterReport;
                if (DesignMode || rpt == null)
                    return -1;
                return Convert.ToInt32(rpt.Parameters["ParameterClient"].Value, System.Globalization.CultureInfo.InvariantCulture);
            }
            // Do not allow the client to be changed. It is set only in the master report.
            set { }
        }
    }
}
