using System;

namespace DebtPlus.Reports.CS.Template.Packets
{
    partial class PacketBaseSubreport : DevExpress.XtraReports.UI.XtraReport
    {
        //XtraReport overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Designer
        //It can be modified using the Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.XrControlStyle_Header = new DevExpress.XtraReports.UI.XRControlStyle();
            this.XrControlStyle_Totals = new DevExpress.XtraReports.UI.XRControlStyle();
            this.XrControlStyle_HeaderPannel = new DevExpress.XtraReports.UI.XRControlStyle();
            this.XrControlStyle_GroupHeader = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            //
            //Detail
            //
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.Detail.StylePriority.UseTextAlignment = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //TopMargin
            //
            this.TopMargin.HeightF = 0f;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //BottomMargin
            //
            this.BottomMargin.HeightF = 0f;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            //
            //XrControlStyle_Header
            //
            this.XrControlStyle_Header.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_Header.ForeColor = System.Drawing.Color.Teal;
            this.XrControlStyle_Header.Name = "XrControlStyle_Header";
            this.XrControlStyle_Header.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrControlStyle_Header.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //XrControlStyle_Totals
            //
            this.XrControlStyle_Totals.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_Totals.Name = "XrControlStyle_Totals";
            this.XrControlStyle_Totals.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrControlStyle_Totals.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrControlStyle_HeaderPannel
            //
            this.XrControlStyle_HeaderPannel.BackColor = System.Drawing.Color.Teal;
            this.XrControlStyle_HeaderPannel.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_HeaderPannel.ForeColor = System.Drawing.Color.White;
            this.XrControlStyle_HeaderPannel.Name = "XrControlStyle_HeaderPannel";
            this.XrControlStyle_HeaderPannel.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
            this.XrControlStyle_HeaderPannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrControlStyle_GroupHeader
            //
            this.XrControlStyle_GroupHeader.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrControlStyle_GroupHeader.ForeColor = System.Drawing.Color.Maroon;
            this.XrControlStyle_GroupHeader.Name = "XrControlStyle_GroupHeader";
            this.XrControlStyle_GroupHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //PacketBaseSubreport
            //
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
				this.Detail,
				this.TopMargin,
				this.BottomMargin
			});
            this.Margins = new System.Drawing.Printing.Margins(25, 25, 0, 0);
            this.RequestParameters = false;
            this.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
				this.XrControlStyle_Header,
				this.XrControlStyle_Totals,
				this.XrControlStyle_HeaderPannel,
				this.XrControlStyle_GroupHeader
			});
            this.Version = "10.1";
            ((System.ComponentModel.ISupportInitialize)this).EndInit();
        }
        protected DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        protected DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        protected DevExpress.XtraReports.UI.DetailBand Detail;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_Header;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_Totals;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_HeaderPannel;
        protected DevExpress.XtraReports.UI.XRControlStyle XrControlStyle_GroupHeader;
    }
}
