using System;
using System.ComponentModel;
using DebtPlus.Data.Forms;

namespace DebtPlus.Reports.CS.Template.Packets
{
    public partial class PacketSubreport : DevExpress.XtraReports.UI.XRSubreport
    {
        public PacketSubreport()
            : base()
        {
        }

        private string privateReportDefinition = string.Empty;

        [Category("Data"), Description("Subreport's REPX source file for the design information"), DefaultValue(typeof(string), ""), Browsable(true)]
        public string ReportDefinition
        {
            get { return privateReportDefinition; }
            set
            {
                if (value != privateReportDefinition)
                {
                    privateReportDefinition = value;
                }
            }
        }

        protected override void BeforeReportPrint()
        {
            try
            {
                // Load the report before we trip the "BeforePrint" events

                if (ReportSource == null && ReportDefinition != string.Empty)
                {
                    // Try for the item in the reports directory.
                    string Fname = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportDefinition);

                    if (!System.IO.File.Exists(Fname))
                    {
                        // If it is not there then try for it in the current code file directory
                        var asm = System.Reflection.Assembly.GetExecutingAssembly();
                        string Path = System.IO.Path.GetDirectoryName(asm.Location);
                        Fname = System.IO.Path.Combine(Path, ReportDefinition);
                    }

                    var rpt = new PacketBaseSubreport();
                    try
                    {
                        rpt.LoadLayout(Fname);
                        rpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();
                        ReportSource = rpt;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("An error occurred loading the required subreport. The subreport information has not been included. Please correct the reference and retry.{1}{1}Error: {2}{1}Filename: {0}", Fname, Environment.NewLine, ex.Message), "Error loading subreport", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    }
                }
            }
            finally
            {
                // Do the standard logic for the BeforePrint event
                base.BeforeReportPrint();
            }
        }
    }
}