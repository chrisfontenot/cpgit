using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.Reports.CS.Template
{
	partial class SelectExpertForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null)
            {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.FilterControl1 = new DevExpress.XtraEditors.FilterControl();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.Bar_Status = new DevExpress.XtraBars.Bar();
			this.BarStaticItem_ItemCount = new DevExpress.XtraBars.BarStaticItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
			this.SuspendLayout();
			//
			//FilterControl1
			//
			this.FilterControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.FilterControl1.Cursor = System.Windows.Forms.Cursors.Arrow;
			this.FilterControl1.Location = new System.Drawing.Point(12, 12);
			this.FilterControl1.Name = "FilterControl1";
			this.FilterControl1.ShowDateTimeOperators = false;
			this.FilterControl1.Size = new System.Drawing.Size(303, 226);
			this.FilterControl1.TabIndex = 0;
			this.FilterControl1.Text = "FilterControl1";
			//
			//Button_OK
			//
			this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Button_OK.Location = new System.Drawing.Point(87, 242);
			this.Button_OK.MaximumSize = new System.Drawing.Size(75, 23);
			this.Button_OK.MinimumSize = new System.Drawing.Size(75, 23);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.StyleController = this.LayoutControl1;
			this.Button_OK.TabIndex = 1;
			this.Button_OK.Text = "&OK";
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.Button_Cancel);
			this.LayoutControl1.Controls.Add(this.Button_OK);
			this.LayoutControl1.Controls.Add(this.FilterControl1);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(327, 277);
			this.LayoutControl1.TabIndex = 7;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(166, 242);
			this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
			this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.StyleController = this.LayoutControl1;
			this.Button_Cancel.TabIndex = 2;
			this.Button_Cancel.Text = "&Cancel";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem3,
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(327, 277);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.FilterControl1;
			this.LayoutControlItem3.CustomizationFormText = "Filter Information";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(307, 230);
			this.LayoutControlItem3.Text = "Filter Information";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem3.TextToControlDistance = 0;
			this.LayoutControlItem3.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.Button_OK;
			this.LayoutControlItem1.CustomizationFormText = "OK Button";
			this.LayoutControlItem1.Location = new System.Drawing.Point(75, 230);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem1.Text = "OK";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem1.TextToControlDistance = 0;
			this.LayoutControlItem1.TextVisible = false;
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.Button_Cancel;
			this.LayoutControlItem2.CustomizationFormText = "Cancel Button";
			this.LayoutControlItem2.Location = new System.Drawing.Point(154, 230);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem2.Text = "Cancel";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem2.TextToControlDistance = 0;
			this.LayoutControlItem2.TextVisible = false;
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.AllowHotTrack = false;
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 230);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(75, 27);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.AllowHotTrack = false;
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(233, 230);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(74, 27);
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//barManager1
			//
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar_Status });
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] { this.BarStaticItem_ItemCount });
			this.barManager1.MaxItemId = 1;
			this.barManager1.StatusBar = this.Bar_Status;
			//
			//Bar_Status
			//
			this.Bar_Status.BarName = "Status bar";
			this.Bar_Status.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
			this.Bar_Status.DockCol = 0;
			this.Bar_Status.DockRow = 0;
			this.Bar_Status.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
			this.Bar_Status.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_ItemCount) });
			this.Bar_Status.OptionsBar.AllowQuickCustomization = false;
			this.Bar_Status.OptionsBar.DrawDragBorder = false;
			this.Bar_Status.OptionsBar.UseWholeRow = true;
			this.Bar_Status.Text = "Status bar";
			//
			//BarStaticItem_ItemCount
			//
			this.BarStaticItem_ItemCount.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
			this.BarStaticItem_ItemCount.Caption = "Item Count";
			this.BarStaticItem_ItemCount.Id = 0;
			this.BarStaticItem_ItemCount.Name = "BarStaticItem_ItemCount";
			this.BarStaticItem_ItemCount.TextAlignment = System.Drawing.StringAlignment.Near;
			this.BarStaticItem_ItemCount.Width = 32;
			//
			//barDockControlTop
			//
			this.barDockControlTop.CausesValidation = false;
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(327, 0);
			//
			//barDockControlBottom
			//
			this.barDockControlBottom.CausesValidation = false;
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 277);
			this.barDockControlBottom.Size = new System.Drawing.Size(327, 28);
			//
			//barDockControlLeft
			//
			this.barDockControlLeft.CausesValidation = false;
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 277);
			//
			//barDockControlRight
			//
			this.barDockControlRight.CausesValidation = false;
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(327, 0);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 277);
			//
			//SelectExpertForm
			//
			this.AcceptButton = this.Button_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(327, 305);
			this.Controls.Add(this.LayoutControl1);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "SelectExpertForm";
			this.Text = "Report Selection Criteria";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
			this.ResumeLayout(false);

		}
		private DevExpress.XtraEditors.FilterControl FilterControl1;
		private DevExpress.XtraEditors.SimpleButton Button_OK;
		private DevExpress.XtraEditors.SimpleButton Button_Cancel;
		private DevExpress.XtraBars.BarManager barManager1;
		private DevExpress.XtraBars.Bar Bar_Status;
		private DevExpress.XtraBars.BarDockControl barDockControlTop;
		private DevExpress.XtraBars.BarDockControl barDockControlBottom;
		private DevExpress.XtraBars.BarDockControl barDockControlLeft;
		private DevExpress.XtraBars.BarDockControl barDockControlRight;
		private DevExpress.XtraLayout.LayoutControl LayoutControl1;
		private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		private DevExpress.XtraBars.BarStaticItem BarStaticItem_ItemCount;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
	}
}
