namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class DateReportParametersForm
    {
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        protected DevExpress.XtraEditors.GroupControl XrGroup_param_08_1;
        protected DevExpress.XtraEditors.ComboBoxEdit XrCombo_param_08_1;
        protected DevExpress.XtraEditors.DateEdit XrDate_param_08_2;
        protected DevExpress.XtraEditors.DateEdit XrDate_param_08_1;
        protected DevExpress.XtraEditors.LabelControl Label3;
        protected DevExpress.XtraEditors.LabelControl Label4;
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.XrGroup_param_08_1 = new DevExpress.XtraEditors.GroupControl();
            this.XrCombo_param_08_1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.XrDate_param_08_2 = new DevExpress.XtraEditors.DateEdit();
            this.XrDate_param_08_1 = new DevExpress.XtraEditors.DateEdit();
            this.Label3 = new DevExpress.XtraEditors.LabelControl();
            this.Label4 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GroupBox1
            //
            this.XrGroup_param_08_1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowOnly;
            this.XrGroup_param_08_1.Controls.Add(this.XrCombo_param_08_1);
            this.XrGroup_param_08_1.Controls.Add(this.XrDate_param_08_2);
            this.XrGroup_param_08_1.Controls.Add(this.XrDate_param_08_1);
            this.XrGroup_param_08_1.Controls.Add(this.Label3);
            this.XrGroup_param_08_1.Controls.Add(this.Label4);
            this.XrGroup_param_08_1.Location = new System.Drawing.Point(8, 9);
            this.XrGroup_param_08_1.Name = "GroupBox1";
            this.XrGroup_param_08_1.Size = new System.Drawing.Size(216, 112);
            this.ToolTipController1.SetSuperTip(this.XrGroup_param_08_1, null);
            this.XrGroup_param_08_1.TabIndex = 0;
            this.XrGroup_param_08_1.Text = " Date Range ";
            //
            //ComboboxEdit_DateRange
            //
            this.XrCombo_param_08_1.EditValue = "";
            this.XrCombo_param_08_1.Location = new System.Drawing.Point(16, 26);
            this.XrCombo_param_08_1.Name = "ComboboxEdit_DateRange";
            this.XrCombo_param_08_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.XrCombo_param_08_1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.XrCombo_param_08_1.Size = new System.Drawing.Size(184, 20);
            this.XrCombo_param_08_1.TabIndex = 0;
            this.XrCombo_param_08_1.ToolTip = "Select a standard period for the date ranges";
            this.XrCombo_param_08_1.ToolTipController = this.ToolTipController1;
            //
            //DateEdit_Ending
            //
            this.XrDate_param_08_2.EditValue = new System.DateTime(2005, 12, 24, 0, 0, 0, 0);
            this.XrDate_param_08_2.Location = new System.Drawing.Point(112, 78);
            this.XrDate_param_08_2.Name = "DateEdit_Ending";
            this.XrDate_param_08_2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.XrDate_param_08_2.Properties.NullDate = "";
            this.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.XrDate_param_08_2.Size = new System.Drawing.Size(88, 20);
            this.XrDate_param_08_2.TabIndex = 4;
            this.XrDate_param_08_2.ToolTip = "Enter the ending date for the report";
            this.XrDate_param_08_2.ToolTipController = this.ToolTipController1;
            //
            //DateEdit_Starting
            //
            this.XrDate_param_08_1.EditValue = new System.DateTime(2005, 8, 24, 0, 0, 0, 0);
            this.XrDate_param_08_1.Location = new System.Drawing.Point(112, 52);
            this.XrDate_param_08_1.Name = "DateEdit_Starting";
            this.XrDate_param_08_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.XrDate_param_08_1.Properties.NullDate = "";
            this.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.XrDate_param_08_1.Size = new System.Drawing.Size(88, 20);
            this.XrDate_param_08_1.TabIndex = 2;
            this.XrDate_param_08_1.ToolTip = "Enter the starting date for the report";
            this.XrDate_param_08_1.ToolTipController = this.ToolTipController1;
            //
            //Label3
            //
            this.Label3.Location = new System.Drawing.Point(16, 80);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(69, 13);
            this.Label3.TabIndex = 3;
            this.Label3.Text = "&Ending Period:";
            this.Label3.ToolTipController = this.ToolTipController1;
            //
            //Label4
            //
            this.Label4.Location = new System.Drawing.Point(16, 54);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(75, 13);
            this.Label4.TabIndex = 1;
            this.Label4.Text = "&Starting Period:";
            this.Label4.ToolTipController = this.ToolTipController1;
            //
            //DateReportParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(336, 137);
            this.Controls.Add(this.XrGroup_param_08_1);
            this.Name = "DateReportParametersForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.XrGroup_param_08_1, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).EndInit();
            this.ResumeLayout(false);
        }
    }
}
