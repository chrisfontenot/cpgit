﻿namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class CounselorParametersForm : Forms.ReportParametersForm
    {
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        protected DevExpress.XtraEditors.LabelControl Label1;
        protected DevExpress.XtraEditors.LookUpEdit XrCounselor_param_02_1;
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.XrCounselor_param_02_1 = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCounselor_param_02_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ButtonOK
            //
            this.ButtonOK.Location = new System.Drawing.Point(247, 16);
            this.ButtonOK.TabIndex = 2;
            //
            //ButtonCancel
            //
            this.ButtonCancel.Location = new System.Drawing.Point(247, 48);
            this.ButtonCancel.TabIndex = 3;
            //
            //Label1
            //
            this.Label1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Label1.Location = new System.Drawing.Point(8, 35);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(52, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Counse&lor:";
            //
            //XrCounselor_param_02_1
            //
            this.XrCounselor_param_02_1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.XrCounselor_param_02_1.Location = new System.Drawing.Point(66, 32);
            this.XrCounselor_param_02_1.Name = "XrCounselor_param_02_1";
            this.XrCounselor_param_02_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.XrCounselor_param_02_1.Properties.PopupWidth = 300;
            this.XrCounselor_param_02_1.Properties.ShowFooter = false;
            this.XrCounselor_param_02_1.Properties.ShowHeader = false;
            this.XrCounselor_param_02_1.Properties.SortColumnIndex = 1;
            this.XrCounselor_param_02_1.Size = new System.Drawing.Size(175, 20);
            this.XrCounselor_param_02_1.TabIndex = 1;
            this.XrCounselor_param_02_1.ToolTipController = this.ToolTipController1;
            //
            //CounselorParametersForm
            //
            this.ClientSize = new System.Drawing.Size(335, 157);
            this.Controls.Add(this.XrCounselor_param_02_1);
            this.Controls.Add(this.Label1);
            this.Name = "CounselorParametersForm";
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.Label1, 0);
            this.Controls.SetChildIndex(this.XrCounselor_param_02_1, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCounselor_param_02_1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
