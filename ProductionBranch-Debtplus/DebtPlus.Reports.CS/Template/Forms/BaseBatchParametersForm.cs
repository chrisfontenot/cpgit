using System;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class BaseBatchParametersForm : ReportParametersForm
    {
        public BaseBatchParametersForm()
            : base()
        {
            InitializeComponent();

            // Allow for the form to be designed without trapping.
            if (!InDesignMode)
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Load += RequestParametersForm_Load;
            XrRadio_param_09_1.EditValueChanging += XrRadio_param_09_1_EditValueChanging;
            XrText_param_09_1.EditValueChanged += Control_EditValueChanged;
            XrLookup_param_09_1.EditValueChanged += Control_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= RequestParametersForm_Load;
            XrRadio_param_09_1.EditValueChanging -= XrRadio_param_09_1_EditValueChanging;
            XrText_param_09_1.EditValueChanged -= Control_EditValueChanged;
            XrLookup_param_09_1.EditValueChanged -= Control_EditValueChanged;
        }

        /// <summary>
        /// Get the selected ID from the input fields
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected System.Nullable<Int32> getSelectedID()
        {
            // Use the value from the lookup control.
            if (XrRadio_param_09_1.Checked)
            {
                return DebtPlus.Utils.Nulls.v_Int32(XrLookup_param_09_1.EditValue);
            }

            // Retrieve the value from the text buffer
            return DebtPlus.Utils.Nulls.v_Int32(XrText_param_09_1.EditValue);
        }

        /// <summary>
        /// Handle the change in the selection radio button
        /// </summary>
        private void XrRadio_param_09_1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                System.Nullable<System.Int32> value = getSelectedID();

                // Correct the "new" control's value with the old one.
                XrLookup_param_09_1.EditValue = value;
                XrText_param_09_1.EditValue = value;

                // Enable or disable the input field accordingly
                XrText_param_09_1.Enabled = !(bool)e.NewValue;
                XrLookup_param_09_1.Enabled = !XrText_param_09_1.Enabled;

                ButtonOK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        protected System.Windows.Forms.GroupBox XrGroup_param_09_1;
        protected DevExpress.XtraEditors.CheckEdit XrRadio_param_09_1;
        protected DevExpress.XtraEditors.CheckEdit XrRadio_param_09_2;
        protected DevExpress.XtraEditors.LabelControl XrLbl_param_09_1;
        protected DevExpress.XtraEditors.TextEdit XrText_param_09_1;

        protected DevExpress.XtraEditors.LookUpEdit XrLookup_param_09_1;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.XrGroup_param_09_1 = new System.Windows.Forms.GroupBox();
            this.XrLbl_param_09_1 = new DevExpress.XtraEditors.LabelControl();
            this.XrRadio_param_09_2 = new DevExpress.XtraEditors.CheckEdit();
            this.XrRadio_param_09_1 = new DevExpress.XtraEditors.CheckEdit();
            this.XrText_param_09_1 = new DevExpress.XtraEditors.TextEdit();
            this.XrLookup_param_09_1 = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.XrGroup_param_09_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrRadio_param_09_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrRadio_param_09_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrText_param_09_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrLookup_param_09_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ButtonOK
            //
            this.ButtonOK.Location = new System.Drawing.Point(248, 18);
            this.ButtonOK.Size = new System.Drawing.Size(80, 28);
            this.ButtonOK.TabIndex = 3;
            //
            //ButtonCancel
            //
            this.ButtonCancel.Location = new System.Drawing.Point(248, 56);
            this.ButtonCancel.Size = new System.Drawing.Size(80, 28);
            this.ButtonCancel.TabIndex = 4;
            //
            //DefaultLookAndFeel1
            //
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            //
            //XrGroup_param_09_1
            //
            this.XrGroup_param_09_1.Controls.Add(this.XrLbl_param_09_1);
            this.XrGroup_param_09_1.Controls.Add(this.XrRadio_param_09_2);
            this.XrGroup_param_09_1.Controls.Add(this.XrRadio_param_09_1);
            this.XrGroup_param_09_1.Controls.Add(this.XrText_param_09_1);
            this.XrGroup_param_09_1.Controls.Add(this.XrLookup_param_09_1);
            this.XrGroup_param_09_1.Location = new System.Drawing.Point(8, 9);
            this.XrGroup_param_09_1.Name = "XrGroup_param_09_1";
            this.XrGroup_param_09_1.Size = new System.Drawing.Size(232, 112);
            this.XrGroup_param_09_1.TabIndex = 0;
            this.XrGroup_param_09_1.TabStop = false;
            this.XrGroup_param_09_1.Text = " Batch ID  ";
            //
            //XrLbl_param_09_1
            //
            this.XrLbl_param_09_1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrLbl_param_09_1.Location = new System.Drawing.Point(32, 54);
            this.XrLbl_param_09_1.Name = "XrLbl_param_09_1";
            this.XrLbl_param_09_1.Size = new System.Drawing.Size(122, 13);
            this.XrLbl_param_09_1.TabIndex = 2;
            this.XrLbl_param_09_1.Text = "Or enter a specific ID";
            //
            //XrRadio_param_09_2
            //
            this.XrRadio_param_09_2.AllowDrop = true;
            this.XrRadio_param_09_2.Location = new System.Drawing.Point(8, 78);
            this.XrRadio_param_09_2.Name = "XrRadio_param_09_2";
            this.XrRadio_param_09_2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.XrRadio_param_09_2.Properties.RadioGroupIndex = 0;
            this.XrRadio_param_09_2.Size = new System.Drawing.Size(20, 21);
            this.XrRadio_param_09_2.TabIndex = 3;
            this.XrRadio_param_09_2.TabStop = false;
            this.XrRadio_param_09_2.Tag = "text";
            //
            //XrRadio_param_09_1
            //
            this.XrRadio_param_09_1.EditValue = true;
            this.XrRadio_param_09_1.Location = new System.Drawing.Point(8, 28);
            this.XrRadio_param_09_1.Name = "XrRadio_param_09_1";
            this.XrRadio_param_09_1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.XrRadio_param_09_1.Properties.RadioGroupIndex = 0;
            this.XrRadio_param_09_1.Size = new System.Drawing.Size(20, 21);
            this.XrRadio_param_09_1.TabIndex = 0;
            this.XrRadio_param_09_1.Tag = "list";
            //
            //XrText_param_09_1
            //
            this.XrText_param_09_1.Enabled = false;
            this.XrText_param_09_1.Location = new System.Drawing.Point(32, 78);
            this.XrText_param_09_1.Name = "XrText_param_09_1";
            this.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = true;
            this.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XrText_param_09_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0";
            this.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.XrText_param_09_1.Properties.EditFormat.FormatString = "f0";
            this.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.XrText_param_09_1.Properties.Mask.BeepOnError = true;
            this.XrText_param_09_1.Properties.Mask.EditMask = "\\d*";
            this.XrText_param_09_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.XrText_param_09_1.Properties.ValidateOnEnterKey = true;
            this.XrText_param_09_1.Size = new System.Drawing.Size(64, 20);
            this.XrText_param_09_1.TabIndex = 4;
            this.XrText_param_09_1.ToolTip = "Enter a specific batch if it is not included in the above list";
            this.XrText_param_09_1.ToolTipController = this.ToolTipController1;
            //
            //XrLookup_param_09_1
            //
            this.XrLookup_param_09_1.Location = new System.Drawing.Point(32, 26);
            this.XrLookup_param_09_1.Name = "XrLookup_param_09_1";
            this.XrLookup_param_09_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.XrLookup_param_09_1.Properties.NullText = "";
            this.XrLookup_param_09_1.Properties.PopupWidth = 500;
            this.XrLookup_param_09_1.Properties.ShowFooter = false;
            this.XrLookup_param_09_1.Properties.SortColumnIndex = 1;
            this.XrLookup_param_09_1.Size = new System.Drawing.Size(184, 20);
            this.XrLookup_param_09_1.TabIndex = 1;
            this.XrLookup_param_09_1.ToolTipController = this.ToolTipController1;
            //
            //BaseBatchParametersForm
            //
            this.ClientSize = new System.Drawing.Size(336, 133);
            this.Controls.Add(this.XrGroup_param_09_1);
            this.Name = "BaseBatchParametersForm";
            this.Text = "Batch Selection";
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.XrGroup_param_09_1, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.XrGroup_param_09_1.ResumeLayout(false);
            this.XrGroup_param_09_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrRadio_param_09_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrRadio_param_09_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrText_param_09_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrLookup_param_09_1.Properties).EndInit();
            this.ResumeLayout(false);
        }

        /// <summary>
        /// Handle the load event for the form
        /// </summary>
        private void RequestParametersForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ButtonOK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if the input form is valid for a submission
        /// </summary>
        protected override bool HasErrors()
        {
            return !getSelectedID().HasValue;
        }

        /// <summary>
        /// Handle a change in the text box's value
        /// </summary>
        private void Control_EditValueChanged(object sender, System.EventArgs e)
        {
            ButtonOK.Enabled = !HasErrors();
        }
    }
}