using System;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class DatedCreditorParametersForm
    {
        private string privateCreditor = string.Empty;

        public string Parameter_Creditor
        {
            get { return privateCreditor; }
            set { privateCreditor = value; }
        }

        public DatedCreditorParametersForm(DebtPlus.Utils.DateRange defaultItem)
            : base(defaultItem)
        {
            InitializeComponent();
            this.Load += RequestParametersForm_Load;
        }

        public DatedCreditorParametersForm()
            : base()
        {
            InitializeComponent();
            this.Load += RequestParametersForm_Load;
        }

        private void RequestParametersForm_Load(object sender, EventArgs e)
        {
            // Define the period for the item
            XrCombo_param_08_1.SelectedIndex = (int)DebtPlus.Utils.DateRange.Last_Month;

            // Load the creditor id into the control
            if (Parameter_Creditor != string.Empty)
            {
                XrCreditor_param_06_1.Enabled = false;
                XrCreditor_param_06_1.Text = Parameter_Creditor;
            }
            else
            {
                XrCreditor_param_06_1.EditValue = null;
                XrCreditor_param_06_1.Enabled = true;
            }

            ButtonOK.Enabled = !HasErrors();

            // Add the handler to detect changes in the creditor id
            XrCreditor_param_06_1.EditValueChanging += CreditorID1_EditValueChanging;
        }

        private void CreditorID1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (e.NewValue == null)
            {
                Parameter_Creditor = string.Empty;
                ButtonOK.Enabled = false;
            }
            else if (object.ReferenceEquals(e.NewValue, DBNull.Value))
            {
                Parameter_Creditor = string.Empty;
                ButtonOK.Enabled = false;
            }
            else
            {
                Parameter_Creditor = Convert.ToString(e.NewValue);
                ButtonOK.Enabled = !HasErrors();
            }
        }

        protected override bool HasErrors()
        {
            return base.HasErrors() || Parameter_Creditor == string.Empty;
        }
    }
}
