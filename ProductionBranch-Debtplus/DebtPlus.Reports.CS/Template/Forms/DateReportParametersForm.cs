using System;
using System.ComponentModel;
using DebtPlus.Data.Controls;
using DevExpress.XtraEditors;
namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class DateReportParametersForm : ReportParametersForm
	{
		public DateReportParametersForm() : this(DebtPlus.Utils.DateRange.Today)
		{
		}

		public DateReportParametersForm(DebtPlus.Utils.DateRange defaultItem) : base()
		{
			InitializeComponent();
			this.Load += DateReportParametersForm_Load;
			DefaultSelectedItem = defaultItem;
		}

		// Default item for the new form
		DebtPlus.Utils.DateRange DefaultSelectedItem = DebtPlus.Utils.DateRange.Today;
		public virtual DateTime Parameter_FromDate {
			get { return Convert.ToDateTime(XrDate_param_08_1.EditValue); }
		}

		public virtual DateTime Parameter_ToDate {
			get { return Convert.ToDateTime(XrDate_param_08_2.EditValue); }
		}


		private void DateReportParametersForm_Load(object sender, EventArgs e)
		{
			XrDate_param_08_1.EditValue = System.DateTime.Now.Date;
			XrDate_param_08_1.Properties.ShowClear = false;
			XrDate_param_08_1.Properties.ValidateOnEnterKey = true;

			XrDate_param_08_2.EditValue = System.DateTime.Now.Date;
			XrDate_param_08_2.Properties.ShowClear = false;
			XrDate_param_08_2.Properties.ValidateOnEnterKey = true;

			XrCombo_param_08_1.Properties.Items.Clear();
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Today", DebtPlus.Utils.DateRange.Today));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Tomorrow", DebtPlus.Utils.DateRange.Tomorrow));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Yesterday", DebtPlus.Utils.DateRange.Yesterday));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("This Week", DebtPlus.Utils.DateRange.This_Week));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Last Week", DebtPlus.Utils.DateRange.Last_Week));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("This Month", DebtPlus.Utils.DateRange.This_Month));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Last Month", DebtPlus.Utils.DateRange.Last_Month));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Last 30 Days", DebtPlus.Utils.DateRange.Days_30));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Last 60 Days", DebtPlus.Utils.DateRange.Days_60));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Last 90 Days", DebtPlus.Utils.DateRange.Days_90));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Quarter to date", DebtPlus.Utils.DateRange.Quarter_To_Date));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Last Quarter", DebtPlus.Utils.DateRange.Last_Quarter));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Year to date", DebtPlus.Utils.DateRange.Year_To_Date));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Last year", DebtPlus.Utils.DateRange.Last_Year));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("All Transactions", DebtPlus.Utils.DateRange.All_Transactions));
			XrCombo_param_08_1.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Specific Date Range", DebtPlus.Utils.DateRange.Specific_Date_Range));

			// Find the item selected as the default
			foreach (ComboboxItem item in XrCombo_param_08_1.Properties.Items)
            {
				if ((DebtPlus.Utils.DateRange)item.value == DefaultSelectedItem)
                {
					XrCombo_param_08_1.SelectedIndex = XrCombo_param_08_1.Properties.Items.IndexOf(item);
					break;
				}
			}

			// Correct the date values if we have chosen something in the list by default
			AdjustDateValues(DefaultSelectedItem);

			// Add the parameters
			XrDate_param_08_1.Validating += DateEdit_Validating;
			XrDate_param_08_2.Validating += DateEdit_Validating;
			XrCombo_param_08_1.SelectedIndexChanged += ComboBoxEdit_DateRange_EditValueChanged;

			// Enable or disable the OK button
			EnableOK();
		}

		protected void DateEdit_Validating(object sender, CancelEventArgs e)
		{
			string errorMessage = string.Empty;

			// Try to convert the date
			var ctl = (DateEdit)sender;
			DateTime testDate = default(DateTime);
			if (!System.DateTime.TryParse(ctl.Text, out testDate))
            {
				errorMessage = "Invalid Date. Use a format like MM/DD/YYYY";
				e.Cancel = true;
			}

			// Set the error text
			DxErrorProvider1.SetError((System.Windows.Forms.Control)sender, errorMessage);

			// Enable or disable the OK button
			EnableOK();
		}

		protected override bool HasErrors()
		{
			// Look for any error condition. Disable the OK button if the form is not complete.
            if (DxErrorProvider1.GetError(XrDate_param_08_2) != string.Empty)
            {
                return true;
            }

			if (DxErrorProvider1.GetError(XrDate_param_08_1) != string.Empty)
            {
                return true;
            }

            if (DxErrorProvider1.GetError(XrCombo_param_08_1) != string.Empty)
            {
                return true;
            }

			// There is no shown error.
			return false;
		}

		private void EnableOK()
		{
			ButtonOK.Enabled = !HasErrors();
		}

		protected virtual void ComboBoxEdit_DateRange_EditValueChanged(object sender, EventArgs e)
		{
			AdjustDateValues((DebtPlus.Utils.DateRange)((DebtPlus.Data.Controls.ComboboxItem)XrCombo_param_08_1.SelectedItem).value);
		}

		protected void AdjustDateValues(DebtPlus.Utils.DateRange SelectedItem)
		{
			// Enable or disable the date fields
			XrDate_param_08_2.Enabled = false;
			XrDate_param_08_1.Enabled = false;

            var Today = System.DateTime.Now.Date;

			// Determine the relative dates for the period
			switch (SelectedItem)
            {
				case DebtPlus.Utils.DateRange.Today:
					XrDate_param_08_1.EditValue = Today;
					XrDate_param_08_2.EditValue = Today;
					break;

				case DebtPlus.Utils.DateRange.Yesterday:
					XrDate_param_08_1.EditValue = Today.AddDays(-1);
					XrDate_param_08_2.EditValue = Today.AddDays(-1);
					break;

				case DebtPlus.Utils.DateRange.Tomorrow:
					XrDate_param_08_1.EditValue = Today.AddDays(1);
					XrDate_param_08_2.EditValue = Today.AddDays(1);
					break;

				case DebtPlus.Utils.DateRange.This_Week:
					XrDate_param_08_1.EditValue = Today.AddDays((double)(0 - Today.DayOfWeek));
					XrDate_param_08_2.EditValue = Today;
					break;

				case DebtPlus.Utils.DateRange.Last_Week:
					System.DateTime LastWeek_1 = Today.AddDays((double) (-1 - Today.DayOfWeek));
                    XrDate_param_08_2.EditValue = LastWeek_1;
                    XrDate_param_08_1.EditValue = LastWeek_1.AddDays(-6);
					break;

				case DebtPlus.Utils.DateRange.This_Month:
					XrDate_param_08_1.EditValue = new System.DateTime(Today.Year, Today.Month, 1);
					XrDate_param_08_2.EditValue = Today;
					break;

				case DebtPlus.Utils.DateRange.Last_Month:
					System.DateTime ThisMonth = new System.DateTime(Today.Year, Today.Month, 1);
					XrDate_param_08_1.EditValue = ThisMonth.AddMonths(-1);
					XrDate_param_08_2.EditValue = ThisMonth.AddDays(-1);
					break;

				case DebtPlus.Utils.DateRange.Days_30:
					System.DateTime ThisMonth_2 = Today.Date;
                    XrDate_param_08_2.EditValue = ThisMonth_2;
                    XrDate_param_08_1.EditValue = ThisMonth_2.AddMonths(-1);
					break;

				case DebtPlus.Utils.DateRange.Days_60:
					System.DateTime ThisMonth_3 = Today.Date;
                    XrDate_param_08_2.EditValue = ThisMonth_3;
                    XrDate_param_08_1.EditValue = ThisMonth_3.AddMonths(-2);
					break;

				case DebtPlus.Utils.DateRange.Days_90:
					System.DateTime ThisMonth_4 = Today.Date;
                    XrDate_param_08_2.EditValue = ThisMonth_4;
                    XrDate_param_08_1.EditValue = ThisMonth_4.AddMonths(-3);
					break;

				case DebtPlus.Utils.DateRange.Quarter_To_Date:
					System.DateTime ThisMonth_5 = new System.DateTime(Today.Year, Today.Month, 1);
                    switch (ThisMonth_5.Month)
                    {
						case 1:
						case 4:
						case 7:
						case 10:
							break;
						case 2:
						case 5:
						case 8:
						case 11:
                            ThisMonth_5 = ThisMonth_5.AddMonths(-1);
							break;
						case 3:
						case 6:
						case 9:
						case 12:
                            ThisMonth_5 = ThisMonth_5.AddMonths(-2);
							break;
					}
                    XrDate_param_08_1.EditValue = ThisMonth_5;
					XrDate_param_08_2.EditValue = Today;
					break;

				case DebtPlus.Utils.DateRange.Last_Quarter:
					System.DateTime ThisMonth_6 = new System.DateTime(Today.Year, Today.Month, 1);
                    switch (ThisMonth_6.Month)
                    {
						case 1:
						case 4:
						case 7:
						case 10:
							break;
						case 2:
						case 5:
						case 8:
						case 11:
                            ThisMonth_6 = ThisMonth_6.AddMonths(-1);
							break;
						case 3:
						case 6:
						case 9:
						case 12:
                            ThisMonth_6 = ThisMonth_6.AddMonths(-2);
							break;
					}
                    XrDate_param_08_1.EditValue = ThisMonth_6.AddMonths(-3);
                    XrDate_param_08_2.EditValue = ThisMonth_6.AddDays(-1);
					break;

				case DebtPlus.Utils.DateRange.Year_To_Date:
					XrDate_param_08_1.EditValue = new System.DateTime(Today.Year, 1, 1);
					XrDate_param_08_2.EditValue = Today.Date;
					break;

				case DebtPlus.Utils.DateRange.Last_Year:
					System.DateTime ThisYear_2 = new System.DateTime(Today.Year, 1, 1);
                    XrDate_param_08_1.EditValue = ThisYear_2.AddYears(-1);
                    XrDate_param_08_2.EditValue = ThisYear_2.AddDays(-1);
					break;

				case DebtPlus.Utils.DateRange.All_Transactions:
					XrDate_param_08_1.EditValue = new DateTime(1900, 1, 1);
					XrDate_param_08_2.EditValue = new DateTime(2099, 12, 31);
					break;

				case DebtPlus.Utils.DateRange.Specific_Date_Range:
					XrDate_param_08_2.Enabled = true;
					XrDate_param_08_1.Enabled = true;
					break;
			}
		}
	}
}
