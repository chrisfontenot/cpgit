using System.Linq;
using DebtPlus.LINQ;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class ProposalParametersForm : DepositParametersForm
    {
        /// <summary>
        /// Should closed batches be shown?
        /// </summary>
        public bool ShowClosedBatches { get; set; }

        public ProposalParametersForm()
            : base()
        {
            InitializeComponent();
        }

        protected override System.Collections.Generic.List<DepositParametersForm.Lookup_Item> getRecords()
        {
            using (BusinessContext bc = new BusinessContext())
            {
                // Start with the base query
                var q = bc.proposal_batch_ids.Join(bc.banks, pr => pr.bank, b => b.Id, (pr, b) => new { pr = pr, b = b }).OrderByDescending(x => x.pr.date_created).Take(50);

                // If we don't want closed batches then discard them here.
                if (!ShowClosedBatches)
                {
                    q = q.Where(x => x.pr.date_closed == null);
                }

                // Return the list.
                return q.Select(x => new Lookup_Item() { Id = x.pr.Id, date_created = x.pr.date_created, created_by = x.pr.created_by, bank = x.b.bank_name, note = x.pr.note, proposal_type = x.pr.proposal_type, date_transmitted = x.pr.date_transmitted, date_closed = x.pr.date_closed, bank_type = x.b.type }).ToList();
            }
        }

        protected override void load_Lookup()
        {
            base.load_Lookup();

            // Reconfigure the lookup for proposals
            this.XrLookup_param_09_1.ToolTip = "Choose a proposal batch from the list if desired";

            XrLookup_param_09_1.Properties.Columns.Clear();
            this.XrLookup_param_09_1.Properties.Columns.AddRange(new LookUpColumnInfo[] {
				new LookUpColumnInfo("Id", "ID", 5, FormatType.Numeric, "f0", true, HorzAlignment.Far),
				new LookUpColumnInfo("note", "description", 20, FormatType.None, "", true, HorzAlignment.Near, ColumnSortOrder.Ascending),
				new LookUpColumnInfo("date_created", "Date", 8, FormatType.DateTime, "d", true, HorzAlignment.Far, ColumnSortOrder.Descending),
				new LookUpColumnInfo("formatted_bank_type", "Bank", 8, FormatType.None, "", true, HorzAlignment.Default, ColumnSortOrder.Ascending),
				new LookUpColumnInfo("formatted_proposal_type", "Type", 9, FormatType.None, "", true, HorzAlignment.Default, ColumnSortOrder.Ascending)
			});

            // Sort the item by the date
            XrLookup_param_09_1.Properties.SortColumnIndex = 2;
        }
    }
}
