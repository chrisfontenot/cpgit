namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class DatedOfficeParametersForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DebtPlus.Data.Controls.ComboboxItem ComboboxItem1 = new DebtPlus.Data.Controls.ComboboxItem();
            this.XrOffice_param_07_1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrOffice_param_07_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ComboboxEdit_DateRange
            //
            ComboboxItem1.tag = null;
            ComboboxItem1.value = DebtPlus.Utils.DateRange.Today;
            this.XrCombo_param_08_1.EditValue = ComboboxItem1;
            //
            //DateEdit_Ending
            //
            this.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton()
			});
            //
            //DateEdit_Starting
            //
            this.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton()
			});
            //
            //ButtonOK
            //
            this.ButtonOK.TabIndex = 3;
            //
            //ButtonCancel
            //
            this.ButtonCancel.TabIndex = 4;
            //
            //LookUpEdit1
            //
            this.XrOffice_param_07_1.Location = new System.Drawing.Point(48, 127);
            this.XrOffice_param_07_1.Name = "LookUpEdit1";
            this.XrOffice_param_07_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.XrOffice_param_07_1.Properties.NullText = "";
            this.XrOffice_param_07_1.Properties.ShowFooter = false;
            this.XrOffice_param_07_1.Size = new System.Drawing.Size(280, 20);
            this.XrOffice_param_07_1.TabIndex = 2;
            this.XrOffice_param_07_1.ToolTipController = this.ToolTipController1;
            this.XrOffice_param_07_1.Properties.SortColumnIndex = 1;
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(13, 131);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(29, 13);
            this.LabelControl1.TabIndex = 1;
            this.LabelControl1.Text = "Office";
            //
            //DatedOfficeParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 158);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.XrOffice_param_07_1);
            this.Name = "DatedOfficeParametersForm";
            this.Controls.SetChildIndex(this.XrOffice_param_07_1, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.XrGroup_param_08_1, 0);
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrOffice_param_07_1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        protected DevExpress.XtraEditors.LookUpEdit XrOffice_param_07_1;
        protected DevExpress.XtraEditors.LabelControl LabelControl1;
    }
}
