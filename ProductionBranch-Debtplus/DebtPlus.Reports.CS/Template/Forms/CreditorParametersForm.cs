using System;
using DebtPlus.UI.Creditor.Widgets.Controls;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class CreditorParametersForm : ReportParametersForm
    {
        public string Parameter_Creditor
        {
            get
            {
                if (XrCreditor_param_03_1.Text.Length < 3)
                {
                    return string.Empty;
                }
                return XrCreditor_param_03_1.Text.Trim();
            }
        }

        public CreditorParametersForm()
            : base()
        {
            InitializeComponent();
            this.Load += ParametersForm_Load;
            XrCreditor_param_03_1.EditValueChanged += CreditorID1_EditValueChanged;
        }

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        protected CreditorID XrCreditor_param_03_1;

        protected DevExpress.XtraEditors.LabelControl Label1;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditorParametersForm));
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.XrCreditor_param_03_1 = new CreditorID();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCreditor_param_03_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ButtonOK
            //
            this.ButtonOK.Location = new System.Drawing.Point(162, 16);
            this.ButtonOK.TabIndex = 2;
            //
            //ButtonCancel
            //
            this.ButtonCancel.Location = new System.Drawing.Point(162, 48);
            this.ButtonCancel.TabIndex = 3;
            //
            //Label1
            //
            this.Label1.Location = new System.Drawing.Point(16, 21);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(43, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Creditor:";
            //
            //CreditorID1
            //
            this.XrCreditor_param_03_1.EditValue = null;
            this.XrCreditor_param_03_1.Location = new System.Drawing.Point(65, 18);
            this.XrCreditor_param_03_1.Name = "CreditorID1";
            this.XrCreditor_param_03_1.Properties.Buttons.Add(new DebtPlus.UI.Common.Controls.SearchButton());
            this.XrCreditor_param_03_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.XrCreditor_param_03_1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.XrCreditor_param_03_1.Properties.Mask.BeepOnError = true;
            this.XrCreditor_param_03_1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.XrCreditor_param_03_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.XrCreditor_param_03_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.XrCreditor_param_03_1.Properties.MaxLength = 10;
            this.XrCreditor_param_03_1.Size = new System.Drawing.Size(86, 20);
            this.XrCreditor_param_03_1.TabIndex = 1;
            //
            //CreditorParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(250, 88);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.XrCreditor_param_03_1);
            this.Name = "CreditorParametersForm";
            this.TopMost = false;
            this.Controls.SetChildIndex(this.XrCreditor_param_03_1, 0);
            this.Controls.SetChildIndex(this.Label1, 0);
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCreditor_param_03_1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        protected override bool HasErrors()
        {
            return Parameter_Creditor == string.Empty;
        }

        private void ParametersForm_Load(object sender, EventArgs e)
        {
            ButtonOK.Enabled = !HasErrors();
        }

        private void CreditorID1_EditValueChanged(object sender, EventArgs e)
        {
            ButtonOK.Enabled = !HasErrors();
        }
    }
}