using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class DisbursementParametersForm : DepositParametersForm
    {
        public DisbursementParametersForm()
            : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.XrGroup_param_09_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrText_param_09_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrLookup_param_09_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //XrGroup_param_09_1
            //
            this.XrGroup_param_09_1.Text = " Disbursement Batch ID  ";
            //
            //XrRadio_param_09_1
            //
            this.XrRadio_param_09_1.Checked = false;
            this.XrRadio_param_09_1.TabStop = false;
            //
            //XrRadio_param_09_2
            //
            this.XrRadio_param_09_2.Checked = true;
            this.XrRadio_param_09_2.TabStop = true;
            //
            //Label1
            //
            this.XrLbl_param_09_1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.XrLbl_param_09_1.Appearance.Options.UseFont = true;
            //
            //TextEdit_deposit_batch_id
            //
            this.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = true;
            this.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0";
            this.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.XrText_param_09_1.Properties.EditFormat.FormatString = "f0";
            this.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            //
            //DisbursementParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(336, 133);
            this.Name = "DisbursementParametersForm";
            this.XrGroup_param_09_1.ResumeLayout(false);
            this.XrGroup_param_09_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrText_param_09_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrLookup_param_09_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        protected override System.Collections.Generic.List<DepositParametersForm.Lookup_Item> getRecords()
        {
            using (BusinessContext bc = new BusinessContext())
            {
                return bc.registers_disbursements.OrderByDescending(rd => rd.date_created).Take(50).Select(rd => new Lookup_Item() { Id = rd.Id, date_created = rd.date_created, created_by = rd.created_by, note = rd.note }).ToList();
            }
        }

        protected override void load_Lookup()
        {
            base.load_Lookup();

            this.XrLookup_param_09_1.Properties.Columns.Clear();
            this.XrLookup_param_09_1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "d", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "Creator", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("note", "Note", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.XrLookup_param_09_1.Properties.DisplayMember = "note";
            this.XrLookup_param_09_1.ToolTip = "Choose a disbursement batch from the list if desired";
        }
    }
}