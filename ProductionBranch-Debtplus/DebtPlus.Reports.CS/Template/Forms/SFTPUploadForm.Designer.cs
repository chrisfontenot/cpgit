﻿namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class SFTPUploadForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.CheckEdit_SFTP_Upload = new DevExpress.XtraEditors.CheckEdit();
            this.TextEdit_Password = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_UserName = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_Directory = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_Site = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_SFTP_Upload.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Password.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_UserName.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Directory.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Site.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem_Left).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem_Right).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            this.SuspendLayout();
            //
            //DefaultLookAndFeel1
            //
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.TextEdit1);
            this.LayoutControl1.Controls.Add(this.SimpleButton2);
            this.LayoutControl1.Controls.Add(this.SimpleButton1);
            this.LayoutControl1.Controls.Add(this.CheckEdit_SFTP_Upload);
            this.LayoutControl1.Controls.Add(this.TextEdit_Password);
            this.LayoutControl1.Controls.Add(this.TextEdit_UserName);
            this.LayoutControl1.Controls.Add(this.TextEdit_Directory);
            this.LayoutControl1.Controls.Add(this.TextEdit_Site);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(405, 206);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //TextEdit1
            //
            this.TextEdit1.Location = new System.Drawing.Point(67, 60);
            this.TextEdit1.Name = "TextEdit1";
            this.TextEdit1.Size = new System.Drawing.Size(326, 20);
            this.TextEdit1.StyleController = this.LayoutControl1;
            this.TextEdit1.TabIndex = 11;
            //
            //SimpleButton2
            //
            this.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton2.Location = new System.Drawing.Point(195, 168);
            this.SimpleButton2.MaximumSize = new System.Drawing.Size(75, 26);
            this.SimpleButton2.MinimumSize = new System.Drawing.Size(75, 26);
            this.SimpleButton2.Name = "SimpleButton2";
            this.SimpleButton2.Size = new System.Drawing.Size(75, 26);
            this.SimpleButton2.StyleController = this.LayoutControl1;
            this.SimpleButton2.TabIndex = 10;
            this.SimpleButton2.Text = "&Cancel";
            //
            //SimpleButton1
            //
            this.SimpleButton1.Location = new System.Drawing.Point(116, 168);
            this.SimpleButton1.MaximumSize = new System.Drawing.Size(75, 26);
            this.SimpleButton1.MinimumSize = new System.Drawing.Size(75, 26);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(75, 26);
            this.SimpleButton1.StyleController = this.LayoutControl1;
            this.SimpleButton1.TabIndex = 9;
            this.SimpleButton1.Text = "&Send File";
            //
            //CheckEdit_SFTP_Upload
            //
            this.CheckEdit_SFTP_Upload.Location = new System.Drawing.Point(12, 132);
            this.CheckEdit_SFTP_Upload.Name = "CheckEdit_SFTP_Upload";
            this.CheckEdit_SFTP_Upload.Properties.Caption = "Use Secure FTP for the transfer";
            this.CheckEdit_SFTP_Upload.Size = new System.Drawing.Size(381, 20);
            this.CheckEdit_SFTP_Upload.StyleController = this.LayoutControl1;
            this.CheckEdit_SFTP_Upload.TabIndex = 8;
            //
            //TextEdit_Password
            //
            this.TextEdit_Password.Location = new System.Drawing.Point(67, 108);
            this.TextEdit_Password.Name = "TextEdit_Password";
            this.TextEdit_Password.Size = new System.Drawing.Size(326, 20);
            this.TextEdit_Password.StyleController = this.LayoutControl1;
            this.TextEdit_Password.TabIndex = 7;
            //
            //TextEdit_UserName
            //
            this.TextEdit_UserName.Location = new System.Drawing.Point(67, 84);
            this.TextEdit_UserName.Name = "TextEdit_UserName";
            this.TextEdit_UserName.Size = new System.Drawing.Size(326, 20);
            this.TextEdit_UserName.StyleController = this.LayoutControl1;
            this.TextEdit_UserName.TabIndex = 6;
            //
            //TextEdit_Directory
            //
            this.TextEdit_Directory.Location = new System.Drawing.Point(67, 36);
            this.TextEdit_Directory.Name = "TextEdit_Directory";
            this.TextEdit_Directory.Size = new System.Drawing.Size(326, 20);
            this.TextEdit_Directory.StyleController = this.LayoutControl1;
            this.TextEdit_Directory.TabIndex = 5;
            //
            //TextEdit_Site
            //
            this.TextEdit_Site.Location = new System.Drawing.Point(67, 12);
            this.TextEdit_Site.Name = "TextEdit_Site";
            this.TextEdit_Site.Size = new System.Drawing.Size(326, 20);
            this.TextEdit_Site.StyleController = this.LayoutControl1;
            this.TextEdit_Site.TabIndex = 4;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.LayoutControlItem7,
				this.EmptySpaceItem1,
				this.EmptySpaceItem_Left,
				this.EmptySpaceItem_Right,
				this.LayoutControlItem8
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(405, 206);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.TextEdit_Site;
            this.LayoutControlItem1.CustomizationFormText = "Site";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(385, 24);
            this.LayoutControlItem1.Text = "Site";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(52, 13);
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.TextEdit_Directory;
            this.LayoutControlItem2.CustomizationFormText = "Directory";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(385, 24);
            this.LayoutControlItem2.Text = "Directory";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(52, 13);
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.TextEdit_UserName;
            this.LayoutControlItem3.CustomizationFormText = "User Name";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 72);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(385, 24);
            this.LayoutControlItem3.Text = "User Name";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(52, 13);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.TextEdit_Password;
            this.LayoutControlItem4.CustomizationFormText = "Password";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 96);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(385, 24);
            this.LayoutControlItem4.Text = "Password";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(52, 13);
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.CheckEdit_SFTP_Upload;
            this.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 120);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(385, 24);
            this.LayoutControlItem5.Text = "LayoutControlItem5";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem5.TextToControlDistance = 0;
            this.LayoutControlItem5.TextVisible = false;
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.SimpleButton1;
            this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
            this.LayoutControlItem6.Location = new System.Drawing.Point(104, 156);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(79, 30);
            this.LayoutControlItem6.Text = "LayoutControlItem6";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem6.TextToControlDistance = 0;
            this.LayoutControlItem6.TextVisible = false;
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.SimpleButton2;
            this.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7";
            this.LayoutControlItem7.Location = new System.Drawing.Point(183, 156);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(79, 30);
            this.LayoutControlItem7.Text = "LayoutControlItem7";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem7.TextToControlDistance = 0;
            this.LayoutControlItem7.TextVisible = false;
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 144);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(385, 12);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem_Left
            //
            this.EmptySpaceItem_Left.AllowHotTrack = false;
            this.EmptySpaceItem_Left.CustomizationFormText = "Left Edge Spacer";
            this.EmptySpaceItem_Left.Location = new System.Drawing.Point(0, 156);
            this.EmptySpaceItem_Left.Name = "EmptySpaceItem_Left";
            this.EmptySpaceItem_Left.Size = new System.Drawing.Size(104, 30);
            this.EmptySpaceItem_Left.Text = "Left";
            this.EmptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem_Right
            //
            this.EmptySpaceItem_Right.AllowHotTrack = false;
            this.EmptySpaceItem_Right.CustomizationFormText = "Right Edge Spacer";
            this.EmptySpaceItem_Right.Location = new System.Drawing.Point(262, 156);
            this.EmptySpaceItem_Right.Name = "EmptySpaceItem_Right";
            this.EmptySpaceItem_Right.Size = new System.Drawing.Size(123, 30);
            this.EmptySpaceItem_Right.Text = "Right";
            this.EmptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.Control = this.TextEdit1;
            this.LayoutControlItem8.CustomizationFormText = "FileName";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 48);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(385, 24);
            this.LayoutControlItem8.Text = "FileName";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(52, 13);
            //
            //SFTPUploadForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton2;
            this.ClientSize = new System.Drawing.Size(405, 206);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "SFTPUploadForm";
            this.Text = "FTP Upload";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit_SFTP_Upload.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Password.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_UserName.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Directory.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Site.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem_Left).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem_Right).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraLayout.LayoutControl LayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit TextEdit1;
        private DevExpress.XtraEditors.SimpleButton SimpleButton2;
        private DevExpress.XtraEditors.SimpleButton SimpleButton1;
        private DevExpress.XtraEditors.CheckEdit CheckEdit_SFTP_Upload;
        private DevExpress.XtraEditors.TextEdit TextEdit_Password;
        private DevExpress.XtraEditors.TextEdit TextEdit_UserName;
        private DevExpress.XtraEditors.TextEdit TextEdit_Directory;
        private DevExpress.XtraEditors.TextEdit TextEdit_Site;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_Left;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_Right;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
    }
}
