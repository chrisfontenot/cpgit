using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class DepositParametersForm : BaseBatchParametersForm
    {
        public DepositParametersForm()
            : base()
        {
            InitializeComponent();

            if (!InDesignMode)
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Load += RequestParametersForm_Load;
        }

        private void UnRegisterHandlers()
        {
            Load -= RequestParametersForm_Load;
        }

        /// <summary>
        /// Deposit batch ID
        /// </summary>
        public Int32 Parameter_BatchID
        {
            get { return getSelectedID().GetValueOrDefault(-1); }
            set
            {
                XrLookup_param_09_1.EditValue = value;
                XrText_param_09_1.EditValue = value;
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.Text = "Deposit Batch Selection";
            this.XrText_param_09_1.ToolTip = "Enter a specific deposit batch if it is not included in the above list";
            this.XrGroup_param_09_1.Text = " Deposit Batch ID ";
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void RequestParametersForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                load_Lookup();
                ButtonOK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected class Lookup_Item
        {
            public Int32 Id { get; set; }

            public DateTime date_created { get; set; }

            public string created_by { get; set; }

            public string note { get; set; }

            public string bank { get; set; }

            public string bank_type { get; set; }

            // Used for proposals
            public System.Nullable<System.Int32> proposal_type { get; set; }

            public System.Nullable<System.DateTime> date_closed { get; set; }

            public System.Nullable<System.DateTime> date_transmitted { get; set; }

            public bool IsClosed
            {
                get { return (date_closed.HasValue || date_transmitted.HasValue); }
            }

            public string formatted_proposal_type
            {
                get
                {
                    if (proposal_type.HasValue)
                    {
                        switch (proposal_type.Value)
                        {
                            case 1:
                                return "Standard";
                            case 2:
                                return "FULL";
                            case 3:
                                return "EFT";
                            case 4:
                                return "EFT";
                            default:
                                break;
                        }
                    }
                    return string.Empty;
                }
            }

            public string formatted_bank_type
            {
                get
                {
                    if (bank_type != null)
                    {
                        switch (bank_type)
                        {
                            case "A":
                                return "ACH";
                            case "C":
                                return "Printed";
                            case "D":
                                return "Deposit";
                            case "R":
                                return "RPPS";
                            default:
                                break;
                        }
                    }
                    return string.Empty;
                }
            }

            public Lookup_Item()
            {
            }
        }

        private System.Collections.Generic.List<Lookup_Item> colRecords;

        protected virtual System.Collections.Generic.List<Lookup_Item> getRecords()
        {
            using (var bc = new BusinessContext())
            {
                return (from ids in bc.deposit_batch_ids
                        join b in bc.banks on ids.bank equals b.Id
                        where ids.date_posted == null && ids.batch_type == "CL"
                        orderby ids.date_created descending
                        select new Lookup_Item()
                        {
                            Id = ids.Id,
                            date_created = ids.date_created,
                            created_by = ids.created_by,
                            bank = b.bank_name,
                            note = ids.note
                        }).ToList();
            }
        }

        protected virtual void load_Lookup()
        {
            this.XrLookup_param_09_1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "d", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "Creator", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("bank", "Bank", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("note", "Note", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
            this.XrLookup_param_09_1.Properties.DisplayMember = "date_created";
            this.XrLookup_param_09_1.Properties.ValueMember = "Id";
            this.XrLookup_param_09_1.ToolTip = "Choose a deposit batch from the list if desired";

            try
            {
                using (DebtPlus.UI.Common.CursorManager cm = new DebtPlus.UI.Common.CursorManager())
                {
                    colRecords = getRecords();

                    // Force a change in the control's value if the value is defined.
                    object obj = XrLookup_param_09_1.EditValue;
                    XrLookup_param_09_1.EditValue = null;
                    XrLookup_param_09_1.EditValue = obj;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                return;
            }

            // Define the lookup data source
            XrLookup_param_09_1.Properties.DataSource = colRecords;
            if (colRecords != null && colRecords.Count > 0)
            {
                Parameter_BatchID = colRecords[0].Id;
            }
        }
    }
}
