﻿using System;

namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class ProposalParametersForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.XrGroup_param_09_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrText_param_09_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrLookup_param_09_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //GroupBox1
            //
            this.XrGroup_param_09_1.Text = " Proposal Batch ID ";
            //
            //Label1
            //
            this.XrLbl_param_09_1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            //Me.Label1.Appearance.Options.UseFont = True
            //
            //TextEdit_deposit_batch_id
            //
            this.XrText_param_09_1.Properties.Appearance.Options.UseTextOptions = true;
            this.XrText_param_09_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XrText_param_09_1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.XrText_param_09_1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XrText_param_09_1.Properties.DisplayFormat.FormatString = "f0";
            this.XrText_param_09_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.XrText_param_09_1.Properties.EditFormat.FormatString = "f0";
            this.XrText_param_09_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            //
            //ProposalParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 131);
            this.Name = "ProposalParametersForm";
            this.XrGroup_param_09_1.ResumeLayout(false);
            this.XrGroup_param_09_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrText_param_09_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrLookup_param_09_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

    }
}
