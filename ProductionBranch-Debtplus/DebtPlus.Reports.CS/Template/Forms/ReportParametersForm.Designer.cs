using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.LINQ;
using System.Linq;
using DebtPlus.UI.Common;
using DebtPlus.Data.Forms;
using DevExpress.XtraEditors;
using DebtPlus.UI.Client.Widgets.Controls;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DebtPlus.Utils.Format;

namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class ReportParametersForm
	{
		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing)
            {
				if (components != null)
                {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.ButtonOK = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonCancel = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();

			//
			//ButtonOK
			//
			this.ButtonOK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonOK.Enabled = false;
			this.ButtonOK.Location = new System.Drawing.Point(248, 17);
			this.ButtonOK.Name = "ButtonOK";
			this.ButtonOK.Size = new System.Drawing.Size(80, 26);
			this.ButtonOK.TabIndex = 80;
			this.ButtonOK.Text = "&OK";
			this.ButtonOK.ToolTip = "Press here to start the report";
			this.ButtonOK.ToolTipController = this.ToolTipController1;
			//
			//ButtonCancel
			//
			this.ButtonCancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonCancel.Location = new System.Drawing.Point(248, 52);
			this.ButtonCancel.Name = "ButtonCancel";
			this.ButtonCancel.Size = new System.Drawing.Size(80, 26);
			this.ButtonCancel.TabIndex = 81;
			this.ButtonCancel.Text = "&Cancel";
			this.ButtonCancel.ToolTip = "Press here to cancel the report and return to the previous screen";
			this.ButtonCancel.ToolTipController = this.ToolTipController1;
			//
			//ReportParametersForm
			//
			this.AcceptButton = this.ButtonOK;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.CancelButton = this.ButtonCancel;
			this.ClientSize = new System.Drawing.Size(336, 135);
			this.ControlBox = false;
			this.Controls.Add(this.ButtonCancel);
			this.Controls.Add(this.ButtonOK);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ReportParametersForm";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Report Parameters";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.TopMost = true;

			this.ResumeLayout(false);
		}
		protected DevExpress.XtraEditors.SimpleButton ButtonOK;
		protected DevExpress.XtraEditors.SimpleButton ButtonCancel;
	}
}
