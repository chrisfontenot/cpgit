using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.LINQ;
using System.Linq;
using DebtPlus.UI.Common;
using DebtPlus.Data.Forms;
using DevExpress.XtraEditors;
using DebtPlus.UI.Client.Widgets.Controls;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DebtPlus.Utils.Format;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class ReportParametersForm : DebtPlusForm
	{
		public ReportParametersForm()
		{
			InitializeComponent();
			RegisterHandlers();

			// For some strange reason, the dialog results keep getting set. DON'T SET THEM!!!!
			ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            ButtonOK.DialogResult = System.Windows.Forms.DialogResult.None;
		}

		private void RegisterHandlers()
		{
			ButtonCancel.Click += ButtonCancel_Click;
			ButtonOK.Click += ButtonOK_Click;
		}

		private void UnRegisterHandlers()
		{
			ButtonCancel.Click -= ButtonCancel_Click;
			ButtonOK.Click -= ButtonOK_Click;
		}

		//' DO NOT DELETE THIS PROCEDURE!!!!
		protected virtual void ButtonOK_Click(object sender, System.EventArgs e)
		{
            DialogResult = System.Windows.Forms.DialogResult.OK;
			if (!Modal)
            {
				Close();
			}
		}

		//' DO NOT DELETE THIS PROCEDURE!!!!
		protected virtual void ButtonCancel_Click(object sender, System.EventArgs e)
		{
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
			if (!Modal)
            {
				Close();
			}
		}

		protected virtual bool HasErrors()
		{
			return false;
		}

		protected virtual void FormChanged(object sender, EventArgs e)
		{
			ButtonOK.Enabled = !HasErrors();
		}
	}
}
