using System;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class DatedClientParametersForm : DateReportParametersForm
    {
        public DatedClientParametersForm()
            : this(DebtPlus.Utils.DateRange.Today)
        {
        }

        public DatedClientParametersForm(DebtPlus.Utils.DateRange defaultItem)
            : base(defaultItem)
        {
            InitializeComponent();
            this.Load += RequestParametersForm_Load;
        }

        private Int32 privateClient = -1;

        public Int32 Parameter_Client
        {
            get { return privateClient; }
            set { privateClient = value; }
        }

        private void RequestParametersForm_Load(object sender, EventArgs e)
        {
            // Load the client id into the control
            if (Parameter_Client >= 0)
            {
                XrClient_param_04_1.Enabled = false;
                XrClient_param_04_1.Text = string.Format("{0:0000000}", Parameter_Client);
            }
            else
            {
                XrClient_param_04_1.EditValue = null;
                XrClient_param_04_1.Enabled = true;
            }

            ButtonOK.Enabled = !HasErrors();

            // Add the handler to detect changes in the client id
            XrClient_param_04_1.EditValueChanging += XrClient_param_04_1_EditValueChanging;
        }

        private void XrClient_param_04_1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            Int32 NewClientID = -1;

            if (e.NewValue == null)
            {
                Parameter_Client = -1;
                ButtonOK.Enabled = false;
            }
            else if (object.ReferenceEquals(e.NewValue, DBNull.Value) || !Int32.TryParse(Convert.ToString(e.NewValue), out NewClientID))
            {
                Parameter_Client = -1;
                ButtonOK.Enabled = false;
            }
            else
            {
                Parameter_Client = NewClientID;
                ButtonOK.Enabled = !HasErrors();
            }
        }

        protected override bool HasErrors()
        {
            bool answer = base.HasErrors();
            if (!answer && Parameter_Client < 0)
                answer = true;
            return answer;
        }
    }
}
