﻿using DebtPlus.Data.Forms;

namespace DebtPlus.Reports.CS.Template.Forms
{
    internal partial class SFTPUploadForm : DebtPlusForm
    {
        internal delegate void PerformUploadEventHandler(object sender, PerformUploadEventArgs e);
        internal event PerformUploadEventHandler PerformUpload;

        protected void RaisePerformUpload(PerformUploadEventArgs e)
        {
            if (PerformUpload != null)
            {
                PerformUpload(this, e);
            }
        }

        protected virtual void OnPerformUpload(PerformUploadEventArgs e)
        {
            RaisePerformUpload(e);
        }

        public SFTPUploadForm()
            : base()
        {
            InitializeComponent();
        }

        private SFTPUloadParameers parm = null;

        public SFTPUploadForm(SFTPUloadParameers parm)
            : this()
        {
            this.parm = parm;
            RegisterHandlers();
        }

        /// <summary>
        /// Add the event handler routines
        /// </summary>
        /// <remarks></remarks>
        private void RegisterHandlers()
        {
            this.Load                            += SFTPUploadForm_Load;
            TextEdit1.Validating                 += TextEdit1_Validating;
            TextEdit1.EditValueChanged           += FormChanged;
            TextEdit_Directory.EditValueChanged  += FormChanged;
            TextEdit_Password.EditValueChanged   += FormChanged;
            TextEdit_Site.EditValueChanged       += FormChanged;
            TextEdit_UserName.EditValueChanged   += FormChanged;
            CheckEdit_SFTP_Upload.CheckedChanged += FormChanged;
            SimpleButton1.Click                  += SimpleButton1_Click;
        }

        /// <summary>
        /// Remove the event handler routines
        /// </summary>
        /// <remarks></remarks>
        private void UnRegisterHandlers()
        {
            this.Load                            -= SFTPUploadForm_Load;
            TextEdit1.Validating                 -= TextEdit1_Validating;
            TextEdit1.EditValueChanged           -= FormChanged;
            TextEdit_Directory.EditValueChanged  -= FormChanged;
            TextEdit_Password.EditValueChanged   -= FormChanged;
            TextEdit_Site.EditValueChanged       -= FormChanged;
            TextEdit_UserName.EditValueChanged   -= FormChanged;
            CheckEdit_SFTP_Upload.CheckedChanged -= FormChanged;
            SimpleButton1.Click                  -= SimpleButton1_Click;
        }

        /// <summary>
        /// Handle a change in the input controls
        /// </summary>
        private void FormChanged(object sender, System.EventArgs e)
        {
            SimpleButton1.Enabled = !HasErrors();
        }

        /// <summary>
        /// Look for an error condition in the input form data
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool HasErrors()
        {
            // Some fields are required to be present if we are to have a valid upload
            if (string.IsNullOrWhiteSpace(TextEdit_Directory.Text) || string.IsNullOrWhiteSpace(TextEdit_Site.Text) || string.IsNullOrWhiteSpace(TextEdit1.Text))
            {
                return true;
            }

            return false;
        }

        private void SFTPUploadForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                EmptySpaceItem_Left.Width = (EmptySpaceItem_Left.Width + EmptySpaceItem_Right.Width) / 2;

                TextEdit_Directory.EditValue  = parm.Directory;
                TextEdit_Password.EditValue   = parm.Password;
                TextEdit_Site.EditValue       = parm.Site;
                TextEdit_UserName.EditValue   = parm.UserName;
                TextEdit1.EditValue           = parm.FileName;
                CheckEdit_SFTP_Upload.Checked = parm.isSecure;

                SimpleButton1.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void TextEdit1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Get the file name. If the entry is not defined then the error routine will catch it later.
                string strValue = DebtPlus.Utils.Nulls.v_String(TextEdit1.EditValue);
                if (string.IsNullOrWhiteSpace(strValue))
                {
                    return;
                }

                // Ensure that the extension name is "PDF"
                string extName = System.IO.Path.GetExtension(strValue);
                if (string.Compare(extName, ".pdf", true) != 0)
                {
                    strValue = System.IO.Path.ChangeExtension(strValue, ".pdf");
                    TextEdit1.EditValue = strValue;
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void SimpleButton1_Click(object sender, System.EventArgs e)
        {
            // Retrieve the record parameters from the edit controls
            parm.Directory = DebtPlus.Utils.Nulls.v_String(TextEdit_Directory.EditValue);
            parm.Password  = DebtPlus.Utils.Nulls.v_String(TextEdit_Password.EditValue);
            parm.Site      = DebtPlus.Utils.Nulls.v_String(TextEdit_Site.EditValue);
            parm.UserName  = DebtPlus.Utils.Nulls.v_String(TextEdit_UserName.EditValue);
            parm.FileName  = DebtPlus.Utils.Nulls.v_String(TextEdit1.EditValue);
            parm.isSecure  = CheckEdit_SFTP_Upload.Checked;

            // Tell the caller that we have processed the OK button and to do the upload now.
            var eProcess = new PerformUploadEventArgs
            {
                Cancel = false,
                Parameters = parm
            };
            OnPerformUpload(eProcess);

            // Complete the dialog unless the event is canceled.
            if (!eProcess.Cancel)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class SFTPUloadParameers
    {
        public SFTPUloadParameers()
        {
            this.UserName = null;
            this.FileName = null;
            this.Password = null;
            this.Site = null;
            this.Directory = null;
            this.isSecure = true;
        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Site { get; set; }
        public string Directory { get; set; }
        public string FileName { get; set; }
        public bool isSecure { get; set; }
    }

    internal class PerformUploadEventArgs : System.ComponentModel.CancelEventArgs
    {
        public PerformUploadEventArgs()
            : base()
        {
        }

        public PerformUploadEventArgs(bool Cancel)
            : base(Cancel)
        {
        }

        public SFTPUloadParameers Parameters { get; set; }
    }
}