using System;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class CounselorParametersForm
    {
        // TRUE if "[All Counselors]" is in the list
        public bool EnableAllCounselors { get; set; }

        public CounselorParametersForm()
            : this(false)
        {
        }

        public CounselorParametersForm(bool enableAllCounselors)
        {
            InitializeComponent();
            this.Load += DateCounselorReportParametersForm_Load;
            this.EnableAllCounselors = enableAllCounselors;
        }

        /// <summary>
        /// Return the counselor selected in the form
        /// </summary>
        public object Parameter_Counselor
        {
            get
            {
                Int32 value = DebtPlus.Utils.Nulls.v_Int32(XrCounselor_param_02_1.EditValue).GetValueOrDefault(-1);
                if (value < 0 && EnableAllCounselors)
                {
                    return System.DBNull.Value;
                }
                return value;
            }
        }

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void DateCounselorReportParametersForm_Load(object sender, EventArgs e)
        {
            CounselorDropDown_Load();
            ButtonOK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Load the list of counselors
        /// </summary>

        protected void CounselorDropDown_Load()
        {
            // Set the parameters that define the counselor lookup
            this.XrCounselor_param_02_1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.XrCounselor_param_02_1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)
			});
            this.XrCounselor_param_02_1.Properties.DisplayMember = "Name";
            this.XrCounselor_param_02_1.ToolTip = "Choose the counselor to which you wish to limit the report";
            this.XrCounselor_param_02_1.Properties.ValueMember = "Id";

            // If we don't want all counselors then just load the standard list
            if (!EnableAllCounselors)
            {
                XrCounselor_param_02_1.Properties.DataSource = DebtPlus.LINQ.Cache.counselor.getList();
                XrCounselor_param_02_1.EditValue = DebtPlus.LINQ.Cache.counselor.getDefault();
            }
            else
            {
                // Populate the current list of counselors. We need to copy it first before we add a new item to the list.
                System.Collections.Generic.List<DebtPlus.LINQ.counselor> colCounselors = new System.Collections.Generic.List<DebtPlus.LINQ.counselor>();
                colCounselors.AddRange(DebtPlus.LINQ.Cache.counselor.getList());

                // Create a fake counselor by the name "[All Counselors]" and make that the default item.
                DebtPlus.LINQ.Name nameRecord = DebtPlus.LINQ.Factory.Manufacture_Name();
                nameRecord.Last = "[All Counselors]";
                nameRecord.Id = -1;

                DebtPlus.LINQ.counselor counselorRecord = DebtPlus.LINQ.Factory.Manufacture_counselor();
                counselorRecord.Name = nameRecord;
                counselorRecord.NameID = -1;
                counselorRecord.Id = -1;
                counselorRecord.Default = true;
                colCounselors.Add(counselorRecord);

                XrCounselor_param_02_1.Properties.DataSource = colCounselors;
                XrCounselor_param_02_1.EditValue = -1;
            }

            XrCounselor_param_02_1.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            XrCounselor_param_02_1.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Look for errors in the counselor list
        /// </summary>
        protected override bool HasErrors()
        {
            return base.HasErrors() || (XrCounselor_param_02_1.EditValue == null);
        }
    }
}