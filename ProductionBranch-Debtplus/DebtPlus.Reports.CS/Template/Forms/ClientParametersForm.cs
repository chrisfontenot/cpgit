using System;
using System.ComponentModel;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class ClientParametersForm
    {
        public ClientParametersForm()
            : base()
        {
            InitializeComponent();
            this.Load += RequestParametersForm_Load;
            XrClient_param_01_1.Validating += ClientID1_Validating;
        }

        public Int32 Parameter_Client
        {
            get { return XrClient_param_01_1.EditValue.GetValueOrDefault(-1); }
            set
            {
                if (value < 0)
                {
                    XrClient_param_01_1.EditValue = null;
                }
                else
                {
                    XrClient_param_01_1.EditValue = value;
                }
            }
        }

        private void RequestParametersForm_Load(object sender, EventArgs e)
        {
            ButtonOK.Enabled = !HasErrors();
        }

        private void ClientID1_Validating(object sender, CancelEventArgs e)
        {
            ButtonOK.Enabled = !HasErrors();
        }

        protected override bool HasErrors()
        {
            return base.HasErrors() || (Parameter_Client < 0);
        }
    }
}