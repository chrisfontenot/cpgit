﻿namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class DatedCounselorParametersForm : DateReportParametersForm
    {
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        protected DevExpress.XtraEditors.LabelControl Label1;
        protected DevExpress.XtraEditors.LookUpEdit XrCounselor_param_05_1;
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DebtPlus.Data.Controls.ComboboxItem ComboboxItem1 = new DebtPlus.Data.Controls.ComboboxItem();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.XrCounselor_param_05_1 = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCounselor_param_05_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GroupBox1
            //
            this.ToolTipController1.SetSuperTip(this.XrGroup_param_08_1, null);
            //
            //ComboboxEdit_DateRange
            //
            ComboboxItem1.tag = null;
            ComboboxItem1.value = 12;
            this.XrCombo_param_08_1.EditValue = ComboboxItem1;
            //
            //DateEdit_Ending
            //
            this.XrDate_param_08_2.EditValue = new System.DateTime(2008, 9, 12, 0, 0, 0, 0);
            this.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton()
			});
            //
            //DateEdit_Starting
            //
            this.XrDate_param_08_1.EditValue = new System.DateTime(2008, 9, 12, 0, 0, 0, 0);
            this.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton()
			});
            //
            //ButtonOK
            //
            this.ButtonOK.Location = new System.Drawing.Point(247, 16);
            this.ButtonOK.TabIndex = 3;
            //
            //ButtonCancel
            //
            this.ButtonCancel.Location = new System.Drawing.Point(247, 48);
            this.ButtonCancel.TabIndex = 4;
            //
            //Label1
            //
            this.Label1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.Label1.Appearance.Options.UseTextOptions = true;
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Label1.Location = new System.Drawing.Point(12, 130);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(52, 13);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Counse&lor:";
            //
            //CounselorDropDown
            //
            this.XrCounselor_param_05_1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.XrCounselor_param_05_1.Location = new System.Drawing.Point(78, 127);
            this.XrCounselor_param_05_1.Size = new System.Drawing.Size(249, 20);
            this.XrCounselor_param_05_1.TabIndex = 2;
            this.XrCounselor_param_05_1.ToolTipController = this.ToolTipController1;
            this.XrCounselor_param_05_1.Name = "XrCounselor_param_05_1";
            this.XrCounselor_param_05_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.XrCounselor_param_05_1.Properties.PopupWidth = 300;
            this.XrCounselor_param_05_1.Properties.ShowFooter = false;
            this.XrCounselor_param_05_1.Properties.ShowHeader = false;
            this.XrCounselor_param_05_1.Properties.SortColumnIndex = 1;
            //
            //DatedCounselorParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(335, 157);
            this.Controls.Add(this.XrCounselor_param_05_1);
            this.Controls.Add(this.Label1);
            this.Name = "DatedCounselorParametersForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Controls.SetChildIndex(this.XrGroup_param_08_1, 0);
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.Label1, 0);
            this.Controls.SetChildIndex(this.XrCounselor_param_05_1, 0);
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCounselor_param_05_1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
    }
}
