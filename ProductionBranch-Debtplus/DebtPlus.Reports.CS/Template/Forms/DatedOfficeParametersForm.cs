using System;
using DevExpress.Utils;

namespace DebtPlus.Reports.CS.Template.Forms
{
    public partial class DatedOfficeParametersForm : DateReportParametersForm
    {
        // TRUE if "[All Offices]" is in the list
        protected bool EnableAllOffices { get; set; }

        public DatedOfficeParametersForm()
            : this(DebtPlus.Utils.DateRange.Today, true)
        {
        }

        public DatedOfficeParametersForm(DebtPlus.Utils.DateRange defaultItem)
            : this(defaultItem, true)
        {
        }

        public DatedOfficeParametersForm(bool enableAllOffices)
            : this(DebtPlus.Utils.DateRange.Today, enableAllOffices)
        {
        }

        public DatedOfficeParametersForm(DebtPlus.Utils.DateRange defaultItem, bool EnableAllOffices)
            : base(defaultItem)
        {
            InitializeComponent();
            this.Load += DateCounselorReportParametersForm_Load;
            this.EnableAllOffices = EnableAllOffices;
        }

        /// <summary>
        /// Return the office selected in the form
        /// </summary>
        public object Parameter_Office
        {
            get
            {
                Int32 value = DebtPlus.Utils.Nulls.v_Int32(XrOffice_param_07_1.EditValue).GetValueOrDefault(-1);
                if (value < 0 && EnableAllOffices)
                {
                    return System.DBNull.Value;
                }
                return value;
            }
        }

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void DateCounselorReportParametersForm_Load(object sender, EventArgs e)
        {
            OfficeDropDown_Load();
            ButtonOK.Enabled = !HasErrors();
        }

        protected void OfficeDropDown_Load()
        {
            // Set the parameters that define the office lookup
            this.XrOffice_param_07_1.Properties.AllowNullInput = DefaultBoolean.False;
            this.XrOffice_param_07_1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Descending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("formatted_type", 15, "Type")
			});
            this.XrOffice_param_07_1.Properties.DisplayMember = "name";
            this.XrOffice_param_07_1.Properties.ValueMember = "Id";

            // If we don't want all offices then just load the standard list
            if (!EnableAllOffices)
            {
                XrOffice_param_07_1.Properties.DataSource = DebtPlus.LINQ.Cache.office.getList();
                XrOffice_param_07_1.EditValue = DebtPlus.LINQ.Cache.office.getDefault();
            }
            else
            {
                // Populate the current list of offices. We need to copy it first before we add a new item to the list.
                System.Collections.Generic.List<DebtPlus.LINQ.office> colOffices = new System.Collections.Generic.List<DebtPlus.LINQ.office>();
                colOffices.AddRange(DebtPlus.LINQ.Cache.office.getList());

                // Create a fake office by the name "[All Offices]" and make that the default item.
                DebtPlus.LINQ.office officeRecord = DebtPlus.LINQ.Factory.Manufacture_office();
                officeRecord.name = "[All Offices]";
                officeRecord.Id = -1;
                officeRecord.Default = true;
                colOffices.Add(officeRecord);

                XrOffice_param_07_1.Properties.DataSource = colOffices;
                XrOffice_param_07_1.EditValue = -1;
            }

            XrOffice_param_07_1.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            XrOffice_param_07_1.EditValueChanged += FormChanged;
        }

        /// <summary>
        /// Look for errors in the counselor list
        /// </summary>
        protected override bool HasErrors()
        {
            return base.HasErrors() || (!EnableAllOffices && object.ReferenceEquals(XrOffice_param_07_1.EditValue, DBNull.Value));
        }
    }
}
