﻿namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class ClientParametersForm : Forms.ReportParametersForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientParametersForm));
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.XrClient_param_01_1 = new DebtPlus.UI.Client.Widgets.Controls.ClientID();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrClient_param_01_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ButtonOK
            //
            this.ButtonOK.TabIndex = 2;
            //
            //ButtonCancel
            //
            this.ButtonCancel.TabIndex = 3;
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(13, 28);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(45, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Client ID:";
            //
            //ClientID1
            //
            this.XrClient_param_01_1.Location = new System.Drawing.Point(74, 25);
            this.XrClient_param_01_1.Name = "ClientID1";
            this.XrClient_param_01_1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.XrClient_param_01_1.Properties.Appearance.Options.UseTextOptions = true;
            this.XrClient_param_01_1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.XrClient_param_01_1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DebtPlus.UI.Common.Controls.SearchButton() });
            this.XrClient_param_01_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.XrClient_param_01_1.Properties.DisplayFormat.FormatString = "0000000";
            this.XrClient_param_01_1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.XrClient_param_01_1.Properties.EditFormat.FormatString = "f0";
            this.XrClient_param_01_1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.XrClient_param_01_1.Properties.Mask.BeepOnError = true;
            this.XrClient_param_01_1.Properties.Mask.EditMask = "\\d*";
            this.XrClient_param_01_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.XrClient_param_01_1.Properties.ValidateOnEnterKey = true;
            this.XrClient_param_01_1.Size = new System.Drawing.Size(100, 20);
            this.XrClient_param_01_1.TabIndex = 1;
            //
            //ClientParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 135);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.XrClient_param_01_1);
            this.Name = "ClientParametersForm";
            this.Controls.SetChildIndex(this.XrClient_param_01_1, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrClient_param_01_1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        protected DebtPlus.UI.Client.Widgets.Controls.ClientID XrClient_param_01_1;
        protected DevExpress.XtraEditors.LabelControl LabelControl1;
    }
}
