﻿namespace DebtPlus.Reports.CS.Template.Forms
{
    partial class DatedCreditorParametersForm : DateReportParametersForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DebtPlus.Data.Controls.ComboboxItem ComboboxItem1 = new DebtPlus.Data.Controls.ComboboxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatedCreditorParametersForm));
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.XrCreditor_param_06_1 = new DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).BeginInit();
            this.XrGroup_param_08_1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCreditor_param_06_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ComboboxEdit_DateRange
            //
            ComboboxItem1.tag = null;
            ComboboxItem1.value = 12;
            this.XrCombo_param_08_1.EditValue = ComboboxItem1;
            //
            //DateEdit_Ending
            //
            this.XrDate_param_08_2.EditValue = new System.DateTime(2008, 5, 23, 0, 0, 0, 0);
            this.XrDate_param_08_2.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton()
			});
            //
            //DateEdit_Starting
            //
            this.XrDate_param_08_1.EditValue = new System.DateTime(2008, 5, 23, 0, 0, 0, 0);
            this.XrDate_param_08_1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton(),
				new DevExpress.XtraEditors.Controls.EditorButton()
			});
            //
            //ButtonOK
            //
            this.ButtonOK.TabIndex = 3;
            //
            //ButtonCancel
            //
            this.ButtonCancel.TabIndex = 4;
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(24, 132);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(53, 13);
            this.LabelControl1.TabIndex = 1;
            this.LabelControl1.Text = "Creditor ID";
            //
            //CreditorID1
            //
            this.XrCreditor_param_06_1.EditValue = null;
            this.XrCreditor_param_06_1.Location = new System.Drawing.Point(108, 129);
            this.XrCreditor_param_06_1.Name = "CreditorID1";
            this.XrCreditor_param_06_1.Properties.Buttons.Add(new DebtPlus.UI.Common.Controls.SearchButton());
            this.XrCreditor_param_06_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.XrCreditor_param_06_1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.XrCreditor_param_06_1.Properties.Mask.BeepOnError = true;
            this.XrCreditor_param_06_1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.XrCreditor_param_06_1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.XrCreditor_param_06_1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.XrCreditor_param_06_1.Properties.MaxLength = 10;
            this.XrCreditor_param_06_1.Size = new System.Drawing.Size(100, 20);
            this.XrCreditor_param_06_1.TabIndex = 2;
            //
            //DatedCreditorParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 161);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.XrCreditor_param_06_1);
            this.Name = "DatedCreditorParametersForm";
            this.Controls.SetChildIndex(this.XrCreditor_param_06_1, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.XrGroup_param_08_1, 0);
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.XrGroup_param_08_1).EndInit();
            this.XrGroup_param_08_1.ResumeLayout(false);
            this.XrGroup_param_08_1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)this.XrCombo_param_08_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrDate_param_08_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XrCreditor_param_06_1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        protected DevExpress.XtraEditors.LabelControl LabelControl1;
        protected DebtPlus.UI.Creditor.Widgets.Controls.CreditorID XrCreditor_param_06_1;
    }
}
