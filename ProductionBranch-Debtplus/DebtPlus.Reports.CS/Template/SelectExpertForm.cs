using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DevExpress.XtraEditors.Controls;
using DebtPlus.Data.Forms;
using DevExpress.Data.Filtering;
using DevExpress.XtraEditors.Filtering;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraReports.UI;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using DebtPlus.Interfaces.Reports;

namespace DebtPlus.Reports.CS.Template
{
	public partial class SelectExpertForm : DebtPlusForm
	{
		/// <summary>
		/// Return the criteria for the filter. This is the original parse tree from the filter control.
		/// </summary>
		public CriteriaOperator CriteriaFilter
        {
			get
            {
                return FilterControl1.FilterCriteria;
            }
		}

		private readonly IReports ParentReport;

		/// <summary>
		/// Initialize the blank form
		/// </summary>
		/// <remarks>This will use the default database access methods</remarks>
		public SelectExpertForm() : base()
		{
			InitializeComponent();
			this.Load += Form_Load;
		}

		public SelectExpertForm(IReports ParentReport) : this()
		{
			this.ParentReport = ParentReport;
		}

		public SelectExpertForm(IReports parentReport, CriteriaOperator filterCriteria) : this(parentReport)
		{
			FilterControl1.FilterCriteria = filterCriteria;
		}

		/// <summary>
		/// Perform the LOAD event processing on the form.
		/// </summary>
		private void Form_Load(object sender, EventArgs e)
		{
			// Obtain a pointer to the dataset if needed
			object dataSource = (ParentReport as XtraReportBase).DataSource;
			object dataMember = (ParentReport as XtraReportBase).DataMember;

			if (dataSource != null)
            {
				if (dataSource is DataSet)
                {
					ConfigureDataSource((dataSource as DataSet).Tables[(string) dataMember]);
				}
                else if (dataSource is DataView)
                {
					ConfigureDataSource((dataSource as DataView).Table);
				}
                else if (dataSource is DataTable)
                {
					ConfigureDataSource(dataSource as DataTable);
				}
                else if (dataSource is IList)
                {
					throw new NotImplementedException("Filtering IList items is not supported");
					//ConfigureDataSource(dataSource)
				}
			}
		}

		Int32 ItemCount;

		private void ConfigureDataSource(DataTable tbl)
		{
			// Set the pointer to the editing source
            FilterControl1.SortFilterColumns = true;
            FilterControl1.UseMenuForOperandsAndOperators = true;
            FilterControl1.SourceControl = tbl;
            FilterControl1.BeforeShowValueEditor += FilterControl1_BeforeShowValueEditor;

			// Set the number of items in the status information
			ItemCount = tbl.Rows.Count;
			BarStaticItem_ItemCount.Caption = string.Format("{0:n0} item(s) in the database", ItemCount);
		}

		/// <summary>
		/// Update the filter control with the possible values for the item
		/// </summary>
		private void FilterControl1_BeforeShowValueEditor(object sender, ShowValueEditorEventArgs e)
		{
			ProcessDataTable(sender, e);
		}

		private void ProcessDataTable(object Sender, ShowValueEditorEventArgs e)
		{
			DataTable tbl = ((FilterControl)Sender).SourceControl as DataTable;
			if (tbl != null)
            {
				string FieldName = e.CurrentNode.FirstOperand.PropertyName;
				Int32 col = tbl.Columns.IndexOf(FieldName);
				if (col >= 0)
                {
					Type PropertyType = tbl.Columns[col].DataType;

					// Determine the type of control based upon the nature of the field
					if (object.ReferenceEquals(PropertyType, typeof(decimal)))
                    {
						e.CustomRepositoryItem = GetCalcEdit();
					}
                    else if (object.ReferenceEquals(PropertyType, typeof(DateTime)) || object.ReferenceEquals(PropertyType, typeof(System.DateTime)))
                    {
						e.CustomRepositoryItem = GetDateEdit();
					}
                    // For bit fields, just use the single checkbox that is standard.
                    else if (object.ReferenceEquals(PropertyType, typeof(bool)))
                    {
						e.CustomRepositoryItem = null;
					}
                    // The default is to make a combobox item. But, do not do this for LARGE datasets! Just leave it as a text box.
                    else if (ItemCount < 10000)
                    {
						e.CustomRepositoryItem = GetTableCombobox(ref tbl, col);
					}
				}
			}
		}

		/// <summary>
		/// Return a combobox with the items that we are searching in the text string
		/// </summary>
		private static RepositoryItem GetTableCombobox(ref DataTable Tbl, Int32 Col)
		{
			var item = new RepositoryItemComboBox();

			// Create a combobox item list. We want to show the strings but allow the edit operation as well.

			// Do a little amount of configuration for the standard fields.
			item.NullValuePrompt = string.Empty;
			item.ImmediatePopup = true;
			item.TextEditStyle = TextEditStyles.Standard;
			item.AutoComplete = false;
			item.Sorted = true;

			// Process each row in the source dataset. This can take a while on a big table.
			foreach (DataRow row in Tbl.Rows)
            {
				// Find the current column value for the current row.
				object value = row[Col];

				if (value != null && !object.ReferenceEquals(value, DBNull.Value))
                {
					// Convert the item to a string so that we can put it in our list
					string stringValue = null;
					if (value is decimal)
                    {
						stringValue = Convert.ToDecimal(value).ToString("0.00");  // I don't like the default of 4 decimal places!!
					}
                    else
                    {
						stringValue = Convert.ToString(value);
					}

					// If there is a string then add it
					if (stringValue != string.Empty)
                    {
						// Look to see if the item is already in the list. Don't add duplicates.
						bool isDuplicated = false;
						foreach (string listItem in item.Items)
                        {
							if (string.Compare(listItem, stringValue, true) == 0)
                            {
								isDuplicated = true;
								break;
							}
						}

						// If the item is not duplicated then add it
						if (!isDuplicated)
                        {
							item.Items.Add(stringValue);
						}
					}
				}
			}

			return item;
		}

		/// <summary>
		/// Return a calculator field for the decimal numbers
		/// </summary>
		private static RepositoryItem GetCalcEdit()
		{
			RepositoryItemCalcEdit item = new RepositoryItemCalcEdit();

			// Configure the item
            item.DisplayFormat.FormatString = "{0:c}";
            item.DisplayFormat.FormatType = FormatType.Numeric;
            item.EditFormat.FormatString = "{0:f2}";
            item.EditFormat.FormatType = FormatType.Numeric;
			item.EditMask = "c";
			item.Mask.BeepOnError = true;
			item.NullText = string.Empty;
			item.Precision = 2;

			return item;
		}

		/// <summary>
		/// Return a Date field
		/// </summary>
		private static RepositoryItem GetDateEdit()
		{
			RepositoryItemDateEdit item = new RepositoryItemDateEdit();

			// Configure the item
			item.ShowClear = true;
			item.ShowToday = true;
			item.ShowWeekNumbers = false;
			item.VistaDisplayMode = DefaultBoolean.True;
            item.DisplayFormat.FormatString = "{0:d}";
            item.DisplayFormat.FormatType = FormatType.DateTime;
            item.EditFormat.FormatString = "{0:d}";
            item.EditFormat.FormatType = FormatType.DateTime;
			item.EditMask = "d";
			item.Mask.BeepOnError = true;
			item.NullText = string.Empty;

			return item;
		}
	}
}
