using System;
using System.Data;
using System.IO;
using DebtPlus.Interfaces.Creditor;
using DebtPlus.Reports.CS.Template.Forms;
using DevExpress.XtraReports.UI;

namespace DebtPlus.Reports.CS.Creditor.ClientList
{
    public class ClientsByCreditorListReport : DebtPlus.Reports.CS.Template.TemplateXtraReportClass, ICreditor
    {
        private static readonly string namespaceValue = "DebtPlus.Reports.CS.Creditor.ClientList";
        public ClientsByCreditorListReport()
            : base()
        {
            InitializeComponent();

            // Set the script references as needed
            ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();

            // Copy the script references to the subreports as well
            foreach (DevExpress.XtraReports.UI.Band Band in Bands)
            {
                foreach (var ctl in Band.Controls)
                {
                    var rpt = ctl as XRSubreport;
                    if (rpt != null)
                    {
                        var SubRpt = rpt.ReportSource as XtraReport;
                        if (SubRpt != null)
                        {
                            SubRpt.ScriptReferences = DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();
                        }
                    }
                }
            }

            // Remove the parameter values
            Parameters["ParameterCreditor"].Value = string.Empty;

            // Enable the select expert
            ReportFilter.IsEnabled = true;
        }

        private void InitializeComponent()
        {
            const string ReportName = "DebtPlus.Reports.Creditor.ClientList.repx";

            // See if there is a report reference that we can use
            string Fname = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName);
            bool UseDefault = true;
            try
            {
                if ((!DebtPlus.Configuration.Config.LocalReportsOnly) && System.IO.File.Exists(Fname))
                {
                    LoadLayout(Fname);
                    UseDefault = false;
                }
            }
            catch
            {
            }

            // Load the standard report definition if we need a new item
            if (UseDefault)
            {
                var asm = System.Reflection.Assembly.GetExecutingAssembly();
                string manifestName = string.Format("{0}.{1}", namespaceValue, ReportName);
                using (Stream ios = asm.GetManifestResourceStream(manifestName))
                {
                    if (ios != null)
                    {
                        LoadLayout(ios);
                    }
                }
            }
        }

        /// <summary>
        ///     Report title information
        /// </summary>
        public override string ReportTitle
        {
            get
            {
                return "Clients By Creditor";
            }
        }

        /// <summary>
        ///     Report subtitle information
        /// </summary>
        public override string ReportSubTitle
        {
            get
            {
                string CreditorName = string.Empty;
                DataView vue = DataSource as DataView;
                if (vue != null)
                {
                    DataSet ds = (DataSet)vue.Table.DataSet;
                    DataTable tbl = ds.Tables["creditors"];
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        DataRow row = tbl.Rows[0];
                        CreditorName = DebtPlus.Utils.Nulls.DStr(row["creditor_name"]).Trim();
                    }
                }

                string Answer = null;
                string id = DebtPlus.Utils.Nulls.DStr(Parameters["ParameterCreditor"].Value);
                if (id != string.Empty && CreditorName != string.Empty)
                {
                    Answer = string.Format("{0} {1}", id, CreditorName);
                }
                else
                {
                    Answer = id + CreditorName;
                }

                return Answer;
            }
        }

        /// <summary>
        ///     Parameter for the creditor ID
        /// </summary>
        public string Parameter_Creditor
        {
            get
            {
                return Convert.ToString(Parameters["ParameterCreditor"].Value);
            }
            set
            {
                Parameters["ParameterCreditor"].Value = value;
            }
        }

        string ICreditor.Creditor
        {
            get
            {
                return Parameter_Creditor;
            }
            set
            {
                Parameter_Creditor = value;
            }
        }

        /// <summary>
        ///     Do we need to request parameters?
        /// </summary>
        public override bool NeedParameters()
        {
            return base.NeedParameters() || Parameter_Creditor == string.Empty;
        }

        /// <summary>
        ///     Change the parameter value for this report
        /// </summary>
        public override void SetReportParameter(string Parameter, object Value)
        {
            switch (Parameter)
            {
                case "Creditor":
                    Parameter_Creditor = Convert.ToString(Value);
                    break;
                default:
                    base.SetReportParameter(Parameter, Value);
                    break;
            }
        }

        /// <summary>
        ///     Request the parameters for the report from the user
        /// </summary>
        public override System.Windows.Forms.DialogResult RequestReportParameters()
        {
            var answer = System.Windows.Forms.DialogResult.OK;
            if (NeedParameters())
            {
                using (var frm = new CreditorParametersForm())
                {
                    answer = frm.ShowDialog();
                    Parameter_Creditor = frm.Parameter_Creditor;
                }
            }
            return answer;
        }
    }
}
