namespace DebtPlus.Reports.CS.Creditor.ClientList
{
    public partial class ClientsByCreditorListReport : DebtPlus.Interfaces.Creditor.ICreditor
    {
        /// <summary>
        /// Create an instance of our report
        /// </summary>
        public ClientsByCreditorListReport() : base()
        {
            InitializeComponent();

            // Add handler routines
            BeforePrint += ClientsByCreditorListReport_BeforePrint;

            // Default the parameters
            ParameterCreditor.Value = string.Empty;

            // Enable the select expert
            ReportFilter.IsEnabled = true;
        }

        /// <summary>
        /// Report title information
        /// </summary>
        public override string ReportTitle
        {
            get
            {
                return "Clients By Creditor";
            }
        }

        /// <summary>
        /// Report subtitle information
        /// </summary>
        public override string ReportSubTitle
        {
            get
            {
                string CreditorName = string.Empty;
                var vue = DataSource as System.Data.DataView;
                if (vue != null)
                {
                    var ds = (System.Data.DataSet)vue.Table.DataSet;
                    var tbl = ds.Tables["creditors"];
                    if (tbl != null && tbl.Rows.Count > 0)
                    {
                        System.Data.DataRow row = tbl.Rows[0];
                        CreditorName = (DebtPlus.Utils.Nulls.v_String(row["creditor_name"]) ?? string.Empty).Trim();
                    }
                }

                string id = DebtPlus.Utils.Nulls.v_String(Parameters["ParameterCreditor"].Value);
                return (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(CreditorName)) ? string.Format("{0} {1}", id, CreditorName) : id + CreditorName;
            }
        }

        public string Creditor
        {
            get
            {
                return Parameter_Creditor;
            }
            set
            {
                Parameter_Creditor = value;
            }
        }

        /// <summary>
        /// Parameter for the creditor ID
        /// </summary>
        public string Parameter_Creditor
        {
            get
            {
                var parm = FindParameter("ParameterCreditor");
                return (parm == null) ? string.Empty : System.Convert.ToString(parm.Value);
            }
            set
            {
                SetParameter("ParameterCreditor", typeof(string), value, "Creditor Label", false);
            }
        }

        /// <summary>
        /// Do we need to request parameters?
        /// </summary>
        public override bool NeedParameters()
        {
            return base.NeedParameters() || Parameter_Creditor == string.Empty;
        }

        /// <summary>
        /// Change the parameter value for this report
        /// </summary>
        public override void SetReportParameter(string Parameter, object Value)
        {
            switch (Parameter)
            {
                case "Creditor":
                    Parameter_Creditor = System.Convert.ToString(Value);
                    break;
                default:
                    base.SetReportParameter(Parameter, Value);
                    break;
            }
        }

        /// <summary>
        /// Request the parameters for the report from the user
        /// </summary>
        public override System.Windows.Forms.DialogResult RequestReportParameters()
        {
            var Answer = System.Windows.Forms.DialogResult.OK;
            if (NeedParameters())
            {
                using (var frm = new DebtPlus.Reports.CS.Template.Forms.CreditorParametersForm())
                {
                    Answer = frm.ShowDialog();
                    Parameter_Creditor = frm.Parameter_Creditor;
                }
            }
            return Answer;
        }

        /// <summary>
        /// Build the report dataset
        /// </summary>
        System.Data.DataSet ds = new System.Data.DataSet("ds");
        private void ClientsByCreditorListReport_BeforePrint(object sender, System.EventArgs e)
        {
            const string TableName = "view_clients_by_creditor";
            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)sender;
            System.Data.DataTable tbl = ds.Tables[TableName];

            if (tbl == null)
            {
                var sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                try
                {
                    using (var cn = new System.Data.SqlClient.SqlConnection(sqlInfo.ConnectionString))
                    {
                        cn.Open();

                        using (var cmd = new System.Data.SqlClient.SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT creditor, creditor_name FROM creditors WHERE creditor=@creditor";
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters["ParameterCreditor"].Value;

                            using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, "creditors");
                            }
                        }

                        using (var cmd = new System.Data.SqlClient.SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT always_disburse,[last_payment_date],[last_payment_amount],[client],[name],[status],[start_date],[account_number],[orig_balance],[principle_payments],[current_balance] FROM view_clients_by_creditor WITH (NOLOCK) WHERE (creditor=@creditor) AND ([status] in ('A','AR','EX') OR [always_disburse] <> 0) ORDER BY client";
                            cmd.CommandType = System.Data.CommandType.Text;
                            cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = rpt.Parameters["ParameterCreditor"].Value;
                            cmd.CommandTimeout = 0;

                            using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, TableName);
                                tbl = ds.Tables[TableName];
                            }
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading information for report");
                }
            }

            if (tbl != null)
            {
                rpt.DataSource = new System.Data.DataView(tbl, ((DebtPlus.Interfaces.Reports.IReportFilter)rpt).ReportFilter.ViewFilter, string.Empty, System.Data.DataViewRowState.CurrentRows);
            }
        }
    }
}
