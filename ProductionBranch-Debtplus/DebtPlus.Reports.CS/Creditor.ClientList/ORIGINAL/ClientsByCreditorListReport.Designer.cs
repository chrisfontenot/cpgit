namespace DebtPlus.Reports.CS.Creditor.ClientList
{
    partial class ClientsByCreditorListReport : DebtPlus.Reports.CS.Template.TemplateXtraReportClass
    {
        //XtraReport overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Designer
        //It can be modified using the Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientsByCreditorListReport));
            this.Style_Detail_LText = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Total_Amount = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Heading_Amount = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Heading_LText = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Heading_Pannel = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Detail_Amount = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Heading_RText = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Total_LText = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Detail_RText = new DevExpress.XtraReports.UI.XRControlStyle();
            this.Style_Total_RText = new DevExpress.XtraReports.UI.XRControlStyle();
            this.XrLabel_Client = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_OriginalBalance = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_CurrentBalance = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_LastPaymentDate = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_LastPaymentAmount = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_TotalPayments = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_StartDate = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_Status = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_AccountNumber = new DevExpress.XtraReports.UI.XRLabel();
            this.XrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.XrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.XrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.XrLabel_Total_CurrentBalance = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_Total_TotalPayments = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_Total_OriginalBalance = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.ParameterCreditor = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrPanel1});
            this.PageHeader.HeightF = 147F;
            this.PageHeader.Controls.SetChildIndex(this.XrPanel1, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrPanel_AgencyAddress, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrLabel_Title, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrLine_Title, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrLabel_Subtitle, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrPageInfo1, 0);
            // 
            // PageFooter
            // 
            this.PageFooter.HeightF = 35F;
            // 
            // XrLabel_Title
            // 
            this.XrLabel_Title.StylePriority.UseFont = false;
            // 
            // XrPageInfo_PageNumber
            // 
            this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
            // 
            // XRLabel_Agency_Name
            // 
            this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Address3
            // 
            this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Address1
            // 
            this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Phone
            // 
            this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
            this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
            // 
            // XrLabel_Agency_Address2
            // 
            this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel_AccountNumber,
            this.XrLabel_Status,
            this.XrLabel_StartDate,
            this.XrLabel_TotalPayments,
            this.XrLabel_LastPaymentAmount,
            this.XrLabel_LastPaymentDate,
            this.XrLabel_CurrentBalance,
            this.XrLabel_OriginalBalance,
            this.XrLabel_Client});
            this.Detail.HeightF = 15F;
            // 
            // Style_Detail_LText
            // 
            this.Style_Detail_LText.Name = "Style_Detail_LText";
            this.Style_Detail_LText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Style_Total_Amount
            // 
            this.Style_Total_Amount.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Style_Total_Amount.Name = "Style_Total_Amount";
            // 
            // Style_Heading_Amount
            // 
            this.Style_Heading_Amount.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Style_Heading_Amount.ForeColor = System.Drawing.Color.White;
            this.Style_Heading_Amount.Name = "Style_Heading_Amount";
            this.Style_Heading_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Style_Heading_LText
            // 
            this.Style_Heading_LText.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Style_Heading_LText.ForeColor = System.Drawing.Color.White;
            this.Style_Heading_LText.Name = "Style_Heading_LText";
            this.Style_Heading_LText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Style_Heading_Pannel
            // 
            this.Style_Heading_Pannel.BackColor = System.Drawing.Color.Teal;
            this.Style_Heading_Pannel.BorderColor = System.Drawing.Color.Teal;
            this.Style_Heading_Pannel.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Style_Heading_Pannel.ForeColor = System.Drawing.Color.White;
            this.Style_Heading_Pannel.Name = "Style_Heading_Pannel";
            this.Style_Heading_Pannel.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // Style_Detail_Amount
            // 
            this.Style_Detail_Amount.Name = "Style_Detail_Amount";
            this.Style_Detail_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Style_Heading_RText
            // 
            this.Style_Heading_RText.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Style_Heading_RText.ForeColor = System.Drawing.Color.White;
            this.Style_Heading_RText.Name = "Style_Heading_RText";
            this.Style_Heading_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Style_Total_LText
            // 
            this.Style_Total_LText.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Style_Total_LText.Name = "Style_Total_LText";
            // 
            // Style_Detail_RText
            // 
            this.Style_Detail_RText.Name = "Style_Detail_RText";
            this.Style_Detail_RText.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // Style_Total_RText
            // 
            this.Style_Total_RText.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.Style_Total_RText.Name = "Style_Total_RText";
            // 
            // XrLabel_Client
            // 
            this.XrLabel_Client.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.XrLabel_Client.Name = "XrLabel_Client";
            this.XrLabel_Client.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_Client.SizeF = new System.Drawing.SizeF(190.1666F, 15F);
            this.XrLabel_Client.StyleName = "Style_Detail_LText";
            this.XrLabel_Client.Text = "[client!0000000] [name]";
            this.XrLabel_Client.WordWrap = false;
            // 
            // XrLabel_OriginalBalance
            // 
            this.XrLabel_OriginalBalance.CanGrow = false;
            this.XrLabel_OriginalBalance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "orig_balance", "{0:c}")});
            this.XrLabel_OriginalBalance.KeepTogether = true;
            this.XrLabel_OriginalBalance.LocationFloat = new DevExpress.Utils.PointFloat(361F, 0F);
            this.XrLabel_OriginalBalance.Name = "XrLabel_OriginalBalance";
            this.XrLabel_OriginalBalance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_OriginalBalance.SizeF = new System.Drawing.SizeF(75F, 15F);
            this.XrLabel_OriginalBalance.StyleName = "Style_Detail_Amount";
            this.XrLabel_OriginalBalance.StylePriority.UseTextAlignment = false;
            this.XrLabel_OriginalBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_OriginalBalance.WordWrap = false;
            // 
            // XrLabel_CurrentBalance
            // 
            this.XrLabel_CurrentBalance.CanGrow = false;
            this.XrLabel_CurrentBalance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "current_balance", "{0:c}")});
            this.XrLabel_CurrentBalance.KeepTogether = true;
            this.XrLabel_CurrentBalance.LocationFloat = new DevExpress.Utils.PointFloat(720.4583F, 0F);
            this.XrLabel_CurrentBalance.Name = "XrLabel_CurrentBalance";
            this.XrLabel_CurrentBalance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_CurrentBalance.SizeF = new System.Drawing.SizeF(79.54169F, 15F);
            this.XrLabel_CurrentBalance.StyleName = "Style_Detail_Amount";
            this.XrLabel_CurrentBalance.StylePriority.UseTextAlignment = false;
            this.XrLabel_CurrentBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_CurrentBalance.WordWrap = false;
            // 
            // XrLabel_LastPaymentDate
            // 
            this.XrLabel_LastPaymentDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "last_payment_date", "{0:d}")});
            this.XrLabel_LastPaymentDate.KeepTogether = true;
            this.XrLabel_LastPaymentDate.LocationFloat = new DevExpress.Utils.PointFloat(651.0001F, 0F);
            this.XrLabel_LastPaymentDate.Name = "XrLabel_LastPaymentDate";
            this.XrLabel_LastPaymentDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_LastPaymentDate.SizeF = new System.Drawing.SizeF(69.45825F, 15F);
            this.XrLabel_LastPaymentDate.StyleName = "Style_Detail_RText";
            this.XrLabel_LastPaymentDate.StylePriority.UseTextAlignment = false;
            this.XrLabel_LastPaymentDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_LastPaymentDate.WordWrap = false;
            // 
            // XrLabel_LastPaymentAmount
            // 
            this.XrLabel_LastPaymentAmount.CanGrow = false;
            this.XrLabel_LastPaymentAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "last_payment_amount", "{0:c}")});
            this.XrLabel_LastPaymentAmount.KeepTogether = true;
            this.XrLabel_LastPaymentAmount.LocationFloat = new DevExpress.Utils.PointFloat(584F, 0F);
            this.XrLabel_LastPaymentAmount.Name = "XrLabel_LastPaymentAmount";
            this.XrLabel_LastPaymentAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_LastPaymentAmount.SizeF = new System.Drawing.SizeF(66F, 15F);
            this.XrLabel_LastPaymentAmount.StyleName = "Style_Detail_Amount";
            this.XrLabel_LastPaymentAmount.StylePriority.UseTextAlignment = false;
            this.XrLabel_LastPaymentAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_LastPaymentAmount.WordWrap = false;
            // 
            // XrLabel_TotalPayments
            // 
            this.XrLabel_TotalPayments.CanGrow = false;
            this.XrLabel_TotalPayments.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "principle_payments", "{0:c}")});
            this.XrLabel_TotalPayments.KeepTogether = true;
            this.XrLabel_TotalPayments.LocationFloat = new DevExpress.Utils.PointFloat(509F, 0F);
            this.XrLabel_TotalPayments.Name = "XrLabel_TotalPayments";
            this.XrLabel_TotalPayments.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_TotalPayments.SizeF = new System.Drawing.SizeF(75F, 15F);
            this.XrLabel_TotalPayments.StyleName = "Style_Detail_Amount";
            this.XrLabel_TotalPayments.StylePriority.UseTextAlignment = false;
            this.XrLabel_TotalPayments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_TotalPayments.WordWrap = false;
            // 
            // XrLabel_StartDate
            // 
            this.XrLabel_StartDate.CanGrow = false;
            this.XrLabel_StartDate.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "start_date", "{0:d}")});
            this.XrLabel_StartDate.KeepTogether = true;
            this.XrLabel_StartDate.LocationFloat = new DevExpress.Utils.PointFloat(436F, 0F);
            this.XrLabel_StartDate.Name = "XrLabel_StartDate";
            this.XrLabel_StartDate.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_StartDate.SizeF = new System.Drawing.SizeF(73.00003F, 15F);
            this.XrLabel_StartDate.StyleName = "Style_Detail_RText";
            this.XrLabel_StartDate.StylePriority.UseTextAlignment = false;
            this.XrLabel_StartDate.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_StartDate.WordWrap = false;
            // 
            // XrLabel_Status
            // 
            this.XrLabel_Status.CanGrow = false;
            this.XrLabel_Status.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "status")});
            this.XrLabel_Status.KeepTogether = true;
            this.XrLabel_Status.LocationFloat = new DevExpress.Utils.PointFloat(322.9999F, 0F);
            this.XrLabel_Status.Name = "XrLabel_Status";
            this.XrLabel_Status.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_Status.SizeF = new System.Drawing.SizeF(38.00003F, 15F);
            this.XrLabel_Status.StyleName = "Style_Detail_RText";
            this.XrLabel_Status.StylePriority.UseTextAlignment = false;
            this.XrLabel_Status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XrLabel_Status.WordWrap = false;
            // 
            // XrLabel_AccountNumber
            // 
            this.XrLabel_AccountNumber.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "account_number")});
            this.XrLabel_AccountNumber.LocationFloat = new DevExpress.Utils.PointFloat(190.1666F, 0F);
            this.XrLabel_AccountNumber.Name = "XrLabel_AccountNumber";
            this.XrLabel_AccountNumber.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_AccountNumber.SizeF = new System.Drawing.SizeF(132.8333F, 15F);
            this.XrLabel_AccountNumber.StyleName = "Style_Detail_LText";
            this.XrLabel_AccountNumber.WordWrap = false;
            // 
            // XrPanel1
            // 
            this.XrPanel1.BackColor = System.Drawing.Color.Teal;
            this.XrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLabel9,
            this.XrLabel8,
            this.XrLabel7,
            this.XrLabel6,
            this.XrLabel5,
            this.XrLabel3,
            this.XrLabel2,
            this.XrLabel1});
            this.XrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 125F);
            this.XrPanel1.Name = "XrPanel1";
            this.XrPanel1.SizeF = new System.Drawing.SizeF(800F, 16F);
            this.XrPanel1.StyleName = "Style_Heading_Pannel";
            // 
            // XrLabel9
            // 
            this.XrLabel9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel9.ForeColor = System.Drawing.Color.White;
            this.XrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(190.1666F, 0F);
            this.XrLabel9.Name = "XrLabel9";
            this.XrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel9.SizeF = new System.Drawing.SizeF(132.8333F, 15F);
            this.XrLabel9.StyleName = "Style_Heading_LText";
            this.XrLabel9.Text = "ACCOUNT";
            this.XrLabel9.WordWrap = false;
            // 
            // XrLabel8
            // 
            this.XrLabel8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel8.ForeColor = System.Drawing.Color.White;
            this.XrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.XrLabel8.Name = "XrLabel8";
            this.XrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel8.SizeF = new System.Drawing.SizeF(190.1666F, 15F);
            this.XrLabel8.StyleName = "Style_Heading_LText";
            this.XrLabel8.Text = "CLIENT";
            this.XrLabel8.WordWrap = false;
            // 
            // XrLabel7
            // 
            this.XrLabel7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel7.ForeColor = System.Drawing.Color.White;
            this.XrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(720.4583F, 0F);
            this.XrLabel7.Name = "XrLabel7";
            this.XrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel7.SizeF = new System.Drawing.SizeF(79.54169F, 15F);
            this.XrLabel7.StyleName = "Style_Heading_RText";
            this.XrLabel7.Text = "CUR $";
            this.XrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.XrLabel7.WordWrap = false;
            // 
            // XrLabel6
            // 
            this.XrLabel6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel6.ForeColor = System.Drawing.Color.White;
            this.XrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(436F, 0F);
            this.XrLabel6.Name = "XrLabel6";
            this.XrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel6.SizeF = new System.Drawing.SizeF(73.00003F, 15F);
            this.XrLabel6.StyleName = "Style_Heading_RText";
            this.XrLabel6.Text = "START";
            this.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.XrLabel6.WordWrap = false;
            // 
            // XrLabel5
            // 
            this.XrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel5.ForeColor = System.Drawing.Color.White;
            this.XrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(360.9999F, 0F);
            this.XrLabel5.Name = "XrLabel5";
            this.XrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel5.SizeF = new System.Drawing.SizeF(75F, 15F);
            this.XrLabel5.StyleName = "Style_Heading_RText";
            this.XrLabel5.Text = "ORIG $";
            this.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.XrLabel5.WordWrap = false;
            // 
            // XrLabel3
            // 
            this.XrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel3.ForeColor = System.Drawing.Color.White;
            this.XrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(322.9999F, 0F);
            this.XrLabel3.Name = "XrLabel3";
            this.XrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel3.SizeF = new System.Drawing.SizeF(38.00003F, 15F);
            this.XrLabel3.StyleName = "Style_Heading_RText";
            this.XrLabel3.StylePriority.UseTextAlignment = false;
            this.XrLabel3.Text = "STS";
            this.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.XrLabel3.WordWrap = false;
            // 
            // XrLabel2
            // 
            this.XrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel2.ForeColor = System.Drawing.Color.White;
            this.XrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(584F, 0F);
            this.XrLabel2.Name = "XrLabel2";
            this.XrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel2.SizeF = new System.Drawing.SizeF(136.4583F, 15F);
            this.XrLabel2.StyleName = "Style_Heading_RText";
            this.XrLabel2.Text = "LAST PMT&DATE";
            this.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.XrLabel2.WordWrap = false;
            // 
            // XrLabel1
            // 
            this.XrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel1.ForeColor = System.Drawing.Color.White;
            this.XrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(509F, 0F);
            this.XrLabel1.Name = "XrLabel1";
            this.XrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel1.SizeF = new System.Drawing.SizeF(75F, 15F);
            this.XrLabel1.StyleName = "Style_Heading_RText";
            this.XrLabel1.Text = "PTD";
            this.XrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.XrLabel1.WordWrap = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.XrLine1,
            this.XrLabel_Total_CurrentBalance,
            this.XrLabel_Total_TotalPayments,
            this.XrLabel_Total_OriginalBalance,
            this.XrLabel4});
            this.ReportFooter.HeightF = 44F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.PrintAtBottom = true;
            // 
            // XrLine1
            // 
            this.XrLine1.ForeColor = System.Drawing.Color.Teal;
            this.XrLine1.LineWidth = 2;
            this.XrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 7.999992F);
            this.XrLine1.Name = "XrLine1";
            this.XrLine1.SizeF = new System.Drawing.SizeF(800F, 9F);
            // 
            // XrLabel_Total_CurrentBalance
            // 
            this.XrLabel_Total_CurrentBalance.CanGrow = false;
            this.XrLabel_Total_CurrentBalance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "current_balance")});
            this.XrLabel_Total_CurrentBalance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel_Total_CurrentBalance.LocationFloat = new DevExpress.Utils.PointFloat(592F, 17F);
            this.XrLabel_Total_CurrentBalance.Name = "XrLabel_Total_CurrentBalance";
            this.XrLabel_Total_CurrentBalance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_Total_CurrentBalance.SizeF = new System.Drawing.SizeF(208F, 15F);
            this.XrLabel_Total_CurrentBalance.StyleName = "Style_Total_Amount";
            this.XrLabel_Total_CurrentBalance.StylePriority.UseTextAlignment = false;
            xrSummary1.FormatString = "{0:c}";
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_Total_CurrentBalance.Summary = xrSummary1;
            this.XrLabel_Total_CurrentBalance.Tag = "current_balance";
            this.XrLabel_Total_CurrentBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_Total_CurrentBalance.WordWrap = false;
            // 
            // XrLabel_Total_TotalPayments
            // 
            this.XrLabel_Total_TotalPayments.CanGrow = false;
            this.XrLabel_Total_TotalPayments.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "principle_payments")});
            this.XrLabel_Total_TotalPayments.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel_Total_TotalPayments.LocationFloat = new DevExpress.Utils.PointFloat(468F, 17F);
            this.XrLabel_Total_TotalPayments.Name = "XrLabel_Total_TotalPayments";
            this.XrLabel_Total_TotalPayments.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_Total_TotalPayments.SizeF = new System.Drawing.SizeF(116F, 15F);
            this.XrLabel_Total_TotalPayments.StyleName = "Style_Total_Amount";
            this.XrLabel_Total_TotalPayments.StylePriority.UseTextAlignment = false;
            xrSummary2.FormatString = "{0:c}";
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_Total_TotalPayments.Summary = xrSummary2;
            this.XrLabel_Total_TotalPayments.Tag = "principle_payments";
            this.XrLabel_Total_TotalPayments.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_Total_TotalPayments.WordWrap = false;
            // 
            // XrLabel_Total_OriginalBalance
            // 
            this.XrLabel_Total_OriginalBalance.CanGrow = false;
            this.XrLabel_Total_OriginalBalance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "orig_balance")});
            this.XrLabel_Total_OriginalBalance.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel_Total_OriginalBalance.LocationFloat = new DevExpress.Utils.PointFloat(233F, 17F);
            this.XrLabel_Total_OriginalBalance.Name = "XrLabel_Total_OriginalBalance";
            this.XrLabel_Total_OriginalBalance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel_Total_OriginalBalance.SizeF = new System.Drawing.SizeF(208F, 15F);
            this.XrLabel_Total_OriginalBalance.StyleName = "Style_Total_Amount";
            this.XrLabel_Total_OriginalBalance.StylePriority.UseTextAlignment = false;
            xrSummary3.FormatString = "{0:c}";
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_Total_OriginalBalance.Summary = xrSummary3;
            this.XrLabel_Total_OriginalBalance.Tag = "orig_balance";
            this.XrLabel_Total_OriginalBalance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.XrLabel_Total_OriginalBalance.WordWrap = false;
            // 
            // XrLabel4
            // 
            this.XrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.XrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 17F);
            this.XrLabel4.Name = "XrLabel4";
            this.XrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XrLabel4.SizeF = new System.Drawing.SizeF(208F, 15F);
            this.XrLabel4.StyleName = "Style_Total_LText";
            this.XrLabel4.StylePriority.UseTextAlignment = false;
            this.XrLabel4.Text = "Grand Total";
            this.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // ParameterCreditor
            // 
            this.ParameterCreditor.Description = "Creditor ID";
            this.ParameterCreditor.Name = "ParameterCreditor";
            this.ParameterCreditor.Visible = false;
            // 
            // ClientsByCreditorListReport
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter,
            this.TopMarginBand1,
            this.BottomMarginBand1,
            this.ReportFooter});
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] {
            this.ParameterCreditor});
            this.RequestParameters = false;
            this.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString");
            this.Scripts.OnBeforePrint = "ClientsByCreditorListReport_BeforePrint";
            this.ScriptsSource = resources.GetString("$this.ScriptsSource");
            this.ShowPrintMarginsWarning = false;
            this.ShowPrintStatusDialog = false;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.XrControlStyle_Header,
            this.XrControlStyle_Totals,
            this.XrControlStyle_HeaderPannel,
            this.XrControlStyle_GroupHeader,
            this.Style_Detail_LText,
            this.Style_Total_Amount,
            this.Style_Heading_Amount,
            this.Style_Heading_LText,
            this.Style_Heading_Pannel,
            this.Style_Detail_Amount,
            this.Style_Heading_RText,
            this.Style_Total_LText,
            this.Style_Detail_RText,
            this.Style_Total_RText});
            this.Version = "11.2";
            this.Controls.SetChildIndex(this.ReportFooter, 0);
            this.Controls.SetChildIndex(this.BottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.TopMarginBand1, 0);
            this.Controls.SetChildIndex(this.PageFooter, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        private DevExpress.XtraReports.UI.XRLabel XrLabel_Client;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_OriginalBalance;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_CurrentBalance;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_LastPaymentDate;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_LastPaymentAmount;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_TotalPayments;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_StartDate;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_Status;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_AccountNumber;
        private DevExpress.XtraReports.UI.XRPanel XrPanel1;
        private DevExpress.XtraReports.UI.XRLabel XrLabel1;
        private DevExpress.XtraReports.UI.XRLabel XrLabel2;
        private DevExpress.XtraReports.UI.XRLabel XrLabel3;
        private DevExpress.XtraReports.UI.XRLabel XrLabel5;
        private DevExpress.XtraReports.UI.XRLabel XrLabel6;
        private DevExpress.XtraReports.UI.XRLabel XrLabel7;
        private DevExpress.XtraReports.UI.XRLabel XrLabel8;
        private DevExpress.XtraReports.UI.XRLabel XrLabel9;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel XrLabel4;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_Total_OriginalBalance;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_Total_TotalPayments;
        private DevExpress.XtraReports.UI.XRLabel XrLabel_Total_CurrentBalance;
        private DevExpress.XtraReports.UI.XRLine XrLine1;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Detail_LText;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Total_Amount;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Heading_Amount;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Heading_LText;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Heading_Pannel;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Detail_Amount;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Heading_RText;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Total_LText;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Detail_RText;
        private DevExpress.XtraReports.UI.XRControlStyle Style_Total_RText;
        public DevExpress.XtraReports.Parameters.Parameter ParameterCreditor;
    }
}
