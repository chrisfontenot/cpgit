﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Data.Forms
{
    partial class DebtPlusForm
    {
        /// <summary>
        /// Class used to hold the form location information used by the serializer.
        /// </summary>
        public class FormLocation : IDisposable
        {
            public Int32 X { get; set; }
            public Int32 Y { get; set; }
            public Int32 W { get; set; }
            public Int32 H { get; set; }
            public string Version { get; set; }

            public FormLocation()
            {
                Version = "1";
            }

            public FormLocation(DebtPlus.Data.Forms.DebtPlusForm frm)
                : this()
            {
                this.X = frm.Location.X;
                this.Y = frm.Location.Y;
                this.W = frm.Size.Width;
                this.H = frm.Size.Height;
            }

            public void Dispose() // Implements IDisposable.Dispose
            {
                GC.SuppressFinalize(this);
            }
        }
    }
}
