﻿namespace DebtPlus.Data.Forms
{
    sealed partial class AboutBoxForm
    {
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBoxForm));
            this.Line = new System.Windows.Forms.Label();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.TextBox1 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_copyright = new DevExpress.XtraEditors.LabelControl();
            this.LinkLabel1 = new System.Windows.Forms.LinkLabel();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.Registered = new DevExpress.XtraEditors.LabelControl();
            this.Label3 = new DevExpress.XtraEditors.LabelControl();
            this.Version = new DevExpress.XtraEditors.LabelControl();
            this.Button_SystemInfo = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.PictureBox1).BeginInit();
            this.SuspendLayout();
            //
            //Line
            //
            this.Line.Location = new System.Drawing.Point(12, 187);
            this.Line.Name = "Line";
            this.Line.Size = new System.Drawing.Size(380, 10);
            this.ToolTipController1.SetSuperTip(this.Line, null);
            this.Line.TabIndex = 5;
            this.Line.UseMnemonic = false;
            //
            //PictureBox1
            //
            this.PictureBox1.Image = ((System.Drawing.Image)resources.GetObject("PictureBox1.Image"));
            this.PictureBox1.Location = new System.Drawing.Point(8, 0);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(64, 181);
            this.ToolTipController1.SetSuperTip(this.PictureBox1, null);
            this.PictureBox1.TabIndex = 0;
            this.PictureBox1.TabStop = false;
            //
            //TextBox1
            //
            this.TextBox1.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            this.TextBox1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.TextBox1.Appearance.Options.UseBackColor = true;
            this.TextBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.TextBox1.Location = new System.Drawing.Point(80, 11);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(171, 26);
            this.TextBox1.TabIndex = 0;
            this.TextBox1.Text = "DebtPlus Debt Management System\r\nFor Consumer Credit Counseling";
            //
            //lbl_copyright
            //
            this.lbl_copyright.Location = new System.Drawing.Point(80, 43);
            this.lbl_copyright.Name = "lbl_copyright";
            this.lbl_copyright.Size = new System.Drawing.Size(196, 13);
            this.lbl_copyright.TabIndex = 1;
            this.lbl_copyright.Text = "Copyright � 2000-2008 DebtPlus, L.L.C.";
            //
            //LinkLabel1
            //
            this.LinkLabel1.Location = new System.Drawing.Point(80, 83);
            this.LinkLabel1.Name = "LinkLabel1";
            this.LinkLabel1.Size = new System.Drawing.Size(232, 17);
            this.ToolTipController1.SetSuperTip(this.LinkLabel1, null);
            this.LinkLabel1.TabIndex = 2;
            this.LinkLabel1.TabStop = true;
            this.LinkLabel1.Text = "http://www.DebtPlus.com/";
            //
            //Label2
            //
            this.Label2.Location = new System.Drawing.Point(80, 103);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(127, 13);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "This product is licensed to:";
            //
            //Registered
            //
            this.Registered.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Registered.Appearance.Options.UseBackColor = true;
            this.Registered.Appearance.Options.UseTextOptions = true;
            this.Registered.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Registered.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.Registered.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Registered.Location = new System.Drawing.Point(80, 123);
            this.Registered.Name = "Registered";
            this.Registered.Size = new System.Drawing.Size(308, 58);
            this.Registered.TabIndex = 4;
            //
            //Label3
            //
            this.Label3.Appearance.Options.UseTextOptions = true;
            this.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Label3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.Label3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Label3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label3.Location = new System.Drawing.Point(12, 201);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(278, 77);
            this.Label3.TabIndex = 6;
            this.Label3.Text = resources.GetString("Label3.Text");
            this.Label3.UseMnemonic = false;
            //
            //Version
            //
            this.Version.Appearance.ForeColor = System.Drawing.Color.Purple;
            this.Version.Appearance.Options.UseForeColor = true;
            this.Version.Location = new System.Drawing.Point(12, 284);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(140, 13);
            this.Version.TabIndex = 7;
            this.Version.Text = "Version PRE-RELEASED 0.0.0";
            //
            //Button_SystemInfo
            //
            this.Button_SystemInfo.Location = new System.Drawing.Point(296, 233);
            this.Button_SystemInfo.Name = "Button_SystemInfo";
            this.Button_SystemInfo.Size = new System.Drawing.Size(96, 24);
            this.Button_SystemInfo.TabIndex = 9;
            this.Button_SystemInfo.Text = "System Info...";
            this.Button_SystemInfo.ToolTip = "Click here to show additional information about your system configuration.";
            //
            //Button_OK
            //
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(296, 198);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(96, 25);
            this.Button_OK.TabIndex = 8;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to dismiss the dialog and return to the previous form";
            //
            //AboutBoxForm
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.ClientSize = new System.Drawing.Size(400, 306);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Button_SystemInfo);
            this.Controls.Add(this.Version);
            this.Controls.Add(this.Registered);
            this.Controls.Add(this.TextBox1);
            this.Controls.Add(this.LinkLabel1);
            this.Controls.Add(this.lbl_copyright);
            this.Controls.Add(this.PictureBox1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Line);
            this.Controls.Add(this.Label2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutBoxForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "About DebtPlus";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.PictureBox1).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        private DevExpress.XtraEditors.LabelControl TextBox1;
        private DevExpress.XtraEditors.LabelControl lbl_copyright;
        private System.Windows.Forms.LinkLabel LinkLabel1;
        private DevExpress.XtraEditors.LabelControl Label2;
        private DevExpress.XtraEditors.LabelControl Registered;
        private DevExpress.XtraEditors.LabelControl Label3;
        private System.Windows.Forms.PictureBox PictureBox1;
        private DevExpress.XtraEditors.LabelControl Version;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.SimpleButton Button_SystemInfo;
        private System.Windows.Forms.Label Line;
    }
}
