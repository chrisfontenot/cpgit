#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.Data.Forms
{
    public class InputBox
    {

        /// <summary>
        /// Input box routine for DebtPlus. This variation allows the Cancel
        /// button status to be returned to the application.
        /// </summary>
        internal class XtraInputBoxArgs
        {
            private DevExpress.LookAndFeel.UserLookAndFeel privateUserLookAndFeel;

            public XtraInputBoxArgs() :
                this(null, null, string.Empty, string.Empty, string.Empty, Int32.MaxValue)
            {
            }

            public XtraInputBoxArgs(IWin32Window Owner, string Caption, string Text, string DefaultResponse, Int32 MaximumLength) :
                this(null, Owner, Caption, Text, DefaultResponse, MaximumLength)
            {
            }

            public XtraInputBoxArgs(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, IWin32Window Owner, string Caption, string Text, string DefaultResponse, Int32 MaximumLength)
            {
                privateUserLookAndFeel = UserLookAndFeel;
                this.Owner = Owner;
                this.Text = Text;
                this.Caption = Caption;
                this.DefaultResponse = DefaultResponse;
                this.Response = DefaultResponse;
                this.MaximumLength = MaximumLength;
            }

            public IWin32Window Owner { get; private set; }
            public static Icon Icon { get; private set; }
            public string Caption { get; private set; }
            public string Text { get; private set; }

            private DialogResult[] privateButtons = new DialogResult[] { DialogResult.OK, DialogResult.Cancel };
            public DialogResult[] Buttons
            {
                get
                {
                    return privateButtons;
                }
            }

            public static Int32 DefaultButtonIndex { get; private set; }

            public DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel
            {
                get
                {
                    if (privateUserLookAndFeel != null)
                    {
                        return privateUserLookAndFeel;
                    }
                    if (Owner is DevExpress.XtraEditors.XtraForm)
                    {
                        DevExpress.XtraEditors.XtraForm form = ((DevExpress.XtraEditors.XtraForm)Owner);
                        if (form != null)
                        {
                            return form.LookAndFeel;
                        }
                    }
                    return null;
                }

                set
                {
                    privateUserLookAndFeel = value;
                }
            }

            public string Response { get; set; }
            public string DefaultResponse { get; private set; }
            public Int32 MaximumLength { get; private set; }
        }

        // List of functions with lookandfeel but without an owner window
        public static DialogResult Show(string Prompt, ref string Response)
        {
            return ConductDialog(new XtraInputBoxArgs(null, string.Empty, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(string Prompt, ref string Response, string Caption)
        {
            return ConductDialog(new XtraInputBoxArgs(null, Caption, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(string Prompt, ref string Response, string Caption, string DefaultResponse)
        {
            return ConductDialog(new XtraInputBoxArgs(null, Caption, Prompt, DefaultResponse, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(string Prompt, ref string Response, string Caption, string DefaultResponse, Int32 MaximumLength)
        {
            return ConductDialog(new XtraInputBoxArgs(null, Caption, Prompt, DefaultResponse, MaximumLength), ref Response);
        }

        // list of functions with lookandfeel and an owner window
        public static DialogResult Show(IWin32Window Owner, string Prompt, ref string Response)
        {
            return ConductDialog(new XtraInputBoxArgs(Owner, string.Empty, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(IWin32Window Owner, string Prompt, ref string Response, string Caption)
        {
            return ConductDialog(new XtraInputBoxArgs(Owner, Caption, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(IWin32Window Owner, string Prompt, ref string Response, string Caption, string DefaultResponse)
        {
            return ConductDialog(new XtraInputBoxArgs(Owner, Caption, Prompt, DefaultResponse, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(IWin32Window Owner, string Prompt, ref string Response, string Caption, string DefaultResponse, Int32 MaximumLength)
        {
            return ConductDialog(new XtraInputBoxArgs(Owner, Caption, Prompt, DefaultResponse, MaximumLength), ref Response);
        }

        // List of functions with lookandfeel but without an owner window
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, string Prompt, ref string Response)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, null, string.Empty, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, string Prompt, ref string Response, string Caption)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, null, Caption, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, string Prompt, ref string Response, string Caption, string DefaultResponse)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, null, Caption, Prompt, DefaultResponse, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, string Prompt, ref string Response, string Caption, string DefaultResponse, Int32 MaximumLength)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, null, Caption, Prompt, DefaultResponse, MaximumLength), ref Response);
        }

        // list of functions with lookandfeel and an owner window
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, IWin32Window Owner, string Prompt, ref string Response)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, Owner, string.Empty, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, IWin32Window Owner, string Prompt, ref string Response, string Caption)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, Owner, Caption, Prompt, string.Empty, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, IWin32Window Owner, string Prompt, ref string Response, string Caption, string DefaultResponse)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, Owner, Caption, Prompt, DefaultResponse, Int32.MaxValue), ref Response);
        }
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, IWin32Window Owner, string Prompt, ref string Response, string Caption, string DefaultResponse, Int32 MaximumLength)
        {
            return ConductDialog(new XtraInputBoxArgs(UserLookAndFeel, Owner, Caption, Prompt, DefaultResponse, MaximumLength), ref Response);
        }

        private static DialogResult ConductDialog(XtraInputBoxArgs Msg, ref string Response)
        {
            DialogResult Answer;

            // Beep the speaker
            DebtPlus.Data.Forms.DebtPlusForm.DoMessageBeep(DebtPlus.Data.Forms.DebtPlusForm.MessageBeepEnum.Question);

            // Conduct the form
            using (var frm = new XtraInputBoxForm(Msg))
            {
                Answer = frm.ShowMessageBoxDialog();
                Response = Msg.Response;
            }

            return Answer;
        }

        public class XtraInputBoxForm : DebtPlus.Data.Forms.DebtPlusForm
        {
            private const Int32 Spacing = 12;
            private ArrayList buttons = new ArrayList();

            public Rectangle IconRectangle;
            public Rectangle MessageRectangle;
            public Rectangle ResponseRectangle;

            public XtraInputBoxForm()
                : this(new XtraInputBoxArgs())
            {
            }

            internal XtraInputBoxForm(XtraInputBoxArgs Message)
                : base()
            {
                KeyPreview = true;
                this.Message = Message;
            }

            public static bool AllowCustomLookAndFeel { get; set; }

            internal XtraInputBoxArgs Message { get; set; }

            private void CalcIconBounds()
            {
                if (XtraInputBoxArgs.Icon != null)
                {
                    IconRectangle = new Rectangle(Spacing, Spacing, XtraInputBoxArgs.Icon.Height, XtraInputBoxArgs.Icon.Width);
                }
                else
                {
                    IconRectangle = new Rectangle(Spacing, Spacing, 0, 0);
                }
            }

            private DevExpress.Utils.AppearanceObject GetPaintAppearance()
            {
                DevExpress.Utils.AppearanceObject paintAppearance = new DevExpress.Utils.AppearanceObject(Appearance, DefaultAppearance);
                paintAppearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                paintAppearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
                paintAppearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
                return paintAppearance;
            }

            protected override void OnVisibleChanged(EventArgs e)
            {
                base.OnVisibleChanged(e);
                if (Visible && !ContainsFocus)
                {
                    Activate();
                }
            }

            private void CalcMessageBounds()
            {
                Int32 messageTop, messageLeft, messageWidth, messageHeight;
                messageTop = Spacing;
                messageLeft = ((XtraInputBoxArgs.Icon == null) ? 0 : IconRectangle.Left + IconRectangle.Width) + Spacing;

                Int32 maxFormWidth = MaximumSize.Width;
                if (maxFormWidth <= 0)
                {
                    maxFormWidth = SystemInformation.WorkingArea.Width;
                }

                Int32 maxTextWidth = maxFormWidth - Spacing - messageLeft;
                if ((maxTextWidth < 10))
                {
                    maxTextWidth = 10;
                }

                SizeF textSize;
                DevExpress.Utils.Drawing.GraphicsInfo ginfo = new DevExpress.Utils.Drawing.GraphicsInfo();
                try
                {
                    ginfo.AddGraphics(null);
                    textSize = GetPaintAppearance().CalcTextSize(ginfo.Graphics, Message.Text, maxTextWidth);
                }
                finally
                {
                    ginfo.ReleaseGraphics();
                }

                messageWidth = Convert.ToInt32(System.Math.Ceiling(textSize.Width));
                Int32 maxFormHeight = MaximumSize.Height;
                if (maxFormHeight <= 0)
                {
                    maxFormHeight = SystemInformation.WorkingArea.Height;
                }

                //System.Int32 maxTextHeight = maxFormHeight - Spacing - ((DevExpress.XtraEditors.SimpleButton) buttons(0)).Height - Spacing - Text_Input.Height - Spacing - messageTop - SystemInformation.CaptionHeight
                Int32 maxTextHeight = maxFormHeight - Spacing - ((DevExpress.XtraEditors.SimpleButton)buttons[0]).Height - Spacing - messageTop - SystemInformation.CaptionHeight;
                if (maxTextHeight < 10)
                {
                    maxTextHeight = 10;
                }

                messageHeight = Convert.ToInt32(System.Math.Ceiling(textSize.Height));
                if (messageHeight > maxTextHeight)
                {
                    messageHeight = maxTextHeight;
                }

                MessageRectangle = new Rectangle(messageLeft, messageTop, messageWidth, messageHeight);
            }

            protected string GetButtonText(DialogResult Target)
            {
                switch (Target)
                {
                    case DialogResult.Abort:
                        return "&Abort";
                    case DialogResult.Cancel:
                        return "&Cancel";
                    case DialogResult.Ignore:
                        return "&Ignore";
                    case DialogResult.No:
                        return "&No";
                    case DialogResult.OK:
                        return "&Ok";
                    case DialogResult.Retry:
                        return "&Retry";
                    case DialogResult.Yes:
                        return "&Yes";
                    case DialogResult.None:
                        return string.Empty;
                    default:
                        return "&" + Target.ToString();
                }
            }

            DevExpress.XtraEditors.ButtonEdit ResponseInputBuffer = new DevExpress.XtraEditors.ButtonEdit();
            Int32 privateTabIndex;
            private void CalcFinalSizes()
            {

                Int32 buttonsTotalWidth = 0;
                foreach (DevExpress.XtraEditors.SimpleButton button in buttons)
                {
                    if (buttonsTotalWidth != 0)
                    {
                        buttonsTotalWidth += Spacing;
                    }
                    buttonsTotalWidth += button.Width;
                }

                Int32 buttonsTop = MessageRectangle.Bottom + Spacing;
                if (XtraInputBoxArgs.Icon != null && IconRectangle.Bottom + Spacing > buttonsTop)
                {
                    buttonsTop = IconRectangle.Bottom + Spacing;
                }

                Int32 wantedFormWidth = MinimumSize.Width;
                if (wantedFormWidth == 0)
                {
                    wantedFormWidth = SystemInformation.MinimumWindowSize.Width;
                }
                if (wantedFormWidth < MessageRectangle.Right + Spacing)
                {
                    wantedFormWidth = MessageRectangle.Right + Spacing;
                }
                if (wantedFormWidth < buttonsTotalWidth + Spacing + Spacing)
                {
                    wantedFormWidth = buttonsTotalWidth + Spacing + Spacing;
                }

                DevExpress.Utils.Drawing.GraphicsInfo ginfo = new DevExpress.Utils.Drawing.GraphicsInfo();
                try
                {
                    ginfo.AddGraphics(null);
                    StringFormat fmt = ((StringFormat)DevExpress.Utils.TextOptions.DefaultStringFormat.Clone());
                    fmt.FormatFlags = fmt.FormatFlags | StringFormatFlags.MeasureTrailingSpaces;
                    Font captionFont = DevExpress.Utils.ControlUtils.GetCaptionFont();
                    Int32 maxCaptionForcedWidth = SystemInformation.WorkingArea.Width / 3 * 2;
                    Int32 captionTextWidth = 1 + 4 + Convert.ToInt32(ginfo.Cache.CalcTextSize(Text, captionFont, fmt, 0).Width);
                    Int32 captionTextWidthWithButtonsAndSpacing = captionTextWidth + SystemInformation.CaptionButtonSize.Width * 5 / 4;
                    Int32 captionWidth = System.Math.Min(maxCaptionForcedWidth, captionTextWidthWithButtonsAndSpacing);
                    if (wantedFormWidth < captionWidth)
                    {
                        wantedFormWidth = captionWidth;
                    }
                }
                finally
                {
                    ginfo.ReleaseGraphics();
                }

                // Allocate the response buffer to the form following the prompt area
                Int32 ResponseTop = buttonsTop;
                Int32 ResponseWidth = wantedFormWidth - (2 * Spacing);
                const Int32 ResponseHeight = 18;

                ResponseInputBuffer.TabIndex = privateTabIndex;
                privateTabIndex += 1;
                ResponseInputBuffer.Location = new Point(Spacing, ResponseTop);
                ResponseInputBuffer.Size = new Size(ResponseWidth, ResponseHeight);
                ResponseInputBuffer.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
                ResponseInputBuffer.Properties.MaxLength = Message.MaximumLength;
                ResponseInputBuffer.Properties.Buttons[0].Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Undo;

                ResponseInputBuffer.LookAndFeel.Assign(LookAndFeel);
                ResponseInputBuffer.Text = Message.DefaultResponse.Trim();
                Controls.Add(ResponseInputBuffer);

                ResponseInputBuffer.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(ResponseBuffer_ButtonPressed);
                ResponseInputBuffer.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(ResponseBuffer_ParseEditValue);

                // Compute our rectangle for input
                ResponseRectangle = new Rectangle(ResponseInputBuffer.Left, ResponseInputBuffer.Top, ResponseInputBuffer.Width, ResponseInputBuffer.Height);

                // Push the buttons down a bit
                buttonsTop += Spacing + ResponseInputBuffer.Size.Height;

                Width = wantedFormWidth + 2 * SystemInformation.FixedFrameBorderSize.Width;
                Height = buttonsTop + ((DevExpress.XtraEditors.SimpleButton)buttons[0]).Height + Spacing + (2 * SystemInformation.FixedFrameBorderSize.Height) + SystemInformation.CaptionHeight;

                Int32 nextButtonPos = (wantedFormWidth - buttonsTotalWidth) / 2;
                for (Int32 i = 0; i <= buttons.Count - 1; i += 1)
                {
                    DevExpress.XtraEditors.SimpleButton btn = (DevExpress.XtraEditors.SimpleButton)buttons[i];
                    btn.TabIndex = privateTabIndex;
                    privateTabIndex += 1;
                    btn.Location = new Point(nextButtonPos, buttonsTop);
                    nextButtonPos += btn.Width + Spacing;
                }

                if (XtraInputBoxArgs.Icon == null)
                {
                    MessageRectangle.Offset((wantedFormWidth - (MessageRectangle.Right + Spacing)) / 2, 0);
                }
                if (XtraInputBoxArgs.Icon != null && MessageRectangle.Height < IconRectangle.Height)
                {
                    MessageRectangle.Offset(0, (IconRectangle.Height - MessageRectangle.Height) / 2);
                }
            }

            private void ResponseBuffer_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
            {
                ((DevExpress.XtraEditors.ButtonEdit)sender).EditValue = Message.DefaultResponse;
            }

            private void ResponseBuffer_ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
            {
                Message.Response = Convert.ToString(e.Value).Trim();
                e.Value = Message.Response;
                e.Handled = true;
            }

            private void CreateButtons()
            {
                if (Message.Buttons == null || Message.Buttons.Length <= 0)
                {
                    throw new ArgumentException("At least one button must be specified", "buttons");
                }

                // Allocate the space for the array object
                buttons = new ArrayList(Message.Buttons.Length);

                // Populate the button list
                for (Int32 i = 0; i <= Message.Buttons.Length - 1; i += 1)
                {
                    DevExpress.XtraEditors.SimpleButton currentButton = new DevExpress.XtraEditors.SimpleButton();
                    currentButton.LookAndFeel.Assign(LookAndFeel);
                    currentButton.DialogResult = Message.Buttons[i];
                    currentButton.Text = GetButtonText(currentButton.DialogResult);
                    buttons.Add(currentButton);
                    Controls.Add(currentButton);

                    if (currentButton.DialogResult == DialogResult.None)
                    {
                        throw new ArgumentException("The 'DialogResult.None' button cannot be specified", "buttons");
                    }

                    if (currentButton.DialogResult == DialogResult.Cancel)
                    {
                        CancelButton = currentButton;
                    }
                    else
                    {
                        AcceptButton = currentButton;
                    }
                }
            }

            public DialogResult ShowMessageBoxDialog()
            {

                if (Message.LookAndFeel != null)
                {
                    LookAndFeel.Assign(Message.LookAndFeel);
                }

                if (!AllowCustomLookAndFeel)
                {
                    if (LookAndFeel.ActiveStyle != DevExpress.LookAndFeel.ActiveLookAndFeelStyle.Skin)
                    {
                        DevExpress.LookAndFeel.ActiveLookAndFeelStyle active = DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveStyle;
                        if (active == DevExpress.LookAndFeel.ActiveLookAndFeelStyle.Office2003)
                        {
                            LookAndFeel.SetStyle(DevExpress.LookAndFeel.LookAndFeelStyle.Office2003, true, false, string.Empty);
                        }
                        else
                        {
                            LookAndFeel.SetDefaultStyle();
                        }
                    }
                }

                Text = Message.Caption;

                FormBorderStyle = FormBorderStyle.FixedDialog;
                MinimizeBox = false;
                MaximizeBox = false;

                IWin32Window owner = Message.Owner;
                if (owner == null)
                {
                    System.Windows.Forms.Form activeForm = Form.ActiveForm;
                    if (activeForm != null && !activeForm.InvokeRequired)
                    {
                        owner = activeForm;
                    }
                }

                if (owner != null)
                {
                    Control ownerControl = ((Control)owner);
                    if (ownerControl != null)
                    {
                        if (!ownerControl.Visible)
                        {
                            owner = null;
                        }
                        else
                        {
                            System.Windows.Forms.Form ownerForm = ownerControl.FindForm() as System.Windows.Forms.Form;
                            if (ownerForm != null)
                            {
                                if (!ownerForm.Visible || ownerForm.WindowState == FormWindowState.Minimized || ownerForm.Right <= 0 || ownerForm.Bottom <= 0)
                                {
                                    owner = null;
                                }
                            }
                        }
                    }
                }

                if (owner == null)
                {
                    ShowInTaskbar = true;
                    StartPosition = FormStartPosition.CenterScreen;
                }
                else
                {
                    ShowInTaskbar = false;
                    StartPosition = FormStartPosition.CenterParent;
                }

                // Do the layout operations to position the controls.
                CreateButtons();
                CalcIconBounds();
                CalcMessageBounds();
                CalcFinalSizes();

                // Kill the close box if there is a cancel button.
                DisableCloseButtonIfNeeded();

                // Make the form on top of others if required.
                System.Windows.Forms.Form frm = owner as System.Windows.Forms.Form;
                if (frm != null && frm.TopMost)
                {
                    TopMost = true;
                }

                Activate();

                // Conduct the dialog now.
                return DoShowDialog(owner);
            }

            protected DialogResult DoShowDialog(IWin32Window owner)
            {
                DialogResult Result = ShowDialog(owner);
                if (Array.IndexOf(Message.Buttons, Result) < 0)
                {
                    Result = Message.Buttons[0];
                }
                return Result;
            }

            private void DisableCloseButtonIfNeeded()
            {
                // if there is no cancel button then we need to kill the close button.
                if (CancelButton == null)
                {
                    CloseBox = false;
                }
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                using (var gcache = new DevExpress.Utils.Drawing.GraphicsCache(e))
                {
                    if (XtraInputBoxArgs.Icon != null)
                    {
                        gcache.Graphics.DrawIcon(XtraInputBoxArgs.Icon, IconRectangle);
                    }
                    GetPaintAppearance().DrawString(gcache, Message.Text, MessageRectangle);
                }
            }

            protected override void OnClosing(CancelEventArgs e)
            {
                if (CancelButton == null && DialogResult == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                base.OnClosing(e);
            }

            protected override void OnSizeChanged(EventArgs e)
            {
                base.OnSizeChanged(e);
                DisableCloseButtonIfNeeded();
            }

            protected override void OnKeyPress(KeyPressEventArgs e)
            {
                if (e.KeyChar == '\x03')
                {
                    e.Handled = true;
                    Clipboard.SetDataObject(Message.Caption + Environment.NewLine + Environment.NewLine + Message.Text, true);
                }
                else
                {
                    base.OnKeyPress(e);
                }
            }

            DevExpress.Accessibility.BaseAccessible privateDXAccessible;
            protected DevExpress.Accessibility.BaseAccessible DxAccessible
            {
                get
                {
                    if (privateDXAccessible == null)
                    {
                        privateDXAccessible = new InputBoxAccessible(this);
                    }
                    return privateDXAccessible;
                }
                set
                {
                    privateDXAccessible = value;
                }
            }

            protected internal AccessibleObject GetParentAccessible()
            {
                return base.CreateAccessibilityInstance().Parent;
            }

            protected override AccessibleObject CreateAccessibilityInstance()
            {
                if (DxAccessible == null)
                {
                    return base.CreateAccessibilityInstance();
                }
                return DxAccessible.Accessible;
            }
        }

        public class InputBoxAccessible : DevExpress.Accessibility.ContainerBaseAccessible
        {
            public InputBoxAccessible(XtraInputBoxForm form)
                : base(form)
            {
            }

            public InputBoxAccessible(Control control)
                : base(control)
            {
            }

            public new XtraInputBoxForm Control
            {
                get
                {
                    return ((XtraInputBoxForm)base.Control);
                }
            }

            protected override DevExpress.Accessibility.ChildrenInfo GetChildrenInfo()
            {
                DevExpress.Accessibility.ChildrenInfo res = base.GetChildrenInfo();
                if (XtraInputBoxArgs.Icon != null)
                {
                    res["icon"] = 1;
                }
                res[DevExpress.Accessibility.ChildType.Text] = 1;
                return res;
            }

            public new AccessibleObject Parent
            {
                get
                {
                    return Control.GetParentAccessible();
                }
            }

            protected static new AccessibleRole GetRole
            {
                get
                {
                    return AccessibleRole.Dialog;
                }
            }

            protected new string GetName
            {
                get
                {
                    return Control.Message.Caption;
                }
            }

            protected override void OnChildrenCountChanged()
            {
                if (XtraInputBoxArgs.Icon != null)
                {
                    AddChild(new MessageIconAccessible(Control));
                }
                AddChild(new MessageLabelAccessible(Control));
                AddChild(new MessageResponseAccessible(Control));
                base.OnChildrenCountChanged();
            }

            protected class MessageResponseAccessible : DevExpress.Accessibility.BaseAccessible
            {
                private XtraInputBoxForm form;

                public MessageResponseAccessible(XtraInputBoxForm form)
                {
                    this.form = form;
                }

                public override Rectangle ClientBounds
                {
                    get
                    {
                        return form.ResponseRectangle;
                    }
                }

                protected static new AccessibleRole GetRole
                {
                    get
                    {
                        return AccessibleRole.Text;
                    }
                }

                protected new string GetName
                {
                    get
                    {
                        return form.Message.Response;
                    }
                }
            }

            protected class MessageLabelAccessible : DevExpress.Accessibility.BaseAccessible
            {
                private XtraInputBoxForm form;

                public MessageLabelAccessible(XtraInputBoxForm form)
                {
                    this.form = form;
                }

                public override Rectangle ClientBounds
                {
                    get
                    {
                        return form.MessageRectangle;
                    }
                }

                protected static new AccessibleRole GetRole
                {
                    get
                    {
                        return AccessibleRole.StaticText;
                    }
                }

                protected new string GetName
                {
                    get
                    {
                        return form.Message.Text;
                    }
                }
            }

            protected class MessageIconAccessible : DevExpress.Accessibility.BaseAccessible
            {
                private XtraInputBoxForm form;

                public MessageIconAccessible(XtraInputBoxForm form)
                {
                    this.form = form;
                }

                public override Rectangle ClientBounds
                {
                    get
                    {
                        return form.IconRectangle;
                    }
                }

                protected static new AccessibleRole GetRole
                {
                    get
                    {
                        return AccessibleRole.Graphic;
                    }
                }
            }
        }
    }
}
