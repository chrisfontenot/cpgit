#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.Data.Forms
{
    public class MessageBox
    {
        // List of functions with with no lookandfeel and owner values
        public static DialogResult Show(string Text)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(string Text, string Caption)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(string Text, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, Buttons, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, Buttons, Icon, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, Buttons, Icon, defaultButton));
        }

        // list of functions with owner but no lookandfeel
        public static DialogResult Show(IWin32Window Owner, string Text)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Owner, Text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(IWin32Window Owner, string Text, string Caption)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Owner, Text, Caption, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Owner, Text, Caption, Buttons, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Owner, Text, Caption, Buttons, Icon, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Owner, Text, Caption, Buttons, Icon, defaultButton));
        }

        // List of functions with lookandfeel but without an owner window
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, string Text)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, string Text, string Caption)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, string Text, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, Buttons, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, Buttons, Icon, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new XtraMessageBoxArgs(null, Text, Caption, Buttons, Icon, defaultButton));
        }

        // list of functions with lookandfeel and an owner window
        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, IWin32Window Owner, string Text)
        {
            return ConductDialog(new XtraMessageBoxArgs(LookAndFeel, Owner, Text, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, IWin32Window Owner, string Text, string Caption)
        {
            return ConductDialog(new XtraMessageBoxArgs(LookAndFeel, Owner, Text, Caption, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new XtraMessageBoxArgs(LookAndFeel, Owner, Text, Caption, Buttons, MessageBoxIcon.None, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon)
        {
            return ConductDialog(new XtraMessageBoxArgs(LookAndFeel, Owner, Text, Caption, Buttons, Icon, MessageBoxDefaultButton.Button1));
        }

        public static DialogResult Show(DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel, IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new XtraMessageBoxArgs(LookAndFeel, Owner, Text, Caption, Buttons, Icon, defaultButton));
        }

        /// <summary>
        /// Input box routine for DebtPlus. This variation allows the Cancel
        /// button status to be returned to the application.
        /// </summary>
        internal class XtraMessageBoxArgs
        {
            private readonly IWin32Window privateOwner;
            private readonly string privateText;
            private readonly string privateCaption;
            private readonly MessageBoxButtons privateButtons;
            private readonly MessageBoxIcon privateIcon;
            private readonly MessageBoxDefaultButton privateDefaultButton;
            private DevExpress.LookAndFeel.UserLookAndFeel privateUserLookAndFeel;

            public XtraMessageBoxArgs()
                : this(null, null, string.Empty, string.Empty, MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1)
            {
            }

            public XtraMessageBoxArgs(IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton DefaultButton)
                : this(null, Owner, Text, Caption, Buttons, Icon, DefaultButton)
            {
            }

            public XtraMessageBoxArgs(DevExpress.LookAndFeel.UserLookAndFeel UserLookAndFeel, IWin32Window Owner, string Text, string Caption, MessageBoxButtons Buttons, MessageBoxIcon Icon, MessageBoxDefaultButton DefaultButton)
            {
                privateUserLookAndFeel = UserLookAndFeel;
                privateOwner = Owner;
                privateText = Text;
                privateCaption = Caption;
                privateButtons = Buttons;
                privateIcon = Icon;
                privateDefaultButton = DefaultButton;
            }

            public IWin32Window Owner
            {
                get
                {
                    return privateOwner;
                }
            }

            public MessageBoxIcon TypeIcon
            {
                get
                {
                    return privateIcon;
                }
            }

            public Icon Icon
            {
                get
                {
                    switch (TypeIcon)
                    {
                        case MessageBoxIcon.None:
                            return null;

                        case MessageBoxIcon.Error:
                            return SystemIcons.Error;

                        case MessageBoxIcon.Exclamation:
                            return SystemIcons.Exclamation;

                        case MessageBoxIcon.Information:
                            return SystemIcons.Information;

                        case MessageBoxIcon.Question:
                            return SystemIcons.Question;

                        default:
                            throw new InvalidEnumArgumentException("icons", Convert.ToInt32(privateIcon), typeof(DialogResult));
                    }
                }
            }

            public string Caption
            {
                get
                {
                    return privateCaption;
                }
            }

            public string Text
            {
                get
                {
                    return privateText;
                }
            }

            public MessageBoxDefaultButton TypeDefaultButton
            {
                get
                {
                    return privateDefaultButton;
                }
            }

            public Int32 DefaultButtonIndex
            {
                get
                {
                    switch (TypeDefaultButton)
                    {
                        case MessageBoxDefaultButton.Button1:
                            return 0;

                        case MessageBoxDefaultButton.Button2:
                            if (Buttons.Length < 2)
                            {
                                return 0;
                            }
                            return 1;

                        case MessageBoxDefaultButton.Button3:
                            if (Buttons.Length < 3)
                            {
                                return 0;
                            }
                            return 2;

                        default:
                            throw new InvalidEnumArgumentException("defaultButton", Convert.ToInt32(TypeDefaultButton), typeof(DialogResult));
                    }
                }
            }

            public MessageBoxButtons TypeButtons
            {
                get
                {
                    return privateButtons;
                }
            }

            public DialogResult[] Buttons
            {
                get
                {
                    switch (TypeButtons)
                    {
                        case MessageBoxButtons.OK:
                            return new DialogResult[] { DialogResult.OK };

                        case MessageBoxButtons.OKCancel:
                            return new DialogResult[] { DialogResult.OK, DialogResult.Cancel };

                        case MessageBoxButtons.AbortRetryIgnore:
                            return new DialogResult[] { DialogResult.Abort, DialogResult.Retry, DialogResult.Ignore };

                        case MessageBoxButtons.YesNo:
                            return new DialogResult[] { DialogResult.Yes, DialogResult.No };

                        case MessageBoxButtons.YesNoCancel:
                            return new DialogResult[] { DialogResult.Yes, DialogResult.No, DialogResult.Cancel };

                        case MessageBoxButtons.RetryCancel:
                            return new DialogResult[] { DialogResult.Retry, DialogResult.Cancel };

                        default:
                            throw new InvalidEnumArgumentException("buttons", Convert.ToInt32(privateButtons), typeof(DialogResult));
                    }
                }
            }

            public DevExpress.LookAndFeel.UserLookAndFeel LookAndFeel
            {
                get
                {
                    if (privateUserLookAndFeel != null)
                    {
                        return privateUserLookAndFeel;
                    }

                    if (Owner is DevExpress.XtraEditors.XtraForm)
                    {
                        DevExpress.XtraEditors.XtraForm form = ((DevExpress.XtraEditors.XtraForm)Owner);
                        if (form != null)
                        {
                            return form.LookAndFeel;
                        }
                    }
                    return null;
                }

                set
                {
                    privateUserLookAndFeel = value;
                }
            }
        }

        private static DialogResult ConductDialog(XtraMessageBoxArgs Msg)
        {
            DialogResult Answer;

            try
            {
                // Beep the speaker depending upon the icon desired
                switch (Msg.TypeIcon)
                {
                    case MessageBoxIcon.None:
                        // do not beep the speaker
                        break;

                    case MessageBoxIcon.Question:
                        DebtPlus.Data.Forms.DebtPlusForm.DoMessageBeep(DebtPlus.Data.Forms.DebtPlusForm.MessageBeepEnum.Question);
                        break;

                    case MessageBoxIcon.Error:
                        DebtPlus.Data.Forms.DebtPlusForm.DoMessageBeep(DebtPlus.Data.Forms.DebtPlusForm.MessageBeepEnum.Hand);
                        break;

                    case MessageBoxIcon.Exclamation:
                        DebtPlus.Data.Forms.DebtPlusForm.DoMessageBeep(DebtPlus.Data.Forms.DebtPlusForm.MessageBeepEnum.Exclamation);
                        break;

                    case MessageBoxIcon.Information:
                        DebtPlus.Data.Forms.DebtPlusForm.DoMessageBeep(DebtPlus.Data.Forms.DebtPlusForm.MessageBeepEnum.Asterisk);
                        break;

                    default:
                        break;
                }
            }

            catch { }

            // Conduct the form
            using (var frm = new XtraMessageBoxForm(Msg))
            {
                Answer = frm.ShowMessageBoxDialog();
            }
            return Answer;
        }

        public class XtraMessageBoxForm : DebtPlus.Data.Forms.DebtPlusForm
        {
            // Context information for the popup menu
            ContextMenu MenuStrip1 = null;

            private const Int32 Spacing = 12;
            private ArrayList buttons = new ArrayList();
            public Rectangle IconRectangle;
            public Rectangle MessageRectangle;

            public XtraMessageBoxForm()
            {
                KeyPreview = true;
                Message = new XtraMessageBoxArgs();
            }

            internal XtraMessageBoxForm(XtraMessageBoxArgs Message)
            {
                KeyPreview = true;
                this.Message = Message;
            }

            public static bool AllowCustomLookAndFeel { get; set; }

            internal XtraMessageBoxArgs Message { get; set; }

            private void CalcIconBounds()
            {
                if (Message.Icon != null)
                {
                    IconRectangle = new Rectangle(Spacing, Spacing, Message.Icon.Height, Message.Icon.Width);
                }
                else
                {
                    IconRectangle = new Rectangle(Spacing, Spacing, 0, 0);
                }
            }

            private DevExpress.Utils.AppearanceObject GetPaintAppearance()
            {
                DevExpress.Utils.AppearanceObject paintAppearance = new DevExpress.Utils.AppearanceObject(Appearance, DefaultAppearance);
                paintAppearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
                paintAppearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
                paintAppearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
                return paintAppearance;
            }

            protected override void OnVisibleChanged(EventArgs e)
            {
                base.OnVisibleChanged(e);
                if (Visible && !ContainsFocus)
                {
                    Activate();
                }
            }

            private void CalcMessageBounds()
            {
                Int32 messageLeft, messageWidth, messageHeight;
                Int32 messageTop = Spacing;
                messageLeft = Convert.ToInt32(((Message.Icon == null) ? Spacing : IconRectangle.Left + IconRectangle.Width + Spacing));

                Int32 maxFormWidth = MaximumSize.Width;
                if (maxFormWidth <= 0)
                {
                    maxFormWidth = SystemInformation.WorkingArea.Width;
                }

                Int32 maxTextWidth = maxFormWidth - Spacing - messageLeft;
                if ((maxTextWidth < 10))
                {
                    maxTextWidth = 10;
                }

                SizeF textSize;
                DevExpress.Utils.Drawing.GraphicsInfo ginfo = new DevExpress.Utils.Drawing.GraphicsInfo();
                ginfo.AddGraphics(null);
                textSize = GetPaintAppearance().CalcTextSize(ginfo.Graphics, Message.Text, maxTextWidth);
                ginfo.ReleaseGraphics();

                messageWidth = Convert.ToInt32(System.Math.Ceiling(textSize.Width));
                Int32 maxFormHeight = MaximumSize.Height;
                if (maxFormHeight <= 0)
                {
                    maxFormHeight = SystemInformation.WorkingArea.Height;
                }

                // System.Int32 maxTextHeight = maxFormHeight - Spacing - ((DevExpress.XtraEditors.SimpleButton) buttons[0]).Height - Spacing - Text_Input.Height - Spacing - messageTop - SystemInformation.CaptionHeight
                Int32 maxTextHeight = maxFormHeight - Spacing - ((DevExpress.XtraEditors.SimpleButton)buttons[0]).Height - Spacing - messageTop - SystemInformation.CaptionHeight;
                if (maxTextHeight < 10)
                {
                    maxTextHeight = 10;
                }

                messageHeight = Convert.ToInt32(System.Math.Ceiling(textSize.Height));
                if (messageHeight > maxTextHeight)
                {
                    messageHeight = maxTextHeight;
                }

                MessageRectangle = new Rectangle(messageLeft, messageTop, messageWidth, messageHeight);
            }

            protected static string GetButtonText(DialogResult Target)
            {
                switch (Target)
                {
                    case DialogResult.Abort:
                        return "&Abort";

                    case DialogResult.Cancel:
                        return "&Cancel";

                    case DialogResult.Ignore:
                        return "&Ignore";

                    case DialogResult.No:
                        return "&No";

                    case DialogResult.OK:
                        return "&Ok";

                    case DialogResult.Retry:
                        return "&Retry";

                    case DialogResult.Yes:
                        return "&Yes";

                    case DialogResult.None:
                        return string.Empty;

                    default:
                        return "&" + Target;
                }
            }

            private void CalcFinalSizes()
            {
                Int32 buttonsTotalWidth = 0;
                foreach (DevExpress.XtraEditors.SimpleButton button in buttons)
                {
                    if (buttonsTotalWidth != 0)
                    {
                        buttonsTotalWidth += Spacing;
                    }
                    buttonsTotalWidth += button.Width;
                }

                Int32 buttonsTop = MessageRectangle.Bottom + Spacing;
                if (Message.Icon != null && IconRectangle.Bottom + Spacing > buttonsTop)
                {
                    buttonsTop = IconRectangle.Bottom + Spacing;
                }

                Int32 wantedFormWidth = MinimumSize.Width;

                if (wantedFormWidth == 0)
                {
                    wantedFormWidth = SystemInformation.MinimumWindowSize.Width;
                }

                if (wantedFormWidth < MessageRectangle.Right + Spacing)
                {
                    wantedFormWidth = MessageRectangle.Right + Spacing;
                }

                if (wantedFormWidth < buttonsTotalWidth + Spacing + Spacing)
                {
                    wantedFormWidth = buttonsTotalWidth + Spacing + Spacing;
                }

                DevExpress.Utils.Drawing.GraphicsInfo ginfo = new DevExpress.Utils.Drawing.GraphicsInfo();
                try
                {
                    ginfo.AddGraphics(null);
                    StringFormat fmt = ((StringFormat)DevExpress.Utils.TextOptions.DefaultStringFormat.Clone());
                    fmt.FormatFlags = fmt.FormatFlags | StringFormatFlags.MeasureTrailingSpaces;
                    Font captionFont = DevExpress.Utils.ControlUtils.GetCaptionFont();
                    Int32 maxCaptionForcedWidth = (SystemInformation.WorkingArea.Width * 2) / 3;
                    Int32 captionTextWidth = 1 + 4 + Convert.ToInt32(ginfo.Cache.CalcTextSize(Text, captionFont, fmt, 0).Width);
                    Int32 captionTextWidthWithButtonsAndSpacing = captionTextWidth + (SystemInformation.CaptionButtonSize.Width * 5) / 4;
                    Int32 captionWidth = System.Math.Min(maxCaptionForcedWidth, captionTextWidthWithButtonsAndSpacing);
                    if (wantedFormWidth < captionWidth)
                    {
                        wantedFormWidth = captionWidth;
                    }

                }
                finally
                {
                    ginfo.ReleaseGraphics();
                }

                Width = wantedFormWidth + 2 * SystemInformation.FixedFrameBorderSize.Width;
                Height = buttonsTop + ((DevExpress.XtraEditors.SimpleButton)buttons[0]).Height + Spacing + 2 * SystemInformation.FixedFrameBorderSize.Height + SystemInformation.CaptionHeight;

                Int32 nextButtonPos = (wantedFormWidth - buttonsTotalWidth) / 2;
                for (Int32 i = 0; i < buttons.Count; i += 1)
                {
                    DevExpress.XtraEditors.SimpleButton btn = ((DevExpress.XtraEditors.SimpleButton)buttons[i]);
                    btn.Location = new Point(nextButtonPos, buttonsTop);
                    nextButtonPos += btn.Width + Spacing;
                }

                if (Message.Icon == null)
                {
                    MessageRectangle.Offset((wantedFormWidth - (MessageRectangle.Right + Spacing)) / 2, 0);
                }

                if (Message.Icon != null && MessageRectangle.Height < IconRectangle.Height)
                {
                    MessageRectangle.Offset(0, (IconRectangle.Height - MessageRectangle.Height) / 2);
                }
            }

            private void CreateButtons()
            {
                if (Message.Buttons == null || Message.Buttons.Length <= 0)
                {
                    throw new ArgumentException("At least one button must be specified", "buttons");
                }

                // Allocate the space for the array object
                buttons = new ArrayList(Message.Buttons.Length);

                // Populate the button list
                for (Int32 i = 0; i < Message.Buttons.Length; ++i)
                {
                    DevExpress.XtraEditors.SimpleButton currentButton = new DevExpress.XtraEditors.SimpleButton();
                    currentButton.LookAndFeel.Assign(LookAndFeel);
                    currentButton.DialogResult = Message.Buttons[i];
                    currentButton.Text = GetButtonText(currentButton.DialogResult);
                    buttons.Add(currentButton);
                    Controls.Add(currentButton);

                    if (currentButton.DialogResult == DialogResult.None)
                    {
                        throw new ArgumentException("The 'DialogResult.None' button cannot be specified", "buttons");
                    }

                    if (currentButton.DialogResult == DialogResult.Cancel)
                    {
                        CancelButton = currentButton;
                    }
                }

                // if there is only one button then it is, by definition, CANCEL.
                if (buttons.Count == 1)
                {
                    CancelButton = ((DevExpress.XtraEditors.SimpleButton)buttons[0]);
                }

                // Select the default button.
                ((DevExpress.XtraEditors.SimpleButton)buttons[Message.DefaultButtonIndex]).Select();
            }

            public DialogResult ShowMessageBoxDialog()
            {
                MenuStrip1 = null;
                try
                {
                    if (Message.LookAndFeel != null)
                    {
                        LookAndFeel.Assign(Message.LookAndFeel);
                    }

                    if (!AllowCustomLookAndFeel)
                    {
                        if (LookAndFeel.ActiveStyle != DevExpress.LookAndFeel.ActiveLookAndFeelStyle.Skin)
                        {
                            DevExpress.LookAndFeel.ActiveLookAndFeelStyle active = DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveStyle;
                            if (active == DevExpress.LookAndFeel.ActiveLookAndFeelStyle.Office2003)
                            {
                                LookAndFeel.SetStyle(DevExpress.LookAndFeel.LookAndFeelStyle.Office2003, true, false, string.Empty);
                            }
                            else
                            {
                                LookAndFeel.SetDefaultStyle();
                            }
                        }
                    }

                    Text = Message.Caption;

                    FormBorderStyle = FormBorderStyle.FixedDialog;
                    MinimizeBox = false;
                    MaximizeBox = false;

                    IWin32Window owner = Message.Owner;
                    if (owner == null)
                    {
                        System.Windows.Forms.Form activeForm = Form.ActiveForm;
                        if (activeForm != null && !activeForm.InvokeRequired)
                        {
                            owner = activeForm;
                        }
                    }

                    if (owner != null)
                    {
                        Control ownerControl = ((Control)owner);
                        if (ownerControl != null)
                        {
                            if (!ownerControl.Visible)
                            {
                                owner = null;
                            }
                            else
                            {
                                System.Windows.Forms.Form ownerForm = ownerControl.FindForm() as System.Windows.Forms.Form;
                                if (ownerForm != null)
                                {
                                    if (!ownerForm.Visible || ownerForm.WindowState == FormWindowState.Minimized || ownerForm.Right <= 0 || ownerForm.Bottom <= 0)
                                    {
                                        owner = null;
                                    }
                                }
                            }
                        }
                    }

                    if (owner == null)
                    {
                        ShowInTaskbar = true;
                        StartPosition = FormStartPosition.CenterScreen;
                    }
                    else
                    {
                        ShowInTaskbar = false;
                        StartPosition = FormStartPosition.CenterParent;
                    }

                    // Do the layout operations to position the controls.
                    CreateButtons();
                    CalcIconBounds();
                    CalcMessageBounds();
                    CalcFinalSizes();

                    // Kill the close box if there is a cancel button.
                    DisableCloseButtonIfNeeded();

                    // Make the form on top of others if required.
                    Form frm = (Form)owner;
                    if (frm != null && frm.TopMost)
                    {
                        TopMost = true;
                    }

                    // Conduct the dialog now.
                    return DoShowDialog(owner);
                }
                finally
                {
                    if (MenuStrip1 != null)
                    {
                        MenuStrip1.Dispose();
                    }
                }

            }

            protected DialogResult DoShowDialog(IWin32Window owner)
            {
                DialogResult Result = ShowDialog(owner);
                if (Array.IndexOf(Message.Buttons, Result) < 0)
                {
                    Result = Message.Buttons[0];
                }
                return Result;
            }

            private void DisableCloseButtonIfNeeded()
            {
                // if there is no cancel button then we need to kill the close button.
                if (CancelButton == null)
                {
                    CloseBox = false;
                }
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                base.OnPaint(e);
                using (var gcache = new DevExpress.Utils.Drawing.GraphicsCache(e))
                {
                    if (Message.Icon != null)
                    {
                        gcache.Graphics.DrawIcon(Message.Icon, IconRectangle);
                    }
                    GetPaintAppearance().DrawString(gcache, Message.Text, MessageRectangle);
                }
            }

            protected override void OnClosing(CancelEventArgs e)
            {
                if (CancelButton == null && DialogResult == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                base.OnClosing(e);
            }

            protected override void OnSizeChanged(EventArgs e)
            {
                base.OnSizeChanged(e);
                DisableCloseButtonIfNeeded();
            }

            protected override void OnKeyPress(KeyPressEventArgs e)
            {
                if (e.KeyChar == '\x03' /* Ctrl-C */ )
                {
                    e.Handled = true;
                    Clipboard.SetDataObject(Message.Caption + Environment.NewLine + Environment.NewLine + Message.Text, true);
                }
                else
                {
                    base.OnKeyPress(e);
                }
            }

            DevExpress.Accessibility.BaseAccessible privateDXAccessible;
            protected DevExpress.Accessibility.BaseAccessible DxAccessible
            {
                get
                {
                    if (privateDXAccessible == null)
                    {
                        privateDXAccessible = new MessageBoxAccessible(this);
                    }
                    return privateDXAccessible;
                }
                set
                {
                    privateDXAccessible = value;
                }
            }

            protected internal AccessibleObject GetParentAccessible()
            {
                return base.CreateAccessibilityInstance().Parent;
            }

            protected override AccessibleObject CreateAccessibilityInstance()
            {
                if (DxAccessible == null)
                {
                    return base.CreateAccessibilityInstance();
                }
                return DxAccessible.Accessible;
            }

            protected override void OnMouseDown(MouseEventArgs e)
            {
                base.OnMouseDown(e);
                if (e.Button == MouseButtons.Right && MessageRectangle.Contains(e.Location))
                {
                    if (MenuStrip1 == null)
                    {
                        MenuStrip1 = new ContextMenu();
                        var Item = new MenuItem() { Text = "&Copy Text", Shortcut = Shortcut.CtrlC, ShowShortcut = true };
                        Item.Click += ClipboardCopy;
                        MenuStrip1.MenuItems.Add(Item);
                    }
                    MenuStrip1.Show(this, e.Location);
                }
            }

            private void ClipboardCopy(object sender, EventArgs e)
            {
                Clipboard.Clear();
                Clipboard.SetText(Message.Text, TextDataFormat.Text);
            }
        }

        public class MessageBoxAccessible : DevExpress.Accessibility.ContainerBaseAccessible
        {
            public MessageBoxAccessible(XtraMessageBoxForm form)
                : base(form)
            {
            }

            public MessageBoxAccessible(Control control)
                : base(control)
            {
            }

            public new XtraMessageBoxForm Control
            {
                get
                {
                    return ((XtraMessageBoxForm)base.Control);
                }
            }

            protected override DevExpress.Accessibility.ChildrenInfo GetChildrenInfo()
            {
                DevExpress.Accessibility.ChildrenInfo res = base.GetChildrenInfo();
                if (Control.Message.Icon != null)
                {
                    res["icon"] = 1;
                }
                res[DevExpress.Accessibility.ChildType.Text] = 1;
                return res;
            }

            public override AccessibleObject Parent
            {
                get
                {
                    return Control.GetParentAccessible();
                }
            }

            protected new static AccessibleRole GetRole
            {
                get
                {
                    return AccessibleRole.Dialog;
                }
            }

            protected new string GetName
            {
                get
                {
                    return Control.Message.Caption;
                }
            }

            protected override void OnChildrenCountChanged()
            {
                if (Control.Message.Icon != null)
                {
                    AddChild(new MessageIconAccessible(Control));
                }
                AddChild(new MessageLabelAccessible(Control));
                base.OnChildrenCountChanged();
            }

            protected class MessageLabelAccessible : DevExpress.Accessibility.BaseAccessible
            {
                private XtraMessageBoxForm form;

                public MessageLabelAccessible(XtraMessageBoxForm form)
                {
                    this.form = form;
                }

                public override Rectangle ClientBounds
                {
                    get
                    {
                        return form.MessageRectangle;
                    }
                }

                protected static new AccessibleRole GetRole
                {
                    get
                    {
                        return AccessibleRole.StaticText;
                    }
                }

                protected new string GetName
                {
                    get
                    {
                        return form.Message.Text;
                    }
                }
            }

            protected class MessageIconAccessible : DevExpress.Accessibility.BaseAccessible
            {
                private XtraMessageBoxForm form;

                public MessageIconAccessible(XtraMessageBoxForm form)
                {
                    this.form = form;
                }

                public override Rectangle ClientBounds
                {
                    get
                    {
                        return form.IconRectangle;
                    }
                }

                protected static new AccessibleRole GetRole
                {
                    get
                    {
                        return AccessibleRole.Graphic;
                    }
                }
            }
        }
    }
}
