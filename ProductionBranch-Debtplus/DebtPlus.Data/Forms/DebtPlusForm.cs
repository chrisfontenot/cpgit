#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DebtPlus.Data.Forms
{
    public partial class DebtPlusForm : DevExpress.XtraEditors.XtraForm, DebtPlus.Interfaces.IForm
    {
        /// <summary>
        /// True if the environment is really in the DesignMode. FALSE if it is really in the runtime mode.
        /// We can't really use "DesignMode" because that only works on the top-most form in the entire application.
        /// We want something that works on controls and inherited forms, ergo this routine.
        /// </summary>
        public bool InDesignMode
        {
            get
            {
                System.Windows.Forms.Control ctrl = this;
                while (ctrl != null)
                {
                    if ((ctrl.Site != null) && ctrl.Site.DesignMode)
                    {
                        return true;
                    }
                    ctrl = ctrl.Parent;
                }
                return false;
            }
        }

        /// <summary>
        /// Get or set the default skin name for the look and feel properties
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks>if there is no default, the value returned is "DevExpress Style"</remarks>
        [Obsolete("Use DebtPlus.Configuration.Config.SkinName()")]
        protected static string SkinName
        {
            get
            {
                return DebtPlus.Configuration.Config.SkinName;
            }

            set
            {
                DebtPlus.Configuration.Config.SkinName = value;
            }
        }

        /// <summary>
        /// Create an instance of our form class
        /// </summary>
        /// <param name="DatabaseInfo">Pointer to the database connection object</param>
        /// <remarks></remarks>
        public DebtPlusForm() : base()
        {
            // Register the skins. Do this if there are too few skins
            // "too few" is only a guess. There is no indicator that this was done before. :(
            if (DevExpress.Skins.SkinManager.Default.Skins.Count < 10)
            {
                DevExpress.UserSkins.BonusSkins.Register();
            }

            // Do the standard initialization once the Skins are defined.
            InitializeComponent();
            RegisterHandlers();

            // Force compatibility with terminal servers
            DevExpress.XtraEditors.Popup.PopupBaseForm.ForceRemotingCompatibilityMode = true;
        }

        /// <summary>
        /// Add the form events to the system
        /// </summary>
        private void RegisterHandlers()
        {
            Load += Form_Load;
        }

        /// <summary>
        /// Remove the event registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= Form_Load;
        }

        /// <summary>
        /// Handle the form LOAD event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void Form_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Ensure that the lookandfeel was not accidentally overridden by my design work.
                DefaultLookAndFeel1.LookAndFeel.SkinName = DebtPlus.Configuration.Config.SkinName;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Should the tool-tip controller be enabled?
        /// 
        /// This may be overridden on a form by form basis for testing or b items.
        /// </summary>
        [System.ComponentModel.Description("Should the tool tips be enabled for the form?"), Browsable(false)]
        protected virtual bool IsToolTipEnabled()
        {
            bool result = true;
            RegistryKey RegKey = null;

            try
            {
                RegKey = Registry.CurrentUser.OpenSubKey(AssemblyInfo.InstallationRegistryKey + @"\Options", false);
                if (RegKey != null)
                {
                    result = Convert.ToBoolean(RegKey.GetValue("Enable Tool Tips", result.ToString()));
                }

            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }

            finally
            {
                if (RegKey != null)
                {
                    RegKey.Close();
                }
            }

            return result;
        }

        /// <summary>
        /// Screen resolution as a string
        /// </summary>
        /// <returns></returns>
        [System.ComponentModel.Description("A string of the screen resolution"), Browsable(false)]
        private static string ScreenResolution()
        {
            return string.Format("{0:f0}x{1:f0}", SystemInformation.WorkingArea.Width, SystemInformation.WorkingArea.Height);
        }

        private string savedNameString = string.Empty;
        private string savedVersionString = string.Empty;

        /// <summary>
        /// Load the placement using the saved name
        /// </summary>
        [System.ComponentModel.Description("Restore the placement of the form to the previous location"), Browsable(false)]
        protected virtual void LoadPlacement()
        {
            LoadPlacement(Name);
        }

        /// <summary>
        /// Define the name for the load/save and load the placement
        /// </summary>
        /// <param name="NameString"></param>
        [System.ComponentModel.Description("Restore the placement of the form to the previous location"), Browsable(false)]
        protected virtual void LoadPlacement(string NameString)
        {
            LoadPlacement(NameString, "1");
        }

        /// <summary>
        /// Restore the placement and size of the form from the registry setting
        /// </summary>
        /// <param name="NameString"></param>
        /// <param name="VersionString"></param>
        /// <remarks></remarks>
        [System.ComponentModel.Description("Restore the placement of the form to the previous location"), Browsable(false)]
        protected virtual void LoadPlacement(string NameString, string VersionString)
        {
            if (NameString != string.Empty)
            {
                savedNameString = NameString;
                savedVersionString = VersionString;

                try
                {
                    string SavePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Forms");
                    string SaveFile = string.Format("{0}_{1}.xml", NameString, ScreenResolution());

                    string Fname = System.IO.Path.Combine(SavePath, SaveFile);
                    if (System.IO.File.Exists(Fname))
                    {
                        using (System.IO.FileStream fs = System.IO.File.OpenRead(Fname))
                        {
                            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FormLocation), string.Empty);
                            FormLocation clx = (FormLocation)ser.Deserialize(fs);

                            // If the versions match then load the location and size of the form from the
                            // saved values.
                            if (clx.Version == VersionString)
                            {
                                Location = new Point(clx.X, clx.Y);
                                Size = new Size(clx.W, clx.H);
                                this.Select();
                            }
                            fs.Close();
                            clx.Dispose();
                        }
                    }
                }

                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Save the window position
        /// </summary>
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            base.OnFormClosing(e);
            SavePlacement();
        }

        /// <summary>
        /// Save the form placement into the registry
        /// </summary>
        /// <remarks></remarks>
        [System.ComponentModel.Description("Save the form location for the next invocation of the program"), Browsable(false)]
        protected virtual void SavePlacement()
        {
            if (savedNameString != string.Empty && savedVersionString != string.Empty)
            {
                SavePlacement(savedNameString, savedVersionString);
            }
        }

        /// <summary>
        /// Save the form placement into the registry
        /// </summary>
        /// <param name="NameString"></param>
        /// <remarks></remarks>
        [System.ComponentModel.Description("Save the form location for the next invocation of the program"), Browsable(false)]
        protected virtual void SavePlacement(string NameString)
        {
            if (savedNameString != string.Empty)
            {
                if (savedVersionString != string.Empty)
                {
                    SavePlacement(NameString, savedVersionString);
                }
                else
                {
                    SavePlacement(NameString, "1");
                }
            }
        }

        /// <summary>
        /// Save the form placement into the registry
        /// </summary>
        /// <param name="NameString">Name of the form in the system. This should be unique for all forms.</param>
        /// <param name="VersionString">Version of this form. The version is written to the XML file and used when the form is loaded. It must match the load sequence.</param>
        /// <remarks></remarks>
        [System.ComponentModel.Description("Save the form location for the next invocation of the program"), Browsable(false)]
        protected virtual void SavePlacement(string NameString, string VersionString)
        {
            // Do this only if the window is in the "normal" state.
            if (WindowState == FormWindowState.Normal)
            {
                try
                {
                    using (var clx = new FormLocation(this))
                    {
                        clx.Version = VersionString;
                        string SavePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Forms");

                        string SaveFile = string.Format("{0}_{1}.xml", NameString, ScreenResolution());
                        string Fname = System.IO.Path.Combine(SavePath, SaveFile);

                        // Create the directory if needed
                        if (!System.IO.Directory.Exists(SavePath))
                        {
                            System.IO.Directory.CreateDirectory(SavePath);
                        }

                        // Open the output file with create if needed
                        using (System.IO.FileStream fs = System.IO.File.Open(Fname, System.IO.FileMode.OpenOrCreate))
                        {
                            fs.SetLength(0);    // Toss any existing data so that there is no junk left in the file from previous save operations.
                            try
                            {
                                System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(FormLocation), string.Empty);
                                ser.Serialize(fs, clx);
                                fs.Flush();
                            }
                            finally
                            {
                                fs.Close(); // Ensure that the file is closed before it is disposed! The data needs to be written first, fully and completely.
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }
            }
        }

        /// <summary>
        /// Process the load request for the form
        /// </summary>
        /// <param name="e"></param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ToolTipController1.Active = IsToolTipEnabled();
        }

        /// <summary>
        /// return the current error provider for the form
        /// </summary>
        [Browsable(false)]
        public DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider GetErrorProvider // Implements IForm.GetErrorProvider
        {
            get
            {
                return DxErrorProvider1;
            }
        }

        /// <summary>
        /// return the tooltip controller for the form
        /// </summary>
        [Browsable(false)]
        public DevExpress.Utils.ToolTipController GetToolTipController // Implements IForm.GetToolTipController
        {
            get
            {
                return ToolTipController1;
            }
        }
    }
}
