#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Diagnostics;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DebtPlus.Data.Forms
{
    public sealed partial class AboutBoxForm : DebtPlusForm
    {
        public AboutBoxForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += AboutBoxForm_Load;
            Button_SystemInfo.Click += Button_SystemInfo_Click;
            LinkLabel1.LinkClicked += LinkLabel1_LinkClicked;
            Line.Paint += Line_Paint;
        }

        private void UnRegisterHandlers()
        {
            Load -= AboutBoxForm_Load;
            Button_SystemInfo.Click -= Button_SystemInfo_Click;
            LinkLabel1.LinkClicked -= LinkLabel1_LinkClicked;
            Line.Paint -= Line_Paint;
        }

        private void Button_SystemInfo_Click(object sender, EventArgs e) // Handles Button_SystemInfo.Click
        {
            RegistryKey aKey;
            string pVal;

            //  Start with the current User field
            aKey = Registry.LocalMachine;
            try
            {
                //  This call goes to the Catch block if the registry key is not set.
                aKey = aKey.OpenSubKey(@"SOFTWARE\Microsoft\Shared Tools\MSINFO", false);
                if (aKey != null)
                {
                    object oValue = aKey.GetValue("PATH", string.Empty);
                    pVal = Convert.ToString(oValue);

                    // Try to run the program. Catch an error
                    if (pVal != string.Empty)
                    {
                        using (var pgm = new System.Diagnostics.Process())
                        {
                            pgm.StartInfo.FileName = pVal;
                            pgm.StartInfo.Verb = "Open";
                            pgm.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                            pgm.Start();
                        }
                        return;
                    }
                }
            }

            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }

            finally
            {
                if (aKey != null)
                {
                    aKey.Close();
                }
            }

            // if the path is not defined then try the next location
            aKey = Registry.LocalMachine;
            try
            {
                //  This call goes to the Catch block if the registry key is not set.
                aKey = aKey.OpenSubKey(@"SOFTWARE\Microsoft\Shared Tools Location");
                if (aKey != null)
                {
                    object oValue = aKey.GetValue("MSINFO", string.Empty);
                    pVal = Convert.ToString(oValue);

                    using (var pgm = new System.Diagnostics.Process())
                    {
                        pgm.StartInfo.FileName = pVal + @"\MSINFO32.EXE";
                        pgm.StartInfo.Verb = "Open";
                        pgm.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                        pgm.Start();
                    }
                }
            }

            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }

            finally
            {
                if (aKey != null)
                {
                    aKey.Close();
                }
            }

            // Tell the user that the information is not valid.
            DebtPlus.Data.Forms.MessageBox.Show("System Information Is Unavailable At This Time", "System Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
        }

        private void LinkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e) // Handles LinkLabel1.LinkClicked
        {
            System.Diagnostics.Process.Start(LinkLabel1.Text);
        }

        private void AboutBoxForm_Load(object sender, EventArgs e) // Handles base.Load
        {
            string NameString = string.Empty;
            string CompanyString = string.Empty;
            string ModuleFileName = string.Empty;

            UnRegisterHandlers();
            try
            {
                // Fetch the registered owner from the registry
                RegistryKey RegKey = Registry.LocalMachine;
                try
                {
                    RegKey = RegKey.OpenSubKey(AssemblyInfo.InstallationRegistryKey + @"\Installation", false);
                    if (RegKey != null)
                    {
                        NameString = Convert.ToString(RegKey.GetValue("name", string.Empty));
                        CompanyString = Convert.ToString(RegKey.GetValue("company", string.Empty));
                    }
                }

                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }

                finally
                {
                    if (RegKey != null)
                    {
                        RegKey.Close();
                    }
                }

                if (NameString != string.Empty && CompanyString != string.Empty)
                {
                    Registered.Text = NameString + Environment.NewLine + CompanyString;
                }
                else
                {
                    NameString = NameString + CompanyString;
                    if (NameString != string.Empty)
                    {
                        Registered.Text = NameString;
                    }
                    else
                    {
                        Registered.Text = "UNREGISTERED COPY";
                    }
                }

                // Include the version information
                Version.Text = String.Format("Version: {0}{1}", DebtPlus.Configuration.AssemblyInfo.Version, BuildDate());
                lbl_copyright.Text = CopyrightString();
            }

            finally
            {
                RegisterHandlers();
            }
        }

        private String BuildDate()
        {
            System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();

            // Try to use DebtPlus.Desktop.exe as the name for the date information since it is the value
            // that changes the most often when a new build is generated as it requires the remainder of the
            // package as a reference.
            String l = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(a.Location), "DebtPlus.Desktop.exe");

            // If the file is not found then use the current module's value.
            if( ! System.IO.File.Exists(l) )
            {
                l = a.Location;

                // If this is still not found then just return a minimum value for the date.
                if (!System.IO.File.Exists(l))
                {
                    return string.Empty;
                }
            }

            return String.Format(" Built: {0:d}", System.IO.File.GetCreationTime(l));
        }

        private static string CopyrightString()
        {
            string Answer = "Copyright 2000-2008 DebtPlus, L.L.C.";

            System.Reflection.Assembly _asm = System.Reflection.Assembly.GetExecutingAssembly();
            object[] attributes = _asm.GetCustomAttributes(typeof(System.Reflection.AssemblyCopyrightAttribute), false);
            foreach (Attribute attribute in attributes)
            {
                if (attribute is System.Reflection.AssemblyCopyrightAttribute)
                {
                    Answer = ((System.Reflection.AssemblyCopyrightAttribute)attribute).Copyright;
                    break;
                }
            }

            return Answer;
        }

        private void Line_Paint(object sender, System.Windows.Forms.PaintEventArgs e) // Handles Line.Paint
        {
            System.Drawing.Graphics g = e.Graphics;
            using (var p = new System.Drawing.Pen(System.Drawing.Color.Black, 3))
            {
                g.FillRectangle(System.Drawing.Brushes.Transparent, Line.ClientRectangle);
                g.DrawLine(p, 0, 0, Line.Width, 0);
            }
        }
    }
}
