﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.Data.Controls
{
    public partial class EmailControl
    {
        public const string cREFUSED = "DECLINED";
        public const string cNONE = "NONE";
        public const string cMISSING = "NOT SPECIFIED";

        /// <summary>
        /// List of items that are stored in the database "validation" field for the various settings.
        /// Internally, "valid" and "invalid" are the same thing. It is only when the email is validated
        /// and proven to be invalid that we change the type to invalid.
        /// </summary>
        /// <remarks></remarks>
        public enum EmailValidationEnum
        {
            Missing = 0,
            Valid = 1,
            Invalid = 2,
            Refused = 3,
            None = 4
        }
    }
}
