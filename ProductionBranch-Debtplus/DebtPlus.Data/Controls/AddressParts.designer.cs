﻿namespace DebtPlus.Data.Controls
{
    /// <summary>
    /// Control to display the address information form
    /// </summary>
    public partial class AddressParts
    {
        /// <summary>
        /// Form overrides dispose to clean up the component list.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            UnRegisterHandlers();
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.Line2 = new DevExpress.XtraEditors.TextEdit();
            this.Line1 = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Line2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Line1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Line2
            // 
            this.Line2.Location = new System.Drawing.Point(0, 24);
            this.Line2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Line2.Name = "Line2";
            this.Line2.Properties.MaxLength = 80;
            this.Line2.Size = new System.Drawing.Size(480, 20);
            this.Line2.TabIndex = 4;
            this.Line2.ToolTip = "If the address requires two lines, enter the second line of the address here.";
            // 
            // Line1
            // 
            this.Line1.Location = new System.Drawing.Point(0, 0);
            this.Line1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Line1.Name = "Line1";
            this.Line1.Properties.MaxLength = 80;
            this.Line1.Size = new System.Drawing.Size(480, 20);
            this.Line1.TabIndex = 3;
            this.Line1.ToolTip = "This is the first line of the address. It is the main address line.";
            // 
            // AddressParts
            // 
            this.Controls.Add(this.Line2);
            this.Controls.Add(this.Line1);
            this.Name = "AddressParts";
            this.Size = new System.Drawing.Size(480, 72);
            ((System.ComponentModel.ISupportInitialize)(this.Line2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Line1.Properties)).EndInit();
            this.ResumeLayout(false);
        }

        public DevExpress.XtraEditors.TextEdit Line1;
        public DevExpress.XtraEditors.TextEdit Line2;
    }
}