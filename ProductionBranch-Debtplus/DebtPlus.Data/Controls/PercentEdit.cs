#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;

namespace DebtPlus.Data.Controls
{
    /// <summary>
    /// This is the percentage edit control.
    /// </summary>
    public class PercentEdit : DevExpress.XtraEditors.CalcEdit
    {
        //The static constructor which calls the registration method
        static PercentEdit()
        {
            RepositoryItemPercentEdit.RegisterPercentEdit();
        }

        public PercentEdit()
            : base()
        {
        }

        //return the unique name
        public override string EditorTypeName
        {
            get
            {
                return RepositoryItemPercentEdit.PercentEditName;
            }
        }

        //Override the Properties property
        //Simply type-cast the object to the Percent repository item type
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public new RepositoryItemPercentEdit Properties
        {
            get
            {
                return (RepositoryItemPercentEdit)(object)base.Properties;
            }
        }

        /// <summary>
        /// Numerical value of the control. This is the same as the EditValue, but is
        /// converted to a usable decimal number.
        /// </summary>
        public override decimal Value
        {
            get
            {
                decimal dbl = 0M;
                if (EditValue != DBNull.Value && EditValue != null)
                {
                    try
                    {
                        dbl = Convert.ToDecimal(EditValue);
                    }
                    catch { }
                }
                return dbl;
            }
            set
            {
                EditValue = value;
            }
        }

        /// <summary>
        /// Edit value for the control
        /// </summary>
        public override object EditValue
        {
            get
            {
                return base.EditValue;
            }
            set
            {
                // Null values are just passed through.
                if (value != DBNull.Value && value != null)
                {
                    try
                    {
                        // If the value is negative, make it zero. Negative values are not acceptable.
                        double dblValue = Convert.ToDouble(value);
                        if (dblValue < 0.0D)
                        {
                            dblValue = 0.0D;
                        }

                        // Reduce items that are over 100% to a smaller value.
                        while (dblValue > 1.0D)
                        {
                            dblValue /= 100.0D;
                        }

                        // If we don't allow 100% and it is, use 1% for this setting.
                        if (!Properties.Allow100Percent && dblValue == 1.0D)
                        {
                            dblValue = 0.01D;
                        }

                        base.EditValue = dblValue;
                        return;
                    }
                    catch { }
                }
                base.EditValue = value;
            }
        }
    }

    public class PercentEditViewInfo : DevExpress.XtraEditors.ViewInfo.CalcEditViewInfo
    {
        public PercentEditViewInfo(DevExpress.XtraEditors.Repository.RepositoryItem Item)
            : base(Item)
        {
        }

        /// <summary>
        /// Edit value for the control.
        /// </summary>
        public override object EditValue
        {
            get
            {
                return base.EditValue;
            }
            set
            {
                // Null values are just passed through.
                if (value != DBNull.Value && value != null)
                {
                    try
                    {
                        // Do not allow the value to be negative
                        double dblValue = Convert.ToDouble(value);
                        if (dblValue < 0.0D)
                        {
                            dblValue = 0.0D;
                        }

                        // Reduce the value if it is over 100%
                        while (dblValue > 1.0D)
                        {
                            dblValue /= 100.0D;
                        }

                        base.EditValue = dblValue;
                        return;
                    }
                    catch { }
                }
                base.EditValue = value;
            }
        }

        /// <summary>
        /// Conver the value to a suitable display text
        /// </summary>
        /// <returns></returns>
        protected override string GetDisplayText()
        {
            if (!IsShowNullValuePrompt())
            {
                try
                {
                    if (EditValue is double)
                    {
                        return string.Format("{0:p3}", EditValue);
                    }
                }
                catch { }
                return string.Empty;
            }
            return base.GetDisplayText();
        }
    }
}
