namespace DebtPlus.Data.Controls
{
    partial class TelephoneNumberControl
    {
        //UserControl overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.ButtonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonEdit1
            // 
            this.ButtonEdit1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButtonEdit1.Location = new System.Drawing.Point(0, 0);
            this.ButtonEdit1.Margin = new System.Windows.Forms.Padding(0);
            this.ButtonEdit1.Name = "ButtonEdit1";
            this.ButtonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "Update Form", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Update telephone number with form", null, null, false)});
            this.ButtonEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ButtonEdit1.Properties.Mask.BeepOnError = true;
            this.ButtonEdit1.Properties.Mask.EditMask = "(\\+\\d+\\s*)?(\\(\\d+\\)\\s*)?[-0-9]*\\s*(x\\s*\\d+)?";
            this.ButtonEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.ButtonEdit1.Properties.Mask.ShowPlaceHolders = false;
            this.ButtonEdit1.Size = new System.Drawing.Size(150, 20);
            this.ButtonEdit1.TabIndex = 0;
            // 
            // TelephoneNumberControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ButtonEdit1);
            this.Name = "TelephoneNumberControl";
            this.Size = new System.Drawing.Size(150, 20);
            ((System.ComponentModel.ISupportInitialize)(this.ButtonEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }
        internal DevExpress.XtraEditors.ButtonEdit ButtonEdit1;
    }
}
