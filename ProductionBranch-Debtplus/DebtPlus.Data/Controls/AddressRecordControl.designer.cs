namespace DebtPlus.Data.Controls
{
    partial class AddressRecordControl
    {
        //UserControl overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            AddressRecordControl_Dispose(disposing);
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddressRecordControl));
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.Line2 = new DevExpress.XtraEditors.TextEdit();
            this.House = new DevExpress.XtraEditors.TextEdit();
            this.Direction = new DevExpress.XtraEditors.LookUpEdit();
            this.Street = new DevExpress.XtraEditors.TextEdit();
            this.Suffix = new DevExpress.XtraEditors.LookUpEdit();
            this.Modifier = new DevExpress.XtraEditors.LookUpEdit();
            this.Modifier_Value = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_Creditor_Prefix_1 = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_Creditor_Prefix_2 = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_Attn = new DevExpress.XtraEditors.TextEdit();
            this.line3 = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl_Attn = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.Line2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.House.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Direction.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Street.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Suffix.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier_Value.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Attn.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.line3.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Line2
            //
            this.Line2.Location = new System.Drawing.Point(0, 101);
            this.Line2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Line2.Name = "Line2";
            this.Line2.Properties.MaxLength = 80;
            this.Line2.Size = new System.Drawing.Size(476, 20);
            this.Line2.TabIndex = 13;
            this.Line2.ToolTip = "If the address requires two lines, enter the second line of the address here. DO NOT use appartment numbers or PO Boxes here since that belongs on the line above.";
            //
            //House
            //
            this.House.Location = new System.Drawing.Point(0, 76);
            this.House.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.House.Name = "House";
            this.House.Properties.Appearance.Options.UseTextOptions = true;
            this.House.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.House.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.None;
            this.House.Properties.Mask.BeepOnError = true;
            this.House.Properties.Mask.EditMask = "[0-9A-Z]*";
            this.House.Properties.Mask.IgnoreMaskBlank = false;
            this.House.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.House.Properties.Mask.SaveLiteral = false;
            this.House.Properties.Mask.ShowPlaceHolders = false;
            this.House.Properties.MaxLength = 10;
            this.House.Size = new System.Drawing.Size(62, 20);
            this.House.TabIndex = 7;
            this.House.ToolTip = "Number";
            //
            //Direction
            //
            this.Direction.Location = new System.Drawing.Point(68, 76);
            this.Direction.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Direction.Name = "Direction";
            this.Direction.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.Direction.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Direction.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("abbreviation", 5, "Abbreviation"), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending) });
            this.Direction.Properties.DisplayMember = "abbreviation";
            this.Direction.Properties.NullText = "";
            this.Direction.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.Direction.Properties.ShowFooter = false;
            this.Direction.Properties.ValueMember = "abbreviation";
            this.Direction.Size = new System.Drawing.Size(45, 20);
            this.Direction.TabIndex = 8;
            this.Direction.ToolTip = "Direction (N, S, E, W, NE, SE, NW, SW)";
            this.Direction.Properties.SortColumnIndex = 0;
            //
            //Street
            //
            this.Street.Location = new System.Drawing.Point(119, 76);
            this.Street.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Street.Name = "Street";
            this.Street.Properties.MaxLength = 60;
            this.Street.Size = new System.Drawing.Size(149, 20);
            this.Street.TabIndex = 9;
            this.Street.ToolTip = "Street name. This is the primary field for the address. Do NOT include thing such as BLVD, ST, PLACE, etc. but just the street name. If this is a PO BOX, then say so as in \"PO BOX 1134\"";
            //
            //Suffix
            //
            this.Suffix.Location = new System.Drawing.Point(274, 76);
            this.Suffix.Name = "Suffix";
            this.Suffix.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Suffix.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Suffix.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("abbreviation", 5, "Abbreviation"), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending) });
            this.Suffix.Properties.DisplayMember = "abbreviation";
            this.Suffix.Properties.NullText = "";
            this.Suffix.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.Suffix.Properties.ShowFooter = false;
            this.Suffix.Properties.ValueMember = "abbreviation";
            this.Suffix.Size = new System.Drawing.Size(64, 20);
            this.Suffix.TabIndex = 10;
            this.Suffix.ToolTip = "This is the modifier for the street name, such as PLACE, STREET, BLVD, etc.";
            this.Suffix.Properties.SortColumnIndex = 0;
            //
            //Modifier
            //
            this.Modifier.Location = new System.Drawing.Point(344, 76);
            this.Modifier.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Modifier.Name = "Modifier";
            this.Modifier.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Modifier.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("abbreviation", 5, "Abbreviation"), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("require_modifier", 5, "Needs Number") });
            this.Modifier.Properties.DisplayMember = "abbreviation";
            this.Modifier.Properties.NullText = "";
            this.Modifier.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.AutoComplete;
            this.Modifier.Properties.ShowFooter = false;
            this.Modifier.Properties.ValueMember = "abbreviation";
            this.Modifier.Size = new System.Drawing.Size(64, 20);
            this.Modifier.TabIndex = 11;
            this.Modifier.ToolTip = "Choose the appropriate prefix such as APT, STE (suite), FLOOR, BLDG, etc.";
            this.Modifier.Properties.SortColumnIndex = 1;
            //
            //Modifier_Value
            //
            this.Modifier_Value.Location = new System.Drawing.Point(414, 76);
            this.Modifier_Value.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Modifier_Value.Name = "Modifier_Value";
            this.Modifier_Value.Properties.MaxLength = 10;
            this.Modifier_Value.Size = new System.Drawing.Size(62, 20);
            this.Modifier_Value.TabIndex = 12;
            this.Modifier_Value.ToolTip = "If there is an Modifier, a floor, a building, etc. then put the number here.";
            //
            //TextEdit_Creditor_Prefix_1
            //
            this.TextEdit_Creditor_Prefix_1.Location = new System.Drawing.Point(0, 26);
            this.TextEdit_Creditor_Prefix_1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextEdit_Creditor_Prefix_1.Name = "TextEdit_Creditor_Prefix_1";
            this.TextEdit_Creditor_Prefix_1.Properties.MaxLength = 80;
            this.TextEdit_Creditor_Prefix_1.Size = new System.Drawing.Size(476, 20);
            this.TextEdit_Creditor_Prefix_1.TabIndex = 5;
            this.TextEdit_Creditor_Prefix_1.ToolTip = resources.GetString("TextEdit_Creditor_Prefix_1.ToolTip");
            //
            //TextEdit_Creditor_Prefix_2
            //
            this.TextEdit_Creditor_Prefix_2.Location = new System.Drawing.Point(0, 51);
            this.TextEdit_Creditor_Prefix_2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextEdit_Creditor_Prefix_2.Name = "TextEdit_Creditor_Prefix_2";
            this.TextEdit_Creditor_Prefix_2.Properties.MaxLength = 80;
            this.TextEdit_Creditor_Prefix_2.Size = new System.Drawing.Size(476, 20);
            this.TextEdit_Creditor_Prefix_2.TabIndex = 6;
            this.TextEdit_Creditor_Prefix_2.ToolTip = "If the creditor requires a division or department name, then put the name here. This is used for mailing purposes only.";
            //
            //TextEdit_Attn
            //
            this.TextEdit_Attn.Location = new System.Drawing.Point(40, 1);
            this.TextEdit_Attn.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextEdit_Attn.Name = "TextEdit_Attn";
            this.TextEdit_Attn.Properties.MaxLength = 80;
            this.TextEdit_Attn.Size = new System.Drawing.Size(436, 20);
            this.TextEdit_Attn.TabIndex = 4;
            this.TextEdit_Attn.ToolTip = "If you need to make the check or proposal to someone's attention, then put the name of the person or department here. DO NOT include the word 'attention' since it will be automatially included.";
            //
            //line3
            //
            this.line3.Location = new System.Drawing.Point(0, 126);
            this.line3.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.line3.Name = "line3";
            this.line3.Properties.MaxLength = 80;
            this.line3.Size = new System.Drawing.Size(476, 20);
            this.line3.TabIndex = 14;
            this.line3.ToolTip = "If the address requires three lines, enter the third line of the address here. DO NOT use the city/state/zip code since that is entered in the next line.";
            //
            //LabelControl_Attn
            //
            this.LabelControl_Attn.Location = new System.Drawing.Point(3, 5);
            this.LabelControl_Attn.Name = "LabelControl_Attn";
            this.LabelControl_Attn.Size = new System.Drawing.Size(30, 13);
            this.LabelControl_Attn.TabIndex = 3;
            this.LabelControl_Attn.Text = "ATTN:";
            //
            //AddressRecordControl
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6.0F, 13.0F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LabelControl_Attn);
            this.Controls.Add(this.TextEdit_Attn);
            this.Controls.Add(this.TextEdit_Creditor_Prefix_1);
            this.Controls.Add(this.TextEdit_Creditor_Prefix_2);
            this.Controls.Add(this.House);
            this.Controls.Add(this.Direction);
            this.Controls.Add(this.Street);
            this.Controls.Add(this.Suffix);
            this.Controls.Add(this.Modifier);
            this.Controls.Add(this.Modifier_Value);
            this.Controls.Add(this.Line2);
            this.Controls.Add(this.line3);
            this.Name = "AddressRecordControl";
            this.Size = new System.Drawing.Size(476, 74);
            ((System.ComponentModel.ISupportInitialize)this.Line2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.House.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Direction.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Street.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Suffix.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Modifier_Value.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Creditor_Prefix_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit_Attn.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.line3.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        public DevExpress.XtraEditors.TextEdit Line2;
        public DevExpress.XtraEditors.TextEdit House;
        public DevExpress.XtraEditors.LookUpEdit Direction;
        public DevExpress.XtraEditors.TextEdit Street;
        public DevExpress.XtraEditors.LookUpEdit Suffix;
        public DevExpress.XtraEditors.LookUpEdit Modifier;
        public DevExpress.XtraEditors.TextEdit Modifier_Value;
        public DevExpress.XtraEditors.TextEdit TextEdit_Creditor_Prefix_1;
        public DevExpress.XtraEditors.TextEdit TextEdit_Creditor_Prefix_2;
        public DevExpress.XtraEditors.TextEdit TextEdit_Attn;
        public DevExpress.XtraEditors.TextEdit line3;
        protected internal DevExpress.XtraEditors.LabelControl LabelControl_Attn;
    }
}
