#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System.ComponentModel;

namespace DebtPlus.Data.Controls
{
        public partial class PhoneNumber : DevExpress.XtraEditors.TextEdit
        {
            /// <summary>
            /// Create an instance of the class
            /// </summary>
            public PhoneNumber()
                : base()
            {
                InitializeComponent();
                RegisterHandlers();
                ToolTip = "This is the telephone number";
            }

            /// <summary>
            /// Register the event handlers
            /// </summary>
            private void RegisterHandlers()
            {
            }

            /// <summary>
            /// Remove the event handler registration
            /// </summary>
            private void UnRegisterHandlers()
            {
            }

            /// <summary>
            /// Handle the LOAD event for the control.
            /// </summary>
            protected override void OnLoaded()
            {
                base.OnLoaded();

                // Define the configuration for the SSN number
                Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
                Properties.Mask.EditMask = "(999) 000-0000";
                Properties.Mask.SaveLiteral = false;
                Properties.Mask.ShowPlaceHolders = true;
                Properties.Mask.UseMaskAsDisplayFormat = true;
                Properties.Mask.BeepOnError = true;
                Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
                Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
                Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                Properties.DisplayFormat.Format = new DebtPlus.Utils.Format.PhoneNumber.CustomFormatter();
                Properties.NullText = string.Empty;
            }

            /// <summary>
            /// Is the control READONLY? Can it be changed?
            /// </summary>
            [Description("Is the control READ-ONLY and prevents changes?"), DefaultValue(typeof(bool), "false"), Browsable(true), Category("DebtPlus")]
            public bool ReadOnly
            {
                get
                {
                    return Properties.ReadOnly;
                }
                set
                {
                    Properties.ReadOnly = value;
                }
            }

            public void MaskNumber()
            {
                Properties.UseSystemPasswordChar = true;
                Properties.ReadOnly = true;
            }
        }
}
