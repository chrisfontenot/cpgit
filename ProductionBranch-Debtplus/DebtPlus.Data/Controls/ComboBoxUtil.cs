﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.Data.Controls
{
    public static class ComboBoxUtil
    {
        /// <summary>
        /// Locate the selected index in a combobox given the description that is displayed
        /// This assumes that the items in the combobox are items of our ComboboxItem class.
        /// </summary>
        /// <param name="Ctl">Pointer to the combobox to be searched</param>
        /// <param name="Search">String to be found</param>
        /// <returns></returns>
        public static Int32 FindString(DevExpress.XtraEditors.ComboBoxEdit Ctl, string Search)
        {
            for (Int32 itemNo = 0; itemNo < Ctl.Properties.Items.Count; ++itemNo)
            {
                // If the string is a match to the description field then return the associated index component.
                DebtPlus.Data.Controls.ComboboxItem item = (DebtPlus.Data.Controls.ComboboxItem)Ctl.Properties.Items[itemNo];
                if (string.Compare(item.description, Search, true, System.Globalization.CultureInfo.InvariantCulture) == 0)
                {
                    return itemNo;
                }
            }
            return -1;
        }
    }
}
