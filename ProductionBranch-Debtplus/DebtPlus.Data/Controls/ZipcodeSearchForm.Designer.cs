﻿namespace DebtPlus.Data.Controls
{
    partial class ZipcodeSearchForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            ZipcodeSearchForm_Dispose(disposing);
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_State = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl_State = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_City = new DevExpress.XtraEditors.LabelControl();
            this.LookupEdit_City = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_State.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEdit_City.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // SimpleButton_OK
            // 
            this.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton_OK.Enabled = false;
            this.SimpleButton_OK.Location = new System.Drawing.Point(58, 142);
            this.SimpleButton_OK.Name = "SimpleButton_OK";
            this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton_OK.TabIndex = 1;
            this.SimpleButton_OK.Text = "&OK";
            // 
            // SimpleButton_Cancel
            // 
            this.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(158, 142);
            this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
            this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton_Cancel.TabIndex = 2;
            this.SimpleButton_Cancel.Text = "&Cancel";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(12, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(256, 51);
            this.LabelControl1.TabIndex = 9;
            this.LabelControl1.Text = "Please choose the state from the list of possible items. When you have choosen th" +
    "e state, you may choose the city that best corresponds to the location that you " +
    "wish.";
            this.LabelControl1.UseMnemonic = false;
            // 
            // LookUpEdit_State
            // 
            this.LookUpEdit_State.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_State.Location = new System.Drawing.Point(84, 79);
            this.LookUpEdit_State.Name = "LookUpEdit_State";
            this.LookUpEdit_State.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_State.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MailingCode", "State", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountryName", "Country", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.LookUpEdit_State.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.LookUpEdit_State.Properties.DisplayMember = "Name";
            this.LookUpEdit_State.Properties.NullText = "";
            this.LookUpEdit_State.Properties.PopupWidth = 400;
            this.LookUpEdit_State.Properties.ShowFooter = false;
            this.LookUpEdit_State.Properties.SortColumnIndex = 1;
            this.LookUpEdit_State.Properties.ValueMember = "Id";
            this.LookUpEdit_State.Size = new System.Drawing.Size(184, 20);
            this.LookUpEdit_State.TabIndex = 7;
            this.LookUpEdit_State.ToolTip = "This is the state or region where the address is located.";
            // 
            // LabelControl_State
            // 
            this.LabelControl_State.Location = new System.Drawing.Point(11, 82);
            this.LabelControl_State.Name = "LabelControl_State";
            this.LabelControl_State.Size = new System.Drawing.Size(26, 13);
            this.LabelControl_State.TabIndex = 6;
            this.LabelControl_State.Text = "State";
            // 
            // LabelControl_City
            // 
            this.LabelControl_City.Location = new System.Drawing.Point(11, 108);
            this.LabelControl_City.Name = "LabelControl_City";
            this.LabelControl_City.Size = new System.Drawing.Size(49, 13);
            this.LabelControl_City.TabIndex = 5;
            this.LabelControl_City.Text = "City Name";
            // 
            // LookupEdit_City
            // 
            this.LookupEdit_City.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookupEdit_City.Enabled = false;
            this.LookupEdit_City.Location = new System.Drawing.Point(84, 105);
            this.LookupEdit_City.Name = "LookupEdit_City";
            this.LookupEdit_City.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.LookupEdit_City.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookupEdit_City.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CityName", "City", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Formatted_CityType", "Type", 10, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ZIPCode", "ZipCode", 10, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.LookupEdit_City.Properties.DisplayMember = "CityName";
            this.LookupEdit_City.Properties.NullText = "";
            this.LookupEdit_City.Properties.ShowFooter = false;
            this.LookupEdit_City.Properties.SortColumnIndex = 1;
            this.LookupEdit_City.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.LookupEdit_City.Properties.ValueMember = "ZIPCode";
            this.LookupEdit_City.Size = new System.Drawing.Size(184, 20);
            this.LookupEdit_City.TabIndex = 8;
            // 
            // ZipcodeSearchForm
            // 
            this.AcceptButton = this.SimpleButton_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(292, 177);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.LookUpEdit_State);
            this.Controls.Add(this.LabelControl_State);
            this.Controls.Add(this.LabelControl_City);
            this.Controls.Add(this.LookupEdit_City);
            this.Controls.Add(this.SimpleButton_Cancel);
            this.Controls.Add(this.SimpleButton_OK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZipcodeSearchForm";
            this.Text = "Zipcode Search";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_State.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEdit_City.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_State;
        internal DevExpress.XtraEditors.LabelControl LabelControl_State;
        internal DevExpress.XtraEditors.LabelControl LabelControl_City;
        internal DevExpress.XtraEditors.LookUpEdit LookupEdit_City;
    }
}