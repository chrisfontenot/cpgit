#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Data.Controls
{
    public partial class AddressParts : AddressBase, DevExpress.Utils.Controls.IXtraResizableControl
    {
        /// <summary>
        /// Create an instance of the class
        /// </summary>
        public AddressParts()
            : base()
        {
            InitializeComponent();
            if (!InDesignMode)
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Resize += AddressParts_Resize;
            Line1.TextChanged += ControlChanged;
            Line2.TextChanged += ControlChanged;
        }

        private void UnRegisterHandlers()
        {
            Resize -= AddressParts_Resize;
            Line1.TextChanged -= ControlChanged;
            Line2.TextChanged -= ControlChanged;
        }

        /// <summary>
        /// Handle the condition where the enable status changes. Enable or disable the Controls
        /// in our frame.
        /// </summary>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            // Enable or disable the frame controls
            bool isEnabled = Enabled;
            Line1.Enabled = isEnabled;
            Line2.Enabled = isEnabled;
        }

        /// <summary>
        /// Handle the change in the read-only property
        /// </summary>
        /// <param name="e"></param>
        protected override void OnReadonlyChanged(EventArgs e)
        {
            // Do the standard logic first
            base.OnReadonlyChanged(e);

            // Control the read-only status on the controls here
            Line1.Properties.ReadOnly = ReadOnly;
            Line2.Properties.ReadOnly = ReadOnly;
        }

        /// <summary>
        /// Process the resize of the control. Layout the sub-controls.
        /// </summary>
        protected virtual void AddressParts_Resize(object sender, EventArgs e) // Handles this.Resize
        {
            try
            {
                // Our minimum size is 70
                if (Size.Height < 70)
                {
                    Size = new System.Drawing.Size(Size.Width, 70);
                }

                // Make the controls the proper locations
                const Int32 newX = 0;
                Int32 newY = 0;

                // First line is upper left corner.
                Line1.Location = new System.Drawing.Point(newX, newY);
                Line1.Size = new System.Drawing.Size(Width, 20);
                newY += Line1.Size.Height + 5;

                // Second line follows the first
                Line2.Location = new System.Drawing.Point(newX, newY);
                Line2.Size = new System.Drawing.Size(Width, 20);
                newY += Line2.Size.Height + 5;

                // Last line is calculated. Postalcode goes at the right edge and is 20% of the width
                Int32 NewWidth = Convert.ToInt32(Convert.ToDouble(Width - States.Width) * 0.2);
                PostalCode.Location = new System.Drawing.Point(Width - NewWidth, newY);
                PostalCode.Size = new System.Drawing.Size(NewWidth, 20);

                // The state lookup goes to its left. Its size does not change.
                States.Location = new System.Drawing.Point(PostalCode.Location.X - States.Width - 1, newY);

                // The city is on the left edge and takes up the remainder of the space
                NewWidth = States.Location.X - 1;
                City.Location = new System.Drawing.Point(newX, newY);
                City.Size = new System.Drawing.Size(NewWidth, 20);
            }
            catch { }
        }

        /// <summary>
        /// Retrieve a partial object for the address block. The "line1" field is placed into the street name
        /// location, which would be a suitable location should the address line be all together.
        /// </summary>
        /// <returns></returns>
        public override DebtPlus.LINQ.address GetCurrentValues()
        {
            DebtPlus.LINQ.address adr = base.GetCurrentValues();
            adr.street = Line1.Text.Trim();
            adr.address_line_2 = Line2.Text.Trim();

            return adr;
        }

        /// <summary>
        /// Process the change in the city. Set the error conditions and state from the input record.
        /// </summary>
        /// <remarks></remarks>
        protected override void City_ChangeErrorStatus()
        {
            // Do the standard logic first
            base.City_ChangeErrorStatus();

            // Set the address line to the POBOX if this is a POBOX zipcode
            ZipCodeSearch zip = City.GetSelectedDataRow() as ZipCodeSearch;
            if (zip != null && zip.ZIPType == 'P' && string.IsNullOrWhiteSpace(Line1.Text))
            {
                Line1.EditValue = "PO BOX";
                Line2.EditValue = string.Empty;
            }
        }

        #region DevExpress.Utils.Controls.IXtraResizableControl
        #region Changed
        /// <summary>
        /// Event to indicate that the State has been changed
        /// </summary>
        public event EventHandler Changed; // Implements DevExpress.Utils.Controls.IXtraResizableControl.Changed

        /// <summary>
        /// Raise the Changed event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseChanged(EventArgs e)
        {
            if (!InInit)
            {
                var evt = Changed;
                if (evt != null)
                {
                    evt(this, e);
                }
            }
        }

        /// <summary>
        /// Generate the Changed event but allow it to be overriden
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnChanged(EventArgs e)
        {
            RaiseChanged(e);
        }
        #endregion

        /// <summary>
        /// Is the caption to be displayed?
        /// </summary>
        /// <value></value>
        /// <returns>TRUE if the caption is shown</returns>
        /// <remarks>Implements DevExpress.Utils.Controls.IXtraResizableControl.IsCaptionVisible</remarks>
        public bool IsCaptionVisible // Implements DevExpress.Utils.Controls.IXtraResizableControl.IsCaptionVisible
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Maximum size of the control.
        /// </summary>
        /// <value></value>
        /// <returns>System.Drawing.Size for the maximum size</returns>
        /// <remarks>Implements DevExpress.Utils.Controls.IXtraResizableControl.MaxSize</remarks>
        public System.Drawing.Size MaxSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MaxSize
        {
            get
            {
                return new System.Drawing.Size(0, 70);
            }
        }

        /// <summary>
        /// Minimum size of the control.
        /// </summary>
        /// <value></value>
        /// <returns>System.Drawing.Size for the minimum size</returns>
        /// <remarks>Implements DevExpress.Utils.Controls.IXtraResizableControl.MinSize</remarks>
        public System.Drawing.Size MinSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MinSize
        {
            get
            {
                return new System.Drawing.Size(360, 70);
            }
        }
        #endregion

    }
}
