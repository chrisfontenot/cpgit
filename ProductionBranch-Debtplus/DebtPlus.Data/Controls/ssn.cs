#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Windows.Forms;

namespace DebtPlus.Data
{
    namespace Controls
    {
        public partial class ssn : DevExpress.XtraEditors.TextEdit
        {
            public ssn()
            {
                InitializeComponent();
                ToolTip = "This is the person's social security number.";
            }

            [System.ComponentModel.Browsable(false)]
            public new object EditValue
            {
                get
                {
                    // Use NULL if possible for an empty string
                    object ItemValue = base.EditValue;
                    if (ItemValue != null && ItemValue != DBNull.Value)
                    {
                        string strValue = Convert.ToString(ItemValue).Trim(new char[] {' ', '_'}).PadLeft(9, '0');
                        if (string.Compare(strValue, "000000000", false) == 0)
                        {
                            strValue = string.Empty;
                        }

                        if (strValue == string.Empty && base.Properties.AllowNullInput != DevExpress.Utils.DefaultBoolean.False)
                        {
                            return DBNull.Value;
                        }
                    }

                    return ItemValue;
                }
                set
                {
                    if (value == System.DBNull.Value || value == null)
                    {
                        base.EditValue = null;
                        return;
                    }

                    string strValue = Convert.ToString(value).Trim(new char[] {' ', '_'}).PadLeft(9, '0');
                    if (string.Compare(strValue, "000000000") == 0)
                    {
                        base.EditValue = null;
                        return;
                    }
                    base.EditValue = strValue;
                }
            }

            protected override void OnLoaded()
            {
                base.OnLoaded();

                // Define the configuration for the SSN number
                Properties.CharacterCasing = CharacterCasing.Upper;

                Properties.Mask.EditMask = "000-00-0000";
                Properties.Mask.SaveLiteral = false;
                Properties.Mask.ShowPlaceHolders = true;
                Properties.Mask.UseMaskAsDisplayFormat = true;
                Properties.Mask.BeepOnError = true;
                Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
                Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;

                Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                Properties.DisplayFormat.Format = new DebtPlus.Utils.Format.SSN.CustomFormatter();

                Properties.NullText = string.Empty;
            }

            protected override void OnEnter(EventArgs e)
            {
                base.OnEnter(e);
                SelectionLength = 0;
                SelectionStart = 0;
            }
        }
    }
}
