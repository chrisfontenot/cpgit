#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace DebtPlus.Data.Controls
{
    public partial class NameParts : UserControl, DevExpress.Utils.Controls.IXtraResizableControl
    {
        #region TextChanged

        /// <summary>
        /// Event to indicate that the text was changed
        /// </summary>
        public new event EventHandler TextChanged;

        /// <summary>
        /// Raise the text changed event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseTextChanged(EventArgs e)
        {
            var evt = TextChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the text changed event
        /// </summary>
        /// <param name="e"></param>
        protected new virtual void OnTextChanged(EventArgs e)
        {
            RaiseTextChanged(e);
        }

        /// <summary>
        /// Raise the text changed event
        /// </summary>
        protected void RaiseTextChanged()
        {
            OnTextChanged(EventArgs.Empty);
        }
        #endregion

        /// <summary>
        /// Create an instance of the class
        /// </summary>
        public NameParts()
        {
            InitializeComponent();

            // Load the list of standard prefix codes
            Combo_Prefix.Properties.Items.Clear();
            foreach (var row in DebtPlus.LINQ.InMemory.NamePrefixes.getList())
            {
                Combo_Prefix.Properties.Items.Add(row);
            }

            // Load the list of standard suffix codes
            Combo_Suffix.Properties.Items.Clear();
            foreach (var row in DebtPlus.LINQ.InMemory.NameSuffixes.getList())
            {
                Combo_Suffix.Properties.Items.Add(row);
            }

            // Finally, register the event handlers
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        protected void RegisterHandlers()
        {
            Resize                            += NameParts_Resize;
            Combo_Prefix.SelectedIndexChanged += SelectedIndexChanged;
            Combo_Suffix.SelectedIndexChanged += SelectedIndexChanged;
            Combo_Prefix.TextChanged          += TrimName;
            Combo_Suffix.TextChanged          += TrimName;
            Text_First.TextChanged            += TrimName;
            Text_Last.TextChanged             += TrimName;
            Text_Middle.TextChanged           += TrimName;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        protected void UnRegisterHandlers()
        {
            Resize                            -= NameParts_Resize;
            Combo_Prefix.SelectedIndexChanged -= SelectedIndexChanged;
            Combo_Suffix.SelectedIndexChanged -= SelectedIndexChanged;
            Combo_Prefix.TextChanged          -= TrimName;
            Combo_Suffix.TextChanged          -= TrimName;
            Text_First.TextChanged            -= TrimName;
            Text_Last.TextChanged             -= TrimName;
            Text_Middle.TextChanged           -= TrimName;
        }

        private bool privateReadOnly;
        /// <summary>
        /// Is the control Read-Only? Can it be changed?
        /// </summary>
        [Description("Is the control READ-ONLY and prevents changes?"), DefaultValue(typeof(bool), "false"), Browsable(true), Category("DebtPlus")]
        public bool ReadOnly
        {
            get
            {
                return privateReadOnly;
            }
            set
            {
                privateReadOnly = value;

                // Set the status for the ReadOnly condition correctly
                Combo_Prefix.Properties.ReadOnly = privateReadOnly;
                Text_First.Properties.ReadOnly   = privateReadOnly;
                Text_Middle.Properties.ReadOnly  = privateReadOnly;
                Text_Last.Properties.ReadOnly    = privateReadOnly;
                Combo_Suffix.Properties.ReadOnly = privateReadOnly;
            }
        }

        /// <summary>
        /// Load the record information into the displayed controls
        /// </summary>
        /// <param name="NewValues">Pointer to the record structure that we wnat to display</param>
        public virtual void SetValues(DebtPlus.LINQ.Name NewValues)
        {
            Combo_Prefix.Text = NewValues.Prefix;
            Text_First.Text   = NewValues.First;
            Text_Middle.Text  = NewValues.Middle;
            Text_Last.Text    = NewValues.Last;
            Combo_Suffix.Text = NewValues.Suffix;
        }

        /// <summary>
        /// Retrieve the current values from the controls and load a record structure with the settings.
        /// </summary>
        /// <returns></returns>
        public virtual DebtPlus.LINQ.Name GetCurrentValues()
        {
            var NewValues    = DebtPlus.LINQ.Factory.Manufacture_Name();

            NewValues.Prefix = Combo_Prefix.Text.Trim();
            NewValues.First  = Text_First.Text.Trim();
            NewValues.Middle = Text_Middle.Text.Trim();
            NewValues.Last   = Text_Last.Text.Trim();
            NewValues.Suffix = Combo_Suffix.Text.Trim();

            return NewValues;
        }

        /// <summary>
        /// Process the RESIZE event on the control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NameParts_Resize(object sender, EventArgs e) // Handled base.Resize
        {
            System.Drawing.Size NewSize = Size;

            // Do not allow the height to go too far
            if (NewSize.Height > 20)
            {
                NewSize.Height = 20;
            }
            if (NewSize.Width < 20)
            {
                NewSize.Width = 20;
            }

            // Resize the control if it is too small
            if (NewSize.Height != Size.Height || NewSize.Width != Size.Width)
            {
                Size = NewSize;
            }

            // Find the locations of the controls.
            // THe first two are easy. They are at the bounds of the current control.
            Combo_Prefix.Location = new System.Drawing.Point(0, 0);
            Combo_Prefix.Size     = new System.Drawing.Size(Combo_Prefix.Width, NewSize.Height);

            Combo_Suffix.Location = new System.Drawing.Point(NewSize.Width - Combo_Suffix.Width, 0);
            Combo_Suffix.Size     = new System.Drawing.Size(Combo_Suffix.Width, NewSize.Height);

            // Calculate the excess that we have to use
            double Excess = Convert.ToDouble(Combo_Suffix.Location.X - Combo_Prefix.Width);

            // Width of the first name is 30 percent of the width
            Int32 NewWidth = Convert.ToInt32(Excess * 0.3);
            Int32 Xloc = Combo_Prefix.Width;

            Text_First.Location = new System.Drawing.Point(Xloc + 1, 0);
            Text_First.Size     = new System.Drawing.Size(NewWidth - 1, NewSize.Height);
            Xloc               += NewWidth;

            // The middle is 10 percent of the width
            NewWidth             = Convert.ToInt32(Excess * 0.1);
            Text_Middle.Location = new System.Drawing.Point(Xloc + 1, 0);
            Text_Middle.Size     = new System.Drawing.Size(NewWidth - 1, NewSize.Height);
            Xloc                += NewWidth;

            // The last name is the remainder of the distance
            Text_Last.Location = new System.Drawing.Point(Xloc + 1, 0);
            Text_Last.Size     = new System.Drawing.Size(Combo_Suffix.Location.X - Xloc - 2, NewSize.Height);
        }

        /// <summary>
        /// Process the TextChanged event on each of the controls.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TrimName(object sender, EventArgs e) // Handled Combo_Prefix.TextChanged e, Combo_Suffix.TextChanged, Text_First.TextChanged, Text_Middle.TextChanged, Text_Last.TextChanged
        {
            DevExpress.XtraEditors.BaseControl ctl = ((DevExpress.XtraEditors.BaseControl)sender);
            string new_text = ctl.Text.Trim();

            // Remove a leading comma from the text field
            if (new_text.Length > 0 && new_text.StartsWith(","))
            {
                new_text = new_text.Substring(1).Trim();
            }

            // if the field changed then update it.
            if (ctl.Text != new_text)
            {
                ctl.Text = new_text;
            }

            // We always raise this even if the truncated field did not change the text
            // The reason is that we were notified that the text field did change and perhaps
            // it was not a change that had extra spaces around the field.
            RaiseTextChanged();
        }

        /// <summary>
        /// Process the condition where the selection was changed in the combobox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedIndexChanged(object sender, EventArgs e) // Handled Combo_Prefix.SelectIndexChanged, Combo_Suffix.SelectIndexChanged
        {
            RaiseTextChanged();
        }

        #region DevExpress.Utils.Controls.IXtraResizableControl
        public event EventHandler Changed; // Implements DevExpress.Utils.Controls.IXtraResizableControl.Changed
        protected void RaiseChanged(EventArgs e)
        {
            EventHandler evt = Changed;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnChanged(EventArgs e)
        {
            RaiseChanged(e);
        }

        public bool IsCaptionVisible // Implements DevExpress.Utils.Controls.IXtraResizableControl.IsCaptionVisible
        {
            get
            {
                return false;
            }
        }

        public System.Drawing.Size MaxSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MaxSize
        {
            get
            {
                return new System.Drawing.Size(0, 20);
            }
        }

        public System.Drawing.Size MinSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MinSize
        {
            get
            {
                return new System.Drawing.Size(120, 20);
            }
        }
        #endregion
    }
}