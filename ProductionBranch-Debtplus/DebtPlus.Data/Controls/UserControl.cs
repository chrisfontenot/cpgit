#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

namespace DebtPlus.Data.Controls
{
    public partial class UserControl : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// True if the environment is really in the DesignMode. FALSE if it is really in the runtime mode.
        /// </summary>
        public bool InDesignMode
        {
            get
            {
                return CtlInDesignMode(this);
            }
        }

        /// <summary>
        /// Is the control currently in a Design mode?
        /// </summary>
        /// <remarks>
        /// The DesignMode is valid only on the top-most level of the form. We need to find
        /// that form location and then look at it's designmode to determine if the lower level
        /// form is in a designmode.
        /// </remarks>
        /// <returns></returns>
        public static bool CtlInDesignMode(System.Windows.Forms.Control ctrl)
        {
            while (ctrl != null)
            {
                if ((ctrl.Site != null) && ctrl.Site.DesignMode)
                {
                    return true;
                }
                ctrl = ctrl.Parent;
            }
            return false;
        }
    }
}