#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace DebtPlus.Data
{
    namespace Controls
    {
        public class ComboboxItem : object, IComparable
        {

/// <summary>
/// //  ListDescriptionClass
/// //      Used in a combobox list to provide a key value to a displayed item.
/// </summary>

            public object tag { get; set; }
            public object value { get; set; }

            [System.Xml.Serialization.XmlIgnore()]
            public bool active { get; private set; }

            [System.Xml.Serialization.XmlIgnore()]
            public string description { get; private set; }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public ComboboxItem()
            {
                description = string.Empty;
                value = null;
                tag = null;
                active = true;
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="Description"></param>
            public ComboboxItem(string Description) : this(Description, null, true)
            {
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="Description"></param>
            /// <param name="Value"></param>
            public ComboboxItem(string Description, object Value) : this(Description, Value, true)
            {
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="Description"></param>
            /// <param name="Active"></param>
            public ComboboxItem(string Description, bool Active) : this(Description, null, Active)
            {
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="Description"></param>
            /// <param name="Value"></param>
            /// <param name="Active"></param>
            public ComboboxItem(string Description, object Value, bool Active) : this()
            {
                description = Description;
                value = Value;
                active = Active;
            }

            /// <summary>
            /// Convert the value to the display format
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return description;
            }

            /// <summary>
            /// Perform comparisons on the items so that they may be sorted
            /// </summary>
            /// <param name="obj"></param>
            /// <returns></returns>
            public Int32 CompareTo(object obj) // Implements System.IComparable.CompareTo
            {
                ComboboxItem b = obj as ComboboxItem;
                Debug.Assert(b != null);

                // Compare numbers only on non-empty strings
                if (b.description != string.Empty && description != string.Empty)
                {

                    // if the strings start with numbers, try to do a numerical sort
                    if (Char.IsDigit(description, 0) && Char.IsDigit(b.description, 0))
                    {
                        double val1, val2;
                        val1 = Val(description);
                        val2 = Val(b.description);
                        Int32 Relation = val1.CompareTo(val2);
                        if (Relation != 0)
                        {
                            return Relation;
                        }
                    }
                }

                // Do a string comparison if the strings do not start with numbers
                return description.CompareTo(((ComboboxItem)obj).description);
            }

            /// <summary>
            /// Return the first numerical value on the input string. It may have leading blanks but it stops at the first non-numeric. This is similar to the VB6 "Val" function, use in VB.NET.
            /// </summary>
            /// <param name="InputString"></param>
            /// <returns></returns>
            private static double Val(string InputString)
            {
                // Find the first numeric field, ignoring whitespace in the string. Stop at the first non-numeric item.
                System.Text.RegularExpressions.Match match = System.Text.RegularExpressions.Regex.Match(InputString, @"^\s*([-+]?\d+(\.{0,1}(\d+?))?)", System.Text.RegularExpressions.RegexOptions.Singleline);

                // If there is a string then return the numerical value for it.
                if (match.Success)
                {
                    string DoubleString = match.Groups[1].Value;
                    return double.Parse(DoubleString);
                }

                // Otherwise, return 0.
                return 0D;
            }
        }
    }
}
