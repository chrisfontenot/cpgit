﻿namespace DebtPlus.Data.Controls
{
    partial class BlinkLabel
    {
        //Control overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Control Designer
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Timer TimerItem;

        // NOTE: The following procedure is required by the Component Designer
        // It can be modified using the Component Designer.  Do not modify it
        // using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            //
            //TimerItem
            //
            TimerItem = new System.Windows.Forms.Timer(this.components);
        }
    }
}
