#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DebtPlus.Events;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Data.Controls
{
    public partial class AddressRecordControl : AddressBase, DevExpress.Utils.Controls.IXtraResizableControl
    {
        /// <summary>
        /// Handle the dispose operation to discard any locally allocated storage
        /// </summary>
        private void AddressRecordControl_Dispose(bool disposing)
        {
            // Remove the event registration so that we may be cleanly destroyed
            UnRegisterHandlers();
        }

        #region AddressChanged
        // Event handling
        public event AddressChangedEventHandler AddressChanged;
        private void RaiseAddressChanged(AddressChangedEventArgs e)
        {
            if (!InInit)
            {
                AddressChangedEventHandler evt = AddressChanged;
                if (evt != null)
                {
                    evt(this, e);
                }
            }
        }

        protected virtual void OnAddressChanged(AddressChangedEventArgs e)
        {
            RaiseAddressChanged(e);
        }
        #endregion

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public AddressRecordControl()
            : base()
        {
            InitializeComponent();
            if (!InDesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                   += AddressRecordControl_Load;
            Resize                                 += AddressRecordControl_Resize;

            House.TextChanged                      += TrimName;
            Line2.TextChanged                      += TrimName;
            line3.TextChanged                      += TrimName;
            TextEdit_Creditor_Prefix_1.TextChanged += TrimName;
            TextEdit_Creditor_Prefix_2.TextChanged += TrimName;
            TextEdit_Attn.TextChanged              += TrimName;
            Modifier_Value.TextChanged             += TrimName;
            Street.TextChanged                     += TrimName;
            Direction.EditValueChanging            += Validation.LookUpEdit_ActiveTest;
            Suffix.EditValueChanging               += Validation.LookUpEdit_ActiveTest;
            Modifier.EditValueChanging             += Modifier_EditValueChanging;

            Line2.TextChanged                      += ControlChanged;
            line3.TextChanged                      += ControlChanged;
            House.TextChanged                      += ControlChanged;
            Direction.TextChanged                  += ControlChanged;
            Street.TextChanged                     += ControlChanged;
            Suffix.TextChanged                     += ControlChanged;
            Modifier.TextChanged                   += ControlChanged;
            TextEdit_Creditor_Prefix_1.TextChanged += ControlChanged;
            TextEdit_Creditor_Prefix_2.TextChanged += ControlChanged;
            TextEdit_Attn.TextChanged              += ControlChanged;
        }

        /// <summary>
        /// Remove the event registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                   -= AddressRecordControl_Load;
            Resize                                 -= AddressRecordControl_Resize;

            House.TextChanged                      -= TrimName;
            Line2.TextChanged                      -= TrimName;
            line3.TextChanged                      -= TrimName;
            TextEdit_Creditor_Prefix_1.TextChanged -= TrimName;
            TextEdit_Creditor_Prefix_2.TextChanged -= TrimName;
            TextEdit_Attn.TextChanged              -= TrimName;
            Modifier_Value.TextChanged             -= TrimName;
            Street.TextChanged                     -= TrimName;
            Direction.EditValueChanging            -= Validation.LookUpEdit_ActiveTest;
            Suffix.EditValueChanging               -= Validation.LookUpEdit_ActiveTest;
            Modifier.EditValueChanging             -= Modifier_EditValueChanging;

            Line2.TextChanged                      -= ControlChanged;
            line3.TextChanged                      -= ControlChanged;
            House.TextChanged                      -= ControlChanged;
            Direction.TextChanged                  -= ControlChanged;
            Street.TextChanged                     -= ControlChanged;
            Suffix.TextChanged                     -= ControlChanged;
            Modifier.TextChanged                   -= ControlChanged;
            TextEdit_Creditor_Prefix_1.TextChanged -= ControlChanged;
            TextEdit_Creditor_Prefix_2.TextChanged -= ControlChanged;
            TextEdit_Attn.TextChanged              -= ControlChanged;
        }

        /// <summary>
        /// Process a change in the modifier value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Modifier_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ChangeModifierValue((string)e.NewValue);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the modifier value
        /// </summary>
        /// <param name="newValue"></param>
        private void ChangeModifierValue(string newValue)
        {
            Modifier_Value.Enabled = ShouldEnableModifier_Value(newValue);
        }

        /// <summary>
        /// Determine if the modifier value should be enabled
        /// </summary>
        /// <returns></returns>
        private bool ShouldEnableModifier_Value()
        {
            return ShouldEnableModifier_Value((string)Modifier.EditValue);
        }

        /// <summary>
        /// Determine if the modifier value should be enabled
        /// </summary>
        /// <param name="currentValue"></param>
        /// <returns></returns>
        private bool ShouldEnableModifier_Value(string currentValue)
        {
            return Enabled && ModifierHasValue(currentValue);
        }

        /// <summary>
        /// Given the ID, find the flag to determine if the value is required for the modifier
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        private bool ModifierHasValue(string ID)
        {
            if (ID != null)
            {
                var q = LINQ.Cache.postal_abbreviation.getList().Find(s => s.abbreviation == ID && s.type == 3);
                if (q != null)
                {
                    return q.require_modifier;
                }
            }
            return false;
        }

        /// <summary>
        /// Process the load event for the control
        /// </summary>
        private void AddressRecordControl_Load(object sender, EventArgs e) // Handles this.Load
        {
            if (InDesignMode)
            {
                return;
            }

            UnRegisterHandlers();
            try
            {
                Direction.Properties.DataSource = (from p in LINQ.Cache.postal_abbreviation.getList() where p.type == 1 orderby p.abbreviation select p).ToList();
                Suffix.Properties.DataSource = (from p in LINQ.Cache.postal_abbreviation.getList() where p.type == 2 orderby p.abbreviation select p).ToList();
                Modifier.Properties.DataSource = (from p in LINQ.Cache.postal_abbreviation.getList() where p.type == 3 orderby p.abbreviation select p).ToList();

                // Load the list of directions
                Direction.Properties.PopupWidth = 400;
                Direction.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;

                // Load the list of street modifiers
                Suffix.Properties.PopupWidth = 400;
                Suffix.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;

                // Set the default modifier if there is one and one is not supplied. We do this here because if there
                // is no record "loaded", but the control is just created, we want a value in this field.
                if (Modifier.EditValue == null)
                {
                    var a = (from p in LINQ.Cache.postal_abbreviation.getList() where p.type == 3 && p.Default select p).FirstOrDefault();
                    if (a != null)
                    {
                        Modifier.EditValue = a.abbreviation;
                    }
                }

                Modifier.Properties.PopupWidth = 400;
                Modifier.Properties.BestFitMode = DevExpress.XtraEditors.Controls.BestFitMode.BestFitResizePopup;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle a change in the read-only status
        /// </summary>
        /// <param name="e"></param>
        protected override void OnReadonlyChanged(EventArgs e)
        {
            // Do the base operations first
            base.OnReadonlyChanged(e);

            // Set the status for the ReadOnly condition correctly
            TextEdit_Attn.Properties.ReadOnly              = ReadOnly;
            TextEdit_Creditor_Prefix_1.Properties.ReadOnly = ReadOnly;
            TextEdit_Creditor_Prefix_2.Properties.ReadOnly = ReadOnly;
            House.Properties.ReadOnly                      = ReadOnly;
            Direction.Properties.ReadOnly                  = ReadOnly;
            Street.Properties.ReadOnly                     = ReadOnly;
            Suffix.Properties.ReadOnly                     = ReadOnly;
            Modifier.Properties.ReadOnly                   = ReadOnly;
            Modifier_Value.Properties.ReadOnly             = ReadOnly;
            Line2.Properties.ReadOnly                      = ReadOnly;
            line3.Properties.ReadOnly                      = ReadOnly;
        }

        /// <summary>
        /// Attention line
        /// </summary>
        [Description("Attention information line"), Browsable(true), Category("DebtPlus"), DefaultValue(typeof(string), ""), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), ReadOnly(true)]
        public string Attention
        {
            get
            {
                return Utils.Nulls.DStr(TextEdit_Attn.EditValue);
            }
            set
            {
                if (string.Compare(Utils.Nulls.DStr(TextEdit_Attn.EditValue), value, true, System.Globalization.CultureInfo.InstalledUICulture) != 0)
                {
                    TextEdit_Attn.EditValue = value;
                    RaisePropertyChanged("Attention");
                }
            }
        }

        /// <summary>
        /// Convert the second line of the address to a valid string
        /// </summary>
        [Description("Convert the second line of the address to a text string"), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), ReadOnly(true)]
        public string AddressLine2
        {
            get
            {
                return Line2.Text.Trim();
            }
        }

        private bool privateShowAttn;
        /// <summary>
        /// Should the attention line be shown?
        /// </summary>
        [Category("DebtPlus"), Description("Show the Attention line?"), Browsable(true), DefaultValue(typeof(bool), "false"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowAttn
        {
            get
            {
                return privateShowAttn;
            }
            set
            {
                if (value != privateShowAttn)
                {
                    privateShowAttn = value;
                    RaisePropertyChanged("ShowAttn");
                    RecalculateLayout();
                }
            }
        }

        private bool privateShowCreditor1;
        /// <summary>
        /// Should the Creditor_Prefix_1 line be shown?
        /// </summary>
        [Category("DebtPlus"), Description("Show the Creditor Prefix line 1?"), Browsable(true), DefaultValue(typeof(bool), "false"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowCreditor1
        {
            get
            {
                return privateShowCreditor1;
            }
            set
            {
                if (value != privateShowCreditor1)
                {
                    privateShowCreditor1 = value;
                    RaisePropertyChanged("ShowCreditor1");
                    RecalculateLayout();
                }
            }
        }

        private bool privateShowCreditor2;
        /// <summary>
        /// Should the Creditor_Prefix_2 line be shown?
        /// </summary>
        [Category("DebtPlus"), Description("Show the Creditor Prefix line 2?"), Browsable(true), DefaultValue(typeof(bool), "false"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowCreditor2
        {
            get
            {
                return privateShowCreditor2;
            }
            set
            {
                if (value != privateShowCreditor2)
                {
                    privateShowCreditor2 = value;
                    RaisePropertyChanged("ShowCreditor2");
                    RecalculateLayout();
                }
            }
        }

        private bool privateShowLine3;
        /// <summary>
        /// Should the Address Line 3 be shown?
        /// </summary>
        [Category("DebtPlus"), Description("Show the line 3?"), Browsable(true), DefaultValue(typeof(bool), "false"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ShowLine3
        {
            get
            {
                return privateShowLine3;
            }
            set
            {
                if (value != privateShowLine3)
                {
                    privateShowLine3 = value;
                    RaisePropertyChanged("ShowLine3");
                    RecalculateLayout();
                }
            }
        }

        #region EditValue
        /// <summary>
        /// Edit value (value of the address record pointer)
        /// </summary>
        [Description("Value for the address record"), Browsable(false), Category("DebtPlus"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), ReadOnly(true), DefaultValue(null)]
        public virtual Int32? EditValue
        {
            get
            {
                return SaveRecord();
            }
            set
            {
                // Define the original data from the database
                OriginalData = ReadRecord(value);

                // Load that into the input form
                SetCurrentValues(OriginalData);
            }
        }

        /// <summary>
        /// Read the record from the database and return the resulting address record object from it.
        /// </summary>
        /// <param name="RecordID">Pointer to the database record. It may be DBNull or null in which case you get back a blank item.</param>
        public virtual address ReadRecord(Int32? RecordID)
        {
            // If there is a record then try to use the values from the database.
            if (RecordID.GetValueOrDefault() > 0)
            {
                var newValues = (from a in bc.addresses where a.Id == RecordID.Value select new LocalAddress(ShowAttn, ShowCreditor1, ShowCreditor2, ShowLine3, a.Id, a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value, a.address_line_2, a.address_line_3, a.creditor_prefix_1, a.creditor_prefix_2, a.city, a.state, a.PostalCode)).FirstOrDefault();
                if (newValues != null)
                {
                    return newValues;
                }
            }

            // Create a blank record with the proper values
            return Factory.Manufacture_address(ShowAttn, ShowCreditor1, ShowCreditor2, ShowLine3);
        }

        /// <summary>
        /// Retrieve the current values from the controls. The result is a new address record
        /// which has not been saved.
        /// </summary>
        /// <returns></returns>
        public override address GetCurrentValues()
        {
            // Fetch the current values from the base control
            address adr = base.GetCurrentValues();

            // Always use the ID of the original data for the new address.
            adr.Id = OriginalData.Id;

            // Augment the values with our data
            adr.attn              = TextEdit_Attn.Text.Trim();
            adr.creditor_prefix_1 = TextEdit_Creditor_Prefix_1.Text.Trim();
            adr.creditor_prefix_2 = TextEdit_Creditor_Prefix_2.Text.Trim();
            adr.house             = House.Text.Trim();
            adr.direction         = Direction.Text.Trim();
            adr.street            = Street.Text.Trim();
            adr.suffix            = Suffix.Text.Trim();
            adr.modifier          = Modifier.Text.Trim();
            adr.modifier_value    = Modifier_Value.Text.Trim();
            adr.address_line_2    = Line2.Text.Trim();
            adr.address_line_3    = line3.Text.Trim();

            return adr;
        }

        /// <summary>
        /// Load the controls with the new value record information
        /// </summary>
        /// <param name="NewValues">Pointer to the structure with the items to be loaded</param>
        public override void SetCurrentValues(address NewValues)
        {
            UnRegisterHandlers();
            try
            {
                // Do the base logic at this point. This is rather complicated since it involves
                // loading three lookup controls and the values must match the table entries.
                base.SetCurrentValues(NewValues);

                // The remainder of the data is not a lookup. Just set the values.
                TextEdit_Attn.EditValue              = NewValues.attn;
                TextEdit_Creditor_Prefix_1.EditValue = NewValues.creditor_prefix_1;
                TextEdit_Creditor_Prefix_2.EditValue = NewValues.creditor_prefix_2;
                House.EditValue                      = NewValues.house;
                Direction.EditValue                  = NewValues.direction;
                Street.EditValue                     = NewValues.street;
                Suffix.EditValue                     = NewValues.suffix;
                Modifier.EditValue                   = NewValues.modifier;
                Modifier_Value.EditValue             = NewValues.modifier_value;
                Line2.EditValue                      = NewValues.address_line_2;
                line3.EditValue                      = NewValues.address_line_3;

                // Set the enabled status when the item has been changed.
                Modifier_Value.Enabled               = ShouldEnableModifier_Value();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change in the city. Set the error conditions and state from the input record.
        /// </summary>
        /// <remarks></remarks>
        protected override void City_ChangeErrorStatus()
        {
            // Do the standard logic first
            base.City_ChangeErrorStatus();

            // Set the address line to the POBOX if this is a POBOX ZIP-Code
            if (getSearchList() != null)
            {
                // Get the zipcode from the entry.
                var rx = new System.Text.RegularExpressions.Regex(@"(\d{5})");
                var m = rx.Match(PostalCode.Text);
                if (m.Success)
                {
                    // If the zipcode is for a PO BOX only, then insert the POBOX request.
                    var zipcode = m.Groups[1].Captures[0].Value;
                    var q = getSearchList().Find(s => string.Compare(s.ZIPCode, zipcode, true) == 0);
                    if (q != null)
                    {
                        if (q.ZIPType == 'P' && string.IsNullOrWhiteSpace(Street.Text))
                        {
                            // Put the text "PO BOX" into the proper location on the form
                            House.EditValue          = string.Empty;
                            Direction.EditValue      = string.Empty;
                            Street.EditValue         = "PO BOX";
                            Suffix.EditValue         = string.Empty;
                            Modifier_Value.EditValue = string.Empty;
                            Line2.EditValue          = string.Empty;
                            line3.EditValue          = string.Empty;

                            // Set the default modifier value from the tables.
                            var a = (from p in LINQ.Cache.postal_abbreviation.getList() where p.type == 3 && p.Default select p).FirstOrDefault();
                            Modifier.EditValue = a != null ? a.abbreviation : string.Empty;

                            // Set the enabled status when the item has been changed.
                            Modifier_Value.Enabled = ShouldEnableModifier_Value();

                            // Set the focus to the component for the "PO BOX"
                            Street.Focus();
                        }
                    }
                }
            }
        }

        // Has the address record been changed and need to be updated in the database?
        private bool SaveRecordChanged = false;

        /// <summary>
        /// Save the record to the database
        /// </summary>
        /// <returns>The record location in the database table</returns>
        protected virtual Int32? SaveRecord()
        {
            UnRegisterHandlers();
            try
            {
                // If the record is not new then try to find the record to be updated
                address NewValues = GetCurrentValues();

                // Do the processing only if we are to allow the data to be changed.
                if (InDesignMode || ReadOnly)
                {
                    return NewValues.Id;
                }

                // If the record is not new then attempt to find it in the database. The current record
                // may not be found, but then we will fall through to the "new record" logic below to add it
                // again.
                if (NewValues.Id >= 1)
                {
                    var q = (from a in bc.addresses where a.Id == NewValues.Id select a).FirstOrDefault();
                    if (q != null)
                    {
                        q.PropertyChanged  += q_PropertyChanged;
                        SaveRecordChanged   = false;

                        // Update the record with the changed data.
                        q.house             = NewValues.house;
                        q.direction         = NewValues.direction;
                        q.street            = NewValues.street;
                        q.suffix            = NewValues.suffix;
                        q.modifier          = NewValues.modifier;
                        q.modifier_value    = NewValues.modifier_value;
                        q.creditor_prefix_1 = NewValues.creditor_prefix_1;
                        q.creditor_prefix_2 = NewValues.creditor_prefix_2;
                        q.address_line_2    = NewValues.address_line_2;
                        q.address_line_3    = NewValues.address_line_3;
                        q.city              = NewValues.city;
                        q.state             = NewValues.state;
                        q.PostalCode        = NewValues.PostalCode;

                        q.PropertyChanged -= q_PropertyChanged;

                        // If the address changed then record the changes in the database.
                        if (SaveRecordChanged)
                        {
                            bc.SubmitChanges();
                            OnAddressChanged(new AddressChangedEventArgs(NewValues, OriginalData));
                            OriginalData = NewValues;
                        }
                        return OriginalData.Id;
                    }
                }

                // Insert the new address into the database
                bc.addresses.InsertOnSubmit(NewValues);
                bc.SubmitChanges();
                OnAddressChanged(new AddressChangedEventArgs(NewValues, OriginalData));
                OriginalData = NewValues;
                return OriginalData.Id;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Event handler for the change in the address record
        /// </summary>
        private void q_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            SaveRecordChanged = true;
        }
        #endregion

        /// <summary>
        /// Handle the condition where the enable status changes. Enable or disable the Controls
        /// in our frame.
        /// </summary>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            // Enable or disable the frame controls
            bool isEnabled                     = Enabled;

            House.Enabled                      = isEnabled;
            Direction.Enabled                  = isEnabled;
            Street.Enabled                     = isEnabled;
            Suffix.Enabled                     = isEnabled;
            Modifier.Enabled                   = isEnabled;
            TextEdit_Attn.Enabled              = isEnabled;
            TextEdit_Creditor_Prefix_1.Enabled = isEnabled;
            TextEdit_Creditor_Prefix_2.Enabled = isEnabled;
            Line2.Enabled                      = isEnabled;
            line3.Enabled                      = isEnabled;
            City.Enabled                       = isEnabled;
            States.Enabled                     = isEnabled;
            PostalCode.Enabled                 = isEnabled;

            // Set the enabled status when the item has been changed.
            Modifier_Value.Enabled             = ShouldEnableModifier_Value();
        }

        /// <summary>
        /// Trim the entered names so that leading and trailing blanks are removed
        /// </summary>
        protected void TrimName(object sender, EventArgs e) // Handles City.TextChanged, House.TextChanged, Line2.TextChanged, Modifier_Value.TextChanged, PostalCode.TextChanged, Street.TextChanged
        {
            // Remove the leading and trailing blanks. Update the text if the field changed.
            DevExpress.XtraEditors.BaseControl ctl = (DevExpress.XtraEditors.BaseControl)sender;
            string new_text = ctl.Text.Trim();

            if (ctl.Text != new_text)
            {
                ctl.Text = new_text;
                OnTextChanged(e);
            }
        }

        /// <summary>
        /// Layout the control when the size changes
        /// </summary>
        protected void AddressRecordControl_Resize(object sender, EventArgs e) // Handles this.Resize
        {
            RecalculateLayout();
        }

        /// <summary>
        /// Local routine to do the resize calculations
        /// </summary>
        protected virtual void RecalculateLayout()
        {
            RecalculateLayout(Padding.Left, Padding.Top, Height - Padding.Top - Padding.Bottom, Width - Padding.Left - Padding.Right);
        }

        /// <summary>
        /// Re-calculate the layout of the controls when the size is changed
        /// </summary>
        /// <param name="ptX">X location of the control</param>
        /// <param name="ptY">Y location of the control</param>
        /// <param name="SizeHeight">Current height</param>
        /// <param name="SizeWidth">Current width</param>
        protected virtual void RecalculateLayout(Int32 ptX, Int32 ptY, Int32 SizeHeight, Int32 SizeWidth)
        {
            Int32 ComboHeight = States.Height;
            System.Drawing.Point EndPos;

            SuspendLayout();
            try
            {
                // if showing the attention line, put it at the top
                if (ShowAttn)
                {
                    LabelControl_Attn.Location = new System.Drawing.Point(ptX, ptY + 3);
                    LabelControl_Attn.Visible = true;

                    TextEdit_Attn.Location = new System.Drawing.Point(ptX + LabelControl_Attn.Width + ControlSpacing, ptY);
                    TextEdit_Attn.Size = new System.Drawing.Size(SizeWidth - TextEdit_Attn.Location.X, ComboHeight);
                    TextEdit_Attn.Visible = true;

                    ptY += ComboHeight + ControlSpacing;
                }
                else
                {
                    LabelControl_Attn.Visible = false;
                    TextEdit_Attn.Visible = false;
                }

                // if showing the prefix line 1, put it next
                if (ShowCreditor1)
                {
                    TextEdit_Creditor_Prefix_1.Location = new System.Drawing.Point(ptX, ptY);
                    TextEdit_Creditor_Prefix_1.Size = new System.Drawing.Size(SizeWidth, ComboHeight);
                    TextEdit_Creditor_Prefix_1.Visible = true;
                    ptY += ComboHeight + ControlSpacing;
                }
                else
                {
                    TextEdit_Creditor_Prefix_1.Visible = false;
                }

                // if showing the prefix line 2, put it next
                if (ShowCreditor2)
                {
                    TextEdit_Creditor_Prefix_2.Location = new System.Drawing.Point(ptX, ptY);
                    TextEdit_Creditor_Prefix_2.Size = new System.Drawing.Size(SizeWidth, ComboHeight);
                    TextEdit_Creditor_Prefix_2.Visible = true;
                    ptY += ComboHeight + ControlSpacing;
                }
                else
                {
                    TextEdit_Creditor_Prefix_2.Visible = false;
                }

                // The next line is the house address.
                System.Drawing.Point StartPos = new System.Drawing.Point(ptX, ptY);
                House.Location = StartPos;
                StartPos.X += House.Width + ControlSpacing;

                Direction.Location = StartPos;
                StartPos.X += Direction.Width + ControlSpacing;

                EndPos = new System.Drawing.Point(SizeWidth, ptY);
                EndPos.X -= Modifier_Value.Width;
                Modifier_Value.Location = EndPos;
                EndPos.X -= ControlSpacing;

                EndPos.X -= Modifier.Width;
                Modifier.Location = EndPos;
                ptY += ComboHeight + ControlSpacing;
                EndPos.X -= ControlSpacing;

                EndPos.X -= Suffix.Width;
                Suffix.Location = EndPos;
                EndPos.X -= ControlSpacing;

                // Calculate the size for the width of the street name.
                Int32 NewWidth = EndPos.X - StartPos.X;
                Street.Location = StartPos;
                Street.Size = new System.Drawing.Size(NewWidth, ComboHeight);

                // The second line of the address is the full width of the control
                Line2.Location = new System.Drawing.Point(ptX, ptY);
                Line2.Size = new System.Drawing.Size(SizeWidth, ComboHeight);
                ptY += ComboHeight + ControlSpacing;

                // if there is a third line to the address, do it next
                if (ShowLine3)
                {
                    line3.Location = new System.Drawing.Point(ptX, ptY);
                    line3.Size = new System.Drawing.Size(SizeWidth, ComboHeight);
                    line3.Visible = true;
                    ptY += ComboHeight + ControlSpacing;
                }
                else
                {
                    line3.Visible = false;
                }

                // The third line is comprised of the city/state/postal-code
                EndPos = new System.Drawing.Point(SizeWidth, ptY);
                EndPos.X -= PostalCode.Width;
                PostalCode.Location = EndPos;
                EndPos.X -= ControlSpacing;

                // Include the state drop-down
                EndPos.X -= States.Width;
                States.Location = EndPos;
                EndPos.X -= ControlSpacing;

                // The city name is the last
                City.Location = new System.Drawing.Point(ptX, ptY);
                City.Size = new System.Drawing.Size(EndPos.X - ptX, ComboHeight);
            }

            catch { }

            finally
            {
                ResumeLayout(false);
                PerformLayout();
            }
        }

        #region IXtraResizableControl
        #region Changed
        public event EventHandler Changed; // Implements DevExpress.Utils.Controls.IXtraResizableControl.Changed
        protected void RaiseChanged(EventArgs e)
        {
            if (!InInit)
            {
                EventHandler evt = Changed;
                if (evt != null)
                {
                    evt(this, e);
                }
            }
        }

        protected void OnChanged(EventArgs e)
        {
            RaiseChanged(e);
        }
        #endregion

        /// <summary>
        /// Should the caption be visible?
        /// </summary>
        public bool IsCaptionVisible // Implements DevExpress.Utils.Controls.IXtraResizableControl.IsCaptionVisible
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Maximum size of the control
        /// </summary>
        public System.Drawing.Size MaxSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MaxSize
        {
            get
            {
                RecalculateLayout();
                return new System.Drawing.Size(0, States.Height + States.Location.Y);
            }
        }

        /// <summary>
        /// Minimum size of the control
        /// </summary>
        public virtual System.Drawing.Size MinSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MinSize
        {
            get
            {
                RecalculateLayout();
                return new System.Drawing.Size(360, States.Height + States.Location.Y);
            }
        }
        #endregion

        #region LocalAddress class
        /// <summary>
        /// Local class to allow the record to be read from the database
        /// The "ShowAttn", "ShowCreditor1", "ShowCreditor2", and "ShowLine3" need to be set but they are
        /// not part of the address record. This class allows them to be set from the input parameters.
        /// </summary>
        private class LocalAddress : address
        {
            public LocalAddress(bool ShowAttn, bool ShowCreditor1, bool ShowCreditor2, bool ShowLine3,
                   Int32 Id, string house, string direction, string street, string suffix, string modifier, string modifier_value,
                   string address_line_2, string address_line_3, string creditor_prefix_1, string creditor_prefix_2,
                   string city, Int32 state, string PostalCode)
                : base(ShowAttn, ShowCreditor1, ShowCreditor2, ShowLine3)
            {
                this.Id                = Id;
                this.house             = house ?? string.Empty;
                this.direction         = direction ?? string.Empty;
                this.street            = street ?? string.Empty;
                this.suffix            = suffix ?? string.Empty;
                this.modifier          = modifier ?? string.Empty;
                this.modifier_value    = modifier_value ?? string.Empty;
                this.address_line_2    = address_line_2 ?? string.Empty;
                this.address_line_3    = address_line_3 ?? string.Empty;
                this.creditor_prefix_1 = creditor_prefix_1 ?? string.Empty;
                this.creditor_prefix_2 = creditor_prefix_2 ?? string.Empty;
                this.city              = city ?? string.Empty;
                this.state             = state;
                this.PostalCode        = PostalCode ?? string.Empty;
            }
        }
        #endregion
    }
}
