#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.Events;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Data.Controls
{
    public class TelephoneNumberRecordControl : TelephoneNumberControl
    {
        protected const string MyTableName = "TelephoneNumbers";
        private DebtPlus.LINQ.TelephoneNumber OriginalData;

        // Indicate that the text was changed in the control.
        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
        }

        #region TelephoneNumberChanged
        public event TelephoneNumberChangedEventHandler TelephoneNumberChanged;
        protected void RaiseTelephoneNumberChanged(TelephoneNumberChangedEventArgs e)
        {
            if (!InInit)
            {
                TelephoneNumberChangedEventHandler evt = TelephoneNumberChanged;
                if (evt != null)
                {
                    evt(this, e);
                }
            }
        }

        protected virtual void OnTelephoneNumberChanged(TelephoneNumberChangedEventArgs e)
        {
            RaiseTelephoneNumberChanged(e);
        }
        #endregion

        public TelephoneNumberRecordControl()
        {
            if (!InDesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Add event handler linkage if needed
        /// </summary>
        protected new void RegisterHandlers()
        {
            // There are none
        }

        /// <summary>
        /// Undo the work of RegisterHandlers
        /// </summary>
        protected new void UnRegisterHandlers()
        {
            // There are none
        }

        /// <summary>
        /// Database row pointer to the current name
        /// </summary>
        [Browsable(false), Description("Value for database"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), ReadOnly(true)]
        public new Int32? EditValue
        {
            get
            {
                UnRegisterHandlers();
                base.UnRegisterHandlers();
                try
                {
                    return SaveRecord();
                }
                finally
                {
                    base.RegisterHandlers();
                    RegisterHandlers();
                }
            }

            set
            {
                // Remove the event handlers to preclude side-effects
                UnRegisterHandlers();
                base.UnRegisterHandlers();
                try
                {
                    OriginalData = ReadRecord(value);
                    SetCurrentValues(OriginalData);
                }
                finally
                {
                    base.RegisterHandlers();
                    RegisterHandlers();
                }
            }
        }

        /// <summary>
        /// Return the fields for the number
        /// </summary>
        /// <returns></returns>
        public override DebtPlus.LINQ.TelephoneNumber GetCurrentValues()
        {
            // Retrieve the base field definitions.
            DebtPlus.LINQ.TelephoneNumber answer = base.GetCurrentValues();

            // Ensure that the record number is passed along with the current information
            if (OriginalData != null)
            {
                answer.Id = OriginalData.Id;
            }

            return answer;
        }

        /// <summary>
        /// Read the record from the database
        /// </summary>
        /// <param name="Record"></param>
        public virtual DebtPlus.LINQ.TelephoneNumber ReadRecord(Int32? Record)
        {
            // Clear the current input record and save the record number
            DebtPlus.LINQ.TelephoneNumber NewValues = null;

            if (Record.HasValue && Record.Value > 0)
            {
                using (var bc = new BusinessContext())
                {
                    NewValues = bc.TelephoneNumbers.Where(s => s.Id == Record.Value).FirstOrDefault();
                }
            }

            if (NewValues == null)
            {
                NewValues = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();
            }

            // Do not allow the country to remain invalid
            if (!NewValues.Country.HasValue || NewValues.Country <= 0)
            {
                NewValues.Country = DebtPlus.LINQ.Cache.country.getDefault();
            }

            // Supply the defaults based upon the current record
            if (string.IsNullOrEmpty(NewValues.Acode) && NewValues.Country.Value == DebtPlus.LINQ.Cache.country.getDefault())
            {
                var cnf = DebtPlus.LINQ.Cache.config.getList().FirstOrDefault();
                if (cnf != null)
                {
                    NewValues.Acode = cnf.areacode;
                }
            }

            return NewValues;
        }

        /// <summary>
        /// Save the record to the database
        /// </summary>
        /// <returns></returns>
        protected virtual Int32? SaveRecord()
        {
            // Move the control values to a new data structure for processing.
            DebtPlus.LINQ.TelephoneNumber NewValues = GetCurrentValues();

            // Determine if we can save the record or not.
            if (!InDesignMode && !ReadOnly)
            {
                using (var dc = new BusinessContext())
                {
                    if (NewValues.Id > 0)
                    {
                        // If the record has not changed then just return the current record
                        // The new number must be valid to be recorded. Ignore invalid numbers.
                        if (! NewValues.IsValid || NewValues.CompareTo(OriginalData) == 0)
                        {
                            return OriginalData.Id;
                        }

                        var q = (from t in dc.TelephoneNumbers where t.Id == NewValues.Id select t).FirstOrDefault();
                        if (q != null)
                        {
                            // Update the record in the database
                            q.Country = NewValues.Country;
                            q.Acode   = NewValues.Acode;
                            q.Number  = NewValues.Number;
                            q.Ext     = NewValues.Ext;

                            // Submit the changes and tell the user of the change
                            dc.SubmitChanges();
                            RaiseTelephoneNumberChanged(new TelephoneNumberChangedEventArgs(OriginalData, NewValues));

                            OriginalData = NewValues;
                            return NewValues.Id;
                        }
                    }

                    // Need to insert a value. Don't insert empty items. Just return the NULL pointer.
                    // The new number must be valid to be recorded. Ignore invalid numbers.
                    if (!NewValues.IsValid || NewValues.IsEmpty())
                    {
                        return new Int32?();
                    }

                    dc.TelephoneNumbers.InsertOnSubmit(NewValues);
                    dc.SubmitChanges();

                    // Update the database with the new record
                    RaiseTelephoneNumberChanged(new TelephoneNumberChangedEventArgs(OriginalData, NewValues));
                    OriginalData = NewValues;
                }
            }

            return NewValues.Id;
        }

        /// <summary>
        /// Do not trip the EditValueChanged event for this control since we
        /// don't do the EditValue the same way. (We can't remove the event,
        /// but we don't generate it either.)
        /// </summary>
        /// <param name="e"></param>
        protected override void OnEditValueChanged(EventArgs e)
        {
            // Do nothing here. This will prevent the event from being generated.
        }

        public void MaskPhone()
        {
            DisableClick();
            ButtonEdit1.Properties.UseSystemPasswordChar = true;
            ButtonEdit1.Enabled = false;
        }
    }
}
