#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Data.Controls
{
    internal partial class TelephoneUpdateForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        DebtPlus.LINQ.TelephoneNumber tn;

        public TelephoneUpdateForm()
            : base()
        {
            InitializeComponent();
        }

        public TelephoneUpdateForm(DebtPlus.LINQ.TelephoneNumber tn)
            : this()
        {
            RegisterHandlers();
            this.tn = tn;
        }

        private void RegisterHandlers()
        {
            Load                                 += Form_Load;
            LookUpEdit_Country.EditValueChanging += Validation.LookUpEdit_ActiveTest;
            LookUpEdit_Country.TextChanged       += RedisplayNumber;
            TextEdit_Acode.TextChanged           += RedisplayNumber;
            TextEdit_Number.TextChanged          += RedisplayNumber;
            TextEdit_Ext.TextChanged             += RedisplayNumber;
            SimpleButton_OK.Click                += SimpleButton_OK_Click;
            SimpleButton_CANCEL.Click            += SimpleButton_CANCEL_Click;
        }

        private void UnRegisterHandlers()
        {
            Load                                 -= Form_Load;
            LookUpEdit_Country.EditValueChanging -= Validation.LookUpEdit_ActiveTest;
            LookUpEdit_Country.TextChanged       -= RedisplayNumber;
            TextEdit_Acode.TextChanged           -= RedisplayNumber;
            TextEdit_Number.TextChanged          -= RedisplayNumber;
            TextEdit_Ext.TextChanged             -= RedisplayNumber;
            SimpleButton_OK.Click                -= SimpleButton_OK_Click;
            SimpleButton_CANCEL.Click            -= SimpleButton_CANCEL_Click;
        }

        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            LookUpEdit_Country.Properties.DataSource = DebtPlus.LINQ.Cache.country.getList();

            try
            {
                // Load the current control values
                LookUpEdit_Country.EditValue = tn.Country;
                TextEdit_Acode.EditValue     = tn.Acode;
                TextEdit_Number.EditValue    = tn.Number;
                TextEdit_Ext.EditValue       = tn.Ext;

                RedisplayNumber(this, EventArgs.Empty);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Display the number as it is changed by the user. We need the ancestor's help to do this.
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void RedisplayNumber(object Sender, EventArgs e)
        {
            var tempTn     = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();
            tempTn.Country = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Country.EditValue);
            tempTn.Acode   = DebtPlus.Utils.Nulls.v_String(TextEdit_Acode.EditValue);
            tempTn.Number  = DebtPlus.Utils.Nulls.v_String(TextEdit_Number.EditValue);
            tempTn.Ext     = DebtPlus.Utils.Nulls.v_String(TextEdit_Ext.EditValue);

            FormattedNumberLabel.Text = tempTn.ToString();
            FormattedNumberLabel.Invalidate();
        }

        private void SimpleButton_CANCEL_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            // Retrieve the current values
            tn.Country = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Country.EditValue);
            tn.Acode   = DebtPlus.Utils.Nulls.v_String(TextEdit_Acode.EditValue);
            tn.Number  = DebtPlus.Utils.Nulls.v_String(TextEdit_Number.EditValue);
            tn.Ext     = DebtPlus.Utils.Nulls.v_String(TextEdit_Ext.EditValue);

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
