#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Repository;

namespace DebtPlus.Data.Controls
{
    internal static class Tables
    {
        /// <summary>
        /// Postal Abbreviations used in the addresses
        /// </summary>
        /// <param name="DataSource"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Obsolete("Use LINQ Tables")]
        internal static DataTable PostalAbbreviationsTable(DataSet DataSource)
        {
            const string MyTableName = "postal_abbreviations";
            DataTable tbl = null;

            if (DataSource != null)
            {
                tbl = DataSource.Tables[MyTableName];
                if (tbl == null)
                {
                    DebtPlus.Repository.Lookups.GetAllPostalAbbreviations(DataSource, MyTableName);
                    tbl = DataSource.Tables[MyTableName];
                }
            }

            return tbl;
        }

        /// <summary>
        /// return the table of states defined in the system
        /// </summary>
        /// <returns>A pointer to the states table in the dataset</returns>
        /// <remarks></remarks>
        [System.Obsolete("Use LINQ Tables")]
        internal static DataTable StatesTable(DataSet DataSource)
        {
            const string MyTableName = "states";
            DataTable tbl = null;

            if (DataSource != null)
            {
                tbl = DataSource.Tables[MyTableName];
                if (tbl == null)
                {
                    DebtPlus.Repository.Lookups.GetAllStates(DataSource, MyTableName);
                    tbl = DataSource.Tables[MyTableName];
                }
            }
            return tbl;
        }

        /// <summary>
        /// Country information table
        /// </summary>
        /// <param name="DatabaseInfo"></param>
        /// <param name="DataSource"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Obsolete("Use LINQ Tables")]
        internal static DataTable CountriesTable(DataSet DataSource)
        {
            const string MyTableName = "countries";
            DataTable tbl = null;

            if (DataSource != null)
            {
                tbl = DataSource.Tables[MyTableName];
                if (tbl == null)
                {
                    DebtPlus.Repository.LookupTables.Countries.GetAll(DataSource, MyTableName);
                    tbl = DataSource.Tables[MyTableName];
                }
            }
            return tbl;
        }

        /// <summary>
        /// Zipcode to state search information table
        /// </summary>
        /// <param name="DataSource">Pointer to the dataset used to hold the items</param>
        /// <param name="PostalCode">if not empty, then retrieve only these items from the database. if empty, just get the schema</param>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Obsolete("Use LINQ Tables")]
        internal static DataTable ZipcodeSearchTable(DataSet DataSource, string PostalCode)
        {
            const string MyTableName = "zipcode_search";

            // if we want just the pointer to the able but no records and the table exists, return the existing table.
            DataTable tbl = null;
            
            if( DataSource != null )
            {
                tbl = DataSource.Tables[MyTableName];
                if (string.IsNullOrEmpty( PostalCode ) && tbl != null)
                {
                    return tbl;
                }

                if (string.IsNullOrEmpty(PostalCode))
                {
                    DebtPlus.Repository.Views.ZipCodeSearch.GetSchema(DataSource, MyTableName);
                }
                else
                {
                    DebtPlus.Repository.Views.ZipCodeSearch.GetByPostalCode(DataSource, MyTableName, PostalCode.Replace("'", "''"));
                }

                tbl = DataSource.Tables[MyTableName];
            }

            return tbl;
        }

        /// <summary>
        /// Zipcode to state search information table
        /// </summary>
        /// <param name="DataSource">Pointer to the dataset used to hold the items</param>
        /// <param name="State">Retrieve the information for the indicated state</param>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Obsolete("Use LINQ Tables")]
        internal static DataTable ZipcodeSearchTable(DataSet DataSource, Int32 State)
        {
            const string MyTableName = "zipcode_search_state";

            // if we want just the pointer to the able but no records and the table exists, return the existing table.
            DataTable tbl = null;

            if (DataSource != null)
            {
                tbl = DataSource.Tables[MyTableName];
                if (State < 0 && tbl != null)
                {
                    return tbl;
                }

                if (State < 0)
                {
                    DebtPlus.Repository.Views.ZipCodeSearch.GetSchema(DataSource, MyTableName);
                }
                else
                {
                    DebtPlus.Repository.Views.ZipCodeSearch.GetByState(DataSource, MyTableName, State);
                }

                tbl = DataSource.Tables[MyTableName];
            }

            return tbl;
        }
    }
}
