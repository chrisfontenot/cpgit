using System;
using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;

namespace DebtPlus.Data.Controls
{
    [UserRepositoryItem("RegisterPercentEdit")]
    public class RepositoryItemPercentEdit : RepositoryItemCalcEdit
    {
        public override string GetDisplayText(object editValue)
        {
            if (editValue == null || editValue == DBNull.Value)
            {
                return NullText;
            }

            double dbl = Convert.ToDouble(editValue);
            if (dbl < 0.0D)
            {
                dbl = 0.0D;
            }

            while (dbl > 1.0D)
            {
                dbl /= 100.0D;
            }

            if (!Allow100Percent && dbl == 1.0D)
            {
                dbl = 0.01D;
            }
            return string.Format("{0:p3}", dbl);
        }

        public override string GetDisplayText(DevExpress.Utils.FormatInfo format, object editValue)
        {
            if (editValue == null || editValue == DBNull.Value)
            {
                return NullText;
            }

            double dbl = Convert.ToDouble(editValue);
            if (dbl < 0.0D)
            {
                dbl = 0.0D;
            }

            while (dbl > 1.0D)
            {
                dbl /= 100.0D;
            }

            if (!Allow100Percent && dbl == 1.0D)
            {
                dbl = 0.01D;
            }

            return string.Format("{0:p3}", dbl);
        }

        //The static constructor which calls the registration method
        static RepositoryItemPercentEdit()
        {
            RegisterPercentEdit();
        }

        //Initialize new properties
        public RepositoryItemPercentEdit()
        {
            ShowCloseButton = false;
            Precision = 3;
            DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            DisplayFormat.FormatString = "{0:p3}";
            EditFormat.FormatString = "{0:f3}";
        }

        // The unique name for the Percent editor
        public const string PercentEditName = "PercentEdit";

        // Return the unique name
        public override string EditorTypeName
        {
            get
            {
                return PercentEditName;
            }
        }

        // Register the editor
        public static void RegisterPercentEdit()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(PercentEditName, typeof(PercentEdit), typeof(RepositoryItemPercentEdit), typeof(PercentEditViewInfo), new ButtonEditPainter(), true));
        }

        // Override the Assign method
        public override void Assign(RepositoryItem item)
        {
            BeginUpdate();
            try
            {
                base.Assign(item);

                RepositoryItemPercentEdit source = (RepositoryItemPercentEdit)item;
                if (source != null)
                {

                }

            }
            finally
            {
                EndUpdate();
            }
        }

        private bool privateAllow100Percent = false;

        /// <summary>
        /// Do we allow 100% in the control. Most range from 0 to almost 100%, but do not include 100%.
        /// </summary>
        public bool Allow100Percent
        {
            get
            {
                return privateAllow100Percent;
            }
            set
            {
                if (value != privateAllow100Percent)
                {
                    privateAllow100Percent = value;
                    RaisePropertiesChanged(EventArgs.Empty);
                }
            }
        }
    }
}
