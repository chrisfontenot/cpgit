#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;

namespace DebtPlus.Data
{
    namespace Controls
    {
        public class SortedCheckedListboxControlItem : DevExpress.XtraEditors.Controls.CheckedListBoxItem
        {
            /// <summary>
            /// Value for the list control item
            /// </summary>
            public new object Value { get; set; }

            /// <summary>
            /// Description for the list control
            /// </summary>
            public new string Description
            {
                get
                {
                    return base.Description;
                }
                set
                {
                    base.Description = value;
                    base.Value = value;
                }
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            public SortedCheckedListboxControlItem()
            {
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            /// <param name="value"></param>
            public SortedCheckedListboxControlItem(object value) :
                this()
            {
                Value = value;
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            /// <param name="value"></param>
            /// <param name="isChecked"></param>
            public SortedCheckedListboxControlItem(object value, bool isChecked) :
                this(value, ((isChecked) ? System.Windows.Forms.CheckState.Checked : System.Windows.Forms.CheckState.Unchecked))
            {
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            /// <param name="value"></param>
            /// <param name="description"></param>
            public SortedCheckedListboxControlItem(object value, string description) :
                this()
            {
                Value = value;
                Description = description;
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            /// <param name="value"></param>
            /// <param name="checkState"></param>
            public SortedCheckedListboxControlItem(object value, CheckState checkState) :
                this()
            {
                Value = value;
                base.CheckState = checkState;
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            /// <param name="value"></param>
            /// <param name="description"></param>
            /// <param name="checkState"></param>
            public SortedCheckedListboxControlItem(object value, string description, CheckState checkState) :
                this(value, description)
            {
                base.CheckState = checkState;
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            /// <param name="value"></param>
            /// <param name="checkState"></param>
            /// <param name="enabled"></param>
            public SortedCheckedListboxControlItem(object value, CheckState checkState, bool enabled) :
                this(value, checkState)
            {
                base.Enabled = enabled;
            }

            /// <summary>
            /// Create an instance of the class
            /// </summary>
            /// <param name="value"></param>
            /// <param name="description"></param>
            /// <param name="checkState"></param>
            /// <param name="enabled"></param>
            public SortedCheckedListboxControlItem(object value, string description, CheckState checkState, bool enabled) :
                this(value, description, checkState)
            {
                base.Enabled = enabled;
            }
        }
    }
}
