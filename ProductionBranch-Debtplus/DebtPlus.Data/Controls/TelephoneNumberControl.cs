#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.Events;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Data.Controls
{
    public partial class TelephoneNumberControl : UserControl, INotifyPropertyChanged, ISupportInitialize, DevExpress.Utils.Controls.IXtraResizableControl
    {
        protected bool InInit = false;
        private Int32? privateCountry = new Int32?();
        private string privateAcode = null;
        private string privateNumber = null;
        private string privateExt = null;

        #region TextChanged
        /// <summary>
        /// Event raised when the text is changed
        /// </summary>
        public new event EventHandler TextChanged;

        /// <summary>
        /// Raise the TextChanged Event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseTextChanged(EventArgs e)
        {
            var evt = TextChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the TextChanged Event
        /// </summary>
        /// <param name="e"></param>
        protected new virtual void OnTextChanged(EventArgs e)
        {
            RaiseTextChanged(e);
        }

        /// <summary>
        /// Handle the text changed event for the input field.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void ButtonEdit1_TextChanged(object sender, EventArgs e)
        {
            OnTextChanged(e);
        }
        #endregion

        #region EditValueChanged
        public event EventHandler EditValueChanged;
        protected void RaiseEditValueChanged(EventArgs e)
        {
            EventHandler evt = EditValueChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnEditValueChanged(EventArgs e)
        {
            RaiseEditValueChanged(e);
        }
        #endregion

        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged; // Implements System.ComponentModel.INotifyPropertyChanged.PropertyChanged
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e);
        }

        protected void RaisePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler evt = PropertyChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected void RaisePropertyChanged(string PropertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(PropertyName));
        }
        #endregion

        /// <summary>
        /// Create an instance of the control
        /// </summary>
        public TelephoneNumberControl()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        protected void RegisterHandlers()
        {
            Resize                    += TelephoneNumberControl_Resize;
            ButtonEdit1.ButtonPressed += ButtonEdit1_ButtonPressed;
            ButtonEdit1.TextChanged   += ButtonEdit1_TextChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        protected void UnRegisterHandlers()
        {
            Resize                    -= TelephoneNumberControl_Resize;
            ButtonEdit1.ButtonPressed -= ButtonEdit1_ButtonPressed;
            ButtonEdit1.TextChanged   -= ButtonEdit1_TextChanged;
        }

        protected void DisableClick()
        {
            ButtonEdit1.ButtonPressed -= ButtonEdit1_ButtonPressed;
        }

        /// <summary>
        /// Prevent the database from being updated
        /// </summary>
        [Description("Is the control READ-ONLY and prevents changes?"), DefaultValue(typeof(bool), "false"), Browsable(true), Category("DebtPlus")]
        public bool ReadOnly
        {
            get
            {
                return ButtonEdit1.Properties.ReadOnly;
            }
            set
            {
                ButtonEdit1.Properties.ReadOnly = value;
            }
        }

        /// <summary>
        /// Start the initialization sequence
        /// </summary>
        public void BeginInit() // Implements System.ComponentModel.ISupportInitialize.BeginInit
        {
            InInit = true;
        }

        /// <summary>
        /// Complete the initialization sequence
        /// </summary>
        public void EndInit() // Implements System.ComponentModel.ISupportInitialize.EndInit
        {
            InInit = false;
        }

        /// <summary>
        /// The country designation for the telephone number
        /// </summary>
        /// <value>The country id.</value>
        /// <returns>The country code for the telephone number.</returns>
        /// <remarks>This is the country id in the countries table and is not the dialing code for the country. You must find the country in the table to obtain the dialing code for that country.</remarks>
        [Browsable(false), Description("Current value for the country field"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Int32? EditValueCountry
        {
            get
            {
                return privateCountry;
            }
            set
            {
                if (privateCountry != value)
                {
                    privateCountry = value;
                    if (!InInit)
                    {
                        RaisePropertyChanged("EditValueCountry");
                        OnEditValueChanged(EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// The area or city code. NOTHING if the value is not defined. It may be blank.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [Browsable(false), Description("Current value for the area or city code field"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string EditValueAcode
        {
            get
            {
                return privateAcode;
            }
            set
            {
                if (string.Compare(privateAcode, value, true) != 0)
                {
                    privateAcode = value;
                    if (!InInit)
                    {
                        RaisePropertyChanged("EditValueAcode");
                        OnEditValueChanged(EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// The local telephone number. USA telephones have a dash. Others don't.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [Browsable(false), Description("Current value for the local telephone number field"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string EditValueNumber
        {
            get
            {
                return privateNumber;
            }
            set
            {
                string ConvertedNumber = value ?? string.Empty;
                DebtPlus.LINQ.country co = null;
                if (EditValueCountry.HasValue && EditValueCountry.Value > 0)
                {
                    co = DebtPlus.LINQ.Cache.country.getList().Find(s => s.Id == EditValueCountry.Value);
                }

                if (co == null || co.country_code == 1)
                {
                    ConvertedNumber = DebtPlus.Utils.Format.Strings.DigitsOnly(value);
                    if (ConvertedNumber.Length > 4)
                    {
                        string LeftPortion = ConvertedNumber.Substring(0, 3);
                        string RightPortion = ConvertedNumber.Substring(3);
                        ConvertedNumber = String.Format("{0}-{1}", LeftPortion, RightPortion);
                    }
                }

                // if the values change then update the item and tell the world
                if (string.Compare(privateNumber, ConvertedNumber, true) != 0)
                {
                    privateNumber = ConvertedNumber;
                    if (!InInit)
                    {
                        RaisePropertyChanged("EditValueNumber");
                        OnEditValueChanged(EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// The extension number at the telephone exchange.
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [Browsable(false), Description("Current value for the telephone number extension field"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string EditValueExt
        {
            get
            {
                return privateExt;
            }

            set
            {
                if (value == null)
                {
                    value = string.Empty;
                }

                if (string.Compare(privateExt, value, true) != 0)
                {
                    privateExt = value;
                    if (!InInit)
                    {
                        RaisePropertyChanged("EditValueExt");
                        OnEditValueChanged(EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// Return the data structure with the current values from the input control
        /// </summary>
        /// <returns></returns>
        public virtual DebtPlus.LINQ.TelephoneNumber GetCurrentValues()
        {
            var data     = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();

            data.Country = DebtPlus.Utils.Nulls.v_Int32(EditValueCountry);
            data.Acode   = DebtPlus.Utils.Nulls.v_String(EditValueAcode);
            data.Number  = DebtPlus.Utils.Nulls.v_String(EditValueNumber);
            data.Ext     = DebtPlus.Utils.Nulls.v_String(EditValueExt);

            return data;
        }

        /// <summary>
        /// Set the fields from the passed structure into the corresponding data fields for display
        /// in the control.
        /// </summary>
        /// <param name="InputFields"></param>
        public virtual void SetCurrentValues(DebtPlus.LINQ.TelephoneNumber InputFields)
        {
            // Update the values for the number
            EditValueCountry = InputFields.Country;
            EditValueAcode   = InputFields.Acode;
            EditValueNumber  = InputFields.Number;
            EditValueExt     = InputFields.Ext;

            // Update the number display accordingly
            ButtonEdit1.Text = InputFields.ToString();
        }

        /// <summary>
        /// Invoke the form update routine to ask the user for the separate fields.
        /// </summary>
        /// <returns>TRUE if the user clicked OK. FALSE otherwise.</returns>
        /// <remarks></remarks>
        private bool ValidateForm(ref DebtPlus.LINQ.TelephoneNumber NewValues)
        {
            using (var frm = new Controls.TelephoneUpdateForm(NewValues))
            {
                return frm.ShowDialog() == System.Windows.Forms.DialogResult.OK;
            }
        }

        /// <summary>
        /// Validate the field and translate the number to a suitable format.
        /// </summary>
        /// <param name="e">Pointer to the event arguments</param>
        /// <remarks></remarks>
        protected override void OnValidating(CancelEventArgs e)
        {
            // Do the base validation now.
            base.OnValidating(e);

            // Process the number if we are not in our basic initialization sequence.
            if (!e.Cancel && !InDesignMode && !InInit)
            {
                // Retrieve the number from the input. Always allow a blank entry
                string InputString = (DebtPlus.Utils.Nulls.v_String(ButtonEdit1.EditValue) ?? string.Empty).Trim();
                if (string.IsNullOrEmpty(InputString))
                {
                    EditValueCountry = null;
                    EditValueAcode   = null;
                    EditValueNumber  = null;
                    EditValueExt     = null;
                    return;
                }

                // HDT-161 kw :: Remove any grouping symbols, aka -, so that it won't pop-up as an issue.
                InputString = InputString.Replace("-", "");

                // Try to parse the number from the input string.
                DebtPlus.LINQ.TelephoneNumber NewValues = DebtPlus.LINQ.TelephoneNumber.TryParse(InputString);
                if (NewValues == null)
                {
                    NewValues = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();
                    NewValues.Number = (ButtonEdit1.Text ?? string.Empty).Trim();
                }

                // Validate the number from the input strings
                if (!NewValues.IsValid && !ValidateForm(ref NewValues))
                {
                    e.Cancel = true;
                    return;
                }

                // Update the values accordingly
                EditValueCountry = NewValues.Country;
                EditValueAcode   = NewValues.Acode;
                EditValueNumber  = NewValues.Number;
                EditValueExt     = NewValues.Ext;

                // Display the button text
                ButtonEdit1.Text = NewValues.ToString();
            }
        }

        /// <summary>
        /// The edit value is the record number in the TelephoneNumbers table
        /// </summary>
        [Browsable(false), Description("Value for database"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string EditValue
        {
            get
            {
                string strText = Text.Trim();
                if (strText == string.Empty)
                {
                    return null;
                }
                return strText;
            }
            set
            {
                // If we are initializing the control then ignore the setting and just set the button text.
                if (!InInit && !InDesignMode)
                {
                    // Otherwise, try to parse the string into the fields
                    DebtPlus.LINQ.TelephoneNumber NewValues = DebtPlus.LINQ.TelephoneNumber.TryParse(value);
                    if (NewValues != null)
                    {
                        // Update the values accordingly
                        EditValueCountry = NewValues.Country;
                        EditValueAcode   = NewValues.Acode;
                        EditValueNumber  = NewValues.Number;
                        EditValueExt     = NewValues.Ext;

                        // Display the button text
                        ButtonEdit1.Text = NewValues.ToString();
                        return;
                    }
                }
                ButtonEdit1.Text = value;
            }
        }

        /// <summary>
        /// Display text
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public new string Text
        {
            get
            {
                return ButtonEdit1.Text;
            }
            set
            {
                ButtonEdit1.Text = value;
            }
        }

        /// <summary>
        /// Parse the input string into the suitable DebtPlus.LINQ.TelephoneNumber structure
        /// </summary>
        /// <param name="InputString"></param>
        /// <returns></returns>
        protected DebtPlus.LINQ.TelephoneNumber Parse(string InputString, bool ShouldValidate)
        {
            // Allocate an empty structure to hold the results
            var NewValues = DebtPlus.LINQ.TelephoneNumber.TryParse(InputString);
            if (NewValues == null)
            {
                return null;
            }

            // If we need to validate then validate the entry.
            if (!ShouldValidate || !ValidateForm(ref NewValues))
            {
                return null;
            }

            // All is well. Return the results.
            return NewValues;
        }

        /// <summary>
        /// Handle the resize event logic for the container
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void TelephoneNumberControl_Resize(object sender, EventArgs e) // Handles this.Resize
        {
            ButtonEdit1.Location = new System.Drawing.Point(0, 0);

            // Set the size to the full control
            ButtonEdit1.Size = Size;

            // if the control takes less, reduce the size to match.
            if (Size != ButtonEdit1.Size)
            {
                Size = ButtonEdit1.Size;
            }
        }

        /// <summary>
        /// When the button is pressed, evoke the update form directly.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void ButtonEdit1_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e) // Handles ButtonEdit1.ButtonPressed
        {
            if (!ReadOnly)
            {
                var NewValues = DebtPlus.LINQ.Factory.Manufacture_TelephoneNumber();
                NewValues.Country    = EditValueCountry;
                NewValues.Acode      = EditValueAcode;
                NewValues.Number     = EditValueNumber;
                NewValues.Ext        = EditValueExt;

                if (ValidateForm(ref NewValues))
                {
                    EditValueCountry = NewValues.Country;
                    EditValueAcode   = NewValues.Acode;
                    EditValueNumber  = NewValues.Number;
                    EditValueExt     = NewValues.Ext;

                    ButtonEdit1.Text = NewValues.ToString();
                }
            }
        }

        /// <summary>
        /// Error information for the telephone number
        /// </summary>
        [Browsable(false)]
        public System.Drawing.Image ErrorIcon
        {
            get
            {
                return ButtonEdit1.ErrorIcon;
            }
            set
            {
                ButtonEdit1.ErrorIcon = value;
            }
        }

        [Browsable(false)]
        public string ErrorText
        {
            get
            {
                return ButtonEdit1.ErrorText;
            }
            set
            {
                ButtonEdit1.ErrorText = value;
            }
        }

        #region IXtraResizableControl
        #region Changed
        public event EventHandler Changed; // Implements DevExpress.Utils.Controls.IXtraResizableControl.Changed
        protected void RaiseChanged(EventArgs e)
        {
            EventHandler evt = Changed;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnChanged(EventArgs e)
        {
            RaiseChanged(e);
        }
        #endregion

        public bool IsCaptionVisible // Implements DevExpress.Utils.Controls.IXtraResizableControl.IsCaptionVisible
        {
            get
            {
                return true;
            }
        }

        public System.Drawing.Size MaxSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MaxSize
        {
            get
            {
                return new System.Drawing.Size(0, 20);
            }
        }

        public System.Drawing.Size MinSize // Implements DevExpress.Utils.Controls.IXtraResizableControl.MinSize
        {
            get
            {
                return new System.Drawing.Size(50, 20);
            }
        }
        #endregion

        /// <summary>
        /// return the dialing code for the indicated country
        /// </summary>
        /// <param name="Country">Country ID in the countries table</param>
        /// <value>The corresponding telephone dialing code for that country</value>
        /// <returns></returns>
        /// <remarks>A zero value indicates that the country is not valid</remarks>
        protected Int32 DialingCode(Int32? Country)
        {
            if (Country.HasValue && Country.Value > 0)
            {
                var q = DebtPlus.LINQ.Cache.country.getList().Find(s => s.Id == Country.Value);
                if (q != null)
                {
                    return q.country_code;
                }
            }

            return 0;
        }
    }
}
