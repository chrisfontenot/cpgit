﻿namespace DebtPlus.Data.Controls
{
    public partial class NameParts
    {
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            UnRegisterHandlers();
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.Combo_Prefix = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Text_First = new DevExpress.XtraEditors.TextEdit();
            this.Text_Middle = new DevExpress.XtraEditors.TextEdit();
            this.Text_Last = new DevExpress.XtraEditors.TextEdit();
            this.Combo_Suffix = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)this.Combo_Prefix.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_First.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Middle.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Last.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Combo_Suffix.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Combo_Prefix
            //
            this.Combo_Prefix.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Combo_Prefix.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Combo_Prefix.Location = new System.Drawing.Point(0, 0);
            this.Combo_Prefix.Name = "Combo_Prefix";
            this.Combo_Prefix.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            //
            //Combo_Prefix.Properties
            //
            this.Combo_Prefix.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Combo_Prefix.Properties.MaxLength = 10;
            this.Combo_Prefix.Size = new System.Drawing.Size(64, 20);
            this.Combo_Prefix.TabIndex = 0;
            //
            //Text_First
            //
            this.Text_First.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Text_First.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text_First.Location = new System.Drawing.Point(72, 0);
            this.Text_First.Name = "Text_First";
            //
            //Text_First.Properties
            //
            this.Text_First.Properties.MaxLength = 40;
            this.Text_First.Size = new System.Drawing.Size(104, 20);
            this.Text_First.TabIndex = 1;
            //
            //Text_Middle
            //
            this.Text_Middle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Text_Middle.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text_Middle.Location = new System.Drawing.Point(180, 0);
            this.Text_Middle.Name = "Text_Middle";
            //
            //Text_Middle.Properties
            //
            this.Text_Middle.Properties.MaxLength = 40;
            this.Text_Middle.Size = new System.Drawing.Size(44, 20);
            this.Text_Middle.TabIndex = 2;
            //
            //Text_Last
            //
            this.Text_Last.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Text_Last.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text_Last.Location = new System.Drawing.Point(228, 0);
            this.Text_Last.Name = "Text_Last";
            //
            //Text_Last.Properties
            //
            this.Text_Last.Properties.MaxLength = 40;
            this.Text_Last.Size = new System.Drawing.Size(132, 20);
            this.Text_Last.TabIndex = 3;
            //
            //Combo_Suffix
            //
            this.Combo_Suffix.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Combo_Suffix.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Combo_Suffix.Location = new System.Drawing.Point(364, 0);
            this.Combo_Suffix.Name = "Combo_Suffix";
            //
            //Combo_Suffix.Properties
            //
            this.Combo_Suffix.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Combo_Suffix.Properties.MaxLength = 10;
            this.Combo_Suffix.Size = new System.Drawing.Size(60, 20);
            this.Combo_Suffix.TabIndex = 4;
            //
            //NameParts
            //
            this.Controls.Add(this.Combo_Suffix);
            this.Controls.Add(this.Text_Last);
            this.Controls.Add(this.Text_Middle);
            this.Controls.Add(this.Text_First);
            this.Controls.Add(this.Combo_Prefix);
            this.Name = "NameParts";
            this.Size = new System.Drawing.Size(432, 20);
            ((System.ComponentModel.ISupportInitialize)this.Combo_Prefix.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_First.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Middle.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Last.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Combo_Suffix.Properties).EndInit();
            this.ResumeLayout(false);
        }
        public DevExpress.XtraEditors.ComboBoxEdit Combo_Prefix;
        public DevExpress.XtraEditors.TextEdit Text_First;
        public DevExpress.XtraEditors.TextEdit Text_Middle;
        public DevExpress.XtraEditors.TextEdit Text_Last;
        public DevExpress.XtraEditors.ComboBoxEdit Combo_Suffix;
    }
}