#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using DebtPlus.Events;
using DebtPlus.LINQ;

namespace DebtPlus.Data.Controls
{
    public class EmailRecordControl : EmailControl
    {
        /// <summary>
        /// Edit value for the control. This is the primary key to the EmailAddresses table.
        /// </summary>
        [Browsable(false), Description("Value for database"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), ReadOnly(true)]
        public new Int32? EditValue
        {
            get
            {
                UnRegisterHandlers();
                try
                {
                    return SaveRecord();
                }
                finally
                {
                    RegisterHandlers();
                }
            }

            set
            {
                UnRegisterHandlers();
                try
                {
                    OriginalData = ReadRecord(value);       // Retrieve the values from the database
                    localSetCurrentValues(OriginalData);    // Load them into the UI control
                }
                finally
                {
                    RegisterHandlers();
                }
            }
        }

        /// <summary>
        /// Read the existing record from the database and create the DebtPlus.LINQ.EmailAddress structure from it
        /// </summary>
        /// <param name="Record"></param>
        /// <returns></returns>
        public virtual DebtPlus.LINQ.EmailAddress ReadRecord(Int32? Record)
        {
            DebtPlus.LINQ.EmailAddress result = null;
            if (Record.HasValue && Record.Value > 0)
            {
                using (var dc = new BusinessContext())
                {
                    result = (from e in dc.EmailAddresses where e.Id == Record.Value select e).FirstOrDefault();
                }
            }

            return result ?? DebtPlus.LINQ.Factory.Manufacture_EmailAddress();
        }

        /// <summary>
        /// Save the record to the database
        /// </summary>
        /// <returns></returns>
        protected virtual Int32? SaveRecord()
        {
            // Do not do the save in the design mode
            if (DebtPlus.Data.Controls.UserControl.CtlInDesignMode(this))
            {
                return new Int32?();
            }

            // If the control is not read-only then look to see if we need to save the data
            if (!ReadOnly)
            {
                // Allocate a new structure to hold the new record data
                DebtPlus.LINQ.EmailAddress NewValues = GetCurrentValues();

                // If the record is new and is empty then just return an empty address
                using (var dc = new BusinessContext())
                {
                    if (NewValues.Id <= 0)
                    {
                        // If the new record is empty then don't bother recording the blank address
                        // in the database. Just return the null pointer to the address.
                        if (NewValues.IsEmpty())
                        {
                            return new Int32?();
                        }

                        // Insert the new values into the database and return the pointer value.
                        dc.EmailAddresses.InsertOnSubmit(NewValues);
                    }
                    else
                    {
                        // If the record has not changed then do not bother any more
                        if (NewValues.CompareTo(OriginalData) == 0)
                        {
                            return new Int32?(NewValues.Id);
                        }

                        // Update the record if is found in the database. (It should be present.)
                        var q = (from e in dc.EmailAddresses where e.Id == NewValues.Id select e).FirstOrDefault();
                        if (q != null)
                        {
                            q.Address = NewValues.Address;
                            q.Validation = NewValues.Validation;
                            q.ValidationCode = NewValues.ValidationCode;
                        }
                        else
                        {
                            dc.EmailAddresses.InsertOnSubmit(NewValues);
                        }
                    }

                    // Submit the changes to the database and return the pointer to the new record
                    dc.SubmitChanges();

                    // Tell the user that the value changed
                    RaiseEmailChanged(NewValues, OriginalData);
                    OriginalData = NewValues;
                }
            }

            // Return the pointer to the value. It is either NULL if the current had no ID
            // or the ID from the current data.
            if (OriginalData.Id > 0)
            {
                return new Int32?(OriginalData.Id);
            }
            return new Int32?();
        }
    }
}