#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;

namespace DebtPlus.Data
{
    namespace Controls
    {
        public partial class BlinkLabel : DevExpress.XtraEditors.LabelControl, INotifyPropertyChanged
        {
            private Int32 privateBlinkTime;
            public event PropertyChangedEventHandler PropertyChanged;

            public BlinkLabel()
            {
                InitializeComponent();

                if (!DebtPlus.Data.Controls.UserControl.CtlInDesignMode(this))
                {
                    RegisterHandlers();
                }
            }

            private void RegisterHandlers()
            {
                TimerItem.Tick += TimerItem_Tick;
            }

            protected void RaisePropertyChanged(PropertyChangedEventArgs e)
            {
                PropertyChangedEventHandler evt = PropertyChanged;
                if (evt != null)
                {
                    evt(this, e);
                }
            }

            protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
            {
                RaisePropertyChanged(e);
            }

            /// <summary>
            /// Blink time changed
            /// </summary>
            /// <param name="PropertyName"></param>
            /// <remarks></remarks>
            protected void RaisePropertyChanged(string PropertyName)
            {
                OnPropertyChanged(new PropertyChangedEventArgs(PropertyName));
            }

            /// <summary>
            /// Blink time in milliseconds
            /// </summary>
            /// <value>Number of msec that the label will blink. 0=no blink</value>
            /// <returns></returns>
            /// <remarks></remarks>
            [Description("Number of msec that the label will blink. 0=no blink"), Category("DebtPlus"), Browsable(true), DefaultValue(typeof(Int32), "0")]
            public Int32 BlinkTime
            {
                get
                {
                    return privateBlinkTime;
                }

                set
                {
                    if (value != privateBlinkTime)
                    {
                        privateBlinkTime = value;
                        RaisePropertyChanged("BlinkTime");
                        if (!DebtPlus.Data.Controls.UserControl.CtlInDesignMode(this))
                        {
                            HandleBlinkChanges();
                        }
                    }
                }
            }

            protected override void OnCreateControl()
            {
                base.OnCreateControl();
                Visible = false;
            }

            /// <summary>
            /// When the blink time changes, update the control.
            /// </summary>
            /// <remarks></remarks>
            protected void HandleBlinkChanges()
            {
                if (Enabled && BlinkTime > 0 && Text != string.Empty)
                {
                    Visible = true;
                    TimerItem.Interval = BlinkTime;
                    TimerItem.Enabled = true;
                    TimerItem.Start();
                }
                else
                {
                    TimerItem.Stop();
                    TimerItem.Enabled = false;
                    Visible = false;
                }
            }

            protected override void OnEnabledChanged(EventArgs e)
            {
                base.OnEnabledChanged(e);
                if (!DebtPlus.Data.Controls.UserControl.CtlInDesignMode(this))
                {
                    HandleBlinkChanges();
                }
            }

            protected override void OnTextChanged(EventArgs e)
            {
                base.OnTextChanged(e);
                if (!DebtPlus.Data.Controls.UserControl.CtlInDesignMode(this))
                {
                    HandleBlinkChanges();
                }
            }

            /// <summary>
            /// Timer tick processing routine
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            /// <remarks></remarks>
            private void TimerItem_Tick(object sender, EventArgs e) // Handles TimerItem.Tick
            {
                Visible = !Visible && Enabled && TimerItem.Enabled;
            }
        }
    }
}
