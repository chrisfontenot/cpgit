namespace DebtPlus.Data.Controls
{
    partial class TelephoneUpdateForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_CANCEL = new DevExpress.XtraEditors.SimpleButton();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.TextEdit_Ext = new DevExpress.XtraEditors.TextEdit();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.FormattedNumberLabel = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TextEdit_Number = new DevExpress.XtraEditors.TextEdit();
            this.TextEdit_Acode = new DevExpress.XtraEditors.TextEdit();
            this.LookUpEdit_Country = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Ext.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Acode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Country.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // SimpleButton_OK
            // 
            this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton_OK.Location = new System.Drawing.Point(355, 12);
            this.SimpleButton_OK.Name = "SimpleButton_OK";
            this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton_OK.TabIndex = 1;
            this.SimpleButton_OK.Text = "&OK";
            // 
            // SimpleButton_CANCEL
            // 
            this.SimpleButton_CANCEL.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton_CANCEL.Location = new System.Drawing.Point(355, 50);
            this.SimpleButton_CANCEL.Name = "SimpleButton_CANCEL";
            this.SimpleButton_CANCEL.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton_CANCEL.TabIndex = 2;
            this.SimpleButton_CANCEL.Text = "&Cancel";
            // 
            // GroupControl1
            // 
            this.ToolTipController1.SetAllowHtmlText(this.GroupControl1, DevExpress.Utils.DefaultBoolean.False);
            this.GroupControl1.Controls.Add(this.TextEdit_Ext);
            this.GroupControl1.Controls.Add(this.LabelControl6);
            this.GroupControl1.Controls.Add(this.FormattedNumberLabel);
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Controls.Add(this.TextEdit_Number);
            this.GroupControl1.Controls.Add(this.TextEdit_Acode);
            this.GroupControl1.Controls.Add(this.LookUpEdit_Country);
            this.GroupControl1.Controls.Add(this.LabelControl5);
            this.GroupControl1.Controls.Add(this.LabelControl4);
            this.GroupControl1.Controls.Add(this.LabelControl3);
            this.GroupControl1.Location = new System.Drawing.Point(12, 12);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(337, 156);
            this.GroupControl1.TabIndex = 0;
            this.GroupControl1.Text = "Phone Details";
            // 
            // TextEdit_Ext
            // 
            this.TextEdit_Ext.Location = new System.Drawing.Point(96, 126);
            this.TextEdit_Ext.Name = "TextEdit_Ext";
            this.TextEdit_Ext.Properties.Mask.BeepOnError = true;
            this.TextEdit_Ext.Properties.Mask.EditMask = "\\d*";
            this.TextEdit_Ext.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_Ext.Properties.MaxLength = 10;
            this.TextEdit_Ext.Size = new System.Drawing.Size(228, 20);
            this.TextEdit_Ext.TabIndex = 9;
            // 
            // LabelControl6
            // 
            this.LabelControl6.Location = new System.Drawing.Point(10, 127);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(47, 13);
            this.LabelControl6.TabIndex = 8;
            this.LabelControl6.Text = "Extension";
            // 
            // FormattedNumberLabel
            // 
            this.FormattedNumberLabel.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.FormattedNumberLabel.Location = new System.Drawing.Point(96, 23);
            this.FormattedNumberLabel.Name = "FormattedNumberLabel";
            this.FormattedNumberLabel.Size = new System.Drawing.Size(228, 13);
            this.FormattedNumberLabel.TabIndex = 1;
            this.FormattedNumberLabel.UseMnemonic = false;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(10, 23);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(37, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Number";
            // 
            // TextEdit_Number
            // 
            this.TextEdit_Number.Location = new System.Drawing.Point(96, 100);
            this.TextEdit_Number.Name = "TextEdit_Number";
            this.TextEdit_Number.Properties.Mask.BeepOnError = true;
            this.TextEdit_Number.Properties.Mask.EditMask = "[-A-Z0-9]+";
            this.TextEdit_Number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_Number.Properties.MaxLength = 10;
            this.TextEdit_Number.Size = new System.Drawing.Size(228, 20);
            this.TextEdit_Number.TabIndex = 7;
            // 
            // TextEdit_Acode
            // 
            this.TextEdit_Acode.Location = new System.Drawing.Point(96, 74);
            this.TextEdit_Acode.Name = "TextEdit_Acode";
            this.TextEdit_Acode.Properties.Mask.BeepOnError = true;
            this.TextEdit_Acode.Properties.Mask.EditMask = "\\d*";
            this.TextEdit_Acode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextEdit_Acode.Properties.MaxLength = 10;
            this.TextEdit_Acode.Size = new System.Drawing.Size(228, 20);
            this.TextEdit_Acode.TabIndex = 5;
            // 
            // LookUpEdit_Country
            // 
            this.LookUpEdit_Country.Location = new System.Drawing.Point(96, 46);
            this.LookUpEdit_Country.Name = "LookUpEdit_Country";
            this.LookUpEdit_Country.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_Country.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Country", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("country_code", "Code", 5, DevExpress.Utils.FormatType.Numeric, "000", true, DevExpress.Utils.HorzAlignment.Default)});
            this.LookUpEdit_Country.Properties.DisplayMember = "description";
            this.LookUpEdit_Country.Properties.NullText = "";
            this.LookUpEdit_Country.Properties.ValueMember = "Id";
            this.LookUpEdit_Country.Size = new System.Drawing.Size(228, 20);
            this.LookUpEdit_Country.TabIndex = 3;
            // 
            // LabelControl5
            // 
            this.LabelControl5.Location = new System.Drawing.Point(10, 101);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(64, 13);
            this.LabelControl5.TabIndex = 6;
            this.LabelControl5.Text = "Local Number";
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(10, 77);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(74, 13);
            this.LabelControl4.TabIndex = 4;
            this.LabelControl4.Text = "Area/City Code";
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(10, 48);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(76, 13);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "Country/Region";
            // 
            // TelephoneUpdateForm
            // 
            this.AcceptButton = this.SimpleButton_OK;
            this.ToolTipController1.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.False);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton_CANCEL;
            this.ClientSize = new System.Drawing.Size(440, 181);
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.SimpleButton_CANCEL);
            this.Controls.Add(this.SimpleButton_OK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TelephoneUpdateForm";
            this.Text = "Check Phone Number";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl1)).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Ext.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Acode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Country.Properties)).EndInit();
            this.ResumeLayout(false);

        }
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_CANCEL;
        internal DevExpress.XtraEditors.GroupControl GroupControl1;
        internal DevExpress.XtraEditors.LabelControl FormattedNumberLabel;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.TextEdit TextEdit_Number;
        internal DevExpress.XtraEditors.TextEdit TextEdit_Acode;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_Country;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.TextEdit TextEdit_Ext;
        internal DevExpress.XtraEditors.LabelControl LabelControl6;
    }
}
