#region Copyright 2000-2012 DebtPlus, L.L.C.

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Data;
using System.Data.SqlClient;
using System.ComponentModel;
using DebtPlus.Events;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Data.Controls
{
    public class NameRecordControl : NameParts
    {
        protected bool InInit;
        private DebtPlus.LINQ.Name OriginalData;

        /// <summary>
        /// Dispose of local storage
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            // Release pointers to other structures
            OriginalData = null;

            // Remove handlers
            UnRegisterHandlers();

            // Do the base disposal
            base.Dispose(disposing);
        }

        #region EditValueChanged

        /// <summary>
        /// Event whe nthe edit value is changed
        /// </summary>

        public event EventHandler EditValueChanged;

        protected void RaiseEditValueChanged(EventArgs e)
        {
            EventHandler evt = EditValueChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnEditValueChanged(EventArgs e)
        {
            RaiseEditValueChanged(e);
        }

        #endregion

        #region NameChanged

        /// <summary>
        /// Event when the name is changed
        /// </summary>
        public event NameChangedEventHandler NameChanged;

        /// <summary>
        /// Raise the NAME CHANGE event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseNameChanged(NameChangeEventArgs e)
        {
            NameChangedEventHandler evt = NameChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the NAME CHANGE event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnNameChanged(NameChangeEventArgs e)
        {
            RaiseNameChanged(e);
        }

        /// <summary>
        /// Raise the NAME CHANGE event
        /// </summary>
        /// <param name="OldValues"></param>
        /// <param name="NewValues"></param>
        protected void RaiseNameChanged(DebtPlus.LINQ.Name OldValues, DebtPlus.LINQ.Name NewValues)
        {
            OnNameChanged(new NameChangeEventArgs(OldValues, NewValues));
        }

        #endregion

        /// <summary>
        /// Database row pointer to the current name
        /// </summary>
        [Browsable(false), Description("Value for database"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), ReadOnly(true)]
        public Int32? EditValue
        {
            get
            {
                UnRegisterHandlers();
                try
                {
                    return SaveRecord();
                }
                finally
                {
                    RegisterHandlers();
                }
            }

            set
            {
                UnRegisterHandlers();
                try
                {
                    OriginalData = ReadRecord(value);
                    SetValues(OriginalData);
                }
                finally
                {
                    RegisterHandlers();
                }
            }
        }

        /// <summary>
        /// Read the record from the database
        /// </summary>
        /// <param name="Record">The record ID number in the database</param>
        public virtual DebtPlus.LINQ.Name ReadRecord(Int32? Record)
        {
            DebtPlus.LINQ.Name NewValues = null;
            if (Record.HasValue && Record.Value > 0)
            {
                using (var dc = new BusinessContext())
                {
                    NewValues = (from n in dc.Names where n.Id == Record.Value select n).FirstOrDefault();
                }
            }

            if (NewValues == null)
            {
                NewValues = DebtPlus.LINQ.Factory.Manufacture_Name();
            }

            return NewValues;
        }


        /// <summary>
        /// Retrieve the current values from the controls and load a record structure with the settings.
        /// </summary>
        /// <returns></returns>
        public override DebtPlus.LINQ.Name GetCurrentValues()
        {
            DebtPlus.LINQ.Name NewValues = base.GetCurrentValues();

            // For the time being, use the same Name ID. Do this just to complete the data for the event.
            if (OriginalData != null)
            {
                NewValues.Id = OriginalData.Id;
            }

            return NewValues;
        }

        /// <summary>
        /// Save the record to the database
        /// </summary>
        /// <returns></returns>
        protected virtual Int32? SaveRecord()
        {
            // If the field is read-only then don't change the ID.    
            if (InDesignMode || ReadOnly)
            {
                return OriginalData.Id;
            }

            // Retrieve the values from the name record
            DebtPlus.LINQ.Name NewValues = GetCurrentValues();
            using (var dc = new BusinessContext())
            {
                // If the name is new and empty then just return the empty pointer to the empty name.
                if (NewValues.Id <= 0)
                {
                    if (NewValues.IsEmpty())
                    {
                        return new Int32?();
                    }

                    // Otherwise, we need to insert the new record into the database.
                    dc.Names.InsertOnSubmit(NewValues);
                }
                else
                {
                    // There is a previous value. Compare it. If they are the same then we can quit
                    // early as we don't update the database in this case either.
                    if (NewValues.CompareTo(OriginalData) == 0)
                    {
                        return new Int32?(OriginalData.Id);
                    }

                    // Retrieve the values from the database and update the record
                    var q = (from n in dc.Names where n.Id == OriginalData.Id select n).FirstOrDefault();
                    if (q != null)
                    {
                        q.Prefix = NewValues.Prefix;
                        q.First = NewValues.First;
                        q.Middle = NewValues.Middle;
                        q.Last = NewValues.Last;
                        q.Suffix = NewValues.Suffix;
                    }
                    else
                    {
                        // It was not found. It was somehow deleted. Insert the new name as if it was fresh.
                        dc.Names.InsertOnSubmit(NewValues);
                    }
                }

                // Correct the database
                dc.SubmitChanges();
            }

            // Raise the change event
            RaiseNameChanged(OriginalData, NewValues);
            OriginalData = NewValues;

            // Return the ID of the record
            if (OriginalData.Id > 0)
            {
                return new Int32?(OriginalData.Id);
            }
            return new Int32?();
        }
    }
}