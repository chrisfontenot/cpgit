﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.Data.Controls
{
    public partial class AddressBase : UserControl, ISupportInitialize
    {
        /// <summary>
        /// Original settings for the new address line
        /// </summary>
        protected address OriginalData = new address();
        protected readonly bool IsInDesignMode;
        protected BusinessContext bc = null;
        protected System.Collections.Generic.List<state> colStates = new System.Collections.Generic.List<state>();

        /// <summary>
        /// ID in the cached list
        /// </summary>
        protected string cached_PostalCode = string.Empty;

        /// <summary>
        /// Cached list of zip-code search items
        /// </summary>
        protected System.Collections.Generic.List<ZipCodeSearch> cached_SearchList = new System.Collections.Generic.List<ZipCodeSearch>();

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public AddressBase() : base()
        {
            InitializeComponent();
            UnRegisterHandlers();
            try
            {
                IsInDesignMode = Configuration.DesignMode.IsInDesignMode();
                if (!IsInDesignMode)
                {
                    bc = new DebtPlus.LINQ.BusinessContext();
                    bc.DeferredLoadingEnabled = true;

                    // Load the list of state references
                    colStates = bc.states.ToList();
                    States.Properties.DataSource = colStates;

                    var defaultState = colStates.Find(s => s.Default);
                    if (defaultState != null)
                    {
                        States.EditValue = defaultState.Id;
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error loading address base control", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// True if the logic is between BeginInit and EndInit
        /// </summary>
        protected bool InInit;

        /// <summary>
        /// Support the BeginInit method for ISupportInitialize
        /// </summary>
        public void BeginInit() // Implements ISupportInitialize.BeginInit
        {
            InInit = true;
        }

        /// <summary>
        /// Support the EndInit method for ISupportInitialize
        /// </summary>
        public void EndInit() // Implements ISupportInitialize.EndInit
        {
            InInit = false;
        }

        /// <summary>
        /// Event raised with a property is changed
        /// </summary>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raise the PropertyChanged event from a protected caller
        /// </summary>
        /// <param name="e"></param>
        protected void RaisePropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (! InInit)
            {
                var evt = PropertyChanged;
                if (evt != null)
                {
                    evt(this, e);
                }
            }
        }

        /// <summary>
        /// Allow the protected callers to hook into the event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
        {
            RaisePropertyChanged(e);
        }

        /// <summary>
        /// Helper function to simply pass the property name and generate the event
        /// </summary>
        /// <param name="propertyName"></param>
        protected void RaisePropertyChanged(string propertyName)
        {
            OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Event to indicate that the StateID has been changed
        /// </summary>
        public event EventHandler StateChanged;

        /// <summary>
        /// Raise the StateChanged event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseStateChanged(EventArgs e)
        {
            if (!InInit)
            {
                var evt = StateChanged;
                if (evt != null)
                {
                    evt(this, e);
                }
            }
        }

        /// <summary>
        /// Generate the StateChanged event but allow it to be overridden
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnStateChanged(EventArgs e)
        {
            RaiseStateChanged(e);
        }

        /// <summary>
        /// Event to determine if we should show the popup windows
        /// </summary>
        public event CancelEventHandler QueryPopup;

        /// <summary>
        /// Raise the QueryPopup event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseQueryPopup(CancelEventArgs e)
        {
            if (!InInit)
            {
                var evt = QueryPopup;
                if (evt != null)
                {
                    evt(this, e);
                }
            }
        }

        /// <summary>
        /// Provide a virtual hook to the QueryPopup event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnQueryPopup(CancelEventArgs e)
        {
            RaiseQueryPopup(e);
        }

        /// <summary>
        /// Prevent the pop-up from being activated when we are loading
        /// </summary>
        protected void LookupEdits_QueryPopUp(object sender, CancelEventArgs e)
        {
            // Ask the parent if the pop-up is to be shown.
            var EventArgs = new CancelEventArgs(false);
            OnQueryPopup(EventArgs);
            if (EventArgs.Cancel)
            {
                e.Cancel = true;
            }
            else
            {
                // The pop-ups would all pop-up since the controls are set to auto-find items
                // when the items are being loaded. Prevent the pop-up from engaging until the
                // control is fully loaded.
                e.Cancel = InInit;
            }
        }

        /// <summary>
        /// Name of the current city in the control
        /// </summary>
        [ReadOnly(true), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        protected string CityName
        {
            get
            {
                return Convert.ToString(City.EditValue);
            }
            set
            {
                if (value != CityName)
                {
                    try
                    {
                        City.QueryPopUp += CityEditValue_QueryPopup;
                        City.EditValue   = City.Text = value;
                    }
                    finally
                    {
                        City.QueryPopUp -= CityEditValue_QueryPopup;
                    }
                }
            }
        }

        /// <summary>
        /// Suppress the popup event from occurring
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>This always returns CANCEL status to the caller.</remarks>
        private void CityEditValue_QueryPopup(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        /// <summary>
        /// Spacing between the controls
        /// </summary>
        protected static Int32 ControlSpacing
        {
            get
            {
                return 6;
            }
        }

        private bool privateReadOnly;

        /// <summary>
        /// Prevent the database from being updated
        /// </summary>
        [Description("Is the control READ-ONLY and prevents changes?"), DefaultValue(typeof(bool), "false"), Browsable(true), Category("DebtPlus")]
        public bool ReadOnly
        {
            get
            {
                return privateReadOnly;
            }
            set
            {
                if (privateReadOnly != value)
                {
                    privateReadOnly                = value;

                    // Set the status for the ReadOnly condition correctly
                    City.Properties.ReadOnly       = value;
                    PostalCode.Properties.ReadOnly = value;
                    States.Properties.ReadOnly     = value;

                    // Tell the user that the property was changed
                    OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs("ReadOnly"));
                    OnReadonlyChanged(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Allow inherited classes to hook into the read-only status change event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnReadonlyChanged(EventArgs e)
        {
        }

        /// <summary>
        /// Handle the condition where the enable status changes. Enable or disable the Controls
        /// in our frame.
        /// </summary>
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            // Enable or disable the frame controls
            bool isEnabled     = Enabled;

            City.Enabled       = isEnabled;
            States.Enabled     = isEnabled;
            PostalCode.Enabled = isEnabled;
        }

        /// <summary>
        /// Register the event handlers for this control's controls.
        /// </summary>
        private void RegisterHandlers()
        {
            City.ProcessNewValue     += City_ProcessNewValue;
            City.QueryCloseUp        += City_QueryCloseUp;
            City.EditValueChanged    += City_EditValueChanged;
            City.QueryPopUp          += City_QueryPopUp;
            City.TextChanged         += ControlChanged;
            States.QueryPopUp        += LookupEdits_QueryPopUp;
            States.TextChanged       += ControlChanged;
            States.EditValueChanging += Validation.LookUpEdit_ActiveTest;
            States.EditValueChanged  += States_EditValueChanged;
            PostalCode.Validated     += PostalCode_Validated;
            PostalCode.ButtonClick   += PostalCode_ButtonClick;
            PostalCode.TextChanged   += ControlChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            City.ProcessNewValue     -= City_ProcessNewValue;
            City.QueryCloseUp        -= City_QueryCloseUp;
            City.EditValueChanged    -= City_EditValueChanged;
            City.QueryPopUp          -= City_QueryPopUp;
            City.TextChanged         -= ControlChanged;
            States.QueryPopUp        -= LookupEdits_QueryPopUp;
            States.TextChanged       -= ControlChanged;
            States.EditValueChanging -= Validation.LookUpEdit_ActiveTest;
            States.EditValueChanged  -= States_EditValueChanged;
            PostalCode.Validated     -= PostalCode_Validated;
            PostalCode.ButtonClick   -= PostalCode_ButtonClick;
            PostalCode.TextChanged   -= ControlChanged;
        }

        /// <summary>
        /// Tripped when the text of the control is changed
        /// </summary>
        public new event EventHandler TextChanged;

        /// <summary>
        /// Raise the TextChanged event
        /// </summary>
        protected void RaiseTextChanged(EventArgs e)
        {
            var evt = TextChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Handle the overloaded function for the text change event
        /// </summary>
        /// <param name="e"></param>
        protected new virtual void OnTextChanged(EventArgs e)
        {
            RaiseTextChanged(e);
        }

        /// <summary>
        /// Invoked when the sub-controls are changed. Raise the TextChanged event
        /// </summary>
        protected void ControlChanged(object sender, EventArgs e)
        {
            OnTextChanged(e);
        }

        /// <summary>
        /// Retrieve the state identifier (number)
        /// </summary>
        [ReadOnly(true), Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Int32 StateID
        {
            get
            {
                // If the current row is not valid then return a state of 0 to indicate an invalid state entry.
                if (States.GetSelectedDataRow() == null)
                {
                    return 0;
                }

                // Otherwise, return the value row
                return Convert.ToInt32(States.EditValue);
            }
            set
            {
                // Look for a change in the value. If so, raise the change event.
                if (StateID != value)
                {
                    // Do not allow invalid values to be used. Change them to a legal blank item.
                    if (value < 0)
                    {
                        value = 0;
                    }

                    States.EditValue = value;
                    OnStateChanged(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Retrieve a partial object for the address block. The "line1" field is placed into the street name
        /// location, which would be a suitable location should the address line be all together.
        /// </summary>
        public virtual DebtPlus.LINQ.address GetCurrentValues()
        {
            var adr        = DebtPlus.LINQ.Factory.Manufacture_address(OriginalData.showattn, OriginalData.showCreditor1, OriginalData.showCreditor2, OriginalData.showLine3);
            adr.Id         = OriginalData.Id;
            adr.city       = City.Text.ToUpper().Trim();
            adr.state      = DebtPlus.Utils.Nulls.v_Int32(States.EditValue).GetValueOrDefault(0);
            adr.PostalCode = PostalCode.Text.Trim().ToUpper();

            return adr;
        }

        /// <summary>
        /// Set the address components into the control
        /// </summary>
        /// <param name="NewValues">Pointer to the address record used as the source</param>
        public virtual void SetCurrentValues(address NewValues)
        {
            UnRegisterHandlers();

            try
            {
                // Set the search list to the city collection
                City.Properties.DataSource = new System.Collections.Generic.List<ZipCodeSearch>();
                City.Properties.DataSource = getSearchList(NewValues.PostalCode);

                // If there is no city name then we can not continue. Stop here and load blanks to wait for the postal code entry.
                if (string.IsNullOrWhiteSpace(NewValues.city))
                {
                    City.EditValue       = string.Empty;
                    PostalCode.EditValue = string.Empty;
                    States.EditValue     = 0;
                    OnStateChanged(System.EventArgs.Empty);
                    return;
                }

                // Find the city in the list. If not found, add it.
                var q = cached_SearchList.Find(s => string.Compare(s.CityName, NewValues.city, true) == 0);
                if (q == null)
                {
                    // We have a value but it is not in the list. Add it.
                    q          = DebtPlus.LINQ.Factory.Manufacture_ZipCodeSearch();
                    q.CityName = NewValues.city;
                    q.state1   = colStates.Find(s => s.Id == NewValues.state);
                    q.Id       = -1 - cached_SearchList.Count();

                    cached_SearchList.Add(q);
                    City.Properties.DataSource = new System.Collections.Generic.List<ZipCodeSearch>();
                    City.Properties.DataSource = cached_SearchList;
                }

                // Define the values from the search list now.
                PostalCode.Text  = NewValues.PostalCode;
                City.EditValue   = q.CityName;
                States.EditValue = q.State;
                OnStateChanged(System.EventArgs.Empty);
            }
            finally
            {
                // Correct the error status on the city name
                City_ChangeErrorStatus();
                RegisterHandlers();
            }
        }

        /// <summary>
        /// When the state is changed, pass the event along
        /// </summary>
        protected void States_EditValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                City.EditValue = string.Empty;
                City_ChangeErrorStatus(System.String.Empty);
                OnTextChanged(System.EventArgs.Empty);
                OnStateChanged(System.EventArgs.Empty);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the VALIDATED event for the postal-code. The data has been changed and is now valid.
        /// </summary>
        protected virtual void PostalCode_Validated(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string newPostalCode = PostalCode.Text.Trim();
                ProcessPostalCodeChange(newPostalCode);
            }
            finally
            {
                City_ChangeErrorStatus();
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the condition where someone types a city name that does not currently exist in the list.
        /// Add the new name to the list but mark it invalid.
        /// </summary>
        protected void City_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e) // Handles City.ProcessNewValue
        {
            UnRegisterHandlers();

            // Retrieve the new city name
            string newCity = DebtPlus.Utils.Nulls.DStr(e.DisplayValue).ToUpper();

            try
            {
                // Find the new city. If it is empty then clear the status values
                if (string.IsNullOrEmpty(newCity))
                {
                    cached_PostalCode          = string.Empty;
                    cached_SearchList          = new System.Collections.Generic.List<ZipCodeSearch>();
                    City.Properties.DataSource = cached_SearchList;

                    States.EditValue           = 0;
                    OnStateChanged(System.EventArgs.Empty);
                    return;
                }

                // A search list should be defined.
                if (cached_SearchList == null)
                {
                    cached_SearchList = new System.Collections.Generic.List<ZipCodeSearch>();
                }
                City.Properties.DataSource = new System.Collections.Generic.List<ZipCodeSearch>();

                // Locate the city in the list of cities. If not found, add it.
                var q = cached_SearchList.Find(s => string.Compare(s.CityName, newCity, true) == 0);
                if (q == null)
                {
                    // We have a value but it is not in the list. Add it.
                    q          = DebtPlus.LINQ.Factory.Manufacture_ZipCodeSearch();
                    q.CityName = newCity;
                    q.state1   = States.GetSelectedDataRow() as DebtPlus.LINQ.state;
                    q.Id       = -1 - cached_SearchList.Count();

                    cached_SearchList.Add(q);
                    City.Properties.DataSource = cached_SearchList;
                }
            }
            finally
            {
                City_ChangeErrorStatus(newCity);
                e.Handled = true;
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the CLOSEUP event for the city list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>This sets the city and state from the selected item in the list. Since we only tell when things change, we can't rely upon the change event to update the entry, especially if there is only one item.</remarks>
        protected void City_QueryCloseUp(object sender, CancelEventArgs e) // Handles City.QueryCloseUp
        {
            UnRegisterHandlers();
            try
            {
                City_ChangeErrorStatus();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change event for the city. Update the state entry.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void City_EditValueChanged(object sender, EventArgs e) // Handles CityNameChanged
        {
            UnRegisterHandlers();
            try
            {
                City_ChangeErrorStatus();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the change in the city. Set the error conditions and state from the input record.
        /// </summary>
        /// <remarks></remarks>
        protected virtual void City_ChangeErrorStatus()
        {
            var newCityName = City.Text.Trim();
            City_ChangeErrorStatus(newCityName);
        }

        /// <summary>
        /// Process the change in the city. Set the error conditions and state from the input record.
        /// </summary>
        /// <remarks></remarks>
        protected virtual void City_ChangeErrorStatus(string newCityName)
        {
            // Find the city name in the list. If found, it can not be "N" status.
            var q = cached_SearchList.Find(s => string.Compare(s.CityName, newCityName, true) == 0);
            if (q != null && q.CityType != 'N')
            {
                City.ErrorText = string.Empty;
            }
            else
            {
                City.ErrorText = "The city is not acceptable for this State and ZIP Code";
            }
            // City.Refresh();
        }

        /// <summary>
        /// Cancel the popup event for the city name change. We don't need it.
        /// </summary>
        protected void City_CancelPopUp(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
        }

        /// <summary>
        /// Handle the BEFORE POPUP event for the city. Widen the popup to have more space since we have additional columns.
        /// </summary>
        protected void City_QueryPopUp(object sender, CancelEventArgs e) // Handles City.QueryPopUp
        {
            // Ask the user if the popup is to be displayed. If not then don't
            OnQueryPopup(e);
            if (e.Cancel)
            {
                return;
            }

            UnRegisterHandlers();
            try
            {
                City.Properties.PopupWidth = City.Width + 140;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a click on the Postal Code's button to search for a zip-code based upon the city and state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PostalCode_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e) // Handles PostalCode.ButtonClick
        {
            UnRegisterHandlers();
            try
            {
                using (var frm = new ZipcodeSearchForm())
                {
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        // Replace the zip-code with the value from the form
                        PostalCode.Text = frm.ZipCode;
                        ProcessPostalCodeChange(PostalCode.Text);
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the postal (zip-code) field.
        /// </summary>
        /// <param name="NewPostalCode">Value for the new postalcode</param>
        private void ProcessPostalCodeChange(string NewPostalCode)
        {
            // Set the search list to the city collection
            City.Properties.DataSource = new System.Collections.Generic.List<ZipCodeSearch>();
            City.Properties.DataSource = getSearchList(NewPostalCode);

            // If there is no city name then we can not continue. Stop here and load blanks to wait for the postal code entry.
            string newCity = City.Text.Trim();
            if (string.IsNullOrWhiteSpace(newCity))
            {
                var q = cached_SearchList.Find(s => s.CityType == 'D');
                if (q == null)
                {
                    q = cached_SearchList.Find(s => s.CityType == 'A');
                }
                if (q != null)
                {
                    newCity = q.CityName;
                }
            }

            // Set the state and the city fields
            var qCity = cached_SearchList.Find(s => string.Compare(s.CityName, newCity, true) == 0);
            if (qCity != null)
            {
                City.QueryPopUp += City_CancelPopUp;     // Disable the popup event
                try
                {
                    States.EditValue = qCity.State;
                    OnStateChanged(System.EventArgs.Empty);

                    City.Text = qCity.CityName.ToUpper();
                    OnTextChanged(System.EventArgs.Empty);
                    City_ChangeErrorStatus();
                }
                finally
                {
                    City.QueryPopUp -= City_CancelPopUp;    // Remove the event handler
                }
                return;
            }

            // If there is a city name then add the invalid item to the list.
            if (! string.IsNullOrWhiteSpace(newCity))
            {
                qCity          = DebtPlus.LINQ.Factory.Manufacture_ZipCodeSearch();
                qCity.Id       = -1 - cached_SearchList.Count();
                qCity.CityName = newCity;
                Int32 newState = DebtPlus.Utils.Nulls.v_Int32(States.EditValue).GetValueOrDefault();
                qCity.state1   = colStates.Find(s => s.Id == newState);

                cached_SearchList.Add(qCity);

                City.EditValue = newCity;
                OnTextChanged(System.EventArgs.Empty);
            }
            else
            {
                City.EditValue = string.Empty;
                OnTextChanged(System.EventArgs.Empty);
            }

            City.Properties.DataSource = cached_SearchList;
            City_ChangeErrorStatus();
        }

        /// <summary>
        /// Get a copy of the cached zipcodesearch table entries. We only do unique searches.
        /// </summary>
        /// <returns></returns>
        protected System.Collections.Generic.List<ZipCodeSearch> getSearchList()
        {
            return cached_SearchList;
        }

        /// <summary>
        /// Get a copy of the cached zipcodesearch table entries. We only do unique searches.
        /// </summary>
        /// <param name="PostalCode">Zip-code for the search request</param>
        /// <returns></returns>
        protected System.Collections.Generic.List<ZipCodeSearch> getSearchList(string PostalCode)
        {
            // Get the zip-code for the search operation.
            var rx = new System.Text.RegularExpressions.Regex(@"(\d{5})");
            var m = rx.Match(PostalCode ?? string.Empty);
            if (m.Success)
            {
                var newZipcode = m.Groups[1].Captures[0].Value;
                if (newZipcode != cached_PostalCode)
                {
                    cached_PostalCode = newZipcode;
                    cached_SearchList = bc.ZipCodeSearches.Where(s => s.ZIPCode == newZipcode).ToList();
                }
                return cached_SearchList;
            }

            // Empty the list on invalid entries
            cached_PostalCode = string.Empty;
            cached_SearchList = new System.Collections.Generic.List<ZipCodeSearch>();
            return cached_SearchList;
        }
    }
}
