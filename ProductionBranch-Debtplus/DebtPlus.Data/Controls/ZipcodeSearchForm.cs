#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.Data.Controls
{
    internal partial class ZipcodeSearchForm : Forms.DebtPlusForm
    {
        private System.Collections.Generic.List<ZipCodeSearch> colRecords;

        /// <summary>
        /// Initialize the new class
        /// </summary>
        internal ZipcodeSearchForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += Form_Load;
            LookUpEdit_State.EditValueChanged += LookUpEdit_State_EditValueChanged;
            LookUpEdit_State.EditValueChanging += Validation.LookUpEdit_ActiveTest;
            LookupEdit_City.EditValueChanging += LookupEdit_City_EditValueChanging;
            LookupEdit_City.EditValueChanging += Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Remove the event registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= Form_Load;
            LookUpEdit_State.EditValueChanged -= LookUpEdit_State_EditValueChanged;
            LookUpEdit_State.EditValueChanging -= Validation.LookUpEdit_ActiveTest;
            LookupEdit_City.EditValueChanging -= LookupEdit_City_EditValueChanging;
            LookupEdit_City.EditValueChanging -= Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_Load(object sender, EventArgs e)
        {
            // Load the list of possible states into the control. The city information will have to wait.
            LookUpEdit_State.Properties.DataSource = DebtPlus.LINQ.Cache.state.getList();
            LookupEdit_City.Enabled = false;
            SimpleButton_OK.Enabled = false;
        }

        /// <summary>
        /// Handle the change in the state value. Reload the city list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LookUpEdit_State_EditValueChanged(object sender, EventArgs e)
        {
            // Get the state. Enable the list of possible cities into the city control
            if (LookUpEdit_State.EditValue != null)
            {
                Int32 State = Convert.ToInt32(LookUpEdit_State.EditValue);
                if (State > 0)
                {
                    System.Windows.Forms.Cursor currentCursor = System.Windows.Forms.Cursor.Current;
                    try
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        using (var dc = new BusinessContext())
                        {
                            colRecords = (from z in dc.ZipCodeSearches where z.State.HasValue && z.State.Value == State && z.ActiveFlag select z).ToList();
                            LookupEdit_City.Properties.DataSource = colRecords.OrderBy(s => CustomOrdering(s)).ToList();
                            LookupEdit_City.Enabled = colRecords.Count() > 0;
                        }
                    }
                    finally
                    {
                        System.Windows.Forms.Cursor.Current = currentCursor;
                    }
                    return;
                }
            }

            // Clear the data source for the lookup until the StateID is proper
            LookupEdit_City.Properties.DataSource = null;
            LookupEdit_City.Enabled = false;
        }

        private String CustomOrdering(DebtPlus.LINQ.ZipCodeSearch s)
        {
            switch (s.CityType)
            {
                case 'D':
                    return s.CityName + "*1";
                case 'A':
                    return s.CityName + "*2";
                case 'N':
                    return s.CityName + "*3";
            }
            return s.CityName + "*9";
        }

        /// <summary>
        /// Process a change in the city list. Enable the OK button if there is a choice.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LookupEdit_City_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            SimpleButton_OK.Enabled = (e.NewValue != null);
        }

        /// <summary>
        /// Retrieve the selected ZIPCode from the indicated list.
        /// </summary>
        internal string ZipCode
        {
            get
            {
                return DebtPlus.Utils.Nulls.v_String(LookupEdit_City.EditValue);
            }
        }

        /// <summary>
        /// Handle the dispose event for the form.
        /// </summary>
        /// <param name="disposing"></param>
        private void ZipcodeSearchForm_Dispose(bool disposing)
        {
            UnRegisterHandlers();
        }
    }
}
