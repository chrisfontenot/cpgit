#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DebtPlus.Events;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.Data.Controls
{
    public partial class EmailControl : DevExpress.XtraEditors.ComboBoxEdit
    {
        public event OpenLinkEventHandler SendEmailMessage;

        // Original values for the data
        protected DebtPlus.LINQ.EmailAddress OriginalData;

        /// <summary>
        /// Discard any allocated temporaries
        /// </summary>
        /// <param name="disposing"></param>
        private void EmailControl_Dispose(bool disposing)
        {
            // Remove pointers to other objects
            OriginalData = null;

            // Remove the event handlers
            UnRegisterHandlers();
        }

        /// <summary>
        /// Event tripped when the Email address is changed
        /// </summary>
        public event DebtPlus.Events.EmailChangedEventHandler EmailChanged;

        /// <summary>
        /// Raise the EMAIL CHAINGE event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseEmailChanged(DebtPlus.Events.EmailChangedEventArgs e)
        {
            DebtPlus.Events.EmailChangedEventHandler evt = EmailChanged;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the EMAIL CHANGE event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnEmailChanged(EmailChangedEventArgs e)
        {
            RaiseEmailChanged(e);
        }

        /// <summary>
        /// Raise the EMAIL CHANGE Event
        /// </summary>
        /// <param name="NewValues"></param>
        /// <param name="OldValues"></param>
        protected void RaiseEmailChanged(DebtPlus.LINQ.EmailAddress NewValues, DebtPlus.LINQ.EmailAddress OldValues)
        {
            OnEmailChanged(new EmailChangedEventArgs(OldValues, NewValues));
        }

        /// <summary>
        /// Send the request to generate the email message
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseSendEmailMessage(OpenLinkEventArgs e)
        {
            OpenLinkEventHandler evt = SendEmailMessage;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Hook into the Send Email message routine if needed
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSendEmailMessage(OpenLinkEventArgs e)
        {
            RaiseSendEmailMessage(e);
        }

        /// <summary>
        /// Create an instance of the class
        /// </summary>
        public EmailControl()
        {
            InitializeComponent();
            RegisterHandlers();

            // Allocate an empty original data should the value be
            // needed before the item is set
            OriginalData = new DebtPlus.LINQ.EmailAddress();

            // Use the default font
            fProperties.Appearance.Options.UseFont = true;
            fProperties.Appearance.Font = DefaultFont;
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        protected virtual void RegisterHandlers()
        {
        }

        /// <summary>
        /// Remove the event handling
        /// </summary>
        protected virtual void UnRegisterHandlers()
        {
        }

        /// <summary>
        /// Edit value for the Email address
        /// </summary>
        [Browsable(false), Description("Value for database"), DefaultValue(typeof(object), "null"), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new object EditValue
        {
            get
            {
                UnRegisterHandlers();
                try
                {
                    DebtPlus.LINQ.EmailAddress NewValues = GetCurrentValues();
                    if (NewValues.CompareTo(OriginalData) != 0)
                    {
                        RaiseEmailChanged(NewValues, OriginalData);
                    }
                    return Text.Trim();
                }
                finally
                {
                    RegisterHandlers();
                }
            }
            set
            {
                UnRegisterHandlers();
                try
                {
                    // Put the text string into the DebtPlus.LINQ.EmailAddress field.
                    SelectedIndex = -1;
                    Text = DebtPlus.Utils.Nulls.DStr(value, string.Empty).Trim();

                    // Retrieve the proper parameter structure from current display
                    OriginalData = GetCurrentValues();

                    // Then set the structure back into the display
                    localSetCurrentValues(OriginalData);
                }
                finally
                {
                    RegisterHandlers();
                }
            }
        }

        /// <summary>
        /// Retrieve the current values from the displayed item into a new data strucutre
        /// </summary>
        /// <returns></returns>
        public virtual DebtPlus.LINQ.EmailAddress GetCurrentValues()
        {
            return GetCurrentValues(this);
        }

        /// <summary>
        /// Retrieve the current values from the displayed item into a new data strucutre
        /// </summary>
        /// <param name="ctl"></param>
        /// <returns></returns>
        public virtual DebtPlus.LINQ.EmailAddress GetCurrentValues(EmailControl ctl)
        {
            var NewValues = DebtPlus.LINQ.Factory.Manufacture_EmailAddress();

            /* Retrieve the current item. If the text is blank then assume that the item is "Missing".*/
            NewValues.Address = ctl.Text.Trim();
            NewValues.ValidationCode = DebtPlus.LINQ.EmailAddress.EmailValidationEnum.Valid;

            if (string.IsNullOrWhiteSpace(NewValues.Address))
            {
                NewValues.ValidationCode = DebtPlus.LINQ.EmailAddress.EmailValidationEnum.Missing;
            }
            else
            {
                // Try to find the validation level from the selected item.
                DebtPlus.Data.Controls.ComboboxItem itm = ctl.SelectedItem as DebtPlus.Data.Controls.ComboboxItem;
                if (itm == null)
                {
                    // If it is not in the list then assume that it is valid until proven otherwise.
                    NewValues.ValidationCode = DebtPlus.LINQ.EmailAddress.EmailValidationEnum.Valid;
                }
                else
                {
                    NewValues.ValidationCode = (DebtPlus.LINQ.EmailAddress.EmailValidationEnum)Enum.ToObject(typeof(DebtPlus.LINQ.EmailAddress.EmailValidationEnum), (Int32)itm.value);
                }
            }

            // Use the same key for the new object as the one currently defined.
            if (OriginalData != null)
            {
                NewValues.Id = OriginalData.Id;
            }

            return NewValues;
        }

        /// <summary>
        /// Load the parameter argument structure into the display control
        /// </summary>
        /// <param name="NewValues"></param>
        public void SetCurrentValues(DebtPlus.LINQ.EmailAddress NewValues)
        {
            UnRegisterHandlers();
            try
            {
                localSetCurrentValues(NewValues);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Load the parameter argument structure into the display control
        /// </summary>
        /// <param name="NewValues"></param>
        protected void localSetCurrentValues(DebtPlus.LINQ.EmailAddress NewValues)
        {
            Int32 index = DebtPlus.Data.Controls.ComboBoxUtil.FindString(this, NewValues.ToString());
            SelectedIndex = index;
            if (index < 0)
            {
                Text = NewValues.Address;
            }
        }

        /// <summary>
        /// Is the control Read-Only and denying updates?
        /// </summary>
        [Description("Is the control READ-ONLY and prevents changes?"), DefaultValue(typeof(bool), "false"), Browsable(true), Category("DebtPlus")]
        public bool ReadOnly
        {
            get
            {
                return Properties.ReadOnly;
            }
            set
            {
                Properties.ReadOnly = value;
            }
        }

        /// <summary>
        /// This is the default font for the control.
        /// </summary>
        public static new Font DefaultFont
        {
            get
            {
                return new Font("Tahoma", 8.25F, FontStyle.Underline);
            }
        }

        /// <summary>
        /// Use this color when the item has not been clicked
        /// </summary>
        public static Color DefaultUnclickedColor
        {
            get
            {
                return Color.Blue;
            }
        }

        /// <summary>
        /// Use this color when the link has been selected
        /// </summary>
        public static Color DefaultClickedColor
        {
            get
            {
                return Color.Maroon;
            }
        }

        /// <summary>
        /// This is the default foreground color
        /// </summary>
        public static new Color DefaultForeColor
        {
            get
            {
                return DefaultUnclickedColor;
            }
        }

        private Color privateClickedColor = DefaultClickedColor;
        [System.ComponentModel.Category("Appearance"), System.ComponentModel.Description("Color used when the address has been clicked")]
        public Color ClickedColor
        {
            get
            {
                return privateClickedColor;
            }
            set
            {
                if (value != privateClickedColor)
                {
                    privateClickedColor = value;
                    OnPropertiesChanged();
                }
            }
        }

        /// <summary>
        /// Process the creation of the control.
        /// </summary>
        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            Font = DefaultFont;
            ForeColor = DefaultForeColor;
            ClickedColor = DefaultClickedColor;
        }

        /// <summary>
        /// Handle the DOUBLE-CLICK event on the control
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);

            // Do not attempt to send email to one of the invalid reasons
            if (SelectedIndex >= 0)
            {
                return;
            }

            // Go to the email address if indicated
            string EmailAddress = Text.Trim();

            // if there is an email address then generate the email message program request
            if (EmailAddress != string.Empty)
            {
                OpenLinkEventArgs EventArgs = new OpenLinkEventArgs(Text.Trim()) { Handled = false };
                OnSendEmailMessage(EventArgs);

                // If the event was not handled then evoke the mailer to send the email message directly.
                if (!EventArgs.Handled)
                {
                    System.Diagnostics.Process.Start("mailto:" + EmailAddress);
                }

                ForeColor = ClickedColor;
            }
        }

        /// <summary>
        /// Process the LOAD event on the control. Load the list of items for the combobx.
        /// </summary>
        protected override void OnLoaded()
        {
            base.OnLoaded();

            // Add the additional items to the list of reserved types
            Properties.Items.Clear();
            if (!DebtPlus.Data.Controls.UserControl.CtlInDesignMode(this))
            {
                Properties.Items.Add(new ComboboxItem(DebtPlus.LINQ.EmailAddress.cREFUSED, DebtPlus.LINQ.EmailAddress.EmailValidationEnum.Refused));
                Properties.Items.Add(new ComboboxItem(DebtPlus.LINQ.EmailAddress.cNONE, DebtPlus.LINQ.EmailAddress.EmailValidationEnum.None));
                Properties.Items.Add(new ComboboxItem(DebtPlus.LINQ.EmailAddress.cMISSING, DebtPlus.LINQ.EmailAddress.EmailValidationEnum.Missing));

                SelectedIndex = -1;
            }
        }

        #region  drag/drop
        /// <summary>
        /// Process the mouse move operation on the control to support drag/drop.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit ctl = (DevExpress.XtraEditors.TextEdit)this;
            base.OnMouseMove(e);

            // If there is no button down then we don't care about movements.
            if (e.Button == 0)
            {
                return;
            }

            // If the point of the click is not in our rectangle then leave.
            if (!ctl.ClientRectangle.Contains(e.X, e.Y))
            {
                return;
            }

            string txt;

            // if this is a text box then look for the selected text component of the text field
            if (ctl.SelectionLength > 0)
            {
                txt = SelectedText;
            }
            else
            {
                txt = string.Empty;
            }

            // if the text is missing then try the entire text field
            if (string.IsNullOrWhiteSpace(txt))
            {
                txt = Text.Trim();
            }

            // if there is no text then do nothing
            if (string.IsNullOrWhiteSpace(txt))
            {
                return;
            }

            // Start a drag-drop operation when the mouse moves outside the control
            var dobj = new DataObject();
            dobj.SetData(DataFormats.Text, true, txt);

            // Do the operation. It will return when the operation is complete.
            DoDragDrop(dobj, DragDropEffects.Copy);
        }

        /// <summary>
        /// Process the DRAG ENTER event for the control to support drag/drop.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);

            // if there is a text field then look to determine the accepable processing
            if (e.Data.GetDataPresent(DataFormats.Text, true))
            {
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Process the drag/drop interface for the control.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDragDrop(DragEventArgs e)
        {
            base.OnDragDrop(e);

            // if there is a text field then look to determine the accepable processing
            if (e.Data.GetDataPresent(DataFormats.Text, true))
            {

                // We only support copy. No modifier needs to be tested
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;

                // Paste the text into the control
                EditValue = Convert.ToInt32(e.Data.GetData(DataFormats.Text, true));
            }
        }

        /// <summary>
        /// Determine the drag/drop status for the control. If there is text, we support the drop.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnQueryContinueDrag(QueryContinueDragEventArgs e)
        {
            // if the escape key is pressed then cancel the operation
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
            }
            else
            {
                base.OnQueryContinueDrag(e);
            }
        }
        #endregion

        /// <summary>
        /// Hook into the validation event logic for the control.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnValidating(CancelEventArgs e)
        {
            base.OnValidating(e);
            ValidateEmailAddress();
        }

        /// <summary>
        /// Validate the EMAIL address as it was entered.
        /// </summary>
        protected void ValidateEmailAddress()
        {
            ErrorText = string.Empty;
            if (SelectedIndex == -1)
            {
                string Address = Text.Trim();
                if (Address != string.Empty)
                {
                    // Use the standard EMAIL validation routine.
                    DebtPlus.EmailValidation.IsEMail ValidationClass = new DebtPlus.EmailValidation.IsEMail();
                    if (!ValidationClass.IsEmailValid(Address) && ValidationClass.ResultInfo.Count > 0)
                    {
                        ErrorText = ValidationClass.ResultInfo[0];
                    }
                }
            }
        }
    }
}