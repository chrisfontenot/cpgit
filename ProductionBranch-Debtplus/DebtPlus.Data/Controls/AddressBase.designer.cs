﻿namespace DebtPlus.Data.Controls
{
    partial class AddressBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                colStates = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.States = new DevExpress.XtraEditors.LookUpEdit();
            this.City = new DevExpress.XtraEditors.LookUpEdit();
            this.PostalCode = new DevExpress.XtraEditors.ButtonEdit();
            ((System.ComponentModel.ISupportInitialize)(this.States.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.City.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostalCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // States
            // 
            this.States.Location = new System.Drawing.Point(295, 5);
            this.States.Name = "States";
            this.States.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.States.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.States.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MailingCode", "State", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountryName", "Country", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.States.Properties.DisplayMember = "MailingCode";
            this.States.Properties.NullText = "";
            this.States.Properties.PopupWidth = 400;
            this.States.Properties.ValueMember = "Id";
            this.States.Size = new System.Drawing.Size(64, 20);
            this.States.TabIndex = 5;
            this.States.ToolTip = "This is the state or region where the address is located.";
            // 
            // City
            // 
            this.City.Location = new System.Drawing.Point(3, 5);
            this.City.Name = "City";
            this.City.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.City.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.City.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CityName", "City", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Formatted_StateName", 10, "State"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Formatted_ZIPType", 15, "ZIP-code Type"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Formatted_CityType", 15, "Location Type"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ActiveFlag", "ActiveFlag", 15, DevExpress.Utils.FormatType.Numeric, "\\Y\\e\\s;\\Y\\e\\s;\\N\\o", true, DevExpress.Utils.HorzAlignment.Default)});
            this.City.Properties.DisplayMember = "CityName";
            this.City.Properties.MaxLength = 60;
            this.City.Properties.NullText = "";
            this.City.Properties.ShowFooter = false;
            this.City.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.City.Properties.SearchMode = DevExpress.XtraEditors.Controls.SearchMode.OnlyInPopup;
            this.City.Properties.ValidateOnEnterKey = true;
            this.City.Properties.ValueMember = "CityName";
            this.City.Size = new System.Drawing.Size(286, 20);
            this.City.TabIndex = 4;
            this.City.ToolTip = "This is the city or principality where the address is located";
            // 
            // PostalCode
            // 
            this.PostalCode.Location = new System.Drawing.Point(365, 5);
            this.PostalCode.Name = "PostalCode";
            this.PostalCode.Properties.Appearance.Options.UseTextOptions = true;
            this.PostalCode.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.PostalCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "Search...", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to look up zip-code by city and state", null, null, true)});
            this.PostalCode.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.PostalCode.Properties.MaxLength = 50;
            this.PostalCode.Size = new System.Drawing.Size(112, 20);
            this.PostalCode.TabIndex = 3;
            this.PostalCode.ToolTip = "This is the postal code for the address. In the U.S., it is called a \"Zip Code\"";
            // 
            // AddressBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.States);
            this.Controls.Add(this.City);
            this.Controls.Add(this.PostalCode);
            this.Name = "AddressBase";
            this.Size = new System.Drawing.Size(480, 72);
            ((System.ComponentModel.ISupportInitialize)(this.States.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.City.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostalCode.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        public DevExpress.XtraEditors.LookUpEdit States;
        public DevExpress.XtraEditors.LookUpEdit City;
        public DevExpress.XtraEditors.ButtonEdit PostalCode;
    }
}
