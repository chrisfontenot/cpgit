#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Diagnostics;

namespace DebtPlus.Data
{
    public static class Validation
    {
        /// <summary>
        /// Validate the entry to ensure that the value entered is at least 0 and not negative. We don't check
        /// the upper range.
        /// </summary>
        public static void NonNegative(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Ignore a missing item. If the cancel is already set then just leave it alone.
            var ctl = sender as DevExpress.XtraEditors.BaseEdit;
            if (ctl == null || e.Cancel)
            {
                return;
            }

            // Find the edit value from the current control setting.
            var EditValue = ctl.EditValue;
            if (EditValue == null)
            {
                return;
            }

            // If the value is a double then check the double range
            if (EditValue is double && Convert.ToDouble(EditValue) < 0.0)
            {
                e.Cancel = true;
            }
            else if (EditValue is decimal && Convert.ToDecimal(EditValue) < 0M)
            {
                e.Cancel = true;
            }

            // Set the error text if indicated
            ctl.ErrorText = e.Cancel ? "Minimum values is 0" : null;
        }

        /// <summary>
        /// Validate the lookup entry to ensure that the items is marked "Active"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        public static void LookUpEdit_ActiveTest(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            // Find the control. We care only about lookup controls here.
            var ctl = sender as DevExpress.XtraEditors.LookUpEdit;
            if (ctl == null)
            {
                return;
            }

            // if the new value is defined then make sure that the office is still active.
            if (e.NewValue == null || System.Object.Equals(System.DBNull.Value, e.NewValue))
            {
                ctl.ErrorText = null;
                return;
            }

            // Find the row for the selected item
            object row = ctl.Properties.GetDataSourceRowByKeyValue(e.NewValue);
            if (row == null)
            {
                ctl.ErrorText = null;
                return;
            }

            string ErrorMessage = string.Empty;
            bool ActiveFlag = true;

            // if the type is a system.data.datarowview then find the corresponding row item
            if (row is System.Data.DataRowView)
            {
                row = ((System.Data.DataRowView)row).Row;
            }

            // if the type is a row, find the column for the active flag
            if (row is System.Data.DataRow)
            {
                System.Data.DataTable tbl = ((System.Data.DataRow)row).Table;
                System.Data.DataColumn col = tbl.Columns["ActiveFlag"];

                // if there is an active column then look for the value
                if (col != null)
                {
                    ActiveFlag = DebtPlus.Utils.Nulls.DBool(((System.Data.DataRow)row)[col]);
                }
            }
            else
            {
                // The item is a class list. Use reflection to find the ActiveFlag member.
                ActiveFlag = true;
                try
                {
                    Type t = row.GetType();

                    // Since we filter the property based upon the return type, we don't need to use "Convert..."
                    var pi = t.GetProperty("ActiveFlag", typeof(Boolean));
                    if (pi != null)
                    {
                        // Just do the "it is a Boolean, believe me!" logic here.
                        ActiveFlag = (Boolean) pi.GetValue(row, null);
                    }
                }
                catch { }
            }

            if (!ActiveFlag)
            {
                ErrorMessage = "INACTIVE item";
                e.Cancel = true;
            }

            // Set the error text
            ctl.ErrorText = ErrorMessage;
        }

        /// <summary>
        /// Validate the lookup entry to ensure that the items is marked "Active"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        public static void Combobox_ActiveTest(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            // Find the control. We care only about lookup controls here.
            var ctl = sender as DevExpress.XtraEditors.ComboBoxEdit;
            if (ctl == null)
            {
                return;
            }

            DebtPlus.Data.Controls.ComboboxItem NewItem = e.NewValue as DebtPlus.Data.Controls.ComboboxItem;
            if (NewItem != null && !System.Object.Equals(NewItem, System.DBNull.Value))
            {
                if (!NewItem.active)
                {
                    ctl.ErrorText = "INACTIVE item";
                    e.Cancel = true;
                    return;
                }
            }

            ctl.ErrorText = null;
            return;
        }
    }
}