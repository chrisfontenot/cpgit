#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

namespace DebtPlus.Data
{
    internal static class DevExpressTags
    {

        // These type references are to items placed into the global assembly cache. As such, they are locked to the version and modification level.
        // Change these as needed to load the modules.

        // Alternately, you can load the reference in the program. These strings are only used if the modules are not presently loaded. So, either load them
        // in the application or make these have the proper values.

        // The version will be used for arguments 0 - 3. It comes from the version of DevExpress.Utils. and should be the current version for the package.
        internal const string BONUS_SKINS_TAG = "DevExpress.BonusSkins.v{0}.{1}, Version={0}.{1}.{2}.{3}, Culture=neutral, PublicKeyToken=95fc6c5621878f0a";
        internal const string OFFICE_SKINS_TAG = "DevExpress.OfficeSkins.v{0}.{1}, Version={0}.{1}.{2}.{3}, Culture=neutral, PublicKeyToken=95fc6c5621878f0a";
    }
}
