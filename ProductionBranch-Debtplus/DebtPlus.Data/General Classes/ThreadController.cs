﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Data
{
    /// <summary>
    /// Controller to manage our threads.
    /// </summary>
    public class ThreadController : System.Collections.Generic.List<System.Threading.Thread>, System.IDisposable
    {
        /// <summary>
        /// Terminate all of the pending threads in the collection
        /// </summary>
        public void KillAll()
        {
#if false   // Do not attempt to kill the threads. It causes threading problems later.
            // Kill all of the threads in the list.
            foreach (System.Threading.Thread thrd in this)
            {
                // Don't do something stupid such as killing our own thread.
                if (thrd != System.Threading.Thread.CurrentThread)
                {
                    try
                    {
                        // Do an abort operation on the threads
                        thrd.Abort();
                    }
                    catch { }
                }
            }
#endif
            // Toss all of the items in the list since we just emptied it.
            this.Clear();
        }

        /// <summary>
        /// Create a new thread and add it to the collection of managed threads
        /// </summary>
        /// <param name="start">Starting parameter for the thread</param>
        /// <returns></returns>
        public System.Threading.Thread Create(System.Threading.ThreadStart start)
        {
            System.Threading.Thread thrd = new System.Threading.Thread(start);
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            Add(thrd);
            return thrd;
        }

        /// <summary>
        /// Create a new thread and add it to the collection of managed threads
        /// </summary>
        /// <param name="start">Starting parameter for the thread</param>
        /// <returns></returns>
        public System.Threading.Thread Create(System.Threading.ParameterizedThreadStart start)
        {
            System.Threading.Thread thrd = new System.Threading.Thread(start);
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            Add(thrd);
            return thrd;
        }

        /// <summary>
        /// Dispose of the local storage
        /// </summary>
        public void Dispose()
        {
            KillAll();
            GC.SuppressFinalize(this);
        }
    }
}
