#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.Data
{
    /// <summary>
    /// Static class for comparison of items
    /// </summary>
    public static class Comparison
    {
        /// <summary>
        /// Compare two objects for equality
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <returns></returns>
        public static Int32 Compare(object obj1, object obj2)
        {
            return Compare(obj1, obj2, true);
        }

        /// <summary>
        /// Compare two strings for equality
        /// </summary>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <param name="IgnoreCase"></param>
        /// <returns></returns>
        public static Int32 Compare(object obj1, object obj2, bool IgnoreCase)
        {
            // if both are missing then they are equal
            if ((obj1 == null || obj1 == DBNull.Value) && (obj2 == null || obj2 == DBNull.Value))
            {
                return 0;
            }

            // if the first object is missing, it is less
            if (obj1 == null || obj1 == DBNull.Value)
            {
                return -1;
            }

            // if the second object is missing, it is greater
            if (obj2 == null || obj2 == DBNull.Value)
            {
                return 1;
            }

            // Do comparisons on booleans
            if (obj1 is bool)
            {
                bool bool1 = Convert.ToBoolean(obj1);
                bool bool2 = Convert.ToBoolean(obj2);
                if (bool1 == bool2)
                {
                    return 0;
                }
                if (bool2)
                {
                    return 1;
                }
                return -1;
            }

            // Do comparisons on strings without a test for case
            if (obj1 is string)
            {
                return string.Compare(Convert.ToString(obj1), Convert.ToString(obj2), IgnoreCase);
            }

            // Comparisons on numerical quantites are easier
            if (obj1 is DateTime)
            {
                return DateTime.Compare(Convert.ToDateTime(obj1), Convert.ToDateTime(obj2));
            }

            if (obj1 is decimal)
            {
                return decimal.Compare(Convert.ToDecimal(obj1), Convert.ToDecimal(obj2));
            }

            if (obj1 is double || obj1 is Single)
            {
                return Convert.ToDouble(obj1).CompareTo(Convert.ToDouble(obj2));
            }

            if (obj1 is byte)
            {
                return Convert.ToByte(obj1).CompareTo(Convert.ToByte(obj2));
            }

            // Do the same for fixed point values
            if (obj1 is Int16 || obj1 is Int32 || obj1 is Int64)
            {
                return Convert.ToInt64(obj1).CompareTo(Convert.ToInt64(obj2));
            }

            // The items must not be simple types.
            // if they implement ICompariable then use it to return the results
            IComparable ic = obj1 as IComparable;
            if (ic != null)
            {
                return ic.CompareTo(obj2);
            }

            // This is just a non-event
            return -1;
        }
    }
}
