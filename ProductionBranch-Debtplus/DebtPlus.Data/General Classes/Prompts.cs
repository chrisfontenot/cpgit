#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

namespace DebtPlus.Data
{
    public static class Prompts
    {
        /// <summary>
        /// Request a delete item confirmation
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static System.Windows.Forms.DialogResult RequestConfirmation_Delete()
        {
            return RequestConfirmation_Delete("Are you sure that you wish to delete this item?");
        }

        /// <summary>
        /// Request a delete item confirmation
        /// </summary>
        /// <param name="PromptString"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static System.Windows.Forms.DialogResult RequestConfirmation_Delete(string PromptString)
        {
            return RequestConfirmation(PromptString);
        }

        /// <summary>
        /// Request a confirmation
        /// </summary>
        /// <param name="Prompt"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static System.Windows.Forms.DialogResult RequestConfirmation(string Prompt)
        {
            return DebtPlus.Data.Forms.MessageBox.Show(Prompt, "Are you sure?", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2);
        }
    }
}
