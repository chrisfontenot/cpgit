#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Diagnostics;

namespace DebtPlus.Data
{
    public class Expression : IDisposable, DebtPlus.Interfaces.ISupportQueryValue
    {
        protected string ExpString;
        protected Int32 ExpStringIdx;
        protected Char ENDSTRING = '\0';

        /// <summary>
        /// Pass along a request for symbolic lookup
        /// </summary>
        public event DebtPlus.Events.ParameterValueEventHandler QueryValue; // Implements DebtPlus.Events.ISupportQueryValue.QueryValue

        /// <summary>
        /// Raise the QueryValue event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseQueryValue(DebtPlus.Events.ParameterValueEventArgs e)
        {
            DebtPlus.Events.ParameterValueEventHandler evt = QueryValue;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Hook into getting the value event.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnQueryValue(DebtPlus.Events.ParameterValueEventArgs e)
        {
            RaiseQueryValue(e);
        }

        /// <summary>
        /// Retrieve the variable value from the caller. There may be arguments to the function if needed.
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected virtual object GetVariableValue(string Name, System.Collections.ArrayList Arguments)
        {
            DebtPlus.Events.ParameterValueEventArgs e = new DebtPlus.Events.ParameterValueEventArgs(Name, Arguments);
            OnQueryValue(e);
            return e.Value;
        }

        /// <summary>
        /// Obtain the value from the function name
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected virtual Token GetFunctionValue(string Name, System.Collections.ArrayList Arguments)
        {
            // Look for known function names if there are arguments to the name.
            if (Arguments != null && Arguments.Count > 0)
            {
                if (string.Compare(Name, "abs", true) == 0)
                {
                    return EvaluateABS(Arguments);
                }
                else if (string.Compare(Name, "len", true) == 0)
                {
                    return EvaluateLEN(Arguments);
                }
                else if (string.Compare(Name, "min", true) == 0)
                {
                    return EvaluateMIN(Arguments);
                }
                else if (string.Compare(Name, "max", true) == 0)
                {
                    return EvaluateMAX(Arguments);
                }
                else if (string.Compare(Name, "left", true) == 0)
                {
                    return EvaluateLEFT(Arguments);
                }
                else if (string.Compare(Name, "right", true) == 0)
                {
                    return EvaluateRIGHT(Arguments);
                }
                else if (string.Compare(Name, "substring", true) == 0)
                {
                    return EvaluateSUBSTRING(Arguments);
                }
            }

            // finally, this is not a function. Evaluate the items
            object Item = GetVariableValue(Name, Arguments);
            if (Item == null)
            {
                return new ErrorToken(string.Format("'{0}' is not defined", Name));
            }
            if (Item is string)
            {
                return new StringToken(Convert.ToString(Item));
            }

            return new NumericToken(Convert.ToDecimal(Item));
        }

        /// <summary>
        /// Evaluate the ABS(...) function
        /// </summary>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected static Token EvaluateABS(System.Collections.ArrayList Arguments)
        {
            Token Answer;

            // An argument single is required
            if (Arguments.Count != 1)
            {
                Answer = ErrorToken.SyntaxError;
            }
            else
            {

                // Ensure that there is no error on the arguments
                Token value = ((Token)Arguments[0]);
                if (value.TokenType != Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    Answer = ErrorToken.SyntaxError;
                }
                else
                {
                    Answer = new NumericToken(System.Math.Abs(Convert.ToDouble(((NumericToken)value).Value)));
                }
            }

            return Answer;
        }

        /// <summary>
        /// Evaluate the MIN(..., ..., ..., ...) function
        /// </summary>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected static Token EvaluateMIN(System.Collections.ArrayList Arguments)
        {
            decimal DecimalItem = decimal.MaxValue;

            foreach (Token currentValue in Arguments)
            {
                if (currentValue.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    decimal Value = Convert.ToDecimal(currentValue.Value);
                    if (Value < DecimalItem)
                    {
                        DecimalItem = Value;
                    }
                }
                else
                {
                    return ErrorToken.SyntaxError;
                }
            }

            return new NumericToken(DecimalItem);
        }

        /// <summary>
        /// Evaluate the MAX(..., ..., ..., ...) function
        /// </summary>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected static Token EvaluateMAX(System.Collections.ArrayList Arguments)
        {
            decimal DecimalItem = decimal.MinValue;

            foreach (Token currentValue in Arguments)
            {
                if (currentValue.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    decimal Value = Convert.ToDecimal(currentValue.Value);
                    if (Value > DecimalItem)
                    {
                        DecimalItem = Value;
                    }
                }
                else
                {
                    return ErrorToken.SyntaxError;
                }
            }

            return new NumericToken(DecimalItem);
        }

        /// <summary>
        /// Evaluate the LEFT(string,number) function
        /// </summary>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected static Token EvaluateLEFT(System.Collections.ArrayList Arguments)
        {
            if (Arguments.Count == 2)
            {
                Token strToken = ((Token)Arguments[0]);
                if (strToken.TokenType == Token.TokenTypeEnum.TOKEN_STRING)
                {
                    string InputString = Convert.ToString(strToken.Value);

                    Token LenToken = ((Token)Arguments[1]);
                    if (LenToken.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                    {
                        Int32 LenValue = Convert.ToInt32(LenToken.Value);
                        if (LenValue <= 0)
                        {
                            return new StringToken(string.Empty);
                        }
                        if (LenValue >= InputString.Length)
                        {
                            return new StringToken(InputString);
                        }

                        return new StringToken(InputString.Substring(LenValue - 1));
                    }
                }
            }

            return ErrorToken.SyntaxError;
        }

        /// <summary>
        /// Evaluate the RIGHT(string,number) function
        /// </summary>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected static Token EvaluateRIGHT(System.Collections.ArrayList Arguments)
        {
            if (Arguments.Count == 2)
            {
                Token strToken = ((Token)Arguments[0]);
                if (strToken.TokenType == Token.TokenTypeEnum.TOKEN_STRING)
                {
                    string InputString = Convert.ToString(strToken.Value);

                    Token LenToken = ((Token)Arguments[1]);
                    if (LenToken.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                    {
                        Int32 LenValue = Convert.ToInt32(LenToken.Value);
                        if (LenValue <= 0)
                        {
                            return new StringToken(string.Empty);
                        }
                        if (LenValue >= InputString.Length)
                        {
                            return new StringToken(InputString);
                        }

                        return new StringToken(InputString.Substring(InputString.Length - LenValue));
                    }
                }
            }

            return ErrorToken.SyntaxError;
        }

        /// <summary>
        /// Evaluate the SUBSTRING(string,number,number) function
        /// </summary>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected static Token EvaluateSUBSTRING(System.Collections.ArrayList Arguments)
        {
            if (Arguments.Count > 0)
            {
                Token strToken = ((Token)Arguments[0]);
                if (strToken.TokenType != Token.TokenTypeEnum.TOKEN_STRING)
                {
                    return ErrorToken.SyntaxError;
                }

                string InputString = Convert.ToString(strToken.Value);
                if (Arguments.Count < 2)
                {
                    return new StringToken(InputString);
                }

                Token PosToken = ((Token)Arguments[1]);
                if (PosToken.TokenType != Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return ErrorToken.SyntaxError;
                }

                Int32 posValue = Convert.ToInt32(PosToken.Value);
                if (posValue <= 0)
                {
                    return ErrorToken.SyntaxError;
                }

                if (Arguments.Count == 3)
                {
                    Token LenToken = ((Token)Arguments[2]);
                    if (LenToken.TokenType != Token.TokenTypeEnum.TOKEN_NUMBER)
                    {
                        return ErrorToken.SyntaxError;
                    }

                    Int32 LenValue = Convert.ToInt32(LenToken.Value);
                    if (LenValue <= 0)
                    {
                        return ErrorToken.SyntaxError;
                    }

                    if (posValue + LenValue >= InputString.Length)
                    {
                        LenValue = InputString.Length - LenValue;
                        if (LenValue <= 0)
                        {
                            return new StringToken(InputString.Substring(posValue));
                        }
                    }
                    return new StringToken(InputString.Substring(posValue, LenValue));
                }
            }

            return ErrorToken.SyntaxError;
        }

        /// <summary>
        /// Evaluate the LEN(string) function
        /// </summary>
        /// <param name="Arguments"></param>
        /// <returns></returns>
        protected static Token EvaluateLEN(System.Collections.ArrayList Arguments)
        {
            Token Answer;

            // An argument single is required
            if (Arguments.Count != 1)
            {
                Answer = ErrorToken.SyntaxError;
            }
            else
            {

                // Ensure that there is no error on the arguments
                Token value = ((Token)Arguments[0]);
                if (value.TokenType == Token.TokenTypeEnum.TOKEN_STRING)
                {
                    Answer = new NumericToken(Convert.ToString(value.Value).Length);
                }
                else
                {
                    Answer = ErrorToken.SyntaxError;
                }
            }

            return Answer;
        }

        #region  Scanner
        /// <summary>
        /// Retrieve the next character from the input
        /// </summary>
        /// <returns></returns>
        protected Char NextChar()
        {
            Char result = ExpString[ExpStringIdx];
            ExpStringIdx += 1;
            return result;
        }

        /// <summary>
        /// Push the character back on the input queue
        /// </summary>
        protected void ScannerPushback()
        {
            ExpStringIdx -= 1;
            Debug.Assert(ExpStringIdx >= 0);
        }

        /// <summary>
        /// Ignore leading space characters
        /// </summary>
        protected void SkipSpaces()
        {
            Char Answer = NextChar();
            while (Char.IsWhiteSpace(Answer))
            {
                Answer = NextChar();
            }
            ScannerPushback();
        }

        private Token PushedBackToken;
        /// <summary>
        /// Scan the string for the next token
        /// </summary>
        /// <returns></returns>
        protected Token NextToken()
        {

            // Use the previously scanned token if there is one.
            if (PushedBackToken != null)
            {
                Token NextTokenResult = PushedBackToken;
                PushedBackToken = null;
                return NextTokenResult;
            }

            // Empty the token string buffer
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            SkipSpaces();
            Char Answer = NextChar();

            // if the end of the string is found then return the token of END-OF-STRING
            if (Answer == ENDSTRING)
            {
                ScannerPushback();   //trick -- keep scanning EOS from this point on....
                return new Token(Token.TokenTypeEnum.TOKEN_EOS);
            }

            // Look for a variable reference. Variables are further qualified later.
            if (Char.IsLetter(Answer))
            {
                do
                {
                    sb.Append(Answer);
                    Answer = NextChar();
                    if (Answer == ENDSTRING)
                    {
                        break;
                    }
                }
                while (Char.IsLetterOrDigit(Answer) | Answer == '.');

                ScannerPushback();
                return new Token(Token.TokenTypeEnum.TOKEN_VARIABLE, sb.ToString());
            }

            // Process a date constant
            if (Answer == '#')
            {
                Answer = NextChar();
                while (Answer != '#')
                {
                    if (Answer == ENDSTRING)
                    {
                        return new ErrorToken("Unmatched # character");
                    }

                    sb.Append(Answer);
                    Answer = NextChar();
                }

                ScannerPushback();

                try
                {
                    return new DateToken(sb.ToString());
                }
                catch (Exception ex)
                {
                    return new ErrorToken(ex.Message);
                }
            }

            // Process a number
            if (Answer == System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol[0])
            {
                Answer = NextChar();
            }

            if (Char.IsDigit(Answer))
            {
                do
                {
                    sb.Append(Answer);
                    Answer = NextChar();
                    if (Answer == ENDSTRING)
                    {
                        break;
                    }
                } while (Char.IsDigit(Answer) || Answer == System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0]);

                ScannerPushback();

                try
                {
                    return new NumericToken(sb.ToString());
                }
                catch (Exception ex)
                {
                    return new ErrorToken(ex.Message);
                }
            }

            // Process a string
            if (Answer == '"')
            {
                for (; ; )
                {
                    Answer = NextChar();
                    if (Answer == ENDSTRING)
                    {
                        ScannerPushback();
                        return new ErrorToken("Unterminated string constant");
                    }

                    // look for the closing quote character. Process double-quotes as a single quote character.
                    if (Answer == '"')
                    {
                        Answer = NextChar();
                        if (Answer != '"')
                        {
                            break;
                        }
                    }

                    sb.Append(Answer);
                    Answer = NextChar();
                }

                ScannerPushback();
                return new StringToken(sb.ToString());
            }

            // Process punctuation characters
            switch (Answer)
            {
                case '*':
                    return new Token(Token.TokenTypeEnum.TOKEN_ASTERISK);

                case ',':
                    return new Token(Token.TokenTypeEnum.TOKEN_COMMA);

                case '-':
                    return new Token(Token.TokenTypeEnum.TOKEN_DASH);

                case '=':
                    return new Token(Token.TokenTypeEnum.TOKEN_EQUAL);

                case '(':
                    return new Token(Token.TokenTypeEnum.TOKEN_LPAREN);

                case ')':
                    return new Token(Token.TokenTypeEnum.TOKEN_RPAREN);

                case '>':
                    Answer = NextChar();
                    if (Answer == '=')
                    {
                        return new Token(Token.TokenTypeEnum.TOKEN_GREATEREQUAL);
                    }
                    ExpStringIdx -= 1;
                    return new Token(Token.TokenTypeEnum.TOKEN_GREATER);

                case '<':
                    Answer = NextChar();
                    if (Answer == '>')
                    {
                        return new Token(Token.TokenTypeEnum.TOKEN_NOTEQUAL);
                    }
                    if (Answer == '=')
                    {
                        return new Token(Token.TokenTypeEnum.TOKEN_LESSEQUAL);
                    }
                    ExpStringIdx -= 1;
                    return new Token(Token.TokenTypeEnum.TOKEN_LESS);

                case '!':
                    Answer = NextChar();
                    if (Answer == '=')
                    {
                        return new Token(Token.TokenTypeEnum.TOKEN_NOTEQUAL);
                    }
                    ExpStringIdx -= 1;
                    return new Token(Token.TokenTypeEnum.TOKEN_NOT);

                case '+':
                    return new Token(Token.TokenTypeEnum.TOKEN_PLUS);

                case '/':
                    return new Token(Token.TokenTypeEnum.TOKEN_SLASH);

                default:
                    return new ErrorToken(string.Format("Invalid character '{0}'", Answer));
            }
        }

        /// <summary>
        /// Push the parsed token back on the input queue
        /// </summary>
        /// <param name="TokenValue"></param>
        protected void PushbackToken(Token TokenValue)
        {
            Debug.Assert(PushedBackToken == null);
            PushedBackToken = TokenValue;
        }
        #endregion

        #region  Token
        /// <summary>
        /// General token class
        /// </summary>
        protected class Token
        {
            public enum TokenTypeEnum
            {
                TOKEN_EOS = 0,
                TOKEN_ERROR,

                TOKEN_LPAREN,
                TOKEN_RPAREN,

                TOKEN_VARIABLE,
                TOKEN_NUMBER,
                TOKEN_STRING,
                TOKEN_DATE,

                TOKEN_PLUS,
                TOKEN_ASTERISK,
                TOKEN_SLASH,
                TOKEN_DASH,
                TOKEN_COMMA,
                TOKEN_LESS,
                TOKEN_LESSEQUAL,
                TOKEN_EQUAL,
                TOKEN_GREATER,
                TOKEN_GREATEREQUAL,
                TOKEN_NOTEQUAL,

                TOKEN_NOT,
                TOKEN_AND,
                TOKEN_OR
            }

            /// <summary>
            /// Create instances of this class
            /// </summary>
            public Token()
            {
                TokenType = Token.TokenTypeEnum.TOKEN_ERROR;
                Value = string.Empty;
            }

            public Token(TokenTypeEnum TokenType, string Value = "")
                : this()
            {
                this.TokenType = TokenType;
                this.Value = Value;
            }

            public TokenTypeEnum TokenType { get; set; }

            /// <summary>
            /// Value of the token item
            /// </summary>
            public object Value { get; set; }

            /// <summary>
            /// Arithmetic operations are all invalid for standard tokens
            /// </summary>
            /// <param name="Term"></param>
            /// <returns></returns>
            public virtual Token Add(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token Subtract(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token Multiply(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token Divide(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token IsEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token IsNotEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token IsGreater(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token IsLess(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token IsLessEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token IsGreaterEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public virtual Token NotEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }
        }

        /// <summary>
        /// Numeric and bool value tokens
        /// </summary>
        protected class NumericToken : Token
        {
            public NumericToken()
                : base(Token.TokenTypeEnum.TOKEN_NUMBER)
            {
            }

            public NumericToken(string TokenString)
                : this(Convert.ToDecimal(TokenString))
            {
            }

            public NumericToken(Int32 Value)
                : base(Token.TokenTypeEnum.TOKEN_NUMBER)
            {
                base.Value = Convert.ToDecimal(Value);
            }

            public NumericToken(decimal Value)
                : base(Token.TokenTypeEnum.TOKEN_NUMBER)
            {
                base.Value = Value;
            }

            public NumericToken(double Value)
                : base(Token.TokenTypeEnum.TOKEN_NUMBER)
            {
                base.Value = Convert.ToDecimal(Value);
            }

            public NumericToken(bool Value)
                : base(Token.TokenTypeEnum.TOKEN_NUMBER)
            {
                if (Value)
                {
                    base.Value = 1;
                }
                else
                {
                    base.Value = 0;
                }
            }

            public NumericToken(TokenTypeEnum TokenType, string Value = "")
                : base(TokenType, Value)
            {
            }

            public bool ToBoolean()
            {
                return ((decimal)Value) != 0M;
            }

            /// <summary>
            /// Do arithmetic operations
            /// </summary>
            /// <param name="Term"></param>
            /// <returns></returns>
            public override Token Add(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    try
                    {
                        return new NumericToken(((decimal)Value) + Convert.ToDecimal(Term.Value));
                    }
                    catch (ArithmeticException ex)
                    {
                        return new ErrorToken(ex.Message);
                    }
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_STRING)
                {
                    try
                    {
                        return new StringToken(Convert.ToString(Value) + Convert.ToString(Term.Value));
                    }
                    catch (ArithmeticException ex)
                    {
                        return new ErrorToken(ex.Message);
                    }
                }
                return ErrorToken.SyntaxError;
            }

            public override Token Subtract(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return new NumericToken(((decimal)Value) - Convert.ToDecimal(Term.Value));
                }
                return ErrorToken.SyntaxError;
            }

            public override Token Multiply(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    try
                    {
                        return new NumericToken(((decimal)Value) * Convert.ToDecimal(Term.Value));
                    }
                    catch (ArithmeticException ex)
                    {
                        return new ErrorToken(ex.Message);
                    }
                }
                return ErrorToken.SyntaxError;
            }

            public override Token Divide(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    try
                    {
                        return new NumericToken(((decimal)Value) / Convert.ToDecimal(Term.Value));
                    }
                    catch (ArithmeticException ex)
                    {
                        return new ErrorToken(ex.Message);
                    }
                }
                return ErrorToken.SyntaxError;
            }

            /// <summary>
            /// Do relational operations
            /// </summary>
            /// <param name="Term"></param>
            /// <returns></returns>
            public override Token IsEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return new NumericToken(((decimal)Value) == Convert.ToDecimal(Term.Value));
                }
                return ErrorToken.SyntaxError;
            }

            public override Token IsNotEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return new NumericToken(((decimal)Value) != Convert.ToDecimal(Term.Value));
                }
                return ErrorToken.SyntaxError;
            }

            public override Token IsGreater(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return new NumericToken(((decimal)Value) > Convert.ToDecimal(Term.Value));
                }
                return ErrorToken.SyntaxError;
            }

            public override Token IsLess(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return new NumericToken(((decimal)Value) < Convert.ToDecimal(Term.Value));
                }
                return ErrorToken.SyntaxError;
            }

            public override Token IsLessEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return new NumericToken(((decimal)Value) <= Convert.ToDecimal(Term.Value));
                }
                return ErrorToken.SyntaxError;
            }

            public override Token IsGreaterEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_NUMBER)
                {
                    return new NumericToken(((decimal)Value) >= Convert.ToDecimal(Term.Value));
                }
                return ErrorToken.SyntaxError;
            }
        }

        /// <summary>
        /// string token types
        /// </summary>
        protected class StringToken : Token
        {
            public StringToken() :
                base(Token.TokenTypeEnum.TOKEN_STRING)
            {
            }

            public StringToken(string TokenString) :
                base(Token.TokenTypeEnum.TOKEN_STRING, TokenString)
            {
            }

            public StringToken(TokenTypeEnum TokenType, string Value = "") :
                base(TokenType, Value)
            {
            }

            /// <summary>
            /// Do arithmetic operations
            /// </summary>
            /// <param name="Term"></param>
            /// <returns></returns>
            public override Token Add(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                return new StringToken(((string)Value) + Convert.ToString(Term.Value));
            }

            /// <summary>
            /// Do relational operations
            /// </summary>
            /// <param name="Term"></param>
            /// <returns></returns>
            public override Token IsEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                return new NumericToken(string.Compare(((string)Value), Convert.ToString(Term.Value), true) == 0);
            }

            public override Token IsNotEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                return new NumericToken(string.Compare(((string)Value), Convert.ToString(Term.Value), true) != 0);
            }

            public override Token IsGreater(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                return new NumericToken(string.Compare(((string)Value), Convert.ToString(Term.Value), true) > 0);
            }

            public override Token IsLess(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                return new NumericToken(string.Compare(((string)Value), Convert.ToString(Term.Value), true) < 0);
            }

            public override Token IsLessEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                return new NumericToken(string.Compare(((string)Value), Convert.ToString(Term.Value), true) <= 0);
            }

            public override Token IsGreaterEqual(Token Term)
            {
                if (Term.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                {
                    return Term;
                }
                return new NumericToken(string.Compare(((string)Value), Convert.ToString(Term.Value), true) >= 0);
            }
        }

        /// <summary>
        /// DATE token types
        /// </summary>
        protected class DateToken : Token
        {
            public DateToken() :
                base(Token.TokenTypeEnum.TOKEN_DATE)
            {
            }

            public DateToken(string Value) :
                base(Token.TokenTypeEnum.TOKEN_DATE)
            {
                base.Value = Convert.ToDateTime(Value);
            }

            public DateToken(DateTime Value) :
                base(Token.TokenTypeEnum.TOKEN_DATE)
            {
                base.Value = Value;
            }

            public DateToken(TokenTypeEnum TokenType, string Value = "") :
                base(TokenType, Value)
            {
            }

            /// <summary>
            /// Arithmethic operations are invalid. See "DateAdd" and "DateDiff"
            /// </summary>
            /// <param name="Term"></param>
            /// <returns></returns>
            public override Token Add(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            /// <summary>
            /// Do relational operations
            /// </summary>
            /// <param name="Term"></param>
            /// <returns></returns>
            public override Token IsEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public override Token IsNotEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public override Token IsGreater(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public override Token IsLess(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public override Token IsLessEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public override Token IsGreaterEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }

            public override Token NotEqual(Token Term)
            {
                return ErrorToken.SyntaxError;
            }
        }

        /// <summary>
        /// INVALID tokens
        /// </summary>
        protected class ErrorToken : Token
        {
            public ErrorToken() :
                base(Token.TokenTypeEnum.TOKEN_ERROR)
            {
            }

            public ErrorToken(string TokenString) :
                base(Token.TokenTypeEnum.TOKEN_ERROR, TokenString)
            {
            }

            public ErrorToken(TokenTypeEnum TokenType, string Value = "") :
                base(TokenType, Value)
            {
            }

            internal static ErrorToken SyntaxError
            {
                get
                {
                    return new ErrorToken("Syntax Error");
                }
            }
        }
        #endregion

        /// <summary>
        /// Process the PROGRAM item
        /// </summary>
        /// <returns></returns>
        protected virtual Token Program()
        {
            // Program = Block .
            Token Answer = Condition();
            if (Answer.TokenType != Token.TokenTypeEnum.TOKEN_ERROR && NextToken().TokenType != Token.TokenTypeEnum.TOKEN_EOS)
            {
                Answer = ErrorToken.SyntaxError;
            }

            return Answer;
        }

#if false
        /// <summary>
        /// Process the BLOCK item
        /// </summary>
        /// <returns></returns>
        protected virtual Token Block()
        {
            // Block = ["const" ident "=" number {"," ident "=" number} ";"]
            //         ["var" ident {"," ident} ";"]
            //         {"procedure" ident ";" block ";"} statement .
        }

        /// <summary>
        /// Process the STATEMENT item
        /// </summary>
        /// <returns></returns>
        protected virtual Token Statement()
        {
            // statement = [ "if" condition "then" statement {"else" statement}
        }
#endif

        /// <summary>
        /// Process the CONDITION item
        /// </summary>
        /// <returns></returns>
        protected virtual Token Condition()
        {
            // condition = expression [("="|"<>"|"<"|"<="|">"|">=") expression] .

            // Look for the leading plus and minus values
            Token Answer = Expr();

            // Look for the relational operators
            Token CurrentToken = NextToken();
            switch (CurrentToken.TokenType)
            {
                case Token.TokenTypeEnum.TOKEN_EQUAL:
                    Answer = Answer.IsEqual(Expr());
                    break;

                case Token.TokenTypeEnum.TOKEN_GREATER:
                    Answer = Answer.IsGreater(Expr());
                    break;

                case Token.TokenTypeEnum.TOKEN_GREATEREQUAL:
                    Answer = Answer.IsGreaterEqual(Expr());
                    break;

                case Token.TokenTypeEnum.TOKEN_LESS:
                    Answer = Answer.IsLess(Expr());
                    break;

                case Token.TokenTypeEnum.TOKEN_LESSEQUAL:
                    Answer = Answer.IsLessEqual(Expr());
                    break;

                case Token.TokenTypeEnum.TOKEN_NOTEQUAL:
                    Answer = Answer.IsNotEqual(Expr());
                    break;

                default:
                    PushbackToken(CurrentToken);
                    break;
            }

            return Answer;
        }

        /// <summary>
        /// Process the TERM item
        /// </summary>
        /// <returns></returns>
        protected virtual Token Term()
        {
            // term = factor {("*"|"/") factor} .

            // Look for the leading plus and minus values
            Token Answer = Factor();

            // Look for the operators * and /
            Token CurrentToken;
            for (; ; )
            {
                CurrentToken = NextToken();
                if (CurrentToken.TokenType == Token.TokenTypeEnum.TOKEN_ASTERISK)
                {
                    Answer = Answer.Multiply(Factor());
                }
                else if (CurrentToken.TokenType == Token.TokenTypeEnum.TOKEN_SLASH)
                {
                    Answer = Answer.Divide(Factor());
                }
                else
                {
                    break;
                }
            }

            PushbackToken(CurrentToken);
            return Answer;
        }

        /// <summary>
        /// Process the EXPRESSION item
        /// </summary>
        /// <returns></returns>
        protected virtual Token Expr()
        {
            // expression = ["+"|"-"] term {("+"|"-") term} .

            // Look for the leading plus and minus values
            Token Answer = NextToken();
            bool Negate = false;

            // Look for the leading plus and minus values.
            if (Answer.TokenType != Token.TokenTypeEnum.TOKEN_PLUS)
            {
                if (Answer.TokenType == Token.TokenTypeEnum.TOKEN_DASH)
                {
                    Negate = true;
                }
                else
                {
                    PushbackToken(Answer);
                }
            }

            // Evaluate the first term. Negate it if needed
            if (Negate)
            {
                Answer = (new NumericToken(0)).Subtract(Term());
            }
            else
            {
                Answer = Term();
            }

            // Look for the operators + and -
            Token CurrentToken;
            for (; ; )
            {
                CurrentToken = NextToken();
                if (CurrentToken.TokenType == Token.TokenTypeEnum.TOKEN_PLUS)
                {
                    Answer = Answer.Add(Term());
                }
                else if (CurrentToken.TokenType == Token.TokenTypeEnum.TOKEN_DASH)
                {
                    Answer = Answer.Subtract(Term());
                }
                else
                {
                    break;
                }
            }

            PushbackToken(CurrentToken);
            return Answer;
        }

        /// <summary>
        /// Process the FACTOR item
        /// </summary>
        /// <returns></returns>
        protected virtual Token Factor()
        {
            // factor = ident | number | "(" expression ")" | ident( [expression {"," expression }] ) .

            // Evaluate the next item from the scanner
            Token CurrentToken = NextToken();
            switch (CurrentToken.TokenType)
            {
                case Token.TokenTypeEnum.TOKEN_LPAREN:
                    CurrentToken = Expr();
                    if (NextToken().TokenType != Token.TokenTypeEnum.TOKEN_RPAREN)
                    {
                        return ErrorToken.SyntaxError;
                    }
                    break;

                case Token.TokenTypeEnum.TOKEN_VARIABLE:
                    System.Collections.ArrayList Arguments = new System.Collections.ArrayList();
                    Token TestToken = NextToken();
                    if (TestToken.TokenType == Token.TokenTypeEnum.TOKEN_LPAREN)
                    {
                        // Look for the closing paren. It is expected but there may not be any arguments.
                        TestToken = NextToken();
                        if (TestToken.TokenType == Token.TokenTypeEnum.TOKEN_EOS)
                        {
                            return ErrorToken.SyntaxError;
                        }

                        // if the token is not a close paren the evaluate the first parameter
                        if (TestToken.TokenType != Token.TokenTypeEnum.TOKEN_RPAREN)
                        {
                            PushbackToken(TestToken);
                            Token ValueToken = Expr();
                            for (; ; )
                            {
                                if (ValueToken.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
                                {
                                    return ValueToken;
                                }
                                Arguments.Add(ValueToken);

                                TestToken = NextToken();
                                if (TestToken.TokenType == Token.TokenTypeEnum.TOKEN_RPAREN)
                                {
                                    break;
                                }
                                if (TestToken.TokenType != Token.TokenTypeEnum.TOKEN_COMMA)
                                {
                                    return ErrorToken.SyntaxError;
                                }
                                ValueToken = Expr();
                            }
                        }
                    }

                    // Evaluate the variable/function
                    return GetFunctionValue(Convert.ToString(CurrentToken.Value), Arguments);
            }
            return CurrentToken;
        }

        /// <summary>
        /// Parse the expression. return the resulting
        /// </summary>
        /// <param name="ExpString"></param>
        /// <returns></returns>
        public object Parse(string ExpString)
        {
            // Save the expression string.
            this.ExpString = ExpString + ENDSTRING;

            // Process the expression
            Token ExpressionToken = Program();
            if (ExpressionToken.TokenType == Token.TokenTypeEnum.TOKEN_ERROR)
            {
                throw new ApplicationException(Convert.ToString(ExpressionToken.Value));
            }

            return ExpressionToken.Value;
        }

        private bool disposedValue = false;        //To detect redundant calls

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    // TODO: free b state (managed objects).
                }

                // TODO: free your own state (unmanaged objects).
                // TODO: set large fields to null.
            }
            this.disposedValue = true;
        }

        #region  IDisposable Support
        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose() // Implements IDisposable.Dispose
        {
            // Do not change this code.  Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
