﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.Data
{
    public static class Fonts
    {
        public static System.Collections.Generic.List<System.Drawing.FontFamily> GetFonts()
        {
            // System.Collections.Generic.List<> colResult;
            var col = new System.Drawing.Text.InstalledFontCollection();
            return col.Families.ToList<System.Drawing.FontFamily>();
        }

        public static string GetExcludeList()
        {
            // Find the list of families involved in the current system.
            var sb = new System.Text.StringBuilder();
            foreach (var family in GetFonts().Where(s => s.Name.StartsWith("Calibri")))
            {
                if (family.IsStyleAvailable(System.Drawing.FontStyle.Regular))
                {
                    sb.AppendFormat(";{0}", family.Name);
                }
            }

            // Return the resulting list of family names
            if (sb.Length > 0)
            {
                sb.Remove(0, 1);
            }

            return sb.ToString();
        }
    }
}
