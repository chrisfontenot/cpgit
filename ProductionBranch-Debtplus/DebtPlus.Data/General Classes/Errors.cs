
#region Copyright 2000-2012 DebtPlus, L.L.C.

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion

using System;
using System.Linq;
using System.Windows.Forms;
    
namespace DebtPlus.Data
{
    public class Errors
    {
        /// <summary>
        /// Don't do anything. This is just a convient spot to put a breakpoint when debugging the code.
        /// All exceptions call this routine, even if they don't do anything about the error.
        /// </summary>
        /// <remarks>This routine does nothing by default.</remarks>
        [Obsolete("Use DebtPlus.Svc.Common.ErrorHandling.HandleErrors")]
        public static void DebuggingException()
        {
            DebuggingException((Exception)null);
        }
        
        /// <summary>
        /// Don't do anything. This is just a convient spot to put a breakpoint when debugging the code.
        /// All exceptions call this routine, even if they don't do anything about the error.
        /// </summary>
        /// <param name="ex">The current exception</param>
        /// <remarks>This routine does nothing by default.</remarks>
        [Obsolete("Use DebtPlus.Svc.Common.ErrorHandling.HandleErrors")]
        public static void DebuggingException(Exception ex)
        {
        }
        
        /// <summary>
        /// Generate a message box showing the database error that occurred. This is not neceissarly an
        /// error condition (indicating that it is broken), but it indicates that the expected operation did not
        /// complete successfully.
        /// </summary>
        /// <param name="ex">The database error exception</param>
        /// <remarks></remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static void DatabaseError(Exception ex)
        {
            ExceptionMessageBox(ex);
        }
        
        /// <summary>
        /// Generate a message box showing the database error that occurred. This is not neceissarly an
        /// error condition (indicating that it is broken), but it indicates that the expected operation did not
        /// complete successfully.
        /// </summary>
        /// <param name="ex">The database error exception</param>
        /// <param name="caption">Caption for the message box</param>
        /// <remarks></remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static void DatabaseError(Exception ex, string caption)
        {
            ExceptionMessageBox(ex, caption);
        }
            
        private class ExceptionArguments
        {
            // Information about the message box
            public DialogResult Result = DialogResult.OK;
            public bool ShowStackTrace = true;
            
            // Arguments to display in the message box
            public readonly Exception ex;
            public readonly string Caption = "Database Error";
            public readonly MessageBoxButtons Buttons = MessageBoxButtons.OK;
            public readonly MessageBoxDefaultButton DefaultButton = MessageBoxDefaultButton.Button1;
            public readonly MessageBoxOptions Options = MessageBoxOptions.DefaultDesktopOnly;
            public readonly MessageBoxIcon Icon = MessageBoxIcon.Error;
            public readonly bool IsModal;
                
            public string MessageText
            {
                get
                {
                    if (ShowStackTrace)
                    {
                        return ex.ToString();
                    }
                    else
                    {
                        return ex.Message;
                    }
                }
            }

            public ExceptionArguments(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton DefaultButton, MessageBoxOptions Options, bool IsModal)
            {
                this.ex = ex;
                if (Caption != string.Empty)
                {
                    this.Caption = Caption;
                }
                if (Buttons != MessageBoxButtons.OK)
                {
                    this.Buttons = Buttons;
                }
                if (DefaultButton != MessageBoxDefaultButton.Button1)
                {
                    this.DefaultButton = DefaultButton;
                }
                if (Options != MessageBoxOptions.DefaultDesktopOnly)
                {
                    this.Options = Options;
                }
                this.IsModal = IsModal;
            }
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBox(Exception ex)
        {
            return ConductDialog(new ExceptionArguments(ex, string.Empty, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, MessageBoxOptions.DefaultDesktopOnly, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <param name="Options">Options for the message box</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modless dialog if the expected response is simply OK.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBox(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton, MessageBoxOptions Options)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, Options, false));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBoxDialog(Exception ex)
        {
            return ConductDialog(new ExceptionArguments(ex, string.Empty, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, true));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, MessageBoxButtons.OK, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, true));
        }

        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption, MessageBoxButtons Buttons)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly, true));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="caption">Caption for the message window</param>
        /// <param name="buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, MessageBoxOptions.DefaultDesktopOnly, true));
        }
        
        /// <summary>
        /// Log the error from the database
        /// </summary>
        /// <param name="ex">Error to be logged</param>
        /// <param name="Caption">Caption for the message window</param>
        /// <param name="Buttons">Type(s) of buttons to be used on the dialog</param>
        /// <param name="defaultButton">The default button for the error dialog.</param>
        /// <param name="Options">Options for the message box</param>
        /// <returns>The user's response to the error window</returns>
        /// <remarks>This is a modal dialog.</remarks>
        [Obsolete("Use DebtPlus.UI.Common.ErrorHandling.HandleErrors")]
        public static DialogResult ExceptionMessageBoxDialog(Exception ex, string Caption, MessageBoxButtons Buttons, MessageBoxDefaultButton defaultButton, MessageBoxOptions Options)
        {
            return ConductDialog(new ExceptionArguments(ex, Caption, Buttons, defaultButton, Options, true));
        }

        private static DialogResult ConductDialog(ExceptionArguments args)
        {
            // if there is a single button of OK, then start a thread to run the message in the background.
            // Otherwise, do it in the foreground so that we can return the proper result.
            if (args.Buttons == MessageBoxButtons.OK && !args.IsModal)
            {
                var thrd = new System.Threading.Thread(DatabaseError_Thread);
                thrd.SetApartmentState(System.Threading.ApartmentState.STA);
                thrd.Start(args);
                return DialogResult.OK;
            }
            DatabaseError_Thread(args);
            return args.Result;
        }
        
        /// <summary>
        /// Thread to handle the message box for the database error. We do this in a seperate
        /// thread to allow the calling thread to return and do the cleanup operation on the
        /// failure while the user is looking at the error window. It prevents a lockup condition
        /// should a transaction be used.
        /// </summary>
        /// <param name="obj">Parameters for the messagebox event</param>
        private static void DatabaseError_Thread(object obj)
        {
            var args = ((ExceptionArguments)obj);
            
            // Write the information to the system log
            using (var log = new System.Diagnostics.EventLog() { Source = "DebtPlus" })
            {
                try
                {
                    log.WriteEntry(args.ex.ToString(), System.Diagnostics.EventLogEntryType.Error); //Write the event to the system log so that we don't loose it.
                    args.ShowStackTrace = false;
                }
                catch (System.Security.SecurityException)
                {
                    // Recognized exception. Ignore it.
                }
                catch (Exception exLog)
                {
                    // All others generate an additional alert condition
                    DebtPlus.Data.Forms.MessageBox.Show(exLog.ToString(), "Error writing to the event log", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        
            // Tell the user that the event and read the response.
            args.Result = DebtPlus.Data.Forms.MessageBox.Show(args.MessageText, args.Caption, args.Buttons, args.Icon, args.DefaultButton);
        }
    }
}