﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Diagnostics;

namespace DebtPlus.Data
{
    public class MRU : IDisposable
    {
        private readonly System.Collections.Generic.List<DetailItem> ItemList = new System.Collections.Generic.List<DetailItem>();
        private readonly System.Collections.Generic.List<string> mruList = new System.Collections.Generic.List<string>();

        private const string sequenceKey = "sequence";
        private const string historyKey = "History";
        private readonly string registryLocation;

        /// <summary>
        /// Create an empty instance of our class
        /// </summary>
        /// <remarks></remarks>
        private MRU()
        {
            MaxCount = 20;
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="KeyName">The key to the registry list</param>
        /// <remarks></remarks>
        public MRU(string KeyName)
            : this()
        {
            // Save the key name for later use
            registryLocation = KeyName;

            // Ensure that the lists are clear
            mruList.Clear();
            ItemList.Clear();

            // Read the list of items from the system.
            // Retrieve the item from the registry
            string SequenceString = string.Empty;
            using (var reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(baseKey(registryLocation), true))
            {
                if (reg != null)
                {
                    SequenceString = Convert.ToString(reg.GetValue(sequenceKey, string.Empty));
                    string[] sequenceIds = SequenceString.Split(',');
                    mruList.AddRange(sequenceIds);

                    // Read the values associated with those ids
                    foreach (string key in sequenceIds)
                    {
                        string RegValue = Convert.ToString(reg.GetValue(key, string.Empty));
                        if (RegValue != string.Empty)
                        {
                            DetailItem newItem = new DetailItem(key, RegValue);
                            ItemList.Add(newItem);
                            newItem.PropertyChanged += DetailItem_PropertyChanged;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Maximum number of items in the list.
        /// </summary>
        public Int32 MaxCount { get; set; }

        /// <summary>
        /// The key to the CURRENTUSER registry settings for the history items
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string baseKey()
        {
            return string.Format(@"{0}\{1}", DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey, historyKey);
        }

        /// <summary>
        /// The key to the CURRENTUSER registry settings for the history items
        /// </summary>
        /// <param name="SubKey"></param>
        /// <returns></returns>
        private static string baseKey(string SubKey)
        {
            return string.Format(@"{0}\{1}", baseKey(), SubKey);
        }

        private class DetailItem : System.ComponentModel.INotifyPropertyChanged
        {
            // Local storage
            private string privateKey;
            private string privateValue;

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public DetailItem()
            {
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="Key"></param>
            public DetailItem(string Key)
                : this()
            {
                this.Key = Key;
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="Key"></param>
            /// <param name="Value"></param>
            public DetailItem(string Key, string Value)
                : this(Key)
            {
                this.Value = Value;
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="id"></param>
            public DetailItem(Int32 id)
                : this(MapIdToKey(id))
            {
            }

            /// <summary>
            /// Convert a numerical value to a key string. The keys go a, b, ... z, aa, ab, ... az, ba, ... bz
            /// </summary>
            /// <param name="id">The numerical value for the key sequence</param>
            /// <returns>A string for the key</returns>
            /// <remarks>This logic breaks down at 26 squared or 676.</remarks>
            public static string MapIdToKey(Int32 id)
            {
                // Ensure that the value is not too large.
                Debug.Assert(id < 676);

                // if the value is more than we have for a single letter, go to two letters.
                if (id > 25)
                {
                    return MapIdToKey((id / 26) - 1) + MapIdToKey(id % 26);
                }

                // return the single letter associated with the key value.
                return "abcdefghijklmnopqrstuvwxyz".Substring(id, 1);
            }

            /// <summary>
            /// Key value associated with the item value
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public string Key
            {
                get
                {
                    return privateKey;
                }
                set
                {
                    if (string.Compare(value, privateKey, true) != 0)
                    {
                        privateKey = value;
                        RaisePropertyChanged("Key");
                    }
                }
            }

            /// <summary>
            /// Item value associated with the indicated key
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public string Value
            {
                get
                {
                    return privateValue;
                }
                set
                {
                    if (string.Compare(value, privateValue, true) != 0)
                    {
                        privateValue = value;
                        RaisePropertyChanged("Value");
                    }
                }
            }

            #region PropertyChanged
            /// <summary>
            /// Raized with the key or value is changed. The item is then written to the registry.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            /// <remarks></remarks>
            public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged; // Implements INotifyPropertyChanged.PropertyChanged

            /// <summary>
            /// Process the operty change status
            /// </summary>
            /// <param name="e"></param>
            protected virtual void OnPropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
            {
                RaisePropertyChanged(e);
            }

            /// <summary>
            /// Raise the PropertyChanged event
            /// </summary>
            /// <param name="e"></param>
            protected void RaisePropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
            {
                System.ComponentModel.PropertyChangedEventHandler evt = PropertyChanged;
                if (evt != null)
                {
                    evt(this, e);
                }
            }

            /// <summary>
            /// Local routine to create the parameters and raise the PropertyChanged event
            /// </summary>
            /// <param name="PropertyName"></param>
            protected void RaisePropertyChanged(string PropertyName)
            {
                OnPropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
            }
            #endregion
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="KeyName">The key to the registry list</param>
        /// <param name="OldName">if there are no items, this is the key to the old list.</param>
        /// <remarks></remarks>
        public MRU(string KeyName, string OldName)
            : this(KeyName)
        {
            // if there are no "new" items then look for the items under the old key.
            if (ItemList.Count == 0 && OldName != string.Empty)
            {
                // Retrieve the item from the registry
                string RegistryOldString = string.Empty;
                using (Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(string.Format(@"{0}\History", DebtPlus.Configuration.AssemblyInfo.InstallationRegistryKey), true))
                {
                    if (reg != null)
                    {
                        RegistryOldString = Convert.ToString(reg.GetValue(OldName, string.Empty));

                        // Split the old key along a comma and add it to the collection
                        string[] ItemStrings = RegistryOldString.Split(',');
                        Int32 id = ItemList.Count;

                        System.Collections.IEnumerator ie = ItemStrings.GetEnumerator();
                        while (ie.MoveNext())
                        {
                            string ItemValue = ((string)ie.Current).Trim();
                            if (ItemValue != string.Empty)
                            {
                                DetailItem Detail = new DetailItem(id) { Value = ItemValue };
                                Detail.PropertyChanged += DetailItem_PropertyChanged;

                                ItemList.Add(Detail);
                                mruList.Add(Detail.Key);
                                SaveItem(Detail);

                                id += 1;
                            }
                        }

                        try
                        {
                            // Purge the old key from the registry.
                            reg.DeleteValue(OldName);
                        }

                        // Handle the error when it does not exist and ignore it.
                        catch (Exception ex)
                        {
                            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                        }
                    }
                }

                // Write the new settings to the registry
                SaveKey();
            }
        }

        private void DetailItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            SaveItem((DetailItem)sender);
        }

        private void SaveItem(DetailItem detail)
        {
            using (Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(baseKey(registryLocation)))
            {
                if (reg != null)
                {
                    reg.SetValue(detail.Key, detail.Value);
                }
            }
        }

        /// <summary>
        /// Record the key list to the database showing the relative order of the items
        /// </summary>
        /// <remarks></remarks>
        public void SaveKey()
        {
            // Build a text buffer with the key strings in the order that they appear.
            string KeyString = string.Join(",", mruList.ToArray());

            // Record the new string of key items
            using (Microsoft.Win32.RegistryKey reg = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(baseKey(registryLocation), true))
            {
                if (reg != null)
                {
                    reg.SetValue(sequenceKey, KeyString);
                }
            }
        }

        /// <summary>
        /// Locate the Item value by the ID
        /// </summary>
        /// <param name="searchKey">The key to the item</param>
        /// <value></value>
        /// <returns>A pointer to the item or NOTHING if the item is not found</returns>
        /// <remarks></remarks>
        private DetailItem FindKey(string searchKey)
        {
            foreach (DetailItem item in ItemList)
            {
                if (item.Key.CompareTo(searchKey) == 0)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        /// Locate the Item value by the value string
        /// </summary>
        /// <param name="searchValue">The value for the item</param>
        /// <value></value>
        /// <returns>A pointer to the item or NOTHING if the item is not found</returns>
        /// <remarks></remarks>
        private DetailItem FindValue(string searchValue)
        {
            searchValue = searchValue.ToUpper();
            foreach (DetailItem item in ItemList)
            {
                if (item.Value.CompareTo(searchValue) == 0)
                {
                    return item;
                }
            }
            return null;
        }

        /// <summary>
        /// Retrieve the list of items in the order defined by the MRU
        /// </summary>
        /// <value></value>
        /// <returns>A list of strings corresponding to the items. Item(0) is the most recent. Item(n-1) is the least.</returns>
        /// <remarks></remarks>
        public string[] GetList
        {
            get
            {
                System.Collections.Generic.List<string> NewList = new System.Collections.Generic.List<string>();
                foreach (string Item in mruList)
                {
                    DetailItem Detail = FindKey(Item);
                    if (Detail != null)
                    {
                        NewList.Add(Detail.Value);
                    }
                }
                return NewList.ToArray();
            }
        }

        /// <summary>
        /// Insert the new item at the top of the MRU list. All others are pushed down a slot.
        /// </summary>
        /// <param name="Value">The value for the new item.</param>
        /// <remarks>The current value may or may not be in the list. if it is in the list then it is simply moved to the top. Otherwise, we
        /// add a new item to the list. if the list is too large, the oldest item is discarded.</remarks>
        public void InsertTopMost(string Value)
        {
            string selectedKey;
            Value = Value.ToUpper();

            // Determine if the item is in the list. if so, just rearrange the keys
            DetailItem detail = FindValue(Value);

            // if there is no pointer then add one to the list. Ensure that the list is not too large.
            if (detail == null)
            {
                if (mruList.Count == MaxCount)
                {
                    selectedKey = mruList[MaxCount - 1];
                    detail = FindKey(selectedKey);
                }
                else
                {
                    detail = new DetailItem(mruList.Count);
                    detail.PropertyChanged += DetailItem_PropertyChanged;
                    ItemList.Add(detail);
                }
                Debug.Assert(detail != null);

                // Update the value for the new item.
                detail.Value = Value;
            }

            // Remove the current item from the list in whatever position it is in.
            selectedKey = detail.Key;
            for (Int32 itemIndex = 0; itemIndex <= mruList.Count - 1; itemIndex += 1)
            {
                string searchKey = mruList[itemIndex];
                if (selectedKey == searchKey)
                {
                    mruList.RemoveAt(itemIndex);
                    break;
                }
            }

            // Insert the new key at the top of the list
            mruList.Insert(0, selectedKey);
        }

        /// <summary>
        /// Insert the new client as a value for the list.
        /// </summary>
        /// <param name="Value">The client ID number</param>
        /// <remarks></remarks>
        public void InsertTopMost(Int32 Value)
        {
            InsertTopMost(String.Format("{0:0000000}",Value));
        }

        #region IDisposable Support
        private bool disposedValue;  //To detect redundant calls
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    SaveKey();
                }
            }
            disposedValue = true;
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose() // Implements IDisposable.Dispose
        {
            // Do not change this code.  Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}