﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DebtPlus.Svc.Payment
{
    public static class FirstData
    {
        public static readonly string DEFAULT_ProviderName = "first_data";
        public static readonly string DEFAULT_AccountDebit = "debit";
        public static readonly string DEFAULT_ReferenceCode = "BKAttorney";
        public static readonly string DEFAULT_Memo = "WEB";

        private static MMI.Payment.IGatewayConfiguration config = null;

        /// <summary>
        /// Read the configuration values from the config file and create the "config" object.
        /// </summary>
        static FirstData()
        {
            var configItems         = new System.Collections.Generic.Dictionary<string, string>();

            // Process all of the keys in the payments section.
            var configurationValues = System.Configuration.ConfigurationManager.GetSection("payments") as System.Collections.Specialized.NameValueCollection;
            foreach(string key in configurationValues.AllKeys)
            {
                configItems.Add(key, configurationValues.Get(key));
            }

            // Translate the list to the config object
            config = new MMI.Payment.AppSettingsConfiguration(configItems);
        }

        public interface IResponse
        {
            MMI.Payment.Status.Code StatusCode { get; }
        }

        public class FailResponse : IResponse
        {
            public MMI.Payment.Status.Code StatusCode { get; private set; }
            public string Message { get; private set; }
            public FailResponse() { }

            public FailResponse(string Message) : this()
            {
                this.StatusCode = MMI.Payment.Status.Code.PROVIDER_ERROR;
                this.Message = Message;
            }

            public FailResponse(MMI.Payment.Status.Code StatusCode, string Message) : this()
            {
                if (StatusCode == MMI.Payment.Status.Code.SUCCESS)
                {
                    throw new ArgumentException("StatusCode can not be SUCCESS");
                }
                this.StatusCode = StatusCode;
                this.Message = Message;
            }
        }

        public class SuccessResponse : IResponse
        {
            public MMI.Payment.Status.Code StatusCode
            {
                get
                {
                    return MMI.Payment.Status.Code.SUCCESS;
                }
            }

            public string ReferenceCode { get; set; }
            public string TransactionID { get; set; }
            public SuccessResponse() { }
            public SuccessResponse(string referenceCode, string transactionId)
                : this()
            {
                ReferenceCode = referenceCode;
                TransactionID = transactionId;
            }
        }

        public static IResponse Submit(CreditCard CardData, Payment ChargeData)
        {
            return Submit(DEFAULT_ProviderName, DEFAULT_AccountDebit, CardData, ChargeData);
        }

        public static IResponse Submit(string ProviderName, string AccountDebit, CreditCard CardData, Payment ChargeData)
        {
            return Submit(ProviderName, AccountDebit, DEFAULT_ReferenceCode, DEFAULT_Memo, CardData, ChargeData);
        }

        public static IResponse Submit(string ProviderName, string AccountDebit, string ReferenceCode, string Memo, CreditCard CardData, Payment ChargeData)
        {
            // Our customer information
            var CustomerData  = new MMI.Payment.Customer()
                {
                    Id        = CardData.Id,
                    FirstName = CardData.FirstName,
                    LastName  = CardData.LastName,
                    Street1   = CardData.Address,
                    Street2   = "",
                    City      = CardData.City,
                    State     = CardData.State,
                    Zip       = CardData.Zipcode
                };

            // The card owner information
            var CreditCardData   = new MMI.Payment.CreditCard()
                {
                    CardNumber       = CardData.CardNumber,
                    Type             = MMI.Payment.AccountType.CREDIT,
                    ExpirationMonth  = CardData.Expiration_Month.ToString("00"),
                    ExpirationYear   = CardData.Expiration_Year.ToString("0000"),
                    VerificationCode = CardData.CVV,
                    CardHolder       = CardData.FullName
                };

            // The Payment data
            var PaymentData   = new MMI.Payment.PaymentInfo()
                {
                    Customer      = CustomerData,
                    PaymentMethod = CreditCardData,
                    Amount        = ChargeData.Amount
                };

            // Do the processing request
            try
            {
                var gateway = new MMI.Payment.Gateway(FirstData.config);
                var status  = gateway.ProccessTransaction(ProviderName, AccountDebit, MMI.Payment.TransactionType.SALE, PaymentData, null, ReferenceCode, Memo);
                if (status.StatusCode == MMI.Payment.Status.Code.SUCCESS)
                {
                    return new SuccessResponse(status.ReferenceCode, status.TransactionId);
                }

                return new FailResponse(status.StatusCode, status.ErrorMessage);
            }
            catch (Exception ex)
            {
                return new FailResponse(MMI.Payment.Status.Code.INTERNAL_ERROR, "EXCEPTION:" + ex.Message);
            }
        }
    }
}
