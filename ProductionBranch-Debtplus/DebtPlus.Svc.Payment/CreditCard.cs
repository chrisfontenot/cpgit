﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DebtPlus.Svc.Payment
{
    public class CreditCard : Owner
    {
        public CreditCard() : base() { }

        // Information about the credit card as a request object
        public virtual string PaymentMedium { get; set; }
        public virtual string CreditCardType { get; set; }
        public virtual string CardStatusType { get; set; }

        public virtual string CardNumber { get; set; }
        public virtual string CVV { get; set; }
        public virtual Int32 Expiration_Month { get; set; }
        public virtual Int32 Expiration_Year { get; set; }
    }
}
