﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DebtPlus.Svc.Payment
{
    public static class SubmissionBatch
    {
        public static System.Nullable<System.DateTime> SubmissionDate { get; private set; }
        public static System.Guid BatchID { get; private set; }

        static SubmissionBatch()
        {
            BatchID = System.Guid.NewGuid();
            SubmissionDate = DateTime.Now;
        }
    }
}
