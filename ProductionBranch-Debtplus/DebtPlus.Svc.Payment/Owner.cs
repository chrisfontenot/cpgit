﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DebtPlus.Svc.Payment
{
    public class Owner : System.IDisposable
    {
        public virtual string Id { get; set; }

        public virtual string EmailAddress { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Address { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Zipcode { get; set; }

        public virtual string FullName
        {
            get
            {
                return ((FirstName ?? "") + " " + (LastName ?? "")).Trim();
            }
        }

        // Storage Management
        public Owner() { }
        ~Owner()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            try
            {
                Dispose(true);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void Dispose(bool Disposing)
        {
        }
    }
}
