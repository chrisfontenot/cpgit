﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DebtPlus.Svc.Payment
{
    public class Payment : IDisposable
    {
        public virtual string InvoiceNumber { get; set; }
        public virtual Decimal Amount { get; set; }
        public virtual DateTime SettlementDate { get; set; }

        // Information about the credit card as a response object
        public virtual string Confirmation { get; set; }
        public virtual string ResponseStatus { get; set; }
        public virtual string ResponseMessage { get; set; }

        // Storage Management
        public Payment()
        {

            // Default the settlement date to tomorrow (excluding weekends)
            SettlementDate = DateTime.Now.Date.AddDays(1);
            switch (SettlementDate.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    SettlementDate = SettlementDate.AddDays(2);
                    break;
                case DayOfWeek.Sunday:
                    SettlementDate = SettlementDate.AddDays(1);
                    break;
            }
        }

        ~Payment()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            try
            {
                Dispose(true);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }

        protected virtual void Dispose(bool Disposing)
        {
        }
    }
}
