﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DebtPlus.Svc.Payment
{
    public static class helpers
    {
        /// <summary>
        /// Return the rightmost "CharCount" characters from the string.
        /// </summary>
        /// <param name="inputString">The string to be used as a substring</param>
        /// <param name="CharCount">The number of characters desired</param>
        /// <returns>If the string is too short or is null, the original input string is returned. This is
        /// not the same processing logic as just using SubString which would fault in these conditions.</returns>
        public static string Right(this string inputString, int CharCount)
        {
            if (CharCount < 1)
            {
                throw new ArgumentException("Count of characters must be at least 1");
            }

            // Use Substring if appropriate
            if (!string.IsNullOrEmpty(inputString) && (inputString.Length > CharCount))
            {
                return inputString.Substring(inputString.Length - CharCount);
            }

            return inputString;
        }
    }
}
