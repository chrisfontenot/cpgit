﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Vendor.Widgets.Search;

namespace DebtPlus.Desktop.CS.Vendor.Update.Utility
{
    public class Mainline : IDesktopMainline
    {
        public void OldAppMain(string[] args)
        {
            var ap = new VendorUpdateArgParser();
            if (!ap.Parse(args))
            {
                return;
            }

            // Start a thread to return to the menu utility as soon as possible
            Thread thrd = new Thread(new ParameterizedThreadStart(UpdateProcedure));
            thrd.SetApartmentState(ApartmentState.STA);
            thrd.IsBackground = false;
            thrd.Name = "VendorUpdate";
            thrd.Start(ap);
        }

        private void UpdateProcedure(object obj)
        {
            var ap = obj as VendorUpdateArgParser;
            if (ap == null)
            {
                return;
            }

            // If a search operation is desired then do it now.
            Int32 VendorID = default(Int32);
            if (ap.VendorID.HasValue)
            {
                VendorID = ap.VendorID.Value;
            }
            else
            {
                using (var search = new VendorSearchClass())
                {
                    if (search.PerformVendorSearch(null) != DialogResult.OK)
                    {
                        return;
                    }

                    if (!search.Vendor.HasValue)
                    {
                        return;
                    }

                    VendorID = search.Vendor.Value;
                }
            }

            // Do the editing on the vendor information
            var bc = new DebtPlus.LINQ.BusinessContext();
            try
            {
                var vendorRecord = bc.vendors.Where(s => s.Id == VendorID).FirstOrDefault();
                if (vendorRecord == null)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The vendor is no longer in the system. The edit operation is cancelled.", "Sorry, can not find the vendor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Update the MRU with the new creditor so that it will show in the list
                using (var mru = new DebtPlus.Data.MRU("Vendors"))
                {
                    mru.InsertTopMost(vendorRecord.Label);
                    mru.SaveKey();
                }

                // If the alerts are not suppressed then show them now.
                if (!ap.NoAlerts)
                {
                    // Display the alert notes for the vendor
                    var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(noteProcedure));
                    thrd.SetApartmentState(ApartmentState.STA);
                    thrd.IsBackground = true;
                    thrd.Name = "VendorNotes";
                    thrd.Start(vendorRecord);
                }

                // Add the handler to generate the system note
                bc.BeforeSubmitChanges += bc.RecordSystemNoteHandler;

                // We have a creditor record. Do the edit operation on the new creditor
                using (var frm = new DebtPlus.UI.Vendor.Update.Forms.Form_VendorUpdate(bc, vendorRecord, false))
                {
                    frm.ShowDialog();
                }

                // Submit the changes before we leave.
                bc.SubmitChanges();
            }
            finally
            {
                bc.BeforeSubmitChanges -= bc.RecordSystemNoteHandler;
                bc.Dispose();
            }
        }

        /// <summary>
        /// Display the alert notes for the vendor
        /// </summary>
        /// <param name="obj">Pointer to the current vendor record</param>
        private void noteProcedure(object obj)
        {
            // Convert the parameter to a vendor record
            var vendorRecord = obj as DebtPlus.LINQ.vendor;
            if (vendorRecord == null)
            {
                return;
            }

            // Generate the notes for the vendor
            using (var noteClass = new DebtPlus.Notes.AlertNotes.Vendor())
            {
                noteClass.ShowAlerts(vendorRecord.Id);
            }
        }
    }
}
