﻿using System;

namespace DebtPlus.Desktop.CS.Vendor.Update.Utility
{
    internal class VendorUpdateArgParser : DebtPlus.Utils.ArgParserBase
    {
        private bool _Search = false;

        /// <summary>
        /// Return the search status
        /// </summary>
        public bool Search
        {
            get { return _Search; }
        }

        /// <summary>
        /// Return the Vendor ID
        /// </summary>
        public Int32? VendorID { get; private set; }

        /// <summary>
        /// "-a" : Do not display alert notes
        /// </summary>
        public bool NoAlerts { get; private set; }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public VendorUpdateArgParser()
            : base(new string[] {
            "a",
			"s",
			"c"
		})
        {
            VendorID = null;
        }

        /// <summary>
        /// Display error information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
        }

        /// <summary>
        /// Handle an option switch
        /// </summary>
        protected override Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            switch (switchSymbol)
            {
                case "?":
                    return DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;

                case "a":
                    NoAlerts = true;
                    return DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

                default:
                    return DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }
        }

        /// <summary>
        /// Handle a non-switched option
        /// </summary>
        protected override Utils.ArgParserBase.SwitchStatus  OnNonSwitch(string value)
        {
            return SetVendor(value);
        }

        /// <summary>
        /// Set the creditor for processing
        /// </summary>
        private SwitchStatus SetVendor(string Value)
        {
            var ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex(@"\d+");
            if (rx.IsMatch(Value))
            {
                VendorID = Int32.Parse(Value);
            }
            else
            {
                AppendErrorLine("Invalid Vendor ID: " + Value);
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
            }
            return ss;
        }
    }
}
