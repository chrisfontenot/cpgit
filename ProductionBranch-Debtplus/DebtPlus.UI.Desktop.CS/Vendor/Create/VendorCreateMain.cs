﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Diagnostics;
using DebtPlus.UI.Vendor.Create;
using DebtPlus.UI.Vendor.Update;
using System.Windows.Forms;

namespace DebtPlus.Desktop.CS.Vendor.Create.Utility
{
    internal class VendorCreateMain : System.IDisposable
    {
        DebtPlus.LINQ.BusinessContext bc = new DebtPlus.LINQ.BusinessContext();
        private string VendorID = System.String.Empty;
        DebtPlus.LINQ.vendor vendorRecord = null;

        public VendorCreateMain(ArgParser ap)
            : base()
        {
        }

        public void ShowDialog()
        {
            // Do the create creditor function. It will create the vendor on the database at this point.
            Int32? VendorID = null;
            using (var createClass = new DebtPlus.UI.Vendor.Create.CreateVendorClass())
            {
                if (createClass.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                VendorID = createClass.Vendor;
                if (!VendorID.HasValue)
                {
                    return;
                }
            }

            // Do the edit operation on the new creditor
            try
            {
                vendorRecord = bc.vendors.Where(s => s.Id == VendorID.Value).FirstOrDefault();
                if (vendorRecord == null)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The vendor is no longer in the system. The edit operation is cancelled.", "Sorry, can not find the vendor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Update the MRU with the new creditor so that it will show in the list
                using (var mru = new DebtPlus.Data.MRU("Vendors"))
                {
                    mru.InsertTopMost(vendorRecord.Label);
                    mru.SaveKey();
                }

                bc.BeforeSubmitChanges += bc.RecordSystemNoteHandler;

                // We have a creditor record. Do the edit operation on the new creditor
                using (var frm = new DebtPlus.UI.Vendor.Update.Forms.Form_VendorUpdate(bc, vendorRecord, true))
                {
                    frm.ShowDialog();
                }

                // Submit the changes before we leave.
                bc.SubmitChanges();
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading vendor information");
            }
        }

#region "IDisposable Support"
        // To detect redundant calls
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    if (bc != null) bc.Dispose();
                }
                bc = null;
            }
            this.disposedValue = true;
        }

        /// <summary>
        /// Finalize the disposal of the current class
        /// </summary>
        ~VendorCreateMain()
        {
            Dispose(false);
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            try
            {
                Dispose(true);
            }
            finally
            {
                GC.SuppressFinalize(this);
            }
        }
        #endregion
    }
}
