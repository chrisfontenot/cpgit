﻿namespace DebtPlus.Desktop.CS.Vendor.Create.Utility
{
    internal class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Client for the update operation
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser()
            : base(new string[] { "t" })
        {
        }

        /// <summary>
        /// Process the switch options
        /// </summary>
        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "t":
                    Type = switchValue;
                    break;

                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
        }
    }
}
