using System;

namespace DebtPlus.UI.Desktop.CS.Proposals.Delete
{
    internal class ProposalDeleteArgParser : DebtPlus.Utils.ArgParserBase
    {
        public ProposalDeleteArgParser() : base(new string[] { })
        {
        }

        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo));
            }

            AppendErrorLine("Usage: DebtPlus.Proposals.Delete.exe");
        }
    }
}