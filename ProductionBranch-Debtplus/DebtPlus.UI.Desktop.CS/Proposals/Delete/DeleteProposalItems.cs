using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Proposals.Delete
{
    internal partial class DeleteProposalItems : DevExpress.XtraEditors.XtraUserControl
    {
        public DeleteProposalItems() : base()
        {
            InitializeComponent();

            // Register the handlers if possible
            if (!DebtPlus.Configuration.DesignMode.IsInDesignMode())
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Button_Apply.Click         += Button_Apply_Click;
            Button_OK.Click            += Button_OK_Click;
            Button_Cancel.Click        += Button_Cancel_Click;
            Button_Remove.Click        += Button_Remove_Click;
            Button_All.Click           += Button_All_Click;
            GridView1.SelectionChanged += GridView1_SelectionChanged;
            GridView1.DoubleClick      += GridView1_DoubleClick;
            Load                       += MyGridControl_Load;
            GridView1.Layout           += LayoutChanged;
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_Apply.Click         -= Button_Apply_Click;
            Button_OK.Click            -= Button_OK_Click;
            Button_Cancel.Click        -= Button_Cancel_Click;
            Button_Remove.Click        -= Button_Remove_Click;
            Button_All.Click           -= Button_All_Click;
            GridView1.SelectionChanged -= GridView1_SelectionChanged;
            GridView1.DoubleClick      -= GridView1_DoubleClick;
            Load                       -= MyGridControl_Load;
            GridView1.Layout           -= LayoutChanged;
        }

        public event System.EventHandler OK_Click;

        private System.Data.DataSet ds = new System.Data.DataSet("ds");

        /// <summary>
        /// Selected Proposal Batch ID
        /// </summary>
        private System.Int32 _batch = -1;

        [Description("Batch ID selected for the transactions"), Browsable(false), Category("DebtPlus"), DefaultValue(typeof(System.Int32), "-1")]
        public System.Int32 Batch
        {
            get { return _batch; }
            set { _batch = value; }
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.GridControl GridControl1;

        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_creditor_proposal;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_account_number;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_name;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        internal DevExpress.XtraEditors.SimpleButton Button_Remove;
        internal DevExpress.XtraEditors.SimpleButton Button_All;
        internal DevExpress.XtraEditors.SimpleButton Button_Apply;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_client_creditor_proposal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client_creditor_proposal.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_account_number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Button_Remove = new DevExpress.XtraEditors.SimpleButton();
            this.Button_All = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Apply = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(400, 256);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_client_creditor_proposal,
                this.GridColumn_client,
                this.GridColumn_creditor,
                this.GridColumn_account_number,
                this.GridColumn_creditor_name,
                this.GridColumn_date_created
            });
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView1.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView1.OptionsLayout.StoreAllOptions = true;
            this.GridView1.OptionsLayout.StoreAppearance = true;
            this.GridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.GridView1.OptionsSelection.MultiSelect = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.GridView1.ViewCaption = "This is the view caption area";
            //
            //GridColumn_client_creditor_proposal
            //
            this.GridColumn_client_creditor_proposal.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client_creditor_proposal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_creditor_proposal.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client_creditor_proposal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_creditor_proposal.Caption = "ID";
            this.GridColumn_client_creditor_proposal.CustomizationCaption = "Record ID";
            this.GridColumn_client_creditor_proposal.DisplayFormat.FormatString = "f0";
            this.GridColumn_client_creditor_proposal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_creditor_proposal.FieldName = "client_creditor_proposal";
            this.GridColumn_client_creditor_proposal.GroupFormat.FormatString = "f0";
            this.GridColumn_client_creditor_proposal.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_creditor_proposal.Name = "GridColumn_client_creditor_proposal";
            this.GridColumn_client_creditor_proposal.OptionsColumn.AllowEdit = false;
            this.GridColumn_client_creditor_proposal.ToolTip = "Record ID for the proposal record";
            //
            //GridColumn_client
            //
            this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.Caption = "Client";
            this.GridColumn_client.CustomizationCaption = "Client ID";
            this.GridColumn_client.DisplayFormat.FormatString = "0000000";
            this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.FieldName = "client";
            this.GridColumn_client.GroupFormat.FormatString = "0000000";
            this.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.Name = "GridColumn_client";
            this.GridColumn_client.ToolTip = "Client ID";
            this.GridColumn_client.Visible = true;
            this.GridColumn_client.VisibleIndex = 0;
            this.GridColumn_client.Width = 61;
            //
            //GridColumn_creditor
            //
            this.GridColumn_creditor.Caption = "Creditor";
            this.GridColumn_creditor.CustomizationCaption = "Creditor";
            this.GridColumn_creditor.FieldName = "creditor";
            this.GridColumn_creditor.Name = "GridColumn_creditor";
            this.GridColumn_creditor.ToolTip = "Creditor ID";
            this.GridColumn_creditor.Visible = true;
            this.GridColumn_creditor.VisibleIndex = 1;
            this.GridColumn_creditor.Width = 60;
            //
            //GridColumn_account_number
            //
            this.GridColumn_account_number.Caption = "Account";
            this.GridColumn_account_number.CustomizationCaption = "Account Number";
            this.GridColumn_account_number.FieldName = "account_number";
            this.GridColumn_account_number.Name = "GridColumn_account_number";
            this.GridColumn_account_number.ToolTip = "Account Number";
            this.GridColumn_account_number.Visible = true;
            this.GridColumn_account_number.VisibleIndex = 2;
            this.GridColumn_account_number.Width = 116;
            //
            //GridColumn_creditor_name
            //
            this.GridColumn_creditor_name.Caption = "Creditor Name";
            this.GridColumn_creditor_name.CustomizationCaption = "Creditor Name";
            this.GridColumn_creditor_name.FieldName = "creditor_name";
            this.GridColumn_creditor_name.Name = "GridColumn_creditor_name";
            this.GridColumn_creditor_name.ToolTip = "Creditor Name";
            this.GridColumn_creditor_name.Visible = true;
            this.GridColumn_creditor_name.VisibleIndex = 3;
            this.GridColumn_creditor_name.Width = 147;
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.Caption = "Date Created";
            this.GridColumn_date_created.CustomizationCaption = "Date Created";
            this.GridColumn_date_created.DisplayFormat.FormatString = "d";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.GroupFormat.FormatString = "d";
            this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.ToolTip = "Date/Time when proposal was created";
            this.GridColumn_date_created.Visible = true;
            this.GridColumn_date_created.VisibleIndex = 4;
            this.GridColumn_date_created.Width = 101;
            //
            //Button_Remove
            //
            this.Button_Remove.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Remove.Enabled = false;
            this.Button_Remove.Location = new System.Drawing.Point(408, 101);
            this.Button_Remove.Name = "Button_Remove";
            this.Button_Remove.Size = new System.Drawing.Size(75, 23);
            this.Button_Remove.TabIndex = 3;
            this.Button_Remove.Text = "Remove";
            this.Button_Remove.ToolTip = "Click here to remove the selected items from the list";
            //
            //Button_All
            //
            this.Button_All.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_All.Location = new System.Drawing.Point(408, 133);
            this.Button_All.Name = "Button_All";
            this.Button_All.Size = new System.Drawing.Size(75, 23);
            this.Button_All.TabIndex = 4;
            this.Button_All.Tag = "1";
            this.Button_All.Text = "Select All";
            this.Button_All.ToolTip = "Select all of the items in the list";
            //
            //Button_Apply
            //
            this.Button_Apply.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Apply.Enabled = false;
            this.Button_Apply.Location = new System.Drawing.Point(408, 216);
            this.Button_Apply.Name = "Button_Apply";
            this.Button_Apply.Size = new System.Drawing.Size(75, 23);
            this.Button_Apply.TabIndex = 5;
            this.Button_Apply.Text = "Apply";
            this.Button_Apply.ToolTip = "Click here to apply the deletions and continue to work this batch";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.Location = new System.Drawing.Point(408, 40);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Tag = "";
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel any deletions (leaving them still in the batch) and return t" + "o the list of batches";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(408, 8);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 1;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to apply the changes and return to the list of batches";
            //
            //DeleteProposalItems
            //
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Button_Apply);
            this.Controls.Add(this.Button_All);
            this.Controls.Add(this.Button_Remove);
            this.Controls.Add(this.GridControl1);
            this.Name = "DeleteProposalItems";
            this.Size = new System.Drawing.Size(488, 256);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Load the list of batches for the ACH files
        /// </summary>
        [Browsable(false)]
        public void Process()
        {
            ds.Clear();

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT client_creditor_proposal, client, creditor, client_creditor, date_created, creditor_name, account_number FROM view_proposal_proof WITH (NOLOCK) WHERE proposal_batch_id=@proposal_batch_id";
                    cmd.Parameters.Add("@proposal_batch_id", SqlDbType.Int).Value = Batch;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "proposal_items");
                    }
                }

                // Bind the data to the grid
                GridControl1.DataSource = ds.Tables[0].DefaultView;
                GridControl1.RefreshDataSource();

                // Enable the ALL button if there is something to process
                Button_All.Enabled = GridView1.RowCount > 0;
                Button_Apply.Enabled = false;
                Button_OK.Enabled = false;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Proposal Batch Items");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Process the apply button
        /// </summary>
        private void Button_Apply_Click(System.Object sender, System.EventArgs e)
        {
            SaveChanges();
        }

        /// <summary>
        /// Process the OK button
        /// </summary>
        private void Button_OK_Click(System.Object sender, System.EventArgs e)
        {
            SaveChanges();
            if (OK_Click != null)
            {
                OK_Click(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Process the CANCEL button
        /// </summary>
        private void Button_Cancel_Click(object sender, System.EventArgs e)
        {
            if (OK_Click != null)
            {
                OK_Click(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Process the CANCEL button
        /// </summary>

        private void Button_Remove_Click(object sender, System.EventArgs e)
        {
            // Delete the rows from the table. The actual deletion will be done when we do the apply.
            while (GridView1.SelectedRowsCount > 0)
            {
                System.Int32 RowHandle = GridView1.GetSelectedRows()[0];
                System.Data.DataRow row = GridView1.GetDataRow(RowHandle);
                if (row != null)
                {
                    row.Delete();
                }
            }

            // Disable the apply/ok buttons
            Button_Apply.Enabled = HasChanges();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        /// <summary>
        /// Process the Select All/Clear All conditions
        /// </summary>

        private void Button_All_Click(object sender, System.EventArgs e)
        {
            if (Convert.ToInt32(Button_All.Tag) == 0)
            {
                GridView1.ClearSelection();
            }
            else
            {
                GridView1.SelectAll();
            }
        }

        /// <summary>
        /// Save any changes to the database
        /// </summary>
        internal void SaveChanges()
        {
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                // Build a command to delete the proposals from the batch
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "xpr_proposal_item_remove";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@client_creditor_proposal", SqlDbType.Int, 0, "client_creditor_proposal");

                    // Do the delete operation to remove the item(s) from the batch.
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                    {
                        da.DeleteCommand = cmd;
                        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                        if (da.Update(ds.Tables[0]) > 0)
                        {
                            DebtPlus.Data.Forms.DebtPlusForm.DoMessageBeep(DebtPlus.Data.Forms.DebtPlusForm.MessageBeepEnum.Simple);
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error clearing proposal from batch");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            // Disable the apply/ok buttons
            Button_Apply.Enabled = HasChanges();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        /// <summary>
        /// Determine if the database has any changes
        /// </summary>
        private bool HasChanges()
        {
            // Determine if there are any deleted items in the update table
            using (System.Data.DataView vue = new System.Data.DataView(ds.Tables[0], string.Empty, string.Empty, DataViewRowState.Deleted))
            {
                return vue.Count > 0;
            }
        }

        /// <summary>
        /// If the selection changes then change the select button
        /// </summary>

        private void GridView1_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            if (GridView1.GetSelectedRows().GetUpperBound(0) >= 0)
            {
                Button_Remove.Enabled = true;
                Button_All.Text = "Clear All";
                Button_All.Tag = 0;
                Button_All.ToolTip = "Click here to deselect (un-highlight) all proposals in the list";
            }
            else
            {
                Button_Remove.Enabled = false;
                Button_All.Text = "Select All";
                Button_All.Tag = 1;
                Button_All.ToolTip = "Click here to select (highlight) all proposals in the list";
            }
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string BasePath = string.Format("{0}{1}DebtPlus", System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar);
            return System.IO.Path.Combine(BasePath, "Proposals.Delete");
        }

        /// <summary>
        /// When loading the control, restore the layout from the file
        /// </summary>
        private void MyGridControl_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ReloadGridControlLayout();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Reload the layout of the grid control if needed
        /// </summary>
        protected void ReloadGridControlLayout()
        {
            // Find the base path to the saved file location
            string PathName = XMLBasePath();
            try
            {
                string FileName = System.IO.Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                if (System.IO.File.Exists(FileName))
                {
                    GridView1.RestoreLayoutFromXml(FileName);
                }
            }
#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, System.EventArgs e)
        {
            string PathName = XMLBasePath();
            if (!System.IO.Directory.Exists(PathName))
            {
                System.IO.Directory.CreateDirectory(PathName);
            }

            string FileName = System.IO.Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
            GridView1.SaveLayoutToXml(FileName);
        }

        /// <summary>
        /// Handle a double-click event on the grid control
        /// </summary>
        private void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // Find the record to be edited from the list
            if (RowHandle >= 0)
            {
                DataRowView drv = GridView1.GetRow(RowHandle) as DataRowView;
                if (drv == null)
                {
                    return;
                }

                // If we are in a cell then we can look for the cell
                if (hi.InRowCell && !hi.InColumnPanel)
                {
                    string fieldName = hi.Column.FieldName ?? string.Empty;
                    if (fieldName == "client")
                    {

                        // Find the client from the dataset
                        Int32? clientID = DebtPlus.Utils.Nulls.v_Int32(drv["client"]);
                        if (!clientID.HasValue)
                        {
                            return;
                        }

                        // Display the client information
                        ShowClient(clientID.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Display the indicated client to the user
        /// </summary>
        /// <param name="ClientID"></param>
        private void ShowClient(Int32 ClientID)
        {
            // Start a thread to show the client
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(ShowClientThread));
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Name = "ClientUpdate";
            thrd.Start(ClientID);
        }

        /// <summary>
        /// Display the client to the user
        /// </summary>
        private void ShowClientThread(object obj)
        {
            // Display the alert notes first
            ShowAlertNotesThread(obj);

            Int32 ClientID = (Int32)obj;
            if (ClientID > 0)
            {
                using (var cuc = new DebtPlus.UI.Client.Service.ClientUpdateClass())
                {
                    cuc.ShowEditDialog(ClientID, false);
                }
            }
        }

        /// <summary>
        /// Display the alert notes to the user
        /// </summary>
        private void ShowAlertNotesThread(object obj)
        {
            Int32 ClientID = (Int32)obj;
            if (ClientID >= 0)
            {
                using (var notes = new DebtPlus.Notes.AlertNotes.Client())
                {
                    notes.ShowAlerts(ClientID);
                }
            }
        }
    }
}