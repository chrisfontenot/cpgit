#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.FormLib.Proposals;

namespace DebtPlus.UI.Desktop.CS.Proposals.Delete
{
    internal partial class Form_Proposals_Delete : DebtPlus.Data.Forms.DebtPlusForm
    {
        // private System.Int32 Batch = -1;

        public Form_Proposals_Delete() : base()
        {
            InitializeComponent();
            this.Load += Proposals_Delete_Load;
            ProposalBatchSelect1.Cancelled += ProposalBatchSelect1_Cancelled;
            ProposalBatchSelect1.Selected += ProposalBatchSelect1_Selected;
            base.Closing += Proposals_Delete_Closing;
            DeleteProposalItems1.OK_Click += DeleteProposalItems1_OK_Click;
            DeleteProposalItems1.Load += DeleteProposalItems1_Load;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal OpenControl ProposalBatchSelect1;

        internal DeleteProposalItems DeleteProposalItems1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.ProposalBatchSelect1 = new OpenControl();
            this.DeleteProposalItems1 = new DeleteProposalItems();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //ProposalBatchSelect1
            //
            this.ProposalBatchSelect1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProposalBatchSelect1.Location = new System.Drawing.Point(0, 0);
            this.ProposalBatchSelect1.Name = "ProposalBatchSelect1";
            this.ProposalBatchSelect1.Size = new System.Drawing.Size(577, 266);
            this.ProposalBatchSelect1.TabIndex = 0;
            //
            //DeleteProposalItems1
            //
            this.DeleteProposalItems1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DeleteProposalItems1.Location = new System.Drawing.Point(0, 0);
            this.DeleteProposalItems1.Name = "DeleteProposalItems1";
            this.DeleteProposalItems1.Size = new System.Drawing.Size(577, 266);
            this.DeleteProposalItems1.TabIndex = 1;
            this.DeleteProposalItems1.Visible = false;
            //
            //Proposals_Delete
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(577, 266);
            this.Controls.Add(this.DeleteProposalItems1);
            this.Controls.Add(this.ProposalBatchSelect1);
            this.Name = "Proposals_Delete";
            this.Text = "List Of Proposals In The Batch";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void Proposals_Delete_Load(System.Object sender, System.EventArgs e)
        {
            // Request the proposal batch to be processed
            ProposalBatchSelect1.RefreshList();
            Text = "Select the Proposal Batch";
        }

        private void ProposalBatchSelect1_Cancelled(object sender, System.EventArgs e)
        {
            Close();
        }

        private void ProposalBatchSelect1_Selected(object sender, System.EventArgs e)
        {
            // Process the information from the sub-control
            System.Int32 batch = 0;
            var _with1 = ProposalBatchSelect1;
            batch = _with1.Batch_ID;
            _with1.Visible = false;

            // Start the processing for the batch selection information
            var _with2 = DeleteProposalItems1;
            _with2.Visible = true;
            _with2.Batch = batch;
            _with2.Process();

            Text = "Delete Proposals From Batch";
        }

        private void Proposals_Delete_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // If we are closing then apply the changes to the database
            if (DeleteProposalItems1.Visible)
                DeleteProposalItems1.SaveChanges();
        }

        private void DeleteProposalItems1_OK_Click(object sender, System.EventArgs e)
        {
            DeleteProposalItems1.Visible = false;
            ProposalBatchSelect1.Visible = true;
            Text = "Select the Proposal Batch";
        }

        private void DeleteProposalItems1_Load(System.Object sender, System.EventArgs e)
        {
            DeleteProposalItems1.Button_Apply.Visible = false;
        }
    }
}