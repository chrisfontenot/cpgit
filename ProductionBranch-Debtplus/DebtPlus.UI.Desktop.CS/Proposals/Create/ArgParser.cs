using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Proposals.Create
{
    public partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Processing mode values
        /// </summary>
        public enum ModeEnum
        {
            Unspecified = 0,
            Create      = 1,
            Close       = 2
        }

        private ModeEnum _mode = ModeEnum.Unspecified;

        /// <summary>
        /// Processing mode
        /// </summary>
        public ModeEnum Mode
        {
            get { return _mode; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] { })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine("Usage: " + fname + " CREATE | CLOSE ")
        }

        /// <summary>
        /// The parsing operation is complete
        /// </summary>
        protected override SwitchStatus OnDoneParse()
        {
            if (Mode == ModeEnum.Unspecified)
            {
                using (var frm = new ModeSelectForm())
                {
                    if (frm.ShowDialog() != DialogResult.OK)
                    {
                        return SwitchStatus.YesError;
                    }

                    _mode = (ModeEnum) frm.Parameter_Mode;
                    if (Mode == ModeEnum.Unspecified)
                    {
                        return SwitchStatus.YesError;
                    }
                }
            }

            return SwitchStatus.NoError;
        }

        /// <summary>
        /// Process a non-switch argument
        /// </summary>
        protected override SwitchStatus OnNonSwitch(string value)
        {
            SwitchStatus ss = SwitchStatus.NoError;

            switch (value.ToLower().Trim())
            {
                case "create":
                    _mode = ModeEnum.Create;
                    break;

                case "close":
                    _mode = ModeEnum.Close;
                    break;

                default:
                    ss = SwitchStatus.ShowUsage;
                    break;
            }

            return ss;
        }
    }
}