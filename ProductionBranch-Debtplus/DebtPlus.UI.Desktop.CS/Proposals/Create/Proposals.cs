using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.FormLib.Proposals;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Proposals.Create
{
    public static class Proposals
    {
        /// <summary>
        /// Process the functions
        /// </summary>

        public static void Process(ArgParser ap)
        {
            // Process the mode
            switch (ap.Mode)
            {
                case ArgParser.ModeEnum.Unspecified:
                    break;
                // Do not do anything. The user cancelled the dialog.

                case ArgParser.ModeEnum.Create:
                    CreateBatch();
                    break;

                case ArgParser.ModeEnum.Close:
                    CloseBatch();
                    break;
            }
        }

        /// <summary>
        /// Parameter values for the reports
        /// </summary>
        public static void CloseBatch()
        {
            System.Text.StringBuilder BatchListString = new System.Text.StringBuilder();
            DialogResult answer = default(DialogResult);

            using (OpenForm frm = new OpenForm())
            {
                var _with1 = frm;
                frm.MultipleSelectionAllowed = true;
                answer = frm.ShowDialog();

                // Build the list of batch IDs as a comma-seperated value string.
                if (answer == DialogResult.OK)
                {
                    foreach (Int32 BatchID in frm.SelectedBatches)
                    {
                        BatchListString.AppendFormat(",{0:d}", BatchID);
                    }
                    BatchListString.Remove(0, 1);
                }
            }

            // If successful then set the wait cursor

            if (answer == DialogResult.OK)
            {
                try
                {
                    // Open the database connection and close the proposal batch(es)
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                        {
                            cn.Open();
                            using (var cmd = new SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandText = "xpr_proposal_close";
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@proposal_batch_id", SqlDbType.VarChar, 256).Value = BatchListString.ToString();
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error closing proposal batch");
                }
            }
        }

        /// <summary>
        /// Create a new batch
        /// </summary>
        public static DialogResult CreateBatch()
        {
            using (BatchCreateForm frm = new BatchCreateForm())
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The proposal batch(es) have been created successfully", "Operation complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return DialogResult.OK;
                }
            }

            return DialogResult.Cancel;
        }
    }
}