using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Proposals.Create
{
    internal partial class ModeSelectForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public ModeSelectForm() : base()
        {
            InitializeComponent();
            this.Load += ModeSelectForm_Load;
        }

        #region "Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        public DevExpress.XtraEditors.LabelControl Label1;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.ComboBoxEdit lst_mode;

        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ModeSelectForm));
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.lst_mode = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.lst_mode.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Label1
            //
            this.Label1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Appearance.Options.UseBackColor = true;
            this.Label1.Appearance.Options.UseFont = true;
            this.Label1.Appearance.Options.UseForeColor = true;
            this.Label1.Appearance.Options.UseTextOptions = true;
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(16, 12);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(215, 14);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "What type of processing do you wish to do?";
            this.Label1.ToolTipController = this.ToolTipController1;
            //
            //lst_mode
            //
            this.lst_mode.Location = new System.Drawing.Point(16, 48);
            this.lst_mode.Name = "lst_mode";
            this.lst_mode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.lst_mode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.lst_mode.Size = new System.Drawing.Size(273, 20);
            this.lst_mode.TabIndex = 4;
            this.lst_mode.ToolTipController = this.ToolTipController1;
            //
            //Button_OK
            //
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(58, 88);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 5;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(178, 88);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 6;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //ModeSelectForm
            //
            this.AcceptButton = this.Button_OK;
            this.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(311, 128);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.lst_mode);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ModeSelectForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Select the processing mode";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.lst_mode.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion "Windows Form Designer generated code "

        public System.Int32 Parameter_Mode
        {
            get
            {
                var _with1 = lst_mode;
                return Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)_with1.SelectedItem).value);
            }
        }

        private void ModeSelectForm_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            lst_mode.Properties.Items.Clear();
            lst_mode.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Create a new proposal batch", 1));
            lst_mode.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Close the batch", 2));
            lst_mode.SelectedIndex = 0;
        }
    }
}