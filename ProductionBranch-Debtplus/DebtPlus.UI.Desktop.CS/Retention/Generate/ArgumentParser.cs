namespace DebtPlus.UI.Desktop.CS.Retention.Generate
{
    internal partial class ArgumentParser : DebtPlus.Utils.ArgParserBase
    {
        // Event to create.
        internal System.Int32 EventToGenerate = -1;

        internal string EventToGenerateTitle = string.Empty;

        public ArgumentParser() : base(new string[] { })
        {
        }

        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}