#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Desktop.CS.Retention.Common;

namespace DebtPlus.UI.Desktop.CS.Retention.Generate
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() : base()
        {
        }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ArgumentParser();
                    if (ap.Parse((string[])param))
                    {
                        MainProcessingRoutine(ap);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "RetentionGenerate"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }

        private void MainProcessingRoutine(ArgumentParser ap)
        {
            // Ask for the first field item
            FieldSelection FirstField = new FieldSelection();
            System.Windows.Forms.DialogResult answer = default(System.Windows.Forms.DialogResult);
            using (var frm = new GetEventForm(false, ap, FirstField))
            {
                frm.StartPosition = FormStartPosition.CenterScreen;
                answer = frm.ShowDialog();
            }

            // If there is a field then run the main application. Otherwise, quit.
            if (answer == DialogResult.OK)
            {
                using (RelationForm dlg = new RelationForm(false, FirstField))
                {
                    dlg.Text = "Generate " + ap.EventToGenerateTitle + " Retention Events";
                    answer = dlg.ShowDialog();

                    // If successful with the dialog then try to create the events
                    if (answer == DialogResult.OK)
                    {
                        SelectionClause Result = dlg.SelectionClause();
                        if (Result.Valid)
                        {
                            string Selection = Result.SelectionString;
                            if (Selection.Length > 0)
                                Selection = " WHERE " + Selection;
                            System.Int32 RecordCount = 0;

                            // Build the insert command from all of the bits and pieces
                            string CommandText = "INSERT INTO client_retention_events (client, retention_event, priority, expire_type) SELECT DISTINCT c.client, " + ap.EventToGenerate.ToString() + ", ev.priority,isnull(ev.expire_type,1) FROM clients c INNER JOIN retention_events ev ON ev.retention_event=" + ap.EventToGenerate.ToString();
                            if (Result.UseClientDepositTable)
                                CommandText += " INNER JOIN client_deposits cd ON c.client = cd.client";
                            if (Result.UseClientRetentionEventsTable)
                                CommandText += " INNER JOIN client_retention_events vc ON vc.client = c.client";
                            CommandText += Selection;

                            // Do the record insert operation
                            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            Cursor current_cursor = Cursor.Current;
                            try
                            {
                                Cursor.Current = Cursors.WaitCursor;
                                cn.Open();
                                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.CommandText = CommandText;
                                    cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                                    cmd.Connection = cn;
                                    RecordCount = cmd.ExecuteNonQuery();
                                }

                                // It worked.
                                DebtPlus.Data.Forms.MessageBox.Show(string.Format("{0:d} clients received the events", RecordCount), "Operation completed successfully", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            catch (System.Data.SqlClient.SqlException ex)
                            {
                                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Inserting Retention Events");
                            }
                            finally
                            {
                                if (cn != null)
                                    cn.Dispose();
                                Cursor.Current = current_cursor;
                            }
                        }
                    }
                }
            }
        }
    }
}