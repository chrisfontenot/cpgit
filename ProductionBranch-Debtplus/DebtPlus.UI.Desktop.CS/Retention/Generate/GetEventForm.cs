using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Desktop.CS.Retention.Common;

namespace DebtPlus.UI.Desktop.CS.Retention.Generate
{
    internal partial class GetEventForm : FieldForm
    {
        public GetEventForm() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Process the creation of the field item
        /// </summary>

        private ArgumentParser ap = null;

        public GetEventForm(bool search) : base(search)
        {
            InitializeComponent();
            this.Load += GetEventForm_Load;
            RetentionEvent.EditValueChanged += RetentionEvent_EditValueChanged;
        }

        public GetEventForm(bool search, ArgumentParser ap, FieldSelection FirstField) : this(search)
        {
            this.ap = ap;
            base.NewFieldItem = FirstField;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LookUpEdit RetentionEvent;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.RetentionEvent = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();

            ((System.ComponentModel.ISupportInitialize)this.RetentionEvent.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Button_OK
            //
            this.Button_OK.Location = new System.Drawing.Point(72, 80);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.TabIndex = 4;
            //
            //LookUpEdit1
            //
            this.LookUpEdit1.EditValue = 0;
            this.LookUpEdit1.Location = new System.Drawing.Point(112, 48);
            this.LookUpEdit1.Name = "LookUpEdit1";
            this.LookUpEdit1.TabIndex = 3;
            //
            //Button_Cancel
            //
            this.Button_Cancel.Location = new System.Drawing.Point(160, 80);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.TabIndex = 5;
            //
            //RetentionEvent
            //
            this.RetentionEvent.Location = new System.Drawing.Point(112, 24);
            this.RetentionEvent.Name = "RetentionEvent";
            //
            //RetentionEvent.Properties
            //
            this.RetentionEvent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.RetentionEvent.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)
            });
            this.RetentionEvent.Properties.NullText = "";
            this.RetentionEvent.Properties.ShowFooter = false;
            this.RetentionEvent.Properties.ShowHeader = false;
            this.RetentionEvent.Properties.ShowLines = false;
            this.RetentionEvent.Size = new System.Drawing.Size(176, 20);
            this.RetentionEvent.TabIndex = 1;
            this.RetentionEvent.Properties.SortColumnIndex = 1;
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 24);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(96, 14);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Event to Generate";
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 48);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(75, 16);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Client Selection";
            //
            //GetEventForm
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(306, 112);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.RetentionEvent);
            this.Name = "GetEventForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Event And Initial Selection Item";
            this.Controls.SetChildIndex(this.RetentionEvent, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.LookUpEdit1, 0);
            this.Controls.SetChildIndex(this.Button_OK, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();

            ((System.ComponentModel.ISupportInitialize)this.RetentionEvent.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void GetEventForm_Load(System.Object sender, System.EventArgs e)
        {
            // Load the list of retention events to generate
            System.Data.DataTable tbl = CommonStorage.RetentionEventsTable();
            var _with1 = RetentionEvent;
            var _with2 = _with1.Properties;
            _with2.DataSource = tbl.DefaultView;
            _with2.DisplayMember = "description";
            _with2.ValueMember = "item_key";

            Button_OK.Enabled = !HasErrors();
        }

        protected override bool HasErrors()
        {
            return base.HasErrors() || ap.EventToGenerate <= 0;
        }

        private void RetentionEvent_EditValueChanged(object sender, System.EventArgs e)
        {
            if (RetentionEvent.EditValue != null)
            {
                ap.EventToGenerate = Convert.ToInt32(RetentionEvent.EditValue);
                ap.EventToGenerateTitle = RetentionEvent.Text;
                Button_OK.Enabled = !HasErrors();
            }
        }
    }
}