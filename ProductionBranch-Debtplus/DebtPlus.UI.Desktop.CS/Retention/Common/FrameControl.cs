#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;

namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    internal partial class FrameControl : DevExpress.XtraEditors.XtraUserControl
    {
        public event System.EventHandler FrameUpdated;

        internal FieldSelection CurrentItem = null;

        public FrameControl(ref FieldSelection CurrentItem) : this()
        {
            this.CurrentItem = CurrentItem;

            // Do not allow the combobox items to be changed if this is a "pick one of the follow" type of input
            switch (CurrentItem.EventID)
            {
                case Retention.Enum_Events.EVENT_ACTIVE_STATUS:
                case Retention.Enum_Events.EVENT_OFFICE:
                case Retention.Enum_Events.EVENT_RETENTION_EVENT:
                case Retention.Enum_Events.EVENT_COUNSELOR:
                case Retention.Enum_Events.EVENT_CSR:
                case Retention.Enum_Events.EVENT_REGION:
                    Value1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                    value2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                    break;

                default:
                    Value1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    value2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    break;
            }
        }

        public FrameControl() : base()
        {
            InitializeComponent();

            relation.SelectedIndexChanged += relation_SelectedIndexChanged;
            Value1.SelectedIndexChanged += Value1_SelectedIndexChanged;
            Value1.TextChanged += Value1_TextChanged;
            value2.SelectedIndexChanged += value2_SelectedIndexChanged;
            value2.TextChanged += value2_TextChanged;
            Button_Add.Click += Button_Add_Click;
            Button_Del.Click += Button_Del_Click;
            ItemList.SelectedIndexChanged += ItemList_SelectedIndexChanged;
            Value1.Validating += Value1_Validating;
            value2.Validating += value2_Validating;
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.ComboBoxEdit relation;

        internal DevExpress.XtraEditors.ComboBoxEdit Value1;
        internal DevExpress.XtraEditors.GroupControl simple_group;
        internal DevExpress.XtraEditors.ComboBoxEdit value2;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.GroupControl item_group;
        internal DevExpress.XtraEditors.SimpleButton Button_Del;
        internal DevExpress.XtraEditors.SimpleButton Button_Add;
        internal DevExpress.XtraEditors.ListBoxControl ItemList;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.relation = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Value1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simple_group = new DevExpress.XtraEditors.GroupControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.value2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.item_group = new DevExpress.XtraEditors.GroupControl();
            this.Button_Del = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Add = new DevExpress.XtraEditors.SimpleButton();
            this.ItemList = new DevExpress.XtraEditors.ListBoxControl();
            ((System.ComponentModel.ISupportInitialize)this.relation.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Value1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.simple_group).BeginInit();
            this.simple_group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.value2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.item_group).BeginInit();
            this.item_group.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.ItemList).BeginInit();
            this.SuspendLayout();
            //
            //relation
            //
            this.relation.Location = new System.Drawing.Point(8, 8);
            this.relation.Name = "relation";
            //
            //relation.Properties
            //
            this.relation.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.relation.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.relation.Size = new System.Drawing.Size(144, 20);
            this.relation.TabIndex = 0;
            //
            //Value1
            //
            this.Value1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.Value1.Location = new System.Drawing.Point(160, 8);
            this.Value1.Name = "Value1";
            //
            //Value1.Properties
            //
            this.Value1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Value1.Size = new System.Drawing.Size(160, 20);
            this.Value1.TabIndex = 1;
            //
            //simple_group
            //
            this.simple_group.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.simple_group.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simple_group.Controls.Add(this.LabelControl1);
            this.simple_group.Controls.Add(this.value2);
            this.simple_group.Location = new System.Drawing.Point(0, 32);
            this.simple_group.Name = "simple_group";
            this.simple_group.ShowCaption = false;
            this.simple_group.Size = new System.Drawing.Size(320, 120);
            this.simple_group.TabIndex = 2;
            this.simple_group.Text = "simple_group";
            this.simple_group.Visible = false;
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl1.Location = new System.Drawing.Point(160, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(160, 20);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "and";
            this.LabelControl1.UseMnemonic = false;
            //
            //value2
            //
            this.value2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.value2.Location = new System.Drawing.Point(160, 32);
            this.value2.Name = "value2";
            //
            //value2.Properties
            //
            this.value2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.value2.Size = new System.Drawing.Size(160, 20);
            this.value2.TabIndex = 1;
            //
            //item_group
            //
            this.item_group.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.item_group.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.item_group.Controls.Add(this.Button_Del);
            this.item_group.Controls.Add(this.Button_Add);
            this.item_group.Controls.Add(this.ItemList);
            this.item_group.Location = new System.Drawing.Point(0, 32);
            this.item_group.Name = "item_group";
            this.item_group.ShowCaption = false;
            this.item_group.Size = new System.Drawing.Size(320, 120);
            this.item_group.TabIndex = 3;
            this.item_group.Text = "GroupControl1";
            this.item_group.Visible = false;
            //
            //Button_Del
            //
            this.Button_Del.Enabled = false;
            this.Button_Del.Location = new System.Drawing.Point(72, 40);
            this.Button_Del.Name = "Button_Del";
            this.Button_Del.TabIndex = 1;
            this.Button_Del.Text = "&Del";
            //
            //Button_Add
            //
            this.Button_Add.Enabled = false;
            this.Button_Add.Location = new System.Drawing.Point(72, 8);
            this.Button_Add.Name = "Button_Add";
            this.Button_Add.TabIndex = 0;
            this.Button_Add.Text = "&Add";
            //
            //ItemList
            //
            this.ItemList.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.ItemList.Location = new System.Drawing.Point(160, 8);
            this.ItemList.Name = "ItemList";
            this.ItemList.Size = new System.Drawing.Size(160, 104);
            this.ItemList.TabIndex = 2;
            //
            //FrameControl
            //
            this.Controls.Add(this.Value1);
            this.Controls.Add(this.relation);
            this.Controls.Add(this.item_group);
            this.Controls.Add(this.simple_group);
            this.Name = "FrameControl";
            this.Size = new System.Drawing.Size(320, 150);
            ((System.ComponentModel.ISupportInitialize)this.relation.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Value1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.simple_group).EndInit();
            this.simple_group.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.value2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.item_group).EndInit();
            this.item_group.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.ItemList).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        protected override void OnLoad(System.EventArgs e)
        {
            base.OnLoad(e);

            // Load the list of items for the relations
            var _with1 = relation;
            var _with2 = _with1.Properties.Items;
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is any value", Retention.Enum_Relations.RELATION_IS_ANY_VALUE));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is equal to", Retention.Enum_Relations.RELATION_IS_EQUAL));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is not equal to", Retention.Enum_Relations.RELATION_IS_NOT_EQUAL));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is one of", Retention.Enum_Relations.RELATION_IS_ONE_OF));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is not one of", Retention.Enum_Relations.RELATION_IS_NOT_ONE_OF));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is less than", Retention.Enum_Relations.RELATION_IS_LESS_THAN));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is less than or equal to", Retention.Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than", Retention.Enum_Relations.RELATION_IS_GREATER_THAN));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is greater than or equal to", Retention.Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is between", Retention.Enum_Relations.RELATION_IS_BETWEEEN));
            _with2.Add(new DebtPlus.Data.Controls.ComboboxItem("is not between", Retention.Enum_Relations.RELATION_IS_NOT_BETWEEN));
            _with1.SelectedIndex = Convert.ToInt32(CurrentItem.Relation);

            // Populate the list of items into the item list
            var _with3 = ItemList;
            var _with4 = _with3.Items;
            _with4.Clear();
            foreach (FieldItem NewItem in CurrentItem.Values)
            {
                _with4.Add(NewItem);
            }

            // If this is a table type of event then load the table contents into the dropdown lists
            switch (CurrentItem.EventID)
            {
                case Retention.Enum_Events.EVENT_OFFICE:
                    LoadTableReferences(ref this.Value1, CommonStorage.OfficesTable().DefaultView, CurrentItem.Field1);
                    LoadTableReferences(ref this.value2, CommonStorage.OfficesTable().DefaultView, CurrentItem.Field2);
                    break;

                case Retention.Enum_Events.EVENT_ACTIVE_STATUS:
                    LoadTableReferences(ref this.Value1, CommonStorage.ActiveStatusTable().DefaultView, CurrentItem.Field1);
                    LoadTableReferences(ref this.value2, CommonStorage.ActiveStatusTable().DefaultView, CurrentItem.Field2);
                    break;

                case Retention.Enum_Events.EVENT_RETENTION_EVENT:
                    LoadTableReferences(ref this.Value1, CommonStorage.RetentionEventsTable().DefaultView, CurrentItem.Field1);
                    LoadTableReferences(ref this.value2, CommonStorage.RetentionEventsTable().DefaultView, CurrentItem.Field2);
                    break;

                case Retention.Enum_Events.EVENT_REGION:
                    LoadTableReferences(ref this.Value1, CommonStorage.RegionsTable().DefaultView, CurrentItem.Field1);
                    LoadTableReferences(ref this.value2, CommonStorage.RegionsTable().DefaultView, CurrentItem.Field2);
                    break;

                case Retention.Enum_Events.EVENT_COUNSELOR:
                    System.Data.DataView vueCNSLR = new System.Data.DataView(CommonStorage.CounselorsTable(), string.Empty, string.Empty, DataViewRowState.CurrentRows);
                    LoadTableReferences(ref this.Value1, vueCNSLR, CurrentItem.Field1);
                    LoadTableReferences(ref this.value2, vueCNSLR, CurrentItem.Field2);
                    break;

                case Retention.Enum_Events.EVENT_CSR:
                    System.Data.DataView vueCSR = new System.Data.DataView(CommonStorage.CSRTable(), string.Empty, string.Empty, DataViewRowState.CurrentRows);
                    LoadTableReferences(ref this.Value1, vueCSR, CurrentItem.Field1);
                    LoadTableReferences(ref this.value2, vueCSR, CurrentItem.Field2);
                    break;
            }
        }

        private void LoadTableReferences(ref DevExpress.XtraEditors.ComboBoxEdit ctl, System.Data.DataView vue, FieldItem SelectedItem)
        {
            var _with5 = ctl;
            var _with6 = _with5.Properties.Items;
            _with6.Clear();
            foreach (System.Data.DataRowView row in vue)
            {
                System.Int32 ItemNumber = _with6.Add(new DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(row[1]), row[0]));
                if (SelectedItem != null)
                {
                    if (SelectedItem.CompareTo(row[1]) == 0)
                        ctl.SelectedIndex = ItemNumber;
                }
            }
        }

        private void relation_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // Set the relation in the clause
            CurrentItem.Relation = (Retention.Enum_Relations)((DebtPlus.Data.Controls.ComboboxItem)relation.SelectedItem).value;

            // Enable/Disable controls based upon the relation
            switch (CurrentItem.Relation)
            {
                case Retention.Enum_Relations.RELATION_IS_ANY_VALUE:
                    this.Value1.Visible = false;
                    this.simple_group.Visible = false;
                    this.item_group.Visible = false;
                    break;

                case Retention.Enum_Relations.RELATION_IS_BETWEEEN:
                case Retention.Enum_Relations.RELATION_IS_NOT_BETWEEN:
                    this.Value1.Visible = true;
                    this.simple_group.Visible = true;
                    this.item_group.Visible = false;
                    break;

                case Retention.Enum_Relations.RELATION_IS_EQUAL:
                case Retention.Enum_Relations.RELATION_IS_GREATER_THAN:
                case Retention.Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL:
                case Retention.Enum_Relations.RELATION_IS_LESS_THAN:
                case Retention.Enum_Relations.RELATION_IS_LESS_THAN_OR_EQUAL:
                case Retention.Enum_Relations.RELATION_IS_NOT_EQUAL:
                    this.Value1.Visible = true;
                    this.simple_group.Visible = false;
                    this.item_group.Visible = false;
                    break;

                case Retention.Enum_Relations.RELATION_IS_NOT_ONE_OF:
                case Retention.Enum_Relations.RELATION_IS_ONE_OF:
                    this.Value1.Visible = true;
                    this.simple_group.Visible = false;
                    this.item_group.Visible = true;
                    break;
            }

            if (FrameUpdated != null)
            {
                FrameUpdated(this, EventArgs.Empty);
            }
        }

        private void Value1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            CurrentItem.Field1.ItemValue = -1;
            CurrentItem.Field1.Value = Value1.Text;
            if (Value1.SelectedIndex >= 0)
            {
                CurrentItem.Field1.ItemValue = ((DebtPlus.Data.Controls.ComboboxItem)Value1.SelectedItem).value;
                Button_Add.Enabled = Value1NotInList();
            }

            if (FrameUpdated != null)
            {
                FrameUpdated(this, EventArgs.Empty);
            }
        }

        private void Value1_TextChanged(object sender, System.EventArgs e)
        {
            CurrentItem.Field1.Value = Value1.Text.Trim();
            Button_Add.Enabled = (CurrentItem.Field1.Value != string.Empty) && Value1NotInList();

            if (FrameUpdated != null)
            {
                FrameUpdated(this, EventArgs.Empty);
            }
        }

        private void value2_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            CurrentItem.Field2.ItemValue = -1;
            CurrentItem.Field2.Value = value2.Text;
            if (Value1.SelectedIndex >= 0)
            {
                CurrentItem.Field2.ItemValue = ((DebtPlus.Data.Controls.ComboboxItem)value2.SelectedItem).value;
            }

            if (FrameUpdated != null)
            {
                FrameUpdated(this, EventArgs.Empty);
            }
        }

        private void value2_TextChanged(object sender, System.EventArgs e)
        {
            CurrentItem.Field2.Value = value2.Text;
            if (FrameUpdated != null)
            {
                FrameUpdated(this, EventArgs.Empty);
            }
        }

        private void Button_Add_Click(object sender, System.EventArgs e)
        {
            // Make a copy of the current item #1
            FieldItem NewField = new FieldItem();
            var _with7 = CurrentItem.Field1;
            NewField.ItemID = _with7.ItemID;
            NewField.ItemValue = _with7.ItemValue;
            NewField.Value = _with7.Value;

            // Add it to the list of items in the group
            CurrentItem.Values.Add(NewField);

            // Put it in the visible list of item and select it
            ItemList.SelectedItem = ItemList.Items.Add(NewField);
            Button_Add.Enabled = Value1NotInList();

            if (FrameUpdated != null)
            {
                FrameUpdated(this, EventArgs.Empty);
            }
        }

        private void Button_Del_Click(object sender, System.EventArgs e)
        {
            // If there is a selected item to be removed ..

            if (ItemList.SelectedIndex >= 0)
            {
                // Find the item that is selected
                FieldItem ItemSelection = (FieldItem)ItemList.SelectedItem;

                // Find the item in our current list of items for this relation
                System.Int32 IndexItem = CurrentItem.Values.IndexOf(ItemSelection);

                // If found then we may delete it. Remove it from both lists.
                if (IndexItem >= 0)
                {
                    Button_Del.Enabled = false;
                    CurrentItem.Values.RemoveAt(IndexItem);
                    ItemList.Items.RemoveAt(ItemList.SelectedIndex);

                    // Enable the ADD button if the current item is not in the list
                    Button_Add.Enabled = Value1NotInList();

                    if (FrameUpdated != null)
                    {
                        FrameUpdated(this, EventArgs.Empty);
                    }
                }
            }
        }

        private void ItemList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            Button_Del.Enabled = (ItemList.SelectedIndex >= 0);
        }

        private void Value1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Start by disabling the add button until we like the data
            Button_Add.Enabled = false;

            // Trim the text around the string and if there is no item then blank the value clause
            CurrentItem.Field1.Value = Value1.Text.Trim();
            if (CurrentItem.Field1.Value == string.Empty)
            {
                CurrentItem.Field1.ItemValue = null;
                if (FrameUpdated != null)
                {
                    FrameUpdated(this, EventArgs.Empty);
                }
                return;
            }

            // If the item is a date then we need to parse the date. If there is an error, it will take an exception.
            switch (CurrentItem.EventID)
            {
                case Retention.Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE:
                case Retention.Enum_Events.EVENT_LAST_DEPOSIT_DATE:
                case Retention.Enum_Events.EVENT_ACTION_DATE:
                    try
                    {
                        CurrentItem.Field1.ItemValue = System.DateTime.Parse(Value1.Text);
                        Value1.Text = CurrentItem.Field1.ToString();
                        Button_Add.Enabled = Value1NotInList();
                        if (FrameUpdated != null)
                        {
                            FrameUpdated(this, EventArgs.Empty);
                        }
                    }
#pragma warning disable 168
                    catch (FormatException ex)
                    {
                        e.Cancel = true;
                        Value1.ErrorText = "Invalid date";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        e.Cancel = true;
                        Value1.ErrorText = "Invalid date";
                    }
#pragma warning restore 168
                    break;

                default:
                    Button_Add.Enabled = (Value1.SelectedIndex >= 0) && Value1NotInList();
                    break;
            }
        }

        private void value2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Trim the text around the string and if there is no item then blank the value clause
            CurrentItem.Field2.Value = value2.Text.Trim();
            if (CurrentItem.Field2.Value == string.Empty)
            {
                CurrentItem.Field2.ItemValue = null;
                if (FrameUpdated != null)
                {
                    FrameUpdated(this, EventArgs.Empty);
                }
                return;
            }

            // If the item is a date then we need to parse the date. If there is an error, it will take an exception.
            switch (CurrentItem.EventID)
            {
                case Retention.Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE:
                case Retention.Enum_Events.EVENT_LAST_DEPOSIT_DATE:
                case Retention.Enum_Events.EVENT_ACTION_DATE:
                    try
                    {
                        CurrentItem.Field2.ItemValue = System.DateTime.Parse(value2.Text);
                        value2.Text = CurrentItem.Field2.ToString();
                        if (FrameUpdated != null)
                        {
                            FrameUpdated(this, EventArgs.Empty);
                        }
                    }
#pragma warning disable 168
                    catch (FormatException ex)
                    {
                        e.Cancel = true;
                        value2.ErrorText = "Invalid date";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        e.Cancel = true;
                        value2.ErrorText = "Invalid date";
                    }
#pragma warning restore 168
                    break;
            }
        }

        private bool Value1NotInList()
        {
            // A "NOTHING" is always in the list
            if (CurrentItem.Field1.ItemValue == null)
                return true;

            // Look for a duplicate item to the current selection
            foreach (FieldItem Item in ItemList.Items)
            {
                if (Item.CompareTo(CurrentItem.Field1) == 0)
                    return false;
            }

            // The item is not in the list if we can't find it.
            return true;
        }
    }
}