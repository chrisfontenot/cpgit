#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    internal partial class RelationForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public RelationForm() : base()
        {
            InitializeComponent();
        }

        private FieldSelection FirstField;

        private bool IsSearch;

        public RelationForm(bool search) : base()
        {
            InitializeComponent();
            this.Load += RelationForm_Load;
            Button_New.Click += Button_New_Click;
            Button_Del.Click += Button_Del_Click;
            IsSearch = search;
        }

        public RelationForm(bool search, FieldSelection FirstField) : this(search)
        {
            this.FirstField = FirstField;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraTab.XtraTabControl XtraTabControl1;

        internal DevExpress.XtraTab.XtraTabPage XtraTabPage1;
        internal DevExpress.XtraEditors.SimpleButton Button_New;
        internal DevExpress.XtraEditors.SimpleButton Button_Del;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.XtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.XtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.Button_New = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Del = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).BeginInit();
            this.XtraTabControl1.SuspendLayout();
            this.SuspendLayout();
            //
            //XtraTabControl1
            //
            this.XtraTabControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.XtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.XtraTabControl1.Name = "XtraTabControl1";
            this.XtraTabControl1.SelectedTabPage = this.XtraTabPage1;
            this.XtraTabControl1.Size = new System.Drawing.Size(336, 264);
            this.XtraTabControl1.TabIndex = 0;
            this.XtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] { this.XtraTabPage1 });
            //
            //XtraTabPage1
            //
            this.XtraTabPage1.Name = "XtraTabPage1";
            this.XtraTabPage1.Size = new System.Drawing.Size(329, 235);
            this.XtraTabPage1.Text = "XtraTabPage1";
            //
            //Button_New
            //
            this.Button_New.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_New.Location = new System.Drawing.Point(344, 8);
            this.Button_New.Name = "Button_New";
            this.Button_New.Size = new System.Drawing.Size(75, 23);
            this.Button_New.TabIndex = 1;
            this.Button_New.Text = "&New";
            //
            //Button_Del
            //
            this.Button_Del.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Del.CausesValidation = false;
            this.Button_Del.Location = new System.Drawing.Point(344, 48);
            this.Button_Del.Name = "Button_Del";
            this.Button_Del.Size = new System.Drawing.Size(75, 23);
            this.Button_Del.TabIndex = 2;
            this.Button_Del.Text = "&Delete";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(16, 272);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 3;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.Button_Cancel.CausesValidation = false;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(112, 272);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 4;
            this.Button_Cancel.Text = "&Cancel";
            //
            //RelationForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(432, 302);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Button_Del);
            this.Controls.Add(this.Button_New);
            this.Controls.Add(this.XtraTabControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "RelationForm";
            this.Text = "Retention Selection Criteria";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).EndInit();
            this.XtraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void RelationForm_Load(object sender, System.EventArgs e)
        {
            // Define the first tab control
            DevExpress.XtraTab.XtraTabPage Page = XtraTabControl1.TabPages[0];
            var _with1 = Page;
            _with1.Text = FirstField.Caption;
            FrameControl ctl = new FrameControl(ref FirstField);
            ctl.FrameUpdated += FrameUpdatedEvent;
            _with1.Controls.Add(ctl);
        }

        private System.Int32 Button_New_Click_PageSequence = 1;

        private void Button_New_Click(object sender, System.EventArgs e)
        {
            FieldSelection NextField = new FieldSelection();
            using (FieldForm FieldChoice = new FieldForm(IsSearch, NextField))
            {
                var _with2 = FieldChoice;
                _with2.Location = new System.Drawing.Point(this.Left + (this.Width - _with2.Width) / 2, this.Top + (this.Height - _with2.Height) / 2);

                // If successful, add a new page to the control group.

                if (_with2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    // Create the tab page
                    DevExpress.XtraTab.XtraTabPage Page = new DevExpress.XtraTab.XtraTabPage();
                    Button_New_Click_PageSequence += 1;
                    Page.Name = string.Format("XtraTabPage{0:d}", Button_New_Click_PageSequence);
                    Page.Text = NextField.Caption;

                    // In that page, add a new frame control to process the input fields
                    FrameControl ctl = new FrameControl(ref NextField);
                    ctl.FrameUpdated += FrameUpdatedEvent;
                    ctl.Name = string.Format("FrameControl{0:d}", Button_New_Click_PageSequence);
                    ctl.Dock = DockStyle.Fill;
                    Page.Controls.Add(ctl);

                    // Add the page and select it.
                    XtraTabControl1.TabPages.Add(Page);
                    XtraTabControl1.SelectedTabPage = Page;
                }
            }
        }

        private void Button_Del_Click(object sender, System.EventArgs e)
        {
            // Remove the page from the tab
            DevExpress.XtraTab.XtraTabPage Page = XtraTabControl1.SelectedTabPage;
            if (Page != null)
            {
                XtraTabControl1.TabPages.Remove(Page);
            }

            // Disable the OK and DELETE buttons if there are no more pages.
            Button_OK.Enabled = SelectionClause().Valid;
            Button_Del.Enabled = XtraTabControl1.TabPages.Count > 0;
        }

        private void FrameUpdatedEvent(object Sender, System.EventArgs e)
        {
            Button_OK.Enabled = SelectionClause().Valid;
        }

        internal SelectionClause SelectionClause()
        {
            SelectionClause answer = new SelectionClause();

            // Process the tab pages. Find the controls from the pages and envoke their "GetSelectionClause" routine to build the selection string.
            foreach (DevExpress.XtraTab.XtraTabPage Page in XtraTabControl1.TabPages)
            {
                FrameControl ctl = (FrameControl)Page.Controls[0];
                FieldSelection item = ctl.CurrentItem;

                // From the item, find the selection clause
                SelectionClause Clause = item.GetSelectionClause();

                // IF the clause is not valid then abort and return an empty condition as an error
                if (!Clause.Valid)
                {
                    answer.Valid = false;
                    return answer;
                }

                // Combine the tables that we need
                answer.UseClientDepositTable = answer.UseClientDepositTable || Clause.UseClientDepositTable;
                answer.UseClientRetentionEventsTable = answer.UseClientRetentionEventsTable || Clause.UseClientRetentionEventsTable;
                answer.SelectionString += " AND " + Clause.SelectionString;
            }

            // Remove the leading " AND " item
            if (answer.SelectionString.Length > 0)
                answer.SelectionString = answer.SelectionString.Substring(5);
            answer.Valid = true;
            return answer;
        }
    }
}