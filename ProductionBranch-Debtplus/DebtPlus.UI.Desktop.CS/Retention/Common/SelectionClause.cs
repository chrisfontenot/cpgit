namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    internal partial class SelectionClause : object
    {
        internal string SelectionString = string.Empty;
        internal bool UseClientDepositTable = false;
        internal bool UseClientRetentionEventsTable = false;

        internal bool Valid = true;

        public SelectionClause() : base()
        {
        }

        public SelectionClause(string SelectionString, bool UseClientDeposit = false, bool UseRetentionTable = false) : this()
        {
            this.SelectionString = SelectionString;
            this.UseClientDepositTable = UseClientDeposit;
            this.UseClientRetentionEventsTable = UseRetentionTable;
        }
    }
}