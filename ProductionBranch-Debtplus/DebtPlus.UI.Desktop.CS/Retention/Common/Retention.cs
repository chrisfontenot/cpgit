using System.Data;

namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    static internal partial class Retention
    {
        internal enum Enum_Events
        {
            // Standard list for creating and searching
            EVENT_ACTIVE_STATUS,

            EVENT_OFFICE,
            EVENT_EXPECTED_DEPOSIT_DATE,
            EVENT_LAST_DEPOSIT_DATE,
            EVENT_RETENTION_EVENT,

            // Additional event criteria for searching
            EVENT_COUNSELOR,

            EVENT_CSR,
            EVENT_REGION,
            EVENT_ACTION_DATE,
            EVENT_CREDITOR
        }

        static internal System.Data.DataTable EventsTable()
        {
            System.Data.DataTable tbl = new System.Data.DataTable("retention_events");
            tbl.Columns.Add("retention_event", typeof(System.Int32), string.Empty);
            tbl.Columns.Add("description", typeof(string), string.Empty).MaxLength = 50;
            tbl.Columns.Add("creation", typeof(bool), string.Empty);

            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_ACTIVE_STATUS,
                "Active Status",
                true
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_OFFICE,
                "Office",
                true
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE,
                "Deposit Date",
                true
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_LAST_DEPOSIT_DATE,
                "Last Deposit Date",
                true
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_RETENTION_EVENT,
                "Retention Event",
                true
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_COUNSELOR,
                "Counselor",
                false
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_CSR,
                "C.S.R.",
                false
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_REGION,
                "Region",
                false
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_ACTION_DATE,
                "Last Action Date",
                false
            });
            tbl.Rows.Add(new object[] {
                Enum_Events.EVENT_CREDITOR,
                "Creditor",
                false
            });

            // The primary key is the value
            tbl.PrimaryKey = new DataColumn[] { tbl.Columns[0] };

            return tbl;
        }

        internal enum Enum_Relations
        {
            RELATION_IS_ANY_VALUE = 0,
            RELATION_IS_EQUAL = 1,
            RELATION_IS_NOT_EQUAL = 2,
            RELATION_IS_ONE_OF = 3,
            RELATION_IS_NOT_ONE_OF = 4,
            RELATION_IS_LESS_THAN = 5,
            RELATION_IS_LESS_THAN_OR_EQUAL = 6,
            RELATION_IS_GREATER_THAN = 7,
            RELATION_IS_GREATER_THAN_OR_EQUAL = 8,
            RELATION_IS_BETWEEEN = 9,
            RELATION_IS_NOT_BETWEEN = 10
        }
    }
}