using System;

namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    internal partial class FieldItem : object, IComparable
    {
        internal string Value = string.Empty;
        internal object ItemValue = null;
        internal System.Int32 ItemID = 0;

        public FieldItem() : base()
        {
        }

        public override string ToString()
        {
            if (ItemValue != null)
            {
                if (ItemValue is DateTime)
                    return string.Format("{0:d}", Convert.ToDateTime(ItemValue));
            }
            return Value;
        }

        public System.Int32 CompareTo(object obj)
        {
            // If the input value is nothing then we are always greater
            if (obj == null)
                return 1;

            // If we are comparing to our own type then compare the string values
            if (obj is FieldItem)
            {
                return string.Compare(((FieldItem)obj).ToString(), this.ToString());
            }

            // Handle the case of a string comparison to ourselves
            if (obj is string && ItemValue is string)
                return string.Compare(Convert.ToString(ItemValue), Convert.ToString(obj));

            // Compare the Int32 value
            if (obj is Int32 && ItemValue is Int32)
            {
                if (Convert.ToInt32(ItemValue) < Convert.ToInt32(obj))
                    return -1;
                if (Convert.ToInt32(ItemValue) == Convert.ToInt32(obj))
                    return 0;
                return 1;
            }

            // We do not compare. So, assume that we are less.
            return -1;
        }
    }
}