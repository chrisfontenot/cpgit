using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    static internal partial class CommonStorage
    {
        static internal System.Data.DataSet ds = new System.Data.DataSet("ds");

        static internal System.Data.DataTable RetentionEventsTable()
        {
            System.Data.DataTable tbl = ds.Tables["retention_events"];

            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with1 = cmd;
                        _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with1.CommandText = "SELECT retention_event as item_key, description, convert(bit,1) as ActiveFlag, convert(bit,0) as [default], priority, initial_retention_action, expire_type FROM retention_events WITH (NOLOCK) ORDER BY description";
                        _with1.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "retention_events");
                        }
                    }

                    tbl = ds.Tables["retention_events"];
                    var _with2 = tbl;
                    _with2.PrimaryKey = new System.Data.DataColumn[] { _with2.Columns["retention_event"] };
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading retention_events table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        static internal System.Data.DataTable RetentionActionsTable()
        {
            System.Data.DataTable tbl = ds.Tables["retention_actions"];
            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with3 = cmd;
                        _with3.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with3.CommandText = "SELECT [retention_action],[description],[letter_code],[date_created],[created_by] FROM retention_actions WITH (NOLOCK) ORDER BY description";
                        _with3.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "retention_actions");
                        }
                    }

                    tbl = ds.Tables["retention_actions"];
                    var _with4 = tbl;
                    _with4.PrimaryKey = new System.Data.DataColumn[] { _with4.Columns["retention_action"] };
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading retention_actions table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        static internal System.Data.DataTable ActiveStatusTable()
        {
            System.Data.DataTable tbl = new System.Data.DataTable();

            var _with5 = tbl;
            _with5.Columns.Add("active_status", typeof(string));
            _with5.Columns.Add("description", typeof(string));
            _with5.PrimaryKey = new System.Data.DataColumn[] { _with5.Columns[0] };

            var _with6 = _with5.Rows;
            _with6.Add(new object[] {
                "A",
                "Active"
            });
            _with6.Add(new object[] {
                "AR",
                "Active Restart"
            });
            _with6.Add(new object[] {
                "APT",
                "Appointment"
            });
            _with6.Add(new object[] {
                "CRE",
                "Created"
            });
            _with6.Add(new object[] {
                "I",
                "Inactive"
            });
            _with6.Add(new object[] {
                "RDY",
                "Ready For Review"
            });
            _with6.Add(new object[] {
                "EX",
                "Terminating"
            });

            return tbl;
        }

        static internal System.Data.DataTable OfficesTable()
        {
            const string TableName = "offices";
            System.Data.DataTable tbl = ds.Tables[TableName];
            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with7 = cmd;
                        _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with7.CommandText = "SELECT office as item_key, name as description, ActiveFlag, [default] FROM offices ORDER BY description";
                        _with7.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                        }
                    }

                    tbl = ds.Tables[TableName];
                    var _with8 = tbl;
                    if (_with8.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        _with8.PrimaryKey = new System.Data.DataColumn[] { _with8.Columns["item_key"] };
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading offices table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        static internal System.Data.DataTable RegionsTable()
        {
            System.Data.DataTable tbl = ds.Tables["regions"];
            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with9 = cmd;
                        _with9.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with9.CommandText = "SELECT region as item_key, description, convert(bit,1) as ActiveFlag, convert(bit,0) as [default] FROM regions ORDER BY description";
                        _with9.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "regions");
                        }
                    }

                    tbl = ds.Tables["regions"];
                    var _with10 = tbl;
                    _with10.PrimaryKey = new System.Data.DataColumn[] { _with10.Columns["region"] };
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading regions table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        static internal System.Data.DataTable CounselorsTable()
        {
            const string TableName = "counselors";
            System.Data.DataTable tbl = ds.Tables[TableName];
            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with11 = cmd;
                        _with11.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with11.CommandText = "SELECT counselor as item_key, name as description, ActiveFlag, [default] FROM view_counselors ORDER BY name";
                        _with11.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                        }
                    }

                    tbl = ds.Tables[TableName];
                    var _with12 = tbl;
                    if (_with12.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        _with12.PrimaryKey = new System.Data.DataColumn[] { _with12.Columns["item_key"] };
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading counselors table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        static internal System.Data.DataTable CSRTable()
        {
            const string TableName = "csrs";
            System.Data.DataTable tbl = ds.Tables[TableName];
            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with13 = cmd;
                        _with13.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with13.CommandText = "SELECT counselor as item_key, name as description, ActiveFlag, [default] FROM view_csrs ORDER BY name";
                        _with13.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, TableName);
                        }
                    }

                    tbl = ds.Tables[TableName];
                    var _with14 = tbl;
                    if (_with14.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        _with14.PrimaryKey = new System.Data.DataColumn[] { _with14.Columns["item_key"] };
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading CSRs table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }
    }
}