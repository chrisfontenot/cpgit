using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    internal partial class FieldForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public FieldForm() : base()
        {
            InitializeComponent();
        }

        protected FieldSelection NewFieldItem;

        protected bool IsSearch;

        public FieldForm(bool search) : base()
        {
            InitializeComponent();
            this.Load += FieldForm_Load;
            Button_OK.Click += Button_OK_Click;
            IsSearch = search;
        }

        public FieldForm(bool search, FieldSelection NewFieldItem) : base()
        {
            InitializeComponent();
            this.NewFieldItem = NewFieldItem;
            IsSearch = search;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit1;

        protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
        protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();

            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();
            this.SuspendLayout();

            //
            //LookUpEdit1
            //
            this.LookUpEdit1.Location = new System.Drawing.Point(8, 8);
            this.LookUpEdit1.Name = "LookUpEdit1";
            //
            //LookUpEdit1.Properties
            //
            this.LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending) });
            this.LookUpEdit1.Properties.NullText = "";
            this.LookUpEdit1.Properties.ShowFooter = false;
            this.LookUpEdit1.Properties.ShowHeader = false;
            this.LookUpEdit1.Properties.ShowLines = false;
            this.LookUpEdit1.Size = new System.Drawing.Size(176, 20);
            this.LookUpEdit1.TabIndex = 0;
            this.LookUpEdit1.ToolTipController = this.ToolTipController1;
            this.LookUpEdit1.Properties.SortColumnIndex = 1;
            //
            //Button_OK
            //
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(200, 8);
            this.Button_OK.Name = "Button_OK";

            this.Button_OK.TabIndex = 1;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(200, 40);
            this.Button_Cancel.Name = "Button_Cancel";

            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //FieldForm
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(292, 78);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LookUpEdit1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FieldForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Select Item";

            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        protected void FieldForm_Load(System.Object sender, System.EventArgs e)
        {
            var _with1 = LookUpEdit1;
            var _with2 = _with1.Properties;
            if (IsSearch)
            {
                _with2.DataSource = Retention.EventsTable().DefaultView;
            }
            else
            {
                _with2.DataSource = new System.Data.DataView(Retention.EventsTable(), "[creation]=True", string.Empty, DataViewRowState.CurrentRows);
            }
            _with2.DisplayMember = "description";
            _with2.ValueMember = "retention_event";

            // Set the value to the first item in the table
            _with1.EditValue = Retention.EventsTable().Rows[0]["retention_event"];

            Button_OK.Enabled = !HasErrors();
        }

        protected virtual void Button_OK_Click(object sender, System.EventArgs e)
        {
            var _with3 = LookUpEdit1;
            NewFieldItem.EventID = (Retention.Enum_Events)_with3.EditValue;
            NewFieldItem.Caption = Convert.ToString(_with3.Text);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        protected virtual bool HasErrors()
        {
            return false;
        }
    }
}