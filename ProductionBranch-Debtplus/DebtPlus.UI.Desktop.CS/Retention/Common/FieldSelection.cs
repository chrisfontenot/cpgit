#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Retention.Common
{
    internal partial class FieldSelection : object
    {
        internal Retention.Enum_Events EventID;
        internal string Caption = string.Empty;
        internal Retention.Enum_Relations Relation = Retention.Enum_Relations.RELATION_IS_ANY_VALUE;
        internal System.Collections.ArrayList Values = new System.Collections.ArrayList();
        internal string FieldName = string.Empty;
        internal string TabID = string.Empty;
        internal FieldItem Field1 = new FieldItem();
        internal FieldItem Field2 = new FieldItem();

        private SelectionClause Clause;

        public FieldSelection() : base()
        {
        }

        /// <summary>
        /// Return a selection clause for the field and values
        /// </summary>
        private string Encode_Value(FieldItem Field, string time_value = "00:00:00")
        {
            string answer = string.Empty;

            if (Field == null || Field.ItemValue == null)
            {
                Clause.Valid = false;
                return string.Empty;
            }

            switch (EventID)
            {
                case Retention.Enum_Events.EVENT_ACTIVE_STATUS:
                    // Strings
                    answer = "'" + Convert.ToString(Field.ItemValue).Replace("'", "''") + "'";
                    break;

                case Retention.Enum_Events.EVENT_ACTION_DATE:
                case Retention.Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE:
                case Retention.Enum_Events.EVENT_LAST_DEPOSIT_DATE:
                    // Dates
                    answer = "'" + Convert.ToDateTime(Field.ItemValue).ToShortDateString() + " " + time_value + "'";
                    break;

                case Retention.Enum_Events.EVENT_COUNSELOR:
                case Retention.Enum_Events.EVENT_CSR:
                case Retention.Enum_Events.EVENT_REGION:
                case Retention.Enum_Events.EVENT_OFFICE:
                case Retention.Enum_Events.EVENT_RETENTION_EVENT:
                    // Table keys
                    answer = Convert.ToInt32(Field.ItemValue).ToString();
                    break;

                default:
                    Debug.Assert(false, "Invalid EventID");
                    break;
            }

            return answer;
        }

        /// <summary>
        /// Return a selection clause for the field and values
        /// </summary>
        private string is_one_of()
        {
            string strClause = string.Empty;

            if (Values.Count == 0)
            {
                Clause.Valid = false;
            }
            else
            {
                foreach (FieldItem item in Values)
                {
                    strClause += "," + Encode_Value(item);
                }

                strClause = "(" + strClause.Substring(1) + ")";
            }

            return strClause;
        }

        /// <summary>
        /// Return a selection clause for the field and values
        /// </summary>
        private string Where_Clause(string strField)
        {
            string answer = string.Empty;

            switch (Relation)
            {
                case Retention.Enum_Relations.RELATION_IS_ANY_VALUE:
                    answer = strField + " is not null";
                    break;

                case Retention.Enum_Relations.RELATION_IS_BETWEEEN:
                    answer = strField + " BETWEEN " + Encode_Value(Field1) + " AND " + Encode_Value(Field2, "23:59:59");
                    break;

                case Retention.Enum_Relations.RELATION_IS_EQUAL:
                    answer = strField + "=" + Encode_Value(Field1);
                    break;

                case Retention.Enum_Relations.RELATION_IS_NOT_EQUAL:
                    answer = strField + "<>" + Encode_Value(Field1);
                    break;

                case Retention.Enum_Relations.RELATION_IS_GREATER_THAN:
                    answer = strField + ">" + Encode_Value(Field1);
                    break;

                case Retention.Enum_Relations.RELATION_IS_GREATER_THAN_OR_EQUAL:
                    answer = strField + ">=" + Encode_Value(Field1);
                    break;

                case Retention.Enum_Relations.RELATION_IS_LESS_THAN:
                    answer = strField + "<" + Encode_Value(Field1);
                    break;

                case Retention.Enum_Relations.RELATION_IS_LESS_THAN_OR_EQUAL:
                    answer = strField + "<=" + Encode_Value(Field1);
                    break;

                case Retention.Enum_Relations.RELATION_IS_NOT_BETWEEN:
                    answer = strField + " NOT BETWEEN " + Encode_Value(Field1) + " AND " + Encode_Value(Field2, "23:59:59");
                    break;

                case Retention.Enum_Relations.RELATION_IS_NOT_ONE_OF:
                    answer = strField + " NOT IN " + is_one_of();
                    break;

                case Retention.Enum_Relations.RELATION_IS_ONE_OF:
                    answer = strField + " IN " + is_one_of();
                    break;

                default:
                    Debug.Assert(false, "Invalid Relation");
                    break;
            }

            return answer;
        }

        internal SelectionClause GetSelectionClause()
        {
            Clause = new SelectionClause();

            try
            {
                switch (EventID)
                {
                    case Retention.Enum_Events.EVENT_ACTIVE_STATUS:
                        Clause.SelectionString = Where_Clause("c.active_status");
                        break;

                    case Retention.Enum_Events.EVENT_EXPECTED_DEPOSIT_DATE:
                        Clause.SelectionString = Where_Clause("cd.deposit_date");
                        Clause.UseClientDepositTable = true;
                        break;

                    case Retention.Enum_Events.EVENT_LAST_DEPOSIT_DATE:
                        Clause.SelectionString = Where_Clause("c.last_deposit_date");
                        break;

                    case Retention.Enum_Events.EVENT_OFFICE:
                        Clause.SelectionString = Where_Clause("c.office");
                        break;

                    case Retention.Enum_Events.EVENT_CSR:
                        Clause.SelectionString = Where_Clause("c.csr");
                        break;

                    case Retention.Enum_Events.EVENT_COUNSELOR:
                        Clause.SelectionString = Where_Clause("c.counselor");
                        break;

                    case Retention.Enum_Events.EVENT_REGION:
                        Clause.SelectionString = Where_Clause("c.region");
                        break;

                    case Retention.Enum_Events.EVENT_ACTION_DATE:
                        Clause.SelectionString = Where_Clause("vc.last_action_date");
                        Clause.UseClientRetentionEventsTable = true;
                        break;

                    case Retention.Enum_Events.EVENT_RETENTION_EVENT:
                        Clause.SelectionString = Where_Clause("vc.retention_event");
                        Clause.UseClientRetentionEventsTable = true;
                        break;

                    default:
                        Debug.Assert(false, "Invalid EventID");
                        break;
                }
            }
#pragma warning disable 168
            catch (Exception ex)
            {
                Clause.Valid = false;
            }
#pragma warning restore 168

            return Clause;
        }
    }
}