#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.Data.Forms;
using DebtPlus.UI.Client.Service;
using DebtPlus.UI.Desktop.CS.Retention.Common;

namespace DebtPlus.UI.Desktop.CS.Retention.Review
{
    internal partial class Form_Review : DebtPlusForm
    {
        public Form_Review() : base()
        {
            InitializeComponent();
        }

        private readonly ArgumentParser ap;
        private bool IsSearch;

        public Form_Review(bool search) : base()
        {
            InitializeComponent();
            IsSearch = search;
        }

        public Form_Review(bool search, ArgumentParser Args) : base()
        {
            InitializeComponent();

            base.Closing += Form_Closing;
            this.Load += Form_Load;
            DataNavigator1.PositionChanged += DataNavigator1_PositionChanged;
            MenuFileRefresh.ItemClick += MenuFileRefresh_Click;
            MenuFileNew.ItemClick += MenuFileNew_Click;
            MenuFileDepositsSecond.ItemClick += MenuFileDepositsSecond_Click;
            MenuFileDepositsFirst.ItemClick += MenuFileDepositsFirst_Click;
            Button_Cancel.Click += ButtonCancel_Click;
            MenuFileExit.ItemClick += ButtonCancel_Click;
            MenuFile.Popup += MenuFile_Popup;
            MenuFileAppointmentsAny.ItemClick += MenuFileAppointmentsAny_Click;
            MenuFileAppointmentsFirst.ItemClick += MenuFileAppointmentsFirst_Click;
            MenuFileAppointmentsSecond.ItemClick += MenuFileAppointmentsSecond_Click;
            MenuFileDepositsAny.ItemClick += MenuFileDepositsAny_Click;
            ButtonShow.Click += ButtonShow_Click;
            MenuFilePrint.ItemClick += MenuFilePrint_Click;

            ap = Args;
            IsSearch = search;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LabelClient = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.RetentionEventList1 = new RetentionEventList();
            this.ButtonShow = new DevExpress.XtraEditors.SimpleButton();
            this.LabelName = new DevExpress.XtraEditors.LabelControl();
            this.LabelCoApplicant = new DevExpress.XtraEditors.LabelControl();
            this.LabelStatus = new DevExpress.XtraEditors.LabelControl();
            this.LabelHomePhone = new DevExpress.XtraEditors.LabelControl();
            this.LabelWorkPhone = new DevExpress.XtraEditors.LabelControl();
            this.DataNavigator1 = new DevExpress.XtraEditors.DataNavigator();
            this.LabelMsgPhone = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.Bar2 = new DevExpress.XtraBars.Bar();
            this.MenuFile = new DevExpress.XtraBars.BarSubItem();
            this.MenuFileDeposits = new DevExpress.XtraBars.BarSubItem();
            this.MenuFileDepositsAny = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileDepositsFirst = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileDepositsSecond = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileAppointments = new DevExpress.XtraBars.BarSubItem();
            this.MenuFileAppointmentsAny = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileAppointmentsFirst = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileAppointmentsSecond = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileNew = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileRefresh = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFilePrint = new DevExpress.XtraBars.BarCheckItem();
            this.MenuFileExit = new DevExpress.XtraBars.BarCheckItem();
            this.MenuEdit = new DevExpress.XtraBars.BarSubItem();
            this.MenuEditCut = new DevExpress.XtraBars.BarCheckItem();
            this.MenuEditCopy = new DevExpress.XtraBars.BarCheckItem();
            this.MenuEditPaste = new DevExpress.XtraBars.BarCheckItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            this.SuspendLayout();
            //
            //LabelClient
            //
            this.LabelClient.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelClient.Location = new System.Drawing.Point(76, 12);
            this.LabelClient.Name = "LabelClient";
            this.LabelClient.Size = new System.Drawing.Size(299, 13);
            this.LabelClient.StyleController = this.LayoutControl1;
            this.LabelClient.TabIndex = 5;
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.LabelClient);
            this.LayoutControl1.Controls.Add(this.Button_Cancel);
            this.LayoutControl1.Controls.Add(this.RetentionEventList1);
            this.LayoutControl1.Controls.Add(this.ButtonShow);
            this.LayoutControl1.Controls.Add(this.LabelName);
            this.LayoutControl1.Controls.Add(this.LabelCoApplicant);
            this.LayoutControl1.Controls.Add(this.LabelStatus);
            this.LayoutControl1.Controls.Add(this.LabelHomePhone);
            this.LayoutControl1.Controls.Add(this.LabelWorkPhone);
            this.LayoutControl1.Controls.Add(this.DataNavigator1);
            this.LayoutControl1.Controls.Add(this.LabelMsgPhone);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 24);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(544, 438);
            this.LayoutControl1.TabIndex = 19;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.Location = new System.Drawing.Point(457, 39);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.StyleController = this.LayoutControl1;
            this.Button_Cancel.TabIndex = 17;
            this.Button_Cancel.Text = "&Cancel";
            //
            //RetentionEventList1
            //
            this.RetentionEventList1.Location = new System.Drawing.Point(12, 107);
            this.RetentionEventList1.Name = "RetentionEventList1";
            this.RetentionEventList1.Size = new System.Drawing.Size(520, 296);
            this.RetentionEventList1.TabIndex = 18;
            //
            //ButtonShow
            //
            this.ButtonShow.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.ButtonShow.Location = new System.Drawing.Point(457, 12);
            this.ButtonShow.MaximumSize = new System.Drawing.Size(75, 23);
            this.ButtonShow.MinimumSize = new System.Drawing.Size(75, 23);
            this.ButtonShow.Name = "ButtonShow";
            this.ButtonShow.Size = new System.Drawing.Size(75, 23);
            this.ButtonShow.StyleController = this.LayoutControl1;
            this.ButtonShow.TabIndex = 16;
            this.ButtonShow.Text = "&Show...";
            //
            //LabelName
            //
            this.LabelName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelName.Location = new System.Drawing.Point(76, 29);
            this.LabelName.Name = "LabelName";
            this.LabelName.Size = new System.Drawing.Size(299, 13);
            this.LabelName.StyleController = this.LayoutControl1;
            this.LabelName.TabIndex = 6;
            //
            //LabelCoApplicant
            //
            this.LabelCoApplicant.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelCoApplicant.Location = new System.Drawing.Point(76, 46);
            this.LabelCoApplicant.Name = "LabelCoApplicant";
            this.LabelCoApplicant.Size = new System.Drawing.Size(299, 13);
            this.LabelCoApplicant.StyleController = this.LayoutControl1;
            this.LabelCoApplicant.TabIndex = 7;
            //
            //LabelStatus
            //
            this.LabelStatus.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelStatus.Location = new System.Drawing.Point(76, 63);
            this.LabelStatus.Name = "LabelStatus";
            this.LabelStatus.Size = new System.Drawing.Size(99, 13);
            this.LabelStatus.StyleController = this.LayoutControl1;
            this.LabelStatus.TabIndex = 8;
            //
            //LabelHomePhone
            //
            this.LabelHomePhone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelHomePhone.Location = new System.Drawing.Point(243, 80);
            this.LabelHomePhone.Name = "LabelHomePhone";
            this.LabelHomePhone.Size = new System.Drawing.Size(132, 13);
            this.LabelHomePhone.StyleController = this.LayoutControl1;
            this.LabelHomePhone.TabIndex = 9;
            //
            //LabelWorkPhone
            //
            this.LabelWorkPhone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelWorkPhone.Location = new System.Drawing.Point(243, 63);
            this.LabelWorkPhone.Name = "LabelWorkPhone";
            this.LabelWorkPhone.Size = new System.Drawing.Size(132, 13);
            this.LabelWorkPhone.StyleController = this.LayoutControl1;
            this.LabelWorkPhone.TabIndex = 13;
            //
            //DataNavigator1
            //
            this.DataNavigator1.Buttons.Append.Visible = false;
            this.DataNavigator1.Buttons.CancelEdit.Visible = false;
            this.DataNavigator1.Buttons.EndEdit.Visible = false;
            this.DataNavigator1.Buttons.Remove.Visible = false;
            this.DataNavigator1.Location = new System.Drawing.Point(12, 407);
            this.DataNavigator1.Name = "DataNavigator1";
            this.DataNavigator1.Size = new System.Drawing.Size(520, 19);
            this.DataNavigator1.StyleController = this.LayoutControl1;
            this.DataNavigator1.TabIndex = 15;
            this.DataNavigator1.Text = "DataNavigator1";
            this.DataNavigator1.TextLocation = DevExpress.XtraEditors.NavigatorButtonsTextLocation.Center;
            //
            //LabelMsgPhone
            //
            this.LabelMsgPhone.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelMsgPhone.Location = new System.Drawing.Point(76, 80);
            this.LabelMsgPhone.Name = "LabelMsgPhone";
            this.LabelMsgPhone.Size = new System.Drawing.Size(99, 13);
            this.LabelMsgPhone.StyleController = this.LayoutControl1;
            this.LabelMsgPhone.TabIndex = 12;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
                this.LayoutControlItem1,
                this.LayoutControlItem2,
                this.LayoutControlItem8,
                this.LayoutControlItem9,
                this.LayoutControlItem10,
                this.LayoutControlItem11,
                this.LayoutControlItem3,
                this.LayoutControlItem6,
                this.LayoutControlItem7,
                this.EmptySpaceItem1,
                this.EmptySpaceItem2,
                this.LayoutControlItem4,
                this.LayoutControlItem5
            });
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(544, 438);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.LabelClient;
            this.LayoutControlItem1.CustomizationFormText = "Client";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(367, 17);
            this.LayoutControlItem1.Text = "Client";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(60, 13);
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.LabelName;
            this.LayoutControlItem2.CustomizationFormText = "Name";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(367, 17);
            this.LayoutControlItem2.Text = "Name";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(60, 13);
            //
            //LayoutControlItem8
            //
            this.LayoutControlItem8.Control = this.RetentionEventList1;
            this.LayoutControlItem8.CustomizationFormText = "Events List";
            this.LayoutControlItem8.Location = new System.Drawing.Point(0, 95);
            this.LayoutControlItem8.Name = "LayoutControlItem8";
            this.LayoutControlItem8.Size = new System.Drawing.Size(524, 300);
            this.LayoutControlItem8.Text = "Events List";
            this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem8.TextToControlDistance = 0;
            this.LayoutControlItem8.TextVisible = false;
            //
            //LayoutControlItem9
            //
            this.LayoutControlItem9.Control = this.DataNavigator1;
            this.LayoutControlItem9.ControlAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.LayoutControlItem9.CustomizationFormText = "Record Control";
            this.LayoutControlItem9.ImageAlignment = System.Drawing.ContentAlignment.BottomCenter;
            this.LayoutControlItem9.Location = new System.Drawing.Point(0, 395);
            this.LayoutControlItem9.MaxSize = new System.Drawing.Size(0, 23);
            this.LayoutControlItem9.MinSize = new System.Drawing.Size(177, 23);
            this.LayoutControlItem9.Name = "LayoutControlItem9";
            this.LayoutControlItem9.Size = new System.Drawing.Size(524, 23);
            this.LayoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutControlItem9.Text = "Record Control";
            this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem9.TextToControlDistance = 0;
            this.LayoutControlItem9.TextVisible = false;
            //
            //LayoutControlItem10
            //
            this.LayoutControlItem10.Control = this.ButtonShow;
            this.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10";
            this.LayoutControlItem10.Location = new System.Drawing.Point(445, 0);
            this.LayoutControlItem10.Name = "LayoutControlItem10";
            this.LayoutControlItem10.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem10.Text = "LayoutControlItem10";
            this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem10.TextToControlDistance = 0;
            this.LayoutControlItem10.TextVisible = false;
            //
            //LayoutControlItem11
            //
            this.LayoutControlItem11.Control = this.Button_Cancel;
            this.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11";
            this.LayoutControlItem11.Location = new System.Drawing.Point(445, 27);
            this.LayoutControlItem11.Name = "LayoutControlItem11";
            this.LayoutControlItem11.Size = new System.Drawing.Size(79, 58);
            this.LayoutControlItem11.Text = "LayoutControlItem11";
            this.LayoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem11.TextToControlDistance = 0;
            this.LayoutControlItem11.TextVisible = false;
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.LabelCoApplicant;
            this.LayoutControlItem3.CustomizationFormText = "CoApplicant";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 34);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(367, 17);
            this.LayoutControlItem3.Text = "CoApplicant";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(60, 13);
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.LabelMsgPhone;
            this.LayoutControlItem6.CustomizationFormText = "Msg Phone";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 68);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(167, 17);
            this.LayoutControlItem6.Text = "Msg Phone";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(60, 13);
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.LabelWorkPhone;
            this.LayoutControlItem7.CustomizationFormText = "Work Phone";
            this.LayoutControlItem7.Location = new System.Drawing.Point(167, 51);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(200, 17);
            this.LayoutControlItem7.Text = "Work Phone";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(60, 13);
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 85);
            this.EmptySpaceItem1.MaxSize = new System.Drawing.Size(0, 10);
            this.EmptySpaceItem1.MinSize = new System.Drawing.Size(10, 10);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(524, 10);
            this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem2
            //
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(367, 0);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(78, 85);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.LabelStatus;
            this.LayoutControlItem4.CustomizationFormText = "Status";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 51);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(167, 17);
            this.LayoutControlItem4.Text = "Status";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(60, 13);
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.LabelHomePhone;
            this.LayoutControlItem5.CustomizationFormText = "Home Phone";
            this.LayoutControlItem5.Location = new System.Drawing.Point(167, 68);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(200, 17);
            this.LayoutControlItem5.Text = "Home Phone";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(60, 13);
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
                this.MenuFile,
                this.MenuEdit,
                this.MenuEditCopy,
                this.MenuEditPaste,
                this.MenuFileDeposits,
                this.MenuFileAppointments,
                this.MenuFileNew,
                this.MenuFileRefresh,
                this.MenuFilePrint,
                this.MenuFileExit,
                this.MenuFileDepositsAny,
                this.MenuFileDepositsFirst,
                this.MenuFileDepositsSecond,
                this.MenuFileAppointmentsAny,
                this.MenuFileAppointmentsFirst,
                this.MenuFileAppointmentsSecond,
                this.MenuEditCut
            });
            this.barManager1.MainMenu = this.Bar2;
            this.barManager1.MaxItemId = 18;
            //
            //Bar2
            //
            this.Bar2.BarName = "Main menu";
            this.Bar2.DockCol = 0;
            this.Bar2.DockRow = 0;
            this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFile),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuEdit)
            });
            this.Bar2.OptionsBar.MultiLine = true;
            this.Bar2.OptionsBar.UseWholeRow = true;
            this.Bar2.Text = "Main menu";
            //
            //MenuFile
            //
            this.MenuFile.Caption = "&File";
            this.MenuFile.Id = 0;
            this.MenuFile.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileDeposits),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileAppointments),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileNew, true),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileRefresh),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFilePrint),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileExit, true)
            });
            this.MenuFile.Name = "MenuFile";
            //
            //MenuFileDeposits
            //
            this.MenuFileDeposits.Caption = "&Deposits";
            this.MenuFileDeposits.Id = 5;
            this.MenuFileDeposits.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileDepositsAny),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileDepositsFirst),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileDepositsSecond)
            });
            this.MenuFileDeposits.Name = "MenuFileDeposits";
            //
            //MenuFileDepositsAny
            //
            this.MenuFileDepositsAny.Caption = "Missed &Any Deposit";
            this.MenuFileDepositsAny.Id = 11;
            this.MenuFileDepositsAny.Name = "MenuFileDepositsAny";
            //
            //MenuFileDepositsFirst
            //
            this.MenuFileDepositsFirst.Caption = "Missed &First Deposit";
            this.MenuFileDepositsFirst.Id = 12;
            this.MenuFileDepositsFirst.Name = "MenuFileDepositsFirst";
            //
            //MenuFileDepositsSecond
            //
            this.MenuFileDepositsSecond.Caption = "Missed &Subsequent Deposit";
            this.MenuFileDepositsSecond.Id = 13;
            this.MenuFileDepositsSecond.Name = "MenuFileDepositsSecond";
            //
            //MenuFileAppointments
            //
            this.MenuFileAppointments.Caption = "&Appointments";
            this.MenuFileAppointments.Id = 6;
            this.MenuFileAppointments.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileAppointmentsAny),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileAppointmentsFirst),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuFileAppointmentsSecond)
            });
            this.MenuFileAppointments.Name = "MenuFileAppointments";
            //
            //MenuFileAppointmentsAny
            //
            this.MenuFileAppointmentsAny.Caption = "Missed &Any Appointment";
            this.MenuFileAppointmentsAny.Id = 14;
            this.MenuFileAppointmentsAny.Name = "MenuFileAppointmentsAny";
            //
            //MenuFileAppointmentsFirst
            //
            this.MenuFileAppointmentsFirst.Caption = "Missed &First Appointment";
            this.MenuFileAppointmentsFirst.Id = 15;
            this.MenuFileAppointmentsFirst.Name = "MenuFileAppointmentsFirst";
            //
            //MenuFileAppointmentsSecond
            //
            this.MenuFileAppointmentsSecond.Caption = "Missed &Subsequent Appointment";
            this.MenuFileAppointmentsSecond.Id = 16;
            this.MenuFileAppointmentsSecond.Name = "MenuFileAppointmentsSecond";
            //
            //MenuFileNew
            //
            this.MenuFileNew.Caption = "&New ...";
            this.MenuFileNew.Id = 7;
            this.MenuFileNew.Name = "MenuFileNew";
            //
            //MenuFileRefresh
            //
            this.MenuFileRefresh.Caption = "&Refresh";
            this.MenuFileRefresh.Id = 8;
            this.MenuFileRefresh.Name = "MenuFileRefresh";
            //
            //MenuFilePrint
            //
            this.MenuFilePrint.Caption = "&Print ...";
            this.MenuFilePrint.Id = 9;
            this.MenuFilePrint.Name = "MenuFilePrint";
            //
            //MenuFileExit
            //
            this.MenuFileExit.Caption = "&Exit";
            this.MenuFileExit.Id = 10;
            this.MenuFileExit.Name = "MenuFileExit";
            //
            //MenuEdit
            //
            this.MenuEdit.Caption = "&Edit";
            this.MenuEdit.Id = 1;
            this.MenuEdit.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuEditCut),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuEditCopy),
                new DevExpress.XtraBars.LinkPersistInfo(this.MenuEditPaste)
            });
            this.MenuEdit.Name = "MenuEdit";
            //
            //MenuEditCut
            //
            this.MenuEditCut.Caption = "Cu&t";
            this.MenuEditCut.Id = 2;
            this.MenuEditCut.Name = "MenuEditCut";
            //
            //MenuEditCopy
            //
            this.MenuEditCopy.Caption = "&Copy";
            this.MenuEditCopy.Id = 3;
            this.MenuEditCopy.Name = "MenuEditCopy";
            //
            //MenuEditPaste
            //
            this.MenuEditPaste.Caption = "&Paste";
            this.MenuEditPaste.Id = 4;
            this.MenuEditPaste.Name = "MenuEditPaste";
            //
            //barDockControlTop
            //
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(544, 24);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 462);
            this.barDockControlBottom.Size = new System.Drawing.Size(544, 0);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 438);
            //
            //barDockControlRight
            //
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(544, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 438);
            //
            //Form_Review
            //
            this.ClientSize = new System.Drawing.Size(544, 462);
            this.Controls.Add(this.LayoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Form_Review";
            this.Text = "List of Pending Events";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            this.ResumeLayout(false);
        }

        internal DevExpress.XtraEditors.DataNavigator DataNavigator1;
        internal DevExpress.XtraEditors.LabelControl LabelClient;
        internal DevExpress.XtraEditors.LabelControl LabelName;
        internal DevExpress.XtraEditors.LabelControl LabelCoApplicant;
        internal DevExpress.XtraEditors.LabelControl LabelStatus;
        internal DevExpress.XtraEditors.LabelControl LabelHomePhone;
        internal DevExpress.XtraEditors.LabelControl LabelWorkPhone;
        internal DevExpress.XtraEditors.LabelControl LabelMsgPhone;
        internal DevExpress.XtraEditors.SimpleButton ButtonShow;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal RetentionEventList RetentionEventList1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar Bar2;
        internal DevExpress.XtraBars.BarSubItem MenuFile;
        internal DevExpress.XtraBars.BarSubItem MenuFileDeposits;
        internal DevExpress.XtraBars.BarCheckItem MenuFileDepositsAny;
        internal DevExpress.XtraBars.BarCheckItem MenuFileDepositsFirst;
        internal DevExpress.XtraBars.BarCheckItem MenuFileDepositsSecond;
        internal DevExpress.XtraBars.BarSubItem MenuFileAppointments;
        internal DevExpress.XtraBars.BarCheckItem MenuFileAppointmentsAny;
        internal DevExpress.XtraBars.BarCheckItem MenuFileAppointmentsFirst;
        internal DevExpress.XtraBars.BarCheckItem MenuFileAppointmentsSecond;
        internal DevExpress.XtraBars.BarCheckItem MenuFileNew;
        internal DevExpress.XtraBars.BarCheckItem MenuFileRefresh;
        internal DevExpress.XtraBars.BarCheckItem MenuFilePrint;
        internal DevExpress.XtraBars.BarCheckItem MenuFileExit;
        internal DevExpress.XtraBars.BarSubItem MenuEdit;
        internal DevExpress.XtraBars.BarCheckItem MenuEditCut;
        internal DevExpress.XtraBars.BarCheckItem MenuEditCopy;
        internal DevExpress.XtraBars.BarCheckItem MenuEditPaste;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;

        internal DevExpress.XtraBars.BarDockControl barDockControlRight;

        #endregion " Windows Form Designer generated code "

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MenuFile_Popup(object sender, EventArgs e)
        {
            DataTable tbl = CommonStorage.ds.Tables["selected_clients"];
            MenuFilePrint.Enabled = tbl != null && tbl.Rows.Count > 0;
        }

        private void MenuFileAppointmentsAny_Click(object sender, EventArgs e)
        {
            string CounselorString = CounselorAndOffice();

            if (CounselorString != "CANCEL")
            {
                // Clear the current cheched status items for the file menus
                //MenuFileDepositsFirst.Checked = False
                //MenuFileDepositsSecond.Checked = False
                //MenuFileDepositsAny.Checked = False
                //MenuFileAppointmentsFirst.Checked = False
                //MenuFileAppointmentsSecond.Checked = False
                //MenuFileAppointmentsAny.Checked = False
                //MenuFileNew.Checked = False

                //MenuFileAppointmentsAny.Checked = True
                MenuSelections("vc.retention_event in (3,4) " + CounselorString, false, false);
            }
        }

        private void MenuFileAppointmentsFirst_Click(object sender, EventArgs e)
        {
            string CounselorString = CounselorAndOffice();

            if (CounselorString != "CANCEL")
            {
                // Clear the current cheched status items for the file menus
                //MenuFileDepositsFirst.Checked = False
                //MenuFileDepositsSecond.Checked = False
                //MenuFileDepositsAny.Checked = False
                //MenuFileAppointmentsFirst.Checked = False
                //MenuFileAppointmentsSecond.Checked = False
                //MenuFileAppointmentsAny.Checked = False
                //MenuFileNew.Checked = False

                //MenuFileAppointmentsFirst.Checked = True
                MenuSelections("vc.retention_event = 3 " + CounselorString, false, false);
            }
        }

        private void MenuFileAppointmentsSecond_Click(object sender, EventArgs e)
        {
            string CounselorString = CounselorAndOffice();

            if (CounselorString != "CANCEL")
            {
                // Clear the current cheched status items for the file menus
                //MenuFileDepositsFirst.Checked = False
                //MenuFileDepositsSecond.Checked = False
                //MenuFileDepositsAny.Checked = False
                //MenuFileAppointmentsFirst.Checked = False
                //MenuFileAppointmentsSecond.Checked = False
                //MenuFileAppointmentsAny.Checked = False
                //MenuFileNew.Checked = False

                //MenuFileAppointmentsSecond.Checked = True
                MenuSelections("vc.retention_event = 4 " + CounselorString, false, false);
            }
        }

        private void MenuFileDepositsAny_Click(object sender, EventArgs e)
        {
            string CounselorString = CounselorAndOffice();

            if (CounselorString != "CANCEL")
            {
                // Clear the current cheched status items for the file menus
                //MenuFileDepositsFirst.Checked = False
                //MenuFileDepositsSecond.Checked = False
                //MenuFileDepositsAny.Checked = False
                //MenuFileAppointmentsFirst.Checked = False
                //MenuFileAppointmentsSecond.Checked = False
                //MenuFileAppointmentsAny.Checked = False
                //MenuFileNew.Checked = False

                //MenuFileDepositsAny.Checked = True
                MenuSelections("vc.retention_event IN (1,2) " + CounselorString, false, false);
            }
        }

        private void MenuFileDepositsFirst_Click(object sender, EventArgs e)
        {
            string CounselorString = CounselorAndOffice();

            if (CounselorString != "CANCEL")
            {
                // Clear the current cheched status items for the file menus
                //MenuFileDepositsFirst.Checked = False
                //MenuFileDepositsSecond.Checked = False
                //MenuFileDepositsAny.Checked = False
                //MenuFileAppointmentsFirst.Checked = False
                //MenuFileAppointmentsSecond.Checked = False
                //MenuFileAppointmentsAny.Checked = False
                //MenuFileNew.Checked = False

                //MenuFileDepositsFirst.Checked = True
                MenuSelections("vc.retention_event = 1 " + CounselorString, false, false);
            }
        }

        private void MenuFileDepositsSecond_Click(object sender, EventArgs e)
        {
            string CounselorString = CounselorAndOffice();

            if (CounselorString != "CANCEL")
            {
                // Clear the current cheched status items for the file menus
                //MenuFileDepositsFirst.Checked = False
                //MenuFileDepositsSecond.Checked = False
                //MenuFileDepositsAny.Checked = False
                //MenuFileAppointmentsFirst.Checked = False
                //MenuFileAppointmentsSecond.Checked = False
                //MenuFileAppointmentsAny.Checked = False
                //MenuFileNew.Checked = False

                //MenuFileDepositsSecond.Checked = True
                MenuSelections("vc.retention_event = 2 " + CounselorString, false, false);
            }
        }

        private void MenuFileNew_Click(object sender, EventArgs e)
        {
            FieldSelection NewField = new FieldSelection();
            DialogResult answer = default(DialogResult);
            SelectionClause clause = null;

            // Ask for a new event id
            using (FieldForm frm = new FieldForm(IsSearch, NewField))
            {
                frm.Location = new Point(Left + (Width - frm.Width) / 2, Top + (Height - frm.Height) / 2);
                answer = frm.ShowDialog();
            }

            // Pass this event id to the relation form to build the selection criteria
            if (answer == DialogResult.OK)
            {
                using (RelationForm frm_relation = new RelationForm(IsSearch, NewField))
                {
                    var _with1 = frm_relation;
                    _with1.Text = "Find Retention Events";
                    _with1.Location = new Point(Left + (Width - _with1.Width) / 2, Top + (Height - _with1.Height) / 2);
                    answer = _with1.ShowDialog();
                    if (answer == DialogResult.OK)
                    {
                        clause = _with1.SelectionClause();
                    }
                }
            }

            // If we have a selection clause then request the information from the system
            if (clause != null && clause.Valid)
            {
                MenuSelections(clause.SelectionString, clause.UseClientDepositTable, clause.UseClientRetentionEventsTable);

                // Clear the current cheched status items for the file menus
                //MenuFileDepositsFirst.Checked = False
                //MenuFileDepositsSecond.Checked = False
                //MenuFileDepositsAny.Checked = False
                //MenuFileAppointmentsFirst.Checked = False
                //MenuFileAppointmentsSecond.Checked = False
                //MenuFileAppointmentsAny.Checked = False
                //MenuFileNew.Checked = False

                //MenuFileNew.Checked = True
            }
        }

        private void MenuFileRefresh_Click(object sender, EventArgs e)
        {
            MenuSelections();
        }

        private DataTable ClientsTable;
        private string CurrentSelectionClause = string.Empty;
        private bool CurrentUseClientDepositTable;

        private bool CurrentUseClientRetentionEventsTable;

        private void MenuSelections(string SelectionClause, bool UseClientDepositTable, bool UseClientRetentionEventsTable)
        {
            CurrentSelectionClause = SelectionClause;
            CurrentUseClientDepositTable = UseClientDepositTable;
            CurrentUseClientRetentionEventsTable = UseClientRetentionEventsTable;
            MenuSelections();
        }

        private void MenuSelections()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT DISTINCT vc.client FROM view_retention_client_events vc WITH (NOLOCK)");
            sb.Append(" INNER JOIN clients c ON vc.client = c.client");

            if (CurrentUseClientDepositTable)
            {
                sb.Append(" LEFT OUTER JOIN client_deposits cd WITH (NOLOCK) ON vc.client = cd.client");
            }

            if (CurrentUseClientRetentionEventsTable)
            {
                // The view contains all that we need now so we don't need this flag now.
            }

            sb.Append(" WHERE (vc.date_expired IS NULL)");

            if (CurrentSelectionClause != string.Empty)
            {
                sb.AppendFormat(" AND ({0})", CurrentSelectionClause);
            }

            sb.Append(" ORDER BY 1");

            MenuSelections(sb.ToString());
        }

        private void MenuSelections(string SelectStatement)
        {
            // Generate a read request to retrieve the client information
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (SelectStatement != string.Empty)
                {
                    ClientsTable = CommonStorage.ds.Tables["selected_clients"];
                    if (ClientsTable != null)
                    {
                        ClientsTable.Clear();
                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with2 = cmd;
                        _with2.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with2.CommandText = SelectStatement;
                        _with2.CommandType = CommandType.Text;
                        _with2.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(CommonStorage.ds, "selected_clients");
                        }
                    }

                    ClientsTable = CommonStorage.ds.Tables["selected_clients"];

                    // Attach the navigator to the dataset
                    DataNavigator1.DataSource = new DataView(ClientsTable, string.Empty, "client", DataViewRowState.CurrentRows);
                    ButtonShow.Enabled = ClientsTable.Rows.Count > 0;
                    MenuFileRefresh.Enabled = true;
                }

                DisplayClientInfo();
                RetentionEventList1.Reload(client);
            }
            catch (SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client list");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }
        }

        private static string CounselorAndOffice()
        {
            string answer = string.Empty;

            using (CounselorOfficeSelectionForm frm = new CounselorOfficeSelectionForm())
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    answer = frm.SelectionClause;
                }
                else
                {
                    answer = "CANCEL";
                }
            }

            return answer;
        }

        private void DataNavigator1_PositionChanged(object sender, EventArgs e)
        {
            RetentionEventList1.Reload(client);
            DisplayClientInfo();
        }

        private Int32 client
        {
            get
            {
                // Find the client that is current selected by the navigator
                Int32 CurrentClient = -1;
                var _with3 = DataNavigator1;
                DataView vue = (DataView)DataNavigator1.DataSource;
                if (_with3.Position >= 0)
                    CurrentClient = Convert.ToInt32(vue[_with3.Position]["client"]);

                return CurrentClient;
            }
        }

        private void DisplayClientInfo()
        {
            // Clear the client information
            LabelClient.Text = string.Empty;
            LabelCoApplicant.Text = string.Empty;
            LabelHomePhone.Text = string.Empty;
            LabelMsgPhone.Text = string.Empty;
            LabelName.Text = string.Empty;
            LabelStatus.Text = string.Empty;
            LabelWorkPhone.Text = string.Empty;

            // If there is a client then format the information for the display
            if (client < 0)
            {
                return;
            }
            LabelClient.Text = string.Format("{0:0000000}", client);

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT name, coapplicant, active_status, phone, work_ph, work_ext, message_ph FROM view_client_address with (nolock) WHERE client=@client";
                            cmd.Parameters.Add("@client", SqlDbType.Int).Value = client;
                            using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow))
                            {
                                if (! rd.Read())
                                {
                                    return;
                                }

                                string work_ph = null;
                                string work_ext = null;

                                if (!rd.IsDBNull(0))
                                {
                                    LabelName.Text = rd.GetString(0).Trim();
                                }

                                if (!rd.IsDBNull(1))
                                {
                                    LabelCoApplicant.Text = rd.GetString(1).Trim();
                                }

                                if (!rd.IsDBNull(2))
                                {
                                    LabelStatus.Text = rd.GetString(2).Trim();
                                }

                                if (!rd.IsDBNull(3))
                                {
                                    LabelHomePhone.Text = rd.GetString(3).Trim();
                                }

                                if (!rd.IsDBNull(4))
                                {
                                    work_ph = rd.GetString(4).Trim();
                                }
                                else
                                {
                                    work_ph = string.Empty;
                                }

                                if (!rd.IsDBNull(5))
                                {
                                    work_ext = rd.GetString(5).Trim();
                                }
                                else
                                {
                                    work_ext = string.Empty;
                                }

                                if (work_ph != string.Empty && work_ext != string.Empty)
                                {
                                    LabelWorkPhone.Text = string.Format("{0} Ext. {1}", work_ph, work_ext);
                                }
                                else
                                {
                                    LabelWorkPhone.Text = work_ph + work_ext;
                                }

                                if (!rd.IsDBNull(6))
                                {
                                    LabelMsgPhone.Text = rd.GetString(6).Trim();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information");
            }
        }

        private void Form_Closing(object sender, CancelEventArgs e)
        {
            RetentionEventList1.SaveChanges();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            MenuSelections();
            ButtonShow.Enabled = false;
            MenuFileRefresh.Enabled = false;
        }

        private void ButtonShow_Click(object sender, EventArgs e)
        {
            if (client >= 0)
            {
                var clntThrd = new ClientThread(client);
                var thrd = new Thread(clntThrd.EditClient);
                thrd.IsBackground = true;
                thrd.SetApartmentState(ApartmentState.STA);
                thrd.Name = Guid.NewGuid().ToString();
                thrd.Start();
            }
        }

        private class ClientThread
        {
            private readonly Int32 Client;

            public ClientThread(Int32 Client)
            {
                this.Client = Client;
            }

            public void EditClient()
            {
                using (var noteCls = new Notes.AlertNotes.Client())
                {
                    noteCls.ShowAlerts(Client);
                }

                using (var cuc = new ClientUpdateClass())
                {
                    cuc.ShowEditDialog(Client, false);
                }
            }
        }

        private void MenuFilePrint_Click(object sender, EventArgs e)
        {
            // Construct the selection statement for the report.
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT vc.client_retention_event, vc.client, vc.client_counselor, vc.client_office, vc.priority, vc.retention_event, vc.event_amount, vc.event_message, vc.date_created, vc.created_by, vc.name, vc.co_name, vc.active_status, vc.home_ph, vc.work_ph, vc.message_ph, vc.event_description, vc.action_date, vc.date_expired, vc.expire_type, vc.expire_date, vc.effective_date FROM view_retention_client_events vc WITH (NOLOCK)");
            sb.Append(" INNER JOIN clients c ON vc.client = c.client");

            if (CurrentUseClientDepositTable)
            {
                sb.Append(" LEFT OUTER JOIN client_deposits cd WITH (NOLOCK) ON vc.client = cd.client");
            }

            if (CurrentUseClientRetentionEventsTable)
            {
                // We no longer need the raw table. Everything is in the view. So, just don't bother here.
            }

            sb.Append(" WHERE (vc.date_expired IS NULL)");

            if (CurrentSelectionClause != string.Empty)
            {
                sb.AppendFormat(" AND ({0})", CurrentSelectionClause);
            }

            DebtPlus.Reports.Retention.List.Clients.ListClientsReport rpt = new DebtPlus.Reports.Retention.List.Clients.ListClientsReport();
            rpt.Parameter_SelectionCriteria = sb.ToString();
            rpt.RunReportInSeparateThread();
        }
    }
}