#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.UI.Desktop.CS.Retention.Common;

namespace DebtPlus.UI.Desktop.CS.Retention.Review
{
    internal partial class RetentionActionList : BaseGridControl
    {
        public RetentionActionList() : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_retention_action;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_message;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_action_description;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridColumn_retention_action = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_retention_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_message = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_action_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_action_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.SuspendLayout();
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_retention_action,
                this.GridColumn_message,
                this.GridColumn_date_created,
                this.GridColumn_created_by,
                this.GridColumn_action_description
            });
            this.GridView1.OptionsView.ShowPreview = true;
            this.GridView1.PreviewFieldName = "retention_message";
            //
            //GridColumn_retention_action
            //
            this.GridColumn_retention_action.Caption = "ID";
            this.GridColumn_retention_action.FieldName = "client_retention_action";
            this.GridColumn_retention_action.Name = "GridColumn_retention_action";
            //
            //GridColumn_message
            //
            this.GridColumn_message.Caption = "Note";
            this.GridColumn_message.FieldName = "message";
            this.GridColumn_message.Name = "GridColumn_message";
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.Caption = "Date";
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.Visible = true;
            this.GridColumn_date_created.VisibleIndex = 0;
            this.GridColumn_date_created.Width = 93;
            //
            //GridColumn_created_by
            //
            this.GridColumn_created_by.Caption = "Counselor";
            this.GridColumn_created_by.FieldName = "created_by";
            this.GridColumn_created_by.Name = "GridColumn_created_by";
            this.GridColumn_created_by.Visible = true;
            this.GridColumn_created_by.VisibleIndex = 1;
            this.GridColumn_created_by.Width = 103;
            //
            //GridColumn_action_description
            //
            this.GridColumn_action_description.Caption = "Action";
            this.GridColumn_action_description.FieldName = "retention_description";
            this.GridColumn_action_description.Name = "GridColumn_action_description";
            this.GridColumn_action_description.Visible = true;
            this.GridColumn_action_description.VisibleIndex = 2;
            this.GridColumn_action_description.Width = 240;
            //
            this.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True;
            this.GridView1.OptionsView.RowAutoHeight = true;
            this.GridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.GridView1.OptionsView.ShowPreview = true;
            this.GridView1.PreviewFieldName = "retention_message";
            this.GridView1.Appearance.Preview.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            //
            //RetentionActionList
            //
            this.Controls.Add(this.GridControl1);
            this.Name = "RetentionActionList";
            this.Size = new System.Drawing.Size(440, 150);
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private System.Int32 client_retention_event = -1;

        public void Reload(System.Int32 client_retention_event)
        {
            // Save the existing changes to the table before it is reloaded
            SaveChanges();

            // Save the new retention event if there is one.
            this.client_retention_event = client_retention_event;
            System.Data.DataTable tbl = CommonStorage.ds.Tables["client_retention_actions"];
            if (tbl != null)
                tbl.Clear();

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with1.CommandText = "SELECT [client_retention_action],[client_retention_event],[retention_action],[date_created],[created_by],[retention_message],[retention_description],[retention_letter_code],[letter_description] FROM view_retention_client_actions WHERE client_retention_event=@client_retention_event";
                    _with1.Parameters.Add("@client_retention_event", SqlDbType.Int).Value = client_retention_event;
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(CommonStorage.ds, "client_retention_actions");
                    }
                }

                tbl = CommonStorage.ds.Tables["client_retention_actions"];
                tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["client_retention_action"] };

                var _with2 = tbl.Columns["client_retention_action"];
                _with2.AutoIncrement = true;
                _with2.AutoIncrementSeed = -1;
                _with2.AutoIncrementStep = -1;

                System.Data.DataView vue = tbl.DefaultView;
                var _with3 = GridControl1;
                _with3.DataSource = vue;
                _with3.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client_retention_actions table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        protected override bool EditItem(System.Data.DataRowView drv)
        {
            bool answer = false;
            var _with4 = new Form_Retention_Action(drv);
            if (_with4.ShowDialog() == DialogResult.OK)
                answer = true;

            // If successful then update the date/time of the last action. It is always "now".
            if (answer)
            {
                System.Data.DataTable tbl = CommonStorage.ds.Tables["client_retention_events"];
                if (tbl != null)
                {
                    System.Data.DataRow row = tbl.Rows.Find(client_retention_event);
                    if (row != null)
                        row["action_date"] = DateTime.Now;
                }
            }

            _with4.Dispose();

            return answer;
        }

        protected override bool CreateItem(System.Data.DataRowView drv)
        {
            drv["client_retention_event"] = client_retention_event;
            drv["created_by"] = "me";
            drv["date_created"] = DateTime.Now;

            // Find the edit operation
            bool answer = EditItem(drv);
            return answer;
        }

        protected override bool DeleteItem(System.Data.DataRowView drv)
        {
            return DebtPlus.Data.Prompts.RequestConfirmation_Delete() == DialogResult.OK;
        }

        public void SaveChanges()
        {
            // Do the deletion items first. Delete the action items as well for any item being deleted
            System.Data.DataTable tbl = CommonStorage.ds.Tables["client_retention_actions"];

            if (tbl != null)
            {
                // Find the items that are added to the list
                using (System.Data.DataView vue = new System.Data.DataView(tbl, string.Empty, string.Empty, DataViewRowState.Added))
                {
                    foreach (System.Data.DataRowView drv in vue)
                    {
                        GeneratePossibleLetter(drv);
                    }
                }

                // Do the database update at this point
                System.Data.SqlClient.SqlCommand DeleteCmd = new SqlCommand();
                System.Data.SqlClient.SqlCommand InsertCmd = new SqlCommand();
                System.Data.SqlClient.SqlCommand UpdateCmd = new SqlCommand();
                Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                    {
                        var _with5 = DeleteCmd;
                        _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with5.CommandText = "DELETE FROM client_retention_actions WHERE client_retention_action=@client_retention_action";
                        _with5.Parameters.Add("@client_retention_action", SqlDbType.Int, Convert.ToByte(0), "client_retention_action");
                        da.DeleteCommand = DeleteCmd;

                        // Do the update of the retention events
                        var _with6 = UpdateCmd;
                        _with6.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with6.CommandText = "UPDATE client_retention_actions SET retention_action=@retention_action, message=@message WHERE client_retention_action=@client_retention_action";
                        _with6.Parameters.Add("@retention_action", SqlDbType.Int, Convert.ToByte(0), "retention_action");
                        _with6.Parameters.Add("@message", SqlDbType.VarChar, 1024, "message");
                        _with6.Parameters.Add("@client_retention_action", SqlDbType.Int, Convert.ToByte(0), "client_retention_action");
                        da.UpdateCommand = UpdateCmd;

                        // Do the insert of the retention events
                        var _with7 = InsertCmd;
                        _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with7.CommandText = "xpr_insert_client_retention_action";
                        _with7.CommandType = CommandType.StoredProcedure;
                        _with7.Parameters.Add("@client_retention_action", SqlDbType.Int, Convert.ToByte(0), "client_retention_action").Direction = ParameterDirection.ReturnValue;
                        _with7.Parameters.Add("@client_retention_event", SqlDbType.Int, Convert.ToByte(0), "client_retention_event");
                        _with7.Parameters.Add("@retention_action", SqlDbType.Int, Convert.ToByte(0), "retention_action");
                        _with7.Parameters.Add("@message", SqlDbType.VarChar, 1024, "message");
                        da.InsertCommand = InsertCmd;

                        // Update the table with the retention events
                        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                        da.Update(tbl);
                    }

                    tbl.AcceptChanges();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_retention_actions table");
                }
                finally
                {
                    InsertCmd.Dispose();
                    UpdateCmd.Dispose();
                    DeleteCmd.Dispose();
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }
        }

        private void GeneratePossibleLetter(System.Data.DataRowView drv)
        {
            Int32 RetentionAction = -1;
            if (drv["retention_action"] != null && !object.ReferenceEquals(drv["retention_action"], System.DBNull.Value))
            {
                RetentionAction = Convert.ToInt32(drv["retention_action"]);
            }

            // Find the client for the event. It is needed for the ClientId letters.
            Form_Retention_Event frm = (Form_Retention_Event)this.ParentForm;
            System.Data.DataRowView retention_event_drv = frm.drv;
            Int32 Client = Convert.ToInt32(retention_event_drv["client"]);

            // Generate the letter if needed
            GeneratePossibleLetter(Client, RetentionAction);
        }

        private void GeneratePossibleLetter(Int32 Client, Int32 RetentionAction)
        {
            // Locate the definition for the retention action in the tables
            System.Data.DataTable tbl = CommonStorage.RetentionActionsTable();
            if (tbl != null)
            {
                System.Data.DataRow row = tbl.Rows.Find(RetentionAction);
                if (row != null)
                {
                    string LetterCode = global::DebtPlus.Utils.Nulls.DStr(row["letter_code"]);
                    if (LetterCode != string.Empty)
                    {
                        PrintLetter(Client, LetterCode);
                    }
                }
            }
        }

        private void PrintLetter(Int32 Client, string LetterCode)
        {
            LetterThread letter_class = new LetterThread(Client, LetterCode);
            System.Threading.Thread thrd = new System.Threading.Thread(letter_class.Show);
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.IsBackground = false;
            thrd.Name = "LetterThread";
            thrd.Start();
        }

        private class LetterThread
        {
            private readonly Int32 Client;

            private readonly string LetterCode;

            public LetterThread(Int32 Client, string LetterCode)
            {
                this.Client = Client;
                this.LetterCode = LetterCode;
            }

            public void Show()
            {
                using (DebtPlus.Letters.Compose.Composition ltr = new DebtPlus.Letters.Compose.Composition(LetterCode))
                {
                    ltr.GetValue += LetterGetField;
                    ltr.PrintLetter();
                    ltr.GetValue -= LetterGetField;
                }
            }

            private void LetterGetField(object sender, DebtPlus.Events.ParameterValueEventArgs e)
            {
                if (e.Name == DebtPlus.Events.ParameterValueEventArgs.name_client)
                {
                    e.Value = Client;
                }
            }
        }
    }
}