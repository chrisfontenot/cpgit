using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Desktop.CS.Retention.Common;

namespace DebtPlus.UI.Desktop.CS.Retention.Review
{
    internal partial class Form_Retention_Action : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_Retention_Action() : base()
        {
            InitializeComponent();
            this.Load += Form_Retention_Action_Load;
            retention_action.EditValueChanged += retention_action_EditValueChanged;
        }

        private System.Data.DataRowView drv = null;

        public Form_Retention_Action(System.Data.DataRowView drv) : this()
        {
            this.drv = drv;
        }

        private System.Int32 client_retention_action
        {
            get
            {
                System.Int32 answer = -1;
                if (drv != null)
                {
                    if (drv["client_retention_action"] != null && !object.ReferenceEquals(drv["client_retention_event"], System.DBNull.Value))
                        answer = Convert.ToInt32(drv["client_retention_action"]);
                }
                return answer;
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cance;
        internal DevExpress.XtraEditors.LookUpEdit retention_action;
        internal DevExpress.XtraEditors.MemoEdit retention_message;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.retention_action = new DevExpress.XtraEditors.LookUpEdit();
            this.retention_message = new DevExpress.XtraEditors.MemoEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cance = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.retention_action.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.retention_message.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(8, 3);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(447, 39);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "This form shows the action for the indicated event. Choose the action that you wi" + "sh to post against the current event. You may enter a textual note if you wish t" + "o explain the action.";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            this.LabelControl1.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 48);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(30, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "&Action";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 88);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(23, 13);
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "&Note";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //retention_action
            //
            this.retention_action.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.retention_action.Location = new System.Drawing.Point(88, 48);
            this.retention_action.Name = "retention_action";
            this.retention_action.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.retention_action.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.retention_action.Properties.NullText = "";
            this.retention_action.Properties.ShowFooter = false;
            this.retention_action.Properties.ShowHeader = false;
            this.retention_action.Properties.ShowLines = false;
            this.retention_action.Size = new System.Drawing.Size(367, 20);
            this.retention_action.TabIndex = 2;
            this.retention_action.ToolTipController = this.ToolTipController1;
            this.retention_action.Properties.SortColumnIndex = 1;
            //
            //retention_message
            //
            this.retention_message.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.retention_message.Location = new System.Drawing.Point(88, 80);
            this.retention_message.Name = "retention_message";
            this.retention_message.Properties.MaxLength = 256;
            this.retention_message.Size = new System.Drawing.Size(367, 96);
            this.retention_message.TabIndex = 4;
            this.retention_message.ToolTipController = this.ToolTipController1;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(150, 192);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 5;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cance
            //
            this.Button_Cance.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cance.Location = new System.Drawing.Point(246, 192);
            this.Button_Cance.Name = "Button_Cance";
            this.Button_Cance.Size = new System.Drawing.Size(75, 23);
            this.Button_Cance.TabIndex = 6;
            this.Button_Cance.Text = "&Cancel";
            this.Button_Cance.ToolTipController = this.ToolTipController1;
            //
            //Form_Retention_Action
            //
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cance;
            this.ClientSize = new System.Drawing.Size(471, 230);
            this.Controls.Add(this.Button_Cance);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.retention_message);
            this.Controls.Add(this.retention_action);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Form_Retention_Action";
            this.Text = "Retention Action";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.retention_action.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.retention_message.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void Form_Retention_Action_Load(System.Object sender, System.EventArgs e)
        {
            // Define the list of retention events
            System.Data.DataTable tbl = CommonStorage.RetentionActionsTable();
            var _with1 = retention_action.Properties;
            _with1.DataSource = tbl.DefaultView;
            _with1.DisplayMember = "description";
            _with1.ValueMember = "retention_action";

            // Bind the fields to the items on the form
            retention_message.DataBindings.Clear();
            retention_message.DataBindings.Add("EditValue", drv, "retention_message");

            retention_action.DataBindings.Clear();
            retention_action.DataBindings.Add("EditValue", drv, "retention_action");
        }

        private void retention_action_EditValueChanged(object sender, System.EventArgs e)
        {
            bool Enabled = false;
            if (retention_action.EditValue != null && !object.ReferenceEquals(retention_action.EditValue, System.DBNull.Value))
                Enabled = true;
            Button_OK.Enabled = Enabled;

            if (Enabled)
            {
                System.Data.DataRowView ItemDRV = (System.Data.DataRowView)retention_action.Properties.GetDataSourceRowByKeyValue(retention_action.EditValue);
                if (ItemDRV == null)
                {
                    drv["retention_description"] = string.Empty;
                }
                else
                {
                    drv["retention_description"] = ItemDRV["description"];
                }
            }
        }
    }
}