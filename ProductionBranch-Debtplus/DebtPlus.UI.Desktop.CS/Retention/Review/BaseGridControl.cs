using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;
using System.Drawing;

namespace DebtPlus.UI.Desktop.CS.Retention.Review
{
    internal partial class BaseGridControl : System.Windows.Forms.UserControl
    {
        protected System.Int32 ControlRow = -1;

        public BaseGridControl() : base()
        {
            InitializeComponent();
            this.Load += BaseGridControl_Load;
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        protected DevExpress.XtraGrid.GridControl GridControl1;

        protected DevExpress.XtraGrid.Views.Grid.GridView GridView1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(512, 280);
            this.GridControl1.TabIndex = 15;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            //
            //BaseGridControl
            //
            this.Controls.Add(this.GridControl1);
            this.Name = "BaseGridControl";
            this.Size = new System.Drawing.Size(512, 280);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Create the context menu for the control
        /// </summary>
        protected virtual void DefineMenuItems(System.Windows.Forms.Menu.MenuItemCollection menu)
        {
            var _with1 = menu;
            _with1.Add(new MenuItem("Add", MenuItemCreate, System.Windows.Forms.Shortcut.Ins));
            _with1.Add(new MenuItem("Change", MenuItemEdit));
            _with1.Add(new MenuItem("Delete", MenuItemDelete, System.Windows.Forms.Shortcut.Del));
        }

        protected virtual System.Windows.Forms.ContextMenu myContextMenu()
        {
            System.Windows.Forms.ContextMenu menu = new System.Windows.Forms.ContextMenu();
            DefineMenuItems(menu.MenuItems);
            menu.Popup += ContextMenuPopup;
            return menu;
        }

        /// <summary>
        /// Handle the popup function for the context menu
        /// </summary>
        protected virtual void ContextMenuPopup(object sender, System.EventArgs e)
        {
            HandlePopup(((System.Windows.Forms.ContextMenu)sender).MenuItems);
        }

        private void HandlePopup(System.Windows.Forms.Menu.MenuItemCollection MenuItems)
        {
            foreach (System.Windows.Forms.MenuItem itm in MenuItems)
            {
                switch (itm.Text.ToLower())
                {
                    case "create":
                    case "add":
                        itm.Enabled = OkToCreate((System.Data.DataView)GridControl1.DataSource);
                        break;

                    case "delete":
                    case "remove":
                        itm.Enabled = (ControlRow >= 0) && OkToDelete((System.Data.DataRowView)GridView1.GetRow(ControlRow));
                        break;

                    case "edit":
                    case "update":
                    case "change":
                    case "show":
                        itm.Enabled = (ControlRow >= 0) && OkToEdit((System.Data.DataRowView)GridView1.GetRow(ControlRow));
                        break;
                }
            }
        }

        /// <summary>
        /// Process a CREATE menu item choice
        /// </summary>
        protected void MenuItemCreate(object sender, System.EventArgs e)
        {
            // Find the datarowview from the input tables.
            DataView vue = (DataView)GridControl1.DataSource;
            OnCreateItem(vue.Table.DefaultView);
        }

        /// <summary>
        /// Process an EDIT menu item choice
        /// </summary>

        protected void MenuItemEdit(object sender, System.EventArgs e)
        {
            // Find the datarowview from the input tables.
            System.Data.DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)GridControl1.DataSource;
                drv = (System.Data.DataRowView)GridView1.GetRow(ControlRow);
            }

            if (drv != null)
                OnEditItem(drv);
        }

        /// <summary>
        /// Process a DELETE menu item
        /// </summary>

        protected void MenuItemDelete(object sender, System.EventArgs e)
        {
            // Find the datarowview from the input tables.
            System.Data.DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)GridControl1.DataSource;
                drv = (System.Data.DataRowView)GridView1.GetRow(ControlRow);
            }

            if (drv != null)
                OnDeleteItem(drv);
        }

        /// <summary>
        /// Overridable function to edit the item in the list
        /// </summary>
        protected virtual bool EditItem(System.Data.DataRowView drv)
        {
            return false;
        }

        protected virtual void OnEditItem(System.Data.DataRowView drv)
        {
            drv.BeginEdit();
            if (EditItem(drv))
            {
                drv.EndEdit();
            }
            else
            {
                drv.CancelEdit();
            }
        }

        /// <summary>
        /// Overridable function to delete the item in the list
        /// </summary>
        protected virtual bool DeleteItem(System.Data.DataRowView drv)
        {
            return false;
        }

        protected virtual void OnDeleteItem(System.Data.DataRowView drv)
        {
            if (DeleteItem(drv))
                drv.Delete();
        }

        /// <summary>
        /// Is the item valid to be deleted?
        /// </summary>
        protected virtual bool OkToDelete(System.Data.DataRowView drv)
        {
            return true;
        }

        /// <summary>
        /// Is the item valid to be edited?
        /// </summary>
        protected virtual bool OkToEdit(System.Data.DataRowView drv)
        {
            return true;
        }

        /// <summary>
        /// Is the item valid to be created?
        /// </summary>
        protected virtual bool OkToCreate(System.Data.DataView vue)
        {
            return true;
        }

        /// <summary>
        /// Overridable function to create an item in the list
        /// </summary>
        protected virtual bool CreateItem(System.Data.DataRowView drv)
        {
            return false;
        }

        protected virtual void OnCreateItem(System.Data.DataView vue)
        {
            System.Data.DataRowView drv = vue.AddNew();
            drv.BeginEdit();
            if (CreateItem(drv))
            {
                drv.EndEdit();
            }
            else
            {
                drv.CancelEdit();
            }
        }

        /// <summary>
        /// Overridable function to indicate that the row was selected
        /// </summary>
        protected virtual void OnSelectItem(System.Data.DataRowView drv)
        {
        }

        /// <summary>
        /// Process a change in the current row for the control
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = gv.GridControl;
            ControlRow = e.FocusedRowHandle;

            // Find the datarowview from the input tables.
            System.Data.DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
            }

            // Ask the user to do something with the selected row
            OnSelectItem(drv);
        }

        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>

        private void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            if (hi.InRow)
            {
                ControlRow = hi.RowHandle;

                // Find the datarowview from the input tables.
                System.Data.DataRowView drv = null;
                if (ControlRow >= 0)
                {
                    DataView vue = (DataView)ctl.DataSource;
                    drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
                }

                // If there is a row then perform the edit operation. If successful, complete the changes else roll them back
                if (drv != null)
                    OnEditItem(drv);
            }
        }

        /// <summary>
        /// If we need it, here is a mouse-down procedure for the gridview
        /// </summary>
        private void GridView1_MouseDown(System.Object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo(new Point(e.X, e.Y));

            // Remember the position for the popup menu handler.
            if (hi.InRow)
            {
                ControlRow = hi.RowHandle;
            }
            else
            {
                ControlRow = -1;
            }
        }

        /// <summary>
        /// Load the control
        /// </summary>
        private void BaseGridControl_Load(object sender, System.EventArgs e)
        {
            GridControl1.ContextMenu = myContextMenu();
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
        }
    }
}