using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Desktop.CS.Retention.Common;

namespace DebtPlus.UI.Desktop.CS.Retention.Review
{
    internal partial class Form_Retention_Event : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_Retention_Event() : base()
        {
            InitializeComponent();
        }

        protected internal System.Data.DataRowView drv = null;

        public Form_Retention_Event(System.Data.DataRowView drv) : this()
        {
            this.drv = drv;

            this.Load += Form_Retention_Event_Load;
            base.Closing += Form_Retention_Event_Closing;
            expire_date.EditValueChanged += expire_date_EditValueChanged;
            retention_event.EditValueChanged += retention_event_EditValueChanged;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LookUpEdit retention_event;
        internal DevExpress.XtraEditors.SpinEdit priority;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal RetentionActionList RetentionActionList1;
        internal DevExpress.XtraEditors.CalcEdit amount;
        internal DevExpress.XtraEditors.MemoEdit message;
        internal DevExpress.XtraEditors.GroupControl expire_type;
        internal DevExpress.XtraEditors.DateEdit expire_date;
        internal DevExpress.XtraEditors.CheckEdit expire_type_3;
        internal DevExpress.XtraEditors.CheckEdit expire_type_2;
        internal DevExpress.XtraEditors.CheckEdit expire_type_1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.retention_event = new DevExpress.XtraEditors.LookUpEdit();
            this.amount = new DevExpress.XtraEditors.CalcEdit();
            this.message = new DevExpress.XtraEditors.MemoEdit();
            this.priority = new DevExpress.XtraEditors.SpinEdit();
            this.expire_type = new DevExpress.XtraEditors.GroupControl();
            this.expire_date = new DevExpress.XtraEditors.DateEdit();
            this.expire_type_3 = new DevExpress.XtraEditors.CheckEdit();
            this.expire_type_2 = new DevExpress.XtraEditors.CheckEdit();
            this.expire_type_1 = new DevExpress.XtraEditors.CheckEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.RetentionActionList1 = new RetentionActionList();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.retention_event.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.amount.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.priority.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type).BeginInit();
            this.expire_type.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.expire_date.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type_3.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type_2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type_1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(8, 0);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(490, 42);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "This form shows the client retention event. Below are the actions which have been" + " posted against this event.";
            this.LabelControl1.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(24, 40);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(28, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "&Event";
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(24, 64);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(37, 13);
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "&Amount";
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(24, 91);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(23, 13);
            this.LabelControl4.TabIndex = 7;
            this.LabelControl4.Text = "&Note";
            //
            //LabelControl5
            //
            this.LabelControl5.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl5.Location = new System.Drawing.Point(279, 68);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(34, 13);
            this.LabelControl5.TabIndex = 5;
            this.LabelControl5.Text = "Priority";
            //
            //retention_event
            //
            this.retention_event.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.retention_event.Location = new System.Drawing.Point(104, 40);
            this.retention_event.Name = "retention_event";
            this.retention_event.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.retention_event.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.retention_event.Properties.NullText = "...Please Choose One...";
            this.retention_event.Properties.ShowFooter = false;
            this.retention_event.Properties.ShowHeader = false;
            this.retention_event.Properties.ShowLines = false;
            this.retention_event.Size = new System.Drawing.Size(255, 20);
            this.retention_event.TabIndex = 2;
            this.retention_event.Properties.SortColumnIndex = 1;
            //
            //amount
            //
            this.amount.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.amount.Location = new System.Drawing.Point(104, 64);
            this.amount.Name = "amount";
            this.amount.Properties.Appearance.Options.UseTextOptions = true;
            this.amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.amount.Properties.DisplayFormat.FormatString = "c";
            this.amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.amount.Properties.EditFormat.FormatString = "c";
            this.amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.amount.Properties.Mask.BeepOnError = true;
            this.amount.Properties.Mask.EditMask = "c";
            this.amount.Size = new System.Drawing.Size(99, 20);
            this.amount.TabIndex = 4;
            //
            //message
            //
            this.message.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.message.Location = new System.Drawing.Point(104, 88);
            this.message.Name = "message";
            this.message.Properties.MaxLength = 256;
            this.message.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.message.Size = new System.Drawing.Size(255, 40);
            this.message.TabIndex = 8;
            //
            //priority
            //
            this.priority.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.priority.EditValue = new decimal(new Int32[] {
                9,
                0,
                0,
                0
            });
            this.priority.Location = new System.Drawing.Point(327, 64);
            this.priority.Name = "priority";
            this.priority.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.priority.Properties.IsFloatValue = false;
            this.priority.Properties.Mask.EditMask = "N00";
            this.priority.Properties.MaxLength = 1;
            this.priority.Properties.MaxValue = new decimal(new Int32[] {
                9,
                0,
                0,
                0
            });
            this.priority.Size = new System.Drawing.Size(32, 20);
            this.priority.TabIndex = 6;
            //
            //expire_type
            //
            this.expire_type.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.expire_type.Controls.Add(this.expire_date);
            this.expire_type.Controls.Add(this.expire_type_3);
            this.expire_type.Controls.Add(this.expire_type_2);
            this.expire_type.Controls.Add(this.expire_type_1);
            this.expire_type.Location = new System.Drawing.Point(8, 128);
            this.expire_type.Name = "expire_type";
            this.expire_type.Size = new System.Drawing.Size(495, 80);
            this.expire_type.TabIndex = 9;
            this.expire_type.Text = "Expires";
            //
            //expire_date
            //
            this.expire_date.EditValue = null;
            this.expire_date.Location = new System.Drawing.Point(160, 53);
            this.expire_date.Name = "expire_date";
            this.expire_date.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.expire_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.expire_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.expire_date.Size = new System.Drawing.Size(88, 20);
            this.expire_date.TabIndex = 3;
            //
            //expire_type_3
            //
            this.expire_type_3.Location = new System.Drawing.Point(8, 54);
            this.expire_type_3.Name = "expire_type_3";
            this.expire_type_3.Properties.Caption = "Expires automatically on";
            this.expire_type_3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.expire_type_3.Properties.RadioGroupIndex = 1;
            this.expire_type_3.Size = new System.Drawing.Size(144, 19);
            this.expire_type_3.TabIndex = 2;
            this.expire_type_3.TabStop = false;
            //
            //expire_type_2
            //
            this.expire_type_2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.expire_type_2.Location = new System.Drawing.Point(8, 35);
            this.expire_type_2.Name = "expire_type_2";
            this.expire_type_2.Properties.Caption = "Expires on next posted deposit of at least the dollar amount indicated above";
            this.expire_type_2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.expire_type_2.Properties.RadioGroupIndex = 1;
            this.expire_type_2.Size = new System.Drawing.Size(463, 19);
            this.expire_type_2.TabIndex = 1;
            this.expire_type_2.TabStop = false;
            //
            //expire_type_1
            //
            this.expire_type_1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.expire_type_1.EditValue = true;
            this.expire_type_1.Location = new System.Drawing.Point(8, 16);
            this.expire_type_1.Name = "expire_type_1";
            this.expire_type_1.Properties.Caption = "Never expires. Must be manually cancelled.";
            this.expire_type_1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.expire_type_1.Properties.RadioGroupIndex = 1;
            this.expire_type_1.Size = new System.Drawing.Size(463, 19);
            this.expire_type_1.TabIndex = 0;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(423, 48);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 11;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(423, 80);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 12;
            this.Button_Cancel.Text = "&Cancel";
            //
            //RetentionActionList1
            //
            this.RetentionActionList1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.RetentionActionList1.Location = new System.Drawing.Point(8, 216);
            this.RetentionActionList1.Name = "RetentionActionList1";
            this.RetentionActionList1.Size = new System.Drawing.Size(495, 144);
            this.RetentionActionList1.TabIndex = 10;
            //
            //Form_Retention_Event
            //
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(511, 366);
            this.Controls.Add(this.RetentionActionList1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.expire_type);
            this.Controls.Add(this.priority);
            this.Controls.Add(this.message);
            this.Controls.Add(this.amount);
            this.Controls.Add(this.retention_event);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Form_Retention_Event";
            this.Text = "Retention Event";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.retention_event.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.amount.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.priority.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type).EndInit();
            this.expire_type.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.expire_date.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type_3.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type_2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.expire_type_1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private System.Int32 client_retention_event
        {
            get
            {
                System.Int32 answer = -1;
                if (drv != null)
                {
                    if (drv["client_retention_event"] != null && !object.ReferenceEquals(drv["client_retention_event"], System.DBNull.Value))
                        answer = Convert.ToInt32(drv["client_retention_event"]);
                }
                return answer;
            }
        }

        private void Form_Retention_Event_Load(System.Object sender, System.EventArgs e)
        {
            // Define the list of retention events
            System.Data.DataTable tbl = CommonStorage.RetentionEventsTable();
            var _with1 = retention_event.Properties;
            _with1.DataSource = tbl.DefaultView;
            _with1.DisplayMember = "description";
            _with1.ValueMember = "item_key";

            // Bind the fields to the items on the form
            var _with2 = retention_event;
            _with2.DataBindings.Clear();
            _with2.DataBindings.Add("EditValue", drv, "retention_event");

            var _with3 = amount;
            _with3.DataBindings.Clear();
            _with3.DataBindings.Add("EditValue", drv, "event_amount");

            var _with4 = priority;
            _with4.DataBindings.Clear();
            _with4.DataBindings.Add("EditValue", drv, "priority");

            var _with5 = message;
            _with5.DataBindings.Clear();
            _with5.DataBindings.Add("EditValue", drv, "event_message");

            var _with6 = expire_date;
            _with6.DataBindings.Clear();
            _with6.DataBindings.Add("EditValue", drv, "expire_date");

            expire_type_1.CheckedChanged += expire_type_CheckChanged;
            expire_type_2.CheckedChanged += expire_type_CheckChanged;
            expire_type_3.CheckedChanged += expire_type_CheckChanged;

            // Based upon the expiration type, set the appropriate checkbox
            switch (Convert.ToInt32(drv["expire_type"]))
            {
                case 1:
                    expire_type_1.Checked = true;
                    break;

                case 2:
                    expire_type_2.Checked = true;
                    break;

                case 3:
                    expire_type_3.Checked = true;
                    break;
            }

            // Display the grid information
            var _with7 = RetentionActionList1;
            _with7.Reload(client_retention_event);

            Button_OK.Enabled = !HasErrors();
        }

        private void expire_type_CheckChanged(System.Object sender, System.EventArgs e)
        {
            if (expire_type_1.Checked)
            {
                drv["expire_type"] = 1;
                expire_date.Enabled = false;
            }
            else if (expire_type_2.Checked)
            {
                drv["expire_type"] = 2;
                expire_date.Enabled = false;
            }
            else if (expire_type_3.Checked)
            {
                drv["expire_type"] = 3;
                expire_date.Enabled = true;
            }

            Button_OK.Enabled = !HasErrors();
        }

        private void Form_Retention_Event_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            RetentionActionList1.SaveChanges();
        }

        private void expire_date_EditValueChanged(object sender, System.EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private void retention_event_EditValueChanged(object sender, System.EventArgs e)
        {
            bool Enabled = !HasErrors();
            Button_OK.Enabled = Enabled;

            if (!Enabled)
            {
                drv["event_description"] = string.Empty;
            }
            else
            {
                System.Data.DataRowView event_row = (System.Data.DataRowView)retention_event.Properties.GetDataSourceRowByKeyValue(retention_event.EditValue);
                drv["event_description"] = event_row["description"];
            }
        }

        private bool HasErrors()
        {
            bool answer = false;

            if (retention_event.EditValue == null || object.ReferenceEquals(retention_event.EditValue, System.DBNull.Value))
            {
                answer = true;
            }
            else
            {
                if (expire_type_3.Checked)
                {
                    if (expire_date.EditValue == null || object.ReferenceEquals(expire_date.EditValue, System.DBNull.Value))
                    {
                        answer = true;
                    }

                    if (!answer)
                    {
                        if (expire_date.DateTime.Date <= DateTime.Now.Date)
                        {
                            answer = true;
                        }
                    }
                }
            }

            return answer;
        }
    }
}