#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Retention.Review
{
    internal partial class RetentionEventList : BaseGridControl
    {
        public RetentionEventList() : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_expired;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_message;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_retention_event;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_description;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_amount;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_counselor;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_last_action;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.GridColumn_date_expired = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_expired.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client_retention_event = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client_retention_event.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_message = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_counselor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_last_action = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_last_action.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.SuspendLayout();
            //
            //GridColumn_date_expired
            //
            this.GridColumn_date_expired.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_date_expired.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_expired.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_date_expired.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_expired.Caption = "Expired Date";
            this.GridColumn_date_expired.DisplayFormat.FormatString = "d";
            this.GridColumn_date_expired.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_expired.FieldName = "date_expired";
            this.GridColumn_date_expired.GroupFormat.FormatString = "d";
            this.GridColumn_date_expired.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_expired.Name = "GridColumn_date_expired";
            //
            //GridColumn_client_retention_event
            //
            this.GridColumn_client_retention_event.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client_retention_event.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_retention_event.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client_retention_event.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_retention_event.Caption = "ID";
            this.GridColumn_client_retention_event.CustomizationCaption = "Record ID";
            this.GridColumn_client_retention_event.DisplayFormat.FormatString = "f0";
            this.GridColumn_client_retention_event.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_retention_event.FieldName = "client_retention_event";
            this.GridColumn_client_retention_event.GroupFormat.FormatString = "f0";
            this.GridColumn_client_retention_event.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_retention_event.Name = "GridColumn_client_retention_event";
            this.GridColumn_client_retention_event.Width = 69;
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.Caption = "Date";
            this.GridColumn_date_created.DisplayFormat.FormatString = "d";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.GroupFormat.FormatString = "d";
            this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.Visible = true;
            this.GridColumn_date_created.VisibleIndex = 0;
            this.GridColumn_date_created.Width = 80;
            this.GridColumn_date_created.SortIndex = 0;
            this.GridColumn_date_created.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_date_created.SortOrder = DevExpress.Data.ColumnSortOrder.Descending;
            //
            //GridColumn_description
            //
            this.GridColumn_description.Caption = "Description";
            this.GridColumn_description.FieldName = "event_description";
            this.GridColumn_description.Name = "GridColumn_description";
            this.GridColumn_description.Visible = true;
            this.GridColumn_description.VisibleIndex = 1;
            this.GridColumn_description.Width = 286;
            //
            //GridColumn_message
            //
            this.GridColumn_message.Caption = "Message";
            this.GridColumn_message.FieldName = "message";
            this.GridColumn_message.Name = "GridColumn_message";
            this.GridColumn_message.Visible = false;
            this.GridColumn_message.VisibleIndex = -1;
            this.GridColumn_message.Width = 286;
            //
            //GridColumn_amount
            //
            this.GridColumn_amount.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_amount.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_amount.Caption = "Amount";
            this.GridColumn_amount.DisplayFormat.FormatString = "c";
            this.GridColumn_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_amount.FieldName = "event_amount";
            this.GridColumn_amount.GroupFormat.FormatString = "c";
            this.GridColumn_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_amount.Name = "GridColumn_amount";
            this.GridColumn_amount.Visible = true;
            this.GridColumn_amount.VisibleIndex = 2;
            this.GridColumn_amount.Width = 64;
            //
            //GridColumn_counselor
            //
            this.GridColumn_counselor.Caption = "Counselor";
            this.GridColumn_counselor.FieldName = "created_by";
            this.GridColumn_counselor.Name = "GridColumn_counselor";
            this.GridColumn_counselor.Visible = true;
            this.GridColumn_counselor.VisibleIndex = 3;
            this.GridColumn_counselor.Width = 60;
            //
            //GridColumn_last_action
            //
            this.GridColumn_last_action.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_last_action.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_last_action.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_last_action.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_last_action.Caption = "Lst Action";
            this.GridColumn_last_action.DisplayFormat.FormatString = "d";
            this.GridColumn_last_action.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_last_action.FieldName = "action_date";
            this.GridColumn_last_action.GroupFormat.FormatString = "d";
            this.GridColumn_last_action.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_last_action.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_last_action.Name = "GridColumn_last_action";
            this.GridColumn_last_action.Visible = true;
            this.GridColumn_last_action.VisibleIndex = 4;
            this.GridColumn_last_action.Width = 90;
            //
            //GridView1
            //
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_client_retention_event,
                this.GridColumn_date_created,
                this.GridColumn_description,
                this.GridColumn_amount,
                this.GridColumn_counselor,
                this.GridColumn_last_action,
                this.GridColumn_date_expired,
                this.GridColumn_message
            });
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(192), Convert.ToByte(192));
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(192), Convert.ToByte(192));
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(255), Convert.ToByte(192), Convert.ToByte(192));
            StyleFormatCondition1.Appearance.Options.UseBackColor = true;
            StyleFormatCondition1.Appearance.Options.UseBorderColor = true;
            StyleFormatCondition1.ApplyToRow = true;
            StyleFormatCondition1.Column = this.GridColumn_date_expired;
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.GreaterOrEqual;
            StyleFormatCondition1.Value1 = new System.DateTime(1800, 1, 1, 0, 0, 0, 0);
            this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] { StyleFormatCondition1 });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.OptionsView.ShowPreview = true;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            //
            //Preview lines are the message field
            //
            this.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True;
            this.GridView1.OptionsView.RowAutoHeight = true;
            this.GridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.GridView1.OptionsView.ShowPreview = true;
            this.GridView1.PreviewFieldName = "event_message";
            //
            //RetentionEventList
            //
            this.Controls.Add(this.GridControl1);
            this.Name = "RetentionEventList";
            this.Size = new System.Drawing.Size(512, 280);
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private System.Int32 client = -1;

        public void Reload(System.Int32 Client)
        {
            System.Data.DataSet ds = new System.Data.DataSet("ds");

            // Save the changes to the table before the client is changed
            SaveChanges();

            this.client = Client;
            if (Client < 0)
            {
                GridControl1.DataSource = null;
            }
            else
            {
                // Build the command to retrieve the list of retention events for this client
                System.Data.DataTable tbl = ds.Tables["client_retention_events"];
                if (tbl != null)
                    tbl.Reset();

                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT v.client_retention_event, v.client, v.client_counselor, v.client_office, v.priority, v.retention_event, v.event_amount, v.event_message, v.date_created, v.created_by, v.name, v.co_name, v.active_status, v.home_ph, v.work_ph, v.message_ph, v.event_description, v.action_date, v.date_expired, v.expire_type, v.expire_date, v.effective_date, convert(varchar(10), v.date_expired, 101) as formatted_expire_date FROM view_retention_client_events v with (nolock) WHERE v.client=@client";
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Client;

                    // Read the list of retention events for this client
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "client_retention_events");
                    }
                }
                tbl = ds.Tables[0];

                tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["client_retention_event"] };
                var col = tbl.Columns["client_retention_event"];
                col.AutoIncrement = true;
                col.AutoIncrementSeed = -1;
                col.AutoIncrementStep = -1;

                GridControl1.BeginUpdate();
                GridControl1.DataSource = tbl.DefaultView;
                GridControl1.RefreshDataSource();
                GridControl1.EndUpdate();
            }
        }

        protected override bool EditItem(System.Data.DataRowView drv)
        {
            bool answer = false;
            using (Form_Retention_Event frm = new Form_Retention_Event(drv))
            {
                answer = (frm.ShowDialog() == DialogResult.OK);
            }

            return answer;
        }

        protected override bool CreateItem(System.Data.DataRowView drv)
        {
            drv["client"] = client;
            drv["created_by"] = "me";
            drv["date_created"] = DateTime.Now;
            drv["expire_type"] = 1;
            return EditItem(drv);
        }

        protected override bool DeleteItem(System.Data.DataRowView drv)
        {
            // Items no longer pending are not deletable
            bool Pending = true;
            if (drv["date_expired"] != null && !object.ReferenceEquals(drv["date_expired"], System.DBNull.Value))
                Pending = false;
            if (!Pending)
                return false;

            // Ask if the item is to be deleted
            return DebtPlus.Data.Prompts.RequestConfirmation_Delete() == DialogResult.Yes;
        }

        public void SaveChanges()
        {
            System.Data.DataView vue = (System.Data.DataView)GridControl1.DataSource;
            if (vue != null)
            {
                System.Data.DataTable tbl = vue.Table;

                // Do the deletion items first. Delete the action items as well for any item being deleted
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                System.Data.SqlClient.SqlCommand DeleteCmd = new SqlCommand();
                System.Data.SqlClient.SqlCommand UpdateCmd = new SqlCommand();
                System.Data.SqlClient.SqlCommand InsertCmd = new SqlCommand();
                Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                    var _with4 = DeleteCmd;
                    _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with4.CommandText = "DELETE FROM client_retention_actions WHERE client_retention_event=@client_retention_event; DELETE FROM client_retention_events WHERE client_retention_event=@client_retention_event";
                    _with4.Parameters.Add("@client_retention_event", SqlDbType.Int, Convert.ToByte(0), "client_retention_event");
                    da.DeleteCommand = DeleteCmd;

                    // Do the update of the retention events
                    var _with5 = UpdateCmd;
                    _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with5.CommandText = "UPDATE client_retention_events SET retention_event=@retention_event, amount=@amount, priority=@priority, message=@message, expire_date=@expire_date, expire_type=@expire_type WHERE client_retention_event=@client_retention_event";
                    _with5.Parameters.Add("@retention_event", SqlDbType.Int, Convert.ToByte(0), "retention_event");
                    _with5.Parameters.Add("@amount", SqlDbType.Decimal, Convert.ToByte(0), "event_amount");
                    _with5.Parameters.Add("@priority", SqlDbType.Int, Convert.ToByte(0), "priority");
                    _with5.Parameters.Add("@message", SqlDbType.VarChar, 1024, "event_message");
                    _with5.Parameters.Add("@expire_date", SqlDbType.VarChar, 10, "formatted_expire_date");
                    _with5.Parameters.Add("@expire_type", SqlDbType.Int, Convert.ToByte(0), "expire_type");
                    _with5.Parameters.Add("@client_retention_event", SqlDbType.Int, Convert.ToByte(0), "client_retention_event");
                    da.UpdateCommand = UpdateCmd;

                    // Do the insert of the retention events
                    var _with6 = InsertCmd;
                    _with6.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with6.CommandText = "xpr_client_retention_event_create";
                    _with6.CommandType = CommandType.StoredProcedure;
                    _with6.Parameters.Add("@client_retention_event", SqlDbType.Int, Convert.ToByte(0), "client_retention_event").Direction = ParameterDirection.ReturnValue;
                    _with6.Parameters.Add("@retention_event", SqlDbType.Int, Convert.ToByte(0), "retention_event");
                    _with6.Parameters.Add("@amount", SqlDbType.Decimal, Convert.ToByte(0), "event_amount");
                    _with6.Parameters.Add("@priority", SqlDbType.Int, Convert.ToByte(0), "priority");
                    _with6.Parameters.Add("@message", SqlDbType.VarChar, 1024, "event_message");
                    _with6.Parameters.Add("@expire_date", SqlDbType.VarChar, 14, "formatted_expire_date");
                    _with6.Parameters.Add("@expire_type", SqlDbType.Int, Convert.ToByte(0), "expire_type");
                    da.InsertCommand = InsertCmd;

                    // Update the table with the retention events
                    da.Update(tbl);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client_retention_events table");
                }
                finally
                {
                    da.Dispose();
                    InsertCmd.Dispose();
                    UpdateCmd.Dispose();
                    DeleteCmd.Dispose();
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }
        }
    }
}