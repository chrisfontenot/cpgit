using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Desktop.CS.Retention.Common;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Retention.Review
{
    internal partial class CounselorOfficeSelectionForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public CounselorOfficeSelectionForm() : base()
        {
            InitializeComponent();
            this.Load += CounselorOfficeSelectionForm_Load;
            office.EditValueChanged += office_EditValueChanged;
            counselor.EditValueChanged += office_EditValueChanged;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.CheckEdit CheckEdit1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.LookUpEdit counselor;
        internal DevExpress.XtraEditors.LookUpEdit office;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.CheckEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.counselor = new DevExpress.XtraEditors.LookUpEdit();
            this.office = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.counselor.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.office.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(8, 16);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(272, 32);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Please choose the counselor and/or office to narrow the search.";
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 59);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(52, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "&Counselor:";
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(8, 83);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(33, 13);
            this.LabelControl3.TabIndex = 2;
            this.LabelControl3.Text = "&Office:";
            //
            //CheckEdit1
            //
            this.CheckEdit1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.CheckEdit1.EditValue = true;
            this.CheckEdit1.Location = new System.Drawing.Point(8, 168);
            this.CheckEdit1.Name = "CheckEdit1";
            this.CheckEdit1.Properties.Caption = "Include only &active clients";
            this.CheckEdit1.Size = new System.Drawing.Size(152, 19);
            this.CheckEdit1.TabIndex = 3;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(57, 128);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 4;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(161, 128);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "&Cancel";
            //
            //counselor
            //
            this.counselor.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.counselor.Location = new System.Drawing.Point(96, 56);
            this.counselor.Name = "counselor";
            this.counselor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.counselor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.counselor.Properties.NullText = "ANY COUNSELOR";
            this.counselor.Properties.ShowFooter = false;
            this.counselor.Properties.ShowHeader = false;
            this.counselor.Properties.ShowLines = false;
            this.counselor.Size = new System.Drawing.Size(184, 20);
            this.counselor.TabIndex = 6;
            this.counselor.Properties.SortColumnIndex = 1;
            //
            //office
            //
            this.office.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.office.Location = new System.Drawing.Point(96, 80);
            this.office.Name = "office";
            this.office.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.office.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.office.Properties.NullText = "ANY OFFICE";
            this.office.Properties.ShowFooter = false;
            this.office.Properties.ShowHeader = false;
            this.office.Properties.ShowLines = false;
            this.office.Size = new System.Drawing.Size(184, 20);
            this.office.TabIndex = 7;
            this.office.Properties.SortColumnIndex = 1;
            //
            //CounselorOfficeSelectionForm
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(292, 198);
            this.Controls.Add(this.office);
            this.Controls.Add(this.counselor);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.CheckEdit1);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "CounselorOfficeSelectionForm";
            this.Text = "Selection Information";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckEdit1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.counselor.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.office.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void CounselorOfficeSelectionForm_Load(object sender, System.EventArgs e)
        {
            // Load the counselor table
            System.Data.DataTable tbl = CommonStorage.CounselorsTable();
            System.Data.DataRow row = tbl.Rows.Find(-1);
            if (row == null)
            {
                tbl.Rows.Add(new object[] {
                    -1,
                    "ANY COUNSELOR"
                });
            }

            var _with1 = counselor.Properties;
            _with1.DisplayMember = "description";
            _with1.ValueMember = "item_key";
            _with1.DataSource = new System.Data.DataView(tbl, string.Empty, "description", DataViewRowState.CurrentRows);

            // Load the offices table
            tbl = CommonStorage.OfficesTable();
            row = tbl.Rows.Find(-1);
            if (row == null)
            {
                tbl.Rows.Add(new object[] {
                    -1,
                    "ANY OFFICE"
                });
            }

            var _with2 = office.Properties;
            _with2.DisplayMember = "description";
            _with2.ValueMember = "item_key";
            _with2.DataSource = new System.Data.DataView(tbl, string.Empty, "description", DataViewRowState.CurrentRows);
        }

        public string SelectionClause
        {
            get
            {
                string counselor_string = string.Empty;
                string office_string = string.Empty;
                string active_string = string.Empty;

                var _with3 = counselor;
                if (_with3.EditValue != null)
                    counselor_string = string.Format("(vc.[client_counselor]={0:d})", Convert.ToInt32(_with3.EditValue));

                var _with4 = office;
                if (_with4.EditValue != null)
                    office_string = string.Format("(vc.[client_office]={0:d})", Convert.ToInt32(_with4.EditValue));

                var _with5 = this.CheckEdit1;
                if (_with5.CheckState == CheckState.Checked)
                    active_string = "(vc.[active_status] IN ('A','AR'))";

                string Result = string.Empty;
                if (counselor_string != string.Empty)
                    Result += " AND " + counselor_string;
                if (office_string != string.Empty)
                    Result += " AND " + office_string;
                if (active_string != string.Empty)
                    Result += " AND " + active_string;
                return Result;
            }
        }

        private void office_EditValueChanged(object sender, System.EventArgs e)
        {
            var _with6 = (DevExpress.XtraEditors.LookUpEdit)sender;
            if (_with6.EditValue != null)
            {
                if (Convert.ToInt32(_with6.EditValue) < 0)
                    _with6.EditValue = null;
            }
        }
    }
}