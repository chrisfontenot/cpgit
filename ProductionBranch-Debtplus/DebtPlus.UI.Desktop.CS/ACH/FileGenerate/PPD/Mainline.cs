﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.UI.Desktop.CS.ACH.FileGenerate;

namespace DebtPlus.UI.Desktop.CS.ACH.FileGenerate.PPD
{
    public class Mainline : DebtPlus.Interfaces.Desktop.IDesktopMainline //, DebtPlus.Interfaces.Desktop.ISupportCreateClientRequest
    {
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new DebtPlus.Utils.EmptyArgParser();
                    if (ap.Parse((string[])param))
                    {
                        var frm = new MainForm(ap);
                        System.Windows.Forms.Application.Run(frm);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "ACHFileGeneratePPD"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}
