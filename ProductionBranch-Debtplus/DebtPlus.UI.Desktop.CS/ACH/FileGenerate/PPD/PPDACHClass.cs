﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using DebtPlus.UI.Desktop.CS.ACH.FileGenerate;

namespace DebtPlus.UI.Desktop.CS.ACH.FileGenerate.PPD
{
    internal class PPDACHClass : DebtPlus.UI.Desktop.CS.ACH.FileGenerate.ACHClass
    {
        public override void Dispose()
        {
            ds.Dispose();
            base.Dispose();
        }

        // Linkage to the database
        protected DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

        // Dataset for the transactions. Used in generating the report if desired.
        protected readonly DataSet ds = new DataSet("ds");

        /// <summary>
        /// Do the requested operation to generate the output text file
        /// </summary>
        public bool CreateACHFile(string Schedules, DateTime EffectiveDateValue, Int32 Bank, bool IncludePrenotes, string Filename, string Note)
        {
            bool Successful = false;

            // Save the effective dates for the batch headers
            EffectiveDate = EffectiveDateValue;

            // Open the output file to hold the results
            System.IO.FileStream OutputStream = new System.IO.FileStream(Filename, System.IO.FileMode.Create);
            System.IO.StreamWriter OutputWriter = new System.IO.StreamWriter(OutputStream);

            try
            {
                // Create a transaction for the operation. We want everything to just go away on an error
                DepositFile = xpr_ach_start_file(Schedules, Filename, EffectiveDate, Bank, Note);

                // Build the transactions into the system
                xpr_ach_generate_pull(Schedules, DepositFile, Bank);
                if (IncludePrenotes)
                {
                    xpr_ach_Generate_Prenote(DepositFile, Bank);
                }

                // Build the output file
                Successful = TransactionWrapper(OutputWriter, DepositFile, Bank);
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), Localization.ErrorCreatingACHFile, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Hand, System.Windows.Forms.MessageBoxDefaultButton.Button1);
            }
            finally
            {
                if (OutputWriter != null)
                {
                    OutputWriter.Close();
                    OutputWriter.Dispose();
                }
                if (OutputStream != null)
                {
                    OutputStream.Close();
                    OutputStream.Dispose();
                }

                // If we are not successful then discard the partial output file
                if (!Successful)
                {
                    System.IO.File.Delete(Filename);
                }
            }

            return Successful;
        }


        /// <summary>
        /// Process the main transaction data for the pull.
        /// </summary>
        protected bool TransactionWrapper(System.IO.TextWriter OutputWriter, Int32 DepositFile, Int32 Bank)
        {
            bool Successful = true;

            SqlTransaction txn = null;
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                txn = cn.BeginTransaction(IsolationLevel.RepeatableRead);

                // Read the configuration information now
                xpr_ach_config(cn, txn, Bank);

                // Read the list of items that are still pending for this file.
                using (var cmd = new SqlCommand())
                {
                    cmd.CommandText = "SELECT [ach_transaction],[ach_file],[transaction_code],[routing_number],[account_number],[amount],[client],[discretionary_data],[trace_number],[client_name] FROM view_ACH_Pending_Transactions WHERE ach_file=@ach_file ORDER BY routing_number, account_number";
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("@ach_file", SqlDbType.Int).Value = DepositFile;

                    // Dataset for the updated information
                    using (var da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "transactions");
                    }
                }

                // Process the items in the dataset
                DataView vue = ds.Tables[0].DefaultView;

                if (vue.Count > 0)
                {
                    // Include the prefix line(s)
                    if (! string.IsNullOrEmpty(ACH_bank.prefix_line))
                    {
                        OutputWriter.WriteLine(ACH_bank.prefix_line);
                    }

                    // Write the ACH header line
                    WriteFileHeaderLine(OutputWriter, DepositFile);

                    // Process the transactions
                    Int32 RecordIndex = 0;
                    while (RecordIndex >= 0)
                    {
                        WriteBatch(OutputWriter, ref vue, ref RecordIndex);
                    }

                    // Write the ACH trailer line
                    WriteFileTrailerLine(OutputWriter);

                    // Pad the file to a multiple of 10 lines
                    PaddOutputFile(OutputWriter);

                    // Include the suffix line(s)
                    if (! string.IsNullOrEmpty(ACH_bank.suffix_line))
                    {
                        OutputWriter.WriteLine(ACH_bank.suffix_line);
                    }

                    // Update the trace numbers in the banks table
                    if (!xpr_ach_trace_update(cn, txn, Bank))
                    {
                        DebtPlus.Data.Forms.MessageBox.Show(Localization.AnotherUserWasGeneratingAnACHFileAtTheSameTimeTheCurrentFileIsVoidedPleaseReExecuteTheExtractAgain, "Database changed", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Asterisk);
                        Successful = false;
                    }
                    else
                    {
                        // Perform the update to the database at this point
                        using (var UpdateCmd = new SqlCommand())
                        {
                            UpdateCmd.CommandText = "UPDATE deposit_batch_details SET reference=@trace_number WHERE deposit_batch_detail=@deposit_batch_detail";
                            UpdateCmd.Connection = cn;
                            UpdateCmd.Transaction = txn;

                            UpdateCmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 16, "trace_number");
                            UpdateCmd.Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 4, "ach_transaction");

                            // Set the update command and perform the update to the database for the changed rows
                            using (var da = new SqlDataAdapter() { UpdateCommand = UpdateCmd })
                            {
                                da.Update(ds.Tables[0]);
                            }
                        }
                    }

                    // If we are successful then close out the transaction and commit the changes
                    txn.Commit();
                    txn.Dispose();
                    txn = null;
                }
            }

            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch { }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                    cn.Dispose();
                }
            }

            return Successful;
        }


        /// <summary>
        /// Convert the relative trace number to a proper string
        /// </summary>
        protected string TraceNumber(Int32 TraceNumberDesired)
        {
            return TraceNumber(ACH_bank, TraceNumberDesired);
        }


        /// <summary>
        /// Create the new ACH file
        /// </summary>
        protected Int32 xpr_ach_start_file(string PullDates, string Filename, DateTime EffectiveDate, Int32 Bank, string Note)
        {
            Int32 Result = -1;

            string NewFilename = Filename;
            if (NewFilename.Length > 80)
            {
                NewFilename = NewFilename.Substring(NewFilename.Length - 80);
            }

            // Open the database connection to the store
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                cn.Open();

                // Try to find the directory from the data
                using (var cmd = new SqlCommand())
                {
                    cmd.CommandText = "xpr_ach_start_file";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;

                    // Find the parameters from the stored procedure
                    SqlCommandBuilder.DeriveParameters(cmd);

                    // Set the parameter values accordingly
                    cmd.Parameters["@PullDate"].Value = Convert.ToDateTime(PullDates);
                    cmd.Parameters["@Filename"].Value = NewFilename;
                    cmd.Parameters["@EffectiveDate"].Value = EffectiveDate;
                    cmd.Parameters["@bank"].Value = Bank;
                    if (cmd.Parameters.Contains("@Note"))
                    {
                        cmd.Parameters["@Note"].Value = Note;
                    }

                    // Create the batch
                    cmd.ExecuteNonQuery();

                    // The batch number is the returned parameter if we have no errors
                    Result = Convert.ToInt32(cmd.Parameters["@RETURN_VALUE"].Value);
                }
            }
            finally
            {
                if (cn != null)
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                    cn.Dispose();
                }
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            return Result;
        }


        /// <summary>
        /// Do the pull operation with the parameters previously set
        /// </summary>
        protected void xpr_ach_generate_pull(string PullDates, Int32 DepositBatchID, Int32 Bank)
        {
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                cn.Open();

                // Try to find the directory from the data
                using (var cmd = new SqlCommand())
                {
                    cmd.CommandText = "xpr_ACH_Generate_Pull";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);

                    // The rowcount is the result
                    cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 4).Direction = ParameterDirection.ReturnValue;

                    // Add the parameters
                    cmd.Parameters.Add("@ACH_Pull_Date", SqlDbType.DateTime).Value = Convert.ToDateTime(PullDates);
                    cmd.Parameters.Add("@ach_file", SqlDbType.Int).Value = DepositBatchID;
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = Bank;

                    // Create the batch
                    cmd.ExecuteNonQuery();
                }
            }

            finally
            {
                if (cn != null)
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                    cn.Dispose();
                }
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }


        /// <summary>
        /// Include any pending prenotes into the output file
        /// </summary>
        protected void xpr_ach_Generate_Prenote(Int32 DepositBatchID, Int32 Bank)
        {
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                cn.Open();

                // Try to find the directory from the data
                using (var cmd = new SqlCommand())
                {
                    cmd.CommandText = "xpr_ACH_Generate_Prenote";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;

                    // The rowcount is the result
                    cmd.Parameters.Add("RETURN", SqlDbType.Int, 4).Direction = ParameterDirection.ReturnValue;

                    // Add the parameters
                    cmd.Parameters.Add("@ach_file", SqlDbType.Int).Value = DepositBatchID;
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = Bank;

                    // Create the batch
                    cmd.ExecuteNonQuery();
                }

            }
            finally
            {
                if (cn != null)
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                    cn.Dispose();
                }
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }


        /// <summary>
        /// Retrieve the configuration information
        /// </summary>
        protected void xpr_ach_config(SqlConnection cn, SqlTransaction txn, Int32 bank)
        {
            // Retrieve the bank information
            ACH_bank = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Id == bank);
            if (ACH_bank == null)
            {
                throw new ApplicationException("BANK can not be found in banks table");
            }
#if false
            IDataReader rd = null;
            try
            {
                // Retrieve the parameters for the ACH file
                using (var cmd = new SqlCommand())
                {

                    cmd.CommandText = "xpr_ACH_config";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.Parameters.Add(new SqlParameter("@bank", SqlDbType.Int)).Value = bank;
                    rd = cmd.ExecuteReader(CommandBehavior.SingleRow);
                }

                // Process the fields in the result set to update the local storage
                if (rd != null && rd.Read())
                {
                    for (Int32 FieldNo = rd.FieldCount - 1; FieldNo >= 0; --FieldNo)
                    {
                        if (!rd.IsDBNull(FieldNo))
                        {
                            switch (rd.GetName(FieldNo).ToLower())
                            {
                                case "priority_code":
                                    ach_priority = rd.GetString(FieldNo).Trim();
                                    break;
                                case "immediate_destination":
                                    immediate_destination = rd.GetString(FieldNo).Trim();
                                    break;
                                case "immediate_destination_name":
                                    immediate_destination_name = rd.GetString(FieldNo).Trim();
                                    break;
                                case "immediate_origin":
                                    immediate_origin = rd.GetString(FieldNo).Trim();
                                    break;
                                case "immediate_origin_name":
                                    immediate_origin_name = rd.GetString(FieldNo).Trim();
                                    break;
                                case "reference_code":
                                    reference_code = rd.GetString(FieldNo).Trim();
                                    break;
                                case "originating_dfi_identification":
                                    originating_dfi_identification = rd.GetString(FieldNo).Trim();
                                    break;
                                case "standard_entry_class":
                                    standard_entry_class = rd.GetString(FieldNo).Trim();
                                    break;
                                case "company_entry_description":
                                    company_entry_description = rd.GetString(FieldNo).Trim();
                                    break;
                                case "prefix":
                                    prefix = rd.GetString(FieldNo).Trim();
                                    break;
                                case "suffix":
                                    suffix = rd.GetString(FieldNo).Trim();
                                    break;
                                case "batch_number":
                                    BatchTraceNumber = rd.GetInt32(FieldNo);
                                    break;
                                case "transaction_number":
                                    TransactionTraceNumber = rd.GetInt32(FieldNo);
                                    break;
                                case "ach_message_authentication":
                                    ach_message_authentication = rd.GetString(FieldNo).Trim();
                                    break;
                                case "ach_company_identification":
                                    ach_company_identification = rd.GetString(FieldNo).Trim();
                                    break;
                                case "current_date_time":
                                    CurrentDateTime = rd.GetDateTime(FieldNo);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }

            finally
            {
                if (rd != null && !rd.IsClosed)
                {
                    rd.Close();
                }
            }
#endif
        }


        /// <summary>
        /// Write the ACH file header line
        /// </summary>
        protected void WriteFileHeaderLine(System.IO.TextWriter OutputWriter, Int32 ach_file_number)
        {
            StringBuilder sb = new StringBuilder(100);

            sb.Append("1");                                         // record type
            sb.Append(ACH_bank.ach_priority.PadLeft(2, '0'));                // priority
            sb.Append(ACH_bank.immediate_destination.PadLeft(10, ' '));      // immediate destination
            sb.Append(ACH_bank.immediate_origin.PadRight(10, '0'));          // immediate origin
            sb.AppendFormat("{0:yyMMddHHmm}", CurrentDateTime);     // Current date and time
            sb.Append("1");                                         // Record type = 1 (header)
            sb.Append("094");                                       // record size
            sb.Append("10");                                        // number of records in a block
            sb.Append("1");                                         //

            sb.Append(ACH_bank.immediate_destination_name.PadRight(23).Substring(0, 23));    // Immediate destination name
            sb.Append(ACH_bank.immediate_origin_name.PadRight(23).Substring(0, 23));         // Immediate origin name
            sb.AppendFormat("{0:00000000}", ach_file_number);       // Our output file number

            Debug.Assert(sb.Length == 94);                          // We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString());                  // Write the output line
            LinesWritten += 1;                                      // and count it for the blocking factor

            fileHash = 0;                                           // Generate the new hash key and set it to zeros
            batchOffset = 0;                                        // Total number of batches
            FileTransactions = 0;                                   // Total number of transactions
            FileDebit = 0;                                          // Total debit amount (client's total deposit)
            FileCredit = 0;                                         // Total credit amount (used if offset deposit only)
        }


        /// <summary>
        /// Write the ACH file trailer line
        /// </summary>
        protected void WriteFileTrailerLine(System.IO.TextWriter OutputWriter)
        {
            StringBuilder sb = new StringBuilder(100);
            sb.Append("9");                                     // record type

            // Find the number of blocks written.
            Int32 Quotient;
            Int32 Remainder;
            Quotient = System.Math.DivRem(LinesWritten, 10, out Remainder);

            sb.AppendFormat("{0:000000}", batchOffset);         // Number of batches written
            sb.AppendFormat("{0:000000}", Quotient + 1);        // Number of blocks written
            sb.AppendFormat("{0:00000000}", FileTransactions);  // Number of detail lines written
            sb.AppendFormat("{0:0000000000}", fileHash);        // Hash sequence
            sb.AppendFormat("{0:000000000000}", FileDebit);     // Total debit amount
            sb.AppendFormat("{0:000000000000}", FileCredit);    // Total credit amount
            sb.Append(' ', 39);                                 // Blank fill remainder of the record

            Debug.Assert(sb.Length == 94);                      // We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString());              // Write the output line
            LinesWritten += 1;                                  // and count it for the blocking factor
        }


        /// <summary>
        /// Record the values for the new transaction counts
        /// </summary>
        protected bool xpr_ach_trace_update(SqlConnection cn, SqlTransaction txn, Int32 Bank)
        {
            // Find the next batch for the subsequent executions. We don't want to go to the
            // possible limit since the trace numbers need to be ascending WITHIN THE FILE so
            // wrap at the previous digit and do not allow the trace number of 0 to normally
            // exist in the file.
            Int32 NewBatchNumber = tr(ACH_bank.batch_number + batchOffset) % 9000000;
            if (NewBatchNumber == 0)
            {
                NewBatchNumber = 1;
            }

            // Do the same thing to the transaction trace number.
            Int32 NewTransactionNumber = tr(ACH_bank.transaction_number + FileTransactions) % 9000000;
            if (NewTransactionNumber == 0)
            {
                NewTransactionNumber = 1;
            }

            using (var dc = new DebtPlus.LINQ.BusinessContext(cn))
            {
                dc.Transaction = txn;
                var q = (from b in dc.banks where b.Id == ACH_bank.Id && b.batch_number == ACH_bank.batch_number && b.transaction_number == ACH_bank.transaction_number select b).FirstOrDefault();
                if (q != null)
                {
                    q.transaction_number = NewTransactionNumber;
                    q.batch_number = NewBatchNumber;
                    dc.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Ensure that the output file is a multiple of 10 lines
        /// </summary>
        protected void PaddOutputFile(System.IO.TextWriter OutputWriter)
        {
            // Write the padding lines (94 bytes of "9"s) until we have multiple of 10 lines
            while (LinesWritten % 10 != 0)
            {
                OutputWriter.WriteLine(new string('9', 94));
                LinesWritten += 1;
            }
        }


        /// <summary>
        /// Write the batch transactions to the output file
        /// </summary>
        protected void WriteBatch(System.IO.TextWriter OutputWriter, ref DataView vue, ref Int32 RecordIndex)
        {
            // Find the first pointer to the trustRegister
            DataRowView drv = vue[RecordIndex];

            // Generate the batch header
            WriteBatchHeader(OutputWriter, BatchType);

            // Process the records until we are at the end or the batch type changes
            while(true)
            {
                WriteTransaction(OutputWriter, drv);

                // If this is the eof then indicate that we have reached the end
                RecordIndex += 1;
                if (RecordIndex >= vue.Count)
                {
                    RecordIndex = -1;
                    break;
                }

                // Go on to the next record and look for a change in the type
                drv = vue[RecordIndex];

                //if( Convert.ToString(drv["record_type"]) != BatchType )
                //{
                //    break;
                //}
            }

            // Write the trailer when the type changes or we reach EOF
            WriteBatchTrailer(OutputWriter);
        }


        /// <summary>
        /// Write a batch header
        /// </summary>
        protected void WriteBatchHeader(System.IO.TextWriter OutputWriter, string Type)
        {
            StringBuilder sb = new StringBuilder(100);

            sb.Append("5");                                                 // record type
            sb.Append("200");                                               // record subtype
            sb.Append(ACH_bank.immediate_origin_name.PadRight(36).Substring(0, 36)); // immediate origin name
            sb.Append(ACH_bank.ach_company_identification.PadLeft(10, '0'));         // Company identification
            sb.Append(Type.PadRight(3, ' '));                               // Batch type
            sb.Append(company_entry_description.PadRight(10));              // description
            sb.AppendFormat("{0:yyMMdd}", EffectiveDate);                   // Company descriptive date
            sb.AppendFormat("{0:yyMMdd}", EffectiveDate);                   // Effective date
            sb.Append(new string(' ', 3));                                  // Filler
            sb.Append("1");                                                 // Batch type
            sb.Append(TraceNumber(batchOffset + ACH_bank.batch_number));    // Batch trace number

            Debug.Assert(sb.Length == 94);                                  // We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString());                          // Write the output line
            LinesWritten += 1;                                              // and count it for the blocking factor

            // Clear the batch counters
            BatchDebit = 0;
            BatchCredit = 0;
            BatchTransactions = 0;
            BatchHash = 0;
        }


        /// <summary>
        /// Write a batch trailer
        /// </summary>
        protected void WriteBatchTrailer(System.IO.TextWriter OutputWriter)
        {
            StringBuilder sb = new StringBuilder(100);

            sb.Append("8");                                         // record type
            sb.Append("200");
            sb.AppendFormat("{0:000000}", BatchTransactions);       // Number of transactions
            sb.AppendFormat("{0:0000000000}", BatchHash);           // Hash sequence
            sb.AppendFormat("{0:000000000000}", BatchDebit);        // Total debit amount
            sb.AppendFormat("{0:000000000000}", BatchCredit);       // Total credit amount
            sb.Append(ACH_bank.ach_company_identification.PadRight(10));     // Company identification
            sb.Append(ACH_bank.ach_message_authentication.PadRight(19));     // Message authentication
            sb.Append(new string(' ', 6));
            sb.Append(TraceNumber(batchOffset + ACH_bank.batch_number)); // Batch trace number

            Debug.Assert(sb.Length == 94);                          // We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString());                  // Write the output line
            LinesWritten += 1;                                      // and count it for the blocking factor

            // Count this batch
            batchOffset += 1;

            // Add the information to the file totals
            FileCredit += BatchCredit;
            FileDebit += BatchDebit;
            FileTransactions += BatchTransactions;
            fileHash += BatchHash;
        }


        /// <summary>
        /// Write a transaction
        /// </summary>
        protected void WriteTransaction(System.IO.TextWriter OutputWriter, DataRowView drv)
        {
            // These fields are used later. However, for errors, I want them decoded now.
            string MyAccountNumber = clean_string(drv["account_number"]).PadRight(17).Substring(0, 17);
            string MyClientID = DebtPlus.Utils.Format.Client.FormatClientID(Convert.ToInt32(drv["client"])).PadRight(15).Substring(0, 15);
            string MyClientName = clean_string(drv["client_name"]).PadRight(22).Substring(0, 22);
            string RoutingString = Convert.ToString(drv["routing_number"]);

            // Make the amount a number of cents rather than cents/dollars
            Int32 Amount = Convert.ToInt32(System.Decimal.Floor(Convert.ToDecimal(drv["amount"]) * 100m));

            // Adjust the total information
            switch (Convert.ToString(drv["transaction_code"]))
            {
                case "22":
                case "32":
                    BatchCredit += Amount;
                    break;
                case "27":
                case "37":
                    BatchDebit += Amount;
                    break;
                default:
                    break;
            }

            // The routing number must be exactly nine digits in length to be valid here.
            if (RoutingString.Length != 9)
            {
                throw new ApplicationException(string.Format("ABA number for client {0} is incorrect.", MyClientID));
            }

            // Remove the last digit from the routine number for the checksum algorithm. It is supposed to be only 8 digits
            // in length. The 9th is the checksum of the first 8 and is not included in the checksum here.
            BatchHash += Int64.Parse(RoutingString.Substring(0, 8));

            StringBuilder sb = new StringBuilder(100);

            sb.Append("6");                             // Transaction code
            sb.Append(Convert.ToString(drv["transaction_code"]).PadLeft(2));    // Transaction type
            sb.Append(RoutingString);                   // Routing number (already known to be 9 digits)
            sb.Append(MyAccountNumber);                 // Account number
            sb.AppendFormat("{0:0000000000}", Amount);  // Amount (in cents)
            sb.Append(MyClientID);                      // Client ID
            sb.Append(MyClientName);                    // Client name

            sb.Append("  ");
            sb.Append("0");

            string NewTraceNumber = TraceNumber(BatchTransactions + ACH_bank.transaction_number + FileTransactions);
            sb.Append(NewTraceNumber);

            // Update the trustRegister with the trace number
            drv.BeginEdit();
            drv["trace_number"] = NewTraceNumber;
            drv.EndEdit();

            // Write the text record
            Debug.Assert(sb.Length == 94);              // We must have 94 bytes EXACTLY

            OutputWriter.WriteLine(sb.ToString());      // Write the output line
            LinesWritten += 1;                          // and count it for the blocking factor

            // Count this transaction
            BatchTransactions += 1;
        }
    }
}
