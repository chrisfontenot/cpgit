﻿namespace DebtPlus.UI.Desktop.CS.ACH.FileGenerate.PPD
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            MainForm_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.TextEdit_Note = new DevExpress.XtraEditors.TextEdit();
            this.PullDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.FileNameText = new DevExpress.XtraEditors.ButtonEdit();
            this.CancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.IncludePrenotesCheckbox = new DevExpress.XtraEditors.CheckEdit();
            this.EffectiveDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.BankLookup = new DevExpress.XtraEditors.LookUpEdit();
            this.OKButton = new DevExpress.XtraEditors.SimpleButton();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Note.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PullDateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PullDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FileNameText.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludePrenotesCheckbox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveDateEdit.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankLookup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // TextEdit_Note
            // 
            this.TextEdit_Note.Location = new System.Drawing.Point(85, 148);
            this.TextEdit_Note.Name = "TextEdit_Note";
            this.TextEdit_Note.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.TextEdit_Note.Properties.MaxLength = 50;
            this.TextEdit_Note.Size = new System.Drawing.Size(378, 20);
            this.TextEdit_Note.StyleController = this.layoutControl1;
            this.TextEdit_Note.TabIndex = 24;
            // 
            // PullDateEdit
            // 
            this.PullDateEdit.EditValue = null;
            this.PullDateEdit.Location = new System.Drawing.Point(85, 40);
            this.PullDateEdit.Name = "PullDateEdit";
            this.PullDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PullDateEdit.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.PullDateEdit.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.False;
            this.PullDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.PullDateEdit.Size = new System.Drawing.Size(88, 20);
            this.PullDateEdit.StyleController = this.layoutControl1;
            this.PullDateEdit.TabIndex = 16;
            this.PullDateEdit.ToolTip = "Effective date for the transfer. It must be atleast tomorrow.";
            // 
            // FileNameText
            // 
            this.FileNameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FileNameText.EditValue = "";
            this.FileNameText.Location = new System.Drawing.Point(85, 124);
            this.FileNameText.Name = "FileNameText";
            this.FileNameText.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleRight, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to choose a file from a dialog", "lookup", null, false)});
            this.FileNameText.Properties.MaxLength = 256;
            this.FileNameText.Properties.ValidateOnEnterKey = true;
            this.FileNameText.Size = new System.Drawing.Size(378, 20);
            this.FileNameText.StyleController = this.layoutControl1;
            this.FileNameText.TabIndex = 22;
            this.FileNameText.ToolTip = "Name of the output file to store the ACH information";
            // 
            // CancelBtn
            // 
            this.CancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Location = new System.Drawing.Point(383, 70);
            this.CancelBtn.MaximumSize = new System.Drawing.Size(80, 26);
            this.CancelBtn.MinimumSize = new System.Drawing.Size(80, 26);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(80, 26);
            this.CancelBtn.StyleController = this.layoutControl1;
            this.CancelBtn.TabIndex = 27;
            this.CancelBtn.Text = "&Cancel";
            this.CancelBtn.ToolTip = "Cancel the operation.";
            // 
            // IncludePrenotesCheckbox
            // 
            this.IncludePrenotesCheckbox.EditValue = true;
            this.IncludePrenotesCheckbox.Location = new System.Drawing.Point(12, 172);
            this.IncludePrenotesCheckbox.Name = "IncludePrenotesCheckbox";
            this.IncludePrenotesCheckbox.Properties.Caption = "Include Prenotes in this file";
            this.IncludePrenotesCheckbox.Size = new System.Drawing.Size(451, 20);
            this.IncludePrenotesCheckbox.StyleController = this.layoutControl1;
            this.IncludePrenotesCheckbox.TabIndex = 25;
            this.IncludePrenotesCheckbox.ToolTip = "Should prenotes be included with this file? Normally they are.";
            // 
            // EffectiveDateEdit
            // 
            this.EffectiveDateEdit.EditValue = null;
            this.EffectiveDateEdit.Location = new System.Drawing.Point(85, 64);
            this.EffectiveDateEdit.Name = "EffectiveDateEdit";
            this.EffectiveDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EffectiveDateEdit.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.False;
            this.EffectiveDateEdit.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.False;
            this.EffectiveDateEdit.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.EffectiveDateEdit.Size = new System.Drawing.Size(88, 20);
            this.EffectiveDateEdit.StyleController = this.layoutControl1;
            this.EffectiveDateEdit.TabIndex = 18;
            this.EffectiveDateEdit.ToolTip = "Effective date for the transfer. It must be atleast tomorrow.";
            // 
            // BankLookup
            // 
            this.BankLookup.Location = new System.Drawing.Point(85, 100);
            this.BankLookup.Name = "BankLookup";
            this.BankLookup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.BankLookup.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("type", "Type", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.BankLookup.Properties.DisplayMember = "description";
            this.BankLookup.Properties.NullText = "";
            this.BankLookup.Properties.ShowFooter = false;
            this.BankLookup.Properties.SortColumnIndex = 2;
            this.BankLookup.Properties.ValueMember = "Id";
            this.BankLookup.Size = new System.Drawing.Size(378, 20);
            this.BankLookup.StyleController = this.layoutControl1;
            this.BankLookup.TabIndex = 20;
            this.BankLookup.ToolTip = "Select the ACH bank account that you wish to use for the transfer";
            // 
            // OKButton
            // 
            this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.OKButton.Location = new System.Drawing.Point(383, 40);
            this.OKButton.MaximumSize = new System.Drawing.Size(80, 26);
            this.OKButton.MinimumSize = new System.Drawing.Size(80, 26);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(80, 26);
            this.OKButton.StyleController = this.layoutControl1;
            this.OKButton.TabIndex = 26;
            this.OKButton.Text = "&OK";
            this.OKButton.ToolTip = "Start the ACH file generation using the parameters that you have selected.";
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.Location = new System.Drawing.Point(12, 12);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(450, 13);
            this.Label1.StyleController = this.layoutControl1;
            this.Label1.TabIndex = 14;
            this.Label1.Text = "Please enter the information needed to generate the ACH request data in the follo" +
    "wing fields.";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.TextEdit_Note);
            this.layoutControl1.Controls.Add(this.PullDateEdit);
            this.layoutControl1.Controls.Add(this.FileNameText);
            this.layoutControl1.Controls.Add(this.Label1);
            this.layoutControl1.Controls.Add(this.CancelBtn);
            this.layoutControl1.Controls.Add(this.IncludePrenotesCheckbox);
            this.layoutControl1.Controls.Add(this.OKButton);
            this.layoutControl1.Controls.Add(this.EffectiveDateEdit);
            this.layoutControl1.Controls.Add(this.BankLookup);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(475, 210);
            this.layoutControl1.TabIndex = 28;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem1,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(475, 210);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.PullDateEdit;
            this.layoutControlItem1.CustomizationFormText = "Pull Date";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(165, 24);
            this.layoutControlItem1.Text = "&Pull Date";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.EffectiveDateEdit;
            this.layoutControlItem2.CustomizationFormText = "Effective Date";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 52);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(165, 36);
            this.layoutControlItem2.Text = "&Effective Date";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.BankLookup;
            this.layoutControlItem3.CustomizationFormText = "Bank";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 88);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(455, 24);
            this.layoutControlItem3.Text = "&Bank";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.FileNameText;
            this.layoutControlItem4.CustomizationFormText = "&Output File";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 112);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(455, 24);
            this.layoutControlItem4.Text = "&Output File";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.TextEdit_Note;
            this.layoutControlItem5.CustomizationFormText = "&Note";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 136);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(455, 24);
            this.layoutControlItem5.Text = "&Note";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(69, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.IncludePrenotesCheckbox;
            this.layoutControlItem6.CustomizationFormText = "Prenotes";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 160);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(455, 30);
            this.layoutControlItem6.Text = "Prenotes";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(165, 28);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(206, 60);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.OKButton;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(371, 28);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(84, 30);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.CancelBtn;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(371, 58);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(84, 30);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.Label1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(455, 17);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 17);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 11);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 11);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(455, 11);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // MainForm
            // 
            this.AcceptButton = this.OKButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.CancelBtn;
            this.ClientSize = new System.Drawing.Size(475, 210);
            this.Controls.Add(this.layoutControl1);
            this.Name = "MainForm";
            this.Text = "ACH Deposit Batch Creation";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_Note.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PullDateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PullDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FileNameText.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IncludePrenotesCheckbox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveDateEdit.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EffectiveDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BankLookup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.TextEdit TextEdit_Note;
        internal DevExpress.XtraEditors.DateEdit PullDateEdit;
        internal DevExpress.XtraEditors.ButtonEdit FileNameText;
        internal DevExpress.XtraEditors.SimpleButton CancelBtn;
        internal DevExpress.XtraEditors.CheckEdit IncludePrenotesCheckbox;
        internal DevExpress.XtraEditors.DateEdit EffectiveDateEdit;
        internal DevExpress.XtraEditors.LookUpEdit BankLookup;
        internal DevExpress.XtraEditors.SimpleButton OKButton;
        internal DevExpress.XtraEditors.LabelControl Label1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    }
}