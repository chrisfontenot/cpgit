using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using DebtPlus.UI.Desktop.CS.ACH.FileGenerate;

namespace DebtPlus.UI.Desktop.CS.ACH.FileGenerate.PPD
{
    internal partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        string MySchedules;
        DateTime MyEffectiveDate;
        Int32 MyBank;
        bool MyIncludePrenotes;
        string MyFilename;
        PPDACHClass PPDACHCreateClass;

        DevExpress.Utils.WaitDialogForm wt;
        readonly BackgroundWorker bt = new BackgroundWorker();
        DebtPlus.Interfaces.IArgParser ap;

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="ap"></param>
        public MainForm(DebtPlus.Interfaces.IArgParser ap)
            : this()
        {
            this.ap = ap;
        }

        /// <summary>
        /// Process the disposal for the form. Remove any storage allocaetd in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void MainForm_Dispose(bool disposing)
        {
            if (disposing)
            {
                if (wt != null)
                {
                    if (wt.Visible)
                    {
                        wt.Close();
                    }
                    if (!wt.IsDisposed)
                    {
                        wt.Dispose();
                    }
                }
            }

            // Remove the handlers so that the form may be properly destroyed
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }

            ap = null;
            PPDACHCreateClass = null;
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public MainForm() : base()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            Resize += MainForm_Resize;
            OKButton.Click += OKButton_Click;
            CancelBtn.Click += CancelBtn_Click;
            EffectiveDateEdit.Validating += EffectiveDateEdit_Validating;
            FileNameText.ButtonClick += FileNameText_ButtonClick;
            FileNameText.TextChanged += FileNameText_TextChanged;
            BankLookup.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            BankLookup.EditValueChanged += BankLookup_EditValueChanged;
            bt.DoWork += bt_DoWork;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            Resize -= MainForm_Resize;
            OKButton.Click -= OKButton_Click;
            CancelBtn.Click -= CancelBtn_Click;
            EffectiveDateEdit.Validating -= EffectiveDateEdit_Validating;
            FileNameText.ButtonClick -= FileNameText_ButtonClick;
            FileNameText.TextChanged -= FileNameText_TextChanged;
            BankLookup.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            BankLookup.EditValueChanged -= BankLookup_EditValueChanged;
            bt.DoWork -= bt_DoWork;
            bt.RunWorkerCompleted -= bt_RunWorkerCompleted;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                BankLookup_Load();
                PullDateControl_Load();
                EffectiveDateControl_Load();
                FileNameText_Load();

                // Enable/Disable the OK button
                EnableOK();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private System.Collections.Generic.List<DebtPlus.LINQ.bank> colBanks;

        /// <summary>
        /// Load the bank information
        /// </summary>
        private void BankLookup_Load()
        {
            // Find the ACH banks
            colBanks = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.type == "A");
            BankLookup.Properties.DataSource = colBanks;

            // Set the default item if possible
            DebtPlus.LINQ.bank q = colBanks.Find(s => s.Default == true);
            if (q == null)
            {
                q = colBanks.OrderBy(s => s.Id).FirstOrDefault();
            }

            if (q != null)
            {
                BankLookup.EditValue = q.Id;
            }
        }

        /// <summary>
        /// When the item has been changed, update filename
        /// </summary>
        private void BankLookup_EditValueChanged(object sender, EventArgs e)
        {
            UpdateFilenameFromBanks();
        }

        /// <summary>
        /// Retrieve the list of schedules that are checked
        /// </summary>
        private string Schedules
        {
            get
            {
                return Convert.ToDateTime(PullDateEdit.EditValue).ToShortDateString();
            }
        }

        /// <summary>
        /// Cancel button was pressed
        /// </summary>
        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Load the default information for the pull date
        /// </summary>
        private void PullDateControl_Load()
        {
            PullDateEdit.Properties.NullDate = null;
            PullDateEdit.Properties.NullText = string.Empty;
            PullDateEdit.Properties.ShowToday = true;
            PullDateEdit.Properties.ShowClear = false;
            PullDateEdit.Properties.MinValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            PullDateEdit.EditValue = PullDateEdit.Properties.MinValue;
        }

        /// <summary>
        /// Load the default information for the effective date
        /// </summary>
        private void EffectiveDateControl_Load()
        {
            // Temporary item
            DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

            // Set the normal properties for the control
            EffectiveDateEdit.Properties.NullDate = null;
            EffectiveDateEdit.Properties.NullText = string.Empty;
            EffectiveDateEdit.Properties.ShowToday = false;
            EffectiveDateEdit.Properties.ShowClear = false;

            // Find the starting effective date.
            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT TOP 1 dt FROM calendar WHERE isBankHoliday=0 AND isWeekday=1 AND dt > getdate() ORDER BY dt";
                            cmd.CommandType = CommandType.Text;
                            EffectiveDateEdit.EditValue = cmd.ExecuteScalar();
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                using (var gdr = new DebtPlus.Repository.GetDataResult())
                {
                    gdr.HandleException(ex);
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, Localization.ErrorTryingToFindStartingDate);
                }
            }

            // The minimum date is the starting date.
            EffectiveDateEdit.Properties.MinValue = Convert.ToDateTime(EffectiveDateEdit.EditValue);
        }

        private void EffectiveDateEdit_Validating(object sender, CancelEventArgs e)
        {
            string ErrorMessage = string.Empty;
            DateTime CurrentValue = Convert.ToDateTime(EffectiveDateEdit.EditValue);

            // Temporary item
            DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT dt, isWeekday, isBankHoliday, HolidayDescription FROM calendar WHERE dt = @dt";
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.Add("@dt", SqlDbType.DateTime).Value = CurrentValue;
                            using (SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow))
                            {
                                if (rd != null && rd.Read())
                                {
                                    bool IsWeekday = (bool)rd.GetValue(1);
                                    bool IsBankHoliday = (bool)rd.GetValue(2);
                                    string HolidayDescription;
                                    if (!rd.IsDBNull(3))
                                        HolidayDescription = rd.GetString(3);
                                    else
                                        HolidayDescription = string.Empty;

                                    // Look for the various problems with the date as entered
                                    if (!IsWeekday)
                                    {
                                        ErrorMessage = Localization.MustBeMONDAYThroughFRIDAY;
                                    }
                                    else if (IsBankHoliday)
                                    {
                                        if (HolidayDescription != string.Empty)
                                        {
                                            ErrorMessage = string.Format(Localization.CanNotBeOn0, HolidayDescription);
                                        }
                                        else
                                        {
                                            ErrorMessage = Localization.MustNotBeOnABankingHoliday;
                                        }
                                    }
                                }
                                else
                                {
                                    ErrorMessage = Localization.DateNotDefinedInSystem;
                                }
                            }
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                using (var gdr = new DebtPlus.Repository.GetDataResult())
                {
                    gdr.HandleException(ex);
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, Localization.ErrorTryingToFindStartingDate);
                }
            }

            DxErrorProvider1.SetError(EffectiveDateEdit, ErrorMessage);
            EnableOK();
        }


        /// <summary>
        /// Enable or disable the OK button
        /// </summary>
        private void EnableOK()
        {
            // If the new value is defined then make sure that the office is still active.
            if (Schedules == String.Empty)
            {
                OKButton.Enabled = false;
                return;
            }

            // There must be a bank for the ACH
            if (BankLookup.EditValue == null)
            {
                OKButton.Enabled = false;
                return;
            }

            // There must be a filename for the text
            if (FileNameText.Text == String.Empty)
            {
                OKButton.Enabled = false;
                return;
            }

            // Determine if there are any other error conditions
            if (DxErrorProvider1.GetError(EffectiveDateEdit) != String.Empty)
            {
                OKButton.Enabled = false;
                return;
            }

            // There are no pending errors. Enable the OK button to start processing
            OKButton.Enabled = true;
        }


        /// <summary>
        /// Process the OK button
        /// </summary>
        private void OKButton_Click(object sender, EventArgs e)
        {
            // Determine the values for the new batch
            PPDACHCreateClass = new PPDACHClass();
            MySchedules = Schedules;
            MyEffectiveDate = Convert.ToDateTime(EffectiveDateEdit.EditValue);
            MyBank = Convert.ToInt32(BankLookup.EditValue);
            MyIncludePrenotes = (IncludePrenotesCheckbox.CheckState == CheckState.Checked);
            MyFilename = FileNameText.Text.Trim();

            wt = new DevExpress.Utils.WaitDialogForm("Please wait.", "Creating the ACH batch.");
            wt.Show();

            OKButton.Enabled = false;
            bt.RunWorkerAsync();
        }


        /// <summary>
        /// Generate the default attributes for the filename
        /// </summary>
        private void FileNameText_Load()
        {

            // Load the default directory from the bank table
            string FileName = String.Format(Localization.ACH0YyMMddHHmmTxt, DateTime.Now);
            FileNameText.Text = FileName;
            UpdateFilenameFromBanks();
        }


        /// <summary>
        /// Merge the directory with the filename portion
        /// </summary>
        private void UpdateFilenameFromBanks()
        {
            if (BankLookup.EditValue != null)
            {
                // Find the current bank and from that, the default directory
                Int32 bank = Convert.ToInt32(BankLookup.EditValue);
                string Directory = DefaultBankDirectory(bank);

                // Merge the new directory into the existing filename
                if (Directory != String.Empty)
                {
                    FileNameText.Text = System.IO.Path.Combine(Directory, System.IO.Path.GetFileName(FileNameText.Text.Trim()));
                }
            }
        }


        /// <summary>
        /// Process button clicks on the filename field
        /// </summary>
        private void FileNameText_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            string NewFileName = String.Empty;

            // Determine the type of the button. If lookup, do the filename lookup function.
            DevExpress.XtraEditors.ButtonEdit ctl = (DevExpress.XtraEditors.ButtonEdit)sender;
            switch ((string)e.Button.Tag)
            {
                // Do the filename lookup
                case "lookup":
                    using (var frm = new SaveFileDialog()
{
                        FileName = ((DevExpress.XtraEditors.ButtonEdit)sender).Text,
                        AddExtension = true,
                        CheckFileExists = false,
                        CheckPathExists = true,
                        CreatePrompt = false,
                        DefaultExt = ".txt",
                        Filter = Localization.TextFilesTxtTxtAllFiles,
                        FilterIndex = 0,
                        OverwritePrompt = true,
                        RestoreDirectory = true,
                        Title = Localization.ACHOutputFile,
                        ValidateNames = true })
                    {

                        if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            NewFileName = frm.FileName;
                        }
                    }
                    break;

                default:
                    Debug.Assert(false, Localization.ButtonTypeIsNotDefinedForFileNameTextField);
                    break;
            }

            // Update the control with the new filename if one is used
            if (NewFileName != String.Empty)
            {
                ctl.Text = NewFileName.Trim();
            }
        }


        /// <summary>
        /// Text for the file name was changed
        /// </summary>
        private void FileNameText_TextChanged(object sender, EventArgs e)
        {
            EnableOK();
        }


        /// <summary>
        /// Find the default directory for the output files
        /// </summary>
        private string DefaultBankDirectory(Int32 Bank)
        {
            string Result = string.Empty;

            // Find the bank from the banks table
            DebtPlus.LINQ.bank b = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Id == Bank);
            Result = (b != null) ? b.output_directory : string.Empty;

            // If the string is empty then use the documents work directory
            if (string.IsNullOrEmpty(Result))
            {
                Result = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            }

            // Remove the trailing slash or colon if there is one.
            // It is not acceptable to have a directory separator for
            // the end as we will add it when we merge the strings
            if (! string.IsNullOrEmpty(Result))
            {
                switch (Result[Result.Length - 1])
                {
                    case '/':
                    case '\\':
                        Result = Result.Remove(Result.Length, 1);
                        break;
                    default:
                        break;
                }
            }

            // The resulting directory is returned
            return Result;
        }

        private void bt_DoWork(object sender, DoWorkEventArgs e)
        {
            bool Answer = PPDACHCreateClass.CreateACHFile(MySchedules, MyEffectiveDate, MyBank, MyIncludePrenotes, MyFilename, TextEdit_Note.Text.Trim());
            e.Result = Answer;
        }

        private void bt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (wt != null)
            {
                wt.Close();
                wt.Dispose();
                wt = null;
            }

            bool Answer = (bool)e.Result;
            OKButton.Enabled = true;
            if (Answer)
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format(String.Format("The file generation for batch #{{2:f0}} is complete.{0}{0}There are {{0:n0}} clients totaling {{1:c}}", Environment.NewLine), PPDACHCreateClass.ClientCount, PPDACHCreateClass.TotalDebit, PPDACHCreateClass.DepositBatchID), "File Generation Completed");
                Close();
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            CloseBox = true;
            CloseBox = false;
        }
    }
}
