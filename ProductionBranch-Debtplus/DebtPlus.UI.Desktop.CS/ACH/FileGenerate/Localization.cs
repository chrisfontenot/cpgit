﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.ACH.FileGenerate
{
    internal static class Localization
    {
        public static readonly string ErrorTryingToFindStartingDate = "Error trying to find starting date";
        public static readonly string ACH0YyMMddHHmmTxt = "ACH{0:yyMMddHHmm}.txt";
        public static readonly string ACHOutputFile = "ACH Output File Location";
        public static readonly string TextFilesTxtTxtAllFiles = "Text files (.txt)|*.txt|All Files|*.*";
        public static readonly string AnotherUserWasGeneratingAnACHFileAtTheSameTimeTheCurrentFileIsVoidedPleaseReExecuteTheExtractAgain = "Another user was generating an ACHFile at the same time. The current file is voided. Please re-execute the extract again.";
        public static readonly string ButtonTypeIsNotDefinedForFileNameTextField = "Button Type Is Not Defined For File Name Text Field";
        public static readonly string MustBeMONDAYThroughFRIDAY = "Must Be MONDAY Through FRIDAY";
        public static readonly string CanNotBeOn0 = "Can Not Be On {0}";
        public static readonly string MustNotBeOnABankingHoliday = "Must Not Be On A Banking Holiday";
        public static readonly string DateNotDefinedInSystem = "Date Not Defined In System";
        public static readonly string ErrorCreatingACHFile = "Error Creating ACHFile";
    }
}
