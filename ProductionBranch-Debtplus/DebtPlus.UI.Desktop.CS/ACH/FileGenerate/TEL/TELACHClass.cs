﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.ACH.FileGenerate.TEL
{
    internal class TELACHClass : DebtPlus.UI.Desktop.CS.ACH.FileGenerate.ACHClass
    {
        /// <summary>
        /// Do the requested operation to generate the output text file
        /// </summary>
        public bool CreateACHFile(Int32 BankID, string Filename, string Note, DateTime minimumEffectiveDate)
        {
            try
            {
                // Offsets to the trace numbers
                decimal fileDebit     = 0M;
                decimal fileCredit    = 0M;
                int fileLines         = 0;
                int transactionOffset = 0;
                int physicalWrites    = 0;

                System.Text.StringBuilder sbOutputText = new System.Text.StringBuilder();
                using (var OutputWriter = new System.IO.StringWriter(sbOutputText))
                {
                    // Create a transaction for the operation. We want everything to just go away on an error
                    using (var bc = new BusinessContext())
                    {
                        DebtPlus.LINQ.bank currentBank = (from b in bc.banks where b.Id == BankID select b).FirstOrDefault();
                        if (currentBank == null)
                        {
                            throw new ApplicationException("Unable to find the bank in the banks table.");
                        }

                        // If there is a file header then write it
                        if (!string.IsNullOrWhiteSpace(currentBank.prefix_line))
                        {
                            OutputWriter.WriteLine(currentBank.prefix_line);
                        }

                        // Generate the file header and write it to the output file
                        OutputWriter.WriteLine(FileHeader(currentBank));
                        physicalWrites++;

                        string NewFilename = Filename;
                        if (NewFilename.Length > 4000)
                        {
                            NewFilename = NewFilename.Substring(NewFilename.Length - 4000);
                        }

                        // The length of the note is only 50 characters.
                        string NewNote = Note;
                        if (NewNote.Length > 50)
                        {
                            NewNote = NewNote.Substring(0, 50);
                        }

                        // Allocate a new deposit batch
                        var ACHDepositBatch = new deposit_batch_id()
                        {
                            bank          = currentBank.Id,
                            note          = NewNote,
                            ach_file      = NewFilename,
                            batch_type    = "AC",
                            ach_pull_date = DateTime.Now.Date,
                            date_closed   = DateTime.Now
                        };

                        // Add the deposit batch to the system when we complete the transaction
                        bc.deposit_batch_ids.InsertOnSubmit(ACHDepositBatch);

                        // Compensate the minimum effective date by one day so that we can ignore any possible time values in the dates
                        DateTime compensatedMinimumEffectiveDate = minimumEffectiveDate.Date.AddDays(1);

                        // Retrieve the details from the deposit table
                        var achDetails = (from d in bc.ach_onetimes where d.bank == currentBank.Id && d.EffectiveDate < compensatedMinimumEffectiveDate && d.deposit_batch_date == null orderby d.EffectiveDate select d);
                        IEnumerator<ach_onetime> ieDetails = achDetails.AsEnumerable<ach_onetime>().GetEnumerator();
                        ieDetails.MoveNext();

                        // Process all of the items in the pending file at this point.
                        ach_onetime batchDetailRecord = ieDetails.Current;
                        string batch_type             = DebtPlus.LINQ.Factory.DEFAULT_ACH_BATCH_TYPE;

                        while (batchDetailRecord != null)
                        {
                            // Compute the current trace number for the batch
                            batchOffset += 1;
                            string batchTraceNumber = TraceNumber(currentBank, batchOffset + currentBank.batch_number);
                            batch_type = batchDetailRecord.batch_type;

                            // Write the batch header to the output file.
                            OutputWriter.WriteLine(BatchHeader(currentBank, batch_type, batchTraceNumber, batchDetailRecord.EffectiveDate));
                            physicalWrites++;

                            decimal batchDebit  = 0M;
                            decimal batchCredit = 0M;
                            int batchLines      = 0;
                            long batchHash      = 0L;

                            ach_onetime transactionDetailRecord = batchDetailRecord;
                            while (transactionDetailRecord != null)
                            {
                                // Stop the batch when the effective date changes.
                                if (transactionDetailRecord.EffectiveDate != batchDetailRecord.EffectiveDate || batch_type != batchDetailRecord.batch_type)
                                {
                                    break;
                                }

                                // Generate a new trace number for the transaction
                                transactionOffset++;
                                string transactionTraceNumber = TraceNumber(currentBank, transactionOffset + currentBank.transaction_number);

                                // Allocate a new deposit record for this transaction
                                var depositDetail = new deposit_batch_detail()
                                {
                                    ach_account_number      = ((IACH_Generate_File)transactionDetailRecord).account_number,
                                    ach_routing_number      = ((IACH_Generate_File)transactionDetailRecord).routing_number,
                                    ach_transaction_code    = ((IACH_Generate_File)transactionDetailRecord).transaction_code,
                                    amount                  = ((IACH_Generate_File)transactionDetailRecord).amount,
                                    client                  = ((IACH_Generate_File)transactionDetailRecord).client,
                                    client_product          = ((IACH_Generate_File)transactionDetailRecord).client_product,
                                    item_date               = transactionDetailRecord.EffectiveDate,
                                    reference               = transactionTraceNumber,
                                    ach_authentication_code = null,
                                    ach_response_batch_id   = null,
                                    ok_to_post              = true,
                                    tran_subtype            = "AC"
                                };

                                ACHDepositBatch.deposit_batch_details.Add(depositDetail);

                                // Write the text record out to the file
                                OutputWriter.WriteLine(TransactionLine(transactionDetailRecord, transactionTraceNumber, batch_type));
                                physicalWrites++;

                                // Update the statistics for the trailers
                                batchLines++;
                                batchDebit += depositDetail.amount;
                                batchHash  += Int64.Parse(transactionDetailRecord.ABA.Substring(0, 8));

                                // Go on to the next record
                                ieDetails.MoveNext();
                                transactionDetailRecord = ieDetails.Current;
                            }

                            // Update the file statistics
                            fileDebit  += batchDebit;
                            fileCredit += batchCredit;
                            fileLines  += batchLines;
                            fileHash   += batchHash;

                            // Write the batch trailer
                            OutputWriter.WriteLine(BatchTrailer(currentBank, batchTraceNumber, batchLines, batchHash, batchDebit, batchCredit));
                            physicalWrites++;

                            // The batch detail record is where the transactions left off
                            batchDetailRecord = transactionDetailRecord;
                        }

                        // If we need an offsetting deposit record then generate it here
                        if (currentBank.ach_enable_offset && fileDebit > 0M)
                        {
                            // Compute the current trace number for the batch
                            batchOffset += 1;
                            string batchTraceNumber = TraceNumber(currentBank, batchOffset + currentBank.batch_number);

                            // Write the batch header to the output file.
                            OutputWriter.WriteLine(BatchHeader(currentBank, batch_type, batchTraceNumber, DateTime.Now.AddDays(1).Date));
                            physicalWrites++;

                            // Generate the deposit record. We don't really record this item because it is not associated with any client. It just exists in the file.
                            var offsettingItem = new ach_onetime()
                            {
                                client          = 0,
                                ABA             = currentBank.aba,
                                AccountNumber   = currentBank.account_number,
                                CheckingSavings = 'C',
                                Amount          = fileDebit,
                                EffectiveDate   = DateTime.Now.Date.AddDays(1)
                            };
                            ((IACH_Generate_File)offsettingItem).transaction_code = "32";

                            transactionOffset++;
                            string transactionTraceNumber = TraceNumber(currentBank, transactionOffset + currentBank.transaction_number);
                            OutputWriter.WriteLine(TransactionLine(offsettingItem, transactionTraceNumber, batch_type));
                            physicalWrites++;

                            // Write the batch trailer
                            long batchHash = Int64.Parse(offsettingItem.ABA.Substring(0, 8));
                            OutputWriter.WriteLine(BatchTrailer(currentBank, batchTraceNumber, 1, batchHash, 0M, offsettingItem.Amount));
                            physicalWrites++;

                            // Correct the statistics
                            fileCredit += offsettingItem.Amount;
                            fileHash   += batchHash;
                            fileLines++;
                        }

                        // Write the file trailer
                        OutputWriter.WriteLine(FileTrailerLine(physicalWrites, batchOffset, fileLines, fileHash, fileDebit, fileCredit));
                        physicalWrites++;

                        // Pad the output file to a multiple of 10 lines. This is the block size specified in the header.
                        while ((physicalWrites % 10) != 0)
                        {
                            OutputWriter.WriteLine(new string('9', 94));
                            physicalWrites++;
                        }

                        // Write the bank trailer
                        if (!string.IsNullOrWhiteSpace(currentBank.suffix_line))
                        {
                            OutputWriter.WriteLine(currentBank.suffix_line);
                        }

                        // Allocate a transaction that basically locks the tables. We need exclusive access to these tables for a moment.
                        var opt = new System.Transactions.TransactionOptions();
                        opt.IsolationLevel = System.Transactions.IsolationLevel.RepeatableRead;

                        using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, opt))
                        {
                            // Retrieve the current bank as being selectable once the file has been created.
                            var q = (from b in bc.banks where b.Id == currentBank.Id select b).FirstOrDefault();
                            if (q == null)
                            {
                                throw new ApplicationException("Unable to find the bank in the banks table.");
                            }

                            // The transaction and batch numbers must not have changed since we started. If they did then we need to restart the whole
                            // file generation sequence again since the trace numbers would be duplicated.
                            if (q.transaction_number != currentBank.transaction_number || q.batch_number != currentBank.batch_number)
                            {
                                DebtPlus.Data.Forms.MessageBox.Show("Another user was creating an ACH file at this time.\r\n\r\nThe file generation has been aborted because duplicate trace numbers were generated.\r\n\r\nYou must re-run the extract again to generate a new file with the proper trace numbers.", Localization.ErrorCreatingACHFile, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Hand, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                                return false;
                            }

                            // Reduce the trace number to be less than 7 digits. 7 digits are used, but we wrap at the 7th so that the
                            // trace numbers will always go up in the file and never restart at 1 during the file generation.
                            q.transaction_number = tr(q.transaction_number + transactionOffset);
                            if (q.transaction_number > 10000000)
                            {
                                q.transaction_number -= 10000000;
                            }

                            // Do not allow a trace number of 0 to exist. This is a big no-no.
                            if (q.transaction_number == 0)
                            {
                                q.transaction_number = 1;
                            }

                            // Do the same thing to the batch trace numbers
                            q.batch_number = tr(q.batch_number + batchOffset);
                            if (q.batch_number >= 10000000)
                            {
                                q.batch_number -= 10000000;
                            }

                            if (q.batch_number == 0)
                            {
                                q.batch_number = 1;
                            }

                            // Now, do all pending database writes.
                            bc.SubmitChanges();

                            // Once the deposit batch has been created, update the ID
                            DepositFile = ACHDepositBatch.Id;

                            // Then, update the deposit details with the current deposit file. This
                            // will take them from a "un-posted" to a "pending" status showing that the
                            // request has been submitted to the bank. They will no longer show up in this
                            // program.
                            foreach (var item in achDetails)
                            {
                                item.deposit_batch_id = ACHDepositBatch.Id;
                                item.deposit_batch_date = ACHDepositBatch.date_created;
                            }
                            bc.SubmitChanges();

                            // Commit the transactions to the database
                            txn.Complete();
                        }
                    }
                }

                // Now, write the local buffer to the output file when everything is valid.
                using (var outputStream = new System.IO.FileStream(Filename, System.IO.FileMode.Create, System.IO.FileAccess.Write))
                {
                    using (var outputWriter = new System.IO.StreamWriter(outputStream))
                    {
                        outputWriter.Write(sbOutputText.ToString());
                        outputWriter.Flush();
                    }
                    outputStream.Close();
                }

                // Finally, return success to the caller.
                return true;
            }

            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), Localization.ErrorCreatingACHFile, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Hand, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                return false;
            }
        }

        private string EmptyIfNull(string input)
        {
            return input ?? string.Empty;
        }

        /// <summary>
        /// Write the ACH file header line
        /// </summary>
        protected string FileHeader(bank bankRecord)
        {
            StringBuilder sb = new StringBuilder(100);

            sb.Append("1");
            sb.Append(EmptyIfNull(bankRecord.ach_priority).PadLeft(2, '0'));
            sb.Append(EmptyIfNull(bankRecord.immediate_destination).PadLeft(10, ' '));
            sb.Append(EmptyIfNull(bankRecord.immediate_origin).PadRight(10, '0'));
            sb.AppendFormat("{0:yyMMddHHmm}", CurrentDateTime);
            sb.Append("1");
            sb.Append("094");
            sb.Append("10");
            sb.Append("1");

            sb.Append(EmptyIfNull(bankRecord.immediate_destination_name).PadRight(23).Substring(0, 23));
            sb.Append(EmptyIfNull(bankRecord.immediate_origin_name).PadRight(23).Substring(0, 23));
            sb.Append("00000000");

            // Return the resulting string for the header record
            return sb.ToString();
        }

        /// <summary>
        /// Write the ACH file trailer line
        /// </summary>
        protected string FileTrailerLine(int LinesWritten, int batchCount, int transactionCount, long hash, decimal debitAmt, decimal creditAmt)
        {
            StringBuilder sb = new StringBuilder(100);
            sb.Append("9");

            // Find the number of blocks written.
            Int32 quotient;
            Int32 remainder;
            quotient = System.Math.DivRem(LinesWritten, 10, out remainder);

            sb.AppendFormat("{0:000000}", batchCount);
            sb.AppendFormat("{0:000000}", quotient + 1);
            sb.AppendFormat("{0:00000000}", transactionCount);
            sb.AppendFormat("{0:0000000000}", hash % 10000000000L);
            sb.AppendFormat("{0:000000000000}", cents(debitAmt));
            sb.AppendFormat("{0:000000000000}", cents(creditAmt));
            sb.Append(' ', 39);

            Debug.Assert(sb.Length == 94);

            return sb.ToString();
        }

        /// <summary>
        /// Write a batch header
        /// </summary>
        protected string BatchHeader(bank bankRecord, string batch_type, string traceNumber, DateTime effectiveDate)
        {
            StringBuilder sb = new StringBuilder(100);

            sb.Append("5");
            sb.Append("200");
            sb.Append(EmptyIfNull(bankRecord.immediate_origin_name).PadRight(36).Substring(0, 36));
            sb.Append(EmptyIfNull(bankRecord.ach_company_identification).PadLeft(10, '0'));
            sb.Append(batch_type.PadRight(3, ' '));
            sb.Append(EmptyIfNull(bankRecord.ach_batch_company_id).PadRight(10));
            sb.AppendFormat("{0:yyMMdd}", effectiveDate);
            sb.AppendFormat("{0:yyMMdd}", effectiveDate);
            sb.Append(new string(' ', 3));
            sb.Append("1");
            sb.Append(traceNumber);

            Debug.Assert(sb.Length == 94);
            return sb.ToString();
        }

        /// <summary>
        /// Write a batch trailer
        /// </summary>
        protected string BatchTrailer(bank bankRecord, string traceNumber, int transactionCount, long hash, decimal debitAmt, decimal creditAmt)
        {
            StringBuilder sb = new StringBuilder(100);

            sb.Append("8");
            sb.Append("200");
            sb.AppendFormat("{0:000000}", transactionCount);
            sb.AppendFormat("{0:0000000000}", hash % 10000000000L);
            sb.AppendFormat("{0:000000000000}", cents(debitAmt));
            sb.AppendFormat("{0:000000000000}", cents(creditAmt));
            sb.Append(EmptyIfNull(bankRecord.ach_company_identification).PadRight(10));
            sb.Append(EmptyIfNull(bankRecord.ach_message_authentication).PadRight(19));
            sb.Append(new string(' ', 6));
            sb.Append(traceNumber);

            Debug.Assert(sb.Length == 94);

            return sb.ToString();
        }

        /// <summary>
        /// Write a transaction
        /// </summary>
        protected string TransactionLine(DebtPlus.LINQ.IACH_Generate_File currentRecord, string traceNumber, string batchType)
        {
            StringBuilder sb = new StringBuilder(100);

            sb.Append("6");
            sb.Append(currentRecord.transaction_code.PadRight(2));
            sb.Append(EmptyIfNull(currentRecord.routing_number).PadRight(9));
            sb.Append(clean_string(EmptyIfNull(currentRecord.account_number)).PadRight(17).Substring(0, 17));
            sb.AppendFormat("{0:0000000000}", cents(currentRecord.amount));
            sb.Append(string.Format("{0:0000000}", currentRecord.client).PadRight(15));
            sb.AppendFormat(clean_string(EmptyIfNull(currentRecord.client_name)).PadRight(22).Substring(0, 22));

            // Required for WEB formats
            sb.Append((new string[] { "WEB" }).Contains(batchType) ? 'S' : ' ');

            // Complete the record
            sb.Append(' ');
            sb.Append('0');
            sb.Append(traceNumber);

            // Return the resulting record
            Debug.Assert(sb.Length == 94);
            return sb.ToString();
        }

        private Int64 cents(decimal v)
        {
            Int64 a = Convert.ToInt64(System.Decimal.Floor(v * 100M));
            return a;
        }
    }
}
