#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.ACH.FileGenerate
{
    internal class ACHClass : IDisposable
    {
        protected DateTime EffectiveDate;

        // The deposit batch number for the system
        protected Int32 DepositFile = -1;

        // Number of lines written to the output file
        protected Int32 LinesWritten;

        // Current date and time from the system
        protected DateTime CurrentDateTime = DateTime.Now;

        // Information for the total file
        protected Int32 batchOffset;
        protected Int32 FileTransactions;
        protected Int64 FileDebit = 0;
        protected Int64 FileCredit = 0;

        // Information for the current batch
        protected string BatchType = "PPD";
        protected Int32 BatchTransactions;
        protected Int64 BatchDebit = 0;

        protected Int64 BatchCredit = 0;
        private Int64 privateFileHash = 0;
        private Int64 privateBatchHash = 0;

        // Information about the current ACH bank
        protected DebtPlus.LINQ.bank ACH_bank = null;
        protected string company_entry_description = "CCCS PAYMT";

        /// <summary>
        /// Dispose of local storage
        /// </summary>
        public virtual void Dispose()
        {
            // Set other pointers to null
            erf = null;
            drf = null;
            ae = null;

            // Suppress the finalize event now.
            GC.SuppressFinalize(this);
        }

        // Storage
        protected EncoderReplacementFallback erf;
        protected DecoderReplacementFallback drf;
        protected Encoding ae;

        /// <summary>
        /// Create a new instance of this class
        /// </summary>
        public ACHClass()
        {
            // Create the encoding classes
            erf = new EncoderReplacementFallback(" ");
            drf = new DecoderReplacementFallback(" ");
            ae = Encoding.GetEncoding("us-ascii", erf, drf);
        }

        /// <summary>
        /// File Hash total of the routing numbers
        /// </summary>
        protected Int64 fileHash
        {
            get { return privateFileHash; }
            set
            {
                Debug.Assert(value >= 0);
                if (value >= 10000000000L)
                {
                    value -= 10000000000L;
                }
                privateFileHash = value;
            }
        }

        /// <summary>
        /// Batch Hash total of the routing numbers
        /// </summary>
        protected Int64 BatchHash
        {
            get { return privateBatchHash; }
            set
            {
                Debug.Assert(value >= 0);
                if (value >= 10000000000L)
                {
                    value -= 10000000000L;
                }
                privateBatchHash = value;
            }
        }

        /// <summary>
        /// Deposit batch number used for posting operations
        /// </summary>
        public Int32 DepositBatchID
        {
            get
            {
                return DepositFile;
            }
        }

        /// <summary>
        /// Return the number of type 6 records written to the file
        /// </summary>
        public Int32 ClientCount
        {
            get
            {
                return FileTransactions;
            }
        }

        /// <summary>
        /// Return the total amount of money pulled in the file
        /// </summary>
        public decimal TotalDebit
        {
            get
            {
                return Convert.ToDecimal(FileDebit) / 100m;
            }
        }


        /// <summary>
        /// Return a valid trace number for the next item
        /// </summary>
        protected static Int32 tr(Int32 InputValue)
        {
            return (InputValue % 10000000);
        }


        /// <summary>
        /// Convert the relative trace number to a proper string
        /// </summary>
        protected string TraceNumber(DebtPlus.LINQ.bank bankRecord, Int32 TraceNumberDesired)
        {
            // We need exactly 8 characters from the identification string. No more. No less.
            string ident = bankRecord.ach_origin_dfi;
            if (ident.Length > 8)
            {
                ident = ident.Substring(ident.Length - 8);
            }

            return string.Format("{0:s8}{1:0000000}", ident, tr(TraceNumberDesired));
        }

        /// <summary>
        /// Ensure message does not have funny characters
        /// </summary>
        protected string clean_string(object message)
        {
            // Extract the string from the KeyField buffer
            string StrItem;
            if (message != null && !object.ReferenceEquals(message, System.DBNull.Value))
                StrItem = Convert.ToString(message, System.Globalization.CultureInfo.InvariantCulture).Trim();
            else
                StrItem = string.Empty;

            // Find the length of the KeyField stream. It should be the number of bytes for the KeyField.
            Int32 Length = ae.GetByteCount(StrItem);
            if (Length < 1)
            {
                return string.Empty;
            }

            // Convert the string to a list of bytes. This removes the non-ASCII characters.
            byte[] ab = new byte[Length];
            ae.GetBytes(StrItem, 0, StrItem.Length, ab, 0);

            // Convert the bytes back to a string as ASCII characters
            string Pass1 = ae.GetString(ab);

            // Now, remove any objectionable ASCII characters from the string. This is now a simple string, not unicode.
            string Result = System.Text.RegularExpressions.Regex.Replace(Pass1, "[\\x00-\\x1F\\x7F]", " ");
            return Result;
        }
    }
}

