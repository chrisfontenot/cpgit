using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {

        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }

                    bc.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_item_key = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_trace_number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_transaction_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_authenticaiton_code = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RepositoryItemLookUpEdit_authentication_code = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.Bar1 = new DevExpress.XtraBars.Bar();
            this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Open = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_File_DataEntry = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_Reports_Transactions = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_Reports_ErrorListing = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_Help_About = new DevExpress.XtraBars.BarButtonItem();
            this.Bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.GridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemLookUpEdit_authentication_code).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            this.SuspendLayout();
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.GridControl1);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 29);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(573, 86, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(605, 237);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //GridControl1
            //
            this.GridControl1.Location = new System.Drawing.Point(12, 12);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.MenuManager = this.barManager1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { this.RepositoryItemLookUpEdit_authentication_code });
            this.GridControl1.Size = new System.Drawing.Size(581, 213);
            this.GridControl1.TabIndex = 4;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
				this.GridView1,
				this.GridView2
			});
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButton.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(132), (Int32)Convert.ToByte(171), (Int32)Convert.ToByte(228));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(154), (Int32)Convert.ToByte(190), (Int32)Convert.ToByte(243));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(231), (Int32)Convert.ToByte(242), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.FilterCloseButton.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(132), (Int32)Convert.ToByte(171), (Int32)Convert.ToByte(228));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterCloseButton.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(62), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(185));
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(97), (Int32)Convert.ToByte(156));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(49), (Int32)Convert.ToByte(106), (Int32)Convert.ToByte(197));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.FooterPanel.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(132), (Int32)Convert.ToByte(171), (Int32)Convert.ToByte(228));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(193), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(247));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(193), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(247));
            this.GridView1.Appearance.GroupButton.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(193), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(247));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(193), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(247));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(62), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(185));
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(193), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(247));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(193), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(247));
            this.GridView1.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8f, System.Drawing.FontStyle.Bold);
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseFont = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(132), (Int32)Convert.ToByte(171), (Int32)Convert.ToByte(228));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(221), (Int32)Convert.ToByte(236), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(106), (Int32)Convert.ToByte(153), (Int32)Convert.ToByte(228));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(208), (Int32)Convert.ToByte(224), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(99), (Int32)Convert.ToByte(127), (Int32)Convert.ToByte(196));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(249), (Int32)Convert.ToByte(252), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(88), (Int32)Convert.ToByte(129), (Int32)Convert.ToByte(185));
            this.GridView1.Appearance.Preview.Options.UseBackColor = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(69), (Int32)Convert.ToByte(126), (Int32)Convert.ToByte(217));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(99), (Int32)Convert.ToByte(127), (Int32)Convert.ToByte(196));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_item_key,
				this.GridColumn_trace_number,
				this.GridColumn_client,
				this.GridColumn_amount,
				this.GridColumn_transaction_code,
				this.GridColumn_authenticaiton_code
			});
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            //
            //GridColumn_item_key
            //
            this.GridColumn_item_key.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_item_key.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_item_key.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_item_key.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_item_key.Caption = "ID";
            this.GridColumn_item_key.CustomizationCaption = "Record ID";
            this.GridColumn_item_key.DisplayFormat.FormatString = "f0";
            this.GridColumn_item_key.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_item_key.FieldName = "Id";
            this.GridColumn_item_key.GroupFormat.FormatString = "f0";
            this.GridColumn_item_key.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_item_key.Name = "GridColumn_item_key";
            this.GridColumn_item_key.OptionsColumn.AllowEdit = false;
            //
            //GridColumn_trace_number
            //
            this.GridColumn_trace_number.Caption = "Trace Number";
            this.GridColumn_trace_number.CustomizationCaption = "Trace Number";
            this.GridColumn_trace_number.FieldName = "reference";
            this.GridColumn_trace_number.Name = "GridColumn_trace_number";
            this.GridColumn_trace_number.OptionsColumn.AllowEdit = false;
            this.GridColumn_trace_number.Visible = true;
            this.GridColumn_trace_number.VisibleIndex = 0;
            this.GridColumn_trace_number.Width = 126;
            //
            //GridColumn_client
            //
            this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.Caption = "Client";
            this.GridColumn_client.CustomizationCaption = "Client";
            this.GridColumn_client.DisplayFormat.FormatString = "f0";
            this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.FieldName = "client";
            this.GridColumn_client.GroupFormat.FormatString = "f0";
            this.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.Name = "GridColumn_client";
            this.GridColumn_client.OptionsColumn.AllowEdit = false;
            this.GridColumn_client.Visible = true;
            this.GridColumn_client.VisibleIndex = 1;
            this.GridColumn_client.Width = 76;
            //
            //GridColumn_amount
            //
            this.GridColumn_amount.Caption = "Amount";
            this.GridColumn_amount.CustomizationCaption = "Amount";
            this.GridColumn_amount.DisplayFormat.FormatString = "{0:c}";
            this.GridColumn_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_amount.FieldName = "amount";
            this.GridColumn_amount.GroupFormat.FormatString = "{0:c}";
            this.GridColumn_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_amount.Name = "GridColumn_amount";
            this.GridColumn_amount.OptionsColumn.AllowEdit = false;
            this.GridColumn_amount.Visible = true;
            this.GridColumn_amount.VisibleIndex = 2;
            this.GridColumn_amount.Width = 79;
            //
            //GridColumn_transaction_code
            //
            this.GridColumn_transaction_code.Caption = "Transaction";
            this.GridColumn_transaction_code.CustomizationCaption = "Transaction Code";
            this.GridColumn_transaction_code.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_transaction_code.FieldName = "ach_transaction_code";
            this.GridColumn_transaction_code.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_transaction_code.Name = "GridColumn_transaction_code";
            this.GridColumn_transaction_code.OptionsColumn.AllowEdit = false;
            this.GridColumn_transaction_code.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.GridColumn_transaction_code.Visible = true;
            this.GridColumn_transaction_code.VisibleIndex = 3;
            this.GridColumn_transaction_code.Width = 108;
            //
            //GridColumn_authenticaiton_code
            //
            this.GridColumn_authenticaiton_code.Caption = "Authenication";
            this.GridColumn_authenticaiton_code.ColumnEdit = this.RepositoryItemLookUpEdit_authentication_code;
            this.GridColumn_authenticaiton_code.CustomizationCaption = "Authentication Code";
            this.GridColumn_authenticaiton_code.FieldName = "ach_authentication_code";
            this.GridColumn_authenticaiton_code.Name = "GridColumn_authenticaiton_code";
            this.GridColumn_authenticaiton_code.Visible = true;
            this.GridColumn_authenticaiton_code.VisibleIndex = 4;
            this.GridColumn_authenticaiton_code.Width = 171;
            //
            //RepositoryItemLookUpEdit_authentication_code
            //
            this.RepositoryItemLookUpEdit_authentication_code.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.RepositoryItemLookUpEdit_authentication_code.AutoHeight = false;
            this.RepositoryItemLookUpEdit_authentication_code.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.RepositoryItemLookUpEdit_authentication_code.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 30, "Description")
			});
            this.RepositoryItemLookUpEdit_authentication_code.DisplayMember = "description";
            this.RepositoryItemLookUpEdit_authentication_code.Name = "RepositoryItemLookUpEdit_authentication_code";
            this.RepositoryItemLookUpEdit_authentication_code.NullText = "";
            this.RepositoryItemLookUpEdit_authentication_code.ShowFooter = false;
            this.RepositoryItemLookUpEdit_authentication_code.ValueMember = "Id";
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
				this.Bar1,
				this.Bar3
			});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarSubItem1,
				this.BarSubItem2,
				this.BarSubItem3,
				this.BarButtonItem_File_Open,
				this.BarButtonItem_File_DataEntry,
				this.BarButtonItem_Reports_Transactions,
				this.BarButtonItem_Reports_ErrorListing,
				this.BarButtonItem_Help_About,
				this.BarButtonItem_File_Exit
			});
            this.barManager1.MaxItemId = 9;
            this.barManager1.StatusBar = this.Bar3;
            //
            //Bar1
            //
            this.Bar1.BarName = "Tools";
            this.Bar1.DockCol = 0;
            this.Bar1.DockRow = 0;
            this.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem2),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem3)
			});
            this.Bar1.Text = "Tools";
            //
            //BarSubItem1
            //
            this.BarSubItem1.Caption = "&File";
            this.BarSubItem1.Id = 0;
            this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Open),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_DataEntry),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit, true)
			});
            this.BarSubItem1.Name = "BarSubItem1";
            //
            //BarButtonItem_File_Open
            //
            this.BarButtonItem_File_Open.Caption = "&Open";
            this.BarButtonItem_File_Open.Id = 3;
            this.BarButtonItem_File_Open.Name = "BarButtonItem_File_Open";
            //
            //BarButtonItem_File_DataEntry
            //
            this.BarButtonItem_File_DataEntry.Caption = "&Data Entry ...";
            this.BarButtonItem_File_DataEntry.Id = 4;
            this.BarButtonItem_File_DataEntry.Name = "BarButtonItem_File_DataEntry";
            //
            //BarButtonItem_File_Exit
            //
            this.BarButtonItem_File_Exit.Caption = "&Exit";
            this.BarButtonItem_File_Exit.Id = 8;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            //
            //BarSubItem2
            //
            this.BarSubItem2.Caption = "&Reports";
            this.BarSubItem2.Id = 1;
            this.BarSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Reports_Transactions),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Reports_ErrorListing)
			});
            this.BarSubItem2.Name = "BarSubItem2";
            //
            //BarButtonItem_Reports_Transactions
            //
            this.BarButtonItem_Reports_Transactions.Caption = "&Transactions...";
            this.BarButtonItem_Reports_Transactions.Id = 5;
            this.BarButtonItem_Reports_Transactions.Name = "BarButtonItem_Reports_Transactions";
            //
            //BarButtonItem_Reports_ErrorListing
            //
            this.BarButtonItem_Reports_ErrorListing.Caption = "&Error Listing...";
            this.BarButtonItem_Reports_ErrorListing.Id = 6;
            this.BarButtonItem_Reports_ErrorListing.Name = "BarButtonItem_Reports_ErrorListing";
            //
            //BarSubItem3
            //
            this.BarSubItem3.Caption = "&Help";
            this.BarSubItem3.Id = 2;
            this.BarSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Help_About) });
            this.BarSubItem3.Name = "BarSubItem3";
            //
            //BarButtonItem_Help_About
            //
            this.BarButtonItem_Help_About.Caption = "&About...";
            this.BarButtonItem_Help_About.Id = 7;
            this.BarButtonItem_Help_About.Name = "BarButtonItem_Help_About";
            //
            //Bar3
            //
            this.Bar3.BarName = "Status bar";
            this.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.Bar3.DockCol = 0;
            this.Bar3.DockRow = 0;
            this.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.Bar3.OptionsBar.AllowQuickCustomization = false;
            this.Bar3.OptionsBar.DrawDragBorder = false;
            this.Bar3.OptionsBar.UseWholeRow = true;
            this.Bar3.Text = "Status bar";
            //
            //barDockControlTop
            //
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(605, 29);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 266);
            this.barDockControlBottom.Size = new System.Drawing.Size(605, 25);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 29);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 237);
            //
            //barDockControlRight
            //
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(605, 29);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 237);
            //
            //GridView2
            //
            this.GridView2.GridControl = this.GridControl1;
            this.GridView2.Name = "GridView2";
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] { this.LayoutControlItem1 });
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(605, 237);
            this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.GridControl1;
            this.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(585, 217);
            this.LayoutControlItem1.Text = "LayoutControlItem1";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            //
            //MainForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 291);
            this.Controls.Add(this.LayoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "MainForm";
            this.Text = "Edit ACH Response Information";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemLookUpEdit_authentication_code).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            this.ResumeLayout(false);

        }
        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar Bar1;
        internal DevExpress.XtraBars.BarSubItem BarSubItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Open;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_DataEntry;
        internal DevExpress.XtraBars.BarSubItem BarSubItem2;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Reports_Transactions;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Reports_ErrorListing;
        internal DevExpress.XtraBars.BarSubItem BarSubItem3;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Help_About;
        internal DevExpress.XtraBars.Bar Bar3;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView2;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_item_key;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_trace_number;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_amount;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_transaction_code;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_authenticaiton_code;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit RepositoryItemLookUpEdit_authentication_code;
    }
}
