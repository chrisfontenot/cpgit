#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraBars;
using System.Data.SqlClient;
using DebtPlus.Reports;
using DebtPlus.Data.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    internal partial class MainForm
    {
        // Database information
        DebtPlus.LINQ.BusinessContext bc = new DebtPlus.LINQ.BusinessContext();
        protected System.Linq.IQueryable<DebtPlus.LINQ.deposit_batch_detail> colRecords;
        private Int32 BatchID;

        /// <summary>
        /// Class to format the type identifier to a suitable string
        /// </summary>
        private class TransactionCode_Formatter : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(Type formatType)
            {
                return this;
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                Int32 Value = -1;
                string answer = string.Empty;
                if (arg != null && !object.ReferenceEquals(arg, DBNull.Value))
                {
                    Value = Convert.ToInt32(arg);
                    switch (Value)
                    {
                        case 22:
                            answer = "CHECKING REFUND";
                            break;
                        case 32:
                            answer = "CHECKING REFUND";
                            break;
                        case 23:
                            answer = "SAVINGS REFUND";
                            break;
                        case 33:
                            answer = "SAVINGS REFUND";
                            break;
                        case 27:
                            answer = "CHECKING";
                            break;
                        case 37:
                            answer = "SAVINGS";
                            break;
                        case 28:
                            answer = "PRENOTE CHECKING";
                            break;
                        case 38:
                            answer = "PRENOTE SAVINGS";
                            break;
                        default:
                            break;
                    }
                }
                return answer;
            }
        }

        /// <summary>
        /// Process the initialization of the form information
        /// </summary>
        public MainForm(Int32 BatchID)
            : this()
        {
            this.BatchID = BatchID;
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            GridView1.DoubleClick += new EventHandler(GridView1_DoubleClick);
            BarButtonItem_File_DataEntry.ItemClick += new ItemClickEventHandler(BarButtonItem_File_DataEntry_ItemClick);
            BarButtonItem_File_Exit.ItemClick += new ItemClickEventHandler(BarButtonItem_File_Exit_ItemClick);
            BarButtonItem_Help_About.ItemClick += new ItemClickEventHandler(BarButtonItem_Help_About_ItemClick);
            BarButtonItem_Reports_Transactions.ItemClick += new ItemClickEventHandler(BarButtonItem_Reports_Transactions_ItemClick);
            FormClosing += new FormClosingEventHandler(MainForm_FormClosing);
            GridView1.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(GridView1_CellValueChanged);
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            GridView1.DoubleClick -= new EventHandler(GridView1_DoubleClick);
            BarButtonItem_File_DataEntry.ItemClick -= new ItemClickEventHandler(BarButtonItem_File_DataEntry_ItemClick);
            BarButtonItem_File_Exit.ItemClick -= new ItemClickEventHandler(BarButtonItem_File_Exit_ItemClick);
            BarButtonItem_Help_About.ItemClick -= new ItemClickEventHandler(BarButtonItem_Help_About_ItemClick);
            BarButtonItem_Reports_Transactions.ItemClick -= new ItemClickEventHandler(BarButtonItem_Reports_Transactions_ItemClick);
            FormClosing -= new FormClosingEventHandler(MainForm_FormClosing);
            GridView1.CellValueChanged -= new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(GridView1_CellValueChanged);
        }

        /// <summary>
        /// Process the initialization of the form information
        /// </summary>
        public MainForm()
            : base()
        {
            InitializeComponent();

            // Client formatter
            GridColumn_client.DisplayFormat.Format = new DebtPlus.Utils.Format.Client.CustomFormatter();
            GridColumn_client.GroupFormat.Format = new DebtPlus.Utils.Format.Client.CustomFormatter();

            // Transaction code
            GridColumn_transaction_code.DisplayFormat.Format = new TransactionCode_Formatter();
            GridColumn_transaction_code.GroupFormat.Format = new TransactionCode_Formatter();

            RegisterHandlers();
        }

        /// <summary>
        /// Process the change in the edit value for the update control
        /// </summary>
        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            bc.SubmitChanges();
        }

        /// <summary>
        /// Handle the close for the form
        /// </summary>
        private void MainForm_FormClosing(object sender, CancelEventArgs e)
        {
            bc.SubmitChanges();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the list of reject codes into the editor control
                RepositoryItemLookUpEdit_authentication_code.DataSource = DebtPlus.LINQ.Cache.ach_reject_code.getList();

                // Read the pending information about the deposit batch
                colRecords = from d in bc.deposit_batch_details where d.deposit_batch_id == BatchID && d.ach_transaction_code != "22" orderby d.reference select d;
                GridControl1.DataSource = colRecords;
                GridControl1.RefreshDataSource();
                // colRecords.First<DebtPlus.LINQ.deposit_batch_detail>()
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void BarButtonItem_File_Open_ItemClick(object sender, ItemClickEventArgs e)
        {
            Int32 NewBatch = RequestBatchID();
            if (NewBatch > 0)
            {
                bc.SubmitChanges();

                BatchID = NewBatch;
                colRecords = from d in bc.deposit_batch_details where d.deposit_batch_id == BatchID && d.ach_transaction_code != "22" orderby d.reference select d;
                GridControl1.DataSource = colRecords;
                GridControl1.RefreshDataSource();
            }
        }

        private void BarButtonItem_File_DataEntry_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var frm = new DataEntryForm())
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    string traceNumber = Convert.ToString(frm.TextEdit_trace_number.EditValue);
                    string ErrorCode = Convert.ToString(frm.LookUpEdit_authentication_code.EditValue);

                    // Find the item that is marked for this trace number
                    var q = colRecords.Where(b => b.reference == traceNumber).FirstOrDefault();
                    if (q != null)
                    {
                        q.ach_authentication_code = ErrorCode;
                        bc.SubmitChanges();
                    }
                }
            }
        }

        /// <summary>
        /// Process the FILE / EXIT operation
        /// </summary>
        private void BarButtonItem_File_Exit_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        private void BarButtonItem_Reports_Transactions_ItemClick(object sender, ItemClickEventArgs e)
        {
            // Submit the pending changes to the database
            bc.SubmitChanges();
            DebtPlus.Interfaces.Reports.IReports rpt = new DebtPlus.Reports.ACH.Transactions.AchTransactionsReport();
            PerformReport(ref rpt);
        }

        /// <summary>
        /// Process the error listing report
        /// </summary>
        private void BarButtonItem_Reports_ErrorListing_ItemClick(object sender, ItemClickEventArgs e)
        {
            DebtPlus.Interfaces.Reports.IReports rpt = new DebtPlus.Reports.ACH.Responses.AchResponsesReport();
            PerformReport(ref rpt);
        }

        /// <summary>
        /// Generate a Report
        /// </summary>
        /// <param name="rpt">Pointer to the report to be run</param>
        /// <remarks></remarks>
        private void PerformReport(ref DebtPlus.Interfaces.Reports.IReports rpt)
        {
            rpt.SetReportParameter("DepositBatchID", BatchID);
            rpt.RunReportInSeparateThread();
        }

        /// <summary>
        /// Display the about form
        /// </summary>
        private void BarButtonItem_Help_About_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (var frm = new AboutBoxForm())
            {
                frm.ShowDialog();
            }
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the trustRegister targeted as the double-click item
            GridView gv = (GridView)sender;
            GridControl ctl = (GridControl)gv.GridControl;
            GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(MousePosition)));

            Int32 ControlRow = hi.RowHandle;
            if (ControlRow >= 0)
            {
                DebtPlus.LINQ.deposit_batch_detail detail = gv.GetRow(ControlRow) as DebtPlus.LINQ.deposit_batch_detail;
                if (detail != null)
                {
                    EditItem(detail);
                }
            }
        }

        private void EditItem(DebtPlus.LINQ.deposit_batch_detail detail)
        {
            using (var frm = new EditItemForm(detail))
            {
                DialogResult answer = frm.ShowDialog();
                if (answer == System.Windows.Forms.DialogResult.OK)
                {
                    bc.SubmitChanges();
                }
            }
        }

        public static Int32 RequestBatchID()
        {
            using (var frm = new SelectBatchForm())
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    return frm.BatchID;
                }
            }
            return -1;
        }
    }
}
