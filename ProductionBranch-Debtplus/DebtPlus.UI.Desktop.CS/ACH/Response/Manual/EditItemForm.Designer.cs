using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    partial class EditItemForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.LookUpEdit_authentication_code = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl_amount = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_trace_number = new DevExpress.XtraEditors.LabelControl();
            this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl_client = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_authentication_code.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            this.SuspendLayout();
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.LookUpEdit_authentication_code);
            this.LayoutControl1.Controls.Add(this.LabelControl_amount);
            this.LayoutControl1.Controls.Add(this.LabelControl_trace_number);
            this.LayoutControl1.Controls.Add(this.SimpleButton2);
            this.LayoutControl1.Controls.Add(this.SimpleButton1);
            this.LayoutControl1.Controls.Add(this.LabelControl1);
            this.LayoutControl1.Controls.Add(this.LabelControl_client);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(388, 184);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //LookUpEdit_authentication_code
            //
            this.LookUpEdit_authentication_code.Location = new System.Drawing.Point(83, 107);
            this.LookUpEdit_authentication_code.Name = "LookUpEdit_authentication_code";
            this.LookUpEdit_authentication_code.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_authentication_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookUpEdit_authentication_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 30, "Description")
			});
            this.LookUpEdit_authentication_code.Properties.DisplayMember = "description";
            this.LookUpEdit_authentication_code.Properties.NullText = "";
            this.LookUpEdit_authentication_code.Properties.ShowFooter = false;
            this.LookUpEdit_authentication_code.Properties.ValueMember = "Id";
            this.LookUpEdit_authentication_code.Size = new System.Drawing.Size(293, 20);
            this.LookUpEdit_authentication_code.StyleController = this.LayoutControl1;
            this.LookUpEdit_authentication_code.TabIndex = 10;
            this.LookUpEdit_authentication_code.Properties.SortColumnIndex = 1;
            //
            //LabelControl_amount
            //
            this.LabelControl_amount.Location = new System.Drawing.Point(83, 90);
            this.LabelControl_amount.Name = "LabelControl_amount";
            this.LabelControl_amount.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.LabelControl_amount.Size = new System.Drawing.Size(293, 13);
            this.LabelControl_amount.StyleController = this.LayoutControl1;
            this.LabelControl_amount.TabIndex = 9;
            //
            //LabelControl_trace_number
            //
            this.LabelControl_trace_number.Location = new System.Drawing.Point(83, 56);
            this.LabelControl_trace_number.Name = "LabelControl_trace_number";
            this.LabelControl_trace_number.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.LabelControl_trace_number.Size = new System.Drawing.Size(293, 13);
            this.LabelControl_trace_number.StyleController = this.LayoutControl1;
            this.LabelControl_trace_number.TabIndex = 7;
            //
            //SimpleButton2
            //
            this.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton2.Location = new System.Drawing.Point(194, 149);
            this.SimpleButton2.MaximumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton2.MinimumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton2.Name = "SimpleButton2";
            this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton2.StyleController = this.LayoutControl1;
            this.SimpleButton2.TabIndex = 6;
            this.SimpleButton2.Text = "&Cancel";
            //
            //SimpleButton1
            //
            this.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton1.Location = new System.Drawing.Point(115, 149);
            this.SimpleButton1.MaximumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.MinimumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton1.StyleController = this.LayoutControl1;
            this.SimpleButton1.TabIndex = 5;
            this.SimpleButton1.Text = "&OK";
            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(12, 12);
            this.LabelControl1.MinimumSize = new System.Drawing.Size(0, 40);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Padding = new System.Windows.Forms.Padding(5);
            this.LabelControl1.Size = new System.Drawing.Size(364, 40);
            this.LabelControl1.StyleController = this.LayoutControl1;
            this.LabelControl1.TabIndex = 4;
            this.LabelControl1.Text = "Select the error code associated with the indicated transaction. To remove the er" + "ror, select NO ERROR from the list.";
            //
            //LabelControl_client
            //
            this.LabelControl_client.Location = new System.Drawing.Point(83, 73);
            this.LabelControl_client.Name = "LabelControl_client";
            this.LabelControl_client.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.LabelControl_client.Size = new System.Drawing.Size(293, 13);
            this.LabelControl_client.StyleController = this.LayoutControl1;
            this.LabelControl_client.TabIndex = 8;
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2,
				this.EmptySpaceItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.LayoutControlItem7
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(388, 184);
            this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.LabelControl1;
            this.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(368, 44);
            this.LayoutControlItem1.Text = "LayoutControlItem1";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.SimpleButton1;
            this.LayoutControlItem2.CustomizationFormText = "OK Button";
            this.LayoutControlItem2.Location = new System.Drawing.Point(103, 137);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem2.Text = "OK Button";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.SimpleButton2;
            this.LayoutControlItem3.CustomizationFormText = "CANCEL button";
            this.LayoutControlItem3.Location = new System.Drawing.Point(182, 137);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem3.Text = "CANCEL button";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 119);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(368, 18);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem2
            //
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(261, 137);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(107, 27);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem3
            //
            this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem3.Location = new System.Drawing.Point(0, 137);
            this.EmptySpaceItem3.Name = "EmptySpaceItem3";
            this.EmptySpaceItem3.Size = new System.Drawing.Size(103, 27);
            this.EmptySpaceItem3.Text = "EmptySpaceItem3";
            this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.LabelControl_trace_number;
            this.LayoutControlItem4.CustomizationFormText = "Trace Number";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 44);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(368, 17);
            this.LayoutControlItem4.Text = "Trace Number";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(67, 13);
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.LabelControl_client;
            this.LayoutControlItem5.CustomizationFormText = "Client ID";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 61);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(368, 17);
            this.LayoutControlItem5.Text = "Client ID";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(67, 13);
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.LabelControl_amount;
            this.LayoutControlItem6.CustomizationFormText = "Dollar Amount";
            this.LayoutControlItem6.Location = new System.Drawing.Point(0, 78);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(368, 17);
            this.LayoutControlItem6.Text = "Dollar Amount";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(67, 13);
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.LookUpEdit_authentication_code;
            this.LayoutControlItem7.CustomizationFormText = "Error Code";
            this.LayoutControlItem7.Location = new System.Drawing.Point(0, 95);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(368, 24);
            this.LayoutControlItem7.Text = "Error Code";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(67, 13);
            //
            //EditItemForm
            //
            this.AcceptButton = this.SimpleButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton2;
            this.ClientSize = new System.Drawing.Size(388, 184);
            this.Controls.Add(this.LayoutControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "EditItemForm";
            this.Text = "Edit ACH Transaction";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_authentication_code.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            this.ResumeLayout(false);
        }

        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_authentication_code;
        internal DevExpress.XtraEditors.LabelControl LabelControl_amount;
        internal DevExpress.XtraEditors.LabelControl LabelControl_trace_number;
        internal DevExpress.XtraEditors.LabelControl LabelControl_client;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
    }
}
