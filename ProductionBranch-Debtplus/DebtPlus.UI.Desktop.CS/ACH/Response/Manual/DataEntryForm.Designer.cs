
using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    partial class DataEntryForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.LookUpEdit_authentication_code = new DevExpress.XtraEditors.LookUpEdit();
            this.TextEdit_trace_number = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_authentication_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_trace_number.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.SimpleButton2);
            this.LayoutControl1.Controls.Add(this.SimpleButton1);
            this.LayoutControl1.Controls.Add(this.LookUpEdit_authentication_code);
            this.LayoutControl1.Controls.Add(this.TextEdit_trace_number);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(382, 142);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // SimpleButton2
            // 
            this.SimpleButton2.CausesValidation = false;
            this.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton2.Location = new System.Drawing.Point(195, 107);
            this.SimpleButton2.MaximumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton2.MinimumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton2.Name = "SimpleButton2";
            this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton2.StyleController = this.LayoutControl1;
            this.SimpleButton2.TabIndex = 7;
            this.SimpleButton2.Text = "&Cancel";
            // 
            // SimpleButton1
            // 
            this.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton1.Location = new System.Drawing.Point(116, 107);
            this.SimpleButton1.MaximumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.MinimumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton1.StyleController = this.LayoutControl1;
            this.SimpleButton1.TabIndex = 6;
            this.SimpleButton1.Text = "&Update";
            // 
            // LookUpEdit_authentication_code
            // 
            this.LookUpEdit_authentication_code.Location = new System.Drawing.Point(87, 36);
            this.LookUpEdit_authentication_code.Name = "LookUpEdit_authentication_code";
            this.LookUpEdit_authentication_code.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookUpEdit_authentication_code.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_authentication_code.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 30, "Description")});
            this.LookUpEdit_authentication_code.Properties.DisplayMember = "description";
            this.LookUpEdit_authentication_code.Properties.NullText = "";
            this.LookUpEdit_authentication_code.Properties.ShowFooter = false;
            this.LookUpEdit_authentication_code.Properties.SortColumnIndex = 1;
            this.LookUpEdit_authentication_code.Properties.ValueMember = "Id";
            this.LookUpEdit_authentication_code.Size = new System.Drawing.Size(283, 20);
            this.LookUpEdit_authentication_code.StyleController = this.LayoutControl1;
            this.LookUpEdit_authentication_code.TabIndex = 5;
            // 
            // TextEdit_trace_number
            // 
            this.TextEdit_trace_number.Location = new System.Drawing.Point(87, 12);
            this.TextEdit_trace_number.Name = "TextEdit_trace_number";
            this.TextEdit_trace_number.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_trace_number.Properties.Mask.BeepOnError = true;
            this.TextEdit_trace_number.Properties.Mask.EditMask = "000000000000000";
            this.TextEdit_trace_number.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.TextEdit_trace_number.Properties.Mask.SaveLiteral = false;
            this.TextEdit_trace_number.Properties.MaxLength = 15;
            this.TextEdit_trace_number.Properties.ValidateOnEnterKey = true;
            this.TextEdit_trace_number.Size = new System.Drawing.Size(283, 20);
            this.TextEdit_trace_number.StyleController = this.LayoutControl1;
            this.TextEdit_trace_number.TabIndex = 4;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.EmptySpaceItem1,
            this.EmptySpaceItem2,
            this.EmptySpaceItem3});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(382, 142);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.TextEdit_trace_number;
            this.LayoutControlItem1.CustomizationFormText = "Trace Number";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(362, 24);
            this.LayoutControlItem1.Text = "Trace Number";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.LookUpEdit_authentication_code;
            this.LayoutControlItem2.CustomizationFormText = "Error Condition";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(362, 24);
            this.LayoutControlItem2.Text = "Error Condition";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(72, 13);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.SimpleButton1;
            this.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3";
            this.LayoutControlItem3.Location = new System.Drawing.Point(104, 95);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem3.Text = "LayoutControlItem3";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.SimpleButton2;
            this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4";
            this.LayoutControlItem4.Location = new System.Drawing.Point(183, 95);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem4.Text = "LayoutControlItem4";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 48);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(362, 47);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(262, 95);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(100, 27);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem3
            // 
            this.EmptySpaceItem3.AllowHotTrack = false;
            this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem3.Location = new System.Drawing.Point(0, 95);
            this.EmptySpaceItem3.Name = "EmptySpaceItem3";
            this.EmptySpaceItem3.Size = new System.Drawing.Size(104, 27);
            this.EmptySpaceItem3.Text = "EmptySpaceItem3";
            this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DataEntryForm
            // 
            this.AcceptButton = this.SimpleButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton2;
            this.ClientSize = new System.Drawing.Size(382, 142);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "DataEntryForm";
            this.Text = "Edit Multiple Items";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_authentication_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_trace_number.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }

        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_authentication_code;
        internal DevExpress.XtraEditors.TextEdit TextEdit_trace_number;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
    }
}
