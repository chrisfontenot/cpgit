using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.LINQ;
using System.Linq;
using System.Windows.Forms;

#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    internal partial class EditItemForm
    {
        private DebtPlus.LINQ.deposit_batch_detail detail;

        public EditItemForm(DebtPlus.LINQ.deposit_batch_detail detail)
            : base()
		{
			InitializeComponent();
            this.detail = detail;
            RegisterHandlers();
		}

        private void RegisterHandlers()
        {
            SimpleButton1.Click += new EventHandler(SimpleButton1_Click);
            Load += EditItemForm_Load;
        }

        private void UnRegisterHandlers()
        {
            SimpleButton1.Click -= new EventHandler(SimpleButton1_Click);
            Load -= EditItemForm_Load;
        }

        private void EditItemForm_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LabelControl_amount.Text = string.Format("{0:c}", detail.amount);
                LabelControl_client.Text = string.Format("{0:0000000}", detail.client);
                LabelControl_trace_number.Text = detail.reference;
                LookUpEdit_authentication_code.EditValue = detail.ach_authentication_code;

                // Set the data source for the list of error codes
                LookUpEdit_authentication_code.Properties.DataSource = DebtPlus.LINQ.Cache.ach_reject_code.getList();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void SimpleButton1_Click(object sender, EventArgs e)
        {
            detail.ach_authentication_code = DebtPlus.Utils.Nulls.v_String(LookUpEdit_authentication_code.EditValue);
        }
    }
}
