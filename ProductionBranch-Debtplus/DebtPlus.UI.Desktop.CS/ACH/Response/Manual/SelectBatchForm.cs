#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Utils;
using System.Data.SqlClient;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    public partial class SelectBatchForm
    {
        private System.Collections.Generic.List<DebtPlus.LINQ.deposit_batch_id> colRecords;

        internal SelectBatchForm()
            : base()
		{
			Load += SelectBatchForm_Load;
			InitializeComponent();
		}

        public Int32 BatchID
        {
            get
            {
                if (this.LookUpEdit1.EditValue == null)
                {
                    return -1;
                }

                return System.Convert.ToInt32(this.LookUpEdit1.EditValue);
            }
        }

        private void SelectBatchForm_Load(object sender, System.EventArgs e)
        {
            using (var bc = new DebtPlus.LINQ.BusinessContext())
            {
                colRecords = (from id in bc.deposit_batch_ids where id.date_posted == null && id.batch_type == "AC" orderby id.date_created descending select id).ToList();
                LookUpEdit1.Properties.DataSource = colRecords;
                if (colRecords.Count > 0)
                {
                    LookUpEdit1.EditValue = colRecords[0].Id;
                }

                SimpleButton1.Enabled = (BatchID > 0);
            }
        }

        private void LookUpEdit1_EditValueChanged(object sender, System.EventArgs e)
        {
            SimpleButton1.Enabled = (BatchID > 0);
        }
    }
}
