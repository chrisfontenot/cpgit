using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    partial class SelectBatchForm : DebtPlus.Data.Forms.DebtPlusForm
    {

        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.LookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).BeginInit();
            this.SuspendLayout();
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.SimpleButton2);
            this.LayoutControl1.Controls.Add(this.SimpleButton1);
            this.LayoutControl1.Controls.Add(this.LookUpEdit1);
            this.LayoutControl1.Controls.Add(this.LabelControl1);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(326, 148);
            this.LayoutControl1.TabIndex = 0;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //SimpleButton2
            //
            this.SimpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton2.Location = new System.Drawing.Point(177, 113);
            this.SimpleButton2.MaximumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton2.MinimumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton2.Name = "SimpleButton2";
            this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton2.StyleController = this.LayoutControl1;
            this.SimpleButton2.TabIndex = 7;
            this.SimpleButton2.Text = "&Cancel";
            //
            //SimpleButton1
            //
            this.SimpleButton1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton1.Location = new System.Drawing.Point(98, 113);
            this.SimpleButton1.MaximumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.MinimumSize = new System.Drawing.Size(75, 23);
            this.SimpleButton1.Name = "SimpleButton1";
            this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton1.StyleController = this.LayoutControl1;
            this.SimpleButton1.TabIndex = 6;
            this.SimpleButton1.Text = "&OK";
            //
            //LookUpEdit1
            //
            this.LookUpEdit1.Location = new System.Drawing.Point(15, 59);
            this.LookUpEdit1.Name = "LookUpEdit1";
            this.LookUpEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "dddd, MMMM d, yyyy h:mm tt", true, DevExpress.Utils.HorzAlignment.Default)
			});
            this.LookUpEdit1.Properties.DisplayFormat.FormatString = "{0:dddd, MMMM d, yyyy h:mm tt}";
            this.LookUpEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.LookUpEdit1.Properties.DisplayMember = "date_created";
            this.LookUpEdit1.Properties.NullText = "";
            this.LookUpEdit1.Properties.ShowFooter = false;
            this.LookUpEdit1.Properties.ShowHeader = false;
            this.LookUpEdit1.Properties.ValueMember = "Id";
            this.LookUpEdit1.Size = new System.Drawing.Size(296, 20);
            this.LookUpEdit1.StyleController = this.LayoutControl1;
            this.LookUpEdit1.TabIndex = 5;
            this.LookUpEdit1.Properties.SortColumnIndex = 1;
            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(12, 12);
            this.LabelControl1.MinimumSize = new System.Drawing.Size(0, 40);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Padding = new System.Windows.Forms.Padding(5);
            this.LabelControl1.Size = new System.Drawing.Size(302, 40);
            this.LabelControl1.StyleController = this.LayoutControl1;
            this.LabelControl1.TabIndex = 4;
            this.LabelControl1.Text = "Select the desired ACH batch to be processed. The following list shows the date t" + "hat the batch was generated.";
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2,
				this.EmptySpaceItem3
			});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(326, 148);
            this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.LabelControl1;
            this.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(306, 44);
            this.LayoutControlItem1.Text = "LayoutControlItem1";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem2.AppearanceItemCaption.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LayoutControlItem2.Control = this.LookUpEdit1;
            this.LayoutControlItem2.CustomizationFormText = "Batch List";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 44);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.LayoutControlItem2.Size = new System.Drawing.Size(306, 30);
            this.LayoutControlItem2.Text = "Batch List";
            this.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Top;
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.SimpleButton1;
            this.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3";
            this.LayoutControlItem3.Location = new System.Drawing.Point(86, 101);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem3.Text = "LayoutControlItem3";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.SimpleButton2;
            this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4";
            this.LayoutControlItem4.Location = new System.Drawing.Point(165, 101);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem4.Text = "LayoutControlItem4";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 74);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(354, 27);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem2
            //
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(257, 101);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(97, 27);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem3
            //
            this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem3.Location = new System.Drawing.Point(0, 101);
            this.EmptySpaceItem3.Name = "EmptySpaceItem3";
            this.EmptySpaceItem3.Size = new System.Drawing.Size(99, 27);
            this.EmptySpaceItem3.Text = "EmptySpaceItem3";
            this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            //
            //SelectBatchForm
            //
            this.AcceptButton = this.SimpleButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton2;
            this.ClientSize = new System.Drawing.Size(326, 148);
            this.Controls.Add(this.LayoutControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "SelectBatchForm";
            this.Text = "Select ACH Batch";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).EndInit();
            this.ResumeLayout(false);
        }

        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit1;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
    }
}
