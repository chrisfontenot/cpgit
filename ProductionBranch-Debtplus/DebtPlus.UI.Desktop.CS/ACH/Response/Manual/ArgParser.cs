using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.Manual
{
    /// <summary>
    /// Class to parse the argument strings
    /// </summary>
    internal class ResponseManualArgParser : DebtPlus.Utils.ArgParserBase
    {
		private Int32 privateBatch = -1;

		// private DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
		// Global dataset for use as pseudo-static item

		// public System.Data.DataSet ds = new System.Data.DataSet("ds");

		/// <summary>
		/// Batch ID for processing
		/// </summary>
		/// <value></value>
		/// <returns></returns>
		/// <remarks></remarks>
		public Int32 ProcessingBatch
        {
			get
            {
                return privateBatch;
            }
			set
            {
                privateBatch = value;
            }
		}

		/// <summary>
		/// Create an instance of our class
		/// </summary>
        public ResponseManualArgParser()
            : base(new string[] { })
		{
		}

		protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
		{
			SwitchStatus Result = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
			switch (switchSymbol)
            {
				case "b":
					try
                    {
						privateBatch = Int32.Parse(switchValue);
					}
#pragma warning disable 168
                    catch (System.Exception ex)
                    {
						Result = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
					}
#pragma warning restore 168
					break;

				default:
					Result = base.OnSwitch(switchSymbol, switchValue);
					break;
			}

			return Result;
		}

		/// <summary>
		/// Generate the command usage information
		/// </summary>
		protected override void OnUsage(string errorInfo)
		{
			// deprecated
		}

#if false
		/// <summary>
		/// Return the table with the reject codes
		/// </summary>
		/// <returns></returns>
		/// <remarks></remarks>
		public System.Data.DataTable ACHRejectCodes()
		{
			const string TableName = "ach_reject_codes";
			System.Data.DataTable tbl = ds.Tables[TableName];

			if (tbl == null)
            {
				System.Data.SqlClient.SqlConnection cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
				try
                {
					cn.Open();
					using (var cmd = new System.Data.SqlClient.SqlCommand())
                    {
						cmd.Connection = cn;
						cmd.CommandText = "SELECT ach_reject_code, description, serious_error FROM ach_reject_codes";
						cmd.CommandType = System.Data.CommandType.Text;

						using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
							da.Fill(ds, TableName);
							tbl = ds.Tables[TableName];

							if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                            {
								tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["ach_reject_code"] };
							}

							// Add the pseudo row for "no error" to the list.
							if (tbl.Rows.Find(string.Empty) == null)
                            {
								System.Data.DataRow row = tbl.NewRow();
								row["ach_reject_code"] = string.Empty;
								row["description"] = "NO ERROR";
								row["serious_error"] = false;
								tbl.Rows.Add(row);

								row.AcceptChanges();
								tbl.AcceptChanges();
							}
						}
					}
				}
                catch (System.Data.SqlClient.SqlException ex)
                {
					DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading ACH reject codes");
				}
			}

			return tbl;
		}

		public void SaveChanges()
		{
			const string TableName = "xpr_ACH_file_contents";
			System.Data.DataTable tbl = ds.Tables[TableName];

			if (tbl != null)
            {
				System.Data.SqlClient.SqlCommand UpdateCmd = new System.Data.SqlClient.SqlCommand();
				System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
				Int32 RowsUpdated = default(Int32);
				try
                {
                    UpdateCmd.Connection = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    UpdateCmd.CommandText = "xpr_ACH_response_PAYMENT";
                    UpdateCmd.CommandType = System.Data.CommandType.StoredProcedure;
                    UpdateCmd.Parameters.Add("@deposit_batch_detail", System.Data.SqlDbType.Int, 0, "item_key");
                    UpdateCmd.Parameters.Add("@reason", System.Data.SqlDbType.VarChar, 4, "authentication_code");
                    UpdateCmd.Parameters.Add("@ach_response_batch_id", System.Data.SqlDbType.VarChar, 50).Value = ProcessingBatch;
					da.UpdateCommand = UpdateCmd;
					RowsUpdated = da.Update(tbl);
				}
                catch (System.Data.SqlClient.SqlException ex)
                {
					DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating ACH transactions");
				}
                finally
                {
					da.Dispose();
					UpdateCmd.Dispose();
				}
			}
		}
#endif

	}
}
