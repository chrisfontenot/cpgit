﻿namespace DebtPlus.UI.Desktop.CS.ACH.Response.File
{
    partial class ACHStatus_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            ACHStatus_Form_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_Payments = new DevExpress.XtraEditors.LabelControl();
            this.Label_RecordCount = new DevExpress.XtraEditors.LabelControl();
            this.Label_BatchCount = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Label_Status = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // Label_Payments
            // 
            this.Label_Payments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_Payments.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Label_Payments.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Payments.Location = new System.Drawing.Point(274, 90);
            this.Label_Payments.Name = "Label_Payments";
            this.Label_Payments.Size = new System.Drawing.Size(80, 13);
            this.Label_Payments.TabIndex = 18;
            this.Label_Payments.Text = "0";
            this.Label_Payments.UseMnemonic = false;
            // 
            // Label_RecordCount
            // 
            this.Label_RecordCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_RecordCount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Label_RecordCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_RecordCount.Location = new System.Drawing.Point(274, 71);
            this.Label_RecordCount.Name = "Label_RecordCount";
            this.Label_RecordCount.Size = new System.Drawing.Size(80, 13);
            this.Label_RecordCount.TabIndex = 17;
            this.Label_RecordCount.Text = "0";
            this.Label_RecordCount.UseMnemonic = false;
            // 
            // Label_BatchCount
            // 
            this.Label_BatchCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Label_BatchCount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Label_BatchCount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_BatchCount.Location = new System.Drawing.Point(274, 52);
            this.Label_BatchCount.Name = "Label_BatchCount";
            this.Label_BatchCount.Size = new System.Drawing.Size(80, 13);
            this.Label_BatchCount.TabIndex = 16;
            this.Label_BatchCount.Text = "0";
            this.Label_BatchCount.UseMnemonic = false;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(146, 122);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 15;
            this.Button_OK.Text = "&Cancel";
            // 
            // Label_Status
            // 
            this.Label_Status.Appearance.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.Label_Status.Location = new System.Drawing.Point(12, 12);
            this.Label_Status.Name = "Label_Status";
            this.Label_Status.Size = new System.Drawing.Size(308, 23);
            this.Label_Status.TabIndex = 14;
            this.Label_Status.Text = "Response File Processing Status";
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(12, 90);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(169, 13);
            this.LabelControl3.TabIndex = 13;
            this.LabelControl3.Text = "Number of payment reject records:";
            // 
            // LabelControl2
            // 
            this.LabelControl2.Location = new System.Drawing.Point(12, 71);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(145, 13);
            this.LabelControl2.TabIndex = 12;
            this.LabelControl2.Text = "Number of records processed:";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(12, 52);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(147, 13);
            this.LabelControl1.TabIndex = 11;
            this.LabelControl1.Text = "Number of batches processed:";
            // 
            // ACHStatus_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 157);
            this.Controls.Add(this.Label_Payments);
            this.Controls.Add(this.Label_RecordCount);
            this.Controls.Add(this.Label_BatchCount);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Label_Status);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "ACHStatus_Form";
            this.Text = "ACH Reject File Processing Status";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.LabelControl Label_Payments;
        internal DevExpress.XtraEditors.LabelControl Label_RecordCount;
        internal DevExpress.XtraEditors.LabelControl Label_BatchCount;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.LabelControl Label_Status;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
    }
}