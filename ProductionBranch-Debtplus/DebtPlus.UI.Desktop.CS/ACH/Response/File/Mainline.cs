using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.File
{
    /// <summary>
    /// Class to parse the argument strings
    /// </summary>
    public class ResponseFileArgParser : DebtPlus.Utils.ArgParserBase
    {
        public ResponseFileArgParser()
            : base(new string[] { "" })
        {
            pathnames = new List<string>();
            ACH_Response_File_ID = Guid.NewGuid().ToString().ToUpper();
        }

        public string ACH_Response_File_ID { get; private set; }
        public List<string> pathnames { get; private set; }

        /// <summary>
        /// Handle a switch and its value
        /// </summary>
        /// <param name="switchSymbol"></param>
        /// <param name="switchValue"></param>
        /// <returns></returns>
        protected override DebtPlus.Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            DebtPlus.Utils.ArgParserBase.SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Define the usage of the program.
        /// </summary>
        /// <param name="errorInfo"></param>
        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}\r\n", errorInfo));
            }

            System.Reflection.Assembly _asm = System.Reflection.Assembly.GetExecutingAssembly();
            string fname = System.IO.Path.GetFileName(_asm.CodeBase);
            AppendErrorLine("Usage: " + fname);
        }

        /// <summary>
        /// Process the completion of the parsing logic. This is called at the end just before the ArgParser returns from the Parse method.
        /// </summary>
        /// <returns></returns>
        protected override SwitchStatus OnDoneParse()
        {
            // Sort all the pathnames in the list
            if (pathnames.Count == 0)
            {
                DialogResult Answer;
                using (var dlg = new OpenFileDialog()
                                            {
                                                Title = "Open KeyField response file",
                                                ShowReadOnly = false,
                                                ShowHelp = false,
                                                RestoreDirectory = true,
                                                Multiselect = true,
                                                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                                                DereferenceLinks = true,
                                                CheckPathExists = true,
                                                CheckFileExists = true,
                                                AddExtension = true,
                                                ReadOnlyChecked = true,
                                                AutoUpgradeEnabled = true,
                                                Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*",
                                                DefaultExt = "txt",
                                                ValidateNames = true
                                            } )
                {
                    dlg.FileOk += dlg_FileOk;
                    Answer = dlg.ShowDialog();
                    dlg.FileOk -= dlg_FileOk;

                    // Add the filenames to the list
                    if (Answer == System.Windows.Forms.DialogResult.OK)
                    {
                        foreach (string f in dlg.FileNames)
                        {
                            pathnames.Add(f);
                        }
                    }
                }
            }

            // if there are no names then return an error. Otherwise, sort them.
            if (pathnames.Count == 0)
            {
                return DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }
            pathnames.Sort(0, pathnames.Count, null);

            // Return the ancestor's value now.
            return base.OnDoneParse();
        }


        /// <summary>
        /// Validation routine for the file open dialog
        /// </summary>
        public void dlg_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool Canceled = true;

            // Process the selected file names
            foreach (string fname in ((OpenFileDialog)sender).FileNames)
            {
                Canceled = true;
                if (fname == null)
                {
                    break;
                }

                // Read the first part of the file. It should start with the string "101"
                try
                {
                    string FirstPart = string.Empty;
                    using (var fs = new System.IO.FileStream(fname, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 4096))
                    {
                        using (var txt = new System.IO.StreamReader(fs))
                        {
                            FirstPart = txt.ReadLine();
                            txt.Close();
                        }
                        fs.Close();
                    }

                    // Look for a valid response file
                    if( FirstPart.Length > 80 && FirstPart.StartsWith("101") && FirstPart.Substring(33, 7) == "1094101" )
                    {
                        Canceled = false;
                    }
                }

#pragma warning disable 168
                // These are ignored at this point
                catch (System.IO.FileNotFoundException ex)
{ }
                catch (System.IO.DirectoryNotFoundException ex)
{ }
                catch (System.IO.EndOfStreamException ex)
{ }
#pragma warning restore 168

                // if the entry is not valid, set the cancel status
                if (Canceled)
                {
                    break;
                }
            }

            e.Cancel = Canceled;
        }
    }

    public class Mainline : DebtPlus.Interfaces.Desktop.IDesktopMainline //, DebtPlus.Interfaces.Desktop.ISupportCreateClientRequest
    {
        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ResponseFileArgParser();
                    if (ap.Parse((string[])param))
                    {
                        var frm = new ACHStatus_Form(ap);
                        System.Windows.Forms.Application.Run(frm);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "ACHResponseFile"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}
