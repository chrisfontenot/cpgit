using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.ACH.Response.File
{
    public partial class ACHStatus_Form : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Process the disposal for the form. Remove any storage allocaetd in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void ACHStatus_Form_Dispose(bool disposing)
        {
            // Remove the handlers so that the form may be properly destroyed
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="DatabaseInfo">Pointer to the database connection object</param>
        public ACHStatus_Form()
            : base()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += ACHStatus_Form_Load;
            bw.DoWork += bw_DoWork;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            Button_OK.Click += Button_OK_Click;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= ACHStatus_Form_Load;
            bw.DoWork -= bw_DoWork;
            bw.RunWorkerCompleted -= bw_RunWorkerCompleted;
            Button_OK.Click -= Button_OK_Click;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ACHStatus_Form_Load(object sender, EventArgs e)
        {
            bw.RunWorkerAsync();
        }

        private delegate void StatusDelegate(Int32 Count);
        private readonly System.ComponentModel.BackgroundWorker bw = new System.ComponentModel.BackgroundWorker();
        private readonly ResponseFileArgParser ap;

        internal Int32 record_count;
        internal Int32 Batch_count;
        internal Int32 Payment_count;


        /// <summary>
        /// Normal method of creating our form
        /// </summary>
        public ACHStatus_Form(ResponseFileArgParser ap)
            : this()
        {
            this.ap = ap;
        }

        /// <summary>
        /// Operation is complete
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Thread termination
        /// </summary>
        private void bw_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Label_Status.Text = "Processing Complete. Press OK";
            Button_OK.Text = "&OK";
        }

        /// <summary>
        /// Update the counts for the various items
        /// </summary>
        private void RecordCount(Int32 Count)
        {
            if (InvokeRequired)
            {
                EndInvoke(BeginInvoke(new StatusDelegate(RecordCount), new Object[] { Count }));
            }
            else
            {
                Label_RecordCount.Text = string.Format("{0:n0}", Count);
            }
        }

        /// <summary>
        /// Update the number of batches rejected
        /// </summary>
        private void BatchCount(Int32 Count)
        {
            if (InvokeRequired)
            {
                EndInvoke(BeginInvoke(new StatusDelegate(BatchCount), new Object[] { Count }));
            }
            else
            {
                Label_BatchCount.Text = string.Format("{0:n0}", Count);
            }
        }

        /// <summary>
        /// Update the number of payments rejected
        /// </summary>
        private void PaymentCount(Int32 Count)
        {
            if (InvokeRequired)
            {
                EndInvoke(BeginInvoke(new StatusDelegate(PaymentCount), new Object[] { Count }));
            }
            else
            {
                Label_Payments.Text = string.Format("{0:n0}", Count);
            }
        }

        /// <summary>
        /// Handle the thread procedure
        /// </summary>
        private void bw_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Process the fields in this response file
            foreach (string fname in ap.pathnames)
            {
                var cls = new InputProcessing(ap.ACH_Response_File_ID, this);
                cls.ParseFile(fname);
            }
        }

        /// <summary>
        /// Class to read the KeyField file
        /// </summary>
        private class Reader : IDisposable
        {
            public Reader()
            {
            }

            private ACHStatus_Form statusForm = null;

            public Reader(string FileName, ACHStatus_Form statusForm)
                : this()
            {
                this.FileName = FileName;
                this.statusForm = statusForm;
            }

            private System.IO.FileStream fs;
            private System.IO.StreamReader InputStream;
            private char RecordType = ' ';
            private string privateNextLine = string.Empty;
            private bool privateAtEnd;

            /// <summary>
            /// Input file name
            /// </summary>
            public string FileName { get; set; }

            /// <summary>
            /// Flag to indicate that the file is at the end
            /// </summary>
            public bool AtEnd
            {
                get
                {
                    return privateAtEnd;
                }
            }

            /// <summary>
            /// Line buffer for the KeyField file
            /// </summary>
            public string NextLine
            {
                get
                {
                    return privateNextLine;
                }
            }


            /// <summary>
            /// Access the next line in the KeyField file
            /// </summary>
            public bool ReadNextLine()
            {
                if (!AtEnd && InputStream.EndOfStream)
                {
                    privateAtEnd = true;
                }

                if (privateAtEnd)
                {
                    return false;
                }

                privateNextLine = InputStream.ReadLine().PadRight(94);
                RecordType = privateNextLine[0];

                // Count the line
                statusForm.record_count += 1;
                statusForm.RecordCount(statusForm.record_count);

                return true;
            }


            /// <summary>
            /// Look for a type 1 record
            /// </summary>
            public bool IsType1
            {
                get
                {
                    return RecordType == '1';
                }
            }


            /// <summary>
            /// Look for a type 5 record
            /// </summary>
            public bool IsType5
            {
                get
                {
                    return RecordType == '5';
                }
            }


            /// <summary>
            /// Look for a type 6 record
            /// </summary>
            public bool IsType6
            {
                get
                {
                    return RecordType == '6';
                }
            }


            /// <summary>
            /// Look for a type 7 record
            /// </summary>
            public bool IsType7
            {
                get
                {
                    return RecordType == '7';
                }
            }


            /// <summary>
            /// Look for a type 8 record
            /// </summary>
            public bool IsType8
            {
                get
                {
                    return RecordType == '8';
                }
            }


            /// <summary>
            /// Look for a type 9 record
            /// </summary>
            public bool IsType9
            {
                get
                {
                    return RecordType == '9';
                }
            }


            /// <summary>
            /// Open the KeyField file
            /// </summary>
            public bool OpenFile()
            {
                bool Answer = false;

                try
                {
                    fs = new System.IO.FileStream(FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 4096);
                    InputStream = new System.IO.StreamReader(fs);
                    Answer = true;
                }
#pragma warning disable 168
                catch (System.IO.FileNotFoundException ex)
{ }
                catch (System.IO.DirectoryNotFoundException ex)
{ }
                catch (System.IO.PathTooLongException ex)
{ }
#pragma warning restore 168

                return Answer;
            }


            /// <summary>
            /// Close the KeyField file
            /// </summary>
            public void CloseFile()
            {
                if (InputStream != null)
                {
                    InputStream.Close();
                }
                if (fs != null)
                {
                    fs.Close();
                }
            }


            /// <summary>
            /// Dispose of the class
            /// </summary>
            private bool disposedValue;        // To detect redundant calls
            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        disposedValue = true;

                        CloseFile();

                        // Dispose of the KeyField streams
                        InputStream.Dispose();
                        fs.Dispose();
                    }
                }
            }

            public void Dispose() // Implements IDisposable.Dispose
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            ~Reader()
            {
                Dispose(false);
            }
        }


        /// <summary>
        /// Generic class for files, batches, and transactions
        /// </summary>
        private class Responses
        {
            protected ACHStatus_Form statusForm = null;

            /// <summary>
            /// Linkage to the file reader class
            /// </summary>
            public Reader rdr { get; set; }


            /// <summary>
            /// Set the error code value
            /// </summary>
            public string ErrorCode { get; set; }


            /// <summary>
            /// return the base error code
            /// </summary>
            public virtual string GetError()
            {
                return ErrorCode;
            }


            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public Responses()
            {
                rdr = null;
                ErrorCode = string.Empty;
            }

            public Responses(Reader rdr, ACHStatus_Form statusForm)
                : this()
            {
                this.rdr = rdr;
                this.statusForm = statusForm;
            }

            /// <summary>
            /// Record the transactions to the database
            /// </summary>
            public virtual void UpdateDatabase()
            {
            }
        }


        /// <summary>
        /// A transaction is a representation of the following class
        /// </summary>
        private class StandardTransactionProcessing : Responses
        {
            protected BatchProcessing Batch;

            // List of addendum records
            protected System.Collections.Specialized.StringCollection AddendumCollection = new System.Collections.Specialized.StringCollection();
            protected string TransactionLine = string.Empty;


            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public StandardTransactionProcessing()
            {
            }

            public StandardTransactionProcessing(Reader rdr, BatchProcessing Batch, ACHStatus_Form statusForm)
                : base(rdr, statusForm)
            {
                this.Batch = Batch;
            }

            public StandardTransactionProcessing(Reader rdr, ACHStatus_Form statusForm) : base(rdr, statusForm)
            {
            }


            /// <summary>
            /// Parse the type 6 (Transaction Header) record
            /// </summary>
            public virtual bool ParseType6()
            {
                bool Answer = false;

                // Save the transaction line for later examination
                TransactionLine = rdr.NextLine;

                // Gather all of the addendum records for this line. We don't look at the count
                // since the note buffers don't have a valid count.
                while (rdr.ReadNextLine())
                {
                    if (rdr.IsType7)
                    {
                        AddendumCollection.Add(rdr.NextLine);
                    }
                    else
                    {
                        Answer = true;
                        break;
                    }
                }

                return Answer;
            }


            /// <summary>
            /// Obtain the error status for the file
            /// </summary>
            public override string GetError()
            {
                string Answer = ErrorCode;
                if (Answer == string.Empty)
                {
                    Answer = Batch.GetError();
                }
                return Answer;
            }


            /// <summary>
            /// Record the transactions to the database
            /// </summary>
            public override void UpdateDatabase()
            {
                string TraceNumber = TransactionLine.Substring(79, 15);
                string ErrorCode = TransactionLine.Substring(12, 3);

                // Change the values to the addendum line if there is one
                if (AddendumCollection.Count > 0 && string.Compare(Batch.ResponseType, "RETURN    ", true, System.Globalization.CultureInfo.InvariantCulture) == 0)
                {
                    ErrorCode = AddendumCollection[0].Substring(3, 3);
                    TraceNumber = AddendumCollection[0].Substring(6, 15);
                }

                // Count this payment reject
                statusForm.Payment_count += 1;
                statusForm.PaymentCount(statusForm.Payment_count);

                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    try
                    {
                        DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                        using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                        {
                            cn.Open();
                            using (var cmd = new System.Data.SqlClient.SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandText = "xpr_ach_response_PAYMENT";
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@batch_id", SqlDbType.VarChar, 80).Value = Batch.File.ResponseFileID;
                                cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = TraceNumber;
                                cmd.Parameters.Add("@reason", SqlDbType.VarChar, 80).Value = GetError();
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    catch (SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error writing ACH reject transaction");
                    }
                }
            }
        }


        /// <summary>
        /// A batch is a representation of the following class
        /// </summary>
        private class BatchProcessing : Responses
        {
            // List of transactions for this batch
            private readonly LinkedList<Responses> TranCollection = new LinkedList<Responses>();
            public FileProcessing File;

            // Information from the batch header
            public string BatchHeader = string.Empty;
            public string ResponseType = string.Empty;


            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public BatchProcessing()
            {
            }

            public BatchProcessing(Reader rdr, FileProcessing File, ACHStatus_Form statusForm)
                : base(rdr, statusForm)
            {
                this.File = File;
            }

            public BatchProcessing(Reader rdr, ACHStatus_Form statusForm)
                : base(rdr, statusForm)
            {
            }


            /// <summary>
            /// Obtain the error status for the file
            /// </summary>
            public override string GetError()
            {
                string Answer = ErrorCode;
                if (Answer == string.Empty)
                {
                    Answer = File.GetError();
                }
                return Answer;
            }


            /// <summary>
            /// Process a batch from the creditor
            /// </summary>
            public bool ParseType5()
            {
                bool Answer = false;

                // Determine the biller id for the response
                BatchHeader = rdr.NextLine;
                ResponseType = BatchHeader.Substring(53, 10);

                // Read the first transaction line which should follow the batch header
                if (!rdr.ReadNextLine())
                {
                    return false;
                }

                // If this is the end of the batch then stop processing
                for (; ; )
                {
                    if (rdr.IsType8)
                    {
                        ParseType8();
                        Answer = true;
                        break;
                    }

                    // If this is the end of file record then we missed the batch EOF
                    if (rdr.IsType9 || rdr.IsType1)
                    {
                        Answer = false;
                        break;
                    }

                    // If this is a type 6 record then process the batch
                    if (rdr.IsType6)
                    {
                        StandardTransactionProcessing Tran = new StandardTransactionProcessing(rdr, this, statusForm);
                        if (Tran.ParseType6())
                        {
                            TranCollection.AddLast(Tran);
                        }
                        Answer = false;
                    }

                    // If this is not the expected type, flush the record until we find one.
                    else if (!rdr.ReadNextLine())
                    {
                        Answer = false;
                        break;
                    }
                }

                return Answer;
            }


            /// <summary>
            /// Parse the type 8 (EOB) record for batch trailer
            /// </summary>
            public bool ParseType8()
            {
                // Save the error code associated with the batch
                ErrorCode = rdr.NextLine.Substring(54, 3).Trim();
                return false;
            }


            /// <summary>
            /// Record the transactions to the database
            /// </summary>
            public override void UpdateDatabase()
            {
                // Count this payment reject
                statusForm.Batch_count += 1;
                statusForm.BatchCount(statusForm.Batch_count);

                foreach (Responses tran in TranCollection)
                {
                    tran.UpdateDatabase();
                }
            }
        }


        /// <summary>
        /// A file is a representation of the following class
        /// </summary>
        private class FileProcessing : Responses
        {
            private readonly LinkedList<BatchProcessing> BatchCollection = new LinkedList<BatchProcessing>();
            public string ResponseFileID = string.Empty;
            public string FileHeader = string.Empty;
            public string FileTrailer = string.Empty;


            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public FileProcessing()
            {
            }

            public FileProcessing(Reader rdr, string ResponseFileID, ACHStatus_Form statusForm)
                : base(rdr, statusForm)
            {
                this.ResponseFileID = ResponseFileID;
            }

            public FileProcessing(Reader rdr, ACHStatus_Form statusForm) : base(rdr, statusForm)
            {
            }


            /// <summary>
            /// Examine the type 1 (HEADER) record which starts the file
            /// </summary>
            public bool ParseType1()
            {
                bool Answer = false;

                // Examine the type 1 start of file record
                FileHeader = rdr.NextLine;
                if (FileHeader.Substring(0, 3) != "101" || FileHeader.Substring(33, 7) != "1094101")
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The file does not have a valid RPPS reject header near line 1");
                    return false;
                }

                // Read the first line for the file contents
                if (!rdr.ReadNextLine())
                {
                    return false;
                }

                for (; ; )
                {
                    // If this is the end of the file then stop processing
                    if (rdr.IsType9)
                    {
                        ParseType9();
                        Answer = true;
                        break;
                    }

                    // If this is a new type 1 record then we missed the type 9
                    if (rdr.IsType1)
                    {
                        Answer = false;
                        break;
                    }

                    // If this is a type 5 record then process the batch
                    if (rdr.IsType5)
                    {
                        BatchProcessing batch = new BatchProcessing(rdr, this, statusForm);
                        if (batch.ParseType5())
                        {
                            BatchCollection.AddLast(batch);
                        }
                    }
                    else if (!rdr.ReadNextLine())
                    {
                        Answer = false;
                        break;
                    }
                }

                return Answer;
            }


            /// <summary>
            /// Examine the type 9 (EOF) record which terminates the file
            /// </summary>
            public bool ParseType9()
            {
                // Save the file trailer line
                FileTrailer = rdr.NextLine;

                return true;
            }


            /// <summary>
            /// Record the transactions to the database
            /// </summary>
            public override void UpdateDatabase()
            {
                foreach (BatchProcessing batch in BatchCollection)
                {
                    batch.UpdateDatabase();
                }
            }
        }


        /// <summary>
        /// Class to manage the processing of multiple files in a dataset
        /// </summary>
        private class InputProcessing
        {

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            private readonly string ResponseFileID = string.Empty;
            private ACHStatus_Form statusForm = null;
            public InputProcessing(string ResponseFileID, ACHStatus_Form statusForm)
            {
                this.ResponseFileID = ResponseFileID;
                this.statusForm = statusForm;
            }


            /// <summary>
            /// Parse the KeyField file
            /// </summary>
            public void ParseFile(string FileName)
            {
                using( Reader rdr = new Reader(FileName, statusForm) )
                {
                    // Open and read the first line in the file
                    if (rdr.OpenFile())
                    {
                        while( rdr.ReadNextLine() )
                        {
                            // If the record is a type 1 then process the new file
                            while (rdr.IsType1)
                            {
                                var ctl = new FileProcessing(rdr, ResponseFileID, statusForm);
                                if (ctl.ParseType1())
                                {
                                    ctl.UpdateDatabase();
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
