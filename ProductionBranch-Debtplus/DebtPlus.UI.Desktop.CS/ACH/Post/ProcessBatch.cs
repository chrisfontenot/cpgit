using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    public partial class ProcessBatch : DevExpress.XtraEditors.XtraUserControl
    {
        public event EventHandler Completed;
        private readonly BackgroundWorker bw = new BackgroundWorker();
        public DebtPlus.UI.Desktop.CS.ACH.Post.Form_ACH_Post parentForm;

        protected PostArgParser Globals
        {
            get
            {
                Form_ACH_Post frm = parentForm;
                System.Diagnostics.Debug.Assert(frm != null);
                return frm.ap;
            }
        }

        private void ProcessBatch_Dispose(bool disposing)
        {
            if (disposing)
            {
                bw.Dispose();
            }
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        public ProcessBatch()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Load += ProcessBatch_Load;
            bw.RunWorkerCompleted += bw_RunWorkerCompleted;
            bw.DoWork += bw_DoWork;
        }

        private void UnRegisterHandlers()
        {
            Load -= ProcessBatch_Load;
            bw.RunWorkerCompleted -= bw_RunWorkerCompleted;
            bw.DoWork -= bw_DoWork;
        }


        /// <summary>
        /// Start the posting operation
        /// </summary>
        public void Process()
        {
            MarqueeProgressBarControl1.Properties.Stopped = false;
            bw.RunWorkerAsync();
        }


        /// <summary>
        /// Indicate that we have finished processing
        /// </summary>
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MarqueeProgressBarControl1.Properties.Stopped = true;
            if (Completed != null)
            {
                Completed(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Post the batch
        /// </summary>
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_ACH_Post_Batch";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                        cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                        cmd.Parameters.Add("@ACHFile", SqlDbType.Int).Value = Globals.Batch;
                        cmd.ExecuteNonQuery();

                        Globals.trust_register = Convert.ToInt32(cmd.Parameters[0].Value);
                    }
                }
            }
        }

        /// <summary>
        /// This is the load of the control -- initialize it.
        /// </summary>
        private void ProcessBatch_Load(object sender, EventArgs e)
        {
            MarqueeProgressBarControl1.Properties.Stopped = true;
        }
    }
}
