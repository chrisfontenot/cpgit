namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    partial class SelectBatch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            SelectBatch_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_id;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_pull_date;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_effective_date;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_pull_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_pull_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_effective_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_effective_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(376, 14);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Select the ACH Batch from the following list of outstanding batches.";
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(0, 32);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(400, 144);
            this.GridControl1.TabIndex = 1;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((byte)170, (byte)216, (byte)254);
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((byte)170, (byte)216, (byte)254);
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((byte)236, (byte)246, (byte)255);
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((byte)247, (byte)251, (byte)255);
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((byte)247, (byte)251, (byte)255);
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((byte)236, (byte)246, (byte)255);
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((byte)59, (byte)133, (byte)195);
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((byte)38, (byte)109, (byte)189);
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((byte)59, (byte)139, (byte)206);
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((byte)170, (byte)216, (byte)254);
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((byte)170, (byte)216, (byte)254);
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((byte)236, (byte)246, (byte)255);
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((byte)170, (byte)216, (byte)254);
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((byte)170, (byte)216, (byte)254);
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((byte)139, (byte)201, (byte)254);
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((byte)139, (byte)201, (byte)254);
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((byte)105, (byte)170, (byte)225);
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((byte)83, (byte)155, (byte)215);
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((byte)236, (byte)246, (byte)255);
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((byte)236, (byte)246, (byte)255);
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((byte)236, (byte)246, (byte)255);
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((byte)83, (byte)155, (byte)215);
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((byte)247, (byte)251, (byte)255);
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((byte)236, (byte)246, (byte)255);
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((byte)83, (byte)155, (byte)215);
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((byte)104, (byte)184, (byte)251);
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
			this.GridColumn_id,
			this.GridColumn_pull_date,
			this.GridColumn_effective_date,
			this.GridColumn_date_created
		});
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_id, DevExpress.Data.ColumnSortOrder.Descending) });
            //
            //GridColumn_id
            //
            this.GridColumn_id.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_id.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_id.Caption = "ID";
            this.GridColumn_id.CustomizationCaption = "Batch Number";
            this.GridColumn_id.DisplayFormat.FormatString = "f0";
            this.GridColumn_id.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_id.FieldName = "item_key";
            this.GridColumn_id.GroupFormat.FormatString = "f0";
            this.GridColumn_id.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_id.Name = "GridColumn_id";
            this.GridColumn_id.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_id.Visible = true;
            this.GridColumn_id.VisibleIndex = 0;
            this.GridColumn_id.Width = 73;
            //
            //GridColumn_pull_date
            //
            this.GridColumn_pull_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_pull_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_pull_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_pull_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_pull_date.Caption = "Pull Date";
            this.GridColumn_pull_date.CustomizationCaption = "Pull Date(s)";
            this.GridColumn_pull_date.FieldName = "pull_date";
            this.GridColumn_pull_date.Name = "GridColumn_pull_date";
            this.GridColumn_pull_date.Visible = true;
            this.GridColumn_pull_date.VisibleIndex = 1;
            this.GridColumn_pull_date.Width = 107;
            //
            //GridColumn_effective_date
            //
            this.GridColumn_effective_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_effective_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_effective_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_effective_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_effective_date.Caption = "Effective";
            this.GridColumn_effective_date.CustomizationCaption = "Effective Date";
            this.GridColumn_effective_date.DisplayFormat.FormatString = "d";
            this.GridColumn_effective_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_effective_date.FieldName = "effective_entry_date";
            this.GridColumn_effective_date.GroupFormat.FormatString = "d";
            this.GridColumn_effective_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_effective_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth;
            this.GridColumn_effective_date.Name = "GridColumn_effective_date";
            this.GridColumn_effective_date.Visible = true;
            this.GridColumn_effective_date.VisibleIndex = 2;
            this.GridColumn_effective_date.Width = 107;
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_date_created.Caption = "Created";
            this.GridColumn_date_created.CustomizationCaption = "Date Created";
            this.GridColumn_date_created.DisplayFormat.FormatString = "d";
            this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.GroupFormat.FormatString = "d";
            this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth;
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            this.GridColumn_date_created.Visible = true;
            this.GridColumn_date_created.VisibleIndex = 3;
            this.GridColumn_date_created.Width = 109;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(115, 184);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.TabIndex = 2;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Location = new System.Drawing.Point(211, 184);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.TabIndex = 3;
            this.Button_Cancel.Text = "&Cancel";
            //
            //SelectBatch
            //
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.LabelControl1);
            this.Name = "SelectBatch";
            this.Size = new System.Drawing.Size(400, 224);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }
        #endregion
    }
}
