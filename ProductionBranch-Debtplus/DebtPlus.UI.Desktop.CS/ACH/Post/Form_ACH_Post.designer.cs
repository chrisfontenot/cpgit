﻿namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    partial class Form_ACH_Post
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.Results1 = new DebtPlus.UI.Desktop.CS.ACH.Post.Results();
            this.SelectBatch1 = new DebtPlus.UI.Desktop.CS.ACH.Post.SelectBatch();
            this.ProcessBatch1 = new DebtPlus.UI.Desktop.CS.ACH.Post.ProcessBatch();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // Results1
            // 
            this.Results1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Results1.Location = new System.Drawing.Point(0, 0);
            this.Results1.Name = "Results1";
            this.Results1.Size = new System.Drawing.Size(400, 266);
            this.Results1.TabIndex = 0;
            this.Results1.Visible = false;
            // 
            // SelectBatch1
            // 
            this.SelectBatch1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelectBatch1.Location = new System.Drawing.Point(0, 0);
            this.SelectBatch1.Name = "SelectBatch1";
            this.SelectBatch1.Size = new System.Drawing.Size(400, 266);
            this.SelectBatch1.TabIndex = 1;
            // 
            // ProcessBatch1
            // 
            this.ProcessBatch1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProcessBatch1.Location = new System.Drawing.Point(0, 0);
            this.ProcessBatch1.Name = "ProcessBatch1";
            this.ProcessBatch1.Size = new System.Drawing.Size(400, 266);
            this.ProcessBatch1.TabIndex = 2;
            this.ProcessBatch1.Visible = false;
            // 
            // Form_ACH_Post
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(400, 266);
            this.Controls.Add(this.SelectBatch1);
            this.Controls.Add(this.Results1);
            this.Controls.Add(this.ProcessBatch1);
            this.Name = "Form_ACH_Post";
            this.Text = "Post ACH Deposit Batch";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }
        internal DebtPlus.UI.Desktop.CS.ACH.Post.Results Results1;
        internal DebtPlus.UI.Desktop.CS.ACH.Post.SelectBatch SelectBatch1;
        internal DebtPlus.UI.Desktop.CS.ACH.Post.ProcessBatch ProcessBatch1;

        #endregion
    }
}