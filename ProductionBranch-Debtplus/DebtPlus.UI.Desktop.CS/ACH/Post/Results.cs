﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    public partial class Results : DevExpress.XtraEditors.XtraUserControl
    {
        public Form_ACH_Post parentForm;

        protected PostArgParser Globals
        {
            get
            {
                Form_ACH_Post frm = parentForm;
                System.Diagnostics.Debug.Assert(frm != null);
                return frm.ap;
            }
        }

        private void Results_Dispose(bool disposing)
        {
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        public Results()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Button_OK.Click += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Button_OK.Click -= Button_OK_Click;
        }

        public void Process()
        {
            // If the result does not have a deposit batch because it is not DMP then ignore the reports.
            if (Globals.trust_register == -1)
            {
                Formatted_BatchID.Text = "NOT APPLICABLE";
                check_letters.Checked = false;
                check_letters.Enabled = false;

                check_report.Checked = false;
                check_report.Enabled = false;
                return;
            }

            Formatted_BatchID.Text = string.Format("{0:d8}", Globals.trust_register);
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            // Generate the letters
            if (check_letters.Checked)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(LetterWriter)
                    {
                        Name = "letter_writer"
                    };
                thrd.SetApartmentState(System.Threading.ApartmentState.STA);
                thrd.Start();
            }

            // Generate the deposit batch report
            if (check_report.Checked)
            {
                System.Threading.Thread thrd = new System.Threading.Thread(ReportWriter)
                    {
                        Name = "report_writer"
                    };
                thrd.SetApartmentState(System.Threading.ApartmentState.STA);
                thrd.Start();
            }

            // Generate the close event for our form as if someone pressed "Cancel".
            ParentForm.Close();
        }

        private void LetterWriter()
        {
            DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            Int32 letters_printed = 0;
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_ACH_Reject_Letters";
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlCommandBuilder.DeriveParameters(cmd);
                        cmd.Parameters[1].Value = Globals.Batch;
                        cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);

                        using (SqlDataReader rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
                        {
                            while (rd.Read())
                            {
                                Int32 ClientID = 0;
                                string strCode = string.Empty;
                                Int32 Ordinal = rd.GetOrdinal("Client");
                                if (Ordinal >= 0)
                                {
                                    if (!rd.IsDBNull(Ordinal))
                                    {
                                        ClientID = Convert.ToInt32(rd.GetValue(Ordinal));
                                    }
                                }

                                Ordinal = rd.GetOrdinal("letter_code");
                                if (Ordinal >= 0)
                                {
                                    if (!rd.IsDBNull(Ordinal))
                                    {
                                        strCode = rd.GetString(Ordinal);
                                    }
                                }

                                if (ClientID > 0 && strCode != string.Empty)
                                {
                                    using (var ltr = new DebtPlus.Letters.Compose.Composition(strCode))
                                    {
                                        ((DebtPlus.Interfaces.Client.IClient)ltr).ClientId = ClientID;
                                        ltr.PrintLetter(false, true);
                                        letters_printed += 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error generating letters");
            }

            // Include the one status message indicating that the operation is complete
            if (letters_printed > 0)
            {
                string Message;
                if (letters_printed != 1)
                {
                    Message = string.Format("There were {0:n0} letters printed/queued.", letters_printed);
                }
                else
                {
                    Message = "There was 1 letter printed/queued.";
                }
                DebtPlus.Data.Forms.MessageBox.Show(Message, "Operation complete", System.Windows.Forms.MessageBoxButtons.OK);
            }
        }

        private void ReportWriter()
        {
            using (var rpt = new DebtPlus.Reports.Deposits.Batch.ByTrustRegister.DepositBatchReport())
            {
                rpt.Parameter_DepositBatchID = Globals.trust_register;
                if (rpt.RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
                {
                    rpt.DisplayPreviewDialog();
                }
            }
        }
    }
}
