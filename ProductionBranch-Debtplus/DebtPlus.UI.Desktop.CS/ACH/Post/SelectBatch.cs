#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    public partial class SelectBatch : DevExpress.XtraEditors.XtraUserControl
    {
        public event EventHandler Completed;
        public event EventHandler Cancelled;
        public Form_ACH_Post parentForm;

        protected PostArgParser Globals
        {
            get
            {
                Form_ACH_Post frm = parentForm;
                System.Diagnostics.Debug.Assert(frm != null);
                return frm.ap;
            }
        }

        private void SelectBatch_Dispose(bool disposing)
        {
            if (!DesignMode)
            {
                UnRegisterHanders();
            }
        }

        public SelectBatch()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Button_Cancel.Click += Button_Cancel_Click;
            Button_OK.Click += Button_OK_Click;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.MouseDown += GridView1_MouseDown;
        }

        private void UnRegisterHanders()
        {
            Button_Cancel.Click -= Button_Cancel_Click;
            Button_OK.Click -= Button_OK_Click;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            GridView1.DoubleClick -= GridView1_DoubleClick;
            GridView1.MouseDown -= GridView1_MouseDown;
        }


        /// <summary>
        /// Process the CANCEL button on the form
        /// </summary>
        private void Button_Cancel_Click(Object sender, EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }


        /// <summary>
        /// Process the OK button on the form
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            if (Completed != null)
            {
                Completed(this, EventArgs.Empty);
            }
        }

        System.Data.DataSet ds = new System.Data.DataSet("ds");

        /// <summary>
        /// Load the list of batches for the ACH files
        /// </summary>
        public void Process()
        {
            try
            {
                DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "lst_ACH_files_open";
                            cmd.CommandType = CommandType.StoredProcedure;

                            ds.Clear();
                            using (var da = new SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, "ach_files");

                                GridControl1.DataSource = ds.Tables["ach_files"].DefaultView;
                                GridControl1.RefreshDataSource();
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading ACH batch IDs");
            }
        }


        /// <summary>
        /// Process a change in the current trustRegister for the control
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            Int32 ControlRow = e.FocusedRowHandle;

            // Find the datarowview from the KeyField tables.
            DataRowView drv;
            if (ControlRow >= 0)
                drv = (DataRowView)gv.GetRow(ControlRow);
            else
                drv = null;

            // Ask the user to do something with the selected trustRegister
            Globals.Batch = -1;
            if (drv != null)
            {
                if (drv != null)
                {
                    Globals.Batch = DebtPlus.Utils.Nulls.DInt(drv["item_key"]);
                }
            }
            Button_OK.Enabled = (Globals.Batch > 0);
        }


        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the trustRegister targeted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            Int32 ControlRow = hi.RowHandle;

            // Find the datarowview from the KeyField tables.
            DataRowView drv;
            if (ControlRow >= 0)
                drv = (DataRowView)gv.GetRow(ControlRow);
            else
                drv = null;

            // If there is a trustRegister then select it
            Globals.Batch = -1;
            if (drv != null)
            {
                if (drv != null)
                {
                    Globals.Batch = DebtPlus.Utils.Nulls.DInt(drv["item_key"]);
                }
            }

            // If there is a batch then select it
            if (Globals.Batch > 0)
            {
                Button_OK.PerformClick();
            }
        }


        /// <summary>
        /// Handle a mouse click on the control
        /// </summary>
        private void GridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo(new Point(e.X, e.Y));

            // Remember the position for the popup menu handler.
            Int32 ControlRow;
            if (hi.IsValid && hi.InRow)
                ControlRow = hi.RowHandle;
            else
                ControlRow = -1;

            Globals.Batch = -1;
            if (ControlRow >= 0)
            {
                DataRowView drv = (DataRowView)GridView1.GetRow(ControlRow);
                if (drv != null)
                {
                    Globals.Batch = DebtPlus.Utils.Nulls.DInt(drv["item_key"]);
                }
                Button_OK.Enabled = (Globals.Batch > 0);
            }
        }
    }
}
