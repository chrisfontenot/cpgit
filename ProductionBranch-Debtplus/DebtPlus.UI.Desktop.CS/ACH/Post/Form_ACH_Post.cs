﻿#region Copyright 2000-2012 DebtPlus, L.L.C.
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    public partial class Form_ACH_Post : DebtPlus.Data.Forms.DebtPlusForm
    {
        public PostArgParser ap { get; set; }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="ap"></param>
        public Form_ACH_Post(PostArgParser ap)
            : this()
        {
            this.ap = ap;
        }

        /// <summary>
        /// Process the disposal for the form. Remove any storage allocaetd in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void DebtPlusForm1_Dispose(bool disposing)
        {
            // Remove the handlers so that the form may be properly destroyed
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
            ap = null;
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public Form_ACH_Post() : base()
        {
            InitializeComponent();

            // Set the parent linkage to our form.
            this.ProcessBatch1.parentForm = this;
            this.Results1.parentForm = this;
            this.SelectBatch1.parentForm = this;

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            SelectBatch1.Cancelled += SelectBatch1_Cancelled;
            SelectBatch1.Completed += SelectBatch1_Completed;
            ProcessBatch1.Completed += ProcessBatch1_Completed;
            Load += DebtPlusForm1_Load;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            SelectBatch1.Cancelled -= SelectBatch1_Cancelled;
            SelectBatch1.Completed -= SelectBatch1_Completed;
            ProcessBatch1.Completed -= ProcessBatch1_Completed;
            Load -= DebtPlusForm1_Load;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DebtPlusForm1_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                SelectBatch1.Visible = true;
                SelectBatch1.Enabled = true;
                SelectBatch1.Process();

                ProcessBatch1.Visible = false;
                ProcessBatch1.Enabled = false;

                Results1.Visible = false;
                Results1.Enabled = false;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void SelectBatch1_Completed(object sender, EventArgs e)
        {
            SelectBatch1.Visible = false;
            SelectBatch1.Enabled = false;

            Results1.Visible = false;
            Results1.Enabled = false;

            ProcessBatch1.Visible = true;
            ProcessBatch1.Enabled = true;
            ProcessBatch1.Process();
        }

        private void SelectBatch1_Cancelled(object sender, EventArgs e)
        {
            Close();
        }

        private void ProcessBatch1_Completed(object sender, EventArgs e)
        {
            SelectBatch1.Visible = false;
            SelectBatch1.Enabled = false;

            ProcessBatch1.Visible = false;
            ProcessBatch1.Enabled = false;

            Results1.Visible = true;
            Results1.Enabled = true;
            Results1.Process();
        }
    }
}