﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    /// <summary>
    /// Class to parse the argument strings
    /// </summary>
    public class PostArgParser : DebtPlus.Utils.ArgParserBase
    {
        public PostArgParser() : base(new string[] { "" }) { }
        public Int32 Batch = -1;
        public Int32 trust_register = -1;

        /// <summary>
        /// Global items for the procedure
        /// </summary>
        private System.Int32 privateExisting_RequireClient = 0;

        /// <summary>
        /// Does the existing tab require an existing client?
        /// </summary>
        public bool Existing_RequireClient()
        {
            if (privateExisting_RequireClient == 0)
            {
                string strItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.FaqDetail), "Existing.RequireClient");
                bool ItemValue;
                if (!bool.TryParse(strItem, out ItemValue))
                {
                    ItemValue = true;
                }
                privateExisting_RequireClient = ItemValue ? 1 : 2;
            }
            return privateExisting_RequireClient == 1;
        }

        /// <summary>
        /// Handle a switch and its value
        /// </summary>
        protected override DebtPlus.Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            DebtPlus.Utils.ArgParserBase.SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;
                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Define the usage of the program.
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}\r\n", errorInfo));
            }

            System.Reflection.Assembly _asm = System.Reflection.Assembly.GetExecutingAssembly();
            string fname = System.IO.Path.GetFileName(_asm.CodeBase);
            AppendErrorLine("Usage: " + fname);
        }
    }

    public class Mainline : DebtPlus.Interfaces.Desktop.IDesktopMainline //, DebtPlus.Interfaces.Desktop.ISupportCreateClientRequest
    {
        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new PostArgParser();
                    if (ap.Parse((string[])param))
                    {
                        var frm = new Form_ACH_Post(ap);
                        System.Windows.Forms.Application.Run(frm);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "ACHPost"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}
