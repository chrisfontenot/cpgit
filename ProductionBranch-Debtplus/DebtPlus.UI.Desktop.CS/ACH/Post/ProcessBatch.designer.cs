﻿using DebtPlus;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    partial class ProcessBatch
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            ProcessBatch_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.MarqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //MarqueeProgressBarControl1
            //
            this.MarqueeProgressBarControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.MarqueeProgressBarControl1.EditValue = 0;
            this.MarqueeProgressBarControl1.Location = new System.Drawing.Point(8, 64);
            this.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1";
            this.MarqueeProgressBarControl1.Properties.Stopped = true;
            this.MarqueeProgressBarControl1.Size = new System.Drawing.Size(384, 18);
            this.MarqueeProgressBarControl1.TabIndex = 0;
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl1.Location = new System.Drawing.Point(8, 24);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(233, 13);
            this.LabelControl1.TabIndex = 1;
            this.LabelControl1.Text = "We are currently posting the batch. Please wait.";
            //
            //ProcessBatch
            //
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.MarqueeProgressBarControl1);
            this.Name = "ProcessBatch";
            this.Size = new System.Drawing.Size(400, 224);
            ((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        internal DevExpress.XtraEditors.MarqueeProgressBarControl MarqueeProgressBarControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        #endregion
    }
}
