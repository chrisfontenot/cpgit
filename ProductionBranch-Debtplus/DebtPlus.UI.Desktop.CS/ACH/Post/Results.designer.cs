﻿namespace DebtPlus.UI.Desktop.CS.ACH.Post
{
    partial class Results
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            Results_Dispose(disposing);
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl Formatted_BatchID;
        internal DevExpress.XtraEditors.CheckEdit check_report;
        internal DevExpress.XtraEditors.CheckEdit check_letters;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.Formatted_BatchID = new DevExpress.XtraEditors.LabelControl();
            this.check_report = new DevExpress.XtraEditors.CheckEdit();
            this.check_letters = new DevExpress.XtraEditors.CheckEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.check_report.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.check_letters.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(376, 14);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "The ACH information has been posted to the account.";
            this.LabelControl1.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 40);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(84, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Deposit Batch ID:";
            this.LabelControl2.UseMnemonic = false;
            //
            //Formatted_BatchID
            //
            this.Formatted_BatchID.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, (byte)0);
            this.Formatted_BatchID.Appearance.Options.UseFont = true;
            this.Formatted_BatchID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.Formatted_BatchID.Location = new System.Drawing.Point(104, 32);
            this.Formatted_BatchID.Name = "Formatted_BatchID";
            this.Formatted_BatchID.Size = new System.Drawing.Size(96, 23);
            this.Formatted_BatchID.TabIndex = 2;
            this.Formatted_BatchID.Text = "00000000";
            this.Formatted_BatchID.UseMnemonic = false;
            //
            //check_report
            //
            this.check_report.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.check_report.EditValue = true;
            this.check_report.Location = new System.Drawing.Point(8, 64);
            this.check_report.Name = "check_report";
            //
            //check_report.Properties
            //
            this.check_report.Properties.Caption = "&Print a deposit report for the items";
            this.check_report.Size = new System.Drawing.Size(384, 19);
            this.check_report.TabIndex = 3;
            //
            //check_letters
            //
            this.check_letters.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.check_letters.EditValue = true;
            this.check_letters.Location = new System.Drawing.Point(8, 88);
            this.check_letters.Name = "check_letters";
            //
            //check_letters.Properties
            //
            this.check_letters.Properties.Caption = "Generate Client &letters for failed transactions in this batch";
            this.check_letters.Size = new System.Drawing.Size(384, 19);
            this.check_letters.TabIndex = 4;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(163, 187);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.TabIndex = 5;
            this.Button_OK.Text = "OK";
            //
            //Results
            //
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.check_letters);
            this.Controls.Add(this.check_report);
            this.Controls.Add(this.Formatted_BatchID);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Results";
            this.Size = new System.Drawing.Size(400, 224);
            ((System.ComponentModel.ISupportInitialize)this.check_report.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.check_letters.Properties).EndInit();
            this.ResumeLayout(false);
        }
    }
}
