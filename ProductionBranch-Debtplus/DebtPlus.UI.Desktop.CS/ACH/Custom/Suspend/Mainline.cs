#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections;
using System.Windows.Forms;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.ACH.Custom.Suspend
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() : base()
        {
        }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    ArgParser ap = new ArgParser();
                    if (ap.Parse((string[])param))
                    {
                        if (ap.Success)
                        {
                            IEnumerator ie = ap.GetPathnameEnumerator();
                            while (ie.MoveNext())
                            {
                                Sucess clx = new Sucess();
                                clx.ProcessSuccess(Convert.ToString(ie.Current));
                            }
                        }
                        else
                        {
                            IEnumerator ie = ap.GetPathnameEnumerator();
                            while (ie.MoveNext())
                            {
                                Suspend clx = new Suspend();
                                clx.ProcessSuspend(Convert.ToString(ie.Current));
                            }
                        }
                        DebtPlus.Data.Forms.MessageBox.Show("All of the valid files have been processed.", "End of operation", MessageBoxButtons.OK);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "ACHCustomSuspend"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}