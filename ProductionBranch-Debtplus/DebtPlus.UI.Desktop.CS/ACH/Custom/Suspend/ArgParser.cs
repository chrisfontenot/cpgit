using System;
using System.Collections;
using System.IO;
using System.Security;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.ACH.Custom.Suspend
{
    public partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        public ArrayList pathnames = new ArrayList();

        /// <summary>
        /// TRUE if process the success transaction files
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Process an option
        /// </summary>
        protected override Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "s":
                    Success = true;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }

            return ss;
        }

        /// <summary>
        /// Input file to be read
        /// </summary>
        protected override SwitchStatus OnNonSwitch(string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            // Add command-line argument to array of pathnames.
            // Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            try
            {
                string d = Path.GetDirectoryName(switchValue);
                DirectoryInfo dir = null;
                if ((d.Length == 0))
                {
                    dir = new DirectoryInfo(".");
                }
                else
                {
                    dir = new DirectoryInfo(d);
                }

                FileInfo f = null;
                foreach (FileInfo f_loopVariable in dir.GetFiles(Path.GetFileName(switchValue)))
                {
                    f = f_loopVariable;
                    pathnames.Add(f.FullName);
                }
            }
#pragma warning disable 168
            catch (SecurityException SecEx)
            {
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }
#pragma warning restore 168

            if (pathnames.Count == 0)
            {
                AppendErrorLine("None of the specified files exists.");
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }

            return ss;
        }

        //OnNonSwitch

        /// <summary>
        /// Ensure that we have a file to process
        /// </summary>
        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = base.OnDoneParse();
            if (ss == global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError && pathnames.Count == 0)
            {
                using (var dlg = new OpenFileDialog())
                {
                    // Define the options for the dialog
                    dlg.AddExtension = true;
                    dlg.AutoUpgradeEnabled = true;
                    dlg.CheckFileExists = true;
                    dlg.CheckPathExists = true;
                    dlg.DereferenceLinks = true;
                    dlg.Multiselect = true;
                    dlg.ReadOnlyChecked = true;
                    dlg.ShowReadOnly = false;
                    dlg.SupportMultiDottedExtensions = true;
                    dlg.ValidateNames = true;
                    dlg.DefaultExt = ".xlsx";
                    dlg.Filter = "Excel Files (*.xls;*.xlsx)|*.xls;*.xlsx";
                    dlg.FilterIndex = 0;
                    dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    dlg.Title = "Open Excel Document(s)";

                    // Run the dialog. If not OK then cancel.
                    if (dlg.ShowDialog() != DialogResult.OK)
                    {
                        ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                    }
                    else
                    {
                        // Add the names selected in the input dialog box
                        foreach (string Name in dlg.FileNames)
                        {
                            pathnames.Add(Path.GetFullPath(Name));
                        }

                        // If there are still no names then reject the program.
                        if (pathnames.Count <= 0)
                        {
                            DebtPlus.Data.Forms.MessageBox.Show("You did not select any files. Please try the operation again and choose the input file.", "Sorry, but no files selected", MessageBoxButtons.OK);
                            ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                        }
                    }
                }
            }

            return ss;
        }

        /// <summary>
        /// Return the list of input file name
        /// </summary>
        public IEnumerator GetPathnameEnumerator()
        {
            return pathnames.GetEnumerator(0, pathnames.Count);
        }

        //GetPathnameEnumerator

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] { "s" })
        {
        }

        public ArgParser(string[] switchSymbols) : base(switchSymbols)
        {
        }

        public ArgParser(string[] switchSymbols, bool caseSensitiveSwitches) : base(switchSymbols, caseSensitiveSwitches)
        {
        }

        public ArgParser(string[] switchSymbols, bool caseSensitiveSwitches, string[] switchChars) : base(switchSymbols, caseSensitiveSwitches, switchChars)
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}