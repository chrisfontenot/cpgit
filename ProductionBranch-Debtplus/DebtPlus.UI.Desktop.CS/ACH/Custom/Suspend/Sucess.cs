using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.ACH.Custom.Suspend
{
    public partial class Sucess
    {
        public Sucess() : base()
        {
        }

        public void ProcessSuccess(string Filename)
        {
            System.Data.DataSet ds = null;
            try
            {
                using (System.IO.FileStream fs = new System.IO.FileStream(Filename, System.IO.FileMode.Open))
                {
                    if (System.IO.Path.GetExtension(Filename).ToLower() == ".xlsx")
                    {
                        using (Excel.IExcelDataReader rdr = Excel.ExcelReaderFactory.CreateOpenXmlReader(fs))
                        {
                            rdr.IsFirstRowAsColumnNames = false;
                            ds = rdr.AsDataSet(true);
                        }
                    }
                    else
                    {
                        using (Excel.IExcelDataReader rdr = Excel.ExcelReaderFactory.CreateBinaryReader(fs))
                        {
                            rdr.IsFirstRowAsColumnNames = false;
                            ds = rdr.AsDataSet(true);
                            if (rdr.ExceptionMessage != string.Empty)
                            {
                                DebtPlus.Data.Forms.MessageBox.Show(string.Format("{0}{1}{1}The file can not be read. It is probably too old of an Excel format for use.{1}Please convert it to Office 2007 format (the ones that have an extension{1}of XLSX rather than XLS).", rdr.ExceptionMessage, Environment.NewLine), "Error processing file", MessageBoxButtons.OK);
                                return;
                            }
                        }
                    }
                }

                // Key to the execution
                System.Guid itemGuid = System.Guid.NewGuid();

                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable tbl = ds.Tables[0];

                    // Generate the key id
                    System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                    System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

                    cn.Open();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_FirstNetSuccess";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BatchId", SqlDbType.UniqueIdentifier).Value = itemGuid;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 80, tbl.Columns[0].ColumnName);
                    cmd.Parameters.Add("@Client", SqlDbType.VarChar, 80, tbl.Columns[1].ColumnName);

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                    {
                        da.InsertCommand = cmd;
                        da.Update(tbl);
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error processing file");
            }
        }
    }
}