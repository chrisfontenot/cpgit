using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.ACH.Custom.Suspend
{
    public partial class Suspend
    {
        public Suspend() : base()
        {
        }

        /// <summary>
        /// Process the suspension file.
        /// </summary>
        /// <param name="Filename">Name of the input Excel document</param>
        public void ProcessSuspend(string Filename)
        {
            try
            {
                // Look for the newer ZIPPED archive EXCEL XML format
                if (System.IO.Path.GetExtension(Filename).ToLower() == ".xlsx")
                {
                    using (System.IO.FileStream fs = new System.IO.FileStream(Filename, System.IO.FileMode.Open))
                    {
                        using (Excel.IExcelDataReader rdr = Excel.ExcelReaderFactory.CreateOpenXmlReader(fs))
                        {
                            rdr.IsFirstRowAsColumnNames = false;
                            System.Data.DataSet ds = rdr.AsDataSet(true);
                            if (ds == null && rdr.ExceptionMessage != null)
                            {
                                DebtPlus.Data.Forms.MessageBox.Show(rdr.ExceptionMessage, "Error reading Excel File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }

                            if (ds != null && ds.Tables.Count > 0)
                            {
                                ProcessInputDataTable(ds.Tables[0]);
                            }
                        }
                    }
                    return;
                }

                // Look for the older binary EXCEL format
                using (System.IO.FileStream fs = new System.IO.FileStream(Filename, System.IO.FileMode.Open))
                {
                    using (Excel.IExcelDataReader rdr = Excel.ExcelReaderFactory.CreateBinaryReader(fs))
                    {
                        rdr.IsFirstRowAsColumnNames = false;
                        System.Data.DataSet ds = rdr.AsDataSet(true);
                        if (ds == null && rdr.ExceptionMessage != null)
                        {
                            DebtPlus.Data.Forms.MessageBox.Show(rdr.ExceptionMessage, "Error reading Excel File", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        if (ds != null && ds.Tables.Count > 0)
                        {
                            ProcessInputDataTable(ds.Tables[0]);
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                // Process the error conditions that occurred during the conversion
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing file");
            }
        }

        /// <summary>
        /// Convert the input DataTable to the corresponding output dataset and submit the changes.
        /// </summary>
        /// <param name="tbl"></param>
        private void ProcessInputDataTable(System.Data.DataTable tbl)
        {
            Int32 RowCount = tbl.Rows.Count;

            // Populate the table column names with the headings from the spreadsheet
            System.Data.DataTable outputTbl = new System.Data.DataTable("outputTbl");
            System.Data.DataRow row = tbl.Rows[1];

            for (Int32 col = 0; col <= tbl.Columns.Count - 1; col++)
            {
                outputTbl.Columns.Add(Convert.ToString(row[col]), typeof(string));
            }

            // Correct the non-string fields with the appropriate type
            outputTbl.Columns["MaintDate"].DataType = typeof(DateTime);
            outputTbl.Columns["RestartDate"].DataType = typeof(DateTime);
            outputTbl.Columns["Client"].DataType = typeof(Int32);
            outputTbl.Columns["Amount"].DataType = typeof(decimal);

            // Add the data to the table
            for (Int32 RowNumber = 2; RowNumber <= RowCount - 1; RowNumber++)
            {
                System.Data.DataRow NewRow = outputTbl.NewRow();
                if (GenerateOuputRow(ref NewRow, tbl.Rows[RowNumber]))
                {
                    outputTbl.Rows.Add(NewRow);
                }
            }

            // Submit the changes to the dataset when we have the data complete.
            if (outputTbl.Rows.Count > 0)
            {
                SubmitOutputDataTable(outputTbl);
            }
        }

        /// <summary>
        /// Generate the output row from the input data row. The fields are converted to the proper form and put into the output
        /// to be updated on the database.
        /// </summary>
        /// <param name="OutputRow">Pointer to the DataRow for the output row.</param>
        /// <param name="InputRow">Input row with the current values</param>
        /// <returns></returns>
        private bool GenerateOuputRow(ref System.Data.DataRow OutputRow, System.Data.DataRow InputRow)
        {
            System.Data.DataTable tbl = InputRow.Table;
            bool hasSomeData = false;

            // Process each column in the spreadsheet for its value
            foreach (System.Data.DataColumn col in tbl.Columns)
            {
                string ColName = Convert.ToString(tbl.Rows[1][col]);
                object Value = InputRow[col];

                // The value should not be null.
                if (Value == null || object.ReferenceEquals(Value, DBNull.Value))
                {
                    continue;
                }

                // The value should not be an empty string.
                if (Value is string && string.IsNullOrWhiteSpace((string)Value))
                {
                    continue;
                }

                // Indicate that we have some data in the row. It is now an acceptable row.
                hasSomeData = true;

                // These columns are special. Ensure that they are valid items
                // The client id
                if (String.Compare(ColName, "Client", true) == 0)
                {
                    Int32 Result = default(Int32);
                    if (!Int32.TryParse(Convert.ToString(Value), out Result))
                    {
                        return false;
                    }
                    OutputRow[ColName] = Result;
                    continue;
                }

                // The two datetime dates
                if (ColName.EndsWith("Date", StringComparison.InvariantCultureIgnoreCase))
                {
                    DateTime Result = new DateTime(1980,1,1);
                    if (!DateTime.TryParse(Convert.ToString(Value), out Result))
                    {
                        return false;
                    }
                    OutputRow[ColName] = Result;
                    continue;
                }

                // And the amount field
                if (String.Compare(ColName, "Amount", true) == 0)
                {
                    decimal Result = 0m;
                    if (!decimal.TryParse(Convert.ToString(Value), out Result))
                    {
                        return false;
                    }
                    OutputRow[ColName] = Result;
                    continue;
                }

                // Update the resulting row with the value
                OutputRow[ColName] = Value;
            }

            // All is well with the row if the row is not totally empty
            return hasSomeData;
        }

        /// <summary>
        /// Submit the changes to the database
        /// </summary>
        /// <param name="outputTbl">Pointer to the table with the results to be submitted</param>
        private void SubmitOutputDataTable(System.Data.DataTable outputTbl)
        {
            // Generate the key id
            using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_FirstNetStopSuspend";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add("@BatchId", System.Data.SqlDbType.UniqueIdentifier, 8).Value = System.Guid.NewGuid();
                    cmd.Parameters.Add("@CompanyN", System.Data.SqlDbType.VarChar, 80, "CompanyN");
                    cmd.Parameters.Add("@LastName", System.Data.SqlDbType.VarChar, 80, "LastName");
                    cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar, 80, "FirstName");
                    cmd.Parameters.Add("@client", System.Data.SqlDbType.Int, 4, "client");
                    cmd.Parameters.Add("@MaintDate", System.Data.SqlDbType.DateTime, 8, "MaintDate");
                    cmd.Parameters.Add("@PullDates", System.Data.SqlDbType.VarChar, 80, "PullDates");
                    cmd.Parameters.Add("@Status", System.Data.SqlDbType.VarChar, 80, "Status");
                    cmd.Parameters.Add("@Amount", System.Data.SqlDbType.Decimal, 8, "Amount");
                    cmd.Parameters.Add("@RestartDate", System.Data.SqlDbType.DateTime, 8, "RestartDate");
                    cmd.Parameters.Add("@RoutingNum", System.Data.SqlDbType.VarChar, 80, "RoutingNum");
                    cmd.Parameters.Add("@AccountNum", System.Data.SqlDbType.VarChar, 80, "AccountNum");

                    using (var da = new System.Data.SqlClient.SqlDataAdapter())
                    {
                        da.InsertCommand = cmd;
                        da.Update(outputTbl);
                    }
                }
            }
        }
    }
}