﻿using System;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    public partial class XtraUserControl_Other : XtraUserControl_Memo
    {
        public XtraUserControl_Other()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Other_Disposition.SelectedIndexChanged += new EventHandler(FormChanged);
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Other_Disposition.SelectedIndexChanged -= new EventHandler(FormChanged);
        }

        /// <summary>
        /// Process the initial loading of this control. Define the data in the lookup fields.
        /// </summary>
        public override void LoadHandler()
        {
            UnRegisterHandlers();
            try
            {
                base.LoadHandler();

                // Load the other dispositions
                if (Other_Disposition.Properties.Items.Count == 0)
                {
                    LoadList(ref Other_Disposition, "OTHER DISPOSITION");
                    if (Other_Disposition.Properties.Items.Count == 0)
                    {
                        LoadList(ref Other_Disposition, "DISPOSITION");
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }

            // Enable or disable the OK button based upon the form settings.
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        private void FormChanged(object sender, EventArgs e)
        {
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        private bool OkEnabled()
        {
            return Other_Disposition.SelectedIndex >= 0;
        }

        /// <summary>
        /// Reset the entry information on this form
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            Other_Disposition.SelectedIndex = -1;
            Button_Apply.Enabled = false;
            Button_OK.Enabled = false;
        }

        /// <summary>
        /// The APPLY button was pressed. Generate the new row with the correct information.
        /// </summary>
        protected override void RequestRecordEvent()
        {
            // Build the detail row with the parameters
            DebtPlus.LINQ.faq_detail detail = new DebtPlus.LINQ.faq_detail()
            {
                grouping = 3,   // other
                note = Edit_Notes.Text.Trim(),
                disposition = (Int32)((DebtPlus.Data.Controls.ComboboxItem) Other_Disposition.SelectedItem).value
            };

            using (var dc = new BusinessContext())
            {
                dc.faq_details.InsertOnSubmit(detail);
                dc.SubmitChanges();
            }

            base.RequestRecordEvent();
        }
    }
}
