﻿namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    partial class XtraUserControl_OldClient
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraUserControl_OldClient));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.ExistingClient_Client = new DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.ExistingClient_Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ExistingClient_Reason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ExistingClient_Disposition = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Client.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Reason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Disposition.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl_Note.TabIndex = 8;
            // 
            // Edit_Notes
            // 
            this.Edit_Notes.TabIndex = 9;
            // 
            // Button_OK
            // 
            this.Button_OK.TabIndex = 10;
            // 
            // Button_Apply
            // 
            this.Button_Apply.TabIndex = 11;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.TabIndex = 12;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(4, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(41, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Client ID";
            // 
            // ExistingClient_Client
            // 
            this.ExistingClient_Client.AllowDrop = true;
            this.ExistingClient_Client.Location = new System.Drawing.Point(94, 3);
            this.ExistingClient_Client.Name = "ExistingClient_Client";
            this.ExistingClient_Client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.ExistingClient_Client.Properties.Appearance.Options.UseTextOptions = true;
            this.ExistingClient_Client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ExistingClient_Client.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DebtPlus.UI.Common.Controls.SearchButton()});
            this.ExistingClient_Client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ExistingClient_Client.Properties.DisplayFormat.FormatString = "0000000";
            this.ExistingClient_Client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ExistingClient_Client.Properties.EditFormat.FormatString = "f0";
            this.ExistingClient_Client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ExistingClient_Client.Properties.Mask.BeepOnError = true;
            this.ExistingClient_Client.Properties.Mask.EditMask = "\\d*";
            this.ExistingClient_Client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.ExistingClient_Client.Properties.ValidateOnEnterKey = true;
            this.ExistingClient_Client.Size = new System.Drawing.Size(100, 20);
            this.ExistingClient_Client.TabIndex = 1;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(4, 57);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(24, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Type";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(4, 82);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(26, 13);
            this.labelControl4.TabIndex = 4;
            this.labelControl4.Text = "Issue";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(4, 132);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(51, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Disposition";
            // 
            // ExistingClient_Type
            // 
            this.ExistingClient_Type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ExistingClient_Type.EditValue = "";
            this.ExistingClient_Type.Location = new System.Drawing.Point(94, 53);
            this.ExistingClient_Type.Name = "ExistingClient_Type";
            this.ExistingClient_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExistingClient_Type.Properties.PopupSizeable = true;
            this.ExistingClient_Type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ExistingClient_Type.Size = new System.Drawing.Size(328, 20);
            this.ExistingClient_Type.TabIndex = 3;
            // 
            // ExistingClient_Reason
            // 
            this.ExistingClient_Reason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ExistingClient_Reason.EditValue = "";
            this.ExistingClient_Reason.Location = new System.Drawing.Point(94, 78);
            this.ExistingClient_Reason.Name = "ExistingClient_Reason";
            this.ExistingClient_Reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExistingClient_Reason.Properties.PopupSizeable = true;
            this.ExistingClient_Reason.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ExistingClient_Reason.Size = new System.Drawing.Size(328, 20);
            this.ExistingClient_Reason.TabIndex = 5;
            // 
            // ExistingClient_Disposition
            // 
            this.ExistingClient_Disposition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ExistingClient_Disposition.EditValue = "";
            this.ExistingClient_Disposition.Location = new System.Drawing.Point(94, 128);
            this.ExistingClient_Disposition.Name = "ExistingClient_Disposition";
            this.ExistingClient_Disposition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ExistingClient_Disposition.Properties.PopupSizeable = true;
            this.ExistingClient_Disposition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ExistingClient_Disposition.Size = new System.Drawing.Size(328, 20);
            this.ExistingClient_Disposition.TabIndex = 7;
            // 
            // XtraUserControl_OldClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.ExistingClient_Client);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.ExistingClient_Type);
            this.Controls.Add(this.ExistingClient_Reason);
            this.Controls.Add(this.ExistingClient_Disposition);
            this.Name = "XtraUserControl_OldClient";
            this.Controls.SetChildIndex(this.ExistingClient_Disposition, 0);
            this.Controls.SetChildIndex(this.ExistingClient_Reason, 0);
            this.Controls.SetChildIndex(this.ExistingClient_Type, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.ExistingClient_Client, 0);
            this.Controls.SetChildIndex(this.labelControl_Note, 0);
            this.Controls.SetChildIndex(this.Edit_Notes, 0);
            this.Controls.SetChildIndex(this.Button_OK, 0);
            this.Controls.SetChildIndex(this.Button_Apply, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Client.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Reason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExistingClient_Disposition.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        protected internal DevExpress.XtraEditors.LabelControl labelControl2;
        protected internal DebtPlus.UI.Client.Widgets.Controls.ClientID ExistingClient_Client;
        protected internal DevExpress.XtraEditors.LabelControl labelControl3;
        protected internal DevExpress.XtraEditors.LabelControl labelControl4;
        protected internal DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit ExistingClient_Type;
        private DevExpress.XtraEditors.ComboBoxEdit ExistingClient_Reason;
        private DevExpress.XtraEditors.ComboBoxEdit ExistingClient_Disposition;
    }
}
