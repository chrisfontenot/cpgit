﻿using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.LINQ;
using System.Linq;
using System.Text.RegularExpressions;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    public partial class XtraUserControl_Notices : XtraUserControl_Base
    {
        public XtraUserControl_Notices()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            lookUpEdit_StateID.EditValueChanged += lookUpEdit_StateID_EditValueChanged;
            lookUpEdit_StateID.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            lookUpEdit_StateID.EditValueChanged -= lookUpEdit_StateID_EditValueChanged;
            lookUpEdit_StateID.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
        }

        /// <summary>
        /// Process the initial loading of this control. Define the data in the lookup fields.
        /// </summary>
        public override void LoadHandler()
        {
            base.LoadHandler();

            UnRegisterHandlers();
            try
            {
                // Load the states list and reset the selected item
                lookUpEdit_StateID.Properties.DataSource = DebtPlus.LINQ.Cache.state.getList();
                lookUpEdit_StateID.EditValue = null;
                
                // Clear the notice field
                richEditControl1.Text = string.Empty;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle a change in the state identifier
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lookUpEdit_StateID_EditValueChanged(object sender, EventArgs e)
        {
            // Find the state ID to display
            Int32 stateToDisplay = DebtPlus.Utils.Nulls.DInt(lookUpEdit_StateID.EditValue);
            if (stateToDisplay <= 0)
            {
                return;
            }

            // Find the name of the state from the states table
            DebtPlus.LINQ.state newState = DebtPlus.LINQ.Cache.state.getList().Where(s => s.Id == stateToDisplay).FirstOrDefault();
            if (newState != null)
            {
                richEditControl1.HtmlText = GetDisplayText(stateToDisplay, newState.Name);
            }
        }

        /// <summary>
        /// Retrieve the current state notice text
        /// </summary>
        private string GetDisplayText(Int32 state, string DisplayedState)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            using(BusinessContext dc = new BusinessContext())
            {
                try
                {
                    System.Collections.Generic.List<xpr_StateNotices_SelectResult> colResults = dc.xpr_StateNotices_Select(state, null).ToList();
                    System.Collections.Generic.List<xpr_StateNotices_SelectResult>.Enumerator ieRslt = colResults.GetEnumerator();
                    while(ieRslt.MoveNext())
                    {
                        xpr_StateNotices_SelectResult rslt = ieRslt.Current;

                        DebtPlus.LINQ.StateMessageType StateType = DebtPlus.LINQ.Cache.StateMesageType.getList().Find(f => f.Id == rslt.StateMessageType);
                        if (StateType != null)
                        {
                            // Start with the message type
                            sb.Append ("<tr>");

                            // Append the section name as a bold field across both columns so that it is out-dented
                            sb.Append ("<td colspan=\"2\" align=\"left\" valign=\"top\">");

                            sb.Append ("<b>");
                            sb.Append (ToHTML(StateType.Description));
                            if (rslt.effective.HasValue)
                            {
                                sb.AppendFormat(" (effective {0:d})", rslt.effective.Value);
                            }

                            sb.Append("</b>");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                        }

                        // Append the message text in a section that will wrap while it is indented
                        sb.Append("<tr>");
                        sb.Append("<td>&nbsp;</td>");

                        sb.Append("<td align=\"left\" valign=\"top\">");

                        string MessageString = rslt.Details;
                        if (System.String.IsNullOrEmpty(MessageString))
                        {
                            MessageString = "Nothing was mentioned";
                        }
                        sb.Append(ToHTML(MessageString));

                        sb.Append("</td>");
                        sb.Append("</tr>");

                        // Add a blank row between sections
                        sb.Append("<tr colspan=\"2\"><td><font size=\"1\">&nbsp;</font></td></tr>");
                    }

                    // If there is text then wrap it with the proper table definition.
                    if (sb.Length > 0)
                    {
                        sb.Insert(0, "<table width=\"100%\">");
                        sb.Append("</table>");
                    }
                }
                catch(SqlException ex)
                {
                    using(Repository.GetDataResult gdr = new Repository.GetDataResult())
                    {
                        gdr.HandleException(ex);
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading note text");
                    }
                }

                // If there are no notices then say so. Just don't leave the area blank.
                if (sb.Length == 0)
                {
                    sb.Append("There are no notices for the client's state at this time.");
                }

                // Include the state name as the first item shown.
                if (! System.String.IsNullOrEmpty(DisplayedState))
                {
                    sb.Insert(0, System.String.Format("<font size=\"5\"><b>{0}</b></font><br/>", sbHTML(DisplayedState).ToString()));
                }

                // Wrap the text string with a valid HTML document header and body.
                sb.Insert(0, "<html><head><title></title></head><body>");
                sb.Append("</body></html>");

                return sb.ToString();
            }
        }

        private System.Text.StringBuilder sbHTML(string InputString)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder(InputString);

            sb.Replace("&", "&amp;");
            sb.Replace("<", "&lt;");
            sb.Replace(">", "&gt;");
            sb.Replace("\"", "&quot;");

            sb.Replace("\r\n", "<br/>");

            return sb;
        }

        private string ToHTML(string InputString)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder(InputString);

            // Change the font to size 3
            sb.Insert(0, "<font size=\"3\">");
            sb.Append("</font>");
            sb.Replace("\r\n", "<br>");

            string OutputString = Regex.Replace(sb.ToString(), @"(<br>)+", "<br>");

            return OutputString;
        }
    }
}
