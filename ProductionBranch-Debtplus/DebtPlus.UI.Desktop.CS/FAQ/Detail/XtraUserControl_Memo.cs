﻿using System;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    public partial class XtraUserControl_Memo : XtraUserControl_Base
    {
        public XtraUserControl_Memo()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            // There are none
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            // There are none
        }

        /// <summary>
        /// Reset the entry information on this form
        /// </summary>
        public override void Reset()
        {
            // Clear the base form information
            base.Reset();

            // Clear the note field.
            Edit_Notes.Text = string.Empty;
        }

        /// <summary>
        /// Load the list of items into the combo-box control given the tag
        /// </summary>
        /// <param name="ctl"></param>
        /// <param name="TagID"></param>
        protected void LoadList(ref DevExpress.XtraEditors.ComboBoxEdit ctl, String TagID)
        {
            ctl.Properties.Items.Clear();
            foreach (faq_query_item item in (from i in Cache.faq_item.getList() where i.grouping == TagID orderby i.description select i))
            {
                var boxItem = new DebtPlus.Data.Controls.ComboboxItem(item.description, item.Id, true);
                boxItem.tag = item.attributes;
                ctl.Properties.Items.Add(boxItem);
            }
        }
    }
}
