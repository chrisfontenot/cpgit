﻿using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    public partial class Form_CallTracking : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Process the disposal for the form. Remove any storage allocated in the form.
        /// </summary>
        /// <param name="disposing">TRUE when Dispose() is called. FALSE for Finalize.</param>
        private void Form_CallTracking_Dispose(bool disposing)
        {
            // Remove the handlers so that the form may be properly destroyed
            if (!DesignMode)
            {
                UnRegisterHandlers();
            }
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="DatabaseInfo">Pointer to the database connection object</param>
        public Form_CallTracking() : base()
        {
            InitializeComponent();
        }

        internal ArgParser ap;
        public Form_CallTracking(ArgParser ap) : this()
        {
            this.ap = ap;
            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += new EventHandler(Form_CallTracking_Load);

            // Register the cancel event logic
            newClientPage.Cancelled += new EventHandler(FormCancelled);
            oldClientPage.Cancelled += new EventHandler(FormCancelled);
            otherPage.Cancelled += new EventHandler(FormCancelled);
            noticesPage.Cancelled += new EventHandler(FormCancelled);
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= Form_CallTracking_Load;

            // Register the cancel event logic
            newClientPage.Cancelled -= new EventHandler(FormCancelled);
            oldClientPage.Cancelled -= new EventHandler(FormCancelled);
            otherPage.Cancelled -= new EventHandler(FormCancelled);
            noticesPage.Cancelled -= new EventHandler(FormCancelled);
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_CallTracking_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Force the form to start with the first tab page
                xtraTabControl1.SelectedTabPageIndex = 0;

                // Invoke the LoadHandler for each of the pages
                newClientPage.LoadHandler();
                oldClientPage.LoadHandler();
                otherPage.LoadHandler();
                noticesPage.LoadHandler();

                // Do the common Reset logic now
                ResetForm();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Trip the RESET of the form data for each of the pages
        /// </summary>
        internal void ResetForm()
        {
            newClientPage.Reset();
            oldClientPage.Reset();
            otherPage.Reset();
            noticesPage.Reset();
        }

        /// <summary>
        /// Process the CANCEL button from one of the pages.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormCancelled(object sender, EventArgs e)
        {
            Close();
        }
    }
}