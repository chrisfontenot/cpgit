﻿namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    partial class Form_CallTracking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                Form_CallTracking_Dispose(disposing);
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.noticesPage = new FAQ.Detail.XtraUserControl_Notices();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.newClientPage = new FAQ.Detail.XtraUserControl_NewDMP();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.oldClientPage = new FAQ.Detail.XtraUserControl_OldClient();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.otherPage = new FAQ.Detail.XtraUserControl_Other();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.HeaderAutoFill = DevExpress.Utils.DefaultBoolean.True;
            this.xtraTabControl1.Location = new System.Drawing.Point(5, 5);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.Padding = new System.Windows.Forms.Padding(5);
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage4;
            this.xtraTabControl1.Size = new System.Drawing.Size(458, 362);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.noticesPage);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Padding = new System.Windows.Forms.Padding(5);
            this.xtraTabPage4.Size = new System.Drawing.Size(450, 332);
            this.xtraTabPage4.Text = "State Info";
            // 
            // noticesPage
            // 
            this.noticesPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.noticesPage.Location = new System.Drawing.Point(5, 5);
            this.noticesPage.Name = "noticesPage";
            this.noticesPage.Size = new System.Drawing.Size(440, 322);
            this.noticesPage.TabIndex = 0;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.newClientPage);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Padding = new System.Windows.Forms.Padding(5);
            this.xtraTabPage1.Size = new System.Drawing.Size(450, 332);
            this.xtraTabPage1.Text = "New Client";
            // 
            // newClientPage
            // 
            this.newClientPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newClientPage.Location = new System.Drawing.Point(5, 5);
            this.newClientPage.Name = "newClientPage";
            this.newClientPage.Size = new System.Drawing.Size(440, 322);
            this.newClientPage.TabIndex = 0;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.oldClientPage);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Padding = new System.Windows.Forms.Padding(5);
            this.xtraTabPage2.Size = new System.Drawing.Size(450, 332);
            this.xtraTabPage2.Text = "Existing Client";
            // 
            // oldClientPage
            // 
            this.oldClientPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oldClientPage.Location = new System.Drawing.Point(5, 5);
            this.oldClientPage.Name = "oldClientPage";
            this.oldClientPage.Size = new System.Drawing.Size(440, 322);
            this.oldClientPage.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.otherPage);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Padding = new System.Windows.Forms.Padding(5);
            this.xtraTabPage3.Size = new System.Drawing.Size(450, 332);
            this.xtraTabPage3.Text = "Other";
            // 
            // otherPage
            // 
            this.otherPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.otherPage.Location = new System.Drawing.Point(5, 5);
            this.otherPage.Name = "otherPage";
            this.otherPage.Size = new System.Drawing.Size(440, 322);
            this.otherPage.TabIndex = 0;
            // 
            // Form_CallTracking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(468, 372);
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "Form_CallTracking";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "Call Tracking";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            this.ResumeLayout(false);
        }
        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private XtraUserControl_NewDMP newClientPage;
        private XtraUserControl_OldClient oldClientPage;
        private XtraUserControl_Notices noticesPage;
        private XtraUserControl_Other otherPage;
    }
}