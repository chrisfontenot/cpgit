﻿using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    /// <summary>
    /// Class to parse the argument strings
    /// </summary>
    public class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        public ArgParser() : base(new string[] {""}) { }
        /// <summary>
        /// Global items for the procedure
        /// </summary>
        public ArgParser ap;
        private System.Int32 privateExisting_RequireClient = 0;

        /// <summary>
        /// Does the existing tab require an existing client?
        /// </summary>
        public bool Existing_RequireClient()
        {
            if (privateExisting_RequireClient == 0)
            {
                string strItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.FaqDetail), "Existing.RequireClient");
                bool ItemValue;
                if (!bool.TryParse(strItem, out ItemValue))
                {
                    ItemValue = true;
                }
                privateExisting_RequireClient = ItemValue ? 1 : 2;
            }
            return privateExisting_RequireClient == 1;
        }

        /// <summary>
        /// Handle a switch and its value
        /// </summary>
        protected override DebtPlus.Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            DebtPlus.Utils.ArgParserBase.SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;
                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Define the usage of the program.
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}\r\n", errorInfo));
            }

            System.Reflection.Assembly _asm = System.Reflection.Assembly.GetExecutingAssembly();
            string fname = System.IO.Path.GetFileName(_asm.CodeBase);
            AppendErrorLine("Usage: " + fname);
        }
    }

    public class Mainline : DebtPlus.Interfaces.Desktop.IDesktopMainline //, DebtPlus.Interfaces.Desktop.ISupportCreateClientRequest
    {
        public Mainline()
            : base()
        {
        }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ArgParser();
                    if (ap.Parse((string[])param))
                    {
                        var frm = new Form_CallTracking(ap);
                        System.Windows.Forms.Application.Run(frm);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "FAQDetail"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }

    internal class faq_query_item
    {
        public System.String grouping { get; private set; }
        public System.String attributes { get; private set; }
        public System.String description { get; private set; }
        public System.Int32 Id { get; private set; }

        public faq_query_item(System.Int32 Id, System.String grouping, System.String description, System.String attributes)
        {
            this.Id = Id;
            this.grouping = grouping;
            this.description = description;
            this.attributes = attributes;
        }
    }

    internal static class Cache
    {
        internal static class faq_item
        {
            internal static object lockFAQ_Items = new object();
            internal static System.Collections.Generic.List<faq_query_item> colItems;
            internal static System.Collections.Generic.List<faq_query_item> getList()
            {
                if (colItems == null)
                {
                    lock (lockFAQ_Items)
                    {
                        if (colItems == null)
                        {
                            using (var dc = new BusinessContext())
                            {
                                // Read the non-referral items from the database
                                colItems = (from r in dc.faq_items where r.grouping != "REFERRAL" select new faq_query_item(r.Id, r.grouping, r.description, r.attributes)).ToList();

                                // Merge the list with the referral sources from the system.
                                System.Collections.Generic.List<faq_query_item> referral_list = (from r in dc.referred_bies select new faq_query_item(r.Id, "REFERRAL", r.description, string.Empty)).ToList();
                                colItems = colItems.Concat(referral_list).ToList();
                            }
                        }
                    }
                }

                return colItems;
            }

            internal static object getDefault()
            {
                throw new System.NotImplementedException("There is no default for the FAQ Item list");
            }
        }
    }
}
