﻿namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    partial class XtraUserControl_Notices
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_StateID = new DevExpress.XtraEditors.LookUpEdit();
            this.richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_StateID.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_OK
            // 
            this.Button_OK.Enabled = false;
            // 
            // Button_Apply
            // 
            this.Button_Apply.Enabled = false;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 8);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(26, 13);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "State";
            // 
            // lookUpEdit_StateID
            // 
            this.lookUpEdit_StateID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_StateID.Location = new System.Drawing.Point(36, 4);
            this.lookUpEdit_StateID.Name = "lookUpEdit_StateID";
            this.lookUpEdit_StateID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_StateID.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("MailingCode", "State", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CountryName", "Country", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.lookUpEdit_StateID.Properties.DisplayMember = "Name";
            this.lookUpEdit_StateID.Properties.NullText = "";
            this.lookUpEdit_StateID.Properties.ShowFooter = false;
            this.lookUpEdit_StateID.Properties.ValueMember = "Id";
            this.lookUpEdit_StateID.Size = new System.Drawing.Size(386, 20);
            this.lookUpEdit_StateID.TabIndex = 4;
            // 
            // richEditControl1
            // 
            this.richEditControl1.AcceptsTab = false;
            this.richEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            this.richEditControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richEditControl1.Location = new System.Drawing.Point(4, 30);
            this.richEditControl1.Name = "richEditControl1";
            this.richEditControl1.Options.VerticalScrollbar.Visibility = DevExpress.XtraRichEdit.RichEditScrollbarVisibility.Visible;
            this.richEditControl1.ReadOnly = true;
            this.richEditControl1.ShowCaretInReadOnly = false;
            this.richEditControl1.Size = new System.Drawing.Size(418, 181);
            this.richEditControl1.TabIndex = 6;
            // 
            // XtraUserControl_Notices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.richEditControl1);
            this.Controls.Add(this.lookUpEdit_StateID);
            this.Controls.Add(this.labelControl1);
            this.Name = "XtraUserControl_Notices";
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_StateID, 0);
            this.Controls.SetChildIndex(this.richEditControl1, 0);
            this.Controls.SetChildIndex(this.Button_OK, 0);
            this.Controls.SetChildIndex(this.Button_Apply, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_StateID.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        protected internal DevExpress.XtraEditors.LabelControl labelControl1;
        protected internal DevExpress.XtraEditors.LookUpEdit lookUpEdit_StateID;
        protected internal DevExpress.XtraRichEdit.RichEditControl richEditControl1;
    }
}
