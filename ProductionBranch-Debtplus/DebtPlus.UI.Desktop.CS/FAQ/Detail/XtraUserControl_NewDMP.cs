﻿using System;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    public partial class XtraUserControl_NewDMP : XtraUserControl_Memo
    {
        public XtraUserControl_NewDMP()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            simpleButton_Create.Click += new EventHandler(simpleButton_Create_Click);
            NewClient_Referral.SelectedIndexChanged += new EventHandler(FormChanged);
            NewClient_Disposition.SelectedIndexChanged += new EventHandler(FormChanged);
            NewClient_Name.TextChanged += new EventHandler(FormChanged);
            NewClient_PhoneNumber.TextChanged += new EventHandler(FormChanged);
            NewClient_Type.SelectedIndexChanged += new EventHandler(NewClient_Type_SelectedIndexChanged);
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            simpleButton_Create.Click -= new EventHandler(simpleButton_Create_Click);
            NewClient_Referral.SelectedIndexChanged -= new EventHandler(FormChanged);
            NewClient_Disposition.SelectedIndexChanged -= new EventHandler(FormChanged);
            NewClient_Name.TextChanged -= new EventHandler(FormChanged);
            NewClient_PhoneNumber.TextChanged -= new EventHandler(FormChanged);
            NewClient_Type.SelectedIndexChanged -= new EventHandler(NewClient_Type_SelectedIndexChanged);
        }

        /// <summary>
        /// Process the inital loading of this control. Define the data in the lookup fields.
        /// </summary>
        public override void LoadHandler()
        {
            // Do the base loader
            base.LoadHandler();

            // Remove the handlers since we don't want events just yet.
            UnRegisterHandlers();
            try
            {
                // Load the new client types
                if (NewClient_Type.Properties.Items.Count == 0)
                {
                    // First, load the specific type. If none found, load the default types
                    LoadList(ref NewClient_Type, "NEW TYPE");
                    if (NewClient_Type.Properties.Items.Count == 0)
                    {
                        LoadList(ref NewClient_Type, "TYPE");
                    }

                    // The type of the call must be defined.
                    if (NewClient_Type.Properties.Items.Count == 0)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("The faq_items table does not contain entries for 'TYPE'", "Configuration Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                        RaiseCancelled();
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }

            // Set the OK status on the buttons accordingly
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        /// <summary>
        /// Reset the entry information on this form
        /// </summary>
        public override void Reset()
        {
            // Clear the base form information
            base.Reset();

            // Reset the fields for this form
            NewClient_Disposition.SelectedIndex = -1;
            NewClient_Reason.SelectedIndex = -1;
            NewClient_Referral.SelectedIndex = -1;
            NewClient_Type.SelectedIndex = -1;
            NewClient_Name.EditValue = null;

            // Clear the telephone number field
            NewClient_PhoneNumber.EditValueCountry = null;
            NewClient_PhoneNumber.EditValueAcode = string.Empty;
            NewClient_PhoneNumber.EditValueNumber = string.Empty;
            NewClient_PhoneNumber.EditValueExt = string.Empty;
            NewClient_PhoneNumber.Text = string.Empty;

            // Disable the OK button
            Button_Apply.Enabled = false;
            Button_OK.Enabled = false;
        }

        /// <summary>
        /// Handle the condition where the type of the call is changed for the new client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewClient_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            String ItemValue = string.Empty;
            DebtPlus.Data.Controls.ComboboxItem Item = (DebtPlus.Data.Controls.ComboboxItem)NewClient_Type.SelectedItem;
            if (Item != null)
            {
                ItemValue = Convert.ToString(Item.tag);
            }

            // Empty the current values from the other controls.
            NewClient_Referral.SelectedIndex = -1;
            NewClient_Reason.SelectedIndex = -1;
            NewClient_Disposition.SelectedIndex = -1;

            // If the type is not valid then disable the other controls
            if (ItemValue == string.Empty)
            {
                NewClient_Reason.Enabled = false;
                NewClient_Referral.Enabled = false;
                NewClient_Disposition.Enabled = false;
            }
            else
            {
                // Load the reason values
                LoadList(ref NewClient_Reason, string.Format("NEW {0} REASON", ItemValue));

                if (NewClient_Reason.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Reason, string.Format("{0} REASON", ItemValue));
                }

                if (NewClient_Reason.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Reason, "NEW REASON");
                }

                if (NewClient_Reason.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Reason, "REASON");
                }

                // Load the referral values
                LoadList(ref NewClient_Referral, string.Format("NEW {0} REFERRAL", ItemValue));
                if (NewClient_Referral.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Referral, string.Format("{0} REFERRAL", ItemValue));
                }

                if (NewClient_Referral.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Referral, "NEW REFERRAL");
                }

                if (NewClient_Referral.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Referral, "REFERRAL");
                }

                // Load the disposition values
                LoadList(ref NewClient_Disposition, string.Format("NEW {0} DISPOSITION", ItemValue));
                if (NewClient_Disposition.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Disposition, string.Format("{0} DISPOSITION", ItemValue));
                }

                if (NewClient_Disposition.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Disposition, "NEW DISPOSITION");
                }

                if (NewClient_Disposition.Properties.Items.Count == 0)
                {
                    LoadList(ref NewClient_Disposition, "DISPOSITION");
                }

                // Enable the controls
                NewClient_Reason.Enabled = true;
                NewClient_Referral.Enabled = true;
                NewClient_Disposition.Enabled = true;
            }

            // Enable or disable the OK button
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        private void FormChanged(object sender, EventArgs e)
        {
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        /// <summary>
        /// Should the OK button be enabled?
        /// </summary>
        /// <returns></returns>
        private bool OkEnabled()
        {
            if (NewClient_Disposition.SelectedIndex < 0)
            {
                return false;
            }
            if (NewClient_Reason.SelectedIndex < 0)
            {
                return false;
            }
            if (NewClient_Referral.SelectedIndex < 0)
            {
                return false;
            }
            if (NewClient_Type.SelectedIndex < 0)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Record the item to the database.
        /// </summary>
        protected override void RequestRecordEvent()
        {
            // Build the detail row with the parameters
            DebtPlus.LINQ.faq_detail detail = new DebtPlus.LINQ.faq_detail()
            {
                grouping = 1,   // new client
                note = Edit_Notes.Text.Trim(),
                phone = NewClient_PhoneNumber.Text.Trim(),
                referral = (Int32)((DebtPlus.Data.Controls.ComboboxItem) NewClient_Referral.SelectedItem).value,
                reason_for_call = (Int32)((DebtPlus.Data.Controls.ComboboxItem) NewClient_Reason.SelectedItem).value,
                disposition = (Int32)((DebtPlus.Data.Controls.ComboboxItem) NewClient_Disposition.SelectedItem).value
            };

            using (var dc = new BusinessContext())
            {
                dc.faq_details.InsertOnSubmit(detail);
                dc.SubmitChanges();
            }

            base.RequestRecordEvent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton_Create_Click(object sender, EventArgs e)
        {
            var cls = new DebtPlus.UI.Desktop.CS.Client.Create.DMP.Mainline();

            // Just run the form at this point
            var args = new String[] {};
            cls.OldAppMain(args);
        }
    }
}
