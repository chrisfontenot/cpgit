﻿namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    partial class XtraUserControl_NewDMP
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.NewClient_Name = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.NewClient_PhoneNumber = new DebtPlus.Data.Controls.TelephoneNumberControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton_Create = new DevExpress.XtraEditors.SimpleButton();
            this.NewClient_Type = new DevExpress.XtraEditors.ComboBoxEdit();
            this.NewClient_Reason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.NewClient_Referral = new DevExpress.XtraEditors.ComboBoxEdit();
            this.NewClient_Disposition = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Name.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_PhoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Reason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Referral.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Disposition.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl_Note.TabIndex = 13;
            // 
            // Edit_Notes
            // 
            this.Edit_Notes.TabIndex = 14;
            // 
            // Button_OK
            // 
            this.Button_OK.TabIndex = 15;
            // 
            // Button_Apply
            // 
            this.Button_Apply.TabIndex = 16;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.TabIndex = 17;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(4, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Name";
            // 
            // NewClient_Name
            // 
            this.NewClient_Name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewClient_Name.Location = new System.Drawing.Point(94, 3);
            this.NewClient_Name.Name = "NewClient_Name";
            this.NewClient_Name.Size = new System.Drawing.Size(237, 20);
            this.NewClient_Name.TabIndex = 1;
            this.NewClient_Name.ToolTip = "Enter the name of the person who called.";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(4, 32);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(70, 13);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "Phone Number";
            // 
            // NewClient_PhoneNumber
            // 
            this.NewClient_PhoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewClient_PhoneNumber.ErrorIcon = null;
            this.NewClient_PhoneNumber.ErrorText = "";
            this.NewClient_PhoneNumber.Location = new System.Drawing.Point(94, 28);
            this.NewClient_PhoneNumber.Name = "NewClient_PhoneNumber";
            this.NewClient_PhoneNumber.Size = new System.Drawing.Size(237, 20);
            this.NewClient_PhoneNumber.TabIndex = 4;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(4, 57);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(24, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "Type";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(4, 82);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(75, 13);
            this.labelControl5.TabIndex = 7;
            this.labelControl5.Text = "Reason For Call";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(4, 107);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(39, 13);
            this.labelControl6.TabIndex = 9;
            this.labelControl6.Text = "Referral";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(4, 132);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(51, 13);
            this.labelControl7.TabIndex = 11;
            this.labelControl7.Text = "Disposition";
            // 
            // simpleButton_Create
            // 
            this.simpleButton_Create.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_Create.Location = new System.Drawing.Point(347, 6);
            this.simpleButton_Create.Name = "simpleButton_Create";
            this.simpleButton_Create.Size = new System.Drawing.Size(75, 25);
            this.simpleButton_Create.TabIndex = 2;
            this.simpleButton_Create.Text = "Create...";
            this.simpleButton_Create.ToolTip = "Click here to invoke the \"New Client\" process to create a new client.";
            // 
            // NewClient_Type
            // 
            this.NewClient_Type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewClient_Type.EditValue = "";
            this.NewClient_Type.Location = new System.Drawing.Point(94, 53);
            this.NewClient_Type.Name = "NewClient_Type";
            this.NewClient_Type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NewClient_Type.Properties.PopupSizeable = true;
            this.NewClient_Type.Properties.Sorted = true;
            this.NewClient_Type.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.NewClient_Type.Size = new System.Drawing.Size(328, 20);
            this.NewClient_Type.TabIndex = 6;
            this.NewClient_Type.ToolTip = "Choose a type of the client\'s call. Once you choose the type, the other fields wi" +
    "ll be enabled.";
            // 
            // NewClient_Reason
            // 
            this.NewClient_Reason.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewClient_Reason.EditValue = "";
            this.NewClient_Reason.Location = new System.Drawing.Point(94, 78);
            this.NewClient_Reason.Name = "NewClient_Reason";
            this.NewClient_Reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NewClient_Reason.Properties.PopupSizeable = true;
            this.NewClient_Reason.Properties.Sorted = true;
            this.NewClient_Reason.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.NewClient_Reason.Size = new System.Drawing.Size(328, 20);
            this.NewClient_Reason.TabIndex = 8;
            this.NewClient_Reason.ToolTip = "Choose the closest reason for the call from this list.";
            // 
            // NewClient_Referral
            // 
            this.NewClient_Referral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewClient_Referral.EditValue = "";
            this.NewClient_Referral.Location = new System.Drawing.Point(94, 103);
            this.NewClient_Referral.Name = "NewClient_Referral";
            this.NewClient_Referral.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NewClient_Referral.Properties.PopupSizeable = true;
            this.NewClient_Referral.Properties.Sorted = true;
            this.NewClient_Referral.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.NewClient_Referral.Size = new System.Drawing.Size(328, 20);
            this.NewClient_Referral.TabIndex = 10;
            this.NewClient_Referral.ToolTip = "Choose the closest referral source for the client. Why did the client call this a" +
    "gency?";
            // 
            // NewClient_Disposition
            // 
            this.NewClient_Disposition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NewClient_Disposition.EditValue = "";
            this.NewClient_Disposition.Location = new System.Drawing.Point(94, 128);
            this.NewClient_Disposition.Name = "NewClient_Disposition";
            this.NewClient_Disposition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NewClient_Disposition.Properties.PopupSizeable = true;
            this.NewClient_Disposition.Properties.Sorted = true;
            this.NewClient_Disposition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.NewClient_Disposition.Size = new System.Drawing.Size(328, 20);
            this.NewClient_Disposition.TabIndex = 12;
            this.NewClient_Disposition.ToolTip = "Choose what you did for the client.";
            // 
            // XtraUserControl_NewClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton_Create);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.NewClient_PhoneNumber);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.NewClient_Name);
            this.Controls.Add(this.NewClient_Type);
            this.Controls.Add(this.NewClient_Reason);
            this.Controls.Add(this.NewClient_Referral);
            this.Controls.Add(this.NewClient_Disposition);
            this.Name = "XtraUserControl_NewClient";
            this.Controls.SetChildIndex(this.NewClient_Disposition, 0);
            this.Controls.SetChildIndex(this.NewClient_Referral, 0);
            this.Controls.SetChildIndex(this.NewClient_Reason, 0);
            this.Controls.SetChildIndex(this.NewClient_Type, 0);
            this.Controls.SetChildIndex(this.NewClient_Name, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.Edit_Notes, 0);
            this.Controls.SetChildIndex(this.labelControl_Note, 0);
            this.Controls.SetChildIndex(this.Button_OK, 0);
            this.Controls.SetChildIndex(this.Button_Apply, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.NewClient_PhoneNumber, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.simpleButton_Create, 0);
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Name.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_PhoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Reason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Referral.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewClient_Disposition.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal DevExpress.XtraEditors.LabelControl labelControl2;
        protected internal DevExpress.XtraEditors.TextEdit NewClient_Name;
        protected internal DevExpress.XtraEditors.LabelControl labelControl3;
        protected internal DebtPlus.Data.Controls.TelephoneNumberControl NewClient_PhoneNumber;
        protected internal DevExpress.XtraEditors.LabelControl labelControl4;
        protected internal DevExpress.XtraEditors.LabelControl labelControl5;
        protected internal DevExpress.XtraEditors.LabelControl labelControl6;
        protected internal DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Create;
        private DevExpress.XtraEditors.ComboBoxEdit NewClient_Type;
        private DevExpress.XtraEditors.ComboBoxEdit NewClient_Reason;
        private DevExpress.XtraEditors.ComboBoxEdit NewClient_Referral;
        private DevExpress.XtraEditors.ComboBoxEdit NewClient_Disposition;
    }
}
