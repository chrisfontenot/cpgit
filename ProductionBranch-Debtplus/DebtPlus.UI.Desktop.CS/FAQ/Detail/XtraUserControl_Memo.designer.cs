﻿namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    partial class XtraUserControl_Memo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl_Note = new DevExpress.XtraEditors.LabelControl();
            this.Edit_Notes = new DevExpress.XtraEditors.MemoEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_Note
            // 
            this.labelControl_Note.Location = new System.Drawing.Point(4, 159);
            this.labelControl_Note.Name = "labelControl_Note";
            this.labelControl_Note.Size = new System.Drawing.Size(23, 13);
            this.labelControl_Note.TabIndex = 3;
            this.labelControl_Note.Text = "Note";
            // 
            // Edit_Notes
            // 
            this.Edit_Notes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Edit_Notes.Location = new System.Drawing.Point(94, 157);
            this.Edit_Notes.Name = "Edit_Notes";
            this.Edit_Notes.Properties.MaxLength = 1024;
            this.Edit_Notes.Size = new System.Drawing.Size(328, 54);
            this.Edit_Notes.TabIndex = 4;
            this.Edit_Notes.ToolTip = "Note for the transaction.";
            // 
            // XtraUserControl_Memo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Edit_Notes);
            this.Controls.Add(this.labelControl_Note);
            this.Name = "XtraUserControl_Memo";
            this.Controls.SetChildIndex(this.labelControl_Note, 0);
            this.Controls.SetChildIndex(this.Edit_Notes, 0);
            this.Controls.SetChildIndex(this.Button_OK, 0);
            this.Controls.SetChildIndex(this.Button_Apply, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LabelControl labelControl_Note;
        public DevExpress.XtraEditors.MemoEdit Edit_Notes;

    }
}
