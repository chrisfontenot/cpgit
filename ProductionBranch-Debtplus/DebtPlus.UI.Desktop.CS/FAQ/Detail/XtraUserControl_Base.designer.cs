﻿namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    partial class XtraUserControl_Base
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Apply = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(94, 227);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 0;
            this.Button_OK.Text = "&OK";
            // 
            // Button_Apply
            // 
            this.Button_Apply.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Apply.Location = new System.Drawing.Point(175, 227);
            this.Button_Apply.Name = "Button_Apply";
            this.Button_Apply.Size = new System.Drawing.Size(75, 25);
            this.Button_Apply.TabIndex = 1;
            this.Button_Apply.Text = "&Apply";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Location = new System.Drawing.Point(256, 227);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // XtraUserControl_Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Apply);
            this.Controls.Add(this.Button_OK);
            this.Name = "XtraUserControl_Base";
            this.Size = new System.Drawing.Size(425, 255);
            this.ResumeLayout(false);

        }

        #endregion

        public DevExpress.XtraEditors.SimpleButton Button_OK;
        public DevExpress.XtraEditors.SimpleButton Button_Apply;
        public DevExpress.XtraEditors.SimpleButton Button_Cancel;
    }
}
