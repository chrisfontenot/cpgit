﻿using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    public partial class XtraUserControl_Base : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Obtain the pointer to the "global" context information block
        /// </summary>
        /// <returns></returns>
        protected Form_CallTracking getContext()
        {
            return (Form_CallTracking) ParentForm;
        }

        #region CancelEvent

        /// <summary>
        /// Event to indicate that the CANCEL button was pressed
        /// </summary>
        public event EventHandler Cancelled;

        /// <summary>
        /// Raise the CANCELLED event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseCancelled(EventArgs e)
        {
            var evt = Cancelled;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the CANCELLED event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnCancelled(EventArgs e)
        {
            RaiseCancelled(e);
        }

        /// <summary>
        /// Raise the CANCELLED event
        /// </summary>
        protected void RaiseCancelled()
        {
            OnCancelled(EventArgs.Empty);
        }
        #endregion

        public XtraUserControl_Base()
        {
            InitializeComponent();
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register to handle the events
        /// </summary>
        private void RegisterHandlers()
        {
            Button_OK.Click += new EventHandler(Button_OK_Click);
            Button_Apply.Click += new EventHandler(Button_Apply_Click);
            Button_Cancel.Click += new EventHandler(Button_Cancel_Click);
        }

        /// <summary>
        /// Remove the event registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_OK.Click -= new EventHandler(Button_OK_Click);
            Button_Apply.Click -= new EventHandler(Button_Apply_Click);
            Button_Cancel.Click -= new EventHandler(Button_Cancel_Click);
        }

        /// <summary>
        /// Process the CLICK on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Button_OK_Click(object sender, EventArgs e)
        {
            RequestRecordEvent();       // Do the record operation
            RaiseCancelled();           // Follow it by a cancel operation
        }

        /// <summary>
        /// Process the CLICK on the APPLY button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Button_Apply_Click(object sender, EventArgs e)
        {
            RequestRecordEvent();       // Do the record operation
        }

        /// <summary>
        /// Process the CLICK on the CANCEL button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Button_Cancel_Click(object sender, EventArgs e)
        {
            RaiseCancelled();           // Request that the form be cancelled
        }

        /// <summary>
        /// Raise the appropriate record event
        /// </summary>
        protected virtual void RequestRecordEvent()
        {
            // Reset the form after the record event has been performed
            getContext().ResetForm();
        }

        /// <summary>
        /// Reset the controls for this form to indicate that the items are empty
        /// </summary>
        /// <remarks>
        /// This is called after the record operation has been processed. It is given
        /// to every instance of this class to reset all of the KeyField fields to an empty
        /// condition in preparation for generating a new record.
        /// </remarks>
        public virtual void Reset()
        {
        }

        /// <summary>
        /// Called when the main form is LOADED.
        /// </summary>
        /// <remarks>
        /// This is the time to read the control values from the database storage
        /// and configure the appropriate lookup routines. This is called only once.
        /// It is followed by a Reset call to empty the current record data.
        /// </remarks>
        public virtual void LoadHandler()
        {
        }
    }
}
