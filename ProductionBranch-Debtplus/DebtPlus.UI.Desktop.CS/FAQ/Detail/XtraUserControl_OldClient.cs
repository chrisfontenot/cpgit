using System;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    public partial class XtraUserControl_OldClient : XtraUserControl_Memo
    {
        public XtraUserControl_OldClient()
        {
            InitializeComponent();

            // If this is not the design mode, i.e. we are really running, then register the handlers.
            // If this is design mode then don't as they can cause problems with database accesses, etc.
            if (!DesignMode)
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            ExistingClient_Disposition.SelectedIndexChanged += FormChanged;
            ExistingClient_Client.TextChanged += FormChanged;
            ExistingClient_Reason.SelectedIndexChanged += FormChanged;
            ExistingClient_Type.SelectedIndexChanged += ExistingClient_Type_SelectedIndexChanged;
        }

        /// <summary>
        /// Remove the event handler registration
        /// </summary>
        private void UnRegisterHandlers()
        {
            ExistingClient_Disposition.SelectedIndexChanged -= FormChanged;
            ExistingClient_Client.TextChanged -= FormChanged;
            ExistingClient_Reason.SelectedIndexChanged -= FormChanged;
            ExistingClient_Type.SelectedIndexChanged -= ExistingClient_Type_SelectedIndexChanged;
        }

        /// <summary>
        /// Process the inital loading of this control. Define the data in the lookup fields.
        /// </summary>
        public override void LoadHandler()
        {
            base.LoadHandler();

            UnRegisterHandlers();
            try
            {
                // Change the client to require a valid client if indicated. It will take it from here.
                ExistingClient_Client.ValidationLevel = getContext().ap.Existing_RequireClient() ? DebtPlus.UI.Client.Widgets.Controls.ClientID.ValidationLevelEnum.Exists : DebtPlus.UI.Client.Widgets.Controls.ClientID.ValidationLevelEnum.None;

                // Load the existing client types
                if (ExistingClient_Type.Properties.Items.Count == 0)
                {
                    // First, load the specific type. If none found, load the default types
                    LoadList(ref ExistingClient_Type, "EXISTING TYPE");
                    if (ExistingClient_Type.Properties.Items.Count == 0)
                    {
                        LoadList(ref ExistingClient_Type, "TYPE");
                    }

                    // The type of the call must be defined.
                    if (ExistingClient_Type.Properties.Items.Count == 0)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("The faq_items table does not contain entries for 'TYPE'", "Configuration Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Stop);
                        RaiseCancelled();
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }

            // Enable or disable the OK button based upon the form settings.
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        /// <summary>
        /// Reset the entry information on this form
        /// </summary>
        public override void Reset()
        {
            base.Reset();

            ExistingClient_Reason.SelectedIndex = -1;
            ExistingClient_Disposition.SelectedIndex = -1;
            ExistingClient_Type.SelectedIndex = -1;
            ExistingClient_Client.EditValue = null;

            Button_Apply.Enabled = false;
            Button_OK.Enabled = false;
        }

        /// <summary>
        /// Process a change in the type item
        /// </summary>
        private void ExistingClient_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Find the key to the selected type
            var itemValue = string.Empty;
            var item = (DebtPlus.Data.Controls.ComboboxItem)ExistingClient_Type.SelectedItem;
            if (item != null)
            {
                itemValue = Convert.ToString(item.tag);
            }

            // Remove the other values that are dependent upon our selection
            ExistingClient_Reason.SelectedIndex = -1;
            ExistingClient_Disposition.SelectedIndex = -1;

            // If there is no key then disable the other controls
            if (itemValue == string.Empty)
            {
                ExistingClient_Reason.Enabled = false;
                ExistingClient_Disposition.Enabled = false;
            }
            else
            {
                // Load the reason values
                LoadList(ref ExistingClient_Reason, string.Format("EXISTING {0} REASON", itemValue));
                if (ExistingClient_Reason.Properties.Items.Count == 0)
                {
                    LoadList(ref ExistingClient_Reason, string.Format("{0} REASON", itemValue));
                }

                if (ExistingClient_Reason.Properties.Items.Count == 0)
                {
                    LoadList(ref ExistingClient_Reason, "EXISTING REASON");
                }

                if (ExistingClient_Reason.Properties.Items.Count == 0)
                {
                    LoadList(ref ExistingClient_Reason, "REASON");
                }

                // Load the disposition values
                LoadList(ref ExistingClient_Disposition, string.Format("EXISTING {0} DISPOSITION", itemValue));

                if (ExistingClient_Disposition.Properties.Items.Count == 0)
                {
                    LoadList(ref ExistingClient_Disposition, string.Format("{0} DISPOSITION", itemValue));
                }

                if (ExistingClient_Disposition.Properties.Items.Count == 0)
                {
                    LoadList(ref ExistingClient_Disposition, "EXISTING DISPOSITION");
                }

                if (ExistingClient_Disposition.Properties.Items.Count == 0)
                {
                    LoadList(ref ExistingClient_Disposition, "DISPOSITION");
                }

                // Enable the controls
                ExistingClient_Reason.Enabled = true;
                ExistingClient_Disposition.Enabled = true;
            }

            // Enable or disable the OK button
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        /// <summary>
        /// Validate the form when a control field is changed
        /// </summary>
        private void FormChanged(object sender, EventArgs e)
        {
            Button_Apply.Enabled = OkEnabled();
            Button_OK.Enabled = Button_Apply.Enabled;
        }

        /// <summary>
        /// Should the OK button be enabled?
        /// </summary>
        private bool OkEnabled()
        {
            if (ExistingClient_Type.SelectedIndex < 0)
            {
                return false;
            }
            
            if (ExistingClient_Disposition.SelectedIndex < 0)
            {
                return false;
            }

            if (ExistingClient_Reason.SelectedIndex < 0)
            {
                return false;
            }

            if (DebtPlus.Utils.Nulls.DInt(ExistingClient_Client.EditValue, -1) <= 0)
            {
                return ! getContext().ap.Existing_RequireClient();
            }
            return true;
        }

        /// <summary>
        /// The Apply button was pressed. Generate the event to record the new row.
        /// </summary>
        protected override void RequestRecordEvent()
        {
            // Build the detail row with the parameters
            DebtPlus.LINQ.faq_detail detail = new DebtPlus.LINQ.faq_detail()
            {
                grouping = 2,   // existing client
                note = Edit_Notes.Text.Trim(),
                reason_for_call = (Int32)((DebtPlus.Data.Controls.ComboboxItem)ExistingClient_Reason.SelectedItem).value,
                disposition = (Int32)((DebtPlus.Data.Controls.ComboboxItem)ExistingClient_Disposition.SelectedItem).value,
                client = ExistingClient_Client.EditValue.GetValueOrDefault(-1)
            };

            using (var dc = new BusinessContext())
            {
                dc.faq_details.InsertOnSubmit(detail);
                dc.SubmitChanges();
            }

            base.RequestRecordEvent();
        }
    }
}
