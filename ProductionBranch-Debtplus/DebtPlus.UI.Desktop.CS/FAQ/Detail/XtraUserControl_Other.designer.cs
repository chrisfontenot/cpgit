﻿namespace DebtPlus.UI.Desktop.CS.FAQ.Detail
{
    partial class XtraUserControl_Other
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.Other_Disposition = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Other_Disposition.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_Note
            // 
            this.labelControl_Note.TabIndex = 2;
            // 
            // Edit_Notes
            // 
            this.Edit_Notes.TabIndex = 3;
            // 
            // Button_OK
            // 
            this.Button_OK.TabIndex = 4;
            // 
            // Button_Apply
            // 
            this.Button_Apply.TabIndex = 5;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.TabIndex = 6;
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(4, 132);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(51, 13);
            this.labelControl5.TabIndex = 0;
            this.labelControl5.Text = "Disposition";
            // 
            // Other_Disposition
            // 
            this.Other_Disposition.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Other_Disposition.EditValue = "";
            this.Other_Disposition.Location = new System.Drawing.Point(94, 128);
            this.Other_Disposition.Name = "Other_Disposition";
            this.Other_Disposition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Other_Disposition.Properties.PopupSizeable = true;
            this.Other_Disposition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.Other_Disposition.Size = new System.Drawing.Size(328, 20);
            this.Other_Disposition.TabIndex = 1;
            // 
            // XtraUserControl_Other
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.Other_Disposition);
            this.Name = "XtraUserControl_Other";
            this.Controls.SetChildIndex(this.Other_Disposition, 0);
            this.Controls.SetChildIndex(this.labelControl_Note, 0);
            this.Controls.SetChildIndex(this.Edit_Notes, 0);
            this.Controls.SetChildIndex(this.Button_OK, 0);
            this.Controls.SetChildIndex(this.Button_Apply, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            ((System.ComponentModel.ISupportInitialize)(this.Edit_Notes.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Other_Disposition.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        protected internal DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.ComboBoxEdit Other_Disposition;
    }
}
