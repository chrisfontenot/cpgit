using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.FAQ.Count
{
    public partial class Form_New : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_New() : base()
        {
            InitializeComponent();
            this.Load += Form_New_Load;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit1;
        internal DevExpress.XtraEditors.TextEdit TextEdit1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form_New));
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.ComboBoxEdit1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();

            ((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
            this.SuspendLayout();

            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(416, 24);

            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Enter the text of the question that you wish to add. You must choose an appropria" + "te category for the question.";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 51);
            this.LabelControl2.Name = "LabelControl2";

            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Question:";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(8, 75);
            this.LabelControl3.Name = "LabelControl3";

            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "Category:";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //Button_OK
            //
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(128, 104);
            this.Button_OK.Name = "Button_OK";

            this.Button_OK.TabIndex = 5;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(232, 104);
            this.Button_Cancel.Name = "Button_Cancel";

            this.Button_Cancel.TabIndex = 6;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //ComboBoxEdit1
            //
            this.ComboBoxEdit1.Location = new System.Drawing.Point(88, 72);
            this.ComboBoxEdit1.Name = "ComboBoxEdit1";
            //
            //ComboBoxEdit1.Properties
            //
            this.ComboBoxEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ComboBoxEdit1.Size = new System.Drawing.Size(328, 20);

            this.ComboBoxEdit1.TabIndex = 4;
            this.ComboBoxEdit1.ToolTipController = this.ToolTipController1;
            //
            //TextEdit1
            //
            this.TextEdit1.Location = new System.Drawing.Point(88, 48);
            this.TextEdit1.Name = "TextEdit1";
            this.TextEdit1.Size = new System.Drawing.Size(328, 20);

            this.TextEdit1.TabIndex = 2;
            this.TextEdit1.ToolTipController = this.ToolTipController1;
            this.TextEdit1.Properties.MaxLength = 50;
            //
            //Form_New
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(432, 142);
            this.Controls.Add(this.TextEdit1);
            this.Controls.Add(this.ComboBoxEdit1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Name = "Form_New";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Add New Frequently Asked Question";

            ((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void Form_New_Load(System.Object sender, System.EventArgs e)
        {
            // Load the list of types
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlDataReader rd = null;
            try
            {
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = cn;
                    _with1.CommandText = "lst_faq_types";
                    _with1.CommandType = CommandType.StoredProcedure;
                    rd = _with1.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);
                }

                var _with2 = ComboBoxEdit1.Properties;
                _with2.Items.Clear();
                while (rd.Read())
                {
                    string description = System.String.Empty;
                    System.Int32 item_value = 0;
                    bool default_item = false;
                    for (System.Int32 idx = 0; idx <= rd.FieldCount - 1; idx++)
                    {
                        if (!rd.IsDBNull(idx))
                        {
                            switch (rd.GetName(idx).ToLower())
                            {
                                case "description":
                                    description = rd.GetString(idx);
                                    break;

                                case "item_key":
                                    item_value = Convert.ToInt32(rd.GetValue(idx));
                                    break;

                                case "default":
                                    default_item = Convert.ToInt32(rd.GetValue(idx)) != 0;
                                    break;
                            }
                        }
                    }

                    System.Int32 NewItem = _with2.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(description, item_value));
                    if (default_item)
                        ComboBoxEdit1.SelectedIndex = NewItem;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Reading FAQ types");
            }
            finally
            {
                if (rd != null && !rd.IsClosed)
                    rd.Close();
                if (cn != null)
                {
                    if (cn.State != ConnectionState.Closed)
                        cn.Close();
                    cn.Close();
                }
            }

            // Clear the question field
            TextEdit1.Text = System.String.Empty;

            // Add the handlers for the controls
            ComboBoxEdit1.SelectedIndexChanged += Form_Changed;
            TextEdit1.TextChanged += Form_Changed;

            // Enable the OK button
            Form_Changed(this, new System.EventArgs());
        }

        private void Form_Changed(object sender, System.EventArgs e)
        {
            Button_OK.Enabled = TextEdit1.Text.Trim() != System.String.Empty && ComboBoxEdit1.SelectedIndex >= 0;
        }
    }
}