using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.FAQ.Count
{
    internal partial class Form_Count : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_Count()
            : base()
        {
            InitializeComponent();
            LabelControl1.Text = "This function will help track statistics for frequently asked questions. Not all " + "questions that you may encounter are \"frequent\", however, for our annual report," + " we need to capture a realistic list of questions that are commonly asked when y" + "ou talk to the client." + Environment.NewLine + Environment.NewLine + "To indicate a question was asked, you need only check th" + "e box next to the question and then press the submit button. This form will not " + "go away until you close it, but it will record the question in the database.";
        }

        private ArgParser ap = null;

        public Form_Count(ArgParser ap) : this()
        {
            this.ap = ap;
            this.Load += Form_Count_Load;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.CheckedListBoxControl CheckedListBoxControl1;
        internal DevExpress.XtraEditors.SimpleButton Button_Submit;
        internal DevExpress.XtraEditors.SimpleButton Button_New;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Count));
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.CheckedListBoxControl1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.Button_Submit = new DevExpress.XtraEditors.SimpleButton();
            this.Button_New = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl1).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(388, 107);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = resources.GetString("LabelControl1.Text");
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            this.LabelControl1.UseMnemonic = false;
            //
            //CheckedListBoxControl1
            //
            this.CheckedListBoxControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.CheckedListBoxControl1.CheckOnClick = true;
            this.CheckedListBoxControl1.Location = new System.Drawing.Point(8, 121);
            this.CheckedListBoxControl1.Name = "CheckedListBoxControl1";
            this.CheckedListBoxControl1.Size = new System.Drawing.Size(312, 215);
            this.CheckedListBoxControl1.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this.CheckedListBoxControl1.TabIndex = 1;
            this.CheckedListBoxControl1.ToolTipController = this.ToolTipController1;
            //
            //Button_Submit
            //
            this.Button_Submit.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Submit.Location = new System.Drawing.Point(326, 121);
            this.Button_Submit.Name = "Button_Submit";
            this.Button_Submit.Size = new System.Drawing.Size(75, 23);
            this.Button_Submit.TabIndex = 2;
            this.Button_Submit.Text = "&Submit";
            this.Button_Submit.ToolTip = "Click here to submit these question counts to the database";
            this.Button_Submit.ToolTipController = this.ToolTipController1;
            //
            //Button_New
            //
            this.Button_New.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.Button_New.Location = new System.Drawing.Point(326, 313);
            this.Button_New.Name = "Button_New";
            this.Button_New.Size = new System.Drawing.Size(75, 23);
            this.Button_New.TabIndex = 3;
            this.Button_New.Text = "&New...";
            this.Button_New.ToolTip = "Click here to create a new question for the FAQ system";
            this.Button_New.ToolTipController = this.ToolTipController1;
            //
            //Form_Count
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(408, 350);
            this.Controls.Add(this.Button_New);
            this.Controls.Add(this.Button_Submit);
            this.Controls.Add(this.CheckedListBoxControl1);
            this.Controls.Add(this.LabelControl1);
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Name = "Form_Count";
            this.Text = "Frequently Asked Questions";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBoxControl1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void ReloadQuestionList()
        {
            // Load the list of questions into the control
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlDataReader rd = null;

            try
            {
                // Issue the request to read the database table
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = cn;
                    _with1.CommandType = CommandType.StoredProcedure;
                    _with1.CommandText = "lst_faq_questions";
                    rd = _with1.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);
                }

                // Define the list of questions into the system
                var _with2 = CheckedListBoxControl1;
                _with2.Items.Clear();
                while (rd.Read())
                {
                    string description = string.Empty;
                    System.Int32 value = 0;
                    for (System.Int32 itm = 0; itm <= rd.FieldCount - 1; itm++)
                    {
                        if (!rd.IsDBNull(itm))
                        {
                            switch (rd.GetName(itm).ToLower())
                            {
                                case "description":
                                    description = Convert.ToString(rd.GetValue(itm)).Trim();
                                    break;

                                case "item_key":
                                    value = Convert.ToInt32(rd.GetValue(itm));
                                    break;
                            }
                        }
                    }

                    // Add the item. We need to wrap it in our own description/value class.
                    DebtPlus.Data.Controls.ComboboxItem ui_item = new DebtPlus.Data.Controls.ComboboxItem(description, value);
                    _with2.Items.Add(new DevExpress.XtraEditors.Controls.CheckedListBoxItem(ui_item, CheckState.Unchecked, true));
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // This is the expected error if it occurred
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Loading List of questions");
            }
            finally
            {
                // Close the database connection when completed
                if (rd != null)
                    rd.Dispose();
                if (cn != null)
                    cn.Dispose();
            }
        }

        private void Form_Count_Load(object sender, System.EventArgs e)
        {
            // Load and save the form placement
            LoadPlacement("FAQ.Count.Main");

            // Reload the list of questions
            ReloadQuestionList();

            // Add the handler to process the checked events
            EnableOK();
            CheckedListBoxControl1.ItemCheck += CheckedListBoxControl1_ItemCheck;
            Button_Submit.Click += Button_Submit_Click;
            Button_New.Click += Button_New_Click;
        }

        private void EnableOK()
        {
            // Find if an item is checked. If so, enable the update to be performed.
            var _with3 = CheckedListBoxControl1;
            for (System.Int32 idx = 0; idx <= _with3.Items.Count - 1; idx++)
            {
                if (_with3.Items[idx].Enabled && _with3.Items[idx].CheckState == CheckState.Checked)
                {
                    Button_Submit.Enabled = true;
                    return;
                }
            }

            // The entire list is unchecked. Disable the button
            Button_Submit.Enabled = false;
        }

        private void CheckedListBoxControl1_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            EnableOK();
        }

        private void Button_Submit_Click(object sender, System.EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            var _with4 = CheckedListBoxControl1;
            for (System.Int32 idx = 0; idx <= _with4.Items.Count - 1; idx++)
            {
                if (_with4.Items[idx].Enabled && _with4.Items[idx].CheckState == CheckState.Checked)
                {
                    System.Int32 ID = Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)_with4.Items[idx].Value).value);
                    sb.AppendFormat(",{0}", ID);
                }
            }

            // If there is an checked item then open the database for update
            if (sb.Length > 0)
            {
                sb.Remove(0, 1);
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

                try
                {
                    // Perform the update to the database
                    cn.Open();
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with5 = cmd;
                        _with5.Connection = cn;
                        _with5.CommandText = string.Format("UPDATE faq_questions SET count_mtd = count_mtd + 1 WHERE faq_question in ({0})", sb.ToString());
                        _with5.ExecuteNonQuery();
                    }

                    // Remove the checked status from the event
                    var _with6 = CheckedListBoxControl1;
                    for (System.Int32 idx = 0; idx <= _with6.Items.Count - 1; idx++)
                    {
                        _with6.Items[idx].CheckState = CheckState.Unchecked;
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    // This is the expected error if it occurred
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Updating Count");
                }
                finally
                {
                    // Close the database connection when completed
                    if (cn != null)
                        cn.Dispose();
                }
            }

            // Enable/Disable the OK button
            EnableOK();
        }

        private void Button_New_Click(object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            object objKey = null;

            System.Windows.Forms.DialogResult result = default(System.Windows.Forms.DialogResult);
            string description = null;
            System.Int32 category = 0;

            using (Form_New frm = new Form_New())
            {
                var _with7 = frm;
                result = _with7.ShowDialog();
                if (result == DialogResult.OK)
                {
                    description = _with7.TextEdit1.Text.Trim();
                    category = Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)_with7.ComboBoxEdit1.SelectedItem).value);

                    try
                    {
                        // Open the database and add the row to the table
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with8 = cmd;
                            _with8.Connection = cn;
                            _with8.CommandText = "SET NOCOUNT ON;INSERT INTO faq_questions(faq_type,description) values (@faq_type,@description);SELECT scope_identity() as faq_question;SET NOCOUNT OFF;";
                            _with8.Parameters.Add("@faq_type", SqlDbType.Int).Value = category;
                            _with8.Parameters.Add("@description", SqlDbType.VarChar, 80).Value = description;
                            objKey = _with8.ExecuteScalar();
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        // Generate the error message if needed
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error Adding FAQ question");
                    }
                    finally
                    {
                        // Close the conneciton
                        if (cn != null)
                            cn.Dispose();
                    }
                }
            }

            // Reload the list of questions once the list has been updated
            if (objKey != null)
                ReloadQuestionList();
        }
    }
}