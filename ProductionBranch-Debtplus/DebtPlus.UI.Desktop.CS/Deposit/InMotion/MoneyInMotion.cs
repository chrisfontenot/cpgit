﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public class MoneyInMotion : InMotion
    {
        /// <summary>
        /// Initialize the class.
        /// </summary>
        public MoneyInMotion()
            : base()
        {
            TakeCourseTogether = "N";
        }

        /// <summary>
        /// "Y" if the spouse is taking the course at the same time. "N" otherwise.
        /// </summary>
        public string TakeCourseTogether { get; set; }

        /// <summary>
        /// Process the XML parsing logic for the class
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            Parser(ref rdr, this);
        }

        /// <summary>
        /// Implement the saving of this item to the database
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <param name="Parameter_Bank">Bank number</param>
        /// <param name="Parameter_Batch">Batch number</param>
        public override void RecordTransaction(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, Int32 Parameter_Batch)
        {
            // First, if there is a client then we can just use it.
            Int32 client = -1;
            Int32.TryParse(ClientID, out client);

            // If a client was entered then check to make sure that it is the proper value.
            if (client > 0 && !ValidateClient(cn, txn, client))
            {
                client = -1;
            }

            // If there is no client then try to find one.
            if (client <= 0)
            {
                client = FindClient(cn, txn);
            }

            // If the client could not be found then we need to create it.
            if (client <= 0)
            {
                client = CreateClient(cn, txn);
            }

            // Add the debt if it is not present
            CreateDebt(cn, txn, client, "V68300", true);

            // Add the client note
            SystemNote(cn, txn, client);

            // Record the deposit transaction
            CreateDeposit(cn, txn, client, Parameter_Batch, "VB", "MIM");
        }

        /// <summary>
        /// Validate the client against the database to ensure that the client is the proper value.
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <param name="client">Desired Client ID</param>
        /// <returns></returns>
        private bool ValidateClient(SqlConnection cn, SqlTransaction txn, Int32 client)
        {
            bool Answer = false;

            // THere must be a primary and it must have a name to be considered valid.
            if (Primary != null && Primary.Name != null)
            {
                try
                {
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;

                        // Look for a client with the same name as what was entered.
                        cmd.CommandText = "SELECT client FROM [view_client_address] WHERE [client]=@client AND [FirstName]=@first AND [LastName]=@last";
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@client", SqlDbType.Int).Value = client;
                        cmd.Parameters.Add("@first", SqlDbType.VarChar, 80).Value = (object)Primary.Name.First ?? DBNull.Value;
                        cmd.Parameters.Add("@last", SqlDbType.VarChar, 80).Value = (object)Primary.Name.Last ?? DBNull.Value;

                        // If there is a value then the client is valid. Otherwise, we won't get an answer.
                        object obj = cmd.ExecuteScalar();
                        if (obj != null && obj != DBNull.Value)
                        {
                            Answer = true;
                        }
                    }
                }

                // We don't really care about errors at this point. It is best that it just fail.
                catch { }
            }

            return Answer;
        }

        /// <summary>
        /// Create a client in the system
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <returns></returns>
        protected virtual Int32 CreateClient(SqlConnection cn, SqlTransaction txn)
        {
            Int32 HomeTelephoneID = -1;

            // Find the home telephone pointer to be used for the client
            PhoneType phone = Primary.FindTelephone();
            if (phone == null && Secondary != null && toBool(TakeCourseTogether))
            {
                phone = Secondary.FindTelephone();
            }

            if (phone != null)
            {
                HomeTelephoneID = phone.xpr_insert_telephonenumber(cn, txn);
            }

            // Allocate a client
            Int32 client = xpr_create_client(cn, txn, HomeTelephoneID);

            // Update the client's address information
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "UPDATE [clients] SET [AddressID]=@AddressID,[County]=@County WHERE [client]=@Client";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@AddressID", SqlDbType.Int).Value = Address.xpr_insert_Address(cn, txn);
                cmd.Parameters.Add("@Client", SqlDbType.Int).Value = client;
                cmd.Parameters.Add("@County", SqlDbType.Int).Value = DefaultCounty();
                cmd.ExecuteNonQuery();
            }

            // Insert the applicant information
            if (Primary != null)
            {
                Primary.xpr_insert_person(cn, txn, client, 1);
            }

            // Insert the co-applicant information
            if (Secondary != null && toBool(TakeCourseTogether))
            {
                Secondary.xpr_insert_person(cn, txn, client, 2);
            }

            return client;
        }
    }
}