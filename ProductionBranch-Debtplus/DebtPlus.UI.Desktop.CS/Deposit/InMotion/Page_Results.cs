﻿using System;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public partial class Page_Results : Page_Template
    {
        /// <summary>
        /// Batch ID for the report
        /// </summary>
        private Int32 Parameter_Batch
        {
            get
            {
                return MyParentForm.panelStatus.Parameter_Batch;
            }
        }

        public Page_Results(Form_InMotion MyParentForm)
            : base(MyParentForm)
        {
            InitializeComponent();
        }

        // Hook into the Next button so that it will launch the reports
        private void simpleButton_Next_Click(object sender, EventArgs e)
        {
            // Run the deposit batch report if desired
            if (chk_deposit_batch_report.Checked)
            {
                DebtPlus.Interfaces.Reports.IReports rpt = new DebtPlus.Reports.Deposits.Batch.DepositBatchReport();

                rpt.SetReportParameter("DepositBatchID", Parameter_Batch);
                if (rpt.RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
                {
                    rpt.RunReportInSeparateThread();
                }
            }

            // Run the deposit lockbox report if desired
            if (chk_print_lockbox_deposit_report.Checked)
            {
                DebtPlus.Interfaces.Reports.IReports rpt = new DebtPlus.Reports.Deposits.LockBox.DepositLockboxReport();
                rpt.SetReportParameter("DepositBatchID", Parameter_Batch);
                if (rpt.RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
                {
                    rpt.RunReportInSeparateThread();
                }
            }
        }

        /// <summary>
        /// Handle the starting condition for this page
        /// </summary>
        public override void Start()
        {
            base.Start();

            // Put the batch ID into the status message
            lbl_batch_id.Text = string.Format("Deposit Batch ID: {0:00000000}", Parameter_Batch);

            // Set the buttons to be processed
            MyParentForm.simpleButton_Back.Enabled = false;
            MyParentForm.simpleButton_Next.Enabled = true;
            MyParentForm.simpleButton_Next.Tag = Form_InMotion.Tag_Cancel;

            // Hook into the next button
            MyParentForm.simpleButton_Next.Click += new EventHandler(simpleButton_Next_Click);
        }
    }
}