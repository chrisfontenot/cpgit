﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    /// <summary>
    /// Class for storing personal information
    /// </summary>
    public class PersonalInfoType : MyObject
    {
        /// <summary>
        /// Name of the person
        /// </summary>
        public NameType Name { get; set; }

        /// <summary>
        /// Email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Bankruptcy information
        /// </summary>
        public BankruptcyType Bankruptcy { get; set; }

        /// <summary>
        /// List of telephone numbers for the client
        /// </summary>
        public System.Collections.Generic.List<PhoneType> Phones = new System.Collections.Generic.List<PhoneType>();

        /// <summary>
        /// Use the standard parser for the applicant/coapplicant information
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            this.Parser(ref rdr, this);
        }

        /// <summary>
        /// Process the special fields for this type.
        /// </summary>
        /// <param name="rdr"></param>
        /// <param name="ThisType"></param>
        protected override void ProcessFieldElement(ref XmlTextReader rdr, object ThisType)
        {
            switch (rdr.Name)
            {
                // Name of the client
                case "Name":
                    Name = new NameType();
                    Name.Parser(ref rdr);
                    break;

                // Bankruptcy information
                case "Bankruptcy":
                    Bankruptcy = new BankruptcyType();
                    Bankruptcy.Parser(ref rdr);
                    break;

                // List of telephone numbers
                case "Phones":
                    ParsePhones(ref rdr);
                    break;

                // All others are unknown at this point.
                default:
                    base.ProcessFieldElement(ref rdr, ThisType);
                    break;
            }
        }

        /// <summary>
        /// Parse the phones node since it holds the telephone number sub-nodes.
        /// </summary>
        /// <param name="rdr"></param>
        private void ParsePhones(ref XmlTextReader rdr)
        {
            while (rdr.Read())
            {
                if (rdr.NodeType == XmlNodeType.Element)
                {
                    if (string.Compare(rdr.Name, "Phone", false) == 0)
                    {
                        PhoneType phone = new PhoneType();
                        phone.Parser(ref rdr);
                        Phones.Add(phone);
                        continue;
                    }

                    // Skip the current unknown element.
                    rdr.Skip();
                }
                else if (rdr.NodeType == XmlNodeType.EndElement && rdr.Name == "Phones")
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Locate the home telephone number in the list of possible phone numbers
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public PhoneType FindTelephone(string Type = "R")
        {
            foreach (PhoneType phone in Phones)
            {
                if (phone.Type == Type)
                {
                    return phone;
                }
            }
            return null;
        }

        /// <summary>
        /// Insert the person reference into the database.
        /// </summary>
        /// <param name="cn">Database Connection object</param>
        /// <param name="txn">Transaction for the connection</param>
        /// <param name="Client">Current client number</param>
        /// <param name="Relation">Relationship (1 = self, <> 1 for others)</param>
        /// <returns></returns>
        public Int32 xpr_insert_person(SqlConnection cn, SqlTransaction txn, Int32 Client, Int32 Relation)
        {
            Int32 Person = -1;
            Int32 NameID = -1;
            Int32 WorkTelephoneID = -1;
            Int32 CellTelephoneID = -1;
            Int32 EmailID = -1;

            SqlDataReader rd = null;
            try
            {
                // Try to find the person in the system. If possible, update the fields.
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT TOP 1 Person, NameID, WorkTelephoneID, CellTelephoneID, EmailID FROM people WHERE client = @client AND ";
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Client;

                    // If the item is not the applicant then we don't really care what the relation is, just that it is not the primary applicant.
                    cmd.CommandText += (Relation == 1) ? "[Relation] = 1" : "[Relation] <> 1";

                    rd = cmd.ExecuteReader(CommandBehavior.SingleRow);
                }

                if (rd != null && rd.Read())
                {
                    if (!rd.IsDBNull(0)) Person = Convert.ToInt32(rd.GetValue(0));
                    if (!rd.IsDBNull(1)) NameID = Convert.ToInt32(rd.GetValue(1));
                    if (!rd.IsDBNull(2)) WorkTelephoneID = Convert.ToInt32(rd.GetValue(2));
                    if (!rd.IsDBNull(3)) CellTelephoneID = Convert.ToInt32(rd.GetValue(3));
                }
            }

            finally
            {
                if (rd != null)
                {
                    if (!rd.IsClosed)
                    {
                        rd.Close();
                    }
                    rd.Dispose();
                }
            }

            // Add the name
            if (NameID <= 0)
            {
                NameID = Name.xpr_insert_Name(cn, txn);
            }

            // Add the work telephone number
            PhoneType WorkTelephone = FindTelephone("B");
            if (WorkTelephoneID <= 0 && WorkTelephone != null)
            {
                WorkTelephoneID = WorkTelephone.xpr_insert_telephonenumber(cn, txn);
            }

            // Add the cell telephone number
            PhoneType CellTelephone = FindTelephone("C");
            if (CellTelephoneID <= 0 && CellTelephone != null)
            {
                CellTelephoneID = CellTelephone.xpr_insert_telephonenumber(cn, txn);
            }

            // Add the email address
            if (EmailID <= 0 && !string.IsNullOrEmpty(Email))
            {
                EmailID = xpr_insert_email(cn, txn);
            }

            // If there is no person then create it
            if (Person <= 0)
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "xpr_insert_people";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@Person", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@Client", SqlDbType.Int).Value = Client;
                    cmd.Parameters.Add("@Relation", SqlDbType.Int).Value = Relation;
                    cmd.Parameters.Add("@NameID", SqlDbType.Int).Value = NameID > 0 ? (object)NameID : DBNull.Value;
                    cmd.Parameters.Add("@WorkTelephoneID", SqlDbType.Int).Value = WorkTelephoneID > 0 ? (object)WorkTelephoneID : DBNull.Value;
                    cmd.Parameters.Add("@CellTelephoneID", SqlDbType.Int).Value = CellTelephoneID > 0 ? (object)CellTelephoneID : DBNull.Value;
                    cmd.Parameters.Add("@EmailID", SqlDbType.Int).Value = EmailID > 0 ? (object)EmailID : DBNull.Value;

                    cmd.ExecuteNonQuery();
                    Person = Convert.ToInt32(cmd.Parameters[0].Value);
                }
            }
            else
            {
                // Update the applicant information
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "UPDATE [People] SET [NameID]=@NameID,[WorkTelephoneID]=@WorkTelephoneID,[CellTelephoneID]=@CellTelephoneID,[EmailID]=@EmailID WHERE [Person]=@Person";
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@NameID", SqlDbType.Int).Value = NameID > 0 ? (object)NameID : DBNull.Value;
                    cmd.Parameters.Add("@WorkTelephoneID", SqlDbType.Int).Value = WorkTelephoneID > 0 ? (object)WorkTelephoneID : DBNull.Value;
                    cmd.Parameters.Add("@CellTelephoneID", SqlDbType.Int).Value = CellTelephoneID > 0 ? (object)CellTelephoneID : DBNull.Value;
                    cmd.Parameters.Add("@EmailID", SqlDbType.Int).Value = EmailID > 0 ? (object)EmailID : DBNull.Value;
                    cmd.Parameters.Add("@Person", SqlDbType.Int).Value = Person;

                    cmd.ExecuteNonQuery();
                }
            }

            return Person;
        }

        /// <summary>
        /// Create the Email entry
        /// </summary>
        /// <param name="cn">Database Connection object</param>
        /// <param name="txn">Transaction for the connection</param>
        /// <returns></returns>
        private Int32 xpr_insert_email(SqlConnection cn, SqlTransaction txn)
        {
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "xpr_insert_emailaddress";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@EmailID", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@Address", SqlDbType.VarChar, 512).Value = Email;
                cmd.Parameters.Add("@Validation", SqlDbType.Int).Value = 1;

                cmd.ExecuteNonQuery();
                return Convert.ToInt32(cmd.Parameters[0].Value);
            }
        }
    }
}