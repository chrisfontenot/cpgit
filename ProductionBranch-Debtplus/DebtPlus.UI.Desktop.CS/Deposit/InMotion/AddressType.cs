﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    /// <summary>
    /// Class for storing the address information
    /// </summary>
    public class AddressType : MyObject
    {
        /// <summary>
        /// House number
        /// </summary>
        public string Number { get; set; }

        /// <summary>
        /// Street direction
        /// </summary>
        public string Direction { get; set; }

        /// <summary>
        /// Street name
        /// </summary>
        public string StreetName { get; set; }

        /// <summary>
        /// Street type
        /// </summary>
        public string StreetType { get; set; }

        /// <summary>
        /// Apartment number
        /// </summary>
        public string Appartment { get; set; }

        /// <summary>
        /// Second line of address
        /// </summary>
        public string Address2 { get; set; }

        /// <summary>
        /// City name
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// State name
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Zip(postal) code
        /// </summary>
        public string Zip { get; set; }

        public string Zip5
        {
            get
            {
                if (!string.IsNullOrEmpty(Zip))
                {
                    return Zip.PadRight(5, '0').Substring(0, 5);
                }
                return Zip;
            }
        }

        /// <summary>
        /// Use the standard parser for the address
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            this.Parser(ref rdr, this);
        }

        /// <summary>
        /// Insert the telephone number and return the record pointer
        /// </summary>
        /// <param name="cn">Database Connection object</param>
        /// <param name="txn">Transaction for the connection</param>
        /// <returns></returns>
        public Int32 xpr_insert_Address(SqlConnection cn, SqlTransaction txn)
        {
            using (var bc = new DebtPlus.LINQ.BusinessContext(cn))
            {
                bc.Transaction = txn;

                // Translate the state abbreviation into a record pointer that we can use in our tables
                var qState = DebtPlus.LINQ.Cache.state.getList().Find(s => s.MailingCode == State);
                Int32 stateID = (qState == null) ? DebtPlus.LINQ.Cache.state.getDefault().GetValueOrDefault() : qState.Id;

                // Create an address record that has the proper fields defaulted. Replace those fields that we use with our values.
                var record = DebtPlus.LINQ.Factory.Manufacture_address();

                // Update the new record with the desired values. Make sure that we don't use null as the database won't like the fields being null.
                record.house = Number ?? string.Empty;
                record.direction = Direction ?? string.Empty;
                record.street = StreetName ?? string.Empty;
                record.suffix = StreetType ?? string.Empty;
                record.address_line_2 = Address2 ?? string.Empty;
                record.city = City ?? string.Empty;
                record.state = stateID;
                record.PostalCode = Zip ?? string.Empty;

                // Insert the new address and return the ID for the new record.
                bc.addresses.InsertOnSubmit(record);
                bc.SubmitChanges();

                return record.Id;
            }
        }
    }
}