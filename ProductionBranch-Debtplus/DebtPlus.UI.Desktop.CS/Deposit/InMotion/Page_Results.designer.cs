﻿namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    partial class Page_Results
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Page_Results));
            this.lbl_batch_id = new DevExpress.XtraEditors.LabelControl();
            this.Label10 = new DevExpress.XtraEditors.LabelControl();
            this.chk_print_lockbox_deposit_report = new DevExpress.XtraEditors.CheckEdit();
            this.chk_deposit_batch_report = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_print_lockbox_deposit_report.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_deposit_batch_report.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_batch_id
            // 
            this.lbl_batch_id.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_batch_id.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_batch_id.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_batch_id.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_batch_id.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_batch_id.Location = new System.Drawing.Point(3, 3);
            this.lbl_batch_id.Name = "lbl_batch_id";
            this.lbl_batch_id.Size = new System.Drawing.Size(356, 20);
            this.lbl_batch_id.TabIndex = 19;
            this.lbl_batch_id.Text = "Deposit Batch ID: 00000000";
            this.lbl_batch_id.UseMnemonic = false;
            // 
            // Label10
            // 
            this.Label10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Label10.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.Label10.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Label10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label10.Location = new System.Drawing.Point(3, 29);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(359, 91);
            this.Label10.TabIndex = 20;
            this.Label10.Text = resources.GetString("Label10.Text");
            this.Label10.UseMnemonic = false;
            // 
            // chk_print_lockbox_deposit_report
            // 
            this.chk_print_lockbox_deposit_report.Location = new System.Drawing.Point(3, 126);
            this.chk_print_lockbox_deposit_report.Name = "chk_print_lockbox_deposit_report";
            this.chk_print_lockbox_deposit_report.Properties.Caption = "Print Lockbox deposit Report";
            this.chk_print_lockbox_deposit_report.Size = new System.Drawing.Size(248, 19);
            this.chk_print_lockbox_deposit_report.TabIndex = 23;
            // 
            // chk_deposit_batch_report
            // 
            this.chk_deposit_batch_report.Location = new System.Drawing.Point(3, 151);
            this.chk_deposit_batch_report.Name = "chk_deposit_batch_report";
            this.chk_deposit_batch_report.Properties.Caption = "Print Deposit Batch Report";
            this.chk_deposit_batch_report.Size = new System.Drawing.Size(248, 19);
            this.chk_deposit_batch_report.TabIndex = 24;
            // 
            // Page_Results
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chk_print_lockbox_deposit_report);
            this.Controls.Add(this.chk_deposit_batch_report);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.lbl_batch_id);
            this.Name = "Page_Results";
            this.Size = new System.Drawing.Size(362, 182);
            ((System.ComponentModel.ISupportInitialize)(this.chk_print_lockbox_deposit_report.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chk_deposit_batch_report.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal DevExpress.XtraEditors.LabelControl lbl_batch_id;
        internal DevExpress.XtraEditors.LabelControl Label10;
        internal DevExpress.XtraEditors.CheckEdit chk_print_lockbox_deposit_report;
        internal DevExpress.XtraEditors.CheckEdit chk_deposit_batch_report;
    }
}
