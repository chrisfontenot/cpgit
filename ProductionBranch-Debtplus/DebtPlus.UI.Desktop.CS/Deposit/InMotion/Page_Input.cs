﻿using System;
using System.Data;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public partial class Page_Input : Page_Template
    {
        public Page_Input(Form_InMotion MyParentForm)
            : base(MyParentForm)
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// The bank account selected in the KeyField form
        /// </summary>
        public Int32 Parameter_Bank
        {
            get
            {
                return (Int32)lookUpEdit_Bank.EditValue;
            }
        }

        /// <summary>
        /// The label associated with the deposit batch
        /// </summary>
        public string Parameter_Label
        {
            get
            {
                string Value = (string)textEdit_BatchLabel.EditValue;
                if (string.IsNullOrEmpty(Value))
                {
                    return string.Empty;
                }
                return Value.Trim();
            }
        }

        /// <summary>
        /// Process the starting condition for the panel.
        /// </summary>
        public override void Start()
        {
            base.Start();

            // Set the button status for the panel
            MyParentForm.simpleButton_Back.Enabled = false;
            MyParentForm.simpleButton_Back.Tag = string.Empty;
            MyParentForm.simpleButton_Next.Tag = Form_InMotion.Tag_Status;

            // Load the list of banks into the control
            lookUpEdit_Bank.Properties.DataSource = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.ActiveFlag && (s.type == "D" || s.type == "C"));
            lookUpEdit_Bank.EditValue = DebtPlus.LINQ.Cache.bank.getDefault("C") ?? DebtPlus.LINQ.Cache.bank.getDefault("D");

            // Enable the button if valid
            MyParentForm.simpleButton_Next.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the condition where the form is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_EditValueChanged(object sender, EventArgs e)
        {
            MyParentForm.simpleButton_Next.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the KeyField form has errors that would prevent the Next button from being selected.
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            return (lookUpEdit_Bank.EditValue == null);
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            lookUpEdit_Bank.EditValueChanged += new EventHandler(Form_EditValueChanged);
            textEdit_BatchLabel.EditValueChanged += new EventHandler(Form_EditValueChanged);
        }

        /// <summary>
        /// Remove the registration for the events
        /// </summary>
        private void UnRegisterHandlers()
        {
            lookUpEdit_Bank.EditValueChanged -= new EventHandler(Form_EditValueChanged);
            textEdit_BatchLabel.EditValueChanged -= new EventHandler(Form_EditValueChanged);
        }
    }
}