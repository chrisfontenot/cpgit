﻿namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    partial class Page_Status
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl_dollarAmount = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_mim_items = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_cim_items = new DevExpress.XtraEditors.LabelControl();
            this.marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_processed_items = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_dollarAmount
            // 
            this.labelControl_dollarAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_dollarAmount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_dollarAmount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_dollarAmount.Location = new System.Drawing.Point(246, 100);
            this.labelControl_dollarAmount.Name = "labelControl_dollarAmount";
            this.labelControl_dollarAmount.Size = new System.Drawing.Size(127, 13);
            this.labelControl_dollarAmount.TabIndex = 7;
            this.labelControl_dollarAmount.Text = "$0.00";
            // 
            // labelControl_mim_items
            // 
            this.labelControl_mim_items.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_mim_items.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_mim_items.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_mim_items.Location = new System.Drawing.Point(246, 36);
            this.labelControl_mim_items.Name = "labelControl_mim_items";
            this.labelControl_mim_items.Size = new System.Drawing.Size(127, 13);
            this.labelControl_mim_items.TabIndex = 6;
            this.labelControl_mim_items.Text = "0";
            // 
            // labelControl_cim_items
            // 
            this.labelControl_cim_items.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_cim_items.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_cim_items.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_cim_items.Location = new System.Drawing.Point(246, 4);
            this.labelControl_cim_items.Name = "labelControl_cim_items";
            this.labelControl_cim_items.Size = new System.Drawing.Size(127, 13);
            this.labelControl_cim_items.TabIndex = 5;
            this.labelControl_cim_items.Text = "0";
            // 
            // marqueeProgressBarControl1
            // 
            this.marqueeProgressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.marqueeProgressBarControl1.EditValue = 0;
            this.marqueeProgressBarControl1.Location = new System.Drawing.Point(4, 129);
            this.marqueeProgressBarControl1.Name = "marqueeProgressBarControl1";
            this.marqueeProgressBarControl1.Size = new System.Drawing.Size(369, 18);
            this.marqueeProgressBarControl1.TabIndex = 4;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(4, 100);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(141, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Dollar amount of transactions";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(4, 36);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(165, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Number of Money-In-Motion Items";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(185, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Number of Counseling-In-Motion Items";
            // 
            // labelControl_processed_items
            // 
            this.labelControl_processed_items.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_processed_items.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl_processed_items.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_processed_items.Location = new System.Drawing.Point(246, 68);
            this.labelControl_processed_items.Name = "labelControl_processed_items";
            this.labelControl_processed_items.Size = new System.Drawing.Size(127, 13);
            this.labelControl_processed_items.TabIndex = 9;
            this.labelControl_processed_items.Text = "0";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(4, 68);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(145, 13);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Number of deposits processed";
            // 
            // Page_Status
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControl_processed_items);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl_dollarAmount);
            this.Controls.Add(this.labelControl_mim_items);
            this.Controls.Add(this.labelControl_cim_items);
            this.Controls.Add(this.marqueeProgressBarControl1);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "Page_Status";
            this.Size = new System.Drawing.Size(376, 150);
            ((System.ComponentModel.ISupportInitialize)(this.marqueeProgressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.MarqueeProgressBarControl marqueeProgressBarControl1;
        private DevExpress.XtraEditors.LabelControl labelControl_dollarAmount;
        private DevExpress.XtraEditors.LabelControl labelControl_mim_items;
        private DevExpress.XtraEditors.LabelControl labelControl_cim_items;
        private DevExpress.XtraEditors.LabelControl labelControl_processed_items;
        private DevExpress.XtraEditors.LabelControl labelControl5;
    }
}
