﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    /// <summary>
    /// Class for storing a telephone number
    /// </summary>
    public class PhoneType : MyObject
    {
        public PhoneType()
            : base()
        {
            Type = "R";
            DoNotCall = "N";
        }

        /// <summary>
        /// Type of telephone number. R=Residence, B=Business, C=Cell
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Area code number
        /// </summary>
        public string AreaCode { get; set; }

        private string m_LocalNumber = null;

        /// <summary>
        /// Local number
        /// </summary>
        public string LocalNumber
        {
            get
            {
                return m_LocalNumber;
            }

            set
            {
                // Try to use the number as a telephone number if possible. if the field is 7 digits or 10 digits then
                // accept it as the telephone number. 10 digits would include the area code field.
                System.Text.RegularExpressions.Match matches = System.Text.RegularExpressions.Regex.Match(value, @"^(?<number>([0-9]{1,7})|([0-9]{3}-[0-9]{4}))(\s*(extension\.|extension|hud9902SectionFormatter\.|hud9902SectionFormatter|ext\.|ext)\s*(?<ext>\d+))?$", System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (!matches.Success)
                {
                    matches = System.Text.RegularExpressions.Regex.Match(value, @"^(\+(?<country>\d+))?\s*(?<acode>\d\d\d)?[ -]?(?<number>[0-9][-0-9]+)(\s*(extension\.|extension|hud9902SectionFormatter\.|hud9902SectionFormatter|ext\.|ext)\s*(?<ext>\d+))?$", System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (!matches.Success)
                    {
                        matches = System.Text.RegularExpressions.Regex.Match(value, @"^(\+(?<country>\d+))?\s*(\((?<acode>\d+)\)\s*?|(?<acode>\d+)\-|(?<acode>\d+)\s|(?<acode>\d+)\.)?[ -]?(?<number>[0-9][-0-9]+)(\s*(extension\.|extension|hud9902SectionFormatter\.|hud9902SectionFormatter|ext\.|ext)\s*(?<ext>\d+))?$", System.Text.RegularExpressions.RegexOptions.CultureInvariant | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    }
                }

                // if there is no match then do the form validation
                if (matches.Success)
                {
                    if (matches.Groups["acode"] != null)
                    {
                        AreaCode = matches.Groups["acode"].Value.Trim();
                    }
                    else
                    {
                        AreaCode = string.Empty;
                    }

                    // Obtain the fields from the expression search
                    m_LocalNumber = matches.Groups["number"].Value;
                    Extension = matches.Groups["ext"].Value;
                }
                else
                {
                    m_LocalNumber = value;
                }
            }
        }

        /// <summary>
        /// Extension
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Do not call this number if "Y".
        /// </summary>
        public string DoNotCall { get; set; }

        /// <summary>
        /// Use the standard parser for the phone number
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            this.Parser(ref rdr, this);
        }

        /// <summary>
        /// Insert the telephone number and return the record pointer
        /// </summary>
        /// <param name="cn">Database Connection object</param>
        /// <param name="txn">Transaction for the connection</param>
        /// <returns></returns>
        public Int32 xpr_insert_telephonenumber(SqlConnection cn, SqlTransaction txn)
        {
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "xpr_insert_TelephoneNumbers";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@TelephoneNumber", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@Country", SqlDbType.Int).Value = DefaultCountry(cn, txn);
                cmd.Parameters.Add("@Acode", SqlDbType.VarChar, 10).Value = AreaCode;
                cmd.Parameters.Add("@Number", SqlDbType.VarChar, 40).Value = LocalNumber;
                cmd.Parameters.Add("@Ext", SqlDbType.VarChar, 10).Value = Extension;

                cmd.ExecuteNonQuery();

                return Convert.ToInt32(cmd.Parameters[0].Value);
            }
        }

        /// <summary>
        /// Return the default country code
        /// </summary>
        /// <param name="cn">Database Connection object</param>
        /// <param name="txn">Transaction for the connection</param>
        /// <returns></returns>
        private Int32 DefaultCountry(SqlConnection cn, SqlTransaction txn)
        {
            Int32 Answer = -1;

            // Look for the item marked DEFAULT.
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "SELECT min(country) FROM countries WHERE [Default] <> 0";
                cmd.CommandType = CommandType.Text;

                object obj = cmd.ExecuteScalar();
                if (obj != null && obj != DBNull.Value)
                {
                    Answer = Convert.ToInt32(obj);
                }
            }

            if (Answer <= 0)
            {
                // Look for the first country code
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "SELECT min(country) FROM countries";
                    cmd.CommandType = CommandType.Text;

                    object obj = cmd.ExecuteScalar();
                    if (obj != null && obj != DBNull.Value)
                    {
                        Answer = Convert.ToInt32(obj);
                    }
                }
            }

            return Answer;
        }
    }
}