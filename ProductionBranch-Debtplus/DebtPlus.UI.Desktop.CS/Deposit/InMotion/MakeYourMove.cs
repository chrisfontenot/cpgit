﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public class MakeYourMove : InMotion
    {
        /// <summary>
        /// Initialize the class.
        /// </summary>
        public MakeYourMove()
            : base()
        {
            SpouseJointFiler = "N";
            LivingInSameHousehold = "N";
        }

        /// <summary>
        /// "Y" for spouse is a joint filer. "N" otherwise
        /// </summary>
        public string SpouseJointFiler { get; set; }

        /// <summary>
        /// "Y" if the spouse is living in the same KeyField. "N" otherwise.
        /// </summary>
        public string LivingInSameHousehold { get; set; }

        /// <summary>
        /// Parse the XML document
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            Parser(ref rdr, this);
        }

        /// <summary>
        /// Implement the saving of this item to the database
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <param name="Parameter_Bank">Bank number</param>
        /// <param name="Parameter_Batch">Batch number</param>
        public override void RecordTransaction(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, Int32 Parameter_Batch)
        {
            // First, if there is a client then we can just use it.
            Int32 client = -1;
            Int32.TryParse(ClientID, out client);

            if (client <= 0)
            {
                client = FindClient(cn, txn);
            }

            if (client <= 0)
            {
                client = CreateClient(cn, txn);
            }

            // Add the debt
            CreateDebt(cn, txn, client, "V68496", false);

            // Add the client note
            SystemNote(cn, txn, client);

            // Record the deposit transaction
            CreateDeposit(cn, txn, client, Parameter_Batch, "VA", "MYM");
        }

        /// <summary>
        /// Create a client in the system
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <returns></returns>
        protected virtual Int32 CreateClient(SqlConnection cn, SqlTransaction txn)
        {
            Int32 HomeTelephoneID = -1;

            // Find the home telephone pointer to be used for the client
            PhoneType phone = Primary.FindTelephone();
            if (phone == null && Secondary != null && toBool(LivingInSameHousehold))
            {
                phone = Secondary.FindTelephone();
            }

            if (phone != null)
            {
                HomeTelephoneID = phone.xpr_insert_telephonenumber(cn, txn);
            }

            // Allocate a client
            Int32 client = xpr_create_client(cn, txn, HomeTelephoneID);

            // Update the client's address information
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "UPDATE [clients] SET [AddressID]=@AddressID,[County]=@County WHERE [client]=@Client";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@AddressID", SqlDbType.Int).Value = Address.xpr_insert_Address(cn, txn);
                cmd.Parameters.Add("@Client", SqlDbType.Int).Value = client;
                cmd.Parameters.Add("@County", SqlDbType.Int).Value = DefaultCounty();
                cmd.ExecuteNonQuery();
            }

            // Insert the applicant information
            if (Primary != null)
            {
                Primary.xpr_insert_person(cn, txn, client, 1);
            }

            // Insert the co-applicant information
            if (Secondary != null && toBool(LivingInSameHousehold))
            {
                Secondary.xpr_insert_person(cn, txn, client, 2);
            }

            return client;
        }
    }
}