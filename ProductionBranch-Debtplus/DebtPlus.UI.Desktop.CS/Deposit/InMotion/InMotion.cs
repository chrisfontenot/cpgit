using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    /// <summary>
    /// Class to hold the Money-In-Motion records
    /// </summary>
    public abstract class InMotion : MyObject
    {
        /// <summary>
        /// Class for holding the InMotion objects. This is either Money-In-Motion or Counseling-In-Motion.
        /// </summary>
        /// <param name="rdr"></param>
        public InMotion()
        {
            Type = "Single";
            RegistrationFrequency = "Single";
            Amount = "0";
            RegistrationDate = null;
            ClassType = null;
            ClientID = null;

            Primary = null;
            Secondary = null;
            Address = null;
        }

        /// <summary>
        /// Type of processing. This is either Single or Joint.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Registration frequency. This is either Single or Multiple.
        /// </summary>
        public string RegistrationFrequency { get; set; }

        /// <summary>
        /// Date for the registration.
        /// </summary>
        public string RegistrationDate { get; set; }

        /// <summary>
        /// Class type. This is either Online or InPerson
        /// </summary>
        public string ClassType { get; set; }

        /// <summary>
        /// Client ID if one is present.
        /// </summary>
        public string ClientID { get; set; }

        /// <summary>
        /// Dollar amount
        /// </summary>
        public string Amount { get; set; }

        public decimal AmountValue
        {
            get
            {
                decimal value;
                if (decimal.TryParse(Amount, out value))
                {
                    return value;
                }
                return 0M;
            }
        }

        /// <summary>
        /// Primary (applicant) information
        /// </summary>
        public PersonalInfoType Primary { get; set; }

        /// <summary>
        /// Secondary (co-applicant) information
        /// </summary>
        public PersonalInfoType Secondary { get; set; }

        /// <summary>
        /// Address information
        /// </summary>
        public AddressType Address { get; set; }

        /// <summary>
        /// Process the parsing of the group. Just use the default parser.
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            // Just do the standard parser for this structure.
            this.Parser(ref rdr, this);
        }

        /// <summary>
        /// Override the field processing since we want to handle some fields specially.
        /// </summary>
        /// <param name="rdr"></param>
        /// <param name="ThisType"></param>
        protected override void ProcessFieldElement(ref XmlTextReader rdr, object ThisType)
        {
            switch (rdr.Name)
            {
                // Look for the <Primary> group. Parse this as a sub-structure
                case "Primary":
                    Primary = new PersonalInfoType();
                    Primary.Parser(ref rdr);
                    break;

                // Look for the <Secondary> group. Parse this as a sub-structure
                case "Secondary":
                    Secondary = new PersonalInfoType();
                    Secondary.Parser(ref rdr);
                    break;

                // Look for the <Address> group. Parse this as a sub-structure
                case "Address":
                    Address = new AddressType();
                    Address.Parser(ref rdr);
                    break;

                // Pass everything else up the chain.
                default:
                    base.ProcessFieldElement(ref rdr, ThisType);
                    break;
            }
        }

        /// <summary>
        /// Record the transaction to the database. This is a required procedure for the class
        /// and must be implemented in both the CounselingInMotion and MoneyInMotion classes.
        /// </summary>
        /// <param name="cn">Database Connection object</param>
        /// <param name="txn">Transaction for the connection</param>
        /// <param name="Parameter_Bank">Bank number</param>
        /// <param name="Parameter_Batch">Batch number</param>
        public abstract void RecordTransaction(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, Int32 Parameter_Batch);

        /// <summary>
        /// Find the client in the system based upon the parameters given for the new client
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <returns></returns>
        protected virtual Int32 FindClient(SqlConnection cn, SqlTransaction txn)
        {
            Int32 client = -1;
            using (var cmd = new SqlCommand())
            {
                string Statement = string.Empty;

                // Find the name fields from the primary account
                if (Primary != null)
                {
                    if (Primary.Name != null)
                    {
                        if (!string.IsNullOrEmpty(Primary.Name.Last))
                        {
                            Statement += " AND [LastName]=@LastName";
                            cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 80).Value = Primary.Name.Last;
                        }

                        if (!string.IsNullOrEmpty(Primary.Name.First))
                        {
                            Statement += " AND [FirstName]=@FirstName";
                            cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 80).Value = Primary.Name.First;
                        }
                    }
                }

                // A name is required.
                if (Statement != string.Empty)
                {
                    // If there is a postal code then use it too
                    if (Address != null)
                    {
                        if (!string.IsNullOrEmpty(Address.Zip5))
                        {
                            Statement += " AND LEFT([ZipCode],5) = @PostalCode";
                            cmd.Parameters.Add("@PostalCode", SqlDbType.VarChar, 10).Value = Address.Zip5;
                        }
                    }

                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "SELECT TOP 1 [client] FROM [view_client_address] WITH (NOLOCK) WHERE " + Statement.Substring(5);
                    cmd.CommandType = CommandType.Text;

                    object objAnswer = cmd.ExecuteScalar();
                    if (objAnswer != null && objAnswer != System.DBNull.Value)
                    {
                        client = Convert.ToInt32(objAnswer);
                    }
                }

                return client;
            }
        }

        /// <summary>
        /// Create a client by inserting a row into the clients table
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <returns></returns>
        protected Int32 xpr_create_client(SqlConnection cn, SqlTransaction txn, Int32 HomeTelephoneID)
        {
            // Issue the database request to insert the client into the tables
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "xpr_create_client";
                cmd.CommandType = CommandType.StoredProcedure;

                // The client is the result
                cmd.Parameters.Add("@client", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                // Parameters for the client.
                cmd.Parameters.Add("@HomeTelephoneID", SqlDbType.Int).Value = HomeTelephoneID > 0 ? (object)HomeTelephoneID : DBNull.Value;
                cmd.Parameters.Add("@mortgage_problems", SqlDbType.Bit).Value = false;
                cmd.Parameters.Add("@referral_code", SqlDbType.Int).Value = DefaultReferralCode();
                cmd.Parameters.Add("@method_first_contact", SqlDbType.Int).Value = DefaultFirstContact();
                cmd.Parameters.Add("@bankruptcy", SqlDbType.Bit).Value = false;
                cmd.ExecuteNonQuery();

                return Convert.ToInt32(cmd.Parameters[0].Value);
            }
        }

        /// <summary>
        /// Return the default value for the referral source code.
        /// </summary>
        /// <returns></returns>
        protected object DefaultReferralCode()
        {
            return 574; // Bankruptcy
        }

        /// <summary>
        /// Return the default value for the first contact method type
        /// </summary>
        /// <returns></returns>
        protected object DefaultFirstContact()
        {
            return 5;  // Internet
        }

        /// <summary>
        /// Return the county location from the primary applicant.
        /// </summary>
        /// <returns></returns>
        protected object DefaultCounty()
        {
            // Find the name of the county from the primary applicant.
            string countyName = Primary != null && Primary.Bankruptcy != null && !string.IsNullOrEmpty(Primary.Bankruptcy.County) ? Primary.Bankruptcy.County : string.Empty;
            object answer = 1;

            // Allocate a connection object and open it
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;

                    // Find the county by the name in the tables
                    if (countyName != string.Empty)
                    {
                        cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = countyName;
                        cmd.CommandText = "SELECT TOP 1 county FROM counties WITH (NOLOCK) WHERE name = @name UNION ALL ";
                    }

                    cmd.CommandText += "SELECT County FROM Counties WITH (NOLOCK) WHERE [Default] = 1 UNION ALL SELECT min(county) FROM Counties WITH (NOLOCK) UNION ALL SELECT 1 AS County";
                    answer = cmd.ExecuteScalar();
                    if (answer == null)
                    {
                        answer = System.DBNull.Value;
                    }
                }
            }

            finally
            {
                // Close and dispose of the connection
                if (cn != null)
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                    cn.Dispose();
                }
            }

            return answer;
        }

        /// <summary>
        /// Create the deposit record in the batch tables
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        /// <param name="client"></param>
        /// <param name="Batch"></param>
        /// <param name="TranType"></param>
        /// <param name="Reference"></param>
        protected void CreateDeposit(SqlConnection cn, SqlTransaction txn, Int32 client, Int32 Batch, string TranType, string Reference)
        {
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "INSERT INTO [deposit_batch_details] (deposit_batch_id, ok_to_post, tran_subtype, scanned_client, client, amount, item_date, reference, message, created_by, date_created, date_posted) select @batch, 1, @tran_type, @client, @client, @amount, @scanned_date, @reference, null, suser_sname(), getdate(), null; UPDATE [clients] SET deposit_in_trust = dbo.DepositsInTrust(client) WHERE client = @client;";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("@batch", SqlDbType.Int).Value = Batch;
                cmd.Parameters.Add("@client", SqlDbType.Int, 0).Value = client;
                cmd.Parameters.Add("@scanned_date", SqlDbType.DateTime).Value = DateTime.Now.Date;
                cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = Amount;
                cmd.Parameters.Add("@reference", SqlDbType.VarChar, 50).Value = Reference;
                cmd.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 50).Value = DBNull.Value;
                cmd.Parameters.Add("@tran_type", SqlDbType.VarChar, 2).Value = TranType;

                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Create a system note with the bankruptcy information
        /// </summary>
        /// <param name="cn">Database connection object</param>
        /// <param name="txn">Connection object's transaction</param>
        /// <param name="client">Client ID</param>
        protected void SystemNote(SqlConnection cn, SqlTransaction txn, Int32 client)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // Create the information for the bankruptcy note
            if (Primary != null && Primary.Bankruptcy != null)
            {
                Primary.Bankruptcy.CreateSystemNote("Applicant", ref sb);
            }

            if (Secondary != null && Secondary.Bankruptcy != null)
            {
                Secondary.Bankruptcy.CreateSystemNote("CoApplicant", ref sb);
            }

            if (sb.Length > 0)
            {
                sb.Insert(0, "Additional information received relating to bankruptcy" + Environment.NewLine);
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "INSERT INTO client_notes (client,type,note,subject) VALUES (@client,1,@note,'Bankruptcy Payment Received')";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = client;
                    cmd.Parameters.Add("@note", SqlDbType.Text).Value = sb.ToString();
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Create the debt for the indicated creditor using the amount of the deposit as the balance.
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        /// <param name="Creditor"></param>
        /// <param name="PutOnHold"></param>
        protected void CreateDebt(SqlConnection cn, SqlTransaction txn, Int32 client, string Creditor, Boolean PutOnHold)
        {
            Int32 client_creditor = -1;
            Int32 client_creditor_balance = -1;
            Int32 client_creditor_proposal = -1;

            // If there is a debt then use it.
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "SELECT [client_creditor] FROM [client_creditor] WITH (NOLOCK) WHERE [client]=@client AND [creditor]=@creditor AND [reassigned_debt]=0";
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@client", SqlDbType.Int).Value = client;
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = Creditor;

                object objAnswer = cmd.ExecuteScalar();
                if (objAnswer != null && objAnswer != DBNull.Value)
                {
                    client_creditor = Convert.ToInt32(objAnswer);
                }
            }

            // If there is no debt then create one
            if (client_creditor < 0)
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "xpr_create_debt";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = client;
                    cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = Creditor;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = String.Format("{0:0000000}", client);

                    cmd.ExecuteNonQuery();
                    client_creditor = Convert.ToInt32(cmd.Parameters[0].Value);
                }
            }

            // Read the table pointers from the new debt. We need a proposal and balance pointer
            SqlDataReader rd = null;
            try
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "SELECT client_creditor_proposal,client_creditor_balance FROM client_creditor WITH (NOLOCK) WHERE client_creditor=@client_creditor";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = client_creditor;
                    rd = cmd.ExecuteReader(CommandBehavior.SingleRow);
                }

                if (rd != null && rd.Read())
                {
                    client_creditor_proposal = rd.GetInt32(0);
                    client_creditor_balance = rd.GetInt32(1);
                }
            }
            finally
            {
                if (rd != null)
                {
                    if (!rd.IsClosed)
                    {
                        rd.Close();
                    }
                    rd.Dispose();
                    rd = null;
                }
            }

            // Set the balance information
            if (client_creditor_balance > 0)
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "UPDATE [client_creditor_balances] SET [total_sched_payment]=@Amount,[current_sched_payment]=@Amount";
                    cmd.CommandText += ",[orig_balance]=[orig_balance]+@Amount";
                    cmd.CommandText += " WHERE [client_creditor_balance]=@client_creditor_balance";
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@client_creditor_balance", SqlDbType.Int).Value = client_creditor_balance;
                    cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = Amount;
                    cmd.ExecuteNonQuery();
                }
            }

            // Set the disbursement information
            if (client_creditor > 0)
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "UPDATE client_creditor SET [disbursement_factor]=[disbursement_factor]+@Amount,[sched_payment]=[sched_payment]+@Amount,[start_date]=getdate()";
                    if( PutOnHold )
                    {
                        cmd.CommandText += ",[hold_disbursements]=1";
                    }
                    cmd.CommandText += " WHERE [client_creditor]=@client_creditor";
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = client_creditor;
                    cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = Amount;
                    cmd.ExecuteNonQuery();
                }
            }

            // Correct the proposal information
            if (client_creditor_proposal > 0)
            {
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "UPDATE [client_creditor_proposals] SET [proposal_status]=3,[proposed_amount]=@Amount,[proposed_start_date]=getdate(),[proposed_balance]=@Amount WHERE [client_creditor_proposal]=@client_creditor_proposal";
                    cmd.CommandType = CommandType.Text;

                    cmd.Parameters.Add("@client_creditor_proposal", SqlDbType.Int).Value = client_creditor_proposal;
                    cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = Amount;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}