﻿namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    partial class Page_Input
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Page_Input));
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_Bank = new DevExpress.XtraEditors.LookUpEdit();
            this.textEdit_BatchLabel = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Bank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_BatchLabel.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(4, 4);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(334, 84);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = resources.GetString("labelControl1.Text");
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(4, 105);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(23, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "Bank";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(4, 136);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(55, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Batch Label";
            // 
            // lookUpEdit_Bank
            // 
            this.lookUpEdit_Bank.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_Bank.Location = new System.Drawing.Point(73, 102);
            this.lookUpEdit_Bank.Name = "lookUpEdit_Bank";
            this.lookUpEdit_Bank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_Bank.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Bank", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_Bank.Properties.DisplayMember = "description";
            this.lookUpEdit_Bank.Properties.NullText = "Choose one from the following list...";
            this.lookUpEdit_Bank.Properties.ShowFooter = false;
            this.lookUpEdit_Bank.Properties.ValueMember = "Id";
            this.lookUpEdit_Bank.Properties.SortColumnIndex = 1;
            this.lookUpEdit_Bank.Size = new System.Drawing.Size(246, 20);
            this.lookUpEdit_Bank.TabIndex = 3;
            this.lookUpEdit_Bank.ToolTip = "Select the bank account into which the deposit is to be made.";
            // 
            // textEdit_BatchLabel
            // 
            this.textEdit_BatchLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textEdit_BatchLabel.Location = new System.Drawing.Point(73, 133);
            this.textEdit_BatchLabel.Name = "textEdit_BatchLabel";
            this.textEdit_BatchLabel.Properties.Mask.BeepOnError = true;
            this.textEdit_BatchLabel.Properties.Mask.EditMask = "\\S.*";
            this.textEdit_BatchLabel.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_BatchLabel.Properties.MaxLength = 50;
            this.textEdit_BatchLabel.Size = new System.Drawing.Size(246, 20);
            this.textEdit_BatchLabel.TabIndex = 4;
            this.textEdit_BatchLabel.ToolTip = "Select the bank account into which the deposit is to be made.";
            // 
            // Page_Input
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textEdit_BatchLabel);
            this.Controls.Add(this.lookUpEdit_Bank);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Name = "Page_Input";
            this.Size = new System.Drawing.Size(341, 182);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_Bank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_BatchLabel.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_Bank;
        protected internal DevExpress.XtraEditors.TextEdit textEdit_BatchLabel;
    }
}
