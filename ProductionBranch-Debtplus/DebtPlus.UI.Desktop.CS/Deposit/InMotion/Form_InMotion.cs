﻿using System;
using System.Data;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public partial class Form_InMotion : DebtPlus.Data.Forms.DebtPlusForm
    {
        // IDs of the various button tags
        public const string Tag_Cancel = "Cancel";

        public const string Tag_Input = "Input";
        public const string Tag_Results = "Results";
        public const string Tag_Status = "Status";
        public ArgParser ap;

        // For now, just declare the database connection object
        public DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

        public DataSet ds = new DataSet("ds");

        public Form_InMotion(ArgParser ap)
            : this()
        {
            this.ap = ap;
        }

        public Form_InMotion()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            simpleButton_Back.Click += new EventHandler(simpleButton_Click);
            simpleButton_Next.Click += new EventHandler(simpleButton_Click);
            simpleButton_Cancel.Click += new EventHandler(simpleButton_Click);
            this.Load += new EventHandler(Form_Lockbox_Load);
        }

        private void DeRegisterHandlers()
        {
            simpleButton_Back.Click -= new EventHandler(simpleButton_Click);
            simpleButton_Next.Click -= new EventHandler(simpleButton_Click);
            simpleButton_Cancel.Click -= new EventHandler(simpleButton_Click);
            this.Load -= new EventHandler(Form_Lockbox_Load);
        }

        /// <summary>
        /// Process the button event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void simpleButton_Click(object sender, EventArgs e)
        {
            // Look at the tag for the button. It determines the button meaning.
            string TagValue = (string)((DevExpress.XtraEditors.SimpleButton)sender).Tag;
            if (!string.IsNullOrEmpty(TagValue))
            {
                switch (TagValue)
                {
                    case Tag_Cancel:
                        Close();
                        break;

                    case Tag_Input:
                        Show_Input();
                        break;

                    case Tag_Results:
                        Show_Results();
                        break;

                    case Tag_Status:
                        Show_Status();
                        break;

                    default:
                        throw new ApplicationException("Invalid button tag");
                }
            }
        }

        // The panels to be shown
        public Page_Input panelInput;

        public Page_Results panelResults;
        public Page_Status panelStatus;

        private void Form_Lockbox_Load(object sender, EventArgs e)
        {
            // Create our panels
            panelInput = new Page_Input(this);
            panelResults = new Page_Results(this);
            panelStatus = new Page_Status(this);

            // Tag the cancel button correctly.
            simpleButton_Cancel.Tag = Tag_Cancel;

            // Start with the first KeyField panel
            Show_Input();
        }

        /// <summary>
        /// Display the KeyField panel
        /// </summary>
        private void Show_Input()
        {
            // Configure the panels correctly
            panelControl_Main.Controls.Clear();
            panelControl_Main.Controls.Add(panelInput);
            panelInput.Dock = DockStyle.None;
            panelInput.Dock = DockStyle.Fill;
            panelInput.Visible = true;

            // Call the initial processing vector to bind the buttons
            panelInput.Start();
        }

        /// <summary>
        /// Display the results panel
        /// </summary>
        private void Show_Results()
        {
            // Configure the panels correctly
            panelControl_Main.Controls.Clear();
            panelControl_Main.Controls.Add(panelResults);
            panelResults.Dock = DockStyle.None;
            panelResults.Dock = DockStyle.Fill;
            panelResults.Visible = true;

            // Call the initial processing vector to bind the buttons
            panelResults.Start();
        }

        /// <summary>
        /// Display the status panel
        /// </summary>
        private void Show_Status()
        {
            // Configure the panels correctly
            panelControl_Main.Controls.Clear();
            panelControl_Main.Controls.Add(panelStatus);
            panelStatus.Dock = DockStyle.None;
            panelStatus.Dock = DockStyle.Fill;
            panelStatus.Visible = true;

            // Call the initial processing vector to bind the buttons
            panelStatus.Start();
        }
    }
}