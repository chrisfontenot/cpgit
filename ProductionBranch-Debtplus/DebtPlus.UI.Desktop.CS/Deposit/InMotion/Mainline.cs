﻿namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    /// <summary>
    /// Class to parse the argument strings
    /// </summary>
    public class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        public ArgParser() : base(new string[] { "" }) { }

        /// <summary>
        /// Handle a switch and its value
        /// </summary>
        /// <param name="switchSymbol"></param>
        /// <param name="switchValue"></param>
        /// <returns></returns>
        protected override DebtPlus.Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            DebtPlus.Utils.ArgParserBase.SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Define the usage of the program.
        /// </summary>
        /// <param name="errorInfo"></param>
        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}\r\n", errorInfo));
            }

            System.Reflection.Assembly _asm = System.Reflection.Assembly.GetExecutingAssembly();
            string fname = System.IO.Path.GetFileName(_asm.CodeBase);
            AppendErrorLine("Usage: " + fname);
        }
    }

    public class Mainline : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ArgParser();
                    if (ap.Parse((string[])param))
                    {
                        var frm = new Form_InMotion(ap);
                        System.Windows.Forms.Application.Run(frm);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "DepositInMotion"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}