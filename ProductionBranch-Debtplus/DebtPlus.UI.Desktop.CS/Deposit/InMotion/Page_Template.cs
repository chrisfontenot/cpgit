﻿using System.Data;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public partial class Page_Template : DevExpress.XtraEditors.XtraUserControl
    {
        // Parent form for the current panel
        protected Form_InMotion MyParentForm;

        // Return the pointer to the dataset
        protected DataSet ds
        {
            get
            {
                if (MyParentForm != null)
                {
                    return MyParentForm.ds;
                }
                return null;
            }
        }

        // Return the pointer to the database linkage
        protected DebtPlus.LINQ.SQLInfoClass sqlInfo
        {
            get
            {
                if (MyParentForm != null)
                {
                    return MyParentForm.sqlInfo;
                }
                return null;
            }
        }

        public Page_Template(Form_InMotion MyParentForm)
            : this()
        {
            this.MyParentForm = MyParentForm;
        }

        public Page_Template()
            : base()
        {
            InitializeComponent();
        }

        public virtual void Start()
        {
        }
    }
}