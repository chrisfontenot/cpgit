﻿using System;
using System.Reflection;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public abstract class MyObject
    {
        // Allocate the class
        protected DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();

        /// <summary>
        /// Initialize the class.
        /// </summary>
        public MyObject()
        {
        }

        /// <summary>
        /// Convert the KeyField string to a boolean value.
        /// </summary>
        /// <param name="Input">The KeyField string to be converted</param>
        /// <returns>TRUE or FALSE as appropriate. FALSE is assumed if the value is not valid.</returns>
        protected bool toBool(string Input)
        {
            // Look for an empty string
            if (!string.IsNullOrEmpty(Input))
            {
                // "Y" is defined as TRUE.
                if (Input.Substring(0, 1).ToUpper() == "Y")
                {
                    return true;
                }

                // Try to convert the string to boolean.
                bool boolValue;
                if (Boolean.TryParse(Input, out boolValue))
                {
                    return boolValue;
                }

                // Try to convert a numeric value to boolean. "0" is false. Others are true.
                Int32 intValue;
                if (Int32.TryParse(Input, out intValue))
                {
                    return (intValue != 0);
                }
            }

            // The default is simply FALSE.
            return false;
        }

        /// <summary>
        /// This is the stub for the parser. It should be override by those items where it is required.
        /// </summary>
        /// <param name="rdr"></param>
        public virtual void Parser(ref XmlTextReader rdr)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Do the standard parser for an XML document.
        /// </summary>
        /// <param name="rdr">Pointer to the reader for the XML document</param>
        /// <param name="ThisPtr">Pointer to the class being read</param>
        protected void Parser(ref XmlTextReader rdr, Object ThisPtr)
        {
            // Save the starting name so that we know where to stop.
            string StartingName = rdr.Name;

            // Clear the field name.
            string FieldName = string.Empty;

            // Process the fields in the document until we reach the terminal.
            while (rdr.Read())
            {
                // If this is an element, save the name.
                if (rdr.NodeType == XmlNodeType.Element)
                {
                    FieldName = rdr.Name;
                    ProcessFieldElement(ref rdr, ThisPtr);
                }

                // If this is the end of an element and it matches what we started with then terminate.
                else if (rdr.NodeType == XmlNodeType.EndElement && rdr.Name == StartingName)
                {
                    break;
                }

                // Otherwise, process a text node and ignore all others.
                else if (rdr.NodeType == XmlNodeType.Text)
                {
                    if (FieldName != string.Empty)
                    {
                        ProcessTextElement(ref rdr, FieldName, ThisPtr);
                        FieldName = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Process a field element. This is normally something like <FieldName>value</FieldName>
        /// </summary>
        /// <param name="rdr"></param>
        /// <param name="ThisType"></param>
        protected virtual void ProcessFieldElement(ref XmlTextReader rdr, Object ThisType)
        {
            // This is overridden only when needed. Otherwise, it does nothing.
        }

        /// <summary>
        /// Process the textual component of a field. The default processing is to simply set the value.
        /// </summary>
        /// <param name="rdr"></param>
        /// <param name="FieldName"></param>
        /// <param name="ThisPtr"></param>
        protected virtual void ProcessTextElement(ref XmlTextReader rdr, string FieldName, Object ThisPtr)
        {
            PropertyInfo pi = ThisPtr.GetType().GetProperty(FieldName);
            if (pi != null)
            {
                pi.SetValue(ThisPtr, rdr.Value, null);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine(string.Format("Field {0} is missing", FieldName));
            }
        }
    }
}