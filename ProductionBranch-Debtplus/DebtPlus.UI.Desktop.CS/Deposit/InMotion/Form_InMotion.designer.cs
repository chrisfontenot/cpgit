﻿namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    partial class Form_InMotion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null) components.Dispose();
                ds.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Next = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Back = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl_Main = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_Main)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_Cancel.Location = new System.Drawing.Point(299, 222);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(78, 28);
            this.simpleButton_Cancel.TabIndex = 0;
            this.simpleButton_Cancel.Text = "&Cancel";
            // 
            // simpleButton_Next
            // 
            this.simpleButton_Next.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_Next.Location = new System.Drawing.Point(203, 222);
            this.simpleButton_Next.Name = "simpleButton_Next";
            this.simpleButton_Next.Size = new System.Drawing.Size(78, 28);
            this.simpleButton_Next.TabIndex = 1;
            this.simpleButton_Next.Text = "&Next >";
            // 
            // simpleButton_Back
            // 
            this.simpleButton_Back.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton_Back.Location = new System.Drawing.Point(107, 222);
            this.simpleButton_Back.Name = "simpleButton_Back";
            this.simpleButton_Back.Size = new System.Drawing.Size(78, 28);
            this.simpleButton_Back.TabIndex = 2;
            this.simpleButton_Back.Text = "< &Back";
            // 
            // panelControl_Main
            // 
            this.panelControl_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl_Main.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.panelControl_Main.Location = new System.Drawing.Point(13, 13);
            this.panelControl_Main.Name = "panelControl_Main";
            this.panelControl_Main.Size = new System.Drawing.Size(373, 191);
            this.panelControl_Main.TabIndex = 3;
            // 
            // Form_InMotion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 262);
            this.Controls.Add(this.panelControl_Main);
            this.Controls.Add(this.simpleButton_Back);
            this.Controls.Add(this.simpleButton_Next);
            this.Controls.Add(this.simpleButton_Cancel);
            this.Name = "Form_InMotion";
            this.Text = "Load lockbox deposit information";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl_Main)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        protected internal DevExpress.XtraEditors.SimpleButton simpleButton_Next;
        protected internal DevExpress.XtraEditors.SimpleButton simpleButton_Back;
        private DevExpress.XtraEditors.PanelControl panelControl_Main;

    }
}