﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    /// <summary>
    /// Class for storing a name
    /// </summary>
    public class NameType : MyObject
    {
        /// <summary>
        /// Salutation for the name. DebtPlus stores this as "prefix".
        /// </summary>
        public string Salutation { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        public string First { get; set; }

        /// <summary>
        /// Middle name
        /// </summary>
        public string Middle { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        public string Last { get; set; }

        /// <summary>
        /// Generation name. DebtPlus stores this as suffix.
        /// </summary>
        public string Generation { get; set; }

        /// <summary>
        /// Use the standard parser for the name
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            this.Parser(ref rdr, this);
        }

        /// <summary>
        /// Insert the telephone number and return the record pointer
        /// </summary>
        /// <param name="cn">Database Connection object</param>
        /// <param name="txn">Transaction for the connection</param>
        /// <returns></returns>
        public Int32 xpr_insert_Name(SqlConnection cn, SqlTransaction txn)
        {
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "xpr_insert_names";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@Name", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@Prefix", SqlDbType.VarChar, 10).Value = (object)Salutation ?? DBNull.Value;
                cmd.Parameters.Add("@First", SqlDbType.VarChar, 40).Value = (object)First ?? DBNull.Value;
                cmd.Parameters.Add("@Middle", SqlDbType.VarChar, 40).Value = (object)Middle ?? DBNull.Value;
                cmd.Parameters.Add("@Last", SqlDbType.VarChar, 40).Value = (object)Last ?? DBNull.Value;
                cmd.Parameters.Add("@Suffix", SqlDbType.VarChar, 10).Value = (object)Generation ?? DBNull.Value;

                cmd.ExecuteNonQuery();

                return Convert.ToInt32(cmd.Parameters[0].Value);
            }
        }
    }
}