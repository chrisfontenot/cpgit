﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    public partial class Page_Status : Page_Template
    {
        private const string settings_file_open_ext = ".xml";
        private const string settings_file_open_filter = "XML Files (*.xml)|*.xml|All Files (*.*)|*.*";
        private const Int32 settings_filter_index = 0;
        private const string settings_CreateCommand = "xpr_deposit_batch_create";

        // Thread handler for the background operation of reading the deposit batch
        private System.ComponentModel.BackgroundWorker bt;

        // List of the deposit records in the KeyField file
        public System.Collections.Generic.List<InMotion> Deposits = new System.Collections.Generic.List<InMotion>();

        // Application parameters from the config file
        // public static System.Collections.Specialized.NameValueCollection settings = System.Configuration.ConfigurationManager.AppSettings;

        // Local information to be displayed in the progress form
        private Int32 CIMItems = 0;

        private Int32 MIMItems = 0;
        private Int32 processedItems = 0;
        private decimal dollarAmount = 0M;

        private System.Windows.Forms.OpenFileDialog openFileDlg;

        /// <summary>
        /// Name for the KeyField file
        /// </summary>
        private string FileName { get; set; }

        /// <summary>
        /// Bank number chosen for the deposit batch
        /// </summary>
        private Int32 Parameter_Bank
        {
            get
            {
                return MyParentForm.panelInput.Parameter_Bank;
            }
        }

        /// <summary>
        /// Label associated with the deposit batch
        /// </summary>
        private string Parameter_Label
        {
            get
            {
                return MyParentForm.panelInput.Parameter_Label;
            }
        }

        /// <summary>
        /// Batch ID for the deposits
        /// </summary>
        public Int32 Parameter_Batch { get; set; }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        /// <param name="MyParentForm"></param>
        public Page_Status(Form_InMotion MyParentForm)
            : base(MyParentForm)
        {
            InitializeComponent();

            // Create the background worker
            bt = new System.ComponentModel.BackgroundWorker();
            bt.DoWork += new DoWorkEventHandler(bt_DoWork);
            bt.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bt_RunWorkerCompleted);
            bt.ProgressChanged += new ProgressChangedEventHandler(bt_ProgressChanged);
        }

        /// <summary>
        /// Configure the panel for processing
        /// </summary>
        public override void Start()
        {
            base.Start();

            // Request the name of the KeyField file to be processed for this batch
            FileName = requestFileName();
            if (string.IsNullOrEmpty(FileName))
            {
                MyParentForm.simpleButton_Cancel.PerformClick();
                return;
            }

            // Start the marque
            marqueeProgressBarControl1.Enabled = true;

            // Disable the Next button until we complete
            MyParentForm.simpleButton_Next.Enabled = false;

            // Start a thread to process the deposits. It will take it from here.
            bt.WorkerReportsProgress = true;
            bt.WorkerSupportsCancellation = true;
            bt.RunWorkerAsync();
        }

        /// <summary>
        /// Perform the work of reading the batch data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Generate the deposit batch
            Parameter_Batch = CreateDepositBatch(Parameter_Bank, Parameter_Label, FileName);
            if (Parameter_Batch <= 0)
            {
                return;
            }

            // Open the text file
            XmlTextReader rdr = new XmlTextReader(FileName);
            if (rdr != null)
            {
                try
                {
                    // Process the reader operation
                    while (rdr.Read())
                    {
                        // Look for the InMotions item
                        if (rdr.NodeType == XmlNodeType.Element)
                        {
                            if (rdr.Name == "InMotions")
                            {
                                ParseInMotions(ref rdr);
                            }
                            else
                            {
                                rdr.Skip();
                            }
                        }
                    }
                }
                finally
                {
                    rdr.Close();
                }

                // Open the database connection object
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                SqlTransaction txn = null;
                try
                {
                    // Open the database connection object. It is used for all of the deposit items.
                    cn.Open();

                    // Process the records
                    foreach (InMotion item in Deposits)
                    {
                        try
                        {
                            // Allocate a transaction for this client and process the deposit record.
                            txn = cn.BeginTransaction();
                            item.RecordTransaction(cn, txn, Parameter_Batch);

                            // Commit the client changes to the database at this point.
                            txn.Commit();
                            txn.Dispose();
                            txn = null;

                            // Update the statistics
                            processedItems += 1;
                            bt.ReportProgress(0);
                        }

                        finally
                        {
                            // If there was a pending transaction (because there was an error) then roll back
                            // the changes to the client tables. We don't want a partially created client.
                            if (txn != null)
                            {
                                try
                                {
                                    txn.Rollback();
                                    txn.Dispose();
                                    txn = null;
                                }
                                catch { }
                            }
                        }
                    }
                }

                finally
                {
                    if (cn != null)
                    {
                        if (cn.State != ConnectionState.Closed)
                        {
                            cn.Close();
                        }
                        cn.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Read and parse the MoneyInMotion and CounselingInMotion items
        /// </summary>
        /// <param name="rdr"></param>
        private void ParseInMotions(ref XmlTextReader rdr)
        {
            // Process all of the items in the file until we reach the end.
            while (rdr.Read())
            {
                if (rdr.NodeType == XmlNodeType.EndElement && rdr.Name == "InMotions")
                {
                    break;
                }

                if (rdr.NodeType == XmlNodeType.Element)
                {
                    // The first valid type is CounselingInMotion
                    if (rdr.Name == "CounselingInMotion")
                    {
                        InMotion item = new CounselingInMotion();
                        item.Parser(ref rdr);
                        Deposits.Add(item);

                        CIMItems += 1;
                        dollarAmount += item.AmountValue;
                        bt.ReportProgress(0);
                    }

                    // The next type is MakeYourMove
                    else if (rdr.Name == "MakeYourMove")
                    {
                        InMotion item = new MakeYourMove();
                        item.Parser(ref rdr);
                        Deposits.Add(item);

                        CIMItems += 1;
                        dollarAmount += item.AmountValue;
                        bt.ReportProgress(0);
                    }

                    // The other valid type here is MoneyInMotion
                    else if (rdr.Name == "MoneyInMotion")
                    {
                        InMotion item = new MoneyInMotion();
                        item.Parser(ref rdr);
                        Deposits.Add(item);

                        MIMItems += 1;
                        dollarAmount += item.AmountValue;
                        bt.ReportProgress(0);
                    }
                    else
                    {
                        rdr.Skip();
                    }
                }
            }
        }

        /// <summary>
        /// The batch KeyField thread has completed its processing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Stop the marque
            marqueeProgressBarControl1.Enabled = false;

            // Enable the Next button
            MyParentForm.simpleButton_Next.Tag = Form_InMotion.Tag_Results;
            MyParentForm.simpleButton_Next.Enabled = true;
        }

        /// <summary>
        /// Update the status for the worker thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new ProgressChangedEventHandler(bt_ProgressChanged), new object[] { sender, e });
            }
            else
            {
                labelControl_cim_items.Text = string.Format("{0:n0}", CIMItems);
                labelControl_mim_items.Text = string.Format("{0:n0}", MIMItems);
                labelControl_processed_items.Text = string.Format("{0:n0}", processedItems);
                labelControl_dollarAmount.Text = string.Format("{0:c}", dollarAmount);
            }
        }

        /// <summary>
        /// Request the name of the KeyField file
        /// </summary>
        /// <returns>The file name or empty if there is an error/cancel.</returns>
        private string requestFileName()
        {
            string Answer = string.Empty;
            using (openFileDlg = new System.Windows.Forms.OpenFileDialog()
            {
                AutoUpgradeEnabled = true,
                CheckFileExists = true,
                CheckPathExists = true,
                DereferenceLinks = true,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                Multiselect = false,
                ReadOnlyChecked = true,
                RestoreDirectory = true,
                ShowReadOnly = false,
                ValidateNames = true,
                AddExtension = true,
                DefaultExt = settings_file_open_ext,
                Filter = settings_file_open_filter,
                FilterIndex = settings_filter_index,
                Title = "Name of KeyField file"
            })
            {
                openFileDlg.FileOk += new CancelEventHandler(dlg_FileOk);
                if (openFileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Answer = openFileDlg.FileName;
                }
                openFileDlg.FileOk -= new CancelEventHandler(dlg_FileOk);
            }
            return Answer;
        }

        /// <summary>
        /// Try to ensure that we can open the file. It needs to have read permission.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dlg_FileOk(object sender, CancelEventArgs e)
        {
            try
            {
                // Ensure that we can open the file. If this fails, it will throw an exception for later which will fail the test.
                using (var fs = new System.IO.FileStream(openFileDlg.FileName, System.IO.FileMode.Open))
                {
                    using (var rd = new System.IO.StreamReader(fs, System.Text.Encoding.ASCII, false))
                    {
                        // Read the first part of the file. We just guess at the size but 8K should be enough.
                        char[] Buffer = new Char[8192];
                        int nCount = rd.ReadBlock(Buffer, 0, 8192);
                        if (nCount > 0)
                        {
                            // There should be an <InMotion> tag ....
                            string st = new System.String(Buffer, 0, nCount);
                            if (st.Contains("<InMotions>"))
                            {
                                return;
                            }
                        }
                    }
                }

                // The file is not valid. Do not allow it to be selected.
                e.Cancel = true;
                return;
            }

#pragma warning disable 168
            catch (Exception ex)
            {
                e.Cancel = true;
            }
#pragma warning restore 168
        }

        /// <summary>
        /// Create the deposit batch information
        /// </summary>
        /// <param name="Bank">Bank account number</param>
        /// <param name="Label">Label for the deposit batch</param>
        /// <param name="FileName">Input file name</param>
        /// <returns>The deposit batch ID</returns>
        private Int32 CreateDepositBatch(Int32 Bank, string Label, string FileName)
        {
            Int32 Answer = -1;

            // Create the deposit batch
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = xpr_deposit_batch_create;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                    // Parameters for the request
                    cmd.Parameters.Add("@note", SqlDbType.VarChar, 256).Value = Label;
                    cmd.Parameters.Add("@batch_type", SqlDbType.VarChar, 2).Value = "CL";
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = Bank;

                    cmd.ExecuteNonQuery();

                    // Return the batch ID
                    Answer = (Int32)cmd.Parameters[0].Value;
                }
            }

            // Close the connection
            finally
            {
                if (cn != null)
                {
                    if (cn.State != ConnectionState.Closed)
                    {
                        cn.Close();
                    }
                    cn.Dispose();
                }
            }

            return Answer;
        }

        /// <summary>
        /// Stored procedure to create the deposit batch
        /// </summary>
        private string xpr_deposit_batch_create
        {
            get
            {
                return settings_CreateCommand;
            }
        }
    }
}