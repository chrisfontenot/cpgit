﻿using System;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.Deposit.InMotion
{
    /// <summary>
    /// Class for storing the bankruptcy information
    /// </summary>
    public class BankruptcyType : MyObject
    {
        /// <summary>
        /// Case number
        /// </summary>
        public string CaseNumber { get; set; }

        /// <summary>
        /// State for the case
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// County for the case
        /// </summary>
        public string County { get; set; }

        /// <summary>
        /// Use the standard parser for the bankruptcy
        /// </summary>
        /// <param name="rdr"></param>
        public override void Parser(ref XmlTextReader rdr)
        {
            this.Parser(ref rdr, this);
        }

        /// <summary>
        /// Create the system note reflecting the bankruptcy information
        /// </summary>
        /// <param name="ApplicantName">Applicant or Co-Applicant string</param>
        /// <param name="sb">Pointer to the stringbuilder for the result</param>
        public void CreateSystemNote(string ApplicantName, ref System.Text.StringBuilder sb)
        {
            sb.AppendFormat("\r\n{0} Information", ApplicantName);
            if (!string.IsNullOrEmpty(CaseNumber))
            {
                sb.AppendFormat("\r\nCase Number: {0}", CaseNumber);
            }

            if (!string.IsNullOrEmpty(State))
            {
                sb.AppendFormat("\r\nState: {0}", State);
            }

            if (!string.IsNullOrEmpty(County))
            {
                sb.AppendFormat("\r\nCounty: {0}", County);
            }
            sb.Append(Environment.NewLine);
        }
    }
}