using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual
{
    // singleton
    internal partial class Form_DepositManual
    {
        private readonly DepositManualContext ctx = null;

        public Form_DepositManual(DepositManualContext ctx) : base()
        {
            this.ctx = ctx;
            InitializeComponent();
            this.FormClosing += Form_DepositManual_FormClosing;
            this.Load += Form_DepositManual_Load;
            BarButtonItem1.ItemClick += BarButtonItem1_ItemClick;
        }

        /// <summary>
        /// If the form is closing then check for lost updates
        /// </summary>
        private void Form_DepositManual_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PendingTransactions())
            {
                if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("Warning: You have made changes to this batch.{0}These changes will NOT be saved to the database if you simply close this form.{0}{0}Do you wish to abandon these updates?", Environment.NewLine), "Are you sure", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
                    e.Cancel = true;
            }
        }

        /// <summary>
        /// Determine if there are transactions still pending to be recorded
        /// </summary>
        internal bool PendingTransactions()
        {
            bool answer = false;

            // Find the table for the updates
            DataTable tbl = ctx.ds.Tables["deposit_batch_details"];

            // If there is a table then look for changes to the rows. A changed row will generate a warning dialog.
            if (tbl != null)
            {
                foreach (DataRow row in tbl.Rows)
                {
                    if (row.RowState != DataRowState.Unchanged)
                    {
                        answer = true;
                        break;
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// Process the one-time load event for the form
        /// </summary>

        private void Form_DepositManual_Load(object sender, EventArgs e)
        {
            // If there is no batch then use the form realestate to ask for a batch.

            if (ctx.ap.Batch <= 0)
            {
                var _with1 = ClientDepositControl1;
                _with1.SendToBack();
                _with1.Visible = false;

                var _with2 = NewClientDepositBatch1;
                _with2.Visible = true;
                _with2.BringToFront();
                _with2.ReadForm();
                _with2.Focus();
                _with2.Cancelled += NewClientDepositBatch1_Cancelled;
                _with2.Selected += NewClientDepositBatch1_Selected;
            }
            else
            {
                // Otherwise, do the deposits for this batch
                PerformDeposits();
            }
        }

        /// <summary>
        /// If the entry is to cancel the form then close it
        /// </summary>
        private void NewClientDepositBatch1_Cancelled(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// When a new batch is selected then process the batch updates
        /// </summary>
        private void NewClientDepositBatch1_Selected(object sender, EventArgs e)
        {
            ctx.ap.Batch = NewClientDepositBatch1.DepositBatchID;
            PerformDeposits();
        }

        /// <summary>
        /// Come here once we have a valid batch ID to start the updates
        /// </summary>

        private void PerformDeposits()
        {
            // Make the batch selection control to the back
            var _with3 = NewClientDepositBatch1;
            _with3.SendToBack();
            _with3.Visible = false;

            // Bring forward the batch update control. It will take it from here.
            var _with4 = ClientDepositControl1;
            _with4.BringToFront();
            _with4.Visible = true;
            _with4.ReadForm();
            _with4.Focus();
            _with4.Cancelled += NewClientDepositBatch1_Cancelled;
        }

        private void BarButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        internal DataRow ClientRow(Int32 client)
        {
            DataRow answer = null;
            DataTable tbl = ctx.ds.Tables["clients"];

            // See if the data is currently in the table. If so, return it.
            if (tbl != null)
            {
                DataRow row = tbl.Rows.Find(client);
                if (row != null)
                    answer = row;
            }

            // The item is not in the table. Create a new row
            if (answer == null)
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with5 = cmd;
                    _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with5.CommandText = "SELECT TOP 1 c.client, c.active_status, dbo.format_normal_name(p1n.prefix,p1n.first,p1n.middle,p1n.last,p1n.suffix) as applicant, dbo.format_normal_name(p2n.prefix,p2n.first,p2n.middle,p2n.last,p2n.suffix) as coapplicant, dbo.address_block(dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value), a.address_line_2, dbo.format_city_state_zip(a.city, a.state, a.postalcode),default,default) as address, dbo.client_deposit_amount(c.client) as deposit_amount FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p1 WITH (NOLOCK) ON c.client = p1.client AND 1 = p1.relation LEFT OUTER JOIN names p1n WITH (NOLOCK) ON p1.NameID = p1n.Name LEFT OUTER JOIN people p2 WITH (NOLOCK) ON c.client = p2.Client AND 1 <> p2.relation LEFT OUTER JOIN Names p2n WITH (NOLOCK) ON p2.NameID = p2n.Name LEFT OUTER JOIN Addresses a WITH (NOLOCK) ON c.AddressID = a.Address WHERE c.client = @client";
                    _with5.CommandType = CommandType.Text;
                    _with5.Parameters.Add("@client", SqlDbType.Int).Value = client;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ctx.ds, "clients");
                    }
                }

                if (tbl == null)
                {
                    tbl = ctx.ds.Tables["clients"];
                    var _with6 = tbl;
                    _with6.PrimaryKey = new DataColumn[] { _with6.Columns["client"] };
                }

                answer = tbl.Rows.Find(client);
            }

            return answer;
        }

        internal DataTable DepositSubtypeTable()
        {
            DataTable tbl = ctx.ds.Tables["deposit_subtypes"];
            if (tbl == null)
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with7 = cmd;
                    _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with7.CommandType = CommandType.Text;
                    _with7.CommandText = "SELECT tran_type as tran_subtype, description, sort_order, convert(bit, case when tran_type = 'PC' then 1 else 0 end) as restricted FROM tran_types WHERE deposit_subtype <> 0";

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ctx.ds, "deposit_subtypes");
                    }
                }

                tbl = ctx.ds.Tables["deposit_subtypes"];
                var _with8 = tbl;
                _with8.PrimaryKey = new DataColumn[] { _with8.Columns["tran_subtype"] };
            }

            return tbl;
        }
    }
}