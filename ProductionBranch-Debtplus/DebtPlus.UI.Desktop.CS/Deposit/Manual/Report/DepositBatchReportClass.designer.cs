using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Report
{
	partial class DepositBatchReportClass : DebtPlus.Reports.Template.TemplateXtraReportClass
	{
		//Component overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Component Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Component Designer
		//It can be modified using the Component Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			DevExpress.XtraReports.UI.XRSummary XrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary XrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
			this.StyleColumnHeaderText = new DevExpress.XtraReports.UI.XRControlStyle();
			this.XrControlStyle2 = new DevExpress.XtraReports.UI.XRControlStyle();
			this.XrPanel_BatchHeader = new DevExpress.XtraReports.UI.XRPanel();
			this.XrLabel_BankAccount = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_Bank = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_BatchCreatedOn = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_BatchID = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.XrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_TranType = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_ClientID = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_ClientName = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_Amount = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_Date = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_Reference = new DevExpress.XtraReports.UI.XRLabel();
			this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
			this.XrLine1 = new DevExpress.XtraReports.UI.XRLine();
			this.XrLabel_TotalAmount = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_TotalsLabel = new DevExpress.XtraReports.UI.XRLabel();
			this.ParameterSortString = new DevExpress.XtraReports.Parameters.Parameter();
			((System.ComponentModel.ISupportInitialize)this).BeginInit();
			//
			//Detail
			//
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrLabel_Reference,
				this.XrLabel_Date,
				this.XrLabel_Amount,
				this.XrLabel_ClientName,
				this.XrLabel_ClientID,
				this.XrLabel_TranType
			});
			this.Detail.HeightF = 18f;
			//
			//PageHeader
			//
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrPanel_BatchHeader,
				this.XrPanel1
			});
			this.PageHeader.HeightF = 206f;
			this.PageHeader.Controls.SetChildIndex(this.XrPanel1, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrPanel_BatchHeader, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrPanel_AgencyAddress, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrLabel_Title, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrLine_Title, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrLabel_Subtitle, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrPageInfo1, 0);
			//
			//XrLabel_Title
			//
			this.XrLabel_Title.StylePriority.UseFont = false;
			//
			//XrLabel_Subtitle
			//
			this.XrLabel_Subtitle.LocationFloat = new DevExpress.Utils.PointFloat(275f, 67f);
			this.XrLabel_Subtitle.SizeF = new System.Drawing.SizeF(151f, 17f);
			this.XrLabel_Subtitle.Text = "Subtitle is not used";
			this.XrLabel_Subtitle.Visible = false;
			//
			//XrPageInfo_PageNumber
			//
			this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
			//
			//XRLabel_Agency_Name
			//
			this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Address3
			//
			this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
			this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Address1
			//
			this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
			this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Phone
			//
			this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
			this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Address2
			//
			this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
			this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
			//
			//StyleColumnHeaderText
			//
			this.StyleColumnHeaderText.BackColor = System.Drawing.Color.Transparent;
			this.StyleColumnHeaderText.BorderColor = System.Drawing.Color.Transparent;
			this.StyleColumnHeaderText.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.StyleColumnHeaderText.ForeColor = System.Drawing.Color.White;
			this.StyleColumnHeaderText.Name = "StyleColumnHeaderText";
			//
			//XrControlStyle2
			//
			this.XrControlStyle2.BackColor = System.Drawing.Color.Transparent;
			this.XrControlStyle2.ForeColor = System.Drawing.Color.Black;
			this.XrControlStyle2.Name = "XrControlStyle2";
			//
			//XrPanel_BatchHeader
			//
			this.XrPanel_BatchHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrLabel_BankAccount,
				this.XrLabel_Bank,
				this.XrLabel_BatchCreatedOn,
				this.XrLabel_BatchID,
				this.XrLabel4,
				this.XrLabel3,
				this.XrLabel2,
				this.XrLabel1
			});
			this.XrPanel_BatchHeader.LocationFloat = new DevExpress.Utils.PointFloat(0f, 100f);
			this.XrPanel_BatchHeader.Name = "XrPanel_BatchHeader";
			this.XrPanel_BatchHeader.SizeF = new System.Drawing.SizeF(433f, 60f);
			//
			//XrLabel_BankAccount
			//
			this.XrLabel_BankAccount.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel_BankAccount.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel_BankAccount.LocationFloat = new DevExpress.Utils.PointFloat(158f, 45f);
			this.XrLabel_BankAccount.Name = "XrLabel_BankAccount";
			this.XrLabel_BankAccount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_BankAccount.SizeF = new System.Drawing.SizeF(266f, 15f);
			//
			//XrLabel_Bank
			//
			this.XrLabel_Bank.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel_Bank.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel_Bank.LocationFloat = new DevExpress.Utils.PointFloat(158f, 30f);
			this.XrLabel_Bank.Name = "XrLabel_Bank";
			this.XrLabel_Bank.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_Bank.SizeF = new System.Drawing.SizeF(266f, 15f);
			//
			//XrLabel_BatchCreatedOn
			//
			this.XrLabel_BatchCreatedOn.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel_BatchCreatedOn.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel_BatchCreatedOn.LocationFloat = new DevExpress.Utils.PointFloat(158f, 15f);
			this.XrLabel_BatchCreatedOn.Name = "XrLabel_BatchCreatedOn";
			this.XrLabel_BatchCreatedOn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_BatchCreatedOn.SizeF = new System.Drawing.SizeF(267f, 15f);
			//
			//XrLabel_BatchID
			//
			this.XrLabel_BatchID.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel_BatchID.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel_BatchID.LocationFloat = new DevExpress.Utils.PointFloat(158f, 0f);
			this.XrLabel_BatchID.Name = "XrLabel_BatchID";
			this.XrLabel_BatchID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_BatchID.SizeF = new System.Drawing.SizeF(266f, 15f);
			//
			//XrLabel4
			//
			this.XrLabel4.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel4.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(8f, 45f);
			this.XrLabel4.Name = "XrLabel4";
			this.XrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel4.SizeF = new System.Drawing.SizeF(145f, 15f);
			this.XrLabel4.Text = "Bank Account Number:";
			//
			//XrLabel3
			//
			this.XrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel3.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(8f, 30f);
			this.XrLabel3.Name = "XrLabel3";
			this.XrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel3.SizeF = new System.Drawing.SizeF(145f, 15f);
			this.XrLabel3.Text = "Bank Name:";
			//
			//XrLabel2
			//
			this.XrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel2.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(8f, 15f);
			this.XrLabel2.Name = "XrLabel2";
			this.XrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel2.SizeF = new System.Drawing.SizeF(145f, 15f);
			this.XrLabel2.Text = "Batch Created On:";
			//
			//XrLabel1
			//
			this.XrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel1.ForeColor = System.Drawing.Color.Teal;
			this.XrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(8f, 0f);
			this.XrLabel1.Name = "XrLabel1";
			this.XrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel1.SizeF = new System.Drawing.SizeF(145f, 15f);
			this.XrLabel1.Text = "Batch Number:";
			//
			//XrPanel1
			//
			this.XrPanel1.BackColor = System.Drawing.Color.Teal;
			this.XrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrLabel10,
				this.XrLabel9,
				this.XrLabel8,
				this.XrLabel6,
				this.XrLabel5
			});
			this.XrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 183f);
			this.XrPanel1.Name = "XrPanel1";
			this.XrPanel1.SizeF = new System.Drawing.SizeF(750f, 17f);
			//
			//XrLabel10
			//
			this.XrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(600f, 0f);
			this.XrLabel10.Name = "XrLabel10";
			this.XrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel10.SizeF = new System.Drawing.SizeF(150f, 17f);
			this.XrLabel10.StyleName = "StyleColumnHeaderText";
			this.XrLabel10.Text = "REFERENCE";
			//
			//XrLabel9
			//
			this.XrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(534f, 0f);
			this.XrLabel9.Name = "XrLabel9";
			this.XrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel9.SizeF = new System.Drawing.SizeF(59f, 17f);
			this.XrLabel9.StyleName = "StyleColumnHeaderText";
			this.XrLabel9.Text = "DATE";
			this.XrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			//
			//XrLabel8
			//
			this.XrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(450f, 0f);
			this.XrLabel8.Name = "XrLabel8";
			this.XrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel8.SizeF = new System.Drawing.SizeF(66f, 17f);
			this.XrLabel8.StyleName = "StyleColumnHeaderText";
			this.XrLabel8.Text = "AMOUNT";
			this.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			//
			//XrLabel6
			//
			this.XrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(117f, 0f);
			this.XrLabel6.Name = "XrLabel6";
			this.XrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel6.SizeF = new System.Drawing.SizeF(325f, 17f);
			this.XrLabel6.StyleName = "StyleColumnHeaderText";
			this.XrLabel6.StylePriority.UseTextAlignment = false;
			this.XrLabel6.Text = "Client ID AND NAME";
			this.XrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			//
			//XrLabel5
			//
			this.XrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(8f, 0f);
			this.XrLabel5.Name = "XrLabel5";
			this.XrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel5.SizeF = new System.Drawing.SizeF(92f, 17f);
			this.XrLabel5.StyleName = "StyleColumnHeaderText";
			this.XrLabel5.Text = "ITEM TYPE";
			//
			//XrLabel_TranType
			//
			this.XrLabel_TranType.LocationFloat = new DevExpress.Utils.PointFloat(8f, 0f);
			this.XrLabel_TranType.Name = "XrLabel_TranType";
			this.XrLabel_TranType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_TranType.SizeF = new System.Drawing.SizeF(92f, 17f);
			this.XrLabel_TranType.WordWrap = false;
			//
			//XrLabel_ClientID
			//
			this.XrLabel_ClientID.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "client", "{0:0000000}") });
			this.XrLabel_ClientID.LocationFloat = new DevExpress.Utils.PointFloat(108f, 0f);
			this.XrLabel_ClientID.Name = "XrLabel_ClientID";
			this.XrLabel_ClientID.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_ClientID.SizeF = new System.Drawing.SizeF(75f, 17f);
			this.XrLabel_ClientID.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			this.XrLabel_ClientID.WordWrap = false;
			//
			//XrLabel_ClientName
			//
			this.XrLabel_ClientName.LocationFloat = new DevExpress.Utils.PointFloat(192f, 0f);
			this.XrLabel_ClientName.Multiline = true;
			this.XrLabel_ClientName.Name = "XrLabel_ClientName";
			this.XrLabel_ClientName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_ClientName.SizeF = new System.Drawing.SizeF(250f, 17f);
			//
			//XrLabel_Amount
			//
			this.XrLabel_Amount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "amount", "{0:c}") });
			this.XrLabel_Amount.LocationFloat = new DevExpress.Utils.PointFloat(450f, 0f);
			this.XrLabel_Amount.Name = "XrLabel_Amount";
			this.XrLabel_Amount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_Amount.SizeF = new System.Drawing.SizeF(66f, 17f);
			this.XrLabel_Amount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			this.XrLabel_Amount.WordWrap = false;
			//
			//XrLabel_Date
			//
			this.XrLabel_Date.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "item_date", "{0:d}") });
			this.XrLabel_Date.LocationFloat = new DevExpress.Utils.PointFloat(517f, 0f);
			this.XrLabel_Date.Name = "XrLabel_Date";
			this.XrLabel_Date.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_Date.SizeF = new System.Drawing.SizeF(76f, 17f);
			this.XrLabel_Date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			this.XrLabel_Date.WordWrap = false;
			//
			//XrLabel_Reference
			//
			this.XrLabel_Reference.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "reference") });
			this.XrLabel_Reference.LocationFloat = new DevExpress.Utils.PointFloat(600f, 0f);
			this.XrLabel_Reference.Name = "XrLabel_Reference";
			this.XrLabel_Reference.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_Reference.SizeF = new System.Drawing.SizeF(150f, 17f);
			this.XrLabel_Reference.WordWrap = false;
			//
			//ReportFooter
			//
			this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrLine1,
				this.XrLabel_TotalAmount,
				this.XrLabel_TotalsLabel
			});
			this.ReportFooter.HeightF = 42f;
			this.ReportFooter.Name = "ReportFooter";
			//
			//XrLine1
			//
			this.XrLine1.LocationFloat = new DevExpress.Utils.PointFloat(399f, 8f);
			this.XrLine1.Name = "XrLine1";
			this.XrLine1.SizeF = new System.Drawing.SizeF(117f, 9f);
			//
			//XrLabel_TotalAmount
			//
			this.XrLabel_TotalAmount.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "amount") });
			this.XrLabel_TotalAmount.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.XrLabel_TotalAmount.ForeColor = System.Drawing.Color.Black;
			this.XrLabel_TotalAmount.LocationFloat = new DevExpress.Utils.PointFloat(400f, 25f);
			this.XrLabel_TotalAmount.Name = "XrLabel_TotalAmount";
			this.XrLabel_TotalAmount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_TotalAmount.SizeF = new System.Drawing.SizeF(116f, 17f);
			XrSummary1.FormatString = "{0:c}";
			XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.XrLabel_TotalAmount.Summary = XrSummary1;
			this.XrLabel_TotalAmount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			this.XrLabel_TotalAmount.WordWrap = false;
			//
			//XrLabel_TotalsLabel
			//
			this.XrLabel_TotalsLabel.CanGrow = false;
			this.XrLabel_TotalsLabel.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "client") });
			this.XrLabel_TotalsLabel.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
			this.XrLabel_TotalsLabel.ForeColor = System.Drawing.Color.Blue;
			this.XrLabel_TotalsLabel.LocationFloat = new DevExpress.Utils.PointFloat(0f, 25f);
			this.XrLabel_TotalsLabel.Name = "XrLabel_TotalsLabel";
			this.XrLabel_TotalsLabel.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
			this.XrLabel_TotalsLabel.SizeF = new System.Drawing.SizeF(351.375f, 17f);
			XrSummary2.FormatString = "Grand Total of Batch ({0:n0} items)";
			XrSummary2.Func = DevExpress.XtraReports.UI.SummaryFunc.Count;
			XrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.XrLabel_TotalsLabel.Summary = XrSummary2;
			this.XrLabel_TotalsLabel.Text = "Grand Total of Batch (0 items)";
			//
			//ParameterSortString
			//
			this.ParameterSortString.Description = "Sort Order";
			this.ParameterSortString.Name = "ParameterSortString";
			this.ParameterSortString.Visible = false;
			//
			//DepositBatchReportClass
			//
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
				this.Detail,
				this.PageHeader,
				this.PageFooter,
				this.TopMarginBand1,
				this.BottomMarginBand1,
				this.ReportFooter
			});
			this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] { this.ParameterSortString });
			this.RequestParameters = false;
			this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
				this.XrControlStyle_Header,
				this.XrControlStyle_Totals,
				this.XrControlStyle_HeaderPannel,
				this.XrControlStyle_GroupHeader,
				this.StyleColumnHeaderText,
				this.XrControlStyle2
			});
			this.Version = "11.2";
			this.Controls.SetChildIndex(this.ReportFooter, 0);
			this.Controls.SetChildIndex(this.BottomMarginBand1, 0);
			this.Controls.SetChildIndex(this.TopMarginBand1, 0);
			this.Controls.SetChildIndex(this.PageFooter, 0);
			this.Controls.SetChildIndex(this.PageHeader, 0);
			this.Controls.SetChildIndex(this.Detail, 0);
			((System.ComponentModel.ISupportInitialize)this).EndInit();

		}

		protected internal DevExpress.XtraReports.UI.XRPanel XrPanel_BatchHeader;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel1;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel2;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel3;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel4;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_BatchID;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_BatchCreatedOn;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_Bank;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_BankAccount;
		protected internal DevExpress.XtraReports.UI.XRPanel XrPanel1;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel5;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel6;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel8;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel9;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel10;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_ClientName;
		protected internal DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_TotalsLabel;
		protected internal DevExpress.XtraReports.UI.XRLine XrLine1;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_TranType;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_ClientID;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_Amount;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_Date;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_Reference;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_TotalAmount;
		protected internal DevExpress.XtraReports.UI.XRControlStyle StyleColumnHeaderText;
		protected internal DevExpress.XtraReports.UI.XRControlStyle XrControlStyle2;
		protected internal DevExpress.XtraReports.Parameters.Parameter ParameterSortString;
	}
}
