using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Report
{
    internal partial class RequestParametersForm : DebtPlus.Reports.Template.Forms.ReportParametersForm
    {
        public RequestParametersForm() : base()
        {
            InitializeComponent();
            this.Load += RequestParametersForm_Load;
        }

        internal string sort_order
        {
            get
            {
                DebtPlus.Data.Controls.ComboboxItem item = default(DebtPlus.Data.Controls.ComboboxItem);
                var _with1 = ComboBoxEdit_Sorting;
                if (_with1.SelectedIndex < 0)
                    return System.String.Empty;
                item = (DebtPlus.Data.Controls.ComboboxItem)_with1.SelectedItem;
                return Convert.ToString(item.value);
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl Label2;

        internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_Sorting;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.ComboBoxEdit_Sorting = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_Sorting.Properties).BeginInit();
            this.SuspendLayout();
            //
            //ButtonSelect
            //
            this.ButtonOK.Enabled = true;
            this.ButtonOK.Location = new System.Drawing.Point(248, 18);
            this.ButtonOK.Size = new System.Drawing.Size(80, 28);
            this.ButtonOK.TabIndex = 3;
            //
            //ButtonCancel
            //
            this.ButtonCancel.Location = new System.Drawing.Point(248, 56);
            this.ButtonCancel.Size = new System.Drawing.Size(80, 28);
            this.ButtonCancel.TabIndex = 4;
            //
            //Label2
            //
            this.Label2.Location = new System.Drawing.Point(10, 26);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(38, 13);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Sorting:";
            //
            //ComboBoxEdit_Sorting
            //
            this.ComboBoxEdit_Sorting.EditValue = "";
            this.ComboBoxEdit_Sorting.Location = new System.Drawing.Point(54, 23);
            this.ComboBoxEdit_Sorting.Name = "ComboBoxEdit_Sorting";
            this.ComboBoxEdit_Sorting.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.ComboBoxEdit_Sorting.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ComboBoxEdit_Sorting.Size = new System.Drawing.Size(176, 20);
            this.ComboBoxEdit_Sorting.TabIndex = 2;
            this.ComboBoxEdit_Sorting.ToolTip = "How should the report be sorted?";
            this.ComboBoxEdit_Sorting.ToolTipController = this.ToolTipController1;
            //
            //RequestParametersForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(336, 95);
            this.Controls.Add(this.ComboBoxEdit_Sorting);
            this.Controls.Add(this.Label2);
            this.Name = "RequestParametersForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Deposit Batch Report Parameters";
            this.Controls.SetChildIndex(this.ButtonOK, 0);
            this.Controls.SetChildIndex(this.ButtonCancel, 0);
            this.Controls.SetChildIndex(this.Label2, 0);
            this.Controls.SetChildIndex(this.ComboBoxEdit_Sorting, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_Sorting.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void RequestParametersForm_Load(System.Object sender, System.EventArgs e)
        {
            // Populate the sorting order dropdown
            var _with2 = ComboBoxEdit_Sorting;
            var _with3 = _with2.Properties;
            var _with4 = _with3.Items;
            _with4.Clear();
            ComboBoxEdit_Sorting.SelectedIndex = _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Entry Order", "deposit_batch_detail"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Client ID", "client"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Amount", "amount"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Deposit Type", "tran_subtype"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Date/Time Entered", "date_created"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Instrument Date", "item_date"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Reference Field", "reference"));
        }
    }
}