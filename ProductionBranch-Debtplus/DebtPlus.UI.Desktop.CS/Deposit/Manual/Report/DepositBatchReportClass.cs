using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using DebtPlus.Reports.Template;
using System.Drawing.Printing;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Report
{
    internal partial class DepositBatchReportClass : TemplateXtraReportClass
    {
        public DepositBatchReportClass() : base()
        {
            InitializeComponent();
        }

        private readonly DepositManualContext ctx = null;

        private Form_DepositManual depositManualForm;

        public DepositBatchReportClass(Form_DepositManual p, DepositManualContext ctx) : this(p, ctx, (IContainer)null)
        {
        }

        public DepositBatchReportClass(Form_DepositManual p, DepositManualContext ctx, IContainer Container)
        {
            if (Container != null)
                Container.Add(this);
            this.ctx = ctx;
            depositManualForm = p;
            InitializeComponent();
            XrLabel_ClientName.BeforePrint += XrLabel_ClientName_BeforePrint;
            XrLabel_TranType.BeforePrint += XrLabel_TranType_BeforePrint;
            this.BeforePrint += DepositBatchReportClass_BeforePrint;
        }

        /// <summary>
        /// Title associated with the report
        /// </summary>
        public override string ReportTitle
        {
            get { return "Client Deposit Batch"; }
        }

        /// <summary>
        /// SubTitle associated with the report
        /// </summary>
        public override string ReportSubTitle
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Request the parameters for the report
        /// </summary>
        public override DialogResult RequestReportParameters()
        {
            DialogResult answer = default(DialogResult);

            // Request the disbursement register from the user
            using (RequestParametersForm dialogForm = new RequestParametersForm())
            {
                var _with1 = dialogForm;
                answer = _with1.ShowDialog();
                Parameters["ParameterSortString"].Value = _with1.sort_order;
            }

            // The answer should be OK if we want to show the report.
            return answer;
        }

        /// <summary>
        /// Bind the dataset to the report
        /// </summary>

        private void DepositBatchReportClass_BeforePrint(object sender, PrintEventArgs e)
        {
            // Bind the data fields to the report form
            DataTable NewTbl = ctx.ds.Tables["deposit_batch_details"];
            DataSource = new DataView(NewTbl, string.Empty, Convert.ToString(Parameters["ParameterSortString"].Value), DataViewRowState.CurrentRows);

            // Read the batch header information
            ReadBatchHeader();
        }

        /// <summary>
        /// Read the batch header information
        /// </summary>
        private void ReadBatchHeader()
        {
            SqlDataReader rd = null;
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

            try
            {
                cn.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = cn;
                    _with2.CommandText = "SELECT d.date_created, dbo.format_counselor_name(d.created_by) as created_by, coalesce(b.description,'Bank #' + convert(varchar,d.bank),'') as BankName, isnull(b.account_number,'') as BankAccountNumber FROM deposit_batch_ids d LEFT OUTER JOIN banks b ON d.bank=b.bank WHERE deposit_batch_id=@deposit_batch_id";
                    _with2.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch;
                    rd = _with2.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);
                }

                // Populate the display information with the first row
                if (rd.Read())
                {
                    XrLabel_BatchID.Text = ctx.ap.Batch.ToString();
                    XrLabel_BatchCreatedOn.Text = string.Format("{0} by {1}", rd.GetDateTime(rd.GetOrdinal("date_created")).ToShortDateString(), rd.GetString(rd.GetOrdinal("created_by")));
                    XrLabel_Bank.Text = rd.GetString(rd.GetOrdinal("BankName"));
                    XrLabel_BankAccount.Text = rd.GetString(rd.GetOrdinal("BankAccount"));
                }
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }
        }

        /// <summary>
        /// Format the transaction subtype description
        /// </summary>

        private void XrLabel_TranType_BeforePrint(object sender, PrintEventArgs e)
        {
            var _with3 = XrLabel_TranType;
            object TranType = GetCurrentColumnValue("tran_subtype");
            if (TranType == null || object.ReferenceEquals(TranType, DBNull.Value))
            {
                _with3.Text = string.Empty;
            }
            else
            {
                _with3.Text = Convert.ToString(depositManualForm.DepositSubtypeTable().Rows.Find(TranType)["description"]);
            }
        }

        /// <summary>
        /// Format the client name
        /// </summary>

        private void XrLabel_ClientName_BeforePrint(object sender, PrintEventArgs e)
        {
            var _with4 = XrLabel_ClientName;

            object client = GetCurrentColumnValue("client");
            string Name = string.Empty;
            if (!object.ReferenceEquals(client, DBNull.Value) && Convert.ToInt32(client) > 0)
            {
                DataRow row = depositManualForm.ClientRow(Convert.ToInt32(client));
                if (row != null)
                {
                    if (!object.ReferenceEquals(row["applicant"], DBNull.Value))
                        Name = Convert.ToString(row["applicant"]);
                }
            }

            _with4.Text = Name;
        }
    }
}