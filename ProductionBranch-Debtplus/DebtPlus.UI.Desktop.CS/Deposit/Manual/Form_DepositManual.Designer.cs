using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.FormLib.Deposit;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual
{
	partial class Form_DepositManual : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.NewClientDepositBatch1 = new NewClientDepositBatch();
			this.ClientDepositControl1 = new Controls.ClientDepositControl(this, ctx);
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.Bar2 = new DevExpress.XtraBars.Bar();
			this.BarSubItem_File = new DevExpress.XtraBars.BarSubItem();
			this.BarButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
			this.BarAndDockingController1 = new DevExpress.XtraBars.BarAndDockingController(this.components);
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.BarAndDockingController1).BeginInit();
			this.SuspendLayout();
			//
			//NewClientDepositBatch1
			//
			this.NewClientDepositBatch1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NewClientDepositBatch1.Location = new System.Drawing.Point(0, 24);
			this.NewClientDepositBatch1.Name = "NewClientDepositBatch1";
			this.NewClientDepositBatch1.Size = new System.Drawing.Size(557, 409);
			this.NewClientDepositBatch1.TabIndex = 0;
			//
			//ClientDepositControl1
			//
			this.ClientDepositControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ClientDepositControl1.Location = new System.Drawing.Point(0, 24);
			this.ClientDepositControl1.Name = "ClientDepositControl1";
			this.ClientDepositControl1.Size = new System.Drawing.Size(557, 409);
			this.ClientDepositControl1.TabIndex = 1;
			//
			//barManager1
			//
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
			this.barManager1.Controller = this.BarAndDockingController1;
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarSubItem_File,
				this.BarButtonItem1
			});
			this.barManager1.MainMenu = this.Bar2;
			this.barManager1.MaxItemId = 2;
			//
			//Bar2
			//
			this.Bar2.BarName = "Main menu";
			this.Bar2.DockCol = 0;
			this.Bar2.DockRow = 0;
			this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
			this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_File) });
			this.Bar2.OptionsBar.MultiLine = true;
			this.Bar2.OptionsBar.UseWholeRow = true;
			this.Bar2.Text = "Main menu";
			//
			//BarSubItem_File
			//
			this.BarSubItem_File.Caption = "&File";
			this.BarSubItem_File.Id = 0;
			this.BarSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem1) });
			this.BarSubItem_File.Name = "BarSubItem_File";
			//
			//BarButtonItem1
			//
			this.BarButtonItem1.Caption = "&Exit";
			this.BarButtonItem1.Id = 1;
			this.BarButtonItem1.Name = "BarButtonItem1";
			//
			//Form_DepositManual
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(557, 433);
			this.Controls.Add(this.ClientDepositControl1);
			this.Controls.Add(this.NewClientDepositBatch1);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "Form_DepositManual";
			this.Text = "Add payments to a batch";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.BarAndDockingController1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal NewClientDepositBatch NewClientDepositBatch1;
		protected internal Controls.ClientDepositControl ClientDepositControl1;
		protected internal DevExpress.XtraBars.BarManager barManager1;
		protected internal DevExpress.XtraBars.Bar Bar2;
		protected internal DevExpress.XtraBars.BarAndDockingController BarAndDockingController1;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlTop;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlRight;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem1;
		protected internal DevExpress.XtraBars.BarSubItem BarSubItem_File;
	}
}
