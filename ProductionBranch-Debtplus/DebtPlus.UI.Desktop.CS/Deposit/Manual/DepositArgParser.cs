using System;
using System.Globalization;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual
{
    public partial class DepositArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Deposit batch
        /// </summary>

        private Int32 _batch = -1;

        public Int32 Batch
        {
            get { return _batch; }
            set { _batch = value; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public DepositArgParser() : base(new string[] { "b" })
        {
            // Find the batch from the configuration file if there is one
            string TempString = global::DebtPlus.Configuration.Config.GetConfigValueForKey(global::DebtPlus.Configuration.Config.GetConfigSettings(global::DebtPlus.Configuration.Config.ConfigSet.DepositManual), "batch");

            if (TempString != null && TempString != string.Empty)
            {
                Int32 batchID = Batch;
                if (!Int32.TryParse(TempString, NumberStyles.Integer, CultureInfo.CurrentCulture, out batchID) || batchID <= 0)
                {
                    batchID = -1;
                }
                Batch = batchID;
            }
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine("Usage: " + fname + " [-b#]")
            //AppendErrorLine("       -b# : existing batch ID")
        }

        /// <summary>
        /// Process a switch item
        /// </summary>
        protected override Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = SwitchStatus.NoError;
            switch (switchSymbol.ToLower())
            {
                case "b":
                    Int32 tempBatch = 0;
                    if (Int32.TryParse(switchValue, NumberStyles.Integer, CultureInfo.CurrentCulture, out tempBatch) && tempBatch <= 0)
                    {
                        ss = SwitchStatus.YesError;
                    }
                    Batch = tempBatch;
                    break;

                case "?":
                    ss = SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = SwitchStatus.YesError;
                    break;
            }
            return ss;
        }
    }
}