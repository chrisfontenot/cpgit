using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
    internal partial class BatchListControl
    {
        private DataView vue;

        public BatchListControl(Form_DepositManual p, DepositManualContext ctx) : base(p, ctx)
        {
            InitializeComponent();
            ListBoxControl1.DoubleClick += ListBoxControl1_DoubleClick;
            ListBoxControl1.DrawItem += ListBoxControl1_DrawItem;
        }

        /// <summary>
        /// DOUBLE-CLICK is "EDIT"
        /// </summary>
        private void ListBoxControl1_DoubleClick(object sender, EventArgs e)
        {
            var _with1 = (DevExpress.XtraEditors.ListBoxControl)sender;

            // Find where the use clicked
            Int32 HitIndex = _with1.IndexFromPoint(_with1.PointToClient(System.Windows.Forms.Control.MousePosition));

            // If the user clicked on an item then it should be zero or positive. Blank area = negative

            if (HitIndex >= 0)
            {
                // From the index, find the row being clicked.
                DataRowView new_drv = vue[HitIndex];

                // The row can not be the one that we are just creating
                if (!new_drv.IsNew)
                {
                    new_drv.BeginEdit();

                    // Switch back to the add control
                    var _with2 = (ClientDepositControl)Parent;
                    _with2.NewDepositItemControl1.GiveFocus();
                    _with2.DepositItemControl1.TakeFocus();
                    _with2.DepositItemControl1.ReadRecord(new_drv);
                }
            }
        }

        /// <summary>
        /// Load the batch list on the first call
        /// </summary>

        public void LoadList(DataTable tbl)
        {
            // Update the list with the display information
            vue = new DataView(tbl, string.Empty, "deposit_batch_detail desc", DataViewRowState.CurrentRows);

            var _with3 = ListBoxControl1;
            _with3.BeginUpdate();
            _with3.DataSource = vue;
            _with3.DisplayMember = "amount";
            _with3.ValueMember = "deposit_batch_detail";
            _with3.EndUpdate();
        }

        /// <summary>
        /// Draw the items with a dollar sign
        /// </summary>
        private void ListBoxControl1_DrawItem(object sender, DevExpress.XtraEditors.ListBoxDrawItemEventArgs e)
        {
            var _with4 = (DevExpress.XtraEditors.ListBoxControl)sender;

            // Put the background in the object
            e.Appearance.DrawBackground(e.Cache, e.Bounds);

            // Find the item for the display

            if (e.Index >= 0)
            {
                // From the index, find the row.
                DataRow row = ((DataView)_with4.DataSource)[e.Index].Row;

                // From the row, find the amount to be displayed
                decimal CreditAmt = 0m;
                if (row[_with4.DisplayMember] != null && !object.ReferenceEquals(row[_with4.DisplayMember], DBNull.Value))
                    CreditAmt = Convert.ToDecimal(row[_with4.DisplayMember]);

                // Format the display string and draw it
                string txt = string.Format("{0:c}", CreditAmt);
                e.Appearance.DrawString(e.Cache, txt, e.Bounds);
            }

            // Indicate that we displayed the row and do not do normal processing
            e.Handled = true;
        }

        public void ScrollToRecord(System.Data.DataRow Record)
        {
            // Ensure that the top most item is visable at the insertion. This is where the new item is going to be inserted
            // if it is not inserted yet.
            var _with5 = ListBoxControl1;
            if (_with5.ItemCount > 0)
            {
                ListBoxControl1.MakeItemVisible(0);
                ListBoxControl1.SelectedIndex = 0;
            }
        }
    }
}