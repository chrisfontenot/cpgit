using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
	partial class ClientDepositControl : BaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton_ok = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_cancel = new DevExpress.XtraEditors.SimpleButton();
			this.NewDepositItemControl1 = new DepositItemNewControl(depositManualForm, ctx);
			this.BatchListControl1 = new BatchListControl(depositManualForm, ctx);
			this.BatchStatusControl1 = new BatchStatusControl(depositManualForm, ctx);
			this.DepositItemControl1 = new DepositItemOldControl(depositManualForm, ctx);
			this.SuspendLayout();
			//
			//SimpleButton_ok
			//
			this.SimpleButton_ok.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_ok.Location = new System.Drawing.Point(358, 14);
			this.SimpleButton_ok.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.Name = "SimpleButton_ok";
			this.SimpleButton_ok.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.TabIndex = 3;
			this.SimpleButton_ok.Tag = "new ok";
			this.SimpleButton_ok.Text = "&Save";
			//
			//SimpleButton_cancel
			//
			this.SimpleButton_cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_cancel.Location = new System.Drawing.Point(358, 52);
			this.SimpleButton_cancel.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.Name = "SimpleButton_cancel";
			this.SimpleButton_cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.TabIndex = 4;
			this.SimpleButton_cancel.Tag = "new cancel";
			this.SimpleButton_cancel.Text = "&Quit";
			//
			//NewDepositItemControl1
			//
			this.NewDepositItemControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.NewDepositItemControl1.Location = new System.Drawing.Point(4, 98);
			this.NewDepositItemControl1.Name = "NewDepositItemControl1";
			this.NewDepositItemControl1.Size = new System.Drawing.Size(328, 311);
			this.NewDepositItemControl1.TabIndex = 1;
			//
			//BatchListControl1
			//
			this.BatchListControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Right);
			this.BatchListControl1.Location = new System.Drawing.Point(338, 98);
			this.BatchListControl1.Name = "BatchListControl1";
			this.BatchListControl1.Size = new System.Drawing.Size(115, 309);
			this.BatchListControl1.TabIndex = 5;
			this.BatchListControl1.TabStop = false;
			//
			//BatchStatusControl1
			//
			this.BatchStatusControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.BatchStatusControl1.Location = new System.Drawing.Point(4, 4);
			this.BatchStatusControl1.Name = "BatchStatusControl1";
			this.BatchStatusControl1.Size = new System.Drawing.Size(328, 95);
			this.BatchStatusControl1.TabIndex = 0;
			this.BatchStatusControl1.TabStop = false;
			//
			//DepositItemControl1
			//
			this.DepositItemControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.DepositItemControl1.Location = new System.Drawing.Point(4, 98);
			this.DepositItemControl1.Name = "DepositItemControl1";
			this.DepositItemControl1.Size = new System.Drawing.Size(328, 311);
			this.DepositItemControl1.TabIndex = 2;
			//
			//ClientDepositControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.SimpleButton_cancel);
			this.Controls.Add(this.SimpleButton_ok);
			this.Controls.Add(this.BatchListControl1);
			this.Controls.Add(this.BatchStatusControl1);
			this.Controls.Add(this.DepositItemControl1);
			this.Controls.Add(this.NewDepositItemControl1);
			this.Name = "ClientDepositControl";
			this.Size = new System.Drawing.Size(458, 412);
			this.ResumeLayout(false);

		}
		protected internal BatchStatusControl BatchStatusControl1;
		protected internal BatchListControl BatchListControl1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_ok;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_cancel;
		protected internal DepositItemNewControl NewDepositItemControl1;
		protected internal DepositItemOldControl DepositItemControl1;
	}
}
