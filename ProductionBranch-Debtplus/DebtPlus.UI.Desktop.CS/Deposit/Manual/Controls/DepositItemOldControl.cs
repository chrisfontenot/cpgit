using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
    internal partial class DepositItemOldControl
    {
        public DepositItemOldControl(Form_DepositManual p, DepositManualContext ctx) : base(p, ctx)
        {
            InitializeComponent();
        }

        private DataRowView EditDrv;

        public void ReadRecord(DataRowView drv)
        {
            EditDrv = drv;

            // Ensure that the list is loaded
            if (LookUpEdit_tran_subtype.Properties.DataSource == null)
            {
                LookUpEdit_tran_subtype.Properties.DataSource = new DataView(depositManualForm.DepositSubtypeTable(), string.Empty, "sort_order, description", DataViewRowState.CurrentRows);
                LookUpEdit_tran_subtype.EditValueChanging += global::DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            }

            // Load the control with the values
            DepositClient = EditDrv["client"];
            DepositAmount = EditDrv["amount"];
            DepositTranSubtype = EditDrv["tran_subtype"];
            DepositReference = EditDrv["reference"];

            object obj = EditDrv["item_date"];
            if (obj == null || object.ReferenceEquals(obj, System.DBNull.Value))
                obj = DateTime.Now.Date;
            DepositItemDate = obj;

            base.ReadRecord();
        }

        public void CancelEvent()
        {
            // Stop the alert from blinking
            HideBlinkAlert();

            // Cancel the changes to this row
            EditDrv.CancelEdit();

            // Switch back to the add control
            var _with1 = (ClientDepositControl)Parent;
            _with1.DepositItemControl1.GiveFocus();
            _with1.NewDepositItemControl1.TakeFocus();
        }

        public void AcceptEvent()
        {
            // Stop the alert from blinking
            HideBlinkAlert();

            // Complete the edit operation on the current row
            object obj = DepositItemDate;
            if (obj == null || object.ReferenceEquals(obj, System.DBNull.Value))
                obj = DateTime.Now.Date;
            EditDrv["item_date"] = obj;

            EditDrv["client"] = DepositClient;
            EditDrv["amount"] = DepositAmount;
            EditDrv["tran_subtype"] = DepositTranSubtype;
            EditDrv["reference"] = DepositReference;

            EditDrv.EndEdit();

            // Switch back to the add control
            var _with2 = (ClientDepositControl)Parent;
            _with2.DepositItemControl1.GiveFocus();
            _with2.NewDepositItemControl1.TakeFocus();
        }

        public void TakeFocus()
        {
            // Bring our control to the front
            var _with3 = this;
            _with3.BringToFront();
            _with3.TabStop = true;
            _with3.Visible = true;

            // Go to the client id again.
            _with3.Focus();
            _with3.ClientID_client.Focus();

            // Change the parent form to suit our needs
            var _with4 = (ClientDepositControl)Parent;

            // Disable the batch list control
            _with4.BatchListControl1.Enabled = false;

            // Change the form buttons to come here when they are pressed
            var _with5 = _with4.SimpleButton_cancel;
            _with5.Text = "&Cancel";
            _with5.Tag = "cancel";

            var _with6 = _with4.SimpleButton_ok;
            _with6.Text = "&OK";
            _with6.Tag = "ok";
            _with6.Enabled = !HasErrors();
        }

        public void GiveFocus()
        {
            // Hide our own input form since we don't need it anymore.
            var _with7 = this;
            _with7.SendToBack();
            _with7.Visible = false;
            _with7.TabStop = false;
        }
    }
}