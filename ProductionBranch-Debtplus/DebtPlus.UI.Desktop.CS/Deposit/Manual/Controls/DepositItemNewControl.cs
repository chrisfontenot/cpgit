using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
    internal partial class DepositItemNewControl
    {
        public event RecordAddedEventHandler RecordAdded;

        public delegate void RecordAddedEventHandler(object Sender, DataRow record);

        public DepositItemNewControl(Form_DepositManual p, DepositManualContext ctx) : base(p, ctx)
        {
            InitializeComponent();
        }

        // The client for which the information is displayed

        private DataRowView NewDrv;

        // Information saved for the last item should this be a new record
        private object last_tran_subtype = Defaults.DefaultSubType();

        private object last_item_date = DateTime.Now;

        private object last_reference = DBNull.Value;

        public override void ReadRecord()
        {
            // Ensure that there is a datasource for the lookup control
            if (LookUpEdit_tran_subtype.Properties.DataSource == null)
            {
                LookUpEdit_tran_subtype.Properties.DataSource = new DataView(depositManualForm.DepositSubtypeTable(), string.Empty, "sort_order, description", DataViewRowState.CurrentRows);
                LookUpEdit_tran_subtype.EditValueChanging += global::DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            }

            AddRecord();
            base.ReadRecord();
        }

        public void AddRecord()
        {
            DataTable tbl = ctx.ds.Tables["deposit_batch_details"];

            // Add a new row to the table
            NewDrv = tbl.DefaultView.AddNew();
            NewDrv.BeginEdit();

            // Ensure that the item date has a value.
            object obj = last_item_date;
            if (obj == null || object.ReferenceEquals(obj, DBNull.Value))
                obj = DateTime.Now.Date;
            DepositItemDate = obj;
            NewDrv["item_date"] = obj;

            NewDrv["tran_subtype"] = last_tran_subtype;

            // If we want the same reference then pre-load it
            if (Defaults.UseSameReference())
            {
                NewDrv["reference"] = last_reference;
            }
            else
            {
                NewDrv["reference"] = string.Empty;
            }

            // Load the control with the values
            DepositClient = NewDrv["client"];
            DepositAmount = NewDrv["amount"];
            DepositTranSubtype = NewDrv["tran_subtype"];
            DepositReference = NewDrv["reference"];
        }

        public void AcceptEvent()
        {
            // Ensure that the item date has a value.
            object obj = DepositItemDate;
            if (obj == null || object.ReferenceEquals(obj, DBNull.Value))
                obj = DateTime.Now.Date;
            NewDrv["item_date"] = obj;

            // The item is unbound. Update the row values.
            NewDrv["client"] = DepositClient;
            NewDrv["amount"] = DepositAmount;
            NewDrv["tran_subtype"] = DepositTranSubtype;
            NewDrv["reference"] = DepositReference;

            // Remember the current settings for the new record
            last_tran_subtype = NewDrv["tran_subtype"];
            last_item_date = NewDrv["item_date"];
            last_reference = NewDrv["reference"];

            // Complete the edit operation and create a new record
            NewDrv.EndEdit();

            // Tell the parent that we added the record
            if (RecordAdded != null)
            {
                RecordAdded(this, NewDrv.Row);
            }

            // Create a new record for the list
            AddRecord();

            // Reset the keyboard focus to the client ID
            Focus();
            ClientID_client.Focus();
            ClientID_client.EditValue = null;
            ReadClientInfo(null);
        }

        public void CancelEvent()
        {
            // Find the table and cancel any pending edit operation
            if (NewDrv != null && NewDrv.IsNew)
                NewDrv.CancelEdit();

            // Tell the parent that we have finished
            RaiseCompleted();
        }

        public void TakeFocus()
        {
            // Bring our control to the front
            var _with1 = this;
            _with1.BringToFront();
            _with1.TabStop = true;
            _with1.Visible = true;

            // Go to the client id again.
            _with1.Focus();
            _with1.ClientID_client.Focus();

            // Change the parent form to suit our needs
            var _with2 = (ClientDepositControl)Parent;

            // Disable the batch list control
            _with2.BatchListControl1.Enabled = true;

            // Change the form buttons to come here when they are pressed
            var _with3 = _with2.SimpleButton_cancel;
            _with3.Text = "&Quit";
            _with3.Tag = "new cancel";

            var _with4 = _with2.SimpleButton_ok;
            _with4.Text = "&Save";
            _with4.Tag = "new ok";
            _with4.Enabled = !HasErrors();
        }

        public void GiveFocus()
        {
            // Hide our own input form since we don't need it anymore.
            var _with5 = this;
            _with5.SendToBack();
            _with5.Visible = false;
            _with5.TabStop = false;
        }
    }
}