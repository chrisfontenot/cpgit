using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
	partial class BatchStatusControl : BaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.LabelControl_batch_id = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_total = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_batch_count = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			this.SuspendLayout();
			//
			//GroupControl1
			//
			this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GroupControl1.Controls.Add(this.LabelControl_batch_id);
			this.GroupControl1.Controls.Add(this.LabelControl_total);
			this.GroupControl1.Controls.Add(this.LabelControl_batch_count);
			this.GroupControl1.Controls.Add(this.LabelControl3);
			this.GroupControl1.Controls.Add(this.LabelControl2);
			this.GroupControl1.Controls.Add(this.LabelControl1);
			this.GroupControl1.Location = new System.Drawing.Point(4, 4);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(105, 85);
			this.GroupControl1.TabIndex = 0;
			this.GroupControl1.Text = "Batch Information";
			//
			//LabelControl_batch_id
			//
			this.LabelControl_batch_id.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_batch_id.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl_batch_id.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
			this.LabelControl_batch_id.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl_batch_id.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_batch_id.Location = new System.Drawing.Point(71, 24);
			this.LabelControl_batch_id.Name = "LabelControl_batch_id";
			this.LabelControl_batch_id.Size = new System.Drawing.Size(28, 13);
			this.LabelControl_batch_id.TabIndex = 5;
			this.LabelControl_batch_id.Text = "0";
			this.LabelControl_batch_id.ToolTip = "ID of the deposit batch";
			//
			//LabelControl_total
			//
			this.LabelControl_total.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_total.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl_total.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
			this.LabelControl_total.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl_total.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total.Location = new System.Drawing.Point(71, 62);
			this.LabelControl_total.Name = "LabelControl_total";
			this.LabelControl_total.Size = new System.Drawing.Size(28, 13);
			this.LabelControl_total.TabIndex = 4;
			this.LabelControl_total.Text = "$0.00";
			this.LabelControl_total.ToolTip = "Total amount of the deposit batch";
			//
			//LabelControl_batch_count
			//
			this.LabelControl_batch_count.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_batch_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl_batch_count.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
			this.LabelControl_batch_count.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl_batch_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_batch_count.Location = new System.Drawing.Point(72, 43);
			this.LabelControl_batch_count.Name = "LabelControl_batch_count";
			this.LabelControl_batch_count.Size = new System.Drawing.Size(28, 13);
			this.LabelControl_batch_count.TabIndex = 3;
			this.LabelControl_batch_count.Text = "0";
			this.LabelControl_batch_count.ToolTip = "Number of deposit items in the batch";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(6, 62);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(54, 13);
			this.LabelControl3.TabIndex = 2;
			this.LabelControl3.Text = "Batch Total";
			this.LabelControl3.UseMnemonic = false;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(6, 43);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(59, 13);
			this.LabelControl2.TabIndex = 1;
			this.LabelControl2.Text = "Batch Count";
			this.LabelControl2.UseMnemonic = false;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(6, 24);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(41, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Batch ID";
			this.LabelControl1.UseMnemonic = false;
			//
			//BatchStatusControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GroupControl1);
			this.Name = "BatchStatusControl";
			this.Size = new System.Drawing.Size(113, 95);
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.GroupControl GroupControl1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_batch_id;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_batch_count;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
	}
}
