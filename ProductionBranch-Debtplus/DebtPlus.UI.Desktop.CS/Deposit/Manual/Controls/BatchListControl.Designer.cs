using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
	partial class BatchListControl : BaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			DevExpress.Utils.SuperToolTip SuperToolTip1 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipTitleItem ToolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
			DevExpress.Utils.ToolTipItem ToolTipItem1 = new DevExpress.Utils.ToolTipItem();
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.ListBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.ListBoxControl1).BeginInit();
			this.SuspendLayout();
			//
			//GroupControl1
			//
			this.GroupControl1.Controls.Add(this.ListBoxControl1);
			this.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GroupControl1.Location = new System.Drawing.Point(0, 0);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(150, 150);
			this.GroupControl1.TabIndex = 0;
			this.GroupControl1.Text = "Batch Items";
			//
			//ListBoxControl1
			//
			this.ListBoxControl1.Appearance.Options.UseTextOptions = true;
			this.ListBoxControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ListBoxControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
			this.ListBoxControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.ListBoxControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.ListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ListBoxControl1.Location = new System.Drawing.Point(2, 22);
			this.ListBoxControl1.Name = "ListBoxControl1";
			this.ListBoxControl1.Size = new System.Drawing.Size(146, 126);
			ToolTipTitleItem1.Text = "Current Deposit Items";
			ToolTipItem1.LeftIndent = 6;
			ToolTipItem1.Text = "This is the list of the pending deposits items. It is made to resemble an adding " + "machine tape. If you double-click on an item, you may edit that record.";
			SuperToolTip1.Items.Add(ToolTipTitleItem1);
			SuperToolTip1.Items.Add(ToolTipItem1);
			this.ListBoxControl1.SuperTip = SuperToolTip1;
			this.ListBoxControl1.TabIndex = 1;
			this.ListBoxControl1.SelectionMode = SelectionMode.One;
			//
			//BatchListControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GroupControl1);
			this.Name = "BatchListControl";
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.ListBoxControl1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.GroupControl GroupControl1;
		protected internal DevExpress.XtraEditors.ListBoxControl ListBoxControl1;
	}
}
