using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
	partial class DepositItemOldControl : DepositItemBaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
			this.GroupControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_reference.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_tran_subtype.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_deposit_amount.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID_client.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl4
			//
			this.LabelControl4.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.LabelControl4.Appearance.Options.UseBackColor = true;
			//
			//LabelControl3
			//
			this.LabelControl3.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.LabelControl3.Appearance.Options.UseBackColor = true;
			//
			//LabelControl2
			//
			this.LabelControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.LabelControl2.Appearance.Options.UseBackColor = true;
			//
			//LabelControl1
			//
			this.LabelControl1.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.LabelControl1.Appearance.Options.UseBackColor = true;
			//
			//LabelControl_coapplicant
			//
			this.LabelControl_coapplicant.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			this.LabelControl_coapplicant.Appearance.Options.UseBackColor = true;
			//
			//LabelControl_applicant
			//
			this.LabelControl_applicant.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			this.LabelControl_applicant.Appearance.Options.UseBackColor = true;
			//
			//LabelControl_address
			//
			this.LabelControl_address.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			this.LabelControl_address.Appearance.Options.UseBackColor = true;
			this.LabelControl_address.Appearance.Options.UseTextOptions = true;
			this.LabelControl_address.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl_address.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
			this.LabelControl_address.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl_address.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			//
			//LabelControl_deposit_amount
			//
			this.LabelControl_deposit_amount.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			this.LabelControl_deposit_amount.Appearance.Options.UseBackColor = true;
			this.LabelControl_deposit_amount.Appearance.Options.UseTextOptions = true;
			this.LabelControl_deposit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			//
			//TextEdit1
			//
			//
			//LookUpEdit1
			//
			//
			//CalcEdit1
			//
			this.CalcEdit_deposit_amount.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_deposit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_deposit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_deposit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_deposit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_deposit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_deposit_amount.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_deposit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_deposit_amount.Properties.EditFormat.FormatString = "{0:c}";
			this.CalcEdit_deposit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_deposit_amount.Properties.Mask.BeepOnError = true;
			this.CalcEdit_deposit_amount.Properties.Mask.EditMask = "c";
			//
			//ClientID1
			//
			this.ClientID_client.Properties.Appearance.Options.UseTextOptions = true;
			this.ClientID_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ClientID_client.Properties.DisplayFormat.FormatString = "0000000";
			this.ClientID_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ClientID_client.Properties.EditFormat.FormatString = "f0";
			this.ClientID_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.ClientID_client.Properties.Mask.BeepOnError = true;
			this.ClientID_client.Properties.Mask.EditMask = "\\d*";
			this.ClientID_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			//
			//DepositItemOldControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "DepositItemOldControl";
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
			this.GroupControl2.ResumeLayout(false);
			this.GroupControl2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_reference.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_tran_subtype.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_deposit_amount.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID_client.Properties).EndInit();
			this.ResumeLayout(false);
		}
	}
}
