using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
    internal partial class BatchStatusControl
    {
        private DataTable _tbl = null;

        protected DataTable tbl
        {
            get { return _tbl; }
            set
            {
                if (_tbl != null)
                {
                    tbl.ColumnChanged -= tbl_ColumnChanged;
                    tbl.RowDeleted -= tbl_RowDeleted;
                    tbl.TableCleared -= tbl_TableCleared;
                    tbl.TableNewRow -= tbl_TableNewRow;
                }
                _tbl = value;
                if (_tbl != null)
                {
                    tbl.ColumnChanged += tbl_ColumnChanged;
                    tbl.RowDeleted += tbl_RowDeleted;
                    tbl.TableCleared += tbl_TableCleared;
                    tbl.TableNewRow += tbl_TableNewRow;
                }
            }
        }

        public BatchStatusControl(Form_DepositManual p, DepositManualContext ctx) : base(p, ctx)
        {
            InitializeComponent();
        }

        public void LoadList(DataTable tbl)
        {
            this.tbl = tbl;

            // Calculate the information for the display
            LabelControl_batch_id.Text = string.Format("{0:0000000000}", ctx.ap.Batch);
            RecomputeTotals();
        }

        private void tbl_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            if (e.Column.ColumnName == "amount")
                RecomputeTotals();
        }

        private void tbl_RowDeleted(object sender, DataRowChangeEventArgs e)
        {
            RecomputeTotals();
        }

        private void tbl_TableCleared(object sender, DataTableClearEventArgs e)
        {
            RecomputeTotals();
        }

        private void tbl_TableNewRow(object sender, DataTableNewRowEventArgs e)
        {
            RecomputeTotals();
        }

        private void RecomputeTotals()
        {
            // The total dollar amount of the batch
            object objTotal = tbl.Compute("sum(amount)", string.Empty);
            if (objTotal == null || object.ReferenceEquals(objTotal, DBNull.Value))
                objTotal = 0m;
            LabelControl_total.Text = string.Format("{0:c}", Convert.ToDecimal(objTotal));

            // The total number of items in the batch
            object objCount = tbl.Compute("count(deposit_batch_detail)", string.Empty);
            if (objCount == null || object.ReferenceEquals(objCount, DBNull.Value))
                objCount = 0;
            LabelControl_batch_count.Text = string.Format("{0:n0}", objCount);
        }
    }
}