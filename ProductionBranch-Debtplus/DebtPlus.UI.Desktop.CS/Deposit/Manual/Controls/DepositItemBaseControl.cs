#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.Data.Controls;
using DebtPlus.Notes;
using DebtPlus.UI.Client.Service;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Mask;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
    internal partial class DepositItemBaseControl : BaseControl
    {
        public DepositItemBaseControl() : base()
        {
            InitializeComponent();
        }

        #region " Initialize Component "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && components != null)
                {
                    components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        internal TextEdit TextEditItemDate;

        protected internal BlinkLabel BlinkLabel1;
        //Required by the Windows Form Designer

        private IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private void InitializeComponent()
        {
            SuperToolTip SuperToolTip7 = new SuperToolTip();
            ToolTipTitleItem ToolTipTitleItem7 = new ToolTipTitleItem();
            ToolTipItem ToolTipItem7 = new ToolTipItem();
            ComponentResourceManager resources = new ComponentResourceManager(typeof(DepositItemBaseControl));
            SerializableAppearanceObject SerializableAppearanceObject2 = new SerializableAppearanceObject();
            SuperToolTip SuperToolTip1 = new SuperToolTip();
            ToolTipTitleItem ToolTipTitleItem1 = new ToolTipTitleItem();
            ToolTipItem ToolTipItem1 = new ToolTipItem();
            SuperToolTip SuperToolTip2 = new SuperToolTip();
            ToolTipTitleItem ToolTipTitleItem2 = new ToolTipTitleItem();
            ToolTipItem ToolTipItem2 = new ToolTipItem();
            SuperToolTip SuperToolTip8 = new SuperToolTip();
            ToolTipTitleItem ToolTipTitleItem8 = new ToolTipTitleItem();
            ToolTipItem ToolTipItem8 = new ToolTipItem();
            SuperToolTip SuperToolTip4 = new SuperToolTip();
            ToolTipTitleItem ToolTipTitleItem4 = new ToolTipTitleItem();
            ToolTipItem ToolTipItem4 = new ToolTipItem();
            SuperToolTip SuperToolTip5 = new SuperToolTip();
            ToolTipTitleItem ToolTipTitleItem5 = new ToolTipTitleItem();
            ToolTipItem ToolTipItem5 = new ToolTipItem();
            this.GroupControl1 = new GroupControl();
            this.BlinkLabel1 = new BlinkLabel();
            this.LabelControl_coapplicant = new LabelControl();
            this.LabelControl_applicant = new LabelControl();
            this.LabelControl_address = new LabelControl();
            this.LabelControl_deposit_amount = new LabelControl();
            this.LabelControl4 = new LabelControl();
            this.LabelControl3 = new LabelControl();
            this.LabelControl2 = new LabelControl();
            this.LabelControl1 = new LabelControl();
            this.GroupControl2 = new GroupControl();
            this.TextEditItemDate = new TextEdit();
            this.ClientID_client = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.SimpleButton_Notes = new SimpleButton();
            this.LabelControl9 = new LabelControl();
            this.TextEdit_reference = new TextEdit();
            this.LabelControl8 = new LabelControl();
            this.LabelControl7 = new LabelControl();
            this.LookUpEdit_tran_subtype = new LookUpEdit();
            this.CalcEdit_deposit_amount = new CalcEdit();
            this.LabelControl6 = new LabelControl();
            this.LabelControl5 = new LabelControl();
            ((ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((ISupportInitialize)this.GroupControl2).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((ISupportInitialize)this.TextEditItemDate.Properties).BeginInit();
            ((ISupportInitialize)this.ClientID_client.Properties).BeginInit();
            ((ISupportInitialize)this.TextEdit_reference.Properties).BeginInit();
            ((ISupportInitialize)this.LookUpEdit_tran_subtype.Properties).BeginInit();
            ((ISupportInitialize)this.CalcEdit_deposit_amount.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GroupControl1
            //
            this.GroupControl1.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
            this.GroupControl1.Controls.Add(this.BlinkLabel1);
            this.GroupControl1.Controls.Add(this.LabelControl_coapplicant);
            this.GroupControl1.Controls.Add(this.LabelControl_applicant);
            this.GroupControl1.Controls.Add(this.LabelControl_address);
            this.GroupControl1.Controls.Add(this.LabelControl_deposit_amount);
            this.GroupControl1.Controls.Add(this.LabelControl4);
            this.GroupControl1.Controls.Add(this.LabelControl3);
            this.GroupControl1.Controls.Add(this.LabelControl2);
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Location = new Point(4, 4);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new Size(302, 137);
            this.GroupControl1.TabIndex = 0;
            this.GroupControl1.Text = "Client Information";
            //
            //BlinkLabel1
            //
            this.BlinkLabel1.Anchor = (AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Left) | AnchorStyles.Right);
            this.BlinkLabel1.Appearance.BackColor = Color.Red;
            this.BlinkLabel1.Appearance.Font = new Font("Tahoma", 8.25f, FontStyle.Bold);
            this.BlinkLabel1.Appearance.ForeColor = Color.White;
            this.BlinkLabel1.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
            this.BlinkLabel1.AutoSizeMode = LabelAutoSizeMode.None;
            this.BlinkLabel1.BlinkTime = 500;
            this.BlinkLabel1.CausesValidation = false;
            this.BlinkLabel1.Location = new Point(178, 117);
            this.BlinkLabel1.Margin = new Padding(5);
            this.BlinkLabel1.Name = "BlinkLabel1";
            this.BlinkLabel1.Padding = new Padding(0, 2, 0, 2);
            this.BlinkLabel1.Size = new Size(119, 17);
            this.BlinkLabel1.TabIndex = 9;
            this.BlinkLabel1.Text = "INACTIVE client";
            this.BlinkLabel1.UseMnemonic = false;
            this.BlinkLabel1.Visible = false;
            //
            //LabelControl_coapplicant
            //
            this.LabelControl_coapplicant.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
            this.LabelControl_coapplicant.Appearance.BackColor = Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.LabelControl_coapplicant.AutoSizeMode = LabelAutoSizeMode.None;
            this.LabelControl_coapplicant.Location = new Point(88, 43);
            this.LabelControl_coapplicant.Name = "LabelControl_coapplicant";
            this.LabelControl_coapplicant.Size = new Size(209, 13);
            this.LabelControl_coapplicant.TabIndex = 3;
            this.LabelControl_coapplicant.ToolTip = "Name of the co-applicant for the client";
            this.LabelControl_coapplicant.UseMnemonic = false;
            //
            //LabelControl_applicant
            //
            this.LabelControl_applicant.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
            this.LabelControl_applicant.Appearance.BackColor = Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.LabelControl_applicant.AutoSizeMode = LabelAutoSizeMode.None;
            this.LabelControl_applicant.Location = new Point(88, 24);
            this.LabelControl_applicant.Name = "LabelControl_applicant";
            this.LabelControl_applicant.Size = new Size(209, 13);
            this.LabelControl_applicant.TabIndex = 1;
            this.LabelControl_applicant.ToolTip = "Name of the applicant for the client";
            this.LabelControl_applicant.UseMnemonic = false;
            //
            //LabelControl_address
            //
            this.LabelControl_address.Anchor = (AnchorStyles)(((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right);
            this.LabelControl_address.Appearance.BackColor = Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.LabelControl_address.Appearance.TextOptions.HAlignment = HorzAlignment.Near;
            this.LabelControl_address.Appearance.TextOptions.Trimming = Trimming.EllipsisCharacter;
            this.LabelControl_address.Appearance.TextOptions.VAlignment = VertAlignment.Top;
            this.LabelControl_address.Appearance.TextOptions.WordWrap = WordWrap.NoWrap;
            this.LabelControl_address.AutoSizeMode = LabelAutoSizeMode.None;
            this.LabelControl_address.Location = new Point(88, 62);
            this.LabelControl_address.Name = "LabelControl_address";
            this.LabelControl_address.Size = new Size(209, 51);
            this.LabelControl_address.TabIndex = 5;
            this.LabelControl_address.ToolTip = "Client's home address";
            this.LabelControl_address.UseMnemonic = false;
            //
            //LabelControl_deposit_amount
            //
            this.LabelControl_deposit_amount.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Left);
            this.LabelControl_deposit_amount.Appearance.BackColor = Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.LabelControl_deposit_amount.Appearance.TextOptions.HAlignment = HorzAlignment.Far;
            this.LabelControl_deposit_amount.AutoSizeMode = LabelAutoSizeMode.None;
            this.LabelControl_deposit_amount.Location = new Point(88, 119);
            this.LabelControl_deposit_amount.Name = "LabelControl_deposit_amount";
            this.LabelControl_deposit_amount.Size = new Size(84, 13);
            this.LabelControl_deposit_amount.TabIndex = 7;
            this.LabelControl_deposit_amount.Text = "$0.00";
            this.LabelControl_deposit_amount.ToolTip = "The normal deposit amount for this client";
            this.LabelControl_deposit_amount.UseMnemonic = false;
            //
            //LabelControl4
            //
            this.LabelControl4.Anchor = (AnchorStyles)(AnchorStyles.Bottom | AnchorStyles.Left);
            this.LabelControl4.Appearance.BackColor = Color.Transparent;
            this.LabelControl4.Location = new Point(6, 119);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new Size(76, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Deposit Amount";
            this.LabelControl4.UseMnemonic = false;
            //
            //LabelControl3
            //
            this.LabelControl3.Appearance.BackColor = Color.Transparent;
            this.LabelControl3.Location = new Point(6, 62);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new Size(39, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Address";
            this.LabelControl3.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.Appearance.BackColor = Color.Transparent;
            this.LabelControl2.Location = new Point(6, 43);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new Size(61, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Co-Applicant";
            this.LabelControl2.UseMnemonic = false;
            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.BackColor = Color.Transparent;
            this.LabelControl1.Location = new Point(6, 24);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new Size(44, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Applicant";
            this.LabelControl1.UseMnemonic = false;
            //
            //GroupControl2
            //
            this.GroupControl2.Anchor = (AnchorStyles)((AnchorStyles.Bottom | AnchorStyles.Left) | AnchorStyles.Right);
            this.GroupControl2.Controls.Add(this.TextEditItemDate);
            this.GroupControl2.Controls.Add(this.ClientID_client);
            this.GroupControl2.Controls.Add(this.SimpleButton_Notes);
            this.GroupControl2.Controls.Add(this.LabelControl9);
            this.GroupControl2.Controls.Add(this.TextEdit_reference);
            this.GroupControl2.Controls.Add(this.LabelControl8);
            this.GroupControl2.Controls.Add(this.LabelControl7);
            this.GroupControl2.Controls.Add(this.LookUpEdit_tran_subtype);
            this.GroupControl2.Controls.Add(this.CalcEdit_deposit_amount);
            this.GroupControl2.Controls.Add(this.LabelControl6);
            this.GroupControl2.Controls.Add(this.LabelControl5);
            this.GroupControl2.Location = new Point(4, 147);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new Size(302, 156);
            this.GroupControl2.TabIndex = 1;
            this.GroupControl2.TabStop = true;
            this.GroupControl2.Text = "Deposit Information";
            //
            //TextEditItemDate
            //
            this.TextEditItemDate.EnterMoveNextControl = true;
            this.TextEditItemDate.Location = new Point(88, 130);
            this.TextEditItemDate.Name = "TextEditItemDate";
            this.TextEditItemDate.Properties.DisplayFormat.FormatString = "d";
            this.TextEditItemDate.Properties.DisplayFormat.FormatType = FormatType.DateTime;
            this.TextEditItemDate.Properties.EditFormat.FormatString = "d";
            this.TextEditItemDate.Properties.EditFormat.FormatType = FormatType.DateTime;
            this.TextEditItemDate.Properties.Mask.BeepOnError = true;
            this.TextEditItemDate.Properties.Mask.EditMask = "[0-9/]*";
            this.TextEditItemDate.Properties.Mask.MaskType = MaskType.RegEx;
            this.TextEditItemDate.Size = new Size(116, 20);
            SuperToolTip7.AllowHtmlText = DefaultBoolean.True;
            ToolTipTitleItem7.Text = "Date on deposit";
            ToolTipItem7.LeftIndent = 6;
            ToolTipItem7.Text = resources.GetString("ToolTipItem7.Text");
            SuperToolTip7.Items.Add(ToolTipTitleItem7);
            SuperToolTip7.Items.Add(ToolTipItem7);
            this.TextEditItemDate.SuperTip = SuperToolTip7;
            this.TextEditItemDate.TabIndex = 9;
            //
            //ClientID_client
            //
            this.ClientID_client.EnterMoveNextControl = true;
            this.ClientID_client.Location = new Point(88, 30);
            this.ClientID_client.Name = "ClientID_client";
            this.ClientID_client.Properties.AllowNullInput = DefaultBoolean.False;
            this.ClientID_client.Properties.Appearance.Options.UseTextOptions = true;
            this.ClientID_client.Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Far;
            this.ClientID_client.Properties.Buttons.AddRange(new EditorButton[] { new EditorButton(ButtonPredefines.Glyph, "", -1, true, true, false, ImageLocation.MiddleCenter, (Image)resources.GetObject("ClientID_client.Properties.Buttons"), new KeyShortcut(Keys.None), SerializableAppearanceObject2,
            "Click here to search for a client ID", "search", null, true) });
            this.ClientID_client.Properties.CharacterCasing = CharacterCasing.Upper;
            this.ClientID_client.Properties.DisplayFormat.FormatString = "0000000";
            this.ClientID_client.Properties.DisplayFormat.FormatType = FormatType.Custom;
            this.ClientID_client.Properties.EditFormat.FormatString = "f0";
            this.ClientID_client.Properties.EditFormat.FormatType = FormatType.Numeric;
            this.ClientID_client.Properties.Mask.AutoComplete = AutoCompleteType.None;
            this.ClientID_client.Properties.Mask.BeepOnError = true;
            this.ClientID_client.Properties.Mask.EditMask = "\\d*";
            this.ClientID_client.Properties.Mask.MaskType = MaskType.RegEx;
            this.ClientID_client.Properties.ValidateOnEnterKey = true;
            this.ClientID_client.Size = new Size(116, 20);
            ToolTipTitleItem1.Text = "Client ID Number";
            ToolTipItem1.LeftIndent = 6;
            ToolTipItem1.Text = "Enter the ID number of the client into this field. The ClientId must be acceptable " + "for deposits based upon their active state.";
            SuperToolTip1.Items.Add(ToolTipTitleItem1);
            SuperToolTip1.Items.Add(ToolTipItem1);
            this.ClientID_client.SuperTip = SuperToolTip1;
            this.ClientID_client.TabIndex = 1;
            //
            //SimpleButton_Notes
            //
            this.SimpleButton_Notes.Anchor = (AnchorStyles)(AnchorStyles.Top | AnchorStyles.Right);
            this.SimpleButton_Notes.Location = new Point(222, 27);
            this.SimpleButton_Notes.Name = "SimpleButton_Notes";
            this.SimpleButton_Notes.Size = new Size(75, 23);
            ToolTipTitleItem2.Text = "Client Deposit Note";
            ToolTipItem2.LeftIndent = 6;
            ToolTipItem2.Text = "If the client included a note along with the deposit describing how the money sho" + "uld be disbursed, pressing this button will allow you to create the disbursement" + " note.";
            SuperToolTip2.Items.Add(ToolTipTitleItem2);
            SuperToolTip2.Items.Add(ToolTipItem2);
            this.SimpleButton_Notes.SuperTip = SuperToolTip2;
            this.SimpleButton_Notes.TabIndex = 10;
            this.SimpleButton_Notes.TabStop = false;
            this.SimpleButton_Notes.Text = "&Note...";
            //
            //LabelControl9
            //
            this.LabelControl9.Location = new Point(6, 134);
            this.LabelControl9.Name = "LabelControl9";
            this.LabelControl9.Size = new Size(64, 13);
            this.LabelControl9.TabIndex = 8;
            this.LabelControl9.Text = "&Date of Draft";
            //
            //TextEdit_reference
            //
            this.TextEdit_reference.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
            this.TextEdit_reference.EnterMoveNextControl = true;
            this.TextEdit_reference.Location = new Point(88, 105);
            this.TextEdit_reference.Name = "TextEdit_reference";
            this.TextEdit_reference.Size = new Size(209, 20);
            ToolTipTitleItem8.Text = "Reference Number";
            ToolTipItem8.LeftIndent = 6;
            ToolTipItem8.Text = "Include the check or money order number here.";
            SuperToolTip8.Items.Add(ToolTipTitleItem8);
            SuperToolTip8.Items.Add(ToolTipItem8);
            this.TextEdit_reference.SuperTip = SuperToolTip8;
            this.TextEdit_reference.TabIndex = 7;
            //
            //LabelControl8
            //
            this.LabelControl8.Location = new Point(6, 109);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new Size(64, 13);
            this.LabelControl8.TabIndex = 6;
            this.LabelControl8.Text = "&Reference ID";
            //
            //LabelControl7
            //
            this.LabelControl7.Location = new Point(6, 84);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new Size(65, 13);
            this.LabelControl7.TabIndex = 4;
            this.LabelControl7.Text = "&Type of Draft";
            //
            //LookUpEdit_tran_subtype
            //
            this.LookUpEdit_tran_subtype.Anchor = (AnchorStyles)((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right);
            this.LookUpEdit_tran_subtype.EnterMoveNextControl = true;
            this.LookUpEdit_tran_subtype.Location = new Point(88, 80);
            this.LookUpEdit_tran_subtype.Name = "LookUpEdit_tran_subtype";
            this.LookUpEdit_tran_subtype.Properties.Buttons.AddRange(new EditorButton[] { new EditorButton(ButtonPredefines.Combo) });
            this.LookUpEdit_tran_subtype.Properties.Columns.AddRange(new LookUpColumnInfo[] {
                new LookUpColumnInfo("tran_subtype", "ID", 20, FormatType.None, "", false, HorzAlignment.Default),
                new LookUpColumnInfo("description", "Description", 20, FormatType.None, "", true, HorzAlignment.Default, ColumnSortOrder.Ascending),
                new LookUpColumnInfo("restricted", "Restricted", 20, FormatType.Numeric, "{0:d}", false, HorzAlignment.Default),
                new LookUpColumnInfo("sort_order", "Sequence", 20, FormatType.Numeric, "{0:f0}", false, HorzAlignment.Default, ColumnSortOrder.Ascending)
            });
            this.LookUpEdit_tran_subtype.Properties.DisplayMember = "description";
            this.LookUpEdit_tran_subtype.Properties.NullText = "";
            this.LookUpEdit_tran_subtype.Properties.ShowFooter = false;
            this.LookUpEdit_tran_subtype.Properties.ShowHeader = false;
            this.LookUpEdit_tran_subtype.Properties.ValueMember = "tran_subtype";
            this.LookUpEdit_tran_subtype.Size = new Size(209, 20);
            ToolTipTitleItem4.Text = "Type of deposit item";
            ToolTipItem4.LeftIndent = 6;
            ToolTipItem4.Text = "This is the type of the deposit. It may be a money order, a check, or cash. Perso" + "nal checks are not normally accepted.";
            SuperToolTip4.Items.Add(ToolTipTitleItem4);
            SuperToolTip4.Items.Add(ToolTipItem4);
            this.LookUpEdit_tran_subtype.SuperTip = SuperToolTip4;
            this.LookUpEdit_tran_subtype.TabIndex = 5;
            this.LookUpEdit_tran_subtype.Properties.SortColumnIndex = 1;
            //
            //CalcEdit_deposit_amount
            //
            this.CalcEdit_deposit_amount.EnterMoveNextControl = true;
            this.CalcEdit_deposit_amount.Location = new Point(88, 55);
            this.CalcEdit_deposit_amount.Name = "CalcEdit_deposit_amount";
            this.CalcEdit_deposit_amount.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit_deposit_amount.Properties.Appearance.TextOptions.HAlignment = HorzAlignment.Far;
            this.CalcEdit_deposit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit_deposit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = HorzAlignment.Far;
            this.CalcEdit_deposit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit_deposit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = HorzAlignment.Far;
            this.CalcEdit_deposit_amount.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit_deposit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = HorzAlignment.Far;
            this.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit_deposit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = HorzAlignment.Far;
            this.CalcEdit_deposit_amount.Properties.Buttons.AddRange(new EditorButton[] { new EditorButton(ButtonPredefines.Combo) });
            this.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatString = "{0:c}";
            this.CalcEdit_deposit_amount.Properties.DisplayFormat.FormatType = FormatType.Numeric;
            this.CalcEdit_deposit_amount.Properties.EditFormat.FormatString = "{0:c}";
            this.CalcEdit_deposit_amount.Properties.EditFormat.FormatType = FormatType.Numeric;
            this.CalcEdit_deposit_amount.Properties.Mask.BeepOnError = true;
            this.CalcEdit_deposit_amount.Properties.Mask.EditMask = "c";
            this.CalcEdit_deposit_amount.Properties.Precision = 2;
            this.CalcEdit_deposit_amount.Size = new Size(116, 20);
            ToolTipTitleItem5.Text = "Dollar amount";
            ToolTipItem5.LeftIndent = 6;
            ToolTipItem5.Text = "Enter the amount of the deposit. If you wish, there is a calculator for adding, o" + "r subtracting items. We accept only a single dollar amount for each item.";
            SuperToolTip5.Items.Add(ToolTipTitleItem5);
            SuperToolTip5.Items.Add(ToolTipItem5);
            this.CalcEdit_deposit_amount.SuperTip = SuperToolTip5;
            this.CalcEdit_deposit_amount.TabIndex = 3;
            //
            //LabelControl6
            //
            this.LabelControl6.Location = new Point(6, 59);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new Size(37, 13);
            this.LabelControl6.TabIndex = 2;
            this.LabelControl6.Text = "&Amount";
            //
            //LabelControl5
            //
            this.LabelControl5.Location = new Point(6, 34);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new Size(27, 13);
            this.LabelControl5.TabIndex = 0;
            this.LabelControl5.Text = "&Client";
            //
            //DepositItemBaseControl
            //
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.Controls.Add(this.GroupControl2);
            this.Controls.Add(this.GroupControl1);
            this.Name = "DepositItemBaseControl";
            this.Size = new Size(309, 307);
            ((ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            ((ISupportInitialize)this.GroupControl2).EndInit();
            this.GroupControl2.ResumeLayout(false);
            this.GroupControl2.PerformLayout();
            ((ISupportInitialize)this.TextEditItemDate.Properties).EndInit();
            ((ISupportInitialize)this.ClientID_client.Properties).EndInit();
            ((ISupportInitialize)this.TextEdit_reference.Properties).EndInit();
            ((ISupportInitialize)this.LookUpEdit_tran_subtype.Properties).EndInit();
            ((ISupportInitialize)this.CalcEdit_deposit_amount.Properties).EndInit();
            this.ResumeLayout(false);
        }

        protected internal GroupControl GroupControl1;
        protected internal LabelControl LabelControl4;
        protected internal LabelControl LabelControl3;
        protected internal LabelControl LabelControl2;
        protected internal LabelControl LabelControl1;
        protected internal LabelControl LabelControl_coapplicant;
        protected internal LabelControl LabelControl_applicant;
        protected internal LabelControl LabelControl_address;
        protected internal LabelControl LabelControl_deposit_amount;
        protected internal GroupControl GroupControl2;
        protected internal TextEdit TextEdit_reference;
        protected internal LabelControl LabelControl8;
        protected internal LabelControl LabelControl7;
        protected internal LookUpEdit LookUpEdit_tran_subtype;
        protected internal CalcEdit CalcEdit_deposit_amount;
        protected internal LabelControl LabelControl6;
        protected internal LabelControl LabelControl5;
        protected internal LabelControl LabelControl9;
        protected internal SimpleButton SimpleButton_Notes;

        protected internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID_client;

        #endregion " Initialize Component "

        public event EventHandler Cancelled;

        public event EventHandler Completed;

        public DepositItemBaseControl(Form_DepositManual p, DepositManualContext ctx) : base(p, ctx)
        {
            InitializeComponent();

            SimpleButton_Notes.Click += SimpleButton_Notes_Click;
            CalcEdit_deposit_amount.EditValueChanging += CalcEdit1_EditValueChanging;
            ClientID_client.DoubleClick += ClientID_client_DoubleClick;
            ClientID_client.SearchCompleted += ClientID_client_SearchCompleted;
            ClientID_client.Validated += ClientID_client_Validated;
            ClientID_client.Validating += ClientID_client_Validating;
            TextEditItemDate.Leave += TextEditItemDate_Leave;
            this.Load += DepositItemBaseControl_Load;
        }

        protected virtual void OnCancelled(EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, e);
            }
        }

        protected void RaiseCancelled()
        {
            OnCancelled(EventArgs.Empty);
        }

        protected virtual void OnCompleted(EventArgs e)
        {
            if (Completed != null)
            {
                Completed(this, e);
            }
        }

        protected void RaiseCompleted()
        {
            OnCompleted(EventArgs.Empty);
        }

        protected object DepositItemDate
        {
            get
            {
                object item = TextEditItemDate.EditValue;
                if (item == null || object.ReferenceEquals(item, DBNull.Value))
                    return DBNull.Value;
                DateTime Result = System.DateTime.MinValue;
                string str = Convert.ToString(item).Trim();
                if (str == string.Empty)
                    return DBNull.Value;

                // Try the standard date parse to find the item date
                if (System.DateTime.TryParse(str, out Result))
                {
                    return Result;
                }

                Int32 month = default(Int32);
                Int32 day = default(Int32);
                Int32 year = default(Int32);

                // Look for the month, day, and year
                Match MatchItem = Regex.Match(str, "(<month>\\d{1,2})(<day>\\d\\d)(<year>\\d\\d\\d\\d)");
                if (!MatchItem.Success)
                {
                    MatchItem = Regex.Match(str, "(?<month>\\d{1,2})(?<day>\\d\\d)(?<year>\\d\\d)");
                }

                if (MatchItem.Success)
                {
                    year = Convert.ToInt32(MatchItem.Groups["year"].Value);
                    if (year < 100)
                    {
                        year += 1900;
                        if (year < 1950)
                            year += 100;
                    }
                }
                else
                {
                    year = DateTime.Now.Year;

                    // Look for the month and day entry
                    MatchItem = Regex.Match(str, "(?<month>\\d{1,2})(?<day>\\d\\d)");
                    if (!MatchItem.Success)
                    {
                        month = DateTime.Now.Month;

                        // Look for the day of the month as a single number entry
                        MatchItem = Regex.Match(str, "(?<day>\\d{1,2})");
                        if (!MatchItem.Success)
                        {
                            return DBNull.Value;
                        }

                        // Try to convert the item to a date
                        try
                        {
                            Result = new System.DateTime(year, month, day);
                        }
                        catch { }

                        if (Result == System.DateTime.MinValue)
                        {
                            try
                            {
                                Result = new System.DateTime(year, month, 1).AddMonths(-1);
                                return new System.DateTime(Result.Year, Result.Month, day);
                            }
                            catch { }
                            return DBNull.Value;
                        }
                    }
                }

                if (MatchItem.Success)
                {
                    month = Convert.ToInt32(MatchItem.Groups["month"].Value);
                    day = Convert.ToInt32(MatchItem.Groups["day"].Value);
                    try
                    {
                        Result = new System.DateTime(year, month, day);
                        if (Result > DateTime.Now.Date)
                        {
                            Result = Result.AddYears(-1);
                        }
                        return Result;
                    }
                    catch { }
                }
                return DBNull.Value;
            }

            set { TextEditItemDate.EditValue = value; }
        }

        protected object DepositClient
        {
            get { return global::DebtPlus.Utils.Nulls.ToDbNull(ClientID_client.EditValue); }
            set { ClientID_client.EditValue = global::DebtPlus.Utils.Nulls.v_Int32(value); }
        }

        protected object DepositAmount
        {
            get { return CalcEdit_deposit_amount.EditValue; }
            set { CalcEdit_deposit_amount.EditValue = value; }
        }

        protected object DepositTranSubtype
        {
            get { return LookUpEdit_tran_subtype.EditValue; }
            set { LookUpEdit_tran_subtype.EditValue = value; }
        }

        protected object DepositReference
        {
            get { return TextEdit_reference.EditValue; }
            set { TextEdit_reference.EditValue = value; }
        }

        protected virtual bool HasErrors()
        {
            if (ReadClient <= 0)
                return true;

            // Ensure that there is a deposit amount and type selected
            if (DepositTranSubtype == null || object.ReferenceEquals(DepositTranSubtype, DBNull.Value))
                return true;
            if (DepositAmount == null || object.ReferenceEquals(DepositAmount, DBNull.Value))
                return true;
            return Convert.ToDecimal(DepositAmount) <= 0m;
        }

        protected Int32 ReadClient = -1;

        protected void ReadClientInfo(System.Nullable<Int32> client)
        {
            // Empty the current address information
            LabelControl_address.Text = string.Empty;
            LabelControl_applicant.Text = string.Empty;
            LabelControl_coapplicant.Text = string.Empty;
            LabelControl_deposit_amount.Text = string.Empty;

            string ActiveStatus = "CRE";
            ReadClient = -1;

            if (client.GetValueOrDefault(0) > 0)
            {
                DataRow row = depositManualForm.ClientRow(client.Value);
                if (row != null)
                {
                    ReadClient = client.Value;
                    if (!object.ReferenceEquals(row["active_status"], DBNull.Value))
                        ActiveStatus = Convert.ToString(row["active_status"]);
                    if (!object.ReferenceEquals(row["applicant"], DBNull.Value))
                        LabelControl_applicant.Text = Convert.ToString(row["applicant"]);
                    if (!object.ReferenceEquals(row["coapplicant"], DBNull.Value))
                        LabelControl_coapplicant.Text = Convert.ToString(row["coapplicant"]);
                    if (!object.ReferenceEquals(row["address"], DBNull.Value))
                        LabelControl_address.Text = Convert.ToString(row["address"]);
                    if (!object.ReferenceEquals(row["deposit_amount"], DBNull.Value))
                        LabelControl_deposit_amount.Text = string.Format("{0:c}", Convert.ToDecimal(row["deposit_amount"]));
                }
            }

            // If there is no client then indicate as such
            bool ValidClient = ReadClient > 0;
            if (ValidClient)
            {
                ClientID_client.ErrorText = string.Empty;

                // Show or hide the inactive client alert condition.
                if (ActiveStatus.ToUpper() == "I")
                {
                    ShowBlinkAlert();
                }
                else
                {
                    HideBlinkAlert();
                }
            }
            else
            {
                HideBlinkAlert();
                ClientID_client.ErrorText = "Invalid Client ID";
            }

            // The notes are permitted once we have a valid client
            SimpleButton_Notes.Enabled = ValidClient;

            // Enable the record button based upon a valid record
            var _with1 = (ClientDepositControl)Parent;
            _with1.SimpleButton_ok.Enabled = !HasErrors();
        }

        protected void SimpleButton_Notes_Click(object sender, EventArgs e)
        {
            Int32 client = ClientID_client.EditValue.GetValueOrDefault(-1);
            if (ReadClient == client && client > 0)
            {
                using (NoteClass.DisbursementNote Notes = new NoteClass.DisbursementNote())
                {
                    Notes.Create(client, ctx.ap.Batch);
                }
            }
        }

        private void CalcEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            const string ErrorMessage = "Invalid amount";

            var _with2 = (ClientDepositControl)Parent;

            CalcEdit_deposit_amount.ErrorText = string.Empty;

            // A client is required
            if (ReadClient <= 0)
            {
                _with2.SimpleButton_ok.Enabled = false;
                return;
            }

            // A deposit subtype is required. (It is unlikely that it is missing, but it is required none the less.)
            if (DepositTranSubtype == null || object.ReferenceEquals(DepositTranSubtype, DBNull.Value))
            {
                _with2.SimpleButton_ok.Enabled = false;
                return;
            }

            // Ensure that the new value is positive. If negative, complain. If zero then don't allow the OK button
            if (e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value))
            {
                e.Cancel = true;
                CalcEdit_deposit_amount.ErrorText = ErrorMessage;
                return;
            }

            decimal v = Convert.ToDecimal(e.NewValue, CultureInfo.InvariantCulture);
            if (v < 0m)
            {
                e.Cancel = true;
                CalcEdit_deposit_amount.ErrorText = ErrorMessage;
                return;
            }

            if (v == 0m)
            {
                _with2.SimpleButton_ok.Enabled = false;
                return;
            }

            _with2.SimpleButton_ok.Enabled = true;
        }

        public virtual void ReadRecord()
        {
            // Read the client information if there is a ClientId to be read
            ReadClient = -1;
            ReadClientInfo(ClientID_client.EditValue);

            // Enable the accept button if there are no errors
            var _with3 = (ClientDepositControl)Parent;
            _with3.SimpleButton_ok.Enabled = !HasErrors();
        }

        private void ClientID_client_DoubleClick(object sender, EventArgs e)
        {
            Int32 id = ClientID_client.EditValue.GetValueOrDefault(-1);
            if (id > 0)
            {
                Thread thrd = new Thread(new ParameterizedThreadStart(ShowClientThread));
                thrd.IsBackground = true;
                thrd.SetApartmentState(ApartmentState.STA);
                thrd.Start(id);
            }
        }

        private void ClientID_client_SearchCompleted(object sender, CancelEventArgs e)
        {
            ClientID_client_Validating(sender, e);
            if (!e.Cancel)
            {
                e.Cancel = true;
                if (ClientID_client.ErrorText == string.Empty)
                {
                    CalcEdit_deposit_amount.Focus();
                }
            }
        }

        private Int32 ClientID_client_Validated_LastClientShown;

        private void ClientID_client_Validated(object sender, EventArgs e)
        {
            // Determine the client to be displayed. Ignore blank items.
            Int32 NewClient = ClientID_client.EditValue.GetValueOrDefault(-1);

            if (NewClient > 0)
            {
                // Do not keep displaying the same client over and over again....
                if (ClientID_client_Validated_LastClientShown != NewClient)
                {
                    ClientID_client_Validated_LastClientShown = NewClient;

                    // Do the standard logic to display this client's alert notes.
                    if (Defaults.ShowAlertNotesOnEntry())
                    {
                        using (var Notes = new DebtPlus.Notes.AlertNotes.Client())
                        {
                            Notes.ShowAlerts(NewClient);
                        }
                    }
                }
            }
        }

        private void ClientID_client_Validating(object sender, CancelEventArgs e)
        {
            Int32 NewClient = ClientID_client.EditValue.GetValueOrDefault(-1);
            if (NewClient != ReadClient)
                ReadClientInfo(NewClient);
        }

        private void TextEditItemDate_Leave(object sender, EventArgs e)
        {
            // Redraw the information from the date entered
            DepositItemDate = DepositItemDate;
        }

        private void DepositItemBaseControl_Load(object sender, EventArgs e)
        {
            // Set the status for the various items
            ClientID_client.EnterMoveNextControl = Defaults.EnterMoveNextControl();
            CalcEdit_deposit_amount.EnterMoveNextControl = Defaults.EnterMoveNextControl();
            LookUpEdit_tran_subtype.EnterMoveNextControl = Defaults.EnterMoveNextControl();
            TextEdit_reference.EnterMoveNextControl = Defaults.EnterMoveNextControl();
            TextEditItemDate.EnterMoveNextControl = Defaults.EnterMoveNextControl();
        }

        public void ShowBlinkAlert()
        {
            BlinkLabel1.Enabled = true;
        }

        public void HideBlinkAlert()
        {
            BlinkLabel1.Enabled = false;
        }

        private void ShowClientThread(object obj)
        {
            Int32 clientID = Convert.ToInt32(obj);

            if (ValidClient(clientID))
            {
                // If the alert notes were not shown upon entering then display them now.
                if (Defaults.ShowAlertNotesOnEdit())
                {
                    using (var cls = new DebtPlus.Notes.AlertNotes.Client())
                    {
                        cls.ShowAlerts(clientID);
                    }
                }

                // Do the client edit operation
                using (ClientUpdateClass cuc = new ClientUpdateClass())
                {
                    cuc.ShowEditDialog(clientID, false);
                }
            }
        }

        private bool ValidClient(Int32 ClientID)
        {
            bool answer = false;
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with4 = cmd;
                    _with4.Connection = cn;
                    _with4.CommandText = "SELECT client from clients with (nolock) where client=@client";
                    _with4.CommandType = CommandType.Text;
                    _with4.Parameters.Add("@client", SqlDbType.Int).Value = ClientID;

                    object objAnswer = cmd.ExecuteScalar();
                    if (objAnswer != null && !object.ReferenceEquals(objAnswer, DBNull.Value))
                        answer = true;
                }
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }

            return answer;
        }
    }
}