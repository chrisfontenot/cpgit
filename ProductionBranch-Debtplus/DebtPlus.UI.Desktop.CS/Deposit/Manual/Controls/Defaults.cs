using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
    public static class Defaults
    {
        private static Int32 privateEnterMoveNextControl = 0;
        private static Int32 privateShowAlertNotesOnEntry = 0;
        private static Int32 privateShowAlertNotesOnEdit = 0;
        private static Int32 privateUseSameReference = 0;

        private static string privateDefaultSubType = "*";

        /// <summary>
        /// Show the keypad "Enter" key be used as a TAB key?
        /// </summary>
        public static bool EnterMoveNextControl()
        {
            if (privateEnterMoveNextControl == 0)
            {
                string strItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "EnterMoveNextControl");
                bool ItemValue = false;
                if (!bool.TryParse(strItem, out ItemValue))
                    ItemValue = true;
                privateEnterMoveNextControl = ItemValue ? 1 : 2;
            }

            return privateEnterMoveNextControl == 1;
        }

        /// <summary>
        /// Should alert notes be shown when the client is entered on the keypad?
        /// </summary>
        public static bool ShowAlertNotesOnEntry()
        {
            if (privateShowAlertNotesOnEntry == 0)
            {
                string strItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "ShowAlertNotesOnEntry");
                bool ItemValue = false;
                if (!bool.TryParse(strItem, out ItemValue))
                    ItemValue = true;
                privateShowAlertNotesOnEntry = ItemValue ? 1 : 2;
            }

            return privateShowAlertNotesOnEntry == 1;
        }

        /// <summary>
        /// Should the alert notes be shown when the client is edited?
        /// </summary>
        public static bool ShowAlertNotesOnEdit()
        {
            if (privateShowAlertNotesOnEdit == 0)
            {
                string strItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "ShowAlertNotesOnEdit");
                bool ItemValue = false;
                if (!bool.TryParse(strItem, out ItemValue))
                    ItemValue = false;
                privateShowAlertNotesOnEdit = ItemValue ? 1 : 2;
            }

            return privateShowAlertNotesOnEdit == 1;
        }

        /// <summary>
        /// Should the reference information be preserved from one client to the ntext?
        /// </summary>
        public static bool UseSameReference()
        {
            if (privateUseSameReference == 0)
            {
                string strItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "UseSameReference");
                bool ItemValue = false;
                if (!bool.TryParse(strItem, out ItemValue))
                    ItemValue = true;
                privateUseSameReference = ItemValue ? 1 : 2;
            }

            return privateUseSameReference == 1;
        }

        /// <summary>
        /// Should the reference information be preserved from one client to the ntext?
        /// </summary>
        public static string DefaultSubType()
        {
            if (privateDefaultSubType == "*")
            {
                string strItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "DefaultSubType");
                if (string.IsNullOrEmpty(strItem))
                {
                    strItem = "MO";
                }
                privateDefaultSubType = strItem;
            }

            return privateDefaultSubType;
        }
    }
}