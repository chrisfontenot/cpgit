using System;
using System.Data;
using System.Diagnostics;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.Threading;
using DebtPlus.UI.Desktop.CS.Deposit.Manual.Report;
using DevExpress.XtraPrinting.Drawing;
using System.Windows.Forms;
using System.Drawing;
using DevExpress.XtraReports.UI;

namespace DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls
{
    internal partial class ClientDepositControl
    {
        //Private ReadOnly ctx As DepositManualContext = Nothing

        private DataTable _tbl = null;

        public ClientDepositControl(Form_DepositManual p, DepositManualContext ctx) : base(p, ctx)
        {
            InitializeComponent();
            NewDepositItemControl1.Cancelled += NewDepositItemControl1_Cancelled;
            NewDepositItemControl1.Completed += NewDepositItemControl1_Completed;
            SimpleButton_ok.Click += SimpleButton_ok_Click;
            SimpleButton_ok.Enter += SimpleButton_ok_Enter;
            this.Load += ClientDepositControl_Load;
            NewDepositItemControl1.RecordAdded += NewDepositItemControl1_RecordAdded;
            SimpleButton_cancel.Click += SimpleButton_ok_Click;

            DepositItemControl1.GiveFocus();
            NewDepositItemControl1.TakeFocus();
            _tbl = ctx.ds.Tables["deposit_batch_details"];
        }

        public event EventHandler Cancelled;

        protected virtual void OnCancelled(EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, e);
            }
        }

        protected void RaiseCancelled()
        {
            OnCancelled(EventArgs.Empty);
        }

        public void ReadForm()
        {
            // Add a handler to the menu for printing the report
            var _with1 = depositManualForm;
            BarManager mgr = _with1.barManager1;
            BarButtonItem newItem = new BarButtonItem(mgr, "&Print...");
            var _with2 = newItem;
            _with2.Name = "PrintReport";
            _with2.ItemClick += PrintReport;
            _with1.BarSubItem_File.InsertItem(_with1.BarSubItem_File.ItemLinks[0], newItem);

            // Read the list of pending items for the deposit details
            if (_tbl == null)
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with3 = cmd;
                    _with3.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with3.CommandType = CommandType.Text;
                    _with3.CommandText = "SELECT [deposit_batch_detail],[deposit_batch_id],[tran_subtype],[ok_to_post],[scanned_client],[client],[creditor],[client_creditor],[amount],[fairshare_amt],[fairshare_pct],[creditor_type],[item_date],[reference],[message],[ach_transaction_code],[ach_routing_number],[ach_account_number],[ach_authentication_code],[ach_response_batch_id],[date_posted],[created_by],[date_created] FROM deposit_batch_details WHERE deposit_batch_id = @deposit_batch ORDER BY deposit_batch_detail";
                    _with3.Parameters.Add("@deposit_batch", SqlDbType.Int).Value = ctx.ap.Batch;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ctx.ds, "deposit_batch_details");
                        _tbl = ctx.ds.Tables["deposit_batch_details"];
                    }
                }
            }

            // Supply the necessary default values for the columns
            var _with4 = _tbl;
            _with4.PrimaryKey = new DataColumn[] { _with4.Columns["deposit_batch_detail"] };

            var _with5 = _with4.Columns["deposit_batch_detail"];
            object objAnswer = _tbl.Compute("max(deposit_batch_detail)", string.Empty);
            if (objAnswer == null || object.ReferenceEquals(objAnswer, DBNull.Value))
                objAnswer = 0;

            _with5.AutoIncrement = true;
            _with5.AutoIncrementSeed = Convert.ToInt32(objAnswer) + 1;
            _with5.AutoIncrementStep = 1;

            var _with6 = _with4.Columns["amount"];
            _with6.DefaultValue = 0m;

            var _with7 = _with4.Columns["client"];
            _with7.DefaultValue = DBNull.Value;

            var _with8 = _with4.Columns["item_date"];
            _with8.DefaultValue = DateTime.Now.Date;

            var _with9 = _with4.Columns["tran_subtype"];
            string DefaultItem = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "DefaultSubtype");
            if (string.IsNullOrEmpty(DefaultItem) || DefaultItem.Trim() == string.Empty)
                DefaultItem = "MO";
            _with9.DefaultValue = DefaultItem.Trim();

            // Bind the table to the various controls
            BatchListControl1.LoadList(_tbl);
            BatchStatusControl1.LoadList(_tbl);

            // Add a new row to the view
            NewDepositItemControl1.ReadRecord();
        }

        private void NewDepositItemControl1_Cancelled(object sender, EventArgs e)
        {
            RaiseCancelled();
        }

        private void NewDepositItemControl1_Completed(object sender, EventArgs e)
        {
            // Update the database with the changed items
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlTransaction txn = null;
            try
            {
                cn.Open();
                txn = cn.BeginTransaction();

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    da.UpdateCommand = UpdateCommand(ref cn, ref txn);
                    da.InsertCommand = InsertCommand(ref cn, ref txn);
                    da.DeleteCommand = DeleteCommand(ref cn, ref txn);

                    // Perform the pending updates on the database
                    UseWaitCursor = true;
                    da.Update(_tbl);
                    _tbl.AcceptChanges();
                }

                // Commit the changes
                txn.Commit();
                txn = null;

                // Close the connection while the dialog is pending
                cn.Close();
                UseWaitCursor = false;

                // Close the batch if desired....
                if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("Do you wish to close the current batch as well?{0}{0}Closing the batch will prevent further updates to the deposits and permit them{0}to be posted to the clients account.", Environment.NewLine), "Update completed", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    UseWaitCursor = true;
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with10 = cmd;
                        _with10.Connection = cn;
                        _with10.CommandText = "xpr_deposit_batch_close";
                        _with10.CommandType = CommandType.StoredProcedure;
                        _with10.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch;
                        _with10.ExecuteNonQuery();
                    }
                }

                // Tell the application to terminate. At this point, we just want cancel since that is all that is being monitored.
                RaiseCancelled();
            }
#pragma warning disable 168
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show("The transaction(s) are no longer valid for this batch. The most common cause is that the batch has been posted and edits are no longer permitted.", "Database Consistency Check Violation", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            catch (Exception ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the deposit batch details");
            }
#pragma warning restore 168
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        DebtPlus.Svc.Common.ErrorHandling.HandleErrors(exRollback);
                    }

                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();

                UseWaitCursor = false;
            }
        }

        private SqlCommand UpdateCommand(ref SqlConnection cn, ref SqlTransaction txn)
        {
            SqlCommand cmd = new SqlCommand();
            var _with11 = cmd;
            _with11.Connection = cn;
            _with11.Transaction = txn;
            _with11.CommandText = "UPDATE deposit_batch_details SET client=@client, amount=@amount, tran_subtype=@tran_subtype, reference=@reference, item_date=@item_date WHERE deposit_batch_detail=@deposit_batch_detail";
            _with11.CommandType = CommandType.Text;
            _with11.Parameters.Add("@client", SqlDbType.Int, 0, "client");
            _with11.Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount");
            _with11.Parameters.Add("@tran_subtype", SqlDbType.VarChar, 2, "tran_subtype");
            _with11.Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference");
            _with11.Parameters.Add("@item_date", SqlDbType.DateTime, 0, "item_date");
            _with11.Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail");
            return cmd;
        }

        private SqlCommand InsertCommand(ref SqlConnection cn, ref SqlTransaction txn)
        {
            SqlCommand cmd = new SqlCommand();
            var _with12 = cmd;
            _with12.Connection = cn;
            _with12.Transaction = txn;
            _with12.CommandText = "INSERT INTO deposit_batch_details(client,amount,tran_subtype,reference,item_date,ok_to_post,deposit_batch_id) values (@client,@amount,@tran_subtype,@reference,@item_date,1,@deposit_batch_id)";
            _with12.CommandType = CommandType.Text;
            _with12.Parameters.Add("@client", SqlDbType.Int, 0, "client");
            _with12.Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount");
            _with12.Parameters.Add("@tran_subtype", SqlDbType.VarChar, 2, "tran_subtype");
            _with12.Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference");
            _with12.Parameters.Add("@item_date", SqlDbType.DateTime, 0, "item_date");
            _with12.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch;
            return cmd;
        }

        private SqlCommand DeleteCommand(ref SqlConnection cn, ref SqlTransaction txn)
        {
            SqlCommand cmd = new SqlCommand();
            var _with13 = cmd;
            _with13.Connection = cn;
            _with13.Transaction = txn;
            _with13.CommandText = "DELETE FROM deposit_batch_details WHERE deposit_batch_detail = @deposit_batch_detail AND deposit_batch_id=@deposit_batch_id";
            _with13.CommandType = CommandType.Text;
            _with13.Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail");
            _with13.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch;
            return cmd;
        }

        private void SimpleButton_ok_Click(object sender, EventArgs e)
        {
            var _with14 = (SimpleButton)sender;
            switch (Convert.ToString(_with14.Tag))
            {
                case "new cancel":
                    NewDepositItemControl1.CancelEvent();
                    break;

                case "new ok":
                    NewDepositItemControl1.AcceptEvent();
                    break;

                case "cancel":
                    DepositItemControl1.CancelEvent();
                    break;

                case "ok":
                    DepositItemControl1.AcceptEvent();
                    break;

                default:
                    Debug.Assert(false, "Tag is not valid");
                    break;
            }
        }

        private void PrintReport(object sender, ItemClickEventArgs e)
        {
            var _with15 = new Thread(PrintReport_Thread);
            _with15.SetApartmentState(ApartmentState.STA);
            _with15.Name = "PrintReport_Thread";
            _with15.IsBackground = true;
            _with15.Start();
        }

        private void PrintReport_Thread()
        {
            var _with16 = new DepositBatchReportClass(depositManualForm, ctx);

            if (_with16.RequestReportParameters() == DialogResult.OK)
            {
                if ((depositManualForm).PendingTransactions())
                {
                    var _with17 = _with16.Watermark;
                    _with17.Font = new Font("Arial", 60, FontStyle.Bold, GraphicsUnit.Point, 0);
                    _with17.ImageViewMode = ImageViewMode.Stretch;
                    _with17.ShowBehind = true;
                    _with17.Text = string.Format("DATABASE IS{0}NOT UPDATED", Environment.NewLine);
                    _with17.TextDirection = DirectionMode.BackwardDiagonal;
                    _with17.TextTransparency = 180;
                    _with17.ForeColor = Color.DarkGreen;
                }

                _with16.DisplayPreviewDialog();
            }
            _with16.Dispose();
        }

        private void SimpleButton_ok_Enter(object sender, EventArgs e)
        {
            var _with18 = (SimpleButton)sender;
            if ("new ok".CompareTo(_with18.Tag) == 0)
            {
                if (SimpleButton_ok.Enabled)
                {
                    SimpleButton_ok.PerformClick();
                }
            }
        }

        private void ClientDepositControl_Load(object sender, EventArgs e)
        {
            SimpleButton_cancel.TabStop = !Defaults.EnterMoveNextControl();
        }

        private void NewDepositItemControl1_RecordAdded(object Sender, DataRow record)
        {
            BatchListControl1.ScrollToRecord(record);
        }
    }
}