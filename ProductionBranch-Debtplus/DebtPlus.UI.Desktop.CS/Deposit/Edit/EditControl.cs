using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.Edit
{
    internal partial class EditControl : MyControls
    {
        public EditControl() : base()
        {
            InitializeComponent();
            BarButtonItem_PopupEdit.ItemClick += BarButtonItem1_ItemClick;
            SimpleButton_OK.Click += SimpleButton_OK_Click;
            SimpleButton_Cancel.Click += SimpleButton_Cancel_Click;
            GridView1.MouseUp += GridView1_MouseUp;
            PopupMenu_Items.Popup += PopupMenu_Items_Popup;
            GridView1.Click += GridView1_Click;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            SimpleButton_Apply.Click += SimpleButton_Apply_Click;
            BarButtonItem_PopupDelete.ItemClick += BarButtonItem_PopupDelete_ItemClick;
        }

        /// <summary>
        /// Event tripped when the CANCEL button is pressed
        /// </summary>
        /// <remarks></remarks>
        public event EventHandler Cancelled;

        /// <summary>
        /// Generate the cancel event
        /// </summary>
        /// <remarks></remarks>
        protected void RaiseCancelled()
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Retrieve the current batch number from the parent
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        protected Int32 Batch
        {
            get { return ((Form_DepositEdit)ParentForm).ap.Batch; }
        }

        private DataSet ds = new DataSet("ds");

        /// <summary>
        /// Read the form when we are first loaded
        /// </summary>
        /// <remarks></remarks>
        public void ReadForm()
        {
            ds.Clear();

            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with1 = cmd;
                _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with1.CommandText = "rpt_deposit_detail_ByBatch";
                _with1.CommandType = CommandType.StoredProcedure;
                _with1.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = Batch;

                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                {
                    da.Fill(ds, "deposit_details");
                }
            }

            DataTable tbl = ds.Tables["deposit_details"];
            var _with2 = tbl;
            _with2.PrimaryKey = new DataColumn[] { _with2.Columns["deposit_batch_detail"] };
            var _with3 = _with2.Columns["deposit_batch_detail"];
            _with3.AutoIncrement = true;
            _with3.AutoIncrementSeed = -1;
            _with3.AutoIncrementStep = -1;

            if (!_with2.Columns.Contains("formatted_ok_to_post"))
            {
                _with2.Columns.Add("formatted_ok_to_post", typeof(string), "iif([ok_to_post]<>0,'Y','N')");
            }

            GridControl1.DataSource = tbl.DefaultView;
            GridControl1.RefreshDataSource();
            GridView1.BestFitColumns();

            var _with4 = (Form_DepositEdit)ParentForm;
            _with4.BarButtonItem_File_Print.Enabled = true;
        }

        /// <summary>
        /// When the cancel buttin is clicked, tell the owner.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void SimpleButton_Cancel_Click(object sender, EventArgs e)
        {
            if (SaveForm())
                RaiseCancelled();
        }

        /// <summary>
        /// Handle the MOUSE-UP event on the grid to show the popup menus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void GridView1_MouseUp(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));

            if (hi.InRow)
            {
                Int32 RowHandle = hi.RowHandle;
                GridView1.FocusedRowHandle = RowHandle;
                SimpleButton_OK.Enabled = GridView1.FocusedRowHandle >= 0;
            }
            else
            {
                GridView1.FocusedRowHandle = -1;
                SimpleButton_OK.Enabled = false;
            }

            if (e.Button == MouseButtons.Right)
            {
                PopupMenu_Items.ShowPopup(Control.MousePosition);
            }
        }

        /// <summary>
        /// When the popup menu is being shown, enable or dsiable the items in the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected virtual void PopupMenu_Items_Popup(object sender, EventArgs e)
        {
            BarButtonItem_PopupDelete.Enabled = SimpleButton_OK.Enabled;
            BarButtonItem_PopupEdit.Enabled = SimpleButton_OK.Enabled;
        }

        /// <summary>
        /// Handle the click event on the grid to enable the OK button as needed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void GridView1_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));

            if (hi.InRow)
            {
                Int32 RowHandle = hi.RowHandle;
                GridView1.FocusedRowHandle = RowHandle;
                SimpleButton_OK.Enabled = GridView1.FocusedRowHandle >= 0;
            }
            else
            {
                GridView1.FocusedRowHandle = -1;
                SimpleButton_OK.Enabled = false;
            }
        }

        /// <summary>
        /// Process the edit operation when the row is double-clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            GridView1_Click(sender, e);
            if (SimpleButton_OK.Enabled)
                SimpleButton_OK.PerformClick();
        }

        /// <summary>
        /// Process the edit operaiton when the OK button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            Int32 SelectedRow = GridView1.FocusedRowHandle;
            if (SelectedRow >= 0)
                EditRowHandle(SelectedRow);
        }

        /// <summary>
        /// Edit the row given the handle to the row in the list
        /// </summary>
        /// <param name="RowHandle">Row handle to the list (0 relative)</param>
        /// <remarks></remarks>
        private void EditRowHandle(Int32 RowHandle)
        {
            if (RowHandle >= 0)
            {
                DataRowView drv = (DataRowView)GridView1.GetRow(RowHandle);
                if (drv != null)
                    EditRow(drv);
            }
        }

        /// <summary>
        /// Do an edit operation on the database row
        /// </summary>
        /// <param name="drv">Pointer to the row to be edited</param>
        /// <remarks></remarks>
        private void EditRow(DataRowView drv)
        {
            drv.BeginEdit();
            var _with5 = new EditForm(drv);
            if (_with5.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    drv.EndEdit();
                    SimpleButton_Apply.Enabled = true;
                }
                catch (DataException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Transaction Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    drv.CancelEdit();
                }
            }
            else
            {
                drv.CancelEdit();
            }
            _with5.Dispose();
        }

        /// <summary>
        /// When the focused row changes, enable or disable the OK button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SimpleButton_OK.Enabled = (GridView1.FocusedRowHandle >= 0);
        }

        /// <summary>
        /// Do the APPLY function to apply the changes to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void SimpleButton_Apply_Click(object sender, EventArgs e)
        {
            UpdateDatabase();
            SimpleButton_Apply.Enabled = false;
        }

        /// <summary>
        /// Perform the physical update on the database pending items
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool UpdateDatabase()
        {
            bool answer = false;
            DataTable tbl = ((DataView)GridControl1.DataSource).Table;
            System.Data.SqlClient.SqlCommand UpdateCmd = new SqlCommand();
            System.Data.SqlClient.SqlCommand DeleteCmd = new SqlCommand();

            try
            {
                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                {
                    var _with6 = UpdateCmd;
                    _with6.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with6.CommandText = "UPDATE deposit_batch_details SET client=@deposit_client, amount=@amount, reference=@reference, item_date=@item_date, ok_to_post=@ok_to_post, tran_subtype=@subtype WHERE deposit_batch_detail=@deposit_batch_detail";
                    _with6.CommandType = CommandType.Text;
                    _with6.Parameters.Add("@deposit_client", SqlDbType.Int, 0, "deposit_client");
                    _with6.Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount");
                    _with6.Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference");
                    _with6.Parameters.Add("@item_date", SqlDbType.DateTime, 0, "item_date");
                    _with6.Parameters.Add("@ok_to_post", SqlDbType.Bit, 0, "ok_to_post");
                    _with6.Parameters.Add("@subtype", SqlDbType.VarChar, 2, "subtype");
                    _with6.Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail");
                    da.UpdateCommand = UpdateCmd;

                    var _with7 = DeleteCmd;
                    _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with7.CommandText = "DELETE FROM deposit_batch_details WHERE [deposit_batch_detail]=@deposit_batch_detail";
                    _with7.CommandType = CommandType.Text;
                    _with7.Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail");
                    da.DeleteCommand = DeleteCmd;

                    // Do the update operation now
                    da.Update(tbl);
                }

                answer = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the database");
            }
            finally
            {
                if (UpdateCmd != null)
                    UpdateCmd.Dispose();
                if (DeleteCmd != null)
                    DeleteCmd.Dispose();
            }

            return answer;
        }

        /// <summary>
        /// Save the form when the program terminates.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool SaveForm()
        {
            bool answer = true;

            if (HasChanges())
            {
                switch (DebtPlus.Data.Forms.MessageBox.Show(string.Format("You have pending changes to the database.{0}{0}Do you wish to post these updates before you close the form?", Environment.NewLine), "Pending Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Yes:
                        if (!UpdateDatabase())
                            answer = false;
                        break;

                    case DialogResult.No:
                        break;
                    // Just ignore the updates

                    case DialogResult.Cancel:
                        answer = false;
                        break;
                }
            }

            return answer;
        }

        /// <summary>
        /// Determine if there are pending changes to the database. They may be updates or deletes.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private bool HasChanges()
        {
            using (DataView vue = new DataView(((DataView)GridControl1.DataSource).Table, string.Empty, string.Empty, DataViewRowState.Deleted))
            {
                if (vue.Count > 0)
                    return true;
            }

            using (DataView vue = new DataView(((DataView)GridControl1.DataSource).Table, string.Empty, string.Empty, DataViewRowState.ModifiedCurrent))
            {
                if (vue.Count > 0)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Do the edit operation on the row if you rightclick and choose edit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void BarButtonItem1_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Int32 SelectedRow = GridView1.FocusedRowHandle;
            if (SelectedRow >= 0)
                EditRowHandle(SelectedRow);
        }

        /// <summary>
        /// Do the delete operation on the row if you rightclick on the row and choose delete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void BarButtonItem_PopupDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Int32 SelectedRow = GridView1.FocusedRowHandle;
            DataRowView drv = (DataRowView)GridView1.GetRow(SelectedRow);
            if (drv != null)
            {
                if (DebtPlus.Data.Forms.MessageBox.Show("Are you sure that you wish to delete this item?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    drv.Delete();
                }
            }
        }
    }
}