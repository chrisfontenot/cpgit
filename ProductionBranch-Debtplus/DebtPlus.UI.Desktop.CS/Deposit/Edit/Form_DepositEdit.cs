using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using DebtPlus.Data.Forms;
using DevExpress.XtraBars;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.Edit
{
    public partial class Form_DepositEdit : DebtPlusForm
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        internal DepositEditArgParser ap;

        public Form_DepositEdit(DepositEditArgParser ap) : this()
        {
            this.ap = ap;
        }

        public Form_DepositEdit() : base()
        {
            InitializeComponent();
            this.FormClosing += EditForm_FormClosing;
            this.Load += Form_DepositEdit_Load;
            SelectControl1.Cancelled += SelectControl1_Cancelled;
            SelectControl1.Selected += SelectControl1_Selected;
            EditControl1.Cancelled += EditControl1_Cancelled;
            BarButtonItem_File_Exit.ItemClick += BarButtonItem_File_Exit_ItemClick;
            BarButtonItem_File_Print.ItemClick += BarButtonItem_File_Print_ItemClick;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        internal global::DebtPlus.UI.Desktop.CS.Deposit.Edit.EditControl EditControl1;
        protected internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar BarMainMenu;
        protected internal DevExpress.XtraBars.BarSubItem BarSubItem_File;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Print;
        protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;

        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        internal global::DebtPlus.UI.Desktop.CS.Deposit.Edit.SelectControl SelectControl1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SelectControl1 = new global::DebtPlus.UI.Desktop.CS.Deposit.Edit.SelectControl();
            this.EditControl1 = new global::DebtPlus.UI.Desktop.CS.Deposit.Edit.EditControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.BarMainMenu = new DevExpress.XtraBars.Bar();
            this.BarSubItem_File = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Print = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            this.SuspendLayout();
            //
            //SelectControl1
            //
            this.SelectControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SelectControl1.Location = new System.Drawing.Point(0, 25);
            this.SelectControl1.Name = "SelectControl1";
            this.SelectControl1.Padding = new System.Windows.Forms.Padding(4);
            this.SelectControl1.Size = new System.Drawing.Size(560, 269);
            this.SelectControl1.TabIndex = 0;
            //
            //EditControl1
            //
            this.EditControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditControl1.Location = new System.Drawing.Point(0, 25);
            this.EditControl1.Name = "EditControl1";
            this.EditControl1.Size = new System.Drawing.Size(560, 269);
            this.EditControl1.TabIndex = 1;
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.BarMainMenu });
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
                this.BarSubItem_File,
                this.BarButtonItem_File_Print,
                this.BarButtonItem_File_Exit
            });
            this.barManager1.MainMenu = this.BarMainMenu;
            this.barManager1.MaxItemId = 3;
            //
            //BarMainMenu
            //
            this.BarMainMenu.BarName = "Main menu";
            this.BarMainMenu.DockCol = 0;
            this.BarMainMenu.DockRow = 0;
            this.BarMainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.BarMainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem_File) });
            this.BarMainMenu.OptionsBar.MultiLine = true;
            this.BarMainMenu.OptionsBar.UseWholeRow = true;
            this.BarMainMenu.Text = "Main menu";
            //
            //BarSubItem_File
            //
            this.BarSubItem_File.Caption = "&File";
            this.BarSubItem_File.Id = 0;
            this.BarSubItem_File.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Print),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit)
            });
            this.BarSubItem_File.Name = "BarSubItem_File";
            //
            //BarButtonItem_File_Print
            //
            this.BarButtonItem_File_Print.Caption = "&Print...";
            this.BarButtonItem_File_Print.Id = 1;
            this.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print";
            //
            //BarButtonItem_File_Exit
            //
            this.BarButtonItem_File_Exit.Caption = "&Exit";
            this.BarButtonItem_File_Exit.Id = 2;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            //
            //barDockControlTop
            //
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(560, 25);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 294);
            this.barDockControlBottom.Size = new System.Drawing.Size(560, 0);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 25);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 269);
            //
            //barDockControlRight
            //
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(560, 25);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 269);
            //
            //Form_DepositEdit
            //
            this.ClientSize = new System.Drawing.Size(560, 294);
            this.Controls.Add(this.SelectControl1);
            this.Controls.Add(this.EditControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Form_DepositEdit";
            this.Text = "Edit Deposit Batch";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private MyControls ActivePage;

        private void EditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ActivePage != null)
            {
                if (!ActivePage.SaveForm())
                    e.Cancel = true;
            }
        }

        private void Form_DepositEdit_Load(object sender, EventArgs e)
        {
            if (ap.Batch <= 0)
            {
                var _with1 = EditControl1;
                _with1.Visible = false;
                _with1.TabStop = false;
                _with1.SendToBack();

                var _with2 = SelectControl1;
                _with2.Visible = true;
                _with2.TabStop = true;
                _with2.BringToFront();
                ActivePage = SelectControl1;
                ActivePage.ReadForm();
            }
            else
            {
                ProcessBatch();
            }
        }

        private void SelectControl1_Cancelled(object sender, EventArgs e)
        {
            Close();
        }

        private void ProcessBatch()
        {
            var _with3 = SelectControl1;
            _with3.Visible = false;
            _with3.TabStop = false;
            _with3.SendToBack();

            var _with4 = EditControl1;
            ActivePage = EditControl1;
            ActivePage.ReadForm();
            _with4.Visible = true;
            _with4.TabStop = true;
            _with4.BringToFront();
            _with4.ReadForm();
        }

        private void SelectControl1_Selected(object sender, EventArgs e)
        {
            ap.Batch = SelectControl1.DepositBatchID;
            ProcessBatch();
        }

        private void EditControl1_Cancelled(object sender, EventArgs e)
        {
            var _with5 = EditControl1;
            _with5.Visible = false;
            _with5.TabStop = false;
            _with5.SendToBack();

            var _with6 = SelectControl1;
            _with6.Visible = true;
            _with6.TabStop = true;
            _with6.BringToFront();
            ActivePage = SelectControl1;
            ActivePage.ReadForm();
        }

        private void BarButtonItem_File_Exit_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        private BackgroundWorker btReportThread = new BackgroundWorker();

        private void BarButtonItem_File_Print_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!btReportThread.IsBusy)
            {
                btReportThread.RunWorkerAsync(ap.Batch);
            }
        }

        //AddHandler btReportThread.DoWork, AddressOf btReportThread_DoWork
        //Private Sub btReportThread_DoWork (ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
        //Dim Batch As Int32 = Convert.ToInt32(e.Argument)
        //Dim rpt As New DebtPlus.Reports.Deposits.Batch.DepositBatchReport()
        //    rpt.Parameter_DepositBatchID = Batch
        //    If rpt.RequestReportParameters = System.Windows.Forms.DialogResult.OK Then
        //        rpt.DisplayPreviewDialog()
        //    End If
        //End Sub
    }

    internal interface MyControls
    {
        void ReadForm();

        bool SaveForm();
    }
}