using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.Deposit.Edit
{
    internal partial class EditForm
    {
        private readonly DataRowView drv;

        public EditForm() : this(null)
        {
        }

        public EditForm(DataRowView drv) : base()
        {
            InitializeComponent();
            this.Load += EditForm_Load;
            SimpleButton_OK.Click += SimpleButton_OK_Click;
            this.drv = drv;
        }

        private void EditForm_Load(object sender, EventArgs e)
        {
            DataTable tbl = Globals.dsTranTypes.Tables["tran_types"];

            if (tbl == null)
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with1.CommandText = "SELECT tran_type, description FROM tran_types WHERE deposit_subtype = 1 ORDER BY sort_order, description";
                    _with1.CommandType = CommandType.Text;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(Globals.dsTranTypes, "tran_types");
                        tbl = Globals.dsTranTypes.Tables["tran_types"];
                    }
                }
            }

            LookUpEdit_subtype.Properties.DataSource = tbl.DefaultView;

            // Bind the items to the display fields
            string ScannedClient = null;
            if (drv["scanned_client"] != null && !object.ReferenceEquals(drv["scanned_client"], DBNull.Value))
            {
                ScannedClient = Convert.ToString(drv["scanned_client"]);
            }
            else
            {
                ScannedClient = string.Empty;
            }
            LabelControl_scanned_client.Text = ScannedClient;

            LookUpEdit_subtype.EditValue = drv["subtype"];
            TextEdit_reference.EditValue = drv["reference"];
            CalcEdit_amount.EditValue = global::DebtPlus.Utils.Nulls.v_Decimal(drv["amount"]);
            DateEdit_item_date.EditValue = global::DebtPlus.Utils.Nulls.v_DateTime(drv["item_date"]);
            ClientID_client.EditValue = global::DebtPlus.Utils.Nulls.v_Int32(drv["deposit_client"]);
            CheckEdit_ok_to_post.Checked = Convert.ToBoolean(drv["ok_to_post"]);

            LookUpEdit_subtype.EditValueChanged += FormChanged;
            CalcEdit_amount.EditValueChanged += FormChanged;
            DateEdit_item_date.EditValueChanged += FormChanged;
            TextEdit_reference.EditValueChanged += FormChanged;
            ClientID_client.EditValueChanged += FormChanged;
            CheckEdit_ok_to_post.EditValueChanged += FormChanged;
        }

        private void FormChanged(object Sender, EventArgs e)
        {
            SimpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return false;
        }

        private void SimpleButton_OK_Click(object sender, System.EventArgs e)
        {
            drv["subtype"] = LookUpEdit_subtype.EditValue;
            drv["reference"] = TextEdit_reference.EditValue;
            drv["ok_to_post"] = CheckEdit_ok_to_post.Checked;

            drv["amount"] = global::DebtPlus.Utils.Nulls.ToDbNull(CalcEdit_amount.EditValue);
            drv["item_date"] = global::DebtPlus.Utils.Nulls.ToDbNull(DateEdit_item_date.EditValue);
            drv["deposit_client"] = global::DebtPlus.Utils.Nulls.ToDbNull(ClientID_client.EditValue);
        }
    }

    static internal partial class Globals
    {
        static internal DataSet dsTranTypes = new DataSet("dsTranTypes");
    }
}