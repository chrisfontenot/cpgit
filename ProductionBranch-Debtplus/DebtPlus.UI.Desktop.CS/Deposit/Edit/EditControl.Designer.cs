using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Deposit.Edit
{
	partial class EditControl : DevExpress.XtraEditors.XtraUserControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_type = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_scanned_client = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_deposit_client = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_deposit_amount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_item_date = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_reference = new DevExpress.XtraGrid.Columns.GridColumn();
			this.SimpleButton_Apply = new DevExpress.XtraEditors.SimpleButton();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.BarButtonItem_PopupEdit = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_PopupDelete = new DevExpress.XtraBars.BarButtonItem();
			this.PopupMenu_Items = new DevExpress.XtraBars.PopupMenu(this.components);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_OK.Location = new System.Drawing.Point(352, 13);
			this.SimpleButton_OK.Name = "SimpleButton_OK";
			this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.TabIndex = 0;
			this.SimpleButton_OK.Text = "&Select";
			//
			//SimpleButton_Cancel
			//
			this.SimpleButton_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_Cancel.Location = new System.Drawing.Point(352, 91);
			this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
			this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cancel.TabIndex = 1;
			this.SimpleButton_Cancel.Text = "&Cancel";
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.Location = new System.Drawing.Point(4, 4);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.Size = new System.Drawing.Size(342, 143);
			this.GridControl1.TabIndex = 2;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_type,
				this.GridColumn2,
				this.GridColumn_scanned_client,
				this.GridColumn_deposit_client,
				this.GridColumn_deposit_amount,
				this.GridColumn_item_date,
				this.GridColumn_reference
			});
			this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
			//
			//GridColumn_type
			//
			this.GridColumn_type.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_type.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_type.Caption = "TYP";
			this.GridColumn_type.FieldName = "subtype";
			this.GridColumn_type.Name = "GridColumn_type";
			this.GridColumn_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_type.Visible = true;
			this.GridColumn_type.VisibleIndex = 0;
			//
			//GridColumn_subject
			//
			this.GridColumn2.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn2.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn2.Caption = "V";
			this.GridColumn2.FieldName = "formatted_ok_to_post";
			this.GridColumn2.Name = "GridColumn_subject";
			this.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn2.Visible = true;
			this.GridColumn2.VisibleIndex = 1;
			//
			//GridColumn_scanned_client
			//
			this.GridColumn_scanned_client.Caption = "Scanned";
			this.GridColumn_scanned_client.FieldName = "scanned_client";
			this.GridColumn_scanned_client.Name = "GridColumn_scanned_client";
			this.GridColumn_scanned_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_scanned_client.Visible = true;
			this.GridColumn_scanned_client.VisibleIndex = 2;
			//
			//GridColumn_deposit_client
			//
			this.GridColumn_deposit_client.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_deposit_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_deposit_client.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_deposit_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_deposit_client.Caption = "Client";
			this.GridColumn_deposit_client.FieldName = "deposit_client";
			this.GridColumn_deposit_client.Name = "GridColumn_deposit_client";
			this.GridColumn_deposit_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_deposit_client.Visible = true;
			this.GridColumn_deposit_client.VisibleIndex = 3;
			//
			//GridColumn_deposit_amount
			//
			this.GridColumn_deposit_amount.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_deposit_amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_deposit_amount.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_deposit_amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_deposit_amount.Caption = "Amount";
			this.GridColumn_deposit_amount.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_deposit_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_deposit_amount.FieldName = "amount";
			this.GridColumn_deposit_amount.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_deposit_amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_deposit_amount.Name = "GridColumn_deposit_amount";
			this.GridColumn_deposit_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_deposit_amount.Visible = true;
			this.GridColumn_deposit_amount.VisibleIndex = 4;
			//
			//GridColumn_item_date
			//
			this.GridColumn_item_date.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_item_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_item_date.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_item_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_item_date.Caption = "Date";
			this.GridColumn_item_date.DisplayFormat.FormatString = "d";
			this.GridColumn_item_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_item_date.FieldName = "item_date";
			this.GridColumn_item_date.GroupFormat.FormatString = "d";
			this.GridColumn_item_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_item_date.Name = "GridColumn_item_date";
			this.GridColumn_item_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_item_date.Visible = true;
			this.GridColumn_item_date.VisibleIndex = 5;
			//
			//GridColumn_reference
			//
			this.GridColumn_reference.Caption = "Reference";
			this.GridColumn_reference.FieldName = "reference";
			this.GridColumn_reference.Name = "GridColumn_reference";
			this.GridColumn_reference.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_reference.Visible = true;
			this.GridColumn_reference.VisibleIndex = 6;
			//
			//SimpleButton_Apply
			//
			this.SimpleButton_Apply.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_Apply.Location = new System.Drawing.Point(352, 42);
			this.SimpleButton_Apply.Name = "SimpleButton_Apply";
			this.SimpleButton_Apply.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Apply.TabIndex = 3;
			this.SimpleButton_Apply.Text = "&Apply";
			//
			//barManager1
			//
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarButtonItem_PopupEdit,
				this.BarButtonItem_PopupDelete
			});
			this.barManager1.MaxItemId = 13;
			//
			//barDockControlTop
			//
			this.barDockControlTop.CausesValidation = false;
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(439, 0);
			//
			//barDockControlBottom
			//
			this.barDockControlBottom.CausesValidation = false;
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 150);
			this.barDockControlBottom.Size = new System.Drawing.Size(439, 0);
			//
			//barDockControlLeft
			//
			this.barDockControlLeft.CausesValidation = false;
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 150);
			//
			//barDockControlRight
			//
			this.barDockControlRight.CausesValidation = false;
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(439, 0);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 150);
			//
			//BarButtonItem_PopupEdit
			//
			this.BarButtonItem_PopupEdit.Caption = "&Edit...";
			this.BarButtonItem_PopupEdit.Id = 11;
			this.BarButtonItem_PopupEdit.Name = "BarButtonItem_PopupEdit";
			this.BarButtonItem_PopupEdit.Description = "Change the item in the list";
			//
			//BarButtonItem_PopupDelete
			//
			this.BarButtonItem_PopupDelete.Description = "Remove the item from the list";
			this.BarButtonItem_PopupDelete.Caption = "&Delete...";
			this.BarButtonItem_PopupDelete.Id = 12;
			this.BarButtonItem_PopupDelete.Name = "BarButtonItem_PopupDelete";
			//
			//PopupMenu_Items
			//
			this.PopupMenu_Items.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_PopupEdit),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_PopupDelete)
			});
			this.PopupMenu_Items.Manager = this.barManager1;
			this.PopupMenu_Items.MenuCaption = "Right Click Menus";
			this.PopupMenu_Items.Name = "PopupMenu_Items";
			//
			//EditControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.SimpleButton_Apply);
			this.Controls.Add(this.GridControl1);
			this.Controls.Add(this.SimpleButton_Cancel);
			this.Controls.Add(this.SimpleButton_OK);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "EditControl";
			this.Size = new System.Drawing.Size(439, 150);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.PopupMenu_Items).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_type;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn2;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_deposit_amount;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_scanned_client;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_deposit_client;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_item_date;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_reference;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Apply;
		protected internal DevExpress.XtraBars.BarManager barManager1;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlTop;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlRight;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_PopupEdit;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_PopupDelete;
		protected DevExpress.XtraBars.PopupMenu PopupMenu_Items;
	}
}
