using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Client.Widgets.Controls;

namespace DebtPlus.UI.Desktop.CS.Deposit.Edit
{
	partial class EditForm : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditForm));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.LabelControl_scanned_client = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.CheckEdit_ok_to_post = new DevExpress.XtraEditors.CheckEdit();
			this.LookUpEdit_subtype = new DevExpress.XtraEditors.LookUpEdit();
			this.DateEdit_item_date = new DevExpress.XtraEditors.DateEdit();
			this.CalcEdit_amount = new DevExpress.XtraEditors.CalcEdit();
			this.TextEdit_reference = new DevExpress.XtraEditors.TextEdit();
			this.ClientID_client = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ok_to_post.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_subtype.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_item_date.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_item_date.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_amount.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_reference.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID_client.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 13);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(89, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Scanned Client ID:";
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.SimpleButton_OK.Enabled = false;
			this.SimpleButton_OK.Location = new System.Drawing.Point(57, 184);
			this.SimpleButton_OK.Name = "SimpleButton_OK";
			this.SimpleButton_OK.Size = new System.Drawing.Size(75, 24);
			this.SimpleButton_OK.TabIndex = 13;
			this.SimpleButton_OK.Text = "&OK";
			//
			//SimpleButton_Cancel
			//
			this.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_Cancel.Location = new System.Drawing.Point(157, 184);
			this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
			this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 24);
			this.SimpleButton_Cancel.TabIndex = 14;
			this.SimpleButton_Cancel.Text = "&Cancel";
			//
			//LabelControl_scanned_client
			//
			this.LabelControl_scanned_client.Location = new System.Drawing.Point(108, 13);
			this.LabelControl_scanned_client.Name = "LabelControl_scanned_client";
			this.LabelControl_scanned_client.Size = new System.Drawing.Size(0, 13);
			this.LabelControl_scanned_client.TabIndex = 1;
			this.LabelControl_scanned_client.UseMnemonic = false;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(12, 44);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(63, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Deposit Type";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 71);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(27, 13);
			this.LabelControl3.TabIndex = 4;
			this.LabelControl3.Text = "Client";
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(12, 96);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(23, 13);
			this.LabelControl4.TabIndex = 6;
			this.LabelControl4.Text = "Date";
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(12, 123);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(37, 13);
			this.LabelControl5.TabIndex = 8;
			this.LabelControl5.Text = "Amount";
			//
			//LabelControl6
			//
			this.LabelControl6.Location = new System.Drawing.Point(12, 150);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(50, 13);
			this.LabelControl6.TabIndex = 10;
			this.LabelControl6.Text = "Reference";
			//
			//CheckEdit_ok_to_post
			//
			this.CheckEdit_ok_to_post.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.CheckEdit_ok_to_post.Location = new System.Drawing.Point(12, 221);
			this.CheckEdit_ok_to_post.Name = "CheckEdit_ok_to_post";
			this.CheckEdit_ok_to_post.Properties.Caption = "Transaction Valid";
			this.CheckEdit_ok_to_post.Size = new System.Drawing.Size(268, 19);
			this.CheckEdit_ok_to_post.TabIndex = 12;
			//
			//LookUpEdit_subtype
			//
			this.LookUpEdit_subtype.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LookUpEdit_subtype.Location = new System.Drawing.Point(108, 41);
			this.LookUpEdit_subtype.Name = "LookUpEdit_subtype";
			this.LookUpEdit_subtype.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_subtype.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("tran_type", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_subtype.Properties.DisplayMember = "description";
			this.LookUpEdit_subtype.Properties.NullText = "";
			this.LookUpEdit_subtype.Properties.ShowFooter = false;
			this.LookUpEdit_subtype.Properties.ShowHeader = false;
			this.LookUpEdit_subtype.Properties.ValueMember = "tran_type";
			this.LookUpEdit_subtype.Size = new System.Drawing.Size(172, 20);
			this.LookUpEdit_subtype.TabIndex = 3;
			this.LookUpEdit_subtype.Properties.SortColumnIndex = 1;
			//
			//DateEdit_item_date
			//
			this.DateEdit_item_date.EditValue = null;
			this.DateEdit_item_date.Location = new System.Drawing.Point(108, 93);
			this.DateEdit_item_date.Name = "DateEdit_item_date";
			this.DateEdit_item_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit_item_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit_item_date.Size = new System.Drawing.Size(100, 20);
			this.DateEdit_item_date.TabIndex = 7;
			//
			//CalcEdit_amount
			//
			this.CalcEdit_amount.Location = new System.Drawing.Point(108, 120);
			this.CalcEdit_amount.Name = "CalcEdit_amount";
			this.CalcEdit_amount.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_amount.Properties.EditFormat.FormatString = "{0:f2}";
			this.CalcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_amount.Properties.Mask.BeepOnError = true;
			this.CalcEdit_amount.Properties.Mask.EditMask = "c";
			this.CalcEdit_amount.Properties.Precision = 2;
			this.CalcEdit_amount.Size = new System.Drawing.Size(100, 20);
			this.CalcEdit_amount.TabIndex = 9;
			//
			//TextEdit_reference
			//
			this.TextEdit_reference.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.TextEdit_reference.Location = new System.Drawing.Point(108, 147);
			this.TextEdit_reference.Name = "TextEdit_reference";
			this.TextEdit_reference.Properties.MaxLength = 50;
			this.TextEdit_reference.Size = new System.Drawing.Size(172, 20);
			this.TextEdit_reference.TabIndex = 11;
			//
			//ClientID_client
			//
			this.ClientID_client.Location = new System.Drawing.Point(108, 68);
			this.ClientID_client.Name = "ClientID_client";
			this.ClientID_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.ClientID_client.Properties.Appearance.Options.UseTextOptions = true;
			this.ClientID_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ClientID_client.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("ClientID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a client ID", "search", null, true) });
			this.ClientID_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.ClientID_client.Properties.DisplayFormat.FormatString = "0000000";
			this.ClientID_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ClientID_client.Properties.EditFormat.FormatString = "f0";
			this.ClientID_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.ClientID_client.Properties.Mask.BeepOnError = true;
			this.ClientID_client.Properties.Mask.EditMask = "\\d*";
			this.ClientID_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.ClientID_client.Properties.ValidateOnEnterKey = true;
			this.ClientID_client.Size = new System.Drawing.Size(100, 20);
			this.ClientID_client.TabIndex = 5;
			//
			//EditForm
			//
			this.AcceptButton = this.SimpleButton_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButton_Cancel;
			this.ClientSize = new System.Drawing.Size(292, 252);
			this.Controls.Add(this.ClientID_client);
			this.Controls.Add(this.TextEdit_reference);
			this.Controls.Add(this.CalcEdit_amount);
			this.Controls.Add(this.DateEdit_item_date);
			this.Controls.Add(this.LookUpEdit_subtype);
			this.Controls.Add(this.CheckEdit_ok_to_post);
			this.Controls.Add(this.LabelControl6);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl_scanned_client);
			this.Controls.Add(this.SimpleButton_Cancel);
			this.Controls.Add(this.SimpleButton_OK);
			this.Controls.Add(this.LabelControl1);
			this.Name = "EditForm";
			this.Text = "EditForm";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ok_to_post.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_subtype.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_item_date.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_item_date.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_amount.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_reference.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID_client.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_scanned_client;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl5;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl6;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_ok_to_post;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_subtype;
		protected internal DevExpress.XtraEditors.DateEdit DateEdit_item_date;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_amount;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_reference;
		protected internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID_client;
	}
}
