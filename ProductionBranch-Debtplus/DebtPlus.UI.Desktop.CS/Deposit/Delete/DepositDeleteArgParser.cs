using System;

namespace DebtPlus.UI.Desktop.CS.Deposit.Delete
{
    internal partial class DepositDeleteArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Configuration information
        /// </summary>
        private System.Collections.Specialized.NameValueCollection my_settings = System.Configuration.ConfigurationManager.AppSettings;

        public System.Collections.Specialized.NameValueCollection config
        {
            get { return my_settings; }
        }

        /// <summary>
        /// Deposit batch
        /// </summary>
        private System.Int32 _batch = -1;

        public System.Int32 Batch
        {
            get { return _batch; }
            set { _batch = value; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public DepositDeleteArgParser() : base(new string[] { "b" })
        {
            // Find the batch from the configuration file if there is one
            System.Collections.Specialized.NameValueCollection Config = System.Configuration.ConfigurationManager.AppSettings;

            string TempString = Config["batch"];
            if (TempString != null && TempString != string.Empty)
            {
                double TempDouble = 0;
                if (double.TryParse(TempString, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out TempDouble))
                {
                    if (TempDouble >= 0 && TempDouble <= Convert.ToDouble(Int32.MaxValue))
                    {
                        Batch = Convert.ToInt32(TempDouble);
                    }
                }
            }
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine("Usage: " + fname + " [-b#]")
            //AppendErrorLine("       -b# : existing batch ID")
        }

        /// <summary>
        /// Process a switch item
        /// </summary>
        protected override Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol.ToLower())
            {
                case "b":
                    double value = 0;
                    if (double.TryParse(switchValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out value))
                    {
                        if (value <= 0.0 || value > Convert.ToDouble(Int32.MaxValue))
                        {
                            ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                        }
                        else
                        {
                            Batch = Convert.ToInt32(value);
                        }
                    }
                    else
                    {
                        ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    }
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }
    }
}