#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	internal class Form_Lockbox : DebtPlus.Data.Forms.DebtPlusForm, ISupportContext
	{
		private Page_Template withEventsField_CurrentPage;
		private Page_Template CurrentPage
		{
			get
			{
				return withEventsField_CurrentPage;
			}
			set
			{
				if (withEventsField_CurrentPage != null)
				{
					withEventsField_CurrentPage.ButtonClicked -= CurrentForm_NextButton;
				}
				withEventsField_CurrentPage = value;
				if (withEventsField_CurrentPage != null)
				{
					withEventsField_CurrentPage.ButtonClicked += CurrentForm_NextButton;
				}
			}
		}
		
		public LockboxGlobals Globals { get; set; }

		public Form_Lockbox(ref LockboxGlobals Globals) : this()
		{
			this.Globals = Globals;
		}

		public Form_Lockbox() : base()
		{
			Load += Form_Lockbox_Load;
			InitializeComponent();
		}

#region " Windows Form Designer generated code "
		// Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
					if (components != null)
					{
						components.Dispose();
					}
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		internal Page_Results Page_Results1;
		internal Page_Status Page_Status1;
		internal Page_Input Page_Input1;
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form_Lockbox));
			this.Page_Results1 = new Page_Results();
			this.Page_Status1 = new Page_Status();
			this.Page_Input1 = new Page_Input();

			this.SuspendLayout();

			//
			//Page_Results1
			//
			this.Page_Results1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Page_Results1.Location = new System.Drawing.Point(0, 0);
			this.Page_Results1.Name = "Page_Results1";
			this.Page_Results1.Size = new System.Drawing.Size(352, 286);
			this.ToolTipController1.SetSuperTip(this.Page_Results1, null);
			this.Page_Results1.TabIndex = 0;
			this.Page_Results1.Visible = false;
			//
			//Page_Status1
			//
			this.Page_Status1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Page_Status1.Location = new System.Drawing.Point(0, 0);
			this.Page_Status1.Name = "Page_Status1";
			this.Page_Status1.Size = new System.Drawing.Size(352, 286);
			this.ToolTipController1.SetSuperTip(this.Page_Status1, null);
			this.Page_Status1.TabIndex = 1;
			this.Page_Status1.Visible = false;
			//
			//Page_Input1
			//
			this.Page_Input1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Page_Input1.Location = new System.Drawing.Point(0, 0);
			this.Page_Input1.Name = "Page_Input1";
			this.Page_Input1.Size = new System.Drawing.Size(352, 286);
			this.ToolTipController1.SetSuperTip(this.Page_Input1, null);
			this.Page_Input1.TabIndex = 2;
			this.Page_Input1.Visible = false;
			//
			//Form_Lockbox
			//
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(352, 286);
			this.Controls.Add(this.Page_Input1);
			this.Controls.Add(this.Page_Status1);
			this.Controls.Add(this.Page_Results1);
			this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "Form_Lockbox";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Load lockbox deposit information";

			this.ResumeLayout(false);
		}
#endregion

		private void Form_Lockbox_Load(object sender, EventArgs e)
		{
			NewPage("KeyField");
		}

		/// <summary>
		/// Handle the event from the sub-pages to switch pages
		/// </summary>
		private void CurrentForm_NextButton(object sender, string TagString)
		{
			NewPage(TagString);
		}

		private void NewPage(string id)
		{
			switch (id)
{
				case "ok":
					Close();
					break;
					
				case "cancel":
					Close();
					break;
					
				case "KeyField":
					CurrentPage = Page_Input1;
					Page_Input1.Visible = false;
					Page_Results1.Visible = false;
					Page_Status1.Visible = false;
					CurrentPage.Visible = true;
					CurrentPage.PageLoad();
					break;
					
				case "status":
					CurrentPage = Page_Status1;
					Page_Input1.Visible = false;
					Page_Results1.Visible = false;
					Page_Status1.Visible = false;
					CurrentPage.Visible = true;
					CurrentPage.PageLoad();
					break;
					
				case "results":
					CurrentPage = Page_Results1;
					Page_Input1.Visible = false;
					Page_Results1.Visible = false;
					Page_Status1.Visible = false;
					CurrentPage.Visible = true;
					CurrentPage.PageLoad();
					break;
			}
		}
	}
}