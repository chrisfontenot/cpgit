#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	internal class Page_Results : Page_Template
	{
		public Page_Results() : base()
		{
			InitializeComponent();
		}

		#region " Windows Form Designer generated code "

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing)
{
				if (components != null)
{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		internal DevExpress.XtraEditors.LabelControl Label11;
		internal DevExpress.XtraEditors.CheckEdit chk_print_lockbox_deposit_report;
		internal DevExpress.XtraEditors.CheckEdit chk_deposit_batch_report;
		internal DevExpress.XtraEditors.LabelControl Label10;

		internal DevExpress.XtraEditors.LabelControl lbl_batch_id;
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Page_Results));
			this.Label11 = new DevExpress.XtraEditors.LabelControl();
			this.chk_print_lockbox_deposit_report = new DevExpress.XtraEditors.CheckEdit();
			this.chk_deposit_batch_report = new DevExpress.XtraEditors.CheckEdit();
			this.Label10 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_batch_id = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.chk_print_lockbox_deposit_report.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.chk_deposit_batch_report.Properties).BeginInit();
			this.SuspendLayout();
			//
			//Label11
			//
			this.Label11.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.Label11.Appearance.Options.UseTextOptions = true;
			this.Label11.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.Label11.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.Label11.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.Label11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label11.Location = new System.Drawing.Point(8, 108);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(332, 38);
			this.Label11.TabIndex = 20;
			this.Label11.Text = "The following two reports may be of help showing the transactions. You may print " + "either one (or both).";
			this.Label11.UseMnemonic = false;
			//
			//chk_print_lockbox_deposit_report
			//
			this.chk_print_lockbox_deposit_report.Location = new System.Drawing.Point(8, 152);
			this.chk_print_lockbox_deposit_report.Name = "chk_print_lockbox_deposit_report";
			this.chk_print_lockbox_deposit_report.Properties.Caption = "Print Lockbox deposit Report";
			this.chk_print_lockbox_deposit_report.Size = new System.Drawing.Size(248, 19);
			this.chk_print_lockbox_deposit_report.TabIndex = 21;
			//
			//chk_deposit_batch_report
			//
			this.chk_deposit_batch_report.Location = new System.Drawing.Point(8, 177);
			this.chk_deposit_batch_report.Name = "chk_deposit_batch_report";
			this.chk_deposit_batch_report.Properties.Caption = "Print Deposit Batch Report";
			this.chk_deposit_batch_report.Size = new System.Drawing.Size(248, 19);
			this.chk_deposit_batch_report.TabIndex = 22;
			//
			//Label10
			//
			this.Label10.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.Label10.Appearance.Options.UseTextOptions = true;
			this.Label10.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.Label10.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.Label10.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.Label10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label10.Location = new System.Drawing.Point(8, 40);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(332, 62);
			this.Label10.TabIndex = 19;
			this.Label10.Text = resources.GetString("Label10.Text");
			this.Label10.UseMnemonic = false;
			//
			//lbl_batch_id
			//
			this.lbl_batch_id.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_batch_id.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_batch_id.Appearance.Options.UseFont = true;
			this.lbl_batch_id.Appearance.Options.UseTextOptions = true;
			this.lbl_batch_id.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.lbl_batch_id.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.lbl_batch_id.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_batch_id.Location = new System.Drawing.Point(10, 3);
			this.lbl_batch_id.Name = "lbl_batch_id";
			this.lbl_batch_id.Size = new System.Drawing.Size(330, 20);
			this.lbl_batch_id.TabIndex = 18;
			this.lbl_batch_id.Text = "Deposit Batch ID: {0:d8}";
			this.lbl_batch_id.UseMnemonic = false;
			//
			//Page_Results
			//
			this.Controls.Add(this.Label11);
			this.Controls.Add(this.chk_print_lockbox_deposit_report);
			this.Controls.Add(this.chk_deposit_batch_report);
			this.Controls.Add(this.Label10);
			this.Controls.Add(this.lbl_batch_id);
			this.Name = "Page_Results";
			this.Controls.SetChildIndex(this.Button_Prev, 0);
			this.Controls.SetChildIndex(this.Button_Next, 0);
			this.Controls.SetChildIndex(this.lbl_batch_id, 0);
			this.Controls.SetChildIndex(this.Label10, 0);
			this.Controls.SetChildIndex(this.chk_deposit_batch_report, 0);
			this.Controls.SetChildIndex(this.chk_print_lockbox_deposit_report, 0);
			this.Controls.SetChildIndex(this.Label11, 0);
			((System.ComponentModel.ISupportInitialize)this.chk_print_lockbox_deposit_report.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.chk_deposit_batch_report.Properties).EndInit();
			this.ResumeLayout(false);
		}

		#endregion

		internal override void PageLoad()
		{
			// Format the deposit batch ID on the header
			lbl_batch_id.Text = string.Format(lbl_batch_id.Text, Globals.deposit_batch_id);

			// Enable the buttons with the proper codes
            Button_Prev.Text = LockboxGlobals.button_again;
			Button_Prev.Tag = "KeyField";
			Button_Prev.Enabled = true;

            Button_Next.Text = LockboxGlobals.button_finish;
			Button_Next.Tag = "ok";
			Button_Next.Enabled = true;
		}

		/// <summary>
		/// Process the click event on the button
		/// </summary>

		protected override void Button_Click(object sender, EventArgs e)
		{
			if (chk_print_lockbox_deposit_report.Checked)
            {
				DebtPlus.Reports.Deposits.LockBox.DepositLockboxReport rpt = new DebtPlus.Reports.Deposits.LockBox.DepositLockboxReport();
				rpt.Parameter_DepositBatchID = Globals.deposit_batch_id;
				if (rpt.RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
                {
					rpt.RunReportInSeparateThread();
				}
			}

			if (chk_deposit_batch_report.Checked)
            {
				DebtPlus.Reports.Deposits.Batch.DepositBatchReport rpt = new DebtPlus.Reports.Deposits.Batch.DepositBatchReport();
				rpt.Parameter_DepositBatchID = Globals.deposit_batch_id;
				if (rpt.RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
                {
					rpt.RunReportInSeparateThread();
				}
			}

			base.Button_Click(sender, e);
		}
	}
}
