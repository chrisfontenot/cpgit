#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Resources;
using System.Reflection;
using System.Collections.Specialized;
using System.IO;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	internal class LockboxGlobals : IDisposable
	{
		public Lockbox lbd { get; set; }

		public LockboxGlobals()
		{
			filename = string.Empty;
			bank = -1;
			lbd = null;
			deposit_batch_id = -1;
			batch_hash_total = 0m;
			batch_amount = 0m;
			batch_count = 0;
			ValidTransactions = 0;
			fso = null;
		}

		// Filename for the KeyField file
		internal string filename;

		internal Int32 bank = -1;
		// Current deposit batch ID
		internal Int32 deposit_batch_id = -1;

		internal string deposit_batch_label = string.Empty;
		// Information about the current file
		internal decimal batch_hash_total = 0m;
		internal decimal batch_amount = 0m;
		internal Int32 batch_count = 0;

		internal Int32 ValidTransactions = 0;
		// Input file handle

		internal FileStream fso = null;
		internal static string file_open_title = "Input Lockbox File";
        internal static readonly string button_next_finish = "Finish >";
        internal static readonly string button_next_batch = "Next Batch";
        internal static readonly string button_next_close = "Close";
        internal static readonly string Button_Cancel = "Cancel";
        internal static readonly string button_next = "Next >";
        internal static readonly string button_prev = "< Prev";
        internal static readonly string button_again = "< Again";
        internal static readonly string button_finish = "Finish >";
        internal static readonly string error_dialog_title = "Error loading lockbox file";
        internal static readonly string error_text_invalid_date = "Invalid Date";
        internal static readonly string error_text_invalid_amount = "Invalid Amount";
        internal static readonly string error_text_invalid_client = "Invalid Client";
        internal static readonly string error_msg_write = "Error Writing to the deposit detail table";
        internal static readonly string error_msg_create = "Error creating the deposit batch";
        internal static readonly string error_msg_close = "Error closing the deposit batch";

        internal static readonly string required_field = "This field is required.";
		#region "IDisposable Support"
			// To detect redundant calls
		private bool disposedValue;

		// IDisposable
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposedValue)
{
				if (disposing)
{
					lbd.Dispose();
					if (fso != null)
						fso.Dispose();
				}

				// TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
				// TODO: set large fields to null.
			}
			this.disposedValue = true;
		}

		// TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
		//Protected Overrides Sub Finalize()
		//    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
		//    Dispose(False)
		//    MyBase.Finalize()
		//End Sub

		// This code added by Visual Basic to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion

	}

	internal interface ISupportContext
	{
		LockboxGlobals Globals { get; set; }
	}
}
