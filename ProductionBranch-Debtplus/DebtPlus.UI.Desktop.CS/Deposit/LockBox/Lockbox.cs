using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Collections.Specialized;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	internal class Lockbox : IDisposable
	{
		private ListDictionary privateParsing_data = new ListDictionary(System.Collections.CaseInsensitiveComparer.DefaultInvariant);
		internal ListDictionary parsing_data
		{
			get
			{
				return privateParsing_data;
			}
		}

		private System.Collections.Specialized.NameValueCollection m_configSettings;

		//The folowing private vars mirror entries in the config file; defaults are set just in case they are missing
		private readonly string m_fileOpenFilter = "Text Files (*.txt)|*.txt|Mailbox Files (*.mbx)|*.mbx|All Files (*.*)|*.*";
		private readonly string m_fileOpenExt = ".txt";
		private readonly Int32 m_filterIndex = 0;
		private readonly string m_createCommand = "xpr_deposit_batch_create";
		private readonly string m_loadCommand = "xpr_deposit_batch_entry_LB";

		private readonly string m_closeCommand = "xpr_deposit_batch_close";
		public string FileOpenFilter
		{
			get
			{
				return m_fileOpenFilter;
			}
		}

		public string FileOpenExt
		{
			get
			{
				return m_fileOpenExt;
			}
		}

		public Int32 FilterIndex
		{
			get
			{
				return m_filterIndex;
			}
		}

		// Procedures to deal with items in the lockbox batch

		public string CreateCommand
		{
			get
			{
				return m_createCommand;
			}
		}

		public string LoadCommand
		{
			get
			{
				return m_loadCommand;
			}
		}

		public string CloseCommand
		{
			get
			{
				return m_closeCommand;
			}
		}

		public Lockbox() : this(null)
		{
		}

		public Lockbox(System.Collections.Specialized.NameValueCollection configSettings)
		{
			m_configSettings = configSettings;

			if (m_configSettings != null)
			{
				m_createCommand = DebtPlus.Configuration.Config.GetConfigValueForKey(configSettings, "CreateCommand", m_createCommand);
				m_loadCommand = DebtPlus.Configuration.Config.GetConfigValueForKey(configSettings, "LoadCommand", m_loadCommand);
				m_closeCommand = DebtPlus.Configuration.Config.GetConfigValueForKey(configSettings, "CloseCommand", m_closeCommand);
				m_fileOpenFilter = DebtPlus.Configuration.Config.GetConfigValueForKey(configSettings, "file_open_filter", m_fileOpenFilter);
				m_fileOpenExt = DebtPlus.Configuration.Config.GetConfigValueForKey(configSettings, "file_open_ext", m_fileOpenExt);

				if (!(Int32.TryParse(DebtPlus.Configuration.Config.GetConfigValueForKey(configSettings, "filter_index", m_filterIndex.ToString()), out m_filterIndex)))
				{
					m_filterIndex = 0;
				}

				// Build the list of regular expressions for the various types
				foreach (string key in configSettings.Keys)
				{
					if (key.StartsWith("expr_"))
					{
						parsing_data.Add(key.Substring(5), new System.Text.RegularExpressions.Regex(m_configSettings[key]));
					}
				}
			}

			if (parsing_data.Count == 0)
			{
				parsing_data.Add("HASH", new System.Text.RegularExpressions.Regex("^(?<marker>9{9})\\s{26}TOTAL RECORD\\s{8,}(?<count>\\d+)\\s+(?<cents>\\d+)$"));
				parsing_data.Add("LB", new System.Text.RegularExpressions.Regex("^(?<reference>\\S{9})(?<client>\\d+)\\s+(?<account>\\d{8})\\s+(?<name>.*)(?<month>\\d\\d)(?<day>\\d\\d)(?<year>\\d\\d) (?<cents>\\d{7})\\d{7}$"));
			}
		}

#region "IDisposable Support"
		// To detect redundant calls
		private bool disposedValue;

		// IDisposable
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposedValue)
			{
				if (disposing)
				{
				}

				privateParsing_data = null;
				m_configSettings = null;
			}
			this.disposedValue = true;
		}

		// TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
		//Protected Overrides Sub Finalize()
		//    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
		//    Dispose(False)
		//    MyBase.Finalize()
		//End Sub

		// This code added by Visual Basic to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
			Dispose(true);
			GC.SuppressFinalize(this);
		}
#endregion
	}
}
