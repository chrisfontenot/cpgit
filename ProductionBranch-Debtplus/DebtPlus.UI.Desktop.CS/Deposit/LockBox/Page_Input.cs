#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	internal class Page_Input : Page_Template
	{
		private DataSet ds = new DataSet("ds");
		public Page_Input() : base()
		{
			InitializeComponent();
		}

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
				ds.Dispose();
			}
			base.Dispose(disposing);
		}

		#region " Windows Form Designer generated code "

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;
		internal DevExpress.XtraEditors.LabelControl Label1;
		internal DevExpress.XtraEditors.LabelControl Label2;
		internal DevExpress.XtraEditors.LabelControl Label3;
		internal DevExpress.XtraEditors.LabelControl Label4;
		internal DevExpress.XtraEditors.TextEdit txt_batch_label;
        internal DevExpress.XtraEditors.LookUpEdit BankListLookupEdit;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.BankListLookupEdit = new DevExpress.XtraEditors.LookUpEdit();
			this.txt_batch_label = new DevExpress.XtraEditors.TextEdit();
			this.Label4 = new DevExpress.XtraEditors.LabelControl();
			this.Label3 = new DevExpress.XtraEditors.LabelControl();
			this.Label2 = new DevExpress.XtraEditors.LabelControl();
			this.Label1 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.BankListLookupEdit.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.txt_batch_label.Properties).BeginInit();
			this.SuspendLayout();
			//
			//BankListLookupEdit
			//
			this.BankListLookupEdit.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.BankListLookupEdit.Location = new System.Drawing.Point(80, 112);
			this.BankListLookupEdit.Name = "BankListLookupEdit";
			this.BankListLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.BankListLookupEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Bank", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)
			});
			this.BankListLookupEdit.Properties.DisplayMember = "description";
			this.BankListLookupEdit.Properties.NullText = "Choose one from the following list...";
			this.BankListLookupEdit.Properties.ShowFooter = false;
            this.BankListLookupEdit.Properties.ValueMember = "Id";
			this.BankListLookupEdit.Size = new System.Drawing.Size(256, 20);
			this.BankListLookupEdit.TabIndex = 3;
			//
			//txt_batch_label
			//
			this.txt_batch_label.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.txt_batch_label.EditValue = "";
			this.txt_batch_label.Location = new System.Drawing.Point(80, 137);
			this.txt_batch_label.Name = "txt_batch_label";
			this.txt_batch_label.Properties.MaxLength = 50;
			this.txt_batch_label.Size = new System.Drawing.Size(256, 20);
			this.txt_batch_label.TabIndex = 5;
			this.txt_batch_label.ToolTip = "Enter the optional label associated with this deposit batch";
			//
			//Label4
			//
			this.Label4.Location = new System.Drawing.Point(8, 138);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(59, 13);
			this.Label4.TabIndex = 4;
			this.Label4.Text = "Batch Label:";
			//
			//Label3
			//
			this.Label3.Location = new System.Drawing.Point(8, 113);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(27, 13);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "Bank:";
			//
			//Label2
			//
			this.Label2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.Label2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.Label2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.Label2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.Label2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label2.Location = new System.Drawing.Point(8, 48);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(328, 58);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "Each deposit batch goes to a bank account. You may enter an optional label as wel" + "l. Select the bank account and optionally enter the label below.";
			//
			//Label1
			//
			this.Label1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.Label1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label1.Location = new System.Drawing.Point(8, 8);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(341, 34);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "This application will import the contents of a lockbox file into a new deposit ba" + "tch.";
			//
			//Page_Input
			//
			this.Controls.Add(this.BankListLookupEdit);
			this.Controls.Add(this.txt_batch_label);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.Label2);
			this.Name = "Page_Input";
			this.Controls.SetChildIndex(this.Label2, 0);
			this.Controls.SetChildIndex(this.Label1, 0);
			this.Controls.SetChildIndex(this.Label3, 0);
			this.Controls.SetChildIndex(this.Label4, 0);
			this.Controls.SetChildIndex(this.txt_batch_label, 0);
			this.Controls.SetChildIndex(this.BankListLookupEdit, 0);
			this.Controls.SetChildIndex(this.Button_Prev, 0);
			this.Controls.SetChildIndex(this.Button_Next, 0);
			((System.ComponentModel.ISupportInitialize)this.BankListLookupEdit.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.txt_batch_label.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

        #endregion " Windows Form Designer generated code "

		internal override void PageLoad()
		{
            try
                {
                var colBanks = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.ActiveFlag && (s.type == "C" || s.type == "D"));
                BankListLookupEdit.Properties.DataSource = colBanks;
                BankListLookupEdit.EditValue = DebtPlus.LINQ.Cache.bank.getDefault("C") ?? DebtPlus.LINQ.Cache.bank.getDefault("D");
    			BankListLookupEdit.Properties.PopupWidth = 300;
	    		BankListLookupEdit.EditValueChanged += BankListLookupEdit_EditValueChanged;
		    	BankListLookupEdit.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error loading bank list");
			}

            // Set the bank reference
            Globals.bank = DebtPlus.Utils.Nulls.v_Int32(BankListLookupEdit.EditValue).GetValueOrDefault();

			// Disable the previous button
            Button_Prev.Enabled = true;
            Button_Prev.Tag = "cancel";
            Button_Prev.Text = LockboxGlobals.Button_Cancel;

			// The next button is to the status page.
            Button_Next.Enabled = (Globals.bank > 0);
            Button_Next.Tag = "status";
            Button_Next.Text = LockboxGlobals.button_next;
		}

		/// <summary>
		/// Enable/Disable the button for the next step
		/// </summary>
		private void BankListLookupEdit_EditValueChanged(object Sender, EventArgs e)
		{
			// If there is a bank account selected then store it
            Globals.bank = DebtPlus.Utils.Nulls.v_Int32(BankListLookupEdit.EditValue).GetValueOrDefault();
            Button_Next.Enabled = (Globals.bank > 0);
		}

		/// <summary>
		/// Process the click event on the button
		/// </summary>
		protected override void Button_Click(object sender, EventArgs e)
		{
			// Save the bank desired for the deposit
            Globals.bank = DebtPlus.Utils.Nulls.v_Int32(BankListLookupEdit.EditValue).GetValueOrDefault();

			// Save the batch label
			Globals.deposit_batch_label = string.Empty;
            if (txt_batch_label.EditValue != null)
            {
                Globals.deposit_batch_label = Convert.ToString(txt_batch_label.EditValue);
			}

            var ctl = (SimpleButton)sender;
			// If this is the "NEXT" button then ask for the filename and don't go unless one is given
            if (Convert.ToString(ctl.Tag) == "cancel" || request_filename())
            {
				base.Button_Click(sender, e);
			}
		}

		/// <summary>
		/// Ask for the name of the KeyField file
		/// </summary>
		private bool request_filename()
		{
            using (var dlg = new OpenFileDialog()
            {
                DefaultExt       = Globals.lbd.FileOpenExt,
                Filter           = Globals.lbd.FileOpenFilter,
                FilterIndex      = Globals.lbd.FilterIndex,
                Title            = LockboxGlobals.file_open_title,
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                CheckFileExists  = true,
                CheckPathExists  = true,
                DereferenceLinks = true,
                Multiselect      = false,
                ReadOnlyChecked  = true,
                RestoreDirectory = true,
                ShowReadOnly     = false,
                ValidateNames    = true
            })
            {
                if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return false;
                }
				Globals.filename = dlg.FileName;
			}

			// If the user enter a name then try to open it now.
			try
            {
				Globals.fso = new FileStream(Globals.filename, FileMode.Open, FileAccess.Read, FileShare.Read, 4096);
                return true;
			}
#pragma warning disable 168
            catch (FileNotFoundException ex)
            {
				DebtPlus.Data.Forms.MessageBox.Show(string.Format("The file '{0}' was not found.", Globals.filename), "Error opening KeyField file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
            catch (DirectoryNotFoundException ex)
            {
				DebtPlus.Data.Forms.MessageBox.Show(string.Format("The directory for the file '{0}' was not found.", Globals.filename), "Error opening KeyField file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
            catch (Exception ex)
            {
				DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening KeyField file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
			}
#pragma warning restore 168

			return false;
		}
	}
}
