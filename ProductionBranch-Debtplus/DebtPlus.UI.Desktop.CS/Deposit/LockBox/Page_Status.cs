#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	internal class Page_Status : Page_Template
	{
        private class RecordValueType
        {
            public string deposit_client { get; private set; }
            public string deposit_date { get; private set; }
            public string deposit_month { get; private set; }
            public string deposit_day { get; private set; }
            public string deposit_year { get; private set; }
            public string deposit_amount { get; private set; }
            public string deposit_cents { get; private set; }
            public string deposit_reference { get; private set; }
            public string message { get; private set; }

            public DateTime? transaction_date { get; private set; }
            public Decimal transaction_amount { get; private set; }
            public string transaction_reference { get; private set; }

            public RecordValueType()
            {
			    deposit_client    = string.Empty;
                deposit_date      = string.Empty;
                deposit_month     = string.Empty;
                deposit_day       = string.Empty;
                deposit_year      = string.Empty;
                deposit_amount    = string.Empty;
                deposit_cents     = string.Empty;
                deposit_reference = string.Empty;
                message           = string.Empty;

                transaction_date      = null;
                transaction_amount    = 0m;
                transaction_reference = null;
            }

            /// <summary>
            /// Return the indicated date for the transaction
            /// </summary>
            private DateTime? getItemDate()
            {
                // Look at the full date if one is given
                if (!string.IsNullOrWhiteSpace(deposit_date))
                {
                    return System.DateTime.Parse(deposit_date);
                }

                // Look at the fields if possible
                Int32 tempYear = string.IsNullOrWhiteSpace(deposit_year) ? 0 : Int32.Parse(deposit_year);
                Int32 tempMonth = string.IsNullOrWhiteSpace(deposit_month) ? 1 : Int32.Parse(deposit_month);
                Int32 tempDay = string.IsNullOrWhiteSpace(deposit_day) ? 1 : Int32.Parse(deposit_day);

                // Allow for a two-digit year to occur.
                if (tempYear < 100)
                {
                    tempYear += 1900;
                }

                // If there are no components then just return "missing" value
                if (tempYear < 1980 && tempMonth == 1 && tempDay == 1)
                {
                    return null;
                }

                // Return the resulting year from the component pieces
                return new System.DateTime(tempYear, tempMonth, tempDay);
            }

            /// <summary>
            /// Return the deposit amount from the input fields
            /// </summary>
            private decimal getAmount()
            {
                if (!string.IsNullOrWhiteSpace(deposit_amount))
                {
                    return decimal.Parse(deposit_amount);
                }

                // Look at the cents figure
                if (! string.IsNullOrWhiteSpace(deposit_cents) && System.Text.RegularExpressions.Regex.IsMatch(deposit_cents, @"\d{3}$"))
                {
                    string v_cents = System.Text.RegularExpressions.Regex.Replace(deposit_cents, @"^(\d+)(\d\d)$", @"$1.$2");
                    return decimal.Parse(v_cents);
                }

                // We don't have an amount.
                return 0M;
            }

            public RecordValueType(Match m)
                : this()
            {
                deposit_client    = m.Groups["client"] != null    ? m.Groups["client"].Value.ToString().Trim()    : String.Empty;
                deposit_date      = m.Groups["date"] != null      ? m.Groups["date"].Value.ToString().Trim()      : String.Empty;
                deposit_month     = m.Groups["month"] != null     ? m.Groups["month"].Value.ToString().Trim()     : String.Empty;
                deposit_day       = m.Groups["day"] != null       ? m.Groups["day"].Value.ToString().Trim()       : String.Empty;
                deposit_year      = m.Groups["year"] != null      ? m.Groups["year"].Value.ToString().Trim()      : String.Empty;
                deposit_amount    = m.Groups["amount"] != null    ? m.Groups["amount"].Value.ToString().Trim()    : String.Empty;
                deposit_cents     = m.Groups["cents"] != null     ? m.Groups["cents"].Value.ToString().Trim()     : String.Empty;
                deposit_reference = m.Groups["reference"] != null ? m.Groups["reference"].Value.ToString().Trim() : String.Empty;

                // Find the transaction date if one is given
                try
                {
                    transaction_date = getItemDate();
                }
                catch
                {
        			message = LockboxGlobals.error_text_invalid_date;
                }

                // Try to find the transaction dollar amount
                try
                {
                    transaction_amount = getAmount();
                }
                catch
                {
                    message = LockboxGlobals.error_text_invalid_amount;
                }

                // Try to find the reference field
                if (deposit_reference.Length > 50)
                {
                    transaction_reference = deposit_reference.Substring(0, 50);
                }
                else
                {
                    transaction_reference = deposit_reference;
                }
			}
        }

		private BackgroundWorker bt = new BackgroundWorker();

		// Command to insert transactions into the lockbox batch if we need some special validations.
		public string InsertCmdString
        {
			get
            {
                return Globals.lbd.LoadCommand; 
            }
		}

		// Command to create the new batch
		public string CreateCmdString
        {
			get
            {
                return Globals.lbd.CreateCommand;
            }
		}

		public Page_Status() : base()
		{
			InitializeComponent();

            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
            bt.DoWork += bt_DoWork;
        }

#region Windows Form Designer generated code

		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing)
{
				if (components != null)
{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		internal DevExpress.XtraEditors.MarqueeProgressBarControl MarqueeProgressBarControl1;
		internal DevExpress.XtraEditors.LabelControl lbl_hash_total;
		internal DevExpress.XtraEditors.LabelControl lbl_transaction_dollars;
		internal DevExpress.XtraEditors.LabelControl lbl_valid_transactions;
		internal DevExpress.XtraEditors.LabelControl lbl_deposits_loaded;
		internal DevExpress.XtraEditors.LabelControl Label8;
		internal DevExpress.XtraEditors.LabelControl Label7;
		internal DevExpress.XtraEditors.LabelControl Label6;

		internal DevExpress.XtraEditors.LabelControl Label5;
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.MarqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
			this.lbl_hash_total = new DevExpress.XtraEditors.LabelControl();
			this.lbl_transaction_dollars = new DevExpress.XtraEditors.LabelControl();
			this.lbl_valid_transactions = new DevExpress.XtraEditors.LabelControl();
			this.lbl_deposits_loaded = new DevExpress.XtraEditors.LabelControl();
			this.Label8 = new DevExpress.XtraEditors.LabelControl();
			this.Label7 = new DevExpress.XtraEditors.LabelControl();
			this.Label6 = new DevExpress.XtraEditors.LabelControl();
			this.Label5 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//MarqueeProgressBarControl1
			//
			this.MarqueeProgressBarControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.MarqueeProgressBarControl1.EditValue = 0;
			this.MarqueeProgressBarControl1.Location = new System.Drawing.Point(8, 160);
			this.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1";
			this.MarqueeProgressBarControl1.Size = new System.Drawing.Size(336, 24);
			this.MarqueeProgressBarControl1.TabIndex = 26;
			//
			//lbl_hash_total
			//
			this.lbl_hash_total.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_hash_total.Appearance.Options.UseTextOptions = true;
			this.lbl_hash_total.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_hash_total.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_hash_total.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_hash_total.Location = new System.Drawing.Point(222, 118);
			this.lbl_hash_total.Name = "lbl_hash_total";
			this.lbl_hash_total.Size = new System.Drawing.Size(122, 13);
			this.lbl_hash_total.TabIndex = 25;
			this.lbl_hash_total.Text = "0";
			this.lbl_hash_total.UseMnemonic = false;
			//
			//lbl_transaction_dollars
			//
			this.lbl_transaction_dollars.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_transaction_dollars.Appearance.Options.UseTextOptions = true;
			this.lbl_transaction_dollars.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_transaction_dollars.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_transaction_dollars.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_transaction_dollars.Location = new System.Drawing.Point(222, 81);
			this.lbl_transaction_dollars.Name = "lbl_transaction_dollars";
			this.lbl_transaction_dollars.Size = new System.Drawing.Size(122, 13);
			this.lbl_transaction_dollars.TabIndex = 23;
			this.lbl_transaction_dollars.Text = "0";
			this.lbl_transaction_dollars.UseMnemonic = false;
			//
			//lbl_valid_transactions
			//
			this.lbl_valid_transactions.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_valid_transactions.Appearance.Options.UseTextOptions = true;
			this.lbl_valid_transactions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_valid_transactions.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_valid_transactions.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_valid_transactions.Location = new System.Drawing.Point(222, 45);
			this.lbl_valid_transactions.Name = "lbl_valid_transactions";
			this.lbl_valid_transactions.Size = new System.Drawing.Size(122, 13);
			this.lbl_valid_transactions.TabIndex = 21;
			this.lbl_valid_transactions.Text = "0";
			this.lbl_valid_transactions.UseMnemonic = false;
			//
			//lbl_deposits_loaded
			//
			this.lbl_deposits_loaded.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_deposits_loaded.Appearance.Options.UseTextOptions = true;
			this.lbl_deposits_loaded.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_deposits_loaded.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_deposits_loaded.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_deposits_loaded.Location = new System.Drawing.Point(222, 8);
			this.lbl_deposits_loaded.Name = "lbl_deposits_loaded";
			this.lbl_deposits_loaded.Size = new System.Drawing.Size(122, 13);
			this.lbl_deposits_loaded.TabIndex = 19;
			this.lbl_deposits_loaded.Text = "0";
			this.lbl_deposits_loaded.UseMnemonic = false;
			//
			//Label8
			//
			this.Label8.Location = new System.Drawing.Point(38, 118);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(134, 13);
			this.Label8.TabIndex = 24;
			this.Label8.Text = "Batch hash total from KeyField:";
			this.Label8.UseMnemonic = false;
			//
			//Label7
			//
			this.Label7.Location = new System.Drawing.Point(38, 80);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(145, 13);
			this.Label7.TabIndex = 22;
			this.Label7.Text = "Dollar amount of transactions:";
			this.Label7.UseMnemonic = false;
			//
			//Label6
			//
			this.Label6.Location = new System.Drawing.Point(38, 45);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(137, 13);
			this.Label6.TabIndex = 20;
			this.Label6.Text = "Number of valid transactions";
			this.Label6.UseMnemonic = false;
			//
			//Label5
			//
			this.Label5.Location = new System.Drawing.Point(38, 7);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(128, 13);
			this.Label5.TabIndex = 18;
			this.Label5.Text = "Number of deposits loaded";
			this.Label5.UseMnemonic = false;
			//
			//Page_Status
			//
			this.Controls.Add(this.MarqueeProgressBarControl1);
			this.Controls.Add(this.lbl_hash_total);
			this.Controls.Add(this.lbl_transaction_dollars);
			this.Controls.Add(this.lbl_valid_transactions);
			this.Controls.Add(this.lbl_deposits_loaded);
			this.Controls.Add(this.Label8);
			this.Controls.Add(this.Label7);
			this.Controls.Add(this.Label6);
			this.Controls.Add(this.Label5);
			this.Name = "Page_Status";
			this.Controls.SetChildIndex(this.Button_Prev, 0);
			this.Controls.SetChildIndex(this.Button_Next, 0);
			this.Controls.SetChildIndex(this.Label5, 0);
			this.Controls.SetChildIndex(this.Label6, 0);
			this.Controls.SetChildIndex(this.Label7, 0);
			this.Controls.SetChildIndex(this.Label8, 0);
			this.Controls.SetChildIndex(this.lbl_deposits_loaded, 0);
			this.Controls.SetChildIndex(this.lbl_valid_transactions, 0);
			this.Controls.SetChildIndex(this.lbl_transaction_dollars, 0);
			this.Controls.SetChildIndex(this.lbl_hash_total, 0);
			this.Controls.SetChildIndex(this.MarqueeProgressBarControl1, 0);
			((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		#endregion

		internal override void PageLoad()
		{
            Button_Next.Text    = LockboxGlobals.button_next;
            Button_Next.Tag     = "results";
            Button_Next.Enabled = false;

            Button_Prev.Tag     = "KeyField";
            Button_Prev.Text    = LockboxGlobals.button_prev;
            Button_Prev.Enabled = false;

			// Create a thread to process the load operation in the background
			bt.RunWorkerAsync();
		}

		/// <summary>
		/// The thread performing the load has completed its processing
		/// </summary>
		private void bt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			MarqueeProgressBarControl1.Properties.Stopped = true;
			Button_Next.Enabled = true;
		}

		/// <summary>
		/// update the status for the hash total
		/// </summary>
		private void display_lbl_hash_total()
		{
			if (InvokeRequired)
            {
				BeginInvoke(new MethodInvoker(display_lbl_hash_total));
				return;
			}

			lbl_hash_total.Text = string.Format("{0:c}", Globals.batch_hash_total);
		}

		/// <summary>
		/// update the status for the transaction dollars
		/// </summary>
		private void display_lbl_transaction_dollars()
		{
			if (InvokeRequired)
            {
				BeginInvoke(new MethodInvoker(display_lbl_transaction_dollars));
				return;
			}

			lbl_transaction_dollars.Text = string.Format("{0:c}", Globals.batch_amount);
		}

		/// <summary>
		/// update the status for the number of deposits loaded
		/// </summary>
		private void display_lbl_deposits_loaded()
		{
			if (InvokeRequired)
            {
				BeginInvoke(new MethodInvoker(display_lbl_deposits_loaded));
				return;
			}

			lbl_deposits_loaded.Text = string.Format("{0:n0}", Globals.batch_count);
		}

		/// <summary>
		/// update the status for the number of valid transactions
		/// </summary>
		private void display_lbl_valid_transactions()
		{
			if (InvokeRequired)
            {
				BeginInvoke(new MethodInvoker(display_lbl_valid_transactions));
				return;
			}

			lbl_valid_transactions.Text = string.Format("{0:n0}", Globals.ValidTransactions);
		}

		/// <summary>
		/// Set the hash total to indicate that it is wrong
		/// </summary>
		private void display_lbl_hash_total_alert()
		{
			if (InvokeRequired)
            {
				BeginInvoke(new MethodInvoker(display_lbl_hash_total_alert));
				return;
			}

            lbl_hash_total.Font      = new Font(lbl_hash_total.Font, FontStyle.Bold);
            lbl_hash_total.ForeColor = Color.White;
            lbl_hash_total.BackColor = Color.Red;
		}

		/// <summary>
		/// Set the hash total to indicate that it is valid
		/// </summary>
		private void display_lbl_hash_total_normal()
		{
			if (InvokeRequired)
            {
				BeginInvoke(new MethodInvoker(display_lbl_hash_total_normal));
				return;
			}

            lbl_hash_total.Font      = Label5.Font;
            lbl_hash_total.ForeColor = Label5.ForeColor;
            lbl_hash_total.BackColor = Label5.BackColor;
		}

		/// <summary>
		/// Read the deposit batch details
		/// </summary>
        private void bt_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // Clear the counts
                Globals.batch_hash_total  = -1m;
                Globals.batch_amount      = 0m;
                Globals.batch_count       = 0;
                Globals.deposit_batch_id  = -1;
                Globals.ValidTransactions = 0;

                // Default values for the transactions
                RecordValueType defaults = new RecordValueType();

                // Read the file
                using (var src_text = new StreamReader(Globals.fso))
                {
                    // Find the hash key if there is one
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        string source_line = src_text.ReadLine();
                        while (source_line != null)
                        {
                            // Determine if the KeyField line is valid
                            source_line = source_line.Trim();
                            if (source_line == string.Empty)
                            {
                                source_line = src_text.ReadLine();
                                continue;
                            }

                            // First, we need to find a match to the HASH key since it is usually a special client
                            // and all clients would be the same otherwise.
                            Match match_expression = default(Match);
                            System.Text.RegularExpressions.Regex hash_rx = Globals.lbd.parsing_data["HASH"] as Regex;
                            if (hash_rx != null)
                            {
                                match_expression = hash_rx.Match(source_line);
                                if (match_expression.Success)
                                {
                                    RecordValueType hash_figure = new RecordValueType(match_expression);
                                    Globals.batch_hash_total = hash_figure.transaction_amount;
                                    display_lbl_hash_total();

                                    source_line = src_text.ReadLine();
                                    continue;
                                }
                            }

                            // Look for the non-hash figures for all keys and take the first match.
                            foreach (string key in Globals.lbd.parsing_data.Keys)
                            {
                                // Ignore the HASH record since it was process earlier.
                                if (key.CompareTo("HASH") == 0)
                                {
                                    continue;
                                }

                                Regex rx = Globals.lbd.parsing_data[key] as Regex;
                                match_expression = rx.Match(source_line);
                                if (! match_expression.Success)
                                {
                                    continue;
                                }

                                RecordValueType values = new RecordValueType(match_expression);

                                // If this is the header then save the defaults for the transactions.
                                if (key.CompareTo("HEAD") == 0)
                                {
                                    defaults = values;
                                    break;
                                }

                                // Update the statistics
                                Globals.batch_amount += values.transaction_amount;
                                display_lbl_transaction_dollars();

                                // Count the number of lines read
                                Globals.batch_count += 1;
                                display_lbl_deposits_loaded();

                                // If we don't have a deposit batch then create one.
                                if (Globals.deposit_batch_id <= 0)
                                {
                                    Globals.deposit_batch_id = create_LB_deposit_batch(cn, Globals.deposit_batch_label, Globals.bank);
                                }

                                // Record the transaction
                                try
                                {
                                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                                    {
                                        using (var cmd = new SqlCommand())
                                        {
                                            cmd.Connection = cn;
                                            cmd.CommandText = InsertCmdString;
                                            cmd.CommandType = CommandType.StoredProcedure;
                                            cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                                            cmd.Parameters.Add("@batch", SqlDbType.Int).Value = Globals.deposit_batch_id;
                                            cmd.Parameters.Add("@scanned_client", SqlDbType.VarChar, 20).Value = DebtPlus.Utils.Nulls.ToDbNull(values.deposit_client);
                                            cmd.Parameters.Add("@scanned_date", SqlDbType.DateTime).Value = values.transaction_date.GetValueOrDefault(defaults.transaction_date.GetValueOrDefault(DateTime.Now.Date));
                                            cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = values.transaction_amount;
                                            cmd.Parameters.Add("@reference", SqlDbType.VarChar, 50).Value = values.transaction_reference.Length > 0 ? (object) values.transaction_reference : DebtPlus.Utils.Nulls.ToDbNull(defaults.transaction_reference);
                                            cmd.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 50).Value = DebtPlus.Utils.Nulls.ToDbNull(values.message);
                                            cmd.Parameters.Add("@tran_type", SqlDbType.VarChar, 10).Value = key;
                                            cmd.ExecuteNonQuery();

                                            if (Convert.ToInt32(cmd.Parameters[0].Value) > 0)
                                            {
                                                Globals.ValidTransactions += 1;
                                                display_lbl_valid_transactions();
                                            }
                                        }
                                    }
                                }

                                catch (SqlException ex)
                                {
                                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording deposit details");
                                }
                                break;
                            }

                            // Get the next line from the source file.
                            source_line = src_text.ReadLine();
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing deposit records");
            }

            // If there is a batch total and the amount is different from the current values then make it bold/red
            if (Globals.batch_hash_total >= 0)
            {
                if (Globals.batch_hash_total != Globals.batch_amount)
                {
                    display_lbl_hash_total_alert();
                }
                else
                {
                    display_lbl_hash_total_normal();
                }
            }
        }

		/// <summary>
		/// Create the deposit batch
		/// </summary>
		private Int32 create_LB_deposit_batch(SqlConnection cn)
		{
            return create_LB_deposit_batch(cn, string.Empty, -1);
		}

		private Int32 create_LB_deposit_batch(SqlConnection cn, string note)
		{
		    return create_LB_deposit_batch(cn, note, -1);
        }

		private Int32 create_LB_deposit_batch(SqlConnection cn, Int32 bank)
		{
    		return create_LB_deposit_batch(cn, string.Empty, bank);
        }

		private Int32 create_LB_deposit_batch(SqlConnection cn, string note, Int32 bank)
		{
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.CommandText = CreateCmdString;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                // Parameters for the request
                cmd.Parameters.Add("@note", SqlDbType.VarChar, 256).Value = note;
                cmd.Parameters.Add("@batch_type", SqlDbType.VarChar, 2).Value = "CL";
                cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank;

                // Create the batch
                cmd.ExecuteNonQuery();

                // Return the new batch value
                return Convert.ToInt32(cmd.Parameters[0].Value);
            }
		}
	}
}
