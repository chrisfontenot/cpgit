#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	internal class Page_Template : XtraUserControl
	{
		protected LockboxGlobals Globals
        {
			get
            {
                return ((ISupportContext)ParentForm).Globals;
            }
		}

		// Event to handle the button click operation on the control
		public event ButtonClickedEventHandler ButtonClicked;
		public delegate void ButtonClickedEventHandler(object sender, string ButtonValue);

		public Page_Template() : base()
		{
			InitializeComponent();
            Button_Next.Click += Button_Click;
            Button_Prev.Click += Button_Click;
        }

		#region " Windows Form Designer generated code "

		//UserControl overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing)
            {
				if (components != null)
                {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

		protected DevExpress.XtraEditors.SimpleButton Button_Next;
		protected DevExpress.XtraEditors.SimpleButton Button_Prev;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			this.Button_Next = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Prev = new DevExpress.XtraEditors.SimpleButton();
			this.SuspendLayout();
			//
			//Button_Next
			//
			this.Button_Next.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Next.Location = new System.Drawing.Point(187, 243);
			this.Button_Next.Name = "Button_Next";
			this.Button_Next.Size = new System.Drawing.Size(75, 27);
			this.Button_Next.TabIndex = 17;
			this.Button_Next.Text = "Next >";
			this.Button_Next.ToolTip = "Go to the next step";
			//
			//Button_Prev
			//
			this.Button_Prev.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Prev.Location = new System.Drawing.Point(91, 243);
			this.Button_Prev.Name = "Button_Prev";
			this.Button_Prev.Size = new System.Drawing.Size(75, 27);
			this.Button_Prev.TabIndex = 16;
			this.Button_Prev.Text = "< Prev";
			this.Button_Prev.ToolTip = "Go back to the previous step";
			//
			//Page_Template
			//
			this.Controls.Add(this.Button_Next);
			this.Controls.Add(this.Button_Prev);
			this.Name = "Page_Template";
			this.Size = new System.Drawing.Size(352, 286);
			this.ResumeLayout(false);
		}

		#endregion

		/// <summary>
		/// Process the load event for the page. Initialize the display.
		/// </summary>
		internal virtual void PageLoad()
		{
		}

		/// <summary>
		/// Handle the button clicks
		/// </summary>
		protected virtual void Button_Click(object sender, EventArgs e)
		{
			if (ButtonClicked != null)
            {
				ButtonClicked(this, Convert.ToString(((SimpleButton) sender).Tag));
			}
		}
	}
}
