#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.Deposit.LockBox
{
	public class Mainline : IDesktopMainline
	{
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                // We need the list of strings from the argument list.
                var argList = (string[]) param;
                try
                {
                    var Globals = new LockboxGlobals();

                    // If there is a parameter name then it is the name of the config section that we want to use.
                    string firstArgument = argList != null && argList.GetUpperBound(0) >= 0 ? argList[0] : null;
                    if (firstArgument != null)
                    {
                        string SectionTemplate = DebtPlus.Configuration.Config.GetConfigSettingsName(DebtPlus.Configuration.Config.ConfigSet.DepositLockbox);
                        string SectionName = string.Format(SectionTemplate, firstArgument);
                        System.Collections.Specialized.NameValueCollection NameCollection = DebtPlus.Configuration.Config.GetConfigSettings(SectionName);

                        // Create the parameter pair information from the config section.
                        Globals.lbd = new Lockbox(NameCollection);
                    }
                    else
                    {
                        // Punt if there is no config section named.
                        Globals.lbd = new Lockbox();
                    }

                    // Display the outer form dialog until it is closed.
                    var frm = new Form_Lockbox(ref Globals);
                    System.Windows.Forms.Application.Run(frm);
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "DepositLockBox"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
	}
}
