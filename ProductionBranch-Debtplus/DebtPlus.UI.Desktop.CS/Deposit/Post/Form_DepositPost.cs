#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Deposit.Post
{
    internal partial class Form_DepositPost : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        internal DepositPostArgParser ap;

        public Form_DepositPost(DepositPostArgParser ap) : this()
        {
            this.ap = ap;
            this.Load += Form_DepositPost_Load;
            PostControl1.Cancelled += PostControl1_Cancelled;
        }

        #region " Windows Form Designer generated code "

        public Form_DepositPost() : base()
        {
            //This call is required by the Windows Form Designer.
            InitializeComponent();

            //Add any initialization after the InitializeComponent() call
        }

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal PostControl PostControl1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.PostControl1 = new PostControl();

            this.SuspendLayout();

            //
            //PostControl1
            //
            this.PostControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PostControl1.DockPadding.All = 4;
            this.PostControl1.Location = new System.Drawing.Point(0, 0);
            this.PostControl1.Name = "PostControl1";
            this.PostControl1.Size = new System.Drawing.Size(560, 294);
            this.ToolTipController1.SetSuperTip(this.PostControl1, null);
            this.PostControl1.TabIndex = 0;
            //
            //Form_DepositPost
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(560, 294);
            this.Controls.Add(this.PostControl1);
            this.Name = "Form_DepositPost";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Post Deposit Batch";

            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void Form_DepositPost_Load(object sender, System.EventArgs e)
        {
            PostControl1.ReadForm();
        }

        private void PostControl1_Cancelled(object sender, System.EventArgs e)
        {
            Close();
        }
    }
}