﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    // Event arguments for the search function
    public class SearchEventArgs : System.EventArgs
    {
        public SearchEventArgs()
            : base()
        {
            clientID = new Int32?();
            referralSource = new Int32?();
            methodFirstContact = new Int32?();
            gender = new Int32?();
            language = new Int32?();
            InitialProgram = new Int32?();

            existingClient = false;
            bankruptcyIssues = false;
            housingIssues = false;
            immediateAppointment = false;

            partnerCode = null;
            applicant_name = null;
            email = null;
            address = null;
            clientAppointment = null;
            homePhone = null;
            cellTelephone = null;
            workTelephone = null;
            inboundTelephoneNumber = null;
        }

        // Current client information
        public Int32? clientID { get; set; }
        public bool existingClient { get; set; }

        // Items to find the client
        public bool bankruptcyIssues { get; set; }
        public bool housingIssues { get; set; }
        public Int32? referralSource { get; set; }
        public Int32? InitialProgram { get; set; }
        public Int32? methodFirstContact { get; set; }
        public TelephoneNumber homePhone { get; set; }

        // Information about the client
        public string partnerCode { get; set; }
        public Int32? gender { get; set; }
        public Int32? language { get; set; }
        public DebtPlus.LINQ.Name applicant_name { get; set; }
        public DebtPlus.LINQ.EmailAddress email { get; set; }
        public DebtPlus.LINQ.TelephoneNumber cellTelephone { get; set; }
        public DebtPlus.LINQ.TelephoneNumber workTelephone { get; set; }
        public DebtPlus.LINQ.address address { get; set; }
        public DebtPlus.LINQ.InboundTelephoneNumber inboundTelephoneNumber { get; set; }

        // Information about the appointment
        public DebtPlus.LINQ.client_appointment clientAppointment { get; set; }
        public bool immediateAppointment { get; set; }
    }

    // Delegate for the events
    public delegate void SearchEventHandler(object Sender, SearchEventArgs e);
}
