﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing
{
    partial class FormCreateClient_FHLB_Base
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                components = null;
                bc = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.SearchClientFHLB1 = new DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing.SearchClientFHLB();
            this.selectionControl1 = new DebtPlus.UI.Desktop.CS.Client.Create.selectionControl();
            this.fhlbDetailControl1 = new DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing.FHLBDetailControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // SearchClientFHLB1
            // 
            this.SearchClientFHLB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchClientFHLB1.Location = new System.Drawing.Point(0, 0);
            this.SearchClientFHLB1.Name = "SearchClientFHLB1";
            this.SearchClientFHLB1.Size = new System.Drawing.Size(748, 264);
            this.SearchClientFHLB1.TabIndex = 0;
            // 
            // selectionControl1
            // 
            this.selectionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectionControl1.Location = new System.Drawing.Point(0, 0);
            this.selectionControl1.Name = "selectionControl1";
            this.selectionControl1.Size = new System.Drawing.Size(748, 264);
            this.selectionControl1.TabIndex = 1;
            this.selectionControl1.Visible = false;
            // 
            // fhlbDetailControl1
            // 
            this.fhlbDetailControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fhlbDetailControl1.Location = new System.Drawing.Point(0, 0);
            this.fhlbDetailControl1.Name = "fhlbDetailControl1";
            this.fhlbDetailControl1.Size = new System.Drawing.Size(748, 264);
            this.fhlbDetailControl1.TabIndex = 2;
            // 
            // FormCreateClient_FHLB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 264);
            this.Controls.Add(this.SearchClientFHLB1);
            this.Controls.Add(this.selectionControl1);
            this.Controls.Add(this.fhlbDetailControl1);
            this.MinimumSize = new System.Drawing.Size(764, 302);
            this.Name = "FormCreateClient_FHLB";
            this.Text = "";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);
        }

        public DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing.SearchClientFHLB SearchClientFHLB1;
        public DebtPlus.UI.Desktop.CS.Client.Create.selectionControl selectionControl1;
        public FHLBDetailControl fhlbDetailControl1;
    }
}
