﻿namespace DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing
{
    partial class FHLBDetailControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.lookUpEdit_referred_by = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_language = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_gender = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.telephoneNumberRecordControl_evening = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_day = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_cell = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControlFHLBReservation = new DevExpress.XtraEditors.LabelControl();
            this.textEdit_partner_code = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton_ScheduledAppt = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_EditClient = new DevExpress.XtraEditors.SimpleButton();
            this.emailRecordControl_email = new DebtPlus.Data.Controls.EmailRecordControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.nameRecordControl_name = new DebtPlus.Data.Controls.NameRecordControl();
            this.addressRecordControl1 = new DebtPlus.Data.Controls.AddressRecordControl();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_referred_by.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_language.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_gender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_evening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_day)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_cell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_partner_code.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(444, 224);
            this.simpleButton_Cancel.TabIndex = 22;
            // 
            // lookUpEdit_referred_by
            // 
            this.lookUpEdit_referred_by.Location = new System.Drawing.Point(97, 3);
            this.lookUpEdit_referred_by.Name = "lookUpEdit_referred_by";
            this.lookUpEdit_referred_by.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_referred_by.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_referred_by.Properties.DisplayMember = "description";
            this.lookUpEdit_referred_by.Properties.NullText = "";
            this.lookUpEdit_referred_by.Properties.ShowFooter = false;
            this.lookUpEdit_referred_by.Properties.ShowHeader = false;
            this.lookUpEdit_referred_by.Properties.SortColumnIndex = 1;
            this.lookUpEdit_referred_by.Properties.ValueMember = "Id";
            this.lookUpEdit_referred_by.Size = new System.Drawing.Size(404, 20);
            this.lookUpEdit_referred_by.TabIndex = 1;
            // 
            // lookUpEdit_language
            // 
            this.lookUpEdit_language.Location = new System.Drawing.Point(327, 82);
            this.lookUpEdit_language.Name = "lookUpEdit_language";
            this.lookUpEdit_language.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_language.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_language.Properties.DisplayMember = "Attribute";
            this.lookUpEdit_language.Properties.NullText = "";
            this.lookUpEdit_language.Properties.ShowFooter = false;
            this.lookUpEdit_language.Properties.ShowHeader = false;
            this.lookUpEdit_language.Properties.ValueMember = "Id";
            this.lookUpEdit_language.Size = new System.Drawing.Size(174, 20);
            this.lookUpEdit_language.TabIndex = 9;
            // 
            // lookUpEdit_gender
            // 
            this.lookUpEdit_gender.Location = new System.Drawing.Point(97, 81);
            this.lookUpEdit_gender.Name = "lookUpEdit_gender";
            this.lookUpEdit_gender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_gender.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_gender.Properties.DisplayMember = "description";
            this.lookUpEdit_gender.Properties.NullText = "";
            this.lookUpEdit_gender.Properties.ShowFooter = false;
            this.lookUpEdit_gender.Properties.ShowHeader = false;
            this.lookUpEdit_gender.Properties.ValueMember = "Id";
            this.lookUpEdit_gender.Size = new System.Drawing.Size(150, 20);
            this.lookUpEdit_gender.TabIndex = 7;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Referral";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(271, 86);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(47, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Language";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(4, 86);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(35, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Gender";
            // 
            // telephoneNumberRecordControl_evening
            // 
            this.telephoneNumberRecordControl_evening.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.telephoneNumberRecordControl_evening.ErrorIcon = null;
            this.telephoneNumberRecordControl_evening.ErrorText = "";
            this.telephoneNumberRecordControl_evening.Location = new System.Drawing.Point(544, 109);
            this.telephoneNumberRecordControl_evening.Name = "telephoneNumberRecordControl_evening";
            this.telephoneNumberRecordControl_evening.Size = new System.Drawing.Size(191, 20);
            this.telephoneNumberRecordControl_evening.TabIndex = 17;
            // 
            // telephoneNumberRecordControl_day
            // 
            this.telephoneNumberRecordControl_day.ErrorIcon = null;
            this.telephoneNumberRecordControl_day.ErrorText = "";
            this.telephoneNumberRecordControl_day.Location = new System.Drawing.Point(327, 108);
            this.telephoneNumberRecordControl_day.Name = "telephoneNumberRecordControl_day";
            this.telephoneNumberRecordControl_day.Size = new System.Drawing.Size(174, 20);
            this.telephoneNumberRecordControl_day.TabIndex = 15;
            // 
            // telephoneNumberRecordControl_cell
            // 
            this.telephoneNumberRecordControl_cell.ErrorIcon = null;
            this.telephoneNumberRecordControl_cell.ErrorText = "";
            this.telephoneNumberRecordControl_cell.Location = new System.Drawing.Point(97, 107);
            this.telephoneNumberRecordControl_cell.Name = "telephoneNumberRecordControl_cell";
            this.telephoneNumberRecordControl_cell.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_cell.TabIndex = 13;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(4, 112);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(17, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Cell";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(271, 112);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(25, 13);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Work";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Location = new System.Drawing.Point(509, 112);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(27, 13);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "Home";
            // 
            // labelControlFHLBReservation
            // 
            this.labelControlFHLBReservation.Location = new System.Drawing.Point(4, 33);
            this.labelControlFHLBReservation.Name = "labelControlFHLBReservation";
            this.labelControlFHLBReservation.Size = new System.Drawing.Size(85, 13);
            this.labelControlFHLBReservation.TabIndex = 2;
            this.labelControlFHLBReservation.Text = "FHLB Reservation";
            // 
            // textEdit_partner_code
            // 
            this.textEdit_partner_code.Location = new System.Drawing.Point(97, 29);
            this.textEdit_partner_code.Name = "textEdit_partner_code";
            this.textEdit_partner_code.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_partner_code.Size = new System.Drawing.Size(131, 20);
            this.textEdit_partner_code.TabIndex = 3;
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Location = new System.Drawing.Point(508, 86);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(28, 13);
            this.labelControl8.TabIndex = 10;
            this.labelControl8.Text = "E-Mail";
            // 
            // simpleButton_ScheduledAppt
            // 
            this.simpleButton_ScheduledAppt.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_ScheduledAppt.Enabled = false;
            this.simpleButton_ScheduledAppt.Location = new System.Drawing.Point(335, 224);
            this.simpleButton_ScheduledAppt.Name = "simpleButton_ScheduledAppt";
            this.simpleButton_ScheduledAppt.Size = new System.Drawing.Size(103, 23);
            this.simpleButton_ScheduledAppt.TabIndex = 21;
            this.simpleButton_ScheduledAppt.Text = "Schedule Appt";
            // 
            // simpleButton_EditClient
            // 
            this.simpleButton_EditClient.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_EditClient.Enabled = false;
            this.simpleButton_EditClient.Location = new System.Drawing.Point(226, 224);
            this.simpleButton_EditClient.Name = "simpleButton_EditClient";
            this.simpleButton_EditClient.Size = new System.Drawing.Size(103, 23);
            this.simpleButton_EditClient.TabIndex = 20;
            this.simpleButton_EditClient.Text = "Edit Client";
            // 
            // emailRecordControl_email
            // 
            this.emailRecordControl_email.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.emailRecordControl_email.ClickedColor = System.Drawing.Color.Maroon;
            this.emailRecordControl_email.Location = new System.Drawing.Point(544, 83);
            this.emailRecordControl_email.Name = "emailRecordControl_email";
            this.emailRecordControl_email.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.emailRecordControl_email.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.emailRecordControl_email.Properties.Appearance.Options.UseFont = true;
            this.emailRecordControl_email.Properties.Appearance.Options.UseForeColor = true;
            this.emailRecordControl_email.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emailRecordControl_email.Size = new System.Drawing.Size(191, 20);
            this.emailRecordControl_email.TabIndex = 11;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(4, 134);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 13);
            this.labelControl9.TabIndex = 18;
            this.labelControl9.Text = "Address";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(4, 59);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(27, 13);
            this.labelControl10.TabIndex = 4;
            this.labelControl10.Text = "Name";
            // 
            // nameRecordControl_name
            // 
            this.nameRecordControl_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameRecordControl_name.Location = new System.Drawing.Point(97, 55);
            this.nameRecordControl_name.Name = "nameRecordControl_name";
            this.nameRecordControl_name.Size = new System.Drawing.Size(638, 20);
            this.nameRecordControl_name.TabIndex = 5;
            // 
            // addressRecordControl1
            // 
            this.addressRecordControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addressRecordControl1.Location = new System.Drawing.Point(97, 134);
            this.addressRecordControl1.Name = "addressRecordControl1";
            this.addressRecordControl1.Size = new System.Drawing.Size(638, 73);
            this.addressRecordControl1.TabIndex = 19;
            // 
            // FHLBDetailControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nameRecordControl_name);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.emailRecordControl_email);
            this.Controls.Add(this.simpleButton_EditClient);
            this.Controls.Add(this.simpleButton_ScheduledAppt);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.textEdit_partner_code);
            this.Controls.Add(this.labelControlFHLBReservation);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.telephoneNumberRecordControl_cell);
            this.Controls.Add(this.telephoneNumberRecordControl_day);
            this.Controls.Add(this.telephoneNumberRecordControl_evening);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lookUpEdit_gender);
            this.Controls.Add(this.lookUpEdit_language);
            this.Controls.Add(this.lookUpEdit_referred_by);
            this.Controls.Add(this.addressRecordControl1);
            this.Name = "FHLBDetailControl";
            this.Size = new System.Drawing.Size(745, 262);
            this.Controls.SetChildIndex(this.addressRecordControl1, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_referred_by, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_language, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_gender, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.telephoneNumberRecordControl_evening, 0);
            this.Controls.SetChildIndex(this.telephoneNumberRecordControl_day, 0);
            this.Controls.SetChildIndex(this.telephoneNumberRecordControl_cell, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControlFHLBReservation, 0);
            this.Controls.SetChildIndex(this.textEdit_partner_code, 0);
            this.Controls.SetChildIndex(this.labelControl8, 0);
            this.Controls.SetChildIndex(this.simpleButton_ScheduledAppt, 0);
            this.Controls.SetChildIndex(this.simpleButton_EditClient, 0);
            this.Controls.SetChildIndex(this.emailRecordControl_email, 0);
            this.Controls.SetChildIndex(this.labelControl9, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.labelControl10, 0);
            this.Controls.SetChildIndex(this.nameRecordControl_name, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_referred_by.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_language.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_gender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_evening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_day)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_cell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_partner_code.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.LabelControl labelControl3;
        public DevExpress.XtraEditors.LabelControl labelControl4;
        public DevExpress.XtraEditors.LabelControl labelControl5;
        public DevExpress.XtraEditors.LabelControl labelControl6;
        public DevExpress.XtraEditors.LabelControl labelControlFHLBReservation;
        public DevExpress.XtraEditors.LabelControl labelControl8;
        public DevExpress.XtraEditors.LabelControl labelControl9;
        public DevExpress.XtraEditors.LabelControl labelControl10;
        public DevExpress.XtraEditors.SimpleButton simpleButton_ScheduledAppt;
        public DevExpress.XtraEditors.SimpleButton simpleButton_EditClient;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_referred_by;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_language;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_gender;
        public Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_evening;
        public Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_day;
        public Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_cell;
        public DevExpress.XtraEditors.TextEdit textEdit_partner_code;
        public Data.Controls.EmailRecordControl emailRecordControl_email;
        public Data.Controls.NameRecordControl nameRecordControl_name;
        public Data.Controls.AddressRecordControl addressRecordControl1;
    }
}
