#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing
{
    internal partial class FormCreateClient_FHLB_Base : DebtPlus.Data.Forms.DebtPlusForm
    {
        BusinessContext bc = null;

        // Current search event arguments and resulting client
        public SearchEventArgs ClientParameters { get; internal set; }

        /// <summary>
        /// Client Type for the creation routine
        /// </summary>
        internal enum ClientType
        {
            CLIENT_TYPE_UNKNOWN = 0,
            CLIENT_TYPE_ABT = 1,
            CLIENT_TYPE_FHLB = 2
        };

        /// <summary>
        /// Type of the created client
        /// </summary>
        public ClientType CreatedClientType { get; internal set; }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        public FormCreateClient_FHLB_Base()
            : base()
        {
            CreatedClientType = ClientType.CLIENT_TYPE_UNKNOWN;
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                    += Form_CreateClient_Load;
            SearchClientFHLB1.Cancelled             += SearchClientFHLB1_Cancelled;
            selectionControl1.Cancelled             += selectionControl1_Cancelled;
            SearchClientFHLB1.Search                += SearchClientFHLB1_Search;
            selectionControl1.ClientFound           += selectionControl1_ClientFound;
            fhlbDetailControl1.Cancelled            += fhlbDetailControl1_Cancelled;
            fhlbDetailControl1.EditClient           += fhlbDetailControl1_EditClient;
            fhlbDetailControl1.ScheduledAppointment += fhlbDetailControl1_ScheduledAppointment;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                    -= Form_CreateClient_Load;
            SearchClientFHLB1.Cancelled             -= SearchClientFHLB1_Cancelled;
            selectionControl1.Cancelled             -= selectionControl1_Cancelled;
            SearchClientFHLB1.Search                -= SearchClientFHLB1_Search;
            selectionControl1.ClientFound           -= selectionControl1_ClientFound;
            fhlbDetailControl1.Cancelled            -= new EventHandler(fhlbDetailControl1_Cancelled);
            fhlbDetailControl1.EditClient           -= fhlbDetailControl1_EditClient;
            fhlbDetailControl1.ScheduledAppointment -= fhlbDetailControl1_ScheduledAppointment;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void Form_CreateClient_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Allocate a new business context for the form to use.
                bc = new BusinessContext();

                // Load the placement and hook into the save logic.
                LoadPlacement("Create_FHLB_Housing_Client");

                // Ensure that the client type is specified
                Debug.Assert(CreatedClientType != ClientType.CLIENT_TYPE_UNKNOWN);

                // Go to the search panel
                SwitchToSearch();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// The user canceled the selection form. This is terminal to the form processing.
        /// </summary>
        private void SearchClientFHLB1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// The user canceled the selection form. Go back to the search control.
        /// </summary>
        private void selectionControl1_Cancelled(object sender, EventArgs e)
        {
            SwitchToSearch();
        }

        /// <summary>
        /// Switch the form to the KeyField processing
        /// </summary>
        private void SwitchToSearch()
        {
            // Send the duplicate control to the rear
            this.selectionControl1.Visible = false;
            this.selectionControl1.SendToBack();

            // Send the detail control to the rear
            this.fhlbDetailControl1.Visible = false;
            this.fhlbDetailControl1.SendToBack();

            // Bring the selection control to the front
            this.SearchClientFHLB1.BringToFront();
            this.SearchClientFHLB1.Visible = true;
            this.SearchClientFHLB1.Focus();
        }

        /// <summary>
        /// Switch the form to the selection processing
        /// </summary>
        private void SwitchToSelect()
        {
            // Send the search control to the rear
            this.SearchClientFHLB1.Visible = false;
            this.SearchClientFHLB1.SendToBack();

            // Send the detail control to the rear
            this.fhlbDetailControl1.Visible = false;
            this.fhlbDetailControl1.SendToBack();

            // Bring the select control to the front
            this.selectionControl1.BringToFront();
            this.selectionControl1.Visible = true;
            this.selectionControl1.Focus();
        }

        /// <summary>
        /// Switch the form to the selection processing
        /// </summary>
        private void SwitchToDetail()
        {
            // Send the search control to the rear
            this.SearchClientFHLB1.Visible = false;
            this.SearchClientFHLB1.SendToBack();

            // Send the select control to the rear
            this.selectionControl1.Visible = false;
            this.selectionControl1.SendToBack();

            // Bring the detail control to the front
            this.fhlbDetailControl1.BringToFront();
            this.fhlbDetailControl1.Visible = true;
            this.fhlbDetailControl1.Focus();
        }

        /// <summary>
        /// Do the client search operation. Create the client if indicated.
        /// </summary>
        private void SearchClientFHLB1_Search(object Sender, SearchEventArgs e)
        {
            try
            {
                // Save the search for the result function
                e.existingClient = false;
                ClientParameters = e;

                // If the telephone number is not desired then go to the detail information.
                if (e.homePhone == null)
                {
                    SwitchToDetail();
                    fhlbDetailControl1.SetCurrentValues(e);
                    return;
                }

                // Look for the client in the list of telephone numbers
                System.Collections.Generic.List<DebtPlus.LINQ.xpr_client_create_searchResult> colAnswers = ClientSearch.PerformSearch(e);
                if (colAnswers == null)
                {
                    return;
                }

                // If there are no clients then go ahead and create the client
                if (colAnswers.Count == 0)
                {
                    SwitchToDetail();
                    fhlbDetailControl1.SetCurrentValues(e);
                    return;
                }

                // If there is exactly one client then ask if this is the client
                if (colAnswers.Count == 1)
                {
                    switch (DebtPlus.Data.Forms.MessageBox.Show(string.Format("Is the client named '{0}'?\r\n\r\nThere is only one client currently in the database with\r\nthis telephone number. If this is the client then we don't\r\nwant to create a duplicate entry.", colAnswers[0].name), "Duplicate Telephone Number", System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button3))
                    {
                        case System.Windows.Forms.DialogResult.Yes:
                            e.clientID = colAnswers[0].clientID;
                            e.existingClient = true;
                            ExistingClient();
                            return;

                        case System.Windows.Forms.DialogResult.No:
                            SwitchToDetail();
                            fhlbDetailControl1.SetCurrentValues(e);
                            return;

                        case System.Windows.Forms.DialogResult.Cancel:
                            break;
                    }
                    return;
                }

                // There are more than one. Ask the user for the proper selection now.
                // The control will take it from this point until an item is found.
                SwitchToSelect();
                selectionControl1.SetCurrentValues(e, colAnswers);
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error searching for client");
            }
        }

        /// <summary>
        /// Process the completion of the selection operation on a list of possible client matches.
        /// </summary>
        private void selectionControl1_ClientFound(object sender, SearchEventArgs e)
        {
            ClientParameters = e;

            // Handle the condition where the client was located in the list.
            if (ClientParameters.existingClient)
            {
                ExistingClient();
            }
            else
            {
                // Otherwise, the person checked the box "not in list"
                SwitchToDetail();
                fhlbDetailControl1.SetCurrentValues(e);
            }
        }

        /// <summary>
        /// The detail form was canceled. Go back to the KeyField.
        /// </summary>
        private void fhlbDetailControl1_Cancelled(object sender, EventArgs e)
        {
            SwitchToSearch();
        }

        /// <summary>
        /// Handle the condition where the client exists.
        /// </summary>
        private void ExistingClient()
        {
            switch (DebtPlus.Data.Forms.MessageBox.Show("Do you wish to schedule an appointment for the client?", "Existing Client", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button1))
            {
                case System.Windows.Forms.DialogResult.Yes:
                    CreateScheduledAppointment();
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// Process the Immediate Appointment button for the detail
        /// </summary>
        private void fhlbDetailControl1_EditClient(object sender, EventArgs e)
        {
            ClientParameters = fhlbDetailControl1.GetCurrentValues();
            CreateClient(ClientParameters);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Process the scheduled appointment value for the detail
        /// </summary>
        private void fhlbDetailControl1_ScheduledAppointment(object sender, EventArgs e)
        {
            ClientParameters = fhlbDetailControl1.GetCurrentValues();
            CreateClient(ClientParameters);
            CreateScheduledAppointment();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Create the immediate appointment for the client
        /// </summary>
        private void CreateImmediateAppointment()
        {
        }

        /// <summary>
        /// Create the scheduled appointment for the client
        /// </summary>
        private void CreateScheduledAppointment()
        {
            ClientParameters.immediateAppointment = false;
            using (var frm = new FormCreateAppointment(bc, ClientParameters))
            {
                frm.ShowDialog();
            }

            try
            {
                // Ensure that the changes are submitted to the database
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected DebtPlus.LINQ.client getClient(DebtPlus.LINQ.BusinessContext bc, SearchEventArgs searchParams)
        {
            // If the client is not defined then define it.
            if (searchParams.clientID.GetValueOrDefault(0) < 1)
            {
                searchParams.clientID = bc.xpr_create_client(new Int32?(), searchParams.housingIssues, searchParams.referralSource, searchParams.methodFirstContact, false, 2);
                Debug.Assert(searchParams.clientID.HasValue && searchParams.clientID.Value > 0);

                // Create the FHLB creditor if this is a FHLB client
                if (CreatedClientType == ClientType.CLIENT_TYPE_FHLB)
                {
                    try
                    {
                        // Create a debt for this client if needed
                        bc.xpr_create_client_fhlb(searchParams.clientID.GetValueOrDefault(), searchParams.partnerCode);
                    }
#pragma warning disable 168
                    catch (System.Data.SqlClient.SqlException ex) { }   // Ignore the fact that the stored procedure may not be there.
#pragma warning restore 168
                }
            }

            // Return the client record
            return bc.clients.Where(s => s.Id == searchParams.clientID).FirstOrDefault();
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected DebtPlus.LINQ.people getApplicant(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var q = bc.peoples.Where(s => s.Client == ClientID && s.Relation == DebtPlus.LINQ.Cache.RelationType.Self).FirstOrDefault();
            Debug.Assert(q != null, "applicants are created by xpr_create_client. can not find the applicant.");

            if (q == null)
            {
                q          = DebtPlus.LINQ.Factory.Manufacture_people(ClientID);
                q.Relation = DebtPlus.LINQ.Cache.RelationType.Self;

                bc.peoples.InsertOnSubmit(q);
            }
            return q;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetName(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.Name name)
        {
            if (name == null || name.IsEmpty())
            {
                return new Int32?();
            }

            bc.Names.InsertOnSubmit(name);
            bc.SubmitChanges();
            return name.Id;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetEmail(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.EmailAddress email)
        {
            if (email == null || email.IsEmpty())
            {
                return new Int32?();
            }

            bc.EmailAddresses.InsertOnSubmit(email);
            bc.SubmitChanges();
            return email.Id;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetAddress(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.address address)
        {
            if (address == null || address.IsEmpty())
            {
                return new Int32?();
            }

            bc.addresses.InsertOnSubmit(address);
            bc.SubmitChanges();
            return address.Id;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetTelephone(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.TelephoneNumber phoneNumber)
        {
            if (phoneNumber == null || phoneNumber.IsEmpty())
            {
                return new Int32?();
            }

            bc.TelephoneNumbers.InsertOnSubmit(phoneNumber);
            bc.SubmitChanges();
            return phoneNumber.Id;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected virtual void CreateClient(SearchEventArgs searchParams)
        {
            // Allocate a transaction to hold the creation event. This is an all-or-nothing event.
            using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                cn.Open();
                using (var txn = cn.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    using (var bc = new BusinessContext(cn))
                    {
                        bc.Transaction = txn;

                        DebtPlus.LINQ.client clientRecord = getClient(bc, searchParams);
                        if (clientRecord == null)
                        {
                            txn.Rollback();
                            throw new ApplicationException("Client record could not be located nor created");
                        }

                        // Find the applicant record
                        DebtPlus.LINQ.people applicantRecord = getApplicant(bc, clientRecord.Id);
                        if (applicantRecord == null)
                        {
                            txn.Rollback();
                            throw new ApplicationException("Applicant record could not be located nor created");
                        }

                        using (var trans = new DebtPlus.Svc.DataSets.ValueDescripitions())
                        {
                            // First, set the items that we don't log as a change
                            clientRecord.AddressID       = GetAddress(bc, searchParams.address);
                            clientRecord.HomeTelephoneID = GetTelephone(bc, searchParams.homePhone);

                            // Supply the defaults for the items generated for a new client
                            clientRecord.ClearChangedFieldItems();

                            // Update the record with the items that are logged
                            clientRecord.referred_by          = searchParams.referralSource;
                            clientRecord.language             = searchParams.language;
                            clientRecord.method_first_contact = searchParams.methodFirstContact.GetValueOrDefault(0);
                            clientRecord.mortgage_problems    = searchParams.housingIssues;

                            // Generate the system note for the client changes
                            var sbSubject = new System.Text.StringBuilder();
                            var sbNote = new System.Text.StringBuilder();

                            foreach (ChangedFieldItem field in clientRecord.getChangedFieldItems)
                            {
                                sbNote.AppendFormat("\r\nChanged {0} to '{1}'", field.FieldName, trans.ClientStringValue(field.FieldName, trans.GetFieldType(clientRecord, field.FieldName), field.NewValue));
                                sbSubject.AppendFormat(",{0}", field.FieldName);
                            }

                            // If there is a cell telephone then we changed the cell telephone value
                            if (searchParams.homePhone != null)
                            {
                                sbSubject.Append(",HomePhone");
                                sbNote.AppendFormat("\r\nChanged HomePhone to '{0}'", searchParams.homePhone.ToString());
                            }

                            // If there is a referral source then we changed it too
                            if (searchParams.referralSource.HasValue)
                            {
                                sbSubject.Append(",referred_by");
                                sbNote.AppendFormat("\r\nChanged referred_by to '{0}'", trans.ClientStringValue("referred_by", trans.GetFieldType(clientRecord, "referred_by"), searchParams.referralSource));
                            }

                            // If there is an address then we changed it too
                            if (searchParams.address != null)
                            {
                                sbSubject.Append(",Address");
                                sbNote.AppendFormat("\r\nChanged Address to:\r\n\r\n'{0}'", searchParams.address.ToString());
                            }

                            // Toss the extra characters in the subject and notes
                            if (sbSubject.Length > 0)
                            {
                                // Do not allow the subject to be too long
                                sbSubject.Remove(0, 1);
                                sbSubject.Insert(0, "Changed ");
                                if (sbSubject.Length > 77)
                                {
                                    sbSubject.Remove(77, sbSubject.Length - 77);
                                    sbSubject.Append("...");
                                }

                                // Remove the leading cr/lf from the note text as well.
                                sbNote.Remove(0, 2);

                                // Generate the note and insert it into the pending updates
                                var newNote     = DebtPlus.LINQ.Factory.Manufacture_client_note(clientRecord.Id, 3);
                                newNote.subject = sbSubject.ToString();
                                newNote.note    = sbNote.ToString();

                                bc.client_notes.InsertOnSubmit(newNote);
                            }
                        }

                        // Generate the system note for the client changes
                        using (var trans = new DebtPlus.Svc.DataSets.ValueDescripitions())
                        {
                            // First, set the items that are not logged
                            applicantRecord.NameID          = GetName(bc, searchParams.applicant_name);
                            applicantRecord.EmailID         = GetEmail(bc, searchParams.email);
                            applicantRecord.WorkTelephoneID = GetTelephone(bc, searchParams.workTelephone);
                            applicantRecord.CellTelephoneID = GetTelephone(bc, searchParams.cellTelephone);

                            // Reset the field change tracking information
                            applicantRecord.ClearChangedFieldItems();

                            // Set the items that are logged
                            applicantRecord.Gender = searchParams.gender.GetValueOrDefault(1);

                            var sbSubject = new System.Text.StringBuilder();
                            var sbNote = new System.Text.StringBuilder();
                            foreach (ChangedFieldItem field in applicantRecord.getChangedFieldItems)
                            {
                                sbNote.AppendFormat("\r\nChanged {0} to '{1}'", field.FieldName, trans.ClientStringValue(field.FieldName, trans.GetFieldType(clientRecord, field.FieldName), field.NewValue));
                                sbSubject.AppendFormat(",{0}", field.FieldName);
                            }

                            // If there is a name then insert that
                            if (searchParams.applicant_name != null)
                            {
                                sbSubject.Append(",Name");
                                sbNote.AppendFormat("\r\nChanged Name to '{0}'", searchParams.applicant_name.ToString());
                            }

                            // work telephone number
                            if (searchParams.workTelephone != null)
                            {
                                sbSubject.Append(",Work Phone");
                                sbNote.AppendFormat("\r\nChanged Work Phone Number to '{0}'", searchParams.workTelephone.ToString());
                            }

                            // cell telephone number
                            if (searchParams.cellTelephone != null)
                            {
                                sbSubject.Append(",Cell Phone");
                                sbNote.AppendFormat("\r\nChanged Mobile Phone Number to '{0}'", searchParams.cellTelephone.ToString());
                            }

                            // email address
                            if (searchParams.email != null)
                            {
                                sbSubject.Append(",Email");
                                sbNote.AppendFormat("\r\nChanged Email to '{0}'", searchParams.email.ToString());
                            }

                            // Toss the extra characters in the subject and notes
                            if (sbSubject.Length > 0)
                            {
                                // Do not allow the subject to be too long
                                sbSubject.Remove(0, 1);
                                sbSubject.Insert(0, "Changed Applicant ");
                                if (sbSubject.Length > 77)
                                {
                                    sbSubject.Remove(77, sbSubject.Length - 77);
                                    sbSubject.Append("...");
                                }
                                sbNote.Remove(0, 2);

                                // Generate the note and insert it into the pending updates
                                var newNote     = DebtPlus.LINQ.Factory.Manufacture_client_note(clientRecord.Id, 3);
                                newNote.subject = sbSubject.ToString();
                                newNote.note    = sbNote.ToString();

                                bc.client_notes.InsertOnSubmit(newNote);
                            }
                        }

                        // Finally, submit the changes to the database to complete the client creation
                        bc.SubmitChanges();
                        txn.Commit();
                    }
                }
            }
        }
    }
}
