﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing
{
    public partial class FormCreateAppointment : DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Book
    {
        protected DebtPlus.UI.Desktop.CS.Client.Create.SearchEventArgs searchParams;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public FormCreateAppointment()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create the normal instance of the class
        /// </summary>
        public FormCreateAppointment(DebtPlus.LINQ.BusinessContext bc, SearchEventArgs searchParams)
            : base(bc, searchParams.clientID.Value, searchParams.partnerCode, searchParams.referralSource)
        {
            this.searchParams = searchParams;
            InitializeComponent();
        }

        /// <summary>
        /// Handle the completion of booking a face-to-face appointment
        /// </summary>
        protected override void Appointments_Book_Counseling_Control1_Completed(object sender, LINQ.client_appointment clientAppointment)
        {
            base.Appointments_Book_Counseling_Control1_Completed(sender, clientAppointment);
            searchParams.clientAppointment = clientAppointment;
        }

        /// <summary>
        /// Handle the completion of booking a workshop appointment
        /// </summary>
        protected override void Appointments_Control_Workshop_New1_Completed(object sender, LINQ.client_appointment clientAppointment)
        {
            base.Appointments_Control_Workshop_New1_Completed(sender, clientAppointment);
            searchParams.clientAppointment = clientAppointment;
        }
    }
}
