﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing
{
    internal partial class SearchClientFHLB : DebtPlus.UI.Desktop.CS.Client.Create.SearchWithHousing
    {
        public SearchClientFHLB() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += new EventHandler(SearchClientFHLB_Load);
        }

        private void UnRegisterHandlers()
        {
            Load -= new EventHandler(SearchClientFHLB_Load);
        }

        private void SearchClientFHLB_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the lookup sources
                lookUpEdit_FirstContact.Properties.DataSource = DebtPlus.LINQ.Cache.FirstContactType.getList();

                // Set the defaults since there is no record to edit, use the default as if we are creating a new item.
                lookUpEdit_FirstContact.EditValue = DebtPlus.LINQ.Cache.FirstContactType.getDefault();

                // Assume that there are housing issues for this client or we would not be here.
                checkEdit_housing.Checked = true;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Retrieve the parameters for the search and create operation
        /// </summary>
        protected override SearchEventArgs getSearchEventArgs()
        {
            SearchEventArgs e = base.getSearchEventArgs();
            e.methodFirstContact = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_FirstContact.EditValue);
            return e;
        }
    }
}
