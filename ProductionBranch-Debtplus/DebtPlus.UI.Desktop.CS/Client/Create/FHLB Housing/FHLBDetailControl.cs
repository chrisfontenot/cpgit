﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing
{
    internal partial class FHLBDetailControl : DebtPlus.UI.Desktop.CS.Client.Create.BaseControl
    {
        // The values that were given to the GetCurrentValues routine
        private SearchEventArgs ClientParameters;

        public FHLBDetailControl() : base()
        {
            InitializeComponent();
            RegisterHandlers();

            // Load the lookups with the items
            lookUpEdit_gender.Properties.DataSource      = DebtPlus.LINQ.Cache.GenderType.getList();
            lookUpEdit_language.Properties.DataSource    = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();
            lookUpEdit_referred_by.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList();
        }

        private void RegisterHandlers()
        {
            textEdit_partner_code.TextChanged                += FormChanged;
            nameRecordControl_name.TextChanged               += FormChanged;
            emailRecordControl_email.TextChanged             += FormChanged;
            addressRecordControl1.TextChanged                += FormChanged;
            lookUpEdit_referred_by.TextChanged               += FormChanged;
            lookUpEdit_language.TextChanged                  += FormChanged;
            lookUpEdit_gender.TextChanged                    += FormChanged;
            lookUpEdit_referred_by.EditValueChanged          += lookUpEdit_referred_by_EditValueChanged;
            telephoneNumberRecordControl_cell.TextChanged    += FormChanged;
            telephoneNumberRecordControl_day.TextChanged     += FormChanged;
            telephoneNumberRecordControl_evening.TextChanged += FormChanged;
            simpleButton_EditClient.Click                    += simpleButton_EditClient_Click;
            simpleButton_ScheduledAppt.Click                 += simpleButton_ScheduledAppt_Click;
        }

        private void UnRegisterHandlers()
        {
            textEdit_partner_code.TextChanged                -= FormChanged;
            nameRecordControl_name.TextChanged               -= FormChanged;
            emailRecordControl_email.TextChanged             -= FormChanged;
            addressRecordControl1.TextChanged                -= FormChanged;
            lookUpEdit_referred_by.TextChanged               -= FormChanged;
            lookUpEdit_language.TextChanged                  -= FormChanged;
            lookUpEdit_gender.TextChanged                    -= FormChanged;
            lookUpEdit_referred_by.EditValueChanged          -= lookUpEdit_referred_by_EditValueChanged;
            telephoneNumberRecordControl_cell.TextChanged    -= FormChanged;
            telephoneNumberRecordControl_day.TextChanged     -= FormChanged;
            telephoneNumberRecordControl_evening.TextChanged -= FormChanged;
            simpleButton_EditClient.Click                    -= simpleButton_EditClient_Click;
            simpleButton_ScheduledAppt.Click                 -= simpleButton_ScheduledAppt_Click;
        }

        public event EventHandler ScheduledAppointment;
        protected void RaiseScheduledAppointment(EventArgs e)
        {
            var evt = ScheduledAppointment;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnScheduledAppointment(EventArgs e)
        {
            RaiseScheduledAppointment(e);
        }

        public event EventHandler EditClient;
        protected void RaiseEditClient(EventArgs e)
        {
            var evt = EditClient;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnEditClient(EventArgs e)
        {
            RaiseEditClient(e);
        }

        /// <summary>
        /// Load the controls with the parameters for the client
        /// </summary>
        public virtual void SetCurrentValues(SearchEventArgs e)
        {
            // Save the values for the GetSearchEventArgs since we only augment the arguments, not replace them.
            ClientParameters = e;

            UnRegisterHandlers();
            try
            {
                textEdit_partner_code.EditValue = e.partnerCode;

                // Set the referral source. This should be moved to the create function but that is still in VB.NET so we have to
                // use that method of handling item items.
                var intItem = e.referralSource;
                if (intItem.HasValue)
                {
                    lookUpEdit_referred_by.EditValue = intItem;
                }
                else
                {
                    intItem = DebtPlus.LINQ.Cache.referred_by.getDefault();
                    if (intItem.HasValue)
                    {
                        lookUpEdit_referred_by.EditValue = intItem;
                    }
                }

                intItem = e.language;
                if (intItem.HasValue)
                {
                    lookUpEdit_language.EditValue = intItem;
                }
                else
                {
                    intItem = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage();
                    if (intItem.HasValue)
                    {
                        lookUpEdit_language.EditValue = intItem;
                    }
                }

                intItem = e.gender;
                if (intItem.HasValue)
                {
                    lookUpEdit_gender.EditValue = intItem;
                }
                else
                {
                    intItem = DebtPlus.LINQ.Cache.GenderType.getDefault();
                    if (intItem.HasValue)
                    {
                        lookUpEdit_gender.EditValue = intItem;
                    }
                }

                if (e.applicant_name != null) nameRecordControl_name.SetValues(e.applicant_name);
                if (e.email          != null) emailRecordControl_email.SetCurrentValues(e.email);
                if (e.address        != null) addressRecordControl1.SetCurrentValues(e.address);
                if (e.cellTelephone  != null) telephoneNumberRecordControl_cell.SetCurrentValues(e.cellTelephone);
                if (e.workTelephone  != null) telephoneNumberRecordControl_day.SetCurrentValues(e.workTelephone);
                if (e.homePhone      != null) telephoneNumberRecordControl_evening.SetCurrentValues(e.homePhone);

                bool isEnabled = !HasErrors();
                simpleButton_EditClient.Enabled = isEnabled;
                simpleButton_ScheduledAppt.Enabled = isEnabled;

                // Finally, control the partner code entry
                SetPartnerValidation();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// When the referral source changes then update the partner validation code
        /// </summary>
        private void lookUpEdit_referred_by_EditValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                SetPartnerValidation();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Set the partner code validation logic based upon the selected referral source
        /// </summary>
        private void SetPartnerValidation()
        {
            // Remove the validation handler for the partner. It will be re-enabled if we have one
            textEdit_partner_code.Validating -= textEdit_partner_code_Validating;
            textEdit_partner_code.ErrorText = string.Empty;

            // First, find the selected referral source
            // If there is no referral then the partner code is not allowed
            DebtPlus.LINQ.referred_by rb = lookUpEdit_referred_by.GetSelectedDataRow() as DebtPlus.LINQ.referred_by;
            if (rb == null)
            {
                textEdit_partner_code.EditValue = null;
                textEdit_partner_code.Enabled = false;
                return;
            }

            // Remove the partner code if there is no validation
            string validation = rb.partnerValidation;
            if (string.IsNullOrEmpty(validation))
            {
                textEdit_partner_code.EditValue = null;
                textEdit_partner_code.Enabled = false;
                return;
            }

            // Enable the partner code field
            textEdit_partner_code.Enabled = true;

            // Ensure that the partner code is valid for the entered expression
            // For these, we ignore errors in the validation code
            try
            {
                string currentPartnerCode = textEdit_partner_code.Text.Trim(new char[] {' ','_'});
                if (! System.Text.RegularExpressions.Regex.IsMatch(currentPartnerCode, "^" + validation + "$"))   // Make sure that the validation code matches the entire string and not a sub-string.
                {
                    textEdit_partner_code.EditValue = null;
                }
            }
            catch
            {
            }

            // Once the entry is valid then we can change the validation to match
            try
            {
                textEdit_partner_code.Properties.Mask.EditMask         = validation;
                textEdit_partner_code.Properties.Mask.MaskType         = DevExpress.XtraEditors.Mask.MaskType.RegEx;
                textEdit_partner_code.Properties.Mask.BeepOnError      = true;
                textEdit_partner_code.Properties.Mask.SaveLiteral      = true;
                textEdit_partner_code.Properties.Mask.ShowPlaceHolders = true;

                textEdit_partner_code.Validating                      += textEdit_partner_code_Validating;
                validate_partner_code();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Validate the partner code to ensure that it is acceptable for KeyField
        /// </summary>
        private void textEdit_partner_code_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = validate_partner_code();
        }

        /// <summary>
        /// Validate the partner code to ensure that it is acceptable for KeyField
        /// </summary>
        private bool validate_partner_code()
        {
            // No partner code is acceptable for the entry. It will be caught elsewhere
            string strCode = textEdit_partner_code.EditValue as String;
            if (strCode == null)
            {
                textEdit_partner_code.ErrorText = "A value is required for this field";
                return false;
            }

            // Pass the code through the regular expression routine to see if it is valid. We put the
            // beginning of string and end of string anchors on the expression to ensure that the entire
            // string matches the entire expression and not just some sub-string component.
            try
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(strCode, "^" + textEdit_partner_code.Properties.Mask.EditMask + "$"))
                {
                    return false;
                }

                // The entry is not completely valid. Reject it.
                textEdit_partner_code.ErrorText = "invalid entry";
            }
            catch (Exception ex)
            {
                textEdit_partner_code.ErrorText = ex.Message;
            }

            return true;
        }

        /// <summary>
        /// Retrieve the search parameter items
        /// </summary>
        /// <returns></returns>
        public virtual SearchEventArgs GetCurrentValues()
        {
            System.Diagnostics.Debug.Assert(ClientParameters != null);

            ClientParameters.referralSource = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_referred_by.EditValue);
            ClientParameters.partnerCode    = DebtPlus.Utils.Nulls.v_String(textEdit_partner_code.EditValue);
            ClientParameters.applicant_name = nameRecordControl_name.GetCurrentValues();
            ClientParameters.email          = emailRecordControl_email.GetCurrentValues();
            ClientParameters.gender         = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_gender.EditValue);
            ClientParameters.language       = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_language.EditValue);
            ClientParameters.cellTelephone  = telephoneNumberRecordControl_cell.GetCurrentValues();
            ClientParameters.homePhone      = telephoneNumberRecordControl_evening.GetCurrentValues();
            ClientParameters.workTelephone  = telephoneNumberRecordControl_day.GetCurrentValues();
            ClientParameters.address        = addressRecordControl1.GetCurrentValues();

            return ClientParameters;
        }

        /// <summary>
        /// Are all required fields set properly?
        /// </summary>
        protected virtual bool HasErrors()
        {
            // The partner code is required
            if (textEdit_partner_code.ErrorText != string.Empty)
            {
                return true;
            }

            // The referral source is required
            if (lookUpEdit_referred_by.EditValue == null)
            {
                return true;
            }

            // The gender is required
            if (lookUpEdit_gender.EditValue == null)
            {
                return true;
            }

            // The language is required
            if (lookUpEdit_language.EditValue == null)
            {
                return true;
            }

            // One of the telephone numbers is required
            if (telephoneNumberRecordControl_day.GetCurrentValues().IsEmpty())
            {
                if (telephoneNumberRecordControl_cell.GetCurrentValues().IsEmpty())
                {
                    if (telephoneNumberRecordControl_evening.GetCurrentValues().IsEmpty())
                    {
                        return true;
                    }
                }
            }

            // The address is required
            if (addressRecordControl1.GetCurrentValues().IsEmpty())
            {
                return true;
            }

            // The name is required
            if (nameRecordControl_name.GetCurrentValues().IsEmpty())
            {
                return true;
            }

            // Everything is valid now.
            return false;
        }

        /// <summary>
        /// Process a change in the KeyField form values
        /// </summary>
        protected void FormChanged(object sender, EventArgs e)
        {
            bool isEnabled = !HasErrors();
            simpleButton_EditClient.Enabled = isEnabled;
            simpleButton_ScheduledAppt.Enabled = isEnabled;
        }

        private void simpleButton_EditClient_Click(object sender, EventArgs e)
        {
            OnEditClient(EventArgs.Empty);
        }

        private void simpleButton_ScheduledAppt_Click(object sender, EventArgs e)
        {
            OnScheduledAppointment(EventArgs.Empty);
        }
    }
}
