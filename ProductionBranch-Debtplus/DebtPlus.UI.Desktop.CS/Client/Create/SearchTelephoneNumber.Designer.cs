﻿namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    partial class SearchTelephoneNumber
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchTelephoneNumber));
            this.labelControl_FormTitle = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_Instructions = new DevExpress.XtraEditors.LabelControl();
            this.homeTelephoneNumber = new DebtPlus.Data.Controls.TelephoneNumberControl();
            this.checkEdit_no_telephone = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl_homeTelephoneNumber = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(136, 254);
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(232, 254);
            this.simpleButton_Cancel.TabIndex = 6;
            // 
            // labelControl_FormTitle
            // 
            this.labelControl_FormTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_FormTitle.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold);
            this.labelControl_FormTitle.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl_FormTitle.Location = new System.Drawing.Point(143, 3);
            this.labelControl_FormTitle.Name = "labelControl_FormTitle";
            this.labelControl_FormTitle.Size = new System.Drawing.Size(170, 27);
            this.labelControl_FormTitle.TabIndex = 0;
            this.labelControl_FormTitle.Text = "Create New Client";
            // 
            // labelControl_Instructions
            // 
            this.labelControl_Instructions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl_Instructions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl_Instructions.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.labelControl_Instructions.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl_Instructions.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl_Instructions.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Instructions.Location = new System.Drawing.Point(12, 35);
            this.labelControl_Instructions.MinimumSize = new System.Drawing.Size(401, 0);
            this.labelControl_Instructions.Name = "labelControl_Instructions";
            this.labelControl_Instructions.Size = new System.Drawing.Size(418, 65);
            this.labelControl_Instructions.TabIndex = 1;
            this.labelControl_Instructions.Text = resources.GetString("labelControl_Instructions.Text");
            // 
            // homeTelephoneNumber
            // 
            this.homeTelephoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.homeTelephoneNumber.ErrorIcon = null;
            this.homeTelephoneNumber.ErrorText = "";
            this.homeTelephoneNumber.Location = new System.Drawing.Point(116, 121);
            this.homeTelephoneNumber.Name = "homeTelephoneNumber";
            this.homeTelephoneNumber.Size = new System.Drawing.Size(314, 20);
            this.homeTelephoneNumber.TabIndex = 3;
            // 
            // checkEdit_no_telephone
            // 
            this.checkEdit_no_telephone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_no_telephone.Location = new System.Drawing.Point(10, 219);
            this.checkEdit_no_telephone.Name = "checkEdit_no_telephone";
            this.checkEdit_no_telephone.Properties.Caption = "There is no telephone number. Just create the client.";
            this.checkEdit_no_telephone.Size = new System.Drawing.Size(280, 20);
            this.checkEdit_no_telephone.TabIndex = 4;
            // 
            // labelControl_homeTelephoneNumber
            // 
            this.labelControl_homeTelephoneNumber.Location = new System.Drawing.Point(12, 125);
            this.labelControl_homeTelephoneNumber.Name = "labelControl_homeTelephoneNumber";
            this.labelControl_homeTelephoneNumber.Size = new System.Drawing.Size(80, 13);
            this.labelControl_homeTelephoneNumber.TabIndex = 2;
            this.labelControl_homeTelephoneNumber.Text = "Home Telephone";
            // 
            // SearchTelephoneNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.labelControl_homeTelephoneNumber);
            this.Controls.Add(this.checkEdit_no_telephone);
            this.Controls.Add(this.homeTelephoneNumber);
            this.Controls.Add(this.labelControl_Instructions);
            this.Controls.Add(this.labelControl_FormTitle);
            this.Name = "SearchTelephoneNumber";
            this.Size = new System.Drawing.Size(442, 300);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.labelControl_FormTitle, 0);
            this.Controls.SetChildIndex(this.labelControl_Instructions, 0);
            this.Controls.SetChildIndex(this.homeTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.checkEdit_no_telephone, 0);
            this.Controls.SetChildIndex(this.labelControl_homeTelephoneNumber, 0);
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        public DevExpress.XtraEditors.LabelControl labelControl_FormTitle;
        public DevExpress.XtraEditors.LabelControl labelControl_Instructions;
        public Data.Controls.TelephoneNumberControl homeTelephoneNumber;
        protected DevExpress.XtraEditors.CheckEdit checkEdit_no_telephone;
        public DevExpress.XtraEditors.LabelControl labelControl_homeTelephoneNumber;
    }
}
