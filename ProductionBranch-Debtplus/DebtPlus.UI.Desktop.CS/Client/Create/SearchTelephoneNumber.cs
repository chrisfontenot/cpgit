﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    internal partial class SearchTelephoneNumber : DebtPlus.UI.Desktop.CS.Client.Create.BaseControlWithOK
    {
        /// <summary>
        /// Delegate for the search events
        /// </summary>
        public delegate void SearchEventHandler(object sender, SearchEventArgs e);

        /// <summary>
        /// Event tripped when the search operation is to be performed
        /// </summary>
        public event SearchEventHandler Search;

        /// <summary>
        /// Raise the search event
        /// </summary>
        protected void RaiseSearch(SearchEventArgs e)
        {
            var evt = Search;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Raise the search operation on the OK button
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnSearch(SearchEventArgs e)
        {
            RaiseSearch(e);
        }

        /// <summary>
        /// Retrieve the search event arguments for the search and create operation.
        /// </summary>
        /// <returns></returns>
        protected virtual SearchEventArgs getSearchEventArgs()
        {
            var e = new SearchEventArgs();
            if (!checkEdit_no_telephone.Checked)
            {
                e.homePhone = homeTelephoneNumber.GetCurrentValues();
            }
            return e;
        }

        /// <summary>
        /// Handle the change in the home telephone number entry
        /// </summary>
        private void checkEdit_no_telephone_CheckedChanged(object sender, EventArgs e)
        {
            if (checkEdit_no_telephone.Checked)
            {
                homeTelephoneNumber.Enabled = false;
                homeTelephoneNumber.SetCurrentValues(new DebtPlus.LINQ.TelephoneNumber());
            }
            else
            {
                homeTelephoneNumber.Enabled = true;
            }
        }

        /// <summary>
        /// Is the control valid for performing the search operation?
        /// </summary>
        /// <returns></returns>
        protected virtual bool HasErrors()
        {
            // If there is no telephone number then it is acceptable
            if (checkEdit_no_telephone.Checked)
            {
                return false;
            }

            // If there is a telephone number entered then it is valid too
            TelephoneNumber phone = homeTelephoneNumber.GetCurrentValues();
            if (!string.IsNullOrEmpty(phone.Number))
            {
                return false;
            }

            // There is something still missing.
            return true;
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        public SearchTelephoneNumber()
        {
            InitializeComponent();
            if (!DebtPlus.Configuration.DesignMode.IsInDesignMode())
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            this.Load += Control_Load;
            homeTelephoneNumber.TextChanged += FormChanged;
            checkEdit_no_telephone.CheckedChanged += checkEdit_no_telephone_CheckedChanged;
            checkEdit_no_telephone.CheckedChanged += FormChanged;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            this.Load -= Control_Load;
            homeTelephoneNumber.TextChanged -= FormChanged;
            checkEdit_no_telephone.CheckedChanged -= checkEdit_no_telephone_CheckedChanged;
            checkEdit_no_telephone.CheckedChanged -= FormChanged;
        }

        /// <summary>
        /// Handle the LOAD event of this form object
        /// </summary>
        private void Control_Load(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the event for changing the KeyField controls
        /// </summary>
        protected virtual void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Do the search operation on the OK button being pressed.
        /// </summary>
        protected override void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            OnSearch(getSearchEventArgs());
        }
    }
}
