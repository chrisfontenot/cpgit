﻿namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    partial class BaseControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(174, 251);
            this.simpleButton_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.TabIndex = 7;
            this.simpleButton_Cancel.Text = "&Cancel";
            // 
            // BaseControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton_Cancel);
            this.Name = "BaseControl";
            this.Size = new System.Drawing.Size(423, 290);
            this.ResumeLayout(false);

        }
        #endregion

        protected DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;



    }
}
