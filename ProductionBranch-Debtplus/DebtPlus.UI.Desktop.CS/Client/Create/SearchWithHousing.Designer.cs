﻿namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    partial class SearchWithHousing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkEdit_housing = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_housing.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl_FormTitle
            // 
            this.labelControl_FormTitle.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.labelControl_FormTitle.Appearance.ForeColor = System.Drawing.Color.Maroon;
            this.labelControl_FormTitle.Size = new System.Drawing.Size(157, 26);
            // 
            // labelControl_Instructions
            // 
            this.labelControl_Instructions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl_Instructions.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.labelControl_Instructions.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl_Instructions.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // checkEdit_no_telephone
            // 
            this.checkEdit_no_telephone.Size = new System.Drawing.Size(280, 20);
            this.checkEdit_no_telephone.TabIndex = 5;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.TabIndex = 6;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.TabIndex = 7;
            // 
            // checkEdit_housing
            // 
            this.checkEdit_housing.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_housing.Location = new System.Drawing.Point(10, 194);
            this.checkEdit_housing.Name = "checkEdit_housing";
            this.checkEdit_housing.Properties.Caption = "Starting with housing issues";
            this.checkEdit_housing.Size = new System.Drawing.Size(165, 20);
            this.checkEdit_housing.TabIndex = 4;
            // 
            // SearchWithHousing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.checkEdit_housing);
            this.Name = "SearchWithHousing";
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.labelControl_FormTitle, 0);
            this.Controls.SetChildIndex(this.labelControl_Instructions, 0);
            this.Controls.SetChildIndex(this.homeTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.checkEdit_no_telephone, 0);
            this.Controls.SetChildIndex(this.labelControl_homeTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.checkEdit_housing, 0);
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_housing.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        protected DevExpress.XtraEditors.CheckEdit checkEdit_housing;
    }
}
