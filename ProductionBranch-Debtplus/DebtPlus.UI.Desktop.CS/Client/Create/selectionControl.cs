﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    internal partial class selectionControl : BaseControlWithOK
    {
        // Current SearchEventArgs when we were given the list to be searched
        private SearchEventArgs searchArgs;

        public selectionControl()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            gridView1.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(gridView1_RowClick);
            gridView1.DoubleClick += gridView1_DoubleClick;
            checkEdit_notInList.CheckedChanged += checkEdit_notInList_CheckedChanged;
        }

        private void UnRegisterHandlers()
        {
            gridView1.RowClick -= new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(gridView1_RowClick);
            gridView1.DoubleClick -= gridView1_DoubleClick;
            checkEdit_notInList.CheckedChanged -= checkEdit_notInList_CheckedChanged;
        }

        // Event occurs when the search is completed
        public event SearchEventHandler ClientFound;
        protected void RaiseClientFound(SearchEventArgs e)
        {
            var evt = ClientFound;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnClientFound(SearchEventArgs e)
        {
            RaiseClientFound(e);
        }

        /// <summary>
        /// Perform the search with the list of client IDs that match the selection
        /// </summary>
        public virtual void SetCurrentValues(SearchEventArgs e, System.Collections.Generic.List<DebtPlus.LINQ.xpr_client_create_searchResult> searchList)
        {
            UnRegisterHandlers();
            try
            {
                searchArgs = e;
                gridControl1.DataSource = searchList;
                gridView1.BestFitColumns();
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the check status for the "not in list" item
        /// </summary>
        private void checkEdit_notInList_CheckedChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        protected virtual void gridView1_RowClick(object sender, EventArgs e)
        {
            // Find the position where the user clicked with the mouse.
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            if (hi.IsValid && hi.InRow)
            {
                gridView1.FocusedRowHandle = hi.RowHandle;
                simpleButton_OK.Enabled = !HasErrors();
                return;
            }

            // The row is not valid. Reject the focus.
            gridView1.FocusedRowHandle = -1;
            simpleButton_OK.Enabled = !HasErrors();
        }

        protected virtual void gridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the position where the user clicked with the mouse.
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            if (hi.IsValid && hi.InRow)
            {
                DebtPlus.LINQ.xpr_client_create_searchResult selectedItem = gridView1.GetRow(hi.RowHandle) as DebtPlus.LINQ.xpr_client_create_searchResult;
                if (selectedItem != null)
                {
                    ProcessSelection(selectedItem);
                }
            }
        }

        /// <summary>
        /// Handle the selection of the row from the grid
        /// </summary>
        /// <param name="item"></param>
        protected virtual void ProcessSelection(DebtPlus.LINQ.xpr_client_create_searchResult item)
        {
            searchArgs.clientID = item.clientID;
            searchArgs.existingClient = true;
            OnClientFound(searchArgs);
        }

        protected override void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            // Look to determine if the client is in the list
            if (!checkEdit_notInList.Checked)
            {
                DebtPlus.LINQ.xpr_client_create_searchResult selectedItem = gridView1.GetRow(gridView1.FocusedRowHandle) as DebtPlus.LINQ.xpr_client_create_searchResult;
                if (selectedItem != null)
                {
                    ProcessSelection(selectedItem);
                    return;
                }
            }

            // Indicate that there is no client and just return the results
            searchArgs.existingClient = false;
            OnClientFound(searchArgs);
        }

        /// <summary>
        /// Does the KeyField form have any error conditions?
        /// </summary>
        /// <returns></returns>
        protected virtual bool HasErrors()
        {
            // See if the item is acceptable for processing
            if (checkEdit_notInList.Checked)
            {
                return false;
            }

            // There must be a selected row for the client
            if (gridView1.FocusedRowHandle >= 0)
            {
                return false;
            }

            return true;
        }
    }
}
