﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    internal partial class SearchWithHousing : SearchTelephoneNumber
    {
        public SearchWithHousing() : base()
        {
            InitializeComponent();
            this.labelControl_Instructions.Size = new System.Drawing.Size(418, 65);
            RegisterHandlers();
        }

        public SearchWithHousing(IContainer container)
            : base()
        {
            container.Add(this);
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
        }

        private void UnRegisterHandlers()
        {
        }

        /// <summary>
        /// Retrieve the search event arguments for the search operation and client creation
        /// </summary>
        protected override SearchEventArgs getSearchEventArgs()
        {
            SearchEventArgs e = base.getSearchEventArgs();
            e.housingIssues = checkEdit_housing.Checked;
            return e;
        }
    }
}
