﻿namespace DebtPlus.UI.Desktop.CS.Client.Create.DMP
{
    partial class CreateControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateControl));
            this.telephoneNumberControl1 = new DebtPlus.Data.Controls.TelephoneNumberControl();
            this.checkEdit_HousingProblems = new DevExpress.XtraEditors.CheckEdit();
            this.lookUpEdit_method_first_contact = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_referred_by = new DevExpress.XtraEditors.LookUpEdit();
            this.checkEdit_NoTelephoneNumber = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_HousingProblems.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_method_first_contact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_referred_by.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_NoTelephoneNumber.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(227, 211);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(128, 211);
            // 
            // telephoneNumberControl1
            // 
            this.telephoneNumberControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.telephoneNumberControl1.ErrorIcon = null;
            this.telephoneNumberControl1.ErrorText = "";
            this.telephoneNumberControl1.Location = new System.Drawing.Point(138, 123);
            this.telephoneNumberControl1.Name = "telephoneNumberControl1";
            this.telephoneNumberControl1.Size = new System.Drawing.Size(274, 20);
            this.telephoneNumberControl1.TabIndex = 13;
            // 
            // checkEdit_HousingProblems
            // 
            this.checkEdit_HousingProblems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit_HousingProblems.Location = new System.Drawing.Point(11, 242);
            this.checkEdit_HousingProblems.Name = "checkEdit_HousingProblems";
            this.checkEdit_HousingProblems.Properties.Caption = "Started with Housing Problems";
            this.checkEdit_HousingProblems.Size = new System.Drawing.Size(173, 19);
            this.checkEdit_HousingProblems.TabIndex = 18;
            // 
            // lookUpEdit_method_first_contact
            // 
            this.lookUpEdit_method_first_contact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_method_first_contact.Location = new System.Drawing.Point(138, 176);
            this.lookUpEdit_method_first_contact.Name = "lookUpEdit_method_first_contact";
            this.lookUpEdit_method_first_contact.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_method_first_contact.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_method_first_contact.Properties.DisplayMember = "description";
            this.lookUpEdit_method_first_contact.Properties.NullText = "";
            this.lookUpEdit_method_first_contact.Properties.ShowFooter = false;
            this.lookUpEdit_method_first_contact.Properties.ShowHeader = false;
            this.lookUpEdit_method_first_contact.Properties.SortColumnIndex = 1;
            this.lookUpEdit_method_first_contact.Properties.ValueMember = "item_key";
            this.lookUpEdit_method_first_contact.Size = new System.Drawing.Size(274, 20);
            this.lookUpEdit_method_first_contact.TabIndex = 17;
            // 
            // lookUpEdit_referred_by
            // 
            this.lookUpEdit_referred_by.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_referred_by.Location = new System.Drawing.Point(138, 149);
            this.lookUpEdit_referred_by.Name = "lookUpEdit_referred_by";
            this.lookUpEdit_referred_by.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_referred_by.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_referred_by.Properties.DisplayMember = "description";
            this.lookUpEdit_referred_by.Properties.NullText = "";
            this.lookUpEdit_referred_by.Properties.ShowFooter = false;
            this.lookUpEdit_referred_by.Properties.ShowHeader = false;
            this.lookUpEdit_referred_by.Properties.SortColumnIndex = 1;
            this.lookUpEdit_referred_by.Properties.ValueMember = "item_key";
            this.lookUpEdit_referred_by.Size = new System.Drawing.Size(274, 20);
            this.lookUpEdit_referred_by.TabIndex = 15;
            // 
            // checkEdit_NoTelephoneNumber
            // 
            this.checkEdit_NoTelephoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkEdit_NoTelephoneNumber.Location = new System.Drawing.Point(11, 267);
            this.checkEdit_NoTelephoneNumber.Name = "checkEdit_NoTelephoneNumber";
            this.checkEdit_NoTelephoneNumber.Properties.Caption = "There is no home telephone number. Just create the client.";
            this.checkEdit_NoTelephoneNumber.Size = new System.Drawing.Size(401, 19);
            this.checkEdit_NoTelephoneNumber.TabIndex = 19;
            // 
            // labelControl5
            // 
            this.labelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl5.Location = new System.Drawing.Point(11, 179);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(114, 13);
            this.labelControl5.TabIndex = 16;
            this.labelControl5.Text = "Method of First Contact";
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl4.Location = new System.Drawing.Point(11, 152);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(67, 13);
            this.labelControl4.TabIndex = 14;
            this.labelControl4.Text = "Referral Code";
            // 
            // labelControl3
            // 
            this.labelControl3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl3.Location = new System.Drawing.Point(11, 125);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(120, 13);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "Home Telephone Number";
            // 
            // labelControl_FormTitle
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(4, 3);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(415, 23);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Create New Client";
            // 
            // labelControl_Instructions
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.Location = new System.Drawing.Point(11, 32);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(401, 84);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = resources.GetString("labelControl2.Text");
            // 
            // CreateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.telephoneNumberControl1);
            this.Controls.Add(this.checkEdit_HousingProblems);
            this.Controls.Add(this.lookUpEdit_method_first_contact);
            this.Controls.Add(this.lookUpEdit_referred_by);
            this.Controls.Add(this.checkEdit_NoTelephoneNumber);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl2);
            this.Name = "CreateControl";
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.checkEdit_NoTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_referred_by, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_method_first_contact, 0);
            this.Controls.SetChildIndex(this.checkEdit_HousingProblems, 0);
            this.Controls.SetChildIndex(this.telephoneNumberControl1, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_HousingProblems.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_method_first_contact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_referred_by.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_NoTelephoneNumber.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal Data.Controls.TelephoneNumberControl telephoneNumberControl1;
        internal DevExpress.XtraEditors.CheckEdit checkEdit_HousingProblems;
        internal DevExpress.XtraEditors.LookUpEdit lookUpEdit_method_first_contact;
        internal DevExpress.XtraEditors.LookUpEdit lookUpEdit_referred_by;
        internal DevExpress.XtraEditors.CheckEdit checkEdit_NoTelephoneNumber;
        internal DevExpress.XtraEditors.LabelControl labelControl5;
        internal DevExpress.XtraEditors.LabelControl labelControl4;
        internal DevExpress.XtraEditors.LabelControl labelControl3;
        internal DevExpress.XtraEditors.LabelControl labelControl1;
        internal DevExpress.XtraEditors.LabelControl labelControl2;
    }
}
