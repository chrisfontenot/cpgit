﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.DMP
{
    internal partial class DuplicateControl : DebtPlus.UI.Desktop.CS.Client.Create.BaseControlWithOK
    {
        public DuplicateControl()
            : base()
        {
            InitializeComponent();
        }

        private void RegisterHandlers()
        {
            gridView1.Click += gridView1_Click;
            gridView1.DoubleClick += gridView1_DoubleClick;
        }

        private void UnRegisterHandlers()
        {
            gridView1.Click -= gridView1_Click;
            gridView1.DoubleClick -= gridView1_DoubleClick;
        }

        DataTable tbl = null;

        public void ReadForm(DataTable tbl)
        {
            this.tbl = tbl;

            gridControl1.DataSource = tbl.DefaultView;
            gridControl1.RefreshDataSource();
            gridView1.BestFitColumns();

            SelectRow(gridView1.FocusedRowHandle);
            simpleButton_OK.Enabled = !HasErrors();
        }

        private void SelectRow(Int32 controlRow)
        {
            DataRowView drv = null;
            if (controlRow >= 0)
            {
                drv = (DataRowView)gridView1.GetRow(controlRow);
            }

            // If there is a row then select it
            FormCreateClient formCreateClient = (FormCreateClient)ParentForm;
            formCreateClient.ClientId = -1;
            if (drv != null)
            {
                if (drv["client"] != null && !object.ReferenceEquals(drv["client"], DBNull.Value))
                {
                    formCreateClient.ClientId = Convert.ToInt32(drv["client"]);
                }
            }
        }

        private bool HasErrors()
        {
            return ((FormCreateClient)ParentForm).ClientId < 0;
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));

            Int32 controlRow = -1;
            if (hi.InRow)
            {
                controlRow = hi.RowHandle;
            }
            SelectRow(controlRow);

            gridView1.FocusedRowHandle = controlRow;
            simpleButton_OK.Enabled = !HasErrors();
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));

            Int32 controlRow = -1;
            if (hi.InRow)
            {
                controlRow = hi.RowHandle;
            }
            SelectRow(controlRow);

            gridView1.FocusedRowHandle = controlRow;
            simpleButton_OK.Enabled = !HasErrors();
            if (simpleButton_OK.Enabled)
            {
                simpleButton_OK.PerformClick();
            }
        }
    }
}
