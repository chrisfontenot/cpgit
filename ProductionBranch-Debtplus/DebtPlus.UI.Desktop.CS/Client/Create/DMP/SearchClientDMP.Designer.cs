﻿namespace DebtPlus.UI.Desktop.CS.Client.Create.DMP
{
    partial class SearchClientDMP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_FirstContact = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_ReferredBy = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_InboundCallNumber = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_housing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_FirstContact.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ReferredBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_InboundCallNumber.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // checkEdit_housing
            // 
            this.checkEdit_housing.Location = new System.Drawing.Point(10, 229);
            this.checkEdit_housing.TabIndex = 10;
            // 
            // labelControl_FormTitle
            // 
            this.labelControl_FormTitle.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold);
            this.labelControl_FormTitle.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labelControl_FormTitle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // labelControl_Instructions
            // 
            this.labelControl_Instructions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl_Instructions.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.labelControl_Instructions.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl_Instructions.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // checkEdit_no_telephone
            // 
            this.checkEdit_no_telephone.Location = new System.Drawing.Point(10, 252);
            this.checkEdit_no_telephone.TabIndex = 11;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(136, 287);
            this.simpleButton_OK.TabIndex = 12;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(232, 287);
            this.simpleButton_Cancel.TabIndex = 13;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 175);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(75, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Referral Source";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 199);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Method of Contact";
            // 
            // lookUpEdit_FirstContact
            // 
            this.lookUpEdit_FirstContact.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_FirstContact.Location = new System.Drawing.Point(116, 195);
            this.lookUpEdit_FirstContact.Name = "lookUpEdit_FirstContact";
            this.lookUpEdit_FirstContact.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_FirstContact.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_FirstContact.Properties.DisplayMember = "description";
            this.lookUpEdit_FirstContact.Properties.NullText = "";
            this.lookUpEdit_FirstContact.Properties.ShowFooter = false;
            this.lookUpEdit_FirstContact.Properties.ShowHeader = false;
            this.lookUpEdit_FirstContact.Properties.ValueMember = "Id";
            this.lookUpEdit_FirstContact.Size = new System.Drawing.Size(314, 20);
            this.lookUpEdit_FirstContact.TabIndex = 9;
            // 
            // lookUpEdit_ReferredBy
            // 
            this.lookUpEdit_ReferredBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_ReferredBy.Location = new System.Drawing.Point(116, 171);
            this.lookUpEdit_ReferredBy.Name = "lookUpEdit_ReferredBy";
            this.lookUpEdit_ReferredBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_ReferredBy.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_ReferredBy.Properties.DisplayMember = "description";
            this.lookUpEdit_ReferredBy.Properties.NullText = "";
            this.lookUpEdit_ReferredBy.Properties.ShowFooter = false;
            this.lookUpEdit_ReferredBy.Properties.ShowHeader = false;
            this.lookUpEdit_ReferredBy.Properties.SortColumnIndex = 1;
            this.lookUpEdit_ReferredBy.Properties.ValueMember = "Id";
            this.lookUpEdit_ReferredBy.Size = new System.Drawing.Size(314, 20);
            this.lookUpEdit_ReferredBy.TabIndex = 7;
            // 
            // lookUpEdit_InboundCallNumber
            // 
            this.lookUpEdit_InboundCallNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_InboundCallNumber.Location = new System.Drawing.Point(116, 147);
            this.lookUpEdit_InboundCallNumber.Name = "lookUpEdit_InboundCallNumber";
            this.lookUpEdit_InboundCallNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_InboundCallNumber.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_InboundCallNumber.Properties.DisplayMember = "Description";
            this.lookUpEdit_InboundCallNumber.Properties.NullText = "";
            this.lookUpEdit_InboundCallNumber.Properties.ShowFooter = false;
            this.lookUpEdit_InboundCallNumber.Properties.ShowHeader = false;
            this.lookUpEdit_InboundCallNumber.Properties.SortColumnIndex = 1;
            this.lookUpEdit_InboundCallNumber.Properties.ValueMember = "Id";
            this.lookUpEdit_InboundCallNumber.Size = new System.Drawing.Size(314, 20);
            this.lookUpEdit_InboundCallNumber.TabIndex = 5;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 151);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(100, 13);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Inbound Call Number";
            // 
            // SearchClientDMP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.lookUpEdit_InboundCallNumber);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.lookUpEdit_ReferredBy);
            this.Controls.Add(this.lookUpEdit_FirstContact);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelControl3);
            this.Name = "SearchClientDMP";
            this.Size = new System.Drawing.Size(442, 323);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.labelControl_FormTitle, 0);
            this.Controls.SetChildIndex(this.labelControl_Instructions, 0);
            this.Controls.SetChildIndex(this.homeTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.checkEdit_no_telephone, 0);
            this.Controls.SetChildIndex(this.labelControl_homeTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.checkEdit_housing, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_FirstContact, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_ReferredBy, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_InboundCallNumber, 0);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_housing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_FirstContact.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_ReferredBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_InboundCallNumber.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_FirstContact;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_ReferredBy;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit_InboundCallNumber;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}
