#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.DMP
{
    internal partial class FormCreateClient : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Current search event arguments and resulting client
        private SearchEventArgs currentSearchEventArgs;

        public FormCreateClient()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += Form_CreateClient_Load;
            SearchClientDMP1.Cancelled += SearchClientDMP1_Cancelled;
            selectionControl1.Cancelled += selectionControl1_Cancelled;
            SearchClientDMP1.Search += SearchClientDMP1_Search;
            selectionControl1.ClientFound += selectionControl1_ClientFound;
        }

        private void UnRegisterHandlers()
        {
            Load -= Form_CreateClient_Load;
            SearchClientDMP1.Cancelled -= SearchClientDMP1_Cancelled;
            selectionControl1.Cancelled -= selectionControl1_Cancelled;
            SearchClientDMP1.Search -= SearchClientDMP1_Search;
            selectionControl1.ClientFound -= selectionControl1_ClientFound;
        }

        private void Form_CreateClient_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                SwitchToSearch();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// The user canceled the selection form. This is terminal to the form processing.
        /// </summary>
        private void SearchClientDMP1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// The user canceled the selection form. Go back to the search control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectionControl1_Cancelled(object sender, EventArgs e)
        {
            SwitchToSearch();
        }

        /// <summary>
        /// Switch the form to the KeyField processing
        /// </summary>
        private void SwitchToSearch()
        {
            // Send the duplicate control to the rear
            this.selectionControl1.Visible = false;
            this.selectionControl1.SendToBack();

            // Bring the selection control to the front
            this.SearchClientDMP1.BringToFront();
            this.SearchClientDMP1.Visible = true;
            this.SearchClientDMP1.Focus();
        }

        /// <summary>
        /// Switch the form to the selection processing
        /// </summary>
        private void SwitchToSelect()
        {
            // Send the search control to the rear
            this.SearchClientDMP1.Visible = false;
            this.SearchClientDMP1.SendToBack();

            // Bring the select control to the front
            this.selectionControl1.BringToFront();
            this.selectionControl1.Visible = true;
            this.selectionControl1.Focus();
        }

        /// <summary>
        /// Do the client search operation. Create the client if indicated.
        /// </summary>
        /// <param name="e"></param>
        private void SearchClientDMP1_Search(object Sender, SearchEventArgs e)
        {
            // Save the search for the result function
            currentSearchEventArgs = e;

            // If there is no telephone number then create the client.
            if (e.homePhone == null)
            {
                e.clientID = ClientSearch.CreateClient(e);
                e.existingClient = false;
                CompleteFormProcessing(e);
                return;
            }

            // Look for the client in the list of telephone numbers
            System.Collections.Generic.List<DebtPlus.LINQ.xpr_client_create_searchResult> colAnswers = ClientSearch.PerformSearch(e);
            if (colAnswers == null)
            {
                return;
            }

            // If there are no clients then go ahead and create the client
            if (colAnswers.Count == 0)
            {
                e.clientID = ClientSearch.CreateClient(e);
                e.existingClient = false;
                CompleteFormProcessing(e);
                return;
            }

            // If there is exactly one client then ask if this is the client
            if (colAnswers.Count == 1)
            {
                switch (DebtPlus.Data.Forms.MessageBox.Show(string.Format("Is the client named '{0}'?\r\n\r\nThere is only one client currently in the database with\r\nthis telephone number. If this is the client then we don't\r\nwant to create a duplicate entry.", colAnswers[0].name), "Duplicate Telephone Number", System.Windows.Forms.MessageBoxButtons.YesNoCancel, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button3))
                {
                    case System.Windows.Forms.DialogResult.Yes:
                        e.clientID = colAnswers[0].clientID;
                        e.existingClient = true;
                        CompleteFormProcessing(e);
                        return;

                    case System.Windows.Forms.DialogResult.No:
                        e.clientID = ClientSearch.CreateClient(e);
                        e.existingClient = false;
                        CompleteFormProcessing(e);
                        return;

                    case System.Windows.Forms.DialogResult.Cancel:
                        break;
                }
                return;
            }

            // There are more than one. Ask the user for the proper selection now.
            // The control will take it from this point until an item is found.
            SwitchToSelect();
            selectionControl1.SetCurrentValues(e, colAnswers);
        }

        /// <summary>
        /// Process the completion of the selection operation on a list of possible client matches.
        /// </summary>
        private void selectionControl1_ClientFound(object sender, SearchEventArgs e)
        {
            // Save the search for the result function
            currentSearchEventArgs = e;

            // If the client is not in the list then we need to create it. Leave the
            // "existingClient" flag false to indicate that the client was new. Just
            // plug in the client ID.
            if (! e.existingClient)
            {
                e.clientID = ClientSearch.CreateClient(e);
            }

            CompleteFormProcessing(e);
        }

        /// <summary>
        /// Routine to complete the form processing. It may be overridden if needed.
        /// </summary>
        protected virtual void CompleteFormProcessing(SearchEventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
