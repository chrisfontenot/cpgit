﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.Client.Create.DMP
{
    internal partial class SearchClientDMP : DebtPlus.UI.Desktop.CS.Client.Create.SearchWithHousing
    {
        public SearchClientDMP()
        {
            InitializeComponent();
            if (!DebtPlus.Configuration.DesignMode.IsInDesignMode())
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Load                                           += new EventHandler(SearchClientDMP_Load);
            lookUpEdit_ReferredBy.EditValueChanging        += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lookUpEdit_FirstContact.EditValueChanging      += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lookUpEdit_InboundCallNumber.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lookUpEdit_InboundCallNumber.EditValueChanged  += FormChanged;
            lookUpEdit_ReferredBy.EditValueChanging        += FormChanged;
            lookUpEdit_FirstContact.EditValueChanging      += FormChanged;
            lookUpEdit_InboundCallNumber.EditValueChanged  += lookUpEdit_InboundCallNumber_EditValueChanged;
        }

        private void UnRegisterHandlers()
        {
            Load                                           -= new EventHandler(SearchClientDMP_Load);
            lookUpEdit_ReferredBy.EditValueChanging        -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lookUpEdit_FirstContact.EditValueChanging      -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lookUpEdit_InboundCallNumber.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            lookUpEdit_InboundCallNumber.EditValueChanged  -= FormChanged;
            lookUpEdit_ReferredBy.EditValueChanging        -= FormChanged;
            lookUpEdit_FirstContact.EditValueChanging      -= FormChanged;
            lookUpEdit_InboundCallNumber.EditValueChanged  -= lookUpEdit_InboundCallNumber_EditValueChanged;
        }

        private void SearchClientDMP_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Set the lookup sources
                lookUpEdit_ReferredBy.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList();
                lookUpEdit_FirstContact.Properties.DataSource = DebtPlus.LINQ.Cache.FirstContactType.getList();
                lookUpEdit_InboundCallNumber.Properties.DataSource = DebtPlus.LINQ.Cache.InboundTelephoneNumber.getList();

                // Set the defaults since there is no record to edit, use the default as if we are creating a new item.
                lookUpEdit_ReferredBy.EditValue = DebtPlus.LINQ.Cache.referred_by.getDefault();
                lookUpEdit_FirstContact.EditValue = DebtPlus.LINQ.Cache.FirstContactType.getDefault();
                lookUpEdit_InboundCallNumber.EditValue = DebtPlus.LINQ.Cache.InboundTelephoneNumber.getDefault();

                SetReferredBy();
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        protected override bool HasErrors()
        {
            return base.HasErrors() || lookUpEdit_InboundCallNumber.EditValue == null;
        }

        private void lookUpEdit_InboundCallNumber_EditValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                SetReferredBy();
                simpleButton_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle a change in the referral source
        /// </summary>
        private void SetReferredBy()
        {
            // Find the telephone number from the control
            var item = lookUpEdit_InboundCallNumber.GetSelectedDataRow() as DebtPlus.LINQ.InboundTelephoneNumber;
            if (item == null)
            {
                return;
            }

            // Find the group from the telephone number
            var groupRow = item.InBoundTelephoneNumberGroup1;
            if (groupRow == null)
            {
                return;
            }

            // Find the referral source in the group
            var referredBy = groupRow.referred_by;
            if (referredBy == null)
            {
                return;
            }

            // Correct the referral source
            lookUpEdit_ReferredBy.EditValue = referredBy.Value;
        }

        /// <summary>
        /// Retrieve the parameters for the search and create operation
        /// </summary>
        protected override SearchEventArgs getSearchEventArgs()
        {
            SearchEventArgs e = base.getSearchEventArgs();
            e.methodFirstContact = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_FirstContact.EditValue);
            e.referralSource = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_ReferredBy.EditValue);
            e.inboundTelephoneNumber = lookUpEdit_InboundCallNumber.GetSelectedDataRow() as DebtPlus.LINQ.InboundTelephoneNumber;
            return e;
        }
    }
}
