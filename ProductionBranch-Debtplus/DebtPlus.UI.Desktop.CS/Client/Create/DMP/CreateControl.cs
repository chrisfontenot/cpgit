﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.DMP
{
    internal partial class CreateControl : DebtPlus.UI.Desktop.CS.Client.Create.BaseControlWithOK
    {
        public CreateControl()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            telephoneNumberControl1.EditValueChanged += FormChanged;
            checkEdit_HousingProblems.EditValueChanged += FormChanged;
            lookUpEdit_method_first_contact.EditValueChanged += FormChanged;
            lookUpEdit_referred_by.EditValueChanged += FormChanged;
            checkEdit_NoTelephoneNumber.EditValueChanged += FormChanged;
        }

        private void UnRegisterHandlers()
        {
            telephoneNumberControl1.EditValueChanged -= FormChanged;
            checkEdit_HousingProblems.EditValueChanged -= FormChanged;
            lookUpEdit_method_first_contact.EditValueChanged -= FormChanged;
            lookUpEdit_referred_by.EditValueChanged -= FormChanged;
            checkEdit_NoTelephoneNumber.EditValueChanged -= FormChanged;
        }

        private readonly DataSet ds = new DataSet("ds");

        public DebtPlus.LINQ.TelephoneNumber HomePhone
        {
            get
            {
                return telephoneNumberControl1.GetCurrentValues();
            }
        }

        public Int32 referred_by
        {
            get
            {
                return DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_referred_by.EditValue).GetValueOrDefault(0);
            }
        }

        public Int32 method_first_contact
        {
            get
            {
                return DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_method_first_contact.EditValue).GetValueOrDefault(0);
            }
        }

        public bool MortgageProblem
        {
            get
            {
                return checkEdit_HousingProblems.Checked;
            }
        }

        public override void LoadForm()
        {
            base.LoadForm();

            UnRegisterHandlers();
            try
            {
                // Load the dataset with the list of referral codes
                lookUpEdit_referred_by.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList();
                lookUpEdit_referred_by.EditValue = DebtPlus.LINQ.Cache.referred_by.getDefault();

                // Load the dataset with the list of contact methods
                lookUpEdit_method_first_contact.Properties.DataSource = DebtPlus.LINQ.Cache.FirstContactType.getList();
                lookUpEdit_method_first_contact.EditValue = DebtPlus.LINQ.Cache.FirstContactType.getDefault();

                telephoneNumberControl1.EditValueCountry = DebtPlus.LINQ.Cache.country.getDefault();
            }

            catch(System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error initializing form");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        public override void ReadForm()
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (lookUpEdit_method_first_contact.EditValue == null || object.ReferenceEquals(lookUpEdit_method_first_contact.EditValue, DBNull.Value))
            {
                return true;
            }

            if (lookUpEdit_referred_by.EditValue == null || object.ReferenceEquals(lookUpEdit_referred_by.EditValue, DBNull.Value))
            {
                return true;
            }

            // If the user just said "create it" then accept it without the telephone number
            if (checkEdit_NoTelephoneNumber.Checked)
            {
                return false;
            }

            // Otherwise there must be a telephone number field for the search.
            return DebtPlus.Utils.Nulls.DStr(telephoneNumberControl1.EditValueNumber) == string.Empty;
        }
    }
}
