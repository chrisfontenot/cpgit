﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.DMP
{
    partial class FormCreateClient
    {
        //Form overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.SearchClientDMP1 = new DebtPlus.UI.Desktop.CS.Client.Create.DMP.SearchClientDMP();
            this.selectionControl1 = new DebtPlus.UI.Desktop.CS.Client.Create.selectionControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // SearchClientDMP1
            // 
            this.SearchClientDMP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchClientDMP1.Location = new System.Drawing.Point(0, 0);
            this.SearchClientDMP1.Name = "SearchClientDMP1";
            this.SearchClientDMP1.Size = new System.Drawing.Size(433, 321);
            this.SearchClientDMP1.TabIndex = 0;
            // 
            // selectionControl1
            // 
            this.selectionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectionControl1.Location = new System.Drawing.Point(0, 0);
            this.selectionControl1.Name = "selectionControl1";
            this.selectionControl1.Size = new System.Drawing.Size(433, 321);
            this.selectionControl1.TabIndex = 1;
            this.selectionControl1.Visible = false;
            // 
            // FormCreateClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 321);
            this.Controls.Add(this.SearchClientDMP1);
            this.Controls.Add(this.selectionControl1);
            this.Name = "FormCreateClient";
            this.Text = "Create a new client";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }
        private DebtPlus.UI.Desktop.CS.Client.Create.DMP.SearchClientDMP SearchClientDMP1;
        private DebtPlus.UI.Desktop.CS.Client.Create.selectionControl selectionControl1;
    }
}
