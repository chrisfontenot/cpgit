﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    internal class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        internal ArgParser() : base(new string[] {})
        {
        }

        protected override void OnUsage(string errorInfo)
        {
        }
    }
}
