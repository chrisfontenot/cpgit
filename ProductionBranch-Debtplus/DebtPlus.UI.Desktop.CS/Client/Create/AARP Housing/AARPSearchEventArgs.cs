﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing
{
    public class AARPSearchEventArgs : DebtPlus.UI.Desktop.CS.Client.Create.SearchEventArgs
    {
        public AARPSearchEventArgs()
            : base()
        {
            indicators = new List<int>();
        }

        public enum PreferredMethodTypes
        {
            Email,
            Text,
            Both
        }

        public static AARPSearchEventArgs MapToObject(SearchEventArgs e)
        {
            var aarp = new AARPSearchEventArgs() { 
                address = e.address, 
                applicant_name = e.applicant_name, 
                bankruptcyIssues = e.bankruptcyIssues, 
                cellTelephone = e.cellTelephone, 
                clientAppointment = e.clientAppointment, 
                clientID = e.clientID, 
                email = e.email, 
                existingClient = e.existingClient, 
                gender = e.gender, 
                homePhone = e.homePhone, 
                housingIssues = e.housingIssues, 
                immediateAppointment = e.immediateAppointment, 
                language = e.language, 
                methodFirstContact = e.methodFirstContact, 
                partnerCode = e.partnerCode,
                referralSource = e.referralSource,
                workTelephone = e.workTelephone,
                InitialProgram = e.InitialProgram 
            };

            return aarp;
        }

        public DateTime? dateOfBirth { get; set; }
        public int? job { get; set; }
        public int? militaryStatus { get; set; }
        public int? militaryService { get; set; }
        public int? militaryGrade { get; set; }
        public int? militaryDependent { get; set; }

        public int? housingStatus { get; set; }
        public List<int> indicators { get; set; }
        //public PreferredMethodTypes preferredMethod { get; set; } // use indicators instead, per Varsha 10/14/2014

        public bool? IsAarpClient { get; set; }
        public bool? Btw50ProgramRequired { get; set; }
        public bool? SnapProgramRequired { get; set; }
        public bool? MediaFollowup { get; set; }
        public int? HousingSituation { get; set; }
        public bool? EmailOptIn { get; set; }
        public bool? TextOptIn { get; set; }
        public int PreferredMethod { get; set; }
        public int CallReason { get; set; }
        public int CallResolution { get; set; }
        //public int InitialProgram { get; set; }
        public string AarpCode { get; set; }
    }
}
