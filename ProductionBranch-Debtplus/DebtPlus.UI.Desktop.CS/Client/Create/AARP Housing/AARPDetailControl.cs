﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.Events;

namespace DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing
{
    internal partial class AARPDetailControl : DebtPlus.UI.Desktop.CS.Client.Create.BaseControl
    {
        // The values that were given to the GetCurrentValues routine
        private AARPSearchEventArgs ClientParameters;
        bool isInDesignMode;
        private Size myOrigSize;
        // bool defaultsSet = false; <<<-- declared but never assigned.

        // INDICATOR ID VALUES MUST MATCH CORRESPONDING ENTRIES IN DATABASE
        const int INDICATOR_ID_CURRENTAARP = 236;
        const int INDICATOR_ID_INTRSTD_BTW50 = 237;
        const int INDICATOR_ID_INTRSTD_SNAP = 238;
        const int INDICATOR_ID_MEDIA_FOLLOWUP = 239;
        const int INDICATOR_ID_EMAIL_OPTIN = 240;
        const int INDICATOR_ID_TEXT_OPTIN = 241;

        // preferred method indicators
        const int INDICATOR_ID_PREFERREDMETHOD_EMAIL = 242;
        const int INDICATOR_ID_PREFERREDMETHOD_TEXT = 243;
        const int INDICATOR_ID_PREFERREDMETHOD_BOTH = 244;

        List<int> preferredMethodIndicators = new List<int>(new int[]{
                                                            INDICATOR_ID_PREFERREDMETHOD_EMAIL, 
                                                            INDICATOR_ID_PREFERREDMETHOD_TEXT,
                                                            INDICATOR_ID_PREFERREDMETHOD_BOTH
        });

        // call reason indicators
        const int INDICATOR_ID_SCHED_NEW_APPT = 245;
        const int INDICATOR_ID_RESCHED_APPT = 246;
        const int INDICATOR_ID_CANCEL_APPT = 247;

        List<int> callReasonIndicators = new List<int>( new int[]{
                                                            INDICATOR_ID_SCHED_NEW_APPT, 
                                                            INDICATOR_ID_RESCHED_APPT,
                                                            INDICATOR_ID_CANCEL_APPT
        });


        //1	Answered General Question
        //2	Call Drop
        //3	Cancel Availability
        //4	Colonial Penn
        //5	Referred to HUD (Rent, Repair)
        //6	Remove from list
        //7	Transferred to AARP FDN
        //8	Transferred to Counselor
        //9	Scheduled a New Appointment
        //10	Rescheduled Appointment
        //11	Cancel Availability

        // call resolution indicators
        const int CallResolutionAnsweredGeneralQuestion = 1;
        const int CallResolutionCallDrop = 2;
        const int CallResolutionCancelAvailability = 3;
        const int CallResolutionColonialPenn = 4;
        const int CallResolutionReferredToHUD = 5;
        const int CallResolutionRemoveFromList = 6;
        const int CallResolutionTransferredToAarpFdn = 7;
        const int CallResolutionTransferredToCounselor = 8;
        const int CallResolutionScheduledNewAppointment = 9;
        const int CallResolutionRescheduledAppointment = 10;

        List<int> NewClientCallResolution = new List<int>()
        {
            CallResolutionAnsweredGeneralQuestion,
            CallResolutionCallDrop,
            CallResolutionCancelAvailability,
            CallResolutionColonialPenn,
            CallResolutionReferredToHUD,
            CallResolutionRemoveFromList,
            CallResolutionTransferredToAarpFdn,
            CallResolutionTransferredToCounselor,
            CallResolutionScheduledNewAppointment,
            CallResolutionRescheduledAppointment
        };

        const int CallReasonScheduleNewappointment = 1;

        // initial program requested indicators
        const int INDICATOR_ID_INIT_HOUS_SOLN_CTR = 248;

        List<int> initialPrgmIndicators = new List<int>(new int[]{
                                                            INDICATOR_ID_INIT_HOUS_SOLN_CTR
        });

        const int INDICATOR_ID_OTHER_BTW50 = 249;
        const int INDICATOR_ID_OTHER_SNAP = 250;

        public AARPDetailControl()
            : base()
        {
            isInDesignMode = LicenseManager.UsageMode == LicenseUsageMode.Designtime; // || Debugger.IsAttached == true;
            InitializeComponent();
            RegisterHandlers();
            myOrigSize = this.Size;

            // Load the lookups with the items
            if (!isInDesignMode)
            {
                lookUpEdit_gender.Properties.DataSource = DebtPlus.LINQ.Cache.GenderType.getList();
                lookUpEdit_language.Properties.DataSource = DebtPlus.LINQ.Cache.AttributeType.getLanguageList();
                lookUpEdit_referred_by.Properties.DataSource = DebtPlus.LINQ.Cache.referred_by.getList().Where(x => x.description.ToLower().Contains("aarp")).ToList();

                lookUpEdit_job.Properties.DataSource = DebtPlus.LINQ.Cache.job_description.getAarpList();

                // Military fields
                milDependent.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryDependentType.getList();
                milGrade.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryGradeType.getList();
                milService.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryServiceType.getList();
                milStatus.Properties.DataSource = DebtPlus.LINQ.Cache.MilitaryStatusType.getList();

                lookUpEdit_currentAARPClient.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList();
                lookUpEdit_interestBTW50Prgm.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList();
                lookUpEdit_interestSNAPPrgm.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList();
                lookUpEdit_mediaFollowUp.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList();

                lookUpEdit_housingStatus.Properties.DataSource = DebtPlus.LINQ.Cache.Housing_StatusType.getAarpList();
                lookUpEdit_emailOptIn.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList();
                lookUpEdit_textOptIn.Properties.DataSource = DebtPlus.LINQ.InMemory.YesNos.getList();
                lookUpEdit_CallResolution.Properties.DataSource = DebtPlus.LINQ.Cache.CallResolution.getList();

                lookUpEdit_preferredMethod.Properties.DataSource = DebtPlus.LINQ.Cache.ContactPreferredMethod.getList();

                var initialProgram = DebtPlus.LINQ.Cache.InitialProgram.getList();
                lookUpEdit_initialPrgmRequested.Properties.DataSource = initialProgram;

                if (initialProgram != null && initialProgram.Count > 0)
                {
                    lookUpEdit_initialPrgmRequested.EditValue = initialProgram[0].Id;
                }
            }
        }

        public bool IsExistingClient { get; set; }

        /// <summary>
        /// Returns display text for selected indicator.
        /// To be used if display text is different from description in database
        /// </summary>
        /// <param name="indicator_id">int</param>
        /// <returns>string</returns>
        private string getDisplayText(int indicator_id)
        {
            switch (indicator_id)
            {
                case INDICATOR_ID_PREFERREDMETHOD_EMAIL:
                    return "Email";
                case INDICATOR_ID_PREFERREDMETHOD_TEXT:
                    return "Text";
                case INDICATOR_ID_PREFERREDMETHOD_BOTH:
                    return "Both";

                case INDICATOR_ID_SCHED_NEW_APPT:
                    return "Schedule a New Appointment";
                case INDICATOR_ID_RESCHED_APPT:
                    return "Reschedule Appointment";
                case INDICATOR_ID_CANCEL_APPT:
                    return "Cancel Appointment";

                /*case INDICATOR_ID_INIT_HOUS_SOLN_CTR:
                    return "Housing Solutions Center";*/

                default:
                    return null;
            }
        }

        private void RegisterHandlers()
        {
            nameRecordControl_name.TextChanged += FormChanged;
            emailRecordControl_email.TextChanged += FormChanged;
            addressRecordControl1.TextChanged += FormChanged;
            lookUpEdit_referred_by.TextChanged += FormChanged;
            lookUpEdit_language.TextChanged += FormChanged;
            lookUpEdit_CallResolution.TextChanged += FormChanged;
            lookUpEdit_gender.TextChanged += FormChanged;
            lookUpEdit_referred_by.EditValueChanged += lookUpEdit_referred_by_EditValueChanged;
            telephoneNumberRecordControl_cell.TextChanged += FormChanged;
            telephoneNumberRecordControl_day.TextChanged += FormChanged;
            telephoneNumberRecordControl_evening.TextChanged += FormChanged;
            simpleButton_NoAppt.Click += simpleButton_NoAppt_Click;
            simpleButton_ImmediateAppt.Click += simpleButton_ImmediateAppt_Click;
            simpleButton_ScheduledAppt.Click += simpleButton_ScheduledAppt_Click;
            simpleButton_RescheduleAppt.Click += simpleButton_RescheduleAppt_Click;
            simpleButton_CancelAppt.Click += simpleButton_CancelAppt_Click;
            simpleButton_ApptHistory.Click += simpleButton_ApptHistory_Click;
            // resize this form automatically
            this.VisibleChanged += AdjustContainerSize;

            // new fields
            dateEdit_dob.EditValueChanged += FormChanged;
            popupContainerEdit_Military.EditValueChanged += FormChanged;
            lookUpEdit_job.EditValueChanged += FormChanged;

            lookUpEdit_currentAARPClient.EditValueChanged += FormChanged;
            lookUpEdit_interestBTW50Prgm.EditValueChanged += FormChanged;
            lookUpEdit_interestSNAPPrgm.EditValueChanged += FormChanged;
            lookUpEdit_mediaFollowUp.EditValueChanged += FormChanged;

            lookUpEdit_housingStatus.EditValueChanged += FormChanged;
            lookUpEdit_emailOptIn.EditValueChanged += FormChanged;
            lookUpEdit_textOptIn.EditValueChanged += FormChanged;
            lookUpEdit_preferredMethod.EditValueChanged += FormChanged;

            lookUpEdit_callReason.EditValueChanged += FormChanged;
            lookUpEdit_initialPrgmRequested.EditValueChanged += FormChanged;

            popupContainerEdit_Military.QueryDisplayText += popupContainerEdit_Military_QueryDisplayText;
            popupContainerEdit_Military.QueryResultValue += popupContainerEdit_Military_QueryResultValue;
        }

        void popupContainerEdit_Military_QueryDisplayText(object sender, DevExpress.XtraEditors.Controls.QueryDisplayTextEventArgs e)
        {
            if (e.EditValue != null && e.EditValue != System.DBNull.Value)
            {
                var arg = Convert.ToInt32(e.EditValue);
                var q = DebtPlus.LINQ.Cache.MilitaryStatusType.getList().Find(s => s.Id == arg);
                if (q != null)
                {
                    e.DisplayText = q.description;
                }
            }
        }

        void popupContainerEdit_Military_QueryResultValue(object sender, DevExpress.XtraEditors.Controls.QueryResultValueEventArgs e)
        {
            popupContainerEdit_Military.EditValue = milStatus.EditValue;
            e.Value = milStatus.EditValue;
        }

        private void UnRegisterHandlers()
        {
            nameRecordControl_name.TextChanged -= FormChanged;
            emailRecordControl_email.TextChanged -= FormChanged;
            addressRecordControl1.TextChanged -= FormChanged;
            lookUpEdit_referred_by.TextChanged -= FormChanged;
            lookUpEdit_language.TextChanged -= FormChanged;
            lookUpEdit_CallResolution.TextChanged -= FormChanged;
            lookUpEdit_gender.TextChanged -= FormChanged;
            lookUpEdit_referred_by.EditValueChanged -= lookUpEdit_referred_by_EditValueChanged;
            telephoneNumberRecordControl_cell.TextChanged -= FormChanged;
            telephoneNumberRecordControl_day.TextChanged -= FormChanged;
            telephoneNumberRecordControl_evening.TextChanged -= FormChanged;
            simpleButton_NoAppt.Click -= simpleButton_NoAppt_Click;
            simpleButton_ImmediateAppt.Click -= simpleButton_ImmediateAppt_Click;
            simpleButton_ScheduledAppt.Click -= simpleButton_ScheduledAppt_Click;
            simpleButton_RescheduleAppt.Click -= simpleButton_RescheduleAppt_Click;
            simpleButton_CancelAppt.Click -= simpleButton_CancelAppt_Click;
            simpleButton_ApptHistory.Click -= simpleButton_ApptHistory_Click;

            this.VisibleChanged -= AdjustContainerSize;

            // new fields
            dateEdit_dob.EditValueChanged -= FormChanged;
            popupContainerEdit_Military.EditValueChanged -= FormChanged;
            lookUpEdit_job.EditValueChanged -= FormChanged;

            lookUpEdit_currentAARPClient.EditValueChanged -= FormChanged;
            lookUpEdit_interestBTW50Prgm.EditValueChanged -= FormChanged;
            lookUpEdit_interestSNAPPrgm.EditValueChanged -= FormChanged;
            lookUpEdit_mediaFollowUp.EditValueChanged -= FormChanged;

            lookUpEdit_housingStatus.EditValueChanged -= FormChanged;
            lookUpEdit_emailOptIn.EditValueChanged -= FormChanged;
            lookUpEdit_textOptIn.EditValueChanged -= FormChanged;
            lookUpEdit_preferredMethod.EditValueChanged -= FormChanged;

            lookUpEdit_callReason.EditValueChanged -= FormChanged;
            lookUpEdit_initialPrgmRequested.EditValueChanged -= FormChanged;

            popupContainerEdit_Military.QueryResultValue -= popupContainerEdit_Military_QueryResultValue;
            popupContainerEdit_Military.QueryDisplayText -= popupContainerEdit_Military_QueryDisplayText;
        }

        public event EventHandler NoAppointment;
        protected void RaiseNoAppointment(EventArgs e)
        {
            var evt = NoAppointment;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnNoAppointment(EventArgs e)
        {
            RaiseNoAppointment(e);
        }

        public event EventHandler ImmediateAppointment;
        protected void RaiseImmediateAppointment(EventArgs e)
        {
            var evt = ImmediateAppointment;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnImmediateAppointment(EventArgs e)
        {
            RaiseImmediateAppointment(e);
        }

        public event EventHandler ScheduledAppointment;
        protected void RaiseScheduledAppointment(EventArgs e)
        {
            var evt = ScheduledAppointment;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnScheduledAppointment(EventArgs e)
        {
            RaiseScheduledAppointment(e);
        }

        public event EventHandler RescheduleAppointment;
        protected void RaiseRescheduleAppointment(EventArgs e)
        {
            var evt = RescheduleAppointment;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnRescheduleAppointment(EventArgs e)
        {
            RaiseRescheduleAppointment(e);
        }

        public event EventHandler CancelAppointment;
        protected void RaiseCancelAppointment(EventArgs e)
        {
            var evt = CancelAppointment;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnCancelAppointment(EventArgs e)
        {
            RaiseCancelAppointment(e);
        }

        public event EventHandler AppointmentHistory;
        protected void RaiseAppointmentHistory(EventArgs e)
        {
            var evt = AppointmentHistory;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnAppointmentHistory(EventArgs e)
        {
            RaiseAppointmentHistory(e);
        }

        /// <summary>
        /// Load the controls with the parameters for the client
        /// </summary>
        public virtual void SetCurrentValues(AARPSearchEventArgs e)
        {
            // Save the values for the GetSearchEventArgs since we only augment the arguments, not replace them.
            ClientParameters = e;

            UnRegisterHandlers();
            try
            {
                var intItem = e.InitialProgram;
                if (intItem.HasValue)
                {
                    lookUpEdit_initialPrgmRequested.EditValue = intItem;
                }

                intItem = e.language;
                if (intItem.HasValue)
                {
                    lookUpEdit_language.EditValue = intItem;
                }
                else
                {
                    intItem = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage();
                    if (intItem.HasValue)
                    {
                        lookUpEdit_language.EditValue = intItem;
                    }
                }

                intItem = e.gender;
                if (intItem.HasValue)
                {
                    lookUpEdit_gender.EditValue = intItem;
                }
                else
                {
                    intItem = DebtPlus.LINQ.Cache.GenderType.getDefault();
                    if (intItem.HasValue)
                    {
                        lookUpEdit_gender.EditValue = intItem;
                    }
                }

                if (e.applicant_name != null) nameRecordControl_name.SetValues(e.applicant_name);
                if (e.email != null) emailRecordControl_email.SetCurrentValues(e.email);
                if (e.address != null) addressRecordControl1.SetCurrentValues(e.address);
                if (e.cellTelephone != null) telephoneNumberRecordControl_cell.SetCurrentValues(e.cellTelephone);
                if (e.workTelephone != null) telephoneNumberRecordControl_day.SetCurrentValues(e.workTelephone);
                if (e.homePhone != null) telephoneNumberRecordControl_evening.SetCurrentValues(e.homePhone);

                // set new fields here
                if (e.job.HasValue) lookUpEdit_job.EditValue = e.job;
                if (e.dateOfBirth.HasValue) dateEdit_dob.EditValue = e.dateOfBirth;

                if (e.militaryStatus.HasValue) milStatus.EditValue = e.militaryStatus;
                if (e.militaryService.HasValue) milService.EditValue = e.militaryService;
                if (e.militaryGrade.HasValue) milGrade.EditValue = e.militaryGrade;
                if (e.militaryDependent.HasValue) milDependent.EditValue = e.militaryDependent;

                txtAarpCode.Text = e.AarpCode;

                lookUpEdit_currentAARPClient.EditValue = ReadBoolean(e.IsAarpClient);

                lookUpEdit_emailOptIn.EditValue = ReadBoolean(e.EmailOptIn);
                lookUpEdit_gender.EditValue= e.gender;
                if (e.HousingSituation != null)
                {
                    lookUpEdit_housingStatus.EditValue = e.HousingSituation;
                }
                if (e.InitialProgram != null)
                {
                    lookUpEdit_initialPrgmRequested.EditValue = e.InitialProgram;
                }
                else
                {
                    lookUpEdit_initialPrgmRequested.EditValue = 1;
                }
                lookUpEdit_interestBTW50Prgm.EditValue = ReadBoolean(e.Btw50ProgramRequired);
                lookUpEdit_interestSNAPPrgm.EditValue = ReadBoolean(e.SnapProgramRequired);
                lookUpEdit_language.EditValue = e.language;
                lookUpEdit_mediaFollowUp.EditValue = ReadBoolean(e.MediaFollowup);
                lookUpEdit_preferredMethod.EditValue = e.PreferredMethod;
                lookUpEdit_referred_by.EditValue = e.referralSource;
                lookUpEdit_textOptIn.EditValue = ReadBoolean(e.TextOptIn);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private int? ReadBoolean(bool? boolValue)
        {
            int? value = null;

            if (boolValue != null)
            {
                if (boolValue == true)
                {
                    value = 0;
                }
                else
                {
                    value = 1;
                }
            }
            return value;
        }

        private void ValidateData()
        {

            if (lookUpEdit_CallResolution.EditValue == null || lookUpEdit_CallResolution.EditValue.ToString() == "0" ||
                lookUpEdit_referred_by.EditValue == null)
            {
                simpleButton_NoAppt.Enabled = false;
                simpleButton_ImmediateAppt.Enabled = false;
                simpleButton_ScheduledAppt.Enabled = false;
                simpleButton_RescheduleAppt.Enabled = false;
                simpleButton_CancelAppt.Enabled = false;
            }

            else
            {
                simpleButton_NoAppt.Enabled = true;
                simpleButton_ImmediateAppt.Enabled = true;
                simpleButton_ScheduledAppt.Enabled = true;
                simpleButton_RescheduleAppt.Enabled = true;
                simpleButton_CancelAppt.Enabled = true;
            }

        }

        /// <summary>
        /// When the referral source changes then update the partner validation code
        /// </summary>
        private void lookUpEdit_referred_by_EditValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                //SetPartnerValidation();
            }
            finally
            {
                RegisterHandlers();
            }
        }

               /// <summary>
        /// Retrieve the search parameter items
        /// </summary>
        /// <returns></returns>
        public virtual AARPSearchEventArgs GetCurrentValues()
        {
            System.Diagnostics.Debug.Assert(ClientParameters != null);

            ClientParameters.referralSource = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_referred_by.EditValue);
            ClientParameters.partnerCode = string.Empty; // DebtPlus.Utils.Nulls.v_String(textEdit_partner_code.EditValue);
            ClientParameters.applicant_name = nameRecordControl_name.GetCurrentValues();
            ClientParameters.email = emailRecordControl_email.GetCurrentValues();
            ClientParameters.gender = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_gender.EditValue);
            ClientParameters.language = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_language.EditValue);
            ClientParameters.cellTelephone = telephoneNumberRecordControl_cell.GetCurrentValues();
            ClientParameters.homePhone = telephoneNumberRecordControl_evening.GetCurrentValues();
            ClientParameters.workTelephone = telephoneNumberRecordControl_day.GetCurrentValues();
            ClientParameters.address = addressRecordControl1.GetCurrentValues();

            // get new fields here
            DateTime dob;
            if (dateEdit_dob.EditValue != null  && DateTime.TryParse(dateEdit_dob.EditValue.ToString(), out dob))
            {
                ClientParameters.dateOfBirth = dob;
            }

            ClientParameters.militaryStatus = DebtPlus.Utils.Nulls.v_Int32(milStatus.EditValue);
            ClientParameters.militaryService = DebtPlus.Utils.Nulls.v_Int32(milService.EditValue);
            ClientParameters.militaryGrade = DebtPlus.Utils.Nulls.v_Int32(milGrade.EditValue);
            ClientParameters.militaryDependent = DebtPlus.Utils.Nulls.v_Int32(milDependent.EditValue);

            ClientParameters.job = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_job.EditValue);

            ClientParameters.housingStatus = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_housingStatus.EditValue);

            ClientParameters.IsAarpClient = GetBooleanValue(lookUpEdit_currentAARPClient);
            ClientParameters.Btw50ProgramRequired = GetBooleanValue(lookUpEdit_interestBTW50Prgm);
            ClientParameters.SnapProgramRequired = GetBooleanValue(lookUpEdit_interestSNAPPrgm);
            ClientParameters.MediaFollowup = GetBooleanValue(lookUpEdit_mediaFollowUp);
            ClientParameters.HousingSituation = GetLookupValue(lookUpEdit_housingStatus);
            ClientParameters.EmailOptIn = GetBooleanValue(lookUpEdit_emailOptIn);
            ClientParameters.TextOptIn = GetBooleanValue(lookUpEdit_textOptIn);
            ClientParameters.PreferredMethod = GetLookupValue(lookUpEdit_preferredMethod);
            ClientParameters.CallReason = GetLookupValue(lookUpEdit_callReason);
            ClientParameters.InitialProgram = GetLookupValue(lookUpEdit_initialPrgmRequested);
            ClientParameters.CallResolution = GetLookupValue(lookUpEdit_CallResolution);
            ClientParameters.AarpCode = txtAarpCode.Text.Trim();

            return ClientParameters;
        }

        private int GetLookupValue(DevExpress.XtraEditors.LookUpEdit lookUpEdit)
        {
            int selection = 0;
            /* Due to validation, edit values should not be null */
            if (lookUpEdit != null && lookUpEdit.EditValue != null)
            {
                selection = (int)lookUpEdit.EditValue;
            }
            return selection;
        }

        private bool? GetBooleanValue(DevExpress.XtraEditors.LookUpEdit lookUpEdit)
        {
            bool? selection = null;

            if (lookUpEdit != null && lookUpEdit.EditValue != null)
            {
                int selecteValue = (int)lookUpEdit.EditValue;

                // Yes = 1, No = 0
                selection = (selecteValue == 1) ? true : false;
            }

            return selection;            
        }

        /// <summary>
        /// returns DropDownList for specified indicator
        /// </summary>
        /// <param name="indicator_id">int</param>
        /// <returns>LookUpEdit</returns>
        private DevExpress.XtraEditors.LookUpEdit GetDropDownForIndicator(int indicator_id)
        {
            switch (indicator_id)
            {
                case INDICATOR_ID_CURRENTAARP:
                    return lookUpEdit_currentAARPClient;
                case INDICATOR_ID_INTRSTD_BTW50:
                    return lookUpEdit_interestBTW50Prgm;
                case INDICATOR_ID_INTRSTD_SNAP:
                    return lookUpEdit_interestSNAPPrgm;
                case INDICATOR_ID_MEDIA_FOLLOWUP:
                    return lookUpEdit_mediaFollowUp;

                case INDICATOR_ID_EMAIL_OPTIN:
                    return lookUpEdit_emailOptIn;
                case INDICATOR_ID_TEXT_OPTIN:
                    return lookUpEdit_textOptIn;

                // preferred method indicators
                case INDICATOR_ID_PREFERREDMETHOD_EMAIL:
                case INDICATOR_ID_PREFERREDMETHOD_TEXT:
                case INDICATOR_ID_PREFERREDMETHOD_BOTH:
                    return lookUpEdit_preferredMethod;

                // call reason indicators
                case INDICATOR_ID_RESCHED_APPT:
                case INDICATOR_ID_SCHED_NEW_APPT:
                case INDICATOR_ID_CANCEL_APPT:
                    return lookUpEdit_callReason;

                case INDICATOR_ID_INIT_HOUS_SOLN_CTR:
                    return lookUpEdit_currentAARPClient;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Adds indicator to client according to selected Yes/No/Blank value
        /// </summary>
        /// <param name="client_params">AARPSearchEventArgs</param>
        /// <param name="indicator_id">int</param>
        /// <param name="value">object</param>
        private static void GetIndicatorValue(AARPSearchEventArgs client_params, int indicator_id, object value)
        {
            bool result;
            bool parsed = bool.TryParse(value.ToString(), out result);
            bool changed = false;

            if (parsed && result) // parsed successfully, and value == true
            {
                // insert/update indicator in client_params
                if (!client_params.indicators.Contains(indicator_id))
                {
                    client_params.indicators.Add(indicator_id);
                    changed = true;
                }
            }
            else // either unable to parse, or value == false
            {
                // remove indicator in client_params
                changed = client_params.indicators.Remove(indicator_id);                
            }

            if (changed)
            {
                // TODO notify if indicator has changed?
            }
        }

        /// <summary>
        /// Are all required fields set properly?
        /// </summary>
        protected virtual bool HasErrors()
        {
            // The referral source is required
            if (lookUpEdit_referred_by.EditValue == null)
            {
                return true;
            }

            // The gender is required
            if (lookUpEdit_gender.EditValue == null)
            {
                return true;
            }

            // The language is required
            if (lookUpEdit_language.EditValue == null)
            {
                return true;
            }

            // One of the telephone numbers is required
            if (telephoneNumberRecordControl_day.GetCurrentValues().IsEmpty())
            {
                if (telephoneNumberRecordControl_cell.GetCurrentValues().IsEmpty())
                {
                    if (telephoneNumberRecordControl_evening.GetCurrentValues().IsEmpty())
                    {
                        return true;
                    }
                }
            }

            // The address is required
            if (addressRecordControl1.GetCurrentValues().IsEmpty())
            {
                return true;
            }

            // The name is required
            if (nameRecordControl_name.GetCurrentValues().IsEmpty())
            {
                return true;
            }

            // add validation for new controls
            // 1st column
            if ((lookUpEdit_currentAARPClient.EditValue == null) ||
                (lookUpEdit_interestBTW50Prgm.EditValue == null) ||
                (lookUpEdit_interestSNAPPrgm.EditValue == null) ||
                (lookUpEdit_mediaFollowUp.EditValue == null))
            {
                return true;
            }

            // 2nd column
            if ((lookUpEdit_housingStatus.EditValue == null) ||
                (lookUpEdit_emailOptIn.EditValue == null) ||
                (lookUpEdit_textOptIn.EditValue == null) ||
                (lookUpEdit_preferredMethod.EditValue == null))
            {
                return true;
            }

            // 3rd column
            if ((lookUpEdit_callReason.EditValue == null) ||
                (lookUpEdit_initialPrgmRequested.EditValue == null))
            {
                return true;
            }

            if (lookUpEdit_CallResolution.EditValue == null)
            {
                return true;
            }

            // contact preferred method validation
            if (lookUpEdit_emailOptIn.EditValue.ToString().ToLower() == "true")
            {
                if (emailRecordControl_email.GetCurrentValues().IsEmpty())
                    return true;
            }
            else
            {
                // do not allow preferred method if not opted in
                if (lookUpEdit_preferredMethod.EditValue.ToString() == INDICATOR_ID_PREFERREDMETHOD_EMAIL.ToString() ||
                    lookUpEdit_preferredMethod.EditValue.ToString() == INDICATOR_ID_PREFERREDMETHOD_BOTH.ToString())
                    return true;
            }

            // require cell number for text opt in
            if (lookUpEdit_textOptIn.EditValue.ToString().ToLower() == "true")
            {
                if (telephoneNumberRecordControl_cell.GetCurrentValues().IsEmpty())
                {
                    return true;
                }
            }
            else
            {
                // do not allow preferred method if not opted in
                if (lookUpEdit_preferredMethod.EditValue.ToString() == INDICATOR_ID_PREFERREDMETHOD_TEXT.ToString() ||
                    lookUpEdit_preferredMethod.EditValue.ToString() == INDICATOR_ID_PREFERREDMETHOD_BOTH.ToString())
                    return true;
            }
            // Everything is valid now.
            return false;
        }

        /// <summary>
        /// Process a change in the KeyField form values
        /// </summary>
        protected void FormChanged(object sender, EventArgs e)
        {
            ValidateData();
        }
        
        private void simpleButton_NoAppt_Click(object sender, EventArgs e)
        {
            OnNoAppointment(EventArgs.Empty);
        }

        private void simpleButton_ImmediateAppt_Click(object sender, EventArgs e)
        {
            OnImmediateAppointment(EventArgs.Empty);
        }

        private void simpleButton_ScheduledAppt_Click(object sender, EventArgs e)
        {
            OnScheduledAppointment(EventArgs.Empty);
        }

        private void simpleButton_RescheduleAppt_Click(object sender, EventArgs e)
        {
            OnRescheduleAppointment(EventArgs.Empty);
        }

        private void simpleButton_CancelAppt_Click(object sender, EventArgs e)
        {
            OnCancelAppointment(EventArgs.Empty);
        }

        private void simpleButton_ApptHistory_Click(object sender, EventArgs e)
        {
            OnAppointmentHistory(EventArgs.Empty);
        }

        private Size origParentSize = new System.Drawing.Size(748, 264);

        private void AdjustContainerSize(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                origParentSize = Parent.ClientSize;
                Parent.ClientSize = myOrigSize;

                LoadControlsBasedOnClientStatus();
            }
            else
            {
                if (origParentSize != null)
                    Parent.ClientSize = origParentSize;
            }
        }

        private void LoadControlsBasedOnClientStatus()
        {
            List<LINQ.call_reason> callReasons = null;
            List<LINQ.call_resolution> callResolutions = null;
            
            callReasons = DebtPlus.LINQ.Cache.CallReason.getList();
            callResolutions = DebtPlus.LINQ.Cache.CallResolution.getList();
            
            List<int> callReasonForExistingClient = new List<int>()
            {
                2, //Cancel an Appointment
                3  //Reschedule an Existing Appointment
            };

            List<int> callResolutionForExistingClient = new List<int>()
            {
                3, //Cancel Availability
                //9, //Scheduled Appointment
                10 //Rescheduled Appointment
            };

            if (!IsExistingClient)
            {
                callResolutions = callResolutions.Where(cr => callResolutionForExistingClient.Contains(cr.oID) == false).ToList(); 
            }
            lookUpEdit_callReason.Properties.DataSource = callReasons;

            lookUpEdit_CallResolution.Properties.DataSource = callResolutions;
        }
    }
}
