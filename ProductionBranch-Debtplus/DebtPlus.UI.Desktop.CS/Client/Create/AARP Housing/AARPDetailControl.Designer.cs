﻿namespace DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing
{
    partial class AARPDetailControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        protected void InitializeComponent()
        {
            this.lookUpEdit_referred_by = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_language = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_gender = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.telephoneNumberRecordControl_evening = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_day = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.telephoneNumberRecordControl_cell = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton_ScheduledAppt = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_ImmediateAppt = new DevExpress.XtraEditors.SimpleButton();
            this.emailRecordControl_email = new DebtPlus.Data.Controls.EmailRecordControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.nameRecordControl_name = new DebtPlus.Data.Controls.NameRecordControl();
            this.addressRecordControl1 = new DebtPlus.Data.Controls.AddressRecordControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl12 = new DevExpress.XtraEditors.LabelControl();
            this.dateEdit_dob = new DevExpress.XtraEditors.DateEdit();
            this.labelControl14 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_currentAARPClient = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl15 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_interestBTW50Prgm = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl16 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_interestSNAPPrgm = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl17 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_mediaFollowUp = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl13 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_preferredMethod = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl18 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_textOptIn = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl19 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_emailOptIn = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl20 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_housingStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl21 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_initialPrgmRequested = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl22 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_callReason = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl23 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_job = new DevExpress.XtraEditors.LookUpEdit();
            this.popupContainerControl_Military = new DevExpress.XtraEditors.PopupContainerControl();
            this.milDependent = new DevExpress.XtraEditors.LookUpEdit();
            this.milGrade = new DevExpress.XtraEditors.LookUpEdit();
            this.milService = new DevExpress.XtraEditors.LookUpEdit();
            this.milStatus = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl24 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl25 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl26 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl27 = new DevExpress.XtraEditors.LabelControl();
            this.popupContainerEdit_Military = new DevExpress.XtraEditors.PopupContainerEdit();
            this.labelControl28 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_CallResolution = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl29 = new DevExpress.XtraEditors.LabelControl();
            this.txtAarpCode = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_NoAppt = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_RescheduleAppt = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_CancelAppt = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_ApptHistory = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_referred_by.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_language.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_gender.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_evening)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_day)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_cell)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_email.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_dob.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_dob.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_currentAARPClient.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_interestBTW50Prgm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_interestSNAPPrgm.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_mediaFollowUp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_preferredMethod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_textOptIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_emailOptIn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_housingStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_initialPrgmRequested.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_callReason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_job.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_Military)).BeginInit();
            this.popupContainerControl_Military.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.milDependent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.milGrade.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.milService.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.milStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit_Military.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_CallResolution.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAarpCode.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(644, 389);
            this.simpleButton_Cancel.TabIndex = 28;
            // 
            // lookUpEdit_referred_by
            // 
            this.lookUpEdit_referred_by.Location = new System.Drawing.Point(97, 3);
            this.lookUpEdit_referred_by.Name = "lookUpEdit_referred_by";
            this.lookUpEdit_referred_by.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_referred_by.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_referred_by.Properties.DisplayMember = "description";
            this.lookUpEdit_referred_by.Properties.NullText = "";
            this.lookUpEdit_referred_by.Properties.ShowFooter = false;
            this.lookUpEdit_referred_by.Properties.ShowHeader = false;
            this.lookUpEdit_referred_by.Properties.ValueMember = "Id";
            this.lookUpEdit_referred_by.Size = new System.Drawing.Size(404, 20);
            this.lookUpEdit_referred_by.TabIndex = 1;
            // 
            // lookUpEdit_language
            // 
            this.lookUpEdit_language.Location = new System.Drawing.Point(327, 54);
            this.lookUpEdit_language.Name = "lookUpEdit_language";
            this.lookUpEdit_language.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_language.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Attribute", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_language.Properties.DisplayMember = "Attribute";
            this.lookUpEdit_language.Properties.NullText = "";
            this.lookUpEdit_language.Properties.ShowFooter = false;
            this.lookUpEdit_language.Properties.ShowHeader = false;
            this.lookUpEdit_language.Properties.ValueMember = "Id";
            this.lookUpEdit_language.Size = new System.Drawing.Size(174, 20);
            this.lookUpEdit_language.TabIndex = 4;
            // 
            // lookUpEdit_gender
            // 
            this.lookUpEdit_gender.Location = new System.Drawing.Point(97, 53);
            this.lookUpEdit_gender.Name = "lookUpEdit_gender";
            this.lookUpEdit_gender.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_gender.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_gender.Properties.DisplayMember = "description";
            this.lookUpEdit_gender.Properties.NullText = "";
            this.lookUpEdit_gender.Properties.ShowFooter = false;
            this.lookUpEdit_gender.Properties.ShowHeader = false;
            this.lookUpEdit_gender.Properties.ValueMember = "Id";
            this.lookUpEdit_gender.Size = new System.Drawing.Size(150, 20);
            this.lookUpEdit_gender.TabIndex = 3;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(4, 7);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(39, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Referral";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(271, 58);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(47, 13);
            this.labelControl2.TabIndex = 8;
            this.labelControl2.Text = "Language";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(4, 58);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(35, 13);
            this.labelControl3.TabIndex = 6;
            this.labelControl3.Text = "Gender";
            // 
            // telephoneNumberRecordControl_evening
            // 
            this.telephoneNumberRecordControl_evening.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.telephoneNumberRecordControl_evening.ErrorIcon = null;
            this.telephoneNumberRecordControl_evening.ErrorText = "";
            this.telephoneNumberRecordControl_evening.Location = new System.Drawing.Point(579, 107);
            this.telephoneNumberRecordControl_evening.Name = "telephoneNumberRecordControl_evening";
            this.telephoneNumberRecordControl_evening.Size = new System.Drawing.Size(191, 20);
            this.telephoneNumberRecordControl_evening.TabIndex = 11;
            // 
            // telephoneNumberRecordControl_day
            // 
            this.telephoneNumberRecordControl_day.ErrorIcon = null;
            this.telephoneNumberRecordControl_day.ErrorText = "";
            this.telephoneNumberRecordControl_day.Location = new System.Drawing.Point(327, 106);
            this.telephoneNumberRecordControl_day.Name = "telephoneNumberRecordControl_day";
            this.telephoneNumberRecordControl_day.Size = new System.Drawing.Size(174, 20);
            this.telephoneNumberRecordControl_day.TabIndex = 10;
            // 
            // telephoneNumberRecordControl_cell
            // 
            this.telephoneNumberRecordControl_cell.ErrorIcon = null;
            this.telephoneNumberRecordControl_cell.ErrorText = "";
            this.telephoneNumberRecordControl_cell.Location = new System.Drawing.Point(97, 105);
            this.telephoneNumberRecordControl_cell.Name = "telephoneNumberRecordControl_cell";
            this.telephoneNumberRecordControl_cell.Size = new System.Drawing.Size(150, 20);
            this.telephoneNumberRecordControl_cell.TabIndex = 9;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(4, 110);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(17, 13);
            this.labelControl4.TabIndex = 12;
            this.labelControl4.Text = "Cell";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(271, 110);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(25, 13);
            this.labelControl5.TabIndex = 14;
            this.labelControl5.Text = "Work";
            // 
            // labelControl6
            // 
            this.labelControl6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl6.Location = new System.Drawing.Point(544, 110);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(27, 13);
            this.labelControl6.TabIndex = 16;
            this.labelControl6.Text = "Home";
            // 
            // labelControl8
            // 
            this.labelControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl8.Location = new System.Drawing.Point(543, 58);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(28, 13);
            this.labelControl8.TabIndex = 10;
            this.labelControl8.Text = "E-Mail";
            // 
            // simpleButton_ScheduledAppt
            // 
            this.simpleButton_ScheduledAppt.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_ScheduledAppt.Location = new System.Drawing.Point(251, 389);
            this.simpleButton_ScheduledAppt.Name = "simpleButton_ScheduledAppt";
            this.simpleButton_ScheduledAppt.Size = new System.Drawing.Size(103, 23);
            this.simpleButton_ScheduledAppt.TabIndex = 27;
            this.simpleButton_ScheduledAppt.Text = "Scheduled Appt";
            // 
            // simpleButton_ImmediateAppt
            // 
            this.simpleButton_ImmediateAppt.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_ImmediateAppt.Location = new System.Drawing.Point(142, 389);
            this.simpleButton_ImmediateAppt.Name = "simpleButton_ImmediateAppt";
            this.simpleButton_ImmediateAppt.Size = new System.Drawing.Size(103, 23);
            this.simpleButton_ImmediateAppt.TabIndex = 26;
            this.simpleButton_ImmediateAppt.Text = "Immediate Appt";
            // 
            // emailRecordControl_email
            // 
            this.emailRecordControl_email.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.emailRecordControl_email.ClickedColor = System.Drawing.Color.Maroon;
            this.emailRecordControl_email.Location = new System.Drawing.Point(579, 55);
            this.emailRecordControl_email.Name = "emailRecordControl_email";
            this.emailRecordControl_email.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.emailRecordControl_email.Properties.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.emailRecordControl_email.Properties.Appearance.Options.UseFont = true;
            this.emailRecordControl_email.Properties.Appearance.Options.UseForeColor = true;
            this.emailRecordControl_email.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.emailRecordControl_email.Size = new System.Drawing.Size(191, 20);
            this.emailRecordControl_email.TabIndex = 5;
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(4, 132);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(39, 13);
            this.labelControl9.TabIndex = 18;
            this.labelControl9.Text = "Address";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(4, 33);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(27, 13);
            this.labelControl10.TabIndex = 4;
            this.labelControl10.Text = "Name";
            // 
            // nameRecordControl_name
            // 
            this.nameRecordControl_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nameRecordControl_name.Location = new System.Drawing.Point(97, 29);
            this.nameRecordControl_name.Name = "nameRecordControl_name";
            this.nameRecordControl_name.Size = new System.Drawing.Size(673, 20);
            this.nameRecordControl_name.TabIndex = 2;
            // 
            // addressRecordControl1
            // 
            this.addressRecordControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addressRecordControl1.Location = new System.Drawing.Point(97, 132);
            this.addressRecordControl1.Name = "addressRecordControl1";
            this.addressRecordControl1.Size = new System.Drawing.Size(673, 73);
            this.addressRecordControl1.TabIndex = 12;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(4, 83);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(33, 13);
            this.labelControl7.TabIndex = 23;
            this.labelControl7.Text = "D.O.B.";
            // 
            // labelControl11
            // 
            this.labelControl11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelControl11.Location = new System.Drawing.Point(543, 84);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(17, 13);
            this.labelControl11.TabIndex = 26;
            this.labelControl11.Text = "Job";
            // 
            // labelControl12
            // 
            this.labelControl12.Location = new System.Drawing.Point(271, 84);
            this.labelControl12.Name = "labelControl12";
            this.labelControl12.Size = new System.Drawing.Size(34, 13);
            this.labelControl12.TabIndex = 24;
            this.labelControl12.Text = "Military";
            // 
            // dateEdit_dob
            // 
            this.dateEdit_dob.EditValue = null;
            this.dateEdit_dob.Location = new System.Drawing.Point(97, 79);
            this.dateEdit_dob.Name = "dateEdit_dob";
            this.dateEdit_dob.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEdit_dob.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEdit_dob.Size = new System.Drawing.Size(150, 20);
            this.dateEdit_dob.TabIndex = 6;
            // 
            // labelControl14
            // 
            this.labelControl14.Location = new System.Drawing.Point(4, 250);
            this.labelControl14.Name = "labelControl14";
            this.labelControl14.Size = new System.Drawing.Size(81, 13);
            this.labelControl14.TabIndex = 31;
            this.labelControl14.Text = "First Time Caller?";
            // 
            // lookUpEdit_currentAARPClient
            // 
            this.lookUpEdit_currentAARPClient.Location = new System.Drawing.Point(151, 245);
            this.lookUpEdit_currentAARPClient.Name = "lookUpEdit_currentAARPClient";
            this.lookUpEdit_currentAARPClient.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_currentAARPClient.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "KeyField", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_currentAARPClient.Properties.DisplayMember = "description";
            this.lookUpEdit_currentAARPClient.Properties.NullText = "";
            this.lookUpEdit_currentAARPClient.Properties.ShowFooter = false;
            this.lookUpEdit_currentAARPClient.Properties.ShowHeader = false;
            this.lookUpEdit_currentAARPClient.Properties.ValueMember = "Id";
            this.lookUpEdit_currentAARPClient.Size = new System.Drawing.Size(96, 20);
            this.lookUpEdit_currentAARPClient.TabIndex = 13;
            // 
            // labelControl15
            // 
            this.labelControl15.Location = new System.Drawing.Point(4, 276);
            this.labelControl15.Name = "labelControl15";
            this.labelControl15.Size = new System.Drawing.Size(132, 13);
            this.labelControl15.TabIndex = 33;
            this.labelControl15.Text = "Requested BTW 50+ Prgm?";
            // 
            // lookUpEdit_interestBTW50Prgm
            // 
            this.lookUpEdit_interestBTW50Prgm.Location = new System.Drawing.Point(151, 271);
            this.lookUpEdit_interestBTW50Prgm.Name = "lookUpEdit_interestBTW50Prgm";
            this.lookUpEdit_interestBTW50Prgm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_interestBTW50Prgm.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "KeyField", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_interestBTW50Prgm.Properties.DisplayMember = "description";
            this.lookUpEdit_interestBTW50Prgm.Properties.NullText = "";
            this.lookUpEdit_interestBTW50Prgm.Properties.ShowFooter = false;
            this.lookUpEdit_interestBTW50Prgm.Properties.ShowHeader = false;
            this.lookUpEdit_interestBTW50Prgm.Properties.ValueMember = "Id";
            this.lookUpEdit_interestBTW50Prgm.Size = new System.Drawing.Size(96, 20);
            this.lookUpEdit_interestBTW50Prgm.TabIndex = 16;
            // 
            // labelControl16
            // 
            this.labelControl16.Location = new System.Drawing.Point(4, 302);
            this.labelControl16.Name = "labelControl16";
            this.labelControl16.Size = new System.Drawing.Size(113, 13);
            this.labelControl16.TabIndex = 35;
            this.labelControl16.Text = "Requested SNAP Prgm?";
            // 
            // lookUpEdit_interestSNAPPrgm
            // 
            this.lookUpEdit_interestSNAPPrgm.Location = new System.Drawing.Point(151, 297);
            this.lookUpEdit_interestSNAPPrgm.Name = "lookUpEdit_interestSNAPPrgm";
            this.lookUpEdit_interestSNAPPrgm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_interestSNAPPrgm.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "KeyField", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_interestSNAPPrgm.Properties.DisplayMember = "description";
            this.lookUpEdit_interestSNAPPrgm.Properties.NullText = "";
            this.lookUpEdit_interestSNAPPrgm.Properties.ShowFooter = false;
            this.lookUpEdit_interestSNAPPrgm.Properties.ShowHeader = false;
            this.lookUpEdit_interestSNAPPrgm.Properties.ValueMember = "Id";
            this.lookUpEdit_interestSNAPPrgm.Size = new System.Drawing.Size(96, 20);
            this.lookUpEdit_interestSNAPPrgm.TabIndex = 19;
            // 
            // labelControl17
            // 
            this.labelControl17.Location = new System.Drawing.Point(4, 328);
            this.labelControl17.Name = "labelControl17";
            this.labelControl17.Size = new System.Drawing.Size(83, 13);
            this.labelControl17.TabIndex = 37;
            this.labelControl17.Text = "Media Follow-Up?";
            // 
            // lookUpEdit_mediaFollowUp
            // 
            this.lookUpEdit_mediaFollowUp.Location = new System.Drawing.Point(151, 323);
            this.lookUpEdit_mediaFollowUp.Name = "lookUpEdit_mediaFollowUp";
            this.lookUpEdit_mediaFollowUp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_mediaFollowUp.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "KeyField", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_mediaFollowUp.Properties.DisplayMember = "description";
            this.lookUpEdit_mediaFollowUp.Properties.NullText = "";
            this.lookUpEdit_mediaFollowUp.Properties.ShowFooter = false;
            this.lookUpEdit_mediaFollowUp.Properties.ShowHeader = false;
            this.lookUpEdit_mediaFollowUp.Properties.ValueMember = "Id";
            this.lookUpEdit_mediaFollowUp.Size = new System.Drawing.Size(96, 20);
            this.lookUpEdit_mediaFollowUp.TabIndex = 21;
            // 
            // labelControl13
            // 
            this.labelControl13.Location = new System.Drawing.Point(273, 304);
            this.labelControl13.Name = "labelControl13";
            this.labelControl13.Size = new System.Drawing.Size(90, 13);
            this.labelControl13.TabIndex = 45;
            this.labelControl13.Text = "Preferred Method?";
            // 
            // lookUpEdit_preferredMethod
            // 
            this.lookUpEdit_preferredMethod.Location = new System.Drawing.Point(369, 299);
            this.lookUpEdit_preferredMethod.Name = "lookUpEdit_preferredMethod";
            this.lookUpEdit_preferredMethod.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_preferredMethod.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "oID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_preferredMethod.Properties.DisplayMember = "description";
            this.lookUpEdit_preferredMethod.Properties.NullText = "";
            this.lookUpEdit_preferredMethod.Properties.ShowFooter = false;
            this.lookUpEdit_preferredMethod.Properties.ShowHeader = false;
            this.lookUpEdit_preferredMethod.Properties.ValueMember = "oID";
            this.lookUpEdit_preferredMethod.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit_preferredMethod.TabIndex = 20;
            // 
            // labelControl18
            // 
            this.labelControl18.Location = new System.Drawing.Point(273, 278);
            this.labelControl18.Name = "labelControl18";
            this.labelControl18.Size = new System.Drawing.Size(62, 13);
            this.labelControl18.TabIndex = 43;
            this.labelControl18.Text = "Text Opt-In?";
            // 
            // lookUpEdit_textOptIn
            // 
            this.lookUpEdit_textOptIn.Location = new System.Drawing.Point(369, 273);
            this.lookUpEdit_textOptIn.Name = "lookUpEdit_textOptIn";
            this.lookUpEdit_textOptIn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_textOptIn.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "KeyField", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_textOptIn.Properties.DisplayMember = "description";
            this.lookUpEdit_textOptIn.Properties.NullText = "";
            this.lookUpEdit_textOptIn.Properties.ShowFooter = false;
            this.lookUpEdit_textOptIn.Properties.ShowHeader = false;
            this.lookUpEdit_textOptIn.Properties.ValueMember = "Id";
            this.lookUpEdit_textOptIn.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit_textOptIn.TabIndex = 17;
            // 
            // labelControl19
            // 
            this.labelControl19.Location = new System.Drawing.Point(273, 252);
            this.labelControl19.Name = "labelControl19";
            this.labelControl19.Size = new System.Drawing.Size(64, 13);
            this.labelControl19.TabIndex = 41;
            this.labelControl19.Text = "Email Opt-In?";
            // 
            // lookUpEdit_emailOptIn
            // 
            this.lookUpEdit_emailOptIn.Location = new System.Drawing.Point(369, 247);
            this.lookUpEdit_emailOptIn.Name = "lookUpEdit_emailOptIn";
            this.lookUpEdit_emailOptIn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_emailOptIn.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "KeyField", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_emailOptIn.Properties.DisplayMember = "description";
            this.lookUpEdit_emailOptIn.Properties.NullText = "";
            this.lookUpEdit_emailOptIn.Properties.ShowFooter = false;
            this.lookUpEdit_emailOptIn.Properties.ShowHeader = false;
            this.lookUpEdit_emailOptIn.Properties.ValueMember = "Id";
            this.lookUpEdit_emailOptIn.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit_emailOptIn.TabIndex = 14;
            // 
            // labelControl20
            // 
            this.labelControl20.Location = new System.Drawing.Point(4, 354);
            this.labelControl20.Name = "labelControl20";
            this.labelControl20.Size = new System.Drawing.Size(83, 13);
            this.labelControl20.TabIndex = 39;
            this.labelControl20.Text = "Housing Situation";
            // 
            // lookUpEdit_housingStatus
            // 
            this.lookUpEdit_housingStatus.Location = new System.Drawing.Point(151, 349);
            this.lookUpEdit_housingStatus.Name = "lookUpEdit_housingStatus";
            this.lookUpEdit_housingStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_housingStatus.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("aarp", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_housingStatus.Properties.DisplayMember = "description";
            this.lookUpEdit_housingStatus.Properties.NullText = "";
            this.lookUpEdit_housingStatus.Properties.ShowFooter = false;
            this.lookUpEdit_housingStatus.Properties.ShowHeader = false;
            this.lookUpEdit_housingStatus.Properties.ValueMember = "Id";
            this.lookUpEdit_housingStatus.Size = new System.Drawing.Size(96, 20);
            this.lookUpEdit_housingStatus.TabIndex = 23;
            // 
            // labelControl21
            // 
            this.labelControl21.Location = new System.Drawing.Point(495, 252);
            this.labelControl21.Name = "labelControl21";
            this.labelControl21.Size = new System.Drawing.Size(117, 13);
            this.labelControl21.TabIndex = 49;
            this.labelControl21.Text = "Initial Prgm. Requested?";
            // 
            // lookUpEdit_initialPrgmRequested
            // 
            this.lookUpEdit_initialPrgmRequested.Location = new System.Drawing.Point(620, 247);
            this.lookUpEdit_initialPrgmRequested.Name = "lookUpEdit_initialPrgmRequested";
            this.lookUpEdit_initialPrgmRequested.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_initialPrgmRequested.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "oID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_initialPrgmRequested.Properties.DisplayMember = "description";
            this.lookUpEdit_initialPrgmRequested.Properties.NullText = "";
            this.lookUpEdit_initialPrgmRequested.Properties.ShowFooter = false;
            this.lookUpEdit_initialPrgmRequested.Properties.ShowHeader = false;
            this.lookUpEdit_initialPrgmRequested.Properties.ValueMember = "oID";
            this.lookUpEdit_initialPrgmRequested.Size = new System.Drawing.Size(150, 20);
            this.lookUpEdit_initialPrgmRequested.TabIndex = 15;
            // 
            // labelControl22
            // 
            this.labelControl22.Location = new System.Drawing.Point(273, 330);
            this.labelControl22.Name = "labelControl22";
            this.labelControl22.Size = new System.Drawing.Size(56, 13);
            this.labelControl22.TabIndex = 47;
            this.labelControl22.Text = "Call Reason";
            // 
            // lookUpEdit_callReason
            // 
            this.lookUpEdit_callReason.Location = new System.Drawing.Point(369, 325);
            this.lookUpEdit_callReason.Name = "lookUpEdit_callReason";
            this.lookUpEdit_callReason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_callReason.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "oID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.lookUpEdit_callReason.Properties.DisplayMember = "description";
            this.lookUpEdit_callReason.Properties.NullText = "";
            this.lookUpEdit_callReason.Properties.ShowFooter = false;
            this.lookUpEdit_callReason.Properties.ShowHeader = false;
            this.lookUpEdit_callReason.Properties.ValueMember = "oID";
            this.lookUpEdit_callReason.Size = new System.Drawing.Size(150, 20);
            this.lookUpEdit_callReason.TabIndex = 22;
            // 
            // labelControl23
            // 
            this.labelControl23.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl23.Location = new System.Drawing.Point(4, 222);
            this.labelControl23.Name = "labelControl23";
            this.labelControl23.Size = new System.Drawing.Size(72, 16);
            this.labelControl23.TabIndex = 53;
            this.labelControl23.Text = "AARP Info:";
            // 
            // lookUpEdit_job
            // 
            this.lookUpEdit_job.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_job.Location = new System.Drawing.Point(579, 81);
            this.lookUpEdit_job.Name = "lookUpEdit_job";
            this.lookUpEdit_job.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_job.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("aarp", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_job.Properties.DisplayMember = "description";
            this.lookUpEdit_job.Properties.NullText = "";
            this.lookUpEdit_job.Properties.ShowFooter = false;
            this.lookUpEdit_job.Properties.ShowHeader = false;
            this.lookUpEdit_job.Properties.ValueMember = "Id";
            this.lookUpEdit_job.Size = new System.Drawing.Size(191, 20);
            this.lookUpEdit_job.TabIndex = 8;
            // 
            // popupContainerControl_Military
            // 
            this.popupContainerControl_Military.Controls.Add(this.milDependent);
            this.popupContainerControl_Military.Controls.Add(this.milGrade);
            this.popupContainerControl_Military.Controls.Add(this.milService);
            this.popupContainerControl_Military.Controls.Add(this.milStatus);
            this.popupContainerControl_Military.Controls.Add(this.labelControl24);
            this.popupContainerControl_Military.Controls.Add(this.labelControl25);
            this.popupContainerControl_Military.Controls.Add(this.labelControl26);
            this.popupContainerControl_Military.Controls.Add(this.labelControl27);
            this.popupContainerControl_Military.Location = new System.Drawing.Point(668, 266);
            this.popupContainerControl_Military.Name = "popupContainerControl_Military";
            this.popupContainerControl_Military.Size = new System.Drawing.Size(251, 103);
            this.popupContainerControl_Military.TabIndex = 55;
            // 
            // milDependent
            // 
            this.milDependent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.milDependent.Location = new System.Drawing.Point(68, 79);
            this.milDependent.Name = "milDependent";
            this.milDependent.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.milDependent.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.milDependent.Properties.DisplayMember = "description";
            this.milDependent.Properties.NullText = "";
            this.milDependent.Properties.ShowFooter = false;
            this.milDependent.Properties.ShowHeader = false;
            this.milDependent.Properties.SortColumnIndex = 1;
            this.milDependent.Properties.ValueMember = "Id";
            this.milDependent.Size = new System.Drawing.Size(180, 20);
            this.milDependent.TabIndex = 15;
            // 
            // milGrade
            // 
            this.milGrade.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.milGrade.Location = new System.Drawing.Point(68, 53);
            this.milGrade.Name = "milGrade";
            this.milGrade.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.milGrade.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.milGrade.Properties.DisplayMember = "description";
            this.milGrade.Properties.NullText = "";
            this.milGrade.Properties.ShowFooter = false;
            this.milGrade.Properties.ShowHeader = false;
            this.milGrade.Properties.SortColumnIndex = 1;
            this.milGrade.Properties.ValueMember = "Id";
            this.milGrade.Size = new System.Drawing.Size(180, 20);
            this.milGrade.TabIndex = 13;
            // 
            // milService
            // 
            this.milService.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.milService.Location = new System.Drawing.Point(68, 27);
            this.milService.Name = "milService";
            this.milService.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.milService.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.milService.Properties.DisplayMember = "description";
            this.milService.Properties.NullText = "";
            this.milService.Properties.ShowFooter = false;
            this.milService.Properties.ShowHeader = false;
            this.milService.Properties.SortColumnIndex = 1;
            this.milService.Properties.ValueMember = "Id";
            this.milService.Size = new System.Drawing.Size(180, 20);
            this.milService.TabIndex = 11;
            // 
            // milStatus
            // 
            this.milStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.milStatus.Location = new System.Drawing.Point(68, 1);
            this.milStatus.Name = "milStatus";
            this.milStatus.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.milStatus.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.milStatus.Properties.DisplayMember = "description";
            this.milStatus.Properties.NullText = "";
            this.milStatus.Properties.ShowFooter = false;
            this.milStatus.Properties.ShowHeader = false;
            this.milStatus.Properties.SortColumnIndex = 1;
            this.milStatus.Properties.ValueMember = "Id";
            this.milStatus.Size = new System.Drawing.Size(180, 20);
            this.milStatus.TabIndex = 9;
            // 
            // labelControl24
            // 
            this.labelControl24.Location = new System.Drawing.Point(6, 82);
            this.labelControl24.Name = "labelControl24";
            this.labelControl24.Size = new System.Drawing.Size(53, 13);
            this.labelControl24.TabIndex = 14;
            this.labelControl24.Text = "Dependent";
            // 
            // labelControl25
            // 
            this.labelControl25.Location = new System.Drawing.Point(6, 56);
            this.labelControl25.Name = "labelControl25";
            this.labelControl25.Size = new System.Drawing.Size(24, 13);
            this.labelControl25.TabIndex = 12;
            this.labelControl25.Text = "Rank";
            // 
            // labelControl26
            // 
            this.labelControl26.Location = new System.Drawing.Point(6, 30);
            this.labelControl26.Name = "labelControl26";
            this.labelControl26.Size = new System.Drawing.Size(33, 13);
            this.labelControl26.TabIndex = 10;
            this.labelControl26.Text = "Branch";
            // 
            // labelControl27
            // 
            this.labelControl27.Location = new System.Drawing.Point(6, 4);
            this.labelControl27.Name = "labelControl27";
            this.labelControl27.Size = new System.Drawing.Size(31, 13);
            this.labelControl27.TabIndex = 8;
            this.labelControl27.Text = "Status";
            // 
            // popupContainerEdit_Military
            // 
            this.popupContainerEdit_Military.Location = new System.Drawing.Point(327, 80);
            this.popupContainerEdit_Military.Name = "popupContainerEdit_Military";
            this.popupContainerEdit_Military.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.popupContainerEdit_Military.Properties.PopupControl = this.popupContainerControl_Military;
            this.popupContainerEdit_Military.Properties.ShowPopupCloseButton = false;
            this.popupContainerEdit_Military.Size = new System.Drawing.Size(174, 20);
            this.popupContainerEdit_Military.TabIndex = 7;
            this.popupContainerEdit_Military.ToolTip = "Type of military service";
            // 
            // labelControl28
            // 
            this.labelControl28.Location = new System.Drawing.Point(273, 356);
            this.labelControl28.Name = "labelControl28";
            this.labelControl28.Size = new System.Drawing.Size(70, 13);
            this.labelControl28.TabIndex = 57;
            this.labelControl28.Text = "Call Resolution";
            // 
            // lookUpEdit_CallResolution
            // 
            this.lookUpEdit_CallResolution.Location = new System.Drawing.Point(369, 351);
            this.lookUpEdit_CallResolution.Name = "lookUpEdit_CallResolution";
            this.lookUpEdit_CallResolution.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_CallResolution.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "oID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_CallResolution.Properties.DisplayMember = "description";
            this.lookUpEdit_CallResolution.Properties.NullText = "";
            this.lookUpEdit_CallResolution.Properties.ShowFooter = false;
            this.lookUpEdit_CallResolution.Properties.ShowHeader = false;
            this.lookUpEdit_CallResolution.Properties.ValueMember = "oID";
            this.lookUpEdit_CallResolution.Size = new System.Drawing.Size(150, 20);
            this.lookUpEdit_CallResolution.TabIndex = 24;
            // 
            // labelControl29
            // 
            this.labelControl29.Location = new System.Drawing.Point(495, 280);
            this.labelControl29.Name = "labelControl29";
            this.labelControl29.Size = new System.Drawing.Size(55, 13);
            this.labelControl29.TabIndex = 58;
            this.labelControl29.Text = "AARP Code";
            // 
            // txtAarpCode
            // 
            this.txtAarpCode.Location = new System.Drawing.Point(620, 275);
            this.txtAarpCode.Name = "txtAarpCode";
            this.txtAarpCode.Properties.Mask.EditMask = "[a-zA-Z0-9]+";
            this.txtAarpCode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtAarpCode.Properties.Mask.ShowPlaceHolders = false;
            this.txtAarpCode.Size = new System.Drawing.Size(150, 20);
            this.txtAarpCode.TabIndex = 18;
            // 
            // simpleButton_NoAppt
            // 
            this.simpleButton_NoAppt.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_NoAppt.Location = new System.Drawing.Point(61, 388);
            this.simpleButton_NoAppt.Name = "simpleButton_NoAppt";
            this.simpleButton_NoAppt.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_NoAppt.TabIndex = 25;
            this.simpleButton_NoAppt.Text = "No Appt";
            // 
            // simpleButton_RescheduleAppt
            // 
            this.simpleButton_RescheduleAppt.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_RescheduleAppt.Location = new System.Drawing.Point(360, 389);
            this.simpleButton_RescheduleAppt.Name = "simpleButton_RescheduleAppt";
            this.simpleButton_RescheduleAppt.Size = new System.Drawing.Size(103, 23);
            this.simpleButton_RescheduleAppt.TabIndex = 59;
            this.simpleButton_RescheduleAppt.Text = "Reschedule Appt";
            // 
            // simpleButton_CancelAppt
            // 
            this.simpleButton_CancelAppt.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_CancelAppt.Location = new System.Drawing.Point(469, 389);
            this.simpleButton_CancelAppt.Name = "simpleButton_CancelAppt";
            this.simpleButton_CancelAppt.Size = new System.Drawing.Size(79, 23);
            this.simpleButton_CancelAppt.TabIndex = 60;
            this.simpleButton_CancelAppt.Text = "Cancel Appt";
            // 
            // simpleButton_ApptHistory
            // 
            this.simpleButton_ApptHistory.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_ApptHistory.Location = new System.Drawing.Point(554, 389);
            this.simpleButton_ApptHistory.Name = "simpleButton_ApptHistory";
            this.simpleButton_ApptHistory.Size = new System.Drawing.Size(84, 23);
            this.simpleButton_ApptHistory.TabIndex = 61;
            this.simpleButton_ApptHistory.Text = "Appt History";
            // 
            // AARPDetailControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton_ApptHistory);
            this.Controls.Add(this.simpleButton_RescheduleAppt);
            this.Controls.Add(this.simpleButton_CancelAppt);
            this.Controls.Add(this.simpleButton_NoAppt);
            this.Controls.Add(this.labelControl29);
            this.Controls.Add(this.labelControl28);
            this.Controls.Add(this.lookUpEdit_CallResolution);
            this.Controls.Add(this.popupContainerEdit_Military);
            this.Controls.Add(this.popupContainerControl_Military);
            this.Controls.Add(this.lookUpEdit_job);
            this.Controls.Add(this.labelControl23);
            this.Controls.Add(this.labelControl21);
            this.Controls.Add(this.lookUpEdit_initialPrgmRequested);
            this.Controls.Add(this.labelControl22);
            this.Controls.Add(this.lookUpEdit_callReason);
            this.Controls.Add(this.labelControl13);
            this.Controls.Add(this.lookUpEdit_preferredMethod);
            this.Controls.Add(this.labelControl18);
            this.Controls.Add(this.lookUpEdit_textOptIn);
            this.Controls.Add(this.labelControl19);
            this.Controls.Add(this.lookUpEdit_emailOptIn);
            this.Controls.Add(this.labelControl20);
            this.Controls.Add(this.lookUpEdit_housingStatus);
            this.Controls.Add(this.labelControl17);
            this.Controls.Add(this.lookUpEdit_mediaFollowUp);
            this.Controls.Add(this.labelControl16);
            this.Controls.Add(this.lookUpEdit_interestSNAPPrgm);
            this.Controls.Add(this.labelControl15);
            this.Controls.Add(this.lookUpEdit_interestBTW50Prgm);
            this.Controls.Add(this.labelControl14);
            this.Controls.Add(this.lookUpEdit_currentAARPClient);
            this.Controls.Add(this.dateEdit_dob);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.labelControl12);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.nameRecordControl_name);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.emailRecordControl_email);
            this.Controls.Add(this.simpleButton_ImmediateAppt);
            this.Controls.Add(this.simpleButton_ScheduledAppt);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.telephoneNumberRecordControl_cell);
            this.Controls.Add(this.telephoneNumberRecordControl_day);
            this.Controls.Add(this.telephoneNumberRecordControl_evening);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lookUpEdit_gender);
            this.Controls.Add(this.lookUpEdit_language);
            this.Controls.Add(this.lookUpEdit_referred_by);
            this.Controls.Add(this.addressRecordControl1);
            this.Controls.Add(this.txtAarpCode);
            this.Name = "AARPDetailControl";
            this.Size = new System.Drawing.Size(780, 427);
            this.Controls.SetChildIndex(this.txtAarpCode, 0);
            this.Controls.SetChildIndex(this.addressRecordControl1, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_referred_by, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_language, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_gender, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.labelControl2, 0);
            this.Controls.SetChildIndex(this.labelControl3, 0);
            this.Controls.SetChildIndex(this.telephoneNumberRecordControl_evening, 0);
            this.Controls.SetChildIndex(this.telephoneNumberRecordControl_day, 0);
            this.Controls.SetChildIndex(this.telephoneNumberRecordControl_cell, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.labelControl8, 0);
            this.Controls.SetChildIndex(this.simpleButton_ScheduledAppt, 0);
            this.Controls.SetChildIndex(this.simpleButton_ImmediateAppt, 0);
            this.Controls.SetChildIndex(this.emailRecordControl_email, 0);
            this.Controls.SetChildIndex(this.labelControl9, 0);
            this.Controls.SetChildIndex(this.labelControl10, 0);
            this.Controls.SetChildIndex(this.nameRecordControl_name, 0);
            this.Controls.SetChildIndex(this.labelControl7, 0);
            this.Controls.SetChildIndex(this.labelControl12, 0);
            this.Controls.SetChildIndex(this.labelControl11, 0);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.dateEdit_dob, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_currentAARPClient, 0);
            this.Controls.SetChildIndex(this.labelControl14, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_interestBTW50Prgm, 0);
            this.Controls.SetChildIndex(this.labelControl15, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_interestSNAPPrgm, 0);
            this.Controls.SetChildIndex(this.labelControl16, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_mediaFollowUp, 0);
            this.Controls.SetChildIndex(this.labelControl17, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_housingStatus, 0);
            this.Controls.SetChildIndex(this.labelControl20, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_emailOptIn, 0);
            this.Controls.SetChildIndex(this.labelControl19, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_textOptIn, 0);
            this.Controls.SetChildIndex(this.labelControl18, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_preferredMethod, 0);
            this.Controls.SetChildIndex(this.labelControl13, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_callReason, 0);
            this.Controls.SetChildIndex(this.labelControl22, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_initialPrgmRequested, 0);
            this.Controls.SetChildIndex(this.labelControl21, 0);
            this.Controls.SetChildIndex(this.labelControl23, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_job, 0);
            this.Controls.SetChildIndex(this.popupContainerControl_Military, 0);
            this.Controls.SetChildIndex(this.popupContainerEdit_Military, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_CallResolution, 0);
            this.Controls.SetChildIndex(this.labelControl28, 0);
            this.Controls.SetChildIndex(this.labelControl29, 0);
            this.Controls.SetChildIndex(this.simpleButton_NoAppt, 0);
            this.Controls.SetChildIndex(this.simpleButton_CancelAppt, 0);
            this.Controls.SetChildIndex(this.simpleButton_RescheduleAppt, 0);
            this.Controls.SetChildIndex(this.simpleButton_ApptHistory, 0);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_referred_by.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_language.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_gender.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_evening)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_day)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.telephoneNumberRecordControl_cell)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emailRecordControl_email.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressRecordControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_dob.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEdit_dob.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_currentAARPClient.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_interestBTW50Prgm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_interestSNAPPrgm.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_mediaFollowUp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_preferredMethod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_textOptIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_emailOptIn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_housingStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_initialPrgmRequested.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_callReason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_job.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl_Military)).EndInit();
            this.popupContainerControl_Military.ResumeLayout(false);
            this.popupContainerControl_Military.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.milDependent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.milGrade.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.milService.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.milStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerEdit_Military.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_CallResolution.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAarpCode.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LabelControl labelControl1;
        public DevExpress.XtraEditors.LabelControl labelControl2;
        public DevExpress.XtraEditors.LabelControl labelControl3;
        public DevExpress.XtraEditors.LabelControl labelControl4;
        public DevExpress.XtraEditors.LabelControl labelControl5;
        public DevExpress.XtraEditors.LabelControl labelControl6;
        public DevExpress.XtraEditors.LabelControl labelControl8;
        public DevExpress.XtraEditors.LabelControl labelControl9;
        public DevExpress.XtraEditors.LabelControl labelControl10;
        public DevExpress.XtraEditors.SimpleButton simpleButton_ScheduledAppt;
        public DevExpress.XtraEditors.SimpleButton simpleButton_ImmediateAppt;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_referred_by;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_language;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_gender;
        public Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_evening;
        public Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_day;
        public Data.Controls.TelephoneNumberRecordControl telephoneNumberRecordControl_cell;
        public Data.Controls.EmailRecordControl emailRecordControl_email;
        public Data.Controls.NameRecordControl nameRecordControl_name;
        public Data.Controls.AddressRecordControl addressRecordControl1;
        public DevExpress.XtraEditors.LabelControl labelControl7;
        public DevExpress.XtraEditors.LabelControl labelControl11;
        public DevExpress.XtraEditors.LabelControl labelControl12;
        private DevExpress.XtraEditors.DateEdit dateEdit_dob;
        public DevExpress.XtraEditors.LabelControl labelControl14;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_currentAARPClient;
        public DevExpress.XtraEditors.LabelControl labelControl15;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_interestBTW50Prgm;
        public DevExpress.XtraEditors.LabelControl labelControl16;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_interestSNAPPrgm;
        public DevExpress.XtraEditors.LabelControl labelControl17;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_mediaFollowUp;
        public DevExpress.XtraEditors.LabelControl labelControl13;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_preferredMethod;
        public DevExpress.XtraEditors.LabelControl labelControl18;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_textOptIn;
        public DevExpress.XtraEditors.LabelControl labelControl19;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_emailOptIn;
        public DevExpress.XtraEditors.LabelControl labelControl20;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_housingStatus;
        public DevExpress.XtraEditors.LabelControl labelControl21;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_initialPrgmRequested;
        public DevExpress.XtraEditors.LabelControl labelControl22;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_callReason;
        private DevExpress.XtraEditors.LabelControl labelControl23;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_job;
        private DevExpress.XtraEditors.PopupContainerControl popupContainerControl_Military;
        internal DevExpress.XtraEditors.LookUpEdit milDependent;
        internal DevExpress.XtraEditors.LookUpEdit milGrade;
        internal DevExpress.XtraEditors.LookUpEdit milService;
        internal DevExpress.XtraEditors.LookUpEdit milStatus;
        internal DevExpress.XtraEditors.LabelControl labelControl24;
        internal DevExpress.XtraEditors.LabelControl labelControl25;
        internal DevExpress.XtraEditors.LabelControl labelControl26;
        internal DevExpress.XtraEditors.LabelControl labelControl27;
        private DevExpress.XtraEditors.PopupContainerEdit popupContainerEdit_Military;
        public DevExpress.XtraEditors.LabelControl labelControl28;
        public DevExpress.XtraEditors.LookUpEdit lookUpEdit_CallResolution;
        public DevExpress.XtraEditors.LabelControl labelControl29;
        private DevExpress.XtraEditors.TextEdit txtAarpCode;
        private DevExpress.XtraEditors.SimpleButton simpleButton_NoAppt;
        public DevExpress.XtraEditors.SimpleButton simpleButton_RescheduleAppt;
        public DevExpress.XtraEditors.SimpleButton simpleButton_CancelAppt;
        public DevExpress.XtraEditors.SimpleButton simpleButton_ApptHistory;
    }
}
