﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing
{
    public class Mainline : IDesktopMainline
    {
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ArgParser();
                    if (ap.Parse((string[])param))
                    {
                        using (var frm = new FormCreateClient_AARP())
                        {
                            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK && frm.ClientParameters.clientID.HasValue)
                            {
                                var parameters = frm.ClientParameters;
                                UpdateMru(parameters.clientID.Value);

                                using (DebtPlus.UI.Client.Service.ClientUpdateClass cuc = new DebtPlus.UI.Client.Service.ClientUpdateClass())
                                {
                                    cuc.ShowEditDialog(parameters.clientID.Value, true);
                                }
                            }
                        }
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "ClientCreateAARP"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }

        private void UpdateMru(Int32 client)
        {
            using (DebtPlus.Data.MRU mruList = new DebtPlus.Data.MRU("Clients"))
            {
                mruList.InsertTopMost(client);
                mruList.SaveKey();
            }
        }
    }
}
