﻿namespace DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing
{
    partial class SearchClientAARP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lookUpEdit_FirstContact = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_housing.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_FirstContact.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // checkEdit_housing
            // 
            this.checkEdit_housing.Location = new System.Drawing.Point(208, 168);
            this.checkEdit_housing.Size = new System.Drawing.Size(165, 20);
            this.checkEdit_housing.TabIndex = 8;
            // 
            // labelControl_FormTitle
            // 
            this.labelControl_FormTitle.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl_FormTitle.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 14F, System.Drawing.FontStyle.Bold);
            this.labelControl_FormTitle.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl_FormTitle.Location = new System.Drawing.Point(266, 0);
            this.labelControl_FormTitle.Size = new System.Drawing.Size(228, 27);
            this.labelControl_FormTitle.TabIndex = 1;
            this.labelControl_FormTitle.Text = "Create New AARP Client";
            // 
            // labelControl_Instructions
            // 
            this.labelControl_Instructions.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl_Instructions.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.labelControl_Instructions.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl_Instructions.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl_Instructions.Size = new System.Drawing.Size(721, 52);
            // 
            // homeTelephoneNumber
            // 
            this.homeTelephoneNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.homeTelephoneNumber.Location = new System.Drawing.Point(312, 102);
            this.homeTelephoneNumber.Size = new System.Drawing.Size(225, 20);
            // 
            // checkEdit_no_telephone
            // 
            this.checkEdit_no_telephone.Location = new System.Drawing.Point(208, 191);
            this.checkEdit_no_telephone.Size = new System.Drawing.Size(280, 20);
            this.checkEdit_no_telephone.TabIndex = 9;
            // 
            // labelControl_homeTelephoneNumber
            // 
            this.labelControl_homeTelephoneNumber.Location = new System.Drawing.Point(208, 106);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Location = new System.Drawing.Point(287, 226);
            this.simpleButton_OK.TabIndex = 10;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(383, 226);
            this.simpleButton_Cancel.TabIndex = 11;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(208, 131);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 13);
            this.labelControl1.TabIndex = 4;
            this.labelControl1.Text = "Method of Contact";
            // 
            // lookUpEdit_FirstContact
            // 
            this.lookUpEdit_FirstContact.Location = new System.Drawing.Point(312, 128);
            this.lookUpEdit_FirstContact.Name = "lookUpEdit_FirstContact";
            this.lookUpEdit_FirstContact.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_FirstContact.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_FirstContact.Properties.DisplayMember = "description";
            this.lookUpEdit_FirstContact.Properties.NullText = "";
            this.lookUpEdit_FirstContact.Properties.ShowFooter = false;
            this.lookUpEdit_FirstContact.Properties.ShowHeader = false;
            this.lookUpEdit_FirstContact.Properties.ValueMember = "Id";
            this.lookUpEdit_FirstContact.Size = new System.Drawing.Size(225, 20);
            this.lookUpEdit_FirstContact.TabIndex = 5;
            // 
            // SearchClientAARP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.lookUpEdit_FirstContact);
            this.Controls.Add(this.labelControl1);
            this.MinimumSize = new System.Drawing.Size(745, 262);
            this.Name = "SearchClientAARP";
            this.Size = new System.Drawing.Size(745, 262);
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.Controls.SetChildIndex(this.labelControl_FormTitle, 0);
            this.Controls.SetChildIndex(this.labelControl_Instructions, 0);
            this.Controls.SetChildIndex(this.homeTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.checkEdit_no_telephone, 0);
            this.Controls.SetChildIndex(this.labelControl_homeTelephoneNumber, 0);
            this.Controls.SetChildIndex(this.checkEdit_housing, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.lookUpEdit_FirstContact, 0);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_housing.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeTelephoneNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_no_telephone.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_FirstContact.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        protected DevExpress.XtraEditors.LabelControl labelControl1;
        protected DevExpress.XtraEditors.LookUpEdit lookUpEdit_FirstContact;
    }
}
