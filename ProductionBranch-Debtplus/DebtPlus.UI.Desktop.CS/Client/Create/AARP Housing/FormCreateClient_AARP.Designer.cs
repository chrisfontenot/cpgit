﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Data.Controls;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing
{
    partial class FormCreateClient_AARP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.aarpDetailControl1 = new DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing.AARPDetailControl();
            this.SearchClientAARP1 = new DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing.SearchClientAARP();
            this.selectionControl1 = new DebtPlus.UI.Desktop.CS.Client.Create.selectionControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // aarpDetailControl1
            // 
            this.aarpDetailControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aarpDetailControl1.IsExistingClient = false;
            this.aarpDetailControl1.Location = new System.Drawing.Point(0, 0);
            this.aarpDetailControl1.Name = "aarpDetailControl1";
            this.aarpDetailControl1.Size = new System.Drawing.Size(780, 427);
            this.aarpDetailControl1.TabIndex = 2;
            // 
            // SearchClientAARP1
            // 
            this.SearchClientAARP1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SearchClientAARP1.Location = new System.Drawing.Point(0, 0);
            this.SearchClientAARP1.MinimumSize = new System.Drawing.Size(745, 262);
            this.SearchClientAARP1.Name = "SearchClientAARP1";
            this.SearchClientAARP1.Size = new System.Drawing.Size(780, 427);
            this.SearchClientAARP1.TabIndex = 0;
            // 
            // selectionControl1
            // 
            this.selectionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectionControl1.Location = new System.Drawing.Point(0, 0);
            this.selectionControl1.Name = "selectionControl1";
            this.selectionControl1.Size = new System.Drawing.Size(780, 427);
            this.selectionControl1.TabIndex = 1;
            this.selectionControl1.Visible = false;
            // 
            // FormCreateClient_AARP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 427);
            this.Controls.Add(this.SearchClientAARP1);
            this.Controls.Add(this.aarpDetailControl1);
            this.Controls.Add(this.selectionControl1);
            this.MinimumSize = new System.Drawing.Size(764, 302);
            this.Name = "FormCreateClient_AARP";
            this.Text = "Create AARP Housing Client";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        public AARPDetailControl aarpDetailControl1;
        public SearchClientAARP SearchClientAARP1;
        public DebtPlus.UI.Desktop.CS.Client.Create.selectionControl selectionControl1;
        #endregion
    }
}