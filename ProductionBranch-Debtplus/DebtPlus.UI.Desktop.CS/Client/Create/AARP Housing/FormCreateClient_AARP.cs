﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DebtPlus.UI.Desktop.CS.Client.Create.FHLB_Housing;
using DebtPlus.UI.Client.forms.Appointments;
using DebtPlus.Interfaces.Client;

namespace DebtPlus.UI.Desktop.CS.Client.Create.AARP_Housing
{
    internal partial class FormCreateClient_AARP : DebtPlus.Data.Forms.DebtPlusForm//: FHLB_Housing.FormCreateClient_FHLB
    {
        // Current search event arguments and resulting client
        public AARPSearchEventArgs AARPClientParameters { get; internal set; }
        bool isExistingClient = false;

        private const int InitialProgramReqested_HousingSolutionCenter = 1;
        private const int Language_English = 1;
        private BusinessContext bc = null;

        public FormCreateClient_AARP()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                     += Form_CreateClient_Load;
            FormClosing                              += FormCreateClient_FormClosing;
            SearchClientAARP1.Cancelled              += SearchClientAARP1_Cancelled;
            selectionControl1.Cancelled              += selectionControl1_Cancelled;
            SearchClientAARP1.Search                 += SearchClientAARP1_Search;
            selectionControl1.ClientFound            += selectionControl1_ClientFound;
            aarpDetailControl1.Cancelled             += new EventHandler(aarpDetailControl1_Cancelled);
            aarpDetailControl1.NoAppointment         += aarpDetailControl1_NoAppointment;
            aarpDetailControl1.ImmediateAppointment  += aarpDetailControl1_ImmediateAppointment;
            aarpDetailControl1.ScheduledAppointment  += aarpDetailControl1_ScheduledAppointment;
            aarpDetailControl1.RescheduleAppointment += aarpDetailControl1_RescheduleAppointment;
            aarpDetailControl1.CancelAppointment     += aarpDetailControl1_CancelAppointment;
            aarpDetailControl1.AppointmentHistory    += aarpDetailControl1_AppointmentHistory;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                     -= Form_CreateClient_Load;
            FormClosing                              -= FormCreateClient_FormClosing;
            SearchClientAARP1.Cancelled              -= SearchClientAARP1_Cancelled;
            selectionControl1.Cancelled              -= selectionControl1_Cancelled;
            SearchClientAARP1.Search                 -= SearchClientAARP1_Search;
            selectionControl1.ClientFound            -= selectionControl1_ClientFound;
            aarpDetailControl1.Cancelled             -= new EventHandler(aarpDetailControl1_Cancelled);
            aarpDetailControl1.NoAppointment         -= aarpDetailControl1_NoAppointment;
            aarpDetailControl1.ImmediateAppointment  -= aarpDetailControl1_ImmediateAppointment;
            aarpDetailControl1.ScheduledAppointment  -= aarpDetailControl1_ScheduledAppointment;
            aarpDetailControl1.RescheduleAppointment -= aarpDetailControl1_RescheduleAppointment;
            aarpDetailControl1.CancelAppointment     -= aarpDetailControl1_CancelAppointment;
            aarpDetailControl1.AppointmentHistory    -= aarpDetailControl1_AppointmentHistory;
        }

        /// <summary>
        /// Process the FORM CLOSING event to save the placement for next time.
        /// </summary>
        private void FormCreateClient_FormClosing(object sender, FormClosingEventArgs e)
        {
            SavePlacement();
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void Form_CreateClient_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            bc = new BusinessContext();
            try
            {
                // Define the form location and size
                LoadPlacement("ClientCreateAARP");

                // Go to the search panel
                SwitchToSearch();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// The user canceled the selection form. This is terminal to the form processing.
        /// </summary>
        private void SearchClientAARP1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// The user canceled the selection form. Go back to the search control.
        /// </summary>
        private void selectionControl1_Cancelled(object sender, EventArgs e)
        {
            SwitchToSearch();
        }

        /// <summary>
        /// Switch the form to the KeyField processing
        /// </summary>
        private void SwitchToSearch()
        {
            // Send the duplicate control to the rear
            this.selectionControl1.Visible = false;
            this.selectionControl1.SendToBack();

            // Send the detail control to the rear
            this.aarpDetailControl1.Visible = false;
            this.aarpDetailControl1.SendToBack();

            // Bring the selection control to the front
            this.SearchClientAARP1.BringToFront();
            this.SearchClientAARP1.Visible = true;
            this.SearchClientAARP1.Focus();
        }

        /// <summary>
        /// Switch the form to the selection processing
        /// </summary>
        private void SwitchToSelect()
        {
            // Send the search control to the rear
            this.SearchClientAARP1.Visible = false;
            this.SearchClientAARP1.SendToBack();

            // Send the detail control to the rear
            this.aarpDetailControl1.Visible = false;
            this.aarpDetailControl1.SendToBack();

            // Bring the select control to the front
            this.selectionControl1.BringToFront();
            this.selectionControl1.Visible = true;
            this.selectionControl1.Focus();
        }

        /// <summary>
        /// Switch the form to the selection processing
        /// </summary>
        private void SwitchToDetail()
        {
            // Send the search control to the rear
            this.SearchClientAARP1.Visible = false;
            this.SearchClientAARP1.SendToBack();

            // Send the select control to the rear
            this.selectionControl1.Visible = false;
            this.selectionControl1.SendToBack();

            // Bring the detail control to the front
            this.aarpDetailControl1.BringToFront();
            this.aarpDetailControl1.Visible = true;
            this.aarpDetailControl1.Focus();
        }

        /// <summary>
        /// Do the client search operation. Create the client if indicated.
        /// </summary>
        private void SearchClientAARP1_Search(object Sender, SearchEventArgs e)
        {
            try
            {
                // Save the search for the result function
                e.existingClient = false;
                isExistingClient = false;
                this.aarpDetailControl1.IsExistingClient = isExistingClient;
                e.InitialProgram = InitialProgramReqested_HousingSolutionCenter; //default to HousingSolutionCenter
                e.language = Language_English;
                AARPClientParameters = AARPSearchEventArgs.MapToObject(e);

                // If the telephone number is not desired then go to the detail information.
                if (e.homePhone == null)
                {
                    SwitchToDetail();
                    aarpDetailControl1.SetCurrentValues(AARPClientParameters);
                    return;
                }

                // Look for the client in the list of telephone numbers
                System.Collections.Generic.List<DebtPlus.LINQ.xpr_client_create_searchResult> colAnswers = ClientSearch.PerformSearch(e);
                if (colAnswers == null)
                {
                    return;
                }

                // If there are no clients then go ahead and create the client
                if (colAnswers.Count == 0)
                {
                    SwitchToDetail();
                    aarpDetailControl1.SetCurrentValues(AARPClientParameters);
                    return;
                }

                // If there is exactly one client then ask if this is the client
                if (colAnswers.Count == 1)
                {
                    isExistingClient = true;
                    this.aarpDetailControl1.IsExistingClient = isExistingClient;

                    AARPClientParameters = GetClientDetails(colAnswers[0].clientID, AARPClientParameters);
                    SwitchToDetail();
                    aarpDetailControl1.SetCurrentValues(AARPClientParameters);
                    return;
                }

                // There are more than one. Ask the user for the proper selection now.
                // The control will take it from this point until an item is found.
                SwitchToSelect();
                selectionControl1.SetCurrentValues(e, colAnswers);
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error searching for client");
            }
        }

        /// <summary>
        /// Process the completion of the selection operation on a list of possible client matches.
        /// </summary>
        private void selectionControl1_ClientFound(object sender, SearchEventArgs e)
        {
            ClientParameters = e;
            AARPClientParameters = AARPSearchEventArgs.MapToObject(e);

            isExistingClient = ClientParameters.existingClient;
            this.aarpDetailControl1.IsExistingClient = isExistingClient;

            // Handle the condition where the client was located in the list.
            if (ClientParameters.existingClient)
            {
                // ExistingClient();
                if (e.clientID != null)
                {
                    AARPClientParameters = GetClientDetails((int)e.clientID, AARPClientParameters);
                }
                SwitchToDetail();
                aarpDetailControl1.SetCurrentValues(AARPClientParameters);
            }
            else
            {
                // Otherwise, the person checked the box "not in list"
                SwitchToDetail();
                aarpDetailControl1.SetCurrentValues(AARPClientParameters);
            }
        }

        /// <summary>
        /// Get Client details along with applicant and AARP call log details
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        private AARPSearchEventArgs GetClientDetails(int clientID, AARPSearchEventArgs param)
        {
            AARPSearchEventArgs clientData = null;
            if (clientID > 0)
            {
                var client = (from c in bc.clients where c.Id == clientID select c).FirstOrDefault();
                var applicant = (from p in bc.peoples where p.Client == clientID && p.Relation == 1 select p).FirstOrDefault();
                var address = (client != null) ? (from a in bc.addresses where a.Id == client.AddressID select a).FirstOrDefault() : null;
                var homePhone = (client != null) ? (from t in bc.TelephoneNumbers where t.Id == client.HomeTelephoneID select t).FirstOrDefault() : null;
                var name = (applicant != null) ? (from n in bc.Names where n.Id == applicant.NameID select n).FirstOrDefault() : null;
                var email = (applicant != null) ? (from e in bc.EmailAddresses where e.Id == applicant.EmailID select e).FirstOrDefault() : null;
                var workPhone = (applicant != null) ? (from t in bc.TelephoneNumbers where t.Id == applicant.WorkTelephoneID select t).FirstOrDefault() : null;
                var cellPhone = (applicant != null) ? (from t in bc.TelephoneNumbers where t.Id == applicant.CellTelephoneID select t).FirstOrDefault() : null;
                var callLog = (from a in bc.aarp_call_logs where a.ClientID == clientID && a.IsActive == true select a).FirstOrDefault();
                var appointment = (callLog != null)? (from ca in bc.client_appointments where ca.Id == callLog.AppointmentID select ca).FirstOrDefault() : null;

                clientData = new AARPSearchEventArgs();
                clientData.clientID = clientID;

                if (client != null)
                {
                    clientData.language           = client.language;
                    clientData.methodFirstContact = client.method_first_contact;
                }

                if (callLog != null)
                {
                    clientData.AarpCode             = callLog.AarpCode;
                    clientData.Btw50ProgramRequired = callLog.Btw50ProgramRequested;
                    clientData.CallReason           = callLog.CallReason;
                    clientData.CallResolution       = callLog.CallResolution;
                    clientData.EmailOptIn           = callLog.EmailOptIn;
                    clientData.HousingSituation     = callLog.HousingSituation ?? 0;
                    clientData.InitialProgram       = callLog.InitialProgramRequested;
                    clientData.IsAarpClient         = callLog.IsAarpClient;
                    clientData.MediaFollowup        = callLog.MediaFollowup;
                    clientData.PreferredMethod      = callLog.PreferredMethod ?? 0;
                    clientData.SnapProgramRequired  = callLog.SnapProgramRequested;
                    clientData.TextOptIn            = callLog.TextOptIn;
                    clientData.referralSource       = callLog.ReferralCode;
                };

                // if there is a client appointment, get the referral code from it
                if (appointment != null)
                {
                    clientData.referralSource = appointment.referred_by;
                }
                else
                {
                    if (callLog != null)
                    {
                        clientData.referralSource = callLog.ReferralCode;
                    }
                }

                clientData.address        = address;
                clientData.applicant_name = name;
                clientData.cellTelephone  = cellPhone;

                if (applicant != null)
                {
                    clientData.dateOfBirth       = applicant.Birthdate;
                    clientData.gender            = applicant.Gender;
                    clientData.job               = applicant.job;
                    clientData.militaryDependent = applicant.MilitaryDependentID;
                    clientData.militaryGrade     = applicant.MilitaryGradeID;
                    clientData.militaryService   = applicant.MilitaryServiceID;
                    clientData.militaryStatus    = applicant.MilitaryStatusID;
                }

                clientData.email                = email;
                clientData.existingClient       = param.existingClient;
                clientData.homePhone            = homePhone;
                clientData.workTelephone        = workPhone;

                clientData.partnerCode          = param.partnerCode;
                clientData.immediateAppointment = param.immediateAppointment;
                clientData.housingStatus        = param.housingStatus;
                clientData.housingIssues        = param.housingIssues;
            }
            return clientData;
        }

        /// <summary>
        /// The detail form was canceled. Go back to the KeyField.
        /// </summary>
        private void aarpDetailControl1_Cancelled(object sender, EventArgs e)
        {
            SwitchToSearch();
        }

        /// <summary>
        /// Process the Immediate Appointment button for the detail
        /// </summary>
        private void aarpDetailControl1_NoAppointment(object sender, EventArgs e)
        {
            // create client only once 
            AARPClientParameters = aarpDetailControl1.GetCurrentValues();

            CreateClient(AARPClientParameters);

            ClientParameters     = AARPClientParameters as SearchEventArgs;
            DialogResult         = System.Windows.Forms.DialogResult.OK;
        }


        /// <summary>
        /// Process the Immediate Appointment button for the detail
        /// </summary>
        private void aarpDetailControl1_ImmediateAppointment(object sender, EventArgs e)
        {
            // create client only once 
            AARPClientParameters = aarpDetailControl1.GetCurrentValues();
            CreateClient(AARPClientParameters);
            ClientParameters = AARPClientParameters as SearchEventArgs;
            CreateImmediateAppointment();
            UpdateCallLogAppoinment((int)AARPClientParameters.clientID);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void UpdateCallLogAppoinment(int clientID)
        {
            var callLog = (from cl in bc.aarp_call_logs where cl.ClientID == clientID && cl.IsActive == true select cl).FirstOrDefault();

            if (callLog != null)
            {
                var appointment = bc.client_appointments.Where(ca => ca.client == clientID).OrderByDescending(ca => ca.date_created).FirstOrDefault();
                if (appointment != null)
                {
                    callLog.AppointmentID = appointment.Id;
                    bc.SubmitChanges();
                }
            }
        }

        /// <summary>
        /// Process the scheduled appointment value for the detail
        /// </summary>
        private void aarpDetailControl1_ScheduledAppointment(object sender, EventArgs e)
        {
            // Create the client only once
            AARPClientParameters = aarpDetailControl1.GetCurrentValues();
            CreateClient(AARPClientParameters);
            ClientParameters = AARPClientParameters as SearchEventArgs;
            CreateScheduledAppointment();
            UpdateCallLogAppoinment((int)AARPClientParameters.clientID);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Process the Rescheduled appointment value for the detail
        /// </summary>
        private void aarpDetailControl1_RescheduleAppointment(object sender, EventArgs e)
        {
            AARPClientParameters = aarpDetailControl1.GetCurrentValues();
            CreateClient(AARPClientParameters);
            ClientParameters = AARPClientParameters as SearchEventArgs;
            CreateRescheduleAppointment();
            UpdateCallLogAppoinment((int)AARPClientParameters.clientID);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Process the Rescheduled appointment value for the detail
        /// </summary>
        private void aarpDetailControl1_CancelAppointment(object sender, EventArgs e)
        {
            AARPClientParameters = aarpDetailControl1.GetCurrentValues();
            CreateClient(AARPClientParameters);
            ClientParameters = AARPClientParameters as SearchEventArgs;
            CreateCancelAppointment();
            UpdateCallLogAppoinment((int)AARPClientParameters.clientID);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        /// <summary>
        /// Process the appointment history to display report
        /// </summary>
        private void aarpDetailControl1_AppointmentHistory(object sender, EventArgs e)
        {
            AARPClientParameters = aarpDetailControl1.GetCurrentValues();

            // display the report using a new thread
            var thrd = new System.Threading.Thread(ReportThread);
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.IsBackground = true;
            thrd.Start();
        }

        /// <summary>
        /// Display Appointment History Report
        /// </summary>
        private void ReportThread()
        {
            if (AARPClientParameters.clientID == null)
            {
                DebtPlus.Data.Forms.MessageBox.Show("There are no appointments for this client.", "No Appointments", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                string reportName = null;

                using (var reportBc = new BusinessContext())
                {
                    // get the report details
                    var reportDetails = reportBc.reports.Where(r => r.menu_name == " Appointment History").FirstOrDefault();
                    if (reportDetails != null)
                    {
                        reportName = reportDetails.ClassName;
                    }
                }

                if (!string.IsNullOrWhiteSpace(reportName))
                {
                    // load and display report
                    var report = DebtPlus.Reports.ReportLoader.LoadReport(reportName, null);

                    if (report != null)
                    {
                        var paramType = (IClient)report;
                        if (paramType != null)
                        {
                            paramType.ClientId = (int)AARPClientParameters.clientID;
                        }

                        // Run the report in the current thread
                        ((DebtPlus.Interfaces.Reports.IReports)report).RunReport();
                    }
                }
            }
        }

        protected void CreateImmediateAppointment()
        {
            ClientParameters.immediateAppointment = true;

            // Create the appointment for "now". But, it does not include the seconds so make everything valid to the current minute.
            System.DateTime startingTime = DateTime.Now;
            startingTime                 = new DateTime(startingTime.Year, startingTime.Month, startingTime.Day, startingTime.Hour, startingTime.Minute, 0);

            // Set AARP Branch as the default office for AARP appointments
            Int32? office = 595;
                       
            // Find the appointment type
            DebtPlus.LINQ.appt_type apt = DebtPlus.LINQ.Cache.appt_type.getList().Find(s =>s.Id == 124);
            if (apt == null)
            {
                apt = DebtPlus.LINQ.Cache.appt_type.getList().First();
            }

            // Allocate the new appointment
            var clientAppointment = DebtPlus.LINQ.Factory.Manufacture_client_appointment();

            clientAppointment.status      = 'P';
            clientAppointment.client      = ClientParameters.clientID.Value;
            clientAppointment.appt_type   = apt.Id;
            clientAppointment.start_time  = startingTime;
            clientAppointment.end_time    = startingTime.AddMinutes(apt.appt_duration);
            clientAppointment.office      = office;
            clientAppointment.referred_by = ClientParameters.referralSource;
            clientAppointment.partner     = ClientParameters.partnerCode;

            // Put the appointment into the database
            using (var bc = new BusinessContext())
            {
                bc.client_appointments.InsertOnSubmit(clientAppointment);
                bc.SubmitChanges();

                // Do the post processing for the appointment
                using (var frm = new DebtPlus.UI.Client.forms.Appointments.Appointments_Form_Book_Confirmation_New(bc, clientAppointment))
                {
                    frm.ShowDialog();
                }

                // Ensure that there is nothing left dangling before we dispose of the database connection.
                bc.SubmitChanges();
            }
        }

        /// <summary>
        /// Create the scheduled appointment for the client
        /// </summary>
        protected void CreateScheduledAppointment()
        {
            ClientParameters.immediateAppointment = false;
            using (var frm = new FormCreateAppointment(bc, ClientParameters))
            {
                frm.ShowDialog();
            }
        }

        protected void CreateRescheduleAppointment()
        {
            ClientParameters.immediateAppointment = false;
            using (var frm = new Appointments_Form_Reschedule(bc, (int)ClientParameters.clientID))
            {
                frm.ShowDialog();
            }
        }

        protected void CreateCancelAppointment()
        {
            ClientParameters.immediateAppointment = false;
            using (var frm = new Appointments_Form_Cancel(bc, (int)ClientParameters.clientID))
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected virtual void CreateClient(AARPSearchEventArgs searchParams)
        {
            // Do not use System.Transactions here. DTC does not work in this context. It fails when it tries to deal with the LINQ cache.
            // Allocate a database connection
            using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                System.Data.SqlClient.SqlTransaction txn = null;
                try
                {
                    cn.Open();

                    // Create a new transaction for it. Attach the transaction the BusinessContext so that it is properly
                    // defined for database access.
                    txn = cn.BeginTransaction();
                    using (var transaction_bc = new DebtPlus.LINQ.BusinessContext(cn))
                    {
                        transaction_bc.Transaction = txn;  // <-- CRITICAL. DON'T MESS WITH THIS STATEMENT!

                        // If the client is new, create a new client. Otherwise, update the client.
                        if (!isExistingClient)
                        {
                            CreateNewClient(searchParams, transaction_bc);
                        }
                        else
                        {
                            UpdateClient(searchParams, transaction_bc);
                        }

                        // Finally, submit the changes to the database to complete the client creation
                        transaction_bc.SubmitChanges();
                    }

                    // Commit the transaction to the database if we are successful.
                    txn.Commit();
                    txn.Dispose();
                    txn = null;
                }

                // Attach all errors through this logic at this point.
                catch
                {
                    // Rollback the transaction to ignore the client update/create logic.
                    if (txn != null)
                    {
                        // The rollback can itself have an error. Ignore those.
                        try { txn.Rollback(); } catch { }
                        txn.Dispose();
                        txn = null;
                    }

                    // Re-throw the error to continue with the error handlers.
                    throw;
                }
            }
        }

        public SearchEventArgs ClientParameters { get; internal set; }

        private void CreateNewClient(AARPSearchEventArgs searchParams, DebtPlus.LINQ.BusinessContext bc)
        {
            DebtPlus.LINQ.client clientRecord = getClient(bc, searchParams);
            if (clientRecord == null)
            {
                throw new ApplicationException("Client record could not be located nor created");
            }

            // Find the applicant record
            DebtPlus.LINQ.people applicantRecord = getApplicant(bc, clientRecord.Id);
            if (applicantRecord == null)
            {
                throw new ApplicationException("Applicant record could not be located nor created");
            }

            using (var trans = new DebtPlus.Svc.DataSets.ValueDescripitions())
            {
                // First, set the items that we don't log as a change
                clientRecord.AddressID       = GetAddress(bc, searchParams.address);
                clientRecord.HomeTelephoneID = GetTelephone(bc, searchParams.homePhone);

                // Supply the defaults for the items generated for a new client
                clientRecord.ClearChangedFieldItems();

                // Update the record with the items that are logged
                clientRecord.referred_by          = searchParams.referralSource;
                clientRecord.language             = searchParams.language;
                clientRecord.method_first_contact = searchParams.methodFirstContact.GetValueOrDefault(0);
                clientRecord.mortgage_problems    = searchParams.housingIssues;

                // new AARP fields

                // Generate the system note for the client changes
                {
                    var sbSubject = new System.Text.StringBuilder();
                    var sbNote    = new System.Text.StringBuilder();

                    foreach (ChangedFieldItem field in clientRecord.getChangedFieldItems)
                    {
                        sbNote.AppendFormat("\r\nChanged {0} to '{1}'", field.FieldName, trans.ClientStringValue(field.FieldName, trans.GetFieldType(clientRecord, field.FieldName), field.NewValue));
                        sbSubject.AppendFormat(",{0}", field.FieldName);
                    }

                    // If there is a cell telephone then we changed the cell telephone value
                    if (searchParams.homePhone != null)
                    {
                        sbSubject.Append(",HomePhone");
                        sbNote.AppendFormat("\r\nChanged HomePhone to '{0}'", searchParams.homePhone.ToString());
                    }

                    // If there is a referral source then we changed it too
                    if (searchParams.referralSource.HasValue)
                    {
                        sbSubject.Append(",referred_by");
                        sbNote.AppendFormat("\r\nChanged referred_by to '{0}'", trans.ClientStringValue("referred_by", trans.GetFieldType(clientRecord, "referred_by"), searchParams.referralSource));
                    }

                    // If there is an address then we changed it too
                    if (searchParams.address != null)
                    {
                        sbSubject.Append(",Address");
                        sbNote.AppendFormat("\r\nChanged Address to:\r\n\r\n'{0}'", searchParams.address.ToString());
                    }

                    // Toss the extra characters in the subject and notes
                    if (sbSubject.Length > 0)
                    {
                        // Do not allow the subject to be too long
                        sbSubject.Remove(0, 1);
                        sbSubject.Insert(0, "Changed ");
                        if (sbSubject.Length > 77)
                        {
                            sbSubject.Remove(77, sbSubject.Length - 77);
                            sbSubject.Append("...");
                        }

                        // Remove the leading cr/lf from the note text as well.
                        sbNote.Remove(0, 2);

                        // Generate the note and insert it into the pending updates
                        DebtPlus.LINQ.client_note newNote = new DebtPlus.LINQ.client_note()
                        {
                            client      = clientRecord.Id,
                            type        = 3,
                            subject     = sbSubject.ToString(),
                            note        = sbNote.ToString(),
                            dont_delete = true,
                            dont_edit   = true,
                            dont_print  = true
                        };

                        bc.client_notes.InsertOnSubmit(newNote);
                    }
                }

                if (searchParams.housingStatus.HasValue)
                {
                    var client_housingstatus = bc.client_housings.Where(x => x.Id == clientRecord.Id).FirstOrDefault();
                    bool exists = client_housingstatus != null;
                    if (!exists)
                    {
                        client_housingstatus = new client_housing();
                    }

                    client_housingstatus.Id = clientRecord.Id;
                    client_housingstatus.housing_status = searchParams.housingStatus.Value;

                    if (!exists)
                    {
                        bc.client_housings.InsertOnSubmit(client_housingstatus);
                    }
                }

                // First, set the items that are not logged
                applicantRecord.NameID          = GetName(bc, searchParams.applicant_name);
                applicantRecord.EmailID         = GetEmail(bc, searchParams.email);
                applicantRecord.WorkTelephoneID = GetTelephone(bc, searchParams.workTelephone);
                applicantRecord.CellTelephoneID = GetTelephone(bc, searchParams.cellTelephone);

                /* START NEW AARP FIELDS */
                applicantRecord.Birthdate = searchParams.dateOfBirth;

                applicantRecord.MilitaryStatusID    = searchParams.militaryStatus ?? 0;
                applicantRecord.MilitaryServiceID   = searchParams.militaryService ?? 0;
                applicantRecord.MilitaryGradeID     = searchParams.militaryGrade ?? 0;
                applicantRecord.MilitaryDependentID = searchParams.militaryDependent ?? 0;

                applicantRecord.job = searchParams.job;
                /* END NEW AARP FIELDS */

                // Reset the field change tracking information
                applicantRecord.ClearChangedFieldItems();

                // Set the items that are logged
                applicantRecord.Gender = searchParams.gender.GetValueOrDefault(1);

                {
                    var sbSubject = new System.Text.StringBuilder();
                    var sbNote = new System.Text.StringBuilder();
                    foreach (ChangedFieldItem field in applicantRecord.getChangedFieldItems)
                    {
                        sbNote.AppendFormat("\r\nChanged {0} to '{1}'", field.FieldName, trans.ClientStringValue(field.FieldName, trans.GetFieldType(clientRecord, field.FieldName), field.NewValue));
                        sbSubject.AppendFormat(",{0}", field.FieldName);
                    }

                    // If there is a name then insert that
                    if (searchParams.applicant_name != null)
                    {
                        sbSubject.Append(",Name");
                        sbNote.AppendFormat("\r\nChanged Name to '{0}'", searchParams.applicant_name.ToString());
                    }

                    // work telephone number
                    if (searchParams.workTelephone != null)
                    {
                        sbSubject.Append(",Work Phone");
                        sbNote.AppendFormat("\r\nChanged Work Phone Number to '{0}'", searchParams.workTelephone.ToString());
                    }

                    // cell telephone number
                    if (searchParams.cellTelephone != null)
                    {
                        sbSubject.Append(",Cell Phone");
                        sbNote.AppendFormat("\r\nChanged Mobile Phone Number to '{0}'", searchParams.cellTelephone.ToString());
                    }

                    // email address
                    if (searchParams.email != null)
                    {
                        sbSubject.Append(",Email");
                        sbNote.AppendFormat("\r\nChanged Email to '{0}'", searchParams.email.ToString());
                    }

                    // Toss the extra characters in the subject and notes
                    if (sbSubject.Length > 0)
                    {
                        // Do not allow the subject to be too long
                        sbSubject.Remove(0, 1);
                        sbSubject.Insert(0, "Changed Applicant ");
                        if (sbSubject.Length > 77)
                        {
                            sbSubject.Remove(77, sbSubject.Length - 77);
                            sbSubject.Append("...");
                        }
                        sbNote.Remove(0, 2);

                        // Generate the note and insert it into the pending updates
                        DebtPlus.LINQ.client_note newNote = new DebtPlus.LINQ.client_note()
                        {
                            client      = clientRecord.Id,
                            type        = 3,
                            subject     = sbSubject.ToString(),
                            note        = sbNote.ToString(),
                            dont_delete = true,
                            dont_edit   = true,
                            dont_print  = true
                        };

                        bc.client_notes.InsertOnSubmit(newNote);
                    }
                }
            }

            /* insert AARP call log data */
            DebtPlus.LINQ.aarp_call_log callLog = new aarp_call_log()
            {
                AarpCode = searchParams.AarpCode,
                // AppointmentID
                Btw50ProgramRequested   = searchParams.Btw50ProgramRequired,
                CallReason              = searchParams.CallReason,
                CallResolution          = searchParams.CallResolution,
                ClientID                = clientRecord.Id,
                EmailOptIn              = searchParams.EmailOptIn,
                HousingSituation        = searchParams.HousingSituation,
                InitialProgramRequested = (int)searchParams.InitialProgram,
                IsAarpClient            = searchParams.IsAarpClient,
                IsActive                = true,
                MediaFollowup           = searchParams.MediaFollowup,
                PreferredMethod         = searchParams.PreferredMethod,
                SnapProgramRequested    = searchParams.SnapProgramRequired,
                TextOptIn               = searchParams.TextOptIn,
                ReferralCode            = searchParams.referralSource
            };
            bc.aarp_call_logs.InsertOnSubmit(callLog);
        }

        private void UpdateClient(AARPSearchEventArgs searchParams, BusinessContext bc)
        {
            var client = (from c in bc.clients where c.Id == searchParams.clientID select c).FirstOrDefault();

            if (client != null)
            {
                client.language    = searchParams.language;
                client.referred_by = searchParams.referralSource;
            }

            var callLogs = bc.aarp_call_logs.Where(cl => cl.ClientID == searchParams.clientID && cl.IsActive == true).ToList();
            if (callLogs != null && callLogs.Count > 0)
            {
                foreach (var cl in callLogs)
                {
                    cl.IsActive = false;
                }
            }

            var newCallLog = new aarp_call_log()
            {
                AarpCode              = searchParams.AarpCode,
                Btw50ProgramRequested = searchParams.Btw50ProgramRequired,
                CallReason            = searchParams.CallReason,
                CallResolution        = searchParams.CallResolution,
                ClientID              = (int)searchParams.clientID,
                EmailOptIn            = searchParams.EmailOptIn,
                HousingSituation      = searchParams.HousingSituation,
                IsAarpClient          = searchParams.IsAarpClient,
                IsActive              = true,
                MediaFollowup         = searchParams.MediaFollowup,
                PreferredMethod       = searchParams.PreferredMethod,
                SnapProgramRequested  = searchParams.SnapProgramRequired,
                TextOptIn             = searchParams.TextOptIn,
                ReferralCode          = searchParams.referralSource
            };

            if (searchParams.InitialProgram != null)
            {
                newCallLog.InitialProgramRequested = (int)searchParams.InitialProgram;
            }
            bc.aarp_call_logs.InsertOnSubmit(newCallLog);

            var applicant = (from p in bc.peoples where p.Client == searchParams.clientID && p.Relation == 1 select p).FirstOrDefault();
            if (applicant != null)
            {
                applicant.Birthdate = searchParams.dateOfBirth;
                if (searchParams.gender != null)
                {
                    applicant.Gender = (int)searchParams.gender;
                }

                applicant.job                 = searchParams.job;
                applicant.MilitaryDependentID = searchParams.militaryDependent.GetValueOrDefault();
                applicant.MilitaryGradeID     = searchParams.militaryGrade.GetValueOrDefault();
                applicant.MilitaryServiceID   = searchParams.militaryService.GetValueOrDefault();

                if (searchParams.militaryStatus != null)
                {
                    applicant.MilitaryStatusID = (int)searchParams.militaryStatus;
                }

                var name = (from n in bc.Names where n.Id == applicant.NameID select n).FirstOrDefault();
                if (name != null && searchParams.applicant_name != null)
                {
                    name.First  = searchParams.applicant_name.First;
                    name.Last   = searchParams.applicant_name.Last;
                    name.Middle = searchParams.applicant_name.Middle;
                    name.Prefix = searchParams.applicant_name.Prefix;
                    name.Suffix = searchParams.applicant_name.Suffix;
                }

                var email = (from e in bc.EmailAddresses where e.Id == applicant.EmailID select e).FirstOrDefault();
                if (email != null && searchParams.email != null)
                {
                    email.Address = searchParams.email.Address;
                }

                var workPhone = (from t in bc.TelephoneNumbers where t.Id == applicant.WorkTelephoneID select t).FirstOrDefault();
                if (workPhone != null && searchParams.workTelephone != null)
                {
                    workPhone.Acode   = searchParams.workTelephone.Acode;
                    workPhone.Country = searchParams.workTelephone.Country;
                    workPhone.Ext     = searchParams.workTelephone.Ext;
                    workPhone.Number  = searchParams.workTelephone.Number;
                }

                var cellPhone = (from t in bc.TelephoneNumbers where t.Id == applicant.CellTelephoneID select t).FirstOrDefault();
                if (cellPhone != null && searchParams.cellTelephone != null)
                {
                    cellPhone.Acode   = searchParams.cellTelephone.Acode;
                    cellPhone.Country = searchParams.cellTelephone.Country;
                    cellPhone.Ext     = searchParams.cellTelephone.Ext;
                    cellPhone.Number  = searchParams.cellTelephone.Number;
                }

                var homePhone = (from t in bc.TelephoneNumbers where t.Id == client.HomeTelephoneID select t).FirstOrDefault();
                if (homePhone != null && searchParams.homePhone != null)
                {
                    homePhone.Acode   = searchParams.homePhone.Acode;
                    homePhone.Country = searchParams.homePhone.Country;
                    homePhone.Ext     = searchParams.homePhone.Ext;
                    homePhone.Number  = searchParams.homePhone.Number;
                }

                var address = (from a in bc.addresses where a.Id == client.AddressID select a).FirstOrDefault();
                if (address != null)
                {
                    address.address_line_2 = searchParams.address.address_line_2;
                    address.address_line_3 = searchParams.address.address_line_3;
                    address.attn           = searchParams.address.attn;
                    address.city           = searchParams.address.city;
                    address.direction      = searchParams.address.direction;
                    address.house          = searchParams.address.house;
                    address.modifier       = searchParams.address.modifier;
                    address.modifier_value = searchParams.address.modifier_value;
                    address.PostalCode     = searchParams.address.PostalCode;
                    address.state          = searchParams.address.state;
                    address.street         = searchParams.address.street;
                    address.suffix         = searchParams.address.suffix;
                }
            }
        }

        // <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected DebtPlus.LINQ.client getClient(DebtPlus.LINQ.BusinessContext bc, SearchEventArgs searchParams)
        {
            // If the client is not defined then define it.
            if (searchParams.clientID.GetValueOrDefault(0) < 1)
            {
                searchParams.clientID = bc.xpr_create_client(new Int32?(), searchParams.housingIssues, searchParams.referralSource, searchParams.methodFirstContact, false);
            }

            // Return the client record
            return bc.clients.Where(s => s.Id == searchParams.clientID).FirstOrDefault();
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected DebtPlus.LINQ.people getApplicant(DebtPlus.LINQ.BusinessContext bc, Int32 ClientID)
        {
            var q = bc.peoples.Where(s => s.Client == ClientID && s.Relation == DebtPlus.LINQ.Cache.RelationType.Self).FirstOrDefault();
            System.Diagnostics.Debug.Assert(q != null, "Applicant record could not be found.");

            if (q == null)
            {
                q          = DebtPlus.LINQ.Factory.Manufacture_people(ClientID);
                q.Relation = DebtPlus.LINQ.Cache.RelationType.Self;

                bc.peoples.InsertOnSubmit(q);
            }
            return q;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetName(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.Name name)
        {
            if (name == null || name.IsEmpty())
            {
                return new Int32?();
            }

            bc.Names.InsertOnSubmit(name);
            bc.SubmitChanges();
            return name.Id;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetEmail(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.EmailAddress email)
        {
            if (email == null || email.IsEmpty())
            {
                return new Int32?();
            }

            bc.EmailAddresses.InsertOnSubmit(email);
            bc.SubmitChanges();
            return email.Id;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetAddress(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.address address)
        {
            if (address == null || address.IsEmpty())
            {
                return new Int32?();
            }

            bc.addresses.InsertOnSubmit(address);
            bc.SubmitChanges();
            return address.Id;
        }

        /// <summary>
        /// Create the client and appointment if needed
        /// </summary>
        protected Int32? GetTelephone(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.TelephoneNumber phoneNumber)
        {
            if (phoneNumber == null || phoneNumber.IsEmpty())
            {
                return new Int32?();
            }

            bc.TelephoneNumbers.InsertOnSubmit(phoneNumber);
            bc.SubmitChanges();
            return phoneNumber.Id;
        }
    }
}
