﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    /// <summary>
    /// Perform a search for the client's telephone number
    /// </summary>
    internal static class ClientSearch
    {
        /// <summary>
        /// Perform a search for the client's telephone number
        /// </summary>
        public static System.Collections.Generic.List<DebtPlus.LINQ.xpr_client_create_searchResult> PerformSearch(SearchEventArgs e)
        {
            // Pad the area code on the left with zeros if needed
            string tempAcode = DebtPlus.Utils.Format.Strings.DigitsOnly(e.homePhone.Acode).PadRight(3, '0');
            tempAcode = tempAcode.Substring(tempAcode.Length - 3, 3);

            // Pad the telephone number on the left with zeros if needed
            string tempNumber = DebtPlus.Utils.Format.Strings.DigitsOnly(e.homePhone.Number).PadRight(7, '0');
            tempNumber = tempNumber.Substring(tempNumber.Length - 7, 7);

            // Generate the search key for the lists
            string searchKey = tempAcode + tempNumber;
            System.Diagnostics.Debug.Assert(searchKey.Length == 10);

            // Retrieve the results
            using (var bc = new BusinessContext())
            {
                return bc.xpr_client_create_search(searchKey);
            }
        }

        /// <summary>
        /// Do the client creation.
        /// </summary>
        internal static Int32 CreateClient(SearchEventArgs e)
        {
            using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                cn.Open();
                using (var txn = cn.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
                {
                    using (var bc = new DebtPlus.LINQ.BusinessContext(cn))
                    {
                        bc.Transaction = txn;
                        Int32 answer = -1;

                        // If there is a telephone number then use it for the client's home telephone number
                        // We use a transaction to void the telephone creation should the client fail to create.
                        if (e.homePhone != null)
                        {
                            bc.TelephoneNumbers.InsertOnSubmit(e.homePhone);
                            bc.SubmitChanges();
                            answer = bc.xpr_create_client(e.homePhone.Id, e.housingIssues, e.referralSource, e.methodFirstContact, e.bankruptcyIssues, null, e.inboundTelephoneNumber);
                        }
                        else
                        {
                            answer = bc.xpr_create_client(null, e.housingIssues, e.referralSource, e.methodFirstContact, e.bankruptcyIssues, null, e.inboundTelephoneNumber);
                        }

                        txn.Commit();
                        return answer;
                    }
                }
            }
        }
    }
}
