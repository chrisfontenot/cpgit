﻿namespace DebtPlus.UI.Desktop.CS.Client.Create
{
    partial class BaseControlWithOK
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(222, 254);
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_OK.Location = new System.Drawing.Point(126, 254);
            this.simpleButton_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.TabIndex = 5;
            this.simpleButton_OK.Text = "&OK";
            // 
            // BaseControlWithOK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.simpleButton_OK);
            this.Name = "BaseControlWithOK";
            this.Controls.SetChildIndex(this.simpleButton_Cancel, 0);
            this.Controls.SetChildIndex(this.simpleButton_OK, 0);
            this.ResumeLayout(false);

        }
        #endregion

        protected DevExpress.XtraEditors.SimpleButton simpleButton_OK;
    }
}
