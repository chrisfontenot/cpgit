﻿using System;

namespace DebtPlus.UI.Desktop.CS.Client.Create.ABT_Housing
{
    internal partial class FormCreateClient : FHLB_Housing.FormCreateClient_FHLB_Base
    {
        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public FormCreateClient() : base()
        {
            // Define the client as being ABT
            CreatedClientType = ClientType.CLIENT_TYPE_ABT;

            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load += Form_CreateClient_Load;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load -= Form_CreateClient_Load;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void Form_CreateClient_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Define the form location and size
                LoadPlacement("ClientCreateABT");
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}
