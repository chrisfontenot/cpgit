using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Globalization;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class MonthlyStatement_Myriad : MonthlyStatement
    {
        public MonthlyStatement_Myriad() : base()
        {
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public MonthlyStatement_Myriad(OutsourceArgParser ap) : base(ap)
        {
        }

        /// <summary>
        /// Write the Myraid file header
        /// </summary>
        protected override bool WriteFileHeader(DateTime CurrentTime, DateTime PeriodStart, DateTime PeriodEnd, string StatementNote)
        {
            bool answer = base.WriteFileHeader(CurrentTime, PeriodStart, PeriodEnd, StatementNote);

            // Split the note on word bounderies to form lines of at most 80 characters
            if (answer)
            {
                object[] BufferArea = new object[11];
                BufferArea[4] = StringIfy(string.Empty);
                BufferArea[5] = StringIfy(string.Empty);
                BufferArea[6] = StringIfy(string.Empty);
                BufferArea[7] = StringIfy(string.Empty);
                BufferArea[8] = StringIfy(string.Empty);

                Int32 nIndex = 3;
                while (nIndex < 5)
                {
                    StatementNote = StatementNote.Trim();
                    if (StatementNote.Length < 80)
                    {
                        BufferArea[nIndex] = StringIfy(StatementNote);
                        break;
                    }

                    // Find the first blank to break the string.
                    Int32 WordBreak = StatementNote.LastIndexOf(' ', 80);
                    if (WordBreak > 80)
                        WordBreak = 80;

                    // Break the string at the word break location
                    BufferArea[nIndex] = StringIfy(StatementNote.Substring(0, WordBreak).Trim());
                    StatementNote = StatementNote.Substring(WordBreak);
                    nIndex += 1;
                }

                // Complete the processing for the buffer areas.
                BufferArea[0] = StringIfy(PeriodStart);
                BufferArea[1] = StringIfy(PeriodEnd);
                BufferArea[2] = StringIfy(CurrentTime);

                // Write the corresponding string to the output file
                string TextLine = string.Format(CultureInfo.InvariantCulture, ap.FileHeader, BufferArea);
                WriteTextLine(TextLine);
            }

            return answer;
        }

        /// <summary>
        /// Write the Myraid detail record for the client
        /// </summary>
        protected override bool WriteStatementHeader(Int32 client, DateTime PeriodStart, DateTime PeriodEnd)
        {
            bool answer = base.WriteStatementHeader(client, PeriodStart, PeriodEnd);

            if (answer)
            {
                DataRow row = ds.Tables["client_statement_clients"].Rows.Find(client);
                if (row == null)
                {
                    answer = false;
                }
                else
                {
                    string counselor_name = string.Empty;
                    string expected_deposit_date = string.Empty;
                    string expected_deposit_amt = string.Empty;
                    decimal deposit_amt = 0m;
                    decimal refund_amt = 0m;
                    decimal starting_trust_balance = 0m;
                    decimal held_in_trust = 0m;

                    // Retrieve the fields from the client header
                    if (row["counselor_name"] != null && !object.ReferenceEquals(row["counselor_name"], DBNull.Value))
                        counselor_name = Convert.ToString(row["counselor_name"], CultureInfo.InvariantCulture).Trim();
                    if (row["expected_deposit_date"] != null && !object.ReferenceEquals(row["expected_deposit_date"], DBNull.Value))
                        expected_deposit_date = Convert.ToString(row["expected_deposit_date"], CultureInfo.InvariantCulture);
                    if (row["expected_deposit_amt"] != null && !object.ReferenceEquals(row["expected_deposit_amt"], DBNull.Value))
                        expected_deposit_amt = string.Format("{0:f0}", toCents(Convert.ToDecimal(row["expected_deposit_amt"], CultureInfo.InvariantCulture)));
                    if (row["deposit_amt"] != null && !object.ReferenceEquals(row["deposit_amt"], DBNull.Value))
                        deposit_amt = Convert.ToDecimal(row["deposit_amt"], CultureInfo.InvariantCulture);
                    if (row["refund_amt"] != null && !object.ReferenceEquals(row["refund_amt"], DBNull.Value))
                        refund_amt = Convert.ToDecimal(row["refund_amt"], CultureInfo.InvariantCulture);
                    if (row["starting_trust_balance"] != null && !object.ReferenceEquals(row["starting_trust_balance"], DBNull.Value))
                        starting_trust_balance = Convert.ToDecimal(row["starting_trust_balance"], CultureInfo.InvariantCulture);
                    if (row["held_in_trust"] != null && !object.ReferenceEquals(row["held_in_trust"], DBNull.Value))
                        held_in_trust = Convert.ToDecimal(row["held_in_trust"], CultureInfo.InvariantCulture);

                    if (ap.DebtHeader != string.Empty)
                    {
                        object[] BufferArea = new object[21];
                        BufferArea[0] = StringIfy(PeriodStart);
                        BufferArea[1] = StringIfy(PeriodEnd);
                        BufferArea[2] = StringIfy(string.Format("{0:0000000}", client));
                        BufferArea[3] = StringIfy(counselor_name);
                        BufferArea[4] = StringIfy(expected_deposit_date);
                        BufferArea[5] = StringIfy(expected_deposit_amt);
                        BufferArea[6] = StringIfy(toCents(deposit_amt));
                        BufferArea[7] = StringIfy(toCents(refund_amt));
                        BufferArea[8] = StringIfy(toCents(starting_trust_balance));
                        BufferArea[9] = StringIfy(toCents(held_in_trust));
                        BufferArea[10] = StringIfy(string.Empty);
                        // Client Alert Note
                        BufferArea[11] = BufferArea[10];
                        // Client name and address must be specified as empty strings.
                        BufferArea[12] = BufferArea[10];
                        BufferArea[13] = BufferArea[10];
                        BufferArea[14] = BufferArea[10];

                        // Include the client address information. Skip blank lines
                        Int32 nIndex = 11;
                        string txt = global::DebtPlus.Utils.Nulls.DStr(row["address_1"]);
                        if (txt != string.Empty)
                        {
                            BufferArea[nIndex] = StringIfy(txt);
                            nIndex += 1;
                        }

                        txt = global::DebtPlus.Utils.Nulls.DStr(row["address_2"]);
                        if (txt != string.Empty)
                        {
                            BufferArea[nIndex] = StringIfy(txt);
                            nIndex += 1;
                        }

                        txt = global::DebtPlus.Utils.Nulls.DStr(row["address_3"]);
                        if (txt != string.Empty)
                        {
                            BufferArea[nIndex] = StringIfy(txt);
                            nIndex += 1;
                        }

                        txt = global::DebtPlus.Utils.Nulls.DStr(row["address_4"]);
                        if (txt != string.Empty)
                        {
                            BufferArea[nIndex] = StringIfy(txt);
                            nIndex += 1;
                        }

                        // Write the debt header record (it is also the client information)
                        string TextLine = string.Format(CultureInfo.InvariantCulture, ap.DebtHeader, BufferArea);
                        WriteTextLine(TextLine);
                    }
                }
            }
            return answer;
        }

        protected override bool ProcessNextStatement(DateTime PeriodStart, DateTime PeriodEnd)
        {
            bool answer = true;

            // Process the data in the current debt record
            Int32 CurrentClient = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"));

            // If we can not write a header then we need to flush the details for this client
            if (!WriteStatementHeader(CurrentClient, PeriodStart, PeriodEnd))
            {
                do
                {
                    if (!StatementDetailsDataReader.Read())
                    {
                        answer = false;
                        break;
                    }
                } while (!(CurrentClient != StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"))));
                return answer;
            }

            // Information for the trailer record
            decimal Total_OriginalBalance = 0m;
            decimal Total_CurrentDisbursement = 0m;
            decimal Total_PaymentsToDate = 0m;
            decimal Total_CurrentBalance = 0m;
            Int32 CurrentDebtCount = 0;

            // Generate the lines for all of the client's debts until the ClientId changes.

            while (CurrentClient == StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client")))
            {
                // Generate the detial information
                string creditor_name = string.Empty;
                string account_number = string.Empty;
                decimal original_balance = 0m;
                decimal debits = 0m;
                decimal credits = 0m;
                decimal current_balance = 0m;
                decimal payments_to_date = 0m;
                double interest_rate = 0.0;

                for (Int32 FieldNo = 0; FieldNo <= StatementDetailsDataReader.FieldCount - 1; FieldNo++)
                {
                    if (!StatementDetailsDataReader.IsDBNull(FieldNo))
                    {
                        switch (StatementDetailsDataReader.GetName(FieldNo).ToLower())
                        {
                            case "creditor_name":
                                creditor_name = StatementDetailsDataReader.GetString(FieldNo).Trim();
                                break;

                            case "formatted_account_number":
                                account_number = StatementDetailsDataReader.GetString(FieldNo).Trim();
                                break;

                            case "original_balance":
                                original_balance = StatementDetailsDataReader.GetDecimal(FieldNo);
                                break;

                            case "net_debits":
                                debits = StatementDetailsDataReader.GetDecimal(FieldNo);
                                break;

                            case "credits":
                                credits = StatementDetailsDataReader.GetDecimal(FieldNo);
                                break;

                            case "interest_rate":
                                interest_rate = StatementDetailsDataReader.GetDouble(FieldNo);
                                break;

                            case "payments_to_date":
                                payments_to_date = StatementDetailsDataReader.GetDecimal(FieldNo);
                                break;

                            case "current_balance":
                                current_balance = StatementDetailsDataReader.GetDecimal(FieldNo);
                                break;

                            default:
                                break;
                                // ignore items that we don't want
                        }
                    }
                }

                // Make the strings have a limit
                if (creditor_name.Length > 25)
                    creditor_name = creditor_name.Substring(0, 25).Trim();
                if (account_number.Length > 13)
                    account_number = account_number.Substring(account_number.Length - 13, 13).Trim();

                // Update the global information
                Total_OriginalBalance += original_balance;
                Total_CurrentDisbursement += debits;
                Total_PaymentsToDate += payments_to_date;
                Total_CurrentBalance += current_balance;

                // Format the output detail line
                object[] BufferArea = new object[21];
                BufferArea[0] = StringIfy(creditor_name);
                BufferArea[1] = StringIfy(account_number);
                BufferArea[2] = StringIfy(toCents(original_balance));
                BufferArea[3] = StringIfy(toCents(debits));
                BufferArea[4] = StringIfy(toCents(credits));
                BufferArea[5] = StringIfy(toCents(payments_to_date));
                BufferArea[6] = StringIfy(toCents(current_balance));
                BufferArea[7] = interest_rate != 0.0 ? "*" : string.Empty;
                BufferArea[8] = credits != 0m ? "#" : string.Empty;
                BufferArea[9] = StringIfy(interest_rate, "{0:p}");

                string TextLine = string.Format(CultureInfo.InvariantCulture, ap.DebtDetail, BufferArea);
                WriteTextLine(TextLine);
                CurrentDebtCount += 1;

                // Go the next detail line. Stop reading at the end of the list
                if (!StatementDetailsDataReader.Read())
                {
                    answer = false;
                    break;
                }
            }

            // Write the trailer record if we can
            if (!WriteStatementTrailer(CurrentDebtCount, Total_OriginalBalance, Total_CurrentDisbursement, Total_PaymentsToDate, Total_CurrentBalance))
            {
                answer = false;
            }

            // Always advance the total number of debts written by the count for this client
            TotalDebtCount += CurrentDebtCount;

            return answer;
        }

        protected override bool WriteStatementTrailer(Int32 DebtsWritten, decimal Total_OriginalBalance, decimal total_CurrentDisbursement, decimal Total_PaymentsToDate, decimal Total_CurrentBalance)
        {
            bool answer = true;

            // Write the debt trailer
            if (ap.DebtTrailer != string.Empty)
            {
                object[] BufferArea = new object[6];
                BufferArea[0] = StringIfy(DebtsWritten);
                BufferArea[1] = StringIfy(toCents(Total_OriginalBalance));
                BufferArea[2] = StringIfy(toCents(total_CurrentDisbursement));
                BufferArea[3] = StringIfy(toCents(Total_PaymentsToDate));
                BufferArea[4] = StringIfy(toCents(Total_CurrentBalance));

                string TextLine = string.Format(CultureInfo.InvariantCulture, ap.DebtTrailer, BufferArea);
                WriteTextLine(TextLine);
            }

            return answer;
        }

        /// <summary>
        /// Write the Myraid file trailer
        /// </summary>
        protected override bool WriteFileTrailer(DateTime CurrentTime, Int32 LineCount, Int32 ClientCount, Int32 DebtCount)
        {
            bool answer = base.WriteFileTrailer(CurrentTime, LineCount, ClientCount, DebtCount);

            if (answer)
            {
                if (ap.FileTrailer != string.Empty)
                {
                    object[] BufferArea = new object[4];
                    BufferArea[0] = StringIfy(CurrentTime);
                    BufferArea[1] = StringIfy(LineCount);
                    BufferArea[2] = StringIfy(ClientCount);
                    BufferArea[3] = StringIfy(DebtCount);

                    WriteTextLine(string.Format(CultureInfo.InvariantCulture, ap.FileTrailer, BufferArea));
                }
            }

            return answer;
        }
    }
}