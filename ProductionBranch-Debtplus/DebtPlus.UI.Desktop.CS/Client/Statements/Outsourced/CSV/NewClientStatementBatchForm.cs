using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Data.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class NewClientStatementBatchForm : DebtPlusForm
    {
        private DataRowView drv;

        public NewClientStatementBatchForm() : base()
        {
            //This call is required by the Windows Form Designer.
            InitializeComponent();
            this.Load += NewClientStatementBatchForm_Load;

            //Add any initialization after the InitializeComponent() call
            var _with1 = disbursement_date;
            var _with2 = _with1.Properties;
            var _with3 = _with2.DisplayFormat;
            _with3.Format = new DisbursementCycleFormatter();
            _with3.FormatType = DevExpress.Utils.FormatType.Custom;
        }

        public NewClientStatementBatchForm(DataRowView DataRow) : this()
        {
            drv = DataRow;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.SpinEdit disbursement_date;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.MemoEdit note;
        internal DevExpress.XtraEditors.DateEdit period_start;

        internal DevExpress.XtraEditors.DateEdit period_end;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.disbursement_date = new DevExpress.XtraEditors.SpinEdit();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.note = new DevExpress.XtraEditors.MemoEdit();
            this.period_start = new DevExpress.XtraEditors.DateEdit();
            this.period_end = new DevExpress.XtraEditors.DateEdit();

            ((System.ComponentModel.ISupportInitialize)this.disbursement_date.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.note.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.period_start.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.period_end.Properties).BeginInit();
            this.SuspendLayout();

            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 19);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Group";
            //
            //disbursement_date
            //
            this.disbursement_date.EditValue = new decimal(new Int32[] {
                0,
                0,
                0,
                0
            });
            this.disbursement_date.Location = new System.Drawing.Point(96, 16);
            this.disbursement_date.Name = "disbursement_date";
            //
            //disbursement_date.Properties
            //
            this.disbursement_date.Properties.Appearance.Options.UseTextOptions = true;
            this.disbursement_date.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.disbursement_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.disbursement_date.Properties.IsFloatValue = false;
            this.disbursement_date.Properties.Mask.BeepOnError = true;
            this.disbursement_date.Properties.Mask.EditMask = "f0";
            this.disbursement_date.Properties.MaxLength = 2;
            this.disbursement_date.Properties.MaxValue = new decimal(new Int32[] {
                31,
                0,
                0,
                0
            });
            this.disbursement_date.TabIndex = 1;
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 43);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Start Date";
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(8, 67);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "End Date";
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(8, 91);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Note";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(112, 200);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.TabIndex = 8;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(208, 200);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.TabIndex = 9;
            this.Button_Cancel.Text = "&Cancel";
            //
            //note
            //
            this.note.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.note.Location = new System.Drawing.Point(96, 88);
            this.note.Name = "note";
            this.note.Size = new System.Drawing.Size(280, 96);
            this.note.TabIndex = 7;
            //
            //period_start
            //
            this.period_start.EditValue = null;
            this.period_start.Location = new System.Drawing.Point(96, 40);
            this.period_start.Name = "period_start";
            //
            //period_start.Properties
            //
            this.period_start.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.period_start.TabIndex = 3;
            //
            //period_end
            //
            this.period_end.EditValue = null;
            this.period_end.Location = new System.Drawing.Point(96, 64);
            this.period_end.Name = "period_end";
            //
            //period_end.Properties
            //
            this.period_end.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.period_end.TabIndex = 5;
            //
            //NewClientStatementBatchForm
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(392, 238);
            this.Controls.Add(this.period_end);
            this.Controls.Add(this.period_start);
            this.Controls.Add(this.note);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.disbursement_date);
            this.Controls.Add(this.LabelControl1);
            this.Name = "NewClientStatementBatchForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "NewClientStatementBatchForm";

            ((System.ComponentModel.ISupportInitialize)this.disbursement_date.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.note.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.period_start.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.period_end.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void NewClientStatementBatchForm_Load(object sender, EventArgs e)
        {
            // IF this is a new row then supply the default values
            if (drv.IsNew)
            {
                System.DateTime StartingDate = new System.DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
                System.DateTime EndingDate = new System.DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddSeconds(-1);
                drv["disbursement_date"] = 0;
                drv["period_start"] = StartingDate;
                drv["period_end"] = EndingDate;
                drv["note"] = string.Empty;
            }

            // Bind the record to the controls
            disbursement_date.DataBindings.Add("EditValue", drv, "disbursement_date");
            period_start.DataBindings.Add("EditValue", drv, "period_start");
            period_end.DataBindings.Add("EditValue", drv, "period_end");
            note.DataBindings.Add("EditValue", drv, "note");
        }

        private class DisbursementCycleFormatter : ICustomFormatter, IFormatProvider
        {
            public string Format(string formatString, object arg, IFormatProvider formatProvider)
            {
                Int32 value = global::DebtPlus.Utils.Nulls.DInt(arg);
                if (value == 0)
                    return "All";
                return value.ToString();
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }
        }
    }
}