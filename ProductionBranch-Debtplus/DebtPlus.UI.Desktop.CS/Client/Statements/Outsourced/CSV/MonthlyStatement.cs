using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DevExpress.Utils;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class MonthlyStatement : Statement
    {
        public MonthlyStatement() : base()
        {
        }

        protected Int32 TotalClientCount;
        protected Int32 ExpectedClientCount;
        protected Int32 TotalDebtCount;

        protected SqlDataReader StatementDetailsDataReader;

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public MonthlyStatement(OutsourceArgParser ap) : base(ap)
        {
        }

        /// <summary>
        /// Read the data records from the database
        /// </summary>
        protected virtual bool FillDataSet()
        {
            bool answer = false;
            DataTable tbl = null;

            using (WaitDialogForm dlg = new WaitDialogForm())
            {
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                try
                {
                    using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
                    {
                        dlg.Show();

                        cn.Open();
                        dlg.SetCaption("Reading client statement information");
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandTimeout = 0;
                            cmd.Connection = cn;
                            cmd.CommandText = ap.SelectStatement;
                            cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int).Value = ap.GetBatch();

                            // Ask the database to fill our local offline buffer area
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, "client_statement_clients");
                            }
                        }
                    }

                    // Ensure that there is a primary key to the table because we want the information sorted
                    tbl = ds.Tables["client_statement_clients"];
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["client"] };
                    ExpectedClientCount = tbl.Rows.Count;

                    // We are successful to this point.
                    answer = true;
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information");
                }
                finally
                {
                    dlg.Close();

                    if (cn != null)
                        cn.Dispose();
                }
            }

            return answer;
        }

        /// <summary>
        /// Format the output record and write it to the output file
        /// </summary>
        protected virtual bool WriteFileHeader(DateTime CurrentTime, DateTime PeriodStart, DateTime PeriodEnd, string StatementNote)
        {
            return true;
        }

        /// <summary>
        /// Write the header at the start of each client statement
        /// </summary>
        protected virtual bool WriteStatementHeader(Int32 client, DateTime PeriodStart, DateTime PeriodEnd)
        {
            return true;
        }

        /// <summary>
        /// Generate the next client statement
        /// </summary>
        protected virtual bool ProcessNextStatement(DateTime PeriodStart, DateTime PeriodEnd)
        {
            return true;
        }

        /// <summary>
        /// Write the trailer at the end of the client statement
        /// </summary>
        protected virtual bool WriteStatementTrailer(Int32 DebtCount, decimal Total_OriginalBalance, decimal total_CurrentDisbursement, decimal Total_PaymentsToDate, decimal Total_CurrentBalance)
        {
            return true;
        }

        /// <summary>
        /// Format the output record and write it to the output file
        /// </summary>
        protected virtual bool WriteFileTrailer(DateTime CurrentTime, Int32 LineCount, Int32 ClientCount, Int32 DebtCount)
        {
            return true;
        }

        /// <summary>
        /// Write all records to the output file
        /// </summary>
        public override bool WriteStatements()
        {
            bool answer = base.WriteStatements();

            DateTime CurrentTime = default(DateTime);
            DateTime PeriodStart = DateTime.MinValue;
            DateTime PeriodEnd = DateTime.MaxValue;

            // Retrieve the database information
            answer = FillDataSet();
            if (answer)
            {
                if (ds.Tables[0].Rows.Count <= 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("There do not seem to be any clients for this batch.", "Sorry, but there are no clients", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    answer = false;
                }
            }

            // Open the output file
            if (answer)
            {
                answer = CreateOutputFile();
            }

            // Do the work here
            string StatementNote = string.Empty;

            if (answer)
            {
                if (ap.FileHeader != string.Empty)
                {
                    // Generate the standard file header
                    SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

                    try
                    {
                        using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
                        {
                            cn.Open();
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.CommandText = "SELECT note, statement_date, period_start, period_end FROM client_statement_batches WITH (NOLOCK) WHERE client_statement_batch=@client_statement_batch";
                                cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int).Value = ap.GetBatch();
                                StatementDetailsDataReader = cmd.ExecuteReader(CommandBehavior.SingleRow);
                            }
                        }

                        answer = StatementDetailsDataReader.Read();
                        if (answer)
                        {
                            if (!StatementDetailsDataReader.IsDBNull(0))
                                StatementNote = StatementDetailsDataReader.GetString(0).Trim();
                            if (!StatementDetailsDataReader.IsDBNull(1))
                                CurrentTime = StatementDetailsDataReader.GetDateTime(1);
                            if (!StatementDetailsDataReader.IsDBNull(2))
                                PeriodStart = StatementDetailsDataReader.GetDateTime(2);
                            if (!StatementDetailsDataReader.IsDBNull(3))
                                PeriodEnd = StatementDetailsDataReader.GetDateTime(3);
                        }

                        // Close down the reader so that the conntection may be used again.
                        StatementDetailsDataReader.Close();

                        TotalDebtCount = 0;
                        TotalClientCount = 0;

                        if (answer)
                        {
                            // Write the header for the file
                            answer = WriteFileHeader(CurrentTime, PeriodStart, PeriodEnd, StatementNote);

                            using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Connection = cn;
                                    cmd.CommandText = "SELECT creditor_name, formatted_account_number, original_balance, net_debits, credits, interest_rate, payments_to_date, current_balance, client, client_statement_batch, creditor FROM view_client_statement_details WITH (NOLOCK) WHERE client_statement_batch=@client_statement_batch AND client > 0 ORDER BY client_statement_batch, client, creditor";
                                    cmd.CommandTimeout = Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.Add("@client_statement_batch", SqlDbType.Int).Value = ap.GetBatch();
                                    StatementDetailsDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);
                                }
                            }

                            if (StatementDetailsDataReader != null && StatementDetailsDataReader.Read())
                            {
                                do
                                {
                                    TotalClientCount += 1;
                                    RaiseStatistics(Convert.ToInt32(StatementDetailsDataReader["client"]), ExpectedClientCount, TotalClientCount, TotalLineCount);
                                    if (!ProcessNextStatement(PeriodStart, PeriodEnd))
                                        break;
                                } while (true);
                            }

                            RaiseStatistics(0, TotalClientCount, TotalClientCount, TotalLineCount);
                            WriteFileTrailer(CurrentTime, TotalLineCount, TotalClientCount, TotalDebtCount);
                            CloseOutputFile();
                        }
                    }
                    catch (SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error writing header information");
                    }
                    finally
                    {
                        if (StatementDetailsDataReader != null)
                        {
                            StatementDetailsDataReader.Dispose();
                        }

                        if (cn != null)
                        {
                            cn.Dispose();
                        }
                    }
                }
            }

            return answer;
        }
    }
}