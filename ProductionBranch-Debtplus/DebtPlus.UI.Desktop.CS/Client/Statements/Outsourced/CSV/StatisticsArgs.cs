#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class StatisticsArgs : EventArgs
    {
        public StatisticsArgs() : base()
        {
        }

        /// <summary>
        /// Create an instance of this class
        /// </summary>
        public StatisticsArgs(Int32 client, Int32 TotalClientCount, Int32 CurrentClientCount, Int32 output_line_count) : base()
        {
            _client = client;
            _TotalClientCount = TotalClientCount;
            _CurrentClientCount = CurrentClientCount;
            _ProcessedClientCount = CurrentClientCount;
            _output_line_count = output_line_count;
        }

        public StatisticsArgs(Int32 client, Int32 TotalClientCount, Int32 CurrentClientCount, Int32 ProcessedClientCount, Int32 output_line_count) : base()
        {
            _client = client;
            _TotalClientCount = TotalClientCount;
            _CurrentClientCount = CurrentClientCount;
            _ProcessedClientCount = ProcessedClientCount;
            _output_line_count = output_line_count;
        }

        /// <summary>
        /// Client ID number
        /// </summary>

        private Int32 _client;

        public Int32 client
        {
            get { return _client; }
        }

        /// <summary>
        /// Number of clients in the table to be processed
        /// </summary>

        private Int32 _TotalClientCount;

        public Int32 TotalClientCount
        {
            get { return _TotalClientCount; }
        }

        /// <summary>
        /// Number of clients in the table that have been processed
        /// </summary>

        private Int32 _CurrentClientCount;

        public Int32 CurrentClientCount
        {
            get { return _CurrentClientCount; }
        }

        /// <summary>
        /// Number of clients in the table that have been processed
        /// </summary>

        private Int32 _ProcessedClientCount;

        public Int32 ProcessedClientCount
        {
            get { return _ProcessedClientCount; }
        }

        /// <summary>
        /// Number of lines written to the output file
        /// </summary>

        private Int32 _output_line_count;

        public Int32 output_line_count
        {
            get { return _output_line_count; }
        }
    }
}