using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraEditors;
using System.Threading;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class Statistics : XtraUserControl
    {
        private delegate void ThreadDelegate();

        private ThreadClass ThreadItem;
        internal LabelControl LabelControlRemainingTime;
        internal LabelControl LabelControl3;

        public event EventHandler Completed;

        internal Thread thrd;

        public Statistics() : base()
        {
            InitializeComponent();
            Timer1.Tick += Timer1_Tick;
            Button_Cancel.Click += Button_Cancel_Click;
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl lbl_client;
        internal DevExpress.XtraEditors.LabelControl lbl_client_count;
        internal DevExpress.XtraEditors.LabelControl lbl_line_count;
        internal DevExpress.XtraEditors.LabelControl LabelControl6;
        internal DevExpress.XtraEditors.LabelControl LabelControl7;
        internal DevExpress.XtraEditors.LabelControl LabelControl8;
        internal System.Windows.Forms.Timer Timer1;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.ProgressBarControl ProgressBarControl1;

        internal DevExpress.XtraEditors.LabelControl elapsed_time;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_client = new DevExpress.XtraEditors.LabelControl();
            this.lbl_client_count = new DevExpress.XtraEditors.LabelControl();
            this.lbl_line_count = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.ProgressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            this.elapsed_time = new DevExpress.XtraEditors.LabelControl();
            this.LabelControlRemainingTime = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(40, 16);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(45, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Client ID:";
            this.LabelControl1.UseMnemonic = false;
            //
            //lbl_client
            //
            this.lbl_client.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.lbl_client.Appearance.Options.UseFont = true;
            this.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_client.Location = new System.Drawing.Point(181, 16);
            this.lbl_client.Name = "lbl_client";
            this.lbl_client.Size = new System.Drawing.Size(99, 13);
            this.lbl_client.TabIndex = 1;
            this.lbl_client.Text = "----";
            this.lbl_client.UseMnemonic = false;
            //
            //lbl_client_count
            //
            this.lbl_client_count.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.lbl_client_count.Appearance.Options.UseFont = true;
            this.lbl_client_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_client_count.Location = new System.Drawing.Point(181, 38);
            this.lbl_client_count.Name = "lbl_client_count";
            this.lbl_client_count.Size = new System.Drawing.Size(99, 13);
            this.lbl_client_count.TabIndex = 2;
            this.lbl_client_count.Text = "----";
            this.lbl_client_count.UseMnemonic = false;
            //
            //lbl_line_count
            //
            this.lbl_line_count.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.lbl_line_count.Appearance.Options.UseFont = true;
            this.lbl_line_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_line_count.Location = new System.Drawing.Point(181, 60);
            this.lbl_line_count.Name = "lbl_line_count";
            this.lbl_line_count.Size = new System.Drawing.Size(99, 13);
            this.lbl_line_count.TabIndex = 3;
            this.lbl_line_count.Text = "----";
            this.lbl_line_count.UseMnemonic = false;
            //
            //LabelControl6
            //
            this.LabelControl6.Location = new System.Drawing.Point(40, 82);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(66, 13);
            this.LabelControl6.TabIndex = 7;
            this.LabelControl6.Text = "Elapsed Time:";
            this.LabelControl6.UseMnemonic = false;
            //
            //LabelControl7
            //
            this.LabelControl7.Location = new System.Drawing.Point(40, 60);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(54, 13);
            this.LabelControl7.TabIndex = 6;
            this.LabelControl7.Text = "# Records:";
            this.LabelControl7.UseMnemonic = false;
            //
            //LabelControl8
            //
            this.LabelControl8.Location = new System.Drawing.Point(40, 38);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(47, 13);
            this.LabelControl8.TabIndex = 5;
            this.LabelControl8.Text = "# Clients:";
            this.LabelControl8.UseMnemonic = false;
            //
            //Timer1
            //
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Location = new System.Drawing.Point(165, 197);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 10;
            this.Button_Cancel.Text = "&Cancel";
            //
            //ProgressBarControl1
            //
            this.ProgressBarControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.ProgressBarControl1.Location = new System.Drawing.Point(3, 138);
            this.ProgressBarControl1.Name = "ProgressBarControl1";
            this.ProgressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.ProgressBarControl1.Size = new System.Drawing.Size(392, 18);
            this.ProgressBarControl1.TabIndex = 11;
            //
            //elapsed_time
            //
            this.elapsed_time.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.elapsed_time.Appearance.Options.UseFont = true;
            this.elapsed_time.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.elapsed_time.Location = new System.Drawing.Point(181, 82);
            this.elapsed_time.Name = "elapsed_time";
            this.elapsed_time.Size = new System.Drawing.Size(99, 13);
            this.elapsed_time.TabIndex = 12;
            this.elapsed_time.Text = "----";
            this.elapsed_time.UseMnemonic = false;
            //
            //LabelControlRemainingTime
            //
            this.LabelControlRemainingTime.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.LabelControlRemainingTime.Appearance.Options.UseFont = true;
            this.LabelControlRemainingTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControlRemainingTime.Location = new System.Drawing.Point(181, 104);
            this.LabelControlRemainingTime.Name = "LabelControlRemainingTime";
            this.LabelControlRemainingTime.Size = new System.Drawing.Size(99, 13);
            this.LabelControlRemainingTime.TabIndex = 14;
            this.LabelControlRemainingTime.Text = "----";
            this.LabelControlRemainingTime.UseMnemonic = false;
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(40, 104);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(103, 13);
            this.LabelControl3.TabIndex = 13;
            this.LabelControl3.Text = "Estimated Remaining:";
            this.LabelControl3.UseMnemonic = false;
            //
            //Statistics
            //
            this.Controls.Add(this.LabelControlRemainingTime);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.elapsed_time);
            this.Controls.Add(this.ProgressBarControl1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.LabelControl8);
            this.Controls.Add(this.lbl_line_count);
            this.Controls.Add(this.lbl_client_count);
            this.Controls.Add(this.lbl_client);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Statistics";
            this.Size = new System.Drawing.Size(408, 240);
            ((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Generate the output file once we have the information
        /// </summary>

        public void Run(OutsourceArgParser Args)
        {
            var _with1 = ProgressBarControl1;
            _with1.Enabled = true;

            // Start the timer when the thread is about to start
            var _with2 = Timer1;
            _with2.Enabled = true;
            _with2.Interval = 1000;
            _with2.Start();
            BaseTime = DateTime.Now;

            // Create a thread to process the output request
            ThreadItem = new ThreadClass(Args, new ThreadDelegate(StatusProc), new ThreadDelegate(CompletionProc));
            thrd = new Thread(ThreadItem.Run);
            var _with3 = thrd;
            _with3.SetApartmentState(ApartmentState.STA);
            _with3.IsBackground = true;
            _with3.Priority = ThreadPriority.Normal;
            _with3.Start();
        }

        /// <summary>
        /// Statistics event processing
        /// </summary>

        private void StatusProc()
        {
            // Ensure that we are in the proper thread to update the display.
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(StatusProc));
            }
            else
            {
                // Display the information on the status window
                var _with4 = ThreadItem;
                if (_with4.CurrentClientCount < 4)
                {
                    ProgressBarControl1.Properties.Maximum = _with4.TotalClientCount;
                    ProgressBarControl1.Properties.Minimum = 0;
                }
                ProgressBarControl1.Position = _with4.CurrentClientCount;

                lbl_client.Text = string.Format("{0:0000000}", _with4.client);
                lbl_client_count.Text = string.Format("{0:n0}", _with4.CurrentClientCount);
                lbl_line_count.Text = string.Format("{0:n0}", _with4.output_line_count);

                // Calculate the estimated time remaining
                TimeSpan Diff = DateTime.Now.Subtract(BaseTime);
                double Seconds = Diff.TotalSeconds;

                // Calculate the number of clients per second
                if (_with4.CurrentClientCount > 10 && Seconds > 10.0)
                {
                    double ClientsPerSecond = Convert.ToDouble(_with4.CurrentClientCount) / Seconds;
                    if (ClientsPerSecond > 0)
                    {
                        Int32 ClientsRemaining = _with4.TotalClientCount - _with4.ProcessedClientCount;
                        if (ClientsRemaining < 0)
                            ClientsRemaining = 0;
                        Int32 SecondsRemaining = Convert.ToInt32(Convert.ToDouble(ClientsRemaining) / ClientsPerSecond);

                        Int32 Hours = SecondsRemaining / 3600;
                        SecondsRemaining = SecondsRemaining % 3600;

                        Int32 Minutes = SecondsRemaining / 60;
                        SecondsRemaining = SecondsRemaining % 60;

                        // Update the expected remaining time
                        LabelControlRemainingTime.Text = string.Format("{0:f0}:{1:00}:{2:00}", Hours, Minutes, SecondsRemaining);
                    }
                }
            }
        }

        /// <summary>
        /// Completion event processing
        /// </summary>

        private void CompletionProc()
        {
            // Ensure that we are in the proper thread to process the message.
            if (InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(CompletionProc));
            }
            else
            {
                // Stop the elapsed timer
                var _with5 = Timer1;
                _with5.Stop();
                _with5.Enabled = false;

                // Stop the progress bar
                var _with6 = ProgressBarControl1;
                _with6.Properties.Maximum = 100;
                _with6.Properties.Minimum = 0;
                _with6.Position = 100;

                // Tell the user that the processing is complete and wait for the Close button.
                thrd = null;
                Button_Cancel.Text = "Close";
                DebtPlus.Data.Forms.MessageBox.Show("The file export is complete", "Operation Completed", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Handle the tick of the timer event
        /// </summary>
        private void Timer1_Tick(object sender, EventArgs e)
        {
            ShowElapsed();
        }

        /// <summary>
        /// Display the elapsed time since we started the operation
        /// </summary>

        private System.DateTime BaseTime;

        private void ShowElapsed()
        {
            TimeSpan difference = DateTime.Now.Subtract(BaseTime);
            elapsed_time.Text = string.Format("{0:f0}:{1:00}:{2:00}", difference.Hours, difference.Minutes, difference.Seconds);
        }

        /// <summary>
        /// Handle the Cancel button
        /// </summary>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            if (thrd != null)
                thrd.Abort();
            if (Completed != null)
            {
                Completed(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Private class to wrap the processing
        /// </summary>
        private class ThreadClass
        {
            private OutsourceArgParser ap;
            private ThreadDelegate StatusProc;

            private ThreadDelegate CompletionProc;
            public Int32 client;
            public Int32 output_line_count;
            public Int32 TotalClientCount;
            public Int32 CurrentClientCount;

            public Int32 ProcessedClientCount;

            public ThreadClass(OutsourceArgParser ap, ThreadDelegate StatusProc, ThreadDelegate CompletionProc)
            {
                this.ap = ap;
                this.StatusProc = StatusProc;
                this.CompletionProc = CompletionProc;
            }

            /// <summary>
            /// Run the thread
            /// </summary>
            internal void Run()
            {
                if (ap.Monthly)
                {
                    MonthlyStatement_Myriad clsx = new MonthlyStatement_Myriad(ap);
                    clsx.Statistics += StatsRoutine;
                    clsx.WriteStatements();
                }
                else
                {
                    QuarterlyStatement_Myriad clsx = new QuarterlyStatement_Myriad(ap);
                    clsx.Statistics += StatsRoutine;
                    clsx.WriteStatements();
                }

                // Tell the world that we are complete now
                CompletionProc.Invoke();
            }

            /// <summary>
            /// Handle the statistics routine
            /// </summary>
            private void StatsRoutine(object sender, StatisticsArgs e)
            {
                TotalClientCount = e.TotalClientCount;
                CurrentClientCount = e.CurrentClientCount;
                ProcessedClientCount = e.ProcessedClientCount;
                client = e.client;
                output_line_count = e.output_line_count;
                StatusProc.Invoke();
            }
        }
    }
}