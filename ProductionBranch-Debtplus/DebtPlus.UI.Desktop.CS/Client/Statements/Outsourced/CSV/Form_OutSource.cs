using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using DebtPlus.Data.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class Form_OutSource : DebtPlusForm
    {
        /// <summary>
        /// Initialize the class with the normal sequence
        /// </summary>
        private OutsourceArgParser Args;

        public Form_OutSource() : base()
        {
            InitializeComponent();
            GotFocus += Form_OutSource_GotFocus;
            Load += Form_OutSource_Load;
            Closing += Form_OutSource_Closing;
            Statistics1.Completed += Statistics1_Completed;
            ClientStatementBatch1.Canceled += ClientStatementBatch1_Canceled;
            ClientStatementBatch1.Selected += ClientStatementBatch1_Selected;
        }

        public Form_OutSource(OutsourceArgParser ap) : this()
        {
            Args = ap;

            // Pass along the flag to indicate that we only want to update the notes.
            ClientStatementBatch1.NotesOnly = ap.NotesOnly;

            // If monthly then select only one batch. Quarterly requires 3.
            if (ap.Monthly)
            {
                ClientStatementBatch1.BatchCount = 1;
            }
            else
            {
                ClientStatementBatch1.BatchCount = 3;
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal NewClientStatementBatch ClientStatementBatch1;

        internal global::DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV.Statistics Statistics1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.ClientStatementBatch1 = new global::DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV.NewClientStatementBatch();
            this.Statistics1 = new global::DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV.Statistics();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //ClientStatementBatch1
            //
            this.ClientStatementBatch1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ClientStatementBatch1.Location = new System.Drawing.Point(0, 0);
            this.ClientStatementBatch1.Name = "ClientStatementBatch1";
            this.ClientStatementBatch1.NotesOnly = false;
            this.ClientStatementBatch1.Size = new System.Drawing.Size(528, 266);
            this.ClientStatementBatch1.TabIndex = 0;
            //
            //Statistics1
            //
            this.Statistics1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Statistics1.Location = new System.Drawing.Point(0, 0);
            this.Statistics1.Name = "Statistics1";
            this.Statistics1.Size = new System.Drawing.Size(528, 266);
            this.Statistics1.TabIndex = 1;
            //
            //Form_OutSource
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(528, 266);
            this.Controls.Add(this.Statistics1);
            this.Controls.Add(this.ClientStatementBatch1);
            this.Name = "Form_OutSource";
            this.Text = "Generate Client Statements for Outsource Agency";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Process the CANCEL event from the statement list
        /// </summary>
        private void ClientStatementBatch1_Canceled(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// The statement batch list is complete. Go to the next pannel.
        /// </summary>

        private void ClientStatementBatch1_Selected(object sender, EventArgs e)
        {
            if (!ClientStatementBatch1.NotesOnly)
            {
                Args.SetBatch(0, ClientStatementBatch1.SelectedBatch(0));
                if (!Args.Monthly)
                {
                    Args.SetBatch(1, ClientStatementBatch1.SelectedBatch(1));
                    Args.SetBatch(2, ClientStatementBatch1.SelectedBatch(2));
                }
                GenerateOutput();
            }
        }

        /// <summary>
        /// Run the output statement processing logic
        /// </summary>

        private void GenerateOutput()
        {
            // Disable the client statement portion of the form
            var _with1 = ClientStatementBatch1;
            _with1.Visible = false;

            // Enable the statistics and run the process
            var _with2 = Statistics1;
            _with2.Visible = true;
            _with2.Run(Args);
        }

        /// <summary>
        /// Remove the topmost status when we get the focus
        /// </summary>
        private void Form_OutSource_GotFocus(object sender, EventArgs e)
        {
            TopMost = false;
        }

        /// <summary>
        /// On the form load, enable the list of batches
        /// </summary>

        private void Form_OutSource_Load(object sender, EventArgs e)
        {
            // Determine if we have the batches that we need to process the operation
            bool Complete = (Args.GetBatch(2) >= 0);
            if (!Complete && Args.Monthly && Args.GetBatch(0) >= 0)
                Complete = true;

            // If we are not complete the enable the input form.
            if (Complete)
            {
                GenerateOutput();
            }
            else
            {
                var _with3 = Statistics1;
                _with3.Visible = false;

                var _with4 = ClientStatementBatch1;
                _with4.Visible = true;
                _with4.Reload();
            }
        }

        /// <summary>
        /// When our file is generated, terminate the form
        /// </summary>
        private void Statistics1_Completed(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// We are closing the form. Abort the processing thread.
        /// </summary>
        private void Form_OutSource_Closing(object sender, CancelEventArgs e)
        {
            var _with5 = Statistics1;
            if (_with5.Visible && _with5.thrd != null)
                _with5.thrd.Abort();
        }
    }
}