using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DevExpress.Utils;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class QuarterlyStatement : Statement
    {
        protected Int32 TotalClientCount = 0;
        protected Int32 ExpectedClientCount = 0;
        protected Int32 TotalDebtCount = 0;

        protected SqlDataReader StatementDetailsDataReader = null;

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public QuarterlyStatement(OutsourceArgParser ap) : base(ap)
        {
        }

        /// <summary>
        /// Read the data records from the database
        /// </summary>
        protected virtual bool FillDataSet()
        {
            bool answer = false;

            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = Cursor.Current;
            WaitDialogForm dlg = new WaitDialogForm();
            DataTable tbl = null;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dlg.Show();

                cn.Open();
                dlg.SetCaption("Reading statement batch information");
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = cn;
                    _with1.CommandText = "SELECT * FROM client_statement_batches WHERE client_statement_batch IN (@batch_0,@batch_1,@batch_2)";
                    _with1.Parameters.Add("@batch_0", SqlDbType.Int).Value = ap.GetBatch(0);
                    _with1.Parameters.Add("@batch_1", SqlDbType.Int).Value = ap.GetBatch(1);
                    _with1.Parameters.Add("@batch_2", SqlDbType.Int).Value = ap.GetBatch(2);

                    // Ask the database to fill our local offline buffer area
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "client_statement_batches");
                    }
                }

                // Ensure that there is a primary key to the table because we want the information sorted
                tbl = ds.Tables["client_statement_batches"];
                var _with2 = tbl;
                _with2.PrimaryKey = new DataColumn[] { _with2.Columns["client_statement_batch"] };

                // Set the batch IDs in the proper sequence for processing. We need them in the order of date ranges
                using (DataView vue = new DataView(tbl, string.Empty, "period_start, period_end, client_statement_batch", DataViewRowState.CurrentRows))
                {
                    foreach (DataRowView drv in vue)
                    {
                        ap.SetBatch(0, ap.GetBatch(1));
                        ap.SetBatch(1, ap.GetBatch(2));
                        ap.SetBatch(2, Convert.ToInt32(drv["client_statement_batch"]));
                    }
                }

                // Read the client information
                dlg.SetCaption("Reading client statement information");
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with3 = cmd;
                    _with3.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    _with3.Connection = cn;
                    _with3.CommandText = "SELECT * FROM view_client_statement_clients WITH (NOLOCK) WHERE mail_error_date IS NULL AND client_statement_batch IN (@batch_0,@batch_1,@batch_2)";
                    _with3.Parameters.Add("@batch_0", SqlDbType.Int).Value = ap.GetBatch(0);
                    _with3.Parameters.Add("@batch_1", SqlDbType.Int).Value = ap.GetBatch(1);
                    _with3.Parameters.Add("@batch_2", SqlDbType.Int).Value = ap.GetBatch(2);

                    // Ask the database to fill our local offline buffer area
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "client_statement_clients");
                    }
                }

                // Ensure that there is a primary key to the table because we want the information sorted
                tbl = ds.Tables["client_statement_clients"];
                var _with4 = tbl;
                _with4.PrimaryKey = new DataColumn[] {
                    _with4.Columns["client"],
                    _with4.Columns["client_statement_batch"]
                };

                // We are successful to this point.
                answer = true;
            }
            catch (SqlException ex)
            {
                dlg.Close();
                dlg = null;
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information");
            }
            finally
            {
                if (dlg != null)
                {
                    dlg.Close();
                    dlg = null;
                }

                if (cn != null)
                    cn.Dispose();

                Cursor.Current = current_cursor;
            }

            return answer;
        }

        /// <summary>
        /// Format the output record and write it to the output file
        /// </summary>
        protected virtual bool WriteFileHeader(DataRow Batch1, DataRow Batch2, DataRow Batch3)
        {
            return true;
        }

        /// <summary>
        /// Write the header at the start of each client statement
        /// </summary>
        protected virtual bool WriteStatementHeader(Int32 client, DateTime PeriodStart, DateTime PeriodEnd)
        {
            return true;
        }

        /// <summary>
        /// Generate the next client statement
        /// </summary>
        protected virtual bool ProcessNextStatement(DateTime PeriodStart, DateTime PeriodEnd, Int32 Batch_0, Int32 Batch_1, Int32 Batch_2)
        {
            return true;
        }

        /// <summary>
        /// Write the trailer at the end of the client statement
        /// </summary>
        protected virtual bool WriteStatementTrailer(Int32 DebtCount, decimal Total_OriginalBalance, decimal total_CurrentDisbursement, decimal Total_PaymentsToDate, decimal Total_CurrentBalance)
        {
            return true;
        }

        /// <summary>
        /// Format the output record and write it to the output file
        /// </summary>
        protected virtual bool WriteFileTrailer(DateTime CurrentTime, Int32 LineCount, Int32 ClientCount, Int32 DebtCount)
        {
            return true;
        }

        /// <summary>
        /// Write all records to the output file
        /// </summary>
        public override bool WriteStatements()
        {
            bool answer = base.WriteStatements();

            // Retrieve the database information
            answer = FillDataSet();
            if (answer)
            {
                if (ds.Tables[0].Rows.Count <= 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("There do not seem to be any clients for this batch.", "Sorry, but there are no clients", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    answer = false;
                }
            }

            // Open the output file
            if (answer)
            {
                answer = CreateOutputFile();
            }

            // Do the work here
            string StatementNote = string.Empty;

            if (answer)
            {
                // Find the number of clients for the marquee
                ExpectedClientCount = global::DebtPlus.Utils.Nulls.DInt(ds.Tables["client_statement_clients"].Compute("count(client)", string.Empty));

                // Identify the batch numbers
                Int32 Batch_0 = ap.GetBatch(0);
                Int32 Batch_1 = ap.GetBatch(1);
                Int32 Batch_2 = ap.GetBatch(2);

                // Find the period starting and ending dates from the batches
                DateTime PeriodStart = Convert.ToDateTime(ds.Tables["client_statement_batches"].Rows.Find(Batch_0)["period_start"]);
                DateTime PeriodEnd = Convert.ToDateTime(ds.Tables["client_statement_batches"].Rows.Find(Batch_2)["period_end"]);
                DateTime CurrentTime = Convert.ToDateTime(ds.Tables["client_statement_batches"].Rows.Find(Batch_2)["statement_date"]);

                // Generate the standard file header
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;

                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();

                    TotalDebtCount = 0;
                    TotalClientCount = 0;

                    // Write the header for the file
                    if (ap.FileHeader != string.Empty)
                    {
                        DataTable tbl = ds.Tables["client_statement_batches"];
                        answer = WriteFileHeader(tbl.Rows.Find(Batch_0), tbl.Rows.Find(Batch_1), tbl.Rows.Find(Batch_2));
                    }

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with5 = cmd;
                        _with5.Connection = cn;
                        _with5.CommandText = "SELECT creditor_name, formatted_account_number, original_balance, net_debits, credits, interest_rate, payments_to_date, current_balance, client, client_statement_batch, creditor, client_creditor FROM view_client_statement_details WITH (NOLOCK) WHERE client_statement_batch IN (@batch_0, @batch_1, @batch_2) AND client > 0 ORDER BY client, client_creditor, client_statement_batch";
                        _with5.CommandType = CommandType.Text;
                        _with5.Parameters.Add("@batch_0", SqlDbType.Int).Value = Batch_0;
                        _with5.Parameters.Add("@batch_1", SqlDbType.Int).Value = Batch_1;
                        _with5.Parameters.Add("@batch_2", SqlDbType.Int).Value = Batch_2;
                        StatementDetailsDataReader = _with5.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);
                    }

                    if (StatementDetailsDataReader != null && StatementDetailsDataReader.Read())
                    {
                        do
                        {
                            TotalClientCount += 1;
                            RaiseStatistics(Convert.ToInt32(StatementDetailsDataReader["client"]), ExpectedClientCount, TotalClientCount, TotalClientCount * 3, TotalLineCount);
                            if (!ProcessNextStatement(PeriodStart, PeriodEnd, Batch_0, Batch_1, Batch_2))
                                break;
                        } while (true);
                    }

                    RaiseStatistics(0, TotalClientCount, TotalClientCount, TotalLineCount);
                    if (ap.FileTrailer != string.Empty)
                    {
                        WriteFileTrailer(CurrentTime, TotalLineCount, TotalClientCount, TotalDebtCount);
                    }
                    CloseOutputFile();
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing client statements");
                }
                finally
                {
                    if (StatementDetailsDataReader != null)
                        StatementDetailsDataReader.Dispose();
                    if (cn != null)
                        cn.Dispose();
                    Cursor.Current = current_cursor;
                }
            }

            return answer;
        }
    }
}