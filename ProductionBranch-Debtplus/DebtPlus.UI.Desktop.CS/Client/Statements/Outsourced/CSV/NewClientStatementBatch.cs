using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Client.Widgets.Controls;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    public partial class NewClientStatementBatch : ClientStatementBatch
    {
        public NewClientStatementBatch() : base()
        {
            InitializeComponent();
            Button_New.Click += Button_New_Click;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;
        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.

        internal DevExpress.XtraEditors.SimpleButton Button_New;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Button_New = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemMemoExEdit1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemMemoEdit1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.ds).BeginInit();
            this.SuspendLayout();
            //
            //GridColumn_period_start
            //
            this.GridColumn_period_start.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_period_start.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_start.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_period_start.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_start.DisplayFormat.FormatString = "d";
            this.GridColumn_period_start.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_start.GroupFormat.FormatString = "d";
            this.GridColumn_period_start.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_start.OptionsColumn.AllowEdit = false;
            //
            //GridColumn_client_statement_batch
            //
            this.GridColumn_client_statement_batch.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client_statement_batch.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_statement_batch.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client_statement_batch.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client_statement_batch.DisplayFormat.FormatString = "f0";
            this.GridColumn_client_statement_batch.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_statement_batch.GroupFormat.FormatString = "f0";
            this.GridColumn_client_statement_batch.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_client_statement_batch.OptionsColumn.AllowEdit = false;
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.OptionsSelection.MultiSelect = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //GridColumn_period_end
            //
            this.GridColumn_period_end.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_period_end.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_end.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_period_end.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_period_end.DisplayFormat.FormatString = "d";
            this.GridColumn_period_end.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_end.GroupFormat.FormatString = "d";
            this.GridColumn_period_end.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_period_end.OptionsColumn.AllowEdit = false;
            //
            //GridControl1
            //
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Name = "GridControl1";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Name = "Button_Cancel";
            //
            //Button_OK
            //
            this.Button_OK.Name = "Button_OK";
            //
            //GridColumn_disbursement_date
            //
            this.GridColumn_disbursement_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_disbursement_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_disbursement_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_disbursement_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_disbursement_date.DisplayFormat.FormatString = "f0";
            this.GridColumn_disbursement_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_disbursement_date.GroupFormat.FormatString = "f0";
            this.GridColumn_disbursement_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_disbursement_date.OptionsColumn.AllowEdit = false;
            //
            //Button_New
            //
            this.Button_New.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_New.Location = new System.Drawing.Point(416, 96);
            this.Button_New.Name = "Button_New";
            this.Button_New.TabIndex = 3;
            this.Button_New.Text = "&New...";
            //
            //NewClientStatementBatch
            //
            this.Controls.Add(this.Button_New);
            this.Name = "NewClientStatementBatch";
            this.Controls.SetChildIndex(this.Button_New, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            this.Controls.SetChildIndex(this.Button_OK, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemMemoExEdit1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.RepositoryItemMemoEdit1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.ds).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        public override void Reload()
        {
            base.Reload();
            this.Button_New.Visible = !NotesOnly;
        }

        protected virtual void OnNew(EventArgs e)
        {
            DataTable tbl = ds.Tables["client_statement_batches"];
            DataView vue = tbl.DefaultView;
            DataRowView drv = vue.AddNew();

            // Create the new batch
            using (NewClientStatementBatchForm frm = new NewClientStatementBatchForm(drv))
            {
                var _with1 = frm;
                if (_with1.ShowDialog() == DialogResult.OK && SaveChanges(ref drv))
                {
                    drv.EndEdit();

                    // Refresh the grid with the new changes so that there is no "new" row pointer
                    GridControl1.RefreshDataSource();

                    // Select the current row
                    Int32 client_statement_batch = Convert.ToInt32(drv["client_statement_batch"]);
                    Int32 RowHandle = GridView1.LocateByValue(0, GridColumn_client_statement_batch, client_statement_batch);

                    // If we can find the row then try to select that row with the OK button
                    if (RowHandle >= 0)
                    {
                        if (BatchCount == 1)
                            GridView1.ClearSelection();
                        GridView1.SelectRow(RowHandle);
                        if (GridView1.SelectedRowsCount == BatchCount)
                            RaiseSelected(new EventArgs());
                    }
                }
                else
                {
                    // The form was Canceled. Remove the added row.
                    drv.CancelEdit();
                }
            }
        }

        protected void RaiseNew()
        {
            OnNew(EventArgs.Empty);
        }

        private void Button_New_Click(object sender, EventArgs e)
        {
            RaiseNew();
        }

        private bool SaveChanges(ref DataRowView drv)
        {
            bool answer = false;

            // Insert any rows that need to be inserted
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();

                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = cn;
                    _with2.CommandText = "xpr_create_client_statement_batch";
                    _with2.CommandType = CommandType.StoredProcedure;
                    _with2.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    _with2.Parameters.Add("@disbursement_date", SqlDbType.Int).Value = drv["disbursement_date"];
                    _with2.Parameters.Add("@note", SqlDbType.VarChar, 1024).Value = drv["note"];
                    _with2.Parameters.Add("@period_start", SqlDbType.DateTime).Value = drv["period_start"];
                    _with2.Parameters.Add("@period_end", SqlDbType.DateTime).Value = drv["period_end"];
                    _with2.ExecuteNonQuery();

                    Int32 client_statement_batch = global::DebtPlus.Utils.Nulls.DInt(_with2.Parameters[0].Value);
                    drv["client_statement_batch"] = client_statement_batch;
                    if (client_statement_batch > 0)
                        answer = true;
                }
            }
            catch (SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating client statement batch");
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
                Cursor.Current = current_cursor;
            }

            // Process any changes to the notes
            if (answer)
                base.SaveChanges();
            return answer;
        }
    }
}