using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    static internal partial class Globals
    {
        public static object EmptyString;
    }

    internal abstract class Statement : object
    {
        protected Statement() : base()
        {
        }

        protected StreamWriter OutputStream;

        protected Int32 TotalLineCount;
        // Dataset for the client information

        protected DataSet ds = new DataSet("ds");

        /// <summary>
        /// Linkage to the statitics routine
        /// </summary>
        public delegate void StatisticsHandler(object Sender, StatisticsArgs e);

        public event StatisticsHandler Statistics;

        protected virtual void OnStatistics(StatisticsArgs e)
        {
            if (Statistics != null)
            {
                Statistics(this, e);
            }
        }

        protected void RaiseStatistics(Int32 Client, Int32 TotalClientCount, Int32 CurrentClientCount, Int32 LinesWritten)
        {
            OnStatistics(new StatisticsArgs(Client, TotalClientCount, CurrentClientCount, LinesWritten));
        }

        protected void RaiseStatistics(Int32 Client, Int32 TotalClientCount, Int32 CurrentClientCount, Int32 ProcessedClientCount, Int32 LinesWritten)
        {
            OnStatistics(new StatisticsArgs(Client, TotalClientCount, CurrentClientCount, ProcessedClientCount, LinesWritten));
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>

        protected static OutsourceArgParser ap;

        public Statement(OutsourceArgParser InputParser) : base()
        {
            ap = InputParser;
        }

        /// <summary>
        /// Create the output file
        /// </summary>
        protected virtual bool CreateOutputFile()
        {
            bool answer = true;

            // If there is no name then ask for a name
            string OutputFileName = ap.OutputFileName;

            if (OutputFileName == string.Empty)
            {
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    var _with1 = dlg;
                    _with1.AddExtension = true;
                    _with1.CheckFileExists = false;
                    _with1.CheckPathExists = true;
                    _with1.DefaultExt = "txt";
                    _with1.DereferenceLinks = true;
                    _with1.Filter = "Text Files (*.txt)|(*.txt)|All Files (*.*)|*.*";
                    _with1.FilterIndex = 0;
                    _with1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    _with1.OverwritePrompt = true;
                    _with1.RestoreDirectory = true;
                    _with1.Title = "Output File Name";
                    _with1.ValidateNames = true;

                    answer = (_with1.ShowDialog() == DialogResult.OK);
                    if (answer)
                        OutputFileName = _with1.FileName;
                }
            }

            // If successful then attempt to open the file
            if (answer)
            {
                try
                {
                    answer = false;
                    OutputStream = new StreamWriter(OutputFileName, false, Encoding.ASCII, 4096);
                    answer = true;
                }
#pragma warning disable 168
                catch (DirectoryNotFoundException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The output file's directory can not be found", "Error Opening Output File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                catch (PathTooLongException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The output file's path is too long. Shorten the name or the path.", "Error Opening Output File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
#pragma warning restore 168
            }

            return answer;
        }

        /// <summary>
        /// Write the current batch of statements
        /// </summary>
        public virtual bool WriteStatements()
        {
            Globals.EmptyString = StringIfy(string.Empty);
            return true;
        }

        /// <summary>
        /// Write the text line to the output file
        /// </summary>
        protected void WriteTextLine(string TextLine)
        {
            OutputStream.WriteLine(TextLine);
            TotalLineCount += 1;
        }

        /// <summary>
        /// Close the output stream as needed
        /// </summary>
        protected virtual bool CloseOutputFile()
        {
            // Close the file stream
            if (OutputStream != null)
            {
                OutputStream.Close();
                OutputStream = null;
            }

            // Return success to the caller
            return true;
        }

        /// <summary>
        /// Allow for generic object to be converted
        /// </summary>
        protected static object StringIfy(object Item)
        {
            string answer = string.Empty;
            if (Item == null || object.ReferenceEquals(Item, DBNull.Value))
                return Globals.EmptyString;
            if (Item is DateTime)
                return StringIfy(Convert.ToDateTime(Item));
            if (Item is string)
                return StringIfy(Convert.ToString(Item));
            if (Item is double)
                return StringIfy(Convert.ToDouble(Item));
            if (Item is decimal)
                return StringIfy(Convert.ToDecimal(Item));
            return StringIfy(Convert.ToInt64(Item));
        }

        /// <summary>
        /// Convert decimals to properly delimited output values
        /// </summary>
        protected static object StringIfy(decimal InputDecimal)
        {
            return StringIfy(InputDecimal, "{0:f2}");
        }

        protected static object StringIfy(decimal InputDecimal, string FormatString)
        {
            if (ap.QuoteLevel == OutsourceArgParser.QuoteLevelSetting.All)
            {
                return StringIfy(string.Format(FormatString, InputDecimal));
            }
            else
            {
                return InputDecimal;
            }
        }

        /// <summary>
        /// Convert doubles to properly delimited output values
        /// </summary>
        protected static object StringIfy(double InputDouble)
        {
            return StringIfy(InputDouble, "{0:f2}");
        }

        protected static object StringIfy(double InputDouble, string FormatString)
        {
            if (ap.QuoteLevel == OutsourceArgParser.QuoteLevelSetting.All)
            {
                return StringIfy(string.Format(FormatString, InputDouble));
            }
            else
            {
                return InputDouble;
            }
        }

        /// <summary>
        /// Convert longs to properly delimited output values
        /// </summary>
        protected static object StringIfy(long InputLong)
        {
            return StringIfy(InputLong, "{0:f0}");
        }

        protected static object StringIfy(long InputLong, string FormatString)
        {
            if (ap.QuoteLevel == OutsourceArgParser.QuoteLevelSetting.All)
            {
                return StringIfy(string.Format(FormatString, InputLong));
            }
            else
            {
                return InputLong;
            }
        }

        /// <summary>
        /// Convert dates to properly delimited output values
        /// </summary>
        protected static object StringIfy(System.DateTime InputDate)
        {
            return StringIfy(InputDate, "{0:d}");
        }

        protected static object StringIfy(System.DateTime InputDate, string FormatString)
        {
            if (ap.QuoteLevel == OutsourceArgParser.QuoteLevelSetting.All)
            {
                return StringIfy(string.Format(FormatString, InputDate));
            }
            else
            {
                return InputDate;
            }
        }

        private static readonly StringBuilder sb = new StringBuilder();

        /// <summary>
        /// Convert strings to properly delimited output values
        /// </summary>
        protected static object Stringify(string InputString)
        {
            // If there is no string then return an empty item
            if (InputString == null)
                InputString = string.Empty;
            if (sb.Length > 0)
                sb.Remove(0, sb.Length);
            sb.Append(InputString);

            // If quoting is none then do not do anything to the string. User beware!!

            if (ap.QuoteLevel != OutsourceArgParser.QuoteLevelSetting.None)
            {
                // If not automatic then the string is quoted.
                bool NeedQuotes = false;
                if (ap.QuoteLevel != OutsourceArgParser.QuoteLevelSetting.Auto)
                {
                    NeedQuotes = true;
                }
                else
                {
                    // Automatic items look for the need to quote the string.
                    if (InputString.IndexOf(ap.QuoteChar) >= 0)
                        NeedQuotes = true;
                    if (InputString.IndexOf(ap.SepChar) >= 0)
                        NeedQuotes = true;

                    // Leading or trailing spaces also require quotes
                    if (InputString.StartsWith(" ") || InputString.EndsWith(" "))
                        NeedQuotes = true;
                }

                if (NeedQuotes)
                {
                    sb.Replace(ap.QuoteChar, ap.QuoteChar + ap.QuoteChar, 0, sb.Length);
                    sb.Insert(0, ap.QuoteChar);
                    sb.Append(ap.QuoteChar);
                }
            }

            // Return the input string
            return sb.ToString();
        }

        /// <summary>
        /// Translate the input decimal to a value with an assumed radix
        /// </summary>
        protected static Int64 toCents(decimal InputDec)
        {
            // Convert the value to an Int32 number of cents
            return Convert.ToInt64(InputDec * 100m);
        }
    }
}