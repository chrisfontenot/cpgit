using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class QuarterlyStatement_Myriad : QuarterlyStatement
    {
        public QuarterlyStatement_Myriad(OutsourceArgParser ap) : base(ap)
        {
        }

        /// <summary>
        /// Write the Myraid file trailer
        /// </summary>
        protected override bool WriteFileTrailer(DateTime CurrentTime, Int32 LineCount, Int32 ClientCount, Int32 DebtCount)
        {
            if (ap.FileTrailer != string.Empty)
            {
                object[] BufferArea = new object[4];
                BufferArea[0] = StringIfy(CurrentTime);
                BufferArea[1] = StringIfy(LineCount);
                BufferArea[2] = StringIfy(ClientCount);
                BufferArea[3] = StringIfy(DebtCount);

                WriteTextLine(string.Format(ap.FileTrailer, BufferArea));
            }

            return true;
        }

        /// <summary>
        /// Write the Myraid file header
        /// </summary>
        protected override bool WriteFileHeader(DataRow Batch1, DataRow Batch2, DataRow Batch3)
        {
            // If there is a header record then generate the file header record
            if (ap.FileHeader != string.Empty)
            {
                System.DateTime period_start = System.DateTime.MinValue;
                System.DateTime period_end = System.DateTime.MaxValue;
                string StatementNote = string.Empty;

                object[] BufferArea = new object[16];

                // Load the information for the period information
                BufferArea[0] = StringIfy(Batch1["period_start"]);
                BufferArea[1] = StringIfy(Batch1["period_end"]);
                BufferArea[2] = StringIfy(Batch1["statement_date"]);
                BufferArea[3] = StringIfy(Batch1["note"]);

                BufferArea[4] = StringIfy(Batch2["period_start"]);
                BufferArea[5] = StringIfy(Batch2["period_end"]);
                BufferArea[6] = StringIfy(Batch2["statement_date"]);
                BufferArea[7] = StringIfy(Batch2["note"]);

                BufferArea[8] = StringIfy(Batch3["period_start"]);
                BufferArea[9] = StringIfy(Batch3["period_end"]);
                BufferArea[10] = StringIfy(Batch3["statement_date"]);
                BufferArea[11] = StringIfy(Batch3["note"]);

                // Take the last note and split it into 80 byte chunks
                BufferArea[12] = System.String.Empty;
                BufferArea[13] = System.String.Empty;
                BufferArea[14] = System.String.Empty;
                BufferArea[15] = System.String.Empty;

                Int32 idx = 12;
                string NoteText = global::DebtPlus.Utils.Nulls.DStr(Batch3["note"]);
                while (NoteText.Length > 80 && idx < 15)
                {
                    Int32 pos = NoteText.LastIndexOf(' ', 80);
                    if (pos < 0)
                        pos = 80;
                    BufferArea[idx] = StringIfy(NoteText.Substring(0, pos).Trim());
                    NoteText = NoteText.Substring(pos).Trim();
                    idx += 1;
                }
                BufferArea[idx] = StringIfy(NoteText);

                // Write the corresponding string to the output file
                string TextLine = string.Format(ap.FileHeader, BufferArea);
                WriteTextLine(TextLine);
            }

            return true;
        }

        protected class ClientHeader : IDisposable
        {
            public Int32 Batch = Int32.MinValue;
            public object CounselorName = System.String.Empty;
            public object ExpectedDepositDate = System.String.Empty;
            public object ExpectedDepositAmt = System.String.Empty;
            public object HeldInTrust = System.String.Empty;
            public object StartingTrustBalance = System.String.Empty;
            public object DepositAmt = System.String.Empty;
            public object RefundAmt = System.String.Empty;

            public ClientHeader()
            {
            }

            private DataRow row = null;

            public ClientHeader(DataRow row) : this()
            {
                this.row = row;
                if (row != null)
                {
                    if (!object.ReferenceEquals(row["client_statement_batch"], DBNull.Value))
                        Batch = Convert.ToInt32(row["client_statement_batch"]);

                    ExpectedDepositAmt = StringIfy(toCents(global::DebtPlus.Utils.Nulls.DDec(row["expected_deposit_amt"])));
                    ExpectedDepositDate = StringIfy(toCents(global::DebtPlus.Utils.Nulls.DDec(row["expected_deposit_date"])));
                    CounselorName = StringIfy(row["counselor_name"]);
                    HeldInTrust = StringIfy(toCents(global::DebtPlus.Utils.Nulls.DDec(row["held_in_trust"])));
                    StartingTrustBalance = StringIfy(toCents(global::DebtPlus.Utils.Nulls.DDec(row["starting_trust_balance"])));
                    RefundAmt = StringIfy(toCents(global::DebtPlus.Utils.Nulls.DDec(row["refund_amt"])));
                    DepositAmt = StringIfy(toCents(global::DebtPlus.Utils.Nulls.DDec(row["deposit_amt"])));
                }
            }

            public void SetAllValues(ref object[] Output, Int32 idx)
            {
                Output[idx] = DepositAmt;
                Output[idx + 1] = RefundAmt;
            }

            public void SetHeaderValues(ref object[] output)
            {
                output[3] = CounselorName;
                output[4] = ExpectedDepositDate;
                output[5] = ExpectedDepositAmt;
                output[12] = StartingTrustBalance;
                output[13] = HeldInTrust;
                output[14] = System.String.Empty;
                // Marker for exception message. Not used here.

                // Inlcude the client address
                output[15] = System.String.Empty;
                output[16] = System.String.Empty;
                output[17] = System.String.Empty;
                output[18] = System.String.Empty;
                output[19] = System.String.Empty;

                Int32 idx = 15;
                if (row != null)
                {
                    string txt = global::DebtPlus.Utils.Nulls.DStr(row["address_1"]).Trim();
                    if (txt != string.Empty)
                    {
                        output[idx] = StringIfy(txt);
                        idx += 1;
                    }

                    txt = global::DebtPlus.Utils.Nulls.DStr(row["address_2"]).Trim();
                    if (txt != string.Empty)
                    {
                        output[idx] = StringIfy(txt);
                        idx += 1;
                    }

                    txt = global::DebtPlus.Utils.Nulls.DStr(row["address_3"]).Trim();
                    if (txt != string.Empty)
                    {
                        output[idx] = StringIfy(txt);
                        idx += 1;
                    }

                    txt = global::DebtPlus.Utils.Nulls.DStr(row["address_4"]).Trim();
                    if (txt != string.Empty)
                    {
                        output[idx] = StringIfy(txt);
                        idx += 1;
                    }
                }
            }

            #region "IDisposable Support"

            private bool disposedValue;

            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposedValue)
                {
                    if (disposing)
                    {
                    }
                    row = null;
                }
                this.disposedValue = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion "IDisposable Support"
        }

        protected class DebtHeader : IDisposable
        {
            public DebtHeader()
            {
            }

            public decimal Debits = 0m;
            public decimal Credits = 0m;
            public decimal OriginalBalance = 0m;
            public decimal PaymentsToDate = 0m;
            public decimal CurrentBalance = 0m;
            public Int32 ClientCreditor = 0;
            public string CreditorName = string.Empty;
            public string AccountNumber = string.Empty;

            public double InterestRate = 0.0;

            public DebtHeader(IDataReader rdr)
            {
                for (Int32 FieldNo = 0; FieldNo <= rdr.FieldCount - 1; FieldNo++)
                {
                    if (!rdr.IsDBNull(FieldNo))
                    {
                        switch (rdr.GetName(FieldNo).ToLower())
                        {
                            case "net_debits":
                                Debits = rdr.GetDecimal(FieldNo);
                                break;

                            case "credits":
                                Credits = rdr.GetDecimal(FieldNo);
                                break;

                            case "original_balance":
                                OriginalBalance = rdr.GetDecimal(FieldNo);
                                break;

                            case "payments_to_date":
                                PaymentsToDate = rdr.GetDecimal(FieldNo);
                                break;

                            case "current_balance":
                                CurrentBalance = rdr.GetDecimal(FieldNo);
                                break;

                            case "client_creditor":
                                ClientCreditor = Convert.ToInt32(rdr.GetValue(FieldNo));
                                break;

                            case "creditor_name":
                                CreditorName = rdr.GetString(FieldNo);
                                break;

                            case "formatted_account_number":
                                AccountNumber = rdr.GetString(FieldNo);
                                break;

                            case "interest_rate":
                                InterestRate = Convert.ToDouble(rdr.GetValue(FieldNo));
                                break;

                            default:
                                break;
                                // skip other items
                        }
                    }
                }
            }

            #region "IDisposable Support"

            private bool disposedValue;

            // To detect redundant calls
            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposedValue)
                {
                    if (disposing)
                    {
                    }
                }
                this.disposedValue = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion "IDisposable Support"

            public virtual void SetHeaderFields(ref object[] Output)
            {
                Output[0] = StringIfy(CreditorName);
                Output[1] = StringIfy(AccountNumber);
                Output[2] = StringIfy(toCents(OriginalBalance));
                Output[3] = StringIfy(toCents(PaymentsToDate));
                Output[4] = StringIfy(toCents(CurrentBalance));
                Output[5] = StringIfy(InterestRate, "{0:p}");
                Output[6] = InterestRate != 0.0 ? "*" : string.Empty;
            }

            public virtual void SetItemFields(ref object[] Output, Int32 idx)
            {
                Output[idx] = StringIfy(toCents(Debits));
                Output[idx + 1] = StringIfy(toCents(Credits));
                Output[idx + 2] = Credits != 0m ? "#" : string.Empty;
            }
        }

        protected class DebtTotals : DebtHeader
        {
            public DebtTotals()
            {
            }

            public void AddTotals(DebtHeader @ref)
            {
                Credits += @ref.Credits;
                Debits += @ref.Debits;

                OriginalBalance += @ref.OriginalBalance;
                PaymentsToDate += @ref.PaymentsToDate;
                CurrentBalance += @ref.CurrentBalance;
            }

            public override void SetHeaderFields(ref object[] Output)
            {
                Output[1] = StringIfy(toCents(OriginalBalance));
                Output[2] = StringIfy(toCents(PaymentsToDate));
                Output[3] = StringIfy(toCents(CurrentBalance));
            }

            public override void SetItemFields(ref object[] Output, Int32 idx)
            {
                Output[idx] = StringIfy(toCents(Debits));
            }
        }

        protected void WriteCombinedDebts(ref DebtTotals Totals1, ref DebtTotals Totals2, ref DebtTotals Totals3, ref DebtHeader Debt1, ref DebtHeader Debt2, ref DebtHeader Debt3)
        {
            object[] BufferArea = new object[21];

            if (Debt3 != null)
            {
                Debt3.SetHeaderFields(ref BufferArea);
                Debt3.SetItemFields(ref BufferArea, 13);
                Totals3.AddTotals(Debt3);
                Debt3.Dispose();
                Debt3 = null;
            }

            if (Debt2 != null)
            {
                Debt2.SetHeaderFields(ref BufferArea);
                Debt2.SetItemFields(ref BufferArea, 10);
                Totals2.AddTotals(Debt2);
                Debt2.Dispose();
                Debt2 = null;
            }

            if (Debt1 != null)
            {
                Debt1.SetHeaderFields(ref BufferArea);
                Debt1.SetItemFields(ref BufferArea, 7);
                Totals1.AddTotals(Debt1);
                Debt1.Dispose();
                Debt1 = null;
            }

            // Write the detail line if possible
            if (ap.DebtDetail != string.Empty)
            {
                string txt = string.Format(ap.DebtDetail, BufferArea);
                WriteTextLine(txt);
            }
        }

        /// <summary>
        /// Write the Myraid detail record for the client
        /// </summary>
        protected override bool ProcessNextStatement(DateTime PeriodStart, DateTime PeriodEnd, Int32 Batch_0, Int32 Batch_1, Int32 Batch_2)
        {
            bool answer = base.ProcessNextStatement(PeriodStart, PeriodEnd, Batch_0, Batch_1, Batch_2);
            Int32 Client = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client"));

            // Information from the header record
            if (ap.DebtHeader != string.Empty)
            {
                object[] BufferArea = new object[21];

                // Information about the current client
                BufferArea[0] = StringIfy(PeriodStart);
                BufferArea[1] = StringIfy(PeriodEnd);
                BufferArea[2] = StringIfy(string.Format("{0:0000000}", Client));

                // Process the header information
                DataTable tbl = ds.Tables["client_statement_clients"];
                using (ClientHeader hdr = new ClientHeader(tbl.Rows.Find(new object[] { Client, Batch_2 })))
                {
                    hdr.SetHeaderValues(ref BufferArea);
                    hdr.SetAllValues(ref BufferArea, 10);
                }

                using (ClientHeader hdr = new ClientHeader(tbl.Rows.Find(new object[] { Client, Batch_1 })))
                {
                    hdr.SetHeaderValues(ref BufferArea);
                    hdr.SetAllValues(ref BufferArea, 8);
                }

                using (ClientHeader hdr = new ClientHeader(tbl.Rows.Find(new object[] { Client, Batch_0 })))
                {
                    hdr.SetHeaderValues(ref BufferArea);
                    hdr.SetAllValues(ref BufferArea, 6);
                }

                // Write the debt header record (it is also the client information)
                string TextLine = string.Format(ap.DebtHeader, BufferArea);
                WriteTextLine(TextLine);
            }

            // Initialize our counters for the trailer record
            DebtTotals Totals1 = new DebtTotals();
            DebtTotals Totals2 = new DebtTotals();
            DebtTotals Totals3 = new DebtTotals();

            try
            {
                DebtHeader Period1 = null;
                DebtHeader Period2 = null;
                DebtHeader Period3 = null;

                Int32 DebtsWritten = 0;
                Int32 LastClientCreditor = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client_creditor"));

                // Debts until we are on a different client

                while (Client == StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client")))
                {
                    // When the debt changes, write the records
                    Int32 ClientCreditor = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client_creditor"));
                    if (LastClientCreditor != ClientCreditor)
                    {
                        WriteCombinedDebts(ref Totals1, ref Totals2, ref Totals3, ref Period1, ref Period2, ref Period3);
                        DebtsWritten += 1;
                        LastClientCreditor = ClientCreditor;
                    }

                    // Put the current line into the apporpriate location for the debt collection
                    Int32 ClientStatementBatch = StatementDetailsDataReader.GetInt32(StatementDetailsDataReader.GetOrdinal("client_statement_batch"));
                    if (ClientStatementBatch == Batch_0)
                    {
                        Period1 = new DebtHeader(StatementDetailsDataReader);
                    }
                    else if (ClientStatementBatch == Batch_1)
                    {
                        Period2 = new DebtHeader(StatementDetailsDataReader);
                    }
                    else
                    {
                        Period3 = new DebtHeader(StatementDetailsDataReader);
                    }

                    // Process the next record
                    if (!StatementDetailsDataReader.Read())
                    {
                        answer = false;
                        break;
                    }
                }

                // If there is something to write then write the last debt to the list
                if (Period1 != null || Period2 != null || Period3 != null)
                {
                    WriteCombinedDebts(ref Totals1, ref Totals2, ref Totals3, ref Period1, ref Period2, ref Period3);
                    DebtsWritten += 1;
                }

                // Write the debt trailer
                if (ap.DebtTrailer != string.Empty)
                {
                    object[] BufferArea = new object[7];

                    BufferArea[0] = StringIfy(DebtsWritten);
                    Totals1.SetHeaderFields(ref BufferArea);
                    Totals1.SetItemFields(ref BufferArea, 4);
                    Totals2.SetItemFields(ref BufferArea, 5);
                    Totals3.SetItemFields(ref BufferArea, 6);

                    string TextLine = string.Format(ap.DebtTrailer, BufferArea);
                    WriteTextLine(TextLine);
                }

                // Count the debts into the total field
                base.TotalDebtCount += DebtsWritten;
            }
            finally
            {
                // Discard the client totals
                Totals1.Dispose();
                Totals2.Dispose();
                Totals3.Dispose();
            }

            return answer;
        }
    }
}