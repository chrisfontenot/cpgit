using System;

using System.Collections.Specialized;
using System.Globalization;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Statements.Outsourced.CSV
{
    internal partial class OutsourceArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// How much quoting must be performed on the output text
        /// </summary>
        public enum QuoteLevelSetting
        {
            None = 0,
            Auto,
            Strings,
            All
        }

        private QuoteLevelSetting privateQuoteLevel = QuoteLevelSetting.Auto;

        public QuoteLevelSetting QuoteLevel
        {
            get { return privateQuoteLevel; }
        }

        /// <summary>
        /// Find client information to process
        /// </summary>

        private string privateSelectStatement;

        public string SelectStatement
        {
            get { return privateSelectStatement; }
        }

        /// <summary>
        /// Output formatting for the file header
        /// </summary>

        private string privateFileHeader;

        public string FileHeader
        {
            get { return privateFileHeader; }
        }

        /// <summary>
        /// Update the batch notes only
        /// </summary>

        private bool privateNotesOnly = false;

        public bool NotesOnly
        {
            get { return privateNotesOnly; }
        }

        /// <summary>
        /// Output formatting for the Debt header
        /// </summary>

        private string privateDebtHeader;

        public string DebtHeader
        {
            get { return privateDebtHeader; }
        }

        /// <summary>
        /// Output formatting for the Debt detail
        /// </summary>

        private string privateDebtDetail;

        public string DebtDetail
        {
            get { return privateDebtDetail; }
        }

        /// <summary>
        /// Output formatting for the Debt trailer
        /// </summary>

        private string privateDebtTrailer;

        public string DebtTrailer
        {
            get { return privateDebtTrailer; }
        }

        /// <summary>
        /// Output formatting for the file trailer
        /// </summary>

        private string privateFileTrailer;

        public string FileTrailer
        {
            get { return privateFileTrailer; }
        }

        /// <summary>
        /// String quoting character
        /// </summary>

        private string privateQuoteChar;

        public string QuoteChar
        {
            get { return privateQuoteChar; }
        }

        /// <summary>
        /// Field seperator character
        /// </summary>

        private string privateSepChar;

        public string SepChar
        {
            get { return privateSepChar; }
        }

        /// <summary>
        /// Batch ID to process
        /// </summary>
        private Int32[] privateBatch = new Int32[] {
            -1,
            -1,
            -1
        };

        public Int32 GetBatch()
        {
            return GetBatch(0);
        }

        public Int32 GetBatch(Int32 Index)
        {
            return privateBatch[Index];
        }

        public void SetBatch(Int32 value)
        {
            SetBatch(0, value);
        }

        public void SetBatch(Int32 Index, Int32 value)
        {
            privateBatch[Index] = value;
        }

        /// <summary>
        /// Flag for monthly statements
        /// </summary>

        private bool flagMonthly = false;

        public bool Monthly
        {
            get { return flagMonthly; }
        }

        /// <summary>
        /// Current output file name
        /// </summary>

        private string OutputName = string.Empty;

        public string OutputFileName
        {
            get { return OutputName; }
        }

        /// <summary>
        /// Create a new instance of our class. Set the option flags.
        /// </summary>
        public OutsourceArgParser() : base(new string[] {
            "m",
            "b",
            "q",
            "n"
        })
        {
            // Configuration settings
            NameValueCollection config = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ClientStatementsOutsourcedCsv);

            // Find the file header, trailer, debt header, detail, and trailer records
            privateFileHeader = config["FileHeader"];
            if (privateFileHeader == null)
                privateFileHeader = string.Empty;
            privateFileHeader = Mainline.UnEscape(privateFileHeader);

            privateDebtHeader = config["DebtHeader"];
            if (privateDebtHeader == null)
                privateDebtHeader = string.Empty;
            privateDebtHeader = Mainline.UnEscape(privateDebtHeader);

            privateDebtDetail = config["DebtDetail"];
            if (privateDebtDetail == null)
                privateDebtDetail = string.Empty;
            privateDebtDetail = Mainline.UnEscape(privateDebtDetail);

            privateDebtTrailer = config["DebtTrailer"];
            if (privateDebtTrailer == null)
                privateDebtTrailer = string.Empty;
            privateDebtTrailer = Mainline.UnEscape(privateDebtTrailer);

            privateFileTrailer = config["FileTrailer"];
            if (privateFileTrailer == null)
                privateFileTrailer = string.Empty;
            privateFileTrailer = Mainline.UnEscape(privateFileTrailer);

            privateSelectStatement = config["SelectStatement"];
            if (privateSelectStatement == null)
                privateSelectStatement = string.Empty;
            privateSelectStatement = Mainline.UnEscape(privateSelectStatement).Trim();
            if (privateSelectStatement == string.Empty)
            {
                privateSelectStatement = "SELECT * FROM view_client_statement_clients WHERE client_statement_batch=@client_statement_batch";
            }

            // Find the formatting characters
            privateQuoteChar = config["QuoteChar"];
            if (privateQuoteChar == null)
                privateQuoteChar = string.Empty;
            privateQuoteChar = Mainline.UnEscape(privateQuoteChar);

            privateSepChar = config["SepChar"];
            if (privateSepChar == null)
                privateSepChar = string.Empty;
            privateSepChar = Mainline.UnEscape(privateSepChar);

            string TempLevel = config["QuotedFieldLevel"];
            if (TempLevel != null)
            {
                switch (TempLevel.ToLower())
                {
                    case "auto":
                        privateQuoteLevel = QuoteLevelSetting.Auto;
                        break;

                    case "strings":
                        privateQuoteLevel = QuoteLevelSetting.Strings;
                        break;

                    case "all":
                        privateQuoteLevel = QuoteLevelSetting.All;
                        break;

                    default:
                        privateQuoteLevel = QuoteLevelSetting.None;
                        break;
                }
            }

            // Find the processing mode from the config file
            string Mode = config["Mode"];
            if (Mode != null)
            {
                switch (Mode.ToLower())
                {
                    case "quarterly":
                        flagMonthly = false;
                        break;

                    default:
                        flagMonthly = true;
                        break;
                }
            }

            // If there is no quote or no seperator character then we can't quote fields
            if (QuoteChar == string.Empty || SepChar == string.Empty)
            {
                if (QuoteLevel != QuoteLevelSetting.None)
                {
                    privateQuoteLevel = QuoteLevelSetting.None;
                    DebtPlus.Data.Forms.MessageBox.Show("A seperator and quote character are required for quoted fields. The quoting is disabled.", "Configuration Error", MessageBoxButtons.OK);
                }
            }
        }

        /// <summary>
        /// Handle the parameter items
        /// </summary>

        private Int32 BatchCount = 0;

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "m":
                    switch (switchValue)
                    {
                        case "0":
                            flagMonthly = false;
                            break;

                        case "1":
                            flagMonthly = true;
                            break;

                        default:
                            ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                            break;
                    }
                    break;

                case "n":
                    privateNotesOnly = true;
                    break;

                case "q":
                    switch (switchValue.ToLower())
                    {
                        case "none":
                            privateQuoteLevel = QuoteLevelSetting.None;
                            break;

                        case "auto":
                            privateQuoteLevel = QuoteLevelSetting.Auto;
                            break;

                        case "strings":
                            privateQuoteLevel = QuoteLevelSetting.Strings;
                            break;

                        case "all":
                            privateQuoteLevel = QuoteLevelSetting.All;
                            break;

                        default:
                            ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                            break;
                    }
                    break;

                case "b":
                    double BatchTemp = 0;
                    if (!double.TryParse(switchValue, NumberStyles.Integer, CultureInfo.CurrentCulture, out BatchTemp))
                    {
                        ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    }
                    else
                    {
                        if (BatchCount == 3)
                        {
                            AppendErrorLine("Too many batches specified. The limit is 3.");
                            ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                        }
                        else
                        {
                            SetBatch(BatchCount, Convert.ToInt32(BatchTemp));
                            BatchCount += 1;
                        }
                    }
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Handle the condition where the item is not a parameter.
        /// </summary>
        protected override SwitchStatus OnNonSwitch(string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            OutputName = switchValue;
            return ss;
        }

        /// <summary>
        /// Ensure that we have a output file name when appropriate
        /// </summary>
        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = base.OnDoneParse();

            if (!NotesOnly && (ss == global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError && OutputFileName == string.Empty))
            {
                using (SaveFileDialog frm = new SaveFileDialog())
                {
                    var _with1 = frm;
                    _with1.AddExtension = true;
                    _with1.AutoUpgradeEnabled = true;
                    _with1.CheckFileExists = false;
                    _with1.CheckPathExists = true;
                    _with1.DefaultExt = "txt";
                    _with1.DereferenceLinks = true;
                    _with1.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
                    _with1.FilterIndex = 0;
                    _with1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    _with1.RestoreDirectory = true;
                    _with1.Title = "Filename for Extracted Statements";
                    _with1.ValidateNames = true;
                    DialogResult answer = _with1.ShowDialog();
                    if (answer == DialogResult.OK)
                    {
                        OutputName = _with1.FileName;
                    }
                    else
                    {
                        ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                    }
                }
            }

            return ss;
        }

        /// <summary>
        /// Print the program usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo));
            }

            AppendErrorLine("Usage: DebtPlus.client.Statements.Outsourced.Myraid.exe [-q] [-b#] [-b#] [-b#] OutputFile..." + Environment.NewLine);
            AppendErrorLine("       -q   : Quoting level [None, Auto, Strings, All]");
            AppendErrorLine("       -n   : Update statement notes only");
            AppendErrorLine("       -b   : Statement Batch Number");
        }
    }
}