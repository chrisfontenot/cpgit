namespace DebtPlus.UI.Desktop.CS.Client.Ticklers.Utility
{
    internal partial class MyArgumentParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public MyArgumentParser() : base(new string[] { })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}