using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Interfaces.Client;
using System.Threading;
using DebtPlus.UI.Client.Service;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Update.Utility
{
    public partial class ClientUpdateMain : object
    {
        private bool m_NoAlerts = false;

        private Int32 ClientID = -1;

        public ClientUpdateMain() : base()
        {
        }

        public ClientUpdateMain(Int32 ClientId) : this(ClientId, false)
        {
        }

        public ClientUpdateMain(Int32 ClientId, bool NoAlerts) : this()
        {
            this.ClientID = ClientId;
            m_NoAlerts = NoAlerts;
        }

        public ClientUpdateMain(string Client) : this(Client, false)
        {
        }

        public ClientUpdateMain(string Client, bool NoAlerts) : this()
        {
            if (Client != string.Empty)
            {
                try
                {
                    ClientID = Convert.ToInt32(Client);
                }

                catch
                {
                    ClientID = -1;
                }
            }
            m_NoAlerts = NoAlerts;
        }

        public void ShowDialog()
        {
            // If the client is not present then search for it
            if (ClientID < 0)
            {
                using (global::DebtPlus.UI.Client.Widgets.Search.ClientSearchClass search = new global::DebtPlus.UI.Client.Widgets.Search.ClientSearchClass())
                {
                    if (search.PerformClientSearch() == DialogResult.Cancel)
                        return;
                    ClientID = search.ClientId;
                }
            }

            if (!m_NoAlerts)
            {
                Thread thrd = new Thread(ShowAlertNotes);
                thrd.IsBackground = true;
                thrd.SetApartmentState(ApartmentState.STA);
                thrd.Name = "ShowAlerts";
                thrd.Start();
            }

            // Display the client
            using (ClientUpdateClass cuc = new ClientUpdateClass())
            {
                cuc.ShowEditDialog(ClientID, false);
            }
        }

        private void ShowAlertNotes()
        {
            IAlertNotes Alerts = new Notes.AlertNotes.Client();
            try
            {
                Alerts.ShowAlerts(ClientID);
            }
            finally
            {
                if (Alerts is IDisposable)
                {
                    ((IDisposable)Alerts).Dispose();
                }
            }
        }
    }
}