using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.Client.Update.Utility
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ArgParser();
                    if (ap.Parse((string[])param))
                    {
                        var frm = new ClientUpdateMain(ap.Client, ap.noAlerts);
                        frm.ShowDialog();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "ClientUpdate"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}