using System;
using System.Globalization;

namespace DebtPlus.UI.Desktop.CS.Client.Update.Utility
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Supress alert messages for the client
        /// </summary>

        private bool _noAlerts = false;

        public bool noAlerts
        {
            get { return _noAlerts; }
        }

        /// <summary>
        /// Client for the update operation
        /// </summary>

        private Int32 _client = -1;

        public Int32 Client
        {
            get { return _client; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {
            "a",
            "c"
        })
        {
        }

        /// <summary>
        /// Process the switch options
        /// </summary>
        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "a":
                    _noAlerts = true;
                    break;

                case "c":
                    double TempClient = 0;
                    if (double.TryParse(switchValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out TempClient))
                    {
                        if (TempClient >= 0 && TempClient <= Convert.ToDouble(Int32.MaxValue))
                        {
                            _client = Convert.ToInt32(TempClient);
                            return ss;
                        }
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine("Usage: " + fname + " [-a] [-c#]")
            //AppendErrorLine("       -a : supress alert notes")
            //AppendErrorLine("       -c : specify the client for the update operation")
        }
    }
}