﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    public partial class SearchForm : DebtPlus.Data.Forms.DebtPlusForm, DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        private MapHPFValuesToDebtplus map = null;
        private Int32? propertyAddressRecord = null;
        private Int32? homeAddressRecord = null;

        public SearchForm()
        {
            InitializeComponent();

            searchCall1.SearchResults += searchCall1_SearchResults;
            searchCall1.SearchCancel += searchCall1_Canceled;
            responseGrid1.Canceled += responseGrid1_Canceled;
            responseGrid1.SelectedItem += responseGrid1_SelectedItem;
            Load += SearchForm_Load;
        }

        [STAThread]
        public void OldAppMain(string[] Arguments)
        {
            var th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(AppThread));
            th.SetApartmentState(System.Threading.ApartmentState.STA);
            th.IsBackground = true;
            th.Start(Arguments);
        }

        private void AppThread(object obj)
        {
            using (var frm = new SearchForm())
            {
                frm.ShowDialog();
            }
        }

        /// <summary>
        /// Use the standard DebtPlus forms to edit the client information
        /// </summary>
        /// <param name="ClientID"></param>
        private void DisplayClient(Int32 ClientID)
        {
            // Save the client for the MRU information
            using (var mruList = new DebtPlus.Data.MRU("Clients", "Recent Clients"))
            {
                mruList.InsertTopMost(ClientID);
                mruList.SaveKey();
            }

            // Run the thread to display the client information
            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(DisplayClientThread))
            {
                Name = "DisplayClient",
                IsBackground = false
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(ClientID);
        }

        /// <summary>
        /// Handle the client editing thread
        /// </summary>
        /// <param name="obj">The ID of the client to be edited</param>
        private void DisplayClientThread(object obj)
        {
            Int32 ClientID = Convert.ToInt32(obj);

            // Invoke the client update routine
            using (var cls = new DebtPlus.UI.Client.Service.ClientUpdateClass())
            {
                cls.ShowEditDialog(ClientID);
            }
        }

        /// <summary>
        /// Determine if the input array of strings has a non-blank value.
        /// </summary>
        /// <param name="itemList">The array of strings to be tested</param>
        /// <returns>TRUE if the entire list is empty. FALSE if not.</returns>
        private bool emptyItems(string[] itemList)
        {
            foreach (string str in itemList)
            {
                if (!string.IsNullOrEmpty(str) && !string.IsNullOrWhiteSpace(str))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// From the address components, return a new address record
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="addressLine1"></param>
        /// <param name="addressLine2"></param>
        /// <param name="city"></param>
        /// <param name="stateCode"></param>
        /// <param name="zipCode"></param>
        /// <returns></returns>
        private Int32? GenerateAddress(BusinessContext bc, string addressLine1, string addressLine2, string city, string stateCode, string zipCode)
        {
            // Determine if the entry is defined
            if (emptyItems(new string[] { addressLine1, addressLine2, city, stateCode, zipCode }))
            {
                return null;
            }

            // Allocate the address record and insert it into the tables
            var addr = new address(false, false, false, false)
            {
                usda_status = "?",
                address_line_2 = addressLine2.ToUpper(),
                city = city.ToUpper(),
                state = map.addressState(stateCode),
                PostalCode = zipCode.ToUpper()
            };

            // The first line of the address is complicated. Use the address parsing logic to split it.
            addr.ParseAddress1(addressLine1.ToUpper());

            // If there is a zipcode then fill in the missing city and state
            if (!string.IsNullOrWhiteSpace(addr.PostalCode) && (string.IsNullOrWhiteSpace(addr.city) || addr.state == 0))
            {
                string searchZip = addr.PostalCode.PadLeft(5,'0').Substring(0,5);
                var searchList = (from z in bc.ZipCodeSearches where z.ZIPCode == searchZip select z).ToList();
                var q = searchList.Where(s => s.CityType == "D").FirstOrDefault();  // Take the desired item first
                if (q == null)
                {
                    q = searchList.Where(s => s.CityType == "A").FirstOrDefault();  // Take any allowed entry
                }

                if (q != null)
                {
                    addr.city = q.CityName.ToUpper();
                    addr.state = q.State;
                }
            }

            // Insert the address into the tables to get the ID
            bc.addresses.InsertOnSubmit(addr);
            bc.SubmitChanges();

            return addr.Id;
        }

        /// <summary>
        /// From the email components, return a new email record
        /// </summary>
        /// <param name="bc">Pointer to the business context information</param>
        /// <param name="address">Text for the email address</param>
        /// <returns></returns>
        private Int32? GenerateEmail(BusinessContext bc, string address)
        {
            if (emptyItems(new string[] { address }))
            {
                return null;
            }

            var email = new EmailAddress()
            {
                Address = address,
                ValidationCode = EmailAddress.EmailValidationEnum.Valid
            };

            // Insert the address so that we can get the ID
            bc.EmailAddresses.InsertOnSubmit(email);
            bc.SubmitChanges();

            return email.Id;
        }

        /// <summary>
        /// Create the Loan Detail information record
        /// </summary>
        private Int32? GenerateLoanDetail(BusinessContext bc, Int32 ClientID, Svc.Housing.HPF.CallLog.Log logItem, DebtPlus.LINQ.client_housing clientHousing, DebtPlus.LINQ.Housing_property propertyRecord, DebtPlus.LINQ.Housing_loan loanRecord)
        {
            var detailRecord = new DebtPlus.LINQ.Housing_loan_detail()
                {
                    MortgageTypeCD = DebtPlus.LINQ.Cache.Housing_MortgageType.getDefault(),
                    FinanceTypeCD = DebtPlus.LINQ.Cache.Housing_FinancingType.getDefault(),
                    LoanTypeCD = DebtPlus.LINQ.Cache.Housing_LoanType.getDefault(),
                    InterestRate = 0D,
                    Payment = 0M,
                    ARM_Reset = false,
                    FHA_VA_Insured_Loan = false,
                    Hybrid_ARM_Loan = false,
                    Interest_Only_Loan = false,
                    Option_ARM_Loan = false,
                    Privately_Held_Loan = false
                };

            if (logItem.PaymentAmount.HasValue)
            {
                detailRecord.Payment = (decimal)logItem.PaymentAmount.GetValueOrDefault(0);
                bc.Housing_loan_details.InsertOnSubmit(detailRecord);
                bc.SubmitChanges();
                return detailRecord.Id;
            }
            return null;
        }

        /// <summary>
        /// Create the Loan Lender information record
        /// </summary>
        private Int32? GenerateLoanLender(BusinessContext bc, Int32 ClientID, Svc.Housing.HPF.CallLog.Log logItem, DebtPlus.LINQ.client_housing clientHousing, DebtPlus.LINQ.Housing_property propertyRecord, DebtPlus.LINQ.Housing_loan loanRecord)
        {
            var lenderRecord = new DebtPlus.LINQ.Housing_lender()
                {
                    CaseNumber = string.Empty,
                    FdicNcusNum = string.Empty,
                    ServicerName = logItem.ServicerName,
                    ServicerID = map.loanServicerID(logItem.ServicerId),
                    AcctNum = logItem.LoanAccountNumber
                };

            bc.Housing_lenders.InsertOnSubmit(lenderRecord);
            bc.SubmitChanges();
            return lenderRecord.Id;
        }

        /// <summary>
        /// From the name components, return a new name record
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="firstMiddle"></param>
        /// <param name="lastSuffix"></param>
        /// <returns></returns>
        private Int32? GenerateName(BusinessContext bc, string firstMiddle, string lastSuffix)
        {
            // Determine if the entry is defined
            if (emptyItems(new string[] { firstMiddle, lastSuffix }))
            {
                return null;
            }

            // Allocate the name record
            var name = new DebtPlus.LINQ.Name();

            // Split the first/middle values
            if (!string.IsNullOrEmpty(firstMiddle))
            {
                var rx = new System.Text.RegularExpressions.Regex(@"^\s*(\S+)\s+(\S+)\s*$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
                var m = rx.Match(firstMiddle);
                if (m.Success)
                {
                    name.First = m.Groups[1].Captures[0].Value;
                    name.Middle = m.Groups[2].Captures[0].Value;
                }
                else
                {
                    name.First = firstMiddle;
                }
            }

            // Split the last/suffix values
            if (!string.IsNullOrEmpty(lastSuffix))
            {
                var rx = new System.Text.RegularExpressions.Regex(@"^\s*(\S+)\,?\s+(jr|sr|md|phd|ii|iii|iv|v|vi|vii)\.?\s*$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
                var m = rx.Match(lastSuffix);
                if (m.Success)
                {
                    name.Last = m.Groups[1].Captures[0].Value;
                    name.Suffix = m.Groups[2].Captures[0].Value;
                }
                else
                {
                    name.Last = lastSuffix;
                }
            }

            // Insert the name to get the ID value
            bc.Names.InsertOnSubmit(name);
            bc.SubmitChanges();
            return name.Id;
        }

        /// <summary>
        /// Create a new borrower record for the indicated name
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="codeValue"></param>
        private Int32? GeneratePropertyOwner(BusinessContext bc, Int32 codeValue)
        {
            // Insert the owner information
            var owner = new DebtPlus.LINQ.Housing_borrower()
            {
                PersonID = codeValue,
                CreditAgency = DebtPlus.LINQ.InMemory.CreditAgencyTypes.getDefault(),
                Disabled = 0,
                Ethnicity = DebtPlus.LINQ.Cache.EthnicityType.getDefault(),
                Gender = DebtPlus.LINQ.Cache.GenderType.getDefault(),
                Language = DebtPlus.LINQ.Cache.AttributeType.getDefaultLanguage(),
                marital_status = DebtPlus.LINQ.Cache.MaritalType.getDefault(),
                Race = DebtPlus.LINQ.Cache.RaceType.getDefault(),
                SSN = null,
                Education = DebtPlus.LINQ.Cache.EducationType.getDefault(),
                FICO_Score = 0
            };
            bc.Housing_borrowers.InsertOnSubmit(owner);
            bc.SubmitChanges();
            return owner.Id;
        }

        /// <summary>
        /// From the telephone number components, return a new telephone number record
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        private Int32? GenerateTelephone(BusinessContext bc, string phoneNumber)
        {
            // Determine if the entry is defined
            if (!emptyItems(new string[] { phoneNumber }))
            {
                // Allocate the telephone number and parse the string
                var phone = new TelephoneNumber()
                {
                    Country = DebtPlus.LINQ.Cache.country.getDefault()
                };

                if (phone.TryParse(DebtPlus.Utils.Format.Strings.DigitsOnly(phoneNumber)))
                {
                    // Put a dash between the 3rd and 4thd digits of the number to prevent the editing routine from
                    // doing that and then claiming that the number was changed.
                    if ((new System.Text.RegularExpressions.Regex(@"^\d{4,}$")).IsMatch(phone.Number))
                    {
                        phone.Number = string.Format("{0}-{1}", phone.Number.Substring(0, 3), phone.Number.Substring(4));
                    }

                    bc.TelephoneNumbers.InsertOnSubmit(phone);
                    bc.SubmitChanges();
                    return phone.Id;
                }
            }
            return null;
        }

        /// <summary>
        /// Create items associated with the new applicant (person)
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <param name="logItem"></param>
        private void ProcessApplicantItems(BusinessContext bc, Int32 ClientID, Svc.Housing.HPF.CallLog.Log logItem)
        {
            // Allocate the applicant record. The applicants have a relation = 1 (self) designation.
            // The xpr_create_client procedure inserts this row into the people table.
            var p = (from pr in bc.peoples where pr.Client == ClientID && pr.Relation == 1 select pr).FirstOrDefault();
            System.Diagnostics.Debug.Assert(p != null);

            // Add the email and name values
            p.EmailID = GenerateEmail(bc, logItem.Email);
            p.NameID = GenerateName(bc, logItem.FirstName, logItem.LastName);
            p.gross_income = (decimal)logItem.GrossIncomeAmount.GetValueOrDefault(0);
            p.net_income = (decimal)logItem.GrossIncomeAmount.GetValueOrDefault(0);

            // Determine the military status
            p.MilitaryStatusID = ((logItem.MilActiveInd ?? string.Empty).PadRight(1, 'N')[0] == 'Y') ? 1 : 0;
        }

        /// <summary>
        /// Create items associated with the new client
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <param name="logItem"></param>
        private void ProcessClientItems(BusinessContext bc, Int32 ClientID, Svc.Housing.HPF.CallLog.Log logItem)
        {
            // Retrieve the client record
            var clientRecord = bc.clients.Where(s => s.Id == ClientID).FirstOrDefault();
            System.Diagnostics.Debug.Assert(clientRecord != null);

            // Update the client home address
            clientRecord.AddressID = homeAddressRecord;
            clientRecord.HomeTelephoneID = GenerateTelephone(bc, logItem.CustomerPhone);
        }

        /// <summary>
        /// Process the client_housing table updates
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <param name="logItem"></param>
        private void ProcessHousingItems(BusinessContext bc, Int32 ClientID, Svc.Housing.HPF.CallLog.Log logItem)
        {
            var clientHousing = (from ch in bc.client_housings where ch.Id == ClientID select ch).FirstOrDefault();
            System.Diagnostics.Debug.Assert(clientHousing != null);

            // The DTI indicator just says that the value is greater or equal to 31%.
            clientHousing.BackEnd_DTI = ((logItem.DTIInd ?? string.Empty).PadRight(1))[0] == 'Y' ? 0.31 : 0;

            // Process the property information
            ProcessPropertyItems(bc, ClientID, logItem, clientHousing);
        }

        /// <summary>
        /// Create items associated with the new housing property
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <param name="logItem"></param>
        private void ProcessPropertyItems(BusinessContext bc, Int32 ClientID, Svc.Housing.HPF.CallLog.Log logItem, DebtPlus.LINQ.client_housing clientHousing)
        {
            // Insert a property record
            var prop = new DebtPlus.LINQ.Housing_property()
                        {
                            HousingID = clientHousing.Id,
                            PlanToKeepHomeCD = DebtPlus.LINQ.Cache.Housing_RetentionType.getDefault(),
                            PropertyType = DebtPlus.LINQ.Cache.HousingType.getDefault().GetValueOrDefault(),
                            Occupancy = 1,
                            ImprovementsValue = 0M,
                            LandValue = 0M,
                            annual_ins_amount = 0M,
                            annual_property_tax = 0M,
                            property_tax = 0,
                            SalePrice = 0M,
                            TrustName = string.Empty,
                            FcNoticeReceivedIND = false,
                            UseInReports = true
                        };

            prop.PropertyAddress = propertyAddressRecord;
            prop.UseHomeAddress = (prop.PropertyAddress == null);
            prop.OwnerID = GeneratePropertyOwner(bc, 1);
            prop.CoOwnerID = GeneratePropertyOwner(bc, 2);
            prop.FcSaleDT = logItem.FCSaleDate;
            prop.FcNoticeReceivedIND = prop.ForSaleIND = (prop.FcSaleDT.HasValue);

            // Primary residence only has meaning if the value is "Y". If "N", then we guess that it is a vacation home.
            var col = DebtPlus.LINQ.Cache.Housing_ResidencyType.getList();
            if (((logItem.PrimaryResidenceInd ?? string.Empty).PadRight(1, 'N'))[0] == 'Y')
            {
                var q = col.Where(s => s.primaryResidence).FirstOrDefault();
                if (q != null)
                {
                    prop.Residency = q.Id;
                }
            }
            else
            {
                var q = col.Where(s => !s.vacantOrCondemned && !s.primaryResidence).FirstOrDefault();
                if (q != null)
                {
                    prop.Residency = q.Id;
                }
            }

            bc.Housing_properties.InsertOnSubmit(prop);
            bc.SubmitChanges();

            // Given the property ID, generate the loan records for it
            ProcessPropertyLoanItems(bc, ClientID, logItem, clientHousing, prop);
        }

        /// <summary>
        /// Create items associated with the new property loans
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <param name="logItem"></param>
        /// <param name="clientHousing"></param>
        /// <param name="propertyRecord"></param>
        private void ProcessPropertyLoanItems(BusinessContext bc, Int32 ClientID, Svc.Housing.HPF.CallLog.Log logItem, DebtPlus.LINQ.client_housing clientHousing, DebtPlus.LINQ.Housing_property propertyRecord)
        {
            var loanRecord = new DebtPlus.LINQ.Housing_loan()
                                {
                                    Loan1st2nd = 1,
                                    PropertyID = propertyRecord.Id,
                                    UseInReports = true,
                                    IntakeSameAsCurrent = true,
                                    UseOrigLenderID = true,
                                    LoanDelinquencyMonths = 0,
                                    CurrentLoanBalanceAmt = 0M,
                                    OrigLoanAmt = 0M,
                                    MortgageClosingCosts = 0M
                                };

            loanRecord.IntakeDetailID = GenerateLoanDetail(bc, ClientID, logItem, clientHousing, propertyRecord, loanRecord);
            loanRecord.CurrentLenderID = GenerateLoanLender(bc, ClientID, logItem, clientHousing, propertyRecord, loanRecord);

            // Replace the delinquency code
            loanRecord.LoanDelinquencyMonths = map.loanDelinquency(logItem.LoanDelinqStatusCd);

            // Record the loan record. Defer the insert until the end since we don't need an ID.
            bc.Housing_loans.InsertOnSubmit(loanRecord);
        }

        /// <summary>
        /// Once we have an item to process, generate the client information
        /// </summary>
        /// <param name="logItem">Pointer to the log entry for the client</param>
        private void ProcessSearchResult(Svc.Housing.HPF.CallLog.Log logItem)
        {
            string connectionString = SQLInfoClass.getDefault().ConnectionString;
            Int32 clientID = 0;
            using (var cn = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                System.Data.SqlClient.SqlTransaction txn = null;
                try
                {
                    cn.Open();
                    txn = cn.BeginTransaction();

                    using (var bc = new BusinessContext(cn))
                    {
                        // Attach the transaction to the BusinessContext LINQ item
                        bc.Transaction = txn;

                        // Create the client transaction
                        clientID = bc.xpr_create_client();

                        // Process the client information
                        map = new MapHPFValuesToDebtplus();
                        try
                        {
                            // Find the address records. Sometimes there is no property address record entry.
                            homeAddressRecord = GenerateAddress(bc, logItem.MailingAddr1, logItem.MailingAddr2, logItem.MailingCity, logItem.MailingStateCd, logItem.MailingZip);
                            propertyAddressRecord = GenerateAddress(bc, logItem.PropStreetAddress, string.Empty, logItem.City, logItem.State, logItem.PropZipFull9);

                            // Make sure that the items are proper. Prefer putting the address into the home mailing address field.
                            if (!homeAddressRecord.HasValue && propertyAddressRecord.HasValue)
                            {
                                homeAddressRecord = propertyAddressRecord;
                                propertyAddressRecord = null;
                            }

                            ProcessClientItems(bc, clientID, logItem);
                            ProcessApplicantItems(bc, clientID, logItem);
                            ProcessHousingItems(bc, clientID, logItem);
                        }
                        finally
                        {
                            map.Dispose();
                        }

                        // Submit the changes to the business layer and complete the transaction
                        bc.SubmitChanges();
                        txn.Commit();
                        txn.Dispose();
                        txn = null;

                        // Complete the current form processing
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                }
                catch
                {
                    if (txn != null)
                    {
                        try { txn.Rollback(); }
                        catch { }
                        txn.Dispose();
                        txn = null;
                    }
                    throw;
                }
            }

            // Display the new client for editing
            DisplayClient(clientID);
        }

        /// <summary>
        /// The grid of values returned a CANCEL operation
        /// </summary>
        private void responseGrid1_Canceled(object sender, EventArgs e)
        {
            responseGrid1.Visible = false;
            responseGrid1.SendToBack();

            searchCall1.Visible = true;
            searchCall1.BringToFront();
        }

        /// <summary>
        /// The grid of values returned a selected item
        /// </summary>
        private void responseGrid1_SelectedItem(object sender, HPFCall_ResponseGrid.SelectedItemEventArgs e)
        {
            ProcessSearchResult(e.resp);
            return;
        }

        /// <summary>
        /// The search call input issued a CANCEL operation.
        /// </summary>
        private void searchCall1_Canceled(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// The search returned a set of values.
        /// </summary>
        private void searchCall1_SearchResults(object sender, SearchResultArgs e)
        {
            // If there are no items then tell the user and do nothing.
            if (e.colResults.Count == 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show("The search result returned no cases.", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // If there is exactly one item then that is the one that we want.
            if (e.colResults.Count == 1)
            {
                ProcessSearchResult(e.colResults[0]);
                return;
            }

            // There are more than one. Present the list.
            searchCall1.SendToBack();
            searchCall1.Visible = false;

            responseGrid1.gridControl1.DataSource = e.colResults;
            responseGrid1.Visible = true;
            responseGrid1.BringToFront();
        }

        /// <summary>
        /// Handle the LOAD event for the form
        /// </summary>
        private void SearchForm_Load(object sender, EventArgs e)
        {
            // Show the input form first
            responseGrid1.Visible = false;
            responseGrid1.SendToBack();
            searchCall1.Visible = true;
            searchCall1.BringToFront();
        }

        private class MapHPFValuesToDebtplus : System.IDisposable
        {
            private System.Collections.Generic.List<DebtPlus.LINQ.HPFCodeType> colMappingRecords = null;

            /// <summary>
            /// Obtain the list of records to map items
            /// </summary>
            private System.Collections.Generic.List<DebtPlus.LINQ.HPFCodeType> mappingRecords
            {
                get
                {
                    if (colMappingRecords == null)
                    {
                        using (var bc = new DebtPlus.LINQ.BusinessContext())
                        {
                            colMappingRecords = bc.HPFCodeTypes.ToList();
                        }
                    }
                    return colMappingRecords;
                }
                set
                {
                    colMappingRecords = value;
                }
            }

            /// <summary>
            /// Map the servicer ID from HPF to a value suitable for DebtPlus
            /// </summary>
            /// <param name="inputServicer"></param>
            /// <returns></returns>
            public Int32? loanServicerID(Int32? inputServicer)
            {
                var col = DebtPlus.LINQ.Cache.Housing_lender_servicer.getList();
                DebtPlus.LINQ.Housing_lender_servicer q = null;
                if (inputServicer.HasValue)
                {
                    q = col.Find(s => s.ServiceID == inputServicer);
                }

                if (q == null)
                {
                    q = col.Find(s => s.@Default);
                }

                if (q != null)
                {
                    return q.Id;
                }
                return null;
            }

            /// <summary>
            /// Map the HPF state value to a suitable DebtPlus value. We can not guarantee that the map is 1-to-1, but it is close.
            /// </summary>
            /// <param name="stateCode"></param>
            /// <returns></returns>
            public Int32 addressState(string stateCode)
            {
                var q = mappingRecords.Find(s => String.Compare(s.TableName, "StateCode", true) == 0 && string.Compare(s.HPF, stateCode, true) == 0);
                if (q == null)
                {
                    return 0;
                }
                return q.Id;
            }

            public void Dispose()
            {
                if (colMappingRecords != null)
                {
                    colMappingRecords.Clear();
                    colMappingRecords = null;
                }
                GC.SuppressFinalize(this);
            }

            public Int32 loanDelinquency(string delinquencyCode)
            {
                if (string.Compare("30-59", delinquencyCode, false) == 0)
                {
                    return 1;       // 1 month
                }
                else if (string.Compare("60-89", delinquencyCode, false) == 0)
                {
                    return 2;       // 2 months
                }
                else if (string.Compare("90-119", delinquencyCode, false) == 0)
                {
                    return 3;       // 3 months
                }
                return 0;           // current
            }
        }
    }
}