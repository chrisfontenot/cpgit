﻿namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HPFCase_Search1 = new DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search.HPFCase_Search();
            this.HPFCase_ResponseGrid1 = new DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search.HPFCase_ResponseGrid();
            this.HPFCall_ResponseGrid1 = new DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search.HPFCall_ResponseGrid();
            this.HPFCall_Search1 = new DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search.HPFCall_Search();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // HPFCase_Search1
            // 
            this.HPFCase_Search1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HPFCase_Search1.Location = new System.Drawing.Point(0, 0);
            this.HPFCase_Search1.Name = "HPFCase_Search1";
            this.HPFCase_Search1.Size = new System.Drawing.Size(467, 371);
            this.HPFCase_Search1.TabIndex = 0;
            // 
            // HPFCase_ResponseGrid1
            // 
            this.HPFCase_ResponseGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HPFCase_ResponseGrid1.Location = new System.Drawing.Point(0, 0);
            this.HPFCase_ResponseGrid1.Name = "HPFCase_ResponseGrid1";
            this.HPFCase_ResponseGrid1.Size = new System.Drawing.Size(467, 371);
            this.HPFCase_ResponseGrid1.TabIndex = 1;
            // 
            // HPFCall_ResponseGrid1
            // 
            this.HPFCall_ResponseGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HPFCall_ResponseGrid1.Location = new System.Drawing.Point(0, 0);
            this.HPFCall_ResponseGrid1.Name = "HPFCall_ResponseGrid1";
            this.HPFCall_ResponseGrid1.Size = new System.Drawing.Size(467, 371);
            this.HPFCall_ResponseGrid1.TabIndex = 3;
            // 
            // HPFCall_Search1
            // 
            this.HPFCall_Search1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.HPFCall_Search1.Location = new System.Drawing.Point(0, 0);
            this.HPFCall_Search1.Name = "HPFCall_Search1";
            this.HPFCall_Search1.Size = new System.Drawing.Size(467, 371);
            this.HPFCall_Search1.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 371);
            this.Controls.Add(this.HPFCase_Search1);
            this.Controls.Add(this.HPFCase_ResponseGrid1);
            this.Controls.Add(this.HPFCall_ResponseGrid1);
            this.Controls.Add(this.HPFCall_Search1);
            this.Name = "MainForm";
            this.Text = "HPF Create Client";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private HPFCase_Search HPFCase_Search1;
        private HPFCase_ResponseGrid HPFCase_ResponseGrid1;
        private HPFCall_Search HPFCall_Search1;
        private HPFCall_ResponseGrid HPFCall_ResponseGrid1;
    }
}