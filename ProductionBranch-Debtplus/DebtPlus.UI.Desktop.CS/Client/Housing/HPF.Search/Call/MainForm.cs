﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class MainForm
    {
        private MapHPFValuesToDebtplus map = null;
        private Int32? propertyAddressRecord = null;
        private Int32? homeAddressRecord = null;

        // This really does not change. But, it is here to be altered if needed.
        private bool StartedWithHousingIssues = true;

        /// <summary>
        /// Use the standard DebtPlus forms to edit the client information
        /// </summary>
        /// <param name="ClientID"></param>
        private void DisplayClient(Int32 ClientID)
        {
            // Save the client for the MRU information
            using (var mruList = new DebtPlus.Data.MRU("Clients", "Recent Clients"))
            {
                mruList.InsertTopMost(ClientID);
                mruList.SaveKey();
            }

            // Run the thread to display the client information
            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(DisplayClientThread))
            {
                Name = "DisplayClient",
                IsBackground = false
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start(ClientID);
        }

        /// <summary>
        /// Handle the client editing thread
        /// </summary>
        /// <param name="obj">The ID of the client to be edited</param>
        private void DisplayClientThread(object obj)
        {
            Int32 ClientID = Convert.ToInt32(obj);

            // Invoke the client update routine
            using (var cls = new DebtPlus.UI.Client.Service.ClientUpdateClass())
            {
                cls.ShowEditDialog(ClientID, true);

                // After editing the client, add the privacy policy if needed.
                using (var bc = new BusinessContext())
                {
                    // A client record is required
                    var clientRecord = bc.clients.Where(s => s.Id == ClientID).FirstOrDefault();
                    if (clientRecord == null)
                    {
                        return;
                    }

                    // applicant information.
                    var applicantRecord = bc.peoples.Where(s => s.Client == clientRecord.Id && s.Relation == 1).FirstOrDefault();
                    if (applicantRecord == null)
                    {
                        return;
                    }

                    DebtPlus.LINQ.Name nameRecord = null;
                    if (applicantRecord.NameID != null)
                    {
                        nameRecord = bc.Names.Where(s => s.Id == applicantRecord.NameID).FirstOrDefault();
                    }

                    DebtPlus.LINQ.EmailAddress emailRecord = null;
                    if (applicantRecord.EmailID != null)
                    {
                        emailRecord = bc.EmailAddresses.Where(s => s.Id == applicantRecord.EmailID).FirstOrDefault();
                    }

                    // Create the privacy policy message in the queue
                    CreatePrivacyPolicy(bc, clientRecord, applicantRecord, emailRecord, nameRecord);

                    try
                    {
                        bc.SubmitChanges();
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating email queue for HPF");
                    }
                }
            }
        }

        /// <summary>
        /// Determine if the KeyField array of strings has a non-blank value.
        /// </summary>
        /// <param name="itemList">The array of strings to be tested</param>
        /// <returns>TRUE if the entire list is empty. FALSE if not.</returns>
        private bool emptyItems(string[] itemList)
        {
            foreach (string str in itemList)
            {
                if (!string.IsNullOrEmpty(str) && !string.IsNullOrWhiteSpace(str))
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// From the address components, return a new address record
        /// </summary>
        private Int32? GenerateAddress(BusinessContext bc, string addressLine1, string addressLine2, string city, string stateCode, string zipCode)
        {
            // Determine if the entry is defined
            if (emptyItems(new string[] { addressLine1, addressLine2, city, stateCode, zipCode }))
            {
                return null;
            }

            // Allocate the address record and insert it into the tables
            var addr = DebtPlus.LINQ.Factory.Manufacture_address();

            addr.address_line_2 = string.IsNullOrWhiteSpace(addressLine2) ? string.Empty : addressLine2.ToUpper();
            addr.city           = string.IsNullOrWhiteSpace(city) ? string.Empty : city.ToUpper();
            addr.state          = string.IsNullOrWhiteSpace(stateCode) ? 0 : map.addressState(stateCode);
            addr.PostalCode     = string.IsNullOrWhiteSpace(zipCode) ? string.Empty : zipCode.ToUpper();

            // The first line of the address is complicated. Use the address parsing logic to split it.
            addr.ParseAddress1(string.IsNullOrWhiteSpace(addressLine1) ? string.Empty : addressLine1.ToUpper());

            // If there is a zip code then fill in the missing city and state
            if (!string.IsNullOrWhiteSpace(addr.PostalCode) && (string.IsNullOrWhiteSpace(addr.city) || addr.state == 0))
            {
                string searchZip = addr.PostalCode.PadLeft(5, '0').Substring(0, 5);
                var searchList   = bc.ZipCodeSearches.Where(s => s.ZIPCode == searchZip).ToList();
                var q            = searchList.Where(s => s.CityType == 'D').FirstOrDefault();  // Take the desired item first
                if (q == null)
                {
                    q = searchList.Where(s => s.CityType == 'A').FirstOrDefault();  // Take any allowed entry
                }

                if (q != null)
                {
                    addr.city  = q.CityName;
                    addr.state = q.State.GetValueOrDefault(1);
                }
            }

            // Insert the address into the tables to get the ID
            bc.addresses.InsertOnSubmit(addr);
            bc.SubmitChanges();

            return addr.Id;
        }

        /// <summary>
        /// From the email components, return a new email record
        /// </summary>
        /// <param name="bc">Pointer to the business context information</param>
        /// <param name="address">Text for the email address</param>
        /// <returns></returns>
        private Int32? GenerateEmail(BusinessContext bc, string address)
        {
            if (emptyItems(new string[] { address }))
            {
                return null;
            }

            var email = DebtPlus.LINQ.Factory.Manufacture_EmailAddress();

            email.Address = address;
            email.ValidationCode = EmailAddress.EmailValidationEnum.Valid;

            // Insert the address so that we can get the ID
            bc.EmailAddresses.InsertOnSubmit(email);
            bc.SubmitChanges();

            return email.Id;
        }

        /// <summary>
        /// Create the Loan Detail information record
        /// </summary>
        private Int32? GenerateLoanDetail(BusinessContext bc, Int32 ClientID, global::DebtPlus.UI.Housing.HPF.Data.Log logItem, DebtPlus.LINQ.client_housing clientHousing, DebtPlus.LINQ.Housing_property propertyRecord, DebtPlus.LINQ.Housing_loan loanRecord)
        {
            if (logItem.PaymentAmount.HasValue)
            {
                var detailRecord = DebtPlus.LINQ.Factory.Manufacture_housing_loan_detail();

                detailRecord.Payment = (decimal)logItem.PaymentAmount.GetValueOrDefault(0);
                bc.Housing_loan_details.InsertOnSubmit(detailRecord);
                bc.SubmitChanges();
                return detailRecord.Id;
            }
            return null;
        }

        /// <summary>
        /// Create the Loan Lender information record
        /// </summary>
        private Int32? GenerateLoanLender(BusinessContext bc, Int32 ClientID, DebtPlus.UI.Housing.HPF.Data.Log logItem, DebtPlus.LINQ.client_housing clientHousing, DebtPlus.LINQ.Housing_property propertyRecord, DebtPlus.LINQ.Housing_loan loanRecord)
        {
            var lenderRecord = DebtPlus.LINQ.Factory.Manufacture_housing_lender();

            lenderRecord.ServicerName = logItem.ServicerName;
            lenderRecord.ServicerID = map.loanServicerID(logItem.ServicerId);
            lenderRecord.AcctNum = getAccountNumber(logItem.LoanAccountNumber);

            bc.Housing_lenders.InsertOnSubmit(lenderRecord);
            bc.SubmitChanges();
            return lenderRecord.Id;
        }

        /// <summary>
        /// Obtain the account number to be used in the transaction. It may be too long for our database.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private string getAccountNumber(string source)
        {
            // Blanks are null
            if (string.IsNullOrWhiteSpace(source))
            {
                return null;
            }

            // If the account number length is valid, just use it as given
            if (source.Length < 50)
            {
                return source;
            }

            // Try to remove the dash characters to determine if they will work now.
            source = source.Replace("-", "");
            if (source.Length < 50)
            {
                return source;
            }

            // Split the number at the first non-alphanumeric
            string[] captures = System.Text.RegularExpressions.Regex.Split(source, "[^0-9A-Z]", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (captures.GetUpperBound(0) > 0)
            {
                // Take the first one that contains a number. It will ignore names that may precede the item.
                var str = captures.FirstOrDefault(s => System.Text.RegularExpressions.Regex.IsMatch(s, "[0-9]"));
                if (str != null)
                {
                    source = str;
                    if (source.Length < 50)
                    {
                        return source;
                    }
                }
            }

            // Otherwise, just truncate the number at the length and be done with it.
            return source.Substring(0, 50);
        }

        /// <summary>
        /// From the name components, return a new name record
        /// </summary>
        private Int32? GenerateName(BusinessContext bc, string firstMiddle, string lastSuffix)
        {
            // Determine if the entry is defined
            if (emptyItems(new string[] { firstMiddle, lastSuffix }))
            {
                return null;
            }

            // Allocate the name record
            var name = DebtPlus.LINQ.Factory.Manufacture_Name();

            // Split the first/middle values
            if (!string.IsNullOrEmpty(firstMiddle))
            {
                var rx = new System.Text.RegularExpressions.Regex(@"^\s*(\S+)\s+(\S+)\s*$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
                var m = rx.Match(firstMiddle);
                if (m.Success)
                {
                    name.First = m.Groups[1].Captures[0].Value;
                    name.Middle = m.Groups[2].Captures[0].Value;
                }
                else
                {
                    name.First = firstMiddle;
                }
            }

            // Split the last/suffix values
            if (!string.IsNullOrEmpty(lastSuffix))
            {
                var rx = new System.Text.RegularExpressions.Regex(@"^\s*(\S+)\,?\s+(jr|sr|md|phd|ii|iii|iv|v|vi|vii)\.?\s*$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
                var m = rx.Match(lastSuffix);
                if (m.Success)
                {
                    name.Last = m.Groups[1].Captures[0].Value;
                    name.Suffix = m.Groups[2].Captures[0].Value;
                }
                else
                {
                    name.Last = lastSuffix;
                }
            }

            // Insert the name to get the ID value
            bc.Names.InsertOnSubmit(name);
            bc.SubmitChanges();
            return name.Id;
        }

        /// <summary>
        /// Create a new borrower record for the indicated name
        /// </summary>
        private Int32? GeneratePropertyOwner(BusinessContext bc, Int32 codeValue)
        {
            // Insert the owner information
            var owner = DebtPlus.LINQ.Factory.Manufacture_housing_borrower();
            owner.PersonID = codeValue;

            bc.Housing_borrowers.InsertOnSubmit(owner);
            bc.SubmitChanges();
            return owner.Id;
        }

        /// <summary>
        /// From the telephone number components, return a new telephone number record
        /// </summary>
        private Int32? GenerateTelephone(BusinessContext bc, string phoneNumber)
        {
            // Determine if the entry is defined
            if (!emptyItems(new string[] { phoneNumber }))
            {
                var phone = DebtPlus.LINQ.TelephoneNumber.TryParse(DebtPlus.Utils.Format.Strings.DigitsOnly(phoneNumber));
                if (phone != null)
                {
                    bc.TelephoneNumbers.InsertOnSubmit(phone);
                    bc.SubmitChanges();
                    return phone.Id;
                }
            }
            return null;
        }

        /// <summary>
        /// Create items associated with the new applicant (person)
        /// </summary>
        private void ProcessApplicantItems(BusinessContext bc, Int32 ClientID, DebtPlus.UI.Housing.HPF.Data.Log logItem)
        {
            // Allocate the applicant record. The applicants have a relation = 1 (self) designation.
            // The xpr_create_client procedure inserts this row into the people table.
            var p = bc.peoples.Where(pr => pr.Client == ClientID && pr.Relation == 1).FirstOrDefault();
            System.Diagnostics.Debug.Assert(p != null);

            // Add the email and name values
            p.EmailID = GenerateEmail(bc, logItem.Email);
            p.NameID = GenerateName(bc, logItem.FirstName, logItem.LastName);
            p.gross_income = (decimal)logItem.GrossIncomeAmount.GetValueOrDefault(0);
            p.net_income = (decimal)logItem.GrossIncomeAmount.GetValueOrDefault(0);

            // Determine the military status
            p.MilitaryStatusID = ((logItem.MilActiveInd ?? string.Empty).PadRight(1, 'N')[0] == 'Y') ? 1 : 0;
        }

        /// <summary>
        /// Create items associated with the new client
        /// </summary>
        private void ProcessClientItems(BusinessContext bc, Int32 ClientID, DebtPlus.UI.Housing.HPF.Data.Log logItem)
        {
            // Retrieve the client record
            var clientRecord = bc.clients.Where(s => s.Id == ClientID).FirstOrDefault();
            System.Diagnostics.Debug.Assert(clientRecord != null);

            // Supply the client fields
            clientRecord.AddressID = homeAddressRecord;
            clientRecord.HomeTelephoneID = GenerateTelephone(bc, logItem.CustomerPhone);
            clientRecord.mortgage_problems = StartedWithHousingIssues;
        }

        /// <summary>
        /// Process the client_housing table updates
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="ClientID"></param>
        /// <param name="logItem"></param>
        private void ProcessHousingItems(BusinessContext bc, Int32 ClientID, DebtPlus.UI.Housing.HPF.Data.Log logItem, bool didWorkWithAgency)
        {
            var clientHousing = bc.client_housings.Where(ch => ch.Id == ClientID).FirstOrDefault();
            System.Diagnostics.Debug.Assert(clientHousing != null);

            // The DTI indicator just says that the value is greater or equal to 31%.
            clientHousing.BackEnd_DTI = ((logItem.DTIInd ?? string.Empty).PadRight(1))[0] == 'Y' ? 0.31 : 0;

            // Process the KeyField information
            ProcessPropertyItems(bc, ClientID, logItem, clientHousing, didWorkWithAgency);
        }

        /// <summary>
        /// Create items associated with the new housing KeyField
        /// </summary>
        private void ProcessPropertyItems(BusinessContext bc, Int32 ClientID, DebtPlus.UI.Housing.HPF.Data.Log logItem, DebtPlus.LINQ.client_housing clientHousing, bool didWorkWithAgency)
        {
            // Insert a KeyField record
            var prop = DebtPlus.LINQ.Factory.Manufacture_Housing_property(clientHousing.Id);

            prop.HPF_worked_with_other_agency = didWorkWithAgency;
            prop.UseInReports = true;
            prop.PropertyAddress = propertyAddressRecord;
            prop.UseHomeAddress = (prop.PropertyAddress == null);
            prop.OwnerID = GeneratePropertyOwner(bc, 1);
            prop.CoOwnerID = GeneratePropertyOwner(bc, 2);
            prop.FcSaleDT = logItem.FCSaleDate;
            prop.FcNoticeReceivedIND = prop.ForSaleIND = (prop.FcSaleDT.HasValue);

            // Primary residence only has meaning if the value is "Y". If "N", then we guess that it is a vacation home.
            var col = DebtPlus.LINQ.Cache.Housing_ResidencyType.getList();
            if (((logItem.PrimaryResidenceInd ?? string.Empty).PadRight(1, 'Y'))[0] == 'Y')
            {
                var q = col.Where(s => s.primaryResidence).FirstOrDefault();
                if (q != null)
                {
                    prop.Residency = q.Id;
                }
            }
            else
            {
                var q = col.Where(s => !s.vacantOrCondemned && !s.primaryResidence).FirstOrDefault();
                if (q != null)
                {
                    prop.Residency = q.Id;
                }
            }

            bc.Housing_properties.InsertOnSubmit(prop);
            bc.SubmitChanges();

            // Given the KeyField ID, generate the loan records for it
            ProcessPropertyLoanItems(bc, ClientID, logItem, clientHousing, prop);
        }

        /// <summary>
        /// Create items associated with the new KeyField loans
        /// </summary>
        private void ProcessPropertyLoanItems(BusinessContext bc, Int32 ClientID, DebtPlus.UI.Housing.HPF.Data.Log logItem, DebtPlus.LINQ.client_housing clientHousing, DebtPlus.LINQ.Housing_property propertyRecord)
        {
            var loanRecord = DebtPlus.LINQ.Factory.Manufacture_Housing_loan(propertyRecord.Id);

            loanRecord.Loan1st2nd = 1;
            loanRecord.UseInReports = true;
            loanRecord.IntakeDetailID = GenerateLoanDetail(bc, ClientID, logItem, clientHousing, propertyRecord, loanRecord);
            loanRecord.CurrentLenderID = GenerateLoanLender(bc, ClientID, logItem, clientHousing, propertyRecord, loanRecord);

            // Replace the delinquency code
            loanRecord.LoanDelinquencyMonths = map.loanDelinquency(logItem.LoanDelinqStatusCd);

            // Record the loan record. Defer the insert until the end since we don't need an ID.
            bc.Housing_loans.InsertOnSubmit(loanRecord);
        }

        /// <summary>
        /// Once we have an item to process, generate the client information
        /// </summary>
        /// <param name="logItem">Pointer to the log entry for the client</param>
        private void ProcessSearchResult(DebtPlus.UI.Housing.HPF.Data.Log logItem, bool didWorkWithAgency)
        {
            string connectionString = SQLInfoClass.getDefault().ConnectionString;
            Int32 clientID = 0;
            using (var cn = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                System.Data.SqlClient.SqlTransaction txn = null;
                try
                {
                    cn.Open();
                    txn = cn.BeginTransaction();

                    using (var bc = new BusinessContext(cn))
                    {
                        // Attach the transaction to the BusinessContext LINQ item
                        bc.Transaction = txn;

                        // Create the client transaction
                        clientID = bc.xpr_create_client();

                        // Process the client information
                        map = new MapHPFValuesToDebtplus();
                        try
                        {
                            // Find the address records. Sometimes there is no client address record entry.
                            homeAddressRecord = GenerateAddress(bc, logItem.MailingAddr1, logItem.MailingAddr2, logItem.MailingCity, logItem.MailingStateCd, logItem.MailingZip);
                            propertyAddressRecord = GenerateAddress(bc, logItem.PropStreetAddress, string.Empty, logItem.City, logItem.State, logItem.PropZipFull9);

                            // Make sure that the items are proper. Prefer putting the address into the home mailing address field.
                            if (!homeAddressRecord.HasValue && propertyAddressRecord.HasValue)
                            {
                                homeAddressRecord = propertyAddressRecord;
                                propertyAddressRecord = null;
                            }

                            ProcessClientItems(bc, clientID, logItem);
                            ProcessApplicantItems(bc, clientID, logItem);
                            ProcessHousingItems(bc, clientID, logItem, didWorkWithAgency);
                        }
                        finally
                        {
                            map.Dispose();
                        }

                        // Submit the changes to the business layer and complete the transaction
                        bc.SubmitChanges();
                        txn.Commit();
                        txn.Dispose();
                        txn = null;

                        // Complete the current form processing
                        DialogResult = System.Windows.Forms.DialogResult.OK;

                        // It is possible that we are not a modal dialog. Close the window normally.
                        if (!this.Modal)
                        {
                            this.Close();
                        }
                    }
                }
                catch
                {
                    if (txn != null)
                    {
                        try { txn.Rollback(); }
                        catch { }
                        txn.Dispose();
                        txn = null;
                    }
                    throw;
                }
            }

            // Display the new client for editing
            DisplayClient(clientID);
        }

        /// <summary>
        /// Create a BluePrint email message if desired
        /// </summary>
        private void CreateBluePrintNotice(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.client clientRecord, DebtPlus.LINQ.people applicantRecord, DebtPlus.LINQ.EmailAddress emailRecord, DebtPlus.LINQ.Name nameRecord)
        {
            // Find the client name from the name record
            string clientEmailAddress = null;
            if (emailRecord != null && (emailRecord.ValidationCode == EmailAddress.EmailValidationEnum.Invalid || emailRecord.ValidationCode == EmailAddress.EmailValidationEnum.Valid))
            {
                clientEmailAddress = emailRecord.Address;
            }

            if (string.IsNullOrWhiteSpace(clientEmailAddress))
            {
                return;
            }

            // Add an entry to the email queue if there is an email address
            var qTemplate = bc.email_templates.Where(s => s.description == "HPF BluePrint Notice").FirstOrDefault();
            if (qTemplate != null)
            {
                return;
            }

            // Construct the email queue substitution data
            var sbSub = new System.Text.StringBuilder();
            sbSub.Append("<substitutions>");

            // Find the name of the client
            string clientName = (nameRecord != null) ? string.Empty : nameRecord.ToString();
            sbSub.AppendFormat("<substitution><field>client_name</field><value>{0}</value></substitution>", clientName);

            // Find the name of the counselor
            var counselorID = DebtPlus.LINQ.BusinessContext.suser_sname();
            if (! string.IsNullOrEmpty(counselorID))
            {
                var coRecord = DebtPlus.LINQ.Cache.counselor.getList().Find(s => string.Equals(counselorID, s.Person, StringComparison.InvariantCultureIgnoreCase));
                if (coRecord != null && coRecord.Name != null)
                {
                    sbSub.AppendFormat("<substitution><field>counselor_name</field><value>{0}</value></substitution>", coRecord.Name.ToString());
                }
            }
            sbSub.Append("</substitutions>");

            // Construct the queue entry to cause the message to be sent.
            var qEmail = DebtPlus.LINQ.Factory.Manufacture_email_queue();
            qEmail.email_address = clientEmailAddress;
            qEmail.email_name = string.IsNullOrEmpty(clientName) ? clientEmailAddress : clientName;
            qEmail.template = qTemplate.Id;
            qEmail.substitutions = System.Xml.Linq.XElement.Parse(sbSub.ToString());

            bc.email_queues.InsertOnSubmit(qEmail);
        }

        /// <summary>
        /// Create a privacy policy submission to be either emailed or printed to the client
        /// </summary>
        private void CreatePrivacyPolicy(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.client clientRecord, DebtPlus.LINQ.people applicantRecord, DebtPlus.LINQ.EmailAddress emailRecord, DebtPlus.LINQ.Name nameRecord)
        {
            // Find the client name from the name record
            string clientName = null;
            if (nameRecord != null)
            {
                clientName = nameRecord.ToString();
            }

            // Find the client name from the name record
            string clientEmailAddress = null;
            if (emailRecord != null && (emailRecord.ValidationCode == EmailAddress.EmailValidationEnum.Invalid || emailRecord.ValidationCode == EmailAddress.EmailValidationEnum.Valid))
            {
                clientEmailAddress = emailRecord.Address;
            }

            // Find the physical address for the client
            string mailingAddress = null;
            if (clientRecord.AddressID.HasValue)
            {
                var clientAddress = bc.addresses.Where(s => s.Id == clientRecord.AddressID).FirstOrDefault();
                if (clientAddress != null)
                {
                    mailingAddress = clientAddress.ToString();
                }
            }

            // Email Queue ID for the Privacy Policy notice
            Int32? email_queueID = null;
            // Add an entry to the email queue if there is an email address
            if (!string.IsNullOrEmpty(clientEmailAddress))
            {
                var qTemplate = bc.email_templates.Where(s => s.description == "HPF Privacy Policy").FirstOrDefault();
                if (qTemplate != null)
                {
                    var qEmail           = DebtPlus.LINQ.Factory.Manufacture_email_queue();
                    qEmail.email_address = clientEmailAddress;
                    qEmail.email_name    = string.IsNullOrEmpty(clientName) ? clientEmailAddress : clientName;
                    qEmail.template      = qTemplate.Id;

                    bc.email_queues.InsertOnSubmit(qEmail);
                    bc.SubmitChanges();
                    email_queueID        = qEmail.Id;
                }
            }

            // Create the privacy policy notice
            var privacayPolicyRecord = DebtPlus.LINQ.Factory.Manufacture_HPFPrivacyPolicy(clientRecord.Id);
            privacayPolicyRecord.email_queueID = email_queueID;

            // Find the client name and address information
            if (!string.IsNullOrEmpty(mailingAddress))
            {
                privacayPolicyRecord.address = (string.IsNullOrEmpty(clientName) ? "OCCUPANT" : clientName) + "\r\n" + mailingAddress;
            }
            bc.HPFPrivacyPolicies.InsertOnSubmit(privacayPolicyRecord);
        }

        /// <summary>
        /// The grid of values returned a CANCEL operation
        /// </summary>
        private void HPFCall_ResponseGrid1_Canceled(object sender, EventArgs e)
        {
            HPFCall_Search1.BringToFront();
        }

        /// <summary>
        /// The grid of values returned a selected item
        /// </summary>
        private void HPFCall_ResponseGrid1_SelectedItem(object sender, HPFCall_ResponseGrid.SelectedItemEventArgs e)
        {
            ProcessSearchResult(e.resp, WorkedWithAgency);
            return;
        }

        /// <summary>
        /// The search call KeyField issued a CANCEL operation.
        /// </summary>
        private void HPFCall_Search1_Canceled(object sender, EventArgs e)
        {
            HPFCase_Search1.BringToFront();
        }

        /// <summary>
        /// The search returned a set of values.
        /// </summary>
        private string fmt_phone(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            var rx = new System.Text.RegularExpressions.Regex(@"(\d{3})(\d{3})(\d{4})", System.Text.RegularExpressions.RegexOptions.Singleline);
            var m = rx.Match(input);
            if (m.Success)
            {
                return string.Format("({0}) {1}-{2}", m.Groups[1].Captures[0].Value, m.Groups[2].Captures[0].Value, m.Groups[3].Captures[0].Value);
            }

            rx = new System.Text.RegularExpressions.Regex(@"(\d{3})(\d{4})", System.Text.RegularExpressions.RegexOptions.Singleline);
            m = rx.Match(input);
            if (m.Success)
            {
                return string.Format("{0}-{1}", m.Groups[1].Captures[0].Value, m.Groups[2].Captures[0].Value);
            }

            return input;
        }

        /// <summary>
        /// The search returned a set of values.
        /// </summary>
        private void HPFCall_Search1_SearchResults(object sender, SearchResultArgs e)
        {
            WorkedWithAgency = e.WorkedWithOtherAgency;

            // If there are no items then tell the user and do nothing.
            if (e.colResults.Count == 0)
            {
                if (DebtPlus.Data.Forms.MessageBox.Show("The search result returned no cases.\r\n\r\nDo you wish to create a blank client?", "No Data", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                {
                    ProcessSearchResult(new DebtPlus.UI.Housing.HPF.Data.Log(), false);
                }
                return;
            }

            // If there is exactly one item then that is the one that we want.
            if (e.colResults.Count == 1)
            {
                var item = e.colResults[0];
                string personName = string.Format("{0} {1}", item.FirstName ?? string.Empty, item.LastName ?? string.Empty);

                // If there is no name then the name was bypassed and we just want to create the record.
                if (string.IsNullOrWhiteSpace(personName))
                {
                    ProcessSearchResult(e.colResults[0], e.WorkedWithOtherAgency);
                    return;
                }

                // Otherwise, ask about the single name entry.
                if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("A person was found.\r\n\r\nIs the person named \"{0}\"?\r\n\r\ntelephone number: {3}\r\nAddress: {1} {2}", new string[] { personName, item.PropStreetAddress ?? string.Empty, item.PropZipFull9 ?? string.Empty, fmt_phone(item.CustomerPhone) }), "Search Complete", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                {
                    ProcessSearchResult(e.colResults[0], e.WorkedWithOtherAgency);
                }
                return;
            }

            // There are more than one. Present the list.
            HPFCall_Search1.SendToBack();
            HPFCall_Search1.Visible = false;
            HPFCall_ResponseGrid1.gridControl1.DataSource = e.colResults;
            HPFCall_ResponseGrid1.Visible = true;
            HPFCall_ResponseGrid1.BringToFront();
        }
    }
}