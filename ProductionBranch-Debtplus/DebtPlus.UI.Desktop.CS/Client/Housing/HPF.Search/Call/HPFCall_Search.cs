﻿using System;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    public partial class HPFCall_Search : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Initialize the control
        /// </summary>
        public HPFCall_Search()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Event to designate that the search has returned a log entry list
        /// </summary>
        public event SearchResultsHandler SearchResults;

        /// <summary>
        /// Raise the SearchResults event
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseSearchResults(SearchResultArgs e)
        {
            var evt = SearchResults;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Process the completion of the search operation
        /// </summary>
        protected virtual void OnSearchResults(SearchResultArgs e)
        {
            RaiseSearchResults(e);
        }

        /// <summary>
        /// Indicate that the CANCEL button was pressed
        /// </summary>
        public event EventHandler SearchCancel;

        /// <summary>
        /// Raise the SearchCancel event
        /// </summary>
        protected void RaiseSearchCancel(EventArgs e)
        {
            var evt = SearchCancel;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Process the cancel of the search operation
        /// </summary>
        protected virtual void OnSearchCancel(EventArgs e)
        {
            RaiseSearchCancel(e);
        }

        /// <summary>
        /// Register for the events
        /// </summary>
        private void RegisterHandlers()
        {
            Load                               += SearchCall_Load;
            Resize                             += SearchCall_Resize;
            checkEdit_Bypass.EditValueChanged  += form_Changed;
            textEdit_ICTID.EditValueChanged    += form_Changed;
            simpleButton_Search.Click          += simpleButton_Search_Click;
            simpleButton_Cancel.Click          += simpleButton_Cancel_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                               -= SearchCall_Load;
            Resize                             -= SearchCall_Resize;
            checkEdit_Bypass.EditValueChanged  -= form_Changed;
            textEdit_ICTID.EditValueChanged    -= form_Changed;
            simpleButton_Search.Click          -= simpleButton_Search_Click;
            simpleButton_Cancel.Click          -= simpleButton_Cancel_Click;
        }

        /// <summary>
        /// Process the Control's LOAD event
        /// </summary>
        private void SearchCall_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                simpleButton_Search.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the resize event for the control to center the buttons
        /// </summary>
        private void SearchCall_Resize(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) / 2;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Indicate that the CANCEL button was pressed
        /// </summary>
        private void simpleButton_Cancel_Click(object sender, EventArgs e)
        {
            OnSearchCancel(e);
        }

        /// <summary>
        /// Determine if there is sufficient information to do the search operation
        /// </summary>
        private bool HasErrors()
        {
            // If the bypass is checked then we simply have no errors
            if (checkEdit_Bypass.Checked)
            {
                return false;
            }

            // If there is an ID then there is no error.
            return string.IsNullOrWhiteSpace(DebtPlus.Utils.Nulls.v_String(textEdit_ICTID.Text));
        }

        /// <summary>
        /// Handle a change in the bypass checkbox field
        /// </summary>
        private void form_Changed(object sender, EventArgs e)
        {
            simpleButton_Search.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle the CLICK event on the SEARCH button
        /// </summary>
        private void simpleButton_Search_Click(object sender, EventArgs e)
        {
            // If the bypass is selected then just skip the search operation.
            if (checkEdit_Bypass.Checked)
            {
                PerformBypass();
                return;
            }

            // If there is an ID then do the fetch operation
            var ITCID = textEdit_ICTID.Text.Trim();
            if (! string.IsNullOrEmpty(ITCID))
            {
                PerformFetch(ITCID);
            }
        }

        /// <summary>
        /// Bypass the search and just create the client with a blank set of data
        /// </summary>
        private void PerformBypass()
        {
            // Generate an empty result structure
            var logEntry = new DebtPlus.UI.Housing.HPF.Data.Log();
            var colResults = new System.Collections.Generic.List<DebtPlus.UI.Housing.HPF.Data.Log>();

            // Pass along the one entry so that it is used.
            colResults.Add(logEntry);
            OnSearchResults(new SearchResultArgs(colResults, false));
            return;
        }

        /// <summary>
        /// Process the FETCH operation on the indicated ITC ID Number
        /// </summary>
        private void PerformFetch(string ITCID)
        {
            using (var soapClass = new DebtPlus.SOAP.HPF.Communication())
            {
                var reqOp = new DebtPlus.SOAP.HPF.Agency.CallLogRetrieveRequest()
                {
                    ICTCallId = ITCID
                };

                var resp = soapClass.RetrieveCallLog(reqOp);
                if (resp.Status == DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success)
                {
                    var resultsList = new System.Collections.Generic.List<DebtPlus.UI.Housing.HPF.Data.Log>();
                    foreach (var logResp in resp.CallLogs)
                    {
                        resultsList.Add(new UI.Housing.HPF.Data.Log()
                        {
                            AuthorizedInd              = logResp.AuthorizedInd,
                            CallSourceCd               = logResp.CallSourceCd,
                            City                       = logResp.City,
                            CustomerPhone              = logResp.CustomerPhone,
                            DelinqInd                  = logResp.DelinqInd,
                            DocFulfillmentCd           = logResp.DocFulfillmentCd,
                            DTIInd                     = logResp.DTIInd,
                            Email                      = logResp.Email,
                            EndDate                    = logResp.EndDate,
                            FcId                       = logResp.FcId,
                            FCSaleDate                 = logResp.FCSaleDate,
                            FinalDispoCd               = logResp.FinalDispoCd,
                            FinalDispoDesc             = logResp.FinalDispoDesc,
                            FirstName                  = logResp.FirstName,
                            FnmaLookupCd               = logResp.FnmaLookupCd,
                            FnmApptSetInd              = logResp.FnmApptSetInd,
                            FnmLookupInd               = logResp.FnmLookupInd,
                            FnmRegionalOfficeCd        = logResp.FnmRegionalOfficeCd,
                            GrossIncomeAmount          = logResp.GrossIncomeAmount.HasValue ? new decimal?((decimal) logResp.GrossIncomeAmount.Value) : (decimal?) null,
                            HomeownerInd               = logResp.HomeownerInd,
                            HopeNetCallId              = logResp.HopeNetCallId,
                            ICTCallId                  = logResp.ICTCallId,
                            InvestorRentalInd          = logResp.InvestorRentalInd,
                            LastName                   = logResp.LastName,
                            LoanAccountNumber          = logResp.LoanAccountNumber,
                            LoanDelinqStatusCd         = logResp.LoanDelinqStatusCd,
                            LoanLookupCd               = logResp.LoanLookupCd,
                            LoanModInterestRate        = logResp.LoanModInterestRate,
                            LoanModTerm                = logResp.LoanModTerm,
                            LoanTrialModCd             = logResp.LoanTrialModCd,
                            MailingAddr1               = logResp.MailingAddr1,
                            MailingAddr2               = logResp.MailingAddr2,
                            MailingCity                = logResp.MailingCity,
                            MailingStateCd             = logResp.MailingStateCd,
                            MailingZip                 = logResp.MailingZip,
                            MaxLoanAmountInd           = logResp.MaxLoanAmountInd,
                            MHAEligibilityCd           = logResp.MHAEligibilityCd,
                            MHAEligibilityDesc         = logResp.MHAEligibilityDesc,
                            MHAIneligibilityReasonCd   = logResp.MHAIneligibilityReasonCd,
                            MHAIneligibilityReasonDesc = logResp.MHAIneligibilityReasonDesc,
                            MHAInfoShareInd            = logResp.MHAInfoShareInd,
                            MilActiveInd               = logResp.MilActiveInd,
                            MotherMaidenLastName       = logResp.MotherMaidenLastName,
                            NonprofitReferralDesc1     = logResp.NonprofitReferralDesc1,
                            NonprofitReferralDesc2     = logResp.NonprofitReferralDesc2,
                            NonprofitReferralDesc3     = logResp.NonprofitReferralDesc3,
                            NonprofitReferralKeyNum1   = logResp.NonprofitReferralKeyNum1,
                            NonprofitReferralKeyNum2   = logResp.NonprofitReferralKeyNum2,
                            NonprofitReferralKeyNum3   = logResp.NonprofitReferralKeyNum3,
                            OptOutReasonCd             = logResp.OptOutReasonCd,
                            OriginatedPrior2009Ind     = logResp.OriginatedPrior2009Ind,
                            OtherServicerName          = logResp.OtherServicerName,
                            PaymentAmount              = logResp.PaymentAmount.HasValue ? new decimal?((decimal) logResp.PaymentAmount.Value) : (decimal?) null,
                            PowerOfAttorneyInd         = logResp.PowerOfAttorneyInd,
                            PreviousUpInd              = logResp.PreviousUpInd,
                            PrimaryResidenceInd        = logResp.PrimaryResidenceInd,
                            ProgramId                  = logResp.ProgramId,
                            PropStreetAddress          = logResp.PropStreetAddress,
                            PropZipFull9               = logResp.PropZipFull9,
                            ReasonForCall              = logResp.ReasonForCall,
                            RepeatDiscInd              = logResp.RepeatDiscInd,
                            ServicerCAId               = logResp.ServicerCAId,
                            ServicerCALastContactDate  = logResp.ServicerCALastContactDate,
                            ServicerCAName             = logResp.ServicerCAName,
                            ServicerCANumber           = logResp.ServicerCANumber,
                            ServicerCAOtherName        = logResp.ServicerCAOtherName,
                            ServicerId                 = logResp.ServicerId,
                            ServicerInProcessCd        = logResp.ServicerInProcessCd,
                            ServicerName               = logResp.ServicerName,
                            SpocInd                    = logResp.SpocInd,
                            SponsorId                  = logResp.SponsorId,
                            SponsorLoanNum             = logResp.SponsorLoanNum,
                            SrvcTrnsfrInd              = logResp.SrvcTrnsfrInd,
                            StartDate                  = logResp.StartDate,
                            State                      = logResp.State,
                            StepUpReasonCd             = logResp.StepUpReasonCd,
                            ThirdPartyContactCd        = logResp.ThirdPartyContactCd,
                            TNCountyCd                 = logResp.TNCountyCd,
                            UnemployedInd              = logResp.UnemployedInd,
                            UpBenefitsInd              = logResp.UpBenefitsInd
                        });
                    }
                    OnSearchResults(new SearchResultArgs(resultsList, false));
                    return;
                }

                // Collect the information for the error status
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                foreach (var msg in resp.Messages)
                {
                    sb.Append("\r\n\r\n");
                    sb.Append(msg.Message);
                }

                if (sb.Length > 0)
                {
                    sb.Remove(0, 4);
                }

                if (resp.Status == DebtPlus.SOAP.HPF.Agency.ResponseStatus.AuthenticationFail)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(sb.ToString(), "HPF Authentication Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    DebtPlus.Data.Forms.MessageBox.Show(sb.ToString(), resp.Status == DebtPlus.SOAP.HPF.Agency.ResponseStatus.Fail ? "HPF Authentication Error" : "HPF Authentication Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }

    /// <summary>
    /// Search Event Completion arguments
    /// </summary>
    public class SearchResultArgs : System.EventArgs
    {
        public System.Collections.Generic.List<DebtPlus.UI.Housing.HPF.Data.Log> colResults { get; private set; }

        internal SearchResultArgs(System.Collections.Generic.List<DebtPlus.UI.Housing.HPF.Data.Log> colResults, bool WorkedWithOtherAgency)
        {
            this.colResults = colResults;
            this.WorkedWithOtherAgency = WorkedWithOtherAgency;
        }

        public bool WorkedWithOtherAgency { get; set; }
    }

    /// <summary>
    /// Delegate to the search operation completion
    /// </summary>
    public delegate void SearchResultsHandler(object sender, SearchResultArgs e);
}