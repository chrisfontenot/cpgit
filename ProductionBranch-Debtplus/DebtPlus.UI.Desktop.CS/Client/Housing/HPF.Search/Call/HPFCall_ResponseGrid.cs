﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    public partial class HPFCall_ResponseGrid : DevExpress.XtraEditors.XtraUserControl
    {
        public class SelectedItemEventArgs : System.EventArgs
        {
            public SelectedItemEventArgs()
                : base()
            {
                WorkdWithOtherAgency = false;
            }

            public DebtPlus.UI.Housing.HPF.Data.Log resp { get; private set; }

            public SelectedItemEventArgs(DebtPlus.UI.Housing.HPF.Data.Log resp)
            {
                this.resp = resp;
            }

            public bool WorkdWithOtherAgency { get; set; }
        }

        public delegate void SelectedItemEventHandler(object sender, SelectedItemEventArgs e);

        public event SelectedItemEventHandler SelectedItem;

        protected void RaiseSelectedItem(SelectedItemEventArgs e)
        {
            var evt = SelectedItem;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnSelectedItem(SelectedItemEventArgs e)
        {
            RaiseSelectedItem(e);
        }

        /// <summary>
        /// Indicate that the CANCEL button was pressed
        /// </summary>
        public event EventHandler Canceled;

        /// <summary>
        /// Raise the Canceled event
        /// </summary>
        protected void RaiseCanceled(EventArgs e)
        {
            var evt = Canceled;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Process the cancel of the search operation
        /// </summary>
        protected virtual void OnCanceled(EventArgs e)
        {
            RaiseCanceled(e);
        }

        public HPFCall_ResponseGrid() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            gridView1.DoubleClick += gridView1_DoubleClick;
            gridView1.FocusedRowChanged += gridView1_FocusedRowChanged;
            gridView1.MouseDown += gridView1_MouseDown;
            simpleButton_OK.Click += simpleButton_OK_Click;
            simpleButton_Cancel.Click += simpleButton_Cancel_Click;
        }

        private void UnRegisterHandlers()
        {
            gridView1.DoubleClick -= gridView1_DoubleClick;
            gridView1.FocusedRowChanged -= gridView1_FocusedRowChanged;
            gridView1.MouseDown -= gridView1_MouseDown;
            simpleButton_OK.Click -= simpleButton_OK_Click;
            simpleButton_Cancel.Click -= simpleButton_Cancel_Click;
        }

        private Int32 controlRow = -1;

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            controlRow = e.FocusedRowHandle;

            // Find the row in the KeyField tables.
            if (controlRow >= 0)
            {
                DebtPlus.UI.Housing.HPF.Data.Log row = gridView1.GetRow(controlRow) as DebtPlus.UI.Housing.HPF.Data.Log;
                if (row != null)
                {
                    simpleButton_OK.Enabled = true;
                    return;
                }
            }
            simpleButton_OK.Enabled = false;
        }

        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            var hi = gridView1.CalcHitInfo(new Point(e.X, e.Y));
            if (hi.IsValid && hi.InRow)
            {
                controlRow = hi.RowHandle;
                if (controlRow >= 0)
                {
                    DebtPlus.UI.Housing.HPF.Data.Log row = gridView1.GetRow(controlRow) as DebtPlus.UI.Housing.HPF.Data.Log;
                    if (row != null)
                    {
                        simpleButton_OK.Enabled = true;
                        return;
                    }
                }
            }
            simpleButton_OK.Enabled = false;
        }

        private void simpleButton_Cancel_Click(object sender, EventArgs e)
        {
            OnCanceled(e);
        }

        private void simpleButton_OK_Click(object sender, EventArgs e)
        {
            if (controlRow >= 0)
            {
                var selectedRow = gridView1.GetRow(controlRow) as DebtPlus.UI.Housing.HPF.Data.Log;
                if (selectedRow != null)
                {
                    OnSelectedItem(new SelectedItemEventArgs(selectedRow));
                }
            }
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            if (hi.IsValid)
            {
                System.Int32 rowHandle = hi.RowHandle;

                // Find the record to be edited from the list
                if (rowHandle >= 0)
                {
                    var selectedRow = gridView1.GetRow(rowHandle) as DebtPlus.UI.Housing.HPF.Data.Log;
                    if (selectedRow != null)
                    {
                        OnSelectedItem(new SelectedItemEventArgs(selectedRow));
                    }
                }
            }
        }
    }
}