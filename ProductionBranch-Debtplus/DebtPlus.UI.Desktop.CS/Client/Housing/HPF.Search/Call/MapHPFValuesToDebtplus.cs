﻿using System;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class MainForm
    {
        private class MapHPFValuesToDebtplus : System.IDisposable
        {
            private System.Collections.Generic.List<DebtPlus.LINQ.HPFCodeType> colMappingRecords = null;

            /// <summary>
            /// Obtain the list of records to map items
            /// </summary>
            private System.Collections.Generic.List<DebtPlus.LINQ.HPFCodeType> mappingRecords
            {
                get
                {
                    if (colMappingRecords == null)
                    {
                        using (var bc = new DebtPlus.LINQ.BusinessContext())
                        {
                            colMappingRecords = bc.HPFCodeTypes.ToList();
                        }
                    }
                    return colMappingRecords;
                }
                set
                {
                    colMappingRecords = value;
                }
            }

            /// <summary>
            /// Map the servicer ID from HPF to a value suitable for DebtPlus
            /// </summary>
            /// <param name="inputServicer"></param>
            /// <returns></returns>
            public Int32? loanServicerID(Int32? inputServicer)
            {
                var col = DebtPlus.LINQ.Cache.Housing_lender_servicer.getList();
                DebtPlus.LINQ.Housing_lender_servicer q = null;
                if (inputServicer.HasValue)
                {
                    q = col.Find(s => s.ServiceID == inputServicer);
                }

                if (q == null)
                {
                    q = col.Find(s => s.@Default);
                }

                if (q != null)
                {
                    return q.Id;
                }
                return null;
            }

            /// <summary>
            /// Map the HPF state value to a suitable DebtPlus value. We can not guarantee that the map is 1-to-1, but it is close.
            /// </summary>
            /// <param name="stateCode"></param>
            /// <returns></returns>
            public Int32 addressState(string stateCode)
            {
                var q = DebtPlus.LINQ.Cache.state.getList().Find(s => string.Compare(s.MailingCode, stateCode, true) == 0 && s.Country == 1);
                if (q != null)
                {
                    return q.Id;
                }
                return 0;
            }

            public void Dispose()
            {
                if (colMappingRecords != null)
                {
                    colMappingRecords.Clear();
                    colMappingRecords = null;
                }
                GC.SuppressFinalize(this);
            }

            public Int32 loanDelinquency(string delinquencyCode)
            {
                if (string.Compare("STRUG", delinquencyCode, false) == 0)
                {
                    return -1;      // magic value for "current but struggling"
                }

                if (string.Compare("30-59", delinquencyCode, false) == 0)
                {
                    return 1;       // 1 month
                }
                else if (string.Compare("60-89", delinquencyCode, false) == 0)
                {
                    return 2;       // 2 months
                }
                else if (string.Compare("90-119", delinquencyCode, false) == 0)
                {
                    return 3;       // 3 months
                }

                // Assume all other values are "CUR" which is current. They may or may not be but we can't handle
                // any other strange setting at this point.
                return 0;           // current
            }
        }
    }
}
