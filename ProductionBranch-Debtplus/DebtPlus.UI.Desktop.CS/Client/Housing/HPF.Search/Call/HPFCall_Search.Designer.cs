﻿namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class HPFCall_Search
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.checkEdit_Bypass = new DevExpress.XtraEditors.CheckEdit();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit_ICTID = new DevExpress.XtraEditors.TextEdit();
            this.simpleButton_Search = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Bypass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICTID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.checkEdit_Bypass);
            this.layoutControl1.Controls.Add(this.simpleButton_Cancel);
            this.layoutControl1.Controls.Add(this.textEdit_ICTID);
            this.layoutControl1.Controls.Add(this.simpleButton_Search);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(347, 336);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // checkEdit_Bypass
            // 
            this.checkEdit_Bypass.Location = new System.Drawing.Point(24, 110);
            this.checkEdit_Bypass.Name = "checkEdit_Bypass";
            this.checkEdit_Bypass.Properties.Caption = "Skip the search and just create the client";
            this.checkEdit_Bypass.Size = new System.Drawing.Size(299, 19);
            this.checkEdit_Bypass.StyleController = this.layoutControl1;
            this.checkEdit_Bypass.TabIndex = 12;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(126, 298);
            this.simpleButton_Cancel.MaximumSize = new System.Drawing.Size(75, 26);
            this.simpleButton_Cancel.MinimumSize = new System.Drawing.Size(75, 26);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 26);
            this.simpleButton_Cancel.StyleController = this.layoutControl1;
            this.simpleButton_Cancel.TabIndex = 9;
            this.simpleButton_Cancel.Text = "&Cancel";
            // 
            // textEdit_ICTID
            // 
            this.textEdit_ICTID.Location = new System.Drawing.Point(57, 43);
            this.textEdit_ICTID.Name = "textEdit_ICTID";
            this.textEdit_ICTID.Properties.Appearance.Options.UseTextOptions = true;
            this.textEdit_ICTID.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.textEdit_ICTID.Properties.Mask.BeepOnError = true;
            this.textEdit_ICTID.Properties.Mask.EditMask = "\\d{5,10}";
            this.textEdit_ICTID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_ICTID.Properties.MaxLength = 10;
            this.textEdit_ICTID.Size = new System.Drawing.Size(266, 20);
            this.textEdit_ICTID.StyleController = this.layoutControl1;
            this.textEdit_ICTID.TabIndex = 7;
            // 
            // simpleButton_Search
            // 
            this.simpleButton_Search.Location = new System.Drawing.Point(47, 298);
            this.simpleButton_Search.MaximumSize = new System.Drawing.Size(75, 26);
            this.simpleButton_Search.MinimumSize = new System.Drawing.Size(75, 26);
            this.simpleButton_Search.Name = "simpleButton_Search";
            this.simpleButton_Search.Size = new System.Drawing.Size(75, 26);
            this.simpleButton_Search.StyleController = this.layoutControl1;
            this.simpleButton_Search.TabIndex = 8;
            this.simpleButton_Search.Text = "&OK";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlGroup2,
            this.emptySpaceItem1,
            this.emptySpaceItem_Left,
            this.emptySpaceItem_Right,
            this.layoutControlGroup4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(347, 336);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.simpleButton_Cancel;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(114, 286);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(79, 30);
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.simpleButton_Search;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(35, 286);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(79, 30);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AppearanceGroup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.layoutControlGroup2.AppearanceGroup.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.layoutControlGroup2.AppearanceGroup.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.layoutControlGroup2.AppearanceGroup.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.layoutControlGroup2.AppearanceGroup.Options.UseBackColor = true;
            this.layoutControlGroup2.AppearanceGroup.Options.UseBorderColor = true;
            this.layoutControlGroup2.CustomizationFormText = "Fetch by ID";
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(327, 67);
            this.layoutControlGroup2.Text = "Fetch by ID";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit_ICTID;
            this.layoutControlItem5.CustomizationFormText = "ICT Id";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(303, 24);
            this.layoutControlItem5.Text = "ICT Id";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(30, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 133);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(327, 153);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.AllowHotTrack = false;
            this.emptySpaceItem_Left.CustomizationFormText = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Location = new System.Drawing.Point(0, 286);
            this.emptySpaceItem_Left.Name = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(35, 30);
            this.emptySpaceItem_Left.Text = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.AllowHotTrack = false;
            this.emptySpaceItem_Right.CustomizationFormText = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(193, 286);
            this.emptySpaceItem_Right.Name = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(134, 30);
            this.emptySpaceItem_Right.Text = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Options";
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 67);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(327, 66);
            this.layoutControlGroup4.Text = "Options";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.checkEdit_Bypass;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(303, 23);
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // HPFCall_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "HPFCall_Search";
            this.Size = new System.Drawing.Size(347, 336);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_Bypass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_ICTID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Search;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.CheckEdit checkEdit_Bypass;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        private DevExpress.XtraEditors.TextEdit textEdit_ICTID;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Left;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Right;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
    }
}
