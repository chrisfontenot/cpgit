﻿namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class HPFCall_ResponseGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_ICTCallID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_FCID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_LastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_FirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_LoanNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_TelephoneNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(433, 244);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_ICTCallID,
            this.gridColumn_FCID,
            this.gridColumn_LastName,
            this.gridColumn_FirstName,
            this.gridColumn_LoanNumber,
            this.gridColumn_TelephoneNumber});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_ICTCallID, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn_ICTCallID
            // 
            this.gridColumn_ICTCallID.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_ICTCallID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_ICTCallID.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_ICTCallID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_ICTCallID.Caption = "ICT Call ID";
            this.gridColumn_ICTCallID.CustomizationCaption = "ICT Call ID";
            this.gridColumn_ICTCallID.FieldName = "ICTCallId";
            this.gridColumn_ICTCallID.Name = "gridColumn_ICTCallID";
            this.gridColumn_ICTCallID.OptionsColumn.AllowEdit = false;
            this.gridColumn_ICTCallID.Visible = true;
            this.gridColumn_ICTCallID.VisibleIndex = 0;
            this.gridColumn_ICTCallID.Width = 68;
            // 
            // gridColumn_FCID
            // 
            this.gridColumn_FCID.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_FCID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_FCID.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_FCID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_FCID.Caption = "FC ID";
            this.gridColumn_FCID.FieldName = "FcId";
            this.gridColumn_FCID.Name = "gridColumn_FCID";
            this.gridColumn_FCID.Visible = true;
            this.gridColumn_FCID.VisibleIndex = 1;
            this.gridColumn_FCID.Width = 63;
            // 
            // gridColumn_LastName
            // 
            this.gridColumn_LastName.Caption = "Last Name";
            this.gridColumn_LastName.FieldName = "LastName";
            this.gridColumn_LastName.Name = "gridColumn_LastName";
            this.gridColumn_LastName.Visible = true;
            this.gridColumn_LastName.VisibleIndex = 2;
            this.gridColumn_LastName.Width = 300;
            // 
            // gridColumn_FirstName
            // 
            this.gridColumn_FirstName.Caption = "First Name";
            this.gridColumn_FirstName.FieldName = "FirstName";
            this.gridColumn_FirstName.Name = "gridColumn_FirstName";
            this.gridColumn_FirstName.Visible = true;
            this.gridColumn_FirstName.VisibleIndex = 3;
            this.gridColumn_FirstName.Width = 86;
            // 
            // gridColumn_LoanNumber
            // 
            this.gridColumn_LoanNumber.Caption = "Loan #";
            this.gridColumn_LoanNumber.FieldName = "LoanAccountNumber";
            this.gridColumn_LoanNumber.Name = "gridColumn_LoanNumber";
            this.gridColumn_LoanNumber.Visible = true;
            this.gridColumn_LoanNumber.VisibleIndex = 4;
            this.gridColumn_LoanNumber.Width = 86;
            // 
            // gridColumn_TelephoneNumber
            // 
            this.gridColumn_TelephoneNumber.Caption = "Phone #";
            this.gridColumn_TelephoneNumber.FieldName = "CustomerPhone";
            this.gridColumn_TelephoneNumber.Name = "gridColumn_TelephoneNumber";
            this.gridColumn_TelephoneNumber.Visible = true;
            this.gridColumn_TelephoneNumber.VisibleIndex = 5;
            this.gridColumn_TelephoneNumber.Width = 91;
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_OK.Location = new System.Drawing.Point(138, 265);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(75, 26);
            this.simpleButton_OK.TabIndex = 1;
            this.simpleButton_OK.Text = "&Select";
            this.simpleButton_OK.Enabled = false;
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_Cancel.Location = new System.Drawing.Point(219, 265);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 26);
            this.simpleButton_Cancel.TabIndex = 2;
            this.simpleButton_Cancel.Text = "&Cancel";
            // 
            // ResponseGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.simpleButton_Cancel);
            this.Controls.Add(this.simpleButton_OK);
            this.Controls.Add(this.gridControl1);
            this.Name = "ResponseGrid";
            this.Size = new System.Drawing.Size(433, 311);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        protected internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_ICTCallID;
        protected internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_FCID;
        protected internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_LastName;
        protected internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_FirstName;
        protected internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_LoanNumber;
        protected internal DevExpress.XtraGrid.Columns.GridColumn gridColumn_TelephoneNumber;
        protected internal DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        protected internal DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        protected internal DevExpress.XtraGrid.GridControl gridControl1;
    }
}
