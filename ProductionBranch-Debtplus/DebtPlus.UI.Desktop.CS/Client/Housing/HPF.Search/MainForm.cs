﻿using System;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    public partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Initialize the new form
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers for the form
        /// </summary>
        private void RegisterHandlers()
        {
            Load                               += MainForm_Load;
            HPFCase_Search1.Cancel             += HPFCase_Search1_Cancel;
            HPFCase_Search1.PerformSearch      += HPFCase_Search1_PerformSearch;
            HPFCase_ResponseGrid1.Back         += HPFCase_ResponseGrid1_Back;
            HPFCase_ResponseGrid1.Finish       += HPFCase_ResponseGrid1_Finish;
            HPFCall_Search1.SearchResults      += HPFCall_Search1_SearchResults;
            HPFCall_Search1.SearchCancel       += HPFCall_Search1_Canceled;
            HPFCall_ResponseGrid1.Canceled     += HPFCall_ResponseGrid1_Canceled;
            HPFCall_ResponseGrid1.SelectedItem += HPFCall_ResponseGrid1_SelectedItem;
        }

        /// <summary>
        /// Remove the event handler registration on the form
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                               -= MainForm_Load;
            HPFCase_Search1.Cancel             -= HPFCase_Search1_Cancel;
            HPFCase_Search1.PerformSearch      -= HPFCase_Search1_PerformSearch;
            HPFCase_ResponseGrid1.Back         -= HPFCase_ResponseGrid1_Back;
            HPFCase_ResponseGrid1.Finish       -= HPFCase_ResponseGrid1_Finish;
            HPFCall_Search1.SearchResults      -= HPFCall_Search1_SearchResults;
            HPFCall_Search1.SearchCancel       -= HPFCall_Search1_Canceled;
            HPFCall_ResponseGrid1.Canceled     -= HPFCall_ResponseGrid1_Canceled;
            HPFCall_ResponseGrid1.SelectedItem -= HPFCall_ResponseGrid1_SelectedItem;
        }

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Enable the request panel form
                HPFCase_Search1.BringToFront();
                HPFCase_Search1.Enabled = true;
                HPFCase_Search1.Focus();
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }

    public class Mainline : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        [STAThread]
        public void OldAppMain(string[] Arguments)
        {
            var th = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(AppThread));
            th.SetApartmentState(System.Threading.ApartmentState.STA);
            th.IsBackground = true;
            th.Start(Arguments);
        }

        private void AppThread(object obj)
        {
            using (var frm = new MainForm())
            {
                frm.ShowDialog();
            }
        }
    }
}