﻿using System;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    public partial class DetailsForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private DebtPlus.SOAP.HPF.Agency.ForeclosureCaseWSDTO resp = null;

        public DetailsForm()
        {
            InitializeComponent();
        }

        public DetailsForm(DebtPlus.SOAP.HPF.Agency.ForeclosureCaseWSDTO resp)
            : this()
        {
            this.resp = resp;
            Load += new EventHandler(DetailsForm_Load);
            Resize += new EventHandler(DetailsForm_Resize);
        }

        private void DetailsForm_Resize(object sender, EventArgs e)
        {
            emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) / 2;
        }

        private void DetailsForm_Load(object sender, EventArgs e)
        {
            // Define the fields from the record
            if (resp != null)
            {
                labelControl_address.Text     = resp.address           ?? string.Empty;
                labelControl_agency.Text      = resp.AgencyName        ?? string.Empty;
                labelControl_counselor.Text   = resp.DISPLAY_counselor ?? string.Empty;
                labelControl_fcid.Text        = resp.FcId.HasValue ? resp.FcId.Value.ToString() : string.Empty;
                labelControl_loan_number.Text = resp.LoanNumber        ?? string.Empty;
                labelControl_name.Text        = resp.DISPLAY_name      ?? string.Empty;
                labelControl_record_date.Text = resp.DISPLAY_case_date.HasValue ? resp.DISPLAY_case_date.Value.ToShortDateString() : string.Empty;
                labelControl_svc_number.Text  = resp.AgencyCaseNum     ?? string.Empty;
            }
        }
    }
}
