﻿using System;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    public partial class HPFCase_Search : DevExpress.XtraEditors.XtraUserControl
    {
        #region Search Button

        public class PerformSearchArgs : System.EventArgs
        {
            public PerformSearchArgs()
                : base()
            {
            }

            public PerformSearchArgs(DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchCriteriaDTO req) : base()
            {
                this.req = req;
            }

            /// <summary>
            /// Search request parameters
            /// </summary>
            public DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchCriteriaDTO req { get; private set; }
        }

        /// <summary>
        /// Delegate for the search request
        /// </summary>
        public delegate void PerformSearchHandler(object sender, PerformSearchArgs args);

        /// <summary>
        /// Event raised when the search is to be performed
        /// </summary>
        public event PerformSearchHandler PerformSearch;

        /// <summary>
        /// Do the search operation when it is indicated
        /// </summary>
        protected void RaisePerformSearch(PerformSearchArgs e)
        {
            var evt = PerformSearch;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Do the search operation when it is indicated
        /// </summary>
        protected virtual void OnPerformSearch(PerformSearchArgs e)
        {
            RaisePerformSearch(e);
        }

        #endregion Search Button

        #region Cancel Button

        /// <summary>
        /// Event raised when the Cancel is to be performed
        /// </summary>
        public event EventHandler Cancel;

        /// <summary>
        /// Do the Cancel operation when it is indicated
        /// </summary>
        protected void RaiseCancel(EventArgs e)
        {
            var evt = Cancel;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Do the Cancel operation when it is indicated
        /// </summary>
        protected virtual void OnCancel(EventArgs e)
        {
            RaiseCancel(e);
        }

        /// <summary>
        /// Handle the CLICK event on the CANCEL button
        /// </summary>
        private void simpleButton_Cancel_Click(object sender, EventArgs e)
        {
            OnCancel(e);
        }

        #endregion Cancel Button

        /// <summary>
        /// Initialize the new class structure
        /// </summary>
        public HPFCase_Search() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            layoutControl1.Resize += layoutControl1_Resize;
            Load += XtraUserControl_Request_Load;
            simpleButton_Cancel.Click += simpleButton_Cancel_Click;
            simpleButton_Search.Click += simpleButton_Search_Click;

            textEdit_FirstName.TextChanged += FormChanged;
            textEdit_Last4SSN.TextChanged += FormChanged;
            textEdit_LastName.TextChanged += FormChanged;
            textEdit_LoanNumber.TextChanged += FormChanged;
            textEdit_PropertyZIP.TextChanged += FormChanged;
            textEdit_SVC.TextChanged += FormChanged;

            textEdit_FirstName.KeyPress += TextEdit_KeyPress;
            textEdit_Last4SSN.KeyPress += TextEdit_KeyPress;
            textEdit_LastName.KeyPress += TextEdit_KeyPress;
            textEdit_LoanNumber.KeyPress += TextEdit_KeyPress;
            textEdit_PropertyZIP.KeyPress += TextEdit_KeyPress;
            textEdit_SVC.KeyPress += TextEdit_KeyPress;

            textEdit_FirstName.Validated += TextEdit_Validated;
            textEdit_Last4SSN.Validated += TextEdit_Validated;
            textEdit_LastName.Validated += TextEdit_Validated;
            textEdit_LoanNumber.Validated += TextEdit_Validated;
            textEdit_PropertyZIP.Validated += TextEdit_Validated;
            textEdit_SVC.Validated += TextEdit_Validated;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            layoutControl1.Resize -= layoutControl1_Resize;
            Load -= XtraUserControl_Request_Load;
            simpleButton_Cancel.Click -= simpleButton_Cancel_Click;
            simpleButton_Search.Click -= simpleButton_Search_Click;

            textEdit_FirstName.TextChanged -= FormChanged;
            textEdit_Last4SSN.TextChanged -= FormChanged;
            textEdit_LastName.TextChanged -= FormChanged;
            textEdit_LoanNumber.TextChanged -= FormChanged;
            textEdit_PropertyZIP.TextChanged -= FormChanged;
            textEdit_SVC.TextChanged -= FormChanged;

            textEdit_FirstName.KeyPress -= TextEdit_KeyPress;
            textEdit_Last4SSN.KeyPress -= TextEdit_KeyPress;
            textEdit_LastName.KeyPress -= TextEdit_KeyPress;
            textEdit_LoanNumber.KeyPress -= TextEdit_KeyPress;
            textEdit_PropertyZIP.KeyPress -= TextEdit_KeyPress;
            textEdit_SVC.KeyPress -= TextEdit_KeyPress;

            textEdit_FirstName.Validated -= TextEdit_Validated;
            textEdit_Last4SSN.Validated -= TextEdit_Validated;
            textEdit_LastName.Validated -= TextEdit_Validated;
            textEdit_LoanNumber.Validated -= TextEdit_Validated;
            textEdit_PropertyZIP.Validated -= TextEdit_Validated;
            textEdit_SVC.Validated -= TextEdit_Validated;
        }

        /// <summary>
        /// Process a change in the form controls
        /// </summary>
        private void FormChanged(object sender, EventArgs e)
        {
            simpleButton_Search.Enabled = !HasErrors();
        }

        /// <summary>
        /// Process a keypress into a field
        /// </summary>
        private void TextEdit_KeyPress(object sender, KeyPressEventArgs e)
        {
            DevExpress.XtraEditors.TextEdit ctl = sender as DevExpress.XtraEditors.TextEdit;
            if (ctl != null)
            {
                // Ignore leading blanks in the field
                if (ctl.Text == string.Empty && e.KeyChar == ' ')
                {
                    e.Handled = true;
                }
            }
        }

        /// <summary>
        /// Process the field validated event
        /// </summary>
        private void TextEdit_Validated(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                DevExpress.XtraEditors.TextEdit ctl = sender as DevExpress.XtraEditors.TextEdit;
                if (ctl != null)
                {
                    // Toss the extra spaces from the field
                    ctl.Text = ctl.Text.Trim();

                    // If the field is empty then replace it with NULL
                    if (ctl.Text == string.Empty)
                    {
                        ctl.EditValue = null;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Determine if the KeyField form is valid for submission
        /// </summary>
        private bool HasErrors()
        {
            // Find if any of the fields have data that we can use in the search
            // all of the fields are optional but one must be specified.
            if (isFirstNameValid()) return false;
            if (isLast4SSNValid()) return false;
            if (isLoanNumberValid()) return false;
            if (isSVCValid()) return false;
            if (isLast4SSNValid()) return false;
            if (isPropertyZIPValid()) return false;

            // None of the fields are valid. This is the error condition.
            return true;
        }

        /// <summary>
        /// Is the first name field valid?
        /// </summary>
        private bool isFirstNameValid()
        {
            return !string.IsNullOrWhiteSpace(textEdit_FirstName.Text);
        }

        /// <summary>
        /// Is the first name field valid?
        /// </summary>
        private bool isLastNameValid()
        {
            return !string.IsNullOrWhiteSpace(textEdit_LastName.Text);
        }

        /// <summary>
        /// Is the first name field valid?
        /// </summary>
        private bool isLoanNumberValid()
        {
            return !string.IsNullOrWhiteSpace(textEdit_LoanNumber.Text);
        }

        /// <summary>
        /// Is the first name SVC valid?
        /// </summary>
        private bool isSVCValid()
        {
            return !string.IsNullOrWhiteSpace(textEdit_SVC.Text);
        }

        /// <summary>
        /// Is the Last4SSN valid?
        /// </summary>
        private bool isLast4SSNValid()
        {
            return textEdit_Last4SSN.Text.Length == 4;
        }

        /// <summary>
        /// Is the PropertyZIP valid?
        /// </summary>
        private bool isPropertyZIPValid()
        {
            return textEdit_PropertyZIP.Text.Length == 5;
        }

        /// <summary>
        /// Handle the CLICK event on the SEARCH button
        /// </summary>
        private void simpleButton_Search_Click(object sender, EventArgs e)
        {
            // Generate the request information for the search. This is based upon the KeyField fields.
            var req = new DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchCriteriaDTO()
            {
                FirstName   = DebtPlus.Utils.Nulls.v_String(textEdit_FirstName.EditValue),
                LastName    = DebtPlus.Utils.Nulls.v_String(textEdit_LastName.EditValue),
                Last4_SSN   = DebtPlus.Utils.Nulls.v_String(textEdit_Last4SSN.EditValue),
                PropertyZip = DebtPlus.Utils.Nulls.v_String(textEdit_PropertyZIP.EditValue),
                LoanNumber  = DebtPlus.Utils.Nulls.v_String(textEdit_LoanNumber.EditValue),
                ProgramId   = DebtPlus.Utils.Nulls.v_String(textEdit_SVC.EditValue)
            };

            // Submit the request to the event handler so that it may do the search operation.
            var searchArgs = new PerformSearchArgs(req);
            OnPerformSearch(searchArgs);
        }

        /// <summary>
        /// Process the LOAD event for the control
        /// </summary>
        private void XtraUserControl_Request_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                simpleButton_Search.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the resize event for the control to center the buttons
        /// </summary>
        private void layoutControl1_Resize(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) >> 1;
            }
            catch { }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}