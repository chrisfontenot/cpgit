﻿namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class HPFCase_Search
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HPFCase_Search));
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.textEdit_SVC = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_LoanNumber = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_Last4SSN = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_PropertyZIP = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_LastName = new DevExpress.XtraEditors.TextEdit();
            this.textEdit_FirstName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl_Header = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Search = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SVC.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_LoanNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Last4SSN.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PropertyZIP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_LastName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_FirstName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.textEdit_SVC);
            this.layoutControl1.Controls.Add(this.textEdit_LoanNumber);
            this.layoutControl1.Controls.Add(this.textEdit_Last4SSN);
            this.layoutControl1.Controls.Add(this.textEdit_PropertyZIP);
            this.layoutControl1.Controls.Add(this.textEdit_LastName);
            this.layoutControl1.Controls.Add(this.textEdit_FirstName);
            this.layoutControl1.Controls.Add(this.labelControl_Header);
            this.layoutControl1.Controls.Add(this.simpleButton_Cancel);
            this.layoutControl1.Controls.Add(this.simpleButton_Search);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(536, 361);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // textEdit_SVC
            // 
            this.textEdit_SVC.Location = new System.Drawing.Point(388, 90);
            this.textEdit_SVC.Name = "textEdit_SVC";
            this.textEdit_SVC.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEdit_SVC.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_SVC.Properties.Mask.BeepOnError = true;
            this.textEdit_SVC.Properties.Mask.EditMask = "\\d+";
            this.textEdit_SVC.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_SVC.Properties.MaxLength = 20;
            this.textEdit_SVC.Size = new System.Drawing.Size(136, 20);
            this.textEdit_SVC.StyleController = this.layoutControl1;
            this.textEdit_SVC.TabIndex = 12;
            // 
            // textEdit_LoanNumber
            // 
            this.textEdit_LoanNumber.Location = new System.Drawing.Point(130, 90);
            this.textEdit_LoanNumber.Name = "textEdit_LoanNumber";
            this.textEdit_LoanNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEdit_LoanNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_LoanNumber.Properties.MaxLength = 50;
            this.textEdit_LoanNumber.Size = new System.Drawing.Size(136, 20);
            this.textEdit_LoanNumber.StyleController = this.layoutControl1;
            this.textEdit_LoanNumber.TabIndex = 11;
            // 
            // textEdit_Last4SSN
            // 
            this.textEdit_Last4SSN.Location = new System.Drawing.Point(388, 66);
            this.textEdit_Last4SSN.Name = "textEdit_Last4SSN";
            this.textEdit_Last4SSN.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEdit_Last4SSN.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_Last4SSN.Properties.Mask.BeepOnError = true;
            this.textEdit_Last4SSN.Properties.Mask.EditMask = "\\d{4}";
            this.textEdit_Last4SSN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_Last4SSN.Properties.MaxLength = 4;
            this.textEdit_Last4SSN.Size = new System.Drawing.Size(136, 20);
            this.textEdit_Last4SSN.StyleController = this.layoutControl1;
            this.textEdit_Last4SSN.TabIndex = 10;
            // 
            // textEdit_PropertyZIP
            // 
            this.textEdit_PropertyZIP.Location = new System.Drawing.Point(130, 66);
            this.textEdit_PropertyZIP.Name = "textEdit_PropertyZIP";
            this.textEdit_PropertyZIP.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEdit_PropertyZIP.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_PropertyZIP.Properties.Mask.BeepOnError = true;
            this.textEdit_PropertyZIP.Properties.Mask.EditMask = "\\d{5}";
            this.textEdit_PropertyZIP.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.textEdit_PropertyZIP.Properties.MaxLength = 5;
            this.textEdit_PropertyZIP.Size = new System.Drawing.Size(136, 20);
            this.textEdit_PropertyZIP.StyleController = this.layoutControl1;
            this.textEdit_PropertyZIP.TabIndex = 9;
            // 
            // textEdit_LastName
            // 
            this.textEdit_LastName.Location = new System.Drawing.Point(388, 42);
            this.textEdit_LastName.Name = "textEdit_LastName";
            this.textEdit_LastName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEdit_LastName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_LastName.Properties.MaxLength = 50;
            this.textEdit_LastName.Size = new System.Drawing.Size(136, 20);
            this.textEdit_LastName.StyleController = this.layoutControl1;
            this.textEdit_LastName.TabIndex = 8;
            // 
            // textEdit_FirstName
            // 
            this.textEdit_FirstName.Location = new System.Drawing.Point(130, 42);
            this.textEdit_FirstName.Name = "textEdit_FirstName";
            this.textEdit_FirstName.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.textEdit_FirstName.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textEdit_FirstName.Properties.MaxLength = 50;
            this.textEdit_FirstName.Size = new System.Drawing.Size(136, 20);
            this.textEdit_FirstName.StyleController = this.layoutControl1;
            this.textEdit_FirstName.TabIndex = 7;
            // 
            // labelControl_Header
            // 
            this.labelControl_Header.Appearance.Font = new System.Drawing.Font("Britannic Bold", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl_Header.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.labelControl_Header.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.labelControl_Header.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl_Header.Location = new System.Drawing.Point(12, 12);
            this.labelControl_Header.MinimumSize = new System.Drawing.Size(0, 26);
            this.labelControl_Header.Name = "labelControl_Header";
            this.labelControl_Header.Size = new System.Drawing.Size(512, 26);
            this.labelControl_Header.StyleController = this.layoutControl1;
            this.labelControl_Header.TabIndex = 6;
            this.labelControl_Header.Text = "HPF Create Client";
            // 
            // simpleButton_Cancel
            // 
            this.simpleButton_Cancel.Location = new System.Drawing.Point(198, 326);
            this.simpleButton_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.Name = "simpleButton_Cancel";
            this.simpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Cancel.StyleController = this.layoutControl1;
            this.simpleButton_Cancel.TabIndex = 5;
            this.simpleButton_Cancel.Text = "Cancel";
            this.simpleButton_Cancel.ToolTip = "Click here to cancel the dialog and return to the previous state";
            // 
            // simpleButton_Search
            // 
            this.simpleButton_Search.Enabled = false;
            this.simpleButton_Search.Location = new System.Drawing.Point(277, 326);
            this.simpleButton_Search.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Search.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_Search.Name = "simpleButton_Search";
            this.simpleButton_Search.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Search.StyleController = this.layoutControl1;
            this.simpleButton_Search.TabIndex = 4;
            this.simpleButton_Search.Text = "Next";
            this.simpleButton_Search.ToolTip = "Click here to perform the search";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.emptySpaceItem2,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(536, 361);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "Button controls";
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.emptySpaceItem_Left,
            this.emptySpaceItem_Right,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 314);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(516, 27);
            this.layoutControlGroup2.Text = "Button controls";
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.simpleButton_Cancel;
            this.layoutControlItem2.CustomizationFormText = "Cancel Button";
            this.layoutControlItem2.Location = new System.Drawing.Point(186, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem2.Text = "Cancel";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.AllowHotTrack = false;
            this.emptySpaceItem_Left.CustomizationFormText = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem_Left.Name = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(186, 27);
            this.emptySpaceItem_Left.Text = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.AllowHotTrack = false;
            this.emptySpaceItem_Right.CustomizationFormText = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(344, 0);
            this.emptySpaceItem_Right.Name = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(172, 27);
            this.emptySpaceItem_Right.Text = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.simpleButton_Search;
            this.layoutControlItem1.CustomizationFormText = "Search Button";
            this.layoutControlItem1.Location = new System.Drawing.Point(265, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem1.Text = "Search";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControl_Header;
            this.layoutControlItem3.CustomizationFormText = "Foreclosure Case Search";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(516, 30);
            this.layoutControlItem3.Text = "Foreclosure Case Search";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.textEdit_FirstName;
            this.layoutControlItem4.CustomizationFormText = "First Name";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem4.Text = "First Name";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.textEdit_LastName;
            this.layoutControlItem5.CustomizationFormText = "Last Name";
            this.layoutControlItem5.Location = new System.Drawing.Point(258, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem5.Text = "Last Name";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(115, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 186);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(516, 128);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.textEdit_PropertyZIP;
            this.layoutControlItem6.CustomizationFormText = "Property ZIP";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 54);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem6.Text = "Property ZIP";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.textEdit_Last4SSN;
            this.layoutControlItem7.CustomizationFormText = "Last 4 digits of SSN";
            this.layoutControlItem7.Location = new System.Drawing.Point(258, 54);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem7.Text = "Last 4 digits of SSN";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.textEdit_LoanNumber;
            this.layoutControlItem8.CustomizationFormText = "Loan Number";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 78);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem8.Text = "Loan Number";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(115, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.textEdit_SVC;
            this.layoutControlItem9.CustomizationFormText = "SVC # (Agency Case #)";
            this.layoutControlItem9.Location = new System.Drawing.Point(258, 78);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(258, 24);
            this.layoutControlItem9.Text = "SVC # (Agency Case #)";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(115, 13);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this.labelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.labelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.Location = new System.Drawing.Point(12, 134);
            this.labelControl1.MinimumSize = new System.Drawing.Size(0, 60);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(512, 60);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 13;
            this.labelControl1.Text = resources.GetString("labelControl1.Text");
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.labelControl1;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 122);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(516, 64);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 102);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(0, 20);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(10, 20);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(516, 20);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // HPFCase_Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "HPFCase_Search";
            this.Size = new System.Drawing.Size(536, 361);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_SVC.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_LoanNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_Last4SSN.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_PropertyZIP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_LastName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit_FirstName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Cancel;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Search;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Left;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Right;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.TextEdit textEdit_SVC;
        private DevExpress.XtraEditors.TextEdit textEdit_LoanNumber;
        private DevExpress.XtraEditors.TextEdit textEdit_Last4SSN;
        private DevExpress.XtraEditors.TextEdit textEdit_PropertyZIP;
        private DevExpress.XtraEditors.TextEdit textEdit_LastName;
        private DevExpress.XtraEditors.TextEdit textEdit_FirstName;
        private DevExpress.XtraEditors.LabelControl labelControl_Header;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
    }
}
