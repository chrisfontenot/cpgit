﻿using System;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    public partial class HPFCase_ResponseGrid : DevExpress.XtraEditors.XtraUserControl
    {
        #region Finish Button

        public class FinishEventArgs : System.EventArgs
        {
            public bool WorkedWithAgency { get; set; }
            public DebtPlus.SOAP.HPF.Agency.ForeclosureCaseWSDTO selected { get; private set; }

            public FinishEventArgs(DebtPlus.SOAP.HPF.Agency.ForeclosureCaseWSDTO selected, bool WorkedWithAgency) : base()
            {
                this.selected = selected;
                this.WorkedWithAgency = WorkedWithAgency;
            }
        }

        public delegate void FinishEventHandler(object sender, FinishEventArgs e);

        public event FinishEventHandler Finish;

        protected void RaiseFinishEvent(FinishEventArgs e)
        {
            var evt = Finish;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnFinishEvent(FinishEventArgs e)
        {
            RaiseFinishEvent(e);
        }

        #endregion Finish Button

        public HPFCase_ResponseGrid()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        #region Back Button

        /// <summary>
        /// Event raised when the Back is to be performed
        /// </summary>
        public event EventHandler Back;

        /// <summary>
        /// Do the Back operation when it is indicated
        /// </summary>
        /// <param name="e"></param>
        protected void RaiseBack(EventArgs e)
        {
            var evt = Back;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Do the Back operation when it is indicated
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnBack(EventArgs e)
        {
            RaiseBack(e);
        }

        /// <summary>
        /// Handle the CLICK event on the Back button
        /// </summary>
        private void simpleButton_Back_Click(object sender, EventArgs e)
        {
            OnBack(e);
        }

        #endregion Back Button

        private void RegisterHandlers()
        {
            this.simpleButton_Back.Click   += simpleButton_Back_Click;
            this.simpleButton_Finish.Click += simpleButton_Finish_Click;
            this.gridView1.DoubleClick     += gridView1_DoubleClick;
        }

        private void UnRegisterHandlers()
        {
            this.simpleButton_Back.Click   -= simpleButton_Back_Click;
            this.simpleButton_Finish.Click -= simpleButton_Finish_Click;
            this.gridView1.DoubleClick     -= gridView1_DoubleClick;
        }

        private void simpleButton_Finish_Click(object sender, EventArgs e)
        {
            OnFinishEvent(new FinishEventArgs(null, false));
        }

        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo(gridControl1.PointToClient(MousePosition));
            if (!hi.IsValid || !hi.InRow)
            {
                return;
            }

            var resp = gridView1.GetRow(hi.RowHandle) as DebtPlus.SOAP.HPF.Agency.ForeclosureCaseWSDTO;
            if (resp == null)
            {
                return;
            }

            using (var frm = new DetailsForm(resp))
            {
                var ans = frm.ShowDialog();
                if (ans == DialogResult.OK)
                {
                    OnFinishEvent(new FinishEventArgs(resp, true));
                }
            }
        }
    }
}