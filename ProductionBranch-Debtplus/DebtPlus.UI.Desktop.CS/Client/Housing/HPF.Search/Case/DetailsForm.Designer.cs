﻿namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class DetailsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl_counselor = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_fcid = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_agency = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_svc_number = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_loan_number = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_record_date = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_address = new DevExpress.XtraEditors.LabelControl();
            this.labelControl_name = new DevExpress.XtraEditors.LabelControl();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.simpleButton_OK);
            this.layoutControl1.Controls.Add(this.labelControl_counselor);
            this.layoutControl1.Controls.Add(this.labelControl_fcid);
            this.layoutControl1.Controls.Add(this.labelControl_agency);
            this.layoutControl1.Controls.Add(this.labelControl_svc_number);
            this.layoutControl1.Controls.Add(this.labelControl_loan_number);
            this.layoutControl1.Controls.Add(this.labelControl_record_date);
            this.layoutControl1.Controls.Add(this.labelControl_address);
            this.layoutControl1.Controls.Add(this.labelControl_name);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(413, 12, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(366, 196);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // simpleButton1
            // 
            this.simpleButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton1.Location = new System.Drawing.Point(184, 161);
            this.simpleButton1.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton1.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 15;
            this.simpleButton1.Text = "&Cancel";
            // 
            // simpleButton_OK
            // 
            this.simpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.simpleButton_OK.Location = new System.Drawing.Point(105, 161);
            this.simpleButton_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.Name = "simpleButton_OK";
            this.simpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_OK.StyleController = this.layoutControl1;
            this.simpleButton_OK.TabIndex = 14;
            this.simpleButton_OK.Text = "&Use This One";
            // 
            // labelControl_counselor
            // 
            this.labelControl_counselor.Location = new System.Drawing.Point(103, 80);
            this.labelControl_counselor.Name = "labelControl_counselor";
            this.labelControl_counselor.Size = new System.Drawing.Size(251, 13);
            this.labelControl_counselor.StyleController = this.layoutControl1;
            this.labelControl_counselor.TabIndex = 11;
            // 
            // labelControl_fcid
            // 
            this.labelControl_fcid.Location = new System.Drawing.Point(103, 114);
            this.labelControl_fcid.Name = "labelControl_fcid";
            this.labelControl_fcid.Size = new System.Drawing.Size(251, 13);
            this.labelControl_fcid.StyleController = this.layoutControl1;
            this.labelControl_fcid.TabIndex = 10;
            // 
            // labelControl_agency
            // 
            this.labelControl_agency.Location = new System.Drawing.Point(103, 63);
            this.labelControl_agency.Name = "labelControl_agency";
            this.labelControl_agency.Size = new System.Drawing.Size(251, 13);
            this.labelControl_agency.StyleController = this.layoutControl1;
            this.labelControl_agency.TabIndex = 9;
            // 
            // labelControl_svc_number
            // 
            this.labelControl_svc_number.Location = new System.Drawing.Point(103, 97);
            this.labelControl_svc_number.Name = "labelControl_svc_number";
            this.labelControl_svc_number.Size = new System.Drawing.Size(251, 13);
            this.labelControl_svc_number.StyleController = this.layoutControl1;
            this.labelControl_svc_number.TabIndex = 8;
            // 
            // labelControl_loan_number
            // 
            this.labelControl_loan_number.Location = new System.Drawing.Point(103, 131);
            this.labelControl_loan_number.Name = "labelControl_loan_number";
            this.labelControl_loan_number.Size = new System.Drawing.Size(251, 13);
            this.labelControl_loan_number.StyleController = this.layoutControl1;
            this.labelControl_loan_number.TabIndex = 7;
            // 
            // labelControl_record_date
            // 
            this.labelControl_record_date.Location = new System.Drawing.Point(103, 46);
            this.labelControl_record_date.Name = "labelControl_record_date";
            this.labelControl_record_date.Size = new System.Drawing.Size(251, 13);
            this.labelControl_record_date.StyleController = this.layoutControl1;
            this.labelControl_record_date.TabIndex = 6;
            // 
            // labelControl_address
            // 
            this.labelControl_address.Location = new System.Drawing.Point(103, 29);
            this.labelControl_address.Name = "labelControl_address";
            this.labelControl_address.Size = new System.Drawing.Size(251, 13);
            this.labelControl_address.StyleController = this.layoutControl1;
            this.labelControl_address.TabIndex = 5;
            // 
            // labelControl_name
            // 
            this.labelControl_name.Location = new System.Drawing.Point(103, 12);
            this.labelControl_name.Name = "labelControl_name";
            this.labelControl_name.Size = new System.Drawing.Size(251, 13);
            this.labelControl_name.StyleController = this.layoutControl1;
            this.labelControl_name.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.layoutControlGroup1.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.emptySpaceItem1,
            this.layoutControlItem11,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem5,
            this.layoutControlItem9,
            this.emptySpaceItem_Left,
            this.emptySpaceItem_Right});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(366, 196);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.labelControl_name;
            this.layoutControlItem1.CustomizationFormText = "Name";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem1.Text = "Name";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.labelControl_address;
            this.layoutControlItem2.CustomizationFormText = "Address";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 17);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem2.Text = "Address";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.labelControl_record_date;
            this.layoutControlItem3.CustomizationFormText = "Record Date";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 34);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem3.Text = "Record Date";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.labelControl_agency;
            this.layoutControlItem6.CustomizationFormText = "Agency";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem6.Text = "Agency";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.labelControl_fcid;
            this.layoutControlItem7.CustomizationFormText = "FCID";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 102);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem7.Text = "FCID";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(88, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 136);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(346, 13);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.simpleButton_OK;
            this.layoutControlItem11.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(93, 149);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.labelControl_loan_number;
            this.layoutControlItem4.CustomizationFormText = "Loan #";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 119);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem4.Text = "Loan #";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.labelControl_counselor;
            this.layoutControlItem8.CustomizationFormText = "Counselor";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 68);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem8.Text = "Counselor";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControl_svc_number;
            this.layoutControlItem5.CustomizationFormText = "SVC # (Agency #)";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 85);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(346, 17);
            this.layoutControlItem5.Text = "SVC # (Agency #)";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(88, 13);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.simpleButton1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(172, 149);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.AllowHotTrack = false;
            this.emptySpaceItem_Left.CustomizationFormText = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Location = new System.Drawing.Point(0, 149);
            this.emptySpaceItem_Left.Name = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(93, 27);
            this.emptySpaceItem_Left.Text = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.AllowHotTrack = false;
            this.emptySpaceItem_Right.CustomizationFormText = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(251, 149);
            this.emptySpaceItem_Right.Name = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(95, 27);
            this.emptySpaceItem_Right.Text = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // DetailsForm
            // 
            this.AcceptButton = this.simpleButton_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.simpleButton_OK;
            this.ClientSize = new System.Drawing.Size(366, 196);
            this.ControlBox = false;
            this.Controls.Add(this.layoutControl1);
            this.Name = "DetailsForm";
            this.Text = "Case Details";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.LabelControl labelControl_counselor;
        private DevExpress.XtraEditors.LabelControl labelControl_fcid;
        private DevExpress.XtraEditors.LabelControl labelControl_agency;
        private DevExpress.XtraEditors.LabelControl labelControl_svc_number;
        private DevExpress.XtraEditors.LabelControl labelControl_loan_number;
        private DevExpress.XtraEditors.LabelControl labelControl_record_date;
        private DevExpress.XtraEditors.LabelControl labelControl_address;
        private DevExpress.XtraEditors.LabelControl labelControl_name;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraEditors.SimpleButton simpleButton_OK;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Left;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Right;
    }
}