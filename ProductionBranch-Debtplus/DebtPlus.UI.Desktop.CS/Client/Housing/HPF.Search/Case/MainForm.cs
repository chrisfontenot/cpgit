﻿using System;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class MainForm
    {
#pragma warning disable 414
        /// <summary>
        /// Did the agency work with a different party for this client?
        /// </summary>
        private bool WorkedWithAgency = false;
#pragma warning restore 414

        /// <summary>
        /// Process the cancel event from the search request form.
        /// </summary>
        private void HPFCase_Search1_Cancel(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Handle the search function for HPF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HPFCase_Search1_PerformSearch(object sender, HPFCase_Search.PerformSearchArgs e)
        {
            try
            {
                using (var soapClass = new DebtPlus.SOAP.HPF.Communication())
                {
                    // Do the search operation now.
                    var reqOp = new DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchRequest()
                    {
                        SearchCriteria = new SOAP.HPF.Agency.ForeclosureCaseSearchCriteriaDTO()
                        {
                            AgencyCaseNumber = e.req.AgencyCaseNumber,
                            AgencyId = e.req.AgencyId,
                            CounseledProgramId = e.req.CounseledProgramId,
                            CounselorLname = e.req.CounselorLname,
                            DocPrepEditorInd = e.req.DocPrepEditorInd,
                            DocPrepInd = e.req.DocPrepInd,
                            FCID = e.req.FCID,
                            FirstName = e.req.FirstName,
                            ICTCallID = e.req.ICTCallID,
                            Last4_SSN = e.req.Last4_SSN,
                            LastName = e.req.LastName,
                            LoanNumber = e.req.LoanNumber,
                            ProgramId = e.req.ProgramId,
                            PropertyZip = e.req.PropertyZip,
                            Servicer = e.req.Servicer,
                            State = e.req.State
                        }
                    };

                    DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchResponse resp = soapClass.AgencySearch(reqOp);
                    if (resp == null)
                    {
                        DebtPlus.Data.Forms.MessageBox.Show("An error occurred in talking to the proxy server.\r\nNo data was returned for the search.\r\n\r\nPlease try again a bit later.", "Error in talking to proxy", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Build the message text if there is an error or warning
                    if (resp.Status != DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success && resp.Messages.Length > 0)
                    {
                        var sb = new System.Text.StringBuilder();
                        foreach (var msg in resp.Messages)
                        {
                            sb.AppendFormat("\r\n\r\n{0}", msg.Message);
                        }
                        sb.Remove(0, 4);
                        DebtPlus.Data.Forms.MessageBox.Show(sb.ToString(), resp.Status == DebtPlus.SOAP.HPF.Agency.ResponseStatus.Warning ? "Warning" : resp.Status == DebtPlus.SOAP.HPF.Agency.ResponseStatus.Fail ? "Error" : "Authentication failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    // If the response is successful but there is no data then say so
                    if (resp.Status == DebtPlus.SOAP.HPF.Agency.ResponseStatus.Success && resp.Results.Length == 0)
                    {
                        ItemNotFound(e.req);
                        return;
                    }

                    // Finally, there should be some entries in the list to be shown.
                    if (resp.Results.Length == 0)
                    {
                        return;
                    }

                    // Pass the results field to the result grid control.
                    HPFCase_ResponseGrid1.BringToFront();
                    HPFCase_ResponseGrid1.Focus();

                    // Set the data source for the grid control. It will take it from here.
                    var colResults = new System.Collections.Generic.List<DebtPlus.SOAP.HPF.Agency.ForeclosureCaseWSDTO>();
                    colResults.AddRange(resp.Results);
                    HPFCase_ResponseGrid1.gridControl1.DataSource = colResults;
                    HPFCase_ResponseGrid1.gridView1.UpdateTotalSummary();
                }
            }
            catch (System.Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error talking to HPF proxy server");
            }
        }

        /// <summary>
        /// The case was not located. Do something at this time with the requested case.
        /// </summary>
        /// <param name="req"></param>
        private void ItemNotFound(DebtPlus.SOAP.HPF.Agency.ForeclosureCaseSearchCriteriaDTO req)
        {
            // Bring up the FCID search request
            HPFCall_Search1.BringToFront();
            HPFCall_Search1.Focus();

            // Clear the worked with agency status to show that we did not and this is the first reference.
            WorkedWithAgency = false;
        }

        /// <summary>
        /// Process the BACK button from the result grid. Redisplay the search information.
        /// </summary>
        private void HPFCase_ResponseGrid1_Back(object sender, EventArgs e)
        {
            // Switch back to the KeyField form with the same data on the form.
            HPFCase_Search1.BringToFront();
            HPFCase_Search1.Focus();
        }

        private void HPFCase_ResponseGrid1_Finish(object sender, HPFCase_ResponseGrid.FinishEventArgs e)
        {
            WorkedWithAgency = e.WorkedWithAgency;
            if (e.selected == null)
            {
                HPFCall_Search1.BringToFront();
                HPFCall_Search1.Focus();
                return;
            }

            // Process the item in the selection criteria
            var resp = new DebtPlus.UI.Housing.HPF.Data.Log()
            {
                AuthorizedInd                    = null,
                // AutomaticNumberIdentification = null,
                CallSourceCd                     = null,
                City                             = null,
                CustomerPhone                    = null,
                DelinqInd                        = null,
                DocFulfillmentCd                 = null,
                DTIInd                           = null,
                Email                            = null,
                EndDate                          = null,
                FcId                             = null,
                FCSaleDate                       = null,
                FinalDispoCd                     = null,
                FinalDispoDesc                   = null,
                FirstName                        = e.selected.BorrowerFname,
                FnmaLookupCd                     = null,
                FnmApptSetInd                    = null,
                FnmLookupInd                     = null,
                FnmRegionalOfficeCd              = null,
                GrossIncomeAmount                = null,
                HomeownerInd                     = null,
                HopeNetCallId                    = null,
                ICTCallId                        = null,
                InvestorRentalInd                = null,
                LastName                         = e.selected.BorrowerLname,
                LoanAccountNumber                = e.selected.LoanNumber,
                LoanDelinqStatusCd               = null,
                LoanLookupCd                     = null,
                LoanModInterestRate              = null,
                LoanModTerm                      = null,
                LoanTrialModCd                   = null,
                MailingAddr1                     = e.selected.PropAddr1,
                MailingAddr2                     = e.selected.PropAddr2,
                MailingCity                      = e.selected.PropCity,
                MailingStateCd                   = e.selected.PropStateCd,
                MailingZip                       = e.selected.PropZip,
                MaxLoanAmountInd                 = null,
                MHAEligibilityCd                 = null,
                MHAEligibilityDesc               = null,
                MHAIneligibilityReasonCd         = null,
                MHAIneligibilityReasonDesc       = null,
                MHAInfoShareInd                  = null,
                MilActiveInd                     = null,
                MotherMaidenLastName             = null,
                NonprofitReferralDesc1           = null,
                NonprofitReferralDesc2           = null,
                NonprofitReferralDesc3           = null,
                NonprofitReferralKeyNum1         = null,
                NonprofitReferralKeyNum2         = null,
                NonprofitReferralKeyNum3         = null,
                OptOutReasonCd                   = null,
                OriginatedPrior2009Ind           = null,
                OtherServicerName                = null,
                PaymentAmount                    = null,
                PowerOfAttorneyInd               = null,
                PreviousUpInd                    = null,
                PrimaryResidenceInd              = null,
                ProgramId                        = null,
                PropStreetAddress                = null,
                PropZipFull9                     = null,
                ReasonForCall                    = null,
                RepeatDiscInd                    = null,
                ServicerCAId                     = null,
                ServicerCALastContactDate        = null,
                ServicerCAName                   = null,
                ServicerCANumber                 = null,
                ServicerCAOtherName              = null,
                ServicerId                       = null,
                ServicerInProcessCd              = null,
                ServicerName                     = null,
                SpocInd                          = null,
                SponsorId                        = null,
                SponsorLoanNum                   = null,
                StartDate                        = null,
                State                            = null,
                ThirdPartyContactCd              = null,
                TNCountyCd                       = null,
                UnemployedInd                    = null,
                UpBenefitsInd                    = null
            };

            // Process the search result to create the empty client
            ProcessSearchResult(resp, true);
        }
    }
}