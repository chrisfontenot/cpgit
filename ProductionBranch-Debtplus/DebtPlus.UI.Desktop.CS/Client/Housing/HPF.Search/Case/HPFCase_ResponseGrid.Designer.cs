﻿namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class HPFCase_ResponseGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton_Back = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton_Finish = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_record_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_address = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton_Back
            // 
            this.simpleButton_Back.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_Back.Location = new System.Drawing.Point(142, 235);
            this.simpleButton_Back.Name = "simpleButton_Back";
            this.simpleButton_Back.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Back.TabIndex = 0;
            this.simpleButton_Back.Text = "&Back";
            // 
            // simpleButton_Finish
            // 
            this.simpleButton_Finish.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton_Finish.Location = new System.Drawing.Point(223, 235);
            this.simpleButton_Finish.Name = "simpleButton_Finish";
            this.simpleButton_Finish.Size = new System.Drawing.Size(75, 23);
            this.simpleButton_Finish.TabIndex = 1;
            this.simpleButton_Finish.Text = "&Next";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(440, 225);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_name,
            this.gridColumn_record_date,
            this.gridColumn_address});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn_name, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // gridColumn_name
            // 
            this.gridColumn_name.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_name.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_name.Caption = "Name";
            this.gridColumn_name.CustomizationCaption = "Name";
            this.gridColumn_name.FieldName = "DISPLAY_name";
            this.gridColumn_name.Name = "gridColumn_name";
            this.gridColumn_name.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "name", "{0:n0} Entries")});
            this.gridColumn_name.Visible = true;
            this.gridColumn_name.VisibleIndex = 0;
            this.gridColumn_name.Width = 141;
            // 
            // gridColumn_record_date
            // 
            this.gridColumn_record_date.Caption = "Record Date";
            this.gridColumn_record_date.DisplayFormat.FormatString = "d";
            this.gridColumn_record_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_record_date.FieldName = "DISPLAY_case_date";
            this.gridColumn_record_date.GroupFormat.FormatString = "d";
            this.gridColumn_record_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_record_date.Name = "gridColumn_record_date";
            this.gridColumn_record_date.Visible = true;
            this.gridColumn_record_date.VisibleIndex = 1;
            this.gridColumn_record_date.Width = 83;
            // 
            // gridColumn_address
            // 
            this.gridColumn_address.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_address.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_address.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_address.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn_address.Caption = "Address";
            this.gridColumn_address.CustomizationCaption = "Address";
            this.gridColumn_address.FieldName = "DISPLAY_address";
            this.gridColumn_address.Name = "gridColumn_address";
            this.gridColumn_address.Visible = true;
            this.gridColumn_address.VisibleIndex = 2;
            this.gridColumn_address.Width = 207;
            // 
            // HPFCase_ResponseGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.simpleButton_Finish);
            this.Controls.Add(this.simpleButton_Back);
            this.Name = "HPFCase_ResponseGrid";
            this.Size = new System.Drawing.Size(440, 261);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton_Back;
        private DevExpress.XtraEditors.SimpleButton simpleButton_Finish;
        internal DevExpress.XtraGrid.GridControl gridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_name;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_record_date;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_address;
    }
}
