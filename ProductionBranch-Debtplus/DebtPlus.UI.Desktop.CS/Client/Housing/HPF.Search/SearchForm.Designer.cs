﻿namespace DebtPlus.UI.Desktop.CS.Client.Housing.HPF.Search
{
    partial class SearchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchCall1 = new HPFCall_Search();
            this.responseGrid1 = new HPFCall_ResponseGrid();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // searchCall1
            // 
            this.searchCall1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchCall1.Location = new System.Drawing.Point(0, 0);
            this.searchCall1.Name = "searchCall1";
            this.searchCall1.Size = new System.Drawing.Size(300, 332);
            this.searchCall1.TabIndex = 0;
            // 
            // responseGrid1
            // 
            this.responseGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.responseGrid1.Location = new System.Drawing.Point(0, 0);
            this.responseGrid1.Name = "responseGrid1";
            this.responseGrid1.Size = new System.Drawing.Size(300, 332);
            this.responseGrid1.TabIndex = 1;
            // 
            // SearchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(300, 332);
            this.Controls.Add(this.responseGrid1);
            this.Controls.Add(this.searchCall1);
            this.Name = "SearchForm";
            this.Text = "HPF Call Search";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private HPFCall_Search searchCall1;
        private HPFCall_ResponseGrid responseGrid1;
    }
}