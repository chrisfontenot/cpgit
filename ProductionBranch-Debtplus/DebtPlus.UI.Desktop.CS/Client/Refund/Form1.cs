#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.Check.Print.Library;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Desktop.CS.Client.Refund
{
    internal partial class Form1
    {
        private DataSet ds = new DataSet("ds");

        private ArgParser ap = null;

        public Form1() : base()
        {
            InitializeComponent();
        }

        public Form1(ArgParser ap) : this()
        {
            this.ap = ap;
            this.Load += Form1_Load;
            SimpleButton_ok.Click += SimpleButton_ok_Click;
            SimpleButton_cancel.Click += SimpleButton_cancel_Click;
            CalcEdit_amount.EditValueChanging += CalcEdit_amount_EditValueChanging;
            ClientID1.EditValueChanged += ClientID1_EditValueChanged;
        }

        private void SimpleButton_cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Build the list of check dispositions
            var _with1 = ComboBoxEdit_disposition;
            var _with2 = _with1.Properties;
            var _with3 = _with2.Items;
            _with3.Clear();
            _with3.Add(new DebtPlus.Data.Controls.ComboboxItem("Save the check for the next print run", "QUEUE"));
            _with3.Add(new DebtPlus.Data.Controls.ComboboxItem("Print the check immediately", "PRINT"));
            _with3.Add(new DebtPlus.Data.Controls.ComboboxItem("Enter the information for a previously printed check", "MANUAL"));
            _with1.SelectedIndex = 0;

            // Bind the list of reasons to the control
            var _with4 = ComboBoxEdit_reason;
            var _with5 = _with4.Properties;
            var _with6 = _with5.Items;
            _with6.Clear();
            foreach (DataRow row in ReasonsTable().Rows)
            {
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(row["description"])));
            }
            _with4.SelectedIndex = 0;
        }

        private DataTable ReasonsTable()
        {
            DataTable tbl = ds.Tables["lst_descriptions_CR"];
            if (tbl == null)
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with7 = cmd;
                        _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with7.CommandText = "lst_descriptions_CR";
                        _with7.CommandType = CommandType.StoredProcedure;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "lst_descriptions_CR");
                        }
                    }

                    tbl = ds.Tables["lst_descriptions_CR"];
                    if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        var _with8 = tbl;
                        _with8.PrimaryKey = new DataColumn[] { _with8.Columns["client"] };
                    }
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading lst_descripitons_CR");
                }
            }

            return tbl;
        }

        private void ClientID1_EditValueChanged(object sender, EventArgs e)
        {
            DataRow row = ReadClient(ClientID1.EditValue);
            if (row == null)
            {
                LabelControl_Address.Text = string.Empty;
                SetBalance(0m, 0m);
            }
            else
            {
                LabelControl_Address.Text = Convert.ToString(row["address"]);
                SetBalance(row["held_in_trust"], CalcEdit_amount.EditValue);
            }
        }

        private decimal LastCurrentBalance = 0m;

        private void SetBalance(object CurrentBalance, object Amount)
        {
            if (CurrentBalance == null || object.ReferenceEquals(CurrentBalance, DBNull.Value))
                CurrentBalance = 0m;
            if (Amount == null || object.ReferenceEquals(Amount, DBNull.Value))
                Amount = 0m;

            // Save the last balance for later refresn
            LastCurrentBalance = Convert.ToDecimal(CurrentBalance);

            // Update the display with the amounts
            LabelControl_current_balance.Text = string.Format("{0:c}", LastCurrentBalance);
            LabelControl_ending_balance.Text = string.Format("{0:c}", Convert.ToDecimal(CurrentBalance) - Convert.ToDecimal(Amount));
            SimpleButton_ok.Enabled = Convert.ToDecimal(Amount) > 0m && (Convert.ToDecimal(Amount) <= Convert.ToDecimal(CurrentBalance)) && (LabelControl_Address.Text != string.Empty);
        }

        private void SetBalance(object Amount)
        {
            SetBalance(LastCurrentBalance, Amount);
        }

        /// <summary>
        /// Define the clients table
        /// </summary>
        protected DataRow ReadClient(System.Nullable<Int32> client)
        {
            DataRow answer = null;

            if (client != null)
            {
                // If the table is not defined then read it from the database.
                DataTable tbl = ds.Tables["clients"];
                if (tbl != null)
                    answer = tbl.Rows.Find(client.Value);

                if (answer == null)
                {
                    try
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            var _with9 = cmd;
                            _with9.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            _with9.CommandText = "SELECT c.client, c.held_in_trust, c.active_status, dbo.address_block(dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix),dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),a.address_line_2,dbo.format_city_state_zip(a.city,a.state,a.postalcode),default) as address FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p ON c.client = p.client AND 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name LEFT OUTER JOIN addresses a WITH (NOLOCK) ON c.AddressID = a.Address WHERE c.client = @client";
                            _with9.CommandType = CommandType.Text;
                            _with9.Parameters.Add("@client", SqlDbType.Int).Value = client.Value;

                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, "clients");
                            }
                        }

                        tbl = ds.Tables["clients"];
                        if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                        {
                            var _with10 = tbl;
                            _with10.PrimaryKey = new DataColumn[] { _with10.Columns["client"] };
                        }

                        answer = tbl.Rows.Find(client.Value);
                    }
                    catch (SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading clients");
                    }
                }
            }

            return answer;
        }

        private void CalcEdit_amount_EditValueChanging(object sender, ChangingEventArgs e)
        {
            if (Convert.ToDecimal(e.NewValue) < 0m)
            {
                e.Cancel = true;
            }
            else
            {
                SetBalance(e.NewValue);
            }
        }

        private void SimpleButton_ok_Click(object sender, EventArgs e)
        {
            Int32 trust_register = default(Int32);
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with11 = cmd;
                    _with11.Connection = cn;
                    _with11.CommandText = "xpr_client_refund_CR";
                    _with11.CommandType = CommandType.StoredProcedure;
                    _with11.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    _with11.Parameters.Add("@client", SqlDbType.Int).Value = global::DebtPlus.Utils.Nulls.ToDbNull(ClientID1.EditValue);
                    _with11.Parameters.Add("@amount", SqlDbType.Decimal).Value = CalcEdit_amount.EditValue;
                    _with11.Parameters.Add("@message", SqlDbType.VarChar, 50).Value = ComboBoxEdit_reason.Text.Trim();
                    _with11.Parameters.Add("@tran_type", SqlDbType.VarChar, 10).Value = "CR";
                    _with11.ExecuteNonQuery();
                    trust_register = Convert.ToInt32(_with11.Parameters[0].Value);
                }

                // Toss the current client record from the database because the trust balance is now wrong
                DataRow row = ReadClient(ClientID1.EditValue);
                if (row != null)
                {
                    row.Delete();
                    ds.Tables["clients"].AcceptChanges();
                }
            }
            catch (SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error generating client refund check");
            }

            // Process the disposition once the check has been created
            if (trust_register > 0)
            {
                switch (Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_disposition.SelectedItem).value))
                {
                    case "MANUAL":
                        var _with12 = new PrintChecksClass();
                        _with12.RecordSingleCheck(trust_register);
                        break;

                    case "QUEUE":
                        DoMessageBeep(MessageBeepEnum.OK);
                        break;

                    case "PRINT":
                        var _with13 = new PrintChecksClass();
                        _with13.PrintSingleCheck(trust_register);
                        break;
                }

                // Clear the information for the next cycle
                LabelControl_Address.Text = string.Empty;
                ClientID1.EditValue = null;
                CalcEdit_amount.EditValue = 0m;
                ClientID1.Focus();
            }
        }
    }
}