using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Client.Widgets.Controls;

namespace DebtPlus.UI.Desktop.CS.Client.Refund
{
	partial class Form1 : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_Address = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_current_balance = new DevExpress.XtraEditors.LabelControl();
			this.CalcEdit_amount = new DevExpress.XtraEditors.CalcEdit();
			this.LabelControl_ending_balance = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.ComboBoxEdit_reason = new DevExpress.XtraEditors.ComboBoxEdit();
			this.ComboBoxEdit_disposition = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton_ok = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_cancel = new DevExpress.XtraEditors.SimpleButton();
			this.ClientID1 = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_amount.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_reason.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_disposition.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 13);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(27, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "C&lient";
			//
			//LabelControl_Address
			//
			this.LabelControl_Address.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_Address.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			this.LabelControl_Address.Appearance.Options.UseBackColor = true;
			this.LabelControl_Address.Appearance.Options.UseTextOptions = true;
			this.LabelControl_Address.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl_Address.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl_Address.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl_Address.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_Address.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
			this.LabelControl_Address.Location = new System.Drawing.Point(12, 39);
			this.LabelControl_Address.Name = "LabelControl_Address";
			this.LabelControl_Address.Padding = new System.Windows.Forms.Padding(4);
			this.LabelControl_Address.Size = new System.Drawing.Size(229, 103);
			this.LabelControl_Address.TabIndex = 2;
			this.LabelControl_Address.UseMnemonic = false;
			//
			//LabelControl3
			//
			this.LabelControl3.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl3.Location = new System.Drawing.Point(268, 49);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(77, 13);
			this.LabelControl3.TabIndex = 3;
			this.LabelControl3.Text = "Current Balance";
			//
			//LabelControl4
			//
			this.LabelControl4.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl4.Location = new System.Drawing.Point(268, 71);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(37, 13);
			this.LabelControl4.TabIndex = 5;
			this.LabelControl4.Text = "&Amount";
			//
			//LabelControl5
			//
			this.LabelControl5.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl5.Location = new System.Drawing.Point(268, 98);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(72, 13);
			this.LabelControl5.TabIndex = 7;
			this.LabelControl5.Text = "Ending Balance";
			//
			//LabelControl_current_balance
			//
			this.LabelControl_current_balance.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_current_balance.Appearance.Options.UseTextOptions = true;
			this.LabelControl_current_balance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl_current_balance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_current_balance.Location = new System.Drawing.Point(351, 49);
			this.LabelControl_current_balance.Name = "LabelControl_current_balance";
			this.LabelControl_current_balance.Size = new System.Drawing.Size(85, 13);
			this.LabelControl_current_balance.TabIndex = 4;
			this.LabelControl_current_balance.Text = "$0.00";
			this.LabelControl_current_balance.UseMnemonic = false;
			//
			//CalcEdit_amount
			//
			this.CalcEdit_amount.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.CalcEdit_amount.Location = new System.Drawing.Point(351, 68);
			this.CalcEdit_amount.Name = "CalcEdit_amount";
			this.CalcEdit_amount.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.CalcEdit_amount.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_amount.Properties.EditFormat.FormatString = "{0:f2}";
			this.CalcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_amount.Properties.Mask.BeepOnError = true;
			this.CalcEdit_amount.Properties.Mask.EditMask = "c";
			this.CalcEdit_amount.Properties.Precision = 2;
			this.CalcEdit_amount.Size = new System.Drawing.Size(100, 20);
			this.CalcEdit_amount.TabIndex = 6;
			//
			//LabelControl_ending_balance
			//
			this.LabelControl_ending_balance.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_ending_balance.Appearance.Options.UseTextOptions = true;
			this.LabelControl_ending_balance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl_ending_balance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_ending_balance.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.LabelControl_ending_balance.LineColor = System.Drawing.Color.Transparent;
			this.LabelControl_ending_balance.Location = new System.Drawing.Point(351, 98);
			this.LabelControl_ending_balance.Name = "LabelControl_ending_balance";
			this.LabelControl_ending_balance.Size = new System.Drawing.Size(85, 13);
			this.LabelControl_ending_balance.TabIndex = 8;
			this.LabelControl_ending_balance.Text = "$0.00";
			this.LabelControl_ending_balance.UseMnemonic = false;
			//
			//LabelControl8
			//
			this.LabelControl8.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.LabelControl8.Location = new System.Drawing.Point(12, 152);
			this.LabelControl8.Name = "LabelControl8";
			this.LabelControl8.Size = new System.Drawing.Size(102, 13);
			this.LabelControl8.TabIndex = 9;
			this.LabelControl8.Text = "Reason for the check";
			//
			//ComboBoxEdit_reason
			//
			this.ComboBoxEdit_reason.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.ComboBoxEdit_reason.Location = new System.Drawing.Point(120, 149);
			this.ComboBoxEdit_reason.Name = "ComboBoxEdit_reason";
			this.ComboBoxEdit_reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit_reason.Properties.MaxLength = 50;
			this.ComboBoxEdit_reason.Size = new System.Drawing.Size(331, 20);
			this.ComboBoxEdit_reason.TabIndex = 10;
			//
			//ComboBoxEdit_disposition
			//
			this.ComboBoxEdit_disposition.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.ComboBoxEdit_disposition.Location = new System.Drawing.Point(120, 175);
			this.ComboBoxEdit_disposition.Name = "ComboBoxEdit_disposition";
			this.ComboBoxEdit_disposition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit_disposition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.ComboBoxEdit_disposition.Size = new System.Drawing.Size(331, 20);
			this.ComboBoxEdit_disposition.TabIndex = 12;
			//
			//LabelControl9
			//
			this.LabelControl9.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.LabelControl9.Location = new System.Drawing.Point(12, 178);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(83, 13);
			this.LabelControl9.TabIndex = 11;
			this.LabelControl9.Text = "Check Disposition";
			//
			//SimpleButton_ok
			//
			this.SimpleButton_ok.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_ok.Enabled = false;
			this.SimpleButton_ok.Location = new System.Drawing.Point(142, 221);
			this.SimpleButton_ok.Name = "SimpleButton_ok";
			this.SimpleButton_ok.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.TabIndex = 13;
			this.SimpleButton_ok.Text = "&Record";
			//
			//SimpleButton_cancel
			//
			this.SimpleButton_cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_cancel.CausesValidation = false;
			this.SimpleButton_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_cancel.Location = new System.Drawing.Point(245, 221);
			this.SimpleButton_cancel.Name = "SimpleButton_cancel";
			this.SimpleButton_cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.TabIndex = 14;
			this.SimpleButton_cancel.Text = "&Cancel";
			//
			//ClientID1
			//
			this.ClientID1.Location = new System.Drawing.Point(85, 10);
			this.ClientID1.Name = "ClientID1";
			this.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.ClientID1.Properties.Appearance.Options.UseTextOptions = true;
			this.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ClientID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("ClientID2.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a client ID", "search", null, true) });
			this.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.ClientID1.Properties.DisplayFormat.FormatString = "0000000";
			this.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ClientID1.Properties.EditFormat.FormatString = "f0";
			this.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.ClientID1.Properties.Mask.BeepOnError = true;
			this.ClientID1.Properties.Mask.EditMask = "\\d*";
			this.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.ClientID1.Properties.ValidateOnEnterKey = true;
			this.ClientID1.Size = new System.Drawing.Size(100, 20);
			this.ClientID1.TabIndex = 1;
			//
			//Form1
			//
			this.AcceptButton = this.SimpleButton_ok;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButton_cancel;
			this.ClientSize = new System.Drawing.Size(463, 266);
			this.Controls.Add(this.LabelControl_ending_balance);
			this.Controls.Add(this.CalcEdit_amount);
			this.Controls.Add(this.LabelControl_current_balance);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.SimpleButton_cancel);
			this.Controls.Add(this.SimpleButton_ok);
			this.Controls.Add(this.LabelControl9);
			this.Controls.Add(this.ComboBoxEdit_disposition);
			this.Controls.Add(this.ComboBoxEdit_reason);
			this.Controls.Add(this.LabelControl8);
			this.Controls.Add(this.LabelControl_Address);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.ClientID1);
			this.Name = "Form1";
			this.Text = "Client Refund Check Entry";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_amount.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_reason.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_disposition.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_Address;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl5;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_current_balance;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_amount;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_ending_balance;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl8;
		protected internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_reason;
		protected internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_disposition;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl9;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_ok;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_cancel;
		protected internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID1;
	}
}
