using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Creditor.Widgets.Controls;
using DebtPlus.UI.Creditor.Widgets;

namespace DebtPlus.UI.Desktop.CS.Debt.Reassign
{
	partial class Form_Reassign_EditTemplate : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			DevExpress.Utils.SuperToolTip SuperToolTip1 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipTitleItem ToolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
			DevExpress.Utils.ToolTipItem ToolTipItem1 = new DevExpress.Utils.ToolTipItem();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Reassign_EditTemplate));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.lbl_new_creditor_name = new DevExpress.XtraEditors.LabelControl();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.CreditorID2 = new CreditorID();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID2.Properties).BeginInit();
			this.SuspendLayout();
			//
			//lbl_new_creditor_name
			//
			this.lbl_new_creditor_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_new_creditor_name.Appearance.Options.UseTextOptions = true;
			this.lbl_new_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
			this.lbl_new_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.lbl_new_creditor_name.Location = new System.Drawing.Point(209, 42);
			this.lbl_new_creditor_name.Name = "lbl_new_creditor_name";
			this.lbl_new_creditor_name.Size = new System.Drawing.Size(0, 13);
			this.lbl_new_creditor_name.TabIndex = 4;
			this.lbl_new_creditor_name.UseMnemonic = false;
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(205, 84);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 6;
			this.Button_Cancel.Text = "&Cancel";
			//
			//Button_OK
			//
			this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Button_OK.Location = new System.Drawing.Point(101, 84);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 5;
			this.Button_OK.Text = "&OK";
			//
			//TextEdit1
			//
			this.TextEdit1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.TextEdit1.Location = new System.Drawing.Point(103, 12);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Size = new System.Drawing.Size(246, 20);
			ToolTipTitleItem1.Text = "Account Number Mask";
			ToolTipItem1.LeftIndent = 6;
			ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text");
			SuperToolTip1.Items.Add(ToolTipTitleItem1);
			SuperToolTip1.Items.Add(ToolTipItem1);
			this.TextEdit1.SuperTip = SuperToolTip1;
			this.TextEdit1.TabIndex = 1;
			//
			//CreditorID2
			//
			this.CreditorID2.EditValue = null;
			this.CreditorID2.Location = new System.Drawing.Point(103, 39);
			this.CreditorID2.Name = "CreditorID2";
			this.CreditorID2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("CreditorID2.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a creditor ID", "search", null, true) });
			this.CreditorID2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.CreditorID2.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.CreditorID2.Properties.Mask.BeepOnError = true;
			this.CreditorID2.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
			this.CreditorID2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.CreditorID2.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.CreditorID2.Properties.MaxLength = 10;
			this.CreditorID2.Size = new System.Drawing.Size(100, 20);
			this.CreditorID2.TabIndex = 3;
			this.CreditorID2.ValidationLevel = CreditorID.ValidationLevelEnum.NotProhibitUse;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 15);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(79, 13);
			this.LabelControl3.TabIndex = 0;
			this.LabelControl3.Text = "Account Number";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(12, 42);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(63, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "New Creditor";

			//
			//Form_Reassign_EditTemplate
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(361, 119);
			this.Controls.Add(this.lbl_new_creditor_name);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.Controls.Add(this.TextEdit1);
			this.Controls.Add(this.CreditorID2);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LabelControl2);
			this.Name = "Form_Reassign_EditTemplate";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Reassign Debts";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID2.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		protected internal DevExpress.XtraEditors.LabelControl lbl_new_creditor_name;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit1;
		protected internal CreditorID CreditorID2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
	}
}
