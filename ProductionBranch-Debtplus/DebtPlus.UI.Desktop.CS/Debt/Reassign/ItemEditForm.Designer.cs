using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Creditor.Widgets.Controls;
using DebtPlus.UI.Creditor.Widgets;

namespace DebtPlus.UI.Desktop.CS.Debt.Reassign
{
	partial class ItemEditForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ItemEditForm));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.CreditorID1 = new CreditorID();
			this.lbl_old_creditor_name = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID2.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//lbl_new_creditor_name
			//
			this.lbl_new_creditor_name.Appearance.Options.UseTextOptions = true;
			this.lbl_new_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
			this.lbl_new_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.lbl_new_creditor_name.Location = new System.Drawing.Point(215, 69);
			this.lbl_new_creditor_name.Size = new System.Drawing.Size(77, 13);
			this.lbl_new_creditor_name.TabIndex = 7;
			this.lbl_new_creditor_name.Text = "XXXX NEW XXXX";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Location = new System.Drawing.Point(227, 118);
			this.Button_Cancel.TabIndex = 9;
			//
			//Button_OK
			//
			this.Button_OK.Location = new System.Drawing.Point(123, 118);
			this.Button_OK.TabIndex = 8;
			//
			//TextEdit1
			//
			this.TextEdit1.Location = new System.Drawing.Point(103, 39);
			this.TextEdit1.Size = new System.Drawing.Size(294, 20);
			this.TextEdit1.TabIndex = 4;
			//
			//CreditorID2
			//
			this.CreditorID2.Location = new System.Drawing.Point(103, 65);
			this.CreditorID2.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.CreditorID2.Properties.Mask.BeepOnError = true;
			this.CreditorID2.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
			this.CreditorID2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.CreditorID2.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.CreditorID2.TabIndex = 6;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 43);
			this.LabelControl3.TabIndex = 3;
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(12, 69);
			this.LabelControl2.TabIndex = 5;

			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(12, 17);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(58, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Old Creditor";
			//
			//CreditorID1
			//
			this.CreditorID1.EditValue = null;
			this.CreditorID1.Location = new System.Drawing.Point(103, 13);
			this.CreditorID1.Name = "CreditorID1";
			this.CreditorID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("CreditorID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a creditor ID", "search", null, true) });
			this.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.CreditorID1.Properties.Mask.BeepOnError = true;
			this.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
			this.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.CreditorID1.Properties.MaxLength = 10;
			this.CreditorID1.Size = new System.Drawing.Size(100, 20);
			this.CreditorID1.TabIndex = 1;
			//
			//lbl_old_creditor_name
			//
			this.lbl_old_creditor_name.Location = new System.Drawing.Point(215, 17);
			this.lbl_old_creditor_name.Name = "lbl_old_creditor_name";
			this.lbl_old_creditor_name.Size = new System.Drawing.Size(68, 13);
			this.lbl_old_creditor_name.TabIndex = 2;
			this.lbl_old_creditor_name.Text = "XXX OLD XXXX";
			this.lbl_old_creditor_name.UseMnemonic = false;
			//
			//ItemEditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(409, 153);
			this.Controls.Add(this.lbl_old_creditor_name);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.CreditorID1);
			this.Name = "ItemEditForm";
			this.Text = "Assignment";
			this.Controls.SetChildIndex(this.CreditorID1, 0);
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.lbl_new_creditor_name, 0);
			this.Controls.SetChildIndex(this.TextEdit1, 0);
			this.Controls.SetChildIndex(this.LabelControl3, 0);
			this.Controls.SetChildIndex(this.LabelControl2, 0);
			this.Controls.SetChildIndex(this.CreditorID2, 0);
			this.Controls.SetChildIndex(this.Button_OK, 0);
			this.Controls.SetChildIndex(this.Button_Cancel, 0);
			this.Controls.SetChildIndex(this.lbl_old_creditor_name, 0);
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID2.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal CreditorID CreditorID1;
		protected internal DevExpress.XtraEditors.LabelControl lbl_old_creditor_name;
	}
}
