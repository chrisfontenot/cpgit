#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;
using DebtPlus.UI.Creditor.Widgets.Controls;

namespace DebtPlus.UI.Desktop.CS.Debt.Reassign
{
    internal partial class ItemEditForm : Form_Reassign_EditTemplate
    {
        /// <summary>
        /// The "Normal" way to create an instance of our class
        /// </summary>
        public ItemEditForm() : base()
        {
            InitializeComponent();
        }

        public ItemEditForm(System.Data.DataRowView drv) : base(drv)
        {
            InitializeComponent();
            this.Load += ItemEditForm_Load;
        }

        /// <summary>
        /// Load the form with the current information
        /// </summary>

        private void ItemEditForm_Load(object sender, System.EventArgs e)
        {
            var _with1 = CreditorID1;
            if (drv != null)
            {
                _with1.DataBindings.Clear();
                _with1.DataBindings.Add(new Binding("EditValue", drv, "old_creditor"));
            }
            lbl_old_creditor_name.Text = CreditorLabel(_with1.EditValue);
            _with1.EditValueChanged += EditValueChanged;
            _with1.Validated += CreditorID1_Validated;
        }

        /// <summary>
        /// Enable or disable the OK button
        /// </summary>
        protected override bool HasErrors()
        {
            return base.HasErrors() || !ValidOldCreditor();
        }

        /// <summary>
        /// The new creditor is completly passed the input test. Look it up.
        /// </summary>
        protected void CreditorID1_Validated(object sender, System.EventArgs e)
        {
            var _with2 = (CreditorID)sender;
            lbl_old_creditor_name.Text = CreditorLabel(_with2.EditValue);
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Validate the old creditor field
        /// </summary>
        private bool ValidOldCreditor()
        {
            bool answer = true;
            if (lbl_old_creditor_name.Text == string.Empty)
                answer = false;
            return answer;
        }
    }
}