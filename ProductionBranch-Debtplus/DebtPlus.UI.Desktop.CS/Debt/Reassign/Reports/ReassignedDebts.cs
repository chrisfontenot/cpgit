using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Reports.Template;
using DevExpress.XtraReports.UI;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Debt.Reassign.Reports
{
    internal partial class ReassignedDebtsReport : TemplateXtraReportClass
    {
        private XmlArgParser ap = null;

        public ReassignedDebtsReport(XmlArgParser ap) : this()
        {
            this.ap = ap;
            XrLabel_old_creditor.BeforePrint += XrLabel_old_creditor_BeforePrint;
            new_creditor.BeforePrint += new_creditor_BeforePrint;
            client_id_and_name.BeforePrint += client_id_and_name_BeforePrint;
        }

        /// <summary>
        /// Title associated with the report
        /// </summary>
        public override string ReportTitle
        {
            get { return "Reassigned Debt List"; }
        }

        /// <summary>
        /// Create the report object
        /// </summary>
        public ReassignedDebtsReport() : base()
        {
            InitializeComponent();
        }

        #region " Component Designer generated code "

        //Component overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Designer
        //It can be modified using the Designer.
        //Do not modify it using the code editor.
        internal GroupHeaderBand GroupHeader1;

        internal GroupFooterBand GroupFooter1;
        internal XRPanel XrPanel1;
        internal XRLabel XrLabel_old_creditor;
        internal XRLabel XrLabel2;
        internal XRLabel XrLabel1;
        internal XRLabel XrLabel3;
        internal XRLabel new_creditor;
        internal XRLabel client_id_and_name;
        internal XRLabel account_number;
        internal XRLabel XrLabel5;
        internal XRLabel XrLabel_Balance;
        internal XRLabel XrLabel_debt_count;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.XtraReports.UI.XRSummary XrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.XrLabel_old_creditor = new DevExpress.XtraReports.UI.XRLabel();
            this.XrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.XrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.XrLabel_debt_count = new DevExpress.XtraReports.UI.XRLabel();
            this.new_creditor = new DevExpress.XtraReports.UI.XRLabel();
            this.client_id_and_name = new DevExpress.XtraReports.UI.XRLabel();
            this.account_number = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_Balance = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            //
            //Detail
            //
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
                this.XrLabel_Balance,
                this.account_number,
                this.client_id_and_name,
                this.new_creditor
            });
            this.Detail.HeightF = 16f;
            //
            //PageHeader
            //
            this.PageHeader.HeightF = 122f;
            //
            //XrLabel_Title
            //
            this.XrLabel_Title.StylePriority.UseFont = false;
            //
            //XrPageInfo_PageNumber
            //
            this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
            //
            //XRLabel_Agency_Name
            //
            this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address3
            //
            this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address1
            //
            this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Phone
            //
            this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
            this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address2
            //
            this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
            //
            //GroupHeader1
            //
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
                this.XrLabel_old_creditor,
                this.XrPanel1
            });
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WholePage;
            this.GroupHeader1.HeightF = 56f;
            this.GroupHeader1.Name = "GroupHeader1";
            this.GroupHeader1.RepeatEveryPage = true;
            //
            //XrLabel_old_creditor
            //
            this.XrLabel_old_creditor.CanGrow = false;
            this.XrLabel_old_creditor.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrLabel_old_creditor.ForeColor = System.Drawing.Color.Maroon;
            this.XrLabel_old_creditor.LocationFloat = new DevExpress.Utils.PointFloat(0f, 8f);
            this.XrLabel_old_creditor.Name = "XrLabel_old_creditor";
            this.XrLabel_old_creditor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_old_creditor.SizeF = new System.Drawing.SizeF(800f, 17f);
            //
            //XrPanel1
            //
            this.XrPanel1.BackColor = System.Drawing.Color.Teal;
            this.XrPanel1.BorderColor = System.Drawing.Color.Teal;
            this.XrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
                this.XrLabel5,
                this.XrLabel3,
                this.XrLabel1,
                this.XrLabel2
            });
            this.XrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 33f);
            this.XrPanel1.Name = "XrPanel1";
            this.XrPanel1.SizeF = new System.Drawing.SizeF(800f, 17f);
            //
            //XrLabel5
            //
            this.XrLabel5.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrLabel5.ForeColor = System.Drawing.Color.White;
            this.XrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(692f, 0f);
            this.XrLabel5.Name = "XrLabel5";
            this.XrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel5.SizeF = new System.Drawing.SizeF(100f, 16f);
            this.XrLabel5.StylePriority.UseTextAlignment = false;
            this.XrLabel5.Text = "BALANCE";
            this.XrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel3
            //
            this.XrLabel3.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrLabel3.ForeColor = System.Drawing.Color.White;
            this.XrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(508f, 0f);
            this.XrLabel3.Name = "XrLabel3";
            this.XrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel3.SizeF = new System.Drawing.SizeF(117f, 16f);
            this.XrLabel3.Text = "NEW CREDITOR";
            //
            //XrLabel1
            //
            this.XrLabel1.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrLabel1.ForeColor = System.Drawing.Color.White;
            this.XrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 0f);
            this.XrLabel1.Name = "XrLabel1";
            this.XrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel1.SizeF = new System.Drawing.SizeF(159f, 16f);
            this.XrLabel1.Text = "Client ID AND NAME";
            //
            //XrLabel2
            //
            this.XrLabel2.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrLabel2.ForeColor = System.Drawing.Color.White;
            this.XrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(267f, 0f);
            this.XrLabel2.Name = "XrLabel2";
            this.XrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel2.SizeF = new System.Drawing.SizeF(141f, 16f);
            this.XrLabel2.Text = "ACCOUNT NUMBER";
            //
            //GroupFooter1
            //
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] { this.XrLabel_debt_count });
            this.GroupFooter1.HeightF = 52f;
            this.GroupFooter1.Name = "GroupFooter1";
            //
            //XrLabel_debt_count
            //
            this.XrLabel_debt_count.Font = new System.Drawing.Font("Times New Roman", 9.75f, System.Drawing.FontStyle.Bold);
            this.XrLabel_debt_count.ForeColor = System.Drawing.Color.Maroon;
            this.XrLabel_debt_count.LocationFloat = new DevExpress.Utils.PointFloat(0f, 17f);
            this.XrLabel_debt_count.Name = "XrLabel_debt_count";
            this.XrLabel_debt_count.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_debt_count.SizeF = new System.Drawing.SizeF(550f, 16f);
            XrSummary1.FormatString = "{0:n0} Account(s) are to be reassigned";
            XrSummary1.Func = DevExpress.XtraReports.UI.SummaryFunc.Count;
            XrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.XrLabel_debt_count.Summary = XrSummary1;
            this.XrLabel_debt_count.Text = "Count of debts to be reassigned";
            //
            //new_creditor
            //
            this.new_creditor.CanGrow = false;
            this.new_creditor.ForeColor = System.Drawing.Color.Black;
            this.new_creditor.LocationFloat = new DevExpress.Utils.PointFloat(508f, 0f);
            this.new_creditor.Name = "new_creditor";
            this.new_creditor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.new_creditor.SizeF = new System.Drawing.SizeF(175f, 16f);
            //
            //client_id_and_name
            //
            this.client_id_and_name.CanGrow = false;
            this.client_id_and_name.ForeColor = System.Drawing.Color.Black;
            this.client_id_and_name.LocationFloat = new DevExpress.Utils.PointFloat(0f, 0f);
            this.client_id_and_name.Name = "client_id_and_name";
            this.client_id_and_name.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.client_id_and_name.SizeF = new System.Drawing.SizeF(242f, 16f);
            //
            //account_number
            //
            this.account_number.CanGrow = false;
            this.account_number.ForeColor = System.Drawing.Color.Black;
            this.account_number.LocationFloat = new DevExpress.Utils.PointFloat(267f, 0f);
            this.account_number.Name = "account_number";
            this.account_number.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.account_number.SizeF = new System.Drawing.SizeF(233f, 16f);
            //
            //XrLabel_Balance
            //
            this.XrLabel_Balance.CanGrow = false;
            this.XrLabel_Balance.ForeColor = System.Drawing.Color.Black;
            this.XrLabel_Balance.LocationFloat = new DevExpress.Utils.PointFloat(684f, 0f);
            this.XrLabel_Balance.Name = "XrLabel_Balance";
            this.XrLabel_Balance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_Balance.SizeF = new System.Drawing.SizeF(108f, 16f);
            this.XrLabel_Balance.StylePriority.UseTextAlignment = false;
            this.XrLabel_Balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            this.account_number.DataBindings.Add("Text", null, "account_number");
            this.XrLabel_Balance.DataBindings.Add("Text", null, "balance", "{0:c}");
            this.XrLabel_debt_count.DataBindings.Add("Text", null, "client");
            //
            //ReassignedDebtsReport
            //
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
                this.Detail,
                this.PageHeader,
                this.PageFooter,
                this.TopMarginBand1,
                this.BottomMarginBand1,
                this.GroupHeader1,
                this.GroupFooter1
            });
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
                this.XrControlStyle_Header,
                this.XrControlStyle_Totals,
                this.XrControlStyle_HeaderPannel,
                this.XrControlStyle_GroupHeader
            });
            this.Version = "11.2";
            this.Controls.SetChildIndex(this.GroupFooter1, 0);
            this.Controls.SetChildIndex(this.GroupHeader1, 0);
            this.Controls.SetChildIndex(this.BottomMarginBand1, 0);
            this.Controls.SetChildIndex(this.TopMarginBand1, 0);
            this.Controls.SetChildIndex(this.PageFooter, 0);
            this.Controls.SetChildIndex(this.PageHeader, 0);
            this.Controls.SetChildIndex(this.Detail, 0);
            ((System.ComponentModel.ISupportInitialize)this).EndInit();
        }

        #endregion " Component Designer generated code "

        /// <summary>
        /// Print the old creditor ID
        /// </summary>
        private void XrLabel_old_creditor_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var _with1 = (XRLabel)sender;
            XtraReport rpt = (XtraReport)_with1.Report;
            _with1.Text = CreditorLabel(rpt.GetCurrentColumnValue("old_creditor"));
        }

        /// <summary>
        /// Print the new creditor ID
        /// </summary>
        private void new_creditor_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var _with2 = (XRLabel)sender;
            XtraReport rpt = (XtraReport)_with2.Report;
            _with2.Text = CreditorLabel(rpt.GetCurrentColumnValue("new_creditor"));
        }

        /// <summary>
        /// Print the client ID
        /// </summary>
        private void client_id_and_name_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var _with3 = (XRLabel)sender;
            XtraReport rpt = (XtraReport)_with3.Report;
            _with3.Text = ClientLabel(rpt.GetCurrentColumnValue("client"));
        }

        /// <summary>
        /// Retrieve the information for the creditor ID
        /// </summary>
        private string CreditorLabel(object creditorid)
        {
            string answer = string.Empty;

            // Conver the input to a string and find the row in the creditor table which is a match to the desired value.
            string Creditor = null;
            if (creditorid != null && !object.ReferenceEquals(creditorid, System.DBNull.Value))
            {
                Creditor = Convert.ToString(creditorid);

                DataRow row = null;
                if (Creditor != string.Empty)
                {
                    row = CreditorRow(Creditor);

                    // If there is a row then return the creditor and the name
                    if (row != null)
                    {
                        answer = string.Format("[{0}] {1}", Creditor, row["creditor_name"]);
                    }
                    else
                    {
                        answer = string.Format("[{0}]", Creditor);
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// Database access to find the creditor row in the table
        /// </summary>
        private DataRow CreditorRow(string Creditor)
        {
            const string TableName = "creditors";
            DataTable tbl = ap.ds.Tables[TableName];

            if (tbl != null)
            {
                DataRow row = tbl.Rows.Find(Creditor);
                if (row != null)
                    return row;
            }

            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with4 = cmd;
                    _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with4.CommandText = "SELECT creditor, creditor_name FROM creditors WITH (NOLOCK) WHERE [creditor] = @creditor";
                    _with4.CommandType = CommandType.Text;
                    _with4.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ap.ds, TableName);
                        tbl = ap.ds.Tables[TableName];

                        // Ensure that there is a primary key to the table
                        if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                        {
                            tbl.PrimaryKey = new DataColumn[] { tbl.Columns["creditor"] };
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor information");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // Do the search operation again
            if (tbl != null)
            {
                DataRow row = tbl.Rows.Find(Creditor);
                if (row != null)
                    return row;
            }

            // Return the failure condition
            return null;
        }

        /// <summary>
        /// Retrieve the information for the client ID
        /// </summary>
        private string ClientLabel(object clientid)
        {
            string answer = string.Empty;

            // Conver the input to a string and find the row in the client table which is a match to the desired value.
            Int32 Client = default(Int32);
            if (clientid != null && !object.ReferenceEquals(clientid, System.DBNull.Value))
            {
                Client = Convert.ToInt32(clientid);
                DataRow row = ClientRow(Client);

                // If there is a row then return the client and the name
                if (row != null)
                {
                    answer = string.Format("[{0}] {1}", string.Format("{0:0000000}", Client), row["Client_name"]);
                }
                else
                {
                    answer = string.Format("[{0}]", string.Format("{0:0000000}", Client));
                }
            }

            return answer;
        }

        /// <summary>
        /// Database access to find the creditor row in the table
        /// </summary>
        private DataRow ClientRow(Int32 Client)
        {
            const string TableName = "clients";
            DataTable tbl = ap.ds.Tables[TableName];

            if (tbl != null)
            {
                DataRow row = tbl.Rows.Find(Client);
                if (row != null)
                    return row;
            }

            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with5 = cmd;
                    _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with5.CommandText = "SELECT client, name as client_name FROM view_client_address WITH (NOLOCK) WHERE [client] = @client";
                    _with5.CommandType = CommandType.Text;
                    _with5.Parameters.Add("@client", SqlDbType.Int).Value = Client;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ap.ds, TableName);
                        tbl = ap.ds.Tables[TableName];

                        // Ensure that there is a primary key to the table
                        if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                        {
                            tbl.PrimaryKey = new DataColumn[] { tbl.Columns["client"] };
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // Do the search operation again
            if (tbl != null)
            {
                DataRow row = tbl.Rows.Find(Client);
                if (row != null)
                    return row;
            }

            // Return the failure condition
            return null;
        }
    }
}