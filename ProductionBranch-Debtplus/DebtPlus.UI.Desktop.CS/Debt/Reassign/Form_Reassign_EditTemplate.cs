using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Creditor.Widgets.Controls;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Debt.Reassign
{
    internal partial class Form_Reassign_EditTemplate
    {
        protected System.Data.DataRowView drv = null;

        /// <summary>
        /// The "Normal" way to create an instance of our class
        /// </summary>
        public Form_Reassign_EditTemplate() : base()
        {
            InitializeComponent();
        }

        public Form_Reassign_EditTemplate(System.Data.DataRowView drv) : this()
        {
            this.drv = drv;

            this.Load += ItemEditForm_Load;
        }

        /// <summary>
        /// Load the form with the current information
        /// </summary>
        private void ItemEditForm_Load(object sender, System.EventArgs e)
        {
            var _with1 = TextEdit1;
            if (drv != null)
            {
                _with1.DataBindings.Clear();
                _with1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", drv, "account_number"));
            }
            _with1.EditValueChanged += EditValueChanged;

            var _with2 = CreditorID2;
            if (drv != null)
            {
                _with2.DataBindings.Clear();
                _with2.DataBindings.Add(new Binding("EditValue", drv, "new_creditor"));
            }
            lbl_new_creditor_name.Text = CreditorLabel(_with2.EditValue);
            _with2.EditValueChanged += EditValueChanged;
            _with2.Validated += CreditorID2_Validated;

            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Handle a change in the field
        /// </summary>
        protected void EditValueChanged(object sender, System.EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the form has an error
        /// </summary>
        protected virtual bool HasErrors()
        {
            if (!ValidNewCreditor())
            {
                return true;
            }
            if (!ValidMask())
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Validate the new creditor field
        /// </summary>
        protected bool ValidNewCreditor()
        {
            bool answer = true;
            if (lbl_new_creditor_name.Text == string.Empty)
                answer = false;
            return answer;
        }

        /// <summary>
        /// Validate the account number mask field
        /// </summary>
        protected bool ValidMask()
        {
            string MaskString = TextEdit1.Text.Trim();

            // There must be at least two characters or it is not acceptable
            if (MaskString.Length >= 1)
            {
                // Wild card characters are acceptable anywhere
                if (MaskString.IndexOfAny(new char[] { '%', '_' }) >= 0)
                {
                    return true;
                }

                // Look for a [....] syntax
                System.Int32 First = MaskString.IndexOf('[');
                if (First >= 0)
                {
                    if (MaskString.IndexOf(']', First) > 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// The new creditor is completely passed the input test. Look it up.
        /// </summary>
        protected void CreditorID2_Validated(object sender, System.EventArgs e)
        {
            var _with3 = (CreditorID)sender;
            lbl_new_creditor_name.Text = CreditorLabel(_with3.EditValue);
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Retrieve the information for the creditor ID
        /// </summary>
        private System.Data.DataSet dsLocal = new System.Data.DataSet("dsLocal");

        protected string CreditorLabel(string old_creditor_id)
        {
            string answer = string.Empty;

            // If there is a creditor then try to find the creditor name
            if (old_creditor_id != string.Empty)
            {
                System.Data.DataView creditor_view = null;
                System.Data.DataTable creditors = dsLocal.Tables["creditors"];

                if (creditors != null)
                {
                    creditor_view = new System.Data.DataView(creditors, string.Format("[creditor]='{0}'", old_creditor_id), string.Empty, DataViewRowState.CurrentRows);
                    if (creditor_view.Count == 0)
                    {
                        creditor_view = null;
                    }
                }

                // If the creditor is not in the cache, load it.
                if (creditor_view == null)
                {
                    System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                    var _with4 = cmd;
                    _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with4.CommandText = "SELECT creditor,creditor_name FROM creditors WITH (NOLOCK) WHERE creditor = @creditor";
                    _with4.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = old_creditor_id;

                    System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                    try
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                        da.Fill(dsLocal, "creditors");
                        creditors = dsLocal.Tables["creditors"];
                        if (creditors != null)
                        {
                            creditor_view = new System.Data.DataView(creditors, string.Format("[creditor]='{0}'", old_creditor_id), string.Empty, DataViewRowState.CurrentRows);
                        }
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor information");
                    }
                    finally
                    {
                        System.Windows.Forms.Cursor.Current = current_cursor;
                    }
                }

                // Find the one item for the view of this creditor ID
                if (creditor_view != null && creditor_view.Count > 0)
                {
                    System.Data.DataRowView drv = creditor_view[0];
                    if (drv != null && drv["creditor_name"] != null && !object.ReferenceEquals(drv["creditor_name"], System.DBNull.Value))
                    {
                        answer = Convert.ToString(drv["creditor_name"]);
                    }
                }
            }

            return answer;
        }
    }
}