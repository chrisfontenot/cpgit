#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace DebtPlus.UI.Desktop.CS.Debt.Reassign
{
    internal partial class Form1 : DebtPlus.Data.Forms.DebtPlusForm
    {
        private System.Data.DataView vue_change_list;
        private System.ComponentModel.BackgroundWorker bt = new System.ComponentModel.BackgroundWorker();

        public Form1()
            : base()
        {
            InitializeComponent();
        }

        internal XmlArgParser ap = null;

        public Form1(XmlArgParser ap) : this()
        {
            this.ap = ap;

            Button_Cancel.Click += Button_Cancel_Click;
            Button_OK.Click += Button_OK_Click;
            MenuItem_Add.Click += MenuItem_Add_Click;
            MenuItem_Change.Click += MenuItem_Change_Click;
            MenuItem_Delete.Click += MenuItem_Delete_Click;
            MenuItem_Print.Click += MenuItem_Print_Click;
            bt.DoWork += bt_DoWork;
            ContextMenu1.Popup += ContextMenu1_Popup;
            BarButtonItem_File_Print.ItemClick += BarButtonItem_File_Print_ItemClick;
            BarButtonItem_File_Exit.ItemClick += BarButtonItem_File_Exit_ItemClick;
            this.Load += Form1_Load;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ContextMenu1 = new System.Windows.Forms.ContextMenu();
            this.MenuItem_Add = new System.Windows.Forms.MenuItem();
            this.MenuItem_Change = new System.Windows.Forms.MenuItem();
            this.MenuItem_Delete = new System.Windows.Forms.MenuItem();
            this.MenuItem4 = new System.Windows.Forms.MenuItem();
            this.MenuItem_Print = new System.Windows.Forms.MenuItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.Bar2 = new DevExpress.XtraBars.Bar();
            this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Print = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.generate_proposals = new DevExpress.XtraEditors.CheckEdit();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col_old_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_old_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_account_number = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_new_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_new_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.PanelControl1).BeginInit();
            this.PanelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.generate_proposals.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //ContextMenu1
            //
            this.ContextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                this.MenuItem_Add,
                this.MenuItem_Change,
                this.MenuItem_Delete,
                this.MenuItem4,
                this.MenuItem_Print
            });
            //
            //MenuItem_Add
            //
            this.MenuItem_Add.Index = 0;
            this.MenuItem_Add.Text = "&Add";
            //
            //MenuItem_Change
            //
            this.MenuItem_Change.Index = 1;
            this.MenuItem_Change.Text = "&Change";
            //
            //MenuItem_Delete
            //
            this.MenuItem_Delete.Index = 2;
            this.MenuItem_Delete.Text = "&Delete";
            //
            //MenuItem4
            //
            this.MenuItem4.Index = 3;
            this.MenuItem4.Text = "-";
            //
            //MenuItem_Print
            //
            this.MenuItem_Print.Index = 4;
            this.MenuItem_Print.Text = "&Print...";
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
                this.BarSubItem1,
                this.BarButtonItem_File_Print,
                this.BarButtonItem_File_Exit
            });
            this.barManager1.MainMenu = this.Bar2;
            this.barManager1.MaxItemId = 3;
            //
            //Bar2
            //
            this.Bar2.BarName = "Main menu";
            this.Bar2.DockCol = 0;
            this.Bar2.DockRow = 0;
            this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1) });
            this.Bar2.OptionsBar.MultiLine = true;
            this.Bar2.OptionsBar.UseWholeRow = true;
            this.Bar2.Text = "Main menu";
            //
            //BarSubItem1
            //
            this.BarSubItem1.Caption = "&File";
            this.BarSubItem1.Id = 0;
            this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Print),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit)
            });
            this.BarSubItem1.Name = "BarSubItem1";
            //
            //BarButtonItem_File_Print
            //
            this.BarButtonItem_File_Print.Caption = "&Print...";
            this.BarButtonItem_File_Print.Id = 1;
            this.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print";
            //
            //BarButtonItem_File_Exit
            //
            this.BarButtonItem_File_Exit.Caption = "&Exit...";
            this.BarButtonItem_File_Exit.Id = 2;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            //
            //barDockControlTop
            //
            this.ToolTipController1.SetSuperTip(this.barDockControlTop, null);
            //
            //barDockControlBottom
            //
            this.ToolTipController1.SetSuperTip(this.barDockControlBottom, null);
            //
            //barDockControlLeft
            //
            this.ToolTipController1.SetSuperTip(this.barDockControlLeft, null);
            //
            //barDockControlRight
            //
            this.ToolTipController1.SetSuperTip(this.barDockControlRight, null);
            //
            //PanelControl1
            //
            this.PanelControl1.Controls.Add(this.Button_Cancel);
            this.PanelControl1.Controls.Add(this.Button_OK);
            this.PanelControl1.Controls.Add(this.generate_proposals);
            this.PanelControl1.Controls.Add(this.GridControl1);
            this.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelControl1.Location = new System.Drawing.Point(0, 24);
            this.PanelControl1.Name = "PanelControl1";
            this.PanelControl1.Size = new System.Drawing.Size(504, 242);
            this.ToolTipController1.SetSuperTip(this.PanelControl1, null);
            this.PanelControl1.TabIndex = 4;
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(423, 50);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "&Cancel";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(423, 21);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 6;
            this.Button_OK.Text = "&OK";
            //
            //generate_proposals
            //
            this.generate_proposals.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.generate_proposals.Location = new System.Drawing.Point(7, 215);
            this.generate_proposals.Name = "generate_proposals";
            this.generate_proposals.Properties.Caption = "Generate proposals for all of the changes";
            this.generate_proposals.Size = new System.Drawing.Size(392, 19);
            this.generate_proposals.TabIndex = 5;
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.ContextMenu = this.ContextMenu1;
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(9, 9);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(400, 200);
            this.GridControl1.TabIndex = 4;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.col_old_creditor,
                this.col_account_number,
                this.col_new_creditor
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.col_old_creditor, DevExpress.Data.ColumnSortOrder.Ascending) });
            //
            //col_old_creditor
            //
            this.col_old_creditor.Caption = "Creditor";
            this.col_old_creditor.CustomizationCaption = "Old Creditor ID";
            this.col_old_creditor.FieldName = "old_creditor";
            this.col_old_creditor.Name = "col_old_creditor";
            this.col_old_creditor.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.col_old_creditor.ToolTip = "Old creditor label. This is the existing creditor for the debts.";
            this.col_old_creditor.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.col_old_creditor.Visible = true;
            this.col_old_creditor.VisibleIndex = 0;
            this.col_old_creditor.Width = 95;
            //
            //col_account_number
            //
            this.col_account_number.Caption = "Account Number";
            this.col_account_number.CustomizationCaption = "Account Number Mask";
            this.col_account_number.FieldName = "account_number";
            this.col_account_number.Name = "col_account_number";
            this.col_account_number.ToolTip = "This is the mask to find the accounts that are to be reassigned.";
            this.col_account_number.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.col_account_number.Visible = true;
            this.col_account_number.VisibleIndex = 1;
            this.col_account_number.Width = 150;
            //
            //col_new_creditor
            //
            this.col_new_creditor.Caption = "New Creditor";
            this.col_new_creditor.CustomizationCaption = "New Creditor ID";
            this.col_new_creditor.FieldName = "new_creditor";
            this.col_new_creditor.Name = "col_new_creditor";
            this.col_new_creditor.ToolTip = "New creditor. This is the creditor to which the debts are to be assigned.";
            this.col_new_creditor.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.col_new_creditor.Visible = true;
            this.col_new_creditor.VisibleIndex = 2;
            this.col_new_creditor.Width = 151;
            //
            //Form1
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(504, 266);
            this.Controls.Add(this.PanelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Form1";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Perform a reassignment based upon the account number mask";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.PanelControl1).EndInit();
            this.PanelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.generate_proposals.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar Bar2;
        internal DevExpress.XtraBars.BarSubItem BarSubItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Print;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraEditors.PanelControl PanelControl1;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.CheckEdit generate_proposals;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn col_old_creditor;
        internal DevExpress.XtraGrid.Columns.GridColumn col_account_number;
        internal DevExpress.XtraGrid.Columns.GridColumn col_new_creditor;
        internal System.Windows.Forms.ContextMenu ContextMenu1;
        internal System.Windows.Forms.MenuItem MenuItem_Add;
        internal System.Windows.Forms.MenuItem MenuItem_Change;
        internal System.Windows.Forms.MenuItem MenuItem_Delete;
        internal System.Windows.Forms.MenuItem MenuItem4;
        internal System.Windows.Forms.MenuItem MenuItem_Print;

        #endregion " Windows Form Designer generated code "

        private void Form1_Load(object sender, System.EventArgs e)
        {
            // Add the tables to the dataset
            ap.ds.Tables.Add(selection_list_table());
            ap.ds.Tables.Add(debt_list_table());

            // Create an edit view of the change list table for additions and bind it to the grid
            vue_change_list = ap.ds.Tables["change_list"].DefaultView;
            GridControl1.DataSource = vue_change_list;

            // Enable/Disable the OK button
            EnableOK();
        }

        private System.Data.DataTable selection_list_table()
        {
            // Create the list of debts that are to be reassigned
            System.Data.DataTable tbl = new System.Data.DataTable();

            // Define the columns
            var col = tbl.Columns.Add("id", typeof(System.Int32), string.Empty);
            col.Caption = "ID";

            col = tbl.Columns.Add("count", typeof(System.Int32), string.Empty);
            col.Caption = "# of debts";

            col = tbl.Columns.Add("old_creditor", typeof(string), string.Empty);
            col.Caption = "Old Creditor";
            col.MaxLength = 10;

            col = tbl.Columns.Add("old_creditor_name", typeof(string), string.Empty);
            col.Caption = "Old Creditor Name";
            col.MaxLength = 50;

            col = tbl.Columns.Add("account_number", typeof(string), string.Empty);
            col.Caption = "Account Number";
            col.MaxLength = 128;

            col = tbl.Columns.Add("new_creditor", typeof(string), string.Empty);
            col.Caption = "New Creditor";
            col.MaxLength = 10;

            col = tbl.Columns.Add("new_creditor_name", typeof(string), string.Empty);
            col.Caption = "New Creditor Name";
            col.MaxLength = 50;

            // Set the information for the table
            tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns[0] };

            // ID
            tbl.TableName = "change_list";

            return tbl;
        }

        private System.Data.DataTable debt_list_table()
        {
            // Create the list of debts that are to be reassigned
            System.Data.DataTable tbl = new System.Data.DataTable();

            // Define the columns
            var col = tbl.Columns.Add("selection", typeof(System.Int32), string.Empty);
            col.Caption = "Selection";

            col = tbl.Columns.Add("client_creditor", typeof(System.Int32), string.Empty);
            col.Caption = "Debt Record";

            col = tbl.Columns.Add("old_creditor", typeof(string), string.Empty);
            col.Caption = "Old Creditor";
            col.MaxLength = 10;

            col = tbl.Columns.Add("old_creditor_name", typeof(string), string.Empty);
            col.Caption = "Old Creditor Name";
            col.MaxLength = 50;

            col = tbl.Columns.Add("account_number", typeof(string), string.Empty);
            col.Caption = "Account Number";
            col.MaxLength = 128;

            col = tbl.Columns.Add("client", typeof(System.Int32), string.Empty);
            col.Caption = "Client ID";

            col = tbl.Columns.Add("client_name", typeof(string), string.Empty);
            col.Caption = "Client Name";
            col.MaxLength = 128;

            col = tbl.Columns.Add("new_creditor", typeof(string), string.Empty);
            col.Caption = "New Creditor";
            col.MaxLength = 10;

            col = tbl.Columns.Add("new_creditor_name", typeof(string), string.Empty);
            col.Caption = "New Creditor Name";
            col.MaxLength = 50;

            col = tbl.Columns.Add("id", typeof(System.Int32), string.Empty);
            col.AllowDBNull = false;
            col.AutoIncrement = true;
            col.AutoIncrementSeed = 1;
            col.AutoIncrementStep = 1;
            col.Caption = "ID";

            // Set the information for the table
            tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["id"] };
            tbl.TableName = "debt_list";

            return tbl;
        }

        private void Button_Cancel_Click(System.Object sender, System.EventArgs e)
        {
            Close();
        }

        private void Button_OK_Click(System.Object sender, System.EventArgs e)
        {
            if (DebtPlus.Data.Forms.MessageBox.Show("Warning: Are you really sure that you have checked this list?" + Environment.NewLine + "It will be extremely difficult to reverse these changes if they are incorrect." + Environment.NewLine + "This is your last warning. If you press YES, the changes will be committed to the database.", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                bool answer = false;

                // Do the reassignment operation here.
                DevExpress.Utils.WaitDialogForm dlg = new DevExpress.Utils.WaitDialogForm("Processing debt reassignment", "Reassigning debts. Please wait");
                dlg.Show();
                answer = ReassignDebts(ap.ds.Tables["debt_list"]);
                dlg.Close();
                dlg.Dispose();

                // Tell the user that we are complete if the reassignment was successful.
                if (answer)
                {
                    Button_OK.Enabled = false;
                    if (DebtPlus.Data.Forms.MessageBox.Show("The operation completed successfully." + Environment.NewLine + Environment.NewLine + "Do you wish a report of the reassigned debts? (Once this program is terminated, you can no longer get the report.)", "Completed", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                    {
                        bt.RunWorkerCompleted += bt_RunWorkerCompleted;
                        PrintReport();
                        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    }
                    else
                    {
                        Close();
                    }
                }
            }
        }

        private bool ReassignDebts(System.Data.DataTable tbl)
        {
            bool answer = false;
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlTransaction txn = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                cn.Open();
                txn = cn.BeginTransaction();

                // Process the items in the translation table to reassign the debts
                foreach (System.Data.DataRow row in tbl.Rows)
                {
                    answer = ReassignSingleDebt(cn, txn, Convert.ToInt32(row["client_creditor"]), Convert.ToString(row["new_creditor"]), generate_proposals.Checked);
                    if (!answer)
                    {
                        break;
                    }
                }

                // All is complete. Commit the changes to the database.
                if (answer)
                {
                    txn.Commit();
                    txn = null;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reassigning the debts");
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch { }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                {
                    cn.Dispose();
                }
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            return answer;
        }

        private bool ReassignSingleDebt(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, System.Int32 client_creditor, string new_creditor, bool generate_proposals)
        {
            System.Int32 NewRecord = -1;

            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "xpr_sell_debt";
                cmd.CommandType = CommandType.StoredProcedure;

                System.Data.SqlClient.SqlCommandBuilder.DeriveParameters(cmd);
                cmd.Parameters[1].Value = client_creditor;
                cmd.Parameters[2].Value = new_creditor;
                cmd.Parameters[3].Value = generate_proposals ? 1 : 0;

                cmd.ExecuteNonQuery();

                // Find the new debt record for this client
                NewRecord = Convert.ToInt32(cmd.Parameters[0].Value);
            }

            return NewRecord > 0;
        }

        private void EnableOK()
        {
            if (ap.ds.Tables["change_list"].Rows.Count == 0)
            {
                Button_OK.Enabled = false;
                return;
            }
            Button_OK.Enabled = true;
        }

        private void AddRow()
        {
            System.Data.DataRowView drv = vue_change_list.AddNew();
            drv["id"] = -1;
            if (EditRow(drv))
            {
                drv.EndEdit();
            }
            else
            {
                drv.CancelEdit();
            }

            EnableOK();
        }

        // Create the edit form and allow it to be changed

        // If the value is an edit then remove all of the old items from the list
        private System.Int32 EditRow_NewID;

        private bool EditRow(System.Data.DataRowView drv)
        {
            bool answer = false;
            using (var frm = new ItemEditForm(drv))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    answer = true;
                }
            }

            if (answer)
            {
                EditRow_NewID += 1;

                answer = ReadDebtList(drv, EditRow_NewID);
                if (answer)
                {
                    System.Int32 OldID = Convert.ToInt32(drv["id"]);
                    if (OldID > 0)
                    {
                        System.Data.DataView vue = new System.Data.DataView(ap.ds.Tables["debt_list"], string.Format("[selection]={0:f0}", OldID), string.Empty, DataViewRowState.CurrentRows);
                        for (System.Int32 Item = vue.Count - 1; Item >= 0; Item += -1)
                        {
                            vue[Item].Delete();
                        }
                        ap.ds.Tables["debt_list"].AcceptChanges();
                    }
                    drv["id"] = EditRow_NewID;
                }
            }

            return answer;
        }

        private bool ReadDebtList(System.Data.DataRowView drv, System.Int32 ID)
        {
            bool answer = false;

            // Build the list of debts that are to be reassigned with the OLDID as the key to the origination string
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("SELECT ");
            sb.AppendFormat("{0:f0} AS selection, ", ID);
            sb.Append("cc.client_creditor AS client_creditor, ");

            sb.Append("cc.creditor AS old_creditor, ");
            sb.Append("cro.creditor_name as old_creditor_name, ");

            sb.Append("cc.account_number as account_number, ");

            sb.Append("cc.client as client, ");
            sb.Append("isnull(dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),'') as client_name, ");

            sb.AppendFormat("'{0}' as new_creditor, ", Convert.ToString(drv["new_creditor"]).Replace("'", "''"));
            sb.Append("crn.creditor_name as new_creditor_name, ");
            sb.Append("bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as balance ");

            sb.Append("FROM client_creditor cc WITH (NOLOCK) ");
            sb.Append("INNER JOIN clients c WITH (NOLOCK) ON cc.client = c.client ");
            sb.Append("INNER JOIN client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance ");
            sb.Append("LEFT OUTER JOIN people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation ");
            sb.Append("LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name ");
            sb.Append("LEFT OUTER JOIN creditors cro WITH (NOLOCK) ON cc.creditor = cro.creditor ");
            sb.AppendFormat("LEFT OUTER JOIN creditors crn WITH (NOLOCK) ON '{0}' = crn.creditor ", Convert.ToString(drv["new_creditor"]).Replace("'", "''"));

            sb.Append("WHERE c.active_status NOT IN ('CRE','WKS','I') ");
            sb.Append("AND isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) > isnull(bal.total_payments,0) ");
            sb.AppendFormat("AND cc.creditor = '{0}' ", Convert.ToString(drv["old_creditor"]).Replace("'", "''"));
            sb.Append("AND isnull(cc.reassigned_debt,0) = 0");

            // If we are not accepting all account numbers then filter it by the account number.
            string Mask = Convert.ToString(drv["account_number"]).Replace("'", "''");
            if (Mask != "%")
            {
                sb.AppendFormat(" AND cc.account_number LIKE '{0}'", Mask);
            }

            sb.Append(";");

            cmd.CommandText = sb.ToString();

            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;

            try
            {
                // Tell the user that we are reading the debt list. This may take a moment.
                DevExpress.Utils.WaitDialogForm dlg = new DevExpress.Utils.WaitDialogForm("Reading debt list");
                dlg.Show();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                drv["count"] = da.Fill(ap.ds, "debt_list");

                // Close and dispose of the alert window
                dlg.Close();
                dlg.Dispose();

                answer = true;
                if (Convert.ToInt32(drv["count"]) <= 0)
                {
                    if (DebtPlus.Data.Forms.MessageBox.Show("This selection does not select any valid debts. Do you still wish to include it?", "Sorry, but there are no debts", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        answer = false;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading list of debts");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            return answer;
        }

        private void MenuItem_Add_Click(object sender, System.EventArgs e)
        {
            AddRow();
        }

        private void MenuItem_Change_Click(object sender, System.EventArgs e)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)GridView1.GetRow(GridView1.FocusedRowHandle);
            if (drv != null)
            {
                if (EditRow(drv))
                {
                    drv.EndEdit();
                    ap.ds.Tables["change_list"].AcceptChanges();
                }
                else
                {
                    drv.CancelEdit();
                }

                EnableOK();
            }
        }

        private void MenuItem_Delete_Click(object sender, System.EventArgs e)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)GridView1.GetRow(GridView1.FocusedRowHandle);

            if (drv != null)
            {
                System.Int32 id = Convert.ToInt32(drv["id"]);
                if (id > 0)
                {
                    drv.Delete();
                    ap.ds.Tables["change_list"].AcceptChanges();

                    System.Data.DataView vue = new System.Data.DataView(ap.ds.Tables["debt_list"], string.Format("[selection]={0:f0}", id), string.Empty, DataViewRowState.CurrentRows);
                    for (System.Int32 Item = vue.Count - 1; Item >= 0; Item += -1)
                    {
                        vue[Item].Delete();
                    }
                    ap.ds.Tables["debt_list"].AcceptChanges();
                }

                EnableOK();
            }
        }

        private void MenuItem_Print_Click(object sender, System.EventArgs e)
        {
            PrintReport();
        }

        private void PrintReport()
        {
            var _with25 = bt;
            System.Data.DataTable tbl = ap.ds.Tables["debt_list"];
            _with25.RunWorkerAsync(tbl);
        }

        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Close();
        }

        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            System.Data.DataTable tbl = (System.Data.DataTable)e.Argument;
            using (var rpt = new Reports.ReassignedDebtsReport(ap))
            {
                rpt.DataSource = tbl;
                rpt.DisplayPreviewDialog();
            }
        }

        private void ContextMenu1_Popup(object sender, System.EventArgs e)
        {
            // Enable the edit/delete menu items
            if (GridView1.FocusedRowHandle >= 0)
            {
                MenuItem_Change.Enabled = true;
                MenuItem_Delete.Enabled = true;
            }
            else
            {
                MenuItem_Change.Enabled = false;
                MenuItem_Delete.Enabled = false;
            }

            // Enable the print function if there is some data to print
            MenuItem_Print.Enabled = (ap.ds.Tables["debt_list"].Rows.Count > 0);
        }

        /// <summary>
        /// Process a DOUBLE CLICK event on the trustRegister
        /// </summary>

        private void GridView1_DoubleClick(System.Object sender, System.EventArgs e)
        {
            // Find the trustRegister which was clicked
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // If there is a trustRegister then change the control
            if (RowHandle >= 0)
            {
                gv.FocusedRowHandle = RowHandle;
                System.Data.DataRowView drv = (System.Data.DataRowView)GridView1.GetRow(RowHandle);
                if (drv != null)
                {
                    if (EditRow(drv))
                    {
                        drv.EndEdit();
                        ap.ds.Tables["change_list"].AcceptChanges();
                    }
                    else
                    {
                        drv.CancelEdit();
                    }

                    EnableOK();
                }
            }
        }

        private void BarButtonItem_File_Print_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintReport();
        }

        private void BarButtonItem_File_Exit_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}