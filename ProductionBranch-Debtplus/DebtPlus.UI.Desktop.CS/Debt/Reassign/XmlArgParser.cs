using System;

namespace DebtPlus.UI.Desktop.CS.Debt.Reassign
{
    internal partial class XmlArgParser : DebtPlus.Utils.ArgParserBase, System.IDisposable
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public XmlArgParser() : base(new string[] { })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        // global storage
        public System.Data.DataSet ds = new System.Data.DataSet("ds");

        #region "IDisposable Support"

        // To detect redundant calls
        private bool disposedValue;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    ds.Dispose();
                }
            }
            this.disposedValue = true;
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion "IDisposable Support"
    }
}