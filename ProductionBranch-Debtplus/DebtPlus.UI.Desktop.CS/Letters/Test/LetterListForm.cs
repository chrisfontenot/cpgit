using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Letters.Compose;
using DevExpress.XtraEditors.Controls;
using DebtPlus.Data.Forms;
using DebtPlus.Events;
using System.Globalization;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Letters.Test
{
    internal partial class LetterListForm : DebtPlusForm
    {
        private readonly ArgsParser ap;

        public LetterListForm() : base()
        {
            InitializeComponent();
        }

        public LetterListForm(ArgsParser ap) : this()
        {
            this.ap = ap;
            Load += ReportListForm_Load;
            SimpleButton_Cancel.Click += SimpleButton_Cancel_Click;
            SimpleButton_OK.Click += SimpleButton_OK_Click;
        }

        private readonly string _reportName = string.Empty;

        public string ReportName
        {
            get { return _reportName; }
        }

        /// <summary>
        /// Handle the loading of the form
        /// </summary>

        private void ReportListForm_Load(object sender, EventArgs e)
        {
            LookUpEdit1.Properties.DataSource = DebtPlus.LINQ.Cache.letter_type.getList();
            LookUpEdit1.EditValueChanged += FormChanged;

            TextEdit1.EditValue = string.Empty;
            DateEdit1.EditValue = DateTime.Now.Date;
            DateEdit1.EditValue = null;
            CalcEdit1.EditValue = 0m;

            SimpleButton_OK.Enabled = LookUpEdit1.EditValue != null && !object.ReferenceEquals(LookUpEdit1.EditValue, DBNull.Value);
        }

        /// <summary>
        /// Process a change condition on the form
        /// </summary>
        private void FormChanged(object Sender, EventArgs e)
        {
            SimpleButton_OK.Enabled = LookUpEdit1.EditValue != null && !object.ReferenceEquals(LookUpEdit1.EditValue, DBNull.Value);
        }

        /// <summary>
        /// Close the form on the CANCEL button
        /// </summary>
        private void SimpleButton_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Generate the letter when the OK button is clicked
        /// </summary>
        private void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            using (MyLetterComposition ltr = new MyLetterComposition(Convert.ToInt32(LookUpEdit1.EditValue)))
            {
                ltr.GetValue += Letter_Getvalue;
                ltr.PrintLetter();
            }
        }

        /// <summary>
        /// Process a request for data from the letters
        /// </summary>
        private void Letter_Getvalue(object Sender, DebtPlus.Events.ParameterValueEventArgs e)
        {
            if (e.Name == ParameterValueEventArgs.name_client)
            {
                e.Value = ClientID1.EditValue.GetValueOrDefault(-1);
            }
            else if (e.Name == ParameterValueEventArgs.name_creditor)
            {
                e.Value = CreditorID1.EditValue;
            }
            else if (e.Name == ParameterValueEventArgs.name_debt)
            {
                Int32 IntValue = default(Int32);
                if (Int32.TryParse(TextEdit1.Text.Trim(), NumberStyles.Integer, CultureInfo.InvariantCulture, out IntValue) && IntValue > 0)
                {
                    e.Value = IntValue;
                }
            }
            else if (e.Name == ParameterValueEventArgs.name_appointment)
            {
                Int32 answer = ((MyLetterComposition)Sender).DefaultClientAppointment();
                if (answer > 0)
                    e.Value = answer;
            }
        }

        /// <summary>
        /// Change on the debt number
        /// </summary>
        private void SpinEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            e.Cancel = Convert.ToInt32(e.NewValue) <= 0;
        }

        /// <summary>
        /// Override the base class to not do some things
        /// </summary>
        private class MyLetterComposition : Base
        {
            public MyLetterComposition() : base()
            {
            }

            public MyLetterComposition(string LetterCode) : base(LetterCode)
            {
            }

            public MyLetterComposition(Int32 LetterType) : base(LetterType)
            {
            }

            /// <summary>
            /// Display the letter for the user
            /// </summary>
            protected override void DoPrintLetter(bool ShowPrinterDialog)
            {
                DoShow();
                // this is too easy.

                // To test the queue logic, replace the DoShow with these two statements.
                //queue_name = "Testing Letters" ' A name is required for a queue.
                //DoQueue()
            }

            /// <summary>
            /// If we are queueing the letter in response to a print button, don't
            /// </summary>
            protected override void DoQueueRoutine()
            {
                base.DoPrintRoutine(true);
            }

            /// <summary>
            /// Write the system note that the letter was printed
            /// </summary>
            protected override void WriteSystemNote()
            {
                // Never write a system note so we override this to do nothing.
            }

            /// <summary>
            /// Extend the method to find an appointment, even if it is not for this client
            /// </summary>
            public override Int32 DefaultClientAppointment()
            {
                // Try the standard logic first
                Int32 answer = base.DefaultClientAppointment();

                // If there is no answer then look for any appointment for this client
                if (answer <= 0)
                {
                    using (BusinessContext bc = new BusinessContext())
                    {
                        // Look for a pending appointment for this client
                        client_appointment q = bc.client_appointments.Where(ca => ca.client == ClientId & ca.status == 'P').FirstOrDefault();

                        if (q == null)
                        {
                            // If not for this client then take any pending appointment
                            q = bc.client_appointments.Where(ca => ca.status == 'P').FirstOrDefault();

                            if (q == null)
                            {
                                // Failing all else, just take the last appointment in the system
                                q = bc.client_appointments.FirstOrDefault();
                            }
                        }

                        if (q != null)
                        {
                            answer = q.Id;
                        }
                    }
                }

                return answer;
            }
        }
    }
}