namespace DebtPlus.UI.Desktop.CS.Letters.Test
{
    internal partial class ArgsParser : DebtPlus.Utils.ArgParserBase
    {
        public ArgsParser() : base(new string[] {
            "d",
            "p"
        })
        {
        }

        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            return ss;
        }
    }
}