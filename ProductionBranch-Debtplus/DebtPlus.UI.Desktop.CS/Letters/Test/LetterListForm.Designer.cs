using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using DebtPlus.UI.Creditor.Widgets.Controls;
using DebtPlus.UI.Client.Widgets.Controls;
using DebtPlus.UI.Creditor.Widgets;

namespace DebtPlus.UI.Desktop.CS.Letters.Test
{
	partial class LetterListForm : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LetterListForm));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
			this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.CreditorID1 = new CreditorID();
			this.ClientID1 = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
			this.CalcEdit1 = new DevExpress.XtraEditors.CalcEdit();
			this.LookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
			this.DateEdit1 = new DevExpress.XtraEditors.DateEdit();
			this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem9 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem10 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem7).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem9).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem10).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.SimpleButton_OK.Location = new System.Drawing.Point(73, 182);
			this.SimpleButton_OK.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.Name = "SimpleButton_OK";
			this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.StyleController = this.LayoutControl1;
			this.SimpleButton_OK.TabIndex = 12;
			this.SimpleButton_OK.Text = "OK";
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.TextEdit1);
			this.LayoutControl1.Controls.Add(this.CreditorID1);
			this.LayoutControl1.Controls.Add(this.ClientID1);
			this.LayoutControl1.Controls.Add(this.CalcEdit1);
			this.LayoutControl1.Controls.Add(this.LookUpEdit1);
			this.LayoutControl1.Controls.Add(this.DateEdit1);
			this.LayoutControl1.Controls.Add(this.SimpleButton_Cancel);
			this.LayoutControl1.Controls.Add(this.SimpleButton_OK);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(332, 236);
			this.LayoutControl1.TabIndex = 14;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//TextEdit1
			//
			this.TextEdit1.Location = new System.Drawing.Point(98, 84);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Size = new System.Drawing.Size(103, 20);
			this.TextEdit1.StyleController = this.LayoutControl1;
			this.TextEdit1.TabIndex = 6;
			//
			//CreditorID1
			//
			this.CreditorID1.EditValue = null;
			this.CreditorID1.Location = new System.Drawing.Point(98, 60);
			this.CreditorID1.Name = "CreditorID1";
			this.CreditorID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("CreditorID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a creditor ID", "search", null, true) });
			this.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.CreditorID1.Properties.Mask.BeepOnError = true;
			this.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
			this.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.CreditorID1.Properties.MaxLength = 10;
			this.CreditorID1.Size = new System.Drawing.Size(103, 20);
			this.CreditorID1.StyleController = this.LayoutControl1;
			this.CreditorID1.TabIndex = 5;
			//
			//ClientID1
			//
			this.ClientID1.Location = new System.Drawing.Point(98, 36);
			this.ClientID1.Name = "ClientID1";
			this.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.ClientID1.Properties.Appearance.Options.UseTextOptions = true;
			this.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ClientID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("ClientID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2,
			"Click here to search for a client ID", "search", null, true) });
			this.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.ClientID1.Properties.DisplayFormat.FormatString = "0000000";
			this.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ClientID1.Properties.EditFormat.FormatString = "f0";
			this.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.ClientID1.Properties.Mask.BeepOnError = true;
			this.ClientID1.Properties.Mask.EditMask = "\\d*";
			this.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.ClientID1.Properties.ValidateOnEnterKey = true;
			this.ClientID1.Size = new System.Drawing.Size(103, 20);
			this.ClientID1.StyleController = this.LayoutControl1;
			this.ClientID1.TabIndex = 4;
			//
			//CalcEdit1
			//
			this.CalcEdit1.Location = new System.Drawing.Point(98, 132);
			this.CalcEdit1.Name = "CalcEdit1";
			this.CalcEdit1.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit1.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit1.Properties.EditFormat.FormatString = "{0:c}";
			this.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit1.Properties.Mask.BeepOnError = true;
			this.CalcEdit1.Properties.Mask.EditMask = "c";
			this.CalcEdit1.Properties.Precision = 2;
			this.CalcEdit1.Size = new System.Drawing.Size(103, 20);
			this.CalcEdit1.StyleController = this.LayoutControl1;
			this.CalcEdit1.TabIndex = 11;
			//
			//LookUpEdit1
			//
			this.LookUpEdit1.Location = new System.Drawing.Point(98, 12);
			this.LookUpEdit1.Name = "LookUpEdit1";
			this.LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit1.Properties.DisplayMember = "description";
			this.LookUpEdit1.Properties.NullText = "";
			this.LookUpEdit1.Properties.ShowFooter = false;
			this.LookUpEdit1.Properties.ShowHeader = false;
			this.LookUpEdit1.Properties.ValueMember = "Id";
			this.LookUpEdit1.Size = new System.Drawing.Size(222, 20);
			this.LookUpEdit1.StyleController = this.LayoutControl1;
			this.LookUpEdit1.TabIndex = 1;
			this.LookUpEdit1.Properties.SortColumnIndex = 1;
			//
			//DateEdit1
			//
			this.DateEdit1.EditValue = null;
			this.DateEdit1.Location = new System.Drawing.Point(98, 108);
			this.DateEdit1.Name = "DateEdit1";
			this.DateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit1.Size = new System.Drawing.Size(103, 20);
			this.DateEdit1.StyleController = this.LayoutControl1;
			this.DateEdit1.TabIndex = 9;
			//
			//SimpleButton_Cancel
			//
			this.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_Cancel.Location = new System.Drawing.Point(189, 182);
			this.SimpleButton_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
			this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cancel.StyleController = this.LayoutControl1;
			this.SimpleButton_Cancel.TabIndex = 13;
			this.SimpleButton_Cancel.Text = "Quit";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.LayoutControlItem7,
				this.LayoutControlItem8,
				this.LayoutControlItem4,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2,
				this.EmptySpaceItem3,
				this.EmptySpaceItem4,
				this.EmptySpaceItem5,
				this.EmptySpaceItem6,
				this.EmptySpaceItem7,
				this.EmptySpaceItem8,
				this.EmptySpaceItem9,
				this.EmptySpaceItem10
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(332, 236);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.LookUpEdit1;
			this.LayoutControlItem1.CustomizationFormText = "Letter Type";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(312, 24);
			this.LayoutControlItem1.Text = "Letter Type";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(82, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.ClientID1;
			this.LayoutControlItem2.CustomizationFormText = "Client";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(193, 24);
			this.LayoutControlItem2.Text = "Client";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(82, 13);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.CreditorID1;
			this.LayoutControlItem3.CustomizationFormText = "Creditor";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(193, 24);
			this.LayoutControlItem3.Text = "Creditor";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(82, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.DateEdit1;
			this.LayoutControlItem5.CustomizationFormText = "Followup Date";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 96);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(193, 24);
			this.LayoutControlItem5.Text = "Followup Date";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(82, 13);
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.CalcEdit1;
			this.LayoutControlItem6.CustomizationFormText = "Followup Amount";
			this.LayoutControlItem6.Location = new System.Drawing.Point(0, 120);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(193, 24);
			this.LayoutControlItem6.Text = "Followup Amount";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(82, 13);
			//
			//LayoutControlItem7
			//
			this.LayoutControlItem7.Control = this.SimpleButton_OK;
			this.LayoutControlItem7.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7";
			this.LayoutControlItem7.Location = new System.Drawing.Point(61, 170);
			this.LayoutControlItem7.MaxSize = new System.Drawing.Size(79, 27);
			this.LayoutControlItem7.MinSize = new System.Drawing.Size(79, 27);
			this.LayoutControlItem7.Name = "LayoutControlItem7";
			this.LayoutControlItem7.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.LayoutControlItem7.Text = "LayoutControlItem7";
			this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem7.TextToControlDistance = 0;
			this.LayoutControlItem7.TextVisible = false;
			//
			//LayoutControlItem8
			//
			this.LayoutControlItem8.Control = this.SimpleButton_Cancel;
			this.LayoutControlItem8.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8";
			this.LayoutControlItem8.Location = new System.Drawing.Point(177, 170);
			this.LayoutControlItem8.Name = "LayoutControlItem8";
			this.LayoutControlItem8.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem8.Text = "LayoutControlItem8";
			this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem8.TextToControlDistance = 0;
			this.LayoutControlItem8.TextVisible = false;
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.TextEdit1;
			this.LayoutControlItem4.CustomizationFormText = "Debt Record Number";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(193, 24);
			this.LayoutControlItem4.Text = "Debt Record #";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(82, 13);
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 144);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(312, 26);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 170);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(61, 27);
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem3
			//
			this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
			this.EmptySpaceItem3.Location = new System.Drawing.Point(256, 170);
			this.EmptySpaceItem3.Name = "EmptySpaceItem3";
			this.EmptySpaceItem3.Size = new System.Drawing.Size(56, 27);
			this.EmptySpaceItem3.Text = "EmptySpaceItem3";
			this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem4
			//
			this.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4";
			this.EmptySpaceItem4.Location = new System.Drawing.Point(140, 170);
			this.EmptySpaceItem4.Name = "EmptySpaceItem4";
			this.EmptySpaceItem4.Size = new System.Drawing.Size(37, 27);
			this.EmptySpaceItem4.Text = "EmptySpaceItem4";
			this.EmptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem5
			//
			this.EmptySpaceItem5.CustomizationFormText = "EmptySpaceItem5";
			this.EmptySpaceItem5.Location = new System.Drawing.Point(0, 197);
			this.EmptySpaceItem5.Name = "EmptySpaceItem5";
			this.EmptySpaceItem5.Size = new System.Drawing.Size(312, 19);
			this.EmptySpaceItem5.Text = "EmptySpaceItem5";
			this.EmptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem6
			//
			this.EmptySpaceItem6.CustomizationFormText = "EmptySpaceItem6";
			this.EmptySpaceItem6.Location = new System.Drawing.Point(193, 24);
			this.EmptySpaceItem6.Name = "EmptySpaceItem6";
			this.EmptySpaceItem6.Size = new System.Drawing.Size(119, 24);
			this.EmptySpaceItem6.Text = "EmptySpaceItem6";
			this.EmptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem7
			//
			this.EmptySpaceItem7.CustomizationFormText = "EmptySpaceItem7";
			this.EmptySpaceItem7.Location = new System.Drawing.Point(193, 48);
			this.EmptySpaceItem7.Name = "EmptySpaceItem7";
			this.EmptySpaceItem7.Size = new System.Drawing.Size(119, 24);
			this.EmptySpaceItem7.Text = "EmptySpaceItem7";
			this.EmptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem8
			//
			this.EmptySpaceItem8.CustomizationFormText = "EmptySpaceItem8";
			this.EmptySpaceItem8.Location = new System.Drawing.Point(193, 72);
			this.EmptySpaceItem8.Name = "EmptySpaceItem8";
			this.EmptySpaceItem8.Size = new System.Drawing.Size(119, 24);
			this.EmptySpaceItem8.Text = "EmptySpaceItem8";
			this.EmptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem9
			//
			this.EmptySpaceItem9.CustomizationFormText = "EmptySpaceItem9";
			this.EmptySpaceItem9.Location = new System.Drawing.Point(193, 96);
			this.EmptySpaceItem9.Name = "EmptySpaceItem9";
			this.EmptySpaceItem9.Size = new System.Drawing.Size(119, 24);
			this.EmptySpaceItem9.Text = "EmptySpaceItem9";
			this.EmptySpaceItem9.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem10
			//
			this.EmptySpaceItem10.CustomizationFormText = "EmptySpaceItem10";
			this.EmptySpaceItem10.Location = new System.Drawing.Point(193, 120);
			this.EmptySpaceItem10.Name = "EmptySpaceItem10";
			this.EmptySpaceItem10.Size = new System.Drawing.Size(119, 24);
			this.EmptySpaceItem10.Text = "EmptySpaceItem10";
			this.EmptySpaceItem10.TextSize = new System.Drawing.Size(0, 0);
			//
			//LetterListForm
			//
			this.AcceptButton = this.SimpleButton_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.CancelButton = this.SimpleButton_Cancel;
			this.ClientSize = new System.Drawing.Size(332, 236);
			this.Controls.Add(this.LayoutControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LetterListForm";
			this.Text = "Letter Selection and Parameters";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem7).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem9).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem10).EndInit();
			this.ResumeLayout(false);

		}

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
		protected internal DevExpress.XtraEditors.DateEdit DateEdit1;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit1;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit1;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit1;
		protected internal CreditorID CreditorID1;
		protected internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem4;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem5;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem6;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem7;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem8;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem9;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem10;
	}
}
