using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.Letters.Print
{
	partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_selected = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_selected.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.CheckEdit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
			this.GridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumnStatus = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumnStatus.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_letter_type = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_letter_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
			this.Button_SelectAll = new DevExpress.XtraEditors.SimpleButton();
			this.Button_ClearAll = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit).BeginInit();
			this.SuspendLayout();
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.Location = new System.Drawing.Point(0, 0);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { this.CheckEdit });
			this.GridControl1.Size = new System.Drawing.Size(457, 267);
			this.GridControl1.TabIndex = 0;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_selected,
				this.GridColumn_description,
				this.GridColumnStatus,
				this.GridColumn3,
				this.GridColumn_letter_type
			});
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			//
			//GridColumn_selected
			//
			this.GridColumn_selected.Caption = "selected";
			this.GridColumn_selected.ColumnEdit = this.CheckEdit;
			this.GridColumn_selected.CustomizationCaption = "Checked";
			this.GridColumn_selected.FieldName = "selected";
			this.GridColumn_selected.Name = "GridColumn_selected";
			this.GridColumn_selected.OptionsColumn.AllowMove = false;
			this.GridColumn_selected.OptionsColumn.AllowSize = false;
			this.GridColumn_selected.OptionsColumn.ShowCaption = false;
			this.GridColumn_selected.OptionsColumn.ShowInCustomizationForm = false;
			this.GridColumn_selected.Visible = true;
			this.GridColumn_selected.VisibleIndex = 0;
			this.GridColumn_selected.Width = 20;
			//
			//CheckEdit
			//
			this.CheckEdit.AutoHeight = false;
			this.CheckEdit.Name = "CheckEdit";
			//
			//GridColumn_description
			//
			this.GridColumn_description.Caption = "Description";
			this.GridColumn_description.CustomizationCaption = "Description";
			this.GridColumn_description.FieldName = "description";
			this.GridColumn_description.Name = "GridColumn_description";
			this.GridColumn_description.OptionsColumn.AllowEdit = false;
			this.GridColumn_description.Visible = true;
			this.GridColumn_description.VisibleIndex = 1;
			this.GridColumn_description.Width = 305;
			//
			//GridColumnStatus
			//
			this.GridColumnStatus.Caption = "Status";
			this.GridColumnStatus.FieldName = "status";
			this.GridColumnStatus.Name = "GridColumnStatus";
			this.GridColumnStatus.Visible = true;
			this.GridColumnStatus.VisibleIndex = 2;
			this.GridColumnStatus.Width = 70;
			//
			//GridColumn_Id
			//
			this.GridColumn3.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn3.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn3.Caption = "Count";
			this.GridColumn3.CustomizationCaption = "Count of letters";
			this.GridColumn3.DisplayFormat.FormatString = "n0";
			this.GridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn3.FieldName = "count";
			this.GridColumn3.GroupFormat.FormatString = "n0";
			this.GridColumn3.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn3.Name = "GridColumn_Id";
			this.GridColumn3.OptionsColumn.AllowEdit = false;
			this.GridColumn3.Visible = true;
			this.GridColumn3.VisibleIndex = 3;
			this.GridColumn3.Width = 58;
			//
			//GridColumn_letter_type
			//
			this.GridColumn_letter_type.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_letter_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_letter_type.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_letter_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_letter_type.Caption = "Type";
			this.GridColumn_letter_type.CustomizationCaption = "Letter Type";
			this.GridColumn_letter_type.DisplayFormat.FormatString = "f0";
			this.GridColumn_letter_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_letter_type.FieldName = "letter_type";
			this.GridColumn_letter_type.Name = "GridColumn_letter_type";
			this.GridColumn_letter_type.OptionsColumn.AllowEdit = false;
			//
			//Button_Print
			//
			this.Button_Print.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.Button_Print.Enabled = false;
			this.Button_Print.Location = new System.Drawing.Point(463, 12);
			this.Button_Print.Name = "Button_Print";
			this.Button_Print.Size = new System.Drawing.Size(75, 23);
			this.Button_Print.TabIndex = 1;
			this.Button_Print.Text = "Print...";
			//
			//Button_SelectAll
			//
			this.Button_SelectAll.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.Button_SelectAll.Location = new System.Drawing.Point(463, 101);
			this.Button_SelectAll.Name = "Button_SelectAll";
			this.Button_SelectAll.Size = new System.Drawing.Size(75, 23);
			this.Button_SelectAll.TabIndex = 2;
			this.Button_SelectAll.Text = "Select All";
			//
			//Button_ClearAll
			//
			this.Button_ClearAll.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.Button_ClearAll.Location = new System.Drawing.Point(463, 130);
			this.Button_ClearAll.Name = "Button_ClearAll";
			this.Button_ClearAll.Size = new System.Drawing.Size(75, 23);
			this.Button_ClearAll.TabIndex = 3;
			this.Button_ClearAll.Text = "Clear All";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(463, 231);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 4;
			this.Button_Cancel.Text = "Quit";
			//
			//MainForm
			//
			this.AcceptButton = this.Button_Print;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(550, 266);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_ClearAll);
			this.Controls.Add(this.Button_SelectAll);
			this.Controls.Add(this.Button_Print);
			this.Controls.Add(this.GridControl1);
			this.Name = "MainForm";
			this.Text = "Print Queued Letters";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Print;
		protected internal DevExpress.XtraEditors.SimpleButton Button_SelectAll;
		protected internal DevExpress.XtraEditors.SimpleButton Button_ClearAll;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_selected;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit CheckEdit;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_description;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn3;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_letter_type;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumnStatus;
	}
}
