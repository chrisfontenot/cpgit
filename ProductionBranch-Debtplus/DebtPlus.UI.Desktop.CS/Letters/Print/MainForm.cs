using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace DebtPlus.UI.Desktop.CS.Letters.Print
{
    internal partial class MainForm
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public MainForm() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public MainForm(ArgParser ap) : this()
        {
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += MainForm_Load;
            Button_Cancel.Click += Button_Cancel_Click;
            Button_ClearAll.Click += Button_ClearAll_Click;
            Button_SelectAll.Click += Button_SelectAll_Click;
            Button_Print.Click += Button_Print_Click;
            CheckEdit.EditValueChanging += CheckEdit_EditValueChanging;
        }

        private void UnRegisterHandlers()
        {
            this.Load -= MainForm_Load;
            Button_Cancel.Click -= Button_Cancel_Click;
            Button_ClearAll.Click -= Button_ClearAll_Click;
            Button_SelectAll.Click -= Button_SelectAll_Click;
            Button_Print.Click -= Button_Print_Click;
            CheckEdit.EditValueChanging -= CheckEdit_EditValueChanging;
        }

        /// <summary>
        /// Process the close request for the form.
        /// </summary>
        private class PrinterManager
        {
            [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
            private static extern bool GetDefaultPrinter(string pszBuffer, ref Int32 pcchBuffer);

            [DllImport("winspool.drv", EntryPoint = "SetDefaultPrinter", CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
            private static extern bool SetDefaultPrinter_API(string pszPrinter);

            [System.Security.SecuritySafeCritical]
            private static bool DoGetDefaultPrinter(string pszBuffer, ref Int32 ppchBuffer)
            {
                return GetDefaultPrinter(pszBuffer, ref ppchBuffer);
            }

            private const Int32 ERROR_FILE_NOT_FOUND = 2;

            private const Int32 ERROR_INSUFFICIENT_BUFFER = 122;

            [System.Security.SecuritySafeCritical]
            private static bool DoSetDefaultPrinter_API(string pszBuffer)
            {
                return SetDefaultPrinter_API(pszBuffer);
            }

            /// <summary>
            /// Process the close request for the form.
            /// </summary>
            public static void SetDefaultPrinter(string PrinterName)
            {
                if (!DoSetDefaultPrinter_API(PrinterName))
                {
                    throw new System.ComponentModel.Win32Exception();
                }
            }

            /// <summary>
            /// Process the close request for the form.
            /// </summary>
            public static string GetDefaultPrinter()
            {
                Int32 n = 128;
                string s = new string(' ', n);
                if (DoGetDefaultPrinter(s, ref n))
                {
                    return s.Substring(0, n - 1); // toss the trailing null character
                }

                Int32 LastError = System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                if (LastError == ERROR_FILE_NOT_FOUND)
                {
                    throw new System.ComponentModel.Win32Exception(LastError, "There is no default printer.");
                }

                if (LastError != ERROR_INSUFFICIENT_BUFFER)
                {
                    throw new System.ComponentModel.Win32Exception();
                }

                s = new string(' ', n);
                if (DoGetDefaultPrinter(s, ref n))
                {
                    return s.Substring(0, n - 1);
                }

                throw new System.ComponentModel.Win32Exception();
            }
        }

        internal DataSet ds = new DataSet("ds");

        /// <summary>
        /// Reload the letter queue information
        /// </summary>
        private DataTable MainTable;

        private void RefreshData()
        {
            Cursor current_cursor = Cursor.Current;

            // Disable the other buttons until we find that we have something to do
            Button_ClearAll.Enabled = false;
            Button_SelectAll.Enabled = false;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT convert(bit,0) as selected, letter_type, queue_name as 'description', count(*) AS 'count', convert(varchar(80),'Pending') as 'status' FROM letter_queue WITH (NOLOCK) WHERE date_printed IS NULL GROUP BY letter_type, queue_name";
                    cmd.CommandType = CommandType.Text;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "letter_queue");
                        MainTable = ds.Tables["letter_queue"];
                    }
                }

                GridControl1.DataSource = MainTable.DefaultView;
                GridControl1.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letter queue items");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // Enable the select buttons if there are items
            if (MainTable != null)
            {
                if (MainTable.Rows.Count > 0)
                {
                    Button_ClearAll.Enabled = true;
                    Button_SelectAll.Enabled = true;
                }
            }

            Button_Print.Enabled = false;
        }

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            RefreshData();
        }

        /// <summary>
        /// When QUIT is clicked, close the application down
        /// </summary>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Process the CLEAR ALL button
        /// </summary>
        private void Button_ClearAll_Click(object sender, EventArgs e)
        {
            if (MainTable != null)
            {
                foreach (DataRow row in MainTable.Rows)
                {
                    row.BeginEdit();
                    row["selected"] = 0;
                    row.EndEdit();
                }
            }
            Button_Print.Enabled = false;
        }

        /// <summary>
        /// Process the SELECT ALL button
        /// </summary>
        private void Button_SelectAll_Click(object sender, EventArgs e)
        {
            bool Enabled = false;
            if (MainTable != null)
            {
                foreach (DataRow row in MainTable.Rows)
                {
                    row.BeginEdit();
                    row["selected"] = 1;
                    row.EndEdit();
                    Enabled = true;
                }
            }
            Button_Print.Enabled = Enabled;
        }

        /// <summary>
        /// Process the PRINT button
        /// </summary>

        private System.Threading.Thread PrintThread;

        private void Button_Print_Click(object sender, EventArgs e)
        {
            Button_Print.Enabled = false;

            PrintThread = new System.Threading.Thread(PrintThreadTopHandler);
            PrintThread.SetApartmentState(System.Threading.ApartmentState.STA);
            PrintThread.Name = "Print Thread";
            PrintThread.IsBackground = false;
            PrintThread.Start();
        }

        private delegate void PrintThreadCompletionDelegate();

        private void PrintThreadCompletion()
        {
            if (InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new PrintThreadCompletionDelegate(PrintThreadCompletion));
                EndInvoke(ia);
            }
            else
            {
                PrintThread = null;
            }
        }

        private void PrintThreadTopHandler()
        {
            try
            {
                PrintThreadHandler();
            }
            finally
            {
                PrintThreadCompletion();
            }
        }

        private void PrintThreadHandler()
        {
            string PrinterName = null;

            // Load the default printer into the printing component
            System.Drawing.Printing.PrinterSettings PrintSettings = new System.Drawing.Printing.PrinterSettings();
            PrintSettings.PrinterName = PrinterManager.GetDefaultPrinter();
            PrintSettings.PrintToFile = false;
            PrintSettings.PrintRange = System.Drawing.Printing.PrintRange.AllPages;
            PrintSettings.Copies = 1;
            PrintSettings.Duplex = System.Drawing.Printing.Duplex.Default;
            PrintSettings.FromPage = 1;
            PrintSettings.ToPage = PrintSettings.MaximumPage;

            // Allow the user to change the printer location
            using (PrintDialog frm = new PrintDialog())
            {
                frm.PrinterSettings = PrintSettings;
                frm.AllowCurrentPage = false;
                frm.AllowPrintToFile = false;
                frm.AllowSelection = false;
                frm.PrintToFile = false;
                frm.UseEXDialog = true;

                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                    return;
                PrinterName = frm.PrinterSettings.PrinterName;
            }

            DataView vue = (DataView)GridControl1.DataSource;

            foreach (DataRow row in vue.Table.Select("[selected]<>false", string.Empty))
            {
                row.BeginEdit();
                row["status"] = "PRINTING";
                row.EndEdit();
            }

            // Process the letter types
            using (DataView SelectVue = new DataView(vue.Table, "[selected]<>0", "letter_type", DataViewRowState.CurrentRows))
            {
                foreach (DataRowView drv in SelectVue)
                {
                    PrintLetterType(Convert.ToInt32(drv["letter_type"]), PrinterName);

                    drv.Row.BeginEdit();
                    drv.Row["selected"] = false;
                    drv.Row["status"] = "PRINTED";
                    drv.Row.EndEdit();
                }
            }
        }

        /// <summary>
        /// Print a specific letter type
        /// </summary>
        private void PrintLetterType(Int32 LetterType, string PrinterName)
        {
            DevExpress.Utils.WaitDialogForm StatusForm = new DevExpress.Utils.WaitDialogForm("Printing Letters");
            StatusForm.Show();
            try
            {
                using (DataSet dsLetterType = new DataSet("dsLetterType"))
                {
                    StatusForm.Caption = "Reading letter text";
                    Application.DoEvents();

                    // For the letter type, read the pending letter text from the database
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "SELECT letter_queue, isnull(queue_name,'') as queue_name, date_printed, isnull(top_margin, 0.10) as top_margin, isnull(left_margin,0.10) as left_margin, isnull(bottom_margin,0.10) as bottom_margin, isnull(right_margin,0.10) as right_margin, isnull(priority,0) as priority, isnull(sort_order,'ZZZZZ') as sort_order, formatted_letter FROM letter_queue WITH (NOLOCK) WHERE letter_type=@letter_type AND date_printed IS NULL";
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@letter_type", SqlDbType.Int).Value = LetterType;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.AcceptChangesDuringFill = true;
                            da.FillLoadOption = LoadOption.OverwriteChanges;
                            da.Fill(dsLetterType, "letter_queue_items");
                        }
                    }

                    StatusForm.Caption = "Imaging Letters";
                    Application.DoEvents();

                    // Create the report for the letters
                    using (DataView LetterView = new DataView(dsLetterType.Tables["letter_queue_items"], string.Empty, "sort_order", DataViewRowState.CurrentRows))
                    {
                        if (LetterView.Count > 0)
                        {
                            using (DevExpress.XtraPrinting.PrintingSystem ps = new DevExpress.XtraPrinting.PrintingSystem())
                            {
                                foreach (DataRowView drv in LetterView)
                                {
                                    ps.Pages.AddRange(GetDocumentLink(drv).PrintingSystem.Pages);

                                    // Mark the item as having been printed
                                    drv.BeginEdit();
                                    drv["date_printed"] = DateTime.Now;
                                    drv.EndEdit();
                                }

                                // Set the name of the document for the spooler and suppress the warnings.
                                ps.Document.Name = global::DebtPlus.Utils.Nulls.DStr(LetterView[0]["queue_name"]);
                                ps.ShowMarginsWarning = false;
                                ps.ShowPrintStatusDialog = false;

                                StatusForm.Caption = "Printing Letters";
                                Application.DoEvents();
                                ps.Print(PrinterName);
                            }
                        }
                    }

                    StatusForm.Caption = "Updating print status";
                    Application.DoEvents();

                    // Attempt to update the database with the changed information
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "xpr_letter_mark_printed";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@letter_queue", SqlDbType.Int, 0, "letter_queue");

                        // Do the update operation
                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                        {
                            da.UpdateCommand = cmd;
                            Int32 RowsUpdated = da.Update(dsLetterType.Tables["letter_queue_items"]);
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading letters to print");
            }
            finally
            {
                if (StatusForm != null)
                {
                    StatusForm.Close();
                    StatusForm.Dispose();
                }
            }
        }

        /// <summary>
        /// Get the document link for the new letter template object
        /// </summary>
        private static DevExpress.XtraPrinting.PrintableComponentLink GetDocumentLink(DataRowView drv)
        {
            string FormattedLetterText = Convert.ToString(drv["formatted_letter"]);
            byte[] ab = (new System.Text.ASCIIEncoding()).GetBytes(FormattedLetterText);

            // Create a document that represents the letter image
            DevExpress.XtraRichEdit.RichEditDocumentServer LetterServer = new DevExpress.XtraRichEdit.RichEditDocumentServer();
            using (System.IO.MemoryStream stm = new System.IO.MemoryStream(ab))
            {
                LetterServer.LoadDocument(stm, DevExpress.XtraRichEdit.DocumentFormat.Rtf);
            }

            // Create a Component link to include the document
            DevExpress.XtraPrinting.PrintableComponentLink lk1 = new DevExpress.XtraPrinting.PrintableComponentLink(new DevExpress.XtraPrinting.PrintingSystem()) { Component = LetterServer };

            // Create the document object and return the linkage to it.
            lk1.CreateDocument();
            return lk1;
        }

        /// <summary>
        /// Handle the select item being changed
        /// </summary>

        private void CheckEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            // If we are setting the item then enable the print button
            if (Convert.ToInt32(e.NewValue) != 0)
            {
                Button_Print.Enabled = true;
                return;
            }

            // The item is being reset. There must be at least TWO selected to enable the print mode
            Int32 SelectCount = 0;
            foreach (DataRow row in MainTable.Rows)
            {
                if (Convert.ToInt32(row["selected"]) != 0)
                {
                    SelectCount += 1;
                }
            }

            // We need at least two since we are clearing one of them
            Button_Print.Enabled = (SelectCount > 1);
        }
    }
}