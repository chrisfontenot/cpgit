using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DebtPlus.Interfaces;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import
{
    public partial class IntakeNotesList : DevExpress.XtraEditors.XtraUserControl
    {
        #region " Windows Form Designer generated code "

        public IntakeNotesList() : base()
        {
            InitializeComponent();
        }

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.GridControl GridControl1;

        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Date;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Creator;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Note;
        internal DevExpress.XtraEditors.Repository.RepositoryItemComboBox NoteTextEdit;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Creator = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Creator.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Note = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Note.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.NoteTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.NoteTextEdit).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { this.NoteTextEdit });
            this.GridControl1.Size = new System.Drawing.Size(424, 280);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.UseEmbeddedNavigator = true;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_Date,
                this.GridColumn_Creator,
                this.GridColumn_Note
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.NewItemRowText = "Click here to add a new note";
            this.GridView1.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView1.OptionsNavigation.AutoFocusNewRow = true;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_Date, DevExpress.Data.ColumnSortOrder.Ascending) });
            //
            //GridColumn_Date
            //
            this.GridColumn_Date.Caption = "Date";
            this.GridColumn_Date.CustomizationCaption = "Date Created";
            this.GridColumn_Date.DisplayFormat.FormatString = "d";
            this.GridColumn_Date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Date.FieldName = "date_created";
            this.GridColumn_Date.GroupFormat.FormatString = "d";
            this.GridColumn_Date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_Date.Name = "GridColumn_Date";
            this.GridColumn_Date.OptionsColumn.AllowEdit = false;
            this.GridColumn_Date.Visible = true;
            this.GridColumn_Date.VisibleIndex = 0;
            this.GridColumn_Date.Width = 79;
            //
            //GridColumn_Creator
            //
            this.GridColumn_Creator.Caption = "Creator";
            this.GridColumn_Creator.CustomizationCaption = "Created By";
            this.GridColumn_Creator.FieldName = "created_by";
            this.GridColumn_Creator.Name = "GridColumn_Creator";
            this.GridColumn_Creator.OptionsColumn.AllowEdit = false;
            this.GridColumn_Creator.Visible = true;
            this.GridColumn_Creator.VisibleIndex = 1;
            this.GridColumn_Creator.Width = 90;
            //
            //GridColumn_Note
            //
            this.GridColumn_Note.Caption = "Note";
            this.GridColumn_Note.ColumnEdit = this.NoteTextEdit;
            this.GridColumn_Note.CustomizationCaption = "Note Text";
            this.GridColumn_Note.FieldName = "note";
            this.GridColumn_Note.Name = "GridColumn_Note";
            this.GridColumn_Note.Visible = true;
            this.GridColumn_Note.VisibleIndex = 2;
            this.GridColumn_Note.Width = 234;
            //
            //NoteTextEdit
            //
            this.NoteTextEdit.AutoHeight = false;
            this.NoteTextEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.NoteTextEdit.MaxLength = 50;
            this.NoteTextEdit.Name = "NoteTextEdit";
            this.NoteTextEdit.Sorted = true;
            //
            //NotesList
            //
            this.Controls.Add(this.GridControl1);
            this.Name = "NotesList";
            this.Size = new System.Drawing.Size(424, 280);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.NoteTextEdit).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private Form MyForm;

        private Int32 intake_client = -1;
        private DataTable _tbl = null;

        private DataTable tbl
        {
            get { return _tbl; }
            set
            {
                if (_tbl != null)
                {
                    tbl.RowChanged -= tbl_RowChanged;
                    tbl.RowDeleting -= tbl_RowDeleting;
                }
                _tbl = value;
                if (_tbl != null)
                {
                    tbl.RowChanged += tbl_RowChanged;
                    tbl.RowDeleting += tbl_RowDeleting;
                }
            }
        }

        /// <summary>
        /// Process the loading for the control
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Find the form
            MyForm = FindForm();

            // In case we are called more than once!!!!
            MyForm.Closing -= Form_Closing;
#if false
            // Propigate the tooltip controller to our control
            if (MyForm is IForm)
            {
                var _with1 = GridControl1;
                _with1.ToolTipController = ((IForm)MyForm).GetToolTipController;
            }
#endif

            // Hook into the form closing event to save our changes
            MyForm.Closing += Form_Closing;
        }

        /// <summary>
        /// Read the information from the database
        /// </summary>

        public void ReadForm(Int32 IntakeClient)
        {
            // Save the current set of notes before loading a new batch
            tbl = Storage.ds.Tables["intake_notes"];
            if (tbl != null)
            {
                SaveChanges(tbl);
                tbl.Clear();
            }

            intake_client = IntakeClient;

            // Read the list of notes for this client
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            var _with2 = cmd;
            _with2.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with2.CommandText = "SELECT intake_note, note, date_created, created_by FROM intake_notes WITH (NOLOCK) WHERE intake_client=@intake_client";
            _with2.Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client;

            // Read the notes from the database
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                // Populate the dataset and the editor with the standard set of notes
                ReadStandardNotes();

                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                {
                    da.Fill(Storage.ds, "intake_notes");
                }
                tbl = Storage.ds.Tables["intake_notes"];

                var _with3 = GridControl1;
                _with3.DataSource = tbl.DefaultView;
                _with3.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading intake_notes table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Read the text for the standard messages
        /// </summary>

        private void ReadStandardNotes()
        {
            if (Storage.ds.Tables["intake_message_types"] != null)
            {
                // Read the list of notes for this client
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT intake_message_type, message FROM intake_message_types WITH (NOLOCK)";
                    cmd.CommandType = CommandType.Text;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(Storage.ds, "intake_message_types");
                    }
                }
            }

            // Set the control to use these items
            var _with4 = NoteTextEdit;
            _with4.Items.Clear();
            foreach (DataRowView drv in Storage.ds.Tables["intake_message_types"].DefaultView)
            {
                _with4.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(drv["message"]), Convert.ToInt32(drv["intake_message_type"])));
            }
        }

        /// <summary>
        /// Post the updates to the database
        /// </summary>

        private void Form_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // Do the update if there is a change to the tables
            tbl = Storage.ds.Tables["intake_notes"];
            if (tbl != null)
                SaveChanges(tbl);
        }

        /// <summary>
        /// Save the database changes
        /// </summary>
        private void SaveChanges(DataTable tbl)
        {
            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
            {
                // Build the update command
                System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                var _with5 = cmd;
                _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with5.CommandText = "UPDATE intake_notes SET note=@note WHERE intake_note=@intake_note";
                _with5.Parameters.Add("@note", SqlDbType.VarChar, 50, "note");
                _with5.Parameters.Add("@intake_note", SqlDbType.Int, 0, "intake_note");
                da.UpdateCommand = cmd;

                // Build the delete command
                cmd = new SqlCommand();
                var _with6 = cmd;
                _with6.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with6.CommandText = "DELETE FROM intake_notes WHERE intake_note=@intake_note";
                _with6.Parameters.Add("@intake_note", SqlDbType.Int, 0, "intake_note");
                da.DeleteCommand = cmd;

                // Build the insert command
                cmd = new SqlCommand();
                var _with7 = cmd;
                _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with7.CommandText = "INSERT INTO intake_notes(intake_client,note) VALUES (@intake_client,@note)";
                _with7.Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client;
                _with7.Parameters.Add("@note", SqlDbType.VarChar, 50, "note");
                da.InsertCommand = cmd;

                Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    da.Update(tbl);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }
        }

        /// <summary>
        /// Handle the creation of a new row to insert the data
        /// </summary>
        private void tbl_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            DataRow row = e.Row;

            // Supply needed defaults for the display when a new row is added to the table
            if (e.Action == DataRowAction.Add)
            {
                if (object.ReferenceEquals(row["date_created"], DBNull.Value))
                    row["date_created"] = DateTime.Now;
                if (object.ReferenceEquals(row["created_by"], DBNull.Value))
                    row["created_by"] = "Me";
            }
        }

        private void tbl_RowDeleting(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Delete && DebtPlus.Data.Prompts.RequestConfirmation_Delete() == DialogResult.No)
                throw new ApplicationException("The delete was cancelled");
        }
    }
}