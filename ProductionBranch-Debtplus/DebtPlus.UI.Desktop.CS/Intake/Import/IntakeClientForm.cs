using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Data.Forms;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import
{
    internal partial class IntakeClientForm : DebtPlusForm
    {
        private readonly DataRowView drv;

        public IntakeClientForm() : base()
        {
            InitializeComponent();
        }

        public IntakeClientForm(DataRowView DataRow) : this()
        {
            drv = DataRow;
            this.Load += IntakeClientForm_Load;
            IntakePeople1.DataChanged += IntakePeople1_DataChanged;
            Button_View.Click += Button_View_Click;
            Button_Import.Click += Button_Import_Click;
            IntakePeople2.DataChanged += IntakePeople1_DataChanged;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal IntakeNotesList NotesList1;

        internal global::DebtPlus.UI.Desktop.CS.Intake.Import.IntakePeople IntakePeople1;
        internal global::DebtPlus.UI.Desktop.CS.Intake.Import.IntakePeople IntakePeople2;
        internal DevExpress.XtraEditors.SimpleButton Button_Import;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.SimpleButton Button_View;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl intake_client_id;
        internal DevExpress.XtraEditors.LabelControl intake_id;

        internal DevExpress.XtraEditors.LabelControl submitted_services;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.NotesList1 = new global::DebtPlus.UI.Desktop.CS.Intake.Import.IntakeNotesList();
            this.IntakePeople1 = new global::DebtPlus.UI.Desktop.CS.Intake.Import.IntakePeople();
            this.IntakePeople2 = new global::DebtPlus.UI.Desktop.CS.Intake.Import.IntakePeople();
            this.Button_Import = new DevExpress.XtraEditors.SimpleButton();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.Button_View = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.intake_client_id = new DevExpress.XtraEditors.LabelControl();
            this.intake_id = new DevExpress.XtraEditors.LabelControl();
            this.submitted_services = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //NotesList1
            //
            this.NotesList1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.NotesList1.Location = new System.Drawing.Point(0, 256);
            this.NotesList1.Name = "NotesList1";
            this.NotesList1.Size = new System.Drawing.Size(688, 152);
            this.NotesList1.TabIndex = 0;
            //
            //IntakePeople1
            //
            this.IntakePeople1.Location = new System.Drawing.Point(8, 80);
            this.IntakePeople1.Name = "IntakePeople1";
            this.IntakePeople1.person = 1;
            this.IntakePeople1.Size = new System.Drawing.Size(296, 176);
            this.IntakePeople1.TabIndex = 1;
            //
            //IntakePeople2
            //
            this.IntakePeople2.Location = new System.Drawing.Point(304, 80);
            this.IntakePeople2.Name = "IntakePeople2";
            this.IntakePeople2.person = 2;
            this.IntakePeople2.Size = new System.Drawing.Size(296, 176);
            this.IntakePeople2.TabIndex = 2;
            //
            //Button_Import
            //
            this.Button_Import.Location = new System.Drawing.Point(608, 64);
            this.Button_Import.Name = "Button_Import";
            this.Button_Import.Size = new System.Drawing.Size(75, 23);
            this.Button_Import.TabIndex = 3;
            this.Button_Import.Text = "Import";
            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 14f, System.Drawing.FontStyle.Bold);
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl1.Location = new System.Drawing.Point(8, 56);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(90, 23);
            this.LabelControl1.TabIndex = 4;
            this.LabelControl1.Text = "Applicant";
            //
            //LabelControl2
            //
            this.LabelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 14f, System.Drawing.FontStyle.Bold);
            this.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl2.Location = new System.Drawing.Point(304, 56);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(123, 23);
            this.LabelControl2.TabIndex = 5;
            this.LabelControl2.Text = "Co-Applicant";
            //
            //Button_View
            //
            this.Button_View.Location = new System.Drawing.Point(608, 32);
            this.Button_View.Name = "Button_View";
            this.Button_View.Size = new System.Drawing.Size(75, 23);
            this.Button_View.TabIndex = 6;
            this.Button_View.Text = "View";
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(608, 96);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "Cancel";
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(8, 8);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(49, 13);
            this.LabelControl3.TabIndex = 8;
            this.LabelControl3.Text = "Intake ID:";
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(8, 22);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(65, 13);
            this.LabelControl4.TabIndex = 9;
            this.LabelControl4.Text = "Intake Client:";
            //
            //intake_client_id
            //
            this.intake_client_id.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.intake_client_id.Location = new System.Drawing.Point(88, 24);
            this.intake_client_id.Name = "intake_client_id";
            this.intake_client_id.Size = new System.Drawing.Size(0, 13);
            this.intake_client_id.TabIndex = 11;
            //
            //intake_id
            //
            this.intake_id.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.intake_id.Location = new System.Drawing.Point(88, 8);
            this.intake_id.Name = "intake_id";
            this.intake_id.Size = new System.Drawing.Size(0, 13);
            this.intake_id.TabIndex = 10;
            //
            //submitted_services
            //
            this.submitted_services.Appearance.ForeColor = System.Drawing.Color.Red;
            this.submitted_services.Location = new System.Drawing.Point(232, 8);
            this.submitted_services.Name = "submitted_services";
            this.submitted_services.Size = new System.Drawing.Size(258, 13);
            this.submitted_services.TabIndex = 12;
            this.submitted_services.Text = "Client submitted Agreement for Services electronically";
            this.submitted_services.Visible = false;
            //
            //IntakeClientForm
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(688, 406);
            this.Controls.Add(this.submitted_services);
            this.Controls.Add(this.intake_client_id);
            this.Controls.Add(this.intake_id);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_View);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.Button_Import);
            this.Controls.Add(this.IntakePeople2);
            this.Controls.Add(this.IntakePeople1);
            this.Controls.Add(this.NotesList1);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "IntakeClientForm";
            this.Text = "IntakeClient";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private Int32 intake_client = -1;

        private void IntakeClientForm_Load(object sender, EventArgs e)
        {
            // Load the controls for the other record components
            intake_client = Convert.ToInt32(drv["intake_client"]);
            NotesList1.ReadForm(intake_client);
            IntakePeople1.ReadForm(intake_client);
            IntakePeople2.ReadForm(intake_client);
            Button_Import.Enabled = IntakePeople1.isValid && IntakePeople2.isValid;

            // Read the client information
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlDataReader rd = null;
            bool intake_agreement = false;
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = cn;
                    _with1.CommandText = "SELECT intake_id, intake_agreement FROM view_intake_clients WITH (NOLOCK) WHERE intake_client=@intake_client";
                    _with1.Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client;
                    rd = _with1.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);
                }

                if (rd.Read())
                {
                    if (!rd.IsDBNull(0))
                        intake_id.Text = rd.GetString(0).Trim();
                    if (!rd.IsDBNull(1))
                        intake_agreement = Convert.ToInt32(rd.GetValue(1)) != 0;
                }
            }
            catch (SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading intake_client table");
            }
            finally
            {
                if (rd != null)
                    rd.Dispose();
                if (cn != null)
                    cn.Dispose();
                Cursor.Current = current_cursor;
            }

            submitted_services.Visible = intake_agreement;
            intake_client_id.Text = intake_client.ToString();
        }

        private void IntakePeople1_DataChanged(object sender, EventArgs e)
        {
            Button_Import.Enabled = IntakePeople1.isValid && IntakePeople2.isValid;
        }

        private void Button_View_Click(object sender, EventArgs e)
        {
            DebtPlus.Reports.Intake.ClientInfo.IntakeClientInfoReport rpt = new DebtPlus.Reports.Intake.ClientInfo.IntakeClientInfoReport();

            rpt.Parameter_IntakeClient = intake_client;
            if (((DebtPlus.Interfaces.Reports.IReports)rpt).RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
            {
                rpt.RunReportInSeparateThread();
            }
        }

        private void Button_Import_Click(object sender, EventArgs e)
        {
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Int32 DebtPlusClient = -1;
            try
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = cn;
                    _with2.CommandText = "xpr_intake_import";
                    _with2.CommandType = CommandType.StoredProcedure;
                    _with2.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    _with2.Parameters.Add("@intake_client", SqlDbType.Int).Value = intake_client;
                    _with2.ExecuteNonQuery();
                    DebtPlusClient = Convert.ToInt32(_with2.Parameters[0].Value);
                }

                cn.Close();

                if (DebtPlusClient > 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(string.Format("The client was imported into DebtPlus ClientId # {0}", string.Format("{0:0000000}", DebtPlusClient)), "Operation Completed", MessageBoxButtons.OK);
                    DialogResult = DialogResult.OK;
                }
            }
            catch (SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error importing client");
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }
        }
    }
}