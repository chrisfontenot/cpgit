#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import
{
    internal partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public MainForm() : base()
        {
            InitializeComponent();

            Load += MainForm_Load;
            Closing += MainForm_Closing;
            Button_Edit.Click += Button_Edit_Click;
            Button_Cancel.Click += Button_Cancel_Click;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.MouseDown += GridView1_MouseDown;
            Button_Delete.Click += Button_Delete_Click;
            BarButtonItem2.ItemClick += BarButtonItem2_ItemClick;
            BarButtonItem1.ItemClick += BarButtonItem1_ItemClick;
            BarButtonItem3.ItemClick += BarButtonItem3_ItemClick;
        }

        private Int32 intake_client = -1;
        private Int32 ControlRow = -1;

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.GridControl GridControl1;

        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ID;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_DateCreated;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_HomePh;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Status;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Bankruptcy;
        internal DevExpress.XtraEditors.SimpleButton Button_Edit;
        internal DevExpress.XtraEditors.SimpleButton Button_Delete;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.GridColumn_Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_DateCreated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_HomePh = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Bankruptcy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Button_Edit = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Delete = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.Bar2 = new DevExpress.XtraBars.Bar();
            this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.BarSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            this.SuspendLayout();
            //
            //GridColumn_Status
            //
            this.GridColumn_Status.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_Status.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_Status.Caption = "Status";
            this.GridColumn_Status.CustomizationCaption = "Completion Status";
            this.GridColumn_Status.FieldName = "status";
            this.GridColumn_Status.Name = "GridColumn_Status";
            this.GridColumn_Status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Status.Visible = true;
            this.GridColumn_Status.VisibleIndex = 3;
            this.GridColumn_Status.Width = 81;
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Location = new System.Drawing.Point(12, 12);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(385, 247);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.AppearancePrint.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.GridView1.AppearancePrint.EvenRow.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            this.GridView1.AppearancePrint.EvenRow.Options.UseBackColor = true;
            this.GridView1.AppearancePrint.HeaderPanel.BackColor = System.Drawing.Color.Teal;
            this.GridView1.AppearancePrint.HeaderPanel.BackColor2 = System.Drawing.Color.Teal;
            this.GridView1.AppearancePrint.HeaderPanel.BorderColor = System.Drawing.Color.Teal;
            this.GridView1.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.GridView1.AppearancePrint.HeaderPanel.ForeColor = System.Drawing.Color.White;
            this.GridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.AppearancePrint.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.GridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_ID,
                this.GridColumn_DateCreated,
                this.GridColumn_HomePh,
                this.GridColumn_Status,
                this.GridColumn_Bankruptcy
            });
            StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
            StyleFormatCondition1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            StyleFormatCondition1.Appearance.Options.UseBackColor = true;
            StyleFormatCondition1.Appearance.Options.UseBorderColor = true;
            StyleFormatCondition1.Appearance.Options.UseFont = true;
            StyleFormatCondition1.ApplyToRow = true;
            StyleFormatCondition1.Column = this.GridColumn_Status;
            StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.GreaterOrEqual;
            StyleFormatCondition1.Value1 = "Comp";
            this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] { StyleFormatCondition1 });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsFilter.UseNewCustomFilterDialog = true;
            this.GridView1.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsPrint.ExpandAllDetails = true;
            this.GridView1.OptionsPrint.PrintFooter = false;
            this.GridView1.OptionsPrint.PrintGroupFooter = false;
            this.GridView1.OptionsPrint.PrintHorzLines = false;
            this.GridView1.OptionsPrint.PrintVertLines = false;
            this.GridView1.OptionsPrint.UsePrintStyles = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_DateCreated, DevExpress.Data.ColumnSortOrder.Descending) });
            //
            //GridColumn_ID
            //
            this.GridColumn_ID.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_ID.Caption = "ID";
            this.GridColumn_ID.CustomizationCaption = "Intake ID";
            this.GridColumn_ID.FieldName = "intake_client";
            this.GridColumn_ID.Name = "GridColumn_ID";
            this.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_ID.Visible = true;
            this.GridColumn_ID.VisibleIndex = 0;
            this.GridColumn_ID.Width = 69;
            //
            //GridColumn_DateCreated
            //
            this.GridColumn_DateCreated.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_DateCreated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_DateCreated.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_DateCreated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_DateCreated.Caption = "Created";
            this.GridColumn_DateCreated.CustomizationCaption = "Date Created";
            this.GridColumn_DateCreated.DisplayFormat.FormatString = "d";
            this.GridColumn_DateCreated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_DateCreated.FieldName = "date_created";
            this.GridColumn_DateCreated.GroupFormat.FormatString = "d";
            this.GridColumn_DateCreated.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_DateCreated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.GridColumn_DateCreated.Name = "GridColumn_DateCreated";
            this.GridColumn_DateCreated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_DateCreated.Visible = true;
            this.GridColumn_DateCreated.VisibleIndex = 1;
            this.GridColumn_DateCreated.Width = 81;
            //
            //GridColumn_HomePh
            //
            this.GridColumn_HomePh.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_HomePh.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_HomePh.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_HomePh.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_HomePh.Caption = "Home PH#";
            this.GridColumn_HomePh.CustomizationCaption = "Home Phone #";
            this.GridColumn_HomePh.FieldName = "home_ph";
            this.GridColumn_HomePh.Name = "GridColumn_HomePh";
            this.GridColumn_HomePh.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_HomePh.Visible = true;
            this.GridColumn_HomePh.VisibleIndex = 2;
            this.GridColumn_HomePh.Width = 81;
            //
            //GridColumn_Bankruptcy
            //
            this.GridColumn_Bankruptcy.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Bankruptcy.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_Bankruptcy.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Bankruptcy.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_Bankruptcy.Caption = "Bankruptcy";
            this.GridColumn_Bankruptcy.CustomizationCaption = "Bankruptcy Status";
            this.GridColumn_Bankruptcy.FieldName = "bankruptcy";
            this.GridColumn_Bankruptcy.Name = "GridColumn_Bankruptcy";
            this.GridColumn_Bankruptcy.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Bankruptcy.Visible = true;
            this.GridColumn_Bankruptcy.VisibleIndex = 4;
            this.GridColumn_Bankruptcy.Width = 84;
            //
            //Button_Edit
            //
            this.Button_Edit.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Edit.Location = new System.Drawing.Point(401, 12);
            this.Button_Edit.MaximumSize = new System.Drawing.Size(75, 0);
            this.Button_Edit.Name = "Button_Edit";
            this.Button_Edit.Size = new System.Drawing.Size(75, 22);
            this.Button_Edit.StyleController = this.LayoutControl1;
            this.Button_Edit.TabIndex = 1;
            this.Button_Edit.Text = "&Edit";
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.GridControl1);
            this.LayoutControl1.Controls.Add(this.Button_Cancel);
            this.LayoutControl1.Controls.Add(this.Button_Edit);
            this.LayoutControl1.Controls.Add(this.Button_Delete);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 24);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(488, 271);
            this.LayoutControl1.TabIndex = 4;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.Location = new System.Drawing.Point(401, 38);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 0);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 22);
            this.Button_Cancel.StyleController = this.LayoutControl1;
            this.Button_Cancel.TabIndex = 3;
            this.Button_Cancel.Text = "&Cancel";
            //
            //Button_Delete
            //
            this.Button_Delete.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Delete.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
            this.Button_Delete.Appearance.ForeColor = System.Drawing.Color.Firebrick;
            this.Button_Delete.Appearance.Options.UseFont = true;
            this.Button_Delete.Appearance.Options.UseForeColor = true;
            this.Button_Delete.Location = new System.Drawing.Point(401, 93);
            this.Button_Delete.MaximumSize = new System.Drawing.Size(75, 0);
            this.Button_Delete.Name = "Button_Delete";
            this.Button_Delete.Size = new System.Drawing.Size(75, 22);
            this.Button_Delete.StyleController = this.LayoutControl1;
            this.Button_Delete.TabIndex = 2;
            this.Button_Delete.Text = "&Delete";
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
                this.LayoutControlItem1,
                this.LayoutControlItem3,
                this.LayoutControlItem4,
                this.LayoutControlItem2,
                this.EmptySpaceItem1,
                this.EmptySpaceItem2
            });
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(488, 271);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.GridControl1;
            this.LayoutControlItem1.CustomizationFormText = "Grid";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(389, 251);
            this.LayoutControlItem1.Text = "Grid";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.Button_Delete;
            this.LayoutControlItem3.CustomizationFormText = "Delete Button";
            this.LayoutControlItem3.Location = new System.Drawing.Point(389, 81);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(79, 26);
            this.LayoutControlItem3.Text = "Delete Button";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.Button_Cancel;
            this.LayoutControlItem4.CustomizationFormText = "Cancel Button";
            this.LayoutControlItem4.Location = new System.Drawing.Point(389, 26);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(79, 26);
            this.LayoutControlItem4.Text = "Cancel Button";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextToControlDistance = 0;
            this.LayoutControlItem4.TextVisible = false;
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.Control = this.Button_Edit;
            this.LayoutControlItem2.CustomizationFormText = "Edit Button";
            this.LayoutControlItem2.Location = new System.Drawing.Point(389, 0);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(79, 26);
            this.LayoutControlItem2.Text = "Edit Button";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(389, 52);
            this.EmptySpaceItem1.MaxSize = new System.Drawing.Size(0, 29);
            this.EmptySpaceItem1.MinSize = new System.Drawing.Size(10, 29);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(79, 29);
            this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem2
            //
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(389, 107);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(79, 144);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
                this.BarSubItem1,
                this.BarSubItem2,
                this.BarButtonItem1,
                this.BarButtonItem2,
                this.BarButtonItem3
            });
            this.barManager1.MainMenu = this.Bar2;
            this.barManager1.MaxItemId = 5;
            //
            //Bar2
            //
            this.Bar2.BarName = "Main menu";
            this.Bar2.DockCol = 0;
            this.Bar2.DockRow = 0;
            this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem2)
            });
            this.Bar2.OptionsBar.MultiLine = true;
            this.Bar2.OptionsBar.UseWholeRow = true;
            this.Bar2.Text = "Main menu";
            //
            //BarSubItem1
            //
            this.BarSubItem1.Caption = "&File";
            this.BarSubItem1.Id = 0;
            this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem1) });
            this.BarSubItem1.Name = "BarSubItem1";
            //
            //BarButtonItem1
            //
            this.BarButtonItem1.Caption = "&Exit";
            this.BarButtonItem1.Id = 2;
            this.BarButtonItem1.Name = "BarButtonItem1";
            //
            //BarSubItem2
            //
            this.BarSubItem2.Caption = "&Reports";
            this.BarSubItem2.Id = 1;
            this.BarSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem2),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem3)
            });
            this.BarSubItem2.Name = "BarSubItem2";
            //
            //BarButtonItem2
            //
            this.BarButtonItem2.Caption = "&List...";
            this.BarButtonItem2.Id = 3;
            this.BarButtonItem2.Name = "BarButtonItem2";
            //
            //BarButtonItem3
            //
            this.BarButtonItem3.Caption = "&View Item...";
            this.BarButtonItem3.Id = 4;
            this.BarButtonItem3.Name = "BarButtonItem3";
            //
            //barDockControlTop
            //
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(488, 24);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 295);
            this.barDockControlBottom.Size = new System.Drawing.Size(488, 0);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 271);
            //
            //barDockControlRight
            //
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(488, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 271);
            //
            //MainForm
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(488, 295);
            this.Controls.Add(this.LayoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "MainForm";
            this.Text = "Intake Clients";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            this.ResumeLayout(false);
        }

        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar Bar2;
        internal DevExpress.XtraBars.BarSubItem BarSubItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem1;
        internal DevExpress.XtraBars.BarSubItem BarSubItem2;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem2;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem3;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// The CANCEL button closes the form
        /// </summary>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Load the list of pending INTAKE clients
        /// </summary>

        private void MainForm_Load(object sender, EventArgs e)
        {
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT intake_client, intake_id, date_created, home_ph, status, bankruptcy, date_imported FROM view_intake_clients WITH (NOLOCK) WHERE date_imported IS NULL";

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(Storage.ds, "intake_clients");
                    }
                }

                GridControl1.DataSource = new DataView(Storage.ds.Tables[0], "date_imported IS NULL", string.Empty, DataViewRowState.CurrentRows);
                GridControl1.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading inake client list");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Process a change in the current row for the control
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            ControlRow = e.FocusedRowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            intake_client = -1;

            if (ControlRow >= 0)
            {
                drv = (DataRowView)gv.GetRow(ControlRow);
                if (drv != null && drv["intake_client"] != null && !object.ReferenceEquals(drv["intake_client"], DBNull.Value))
                {
                    intake_client = Convert.ToInt32(drv["intake_client"]);
                }
            }

            Button_Edit.Enabled = (intake_client > 0);
        }

        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            ControlRow = hi.RowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (ControlRow >= 0)
            {
                drv = (DataRowView)gv.GetRow(ControlRow);
            }
            else
            {
                drv = null;
            }

            // If there is a row then select it
            intake_client = -1;
            if (drv != null)
            {
                if (drv["intake_client"] != null && !object.ReferenceEquals(drv["intake_client"], DBNull.Value))
                    intake_client = Convert.ToInt32(drv["intake_client"]);
            }

            // If there is a workshop then select it
            if (intake_client > 0)
                Button_Edit.PerformClick();
        }

        /// <summary>
        /// Process a click event on the row in the grid
        /// </summary>
        private void GridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo(new Point(e.X, e.Y));

            // Remember the position for the popup menu handler.
            intake_client = -1;
            if (hi.InRow)
            {
                ControlRow = hi.RowHandle;

                if (ControlRow >= 0)
                {
                    DataRowView drv = (DataRowView)GridView1.GetRow(ControlRow);
                    if (drv != null)
                    {
                        if (drv["intake_client"] != null && !object.ReferenceEquals(drv["intake_client"], DBNull.Value))
                            intake_client = Convert.ToInt32(drv["intake_client"]);
                    }
                }
            }

            Button_Edit.Enabled = (intake_client > 0);
        }

        /// <summary>
        /// Delete the client in the list
        /// </summary>

        private void Button_Delete_Click(object sender, EventArgs e)
        {
            // Delete the row from the dataset. It will be actually deleted when the form is closed.
            if (ControlRow >= 0)
            {
                DataRowView drv = (DataRowView)GridView1.GetRow(ControlRow);
                if (DebtPlus.Data.Prompts.RequestConfirmation_Delete() == System.Windows.Forms.DialogResult.Yes)
                    drv.Delete();
            }
        }

        /// <summary>
        /// Post the updates to the database
        /// </summary>

        private void MainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand delete_cmd = new SqlCommand())
                {
                    delete_cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    delete_cmd.CommandText = "xpr_intake_delete";
                    delete_cmd.CommandType = CommandType.StoredProcedure;
                    delete_cmd.Parameters.Add("@intake_client", SqlDbType.Int, 0, "intake_client");

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                    {
                        da.DeleteCommand = delete_cmd;
                        da.Update(Storage.ds.Tables[0]);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating intake clients");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Perform the edit of the current row when needed
        /// </summary>

        private void Button_Edit_Click(object sender, EventArgs e)
        {
            DataRowView drv = (DataRowView)GridView1.GetRow(ControlRow);

            // Edit the current row.
            drv.BeginEdit();
            using (IntakeClientForm frm = new IntakeClientForm(drv))
            {
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    drv.EndEdit();
                }
                else
                {
                    drv.CancelEdit();
                }
            }
        }

        private void BarButtonItem2_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DebtPlus.Reports.Intake.ClientList.IntakeClientListReport rpt = new DebtPlus.Reports.Intake.ClientList.IntakeClientListReport();
            if (rpt.RequestReportParameters() == DialogResult.OK)
            {
                rpt.RunReportInSeparateThread();
            }
        }

        private void BarButtonItem1_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void BarButtonItem3_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Int32 RowHandle = GridView1.FocusedRowHandle;
            Int32 InterNetClient = -1;

            if (RowHandle >= 0)
            {
                DataRowView DataRow = (DataRowView)GridView1.GetRow(RowHandle);
                if (DataRow != null)
                {
                    InterNetClient = global::DebtPlus.Utils.Nulls.DInt(DataRow["intake_client"]);
                }
            }

            if (InterNetClient >= 0)
            {
                DebtPlus.Reports.Intake.ClientInfo.IntakeClientInfoReport rpt = new DebtPlus.Reports.Intake.ClientInfo.IntakeClientInfoReport();
                rpt.Parameter_IntakeClient = InterNetClient;
                if (((DebtPlus.Interfaces.Reports.IReports)rpt).RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
                {
                    rpt.RunReportInSeparateThread();
                }
            }
        }
    }
}