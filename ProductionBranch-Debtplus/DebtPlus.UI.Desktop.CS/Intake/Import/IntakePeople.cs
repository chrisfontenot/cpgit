using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import
{
    public partial class IntakePeople : DevExpress.XtraEditors.XtraUserControl
    {
        public event DataChangedEventHandler DataChanged;

        public delegate void DataChangedEventHandler(object sender, EventArgs e);

        private Int32 IntakeClient = -1;
        public Int32 person { get; set; }

        public IntakePeople() : base()
        {
            InitializeComponent();
            Button_Update.Click += Button_Update_Click;
            NoEmployer.EditValueChanged += NoEmployer_EditValueChanged;
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl client_name;
        internal DevExpress.XtraEditors.GroupControl GroupControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl employer;
        internal DevExpress.XtraEditors.SimpleButton Button_Update;
        internal DevExpress.XtraEditors.CheckEdit NoEmployer;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.client_name = new DevExpress.XtraEditors.LabelControl();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.NoEmployer = new DevExpress.XtraEditors.CheckEdit();
            this.Button_Update = new DevExpress.XtraEditors.SimpleButton();
            this.employer = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.NoEmployer.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(8, 0);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(31, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Name:";
            this.LabelControl1.UseMnemonic = false;
            //
            //client_name
            //
            this.client_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.client_name.Location = new System.Drawing.Point(64, 0);
            this.client_name.Name = "client_name";
            this.client_name.Size = new System.Drawing.Size(208, 14);
            this.client_name.TabIndex = 1;
            this.client_name.UseMnemonic = false;
            //
            //GroupControl1
            //
            this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GroupControl1.Controls.Add(this.NoEmployer);
            this.GroupControl1.Controls.Add(this.Button_Update);
            this.GroupControl1.Controls.Add(this.employer);
            this.GroupControl1.Location = new System.Drawing.Point(64, 16);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.ShowCaption = false;
            this.GroupControl1.Size = new System.Drawing.Size(216, 168);
            this.GroupControl1.TabIndex = 2;
            this.GroupControl1.Text = "GroupControl1";
            //
            //NoEmployer
            //
            this.NoEmployer.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.NoEmployer.Location = new System.Drawing.Point(8, 143);
            this.NoEmployer.Name = "NoEmployer";
            //
            //NoEmployer.Properties
            //
            this.NoEmployer.Properties.Caption = "Import without an employer";
            this.NoEmployer.Size = new System.Drawing.Size(208, 19);
            this.NoEmployer.TabIndex = 2;
            //
            //Button_Update
            //
            this.Button_Update.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Update.Location = new System.Drawing.Point(71, 112);
            this.Button_Update.Name = "Button_Update";
            this.Button_Update.TabIndex = 1;
            this.Button_Update.Text = "Update...";
            //
            //employer
            //
            this.employer.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.employer.Location = new System.Drawing.Point(8, 8);
            this.employer.Name = "employer";
            this.employer.Size = new System.Drawing.Size(200, 96);
            this.employer.TabIndex = 0;
            this.employer.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 24);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(48, 13);
            this.LabelControl2.TabIndex = 4;
            this.LabelControl2.Text = "Employer:";
            this.LabelControl2.UseMnemonic = false;
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 40);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(43, 13);
            this.LabelControl3.TabIndex = 5;
            this.LabelControl3.Text = "Address:";
            this.LabelControl3.UseMnemonic = false;
            //
            //IntakePeople
            //
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.GroupControl1);
            this.Controls.Add(this.client_name);
            this.Controls.Add(this.LabelControl1);
            this.Name = "IntakePeople";
            this.Size = new System.Drawing.Size(280, 184);
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.NoEmployer.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        public void ReadForm(Int32 intake_client)
        {
            IntakeClient = intake_client;
            RefreshInformation();
        }

        public bool isValid
        {
            get { return !person_read || (person_employer > 0 || NoEmployer.Checked); }
        }

        private void Button_Update_Click(object sender, EventArgs e)
        {
            // Ignore the button if the person is not valid
            if (!person_read)
                return;

            // If there is an employer then generate a warning.
            if (person_employer > 0)
            {
                if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("The employer is already specified for this person.{0}{0}If you continue, you will erase the previous employer.{0}Are you sure that you want to do this?", Environment.NewLine), "Warning: Employer about to be overwritten", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                {
                    return;
                }
            }

            // Do the update for the employer
            Int32 NewEmployer = -1;
            using (Employers.EmployerListForm frm = new Employers.EmployerListForm())
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    NewEmployer = frm.Employer;
                }
            }

            // If there is a new employer then update the people table accordingly
            if (NewEmployer > 0)
            {
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with1 = cmd;
                        _with1.Connection = cn;
                        _with1.CommandText = "UPDATE intake_people SET employer=e.employer, employer_name=e.name, employer_address1 = dbo.format_Address_Line_1(ea.house,ea.direction,ea.street,ea.suffix,ea.modifier,ea.modifier_value), employer_address2 = ea.address_line_2, employer_city=ea.city, employer_state=ea.state, employer_postalcode=ea.postalcode FROM intake_people p LEFT OUTER JOIN employers e WITH (NOLOCK) ON @employer = e.employer LEFT OUTER JOIN addresses ea WITH (NOLOCK) ON e.AddressID = ea.Address WHERE p.intake_client=@intake_client AND p.person = @person";
                        _with1.Parameters.Add("@intake_client", SqlDbType.Int).Value = IntakeClient;
                        _with1.Parameters.Add("@person", SqlDbType.Int).Value = person;
                        _with1.Parameters.Add("@employer", SqlDbType.Int).Value = NewEmployer;
                        _with1.ExecuteNonQuery();
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting employer in intake_people");
                }
                finally
                {
                    System.Windows.Forms.Cursor.Current = current_cursor;
                    if (cn != null)
                        cn.Dispose();
                }

                // Reload the client information with the new employer data
                RefreshInformation();
                if (DataChanged != null)
                {
                    DataChanged(this, e);
                }
            }
        }

        private bool person_read;
        private Int32 person_employer = -1;
        private string person_name = string.Empty;
        private string person_employer_name = string.Empty;
        private string person_employer_address1 = string.Empty;
        private string person_employer_address2 = string.Empty;

        private string person_employer_address3 = string.Empty;

        private void RefreshInformation()
        {
            // Clear the current settings
            person_read = false;
            person_employer = -1;
            person_name = string.Empty;
            person_employer_name = string.Empty;
            person_employer_address1 = string.Empty;
            person_employer_address2 = string.Empty;
            person_employer_address3 = string.Empty;

            // Read the personal information for the client
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlDataReader rd = null;

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = cn;
                    _with2.CommandText = "SELECT name, employer, employer_name, employer_address1, employer_address2, employer_address3 FROM view_intake_people WITH (NOLOCK) WHERE intake_client=@intake_client AND person=@person";
                    _with2.Parameters.Add("@intake_client", SqlDbType.Int).Value = IntakeClient;
                    _with2.Parameters.Add("@person", SqlDbType.Int).Value = person;
                    rd = _with2.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);
                }

                if (rd.Read())
                {
                    person_read = true;
                    if (!rd.IsDBNull(0))
                        person_name = rd.GetString(0).Trim();
                    if (!rd.IsDBNull(1))
                        person_employer = Convert.ToInt32(rd.GetValue(1));
                    if (!rd.IsDBNull(2))
                        person_employer_name = rd.GetString(2).Trim();
                    if (!rd.IsDBNull(3))
                        person_employer_address1 = rd.GetString(3).Trim();
                    if (!rd.IsDBNull(4))
                        person_employer_address2 = rd.GetString(4).Trim();
                    if (!rd.IsDBNull(5))
                        person_employer_address3 = rd.GetString(5).Trim();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading intake_people");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
                if (rd != null)
                    rd.Dispose();
                if (cn != null)
                    cn.Dispose();
            }

            // Fill in the required values
            client_name.Text = person_name;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            if (person_employer_name != string.Empty)
            {
                sb.Append(Environment.NewLine);
                sb.Append(person_employer_name);
            }

            if (person_employer_address1 != string.Empty)
            {
                sb.Append(Environment.NewLine);
                sb.Append(person_employer_address1);
            }

            if (person_employer_address2 != string.Empty)
            {
                sb.Append(Environment.NewLine);
                sb.Append(person_employer_address2);
            }

            if (person_employer_address3 != string.Empty)
            {
                sb.Append(Environment.NewLine);
                sb.Append(person_employer_address3);
            }

            if (sb.Length > 0)
                sb.Remove(0, 2);
            employer.Text = sb.ToString();

            // If there is an employer then make everything "grey".
            if (person_employer > 0)
            {
                NoEmployer.Checked = false;
                NoEmployer.Enabled = false;
                // Button_Update.Enabled = False
            }
            else
            {
                // The employer is not specified.
                NoEmployer.Checked = false;
                NoEmployer.Enabled = true;
                // Button_Update.Enabled = False

                // If there is no name then don't bother requiring that one be given.
                if (person_employer_name.Length == 0)
                {
                    NoEmployer.Checked = true;
                    NoEmployer.Enabled = false;

                    // Enable the button to update the employer if one is not present
                    //ElseIf person_employer <= 0 Then
                    //Button_Update.Enabled = True
                }
            }
        }

        private void NoEmployer_EditValueChanged(object sender, EventArgs e)
        {
            if (DataChanged != null)
            {
                DataChanged(this, e);
            }
        }
    }
}