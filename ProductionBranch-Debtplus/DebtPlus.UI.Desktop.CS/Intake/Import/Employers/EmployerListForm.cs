using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Intake.Import.Employers
{
    public partial class EmployerListForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Int32 Employer = -1;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public EmployerListForm() : base()
        {
            InitializeComponent();
            this.Load += EmployerListForm_Load;
            EmployerList1.Cancelled += EmployerList1_Cancelled;
            EmployerList1.ItemSelected += EmployerList1_ItemSelected;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal Employers.EmployerList EmployerList1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.EmployerList1 = new EmployerList();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //EmployerList1
            //
            this.EmployerList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EmployerList1.Employer = -1;
            this.EmployerList1.Location = new System.Drawing.Point(0, 0);
            this.EmployerList1.Name = "EmployerList1";
            this.EmployerList1.Size = new System.Drawing.Size(416, 278);
            this.EmployerList1.TabIndex = 0;
            //
            //ListForm
            //
            this.ClientSize = new System.Drawing.Size(416, 278);
            this.Controls.Add(this.EmployerList1);
            this.Name = "ListForm";
            this.Text = "List of Employers";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void EmployerListForm_Load(object sender, EventArgs e)
        {
            EmployerList1.LoadForm(Employer);
        }

        private void EmployerList1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void EmployerList1_ItemSelected(object sender, EventArgs e)
        {
            Employer = EmployerList1.Employer;
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}