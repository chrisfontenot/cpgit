#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import.Employers
{
    public partial class NewForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private readonly DataRowView drv;

        /// <summary>
        /// Initialize the form
        /// </summary>
        public NewForm() : base()
        {
            InitializeComponent();
        }

        public NewForm(DataRowView drv) : this()
        {
            this.drv = drv;
            this.Load += NewEmployerForm_Load;
            Button_OK.Click += Button_OK_Click;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.Utils.SuperToolTip SuperToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem ToolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem ToolTipItem1 = new DevExpress.Utils.ToolTipItem();
            this.employer_name = new DevExpress.XtraEditors.TextEdit();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.TelephoneNumberRecordControl_FAXID = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.TelephoneNumberRecordControl_TelephoneID = new DebtPlus.Data.Controls.TelephoneNumberRecordControl();
            this.AddressRecordControl_AddressID = new DebtPlus.Data.Controls.AddressRecordControl();
            this.industry = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.employer_name.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl_FAXID).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl_TelephoneID).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.AddressRecordControl_AddressID).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.industry.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).BeginInit();
            this.SuspendLayout();
            //
            //employer_name
            //
            this.employer_name.Location = new System.Drawing.Point(57, 12);
            this.employer_name.Name = "employer_name";
            this.employer_name.Properties.MaxLength = 50;
            this.employer_name.Size = new System.Drawing.Size(411, 20);
            this.employer_name.StyleController = this.LayoutControl1;
            ToolTipTitleItem1.Text = "Required Field";
            ToolTipItem1.LeftIndent = 6;
            ToolTipItem1.Text = "The name of the employer";
            SuperToolTip1.Items.Add(ToolTipTitleItem1);
            SuperToolTip1.Items.Add(ToolTipItem1);
            this.employer_name.SuperTip = SuperToolTip1;
            this.employer_name.TabIndex = 1;
            //
            //LayoutControl1
            //
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl_FAXID);
            this.LayoutControl1.Controls.Add(this.TelephoneNumberRecordControl_TelephoneID);
            this.LayoutControl1.Controls.Add(this.AddressRecordControl_AddressID);
            this.LayoutControl1.Controls.Add(this.employer_name);
            this.LayoutControl1.Controls.Add(this.industry);
            this.LayoutControl1.Controls.Add(this.Button_OK);
            this.LayoutControl1.Controls.Add(this.Button_Cancel);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(480, 219);
            this.LayoutControl1.TabIndex = 12;
            this.LayoutControl1.Text = "LayoutControl1";
            //
            //TelephoneNumberRecordControl_FAXID
            //
            this.TelephoneNumberRecordControl_FAXID.Location = new System.Drawing.Point(261, 112);
            this.TelephoneNumberRecordControl_FAXID.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl_FAXID.Name = "TelephoneNumberRecordControl_FAXID";
            this.TelephoneNumberRecordControl_FAXID.Size = new System.Drawing.Size(207, 20);
            this.TelephoneNumberRecordControl_FAXID.TabIndex = 6;
            this.ToolTipController1.SetToolTip(this.TelephoneNumberRecordControl_FAXID, "If you have a FAX number, enter it here");
            //
            //TelephoneNumberRecordControl_TelephoneID
            //
            this.TelephoneNumberRecordControl_TelephoneID.Location = new System.Drawing.Point(57, 112);
            this.TelephoneNumberRecordControl_TelephoneID.Margin = new System.Windows.Forms.Padding(0);
            this.TelephoneNumberRecordControl_TelephoneID.Name = "TelephoneNumberRecordControl_TelephoneID";
            this.TelephoneNumberRecordControl_TelephoneID.Size = new System.Drawing.Size(155, 20);
            this.TelephoneNumberRecordControl_TelephoneID.TabIndex = 5;
            this.ToolTipController1.SetToolTip(this.TelephoneNumberRecordControl_TelephoneID, "If you have a telephone number, enter it here");
            //
            //AddressRecordControl_AddressID
            //
            this.AddressRecordControl_AddressID.Location = new System.Drawing.Point(57, 36);
            this.AddressRecordControl_AddressID.Name = "AddressRecordControl_AddressID";
            this.AddressRecordControl_AddressID.Size = new System.Drawing.Size(411, 72);
            this.AddressRecordControl_AddressID.TabIndex = 4;
            //
            //industry
            //
            this.industry.Location = new System.Drawing.Point(57, 136);
            this.industry.Name = "industry";
            this.industry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.industry.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("industry", "Industry", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.industry.Properties.DisplayMember = "description";
            this.industry.Properties.NullText = "...Please Choose One ...";
            this.industry.Properties.ShowFooter = false;
            this.industry.Properties.ShowHeader = false;
            this.industry.Properties.ShowLines = false;
            this.industry.Properties.ValueMember = "industry";
            this.industry.Size = new System.Drawing.Size(411, 20);
            this.industry.StyleController = this.LayoutControl1;
            this.industry.TabIndex = 9;
            this.industry.ToolTip = "It is helpful if you would please choose an apporiate industry for this employer." + "";
            this.industry.Properties.SortColumnIndex = 1;
            //
            //Button_OK
            //
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(157, 184);
            this.Button_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.StyleController = this.LayoutControl1;
            this.Button_OK.TabIndex = 10;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(236, 184);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.StyleController = this.LayoutControl1;
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Cancel";
            //
            //LayoutControlGroup1
            //
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
                this.LayoutControlItem2,
                this.LayoutControlItem1,
                this.LayoutControlItem3,
                this.LayoutControlItem4,
                this.LayoutControlItem6,
                this.LayoutControlItem7,
                this.LayoutControlItem5,
                this.EmptySpaceItem1,
                this.EmptySpaceItem3,
                this.EmptySpaceItem4
            });
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(480, 219);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            //
            //LayoutControlItem2
            //
            this.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LayoutControlItem2.Control = this.AddressRecordControl_AddressID;
            this.LayoutControlItem2.CustomizationFormText = "Address";
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 24);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(460, 76);
            this.LayoutControlItem2.Text = "Address";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(41, 13);
            //
            //LayoutControlItem1
            //
            this.LayoutControlItem1.Control = this.employer_name;
            this.LayoutControlItem1.CustomizationFormText = "Name";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Size = new System.Drawing.Size(460, 24);
            this.LayoutControlItem1.Text = "Name";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(41, 13);
            //
            //LayoutControlItem3
            //
            this.LayoutControlItem3.Control = this.TelephoneNumberRecordControl_TelephoneID;
            this.LayoutControlItem3.CustomizationFormText = "Phone";
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 100);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(204, 24);
            this.LayoutControlItem3.Text = "Phone";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(41, 13);
            //
            //LayoutControlItem4
            //
            this.LayoutControlItem4.Control = this.TelephoneNumberRecordControl_FAXID;
            this.LayoutControlItem4.CustomizationFormText = "FAX";
            this.LayoutControlItem4.Location = new System.Drawing.Point(204, 100);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(256, 24);
            this.LayoutControlItem4.Text = "FAX";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(41, 13);
            //
            //LayoutControlItem6
            //
            this.LayoutControlItem6.Control = this.Button_OK;
            this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
            this.LayoutControlItem6.Location = new System.Drawing.Point(145, 172);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem6.Text = "LayoutControlItem6";
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem6.TextToControlDistance = 0;
            this.LayoutControlItem6.TextVisible = false;
            //
            //LayoutControlItem7
            //
            this.LayoutControlItem7.Control = this.Button_Cancel;
            this.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7";
            this.LayoutControlItem7.Location = new System.Drawing.Point(224, 172);
            this.LayoutControlItem7.Name = "LayoutControlItem7";
            this.LayoutControlItem7.Size = new System.Drawing.Size(79, 27);
            this.LayoutControlItem7.Text = "LayoutControlItem7";
            this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem7.TextToControlDistance = 0;
            this.LayoutControlItem7.TextVisible = false;
            //
            //LayoutControlItem5
            //
            this.LayoutControlItem5.Control = this.industry;
            this.LayoutControlItem5.CustomizationFormText = "Industry";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 124);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(460, 24);
            this.LayoutControlItem5.Text = "Industry";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(41, 13);
            //
            //EmptySpaceItem1
            //
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 148);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(460, 24);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem3
            //
            this.EmptySpaceItem3.AllowHotTrack = false;
            this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
            this.EmptySpaceItem3.Location = new System.Drawing.Point(0, 172);
            this.EmptySpaceItem3.Name = "EmptySpaceItem3";
            this.EmptySpaceItem3.Size = new System.Drawing.Size(145, 27);
            this.EmptySpaceItem3.Text = "EmptySpaceItem3";
            this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            //
            //EmptySpaceItem4
            //
            this.EmptySpaceItem4.AllowHotTrack = false;
            this.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4";
            this.EmptySpaceItem4.Location = new System.Drawing.Point(303, 172);
            this.EmptySpaceItem4.Name = "EmptySpaceItem4";
            this.EmptySpaceItem4.Size = new System.Drawing.Size(157, 27);
            this.EmptySpaceItem4.Text = "EmptySpaceItem4";
            this.EmptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            //
            //NewForm
            //
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(480, 219);
            this.Controls.Add(this.LayoutControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "NewForm";
            this.Text = "New Employer Information";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.employer_name.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl_FAXID).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TelephoneNumberRecordControl_TelephoneID).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.AddressRecordControl_AddressID).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.industry.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).EndInit();
            this.ResumeLayout(false);
        }

        internal DevExpress.XtraEditors.TextEdit employer_name;
        internal DevExpress.XtraEditors.LookUpEdit industry;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
        internal DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl_FAXID;
        internal DebtPlus.Data.Controls.TelephoneNumberRecordControl TelephoneNumberRecordControl_TelephoneID;
        internal DebtPlus.Data.Controls.AddressRecordControl AddressRecordControl_AddressID;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
        internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem4;

        #endregion " Windows Form Designer generated code "

        private void NewEmployerForm_Load(object sender, EventArgs e)
        {
            // Load the industry table
            DataTable tbl = IndustriesTable();

            // Before we bind the item to the row, make sure that row has a value.
            // Choose the first item in the table.
            if (object.ReferenceEquals(drv["industry"], DBNull.Value))
            {
                object DefaultItem = tbl.Compute("min(industry)", string.Empty);
                if (object.ReferenceEquals(DefaultItem, DBNull.Value))
                    DefaultItem = 0;
                drv["industry"] = DefaultItem;
            }

            var _with1 = industry;
            _with1.Properties.DataSource = tbl.DefaultView;
            _with1.DataBindings.Clear();
            _with1.DataBindings.Add("EditValue", drv, "industry", false, DataSourceUpdateMode.OnPropertyChanged);
            _with1.EditValueChanging += global::DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            // Bind the data to the rows
            var _with2 = employer_name;
            _with2.DataBindings.Clear();
            _with2.DataBindings.Add("EditValue", drv, "name", false, DataSourceUpdateMode.OnPropertyChanged);
            _with2.EditValueChanging += employer_name_EditValueChanging;

            TelephoneNumberRecordControl_TelephoneID.EditValue = global::DebtPlus.Utils.Nulls.v_Int32(drv["TelephoneID"]);
            TelephoneNumberRecordControl_FAXID.EditValue = global::DebtPlus.Utils.Nulls.v_Int32(drv["FAXID"]);
            AddressRecordControl_AddressID.EditValue = global::DebtPlus.Utils.Nulls.v_Int32(drv["AddressID"]);
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            // Save the non-bound controls
            drv["AddressID"] = global::DebtPlus.Utils.Nulls.ToDbNull(AddressRecordControl_AddressID.EditValue);
            drv["TelephoneID"] = global::DebtPlus.Utils.Nulls.ToDbNull(TelephoneNumberRecordControl_TelephoneID.EditValue);
            drv["FAXID"] = global::DebtPlus.Utils.Nulls.ToDbNull(TelephoneNumberRecordControl_FAXID.EditValue);

            // Update the formatted items in the dataset from the displayed controls
            DebtPlus.LINQ.address v = AddressRecordControl_AddressID.GetCurrentValues();
            drv["address"] = v.ToString();

            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void employer_name_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            Button_OK.Enabled = global::DebtPlus.Utils.Nulls.DStr(e.NewValue).Trim() != string.Empty;
        }
    }
}