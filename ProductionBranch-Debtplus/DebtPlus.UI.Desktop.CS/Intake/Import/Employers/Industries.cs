using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import.Employers
{
    partial class NewForm
    {
        /// <summary>
        /// Industry information table
        /// </summary>
        public DataTable IndustriesTable()
        {
            DataTable tbl = Storage.ds.Tables[Names.industries];

            // If the table is not found then read the information from the database
            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    lock (Storage.ds)
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with1 = cmd;
                            _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            _with1.CommandText = "SELECT [industry],[description],[ActiveFlag],[Default],[note] FROM industries WITH (NOLOCK)";

                            // Ask the database for the information
                            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.Fill(Storage.ds, Names.industries);
                                tbl = Storage.ds.Tables[Names.industries];
                            }
                        }
                    }

                    // When we do an update, the update results in zero rows being updated. IGNORE IT.
                }
                catch (DBConcurrencyException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                    // Something happend in trying to talk to the database.
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }
    }
}