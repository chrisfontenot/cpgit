#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import.Employers
{
    internal partial class EmployerList : DevExpress.XtraEditors.XtraUserControl
    {
        public event EventHandler ItemSelected;

        public event EventHandler Cancelled;

        private Int32 ControlRow = -1;

        public EmployerList() : base()
        {
            InitializeComponent();
            Button_Select.Click += Button_Select_Click;
            Button_Cancel.Click += Button_Cancel_Click;
            Button_New.Click += Button_New_Click;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.MouseDown += GridView1_MouseDown;
            TextEditFilter.EditValueChanging += TextEditFilter_EditValueChanging;
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.GridControl GridControl1;

        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Employer;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Name;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Address1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Address2;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Address3;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_City;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_State;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_PostalCode;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Address;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.SimpleButton Button_Select;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.TextEdit TextEditFilter;
        internal DevExpress.XtraEditors.SimpleButton Button_New;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_Employer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Address1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Address2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Address3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_City = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_State = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_PostalCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Address = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_Select = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_New = new DevExpress.XtraEditors.SimpleButton();
            this.TextEditFilter = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEditFilter.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Location = new System.Drawing.Point(0, 0);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(424, 224);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_Employer,
                this.GridColumn_Name,
                this.GridColumn_Address1,
                this.GridColumn_Address2,
                this.GridColumn_Address3,
                this.GridColumn_City,
                this.GridColumn_State,
                this.GridColumn_PostalCode,
                this.GridColumn_Address
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.OptionsView.ShowPreview = true;
            this.GridView1.PreviewFieldName = "address";
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_Name, DevExpress.Data.ColumnSortOrder.Ascending) });
            //
            //GridColumn_Employer
            //
            this.GridColumn_Employer.Caption = "Employer";
            this.GridColumn_Employer.CustomizationCaption = "Employer ID";
            this.GridColumn_Employer.Name = "GridColumn_Employer";
            this.GridColumn_Employer.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_Name
            //
            this.GridColumn_Name.Caption = "Name";
            this.GridColumn_Name.CustomizationCaption = "Employer Name";
            this.GridColumn_Name.FieldName = "name";
            this.GridColumn_Name.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical;
            this.GridColumn_Name.Name = "GridColumn_Name";
            this.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Name.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_Name.Visible = true;
            this.GridColumn_Name.VisibleIndex = 0;
            //
            //GridColumn_Address1
            //
            this.GridColumn_Address1.Caption = "Address Line 1";
            this.GridColumn_Address1.CustomizationCaption = "Address: Line 1";
            this.GridColumn_Address1.FieldName = "employer_address1";
            this.GridColumn_Address1.Name = "GridColumn_Address1";
            this.GridColumn_Address1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_Address2
            //
            this.GridColumn_Address2.Caption = "Address Line 2";
            this.GridColumn_Address2.CustomizationCaption = "Address: Line 2";
            this.GridColumn_Address2.FieldName = "address2";
            this.GridColumn_Address2.Name = "GridColumn_Address2";
            this.GridColumn_Address2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_Address3
            //
            this.GridColumn_Address3.Caption = "Address Line 3";
            this.GridColumn_Address3.CustomizationCaption = "Address: Line 3";
            this.GridColumn_Address3.FieldName = "address3";
            this.GridColumn_Address3.Name = "GridColumn_Address3";
            this.GridColumn_Address3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_City
            //
            this.GridColumn_City.Caption = "City";
            this.GridColumn_City.CustomizationCaption = "Address: City";
            this.GridColumn_City.FieldName = "city";
            this.GridColumn_City.Name = "GridColumn_City";
            this.GridColumn_City.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_State
            //
            this.GridColumn_State.Caption = "State";
            this.GridColumn_State.CustomizationCaption = "Address: State";
            this.GridColumn_State.FieldName = "state";
            this.GridColumn_State.Name = "GridColumn_State";
            this.GridColumn_State.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_PostalCode
            //
            this.GridColumn_PostalCode.Caption = "Postal Code";
            this.GridColumn_PostalCode.CustomizationCaption = "Address: Postal Code";
            this.GridColumn_PostalCode.FieldName = "postalcode";
            this.GridColumn_PostalCode.Name = "GridColumn_PostalCode";
            this.GridColumn_PostalCode.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_Address
            //
            this.GridColumn_Address.Caption = "Address";
            this.GridColumn_Address.CustomizationCaption = "All Address Lines";
            this.GridColumn_Address.FieldName = "address";
            this.GridColumn_Address.Name = "GridColumn_Address";
            this.GridColumn_Address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(16, 244);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(28, 13);
            this.LabelControl1.TabIndex = 2;
            this.LabelControl1.Text = "&Filter:";
            //
            //Button_Select
            //
            this.Button_Select.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Select.Location = new System.Drawing.Point(144, 240);
            this.Button_Select.Name = "Button_Select";
            this.Button_Select.Size = new System.Drawing.Size(75, 23);
            this.Button_Select.TabIndex = 3;
            this.Button_Select.Text = "&Select";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.Location = new System.Drawing.Point(232, 240);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 4;
            this.Button_Cancel.Text = "&Cancel";
            //
            //Button_New
            //
            this.Button_New.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
            this.Button_New.Location = new System.Drawing.Point(320, 240);
            this.Button_New.Name = "Button_New";
            this.Button_New.Size = new System.Drawing.Size(75, 23);
            this.Button_New.TabIndex = 5;
            this.Button_New.Text = "&New...";
            //
            //TextEditFilter
            //
            this.TextEditFilter.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.TextEditFilter.Location = new System.Drawing.Point(50, 240);
            this.TextEditFilter.Name = "TextEditFilter";
            this.TextEditFilter.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEditFilter.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.TextEditFilter.Properties.Appearance.Options.UseFont = true;
            this.TextEditFilter.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextEditFilter.Size = new System.Drawing.Size(78, 20);
            this.TextEditFilter.TabIndex = 1;
            //
            //EmployerList
            //
            this.Controls.Add(this.Button_New);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Select);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.TextEditFilter);
            this.Name = "EmployerList";
            this.Size = new System.Drawing.Size(424, 280);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.TextEditFilter.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Process the load event on the control
        /// </summary>
        public void LoadForm(Int32 Employer)
        {
            this.Employer = Employer;

            var _with1 = Button_Cancel;
            _with1.Tag = "cancel";

            var _with2 = Button_New;
            _with2.Tag = "new";

            var _with3 = Button_Select;
            _with3.Tag = "ok";

            // Mark the current employer in the list
            DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            var _with4 = StyleFormatCondition1;
            var _with5 = _with4.Appearance;
            _with5.BackColor = Color.Navy;
            _with5.Font = new Font("Tahoma", 8.25f, FontStyle.Bold);
            _with5.ForeColor = Color.White;
            _with5.Options.UseBackColor = true;
            _with5.Options.UseFont = true;
            _with5.Options.UseForeColor = true;
            _with4.ApplyToRow = true;
            _with4.Column = GridColumn_Employer;
            _with4.Expression = string.Format("[employer]={0:f0}", Employer);
            _with4.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            GridView1.FormatConditions.Add(StyleFormatCondition1);

            var _with6 = GridView1;
            _with6.OptionsSelection.EnableAppearanceFocusedRow = false;
            _with6.OptionsSelection.EnableAppearanceFocusedCell = true;

            // Disable the SELECT button
            Button_Select.Enabled = false;

            // Preload the grid with the name
            DataRow drv = EmployersDataRow(Employer);
            if (drv != null)
            {
                TextEditFilter.Text = global::DebtPlus.Utils.Nulls.DStr(drv["name"]);
                ReadGridView1(TextEditFilter.Text);
                LoadGridView1(TextEditFilter.Text);
            }

            // Kill any selected employer from the list if the text filter changes.
            Employer = -1;
            Button_Select.Enabled = false;
        }

        private readonly DataSet employerDataSet = new DataSet("employerDataSet");
        private Int32 _Employer = -1;

        public event EventHandler EmployerChanged;

        /// <summary>
        /// The employer changed. Trip the event.
        /// </summary>
        protected virtual void OnEmployerChanged(EventArgs e)
        {
            if (EmployerChanged != null)
            {
                EmployerChanged(this, e);
            }
        }

        /// <summary>
        /// Protected function to trip the employer change event.
        /// </summary>
        protected void PerformEmployerChanged()
        {
            OnEmployerChanged(EventArgs.Empty);
        }

        /// <summary>
        /// The current employer ID
        /// </summary>
        [System.ComponentModel.Browsable(false)]
        public Int32 Employer
        {
            get { return _Employer; }
            set
            {
                if (_Employer != value)
                {
                    _Employer = value;
                    PerformEmployerChanged();
                }
            }
        }

        /// <summary>
        /// Process the SELECT button click
        /// </summary>
        private void Button_Select_Click(object sender, EventArgs e)
        {
            if (ItemSelected != null)
            {
                ItemSelected(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Process the CANCEL button click
        /// </summary>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Process the NEW button click
        /// </summary>
        private void Button_New_Click(object sender, EventArgs e)
        {
            DialogResult answer = default(DialogResult);

            // Create a new row for the employer
            DataTable tbl = EmployersTable();
            DataView vue = tbl.DefaultView;

            DataRowView drv = vue.AddNew();
            drv.BeginEdit();
            using (NewForm frm = new NewForm(drv))
            {
                answer = frm.ShowDialog();
            }

            // If the user cancelled the edit then abort the creation event
            if (answer != DialogResult.OK)
            {
                drv.CancelEdit();
            }
            else
            {
                drv.EndEdit();
                UpdateEmployerTable();

                // Find the employer value and complete the dialog
                Employer = Convert.ToInt32(drv["employer"]);
                if (ItemSelected != null)
                {
                    ItemSelected(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Update the employer when the grid control moves the focus
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            ControlRow = e.FocusedRowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (ControlRow >= 0)
            {
                drv = (DataRowView)gv.GetRow(ControlRow);
            }
            else
            {
                drv = null;
            }

            // Ask the user to do something with the selected row
            Employer = -1;
            if (drv != null)
            {
                if ((drv["employer"] != null) && !object.ReferenceEquals(drv["employer"], DBNull.Value))
                {
                    Employer = Convert.ToInt32(drv["employer"]);
                }
            }
            Button_Select.Enabled = (Employer > 0);
        }

        /// <summary>
        /// Double Click on an item -- select it
        /// </summary>

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            ControlRow = hi.RowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (ControlRow >= 0)
            {
                drv = (DataRowView)gv.GetRow(ControlRow);
            }

            // If there is a row then select it
            Employer = -1;
            if (drv != null)
            {
                if ((drv["employer"] != null) && !object.ReferenceEquals(drv["employer"], DBNull.Value))
                    Employer = Convert.ToInt32(drv["employer"]);
            }

            // If there is an employer then select it
            if (Employer > 0)
            {
                if (ItemSelected != null)
                {
                    ItemSelected(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Process a click event on the row in the grid
        /// </summary>
        private void GridView1_MouseDown(object sender, MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo(new Point(e.X, e.Y));

            // Remember the position for the popup menu handler.
            if (hi.InRow)
            {
                ControlRow = hi.RowHandle;
            }
            else
            {
                ControlRow = -1;
            }

            Employer = -1;
            if (ControlRow >= 0)
            {
                DataRowView drv = (DataRowView)GridView1.GetRow(ControlRow);
                if (drv != null)
                {
                    if ((drv["employer"] != null) && !object.ReferenceEquals(drv["employer"], DBNull.Value))
                        Employer = Convert.ToInt32(drv["employer"]);
                }
            }
            Button_Select.Enabled = (Employer > 0);
        }

        private string LastFilterCriteria;

        private void TextEditFilter_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            string OldValue = global::DebtPlus.Utils.Nulls.DStr(e.OldValue);

            // If the new value is empty then empty the list
            string NewValue = global::DebtPlus.Utils.Nulls.DStr(e.NewValue);
            if (NewValue == string.Empty)
            {
                GridControl1.DataSource = null;
            }
            else
            {
                // If the old value was empty then ensure that the new one is just not a space
                if (OldValue == string.Empty)
                {
                    if (NewValue == " ")
                    {
                        e.Cancel = true;
                        return;
                    }

                    ReadGridView1(NewValue);
                    LastFilterCriteria = NewValue;

                    // Force a reload if the first character differs
                }
                else if (LastFilterCriteria != NewValue.Substring(0, 1))
                {
                    ReadGridView1(NewValue);
                    LastFilterCriteria = NewValue;
                }

                LoadGridView1(NewValue);
            }

            // Kill any selected employer from the list if the text filter changes.
            Employer = -1;
            Button_Select.Enabled = false;
        }

        private void ReadGridView1(string NewValue)
        {
            string SelectionString = null;

            // Retrieve the entries from the database that start with the first letter
            NewValue = NewValue.Substring(0, 1);
            if (char.IsLetter(Convert.ToChar(NewValue)))
            {
                SelectionString = string.Format("e.[NAME] LIKE '{0}%'", NewValue);
            }
            else
            {
                SelectionString = "e.[NAME] NOT LIKE '[A-Z]%'";
            }

            SelectionString = string.Format("SELECT e.[employer],e.[name],dbo.address_block_5 (dbo.format_Address_Line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),a.address_line_2,a.address_line_3,dbo.format_city_state_zip(a.city, a.state, a.postalcode),default) as address FROM employers e with (nolock) LEFT OUTER JOIN addresses a ON e.AddressID = a.Address WHERE {0}", SelectionString);
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with7 = cmd;
                _with7.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with7.CommandText = SelectionString;
                _with7.CommandType = CommandType.Text;

                Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(employerDataSet, "employers");
                        var _with8 = employerDataSet.Tables["employers"];
                        _with8.PrimaryKey = new DataColumn[] { _with8.Columns["employer"] };
                    }
                }
                finally
                {
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }
        }

        private void LoadGridView1(string NewValue)
        {
            // Update the filter with the list new employers
            string SelectionString = string.Format("[name] like '{0}%'", NewValue.Replace("'", "''"));
            DataView vue = new DataView(employerDataSet.Tables["employers"], SelectionString, "[name]", DataViewRowState.CurrentRows);
            GridControl1.DataSource = vue;
        }
    }
}