using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Intake.Import.Employers
{
    partial class EmployerList
    {
        /// <summary>
        /// Update the employer table
        /// </summary>
        public Int32 UpdateEmployerTable()
        {
            DataTable tbl = Storage.ds.Tables[Names.Employers];
            Int32 RowCount = 0;

            if (tbl != null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;

                    // Update the employer row with the data
                    using (System.Data.SqlClient.SqlCommand InsertCmd = new SqlCommand())
                    {
                        var _with1 = InsertCmd;
                        _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with1.CommandText = "xpr_insert_employers";
                        _with1.CommandType = CommandType.StoredProcedure;
                        _with1.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "Employer").Direction = ParameterDirection.ReturnValue;

                        _with1.Parameters.Add("@Name", SqlDbType.VarChar, 50, "name");
                        _with1.Parameters.Add("@AddressID", SqlDbType.Int, 0, "AddressID");
                        _with1.Parameters.Add("@TelephoneID", SqlDbType.Int, 0, "TelephoneID");
                        _with1.Parameters.Add("@FAXID", SqlDbType.Int, 0, "FAXID");
                        _with1.Parameters.Add("@Industry", SqlDbType.Int, 0, "Industry");

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                        {
                            var _with2 = da;
                            _with2.InsertCommand = InsertCmd;
                            _with2.AcceptChangesDuringUpdate = true;

                            lock (Storage.ds)
                            {
                                RowCount = _with2.Update(tbl);
                            }
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding employer to employers table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return RowCount;
        }

        /// <summary>
        /// Known list of employers
        /// </summary>
        public DataTable EmployersTable()
        {
            DataTable tbl = Storage.ds.Tables[Names.Employers];

            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    lock (Storage.ds)
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with3 = cmd;
                            _with3.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            _with3.CommandText = "SELECT [employer],[name],[AddressID],[TelephoneID],[FAXID],[Industry], dbo.format_address_block([AddressID]) AS address FROM employers WHERE [employer]=@employer";
                            _with3.CommandType = CommandType.Text;

                            // Ask the database for the information
                            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.FillSchema(Storage.ds, SchemaType.Source, Names.Employers);
                            }
                        }

                        tbl = Storage.ds.Tables[Names.Employers];
                        var _with4 = tbl;
                        if (_with4.PrimaryKey.GetUpperBound(0) < 0)
                            _with4.PrimaryKey = new DataColumn[] { _with4.Columns["employer"] };

                        // Add the two columns to specifiy the grouping for the name
                        if (!_with4.Columns.Contains("firstchar"))
                        {
                            _with4.Columns.Add("firstchar", typeof(string), "substring([name],1,1)");
                        }
                        if (!_with4.Columns.Contains("grouping"))
                        {
                            _with4.Columns.Add("grouping", typeof(string), "iif([firstchar] IN ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),firstchar,'?')");
                        }

                        var _with5 = _with4.Columns["employer"];
                        if (!_with5.AutoIncrement)
                        {
                            _with5.AutoIncrement = true;
                            _with5.AutoIncrementSeed = -1;
                            _with5.AutoIncrementStep = -1;
                        }
                    }

                    // When we do an update, the update results in zero rows being updated. IGNORE IT.
                }
                catch (DBConcurrencyException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                    // Something happend in trying to talk to the database.
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }
            return tbl;
        }

        /// <summary>
        /// Refresh the employer information
        /// </summary>
        public DataTable ReadEmployersByKey(char KeyID)
        {
            DataTable tbl = Storage.ds.Tables[Names.Employers];

            // If the table is not found then read the information from the database
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                lock (Storage.ds)
                {
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with6 = cmd;
                        _with6.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with6.CommandText = "SELECT [employer],[name],[AddressID],[TelephoneID],[FAXID],[Industry], dbo.format_address_block([AddressID]) AS address FROM employers WHERE [name]";
                        _with6.CommandText += KeyID == '?' ? " NOT LIKE '[A-Z]%'" : string.Format(" LIKE '{0}%'", KeyID);

                        // Ask the database for the information
                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(Storage.ds, Names.Employers);
                            tbl = Storage.ds.Tables[Names.Employers];
                        }
                    }

                    var _with7 = tbl;
                    if (_with7.PrimaryKey.GetUpperBound(0) < 0)
                        _with7.PrimaryKey = new DataColumn[] { _with7.Columns["employer"] };

                    // Add the two columns to specifiy the grouping for the name
                    if (!_with7.Columns.Contains("firstchar"))
                    {
                        _with7.Columns.Add("firstchar", typeof(string), "substring([name],1,1)");
                    }
                    if (!_with7.Columns.Contains("grouping"))
                    {
                        _with7.Columns.Add("grouping", typeof(string), "iif([firstchar] IN ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),firstchar,'?')");
                    }

                    var _with8 = _with7.Columns["employer"];
                    if (!_with8.AutoIncrement)
                    {
                        _with8.AutoIncrement = true;
                        _with8.AutoIncrementSeed = -1;
                        _with8.AutoIncrementStep = -1;
                    }
                }

                // When we do an update, the update results in zero rows being updated. IGNORE IT.
            }
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                // Something happend in trying to talk to the database.
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            return tbl;
        }

        /// <summary>
        /// Employer information
        /// </summary>
        public DataRow EmployersDataRow(object Employer)
        {
            DataTable tbl = Storage.ds.Tables[Names.Employers];

            // If we found the table then look for the item in the table.
            if (tbl != null)
            {
                DataRow row = tbl.Rows.Find(Employer);
                if (row != null)
                    return row;
            }

            // If the table is not found then read the information from the database
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                lock (Storage.ds)
                {
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with9 = cmd;
                        _with9.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with9.CommandText = "SELECT [employer],[name],[AddressID],[TelephoneID],[FAXID],[Industry], dbo.format_address_block([AddressID]) AS address FROM employers WHERE [employer]=@employer";
                        _with9.Parameters.Add("@employer", SqlDbType.Int).Value = Employer;

                        // Ask the database for the information
                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(Storage.ds, Names.Employers);
                        }
                    }

                    tbl = Storage.ds.Tables[Names.Employers];
                    var _with10 = tbl;
                    if (_with10.PrimaryKey.GetUpperBound(0) < 0)
                        _with10.PrimaryKey = new DataColumn[] { _with10.Columns["employer"] };

                    // Add the two columns to specifiy the grouping for the name
                    if (!_with10.Columns.Contains("firstchar"))
                    {
                        _with10.Columns.Add("firstchar", typeof(string), "substring([name],1,1)");
                    }
                    if (!_with10.Columns.Contains("grouping"))
                    {
                        _with10.Columns.Add("grouping", typeof(string), "iif([firstchar] IN ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'),firstchar,'?')");
                    }

                    var _with11 = _with10.Columns["employer"];
                    if (!_with11.AutoIncrement)
                    {
                        _with11.AutoIncrement = true;
                        _with11.AutoIncrementSeed = -1;
                        _with11.AutoIncrementStep = -1;
                    }
                }

                // When we do an update, the update results in zero rows being updated. IGNORE IT.
            }
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                // Something happend in trying to talk to the database.
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // Find the single record in the table that we want
            if (tbl != null)
            {
                return tbl.Rows.Find(Employer);
            }
            return null;
        }

        /// <summary>
        /// Obtain a description for the item
        /// </summary>
        public string GetEmployerDescription(object key)
        {
            string answer = null;
            DataRow row = EmployersDataRow(key);
            if (row != null)
            {
                answer = global::DebtPlus.Utils.Nulls.DStr(row["name"]).Trim();
            }
            else
            {
                answer = string.Empty;
            }

            return answer;
        }
    }
}