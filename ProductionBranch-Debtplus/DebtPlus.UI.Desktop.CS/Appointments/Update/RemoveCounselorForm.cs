#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Desktop.CS.Appointments.Update
{
    internal partial class RemoveCounselorForm
    {
        private BusinessContext bc = null;

        public RemoveCounselorForm() : base()
        {
            InitializeComponent();
        }

        public RemoveCounselorForm(BusinessContext bc)
            : this()
        {
            this.bc = bc;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                                += Form_Load;
            CheckedListBoxControl1.ItemChecking += CheckedListBoxControl1_ItemChecking;
            SimpleButton_OK.Click               += SimpleButton_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load                                -= Form_Load;
            CheckedListBoxControl1.ItemChecking -= CheckedListBoxControl1_ItemChecking;
            SimpleButton_OK.Click               -= SimpleButton_OK_Click;
        }

        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Load the list of counselors to be disabled
                var aryList = new System.Collections.Generic.List<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>();
                foreach (var co in DebtPlus.LINQ.Cache.counselor.getList().OrderBy(s => s.Name))
                {
                    var item = new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(co, co.Name == null ? string.Empty : co.Name.ToString(), CheckState.Unchecked, co.ActiveFlag);
                    aryList.Add(item);
                }
                CheckedListBoxControl1.Items.Clear();
                CheckedListBoxControl1.Items.AddRange(aryList.ToArray());

                // Define the starting date for the date range
                DateNavigator1.Selection.Clear();
                DateNavigator1.DateTime = DateTime.Now.Date;

                // Empty the reason
                TextEdit_reason.EditValue = string.Empty;

                // Prevent the OK button from being clicked until a counselor is checked
                SimpleButton_OK.Enabled = false;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void CheckedListBoxControl1_ItemChecking(object sender, ItemCheckingEventArgs e)
        {
            // If the item is being checked then enable the ok button
            if (e.NewValue == CheckState.Checked)
            {
                SimpleButton_OK.Enabled = true;
                return;
            }

            // Count the number of checked items. If < 2 then disable the button
            Int32 ItemCount = 0;
            foreach (DebtPlus.Data.Controls.SortedCheckedListboxControlItem item in CheckedListBoxControl1.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    ItemCount += 1;
                }
            }
            SimpleButton_OK.Enabled = (ItemCount >= 2);
            // since we are clearing a check state, there can't be just one item.
        }

        private void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            // Indicate that the dialog is completed normally.
            DialogResult = System.Windows.Forms.DialogResult.OK;

            // Construct a list of the counselors to remove
            var colIds = new System.Collections.Generic.List<int>();
            foreach (DebtPlus.Data.Controls.SortedCheckedListboxControlItem item in CheckedListBoxControl1.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    var co = item.Value as DebtPlus.LINQ.counselor;
                    colIds.Add(co.Id);
                }
            }

            // Make an array for the ids are needed in the selection clause.
            var aryIds = colIds.ToArray();

            // From the date selection, find the starting and ending dates. They are not necessarily in order.
            System.DateTime fromDate = DateNavigator1.Selection.OfType<DateTime>().OrderBy(s => s).Min<DateTime>().Date;
            System.DateTime toDate   = DateNavigator1.Selection.OfType<DateTime>().OrderBy(s => s).Max<DateTime>().Date.AddDays(1);

            // Start with the basic selection from the tables
            var qAppt = bc.appt_counselors.Join(bc.appt_times, co => co.appt_time, apt => apt.Id, (co, apt) => new { co = co, apt = apt }).Join(bc.client_appointments, x => x.apt.Id, ca => ca.appt_time, (x, ca) => new { co = x.co, apt = x.apt, ca = ca });
            qAppt = qAppt.Where(s => s.ca.status == 'P');

            // Add the counselor IDs to the list
            qAppt = qAppt.Where(s => s.co.inactive == 0 && colIds.Contains(s.co.counselor));

            // Add the starting times for the items
            qAppt = qAppt.Where(s => s.apt.start_time >= fromDate && s.apt.start_time < toDate);

            foreach (var ca in qAppt.Select(x => x.ca))
            {
                ca.status       = 'C';
                ca.appt_time    = null;
                ca.date_updated = DebtPlus.LINQ.BusinessContext.getdate();
            }

            // Cancel the counselors
            var qCounselor = bc.appt_counselors.Join(bc.appt_times, co => co.appt_time, apt => apt.Id, (co, apt) => new { co = co, apt = apt });
            qCounselor = qCounselor.Where(s => s.co.inactive == 0 && colIds.Contains(s.co.counselor));
            qCounselor = qCounselor.Where(s => s.apt.start_time >= fromDate && s.apt.start_time < toDate);
            foreach (var co in qCounselor.Select(x => x.co))
            {
                co.inactive_reason = TextEdit_reason.Text.Trim();
                co.inactive        = 1;
            }
        }
    }
}