﻿namespace DebtPlus.UI.Desktop.CS.Appointments.Update
{
    partial class EditForm
    {
        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn_client_appointment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_appt_time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_counselor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_start_time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_office = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_appt_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_result = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_priority = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_callback_ph = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_confirmation_status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_date_confirmed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_date_updated = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn_formatted_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.counselors = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.office_name = new DevExpress.XtraEditors.LabelControl();
            this.start_time = new DevExpress.XtraEditors.TimeEdit();
            this.appt_type = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem_left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.counselors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.start_time.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appt_type.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // gridControl1
            // 
            this.gridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridControl1.Location = new System.Drawing.Point(165, 93);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(233, 166);
            this.gridControl1.TabIndex = 8;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn_client_appointment,
            this.gridColumn_client,
            this.gridColumn_appt_time,
            this.gridColumn_counselor,
            this.gridColumn_start_time,
            this.gridColumn_office,
            this.gridColumn_appt_type,
            this.gridColumn_status,
            this.gridColumn_result,
            this.gridColumn_priority,
            this.gridColumn_callback_ph,
            this.gridColumn_confirmation_status,
            this.gridColumn_date_confirmed,
            this.gridColumn_date_updated,
            this.gridColumn_date_created,
            this.gridColumn_created_by,
            this.gridColumn_formatted_name});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.ShowIndicator = false;
            // 
            // gridColumn_client_appointment
            // 
            this.gridColumn_client_appointment.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_client_appointment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_client_appointment.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_client_appointment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_client_appointment.Caption = "Appointment ID";
            this.gridColumn_client_appointment.CustomizationCaption = "Appointment ID";
            this.gridColumn_client_appointment.DisplayFormat.FormatString = "f0";
            this.gridColumn_client_appointment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_client_appointment.FieldName = "ClientAppointmentRecord.Id";
            this.gridColumn_client_appointment.GroupFormat.FormatString = "f0";
            this.gridColumn_client_appointment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_client_appointment.Name = "gridColumn_client_appointment";
            this.gridColumn_client_appointment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_client
            // 
            this.gridColumn_client.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_client.Caption = "Client";
            this.gridColumn_client.CustomizationCaption = "Client ID";
            this.gridColumn_client.DisplayFormat.FormatString = "f0";
            this.gridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_client.FieldName = "ClientAppointmentRecord.client";
            this.gridColumn_client.GroupFormat.FormatString = "f0";
            this.gridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_client.Name = "gridColumn_client";
            this.gridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_client.Visible = true;
            this.gridColumn_client.VisibleIndex = 0;
            this.gridColumn_client.Width = 139;
            // 
            // gridColumn_appt_time
            // 
            this.gridColumn_appt_time.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_appt_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_appt_time.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_appt_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_appt_time.Caption = "Appt Time";
            this.gridColumn_appt_time.CustomizationCaption = "Appt Time ID";
            this.gridColumn_appt_time.DisplayFormat.FormatString = "f0";
            this.gridColumn_appt_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_appt_time.FieldName = "ClientAppointmentRecord.appt_time";
            this.gridColumn_appt_time.GroupFormat.FormatString = "f0";
            this.gridColumn_appt_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_appt_time.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.gridColumn_appt_time.Name = "gridColumn_appt_time";
            this.gridColumn_appt_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_counselor
            // 
            this.gridColumn_counselor.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_counselor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_counselor.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_counselor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_counselor.Caption = "Counselor";
            this.gridColumn_counselor.CustomizationCaption = "Counselor ID";
            this.gridColumn_counselor.DisplayFormat.FormatString = "f0";
            this.gridColumn_counselor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_counselor.FieldName = "ClientAppointmentRecord.counselor";
            this.gridColumn_counselor.GroupFormat.FormatString = "f0";
            this.gridColumn_counselor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_counselor.Name = "gridColumn_counselor";
            this.gridColumn_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_start_time
            // 
            this.gridColumn_start_time.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_start_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_start_time.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_start_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_start_time.Caption = "Date/Time";
            this.gridColumn_start_time.CustomizationCaption = "Starting Time";
            this.gridColumn_start_time.DisplayFormat.FormatString = "d";
            this.gridColumn_start_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_start_time.FieldName = "ClientAppointmentRecord.start_time";
            this.gridColumn_start_time.GroupFormat.FormatString = "d";
            this.gridColumn_start_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_start_time.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.gridColumn_start_time.Name = "gridColumn_start_time";
            this.gridColumn_start_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_office
            // 
            this.gridColumn_office.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_office.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_office.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_office.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_office.Caption = "Office";
            this.gridColumn_office.CustomizationCaption = "Office ID";
            this.gridColumn_office.DisplayFormat.FormatString = "f0";
            this.gridColumn_office.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_office.FieldName = "ClientAppointmentRecord.office";
            this.gridColumn_office.GroupFormat.FormatString = "f0";
            this.gridColumn_office.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_office.Name = "gridColumn_office";
            this.gridColumn_office.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_appt_type
            // 
            this.gridColumn_appt_type.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_appt_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_appt_type.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_appt_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_appt_type.Caption = "Appt Type ID";
            this.gridColumn_appt_type.CustomizationCaption = "Appointment Type ID";
            this.gridColumn_appt_type.DisplayFormat.FormatString = "f0";
            this.gridColumn_appt_type.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_appt_type.FieldName = "ClientAppointmentRecord.appt_type";
            this.gridColumn_appt_type.GroupFormat.FormatString = "f0";
            this.gridColumn_appt_type.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_appt_type.Name = "gridColumn_appt_type";
            this.gridColumn_appt_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_status
            // 
            this.gridColumn_status.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_status.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_status.Caption = "Appt Status";
            this.gridColumn_status.CustomizationCaption = "Appointment Status ID";
            this.gridColumn_status.FieldName = "ClientAppointmentRecord.status";
            this.gridColumn_status.Name = "gridColumn_status";
            this.gridColumn_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_result
            // 
            this.gridColumn_result.Caption = "Appt Result";
            this.gridColumn_result.CustomizationCaption = "Appointment Result";
            this.gridColumn_result.FieldName = "ClientAppointmentRecord.result";
            this.gridColumn_result.Name = "gridColumn_result";
            this.gridColumn_result.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_priority
            // 
            this.gridColumn_priority.Caption = "Priority";
            this.gridColumn_priority.CustomizationCaption = "Priority Appointment?";
            this.gridColumn_priority.DisplayFormat.FormatString = "Y;N;N";
            this.gridColumn_priority.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_priority.FieldName = "ClientAppointmentRecord.priority";
            this.gridColumn_priority.Name = "gridColumn_priority";
            this.gridColumn_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_callback_ph
            // 
            this.gridColumn_callback_ph.Caption = "Callback #";
            this.gridColumn_callback_ph.CustomizationCaption = "Callback Phone ID";
            this.gridColumn_callback_ph.FieldName = "ClientAppointmentRecord.callback_ph";
            this.gridColumn_callback_ph.Name = "gridColumn_callback_ph";
            this.gridColumn_callback_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_confirmation_status
            // 
            this.gridColumn_confirmation_status.Caption = "Confirmed";
            this.gridColumn_confirmation_status.CustomizationCaption = "Confirmation Status ID";
            this.gridColumn_confirmation_status.FieldName = "ClientAppointmentRecord.confirmation_status";
            this.gridColumn_confirmation_status.Name = "gridColumn_confirmation_status";
            this.gridColumn_confirmation_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_date_confirmed
            // 
            this.gridColumn_date_confirmed.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_date_confirmed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_confirmed.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_date_confirmed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_confirmed.Caption = "Date Confirmed";
            this.gridColumn_date_confirmed.CustomizationCaption = "Date Confirmed";
            this.gridColumn_date_confirmed.DisplayFormat.FormatString = "d";
            this.gridColumn_date_confirmed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_confirmed.FieldName = "ClientAppointmentRecord.date_confirmed";
            this.gridColumn_date_confirmed.GroupFormat.FormatString = "d";
            this.gridColumn_date_confirmed.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_confirmed.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.gridColumn_date_confirmed.Name = "gridColumn_date_confirmed";
            this.gridColumn_date_confirmed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_date_updated
            // 
            this.gridColumn_date_updated.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_date_updated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_updated.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_date_updated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_updated.Caption = "Date Updated";
            this.gridColumn_date_updated.CustomizationCaption = "Date Last Changed";
            this.gridColumn_date_updated.DisplayFormat.FormatString = "d";
            this.gridColumn_date_updated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_updated.FieldName = "ClientAppointmentRecord.date_updated";
            this.gridColumn_date_updated.GroupFormat.FormatString = "d";
            this.gridColumn_date_updated.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_updated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.gridColumn_date_updated.Name = "gridColumn_date_updated";
            this.gridColumn_date_updated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_date_created
            // 
            this.gridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_date_created.Caption = "Date Made";
            this.gridColumn_date_created.CustomizationCaption = "Date Appointment Made";
            this.gridColumn_date_created.DisplayFormat.FormatString = "d";
            this.gridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_created.FieldName = "ClientAppointmentRecord.date_created";
            this.gridColumn_date_created.GroupFormat.FormatString = "d";
            this.gridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
            this.gridColumn_date_created.Name = "gridColumn_date_created";
            this.gridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_created_by
            // 
            this.gridColumn_created_by.Caption = "Creator";
            this.gridColumn_created_by.CustomizationCaption = "Appointment Creator";
            this.gridColumn_created_by.FieldName = "ClientAppointmentRecord.created_by";
            this.gridColumn_created_by.Name = "gridColumn_created_by";
            this.gridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // gridColumn_formatted_name
            // 
            this.gridColumn_formatted_name.Caption = "Name";
            this.gridColumn_formatted_name.CustomizationCaption = "Client Name";
            this.gridColumn_formatted_name.FieldName = "NameRecord";
            this.gridColumn_formatted_name.Name = "gridColumn_formatted_name";
            this.gridColumn_formatted_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_formatted_name.Visible = true;
            this.gridColumn_formatted_name.VisibleIndex = 1;
            this.gridColumn_formatted_name.Width = 458;
            // 
            // counselors
            // 
            this.counselors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
            | System.Windows.Forms.AnchorStyles.Left)));
            this.counselors.CheckOnClick = true;
            this.counselors.Location = new System.Drawing.Point(10, 93);
            this.counselors.Name = "counselors";
            this.counselors.Size = new System.Drawing.Size(145, 166);
            this.counselors.StyleController = this.layoutControl1;
            this.counselors.TabIndex = 7;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.office_name);
            this.layoutControl1.Controls.Add(this.start_time);
            this.layoutControl1.Controls.Add(this.appt_type);
            this.layoutControl1.Controls.Add(this.Button_Cancel);
            this.layoutControl1.Controls.Add(this.Button_OK);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.counselors);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(468, 104, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(408, 302);
            this.layoutControl1.TabIndex = 11;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // office_name
            // 
            this.office_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.office_name.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.office_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.office_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.office_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.office_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.office_name.Location = new System.Drawing.Point(67, 10);
            this.office_name.Name = "office_name";
            this.office_name.Size = new System.Drawing.Size(331, 13);
            this.office_name.StyleController = this.layoutControl1;
            this.office_name.TabIndex = 1;
            this.office_name.Text = "LabelControl2";
            // 
            // start_time
            // 
            this.start_time.EditValue = new System.DateTime(2007, 4, 6, 0, 0, 0, 0);
            this.start_time.Location = new System.Drawing.Point(67, 33);
            this.start_time.Name = "start_time";
            this.start_time.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.start_time.Properties.DisplayFormat.FormatString = "hh:mm tt";
            this.start_time.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.start_time.Properties.EditFormat.FormatString = "hh:m tt";
            this.start_time.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.start_time.Size = new System.Drawing.Size(89, 20);
            this.start_time.StyleController = this.layoutControl1;
            this.start_time.TabIndex = 4;
            // 
            // appt_type
            // 
            this.appt_type.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.appt_type.Location = new System.Drawing.Point(67, 63);
            this.appt_type.Name = "appt_type";
            this.appt_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.appt_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Type", 10, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_name", "Description", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_duration", "Duration", 10, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far)});
            this.appt_type.Properties.DisplayMember = "appt_name";
            this.appt_type.Properties.NullText = "All";
            this.appt_type.Properties.ShowFooter = false;
            this.appt_type.Properties.SortColumnIndex = 1;
            this.appt_type.Properties.ValueMember = "Id";
            this.appt_type.Size = new System.Drawing.Size(331, 20);
            this.appt_type.StyleController = this.layoutControl1;
            this.appt_type.TabIndex = 6;
            this.appt_type.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(213, 269);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.StyleController = this.layoutControl1;
            this.Button_Cancel.TabIndex = 10;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(128, 269);
            this.Button_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.StyleController = this.layoutControl1;
            this.Button_OK.TabIndex = 9;
            this.Button_OK.Text = "&OK";
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.emptySpaceItem_left,
            this.emptySpaceItem_right,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.emptySpaceItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup1.Size = new System.Drawing.Size(408, 302);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.Button_Cancel;
            this.layoutControlItem1.CustomizationFormText = "Cancel Button";
            this.layoutControlItem1.Location = new System.Drawing.Point(203, 259);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem1.Size = new System.Drawing.Size(85, 33);
            this.layoutControlItem1.Text = "Cancel Button";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl1;
            this.layoutControlItem3.CustomizationFormText = "Clients";
            this.layoutControlItem3.Location = new System.Drawing.Point(155, 83);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem3.Size = new System.Drawing.Size(243, 176);
            this.layoutControlItem3.Text = "Clients";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.Button_OK;
            this.layoutControlItem2.CustomizationFormText = "OK Button";
            this.layoutControlItem2.Location = new System.Drawing.Point(118, 259);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem2.Size = new System.Drawing.Size(85, 33);
            this.layoutControlItem2.Text = "OK Button";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.office_name;
            this.layoutControlItem7.CustomizationFormText = "Office:";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem7.Size = new System.Drawing.Size(398, 23);
            this.layoutControlItem7.Text = "Office:";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.counselors;
            this.layoutControlItem4.CustomizationFormText = "Counselors";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 83);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem4.Size = new System.Drawing.Size(155, 176);
            this.layoutControlItem4.Text = "Counselors";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // emptySpaceItem_left
            // 
            this.emptySpaceItem_left.AllowHotTrack = false;
            this.emptySpaceItem_left.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem_left.Location = new System.Drawing.Point(0, 259);
            this.emptySpaceItem_left.Name = "emptySpaceItem_left";
            this.emptySpaceItem_left.Size = new System.Drawing.Size(118, 33);
            this.emptySpaceItem_left.Text = "emptySpaceItem_left";
            this.emptySpaceItem_left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_right
            // 
            this.emptySpaceItem_right.AllowHotTrack = false;
            this.emptySpaceItem_right.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem_right.Location = new System.Drawing.Point(288, 259);
            this.emptySpaceItem_right.Name = "emptySpaceItem_right";
            this.emptySpaceItem_right.Size = new System.Drawing.Size(110, 33);
            this.emptySpaceItem_right.Text = "emptySpaceItem_right";
            this.emptySpaceItem_right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.appt_type;
            this.layoutControlItem5.CustomizationFormText = "Appt Type:";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 53);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem5.Size = new System.Drawing.Size(398, 30);
            this.layoutControlItem5.Text = "Appt Type:";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(54, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.start_time;
            this.layoutControlItem6.CustomizationFormText = "Start Time:";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 23);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlItem6.Size = new System.Drawing.Size(156, 30);
            this.layoutControlItem6.Text = "Start Time:";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(54, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(156, 23);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(242, 30);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Form_Item
            // 
            this.ClientSize = new System.Drawing.Size(408, 302);
            this.Controls.Add(this.layoutControl1);
            this.Name = "Form_Item";
            this.Text = "Edit Appointment Information";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.counselors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.start_time.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appt_type.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            this.ResumeLayout(false);

        }
        private DevExpress.XtraEditors.CheckedListBoxControl counselors;
        private DevExpress.XtraEditors.LabelControl office_name;
        private DevExpress.XtraEditors.LookUpEdit appt_type;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.SimpleButton Button_OK;
        private DevExpress.XtraEditors.TimeEdit start_time;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_appt_time;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_appt_type;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_callback_ph;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_client;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_client_appointment;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_confirmation_status;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_counselor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_created_by;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_date_confirmed;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_date_created;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_date_updated;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_formatted_name;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_office;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_priority;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_result;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_start_time;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_status;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_left;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_right;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
    }
}
