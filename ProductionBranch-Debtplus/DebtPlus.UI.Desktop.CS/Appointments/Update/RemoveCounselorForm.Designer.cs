using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Appointments.Update
{
	partial class RemoveCounselorForm : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.DateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.TextEdit_reason = new DevExpress.XtraEditors.TextEdit();
            this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.CheckedListBoxControl1 = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).BeginInit();
            this.LayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_reason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckedListBoxControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(20, 20);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(345, 26);
            this.LabelControl1.StyleController = this.LayoutControl1;
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Select the date range and the counselor(s) that you wish to remove from the appoi" +
    "ntment system.";
            // 
            // LayoutControl1
            // 
            this.LayoutControl1.Controls.Add(this.DateNavigator1);
            this.LayoutControl1.Controls.Add(this.TextEdit_reason);
            this.LayoutControl1.Controls.Add(this.SimpleButton_Cancel);
            this.LayoutControl1.Controls.Add(this.SimpleButton_OK);
            this.LayoutControl1.Controls.Add(this.CheckedListBoxControl1);
            this.LayoutControl1.Controls.Add(this.LabelControl1);
            this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl1.Name = "LayoutControl1";
            this.LayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(434, 124, 250, 350);
            this.LayoutControl1.Root = this.LayoutControlGroup1;
            this.LayoutControl1.Size = new System.Drawing.Size(385, 300);
            this.LayoutControl1.TabIndex = 2;
            this.LayoutControl1.Text = "LayoutControl1";
            // 
            // dateNavigator1
            // 
            this.DateNavigator1.HotDate = null;
            this.DateNavigator1.Location = new System.Drawing.Point(12, 58);
            this.DateNavigator1.Name = "dateNavigator1";
            this.DateNavigator1.ShowTodayButton = true;
            this.DateNavigator1.ShowWeekNumbers = false;
            this.DateNavigator1.Size = new System.Drawing.Size(186, 177);
            this.DateNavigator1.TabIndex = 7;
            // 
            // TextEdit_reason
            // 
            this.TextEdit_reason.Location = new System.Drawing.Point(55, 239);
            this.TextEdit_reason.Name = "TextEdit_reason";
            this.TextEdit_reason.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextEdit_reason.Properties.MaxLength = 50;
            this.TextEdit_reason.Size = new System.Drawing.Size(318, 20);
            this.TextEdit_reason.StyleController = this.LayoutControl1;
            this.TextEdit_reason.TabIndex = 6;
            // 
            // SimpleButton_Cancel
            // 
            this.SimpleButton_Cancel.CausesValidation = false;
            this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(213, 263);
            this.SimpleButton_Cancel.MaximumSize = new System.Drawing.Size(75, 25);
            this.SimpleButton_Cancel.MinimumSize = new System.Drawing.Size(75, 25);
            this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
            this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 25);
            this.SimpleButton_Cancel.StyleController = this.LayoutControl1;
            this.SimpleButton_Cancel.TabIndex = 5;
            this.SimpleButton_Cancel.Text = "&Cancel";
            // 
            // SimpleButton_OK
            // 
            this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton_OK.Enabled = false;
            this.SimpleButton_OK.Location = new System.Drawing.Point(114, 263);
            this.SimpleButton_OK.MaximumSize = new System.Drawing.Size(75, 25);
            this.SimpleButton_OK.MinimumSize = new System.Drawing.Size(75, 25);
            this.SimpleButton_OK.Name = "SimpleButton_OK";
            this.SimpleButton_OK.Size = new System.Drawing.Size(75, 25);
            this.SimpleButton_OK.StyleController = this.LayoutControl1;
            this.SimpleButton_OK.TabIndex = 4;
            this.SimpleButton_OK.Text = "&OK";
            // 
            // CheckedListBoxControl1
            // 
            this.CheckedListBoxControl1.Location = new System.Drawing.Point(202, 76);
            this.CheckedListBoxControl1.Name = "CheckedListBoxControl1";
            this.CheckedListBoxControl1.Size = new System.Drawing.Size(171, 159);
            this.CheckedListBoxControl1.StyleController = this.LayoutControl1;
            this.CheckedListBoxControl1.TabIndex = 8;
            // 
            // LayoutControlGroup1
            // 
            this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
            this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup1.GroupBordersVisible = false;
            this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem1,
            this.LayoutControlItem2,
            this.LayoutControlItem3,
            this.LayoutControlItem4,
            this.LayoutControlItem5,
            this.LayoutControlItem6,
            this.EmptySpaceItem1,
            this.EmptySpaceItem2});
            this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup1.Name = "LayoutControlGroup1";
            this.LayoutControlGroup1.Size = new System.Drawing.Size(385, 300);
            this.LayoutControlGroup1.Text = "LayoutControlGroup1";
            this.LayoutControlGroup1.TextVisible = false;
            // 
            // LayoutControlItem1
            // 
            this.LayoutControlItem1.Control = this.LabelControl1;
            this.LayoutControlItem1.CustomizationFormText = "Selection Message";
            this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem1.Name = "LayoutControlItem1";
            this.LayoutControlItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 10, 10);
            this.LayoutControlItem1.Size = new System.Drawing.Size(365, 46);
            this.LayoutControlItem1.Text = "Selection Message";
            this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem1.TextToControlDistance = 0;
            this.LayoutControlItem1.TextVisible = false;
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.SimpleButton_OK;
            this.LayoutControlItem2.CustomizationFormText = "OK Button";
            this.LayoutControlItem2.Location = new System.Drawing.Point(92, 251);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(99, 29);
            this.LayoutControlItem2.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 0);
            this.LayoutControlItem2.Text = "OK Button";
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextToControlDistance = 0;
            this.LayoutControlItem2.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.SimpleButton_Cancel;
            this.LayoutControlItem3.CustomizationFormText = "CANCEL button";
            this.LayoutControlItem3.Location = new System.Drawing.Point(191, 251);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(99, 29);
            this.LayoutControlItem3.Spacing = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 0);
            this.LayoutControlItem3.Text = "CANCEL button";
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextToControlDistance = 0;
            this.LayoutControlItem3.TextVisible = false;
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.TextEdit_reason;
            this.LayoutControlItem4.CustomizationFormText = "Reason:";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 227);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(365, 24);
            this.LayoutControlItem4.Text = "Reason:";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(40, 13);
            // 
            // LayoutControlItem5
            // 
            this.LayoutControlItem5.Control = this.DateNavigator1;
            this.LayoutControlItem5.CustomizationFormText = "Date Range";
            this.LayoutControlItem5.Location = new System.Drawing.Point(0, 46);
            this.LayoutControlItem5.MaxSize = new System.Drawing.Size(190, 0);
            this.LayoutControlItem5.MinSize = new System.Drawing.Size(190, 24);
            this.LayoutControlItem5.Name = "LayoutControlItem5";
            this.LayoutControlItem5.Size = new System.Drawing.Size(190, 181);
            this.LayoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutControlItem5.Text = "Date Range";
            this.LayoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem5.TextToControlDistance = 0;
            this.LayoutControlItem5.TextVisible = false;
            // 
            // LayoutControlItem6
            // 
            this.LayoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.LayoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlItem6.Control = this.CheckedListBoxControl1;
            this.LayoutControlItem6.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.LayoutControlItem6.CustomizationFormText = "Counselor List";
            this.LayoutControlItem6.Location = new System.Drawing.Point(190, 46);
            this.LayoutControlItem6.Name = "LayoutControlItem6";
            this.LayoutControlItem6.Size = new System.Drawing.Size(175, 181);
            this.LayoutControlItem6.Text = "Counselor List";
            this.LayoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.LayoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top;
            this.LayoutControlItem6.TextSize = new System.Drawing.Size(79, 13);
            this.LayoutControlItem6.TextToControlDistance = 5;
            // 
            // EmptySpaceItem1
            // 
            this.EmptySpaceItem1.AllowHotTrack = false;
            this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
            this.EmptySpaceItem1.Location = new System.Drawing.Point(290, 251);
            this.EmptySpaceItem1.Name = "EmptySpaceItem1";
            this.EmptySpaceItem1.Size = new System.Drawing.Size(75, 29);
            this.EmptySpaceItem1.Text = "EmptySpaceItem1";
            this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // EmptySpaceItem2
            // 
            this.EmptySpaceItem2.AllowHotTrack = false;
            this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
            this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 251);
            this.EmptySpaceItem2.Name = "EmptySpaceItem2";
            this.EmptySpaceItem2.Size = new System.Drawing.Size(92, 29);
            this.EmptySpaceItem2.Text = "EmptySpaceItem2";
            this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // Form_RemoveCounselor
            // 
            this.AcceptButton = this.SimpleButton_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.SimpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(385, 300);
            this.Controls.Add(this.LayoutControl1);
            this.Name = "Form_RemoveCounselor";
            this.Text = "Remove Counselor(s) from Appointments on Date(s)";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl1)).EndInit();
            this.LayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextEdit_reason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckedListBoxControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem2)).EndInit();
            this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_reason;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		protected internal DevExpress.XtraScheduler.DateNavigator DateNavigator1;
		protected internal DevExpress.XtraEditors.CheckedListBoxControl CheckedListBoxControl1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
	}
}
