#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using DebtPlus.Utils;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Drawing;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.Appointments.Update
{
    internal partial class EditForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private const string FORM_NAME = "Appointments.Update.EditForm";
        private appt_time record = null;
        private BusinessContext bc = null;

        /// <summary>
        /// Local class to hold the list of client appointment entries for this time period/office.
        /// </summary>
        private class displayClient
        {
            public displayClient() { }
            public displayClient(client_appointment ClientAppointmentRecord, people PeopleRecord, Name NameRecord)
                : this()
            {
                this.ClientAppointmentRecord = ClientAppointmentRecord;
                this.NameRecord = NameRecord;
                this.PeopleRecord = PeopleRecord;
            }

            public client_appointment ClientAppointmentRecord { get; set; }
            public people PeopleRecord { get; set; }
            public Name NameRecord { get; set; }
        }

        // Collection of client appointments that are booked against this time slot.
        System.Collections.Generic.List<displayClient> colClients = null;

        public EditForm() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create our instance of the class
        /// </summary>
        public EditForm(BusinessContext bc, appt_time record) : this()
        {
            this.bc     = bc;
            this.record = record;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                       += Form_Item_Load;
            counselors.DrawItem        += counselors_DrawItem;
            counselors.ItemChecking    += counselors_ItemChecking;
            Button_OK.Click            += Button_OK_Click;
            Resize                     += Form_Item_Resize;
        }

        private void UnRegisterHandlers()
        {
            Load                       -= Form_Item_Load;
            counselors.DrawItem        -= counselors_DrawItem;
            counselors.ItemChecking    -= counselors_ItemChecking;
            Button_OK.Click            -= Button_OK_Click;
            Resize                     -= Form_Item_Resize;
        }

        /// <summary>
        /// Handle the form being sized to make the buttons in the bottom "centered"
        /// </summary>
        private void Form_Item_Resize(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                emptySpaceItem_left.Width = (emptySpaceItem_left.Width + emptySpaceItem_right.Width) >> 1;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        private void Form_Item_Load(System.Object sender, System.EventArgs e)
        {
            UnRegisterHandlers();

            // Restore the window size and location
            LoadPlacement(FORM_NAME);

            try
            {
                // Generate a list of the appointment types but add "ALL" to the list
                var colApptTypes = new System.Collections.Generic.List<appt_type>();
                colApptTypes.AddRange(DebtPlus.LINQ.Cache.appt_type.getList().ToArray());
                colApptTypes.Add(new DebtPlus.LINQ.appt_type() { appt_duration = 60, appt_name = "ALL", Default = false, Id = -1 });

                // Bind the type of the appointment to the proper item
                appt_type.Properties.DataSource = colApptTypes;
                appt_type.EditValue = record.appt_type.GetValueOrDefault(-1);
                start_time.EditValue = record.start_time;

                // Turn off sorting so that the list may load quicker
                counselors.Items.Clear();
                counselors.SortOrder = System.Windows.Forms.SortOrder.None;

                // Populate the list of possible counselor records
                foreach (var co in DebtPlus.LINQ.Cache.counselor.getList().Where(s => s.ActiveFlag))
                {
                    var listItem = new MySortedCheckedListboxControlItem(co, co.Name == null ? string.Empty : co.Name.ToString(), CheckState.Unchecked, co.ActiveFlag);
                    counselors.Items.Add(listItem);
                }

                // Check the list of counselors who are on this time slot.
                foreach (var apptCounselorRecord in record.appt_counselors)
                {
                    Int32 counselorID = apptCounselorRecord.counselor;
                    var q = counselors.Items.OfType<MySortedCheckedListboxControlItem>().Where(s => (s.Value as DebtPlus.LINQ.counselor).Id == counselorID).FirstOrDefault();
                    if (q != null)
                    {
                        // Save the reason and the status for the CheckState on the counselor record.
                        q.Reason = apptCounselorRecord.inactive_reason;
                        if (apptCounselorRecord.inactive == 0)
                        {
                            q.CheckState = CheckState.Checked;
                        }
                    }
                }

                // Resort the list in the proper order
                counselors.SortOrder = System.Windows.Forms.SortOrder.Ascending;

                // Display the name of the office
                var qOffice = DebtPlus.LINQ.Cache.office.getList().Find(s => s.Id == record.office);
                office_name.Text = (qOffice == null) ? string.Empty : qOffice.name;

                // Supply the appointment information for the time interval
                start_time.EditValue = record.start_time;

                // Finally, load the list of clients who have this time slot booked
                colClients = bc.client_appointments.Join(bc.peoples, ca => ca.client, p => p.Client, (ca, p) => new { ca = ca, p = p }).Join(bc.Names, x => x.p.NameID, n => n.Id, (x, n) => new { ClientAppointmentRecord = x.ca, PeopleRecord = x.p, NameRecord = n }).Where(y => y.ClientAppointmentRecord.status == 'P' && y.ClientAppointmentRecord.appt_time == record.Id && y.PeopleRecord.Relation == 1).Select(x => new displayClient(x.ClientAppointmentRecord, x.PeopleRecord, x.NameRecord)).ToList();
                gridControl1.DataSource = colClients;

                // Go through and count the number of references to a counselor in the client appointments table entries
                // This will make the items "pink" in the list to indicate that there are pending appointments for this counselor.
                foreach (var ca in colClients)
                {
                    var qSelected = counselors.Items.OfType<MySortedCheckedListboxControlItem>().Where(s => ca.ClientAppointmentRecord.counselor.HasValue && (s.Value as counselor).Id == ca.ClientAppointmentRecord.counselor.Value).FirstOrDefault();
                    if (qSelected != null)
                    {
                        qSelected.ApptCount += 1;
                    }
                }

                // Enable or disable the OK button.
                Button_OK.Enabled = !HasErrors();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
                gridView1.RefreshData();
            }
        }

        /// <summary>
        /// Handle the case where a counselor is being marked INACTIVE
        /// </summary>
        private void counselors_ItemChecking(object sender, DevExpress.XtraEditors.Controls.ItemCheckingEventArgs e)
        {
            // Find the corresponding counselor record that we want to cancel.
            var checkedItem = counselors.Items[e.Index] as MySortedCheckedListboxControlItem;

            // If the record could not be located then do not allow the change to be made
            if (checkedItem == null)
            {
                e.Cancel = true;
                return;
            }

            // If the record is going from "active" to "inactive" then ask why
            if (e.OldValue == CheckState.Checked && e.NewValue != CheckState.Checked)
            {
                // Determine if there are any pending appointments to be cancelled for this counselor
                var counselorID = (checkedItem.Value as counselor).Id;
                var qAppt = colClients.Where(s => s.ClientAppointmentRecord.counselor.HasValue && s.ClientAppointmentRecord.counselor == counselorID).FirstOrDefault();
                if (qAppt != null)
                {
                    if (DebtPlus.Data.Forms.MessageBox.Show("If you cancel this counselor, you will also cancel the associated client appointment.\r\n\r\nAre you sure that you wnat to do this?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
                    {
                        e.Cancel = true;
                        return;
                    }
                }

                // Ask why the counselor is being marked inactive
                string Reason = checkedItem.Reason ?? string.Empty;
                if (DebtPlus.Data.Forms.InputBox.Show("Why is the counselor being removed from this schedule?", ref Reason, "Inactive Status Reason", Reason, 50) == System.Windows.Forms.DialogResult.OK)
                {
                    checkedItem.Reason = Reason;
                }
            }
        }

        private void counselors_DrawItem(object sender, DevExpress.XtraEditors.ListBoxDrawItemEventArgs e)
        {
            // Change the background color on the item when needed
            if (e.Index >= 0 && e.Index < counselors.Items.Count)
            {
                if (((MySortedCheckedListboxControlItem)counselors.Items[e.Index]).ApptCount > 0)
                {
                    e.Appearance.BackColor = Color.PeachPuff;
                }
            }
        }

        /// <summary>
        /// Determine if the input form is valid for the record
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            // The appointment type is required
            if (!DebtPlus.Utils.Nulls.v_Int32(appt_type.EditValue).HasValue)
            {
                return true;
            }

            // Everything is valid for the record at this point.
            return false;
        }

        /// <summary>
        /// Handle the CLICK event on the OK button to process the items.
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            // Mark the dialog as completed normally.
            DialogResult = System.Windows.Forms.DialogResult.OK;

            // Save the parameters for the current record when the OK button is pressed.
            Int32 newType = DebtPlus.Utils.Nulls.v_Int32(appt_type.EditValue).GetValueOrDefault(-1);
            record.appt_type = (newType >= 0) ? new System.Nullable<Int32>(newType) : new System.Nullable<Int32>();

            // There must be a starting time for the appointment.
            record.start_time = DebtPlus.Utils.Nulls.v_DateTime(start_time.EditValue).GetValueOrDefault();

            // Process the counselor list. If we find counselors to be disconnected, then we need to cancel their appointments as well.
            foreach (var chkItem in counselors.Items.OfType<MySortedCheckedListboxControlItem>())
            {
                var counselorRecord = chkItem.Value as counselor;
                var qApptCounselor = record.appt_counselors.Where(s => s.counselor == counselorRecord.Id).FirstOrDefault();

                if (chkItem.CheckState != CheckState.Checked)
                {
                    // Ignore items that are inactive. The reason stands.
                    if (qApptCounselor == null || qApptCounselor.inactive != 0)
                    {
                        continue;
                    }

                    // We need to mark the counselor as being inactive at this point. We don't delete the record
                    // as that would cause the history to be lost. Just say that it is no longer active.
                    qApptCounselor.inactive        = 1;
                    qApptCounselor.inactive_reason = chkItem.Reason;

                    // Determine if there is a pending appointment for this counselor at this time period.
                    // Look first in the local cache. It is faster than searching the database.
                    var qAppt = colClients.Where(s => s.ClientAppointmentRecord.counselor == counselorRecord.Id).FirstOrDefault();
                    if (qAppt != null)
                    {
                        // If we can find it in the cache, use the ID to find it in the database. If found, cancel the appointment.
                        var qAppointment = bc.client_appointments.Where(s => s.Id == qAppt.ClientAppointmentRecord.Id).FirstOrDefault();
                        if (qAppointment != null)
                        {
                            qAppointment.status       = 'C';
                            qAppointment.appt_time    = null;
                            qAppointment.date_updated = DebtPlus.LINQ.BusinessContext.getdate();
                        }
                    }
                    continue;
                }

                // Process a new counselor (one that was not entered earlier)
                // If it is found and marked active then just continue.
                if (qApptCounselor == null)
                {
                    qApptCounselor           = DebtPlus.LINQ.Factory.Manufacture_appt_counselor();
                    qApptCounselor.counselor = counselorRecord.Id;
                    record.appt_counselors.Add(qApptCounselor);
                }

                // The counselor is no longer "inactive".
                qApptCounselor.inactive = 0;
                qApptCounselor.inactive_reason = null;
            }
        }

        /// <summary>
        /// Class to hold the checked items in the counselor list
        /// </summary>
        private class MySortedCheckedListboxControlItem : DebtPlus.Data.Controls.SortedCheckedListboxControlItem
        {
            public MySortedCheckedListboxControlItem() : base()
            {
            }

            public MySortedCheckedListboxControlItem(object value) : base(value)
            {
            }

            public MySortedCheckedListboxControlItem(object value, bool isChecked) : base(value, isChecked)
            {
            }

            public MySortedCheckedListboxControlItem(object value, string description) : base(value, description)
            {
            }

            public MySortedCheckedListboxControlItem(object value, CheckState checkState) : base(value, checkState)
            {
            }

            public MySortedCheckedListboxControlItem(object value, string description, CheckState checkState) : base(value, description, checkState)
            {
            }

            public MySortedCheckedListboxControlItem(object value, CheckState checkState, bool enabled) : base(value, checkState, enabled)
            {
            }

            public MySortedCheckedListboxControlItem(object value, string description, CheckState checkState, bool enabled) : base(value, description, checkState, enabled)
            {
            }

            public Int32 ApptCount { get; set; }
            public string Reason { get; set; }
        }
    }
}