#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Linq;
using DebtPlus.LINQ;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Appointments.Update
{
    internal partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private class appointmentCount
        {
            public Int32 Id { get; set; }
            public Int32 Count { get; set; }
        }

        private readonly string FORM_NAME = "Appointments.Update.MainForm";
        private System.Collections.Generic.List<appt_time> colRecords = null;
        private DebtPlus.Utils.ArgParserBase ap;
        private BusinessContext bc = null;
        private System.Collections.Generic.List<appointmentCount> apptCountCache = new System.Collections.Generic.List<appointmentCount>();

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        public MainForm() : base()
        {
            InitializeComponent();

            dateNavigator1.Selection.Clear();
            dateNavigator1.DateTime = System.DateTime.Now.Date;
        }

        /// <summary>
        /// Initialize the new form class
        /// </summary>
        public MainForm(DebtPlus.Utils.ArgParserBase ap) : this()
        {
            this.ap = ap;
            this.bc = new BusinessContext();

            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            barButtonCounselors.ItemClick       += barButtonCounselors_ItemClick;
            contextMenu1_Create.ItemClick       += contextMenu1_Create_ItemClick;
            contextMenu1_Delete.ItemClick       += contextMenu1_Delete_ItemClick;
            contextMenu1_Edit.ItemClick         += contextMenu1_Edit_ItemClick;
            gridView1.CustomColumnDisplayText   += gridView1_CustomColumnDisplayText;
            gridView1.CustomUnboundColumnData   += gridView1_CustomUnboundColumnData;
            gridView1.DoubleClick               += gridView1_DoubleClick;
            gridView1.MouseDown                 += gridView1_MouseDown;
            gridView1.Layout                    += gridView1_Layout;
            Load                                += Form_Load;
            lookUpEdit_office.EditValueChanged  += control_EditValueChanged;
            lookUpEdit_office.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            dateNavigator1.EditDateModified     += control_EditValueChanged;
            dateNavigator1.Validating           += dateNavigator1_Validating;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            barButtonCounselors.ItemClick       -= barButtonCounselors_ItemClick;
            contextMenu1_Create.ItemClick       -= contextMenu1_Create_ItemClick;
            contextMenu1_Delete.ItemClick       -= contextMenu1_Delete_ItemClick;
            contextMenu1_Edit.ItemClick         -= contextMenu1_Edit_ItemClick;
            gridView1.CustomColumnDisplayText   -= gridView1_CustomColumnDisplayText;
            gridView1.CustomUnboundColumnData   -= gridView1_CustomUnboundColumnData;
            gridView1.DoubleClick               -= gridView1_DoubleClick;
            gridView1.MouseDown                 -= gridView1_MouseDown;
            gridView1.Layout                    -= gridView1_Layout;
            Load                                -= Form_Load;
            lookUpEdit_office.EditValueChanged  -= control_EditValueChanged;
            lookUpEdit_office.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            dateNavigator1.EditDateModified     -= control_EditValueChanged;
            dateNavigator1.Validating           -= dateNavigator1_Validating;
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        private string XMLBasePath()
        {
            return System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "DebtPlus", "Client.Appointment.Update");
        }

        /// <summary>
        /// Handle a change in the layout of the grid.
        /// </summary>
        private void gridView1_Layout(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                var PathName = XMLBasePath();
                if (!string.IsNullOrEmpty(PathName))
                {
                    if (!System.IO.Directory.Exists(PathName))
                    {
                        System.IO.Directory.CreateDirectory(PathName);
                    }

                    var fileName = System.IO.Path.Combine(PathName, gridView1.Name + ".xml");
                    gridView1.SaveLayoutToXml(fileName);
                }
            }

            // Ignore errors in the save operation. Catch all others.
            catch (System.IO.IOException) { }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Ensure that the user does not choose a date before today.
        /// </summary>
        private void dateNavigator1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var today = DateTime.Now.Date;
            if (dateNavigator1.Selection.OfType<System.DateTime>().Any(s => s < today))
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Restore the form placement
                LoadPlacement(FORM_NAME);

                // Set the source for the list of offices and the default value for the "new" office
                lookUpEdit_office.Properties.DataSource = DebtPlus.LINQ.Cache.office.getList().Where(s => s.ActiveFlag).ToList();
                lookUpEdit_office.EditValue = DebtPlus.LINQ.Cache.office.getDefault();

                // Load the initial selection if there is one
                readGrid();

                // Restore the layout if needed
                var PathName = XMLBasePath();
                if (!string.IsNullOrEmpty(PathName))
                {
                    var fileName = System.IO.Path.Combine(PathName, gridView1.Name + ".xml");
                    try
                    {
                        if (System.IO.File.Exists(fileName))
                        {
                            gridView1.RestoreLayoutFromXml(fileName);
                        }
                    }
                    catch { }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }

            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the mouse down event on the grid
        /// </summary>
        private void gridView1_MouseDown(object sender, MouseEventArgs e)
        {
            // Look only at the right button
            if (e.Button != System.Windows.Forms.MouseButtons.Right)
            {
                return;
            }

            // The mouse must be in a row
            var hi = gridView1.CalcHitInfo(new System.Drawing.Point(e.X, e.Y));
            if (!hi.IsValid)
            {
                return;
            }

            // Enable or disable the menu items based upon where the mouse was clicked.
            var obj = gridView1.GetRow(hi.RowHandle);
            if (hi.InRow && obj != null)
            {
                gridView1.FocusedRowHandle = hi.RowHandle;
                contextMenu1_Edit.Enabled = true;
                contextMenu1_Delete.Enabled = true;
            }
            else
            {
                contextMenu1_Edit.Enabled = false;
                contextMenu1_Delete.Enabled = false;
            }

            // The selected date must be at least today
            contextMenu1_Create.Enabled = (dateNavigator1.DateTime.Date >= System.DateTime.Now.Date);

            // Trip the popup event
            popupMenu1.ShowPopup(gridControl1.PointToScreen(e.Location));
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void gridView1_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gridView1.CalcHitInfo((gridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            if (!hi.IsValid || !hi.InRow)
            {
                return;
            }

            // Do the update operation
            updateRecord(gridView1.GetRow(hi.RowHandle));
        }

        /// <summary>
        /// Edit -> Create menu item clicked
        /// </summary>
        private void contextMenu1_Create_ItemClick(object sender, EventArgs e)
        {
            createRecord();
        }

        /// <summary>
        /// Edit -> Edit menu item clicked
        /// </summary>
        private void contextMenu1_Edit_ItemClick(object sender, EventArgs e)
        {
            updateRecord(gridView1.GetFocusedRow());
        }

        /// <summary>
        /// Edit -> Delete menu item clicked
        /// </summary>
        private void contextMenu1_Delete_ItemClick(object sender, EventArgs e)
        {
            deleteRecord(gridView1.GetFocusedRow());
        }

        /// <summary>
        /// Create the new record for the collection
        /// </summary>
        private void createRecord()
        {
            System.DateTime fromDate = dateNavigator1.DateTime.Date;

            // Do not allow the system to create appointment slots before today.
            if (fromDate < System.DateTime.Now.Date)
            {
                return;
            }

            var record = DebtPlus.LINQ.Factory.Manufacture_appt_time();

            // Take the office from the input control
            Int32 officeID = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_office.EditValue).GetValueOrDefault();
            record.office = officeID;

            // Find the last time for the current date. Add one hour to that for the new starting time.
            System.DateTime toDate = fromDate.AddDays(1);
            var q = colRecords.Where(s => s.start_time >= fromDate && s.start_time < toDate && s.office == officeID).OrderByDescending(s => s.start_time).FirstOrDefault();
            record.start_time = (q != null) ? q.start_time.AddHours(1) : fromDate.AddHours(8);

            // Do the edit operation on the appointment time slot
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Add the new record to the collection
            colRecords.Add(record);
            bc.appt_times.InsertOnSubmit(record);

            try
            {
                // Submit the changes to the database
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Edit the existing record in the collection
        /// </summary>
        private void updateRecord(object obj)
        {
            var record = obj as appt_time;
            if (record == null)
            {
                return;
            }

            // Do the edit operation on the appointment time slot
            using (var frm = new EditForm(bc, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Submit the changes to the database
                bc.SubmitChanges();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Delete the record from the collection
        /// </summary>
        private void deleteRecord(object obj)
        {
            var record = obj as appt_time;
            if (record == null)
            {
                return;
            }

            // If there are items that are active then ask why we are removing the counselors
            string reason = string.Empty;
            if (record.appt_counselors.Any(s => s.inactive == 0))
            {
                if (DebtPlus.Data.Forms.InputBox.Show("Why are you removing the counselor(s)?", ref reason, "Reason for Removal", string.Empty) != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Ensure that the user really wants to remove this time slot.
            System.Collections.Generic.List<client_appointment> colAppointments = bc.client_appointments.Where(s => s.status == 'P' && s.appt_time == record.Id).ToList();
            if (colAppointments.Count() > 0)
            {
                if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("Warning. There are {0:f0} client appointments for this time record.\r\n\r\nIf you delete the time slot, you will cancel these appointments.\r\nAre you really sure that you want to do that?", colAppointments.Count()), "Are you sure", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }

                // Cancel the appointments
                foreach (var appt in colAppointments)
                {
                    appt.status       = 'C';
                    appt.appt_time    = null;
                    appt.date_updated = DebtPlus.LINQ.BusinessContext.getdate();
                }
            }

            // Mark the counselors as being inactive
            foreach (var co in record.appt_counselors.Where(s => s.inactive == 0))
            {
                co.inactive        = 1;
                co.inactive_reason = reason;
            }

            try
            {
                // Submit the changes to the database
                bc.SubmitChanges();
                gridView1.RefreshData();
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// When the date or office changes then we need to reload the grid control.
        /// </summary>
        private void control_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                readGrid();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Refresh the grid with the changes
        /// </summary>
        private void readGrid()
        {
            // If there is an office then we can look a bit more deeply for the matching records
            System.Nullable<Int32> officeId = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_office.EditValue);
            if (officeId.HasValue)
            {
                System.DateTime FromDate = dateNavigator1.DateTime.Date;
                System.DateTime ToDate   = FromDate.AddDays(1);
                colRecords     = bc.appt_times.Where(s => s.office == officeId.Value && s.start_time >= FromDate && s.start_time < ToDate).ToList();

                apptCountCache = (from demoClass in bc.client_appointments
                                  join apt in bc.appt_times on demoClass.appt_time equals apt.Id
                                  where (demoClass.status == 'P') && (apt.office == officeId.Value) && (apt.start_time >= FromDate && apt.start_time < ToDate)
                                  group demoClass by demoClass.appt_time into groupedDemoClass
                                  let itemCount = groupedDemoClass.Count()
                                  select new appointmentCount() {Id = (Int32) groupedDemoClass.Key, Count = itemCount}).ToList();
            }
            else
            {
                colRecords     = new System.Collections.Generic.List<appt_time>();
                apptCountCache = new System.Collections.Generic.List<appointmentCount>();
            }

            gridControl1.DataSource = colRecords;
            gridView1.RefreshData();
        }

        /// <summary>
        /// Handle the situation where we are to format a specific column in the record.
        /// </summary>
        private void gridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the current record that we are showing.
            var record = gridView1.GetRow(e.RowHandle) as appt_time;
            if (record == null)
            {
                e.DisplayText = string.Empty;
                return;
            }

            // If we want the appt_type column then translate the type ID to a name
            if (e.Column == gridColumn_type)
            {
                // No type means "all types".
                if (record.appt_type == null)
                {
                    e.DisplayText = "ALL";
                    return;
                }

                // Look at the type field to determine if it can be found
                var q = DebtPlus.LINQ.Cache.appt_type.getList().Find(s => s.Id == record.appt_type);
                if (q != null)
                {
                    e.DisplayText = q.appt_name;
                    return;
                }

                // If the entry is not defined then make it blank
                e.DisplayText = string.Empty;
                return;
            }

            // Look at the counselor field to determine the list of possible counselors
            if (e.Column == gridColumn_counselors)
            {
                var sb = new System.Text.StringBuilder();
                var colCounselors = DebtPlus.LINQ.Cache.counselor.getList();
                foreach (var item in record.appt_counselors)
                {
                    if (item.counselor > 0 && item.inactive == 0)
                    {
                        var q = colCounselors.Find(s => s.Id == item.counselor);
                        if (q != null && q.Name != null)
                        {
                            sb.AppendFormat(",{0}", q.Name.ToString());
                        }
                    }
                }

                if (sb.Length > 0)
                {
                    sb.Remove(0, 1);
                }
                e.DisplayText = sb.ToString();
                return;
            }

            // Everything else is normally displayed from the record and does not need to be filtered here.
        }

        /// <summary>
        /// Handle the CLICK event to remove the counselor from the system.
        /// </summary>
        private void barButtonCounselors_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (var frm = new RemoveCounselorForm(bc))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            try
            {
                // Submit the changes to the database
                bc.SubmitChanges();

                // Reload the grid with the new values since the database has been changed.
                readGrid();
                gridView1.RefreshData();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Retrieve the unbound column for the count of counselors. We can't bind a method to the grid control.
        /// </summary>
        private void gridView1_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            // Find the record that corresponds to the unbound field. Ignore invalid records.
            var record = e.Row as DebtPlus.LINQ.appt_time;
            if (record == null)
            {
                return;
            }

            // If the request is to obtain data then look for the columns that we are interested in seeing.
            if (e.IsGetData)
            {
                // Handle the count for the number of counselors.
                if (e.Column == gridColumn_count)
                {
                    e.Value = record.appt_counselors == null ? 0 : record.appt_counselors.Count(s => s.inactive == 0);
                    return;
                }

                // Booked appointment count. It comes the from the side cache.
                if (e.Column == gridColumn_booked)
                {
                    var q = apptCountCache.Find(s => s.Id == record.Id);
                    e.Value = (q == null) ? 0 : q.Count;
                    return;
                }                    
                return;
            }

            // Ignore the IsSetData since we don't allow the columns to be updated.
        }
    }
}