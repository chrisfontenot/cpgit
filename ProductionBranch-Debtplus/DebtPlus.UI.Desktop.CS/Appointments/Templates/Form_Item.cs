#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Appointments.Templates
{
    internal partial class Form_Item : DebtPlus.UI.Common.Templates.ADO.Net.EditTemplateForm
    {
        // Magic value for "ALL" in the list.
        const Int32 Marker_ALL = -1;

        private BusinessContext bc = null;
        private appt_time_template record = null;
        private System.Collections.Generic.List<office> colOffices = null;
        private System.Collections.Generic.List<appt_type> colAppointmentTypes = null;

        public Form_Item() : base()
        {
            InitializeComponent();
        }

        public Form_Item(BusinessContext bc, System.Collections.Generic.List<office> colOffices, appt_time_template record) : this()
        {
            this.bc         = bc;
            this.record     = record;
            this.colOffices = colOffices;
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                        += Form_Item_Load;
            start_time.EditValueChanged += Form_Changed;
            appt_type.EditValueChanged  += Form_Changed;
            counselors.ItemCheck        += Form_Changed;
        }

        private void UnRegisterHandlers()
        {
            Load                        -= Form_Item_Load;
            start_time.EditValueChanged -= Form_Changed;
            appt_type.EditValueChanged  -= Form_Changed;
            counselors.ItemCheck        -= Form_Changed;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl office_name;
        internal DevExpress.XtraEditors.LabelControl day_of_week;
        internal DevExpress.XtraEditors.TimeEdit start_time;
        internal DevExpress.XtraEditors.LookUpEdit appt_type;
        internal DevExpress.XtraEditors.CheckedListBoxControl counselors;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.office_name = new DevExpress.XtraEditors.LabelControl();
            this.day_of_week = new DevExpress.XtraEditors.LabelControl();
            this.start_time = new DevExpress.XtraEditors.TimeEdit();
            this.appt_type = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.counselors = new DevExpress.XtraEditors.CheckedListBoxControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.start_time.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.appt_type.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.counselors).BeginInit();
            this.SuspendLayout();
            //
            //SimpleButton_OK
            //
            this.SimpleButton_OK.Location = new System.Drawing.Point(370, 43);
            this.SimpleButton_OK.TabIndex = 10;
            //
            //SimpleButton_Cancel
            //
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(370, 86);
            this.SimpleButton_Cancel.TabIndex = 11;
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(29, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "officeID";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 24);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(79, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Day of the week";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(8, 51);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(86, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Appointment Time";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(8, 75);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(88, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Appointment Type";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            //
            //office_name
            //
            this.office_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.office_name.Location = new System.Drawing.Point(112, 8);
            this.office_name.Name = "office_name";
            this.office_name.Size = new System.Drawing.Size(29, 13);
            this.office_name.TabIndex = 1;
            this.office_name.Text = "officeID";
            this.office_name.ToolTipController = this.ToolTipController1;
            //
            //day_of_week
            //
            this.day_of_week.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.day_of_week.Location = new System.Drawing.Point(112, 24);
            this.day_of_week.Name = "day_of_week";
            this.day_of_week.Size = new System.Drawing.Size(29, 13);
            this.day_of_week.TabIndex = 3;
            this.day_of_week.Text = "officeID";
            this.day_of_week.ToolTipController = this.ToolTipController1;
            //
            //start_time
            //
            this.start_time.EditValue = new System.DateTime(2007, 4, 4, 0, 0, 0, 0);
            this.start_time.Location = new System.Drawing.Point(112, 48);
            this.start_time.Name = "start_time";
            this.start_time.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.start_time.Size = new System.Drawing.Size(100, 20);
            this.start_time.TabIndex = 5;
            this.start_time.ToolTipController = this.ToolTipController1;
            //
            //appt_type
            //
            this.appt_type.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.appt_type.Location = new System.Drawing.Point(112, 72);
            this.appt_type.Size = new System.Drawing.Size(250, 20);
            this.appt_type.Name = "appt_type";
            this.appt_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.appt_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Type", 10, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_name", "Description", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("appt_duration", "Duration", 10, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far)
            });
            this.appt_type.Properties.NullText = "All";
            this.appt_type.TabIndex = 7;
            this.appt_type.ToolTipController = this.ToolTipController1;
            this.appt_type.Properties.SortColumnIndex = 1;
            this.appt_type.Properties.ValueMember = "Id";
            this.appt_type.Properties.DisplayMember = "appt_name";
            this.appt_type.Properties.ShowFooter = false;
            this.appt_type.Properties.PopupWidth = 370;
            //
            //LabelControl5
            //
            this.LabelControl5.Location = new System.Drawing.Point(8, 96);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(53, 13);
            this.LabelControl5.TabIndex = 8;
            this.LabelControl5.Text = "Counselors";
            this.LabelControl5.ToolTipController = this.ToolTipController1;
            //
            //counselors
            //
            this.counselors.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.counselors.CheckOnClick = true;
            this.counselors.Location = new System.Drawing.Point(112, 96);
            this.counselors.Name = "counselors";
            this.counselors.Size = new System.Drawing.Size(250, 116);
            this.counselors.TabIndex = 9;
            this.counselors.ToolTipController = this.ToolTipController1;
            //
            //Form_Item
            //
            this.AcceptButton = null;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = null;
            this.ClientSize = new System.Drawing.Size(466, 224);
            this.Controls.Add(this.counselors);
            this.Controls.Add(this.appt_type);
            this.Controls.Add(this.start_time);
            this.Controls.Add(this.day_of_week);
            this.Controls.Add(this.office_name);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
            this.Name = "Form_Item";
            this.Text = "Appointment Template Item";
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.LabelControl2, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.LabelControl5, 0);
            this.Controls.SetChildIndex(this.office_name, 0);
            this.Controls.SetChildIndex(this.day_of_week, 0);
            this.Controls.SetChildIndex(this.start_time, 0);
            this.Controls.SetChildIndex(this.appt_type, 0);
            this.Controls.SetChildIndex(this.counselors, 0);
            this.Controls.SetChildIndex(this.SimpleButton_OK, 0);
            this.Controls.SetChildIndex(this.SimpleButton_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.start_time.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.appt_type.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.counselors).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Process the form load operation
        /// </summary>
        private void Form_Item_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                var qOffice = colOffices.Find(s => s.Id == record.office);
                office_name.Text = (qOffice != null) ? qOffice.name : string.Empty;

                // Display the day of the week
                var qDow = DebtPlus.LINQ.InMemory.DaysOfWeeks.getList().Find(s => s.Id == record.dow);
                day_of_week.Text = (qDow != null) ? qDow.description : string.Empty;

                // Set the starting time into the time control
                start_time.EditValue = System.DateTime.MinValue.AddMinutes(Convert.ToDouble(record.start_time));

                // Get a list of the appointment types. We need to define a fake item that is "ALL".
                // Therefore, we can't use the original list. We need to make a copy first.
                colAppointmentTypes = new System.Collections.Generic.List<DebtPlus.LINQ.appt_type>();
                colAppointmentTypes.AddRange(DebtPlus.LINQ.Cache.appt_type.getList());

                DebtPlus.LINQ.appt_type newType = DebtPlus.LINQ.Factory.Manufacture_appt_type();
                newType.Id                      = Marker_ALL;
                newType.appt_name               = "ALL";
                newType.appt_duration           = 60;
                colAppointmentTypes.Add(newType);

                // Set the information for the appointment types
                appt_type.Properties.DataSource = colAppointmentTypes;
                appt_type.EditValue = record.appt_type.GetValueOrDefault(-1);

                // Build the list of counselor items
                var aryList = new System.Collections.ArrayList();
                foreach (var counselorRecord in DebtPlus.LINQ.Cache.counselor.CounselorsList())
                {
                    aryList.Add(new DebtPlus.Data.Controls.SortedCheckedListboxControlItem(counselorRecord, (counselorRecord.Name != null) ? counselorRecord.Name.ToString() : string.Empty, CheckState.Unchecked));
                }

                // Add the array to the checkbox. It needs to be set to sorted before the additions, but
                // trying to add the items one at a time with the list sorted causes the list to be resorted
                // with every addition. So, add the items to a temporary (unsorted) collection, turn on the sort
                // status for the list and then add all of the items. This sorts the list only once.
                counselors.Items.Clear();
                counselors.SortOrder = SortOrder.Ascending;
                counselors.Items.AddRange(aryList.ToArray());

                // Check the items that are currently defined for the appointment record
                foreach (var selected in record.appt_counselor_templates)
                {
                    var qSelected = counselors.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>().Where(s => (s.Value as DebtPlus.LINQ.counselor).Id == selected.counselor).FirstOrDefault();
                    if (qSelected != null)
                    {
                        qSelected.CheckState = CheckState.Checked;
                    }
                }

                // We need to change something to enable the OK button.
                SimpleButton_OK.Enabled = false;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Handle the change in the start time.
        /// </summary>
        private void Form_Changed(object sender, EventArgs e)
        {
            SimpleButton_OK.Enabled = true;
        }

        /// <summary>
        /// Write the counselor information and create the template item
        /// </summary>
        protected override void SimpleButton_OK_Click(object sender, EventArgs e)
        {
            base.SimpleButton_OK_Click(sender, e);

            // Update the number of minutes since midnight for the starting time. We want just the time, not the date component.
            var ts = start_time.Time.TimeOfDay;
            record.start_time = Convert.ToInt32(ts.TotalMinutes);

            // Find the appointment type. Translate the "ALL" to be "NULL".
            var qType = appt_type.GetSelectedDataRow() as DebtPlus.LINQ.appt_type;
            if (qType == null || qType.Id == Marker_ALL)
            {
                record.appt_type = null;
            }
            else
            {
                record.appt_type = qType.Id;
            }

            // Add or delete any counselors based upon the checked status.
            foreach (var counselorItem in counselors.Items.OfType<DebtPlus.Data.Controls.SortedCheckedListboxControlItem>())
            {
                var counselorID = (counselorItem.Value as DebtPlus.LINQ.counselor).Id;
                var qFound = record.appt_counselor_templates.Where(s => s.counselor == counselorID).FirstOrDefault();
                if (counselorItem.CheckState == CheckState.Checked)
                {
                    // The item was checked. Add it if it is not present.
                    if (qFound == null)
                    {
                        qFound = DebtPlus.LINQ.Factory.Manufacture_appt_counselor_template();
                        qFound.counselor = counselorID;
                        record.appt_counselor_templates.Add(qFound);
                    }
                }
                else
                {
                    // The value is not checked. Remove it if it was found.
                    if (qFound != null)
                    {
                        record.appt_counselor_templates.Remove(qFound);
                        qFound.appt_time_template1 = null;
                    }
                }
            }
        }
    }
}