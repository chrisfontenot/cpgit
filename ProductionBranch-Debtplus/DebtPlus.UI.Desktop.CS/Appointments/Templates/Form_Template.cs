#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using DebtPlus.UI.Common;
using System.Data.SqlClient;
using DebtPlus.UI.FormLib.Offices;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.Appointments.Templates
{
    internal partial class Form_Template : DebtPlus.Data.Forms.DebtPlusForm
    {
        // Connection to the database
        private BusinessContext bc = null;
        private System.Collections.Generic.List<appt_time_template> colRecords = null;
        private System.Collections.Generic.List<office> colOffices = null;

        public Form_Template() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            lblOffice.Click                     += lblOffice_Click;
            Load                                += Form_Template_Load;
            GridView1.DoubleClick               += GridView1_DoubleClick;
            Button_Edit.Click                   += Button_Edit_Click;
            ContextMenu1.Popup                  += ContextMenu1_Popup;
            ContextMenu1_Create.Click           += ContextMenu1_Create_Click;
            ContextMenu1_Edit.Click             += ContextMenu1_Edit_Click;
            ContextMenu1_Delete.Click           += ContextMenu1_Delete_Click;
            Button_Cancel.Click                 += Button_Cancel_Click;
            LookUpEdit_Office.EditValueChanged  += LookupEditChanged;
            LookUpEdit_Weekday.EditValueChanged += LookupEditChanged;
            LookUpEdit_Office.EditValueChanging += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            GridView1.CustomColumnDisplayText   += GridView1_CustomColumnDisplayText;
        }

        private void UnRegisterHandlers()
        {
            lblOffice.Click                     -= lblOffice_Click;
            Load                                -= Form_Template_Load;
            GridView1.DoubleClick               -= GridView1_DoubleClick;
            Button_Edit.Click                   -= Button_Edit_Click;
            ContextMenu1.Popup                  -= ContextMenu1_Popup;
            ContextMenu1_Create.Click           -= ContextMenu1_Create_Click;
            ContextMenu1_Edit.Click             -= ContextMenu1_Edit_Click;
            ContextMenu1_Delete.Click           -= ContextMenu1_Delete_Click;
            Button_Cancel.Click                 -= Button_Cancel_Click;
            LookUpEdit_Office.EditValueChanged  -= LookupEditChanged;
            LookUpEdit_Weekday.EditValueChanged -= LookupEditChanged;
            LookUpEdit_Office.EditValueChanging -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            GridView1.CustomColumnDisplayText   -= GridView1_CustomColumnDisplayText;
        }

#region Windows Form Designer generated code

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }
                bc = null;
                components = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl lblOffice;
        internal DevExpress.XtraEditors.LabelControl lblDayOfWeek;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_Office;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_Weekday;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_time;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_appt_type;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_appt_template_time;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_counselors;
        internal System.Windows.Forms.ContextMenu ContextMenu1;
        internal DevExpress.XtraEditors.SimpleButton Button_Edit;
        internal System.Windows.Forms.MenuItem ContextMenu1_Delete;
        internal System.Windows.Forms.MenuItem ContextMenu1_Edit;
        internal System.Windows.Forms.MenuItem ContextMenu1_Create;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.ContextMenu1 = new System.Windows.Forms.ContextMenu();
            this.ContextMenu1_Create = new System.Windows.Forms.MenuItem();
            this.ContextMenu1_Edit = new System.Windows.Forms.MenuItem();
            this.ContextMenu1_Delete = new System.Windows.Forms.MenuItem();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_appt_template_time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_appt_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_counselors = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblOffice = new DevExpress.XtraEditors.LabelControl();
            this.lblDayOfWeek = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Office = new DevExpress.XtraEditors.LookUpEdit();
            this.LookUpEdit_Weekday = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_Edit = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Office.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Weekday.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.ContextMenu = this.ContextMenu1;
            this.GridControl1.Location = new System.Drawing.Point(0, 56);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(384, 200);
            this.GridControl1.TabIndex = 4;
            this.GridControl1.ToolTipController = this.ToolTipController1;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // ContextMenu1
            // 
            this.ContextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.ContextMenu1_Create,
            this.ContextMenu1_Edit,
            this.ContextMenu1_Delete});
            // 
            // ContextMenu1_Create
            // 
            this.ContextMenu1_Create.Index = 0;
            this.ContextMenu1_Create.Text = "&Add";
            // 
            // ContextMenu1_Edit
            // 
            this.ContextMenu1_Edit.Index = 1;
            this.ContextMenu1_Edit.Text = "&Change";
            // 
            // ContextMenu1_Delete
            // 
            this.ContextMenu1_Delete.Index = 2;
            this.ContextMenu1_Delete.Text = "&Delete";
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_appt_template_time,
            this.GridColumn_appt_type,
            this.GridColumn_time,
            this.GridColumn_counselors});
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsDetail.AllowZoomDetail = false;
            this.GridView1.OptionsDetail.EnableMasterViewMode = false;
            this.GridView1.OptionsDetail.ShowDetailTabs = false;
            this.GridView1.OptionsDetail.SmartDetailExpand = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_time, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // GridColumn_appt_template_time
            // 
            this.GridColumn_appt_template_time.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_appt_template_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_appt_template_time.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_appt_template_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_appt_template_time.Caption = "Record ID";
            this.GridColumn_appt_template_time.CustomizationCaption = "Record ID";
            this.GridColumn_appt_template_time.DisplayFormat.FormatString = "f0";
            this.GridColumn_appt_template_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_appt_template_time.FieldName = "Id";
            this.GridColumn_appt_template_time.GroupFormat.FormatString = "f0";
            this.GridColumn_appt_template_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_appt_template_time.Name = "GridColumn_appt_template_time";
            this.GridColumn_appt_template_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn_appt_type
            // 
            this.GridColumn_appt_type.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_appt_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_appt_type.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_appt_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_appt_type.Caption = "Appt Type ID";
            this.GridColumn_appt_type.CustomizationCaption = "Appt Type ID";
            this.GridColumn_appt_type.Name = "GridColumn_appt_type";
            this.GridColumn_appt_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_appt_type.Visible = true;
            this.GridColumn_appt_type.VisibleIndex = 1;
            this.GridColumn_appt_type.Width = 96;
            // 
            // GridColumn_time
            // 
            this.GridColumn_time.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_time.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_time.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_time.Caption = "Time";
            this.GridColumn_time.CustomizationCaption = "Time of day";
            this.GridColumn_time.Name = "GridColumn_time";
            this.GridColumn_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_time.Visible = true;
            this.GridColumn_time.VisibleIndex = 0;
            this.GridColumn_time.Width = 111;
            // 
            // GridColumn_counselors
            // 
            this.GridColumn_counselors.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_counselors.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_counselors.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_counselors.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.GridColumn_counselors.Caption = "Counselors";
            this.GridColumn_counselors.CustomizationCaption = "Counselor Name(s)";
            this.GridColumn_counselors.FieldName = "Id";
            this.GridColumn_counselors.Name = "GridColumn_counselors";
            this.GridColumn_counselors.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_counselors.Visible = true;
            this.GridColumn_counselors.VisibleIndex = 2;
            this.GridColumn_counselors.Width = 410;
            // 
            // lblOffice
            // 
            this.lblOffice.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Underline);
            this.lblOffice.Location = new System.Drawing.Point(8, 11);
            this.lblOffice.Name = "lblOffice";
            this.lblOffice.Size = new System.Drawing.Size(42, 13);
            this.lblOffice.TabIndex = 0;
            this.lblOffice.Text = "officeID:";
            this.lblOffice.ToolTipController = this.ToolTipController1;
            // 
            // lblDayOfWeek
            // 
            this.lblDayOfWeek.Location = new System.Drawing.Point(8, 35);
            this.lblDayOfWeek.Name = "lblDayOfWeek";
            this.lblDayOfWeek.Size = new System.Drawing.Size(79, 13);
            this.lblDayOfWeek.TabIndex = 1;
            this.lblDayOfWeek.Text = "Day of the week";
            this.lblDayOfWeek.ToolTipController = this.ToolTipController1;
            // 
            // LookUpEdit_Office
            // 
            this.LookUpEdit_Office.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_Office.Location = new System.Drawing.Point(96, 8);
            this.LookUpEdit_Office.Name = "LookUpEdit_Office";
            this.LookUpEdit_Office.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_Office.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "officeID", 20, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 50, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_Office.Properties.DisplayMember = "name";
            this.LookUpEdit_Office.Properties.NullText = "...Please Choose one...";
            this.LookUpEdit_Office.Properties.ShowFooter = false;
            this.LookUpEdit_Office.Properties.SortColumnIndex = 1;
            this.LookUpEdit_Office.Properties.ValueMember = "Id";
            this.LookUpEdit_Office.Size = new System.Drawing.Size(280, 20);
            this.LookUpEdit_Office.TabIndex = 2;
            this.LookUpEdit_Office.ToolTipController = this.ToolTipController1;
            // 
            // LookUpEdit_Weekday
            // 
            this.LookUpEdit_Weekday.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_Weekday.Location = new System.Drawing.Point(96, 32);
            this.LookUpEdit_Weekday.Name = "LookUpEdit_Weekday";
            this.LookUpEdit_Weekday.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_Weekday.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Weekday", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near)});
            this.LookUpEdit_Weekday.Properties.DisplayMember = "description";
            this.LookUpEdit_Weekday.Properties.NullText = "...Please Choose one...";
            this.LookUpEdit_Weekday.Properties.ShowFooter = false;
            this.LookUpEdit_Weekday.Properties.ShowHeader = false;
            this.LookUpEdit_Weekday.Properties.ShowLines = false;
            this.LookUpEdit_Weekday.Properties.SortColumnIndex = 1;
            this.LookUpEdit_Weekday.Properties.ValueMember = "Id";
            this.LookUpEdit_Weekday.Size = new System.Drawing.Size(280, 20);
            this.LookUpEdit_Weekday.TabIndex = 3;
            this.LookUpEdit_Weekday.ToolTipController = this.ToolTipController1;
            // 
            // Button_Edit
            // 
            this.Button_Edit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Edit.Location = new System.Drawing.Point(111, 264);
            this.Button_Edit.Name = "Button_Edit";
            this.Button_Edit.Size = new System.Drawing.Size(75, 23);
            this.Button_Edit.TabIndex = 5;
            this.Button_Edit.Text = "Edit...";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Location = new System.Drawing.Point(199, 264);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 6;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Form_Template
            // 
            this.ClientSize = new System.Drawing.Size(384, 302);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Edit);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.LookUpEdit_Weekday);
            this.Controls.Add(this.LookUpEdit_Office);
            this.Controls.Add(this.lblDayOfWeek);
            this.Controls.Add(this.lblOffice);
            this.Name = "Form_Template";
            this.Text = "Appointment Template";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Office.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Weekday.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
#endregion Windows Form Designer generated code

        /// <summary>
        /// Load the form
        /// </summary>
        private void Form_Template_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Update the BusinessContext before we do any accesses on it
                bc = new BusinessContext();

                var opt = new System.Data.Linq.DataLoadOptions();
                opt.LoadWith<appt_counselor_template>(s => s.appt_time_template);
                bc.LoadOptions = opt;

                // Obtain the current list of offices. We update the list so it needs to be current
                colOffices = bc.offices.ToList();
                LookUpEdit_Office.Properties.DataSource = colOffices;

                // The weekday names are static
                LookUpEdit_Weekday.Properties.DataSource = DebtPlus.LINQ.InMemory.DaysOfWeeks.getList();

                // There is no office nor weekday at the present time.
                LookUpEdit_Office.EditValue = null;
                LookUpEdit_Weekday.EditValue = null;

                // Define the grid contents as needed and enable the menu items appropriately.
                LoadNewGrid();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process a change in the controls to reload the list of appointment time periods
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LookupEditChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LoadNewGrid();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Define the contents of the grid based upon the selection criteria in the lookup controls
        /// </summary>
        private void LoadNewGrid()
        {
            // Determine if we have enough information to reload the list
            Int32? office = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Office.EditValue);
            Int32? weekday = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Weekday.EditValue);
            if (!office.HasValue || !weekday.HasValue)
            {
                ContextMenu1_Create.Enabled = false;
                ContextMenu1_Edit.Enabled   = false;
                ContextMenu1_Delete.Enabled = false;
                Button_Edit.Enabled         = false;
                return;
            }

            // Retrieve the list of appointment times for this period
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                colRecords = bc.appt_time_templates.Where(s => s.dow == weekday.Value && s.office == office.Value).ToList();
                GridControl1.DataSource = colRecords;
                GridControl1.RefreshDataSource();
                GridView1.RefreshData();

                ContextMenu1_Create.Enabled = true;
                Button_Edit.Enabled = (colRecords.Count > 0);
            }
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.IsValid ? hi.RowHandle : -1;

            // Find the trustRegister view in the dataview object for this trustRegister.
            if (RowHandle >= 0)
            {
                EditRecord(GridView1.GetRow(RowHandle));
            }
        }

        /// <summary>
        /// Edit the current record
        /// </summary>
        private void Button_Edit_Click(System.Object sender, System.EventArgs e)
        {
            if (GridView1.FocusedRowHandle >= 0)
            {
                EditRecord(GridView1.GetRow(GridView1.FocusedRowHandle));
            }
        }

        /// <summary>
        /// Edit -> menu being shown
        /// </summary>
        private void ContextMenu1_Popup(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.IsValid ? hi.RowHandle : -1;
            GridView1.FocusedRowHandle = RowHandle;
            object obj = GridView1.GetRow(RowHandle);

            if (obj != null)
            {
                ContextMenu1_Edit.Enabled = true;
                ContextMenu1_Delete.Enabled = true;
                return;
            }

            ContextMenu1_Edit.Enabled = false;
            ContextMenu1_Delete.Enabled = false;
        }

        /// <summary>
        /// Edit -> Create menu item clicked
        /// </summary>
        private void ContextMenu1_Create_Click(System.Object sender, System.EventArgs e)
        {
            CreateRecord();
        }

        /// <summary>
        /// Edit -> Edit menu item clicked
        /// </summary>
        private void ContextMenu1_Edit_Click(System.Object sender, System.EventArgs e)
        {
            EditRecord(GridView1.GetRow(GridView1.FocusedRowHandle));
        }

        /// <summary>
        /// Edit -> Delete menu item clicked
        /// </summary>
        private void ContextMenu1_Delete_Click(System.Object sender, System.EventArgs e)
        {
            DeleteRecord(GridView1.GetRow(GridView1.FocusedRowHandle));
        }

        /// <summary>
        /// Edit (update) the current record in the collection
        /// </summary>
        /// <param name="obj">Pointer to the current record</param>
        private void EditRecord(object obj)
        {
            // Find the record. It must exist.
            var record = obj as appt_time_template;
            if (record == null)
            {
                return;
            }

            // Edit the record
            using (var frm = new Form_Item(bc, colOffices, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            bc.SubmitChanges();
            GridView1.RefreshData();
        }

        /// <summary>
        /// Create a new record for the collection
        /// </summary>
        private void CreateRecord()
        {
            // Find the office and day of the week. Both must be present to load the grid control.
            Int32? office = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Office.EditValue);
            Int32? weekday = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Weekday.EditValue);
            if (!office.HasValue || !weekday.HasValue)
            {
                return;
            }

            // Create a new record
            var record = DebtPlus.LINQ.Factory.Manufacture_appt_time_template();
            record.office = office.Value;
            record.dow = weekday.Value;

            // Calculate the next starting time for the new record. Use the next hour from the last time for this day/office.
            Int32 lastStartTime = (colRecords.Count <= 0) ? 420 : colRecords.Max(s => s.start_time);

            // Start one hour after the last time period for that office.
            record.start_time = lastStartTime + 60;

            // Update the new record. The defaults have already been set.
            using (var frm = new Form_Item(bc, colOffices, record))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            bc.appt_time_templates.InsertOnSubmit(record);
            bc.SubmitChanges();
            colRecords.Add(record);
            GridView1.RefreshData();
        }

        /// <summary>
        /// Delete the indicated appt_time_template record
        /// </summary>
        /// <param name="obj">Pointer to the current record</param>
        private void DeleteRecord(object obj)
        {
            // Find the record to be deleted. It must exist.
            var record = obj as appt_time_template;
            if (record == null)
            {
                return;
            }

            try
            {
                // Toss the counselor references to this time slot.
                while (record.appt_counselor_templates.Count > 0)
                {
                    var item = record.appt_counselor_templates[0];
                    item.appt_time_template1 = null;
                    record.appt_counselor_templates.Remove(item);
                }

                // Remove the record
                bc.appt_time_templates.DeleteOnSubmit(record);
                bc.SubmitChanges();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }

            finally
            {
                // Remove the record from the display list.
                colRecords.Remove(record);
                GridView1.RefreshData();
            }
        }

        /// <summary>
        /// Handle the Cancel button to close the form
        /// </summary>
        private void Button_Cancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Process the click event on the offices label
        /// </summary>
        private void lblOffice_Click(System.Object sender, System.EventArgs e)
        {
            // We need a selected office to do the edit operation
            var officeRecord = LookUpEdit_Office.GetSelectedDataRow() as DebtPlus.LINQ.office;            
            if (officeRecord == null)
            {
                return;
            }

            // Update the office record. This is why we make a copy rather than use the cached items.
            using (var frm = new DebtPlus.UI.TableAdministration.CS.Offices.OfficeEditForm(bc, officeRecord))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
            }

            // Update the office record accordingly
            bc.SubmitChanges();
        }

        /// <summary>
        /// Handle the custom formatting for the counselor names.
        /// </summary>
        private void GridView1_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            // Find the current row being displayed
            var record = GridView1.GetRow(e.RowHandle) as appt_time_template;
            if (record == null)
            {
                return;
            }

            // Process the appointment type column
            if (e.Column == GridColumn_appt_type)
            {
                if (record.appt_type.HasValue)
                {
                    var q = DebtPlus.LINQ.Cache.appt_type.getList().Find(s => s.Id == record.appt_type.Value);
                    if (q != null)
                    {
                        e.DisplayText = q.appt_name;
                    }
                    else
                    {
                        e.DisplayText = string.Format("??{0:f0}", record.appt_type.Value);
                    }
                    return;
                }

                e.DisplayText = "ALL";
                return;
            }

            // Process the starting time column
            if (e.Column == GridColumn_time)
            {
                var ts = new TimeSpan(record.start_time * TimeSpan.TicksPerSecond * 60);
                var dt = DateTime.MinValue.Add(ts);
                e.DisplayText = string.Format("{0:t}", dt);
                return;
            }

            // Do nothing if we are not doing the counselor names.
            if (e.Column != GridColumn_counselors)
            {
                return;
            }

            // Now, build a list of the counselor names
            var sb = new System.Text.StringBuilder();
            foreach (var coTemplate in record.appt_counselor_templates)
            {
                var counselorID = coTemplate.counselor;
                if (counselorID > 0)
                {
                    var q = DebtPlus.LINQ.Cache.counselor.getList().Find(s => s.Id == counselorID);
                    if (q != null)
                    {
                        sb.AppendFormat(", {0}", q.Name);
                    }
                }
            }

            // Toss the leading ", " sequence
            if (sb.Length > 0)
            {
                sb.Remove(0, 2);
            }

            e.DisplayText = sb.ToString();
        }
    }
}