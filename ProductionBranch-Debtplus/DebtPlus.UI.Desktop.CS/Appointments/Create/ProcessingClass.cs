#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using DebtPlus.LINQ;
using System.Linq;
using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.Appointments.Create
{
    internal partial class ProcessingClass : INotifyCountChanging
    {
        private System.Collections.Generic.List<appt_time_template> colTemplates = null;
        private System.Collections.Generic.List<ProcessingRecordType> colRecords = null;
        private Int32 privateCount = 0;

        /// <summary>
        /// Event to indicate the count is being changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        public event CountChangingEventHandler CountChanging;

        /// <summary>
        /// Raise the CountChanging event
        /// </summary>
        /// <param name="PropertyName"></param>
        /// <param name="OldValue"></param>
        /// <param name="NewValue"></param>
        /// <remarks></remarks>
        protected void RaiseCountChanging(string PropertyName, Int32 OldValue, Int32 NewValue)
        {
            var evt = CountChanging;
            if (evt != null)
            {
                evt(this, new CountChangingEventArgs(PropertyName, OldValue, NewValue));
            }
        }

        public class submitChangesEventArgs : System.EventArgs
        {
            public submitChangesEventArgs() : base()
            {
                Duplicates = false;
            }

            public bool Duplicates { get; set; }
        }

        public delegate void submitChangesEventHandler(object sender, submitChangesEventArgs e);
        public event submitChangesEventHandler SubmitChanges;
        protected void RaiseSubmitChanges(submitChangesEventArgs e)
        {
            var evt = SubmitChanges;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <remarks></remarks>
        public ProcessingClass() : base()
        {
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <param name="tbl"></param>
        /// <remarks></remarks>
        public ProcessingClass(System.Collections.Generic.List<ProcessingRecordType> colRecords, System.Collections.Generic.List<appt_time_template> colTemplates) : this()
        {
            this.colRecords   = colRecords;
            this.colTemplates = colTemplates;
        }

        /// <summary>
        /// Number of appointments created by this thread
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Int32 Count
        {
            get { return privateCount; }
            set
            {
                if (privateCount != value)
                {
                    RaiseCountChanging("Count", privateCount, value);
                    privateCount = value;
                }
            }
        }

        /// <summary>
        /// Date for the appointments
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public DateTime CurrentDate { get; set; }

        /// <summary>
        /// Execute the procedure to create the appointment time slots
        /// </summary>
        /// <remarks></remarks>
        public void RunProcedure()
        {
            Boolean duplicates = false;

            try
            {
                // We need to have a new context for this thread since the updates are thread-local only.
                using (var bc = new BusinessContext())
                {
                    // Make the timeout a bit longer (like 10 minutes!!!)
                    bc.CommandTimeout = 600;

                    // Find the current day of the week from the current date
                    System.DayOfWeek dowEnum = CurrentDate.DayOfWeek;
                    Int32 dow = ((int)dowEnum) + 1;        // Sunday = 1, ... Saturday = 7 for database where Sunday = 0 for enum

                    // Find the offices that have appointments for this day of the week
                    var officeList = new System.Collections.Generic.List<Int32>();
                    foreach (var selItem in colRecords.Where(s => s.isChecked(dow)))
                    {
                        officeList.Add(selItem.OfficeID);
                    }

                    // Take an early exit if there are no selected offices
                    if (officeList.Count < 1)
                    {
                        return;
                    }

                    // Collection of new appt_time records to be submitted
                    System.DateTime fromDate = CurrentDate.Date;
                    System.DateTime toDate = fromDate.AddDays(1);

                    // Find the collection of time entries that are suitable for today in any office.
                    var colApptTime = bc.appt_times.Where(s => s.start_time >= fromDate && s.start_time < toDate).ToList();

                    // Process the entries with the aid of the factory to create the new items
                    // Retrieve the records associated with the office on this day
                    foreach (var templateRecord in colTemplates.Where(s => s.dow == dow && officeList.Contains(s.office)).ToList())
                    {
                        var appointmentTime      = CurrentDate.Date.AddMinutes((double)templateRecord.start_time);
                        var appointmentOffice    = templateRecord.office;
                        appt_time apptTimeRecord = colApptTime.Find(s => s.office == appointmentOffice && s.start_time == appointmentTime);

                        if (apptTimeRecord      != null)
                        {
                            duplicates = true;
                        }
                        else
                        {
                            apptTimeRecord            = DebtPlus.LINQ.Factory.Manufacture_appt_time();
                            apptTimeRecord.appt_type  = templateRecord.appt_type;
                            apptTimeRecord.office     = appointmentOffice;
                            apptTimeRecord.start_time = appointmentTime;

                            // Add the record to the pending queue for today
                            bc.appt_times.InsertOnSubmit(apptTimeRecord);
                        }

                        // To that, add the counselor records
                        foreach (var counselorTemplateRecord in templateRecord.appt_counselor_templates)
                        {
                            var appointmentCounselor = counselorTemplateRecord.counselor;
                            if (apptTimeRecord.appt_counselors.Any(s => s.counselor == appointmentCounselor))
                            {
                                duplicates = true;
                            }
                            else
                            {
                                appt_counselor counselorRecord  = DebtPlus.LINQ.Factory.Manufacture_appt_counselor();
                                counselorRecord.counselor       = counselorTemplateRecord.counselor;
                                counselorRecord.inactive        = 0;
                                counselorRecord.inactive_reason = null;
                                apptTimeRecord.appt_counselors.Add(counselorRecord);

                                // Increment the number of records generated
                                Count += 1;
                            }
                        }
                    }

                    // Submit the changes to the database once we have processed this day
                    bc.SubmitChanges();
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating appointment time slots");
            }

            // Submit all of the daily changes when complete.
            RaiseSubmitChanges(new submitChangesEventArgs() { Duplicates = duplicates });
        }
    }
}