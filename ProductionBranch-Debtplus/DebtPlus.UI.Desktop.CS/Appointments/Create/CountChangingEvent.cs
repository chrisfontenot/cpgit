using System;

namespace DebtPlus.UI.Desktop.CS.Appointments.Create
{
    /// <summary>
    /// Class for the CountChangingEventHandler routine arguments. They contain both the old and new values along with the property name
    /// </summary>
    /// <remarks></remarks>
    internal partial class CountChangingEventArgs : System.ComponentModel.PropertyChangingEventArgs
    {
        public CountChangingEventArgs() : base(null)
        {
        }

        public Int32 OldValue { get; set; }
        public Int32 NewValue { get; set; }

        public CountChangingEventArgs(string PropertyName) : base(PropertyName)
        {
        }

        public CountChangingEventArgs(string PropertyName, Int32 OldValue, Int32 NewValue) : this(PropertyName)
        {
            this.OldValue = OldValue;
            this.NewValue = NewValue;
        }
    }

    /// <summary>
    /// Delegate to the event for the change in the count of appointments created
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <remarks></remarks>
    internal delegate void CountChangingEventHandler(object sender, CountChangingEventArgs e);

    /// <summary>
    /// Interface to describe the event for the count of appointments created
    /// </summary>
    /// <remarks></remarks>
    internal interface INotifyCountChanging
    {
        event CountChangingEventHandler CountChanging;
    }
}