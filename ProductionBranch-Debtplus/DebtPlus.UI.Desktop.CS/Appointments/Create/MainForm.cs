#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Appointments.Create
{
    internal partial class MainForm
    {
        // Array of tasks to process
        private BusinessContext bc                                               = null;
        private System.Collections.Generic.List<ProcessingRecordType> colRecords = new System.Collections.Generic.List<ProcessingRecordType>();
        private System.Collections.Generic.List<appt_time_template> colTemplates = null;
        private System.Collections.Generic.List<Task> lstProcessing              = new System.Collections.Generic.List<Task>();
        private System.ComponentModel.BackgroundWorker bt                        = new System.ComponentModel.BackgroundWorker();
        private Int32 TotalCount                                                 = 0;

        public MainForm()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load                                           += Form_Create_Load;
            bt.DoWork                                      += bt_DoWork;
            bt.RunWorkerCompleted                          += bt_RunWorkerCompleted;
            DateNavigator1.EditDateModified                += DateNavigator1_EditDateModified;
            DateNavigator1.EditDateModified                += formChanged;
            DayOfWeekRepositoryCheckedit.EditValueChanged  += formChanged;
            Button_Cancel.Click                            += Button_Cancel_Click;
            Button_SelectAll.Click                         += Button_SelectAll_Click;
            Button_OK.Click                                += Button_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            Load                                           -= Form_Create_Load;
            bt.DoWork                                      -= bt_DoWork;
            bt.RunWorkerCompleted                          -= bt_RunWorkerCompleted;
            DateNavigator1.EditDateModified                -= DateNavigator1_EditDateModified;
            DateNavigator1.EditDateModified                -= formChanged;
            DayOfWeekRepositoryCheckedit.EditValueChanged  -= formChanged;
            Button_Cancel.Click                            -= Button_Cancel_Click;
            Button_SelectAll.Click                         -= Button_SelectAll_Click;
            Button_OK.Click                                -= Button_OK_Click;
        }

        /// <summary>
        /// Load the form
        /// </summary>
        private void Form_Create_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();

            // Load the saved dialog location and size
            LoadPlacement("Appointments_Create_Form");

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    // Load the counselors with the appointment time values
                    var dl = new System.Data.Linq.DataLoadOptions();
                    dl.LoadWith<appt_time_template>(s => s.appt_counselor_templates);
                    bc = new BusinessContext();
                    bc.LoadOptions = dl;

                    // Load the entire list of appointment time template items
                    colTemplates = bc.appt_time_templates.ToList();

                    // Build a list of the records to be processed
                    foreach (var office in DebtPlus.LINQ.Cache.office.getList().FindAll(s => s.ActiveFlag))
                    {
                        colRecords.Add(new ProcessingRecordType(office));
                    }
                }

                // The result is the possible items that we can create.
                GridControl1.DataSource = colRecords;
                GridControl1.RefreshDataSource();

                // Ensure that the status window is blank
                barStaticItem_AppointmentCount.Caption = string.Empty;
                barStaticItem_Status.Caption = string.Empty;

                // Position the calendar to today
                DateNavigator1.DateTime = System.DateTime.Now.Date;
                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Cancel button :: close the form
        /// </summary>
        private void Button_Cancel_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Process the SelectAll or ClearAll button (same button)
        /// </summary>
        private void Button_SelectAll_Click(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                ClearSelection();

                // Invert the button text
                if (Convert.ToInt32(Button_SelectAll.Tag) == 0)
                {
                    Button_SelectAll.Tag = 1;
                    Button_SelectAll.Text = "Select All";
                }
                else
                {
                    Button_SelectAll.Tag = 0;
                    Button_SelectAll.Text = "Clear All";

                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {

                        // Check the items that have a corresponding item in the appt_time_templates table
                        foreach (var apptTimeTemplateRecord in colTemplates)
                        {
                            var officeID = apptTimeTemplateRecord.office;
                            var qOfficeRecord = colRecords.Find(s => s.OfficeID == officeID);
                            if (qOfficeRecord != null)
                            {
                                qOfficeRecord.SetDow(apptTimeTemplateRecord.dow);
                            }
                        }
                    }

                    GridView1.RefreshData();
                    Button_OK.Enabled = !HasErrors();
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Clear the checkboxes on the form
        /// </summary>
        private void ClearSelection()
        {
            // Reset the trustRegister checkboxes
            foreach (var record in colRecords)
            {
                record.ClearAll();
            }

            // Accept the changes and disable the OK button.
            GridView1.RefreshData();
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Check the days for the current office
        /// </summary>
        private void UpdateRow(ref ProcessingRecordType selectionRecord)
        {
            var officeID = selectionRecord.OfficeID;
            foreach (var templateRecord in colTemplates.Where(s => s.Id == officeID))
            {
                selectionRecord.SetDow(templateRecord.dow);
            }
        }

        /// <summary>
        /// Process the change in the form controls
        /// </summary>
        private void formChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the form contains sufficient information to process the record.
        /// </summary>
        private bool HasErrors()
        {
            // The process can not be in the middle of creating appointments.
            if (bt != null && bt.IsBusy)
            {
                return true;
            }

            // Look for selected days and offices
            foreach (var officeRecord in colRecords)
            {
                // If a day of the week is selected then we are good.
                if (officeRecord.AnyChecked)
                {
                    return false;
                }
            }

            // There is nothing selected. Do not allow the Create button.
            return true;
        }

        // True if there are duplicate entries
        bool duplicatesFound = false;

        /// <summary>
        /// Process the CLICK event on the START button. Start the background thread process going.
        /// </summary>
        private void Button_OK_Click(System.Object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Prevent the form from tripping the button again.
                Button_OK.Enabled = false;

                // Update the status information
                barStaticItem_Status.Caption = "Creating Appointments";
                TotalCount                   = 0;
                duplicatesFound              = false;

                // Create the appointments by date.
                System.DateTime CurrentDate = DateTime.Now.Date;
                foreach(System.DateTime sel in DateNavigator1.Selection.OfType<System.DateTime>().OrderBy(s => s))
                {
                    // Get the selected date from the collection. Ignore dates that are too early
                    CurrentDate = sel.Date;
                    if (CurrentDate < System.DateTime.Now.Date)
                    {
                        continue;
                    }

                    // Create the task to process the current date
                    ProcessingClass cls = new ProcessingClass(colRecords, colTemplates);
                    cls.CurrentDate     = CurrentDate;
                    cls.CountChanging  += cls_CountChanging;
                    cls.SubmitChanges  += cls_SubmitChanges;

                    // the factory to create the task takes an "action" delegate. Ensure that we use the proper sequence for it.
                    Task tsk = Task.Factory.StartNew(new System.Action(cls.RunProcedure), TaskCreationOptions.AttachedToParent);
                    lstProcessing.Add(tsk);
                }

                // Make the selection just the first date beyond the range selected
                DateNavigator1.Selection.Clear();
                DateNavigator1.DateTime = CurrentDate.AddDays(1);
                DateNavigator1.Invalidate();

                // Start a thread that will block until the processing threads have terminated
                // This will then trip the Completed processing for the background thread to finish the process.
                bt.RunWorkerAsync();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private delegate void performSubmitChangesDelegate(object sender, ProcessingClass.submitChangesEventArgs e);
        private void cls_SubmitChanges(object sender, ProcessingClass.submitChangesEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var ia = this.BeginInvoke(new performSubmitChangesDelegate(cls_SubmitChanges), new object[] { sender, e });
                this.EndInvoke(ia);
                return;
            }

            duplicatesFound |= e.Duplicates;
        }

        /// <summary>
        /// Background thread to process the function. Allocate sub-threads to do each day and then wait
        /// for all of them to complete.
        /// </summary>
        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Wait for all of the processing threads to terminate. They will all wait here and this
            // thread will be blocked until we complete. That is why it is running as a background thread
            // because the "foreground" thread needs to be free to run the waiting dialog. When we
            // complete, the bt_RunWorkerCompleted event will trip and we will deal with the foreground again.
            if (lstProcessing.Count > 0)
            {
                Task.WaitAll(lstProcessing.ToArray());
            }
        }

        /// <summary>
        /// The main processing class routines have terminated. Now display the event dialog and enable the exit button
        /// </summary>
        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Marshal to the proper thread. It is ok since the thread is just waiting for the events to happen.
            if (InvokeRequired)
            {
                EndInvoke(BeginInvoke(new System.ComponentModel.RunWorkerCompletedEventHandler(bt_RunWorkerCompleted), new object[] { sender, e }));
                return;
            }

            // Clear the status information again
            barStaticItem_Status.Caption = string.Empty;
            barStaticItem_AppointmentCount.Caption = string.Empty;

            // Tell the user that the operation is complete
            string completion = string.Format("{0:n0} Appointments have been processed.", TotalCount);
            if (duplicatesFound)
            {
                completion += "\r\n\r\nThere were items that have been previously created and were ignored this time.\r\nYou should check the schedule for the dates and offices to ensure that they are correct.\r\n";
            }
            completion += "\r\nYou may close the window.";
            DebtPlus.Data.Forms.MessageBox.Show(completion, "Operation Completed");
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Process the change in the count of appointments that have been created. Each thread maintains
        /// its own count but we need the grand total count so it is done here. We pick up the change in
        /// count on each thread and update the count here.
        /// </summary>
        private void cls_CountChanging(object sender, CountChangingEventArgs e)
        {
            if (InvokeRequired)
            {
                EndInvoke(BeginInvoke(new CountChangingEventHandler(cls_CountChanging), new object[] { sender, e }));
                return;
            }

            TotalCount += e.NewValue - e.OldValue;

            // The true count is what was incremented. To get that, subtract the old from the new value.
            barStaticItem_AppointmentCount.Caption = string.Format("{0:n0} Appointment(s) created", TotalCount);
            bar3.Invalidate();
        }

        /// <summary>
        /// Process a change in the date selection navigator. Do not allow the value to be too early.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void DateNavigator1_EditDateModified(object sender, System.EventArgs e)
        {
            if (DateNavigator1.DateTime < DateTime.Now.Date)
            {
                DateNavigator1.DateTime = DateTime.Now.Date;
            }
        }
    }
}