﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.Desktop.CS.Appointments.Create
{
    internal class ProcessingRecordType
    {
        /// <summary>
        /// Initialize the new processing class
        /// </summary>
        public ProcessingRecordType()
        {
            officeRecord = null;
            ClearAll();
        }

        public ProcessingRecordType(DebtPlus.LINQ.office officeRecord)
            : this()
        {
            this.officeRecord = officeRecord;
        }

        /// <summary>
        /// Check the appropriate day of the week as we have it defined in the database
        /// </summary>
        /// <param name="dow"></param>
        public void SetDow(Int32 dow)
        {
            switch (dow)
            {
                case 1: Sun = true; break;
                case 2: Mon = true; break;
                case 3: Tue = true; break;
                case 4: Wed = true; break;
                case 5: Thu = true; break;
                case 6: Fri = true; break;
                case 7: Sat = true; break;
                default:
                    System.Diagnostics.Debug.Assert(false, "invalid SetDow argument");
                    break;
            }
        }

        /// <summary>
        /// Determine if the record is checked when given the day of the week.
        /// </summary>
        /// <param name="dow"></param>
        /// <returns></returns>
        public bool isChecked(int dow)
        {
            switch (dow)
            {
                case 1: return Sun;
                case 2: return Mon;
                case 3: return Tue;
                case 4: return Wed;
                case 5: return Thu;
                case 6: return Fri;
                case 7: return Sat;
                default: return false;
            }
        }

        /// <summary>
        /// Retrieve the array items for the checked days
        /// </summary>
        /// <returns></returns>
        public Int32[] GetDays()
        {
            var colDays = new System.Collections.Generic.List<Int32>();
            if (Sun) { colDays.Add(1); }
            if (Mon) { colDays.Add(2); }
            if (Tue) { colDays.Add(3); }
            if (Wed) { colDays.Add(4); }
            if (Thu) { colDays.Add(5); }
            if (Fri) { colDays.Add(6); }
            if (Sat) { colDays.Add(7); }
            return colDays.ToArray();
        }

        /// <summary>
        /// Current office Record
        /// </summary>
        private DebtPlus.LINQ.office officeRecord { get; set; }

        /// <summary>
        /// ID for the office
        /// </summary>
        public Int32 OfficeID
        {
            get
            {
                return officeRecord == null ? 0 : officeRecord.Id;
            }
        }

        public string Name
        {
            get
            {
                return officeRecord == null ? string.Empty : officeRecord.name;
            }
        }

        /// <summary>
        /// Determine if any of the days are checked.
        /// </summary>
        public bool AnyChecked
        {
            get
            {
                return CheckedCount > 0;
            }
        }

        public Int32 CheckedCount
        {
            get
            {
                Int32 answerCount = 0;
                if (Sun) ++answerCount;
                if (Mon) ++answerCount;
                if (Tue) ++answerCount;
                if (Wed) ++answerCount;
                if (Thu) ++answerCount;
                if (Fri) ++answerCount;
                if (Sat) ++answerCount;
                return answerCount;
            }
        }

        public void ClearAll()
        {
            Sun = false;
            Mon = false;
            Tue = false;
            Wed = false;
            Thu = false;
            Fri = false;
            Sat = false;
        }

        public bool Sun { get; set; }
        public bool Mon { get; set; }
        public bool Tue { get; set; }
        public bool Wed { get; set; }
        public bool Thu { get; set; }
        public bool Fri { get; set; }
        public bool Sat { get; set; }
    }
}
