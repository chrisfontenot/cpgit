using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Appointments.Create
{
	partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			try
            {
				if (disposing)
                {
					if (components    != null) components.Dispose();
					if (bt            != null) bt.Dispose();
                    if (bc            != null) bc.Dispose();
    				if (lstProcessing != null) lstProcessing.Clear();
				}
                components    = null;
                bt            = null;
                bc            = null;
                lstProcessing = null;
			}

            finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_office = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_office_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_SU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DayOfWeekRepositoryCheckedit = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.GridColumn_M = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_TU = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_W = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_TH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_F = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_SA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Button_SelectAll = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.DateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlI_GridControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_DateNavigator1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_Button_SelectAll = new DevExpress.XtraLayout.LayoutControlItem();
            this.item0 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControl_Button_OK = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControl_Button_Cancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem_Status = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem_AppointmentCount = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayOfWeekRepositoryCheckedit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlI_GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_DateNavigator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Button_SelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Button_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Button_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(12, 12);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.DayOfWeekRepositoryCheckedit});
            this.GridControl1.Size = new System.Drawing.Size(360, 179);
            this.GridControl1.TabIndex = 0;
            this.GridControl1.ToolTipController = this.ToolTipController1;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_office,
            this.GridColumn_office_name,
            this.GridColumn_SU,
            this.GridColumn_M,
            this.GridColumn_TU,
            this.GridColumn_W,
            this.GridColumn_TH,
            this.GridColumn_F,
            this.GridColumn_SA});
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_office_name, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // GridColumn_office
            // 
            this.GridColumn_office.Caption = "Office ID";
            this.GridColumn_office.CustomizationCaption = "Office ID";
            this.GridColumn_office.FieldName = "OfficeID";
            this.GridColumn_office.Name = "GridColumn_office";
            this.GridColumn_office.OptionsColumn.AllowEdit = false;
            this.GridColumn_office.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // GridColumn_office_name
            // 
            this.GridColumn_office_name.Caption = "Office";
            this.GridColumn_office_name.CustomizationCaption = "Office Name";
            this.GridColumn_office_name.FieldName = "Name";
            this.GridColumn_office_name.Name = "GridColumn_office_name";
            this.GridColumn_office_name.OptionsColumn.AllowEdit = false;
            this.GridColumn_office_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_office_name.Visible = true;
            this.GridColumn_office_name.VisibleIndex = 0;
            this.GridColumn_office_name.Width = 134;
            // 
            // GridColumn_SU
            // 
            this.GridColumn_SU.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_SU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_SU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridColumn_SU.AppearanceHeader.ForeColor = System.Drawing.Color.Red;
            this.GridColumn_SU.AppearanceHeader.Options.UseFont = true;
            this.GridColumn_SU.AppearanceHeader.Options.UseForeColor = true;
            this.GridColumn_SU.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_SU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_SU.Caption = "S";
            this.GridColumn_SU.ColumnEdit = this.DayOfWeekRepositoryCheckedit;
            this.GridColumn_SU.CustomizationCaption = "Sunday";
            this.GridColumn_SU.FieldName = "Sun";
            this.GridColumn_SU.MinWidth = 32;
            this.GridColumn_SU.Name = "GridColumn_SU";
            this.GridColumn_SU.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_SU.OptionsColumn.AllowIncrementalSearch = false;
            this.GridColumn_SU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_SU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_SU.OptionsColumn.FixedWidth = true;
            this.GridColumn_SU.OptionsColumn.ShowInCustomizationForm = false;
            this.GridColumn_SU.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_SU.Visible = true;
            this.GridColumn_SU.VisibleIndex = 1;
            this.GridColumn_SU.Width = 32;
            // 
            // DayOfWeekRepositoryCheckedit
            // 
            this.DayOfWeekRepositoryCheckedit.Caption = "";
            this.DayOfWeekRepositoryCheckedit.Name = "DayOfWeekRepositoryCheckedit";
            this.DayOfWeekRepositoryCheckedit.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Inactive;
            // 
            // GridColumn_M
            // 
            this.GridColumn_M.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_M.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_M.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridColumn_M.AppearanceHeader.Options.UseFont = true;
            this.GridColumn_M.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_M.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_M.Caption = "M";
            this.GridColumn_M.ColumnEdit = this.DayOfWeekRepositoryCheckedit;
            this.GridColumn_M.CustomizationCaption = "Monday";
            this.GridColumn_M.FieldName = "Mon";
            this.GridColumn_M.MinWidth = 32;
            this.GridColumn_M.Name = "GridColumn_M";
            this.GridColumn_M.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_M.OptionsColumn.AllowIncrementalSearch = false;
            this.GridColumn_M.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_M.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_M.OptionsColumn.FixedWidth = true;
            this.GridColumn_M.OptionsColumn.ShowInCustomizationForm = false;
            this.GridColumn_M.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_M.Visible = true;
            this.GridColumn_M.VisibleIndex = 2;
            this.GridColumn_M.Width = 32;
            // 
            // GridColumn_TU
            // 
            this.GridColumn_TU.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_TU.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_TU.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridColumn_TU.AppearanceHeader.Options.UseFont = true;
            this.GridColumn_TU.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_TU.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_TU.Caption = "T";
            this.GridColumn_TU.ColumnEdit = this.DayOfWeekRepositoryCheckedit;
            this.GridColumn_TU.FieldName = "Tue";
            this.GridColumn_TU.MinWidth = 32;
            this.GridColumn_TU.Name = "GridColumn_TU";
            this.GridColumn_TU.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_TU.OptionsColumn.AllowIncrementalSearch = false;
            this.GridColumn_TU.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_TU.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_TU.OptionsColumn.FixedWidth = true;
            this.GridColumn_TU.OptionsColumn.ShowInCustomizationForm = false;
            this.GridColumn_TU.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_TU.Visible = true;
            this.GridColumn_TU.VisibleIndex = 3;
            this.GridColumn_TU.Width = 32;
            // 
            // GridColumn_W
            // 
            this.GridColumn_W.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_W.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_W.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridColumn_W.AppearanceHeader.Options.UseFont = true;
            this.GridColumn_W.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_W.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_W.Caption = "W";
            this.GridColumn_W.ColumnEdit = this.DayOfWeekRepositoryCheckedit;
            this.GridColumn_W.FieldName = "Wed";
            this.GridColumn_W.MinWidth = 32;
            this.GridColumn_W.Name = "GridColumn_W";
            this.GridColumn_W.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_W.OptionsColumn.AllowIncrementalSearch = false;
            this.GridColumn_W.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_W.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_W.OptionsColumn.FixedWidth = true;
            this.GridColumn_W.OptionsColumn.ShowInCustomizationForm = false;
            this.GridColumn_W.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_W.Visible = true;
            this.GridColumn_W.VisibleIndex = 4;
            this.GridColumn_W.Width = 32;
            // 
            // GridColumn_TH
            // 
            this.GridColumn_TH.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_TH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_TH.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridColumn_TH.AppearanceHeader.Options.UseFont = true;
            this.GridColumn_TH.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_TH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_TH.Caption = "T";
            this.GridColumn_TH.ColumnEdit = this.DayOfWeekRepositoryCheckedit;
            this.GridColumn_TH.FieldName = "Thu";
            this.GridColumn_TH.MinWidth = 32;
            this.GridColumn_TH.Name = "GridColumn_TH";
            this.GridColumn_TH.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_TH.OptionsColumn.AllowIncrementalSearch = false;
            this.GridColumn_TH.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_TH.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_TH.OptionsColumn.FixedWidth = true;
            this.GridColumn_TH.OptionsColumn.ShowInCustomizationForm = false;
            this.GridColumn_TH.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_TH.Visible = true;
            this.GridColumn_TH.VisibleIndex = 5;
            this.GridColumn_TH.Width = 32;
            // 
            // GridColumn_F
            // 
            this.GridColumn_F.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_F.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_F.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridColumn_F.AppearanceHeader.Options.UseFont = true;
            this.GridColumn_F.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_F.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_F.Caption = "F";
            this.GridColumn_F.ColumnEdit = this.DayOfWeekRepositoryCheckedit;
            this.GridColumn_F.FieldName = "Fri";
            this.GridColumn_F.MinWidth = 32;
            this.GridColumn_F.Name = "GridColumn_F";
            this.GridColumn_F.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_F.OptionsColumn.AllowIncrementalSearch = false;
            this.GridColumn_F.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_F.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_F.OptionsColumn.FixedWidth = true;
            this.GridColumn_F.OptionsColumn.ShowInCustomizationForm = false;
            this.GridColumn_F.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_F.Visible = true;
            this.GridColumn_F.VisibleIndex = 6;
            this.GridColumn_F.Width = 32;
            // 
            // GridColumn_SA
            // 
            this.GridColumn_SA.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_SA.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_SA.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.GridColumn_SA.AppearanceHeader.ForeColor = System.Drawing.Color.Blue;
            this.GridColumn_SA.AppearanceHeader.Options.UseFont = true;
            this.GridColumn_SA.AppearanceHeader.Options.UseForeColor = true;
            this.GridColumn_SA.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_SA.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_SA.Caption = "S";
            this.GridColumn_SA.ColumnEdit = this.DayOfWeekRepositoryCheckedit;
            this.GridColumn_SA.FieldName = "Sat";
            this.GridColumn_SA.MinWidth = 32;
            this.GridColumn_SA.Name = "GridColumn_SA";
            this.GridColumn_SA.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_SA.OptionsColumn.AllowIncrementalSearch = false;
            this.GridColumn_SA.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_SA.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.GridColumn_SA.OptionsColumn.FixedWidth = true;
            this.GridColumn_SA.OptionsColumn.ShowInCustomizationForm = false;
            this.GridColumn_SA.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_SA.Visible = true;
            this.GridColumn_SA.VisibleIndex = 7;
            this.GridColumn_SA.Width = 32;
            // 
            // Button_SelectAll
            // 
            this.Button_SelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Button_SelectAll.Location = new System.Drawing.Point(12, 195);
            this.Button_SelectAll.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_SelectAll.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_SelectAll.Name = "Button_SelectAll";
            this.Button_SelectAll.Size = new System.Drawing.Size(75, 23);
            this.Button_SelectAll.StyleController = this.layoutControl1;
            this.Button_SelectAll.TabIndex = 2;
            this.Button_SelectAll.Tag = "1";
            this.Button_SelectAll.Text = "Select All";
            this.Button_SelectAll.ToolTipController = this.ToolTipController1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.GridControl1);
            this.layoutControl1.Controls.Add(this.DateNavigator1);
            this.layoutControl1.Controls.Add(this.Button_OK);
            this.layoutControl1.Controls.Add(this.Button_Cancel);
            this.layoutControl1.Controls.Add(this.Button_SelectAll);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(326, 367, 250, 350);
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(568, 230);
            this.layoutControl1.TabIndex = 6;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // dateNavigator1
            // 
            this.DateNavigator1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DateNavigator1.HotDate = null;
            this.DateNavigator1.Location = new System.Drawing.Point(376, 12);
            this.DateNavigator1.MinimumSize = new System.Drawing.Size(180, 0);
            this.DateNavigator1.Name = "dateNavigator1";
            this.DateNavigator1.ShowWeekNumbers = false;
            this.DateNavigator1.Size = new System.Drawing.Size(180, 179);
            this.DateNavigator1.TabIndex = 5;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OK.Location = new System.Drawing.Point(402, 195);
            this.Button_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.StyleController = this.layoutControl1;
            this.Button_OK.TabIndex = 3;
            this.Button_OK.Text = "Create";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.Location = new System.Drawing.Point(481, 195);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.StyleController = this.layoutControl1;
            this.Button_Cancel.TabIndex = 4;
            this.Button_Cancel.Text = "&Quit";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlI_GridControl1,
            this.layoutControl_DateNavigator1,
            this.layoutControl_Button_SelectAll,
            this.item0,
            this.layoutControl_Button_OK,
            this.layoutControl_Button_Cancel});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(568, 230);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlI_GridControl1
            // 
            this.layoutControlI_GridControl1.Control = this.GridControl1;
            this.layoutControlI_GridControl1.CustomizationFormText = "GridControl1";
            this.layoutControlI_GridControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlI_GridControl1.Name = "layoutControlI_GridControl1";
            this.layoutControlI_GridControl1.Size = new System.Drawing.Size(364, 183);
            this.layoutControlI_GridControl1.Text = "GridControl1";
            this.layoutControlI_GridControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlI_GridControl1.TextToControlDistance = 0;
            this.layoutControlI_GridControl1.TextVisible = false;
            // 
            // layoutControl_DateNavigator1
            // 
            this.layoutControl_DateNavigator1.Control = this.DateNavigator1;
            this.layoutControl_DateNavigator1.CustomizationFormText = "dateNavigator1";
            this.layoutControl_DateNavigator1.Location = new System.Drawing.Point(364, 0);
            this.layoutControl_DateNavigator1.Name = "layoutControl_DateNavigator1";
            this.layoutControl_DateNavigator1.Size = new System.Drawing.Size(184, 183);
            this.layoutControl_DateNavigator1.Text = "dateNavigator1";
            this.layoutControl_DateNavigator1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControl_DateNavigator1.TextToControlDistance = 0;
            this.layoutControl_DateNavigator1.TextVisible = false;
            // 
            // layoutControl_Button_SelectAll
            // 
            this.layoutControl_Button_SelectAll.Control = this.Button_SelectAll;
            this.layoutControl_Button_SelectAll.CustomizationFormText = "Button_SelectAll";
            this.layoutControl_Button_SelectAll.Location = new System.Drawing.Point(0, 183);
            this.layoutControl_Button_SelectAll.Name = "layoutControl_Button_SelectAll";
            this.layoutControl_Button_SelectAll.Size = new System.Drawing.Size(79, 27);
            this.layoutControl_Button_SelectAll.Text = "Button_SelectAll";
            this.layoutControl_Button_SelectAll.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControl_Button_SelectAll.TextToControlDistance = 0;
            this.layoutControl_Button_SelectAll.TextVisible = false;
            // 
            // item0
            // 
            this.item0.AllowHotTrack = false;
            this.item0.CustomizationFormText = "item0";
            this.item0.Location = new System.Drawing.Point(79, 183);
            this.item0.Name = "item0";
            this.item0.Size = new System.Drawing.Size(311, 27);
            this.item0.Text = "item0";
            this.item0.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControl_Button_OK
            // 
            this.layoutControl_Button_OK.Control = this.Button_OK;
            this.layoutControl_Button_OK.CustomizationFormText = "Button_OK";
            this.layoutControl_Button_OK.Location = new System.Drawing.Point(390, 183);
            this.layoutControl_Button_OK.Name = "layoutControl_Button_OK";
            this.layoutControl_Button_OK.Size = new System.Drawing.Size(79, 27);
            this.layoutControl_Button_OK.Text = "Button_OK";
            this.layoutControl_Button_OK.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControl_Button_OK.TextToControlDistance = 0;
            this.layoutControl_Button_OK.TextVisible = false;
            // 
            // layoutControl_Button_Cancel
            // 
            this.layoutControl_Button_Cancel.Control = this.Button_Cancel;
            this.layoutControl_Button_Cancel.CustomizationFormText = "Button_Cancel";
            this.layoutControl_Button_Cancel.Location = new System.Drawing.Point(469, 183);
            this.layoutControl_Button_Cancel.Name = "layoutControl_Button_Cancel";
            this.layoutControl_Button_Cancel.Size = new System.Drawing.Size(79, 27);
            this.layoutControl_Button_Cancel.Text = "Button_Cancel";
            this.layoutControl_Button_Cancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControl_Button_Cancel.TextToControlDistance = 0;
            this.layoutControl_Button_Cancel.TextVisible = false;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem_Status,
            this.barStaticItem_AppointmentCount});
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar3;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(568, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 230);
            this.barDockControlBottom.Size = new System.Drawing.Size(568, 28);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 230);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(568, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 230);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem_Status),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem_AppointmentCount)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticItem_Status
            // 
            this.barStaticItem_Status.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.barStaticItem_Status.Id = 0;
            this.barStaticItem_Status.Name = "barStaticItem_Status";
            this.barStaticItem_Status.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticItem_Status.Width = 32;
            // 
            // barStaticItem_AppointmentCount
            // 
            this.barStaticItem_AppointmentCount.Id = 1;
            this.barStaticItem_AppointmentCount.Name = "barStaticItem_AppointmentCount";
            this.barStaticItem_AppointmentCount.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // Form_Create
            // 
            this.ClientSize = new System.Drawing.Size(568, 258);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "Form_Create";
            this.Text = "Create Schedule Items";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DayOfWeekRepositoryCheckedit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlI_GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_DateNavigator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Button_SelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Button_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl_Button_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

		}
        private DevExpress.XtraScheduler.DateNavigator DateNavigator1;
        private DevExpress.XtraGrid.GridControl GridControl1;
		private DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_office;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_office_name;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_SU;
		private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit DayOfWeekRepositoryCheckedit;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_M;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_TU;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_W;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_TH;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_F;
		private DevExpress.XtraGrid.Columns.GridColumn GridColumn_SA;
		private DevExpress.XtraEditors.SimpleButton Button_SelectAll;
		private DevExpress.XtraEditors.SimpleButton Button_OK;
		private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlI_GridControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_DateNavigator1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_Button_SelectAll;
        private DevExpress.XtraLayout.EmptySpaceItem item0;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_Button_OK;
        private DevExpress.XtraLayout.LayoutControlItem layoutControl_Button_Cancel;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem_Status;
        private DevExpress.XtraBars.BarStaticItem barStaticItem_AppointmentCount;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
	}
}
