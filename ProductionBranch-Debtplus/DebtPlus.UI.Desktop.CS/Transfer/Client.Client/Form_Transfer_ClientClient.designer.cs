﻿namespace DebtPlus.UI.Desktop.CS.Transfer.Client.Client
{
    partial class Form_Transfer_ClientClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Transfer_ClientClient));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Transfer = new DevExpress.XtraEditors.SimpleButton();
            this.cbo_Reason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Label14 = new DevExpress.XtraEditors.LabelControl();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.txb_dest_client = new DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.Label11 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_dest_inactive = new DebtPlus.Data.Controls.BlinkLabel();
            this.lbl_new_dest_trust = new DevExpress.XtraEditors.LabelControl();
            this.Label8 = new DevExpress.XtraEditors.LabelControl();
            this.Label9 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_old_dest_trust = new DevExpress.XtraEditors.LabelControl();
            this.med_TargetName = new DevExpress.XtraEditors.MemoEdit();
            this.Label12 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_dest_amount = new DevExpress.XtraEditors.LabelControl();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.txb_source_client = new DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.Label6 = new DevExpress.XtraEditors.LabelControl();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_source_inactive = new DebtPlus.Data.Controls.BlinkLabel();
            this.lbl_new_source_trust = new DevExpress.XtraEditors.LabelControl();
            this.txc_amount = new DevExpress.XtraEditors.CalcEdit();
            this.Label4 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_old_source_trust = new DevExpress.XtraEditors.LabelControl();
            this.med_SourceName = new DevExpress.XtraEditors.MemoEdit();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).BeginInit();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txb_dest_client.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_TargetName.Properties)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txb_source_client.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_SourceName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.CausesValidation = false;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(258, 301);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 24);
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Quit";
            // 
            // Button_Transfer
            // 
            this.Button_Transfer.Enabled = false;
            this.Button_Transfer.Location = new System.Drawing.Point(146, 301);
            this.Button_Transfer.Name = "Button_Transfer";
            this.Button_Transfer.Size = new System.Drawing.Size(75, 24);
            this.Button_Transfer.TabIndex = 10;
            this.Button_Transfer.Text = "Transfer";
            // 
            // cbo_Reason
            // 
            this.cbo_Reason.AllowDrop = true;
            this.cbo_Reason.EditValue = "";
            this.cbo_Reason.EnterMoveNextControl = true;
            this.cbo_Reason.Location = new System.Drawing.Point(123, 257);
            this.cbo_Reason.Name = "cbo_Reason";
            this.cbo_Reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo_Reason.Properties.MaxLength = 50;
            this.cbo_Reason.Size = new System.Drawing.Size(336, 20);
            this.cbo_Reason.TabIndex = 9;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(3, 259);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(114, 13);
            this.Label14.TabIndex = 8;
            this.Label14.Text = "Reason for the transfer";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.txb_dest_client);
            this.GroupBox2.Controls.Add(this.Label11);
            this.GroupBox2.Controls.Add(this.lbl_dest_inactive);
            this.GroupBox2.Controls.Add(this.lbl_new_dest_trust);
            this.GroupBox2.Controls.Add(this.Label8);
            this.GroupBox2.Controls.Add(this.Label9);
            this.GroupBox2.Controls.Add(this.lbl_old_dest_trust);
            this.GroupBox2.Controls.Add(this.med_TargetName);
            this.GroupBox2.Controls.Add(this.Label12);
            this.GroupBox2.Controls.Add(this.lbl_dest_amount);
            this.GroupBox2.Location = new System.Drawing.Point(3, 128);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(472, 112);
            this.GroupBox2.TabIndex = 7;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = " Transfer to the Client ";
            // 
            // txb_dest_client
            // 
            this.txb_dest_client.AllowDrop = true;
            this.txb_dest_client.EnterMoveNextControl = true;
            this.txb_dest_client.Location = new System.Drawing.Point(118, 23);
            this.txb_dest_client.Name = "txb_dest_client";
            this.txb_dest_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txb_dest_client.Properties.Appearance.Options.UseTextOptions = true;
            this.txb_dest_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txb_dest_client.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("txb_dest_client.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to search for a client ID", "search", null, true)});
            this.txb_dest_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txb_dest_client.Properties.DisplayFormat.FormatString = "0000000";
            this.txb_dest_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txb_dest_client.Properties.EditFormat.FormatString = "f0";
            this.txb_dest_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txb_dest_client.Properties.Mask.BeepOnError = true;
            this.txb_dest_client.Properties.Mask.EditMask = "\\d*";
            this.txb_dest_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txb_dest_client.Properties.ValidateOnEnterKey = true;
            this.txb_dest_client.Size = new System.Drawing.Size(100, 20);
            this.txb_dest_client.TabIndex = 1;
            this.txb_dest_client.ValidationLevel = DebtPlus.UI.Client.Widgets.Controls.ClientID.ValidationLevelEnum.NotZero;
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(256, 26);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(104, 13);
            this.Label11.TabIndex = 3;
            this.Label11.Text = "Original Trust Balance";
            this.Label11.UseMnemonic = false;
            // 
            // lbl_dest_inactive
            // 
            this.lbl_dest_inactive.Appearance.BackColor = System.Drawing.Color.Red;
            this.lbl_dest_inactive.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dest_inactive.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_dest_inactive.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_dest_inactive.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_dest_inactive.BlinkTime = 500;
            this.lbl_dest_inactive.Location = new System.Drawing.Point(152, 0);
            this.lbl_dest_inactive.Name = "lbl_dest_inactive";
            this.lbl_dest_inactive.Size = new System.Drawing.Size(0, 13);
            this.lbl_dest_inactive.TabIndex = 9;
            this.lbl_dest_inactive.UseMnemonic = false;
            this.lbl_dest_inactive.Visible = false;
            // 
            // lbl_new_dest_trust
            // 
            this.lbl_new_dest_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_new_dest_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_new_dest_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_new_dest_trust.Location = new System.Drawing.Point(360, 78);
            this.lbl_new_dest_trust.Name = "lbl_new_dest_trust";
            this.lbl_new_dest_trust.Size = new System.Drawing.Size(71, 13);
            this.lbl_new_dest_trust.TabIndex = 8;
            this.lbl_new_dest_trust.Text = "$0.00";
            this.lbl_new_dest_trust.UseMnemonic = false;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(256, 78);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(81, 13);
            this.Label8.TabIndex = 7;
            this.Label8.Text = "New trust Balace";
            this.Label8.UseMnemonic = false;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(256, 52);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(65, 13);
            this.Label9.TabIndex = 5;
            this.Label9.Text = "Plus transfer:";
            this.Label9.UseMnemonic = false;
            // 
            // lbl_old_dest_trust
            // 
            this.lbl_old_dest_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_old_dest_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_old_dest_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_old_dest_trust.Location = new System.Drawing.Point(360, 26);
            this.lbl_old_dest_trust.Name = "lbl_old_dest_trust";
            this.lbl_old_dest_trust.Size = new System.Drawing.Size(71, 13);
            this.lbl_old_dest_trust.TabIndex = 4;
            this.lbl_old_dest_trust.Text = "$0.00";
            this.lbl_old_dest_trust.UseMnemonic = false;
            // 
            // med_TargetName
            // 
            this.med_TargetName.EditValue = "";
            this.med_TargetName.Location = new System.Drawing.Point(16, 52);
            this.med_TargetName.Name = "med_TargetName";
            this.med_TargetName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.med_TargetName.Properties.LookAndFeel.SkinName = "Coffee";
            this.med_TargetName.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.med_TargetName.Properties.ReadOnly = true;
            this.med_TargetName.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.med_TargetName.Size = new System.Drawing.Size(232, 51);
            this.med_TargetName.TabIndex = 2;
            this.med_TargetName.TabStop = false;
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(16, 26);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(66, 13);
            this.Label12.TabIndex = 0;
            this.Label12.Text = "Target Client:";
            // 
            // lbl_dest_amount
            // 
            this.lbl_dest_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_dest_amount.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_dest_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_dest_amount.Location = new System.Drawing.Point(360, 52);
            this.lbl_dest_amount.Name = "lbl_dest_amount";
            this.lbl_dest_amount.Size = new System.Drawing.Size(71, 13);
            this.lbl_dest_amount.TabIndex = 6;
            this.lbl_dest_amount.Text = "$0.00";
            this.lbl_dest_amount.UseMnemonic = false;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.txb_source_client);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.lbl_source_inactive);
            this.GroupBox1.Controls.Add(this.lbl_new_source_trust);
            this.GroupBox1.Controls.Add(this.txc_amount);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.lbl_old_source_trust);
            this.GroupBox1.Controls.Add(this.med_SourceName);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(3, 8);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(472, 112);
            this.GroupBox1.TabIndex = 6;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = " Transfer From the Client ";
            // 
            // txb_source_client
            // 
            this.txb_source_client.AllowDrop = true;
            this.txb_source_client.EnterMoveNextControl = true;
            this.txb_source_client.Location = new System.Drawing.Point(118, 23);
            this.txb_source_client.Name = "txb_source_client";
            this.txb_source_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txb_source_client.Properties.Appearance.Options.UseTextOptions = true;
            this.txb_source_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txb_source_client.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("txb_source_client.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "Click here to search for a client ID", "search", null, true)});
            this.txb_source_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txb_source_client.Properties.DisplayFormat.FormatString = "0000000";
            this.txb_source_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txb_source_client.Properties.EditFormat.FormatString = "f0";
            this.txb_source_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txb_source_client.Properties.Mask.BeepOnError = true;
            this.txb_source_client.Properties.Mask.EditMask = "\\d*";
            this.txb_source_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txb_source_client.Properties.ValidateOnEnterKey = true;
            this.txb_source_client.Size = new System.Drawing.Size(100, 20);
            this.txb_source_client.TabIndex = 1;
            this.txb_source_client.ValidationLevel = DebtPlus.UI.Client.Widgets.Controls.ClientID.ValidationLevelEnum.NotZero;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(256, 78);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(81, 13);
            this.Label6.TabIndex = 7;
            this.Label6.Text = "New trust Balace";
            this.Label6.UseMnemonic = false;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(256, 26);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(104, 13);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Original Trust Balance";
            this.Label2.UseMnemonic = false;
            // 
            // lbl_source_inactive
            // 
            this.lbl_source_inactive.Appearance.BackColor = System.Drawing.Color.Red;
            this.lbl_source_inactive.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_source_inactive.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_source_inactive.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_source_inactive.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_source_inactive.BlinkTime = 500;
            this.lbl_source_inactive.Location = new System.Drawing.Point(152, 0);
            this.lbl_source_inactive.Name = "lbl_source_inactive";
            this.lbl_source_inactive.Size = new System.Drawing.Size(0, 13);
            this.lbl_source_inactive.TabIndex = 9;
            this.lbl_source_inactive.UseMnemonic = false;
            this.lbl_source_inactive.Visible = false;
            // 
            // lbl_new_source_trust
            // 
            this.lbl_new_source_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_new_source_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_new_source_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_new_source_trust.Location = new System.Drawing.Point(360, 78);
            this.lbl_new_source_trust.Name = "lbl_new_source_trust";
            this.lbl_new_source_trust.Size = new System.Drawing.Size(71, 13);
            this.lbl_new_source_trust.TabIndex = 8;
            this.lbl_new_source_trust.Text = "$0.00";
            this.lbl_new_source_trust.UseMnemonic = false;
            // 
            // txc_amount
            // 
            this.txc_amount.AllowDrop = true;
            this.txc_amount.EnterMoveNextControl = true;
            this.txc_amount.Location = new System.Drawing.Point(360, 53);
            this.txc_amount.Name = "txc_amount";
            this.txc_amount.Properties.Appearance.Options.UseTextOptions = true;
            this.txc_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txc_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txc_amount.Properties.DisplayFormat.FormatString = "C";
            this.txc_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Properties.EditFormat.FormatString = "f0";
            this.txc_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Size = new System.Drawing.Size(88, 20);
            this.txc_amount.TabIndex = 6;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(256, 56);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(67, 13);
            this.Label4.TabIndex = 5;
            this.Label4.Text = "Less transfer:";
            this.Label4.UseMnemonic = false;
            // 
            // lbl_old_source_trust
            // 
            this.lbl_old_source_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_old_source_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_old_source_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_old_source_trust.Location = new System.Drawing.Point(360, 26);
            this.lbl_old_source_trust.Name = "lbl_old_source_trust";
            this.lbl_old_source_trust.Size = new System.Drawing.Size(71, 13);
            this.lbl_old_source_trust.TabIndex = 4;
            this.lbl_old_source_trust.Text = "$0.00";
            this.lbl_old_source_trust.UseMnemonic = false;
            // 
            // med_SourceName
            // 
            this.med_SourceName.EditValue = "";
            this.med_SourceName.Location = new System.Drawing.Point(16, 52);
            this.med_SourceName.Name = "med_SourceName";
            this.med_SourceName.Properties.AcceptsReturn = false;
            this.med_SourceName.Properties.AllowFocused = false;
            this.med_SourceName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.med_SourceName.Properties.LookAndFeel.SkinName = "Coffee";
            this.med_SourceName.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.med_SourceName.Properties.ReadOnly = true;
            this.med_SourceName.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.med_SourceName.Size = new System.Drawing.Size(232, 51);
            this.med_SourceName.TabIndex = 2;
            this.med_SourceName.TabStop = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(16, 26);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(67, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Source Client:";
            // 
            // Form_Transfer_ClientClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 332);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Transfer);
            this.Controls.Add(this.cbo_Reason);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.GroupBox1);
            this.Name = "Form_Transfer_ClientClient";
            this.Text = "Client to Client Transfer";
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txb_dest_client.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_TargetName.Properties)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txb_source_client.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_SourceName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_Transfer;
        internal DevExpress.XtraEditors.ComboBoxEdit cbo_Reason;
        internal DevExpress.XtraEditors.LabelControl Label14;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal DebtPlus.UI.Client.Widgets.Controls.ClientID txb_dest_client;
        internal DevExpress.XtraEditors.LabelControl Label11;
        internal DebtPlus.Data.Controls.BlinkLabel lbl_dest_inactive;
        internal DevExpress.XtraEditors.LabelControl lbl_new_dest_trust;
        internal DevExpress.XtraEditors.LabelControl Label8;
        internal DevExpress.XtraEditors.LabelControl Label9;
        internal DevExpress.XtraEditors.LabelControl lbl_old_dest_trust;
        internal DevExpress.XtraEditors.MemoEdit med_TargetName;
        internal DevExpress.XtraEditors.LabelControl Label12;
        internal DevExpress.XtraEditors.LabelControl lbl_dest_amount;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal DebtPlus.UI.Client.Widgets.Controls.ClientID txb_source_client;
        internal DevExpress.XtraEditors.LabelControl Label6;
        internal DevExpress.XtraEditors.LabelControl Label2;
        internal DebtPlus.Data.Controls.BlinkLabel lbl_source_inactive;
        internal DevExpress.XtraEditors.LabelControl lbl_new_source_trust;
        internal DevExpress.XtraEditors.CalcEdit txc_amount;
        internal DevExpress.XtraEditors.LabelControl Label4;
        internal DevExpress.XtraEditors.LabelControl lbl_old_source_trust;
        internal DevExpress.XtraEditors.MemoEdit med_SourceName;
        internal DevExpress.XtraEditors.LabelControl Label1;
    }
}