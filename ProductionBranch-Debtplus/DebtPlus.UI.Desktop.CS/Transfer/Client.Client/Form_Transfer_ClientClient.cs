#region Copyright 2000-2012 DebtPlus, L.L.C.;
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Linq;
using DebtPlus.LINQ;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.Data;

namespace DebtPlus.UI.Desktop.CS.Transfer.Client.Client
{
    public partial class Form_Transfer_ClientClient : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_Transfer_ClientClient() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += new EventHandler(Form_Transfer_ClientClient_Load);
            cbo_Reason.EditValueChanged += new EventHandler(cbo_Reason_EditValueChanged);

            txb_source_client.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            txb_dest_client.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            txc_amount.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            cbo_Reason.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            med_TargetName.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            med_SourceName.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            lbl_old_source_trust.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            lbl_old_dest_trust.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            lbl_new_dest_trust.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            txb_source_client.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            txb_dest_client.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            txc_amount.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            cbo_Reason.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            med_TargetName.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            med_SourceName.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            lbl_old_source_trust.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            lbl_old_dest_trust.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            lbl_new_dest_trust.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            txb_source_client.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            txb_dest_client.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            txc_amount.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            cbo_Reason.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            med_TargetName.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            med_SourceName.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_old_source_trust.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_old_dest_trust.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_new_dest_trust.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            txb_source_client.DragDrop += new DragEventHandler(drag_drop_DragDrop);
            txb_dest_client.DragDrop += new DragEventHandler(drag_drop_DragDrop);
            txc_amount.DragDrop += new DragEventHandler(drag_drop_DragDrop);
            cbo_Reason.DragDrop += new DragEventHandler(drag_drop_DragDrop);

            Button_Cancel.Click += new EventHandler(Button_Cancel_Click);
            Button_Transfer.Click += new EventHandler(Button_Transfer_Click);

            txb_source_client.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txb_source_client_EditValueChanging);
            txb_source_client.Validated += new EventHandler(txb_source_client_Validated);

            txb_dest_client.Validated += new EventHandler(txb_dest_client_Validated);
            txb_dest_client.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txb_dest_client_EditValueChanging);

            txc_amount.Validated += new EventHandler(txc_amount_Validated);
            txc_amount.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(txc_amount_EditValueChanging);
        }

        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(Form_Transfer_ClientClient_Load);
            cbo_Reason.EditValueChanged -= new EventHandler(cbo_Reason_EditValueChanged);

            txb_source_client.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            txb_dest_client.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            txc_amount.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            cbo_Reason.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            med_TargetName.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            med_SourceName.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            lbl_old_source_trust.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            lbl_old_dest_trust.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            lbl_new_dest_trust.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            txb_source_client.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            txb_dest_client.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            txc_amount.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            cbo_Reason.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            med_TargetName.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            med_SourceName.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            lbl_old_source_trust.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            lbl_old_dest_trust.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            lbl_new_dest_trust.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            txb_source_client.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            txb_dest_client.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            txc_amount.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            cbo_Reason.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            med_TargetName.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            med_SourceName.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_old_source_trust.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_old_dest_trust.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_new_dest_trust.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            txb_source_client.DragDrop -= new DragEventHandler(drag_drop_DragDrop);
            txb_dest_client.DragDrop -= new DragEventHandler(drag_drop_DragDrop);
            txc_amount.DragDrop -= new DragEventHandler(drag_drop_DragDrop);
            cbo_Reason.DragDrop -= new DragEventHandler(drag_drop_DragDrop);

            Button_Cancel.Click -= new EventHandler(Button_Cancel_Click);
            Button_Transfer.Click -= new EventHandler(Button_Transfer_Click);

            txb_source_client.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(txb_source_client_EditValueChanging);
            txb_source_client.Validated -= new EventHandler(txb_source_client_Validated);

            txb_dest_client.Validated -= new EventHandler(txb_dest_client_Validated);
            txb_dest_client.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(txb_dest_client_EditValueChanging);

            txc_amount.Validated -= new EventHandler(txc_amount_Validated);
            txc_amount.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(txc_amount_EditValueChanging);
        }

        private void Form_Transfer_ClientClient_Load(object sender, EventArgs e) // Handles Me.Load
        {
            UnRegisterHandlers();
            try
            {
                Load_Transfer_Reasons("CC");
                clear_form();
            }

            finally
            {
                RegisterHandlers();
            }
        }

        private void clear_form()
        {

            // Clear the transfer information
            txb_source_client.EditValue = null;
            txb_dest_client.EditValue = null;
            txc_amount.EditValue = null;

            // Clear the inactive warning labels
            lbl_dest_inactive.Text = string.Empty;
            lbl_source_inactive.Text = string.Empty;

            // Clear the dollar amounts
            old_source_trust = 0M;
            new_source_trust = 0M;

            old_dest_trust = 0M;
            dest_amount = 0M;
            new_dest_trust = 0M;

            // Set the error conditions for the values that are missing
            validate_source();
            validate_destination();
            validate_amount();
            validate_reason();

            // Disable the transfer button if there are errors
            Button_Transfer.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (DxErrorProvider1.GetError(txb_source_client) != string.Empty)
            {
                return true;
            }

            if (DxErrorProvider1.GetError(txb_dest_client) != string.Empty)
            {
                return true;
            }

            // Ensure that the client numbers are provided
            if (src_client == dest_client)
            {
                DxErrorProvider1.SetError(txb_dest_client, Strings.same_client);
                return true;
            }

            // After checking for duplicate client numbers, look at the other fields.
            if (DxErrorProvider1.GetError(txc_amount) != string.Empty)
            {
                return true;
            }

            if (DxErrorProvider1.GetError(cbo_Reason) != string.Empty)
            {
                return true;
            }

            // If the client has enough in the trust then accept the form
            if (new_source_trust < 0M)
            {
                return true;
            }

            // All is well
            return false;
        }

        private decimal src_amount
        {
            get
            {
                if( txc_amount.Text == string.Empty )
                {
                    return 0M;
                }
                if( txc_amount.EditValue == null )
                {
                    return 0M;
                }
                return Convert.ToDecimal(txc_amount.EditValue);
            }
            set
            {
                txc_amount.EditValue = value;
            }
        }

        private Int32 src_client
        {
            get
            {
                return txb_source_client.EditValue.GetValueOrDefault(-1);
            }
        }

        private decimal _new_source_trust = 0M;
        private decimal new_source_trust
        {
            get
            {
                return _new_source_trust;
            }
            set
            {
                _new_source_trust = value;
                lbl_new_source_trust.Text = string.Format("{0:c}", value);
                if( value < 0M )
                {
                    lbl_new_source_trust.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lbl_new_source_trust.ForeColor = System.Drawing.Color.Black;
                }
            }
        }

        private decimal _old_source_trust = 0M;
        private decimal old_source_trust
        {
            get
            {
                return _old_source_trust;
            }
            set
            {
                _old_source_trust = value;
                lbl_old_source_trust.Text = string.Format("{0:c}", value);
                if( value < 0M )
                {
                    lbl_old_source_trust.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lbl_old_source_trust.ForeColor = System.Drawing.Color.Black;
                }
            }
        }

        private decimal _dest_amount = 0M;
        private decimal dest_amount
        {
            get
            {
                return _dest_amount;
            }
            set
            {
                _dest_amount = value;
                lbl_dest_amount.Text = string.Format("{0:c}", value);
                if( value < 0M )
                {
                    lbl_dest_amount.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lbl_dest_amount.ForeColor = System.Drawing.Color.Black;
                }
            }
        }

        private decimal _new_dest_trust = 0M;
        private decimal new_dest_trust
        {
            get
            {
                return _new_dest_trust;
            }
            set
            {
                _new_dest_trust = value;
                lbl_new_dest_trust.Text = string.Format("{0:c}", value);
                if( value < 0M )
                {
                    lbl_new_dest_trust.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lbl_new_dest_trust.ForeColor = System.Drawing.Color.Black;
                }
            }
        }

        private decimal _old_dest_trust = 0M;
        private decimal old_dest_trust
        {
            get
            {
                if (src_client == dest_client)
                {
                    return new_source_trust;
                }
                else
                {
                    return _old_dest_trust;
                }
            }

            set
            {
                _old_dest_trust = value;

                lbl_old_dest_trust.Text = string.Format("{0:c}", old_dest_trust);
                if( old_dest_trust < 0M )
                {
                    lbl_old_dest_trust.ForeColor = System.Drawing.Color.Red;
                }
                else
                {
                    lbl_old_dest_trust.ForeColor = System.Drawing.Color.Black;
                }
            }
        }

        private void txb_source_client_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e) // Handles txb_source_client.EditValueChanging
        {
            med_SourceName.Text = string.Empty;
        }

        private void cbo_Reason_EditValueChanged(object sender, EventArgs e) // Handles cbo_Reason.EditValueChanged
        {
            validate_reason();
        }

        private void validate_reason()
        {
            string error_message = string.Empty;

            // Ensure that there is a value for the information
            if (cbo_Reason.Text.Trim().Length == 0)
            {
                error_message = Strings.required_value;
            }

            // Set the error text and enable the transfer button if there is no error
            DxErrorProvider1.SetError(cbo_Reason, error_message);
            Button_Transfer.Enabled = ! HasErrors();
        }

        private void txc_amount_Validated(object sender, EventArgs e) // Handles txc_amount.Validated
        {
            validate_amount();
        }

        private void validate_amount()
        {
            string error_message = string.Empty;

            // If the amount is negative then reject the transfer
            if (txc_amount.Text.Trim() == string.Empty)
            {
                error_message = Strings.required_value;
            }
            else if (src_amount <= 0M)
            {
                error_message = Strings.must_be_posative;
            }

            // Update the totals accordingly
            dest_amount = src_amount;
            new_source_trust = old_source_trust - dest_amount;
            new_dest_trust = old_dest_trust + dest_amount;

            // If the new trust is negative then reject the update
            if (DxErrorProvider1.GetError(txc_amount) == string.Empty && new_source_trust < 0M)
            {
                error_message = Strings.insufficient_funds;
            }

            // Set the error text
            DxErrorProvider1.SetError(txc_amount, error_message);
            Button_Transfer.Enabled = !HasErrors();
        }

        private void txb_source_client_Validated(object sender, EventArgs e) // Handles txb_source_client.Validated
        {
            validate_source();
        }

        private void validate_source()
        {
            string error_message = string.Empty;
            lbl_source_inactive.Text = string.Empty;

            // There must be a client
            if (txb_source_client.Text == string.Empty)
            {
                error_message = Strings.required_value;
            }
            else
            {
                // Find the client. If the client is invalid then reject the operation
                if( src_client <= 0 )
                {
                    error_message = Strings.invalid_client;
                }
                else
                {
                    string active_status;
                    string temp_string;
                    decimal temp_decimal;

                    // Read the client information from the database
                    if( ! read_client(src_client, out active_status, out temp_string, out temp_decimal) )
                    {
                        error_message = Strings.invalid_client;
                    }
                    else
                    {
                        med_SourceName.Text = temp_string;
                        old_source_trust = temp_decimal;

                        // If the client is not active then indicate as such
                        switch( active_status )
                        {
                            case "A":
                            case "AR":
                                break;

                            default:
                                lbl_source_inactive.Text = "INACTIVE CLIENT";
                                break;
                        }
                    }
                }
            }

            // Update the amount fields
            validate_amount();

            // Set the error message text
            DxErrorProvider1.SetError(txb_source_client, error_message);
            Button_Transfer.Enabled = ! HasErrors();
        }

        private Int32 dest_client
        {
            get
            {
                return txb_dest_client.EditValue.GetValueOrDefault(-1);
            }
        }

        private void txb_dest_client_Validated(object sender, EventArgs e) // Handles txb_dest_client.Validated
        {
            validate_destination();
        }

        private void validate_destination()
        {
            string error_message = string.Empty;
            lbl_dest_inactive.Text = string.Empty;

            // There must be a client
            if (txb_dest_client.Text == string.Empty)
            {
                error_message = Strings.required_value;
            }
            else
            {

                // Find the client. If the client is invalid then reject the operation
                if( dest_client <= 0 )
                {
                    error_message = Strings.invalid_client;
                }
                else
                {
                    string active_status;
                    string temp_string;
                    decimal temp_decimal;

                    // Read the client information from the database
                    if( ! read_client(dest_client, out active_status, out temp_string, out temp_decimal) )
                    {
                        error_message = Strings.invalid_client;
                    }
                    else
                    {
                        med_TargetName.Text = temp_string;
                        old_dest_trust = temp_decimal;

                        // If the client is not active then indicate as such
                        switch( active_status )
                        {
                            case "A":
                            case "AR":
                                break;

                            default:
                                lbl_dest_inactive.Text = "INACTIVE CLIENT";
                                break;
                        }
                    }
                }
            }

            // Update the amount fields
            validate_amount();

            // Set the error message text
            DxErrorProvider1.SetError(txb_dest_client, error_message);
            Button_Transfer.Enabled = ! HasErrors();
        }

        private void txb_dest_client_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e) // Handles txb_dest_client.EditValueChanging
        {
            med_TargetName.Text = string.Empty;
        }

        private void drag_drop_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e) // Handles lbl_new_dest_trust.MouseMove, cbo_Reason.MouseMove, lbl_new_dest_trust.MouseMove, lbl_new_source_trust.MouseMove, lbl_old_dest_trust.MouseMove, lbl_old_source_trust.MouseMove, med_SourceName.MouseMove, med_TargetName.MouseMove, txc_amount.MouseMove, txb_dest_client.MouseMove, txb_source_client.MouseMove
        {
            System.Windows.Forms.Control ctl = (System.Windows.Forms.Control) sender;

            // Do nothing if the operation is not valid
            if( e.Button == 0 )
            {
                return;
            }

            if( e.X >= 0 && e.X < ctl.Width && e.Y >= 0 && e.Y < ctl.Height )
            {
                return;
            }

            string txt = string.Empty;

            // If this is a text box then look for the selected text component of the text field
            if( sender is DevExpress.XtraEditors.TextEdit )
            {
                DevExpress.XtraEditors.TextEdit txe = (DevExpress.XtraEditors.TextEdit) sender;
                if( txe.SelectionLength > 0 )
                {
                    txt = txe.SelectedText;
                }
            }

            // If the text is missing then try the entire text field
            if( txt == string.Empty )
            {
                txt = ctl.Text.Trim();
            }

            // If there is no text then do nothing
            if( txt == string.Empty )
            {
                return;
            }

            // Start a drag-drop operation when the mouse moves outside the control
            System.Windows.Forms.DataObject dobj = new System.Windows.Forms.DataObject();
            dobj.SetData(DataFormats.Text, true, txt);

            // Do the operation. It will return when the operation is complete.
            DragDropEffects effect = DragDropEffects.Copy;
            effect = ctl.DoDragDrop(dobj, effect);

            // If the operation result was move then erase the text.
            // However, since we don't do the move then nothing more is needed.
        }

        private void drag_drop_DragEnter(object sender, System.Windows.Forms.DragEventArgs e) // Handles txc_amount.DragEnter, cbo_Reason.DragEnter, txb_dest_client.DragEnter, txb_source_client.DragEnter
        {
            // If there is a text field then look to determine the accepable processing
            if (e.Data.GetDataPresent(DataFormats.Text, true))
            {
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void drag_drop_DragDrop(object sender, System.Windows.Forms.DragEventArgs e) // Handles txc_amount.DragDrop, cbo_Reason.DragDrop, txb_dest_client.DragDrop, txb_source_client.DragDrop
        {
            // If there is a text field then look to determine the accepable processing
            if( ! e.Data.GetDataPresent(DataFormats.Text, true) )
            {
                return;
            }

            // We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect & DragDropEffects.Copy;

            // Paste the text into the control
            ((System.Windows.Forms.Control)sender).Text = Convert.ToString(e.Data.GetData(DataFormats.Text, true));

            if( sender == txb_source_client )
            {
                validate_source();
            }
            
            else if( sender == txb_dest_client )
            {
                validate_destination();
            }
            
            else if( sender == txc_amount )
            {
                txc_amount_Validated(txc_amount, new EventArgs());
            }
        }

        private void drag_drop_QueryContinueDrag(object sender, System.Windows.Forms.QueryContinueDragEventArgs e) // Handles lbl_dest_amount.QueryContinueDrag, cbo_Reason.QueryContinueDrag, lbl_new_dest_trust.QueryContinueDrag, lbl_new_dest_trust.QueryContinueDrag, lbl_new_source_trust.QueryContinueDrag, lbl_old_dest_trust.QueryContinueDrag, lbl_old_source_trust.QueryContinueDrag, med_SourceName.QueryContinueDrag, med_TargetName.QueryContinueDrag, txc_amount.QueryContinueDrag, txb_dest_client.QueryContinueDrag, txb_source_client.QueryContinueDrag
        {
            // If the escape key is pressed then cancel the operation
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e) // Handles Button_Cancel.Click
        {
            Close();
        }

        private void txc_amount_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e) // Handles txc_amount.EditValueChanging
        {
            dest_amount = src_amount;
            new_source_trust = old_source_trust - dest_amount;
            new_dest_trust = old_dest_trust + dest_amount;
        }

        private void Button_Transfer_Click(object sender, EventArgs e) // Handles Button_Transfer.Click
        {
            string errorMessage = string.Empty;

            try
            {
                DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var txn = cn.BeginTransaction(IsolationLevel.RepeatableRead))
                    {
                        using (var bc = new DebtPlus.LINQ.BusinessContext(cn))
                        {
                            bc.Transaction = txn;

                            // Retrieve the current trust balance
                            var srcClient = (from c in bc.clients where c.Id == src_client select c).Single();
                            if (srcClient.held_in_trust < dest_amount)
                            {
                                errorMessage = Strings.insufficient_funds;
                            }
                            else
                            {
                                srcClient.held_in_trust -= dest_amount;

                                var destClient = (from c in bc.clients where c.Id == dest_client select c).Single();
                                destClient.held_in_trust += dest_amount;

                                // Generate the two transactions for the transfer
                                var srcTransaction = new DebtPlus.LINQ.registers_client()
                                {
                                    client = srcClient.Id,
                                    debit_amt = dest_amount,
                                    tran_type = "CW",
                                    message = cbo_Reason.Text.Trim()
                                };
                                bc.registers_clients.InsertOnSubmit(srcTransaction);

                                var destTransaction = new DebtPlus.LINQ.registers_client()
                                {
                                    client = destClient.Id,
                                    credit_amt = dest_amount,
                                    tran_type = "CD",
                                    message = cbo_Reason.Text.Trim()
                                };
                                bc.registers_clients.InsertOnSubmit(destTransaction);

                                // Submit the changes once we have made the items correctly
                                bc.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                                // Commit the transaction once we have completed the request
                                txn.Commit();

                                // Complete the operation by clearing the data
                                clear_form();

                                // Go to the source client for the next field
                                txb_source_client.Focus();
                                return;
                            }
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                errorMessage = ex.Message;
            }

#pragma warning disable 168
            catch (System.Data.Linq.ChangeConflictException ex)
            {
                errorMessage = "The transaction has been canceled. Please re-submit it by clicking Transfer again.";
            }
#pragma warning restore 168

            // Things are not correct. Generate the error condition
            DebtPlus.Data.Forms.MessageBox.Show(errorMessage, "Error in transfer", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        private void Load_Transfer_Reasons(string ItemType)
        {
            System.Collections.Generic.List<DebtPlus.LINQ.message> col = DebtPlus.LINQ.Cache.message.getList().FindAll(s => s.item_type == ItemType);
            cbo_Reason.Properties.Items.Clear();
            foreach (DebtPlus.LINQ.message msg in col)
            {
                var NewItem = cbo_Reason.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(msg.description, msg.Id, msg.ActiveFlag));
                if (msg.Default)
                {
                    cbo_Reason.SelectedIndex = NewItem;
                }
            }
        }

        private bool read_client(Int32 client, out string active_status, out string client_name, out decimal trust_balance)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    var q = (from cy in bc.clients join vy in bc.view_client_addresses on cy.Id equals vy.client where cy.Id == client select new { cy.Id, cy.held_in_trust, cy.active_status, vy.name, vy.addr1, vy.addr2, vy.addr3 }).FirstOrDefault();
                    if (q == null)
                    {
                        active_status = string.Empty;
                        client_name = string.Empty;
                        trust_balance = 0m;
                        return false;
                    }

                    active_status = q.active_status;
                    trust_balance = q.held_in_trust;

                    var sb = new System.Text.StringBuilder();
                    foreach (string str in new string[] { q.name, q.addr1, q.addr2, q.addr3 })
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            sb.Append("\r\n");
                            sb.Append(str);
                        }
                    }

                    if (sb.Length > 0)
                    {
                        sb.Remove(0, 2);
                    }
                    client_name = sb.ToString();
                }
            }

            return true;
        }
    }
}