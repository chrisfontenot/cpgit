#region Copyright 2000-2012 DebtPlus, L.L.C.;
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.Data;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.Transfer.Client.Oper
{
    internal partial class Form_Transfer_ClientOper : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_Transfer_ClientOper() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load += new EventHandler(Form_Transfer_ClientOper_Load);
            LookUpEdit_DestAccount.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(DebtPlus.Data.Validation.LookUpEdit_ActiveTest);
            LookUpEdit_DestAccount.EditValueChanged += new EventHandler(LookUpEdit_DestAccount_EditValueChanged);
            txc_amount.Validated += new EventHandler(txc_amount_Validated);
            cbo_Reason.DragDrop += new DragEventHandler(drag_drop_DragDrop);
            cbo_Reason.DragEnter += new DragEventHandler(drag_drop_DragEnter);
            cbo_Reason.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_new_source_trust.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            cbo_Reason.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            lbl_new_source_trust.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            lbl_old_source_trust.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            lbl_new_source_trust.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_old_source_trust.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            med_SourceName.MouseMove += new MouseEventHandler(drag_drop_MouseMove);
            med_SourceName.QueryContinueDrag += new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            ClientID1.Validated += new EventHandler(ClientID1_Validated);
            ClientID1.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(ClientID1_EditValueChanging);
            Button_Cancel.Click += new EventHandler(Button_Cancel_Click);
            Button_Transfer.Click += new EventHandler(Button_Transfer_Click);
        }

        private void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(Form_Transfer_ClientOper_Load);
            LookUpEdit_DestAccount.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(DebtPlus.Data.Validation.LookUpEdit_ActiveTest);
            LookUpEdit_DestAccount.EditValueChanged -= new EventHandler(LookUpEdit_DestAccount_EditValueChanged);
            txc_amount.Validated -= new EventHandler(txc_amount_Validated);
            cbo_Reason.DragDrop -= new DragEventHandler(drag_drop_DragDrop);
            cbo_Reason.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            cbo_Reason.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_new_source_trust.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            cbo_Reason.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            lbl_new_source_trust.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            lbl_old_source_trust.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            lbl_new_source_trust.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            lbl_old_source_trust.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            med_SourceName.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            med_SourceName.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            ClientID1.Validated -= new EventHandler(ClientID1_Validated);
            ClientID1.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(ClientID1_EditValueChanging);
            Button_Cancel.Click -= new EventHandler(Button_Cancel_Click);
            Button_Transfer.Click -= new EventHandler(Button_Transfer_Click);
        }

        private void Form_Transfer_ClientOper_Load(object sender, EventArgs e) // Handles this.Load
        {
            UnRegisterHandlers();
            try
            {
                // Load the ledger code table list
                LookUpEdit_DestAccount.Properties.DataSource = DebtPlus.LINQ.Cache.ledger_code.getList();
                LookUpEdit_DestAccount.EditValue = DebtPlus.LINQ.Cache.ledger_code.getDefault();

                // Load the list of reasons
                Load_Transfer_Reasons("CO");
                clear_form();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void clear_form()
        {

            // Clear the transfer information
            ClientID1.EditValue = null;
            txc_amount.EditValue = null;

            // Clear the inactive warning labels
            lbl_source_inactive.Text = string.Empty;

            // Clear the dollar amounts
            old_source_trust = 0M;
            new_source_trust = 0M;
            dest_amount = 0M;
            old_dest_trust = 0M;
            new_dest_trust = 0M;

            // Set the error conditions for the values that are missing
            validate_destination();
            validate_source();
            validate_reason();
            validate_amount();

            // Disable the transfer button if there are errors
            Button_Transfer.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            bool answer = true;

            for (;;)
            {
                if (DxErrorProvider1.GetError(LookUpEdit_DestAccount) != string.Empty)
                {
                    break;
                }

                if (DxErrorProvider1.GetError(ClientID1) != string.Empty)
                {
                    break;
                }
                
                if (DxErrorProvider1.GetError(cbo_Reason) != string.Empty)
                {
                    break;
                }
                
                if (DxErrorProvider1.GetError(txc_amount) != string.Empty)
                {
                    break;
                }

                // All is well
                answer = false;
                break;
            }

            return answer;
        }

        private decimal old_dest_trust = 0M;
        private decimal new_dest_trust = 0M;
        private decimal _new_source_trust = 0M;

        private decimal new_source_trust
        {
            get
            {
                return _new_source_trust;
            }
            set
            {
                _new_source_trust = value;
                lbl_new_source_trust.Text = string.Format("{0:c}", value);
                if (value < 0M)
                {
                    lbl_new_source_trust.ForeColor = Color.Red;
                }
                else
                {
                    lbl_new_source_trust.ForeColor = Color.Black;
                }
            }
        }

        private decimal _old_source_trust = 0M;
        private decimal old_source_trust
        {
            get
            {
                return _old_source_trust;
            }
            set
            {
                _old_source_trust = value;
                lbl_old_source_trust.Text = string.Format("{0:c}", value);
                if( value < 0M )
                {
                    lbl_old_source_trust.ForeColor = Color.Red;
                }
                else
                {
                    lbl_old_source_trust.ForeColor = Color.Black;
                }
            }
        }

        private decimal _dest_amount = 0M;
        private decimal dest_amount
        {
            get
            {
                return _dest_amount;
            }

            set
            {
                _dest_amount = value;
                txc_amount.Text = string.Format("{0:c}", _dest_amount);
            }
        }

        private decimal src_amount
        {
            get
            {
                return DebtPlus.Utils.Nulls.DDec(txc_amount.EditValue);
            }
            set
            {
                txc_amount.EditValue = value;
            }
        }

        private string reason
        {
            get
            {
                return cbo_Reason.Text.Trim();
            }
        }

        private void validate_reason()
        {
            string error_message;

            // Ensure that there is a value for the information
            if (reason == string.Empty)
            {
                error_message = Strings.required_value;
            }
            else
            {
                error_message = string.Empty;
            }

            // Set the error text and enable the transfer button if( there is no error
            DxErrorProvider1.SetError(cbo_Reason, error_message);
            Button_Transfer.Enabled = !HasErrors();
        }

        private void txc_amount_Validated(object sender, EventArgs e)
        {
            validate_amount();
        }

        private void validate_amount()
        {
            string error_message = string.Empty;

            // If the amount is negative then reject the transfer
            if (txc_amount.Text.Trim() == string.Empty)
            {
                error_message = Strings.required_value;
            }
            else if (src_amount <= 0M)
            {
                error_message = Strings.must_be_posative;
            }

            // Update the totals accordingly
            dest_amount = src_amount;
            new_source_trust = old_source_trust - dest_amount;
            new_dest_trust = old_dest_trust + dest_amount;

            // Set the error text
            DxErrorProvider1.SetError(txc_amount, error_message);
            Button_Transfer.Enabled = !HasErrors();
        }

        private void drag_drop_MouseMove(object sender, MouseEventArgs e)
        {
            Control ctl = (Control)sender;

            // Do nothing if the operation is not valid
            if (e.Button == 0)
            {
                return;
            }

            if (e.X >= 0 && e.X < ctl.Width && e.Y >= 0 && e.Y < ctl.Height)
            {
                return;
            }

            string txt = string.Empty;

            // If this is a text box then look for the selected text component of the text field
            if (sender is DevExpress.XtraEditors.TextEdit)
            {
                DevExpress.XtraEditors.TextEdit txe = (DevExpress.XtraEditors.TextEdit)sender;
                if (txe.SelectionLength > 0)
                {
                    txt = txe.SelectedText;
                }
            }

            // If the text is missing then try the entire text field
            if (txt == string.Empty)
            {
                txt = ctl.Text.Trim();
            }

            // If there is no text then do nothing
            if (txt == string.Empty)
            {
                return;
            }

            // Start a drag-drop operation when the mouse moves outside the control
            DataObject dobj = new DataObject();
            dobj.SetData(DataFormats.Text, true, txt);

            // Do the operation. It will return when the operation is complete.
            DragDropEffects effect = DragDropEffects.Copy;
            effect = ctl.DoDragDrop(dobj, effect);

            // If the operation result was move then erase the text.
            // However, since we don't do the move then nothing more is needed.
        }

        private void drag_drop_DragEnter(object sender, DragEventArgs e)
        {

            // If there is a text field then look to determine the accepable processing
            if (e.Data.GetDataPresent(DataFormats.Text, true))
            {
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void drag_drop_DragDrop(object sender, DragEventArgs e)
        {

            // If there is a text field then look to determine the acceptable processing
            if (!e.Data.GetDataPresent(DataFormats.Text, true))
            {
                return;
            }

            // We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect & DragDropEffects.Copy;

            // Paste the text into the control
            ((Control) sender).Text = Convert.ToString(e.Data.GetData(DataFormats.Text, true));
            if (sender == ClientID1)
            {
                validate_source();
            }

            else if (sender == txc_amount)
            {
                txc_amount_Validated(txc_amount, new EventArgs());
            }
        }

        private void drag_drop_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            // If the escape key is pressed then cancel the operation
            if (e.EscapePressed)
            {
                e.Action = DragAction.Cancel;
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LookUpEdit_DestAccount_EditValueChanged(object sender, EventArgs e)
        {
            validate_destination();
        }

        private void validate_destination()
        {
            string error_message;

            // The only requirement is that there be a value.
            if (dest_ledger_code == string.Empty)
            {
                error_message = Strings.required_value;
            }
            else
            {
                error_message = string.Empty;
            }

            // Set the error message text
            DxErrorProvider1.SetError(LookUpEdit_DestAccount, error_message);
            Button_Transfer.Enabled = !HasErrors();
        }

        private string dest_ledger_code
        {
            get
            {
                string answer;
                if (LookUpEdit_DestAccount.EditValue != null)
                {
                    answer = Convert.ToString(LookUpEdit_DestAccount.EditValue);
                }
                else
                {
                    answer = string.Empty;
                }
                return answer;
            }
        }

        private Int32 source_client
        {
            get
            {
                return ClientID1.EditValue.GetValueOrDefault(-1);
            }
        }

        private void ClientID1_Validated(object sender, EventArgs e)
        {
            validate_source();
        }

        private void validate_source()
        {
            string error_message = string.Empty;
            lbl_source_inactive.Text = string.Empty;

            // There must be a client
            if (ClientID1.Text == string.Empty)
            {
                error_message = Strings.required_value;
            }
            else
            {

                // Find the client. If the client is invalid then reject the operation
                if (source_client <= 0)
                {
                    error_message = Strings.invalid_client;
                }
                else
                {
                    string active_status = string.Empty;

                    // Read the client information from the database
                    decimal temp_decimal = 0M;
                    string temp_string = string.Empty;

                    if (!read_client(source_client, out active_status, out temp_string, out temp_decimal))
                    {
                        error_message = Strings.invalid_client;
                    }
                    else
                    {
                        old_source_trust = temp_decimal;
                        med_SourceName.Text = temp_string;

                        // If the client is not active then indicate as such
                        switch (active_status)
                        {
                            case "A":
                            case "AR":
                                break;

                            default:
                                lbl_source_inactive.Text = "INACTIVE CLIENT";
                                break;
                        }
                    }
                }
            }

            // Update the amount information for the trust balances
            validate_amount();

            // Set the error message text
            DxErrorProvider1.SetError(ClientID1, error_message);
            Button_Transfer.Enabled = !HasErrors();
        }

        private void ClientID1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            med_SourceName.Text = string.Empty;
        }

        private void Button_Transfer_Click(object sender, EventArgs e)
        {
            string errorMessage = string.Empty;

            try
            {
                // Make the transaction "repeatable read" to force the updates to be done all at once or not at all.
                var txn_op = new System.Transactions.TransactionOptions();
                txn_op.IsolationLevel = System.Transactions.IsolationLevel.RepeatableRead;

                using (var txn = new System.Transactions.TransactionScope(System.Transactions.TransactionScopeOption.RequiresNew, txn_op))
                {
                    using (var bc = new DebtPlus.LINQ.BusinessContext())
                    {
                        // Retrieve the current trust balance
                        var srcClient = (from c in bc.clients where c.Id == this.ClientID1.EditValue.Value select c).Single();
                        srcClient.held_in_trust -= dest_amount;

                        // Generate the two transactions for the transfer
                        var srcTransaction = new DebtPlus.LINQ.registers_client()
                        {
                            client = srcClient.Id,
                            debit_amt = dest_amount,
                            tran_type = "MF",
                            message = cbo_Reason.Text.Trim()
                        };
                        bc.registers_clients.InsertOnSubmit(srcTransaction);

                        var destTransaction = new DebtPlus.LINQ.registers_non_ar()
                        {
                            credit_amt = dest_amount,
                            client = srcClient.Id,
                            tran_type = "DM",
                            dst_ledger_account = dest_ledger_code,
                            message = cbo_Reason.Text.Trim()
                        };
                        bc.registers_non_ars.InsertOnSubmit(destTransaction);

                        // Submit the changes once we have made the items correctly
                        bc.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                        // Commit the transaction once we have completed the request
                        txn.Complete();
                    }
                }

                // Success. Reset the form
                clear_form();
                ClientID1.Focus();
                return;
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                errorMessage = ex.Message;
            }

#pragma warning disable 168
            catch (System.Data.Linq.ChangeConflictException ex)
            {
                errorMessage = "The transaction has been canceled. Please re-submit it by clicking Transfer again.";
            }
#pragma warning restore 168

            // Things are not correct. Generate the error condition
            DebtPlus.Data.Forms.MessageBox.Show(errorMessage, "Error in transfer", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        private void Load_Transfer_Reasons(string ItemType)
        {
            System.Collections.Generic.List<DebtPlus.LINQ.message> col = DebtPlus.LINQ.Cache.message.getList().FindAll(s => s.item_type == ItemType);
            cbo_Reason.Properties.Items.Clear();
            foreach (DebtPlus.LINQ.message msg in col)
            {
                var NewItem = cbo_Reason.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(msg.description, msg.Id, msg.ActiveFlag));
                if (msg.Default)
                {
                    cbo_Reason.SelectedIndex = NewItem;
                }
            }
        }

        private bool read_client(Int32 client, out string active_status, out string client_name, out decimal trust_balance)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    var q = (from cy in bc.clients join vy in bc.view_client_addresses on cy.Id equals vy.client where cy.Id == client select new { cy.Id, cy.held_in_trust, cy.active_status, vy.name, vy.addr1, vy.addr2, vy.addr3 }).FirstOrDefault();
                    if (q == null)
                    {
                        active_status = string.Empty;
                        client_name = string.Empty;
                        trust_balance = 0m;
                        return false;
                    }

                    active_status = q.active_status;
                    trust_balance = q.held_in_trust;

                    var sb = new System.Text.StringBuilder();
                    foreach (string str in new string[] { q.name, q.addr1, q.addr2, q.addr3 })
                    {
                        if (!string.IsNullOrEmpty(str))
                        {
                            sb.Append("\r\n");
                            sb.Append(str);
                        }
                    }

                    if (sb.Length > 0)
                    {
                        sb.Remove(0, 2);
                    }
                    client_name = sb.ToString();
                }
            }

            return true;
        }
    }
}