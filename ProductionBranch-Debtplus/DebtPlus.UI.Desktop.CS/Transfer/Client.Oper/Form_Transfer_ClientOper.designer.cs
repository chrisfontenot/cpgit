﻿namespace DebtPlus.UI.Desktop.CS.Transfer.Client.Oper
{
    partial class Form_Transfer_ClientOper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Transfer_ClientOper));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.GroupBox1 = new DevExpress.XtraEditors.GroupControl();
            this.LookUpEdit_DestAccount = new DevExpress.XtraEditors.LookUpEdit();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Transfer = new DevExpress.XtraEditors.SimpleButton();
            this.cbo_Reason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Label14 = new DevExpress.XtraEditors.LabelControl();
            this.GroupBox2 = new DevExpress.XtraEditors.GroupControl();
            this.ClientID1 = new DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.Label8 = new DevExpress.XtraEditors.LabelControl();
            this.Label11 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_source_inactive = new DebtPlus.Data.Controls.BlinkLabel();
            this.lbl_new_source_trust = new DevExpress.XtraEditors.LabelControl();
            this.lbl_old_source_trust = new DevExpress.XtraEditors.LabelControl();
            this.txc_amount = new DevExpress.XtraEditors.CalcEdit();
            this.Label9 = new DevExpress.XtraEditors.LabelControl();
            this.med_SourceName = new DevExpress.XtraEditors.MemoEdit();
            this.Label12 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox1)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_DestAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox2)).BeginInit();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientID1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_SourceName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // GroupBox1
            // 
            this.GroupBox1.AppearanceCaption.BackColor = System.Drawing.Color.DodgerBlue;
            this.GroupBox1.AppearanceCaption.Options.UseBackColor = true;
            this.GroupBox1.Controls.Add(this.LookUpEdit_DestAccount);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(3, 133);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(472, 60);
            this.GroupBox1.TabIndex = 7;
            this.GroupBox1.Text = " To the Operating Account  ";
            // 
            // LookUpEdit_DestAccount
            // 
            this.LookUpEdit_DestAccount.EnterMoveNextControl = true;
            this.LookUpEdit_DestAccount.Location = new System.Drawing.Point(128, 25);
            this.LookUpEdit_DestAccount.Name = "LookUpEdit_DestAccount";
            this.LookUpEdit_DestAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_DestAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ledger_code1", "Account", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LookUpEdit_DestAccount.Properties.DisplayMember = "description";
            this.LookUpEdit_DestAccount.Properties.NullText = "Please choose an account from this list...";
            this.LookUpEdit_DestAccount.Properties.ShowFooter = false;
            this.LookUpEdit_DestAccount.Properties.SortColumnIndex = 2;
            this.LookUpEdit_DestAccount.Properties.ValidateOnEnterKey = true;
            this.LookUpEdit_DestAccount.Properties.ValueMember = "ledger_code1";
            this.LookUpEdit_DestAccount.Size = new System.Drawing.Size(320, 20);
            this.LookUpEdit_DestAccount.TabIndex = 1;
            this.LookUpEdit_DestAccount.ToolTip = "Destination Non-AR ledger account for the transfer";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(16, 25);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(43, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Account:";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.CausesValidation = false;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(258, 297);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 24);
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Quit";
            // 
            // Button_Transfer
            // 
            this.Button_Transfer.Enabled = false;
            this.Button_Transfer.Location = new System.Drawing.Point(146, 297);
            this.Button_Transfer.Name = "Button_Transfer";
            this.Button_Transfer.Size = new System.Drawing.Size(75, 24);
            this.Button_Transfer.TabIndex = 10;
            this.Button_Transfer.Text = "Transfer";
            // 
            // cbo_Reason
            // 
            this.cbo_Reason.AllowDrop = true;
            this.cbo_Reason.EnterMoveNextControl = true;
            this.cbo_Reason.Location = new System.Drawing.Point(131, 252);
            this.cbo_Reason.Name = "cbo_Reason";
            this.cbo_Reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo_Reason.Properties.MaxLength = 50;
            this.cbo_Reason.Properties.NullText = "Please choose a reason from this list...";
            this.cbo_Reason.Properties.ValidateOnEnterKey = true;
            this.cbo_Reason.Size = new System.Drawing.Size(320, 20);
            this.cbo_Reason.TabIndex = 9;
            this.cbo_Reason.ToolTip = "Enter or choose the reason for the transfer.";
            // 
            // Label14
            // 
            this.Label14.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.Label14.Location = new System.Drawing.Point(3, 255);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(114, 13);
            this.Label14.TabIndex = 8;
            this.Label14.Text = "Reason for the transfer";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.ClientID1);
            this.GroupBox2.Controls.Add(this.Label8);
            this.GroupBox2.Controls.Add(this.Label11);
            this.GroupBox2.Controls.Add(this.lbl_source_inactive);
            this.GroupBox2.Controls.Add(this.lbl_new_source_trust);
            this.GroupBox2.Controls.Add(this.lbl_old_source_trust);
            this.GroupBox2.Controls.Add(this.txc_amount);
            this.GroupBox2.Controls.Add(this.Label9);
            this.GroupBox2.Controls.Add(this.med_SourceName);
            this.GroupBox2.Controls.Add(this.Label12);
            this.GroupBox2.Location = new System.Drawing.Point(3, 12);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(472, 112);
            this.GroupBox2.TabIndex = 6;
            this.GroupBox2.Text = " Transfer from the Client ";
            // 
            // ClientID1
            // 
            this.ClientID1.AllowDrop = true;
            this.ClientID1.EnterMoveNextControl = true;
            this.ClientID1.Location = new System.Drawing.Point(128, 26);
            this.ClientID1.Name = "ClientID1";
            this.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.ClientID1.Properties.Appearance.Options.UseTextOptions = true;
            this.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ClientID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("ClientID1.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to search for a client ID", "search", null, true)});
            this.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.ClientID1.Properties.DisplayFormat.FormatString = "0000000";
            this.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.ClientID1.Properties.EditFormat.FormatString = "f0";
            this.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.ClientID1.Properties.Mask.BeepOnError = true;
            this.ClientID1.Properties.Mask.EditMask = "\\d*";
            this.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.ClientID1.Properties.ValidateOnEnterKey = true;
            this.ClientID1.Size = new System.Drawing.Size(88, 20);
            this.ClientID1.TabIndex = 2;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(256, 78);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(81, 13);
            this.Label8.TabIndex = 8;
            this.Label8.Text = "New trust Balace";
            this.Label8.UseMnemonic = false;
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(256, 26);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(104, 13);
            this.Label11.TabIndex = 3;
            this.Label11.Text = "Original Trust Balance";
            this.Label11.UseMnemonic = false;
            // 
            // lbl_source_inactive
            // 
            this.lbl_source_inactive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_source_inactive.Appearance.BackColor = System.Drawing.Color.Red;
            this.lbl_source_inactive.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_source_inactive.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_source_inactive.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_source_inactive.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_source_inactive.BlinkTime = 500;
            this.lbl_source_inactive.CausesValidation = false;
            this.lbl_source_inactive.Location = new System.Drawing.Point(314, 0);
            this.lbl_source_inactive.Name = "lbl_source_inactive";
            this.lbl_source_inactive.Size = new System.Drawing.Size(158, 20);
            this.lbl_source_inactive.TabIndex = 0;
            this.lbl_source_inactive.UseMnemonic = false;
            this.lbl_source_inactive.Visible = false;
            // 
            // lbl_new_source_trust
            // 
            this.lbl_new_source_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_new_source_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_new_source_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_new_source_trust.Location = new System.Drawing.Point(368, 78);
            this.lbl_new_source_trust.Name = "lbl_new_source_trust";
            this.lbl_new_source_trust.Size = new System.Drawing.Size(62, 13);
            this.lbl_new_source_trust.TabIndex = 9;
            this.lbl_new_source_trust.Text = "$0.00";
            this.lbl_new_source_trust.UseMnemonic = false;
            // 
            // lbl_old_source_trust
            // 
            this.lbl_old_source_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_old_source_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_old_source_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_old_source_trust.Location = new System.Drawing.Point(368, 26);
            this.lbl_old_source_trust.Name = "lbl_old_source_trust";
            this.lbl_old_source_trust.Size = new System.Drawing.Size(62, 13);
            this.lbl_old_source_trust.TabIndex = 4;
            this.lbl_old_source_trust.Text = "$0.00";
            this.lbl_old_source_trust.UseMnemonic = false;
            // 
            // txc_amount
            // 
            this.txc_amount.AllowDrop = true;
            this.txc_amount.EnterMoveNextControl = true;
            this.txc_amount.Location = new System.Drawing.Point(368, 52);
            this.txc_amount.Name = "txc_amount";
            this.txc_amount.Properties.Appearance.Options.UseTextOptions = true;
            this.txc_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txc_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txc_amount.Properties.DisplayFormat.FormatString = "c2";
            this.txc_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Properties.EditFormat.FormatString = "c2";
            this.txc_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Properties.Mask.BeepOnError = true;
            this.txc_amount.Properties.Mask.EditMask = "c";
            this.txc_amount.Properties.Precision = 2;
            this.txc_amount.Properties.ValidateOnEnterKey = true;
            this.txc_amount.Size = new System.Drawing.Size(80, 20);
            this.txc_amount.TabIndex = 7;
            this.txc_amount.ToolTip = "Amount to be transferred";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(256, 52);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(67, 13);
            this.Label9.TabIndex = 6;
            this.Label9.Text = "Less transfer:";
            this.Label9.UseMnemonic = false;
            // 
            // med_SourceName
            // 
            this.med_SourceName.EditValue = "";
            this.med_SourceName.Location = new System.Drawing.Point(16, 52);
            this.med_SourceName.Name = "med_SourceName";
            this.med_SourceName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.med_SourceName.Properties.ReadOnly = true;
            this.med_SourceName.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.med_SourceName.Size = new System.Drawing.Size(232, 51);
            this.med_SourceName.TabIndex = 5;
            this.med_SourceName.TabStop = false;
            this.med_SourceName.ToolTip = "Client name and address (display only)";
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(16, 26);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(67, 13);
            this.Label12.TabIndex = 1;
            this.Label12.Text = "Source Client:";
            // 
            // Form_Transfer_ClientOper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 332);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Transfer);
            this.Controls.Add(this.cbo_Reason);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.GroupBox2);
            this.Name = "Form_Transfer_ClientOper";
            this.Text = "Client to Operating Account Transfer";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox1)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_DestAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox2)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ClientID1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_SourceName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.GroupControl GroupBox1;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_DestAccount;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_Transfer;
        internal DevExpress.XtraEditors.ComboBoxEdit cbo_Reason;
        internal DevExpress.XtraEditors.LabelControl Label14;
        internal DevExpress.XtraEditors.GroupControl GroupBox2;
        internal DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID1;
        internal DevExpress.XtraEditors.LabelControl Label8;
        internal DevExpress.XtraEditors.LabelControl Label11;
        internal DebtPlus.Data.Controls.BlinkLabel lbl_source_inactive;
        internal DevExpress.XtraEditors.LabelControl lbl_new_source_trust;
        internal DevExpress.XtraEditors.LabelControl lbl_old_source_trust;
        internal DevExpress.XtraEditors.CalcEdit txc_amount;
        internal DevExpress.XtraEditors.LabelControl Label9;
        internal DevExpress.XtraEditors.MemoEdit med_SourceName;
        internal DevExpress.XtraEditors.LabelControl Label12;

    }
}