﻿namespace DebtPlus.UI.Desktop.CS.Transfer.Oper.Oper
{
    partial class Form_Transfer_OperOper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.LookUpEdit_SourceAccount = new DevExpress.XtraEditors.LookUpEdit();
            this.txc_amount = new DevExpress.XtraEditors.CalcEdit();
            this.Label4 = new DevExpress.XtraEditors.LabelControl();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.LookUpEdit_DestAccount = new DevExpress.XtraEditors.LookUpEdit();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Transfer = new DevExpress.XtraEditors.SimpleButton();
            this.cbo_Reason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Label14 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_SourceAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).BeginInit();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_DestAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.LookUpEdit_SourceAccount);
            this.GroupBox1.Controls.Add(this.txc_amount);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(3, 12);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(472, 86);
            this.GroupBox1.TabIndex = 6;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = " Source Account ";
            // 
            // LookUpEdit_SourceAccount
            // 
            this.LookUpEdit_SourceAccount.EnterMoveNextControl = true;
            this.LookUpEdit_SourceAccount.Location = new System.Drawing.Point(128, 25);
            this.LookUpEdit_SourceAccount.Name = "LookUpEdit_SourceAccount";
            this.LookUpEdit_SourceAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_SourceAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ledger_code1", "Account", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LookUpEdit_SourceAccount.Properties.DisplayMember = "description";
            this.LookUpEdit_SourceAccount.Properties.NullText = "";
            this.LookUpEdit_SourceAccount.Properties.ShowFooter = false;
            this.LookUpEdit_SourceAccount.Properties.SortColumnIndex = 2;
            this.LookUpEdit_SourceAccount.Properties.ValueMember = "ledger_code1";
            this.LookUpEdit_SourceAccount.Size = new System.Drawing.Size(320, 20);
            this.LookUpEdit_SourceAccount.TabIndex = 1;
            // 
            // txc_amount
            // 
            this.txc_amount.AllowDrop = true;
            this.txc_amount.EnterMoveNextControl = true;
            this.txc_amount.Location = new System.Drawing.Point(128, 52);
            this.txc_amount.Name = "txc_amount";
            this.txc_amount.Properties.Appearance.Options.UseTextOptions = true;
            this.txc_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txc_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txc_amount.Properties.DisplayFormat.FormatString = "{0:c}";
            this.txc_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Properties.EditFormat.FormatString = "d2";
            this.txc_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Size = new System.Drawing.Size(80, 20);
            this.txc_amount.TabIndex = 3;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(16, 53);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(41, 13);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "Amount:";
            this.Label4.UseMnemonic = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(16, 25);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(43, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Account:";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.LookUpEdit_DestAccount);
            this.GroupBox2.Controls.Add(this.Label2);
            this.GroupBox2.Location = new System.Drawing.Point(3, 133);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(472, 60);
            this.GroupBox2.TabIndex = 7;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = " To the Operating Account  ";
            // 
            // LookUpEdit_DestAccount
            // 
            this.LookUpEdit_DestAccount.EnterMoveNextControl = true;
            this.LookUpEdit_DestAccount.Location = new System.Drawing.Point(128, 25);
            this.LookUpEdit_DestAccount.Name = "LookUpEdit_DestAccount";
            this.LookUpEdit_DestAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_DestAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ledger_code1", "Account", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.LookUpEdit_DestAccount.Properties.DisplayMember = "description";
            this.LookUpEdit_DestAccount.Properties.NullText = "";
            this.LookUpEdit_DestAccount.Properties.ShowFooter = false;
            this.LookUpEdit_DestAccount.Properties.SortColumnIndex = 2;
            this.LookUpEdit_DestAccount.Properties.ValueMember = "ledger_code1";
            this.LookUpEdit_DestAccount.Size = new System.Drawing.Size(320, 20);
            this.LookUpEdit_DestAccount.TabIndex = 1;
            this.LookUpEdit_DestAccount.ToolTip = "Destination Non-AR ledger account for the transfer";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(16, 25);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(43, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Account:";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(259, 297);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Quit";
            // 
            // Button_Transfer
            // 
            this.Button_Transfer.Enabled = false;
            this.Button_Transfer.Location = new System.Drawing.Point(147, 297);
            this.Button_Transfer.Name = "Button_Transfer";
            this.Button_Transfer.Size = new System.Drawing.Size(75, 25);
            this.Button_Transfer.TabIndex = 10;
            this.Button_Transfer.Text = "Transfer";
            // 
            // cbo_Reason
            // 
            this.cbo_Reason.AllowDrop = true;
            this.cbo_Reason.EditValue = "";
            this.cbo_Reason.EnterMoveNextControl = true;
            this.cbo_Reason.Location = new System.Drawing.Point(131, 252);
            this.cbo_Reason.Name = "cbo_Reason";
            this.cbo_Reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo_Reason.Properties.MaxLength = 50;
            this.cbo_Reason.Size = new System.Drawing.Size(320, 20);
            this.cbo_Reason.TabIndex = 9;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(3, 255);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(114, 13);
            this.Label14.TabIndex = 8;
            this.Label14.Text = "Reason for the transfer";
            // 
            // Form_Transfer_OperOper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 332);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Transfer);
            this.Controls.Add(this.cbo_Reason);
            this.Controls.Add(this.Label14);
            this.Name = "Form_Transfer_OperOper";
            this.Text = "Operating Account to Operating Account Transfer";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_SourceAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_DestAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox1;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_SourceAccount;
        internal DevExpress.XtraEditors.CalcEdit txc_amount;
        internal DevExpress.XtraEditors.LabelControl Label4;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_DestAccount;
        internal DevExpress.XtraEditors.LabelControl Label2;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_Transfer;
        internal DevExpress.XtraEditors.ComboBoxEdit cbo_Reason;
        internal DevExpress.XtraEditors.LabelControl Label14;

    }
}