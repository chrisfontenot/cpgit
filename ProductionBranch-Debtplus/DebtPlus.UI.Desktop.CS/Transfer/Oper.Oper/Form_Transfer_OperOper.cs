#region Copyright 2000-2012 DebtPlus, L.L.C.;
// {*******************************************************************}
// {                                                                   }
// {       DebtPlus Debt Management System                             }
// {                                                                   }
// {       Copyright 2000-2012 DebtPlus, L.L.C.                        }
// {       ALL RIGHTS RESERVED                                         }
// {                                                                   }
// {   The entire contents of this file is protected by U.S. and       }
// {   International Copyright Laws. Unauthorized reproduction,        }
// {   reverse-engineering, and distribution of all or any portion of  }
// {   the code contained in this file is strictly prohibited and may  }
// {   result in severe civil and criminal penalties and will be       }
// {   prosecuted to the maximum extent possible under the law.        }
// {                                                                   }
// {   RESTRICTIONS                                                    }
// {                                                                   }
// {   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
// {   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
// {   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
// {   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
// {   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
// {                                                                   }
// {   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
// {   ADDITIONAL RESTRICTIONS.                                        }
// {                                                                   }
// {*******************************************************************}
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DebtPlus;
using DebtPlus.Data;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.Transfer.Oper.Oper
{
    internal partial class Form_Transfer_OperOper : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_Transfer_OperOper() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        void RegisterHandlers()
        {
            this.Load += new EventHandler(Form_Transfer_OperOper_Load);
            LookUpEdit_DestAccount.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler( DebtPlus.Data.Validation.LookUpEdit_ActiveTest );
            LookUpEdit_SourceAccount.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler( DebtPlus.Data.Validation.LookUpEdit_ActiveTest );
            LookUpEdit_DestAccount.EditValueChanged += new EventHandler(cbo_dest_account_EditValueChanged);
            LookUpEdit_SourceAccount.EditValueChanged += new EventHandler(LookUpEdit_SourceAccount_EditValueChanged);
            txc_amount.Validated += new EventHandler(txc_amount_Validated);
            cbo_Reason.DragDrop += new DragEventHandler( drag_drop_DragDrop );
            cbo_Reason.DragEnter += new DragEventHandler( drag_drop_DragEnter );
            cbo_Reason.MouseMove += new MouseEventHandler( drag_drop_MouseMove );
            cbo_Reason.QueryContinueDrag += new QueryContinueDragEventHandler( drag_drop_QueryContinueDrag );
            cbo_Reason.TextChanged += new EventHandler(cbo_Reason_TextChanged);
            cbo_Reason.EditValueChanged += new EventHandler(cbo_Reason_EditValueChanged);
            Button_Cancel.Click += new EventHandler(Button_Cancel_Click);
            Button_Transfer.Click += new EventHandler(Button_Transfer_Click);
        }

        void UnRegisterHandlers()
        {
            this.Load -= new EventHandler(Form_Transfer_OperOper_Load);
            LookUpEdit_DestAccount.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(DebtPlus.Data.Validation.LookUpEdit_ActiveTest);
            LookUpEdit_SourceAccount.EditValueChanging -= new DevExpress.XtraEditors.Controls.ChangingEventHandler(DebtPlus.Data.Validation.LookUpEdit_ActiveTest);
            LookUpEdit_DestAccount.EditValueChanged -= new EventHandler(cbo_dest_account_EditValueChanged);
            LookUpEdit_SourceAccount.EditValueChanged -= new EventHandler(LookUpEdit_SourceAccount_EditValueChanged);
            txc_amount.Validated -= new EventHandler(txc_amount_Validated);
            cbo_Reason.DragDrop -= new DragEventHandler(drag_drop_DragDrop);
            cbo_Reason.DragEnter -= new DragEventHandler(drag_drop_DragEnter);
            cbo_Reason.MouseMove -= new MouseEventHandler(drag_drop_MouseMove);
            cbo_Reason.QueryContinueDrag -= new QueryContinueDragEventHandler(drag_drop_QueryContinueDrag);
            cbo_Reason.TextChanged -= new EventHandler(cbo_Reason_TextChanged);
            cbo_Reason.EditValueChanged -= new EventHandler(cbo_Reason_EditValueChanged);
            Button_Cancel.Click -= new EventHandler(Button_Cancel_Click);
            Button_Transfer.Click -= new EventHandler(Button_Transfer_Click);
        }

        private void Form_Transfer_OperOper_Load(object sender, EventArgs e) // Handles Me.Load
        {
            UnRegisterHandlers();
            try
            {
                LookUpEdit_SourceAccount.Properties.DataSource = DebtPlus.LINQ.Cache.ledger_code.getList();
                LookUpEdit_SourceAccount.EditValue = DebtPlus.LINQ.Cache.ledger_code.getDefault();

                LookUpEdit_DestAccount.Properties.DataSource = DebtPlus.LINQ.Cache.ledger_code.getList();
                LookUpEdit_DestAccount.EditValue = DebtPlus.LINQ.Cache.ledger_code.getDefault();

                // Load the list of reasons and clear the KeyField form
                Load_Transfer_Reasons("OO");
                clear_form();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void clear_form()
        {
            // Clear the transfer information
            LookUpEdit_SourceAccount.EditValue = null;
            LookUpEdit_DestAccount.EditValue = null;
            txc_amount.EditValue = null;

            // Set the error conditions for the values that are missing
            validate_source();
            validate_destination();
            validate_reason();
            validate_amount();

            // Disable the transfer button if there are errors
            Button_Transfer.Enabled = ! HasErrors();
        }

        private void LookUpEdit_SourceAccount_EditValueChanged(object sender, EventArgs e)
        {
            validate_source();
            validate_destination();
        }

        private bool HasErrors()
        {
            if( DxErrorProvider1.GetError(LookUpEdit_SourceAccount) != string.Empty )
            {
                return true;
            }

            if( DxErrorProvider1.GetError(LookUpEdit_DestAccount) != string.Empty )
            {
                return true;
            }

            // The account numbers must be different
            if( source_ledger_code == dest_ledger_code )
            {
                DxErrorProvider1.SetError(LookUpEdit_DestAccount, Strings.same_account);
                return true;
            }
            DxErrorProvider1.SetError(LookUpEdit_DestAccount, null);

            if (src_amount <= 0m)
            {
                DxErrorProvider1.SetError(txc_amount, Strings.required_value);
                return true;
            }
            DxErrorProvider1.SetError(txc_amount, null);

            if (! string.IsNullOrEmpty(DxErrorProvider1.GetError(cbo_Reason)))
            {
                return true;
            }

            // All is well
            return false;
        }

        private void txc_amount_Validated(object sender, EventArgs e)
        {
            validate_amount();
        }

        private decimal src_amount
        {
            get
            {
                if( txc_amount.Text == string.Empty )
                {
                    return 0M;
                }

                if( txc_amount.EditValue == null )
                {
                    return 0M;
                }
                
                return Convert.ToDecimal(txc_amount.EditValue);
            }
            set
            {
                txc_amount.EditValue = value;
            }
        }

        private void validate_amount()
        {
            string error_message = string.Empty;

            // If the amount is negative then reject the transfer
            if( txc_amount.Text.Trim() == string.Empty )
            {
                error_message = Strings.required_value;
            }

            else if( src_amount <= 0M )
            {
                error_message = Strings.must_be_posative;
            }

            // Set the error text
            DxErrorProvider1.SetError(txc_amount, error_message);
            Button_Transfer.Enabled = ! HasErrors();
        }

        private void drag_drop_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            System.Windows.Forms.Control ctl = ((System.Windows.Forms.Control) sender);

            // Do nothing if the operation is not valid
            if( e.Button == 0 )
            {
                return;
            }
            
            if( e.X >= 0 && e.X < ctl.Width && e.Y >= 0 && e.Y < ctl.Height )
            {
                return;
            }

            string txt = string.Empty;

            // If this is a text box then look for the selected text component of the text field
            if( sender is DevExpress.XtraEditors.TextEdit )
            {
                DevExpress.XtraEditors.TextEdit txe = (DevExpress.XtraEditors.TextEdit) sender;
                if( txe.SelectionLength > 0 )
                {
                    txt = txe.SelectedText;
                }
            }

            // If the text is missing then try the entire text field
            if( txt == string.Empty )
            {
                txt = ctl.Text.Trim();
            }

            // If there is no text then do nothing
            if( txt == string.Empty )
            {
                return;
            }

            // Start a drag-drop operation when the mouse moves outside the control
            System.Windows.Forms.DataObject dobj = new System.Windows.Forms.DataObject();
            dobj.SetData(DataFormats.Text, true, txt);

            // Do the operation. It will return when the operation is complete.
            DragDropEffects effect = DragDropEffects.Copy;
            effect = ctl.DoDragDrop(dobj, effect);

            // If the operation result was move then erase the text.
            // However, since we don't do the move then nothing more is needed.
        }

        private void drag_drop_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {

            // If there is a text field then look to determine the accepable processing
            if( e.Data.GetDataPresent(DataFormats.Text, true) )
            {
                e.Effect = e.AllowedEffect & DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void drag_drop_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {

            // If there is a text field then look to determine the accepable processing
            if( ! e.Data.GetDataPresent(DataFormats.Text, true) )
            {
                return;
            }

            // We only support copy. No modifier needs to be tested
            e.Effect = e.AllowedEffect & DragDropEffects.Copy;

            // Paste the text into the control
            ((System.Windows.Forms.Control) sender).Text = Convert.ToString(e.Data.GetData(DataFormats.Text, true));

            if( sender == cbo_Reason )
            {
                cbo_Reason_EditValueChanged(sender, new EventArgs());
            }

            else if( sender == txc_amount )
            {
                txc_amount_Validated(sender, new EventArgs());
            }
        }

        private void drag_drop_QueryContinueDrag(object sender, System.Windows.Forms.QueryContinueDragEventArgs e)
        {
            if( e.EscapePressed )
            {
                e.Action = DragAction.Cancel;
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e) // Handles Button_Cancel.Click
        {
            Close();
        }

        private void cbo_source_account_EditValueChanged(object sender, EventArgs e)
        {
            validate_source();
        }

        private void validate_source()
        {
            string error_message = string.Empty;

            // The only requirement is that there be a value.
            if( source_ledger_code == string.Empty )
            {
                error_message = Strings.required_value;
            }

            // Set the error message text
            DxErrorProvider1.SetError(LookUpEdit_SourceAccount, error_message);
            Button_Transfer.Enabled = ! HasErrors();
        }

        private string source_ledger_code
        {
            get
            {
                string answer = string.Empty;
                if (LookUpEdit_SourceAccount.EditValue != null)
                {
                    answer = ((string)LookUpEdit_SourceAccount.EditValue);
                }
                return answer;
            }
        }

        private void cbo_dest_account_EditValueChanged(object sender, EventArgs e)
        {
            validate_destination();
        }

        private void validate_destination()
        {
            string error_message = string.Empty;

            // The only requirement is that there be a value.
            if( dest_ledger_code == string.Empty )
            {
                error_message = Strings.required_value;
            }

            // Set the error message text
            DxErrorProvider1.SetError(LookUpEdit_DestAccount, error_message);
            Button_Transfer.Enabled = ! HasErrors();
        }

        private string dest_ledger_code
        {
            get
            {
                string answer = string.Empty;
                if (LookUpEdit_DestAccount.EditValue != null)
                {
                    answer = ((string)LookUpEdit_DestAccount.EditValue);
                }
                return answer;
            }
        }

        private string reason
        {
            get
            {
                return cbo_Reason.Text.Trim();
            }
        }

        private void cbo_Reason_EditValueChanged(object sender, EventArgs e) // Handles cbo_Reason.EditValueChanged
        {
            validate_reason();
        }

        private void cbo_Reason_TextChanged(object sender, EventArgs e) // Handles cbo_Reason.EditValueChanged
        {
            validate_reason();
        }

        private void validate_reason()
        {
            string error_message = string.Empty;

            // Ensure that there is a value for the information
            if(string.IsNullOrEmpty(reason))
            {
                error_message = Strings.required_value;
            }

            // Set the error text and enable the transfer button if there is no error
            DxErrorProvider1.SetError(cbo_Reason, error_message);
            Button_Transfer.Enabled = ! HasErrors();
        }

        private void Button_Transfer_Click(object sender, EventArgs e) // Handles Button_Transfer.Click
        {
            string errorMessage = string.Empty;

            try
            {
                DebtPlus.LINQ.SQLInfoClass sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
                using (var cn = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var txn = cn.BeginTransaction(IsolationLevel.RepeatableRead))
                    {
                        using (var bc = new DebtPlus.LINQ.BusinessContext(cn))
                        {
                            bc.Transaction = txn;

                            // Generate the two transactions for the transfer
                            var srcTransaction = new DebtPlus.LINQ.registers_non_ar()
                            {
                                credit_amt = src_amount,
                                debit_amt = src_amount,
                                tran_type = "NA",
                                src_ledger_account = source_ledger_code,
                                dst_ledger_account = dest_ledger_code,
                                message = cbo_Reason.Text.Trim()
                            };
                            bc.registers_non_ars.InsertOnSubmit(srcTransaction);

                            // Submit the changes once we have made the items correctly
                            bc.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                            // Commit the transaction once we have completed the request
                            txn.Commit();

                            // Success. Reset the form
                            clear_form();
                            LookUpEdit_SourceAccount.Focus();
                            return;
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                errorMessage = ex.Message;
            }
#pragma warning disable 168
            catch (System.Data.Linq.ChangeConflictException ex)
            {
                errorMessage = "The transaction has been canceled. Please re-submit it by clicking Transfer again.";
            }
#pragma warning restore 168

            // Things are not correct. Generate the error condition
            DebtPlus.Data.Forms.MessageBox.Show(errorMessage, "Error in transfer", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
        }

        private void Load_Transfer_Reasons(string ItemType)
        {
            System.Collections.Generic.List<DebtPlus.LINQ.message> col = DebtPlus.LINQ.Cache.message.getList().FindAll(s => s.item_type == ItemType);
            cbo_Reason.Properties.Items.Clear();
            foreach (DebtPlus.LINQ.message msg in col)
            {
                var NewItem = cbo_Reason.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(msg.description, msg.Id, msg.ActiveFlag));
                if (msg.Default)
                {
                    cbo_Reason.SelectedIndex = NewItem;
                }
            }
        }
    }
}