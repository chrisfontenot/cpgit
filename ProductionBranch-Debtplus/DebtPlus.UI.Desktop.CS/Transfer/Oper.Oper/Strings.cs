using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.Transfer.Oper.Oper
{
    public static class Strings
    {
	    public static readonly string required_value = "A KeyField is required for this field";
	    public static readonly string must_be_posative = "The amount must be greater than $0.00";
	    public static readonly string same_account = "The ledger accounts must be different";
	    public static readonly string transfer_error = "Error transferring the money";
    }
}
