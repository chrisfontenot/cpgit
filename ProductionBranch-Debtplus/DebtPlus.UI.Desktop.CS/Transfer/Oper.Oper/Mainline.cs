﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Transfer.Oper.Oper
{
    public class Mainline : DebtPlus.Interfaces.Desktop.IDesktopMainline
    {
        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var frm = new Form_Transfer_OperOper();
                    System.Windows.Forms.Application.Run(frm);
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "OperOperTransfer"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}
