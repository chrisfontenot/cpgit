using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.Transfer.Oper.Client
{
    public static class Strings
    {
	    public static readonly string required_value = "A KeyField is required for this field";
	    public static readonly string must_be_posative = "The amount must be greater than $0.00";
	    public static readonly string insufficient_funds = "There is not enough money in the client account to perform the transfer";
	    public static readonly string invalid_client = "The client ID is not valid.";
	    public static readonly string same_client = "The client numbers must not be the same for both sides of the transfer.";
	    public static readonly string transfer_error = "Error transferring the money";
    }
}
