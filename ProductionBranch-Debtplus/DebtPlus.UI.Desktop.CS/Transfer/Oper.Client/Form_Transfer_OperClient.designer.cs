﻿namespace DebtPlus.UI.Desktop.CS.Transfer.Oper.Client
{
    partial class Form_Transfer_OperClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Transfer_OperClient));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.GroupBox1 = new DevExpress.XtraEditors.GroupControl();
            this.LookUpEdit_SourceAccount = new DevExpress.XtraEditors.LookUpEdit();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Transfer = new DevExpress.XtraEditors.SimpleButton();
            this.cbo_Reason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.Label14 = new DevExpress.XtraEditors.LabelControl();
            this.GroupBox2 = new DevExpress.XtraEditors.GroupControl();
            this.txc_amount = new DevExpress.XtraEditors.CalcEdit();
            this.txb_dest_client = new DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.Label8 = new DevExpress.XtraEditors.LabelControl();
            this.Label9 = new DevExpress.XtraEditors.LabelControl();
            this.Label11 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_dest_inactive = new DebtPlus.Data.Controls.BlinkLabel();
            this.lbl_new_dest_trust = new DevExpress.XtraEditors.LabelControl();
            this.lbl_old_dest_trust = new DevExpress.XtraEditors.LabelControl();
            this.med_TargetName = new DevExpress.XtraEditors.MemoEdit();
            this.Label12 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox1)).BeginInit();
            this.GroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_SourceAccount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox2)).BeginInit();
            this.GroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txb_dest_client.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_TargetName.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.LookUpEdit_SourceAccount);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(3, 12);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(472, 59);
            this.GroupBox1.TabIndex = 6;
            this.GroupBox1.Text = " Source Account ";
            // 
            // LookUpEdit_SourceAccount
            // 
            this.LookUpEdit_SourceAccount.EnterMoveNextControl = true;
            this.LookUpEdit_SourceAccount.Location = new System.Drawing.Point(128, 25);
            this.LookUpEdit_SourceAccount.Name = "LookUpEdit_SourceAccount";
            this.LookUpEdit_SourceAccount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_SourceAccount.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ledger_code1", "Account", 5, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LookUpEdit_SourceAccount.Properties.DisplayMember = "description";
            this.LookUpEdit_SourceAccount.Properties.NullText = "";
            this.LookUpEdit_SourceAccount.Properties.ShowFooter = false;
            this.LookUpEdit_SourceAccount.Properties.SortColumnIndex = 2;
            this.LookUpEdit_SourceAccount.Properties.ValueMember = "ledger_code1";
            this.LookUpEdit_SourceAccount.Size = new System.Drawing.Size(339, 20);
            this.LookUpEdit_SourceAccount.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(16, 28);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(43, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Accoun&t:";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(259, 297);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 24);
            this.Button_Cancel.TabIndex = 11;
            this.Button_Cancel.Text = "&Quit";
            // 
            // Button_Transfer
            // 
            this.Button_Transfer.Enabled = false;
            this.Button_Transfer.Location = new System.Drawing.Point(146, 297);
            this.Button_Transfer.Name = "Button_Transfer";
            this.Button_Transfer.Size = new System.Drawing.Size(75, 24);
            this.Button_Transfer.TabIndex = 10;
            this.Button_Transfer.Text = "Transfer";
            // 
            // cbo_Reason
            // 
            this.cbo_Reason.AllowDrop = true;
            this.cbo_Reason.EditValue = "";
            this.cbo_Reason.EnterMoveNextControl = true;
            this.cbo_Reason.Location = new System.Drawing.Point(131, 252);
            this.cbo_Reason.Name = "cbo_Reason";
            this.cbo_Reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbo_Reason.Properties.MaxLength = 50;
            this.cbo_Reason.Size = new System.Drawing.Size(339, 20);
            this.cbo_Reason.TabIndex = 9;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(3, 255);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(114, 13);
            this.Label14.TabIndex = 8;
            this.Label14.Text = "&Reason for the transfer";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.txc_amount);
            this.GroupBox2.Controls.Add(this.txb_dest_client);
            this.GroupBox2.Controls.Add(this.Label8);
            this.GroupBox2.Controls.Add(this.Label9);
            this.GroupBox2.Controls.Add(this.Label11);
            this.GroupBox2.Controls.Add(this.lbl_dest_inactive);
            this.GroupBox2.Controls.Add(this.lbl_new_dest_trust);
            this.GroupBox2.Controls.Add(this.lbl_old_dest_trust);
            this.GroupBox2.Controls.Add(this.med_TargetName);
            this.GroupBox2.Controls.Add(this.Label12);
            this.GroupBox2.Location = new System.Drawing.Point(3, 133);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(472, 112);
            this.GroupBox2.TabIndex = 7;
            this.GroupBox2.Text = " Transfer to the Client ";
            // 
            // txc_amount
            // 
            this.txc_amount.AllowDrop = true;
            this.txc_amount.EnterMoveNextControl = true;
            this.txc_amount.Location = new System.Drawing.Point(387, 49);
            this.txc_amount.Name = "txc_amount";
            this.txc_amount.Properties.Appearance.Options.UseTextOptions = true;
            this.txc_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txc_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txc_amount.Properties.DisplayFormat.FormatString = "c2";
            this.txc_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Properties.EditFormat.FormatString = "d2";
            this.txc_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txc_amount.Properties.Mask.BeepOnError = true;
            this.txc_amount.Properties.Mask.EditMask = "c";
            this.txc_amount.Properties.Precision = 2;
            this.txc_amount.Size = new System.Drawing.Size(80, 20);
            this.txc_amount.TabIndex = 6;
            // 
            // txb_dest_client
            // 
            this.txb_dest_client.AllowDrop = true;
            this.txb_dest_client.EnterMoveNextControl = true;
            this.txb_dest_client.Location = new System.Drawing.Point(128, 26);
            this.txb_dest_client.Name = "txb_dest_client";
            this.txb_dest_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txb_dest_client.Properties.Appearance.Options.UseTextOptions = true;
            this.txb_dest_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txb_dest_client.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("txb_dest_client.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to search for a client ID", "search", null, true)});
            this.txb_dest_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txb_dest_client.Properties.DisplayFormat.FormatString = "0000000";
            this.txb_dest_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txb_dest_client.Properties.EditFormat.FormatString = "f0";
            this.txb_dest_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txb_dest_client.Properties.Mask.BeepOnError = true;
            this.txb_dest_client.Properties.Mask.EditMask = "\\d*";
            this.txb_dest_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txb_dest_client.Properties.ValidateOnEnterKey = true;
            this.txb_dest_client.Size = new System.Drawing.Size(80, 20);
            this.txb_dest_client.TabIndex = 1;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(256, 78);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(81, 13);
            this.Label8.TabIndex = 7;
            this.Label8.Text = "New trust Balace";
            this.Label8.UseMnemonic = false;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(256, 52);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(41, 13);
            this.Label9.TabIndex = 5;
            this.Label9.Text = "&Amount:";
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(256, 26);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(104, 13);
            this.Label11.TabIndex = 2;
            this.Label11.Text = "Original Trust Balance";
            this.Label11.UseMnemonic = false;
            // 
            // lbl_dest_inactive
            // 
            this.lbl_dest_inactive.Appearance.BackColor = System.Drawing.Color.Red;
            this.lbl_dest_inactive.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_dest_inactive.Appearance.ForeColor = System.Drawing.Color.White;
            this.lbl_dest_inactive.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbl_dest_inactive.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_dest_inactive.Location = new System.Drawing.Point(152, 0);
            this.lbl_dest_inactive.Name = "lbl_dest_inactive";
            this.lbl_dest_inactive.Size = new System.Drawing.Size(0, 13);
            this.lbl_dest_inactive.TabIndex = 0;
            this.lbl_dest_inactive.UseMnemonic = false;
            this.lbl_dest_inactive.Visible = false;
            // 
            // lbl_new_dest_trust
            // 
            this.lbl_new_dest_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_new_dest_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_new_dest_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_new_dest_trust.Location = new System.Drawing.Point(360, 78);
            this.lbl_new_dest_trust.Name = "lbl_new_dest_trust";
            this.lbl_new_dest_trust.Size = new System.Drawing.Size(88, 13);
            this.lbl_new_dest_trust.TabIndex = 8;
            this.lbl_new_dest_trust.Text = "$0.00";
            this.lbl_new_dest_trust.UseMnemonic = false;
            // 
            // lbl_old_dest_trust
            // 
            this.lbl_old_dest_trust.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_old_dest_trust.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lbl_old_dest_trust.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_old_dest_trust.Location = new System.Drawing.Point(360, 26);
            this.lbl_old_dest_trust.Name = "lbl_old_dest_trust";
            this.lbl_old_dest_trust.Size = new System.Drawing.Size(88, 13);
            this.lbl_old_dest_trust.TabIndex = 3;
            this.lbl_old_dest_trust.Text = "$0.00";
            this.lbl_old_dest_trust.UseMnemonic = false;
            // 
            // med_TargetName
            // 
            this.med_TargetName.EditValue = "";
            this.med_TargetName.Location = new System.Drawing.Point(16, 52);
            this.med_TargetName.Name = "med_TargetName";
            this.med_TargetName.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            this.med_TargetName.Properties.ReadOnly = true;
            this.med_TargetName.Properties.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.med_TargetName.Size = new System.Drawing.Size(232, 51);
            this.med_TargetName.TabIndex = 4;
            this.med_TargetName.TabStop = false;
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(16, 26);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(66, 13);
            this.Label12.TabIndex = 0;
            this.Label12.Text = "Target &Client:";
            // 
            // Form_Transfer_OperClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 332);
            this.Controls.Add(this.GroupBox1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Transfer);
            this.Controls.Add(this.cbo_Reason);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.GroupBox2);
            this.Name = "Form_Transfer_OperClient";
            this.Text = "Operating Account to Client Transfer";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox1)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_SourceAccount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbo_Reason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupBox2)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txc_amount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txb_dest_client.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.med_TargetName.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal DevExpress.XtraEditors.GroupControl GroupBox1;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_SourceAccount;
        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.SimpleButton Button_Transfer;
        internal DevExpress.XtraEditors.ComboBoxEdit cbo_Reason;
        internal DevExpress.XtraEditors.LabelControl Label14;
        internal DevExpress.XtraEditors.GroupControl GroupBox2;
        internal DevExpress.XtraEditors.CalcEdit txc_amount;
        internal DebtPlus.UI.Client.Widgets.Controls.ClientID txb_dest_client;
        internal DevExpress.XtraEditors.LabelControl Label8;
        internal DevExpress.XtraEditors.LabelControl Label9;
        internal DevExpress.XtraEditors.LabelControl Label11;
        internal DebtPlus.Data.Controls.BlinkLabel lbl_dest_inactive;
        internal DevExpress.XtraEditors.LabelControl lbl_new_dest_trust;
        internal DevExpress.XtraEditors.LabelControl lbl_old_dest_trust;
        internal DevExpress.XtraEditors.MemoEdit med_TargetName;
        internal DevExpress.XtraEditors.LabelControl Label12;

    }
}