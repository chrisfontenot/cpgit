using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.VB6
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() : base()
        {
        }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                string returnDirectory = Environment.CurrentDirectory;
                try
                {
                    NameValueCollection settings = DebtPlus.Configuration.Config.GetConfigSettings();

                    // Locate the executable address. If not defined then we can not continue.
                    string vb6ExeLocationSetting = settings["vb6.exe.location"];
                    if (string.IsNullOrWhiteSpace(vb6ExeLocationSetting))
                    {
                        return;
                    }

                    string vb6ExeFullPath = Path.GetFullPath(vb6ExeLocationSetting);
                    using (var pgm = new Process())
                    {
                        // Set the parameter for the report. It is normally required.
                        var argsList = (string[])param;
                        if (argsList.GetUpperBound(0) >= 0)
                        {
                            pgm.StartInfo.Arguments = argsList[0];
                        }

                        // Set the filename, directory, and start the process in the foreground.
                        pgm.StartInfo.FileName         = vb6ExeFullPath;
                        pgm.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(vb6ExeFullPath);
                        pgm.Start();
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    Environment.CurrentDirectory = returnDirectory;
                }
            }))
            {
                IsBackground = false,
                Name = "VB6Reports"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }
    }
}