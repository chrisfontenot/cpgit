using System.Windows.Forms;
using DebtPlus.UI.FormLib.Check;

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.Delete
{
    internal partial class ReconcileArgParser : DebtPlus.Utils.ArgParserBase
    {
        private System.Int32 privateBatch = -1;

        public System.Int32 Batch
        {
            get { return privateBatch; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ReconcileArgParser() : base(new string[] { })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            // Request the batch ID
            using (ReconcileBatchForm frm = new ReconcileBatchForm())
            {
                frm.ShowNewButton = false;
                System.Windows.Forms.DialogResult answer = frm.ShowDialog();
                privateBatch = frm.BatchID;
                if (answer != DialogResult.OK)
                {
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                }
            }

            return ss;
        }
    }
}