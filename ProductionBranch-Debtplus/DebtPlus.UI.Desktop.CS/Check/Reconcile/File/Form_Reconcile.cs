#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DebtPlus.Data.Forms;

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.File
{
    internal partial class Form_Reconcile : DebtPlusForm
    {
        private ReconcileArgParser ap                        = null;
        private string FindBankFromAccount_LastAccountNumber = string.Empty;
        private Int32 FindBankFromAccount_LastBank           = -1;
        public decimal batch_hash_total                      = 0m;
        public decimal batch_amount                          = 0m;
        public Int32 batch_count                             = 0;

        private BackgroundWorker bt                          = new BackgroundWorker();

        private delegate void ValueDelegate(string Value);

        // Storage for the program
        public ListDictionary parsing_data = new ListDictionary(Comparer.DefaultInvariant);

        /// <summary>
        /// Initialize the new class
        /// </summary>
        public Form_Reconcile() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new class
        /// </summary>
        /// <param name="ap"></param>
        public Form_Reconcile(ReconcileArgParser ap) : this()
        {
            this.ap = ap;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Button_Cancel.Click   += Button_Cancel_Click;
            Load                  += Form_Reconcile_Load;
            if (bt                != null)
            {
                bt.DoWork             += bt_DoWork;
                bt.RunWorkerCompleted += bt_RunWorkerCompleted;
            }
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Button_Cancel.Click   -= Button_Cancel_Click;
            Load                  -= Form_Reconcile_Load;
            if (bt                != null)
            {
                bt.DoWork             -= bt_DoWork;
                bt.RunWorkerCompleted -= bt_RunWorkerCompleted;
            }
        }

        #region Windows Form Designer generated code

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        internal DevExpress.XtraEditors.LabelControl Label1;
        internal DevExpress.XtraEditors.LabelControl Label2;
        internal DevExpress.XtraEditors.LabelControl Label3;
        internal DevExpress.XtraEditors.LabelControl Label4;
        internal DevExpress.XtraEditors.LabelControl Label5;
        internal DevExpress.XtraEditors.LabelControl Label6;
        internal DevExpress.XtraEditors.LabelControl Label7;
        public DevExpress.XtraEditors.LabelControl Label_Reconciling_Item;
        public DevExpress.XtraEditors.LabelControl Label_Transaction_Type;
        public DevExpress.XtraEditors.LabelControl Label_checknum;
        public DevExpress.XtraEditors.LabelControl Label_Date;
        public DevExpress.XtraEditors.LabelControl Label_Amount;

        public DevExpress.XtraEditors.LabelControl Label_Bank;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Reconcile));
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Label2 = new DevExpress.XtraEditors.LabelControl();
            this.Label3 = new DevExpress.XtraEditors.LabelControl();
            this.Label4 = new DevExpress.XtraEditors.LabelControl();
            this.Label5 = new DevExpress.XtraEditors.LabelControl();
            this.Label6 = new DevExpress.XtraEditors.LabelControl();
            this.Label7 = new DevExpress.XtraEditors.LabelControl();
            this.Label_Reconciling_Item = new DevExpress.XtraEditors.LabelControl();
            this.Label_Transaction_Type = new DevExpress.XtraEditors.LabelControl();
            this.Label_checknum = new DevExpress.XtraEditors.LabelControl();
            this.Label_Date = new DevExpress.XtraEditors.LabelControl();
            this.Label_Amount = new DevExpress.XtraEditors.LabelControl();
            this.Label_Bank = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            this.SuspendLayout();
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Location = new System.Drawing.Point(152, 223);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 25);
            this.Button_Cancel.TabIndex = 2;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Cancel the dialog and return to the previous screen";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //Label1
            //
            this.Label1.Location = new System.Drawing.Point(16, 60);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(80, 13);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "Reconciling item:";
            //
            //Label2
            //
            this.Label2.Location = new System.Drawing.Point(16, 85);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(87, 13);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "Transaction Type:";
            //
            //Label3
            //
            this.Label3.Location = new System.Drawing.Point(16, 110);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(73, 13);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "Check Number:";
            //
            //Label4
            //
            this.Label4.Location = new System.Drawing.Point(16, 135);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(27, 13);
            this.Label4.TabIndex = 6;
            this.Label4.Text = "Date:";
            //
            //Label5
            //
            this.Label5.Location = new System.Drawing.Point(16, 160);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(41, 13);
            this.Label5.TabIndex = 7;
            this.Label5.Text = "Amount:";
            //
            //Label6
            //
            this.Label6.Location = new System.Drawing.Point(16, 185);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(27, 13);
            this.Label6.TabIndex = 8;
            this.Label6.Text = "Bank:";
            //
            //Label7
            //
            this.Label7.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.Label7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Label7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label7.Location = new System.Drawing.Point(5, 12);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(359, 32);
            this.Label7.TabIndex = 9;
            this.Label7.Text = "The system is processing the check reconciliation on the following item. Please l" + "et it continue.";
            this.Label7.UseMnemonic = false;
            //
            //Label_Reconciling_Item
            //
            this.Label_Reconciling_Item.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Reconciling_Item.Location = new System.Drawing.Point(128, 60);
            this.Label_Reconciling_Item.Name = "Label_Reconciling_Item";
            this.Label_Reconciling_Item.Size = new System.Drawing.Size(236, 13);
            this.Label_Reconciling_Item.TabIndex = 10;
            //
            //Label_Transaction_Type
            //
            this.Label_Transaction_Type.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Transaction_Type.Location = new System.Drawing.Point(128, 85);
            this.Label_Transaction_Type.Name = "Label_Transaction_Type";
            this.Label_Transaction_Type.Size = new System.Drawing.Size(236, 13);
            this.Label_Transaction_Type.TabIndex = 11;
            //
            //Label_checknum
            //
            this.Label_checknum.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_checknum.Location = new System.Drawing.Point(128, 110);
            this.Label_checknum.Name = "Label_checknum";
            this.Label_checknum.Size = new System.Drawing.Size(236, 13);
            this.Label_checknum.TabIndex = 12;
            //
            //Label_Date
            //
            this.Label_Date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Date.Location = new System.Drawing.Point(128, 135);
            this.Label_Date.Name = "Label_Date";
            this.Label_Date.Size = new System.Drawing.Size(236, 13);
            this.Label_Date.TabIndex = 13;
            //
            //Label_Amount
            //
            this.Label_Amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Amount.Location = new System.Drawing.Point(128, 160);
            this.Label_Amount.Name = "Label_Amount";
            this.Label_Amount.Size = new System.Drawing.Size(236, 13);
            this.Label_Amount.TabIndex = 14;
            //
            //Label_Bank
            //
            this.Label_Bank.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Bank.Location = new System.Drawing.Point(128, 185);
            this.Label_Bank.Name = "Label_Bank";
            this.Label_Bank.Size = new System.Drawing.Size(236, 13);
            this.Label_Bank.TabIndex = 15;
            //
            //Form_Reconcile
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(376, 264);
            this.Controls.Add(this.Label_Bank);
            this.Controls.Add(this.Label_Amount);
            this.Controls.Add(this.Label_Date);
            this.Controls.Add(this.Label_checknum);
            this.Controls.Add(this.Label_Transaction_Type);
            this.Controls.Add(this.Label_Reconciling_Item);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Button_Cancel);
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "Form_Reconcile";
            this.Text = "Load check reconcile information";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion Windows Form Designer generated code

        /// <summary>
        /// Set the bank name on the display
        /// </summary>
        private void Set_BankName(string Value)
        {
            if (Label_Bank.InvokeRequired)
            {
                IAsyncResult ia = Label_Bank.BeginInvoke(new ValueDelegate(Set_BankName), new object[] { Value });
                Label_Bank.EndInvoke(ia);
                return;
            }

            Label_Bank.Text = Value;
            Label_Bank.Invalidate();
        }

        /// <summary>
        /// Set the dollar amount on the display
        /// </summary>
        private void Set_Amount(string Value)
        {
            if (Label_Amount.InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ValueDelegate(Set_Amount), new object[] { Value });
                Label_Amount.EndInvoke(ia);
                return;
            }

            Label_Amount.Text = Value;
            Label_Amount.Invalidate();
        }

        /// <summary>
        /// Set the check number on the display
        /// </summary>
        private void Set_checknum(string Value)
        {
            if (Label_checknum.InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ValueDelegate(Set_checknum), new object[] { Value });
                Label_checknum.EndInvoke(ia);
                return;
            }

            Label_checknum.Text = Value;
            Label_checknum.Invalidate();
        }

        /// <summary>
        /// Set the type of the item on the display
        /// </summary>
        private void Set_Transaction_Type(string Value)
        {
            if (Label_Transaction_Type.InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ValueDelegate(Set_Transaction_Type), new object[] { Value });
                Label_Transaction_Type.EndInvoke(ia);
                return;
            }

            Label_Transaction_Type.Text = Value;
            Label_Transaction_Type.Invalidate();
        }

        /// <summary>
        /// Process the recon input file name
        /// </summary>
        private void Set_Reconciling_Item(string Value)
        {
            if (Label_Reconciling_Item.InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ValueDelegate(Set_Reconciling_Item), new object[] { Value });
                Label_Reconciling_Item.EndInvoke(ia);
                return;
            }

            Label_Reconciling_Item.Text = Value;
            Label_Reconciling_Item.Invalidate();
        }

        /// <summary>
        /// Set the reconciliation date on the display
        /// </summary>
        private void Set_Date(string Value)
        {
            if (Label_Date.InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ValueDelegate(Set_Date), new object[] { Value });
                Label_Date.EndInvoke(ia);
                return;
            }

            Label_Date.Text = Value;
            Label_Date.Invalidate();
        }

        /// <summary>
        /// Process the CANCEL button CLICK event
        /// </summary>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// Process the LOAD event on the form
        /// </summary>
        private void Form_Reconcile_Load(object sender, EventArgs e)
        {
            // Build the list of regular expressions for the various types
            // There may or may not be items. It is ok. If there are no items then the file won't be parsed and loaded. That is the default "fall-back" condition.
            parsing_data = DebtPlus.Configuration.Config.GetRegexExprList(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CheckReconcileFile));

            // Load the reconciliation file
            bt.RunWorkerAsync();
        }

        /// <summary>
        /// Start the work on the background
        /// </summary>
        private void bt_DoWork(object sender, DoWorkEventArgs e)
        {
            // Read the file
            FileStream fso = new FileStream(ap.FileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            StreamReader src_text = new StreamReader(fso);

            // Find the hash key if there is one
            DebtPlus.Configuration.RegexParser hash_dictonary_expr = (DebtPlus.Configuration.RegexParser)parsing_data["HASH"];
            Regex hash_rx = null;
            if (hash_dictonary_expr != null)
            {
                hash_rx = hash_dictonary_expr.rx;
            }

            try
            {
                using (SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (SqlTransaction txn = cn.BeginTransaction())
                    {
                        while(true)
                        {
                            // Read the next line from the source file. Stop at the EOF condition.
                            string source_line = src_text.ReadLine();
                            if (source_line == null)
                            {
                                break;
                            }

                            // Determine if the input line is valid
                            source_line = source_line.Trim();
                            if (string.IsNullOrEmpty(source_line))
                            {
                                continue;
                            }

                            // Look for the various other matches for the types. Do not process the HASH entry again.
                            foreach (string key in parsing_data.Keys)
                            {
                                // Find the expression for this key.
                                DebtPlus.Configuration.RegexParser expr = (DebtPlus.Configuration.RegexParser)parsing_data[key];

                                // Determine if the regular expression for this type matches the line.
                                Match match_expression = null;
                                Regex rx               = expr.rx;
                                match_expression       = rx.Match(source_line);

                                // If the line matches the hash string then process the hash line
                                if (match_expression.Success)
                                {
                                    process_line(cn, txn, expr.subtype, match_expression, "R");
                                    break;
                                }
                            }
                        }

                        // Close the batch
                        reconcile_batch_close(cn, txn);

                        // Commit the changes to the database
                        txn.Commit();
                    }
                }

                // Display the reconciliation item report for this batch
                print_report();
            }

            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), ap.RM.GetString("error_dialog_title"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Process the completion from the background thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Button_Cancel.Text = "Close";
            Label7.Text = "The operation is complete. Click on the CLOSE button to dismiss this dialog.";

            // Clear the label fields
            Set_Amount(string.Empty);
            Set_checknum(string.Empty);
            Set_Date(string.Empty);
            Set_Reconciling_Item(string.Empty);
            Set_Transaction_Type(string.Empty);
            Set_BankName(string.Empty);
        }

        /// <summary>
        /// Print the report on what was reconciled
        /// </summary>
        private void print_report()
        {
            DebtPlus.Reports.Reconcile.Detail.CheckReconcileDetailListReport rpt = new DebtPlus.Reports.Reconcile.Detail.CheckReconcileDetailListReport();
            rpt.Parameter_BatchID = ap.Batch;
            rpt.RunReportInSeparateThread();
        }

        /// <summary>
        /// Locate the bank from the account number and ABA number
        /// </summary>
        /// <param name="ABA"></param>
        /// <param name="AccountNumber"></param>
        /// <returns></returns>
        private Int32 FindBankFromAccount(string ABA, string AccountNumber)
        {
            // If the bank account is different then lookup the value
            if (FindBankFromAccount_LastAccountNumber == AccountNumber)
            {
                return FindBankFromAccount_LastBank;
            }

            FindBankFromAccount_LastAccountNumber = AccountNumber;
            if (AccountNumber != string.Empty)
            {
                var qBank = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.aba == ABA && s.account_number == AccountNumber);
                if (qBank != null)
                {
                    FindBankFromAccount_LastBank = qBank.Id;
                    Set_BankName(qBank.description);
                }
            }

            return FindBankFromAccount_LastBank;
        }

        /// <summary>
        /// Process the line from the recon input file
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        /// <param name="reconcile_subtype"></param>
        /// <param name="match"></param>
        /// <param name="reconcile_cleared"></param>
        private void process_line(SqlConnection cn, SqlTransaction txn, string reconcile_subtype, Match match, string reconcile_cleared)
        {
            // Locate the fields
            string reconcile_aba       = match.Groups["aba"].Value;
            string reconcile_account   = match.Groups["account"].Value;
            string reconcile_date      = match.Groups["date"].Value;
            string reconcile_month     = match.Groups["month"].Value;
            string reconcile_day       = match.Groups["day"].Value;
            string reconcile_year      = match.Groups["year"].Value;
            string reconcile_amount    = match.Groups["amount"].Value;
            string reconcile_cents     = match.Groups["cents"].Value;
            string reconcile_checknum  = match.Groups["check"].Value;
            string reconcile_reference = match.Groups["reference"].Value;

            System.DateTime transaction_date = default(System.DateTime);
            decimal transaction_amount = 0m;
            object ErrorText = DBNull.Value;

            // Trim all leading blanks/zeros from the check number. If the check is simply "0" then use "0".
            reconcile_checknum = reconcile_checknum.TrimStart(new char[] { '0' });
            if (string.IsNullOrEmpty(reconcile_checknum))
            {
                reconcile_checknum = "0";
            }

            // If there is an account then find the bank with this account
            Int32 Bank = ap.Bank;
            if (Bank < 0 && reconcile_account != string.Empty)
            {
                Bank = FindBankFromAccount(reconcile_aba, reconcile_account);
            }

            // If there is no bank then use the default bank
            if (Bank < 0)
            {
                var qBank = DebtPlus.LINQ.Cache.bank.getDefault("C");
                if (qBank != null)
                {
                    Bank = qBank.Value;
                }
            }

            transaction_date = System.DateTime.MinValue;
            try
            {
                // Determine the appropriate date
                if (reconcile_date != string.Empty)
                {
                    if (!System.DateTime.TryParse(reconcile_date, out transaction_date))
                    {
                        ErrorText = ap.RM.GetString("error_text_invalid_date");
                    }
                }
                else
                {
                    if (reconcile_month != string.Empty && reconcile_day != string.Empty && reconcile_year != string.Empty)
                    {
                        Int32 int_year  = 0;
                        Int32 int_month = 0;
                        Int32 int_day   = 0;

                        if (Int32.TryParse(reconcile_year, out int_year) && Int32.TryParse(reconcile_month, out int_month) && Int32.TryParse(reconcile_day, out int_day))
                        {
                            if (int_year < 100)
                            {
                                if (int_year < 80)
                                {
                                    int_year += 100;
                                }
                                int_year += 1900;
                            }

                            transaction_date = new System.DateTime(int_year, int_month, int_day);
                        }
                    }
                }
            }

            catch
            {
                ErrorText = ap.RM.GetString("error_text_invalid_date");
            }

            try
            {
                // Find the dollar amount of the transaction
                if (reconcile_cents != string.Empty)
                {
                    StringBuilder temp_string = new StringBuilder(reconcile_cents);
                    temp_string.Insert(temp_string.Length - 2, ".", 1);
                    reconcile_amount = temp_string.ToString();
                }

                if (!decimal.TryParse(reconcile_amount, out transaction_amount))
                {
                    ErrorText = ap.RM.GetString("error_text_invalid_amount");
                }
                else
                {
                    transaction_amount = Decimal.Truncate(transaction_amount * 100M) / 100M;
                }
            }

            catch
            {
                ErrorText = ap.RM.GetString("error_text_invalid_amount");
            }

            // Update the statistics
            batch_amount += transaction_amount;
            batch_count  += 1;

            // Refresh the display
            Set_Amount(string.Format("{0:c2}", transaction_amount));
            Set_checknum(reconcile_checknum);
            Set_Reconciling_Item(batch_count.ToString());
            Set_Transaction_Type(reconcile_subtype);
            Set_Date(transaction_date.ToShortDateString());

            // Record the transaction
            reconcile_details_write(cn, txn, ap.Batch, reconcile_checknum, reconcile_cleared, transaction_date, transaction_amount, reconcile_subtype, reconcile_reference, Bank, ErrorText);
        }

        /// <summary>
        /// Record the transaction to the transaction table
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        /// <param name="Batch"></param>
        /// <param name="CheckNumber"></param>
        /// <param name="Cleared"></param>
        /// <param name="ReconDate"></param>
        /// <param name="ReconAmount"></param>
        /// <param name="tran_type"></param>
        /// <param name="Comments"></param>
        /// <param name="Bank"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        private Int32 reconcile_details_write(SqlConnection cn, SqlTransaction txn, Int32 Batch, string CheckNumber, string Cleared, System.DateTime ReconDate, decimal ReconAmount, string tran_type, string Comments, Int32 Bank, object ErrorMessage)
        {
            Int32 answer = -1;

            // Complete the issue by inserting the transaction into the detail items.
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "xpr_insert_recon_detail";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@Batch", SqlDbType.Int).Value = Batch;
                cmd.Parameters.Add("@Bank", SqlDbType.Int).Value = Bank;
                cmd.Parameters.Add("@CheckNumber", SqlDbType.VarChar, 50).Value = CheckNumber;
                cmd.Parameters.Add("@Cleared", SqlDbType.VarChar, 10).Value = Cleared;
                cmd.Parameters.Add("@ReconDate", SqlDbType.DateTime).Value = ReconDate;
                cmd.Parameters.Add("@ReconAmount", SqlDbType.Money).Value = ReconAmount;
                cmd.Parameters.Add("@Tran_Type", SqlDbType.VarChar, 10).Value = tran_type;
                cmd.Parameters.Add("@Comments", SqlDbType.VarChar, 80).Value = Comments;
                cmd.Parameters.Add("@ErrorMessage", SqlDbType.VarChar, 80).Value = ErrorMessage;

                cmd.ExecuteNonQuery();

                answer = Convert.ToInt32(cmd.Parameters[0].Value);
            }

            return answer;
        }

        /// <summary>
        /// Close the reconciliation batch
        /// </summary>
        /// <param name="cn"></param>
        /// <param name="txn"></param>
        private void reconcile_batch_close(SqlConnection cn, SqlTransaction txn)
        {
            Int32 answer = -1;

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "xpr_Recon_Close";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection = cn;
                    cmd.Transaction = txn;

                    SqlCommandBuilder.DeriveParameters(cmd);

                    cmd.Parameters[1].Value = ap.Batch;
                    cmd.ExecuteNonQuery();

                    answer = Convert.ToInt32(cmd.Parameters[0].Value);
                }
            }

            catch
            {
            }
        }
    }
}