using System;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;
using DebtPlus.UI.FormLib.Check;

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.File
{
    internal partial class ReconcileArgParser : DebtPlus.Utils.ArgParserBase
    {
        // Strings for the file
        public ResourceManager RM = new ResourceManager("DebtPlus.UI.Desktop.CS.Form_Reconcile_Strings", Assembly.GetExecutingAssembly());

        public string FileName { get; private set; }
        public Int32 Batch { get; private set; }
        public Int32 Bank { get; private set; }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ReconcileArgParser() : base(new string[] { })
        {
            FileName = string.Empty;
            Batch = -1;
            Bank = -1;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            // Request the file to be processed
            if (!request_filename())
            {
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
            }
            else
            {
                // Request the batch ID
                using (var frm = new ReconcileBatchForm())
                {
                    DialogResult answer = frm.ShowDialog();
                    Batch = frm.BatchID;
                    if (answer != DialogResult.OK)
                    {
                        ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                    }
                }
            }

            return ss;
        }

        public bool request_filename()
        {
            DialogResult answer = default(DialogResult);
            using (var dlg = new OpenFileDialog())
            {
                dlg.CheckFileExists = true;
                dlg.CheckPathExists = true;
                dlg.DereferenceLinks = true;
                dlg.Multiselect = false;
                dlg.ReadOnlyChecked = true;
                dlg.RestoreDirectory = true;
                dlg.ShowReadOnly = false;
                dlg.ValidateNames = true;
                dlg.DefaultExt = RM.GetString("file_open_extnsion");
                dlg.Filter = RM.GetString("file_open_filter");
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                dlg.Title = RM.GetString("file_open_title");
                answer = dlg.ShowDialog();

                FileName = dlg.FileName;
            }

            return (answer == DialogResult.OK);
        }
    }
}