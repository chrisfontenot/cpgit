#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.Manual
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() : base()
        {
        }

        public void OldAppMain(string[] args)
        {
            // This processing does modal dialogs from the start. We need to run it as a new thread so that the menu will
            // continue to operate.
            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(UpdateProcedure));
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.IsBackground = false;
            thrd.Name = "ReconcileManual";
            thrd.Start(args);
        }

        private void UpdateProcedure(object obj)
        {
            string[] args = (string[])obj;
            ArgParser ap = new ArgParser();
            if (ap.Parse(args) && ap.batch > 0)
            {
                var mainForm = new ReconcileItemsForm(ap);
                mainForm.Show();
            }
        }
    }
}