using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using DevExpress.XtraGrid.Columns;
using DebtPlus.Data.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraBars;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.Manual
{
    internal partial class ReconcileItemsForm : DebtPlusForm
    {
        private const string STR_RECONCILED = "R";
        private const string STR_UNRECONCILED = " ";
        private const string STR_NOTFOUND = "NOT FOUND";

        private const string STR_FOUND = "FOUND";
        // Current table with the update transactions

        private DataTable UpdateTable;

        public ReconcileItemsForm(ArgParser ap) : this()
        {
            ReconBatch = ap.batch;
        }

        public ReconcileItemsForm() : base()
        {
            InitializeComponent();
            BarButtonItem_File_Exit.ItemClick += BarButtonItem_File_Exit_ItemClick;
            this.FormClosing += ReconcileItemsForm_FormClosing;
            this.GotFocus += ReconcileItemsForm_GotFocus;
            this.Load += frmItems_Load;
            bt_reader.DoWork += bt_reader_DoWork;
            bt_reader.RunWorkerCompleted += bt_reader_RunWorkerCompleted;
            UpdateTable.RowChanged += UpdateTable_ColumnChanged;
            GridView1.Click += GridView1_Click;
            mnuChangeDate.Click += mnuChangeDate_Click;
            mnuChangeItem.Click += mnuChangeItem_Click;
            mnuChangeStatus.Click += mnuChangeStatus_Click;
            mnuCreateItem.Click += mnuCreateItem_Click;
            BarButtonItem_File_Print.ItemClick += BarButtonItem_File_Print_ItemClick;
            Button_OK.Click += Button_OK_Click;
            ContextMenu1.Popup += ContextMenu1_Popup;
            BarStaticItem_recon_date.ItemDoubleClick += BarStaticItem_recon_date_ItemDoubleClick;
        }

        #region "Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        //Required by the Windows Form Designer

        private IContainer components;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal System.Windows.Forms.ContextMenu ContextMenu1;

        internal System.Windows.Forms.MenuItem MenuItem1;
        public System.Windows.Forms.MenuItem mnuChangeItem;
        public System.Windows.Forms.MenuItem mnuChangeDate;
        public System.Windows.Forms.MenuItem mnuCreateItem;
        public System.Windows.Forms.MenuItem mnuChangeStatus;
        internal DevExpress.XtraBars.BarManager barManager1;
        internal DevExpress.XtraBars.Bar Bar2;
        internal DevExpress.XtraBars.Bar Bar3;
        internal DevExpress.XtraBars.BarDockControl barDockControlTop;
        internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
        internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
        internal DevExpress.XtraBars.BarDockControl barDockControlRight;
        internal DevExpress.XtraEditors.PanelControl PanelControl1;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Item;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Date;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ReconDate;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Amount;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Payee;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Status;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_TrustRegister;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ReconDetail;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.TabPage TabPage2;
        internal System.Windows.Forms.TabPage TabPage3;
        internal System.Windows.Forms.TabPage TabPage4;
        internal System.Windows.Forms.TabPage TabPage5;
        internal System.Windows.Forms.TabPage TabPage6;
        internal System.Windows.Forms.TabPage TabPage7;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_status;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_recon_count;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_recon_amount;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_item_count;
        internal DevExpress.XtraBars.BarStaticItem BarStaticItem_recon_date;
        internal DevExpress.XtraBars.BarSubItem BarSubItem1;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Print;
        internal DevExpress.XtraBars.BarButtonItem BarButtonItem_File_Exit;

        internal System.Windows.Forms.TabPage TabPage8;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReconcileItemsForm));
            this.mnuChangeItem = new System.Windows.Forms.MenuItem();
            this.mnuChangeDate = new System.Windows.Forms.MenuItem();
            this.mnuCreateItem = new System.Windows.Forms.MenuItem();
            this.mnuChangeStatus = new System.Windows.Forms.MenuItem();
            this.ContextMenu1 = new System.Windows.Forms.ContextMenu();
            this.MenuItem1 = new System.Windows.Forms.MenuItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.Bar2 = new DevExpress.XtraBars.Bar();
            this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.BarButtonItem_File_Print = new DevExpress.XtraBars.BarButtonItem();
            this.BarButtonItem_File_Exit = new DevExpress.XtraBars.BarButtonItem();
            this.Bar3 = new DevExpress.XtraBars.Bar();
            this.BarStaticItem_status = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_recon_count = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_recon_amount = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_item_count = new DevExpress.XtraBars.BarStaticItem();
            this.BarStaticItem_recon_date = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_Item = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_ReconDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Amount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Payee = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_TrustRegister = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_ReconDetail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.TabPage3 = new System.Windows.Forms.TabPage();
            this.TabPage4 = new System.Windows.Forms.TabPage();
            this.TabPage5 = new System.Windows.Forms.TabPage();
            this.TabPage6 = new System.Windows.Forms.TabPage();
            this.TabPage7 = new System.Windows.Forms.TabPage();
            this.TabPage8 = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.PanelControl1).BeginInit();
            this.PanelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.TabControl1.SuspendLayout();
            this.SuspendLayout();
            //
            //mnuChangeItem
            //
            this.mnuChangeItem.Index = 0;
            this.mnuChangeItem.Text = "&Reconcile Selected Item(s)";
            //
            //mnuChangeDate
            //
            this.mnuChangeDate.Index = 1;
            this.mnuChangeDate.Text = "Change Reconcile &Date";
            //
            //mnuCreateItem
            //
            this.mnuCreateItem.Index = 2;
            this.mnuCreateItem.Text = "&New Item";
            //
            //mnuChangeStatus
            //
            this.mnuChangeStatus.Index = 3;
            this.mnuChangeStatus.Text = "Change Status to FOUND";
            //
            //ContextMenu1
            //
            this.ContextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
                this.mnuChangeItem,
                this.mnuChangeDate,
                this.mnuCreateItem,
                this.mnuChangeStatus
            });
            //
            //MenuItem1
            //
            this.MenuItem1.Index = -1;
            this.MenuItem1.Text = "";
            //
            //barManager1
            //
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
                this.Bar2,
                this.Bar3
            });
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
                this.BarStaticItem_status,
                this.BarStaticItem_recon_count,
                this.BarStaticItem_recon_amount,
                this.BarStaticItem_item_count,
                this.BarStaticItem_recon_date,
                this.BarSubItem1,
                this.BarButtonItem_File_Print,
                this.BarButtonItem_File_Exit
            });
            this.barManager1.MainMenu = this.Bar2;
            this.barManager1.MaxItemId = 8;
            this.barManager1.StatusBar = this.Bar3;
            //
            //Bar2
            //
            this.Bar2.BarName = "Main menu";
            this.Bar2.DockCol = 0;
            this.Bar2.DockRow = 0;
            this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1) });
            this.Bar2.OptionsBar.MultiLine = true;
            this.Bar2.OptionsBar.UseWholeRow = true;
            this.Bar2.Text = "Main menu";
            //
            //BarSubItem1
            //
            this.BarSubItem1.Caption = "&File";
            this.BarSubItem1.Id = 5;
            this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Print),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_File_Exit)
            });
            this.BarSubItem1.Name = "BarSubItem1";
            //
            //BarButtonItem_File_Print
            //
            this.BarButtonItem_File_Print.Caption = "&Print...";
            this.BarButtonItem_File_Print.Id = 6;
            this.BarButtonItem_File_Print.Name = "BarButtonItem_File_Print";
            //
            //BarButtonItem_File_Exit
            //
            this.BarButtonItem_File_Exit.Caption = "&Exit";
            this.BarButtonItem_File_Exit.Id = 7;
            this.BarButtonItem_File_Exit.Name = "BarButtonItem_File_Exit";
            //
            //Bar3
            //
            this.Bar3.BarName = "Status bar";
            this.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.Bar3.DockCol = 0;
            this.Bar3.DockRow = 0;
            this.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.Bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
                new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_status),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_recon_count),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_recon_amount),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_item_count),
                new DevExpress.XtraBars.LinkPersistInfo(this.BarStaticItem_recon_date)
            });
            this.Bar3.OptionsBar.AllowQuickCustomization = false;
            this.Bar3.OptionsBar.DrawDragBorder = false;
            this.Bar3.OptionsBar.DrawSizeGrip = true;
            this.Bar3.OptionsBar.UseWholeRow = true;
            this.Bar3.Text = "Status bar";
            //
            //BarStaticItem_status
            //
            this.BarStaticItem_status.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.BarStaticItem_status.Id = 0;
            this.BarStaticItem_status.Name = "BarStaticItem_status";
            this.BarStaticItem_status.TextAlignment = System.Drawing.StringAlignment.Near;
            this.BarStaticItem_status.Width = 32;
            //
            //BarStaticItem_recon_count
            //
            this.BarStaticItem_recon_count.Caption = "Reconciled 0 items";
            this.BarStaticItem_recon_count.Id = 1;
            this.BarStaticItem_recon_count.Name = "BarStaticItem_recon_count";
            this.BarStaticItem_recon_count.TextAlignment = System.Drawing.StringAlignment.Near;
            //
            //BarStaticItem_recon_amount
            //
            this.BarStaticItem_recon_amount.Caption = "for $0.00";
            this.BarStaticItem_recon_amount.Id = 2;
            this.BarStaticItem_recon_amount.Name = "BarStaticItem_recon_amount";
            this.BarStaticItem_recon_amount.TextAlignment = System.Drawing.StringAlignment.Near;
            //
            //BarStaticItem_item_count
            //
            this.BarStaticItem_item_count.Caption = "0 items";
            this.BarStaticItem_item_count.Id = 3;
            this.BarStaticItem_item_count.Name = "BarStaticItem_item_count";
            this.BarStaticItem_item_count.TextAlignment = System.Drawing.StringAlignment.Near;
            //
            //BarStaticItem_recon_date
            //
            this.BarStaticItem_recon_date.Caption = "MM/dd/yyyy";
            this.BarStaticItem_recon_date.Id = 4;
            this.BarStaticItem_recon_date.Name = "BarStaticItem_recon_date";
            this.BarStaticItem_recon_date.TextAlignment = System.Drawing.StringAlignment.Near;
            //
            //barDockControlTop
            //
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(588, 22);
            //
            //barDockControlBottom
            //
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 582);
            this.barDockControlBottom.Size = new System.Drawing.Size(588, 26);
            //
            //barDockControlLeft
            //
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 560);
            //
            //barDockControlRight
            //
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(588, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 560);
            //
            //PanelControl1
            //
            this.PanelControl1.Controls.Add(this.GridControl1);
            this.PanelControl1.Controls.Add(this.Button_OK);
            this.PanelControl1.Controls.Add(this.TabControl1);
            this.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelControl1.Location = new System.Drawing.Point(0, 22);
            this.PanelControl1.Name = "PanelControl1";
            this.PanelControl1.Size = new System.Drawing.Size(588, 560);
            this.PanelControl1.TabIndex = 11;
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.ContextMenu = this.ContextMenu1;
            this.GridControl1.Location = new System.Drawing.Point(2, 29);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(584, 493);
            this.GridControl1.TabIndex = 9;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_Item,
                this.GridColumn_Date,
                this.GridColumn_ReconDate,
                this.GridColumn_Amount,
                this.GridColumn_Payee,
                this.GridColumn_Status,
                this.GridColumn_TrustRegister,
                this.GridColumn_ReconDetail
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsSelection.InvertSelection = true;
            this.GridView1.OptionsSelection.MultiSelect = true;
            this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.Add(GridColumn_Item, DevExpress.Data.ColumnSortOrder.Ascending);
            //
            //GridColumn_Item
            //
            this.GridColumn_Item.Caption = "Item";
            this.GridColumn_Item.FieldName = "checknum";
            this.GridColumn_Item.Name = "GridColumn_Item";
            this.GridColumn_Item.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Item.Visible = true;
            this.GridColumn_Item.VisibleIndex = 0;
            this.GridColumn_Item.Width = 83;
            this.GridColumn_Item.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
            //
            //GridColumn_Date
            //
            this.GridColumn_Date.Caption = "Date";
            this.GridColumn_Date.DisplayFormat.FormatString = "d";
            this.GridColumn_Date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Date.FieldName = "item_date";
            this.GridColumn_Date.Name = "GridColumn_Date";
            this.GridColumn_Date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Date.Visible = true;
            this.GridColumn_Date.VisibleIndex = 1;
            this.GridColumn_Date.Width = 93;
            //
            //GridColumn_ReconDate
            //
            this.GridColumn_ReconDate.Caption = "Recon";
            this.GridColumn_ReconDate.DisplayFormat.FormatString = "d";
            this.GridColumn_ReconDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_ReconDate.FieldName = "recon_date";
            this.GridColumn_ReconDate.Name = "GridColumn_ReconDate";
            this.GridColumn_ReconDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_ReconDate.Visible = true;
            this.GridColumn_ReconDate.VisibleIndex = 2;
            this.GridColumn_ReconDate.Width = 99;
            //
            //GridColumn_Amount
            //
            this.GridColumn_Amount.Caption = "Amount";
            this.GridColumn_Amount.DisplayFormat.FormatString = "c2";
            this.GridColumn_Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Amount.FieldName = "item_amount";
            this.GridColumn_Amount.Name = "GridColumn_Amount";
            this.GridColumn_Amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Amount.Visible = true;
            this.GridColumn_Amount.VisibleIndex = 3;
            this.GridColumn_Amount.Width = 92;
            //
            //GridColumn_Payee
            //
            this.GridColumn_Payee.Caption = "Payee";
            this.GridColumn_Payee.FieldName = "payee";
            this.GridColumn_Payee.Name = "GridColumn_Payee";
            this.GridColumn_Payee.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Payee.Visible = true;
            this.GridColumn_Payee.VisibleIndex = 4;
            this.GridColumn_Payee.Width = 305;
            //
            //GridColumn_Status
            //
            this.GridColumn_Status.Caption = "Status";
            this.GridColumn_Status.FieldName = "message";
            this.GridColumn_Status.Name = "GridColumn_Status";
            this.GridColumn_Status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Status.Visible = true;
            this.GridColumn_Status.VisibleIndex = 5;
            this.GridColumn_Status.Width = 118;
            //
            //GridColumn_TrustRegister
            //
            this.GridColumn_TrustRegister.Caption = "TrustRegister";
            this.GridColumn_TrustRegister.DisplayFormat.FormatString = "f0";
            this.GridColumn_TrustRegister.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_TrustRegister.FieldName = "trust_register";
            this.GridColumn_TrustRegister.Name = "GridColumn_TrustRegister";
            this.GridColumn_TrustRegister.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //GridColumn_ReconDetail
            //
            this.GridColumn_ReconDetail.Caption = "ReconDetail";
            this.GridColumn_ReconDetail.DisplayFormat.FormatString = "f0";
            this.GridColumn_ReconDetail.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_ReconDetail.FieldName = "recon_detail";
            this.GridColumn_ReconDetail.Name = "GridColumn_ReconDetail";
            this.GridColumn_ReconDetail.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(251, 529);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 25);
            this.Button_OK.TabIndex = 8;
            this.Button_OK.Text = "&OK";
            //
            //TabControl1
            //
            this.TabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.TabPage3);
            this.TabControl1.Controls.Add(this.TabPage4);
            this.TabControl1.Controls.Add(this.TabPage5);
            this.TabControl1.Controls.Add(this.TabPage6);
            this.TabControl1.Controls.Add(this.TabPage7);
            this.TabControl1.Controls.Add(this.TabPage8);
            this.TabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TabControl1.Location = new System.Drawing.Point(2, 2);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(584, 26);
            this.TabControl1.TabIndex = 7;
            //
            //TabPage1
            //
            this.TabPage1.Location = new System.Drawing.Point(4, 26);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Size = new System.Drawing.Size(576, 0);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Tag = "AD";
            this.TabPage1.Text = "Checks";
            //
            //TabPage2
            //
            this.TabPage2.Location = new System.Drawing.Point(4, 26);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Size = new System.Drawing.Size(576, 0);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Tag = "DP";
            this.TabPage2.Text = "Deposits";
            //
            //TabPage3
            //
            this.TabPage3.Location = new System.Drawing.Point(4, 26);
            this.TabPage3.Name = "TabPage3";
            this.TabPage3.Size = new System.Drawing.Size(576, 0);
            this.TabPage3.TabIndex = 2;
            this.TabPage3.Tag = "BW";
            this.TabPage3.Text = "Wire Transfers";
            //
            //TabPage4
            //
            this.TabPage4.Location = new System.Drawing.Point(4, 26);
            this.TabPage4.Name = "TabPage4";
            this.TabPage4.Size = new System.Drawing.Size(576, 0);
            this.TabPage4.TabIndex = 3;
            this.TabPage4.Tag = "BI";
            this.TabPage4.Text = "Interest";
            //
            //TabPage5
            //
            this.TabPage5.Location = new System.Drawing.Point(4, 26);
            this.TabPage5.Name = "TabPage5";
            this.TabPage5.Size = new System.Drawing.Size(576, 0);
            this.TabPage5.TabIndex = 4;
            this.TabPage5.Tag = "SC";
            this.TabPage5.Text = "Service Charges";
            //
            //TabPage6
            //
            this.TabPage6.Location = new System.Drawing.Point(4, 26);
            this.TabPage6.Name = "TabPage6";
            this.TabPage6.Size = new System.Drawing.Size(576, 0);
            this.TabPage6.TabIndex = 5;
            this.TabPage6.Tag = "RF";
            this.TabPage6.Text = "Creditor Refunds";
            //
            //TabPage7
            //
            this.TabPage7.Location = new System.Drawing.Point(4, 26);
            this.TabPage7.Name = "TabPage7";
            this.TabPage7.Size = new System.Drawing.Size(576, 0);
            this.TabPage7.TabIndex = 6;
            this.TabPage7.Tag = "RR";
            this.TabPage7.Text = "EFT Rejects";
            //
            //TabPage8
            //
            this.TabPage8.Location = new System.Drawing.Point(4, 26);
            this.TabPage8.Name = "TabPage8";
            this.TabPage8.Size = new System.Drawing.Size(576, 0);
            this.TabPage8.TabIndex = 7;
            this.TabPage8.Tag = "BE";
            this.TabPage8.Text = "Errors";
            //
            //ReconcileItemsForm
            //
            this.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 14f);
            this.ClientSize = new System.Drawing.Size(588, 608);
            this.Controls.Add(this.PanelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Location = new System.Drawing.Point(11, 49);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.Name = "ReconcileItemsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
            this.Text = "Reconciliation Items";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.PanelControl1).EndInit();
            this.PanelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.TabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion "Windows Form Designer generated code "

        private Int32 RightClickItem;
        private System.DateTime ReconDate = DateTime.Now.Date;

        private Int32 ReconBatch;
        /// <summary>
        /// When the form is closed, save the changes.
        /// </summary>

        private bool NormalClose;

        private void ReconcileItemsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!NormalClose)
            {
                switch (DebtPlus.Data.Forms.MessageBox.Show("Do you wish to save the changes?", "Save Changes?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Yes:
                        SaveChanges();
                        break;

                    case DialogResult.No:
                        break;

                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Handle the form receiving focus
        /// </summary>

        private void ReconcileItemsForm_GotFocus(object sender, EventArgs e)
        {
            // Give the focus to the grid control so that clicking will work immediately
            GridView1.Focus();
        }

        /// <summary>
        /// Process the load event for the form
        /// </summary>
        private void frmItems_Load(object Sender, EventArgs e)
        {
            BarStaticItem_recon_date.Caption = ReconDate.ToShortDateString();

            // Select the last item in the list and load its information
            TabControl1.SelectedIndex = TabControl1.TabCount - 1;
            load_list();

            // Hook into the events that indicate that the tab is being changed
            TabControl1.SelectedIndexChanged += TabControl1_SelectedIndexChanged;
        }

        /// <summary>
        /// The tab is being changed in the tab list
        /// </summary>
        private void TabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            load_list();
        }

        /// <summary>
        /// Refresh the list of items to be reconciled
        /// </summary>
        public void load_list()
        {
            var _with1 = bt_reader;
            if (!_with1.IsBusy)
            {
                SaveChanges();
                ds.Clear();
                _with1.RunWorkerAsync(TransactionType);
            }
        }

        private BackgroundWorker bt_reader = new BackgroundWorker();

        private DataSet ds = new DataSet("ds");
        /// <summary>
        /// Thread to refresh the list of items
        /// </summary>

        private void bt_reader_DoWork(object sender, DoWorkEventArgs e)
        {
            // Clear the dataset and start with a new copy
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with2.CommandText = "xpr_recon_item_list";
                    _with2.CommandType = CommandType.StoredProcedure;
                    var _with3 = _with2.Parameters;
                    _with3.Add("@recon_batch", SqlDbType.Int).Value = ReconBatch;
                    _with3.Add("@tran_type", SqlDbType.VarChar, 10).Value = e.Argument;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "xpr_recon_item_list");
                    }
                }

                var _with4 = ds.Tables["xpr_recon_item_list"];

                // Define the primary key to the table for new additions
                var _with5 = _with4.Columns["recon_detail"];
                _with5.AutoIncrement = true;
                _with5.AutoIncrementSeed = -1;
                _with5.AutoIncrementStep = -1;

                if (!_with4.Columns.Contains("tran_type"))
                {
                    _with4.Columns.Add("tran_type", typeof(string));
                }

                // Add the "cleared" column to the result
                if (!_with4.Columns.Contains("cleared"))
                {
                    var _with6 = _with4.Columns.Add("cleared", typeof(string));
                    _with6.DefaultValue = STR_UNRECONCILED;
                }

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    row["tran_type"] = e.Argument;
                    if (row["cleared"] == null || object.ReferenceEquals(row["cleared"], DBNull.Value))
                    {
                        row["cleared"] = STR_UNRECONCILED;
                    }
                    row.AcceptChanges();
                }
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // The result is the pointer to the new table with the results
            e.Result = ds.Tables["xpr_recon_item_list"];
        }

        /// <summary>
        /// Save the changes to the recon table when the itesm are switched
        /// </summary>

        private void SaveChanges()
        {
            // The view may be nothing when we first start.
            DataView vue = GridControl1.DataSource as DataView;
            if (vue == null)
                return;

            // From the view, find the current table to be updated.
            DataTable tbl = vue.Table;
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlTransaction txn = null;

            SqlCommand UpdateCmd = new SqlCommand();
            SqlCommand DeleteCmd = new SqlCommand();

            // Find the items that would normally be added but are now no longer required.
            DataRow[] NewItems = tbl.Select("([recon_detail] is null) AND ([recon_date] is null)", string.Empty, DataViewRowState.ModifiedCurrent);
            foreach (DataRow row in NewItems)
            {
                row.AcceptChanges();
            }

            try
            {
                cn.Open();
                txn = cn.BeginTransaction();

                // Find the items that do not have a recon_detail item. These need to be added. They say "modified", but "added" is what they need to be.
                DataRow[] AddedRows = tbl.Select("([recon_detail] is null) AND ([recon_date] is not null)", string.Empty, DataViewRowState.ModifiedCurrent);
                if (AddedRows.GetUpperBound(0) >= 0)
                {
                    using (SqlCommand AddedCmd = new SqlCommand())
                    {
                        var _with7 = AddedCmd;
                        _with7.Connection = cn;
                        _with7.Transaction = txn;
                        _with7.CommandText = "xpr_insert_recon_detail";
                        _with7.CommandType = CommandType.StoredProcedure;
                        var _with8 = _with7.Parameters;
                        _with8.Add("@RETURN_VALUE", SqlDbType.Int, 4, "recon_detail").Direction = ParameterDirection.ReturnValue;
                        _with8.Add("@Batch", SqlDbType.Int).Value = ReconBatch;
                        _with8.Add("@TrustRegister", SqlDbType.Int, 4, "trust_register");
                        _with8.Add("@Cleared", SqlDbType.VarChar, 1, "cleared");
                        _with8.Add("@ReconDate", SqlDbType.DateTime, 8, "recon_date");
                        _with8.Add("@ReconAmount", SqlDbType.Decimal, 8, "item_amount");
                        _with8.Add("@tran_type", SqlDbType.VarChar, 2, "tran_type");
                        _with8.Add("@Comments", SqlDbType.VarChar, 80, "message");

                        // These parameters are not needed since we specify the trust register directly.
                        _with8.Add("@CheckNumber", SqlDbType.VarChar, 50).Value = DBNull.Value;
                        _with8.Add("@Bank", SqlDbType.Int).Value = DBNull.Value;

                        using (SqlDataAdapter da = new SqlDataAdapter())
                        {
                            da.UpdateCommand = AddedCmd;
                            da.AcceptChangesDuringUpdate = true;
                            da.ContinueUpdateOnError = true;
                            da.Update(AddedRows);
                        }
                    }
                }

                // Do the normal operations at this point
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    // Update command
                    var _with9 = UpdateCmd;
                    _with9.Connection = cn;
                    _with9.Transaction = txn;
                    _with9.CommandText = "UPDATE recon_details SET recon_batch=@recon_batch,amount=@item_amount,cleared=@cleared,recon_date=@recon_date,message=@message WHERE recon_detail=@recon_detail";
                    var _with10 = _with9.Parameters;
                    _with10.Add("@tran_type", SqlDbType.VarChar, 2, "tran_type");
                    _with10.Add("@recon_batch", SqlDbType.Int, 4).Value = ReconBatch;
                    _with10.Add("@item_amount", SqlDbType.Decimal, 8, "item_amount");
                    _with10.Add("@cleared", SqlDbType.VarChar, 1, "cleared");
                    _with10.Add("@recon_date", SqlDbType.DateTime, 8, "recon_date");
                    _with10.Add("@message", SqlDbType.VarChar, 80, "message");
                    _with10.Add("@recon_detail", SqlDbType.Int, 4, "recon_detail");
                    da.UpdateCommand = UpdateCmd;

                    // Delete command
                    var _with11 = DeleteCmd;
                    _with11.Connection = cn;
                    _with11.Transaction = txn;
                    _with11.CommandText = "DELETE FROM recon_details WHERE recon_detail=@recon_detail";
                    var _with12 = _with11.Parameters;
                    _with12.Add("@recon_detail", SqlDbType.Int, 0, "recon_detail");
                    da.DeleteCommand = DeleteCmd;

                    // Perform the update on the table with the change
                    da.ContinueUpdateOnError = true;
                    da.Update(tbl);

                    // Do the accept changes on all of the rows so that the ones added without a recon date
                    // will not be used again.
                    tbl.AcceptChanges();
                }

                txn.Commit();
                txn.Dispose();
                txn = null;
            }
            catch (SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating recon items");
            }
            finally
            {
                UpdateCmd.Dispose();
                DeleteCmd.Dispose();

                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch
                    {
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();
            }
        }

        /// <summary>
        /// The thread refresh cycle has completed
        /// </summary>
        private void bt_reader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DataTable tbl = (DataTable)e.Result;

            // Update the pannel with the number of items
            if (tbl != null)
            {
                UpdateTable = tbl;
                BarStaticItem_item_count.Caption = string.Format("{0:n0} item(s)", tbl.Rows.Count);

                // Bind the data to the control
                var _with13 = GridControl1;
                _with13.DataSource = UpdateTable.DefaultView;
                _with13.RefreshDataSource();
                UpdateClearedCount();
            }
        }

        /// <summary>
        /// Group of items to be reconciled
        /// </summary>
        private string TransactionType
        {
            get { return Convert.ToString(TabControl1.SelectedTab.Tag); }
        }

        /// <summary>
        /// Process the table changes
        /// </summary>

        private void UpdateTable_ColumnChanged(object sender, DataRowChangeEventArgs e)
        {
            // If the row count is the reconciled date then update the cleared counnt
            if (e.Action == DataRowAction.Change)
            {
                UpdateClearedCount();
            }
        }

        /// <summary>
        /// Update the statusbar with the cleared information
        /// </summary>

        private void UpdateClearedCount()
        {
            DataView vue = (DataView)GridControl1.DataSource;
            object objCount = 0;
            object ReconciledAmount = 0m;

            if (vue != null)
            {
                var _with14 = vue.Table;
                objCount = _with14.Compute("count(item_amount)", "[recon_date] is not null");
                ReconciledAmount = _with14.Compute("sum(item_amount)", "[recon_date] is not null");
            }

            if (object.ReferenceEquals(objCount, DBNull.Value))
                objCount = 0;
            if (object.ReferenceEquals(ReconciledAmount, DBNull.Value))
                ReconciledAmount = 0m;

            // Update the status bar
            BarStaticItem_recon_count.Caption = string.Format("Reconciled {0:n0} items", Convert.ToInt32(objCount));
            BarStaticItem_recon_amount.Caption = string.Format("for {0:c}", Convert.ToDecimal(ReconciledAmount));
        }

        /// <summary>
        /// Process a click event on an item. Reconcile the item in the proper column.
        /// </summary>
        private void GridView1_Click(object sender, EventArgs e)
        {
            GridView gv = GridView1;
            GridHitInfo hi = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)));

            // Look for a click in the reconciled date for an item
            if (hi.IsValid && hi.InRowCell)
            {
                Int32 RowHandle = hi.RowHandle;

                GridColumn Col = hi.Column;
                if (string.Compare(Col.FieldName, "recon_date", false) == 0)
                {
                    DataRowView drv = (DataRowView)GridView1.GetRow(RowHandle);
                    drv.BeginEdit();

                    // There must be a trust register item for this purpose. Create one if not present.
                    if (object.ReferenceEquals(drv["trust_register"], DBNull.Value) && TransactionType != "AD")
                    {
                        SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        try
                        {
                            cn.Open();
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                var _with15 = cmd;
                                _with15.Connection = cn;
                                _with15.CommandText = string.Format("xpr_trust_register_create_{0}", TransactionType);
                                _with15.CommandType = CommandType.StoredProcedure;
                                var _with16 = _with15.Parameters;
                                _with16.Add("@RETURN_VALUE", SqlDbType.Int, 0).Direction = ParameterDirection.ReturnValue;
                                _with16.Add("@cleared", SqlDbType.VarChar, 2).Value = STR_UNRECONCILED;
                                _with16.Add("@amount", SqlDbType.Money).Value = drv["item_amount"];
                                drv["trust_register"] = _with15.Parameters["@RETURN_VALUE"].Value;
                            }
                        }
                        catch (SqlException ex)
                        {
                            global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating trust register element");
                        }
                        finally
                        {
                            if (cn != null)
                                cn.Dispose();
                        }
                    }

                    // Find the row to be updated. If there is no recon item then set the row as "added" to include a new row.
                    if (object.ReferenceEquals(drv["recon_date"], DBNull.Value))
                    {
                        drv["recon_date"] = ReconDate;
                        drv["cleared"] = STR_RECONCILED;
                    }
                    else
                    {
                        drv["recon_date"] = DBNull.Value;
                        drv["cleared"] = STR_UNRECONCILED;
                    }

                    drv.EndEdit();
                }
            }
        }

        /// <summary>
        /// A keypress event occurred on the list.
        /// </summary>

        private void GridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Control-A : Select all of the rows
            if ((int)e.KeyChar == 1)
            {
                var _with17 = GridView1;
                _with17.SelectAll();
                _with17.InvalidateRows();
            }

            // Escape : Clear the selection of the rows
            if ((int)e.KeyChar == 39)
            {
                var _with18 = GridView1;
                _with18.ClearSelection();
                _with18.InvalidateRows();
            }
        }

        /// <summary>
        /// Change the date for reconciliation
        /// </summary>
        public void mnuChangeDate_Click(object eventSender, EventArgs eventArgs)
        {
            using (ReconcilationDateForm frm = new ReconcilationDateForm())
            {
                var _with19 = frm;
                _with19.Parameter_FormDate = ReconDate;
                _with19.ShowDialog();
                ReconDate = _with19.Parameter_FormDate;
            }

            BarStaticItem_recon_date.Caption = ReconDate.ToShortDateString();
        }

        /// <summary>
        /// Process a change on the menu item
        /// </summary>
        public void mnuChangeItem_Click(object Sender, EventArgs e)
        {
            var _with20 = GridView1;
            _with20.BeginUpdate();

            // Process the rows. Look for a row with a reconciled date and change the reconciled status
            Int32[] SelectedRows = _with20.GetSelectedRows();
            foreach (Int32 RowHandle in SelectedRows)
            {
                DataRowView drv = (DataRowView)_with20.GetRow(RowHandle);

                // If there is a date then remove it. Otherwise, set it.
                //If drv["recon_detail"] Is System.DBNull.Value AndAlso drv.Row.RowState = DataRowState.Unchanged Then drv.Row.SetAdded()
                drv.BeginEdit();

                if (object.ReferenceEquals(drv["recon_date"], DBNull.Value))
                {
                    drv["recon_date"] = ReconDate;
                    drv["cleared"] = STR_RECONCILED;
                }
                else
                {
                    drv["recon_date"] = DBNull.Value;
                    drv["cleared"] = STR_UNRECONCILED;
                }
                drv.EndEdit();
            }

            // Once all of the rows as addressed, remove the selected status
            _with20.ClearSelection();
            _with20.EndUpdate();
            _with20.InvalidateRows();
        }

        /// <summary>
        /// Change the status of item in the list
        /// </summary>
        public void mnuChangeStatus_Click(object Sender, EventArgs e)
        {
            var _with21 = GridView1;
            _with21.BeginUpdate();

            // Process the rows. Look for a row with a reconciled date and change the reconciled status
            Int32[] SelectedRows = _with21.GetSelectedRows();
            foreach (Int32 RowHandle in SelectedRows)
            {
                DataRowView drv = (DataRowView)_with21.GetRow(RowHandle);

                string Message = string.Empty;
                if (!object.ReferenceEquals(drv["message"], DBNull.Value))
                    Message = Convert.ToString(drv["message"]);
                if (string.Compare(Message, STR_NOTFOUND, false) == 0)
                {
                    if (object.ReferenceEquals(drv["recon_detail"], DBNull.Value))
                        drv.Row.SetAdded();
                    drv.BeginEdit();
                    drv["message"] = STR_FOUND;
                    drv.EndEdit();
                }
            }

            // Once all of the rows as addressed, remove the selected status
            _with21.ClearSelection();
            _with21.EndUpdate();
            _with21.InvalidateRows();
        }

        /// <summary>
        /// Create a new item when desired
        /// </summary>

        public void mnuCreateItem_Click(object Sender, EventArgs e)
        {
            // We only permit specific items to be created.
            switch (TransactionType)
            {
                case "SC":
                case "BI":
                case "BE":

                    // Ask for the date and the amount. If successful then create the new item.
                    using (ServiceChargeForm frm = new ServiceChargeForm())
                    {
                        var _with22 = frm;
                        if (_with22.ShowDialog() == DialogResult.OK)
                        {
                            System.DateTime DateCreated = _with22.Parameter_Date;
                            decimal Amount = _with22.Parameter_Amount;

                            Int32 TrustRegister = 0;
                            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

                            Cursor current_cursor = Cursor.Current;
                            Cursor.Current = Cursors.WaitCursor;
                            try
                            {
                                cn.Open();
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    var _with23 = cmd;
                                    _with23.Connection = cn;
                                    _with23.CommandType = CommandType.StoredProcedure;
                                    _with23.CommandText = string.Format("xpr_trust_register_create_{0}", TransactionType);
                                    var _with24 = _with23.Parameters;
                                    _with24.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                                    _with24.Add("@Amount", SqlDbType.Decimal).Value = Amount;
                                    _with24.Add("@ItemDate", SqlDbType.DateTime).Value = DateCreated;
                                    _with24.Add("@Cleared", SqlDbType.VarChar, 1).Value = STR_UNRECONCILED;
                                    _with23.ExecuteNonQuery();
                                    object objResult = _with23.Parameters["@RETURN_VALUE"].Value;
                                    if (objResult != null && !object.ReferenceEquals(objResult, DBNull.Value))
                                        TrustRegister = Convert.ToInt32(objResult);
                                }

                                if (TrustRegister > 0)
                                {
                                    using (DataView vue = (DataView)GridControl1.DataSource)
                                    {
                                        var _with25 = vue;
                                        DataRowView drv = vue.AddNew();
                                        drv.BeginEdit();
                                        drv["trust_register"] = TrustRegister;
                                        drv["tran_type"] = TransactionType;
                                        drv["amount"] = Amount;
                                        drv["cleared"] = STR_RECONCILED;
                                        drv["recon_date"] = ReconDate;
                                        drv.EndEdit();
                                    }
                                }
                            }
                            finally
                            {
                                if (cn != null)
                                    cn.Dispose();
                                Cursor = current_cursor;
                            }
                        }
                    }

                    break;

                default:
                    DebtPlus.Data.Forms.MessageBox.Show("The item can not be created because it involves normal processing of DebtPlus. You may wish to enter a BANK ERROR or SERVICE CHARGE or BANK INTEREST to adjust the trust register but items such as DEPOSITS or CHECKS may only be created by doing the deposit or disbursement operation", "Sorry, the transaction can not be created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
            }
        }

        /// <summary>
        /// Close the program based upon the menu pick
        /// </summary>
        private void BarButtonItem_File_Exit_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (!Modal)
                Close();
        }

        /// <summary>
        /// Print the reconciliation batch report
        /// </summary>

        private void BarButtonItem_File_Print_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveChanges();

            DebtPlus.Reports.Reconcile.Detail.CheckReconcileDetailListReport rpt = new DebtPlus.Reports.Reconcile.Detail.CheckReconcileDetailListReport();
            rpt.Parameter_BatchID = ReconBatch;
            if (rpt.RequestReportParameters() == DialogResult.OK)
            {
                rpt.RunReportInSeparateThread();
            }
        }

        /// <summary>
        /// Process a double-click on the status panel
        /// </summary>

        private void BarStaticItem_recon_date_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            // Look for a double-click on the date pannel to process the date update
            using (ReconcilationDateForm frm = new ReconcilationDateForm())
            {
                var _with26 = frm;
                _with26.Parameter_FormDate = ReconDate;
                _with26.ShowDialog();
                ReconDate = _with26.Parameter_FormDate;
            }

            BarStaticItem_recon_date.Caption = ReconDate.ToShortDateString();
        }

        /// <summary>
        /// Popup event for the grid control
        /// </summary>

        private void ContextMenu1_Popup(object sender, EventArgs e)
        {
            try
            {
                // Find which row the click event occurred
                GridView gv = GridView1;
                GridHitInfo hi = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)));

                // Save the handle for the menu pick event.
                RightClickItem = hi.IsValid && hi.InRowCell ? hi.RowHandle : -1;

                // If the row handle is valid then enable the menu items
                mnuChangeItem.Enabled = (RightClickItem >= 0);
                mnuChangeStatus.Enabled = (RightClickItem >= 0);

                // Change the text for the menu items
                mnuChangeDate.Text = string.Format("Change &Date from {0:d} ...", ReconDate);

                // If the transaction type is valid then allow the create to work
                switch (TransactionType)
                {
                    case "BE":
                    case "BI":
                    case "SC":
                        mnuCreateItem.Enabled = true;
                        break;

                    default:
                        mnuCreateItem.Enabled = false;
                        break;
                }

                // Determine if there are items selected
                Int32 nStatus = 0;
                Int32 nCount = 0;
                Int32[] selectedRows = GridView1.GetSelectedRows();
                if (selectedRows != null)
                {
                    foreach (Int32 rowHandle in selectedRows)
                    {
                        nCount += 1;

                        // Determine if the selected items are reconciled or not. It is valid to have a mixture.
                        DataRowView drv = GridView1.GetRow(rowHandle) as DataRowView as DataRowView;
                        if (drv != null)
                        {
                            if (object.ReferenceEquals(drv["recon_date"], DBNull.Value))
                            {
                                nStatus = nStatus | 1;
                            }
                            else
                            {
                                nStatus = nStatus | 2;
                            }
                        }
                    }
                }

                // Based upon the selected items, determine the appropriate message for the function
                var _with27 = mnuChangeItem;
                switch (nStatus)
                {
                    case 0:
                        _with27.Text = "";
                        _with27.Enabled = false;
                        _with27.Visible = false;
                        break;

                    case 1:
                        _with27.Text = nCount == 1 ? string.Format("Reconcile the item to {0:d}", ReconDate) : string.Format("Reconcile all items to {0:d}", ReconDate);
                        _with27.Enabled = true;
                        _with27.Visible = true;
                        break;

                    case 2:
                        _with27.Text = nCount == 1 ? "Un-Reconcile the item" : "Un-Reconcile all items";
                        _with27.Enabled = true;
                        _with27.Visible = true;
                        break;

                    case 3:
                        _with27.Text = string.Format("Toggle reconciliation status to {0:d}", ReconDate);
                        _with27.Enabled = true;
                        _with27.Visible = true;
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error handling popup menu");
            }
        }

        /// <summary>
        /// Process the OK button
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (!Modal)
                Close();
        }
    }
}