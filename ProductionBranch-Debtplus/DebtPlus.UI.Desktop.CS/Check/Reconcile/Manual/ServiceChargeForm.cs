using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.Manual
{
    internal partial class ServiceChargeForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public ServiceChargeForm() : base()
        {
            InitializeComponent();
            this.Load += frmServiceCharge_Load;
            CalcEdit1.EditValueChanging += CalcEdit1_EditValueChanging;
            CalcEdit1.Enter += CalcEdit1_Enter;
        }

        private string _TransactionType = System.String.Empty;

        public string TransactionType
        {
            get { return _TransactionType; }
            set { _TransactionType = value; }
        }

        public System.DateTime Parameter_Date
        {
            get { return Convert.ToDateTime(DateEdit1.EditValue); }
        }

        public decimal Parameter_Amount
        {
            get { return Convert.ToDecimal(CalcEdit1.EditValue); }
        }

        #region "Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        public DevExpress.XtraEditors.LabelControl _Label1_2;
        public DevExpress.XtraEditors.LabelControl _Label1_1;
        public DevExpress.XtraEditors.LabelControl _Label1_0;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.CalcEdit CalcEdit1;

        internal DevExpress.XtraEditors.DateEdit DateEdit1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServiceChargeForm));
            this._Label1_2 = new DevExpress.XtraEditors.LabelControl();
            this._Label1_1 = new DevExpress.XtraEditors.LabelControl();
            this._Label1_0 = new DevExpress.XtraEditors.LabelControl();
            this.CalcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.DateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();

            ((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties.VistaTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties).BeginInit();
            this.SuspendLayout();

            //
            //_Label1_2
            //
            this._Label1_2.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this._Label1_2.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Label1_2.Appearance.Options.UseFont = true;
            this._Label1_2.Appearance.Options.UseForeColor = true;
            this._Label1_2.Appearance.Options.UseTextOptions = true;
            this._Label1_2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._Label1_2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Label1_2.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label1_2.Location = new System.Drawing.Point(53, 96);
            this._Label1_2.Name = "_Label1_2";
            this._Label1_2.Size = new System.Drawing.Size(62, 14);

            this._Label1_2.TabIndex = 3;
            this._Label1_2.Text = "Item Amount:";
            this._Label1_2.ToolTipController = this.ToolTipController1;
            //
            //_Label1_1
            //
            this._Label1_1.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this._Label1_1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Label1_1.Appearance.Options.UseFont = true;
            this._Label1_1.Appearance.Options.UseForeColor = true;
            this._Label1_1.Appearance.Options.UseTextOptions = true;
            this._Label1_1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._Label1_1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._Label1_1.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label1_1.Location = new System.Drawing.Point(53, 72);
            this._Label1_1.Name = "_Label1_1";
            this._Label1_1.Size = new System.Drawing.Size(47, 14);

            this._Label1_1.TabIndex = 1;
            this._Label1_1.Text = "Item Date:";
            this._Label1_1.ToolTipController = this.ToolTipController1;
            //
            //_Label1_0
            //
            this._Label1_0.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this._Label1_0.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this._Label1_0.Appearance.Options.UseFont = true;
            this._Label1_0.Appearance.Options.UseForeColor = true;
            this._Label1_0.Appearance.Options.UseTextOptions = true;
            this._Label1_0.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._Label1_0.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.None;
            this._Label1_0.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this._Label1_0.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._Label1_0.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._Label1_0.Cursor = System.Windows.Forms.Cursors.Default;
            this._Label1_0.Location = new System.Drawing.Point(8, 12);
            this._Label1_0.Name = "_Label1_0";
            this._Label1_0.Size = new System.Drawing.Size(278, 49);

            this._Label1_0.TabIndex = 0;
            this._Label1_0.Text = "This will allow you to create a reconciled item into your database. Only service " + "charges, bank errors, or interest items may be created.";
            this._Label1_0.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit1
            //
            this.CalcEdit1.Location = new System.Drawing.Point(157, 96);
            this.CalcEdit1.Name = "CalcEdit1";
            this.CalcEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit1.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit1.Properties.EditFormat.FormatString = "d2";
            this.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit1.Size = new System.Drawing.Size(88, 20);

            this.CalcEdit1.TabIndex = 7;
            this.CalcEdit1.ToolTipController = this.ToolTipController1;
            //
            //DateEdit1
            //
            this.DateEdit1.EditValue = new System.DateTime(2005, 10, 11, 0, 0, 0, 0);
            this.DateEdit1.Location = new System.Drawing.Point(157, 72);
            this.DateEdit1.Name = "DateEdit1";
            this.DateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            this.DateEdit1.Size = new System.Drawing.Size(88, 20);

            this.DateEdit1.TabIndex = 8;
            this.DateEdit1.ToolTipController = this.ToolTipController1;
            //
            //Button_OK
            //
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(64, 136);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);

            this.Button_OK.TabIndex = 9;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(160, 136);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);

            this.Button_Cancel.TabIndex = 10;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //ServiceChargeForm
            //
            this.AcceptButton = this.Button_OK;
            this.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(298, 176);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.DateEdit1);
            this.Controls.Add(this.CalcEdit1);
            this.Controls.Add(this._Label1_2);
            this.Controls.Add(this._Label1_1);
            this.Controls.Add(this._Label1_0);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServiceChargeForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Create New Non-Check Item";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();

            ((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties.VistaTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion "Windows Form Designer generated code "

        private void frmServiceCharge_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            Button_OK.Enabled = false;
            Button_Cancel.Enabled = true;
            DateEdit1.EditValue = System.DateTime.Now;
            CalcEdit1.EditValue = 0m;
        }

        private void CalcEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            decimal EditValue = 0;
            var _with1 = CalcEdit1;

            // Retrieve the value from the input.
            if (_with1.Text != System.String.Empty)
            {
                EditValue = Convert.ToDecimal(e.NewValue);
            }

            // Do not allow the value to go negative unless this is a bank error transaction
            if (EditValue < 0m && TransactionType != "BE")
            {
                Button_OK.Enabled = false;
                e.Cancel = true;
                DebtPlus.Data.Forms.DebtPlusForm.DoMessageBeep(DebtPlus.Data.Forms.DebtPlusForm.MessageBeepEnum.Simple);
            }
            else
            {
                Button_OK.Enabled = true;
            }
        }

        private void CalcEdit1_Enter(object sender, System.EventArgs e)
        {
            CalcEdit1.SelectAll();
        }
    }
}