using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.Manual
{
    internal partial class ReconcilationDateForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public ReconcilationDateForm() : base()
        {
            InitializeComponent();
            MonthCalendar1.DoubleClick += MonthCalendar1_DoubleClick;
            MonthCalendar1.EditDateModified += MonthCalendar1_DateSelected;
            this.Load += ReconcilationDateForm_Load;
        }

        private System.DateTime privateSelectedDate;

        public System.DateTime Parameter_FormDate
        {
            get { return privateSelectedDate; }
            set { privateSelectedDate = value; }
        }

        #region "Windows Form Designer generated code "

        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraScheduler.DateNavigator MonthCalendar1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReconcilationDateForm));
            this.MonthCalendar1 = new DevExpress.XtraScheduler.DateNavigator();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.MonthCalendar1).BeginInit();
            this.SuspendLayout();
            //
            //MonthCalendar1
            //
            this.MonthCalendar1.AppearanceCalendar.BackColor = System.Drawing.Color.Cornsilk;
            this.MonthCalendar1.AppearanceCalendar.ForeColor = System.Drawing.SystemColors.WindowText;
            this.MonthCalendar1.AppearanceCalendar.Options.UseBackColor = true;
            this.MonthCalendar1.AppearanceCalendar.Options.UseForeColor = true;
            this.MonthCalendar1.BoldAppointmentDates = false;
            this.MonthCalendar1.DateTime = new System.DateTime(2010, 4, 5, 0, 0, 0, 0);
            this.MonthCalendar1.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.MonthCalendar1.HotDate = null;
            this.MonthCalendar1.Location = new System.Drawing.Point(40, 8);
            this.MonthCalendar1.Multiselect = false;
            this.MonthCalendar1.Name = "MonthCalendar1";
            this.MonthCalendar1.Size = new System.Drawing.Size(179, 175);
            this.MonthCalendar1.TabIndex = 0;
            this.MonthCalendar1.WeekNumberRule = DevExpress.XtraEditors.Controls.WeekNumberRule.FirstDay;
            //
            //ReconcilationDateForm
            //
            this.Appearance.Font = new System.Drawing.Font("Times New Roman", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(258, 200);
            this.Controls.Add(this.MonthCalendar1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Location = new System.Drawing.Point(3, 22);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReconcilationDateForm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Reconciliation Date";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.MonthCalendar1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion "Windows Form Designer generated code "

        private bool InInit = true;

        private bool NewDate;

        private void MonthCalendar1_DoubleClick(object sender, System.EventArgs e)
        {
            if (NewDate)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void MonthCalendar1_DateSelected(object sender, System.EventArgs e)
        {
            if (!InInit)
            {
                Parameter_FormDate = MonthCalendar1.DateTime;
                NewDate = true;
            }
        }

        private void ReconcilationDateForm_Load(object sender, System.EventArgs e)
        {
            InInit = true;
            MonthCalendar1.Selection.Clear();
            MonthCalendar1.DateTime = Parameter_FormDate;
            NewDate = false;
            InInit = false;
        }
    }
}