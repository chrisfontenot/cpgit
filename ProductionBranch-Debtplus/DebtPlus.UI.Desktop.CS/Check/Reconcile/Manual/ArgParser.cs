using System;
using System.Windows.Forms;
using DebtPlus.UI.FormLib.Check;

namespace DebtPlus.UI.Desktop.CS.Check.Reconcile.Manual
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Batch ID
        /// </summary>
        private System.Int32 _batch = -1;

        public System.Int32 batch
        {
            get { return _batch; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] { "b" })
        {
        }

        /// <summary>
        /// Process the switch settings
        /// </summary>
        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "b":
                    System.Int32 TempBatch = 0;
                    if (Int32.TryParse(switchValue, out TempBatch))
                    {
                        _batch = TempBatch;
                    }
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// After processing, look for the batch ID
        /// </summary>
        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            // If there is no batch then ask the user for the batch ID
            if (batch <= 0)
            {
                System.Windows.Forms.DialogResult answer = default(System.Windows.Forms.DialogResult);

                // Find the batch to be processed.
                using (ReconcileBatchForm frm = new ReconcileBatchForm())
                {
                    var _with1 = frm;
                    answer = _with1.ShowDialog();
                    if (answer == DialogResult.OK)
                    {
                        _batch = _with1.BatchID;
                    }
                }
            }

            // If the batch is cancelled then return an error to flush the execution
            if (batch < 0)
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            return ss;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine("Usage: " + fname + " [-b#]")
            //AppendErrorLine("       -b : batch number to process for the inupt")
        }
    }
}