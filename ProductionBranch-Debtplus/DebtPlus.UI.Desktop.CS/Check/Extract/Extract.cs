#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Linq;
using System.Windows.Forms;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Check.Extract
{
	internal class Extract : System.IDisposable
	{
        // Current database connection object
        private BusinessContext bc = null;
        private ArgParser ap = null;

        /// <summary>
        /// Initialize the new class instance
        /// </summary>
        public Extract(BusinessContext bc, ArgParser ap)
        {
            this.bc = bc;
            this.ap = ap;
        }

		internal enum FieldList
		{
			Field_Zeros = 0,
			Field_Empty,
			Field_ABA,
			Field_AccountNumber,
			Field_CheckNumber,
			Field_Cleared,
			Field_Date,
			Field_Amount,
			Field_Cents,
			Field_Payee
		}

        // Get the timestamp to reflect today's date
        private System.DateTime today = DateTime.Now;

        // Build the list of expressions for the various cleared status values
        private System.Collections.Specialized.NameValueCollection my_settings = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CheckExtract);
        private System.Collections.Specialized.ListDictionary parsingList = new System.Collections.Specialized.ListDictionary(Comparer.DefaultInvariant);

        public void Dispose()
        {
            bc = null;

            // Do the "Dispose" function
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Class for the output statistics
        /// </summary>
        internal class statistics
        {
            public Int32 recordCount { get; set; }
            public decimal totalCheckAmount { get; set; }
            public decimal totalRefundAmount { get; set; }

            public statistics()
            {
                recordCount            = 0;
                totalCheckAmount       = 0M;
                totalRefundAmount      = 0M;
            }

            public statistics(Int32 recordCount, decimal totalCheckAmount, decimal totalRefundAmount)
            {
                this.recordCount       = recordCount;
                this.totalCheckAmount  = totalCheckAmount;
                this.totalRefundAmount = totalRefundAmount;
            }

            /// <summary>
            /// Process the add operator for the two class entries.
            /// </summary>
            public static statistics operator+ (statistics a, statistics b)
            {
                return new statistics(  a.recordCount       + b.recordCount,
                                        a.totalCheckAmount  + b.totalCheckAmount,
                                        a.totalRefundAmount + b.totalRefundAmount  );
            }
        }

		internal statistics ExtractFile()
		{
            // File statistics for all of the batches.
            var fileStats = new statistics();

            // List of the transaction types that represent bank checks.
            string[] validCheckTypes = new string[] {"AD", "MD", "CM", "CR", "AR"};
            char[] validStatuses = new char[] { ' ', 'V', 'D' };

			foreach (string key in my_settings.Keys)
            {
				if (key.StartsWith("expr_"))
                {
					parsingList.Add(key, new parsing_expression(key.PadRight(6, ' ')[5], my_settings[key]));
				}
			}

            // These represent "today" for the date ranges.
            System.DateTime fromDate = today.Date;
            System.DateTime toDate = fromDate.AddDays(1);

            // Generate the base query to retrieve the trust records. If there is a bank ID specified then use it to extract just the one bank.
            // Otherwise, take all banks in the list.
            var q = bc.registers_trusts.Where(s => validCheckTypes.Contains(s.tran_type) && (s.bank_xmit_date == null || (s.bank_xmit_date.Value >= fromDate && s.bank_xmit_date.Value < toDate)) && validStatuses.Contains(s.cleared) && s.checknum.HasValue);
            if (ap.Bank > 0)
            {
                q = q.Where<registers_trust>(s => s.bank == ap.Bank);
            }
            System.Collections.Generic.List<registers_trust> colRecords = q.ToList();

            if (colRecords.Count > 0)
            {
                colRecords.ForEach(s => s.bank_xmit_date = null);
                do
                {
                    // Find the first record that we can process. If there are none then stop.
                    var qFirstTrustRecord = colRecords.Find(s => s.bank_xmit_date == null);
                    if (qFirstTrustRecord == null)
                    {
                        break;
                    }

                    // Find the bank record in the system. If not found, just mark the transaction and skip it.
                    var bankID = qFirstTrustRecord.bank;
                    var bankRecord = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Id == bankID);
                    if (bankRecord == null)
                    {
                        qFirstTrustRecord.bank_xmit_date = today;
                        continue;
                    }

                    // Process this bank as a batch
                    var batchSats = extractBatch(bankRecord, colRecords.FindAll(s => s.bank == bankID));
                    if (batchSats == null)
                    {
                        break;
                    }
                    fileStats += batchSats;
                }
                while ( true );
            }

			// Finally, we can close our window and terminate
			DebtPlus.Data.Forms.MessageBox.Show(string.Format("{0}{1}{1}The operation is complete", ap.FileName, Environment.NewLine), "Complete", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information, System.Windows.Forms.MessageBoxDefaultButton.Button1);
            return fileStats;
        }

        /// <summary>
        /// Process the extract for the current bank transactions
        /// </summary>
        /// <param name="bc"></param>
        /// <param name="colRecords"></param>
        private statistics extractBatch(bank bankRecord, System.Collections.Generic.List<registers_trust> trustRecords)
        {
            // Statistics for the current batch extract.
            var batchStats = new statistics();

            // Find any item that is marked "destroyed" but has a check number that matches one that is not destroyed.
            // This occurs when the check run is stopped and the checks voided only to be re-started. In this case the
            // destroyed checks are not really destroyed.
            var removalList = new System.Collections.Generic.List<registers_trust>();
            foreach(var q in trustRecords.FindAll(s => s.cleared != 'D'))
            {
                // Find the items that are marked destroyed but match a check number that is not destroyed.
                removalList.AddRange(trustRecords.FindAll(s => s.cleared == 'D' && s.checknum == q.checknum));
            }

            // We can actually remove the items from the trust register that match a real check. There is no need to keep
            // the bogus entries any more.
            if (removalList.Count > 0)
            {
                // This will delete the items from the database on the SubmitChanges function call.
                bc.registers_trusts.DeleteAllOnSubmit(removalList);

                // For now, just take them out of the processing list.
                trustRecords.RemoveAll(x => (removalList.Select(s => s.Id).ToArray()).Contains(x.Id));
            }

            // If there are no records then we are complete at this point.
            if (trustRecords.Count < 1)
            {
                return batchStats;
            }

            // Open the output file.
			using (var outputfile = CreateFile(bankRecord))
            {
				if (outputfile == null)
                {
                    return null;
                }

                // If there is a prefix line then write it.
                if (!string.IsNullOrEmpty(bankRecord.prefix_line))
                {
                    outputfile.WriteLine(bankRecord.prefix_line);
                }

                // Process all of the items in the list
                System.Collections.Generic.IEnumerator<registers_trust> ie = trustRecords.GetEnumerator();
                while (ie.MoveNext())
                {
                    var currentRecord            = ie.Current;      // Find the current record
                    currentRecord.bank_xmit_date = today;           // Mark the record as having been processed today
                    parsing_expression expr      = null;            // Locate the expression corresponding to the record's cleared status

                    // If the status is not "pending to be cleared" then use the specific type record.
                    if (currentRecord.cleared != ' ')
                    {
                        expr = parsingList["expr_" + currentRecord.cleared] as parsing_expression;
                    }

                    // If the expression is null (not found) then use the default item
                    if (expr == null)
                    {
                        expr = parsingList["expr_"] as parsing_expression;
                    }

                    // If there is no expression still then invent one. It will use the default values.
                    if (expr == null)
                    {
                        expr = new parsing_expression(' ', "{3:000000000000}I  {4:0000000000}{8:000000000000000}{6:MMddyy}{9,-34:s}");
                    }

                    batchStats += ProcessTransaction(bankRecord, outputfile, currentRecord, expr);
                }

                // If there is a suffix line then write it
                // {0} = Number of detail records written
                // {1} = Total amount of checks written
                // {2} = Total amount of checks voided
                // {3} = Net amount (written - voided)
                // {4} = Total amount of checks written as cents
                // {5} = Total amount of checks voided as cents
                // {6} = Net amount (written - voided) as cents
                // {7} = Total dollar amount of detail records as cents
                if (!string.IsNullOrEmpty(bankRecord.suffix_line))
                {
                    outputfile.WriteLine(string.Format(bankRecord.suffix_line, new object[] {
                        batchStats.recordCount,
                        batchStats.totalCheckAmount,
                        batchStats.totalRefundAmount,
                        batchStats.totalCheckAmount - batchStats.totalRefundAmount,
                        Convert.ToInt64(batchStats.totalCheckAmount * 100m),
                        Convert.ToInt64(batchStats.totalRefundAmount * 100m),
                        Convert.ToInt64((batchStats.totalCheckAmount - batchStats.totalRefundAmount) * 100),
                        Convert.ToInt64((batchStats.totalCheckAmount + batchStats.totalRefundAmount) * 100) }));
                }

                // Close the output file when we are complete
                outputfile.Flush();
                outputfile.Close();
            }

            return batchStats;
        }

        /// <summary>
        /// Create and Open the output text file for the extract operation.
        /// </summary>
		private System.IO.StreamWriter CreateFile(bank bankRecord)
		{
            // If a filename was specified then try to open the file.
            if (!string.IsNullOrWhiteSpace(ap.FileName))
            {
                try
                {
                    return new System.IO.StreamWriter(ap.FileName, false, System.Text.Encoding.ASCII, 4096);
                }
                catch { }
            }

			// If there is a passed name then try to open the file
			string LocalName = string.Format("{0:yyyyMMdd}_{0:HHmmss}.txt", System.DateTime.Now);
            string localPath = bankRecord.output_directory;
            string fileName  = null;

            if (!string.IsNullOrWhiteSpace(localPath) && !ap.LocalFile)
            {
                fileName = System.IO.Path.Combine(localPath, LocalName);

                // Open the file
                try
                {
                    return new System.IO.StreamWriter(fileName, false, System.Text.Encoding.ASCII, 4096);
                }

                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Unable to open output file");
                }
            }

            do
            {
                // Ask the user for a new file
                using (var frm = new SaveFileDialog()
                        {
                            FileName = LocalName,
                            Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*",
                            FilterIndex = 0,
                            InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                            DefaultExt = "txt",
                            CheckPathExists = true,
                            CheckFileExists = false,
                            OverwritePrompt = true,
                            RestoreDirectory = true,
                            AutoUpgradeEnabled = true,
                            AddExtension = true,
                            ValidateNames = true,
                            Title = string.Format("{0} Output File", bankRecord.description ?? string.Empty)
                        })
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return null;
                    }
                    fileName = frm.FileName;
                }

                // Try to open the output file with the name supplied
                try
                {
                    return new System.IO.StreamWriter(fileName, false, System.Text.Encoding.ASCII, 4096);
                }

                catch (System.IO.IOException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error Opening output file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }

                catch (Exception ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Opening output file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    return null;
                }
            } while (true);
		}

        /// <summary>
        /// Process the specific transaction
        /// </summary>
        private statistics ProcessTransaction(bank bankRecord, System.IO.StreamWriter outputStream, registers_trust trustRecord, parsing_expression expr)
		{
            // Allocate the empty stats record
            var transactionStat = new statistics() { recordCount = 1 };

            // Array of strings to hold the formatted data.
		    object[] ItemArray = new object[11] { 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty };

            // Set the values for the check
            ItemArray[(int)FieldList.Field_ABA]           = bankRecord.aba;
            ItemArray[(int)FieldList.Field_AccountNumber] = bankRecord.account_number;
            ItemArray[(int)FieldList.Field_CheckNumber]   = trustRecord.checknum;
            ItemArray[(int)FieldList.Field_Amount]        = trustRecord.amount;
            ItemArray[(int)FieldList.Field_Date]          = trustRecord.date_created;
			ItemArray[(int)FieldList.Field_Cleared]       = trustRecord.cleared;
			ItemArray[(int)FieldList.Field_Payee]         = (trustRecord.client == null) ? trustRecord.creditor ?? string.Empty : trustRecord.client.Value.ToString("0000000");
            ItemArray[(int)FieldList.Field_Cents]         = Convert.ToInt64(Convert.ToDecimal(ItemArray[(int)FieldList.Field_Amount]) * 100.0m);
			ItemArray[(int)FieldList.Field_Zeros]         = Convert.ToInt64(0);

            // Count the information for the trailer record.
			if (expr.isCheck)
            {
                transactionStat.totalCheckAmount = trustRecord.amount;
			}
            else
            {
                transactionStat.totalRefundAmount = trustRecord.amount;
			}

            // Write the transaction line and return the stats entry for it
            string OutputString = string.Format(expr.fmt, ItemArray);
            outputStream.WriteLine(OutputString);
            return transactionStat;
		}
    }
}
