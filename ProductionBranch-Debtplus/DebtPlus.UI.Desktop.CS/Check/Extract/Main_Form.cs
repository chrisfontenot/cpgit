#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Utils;

namespace DebtPlus.UI.Desktop.CS.Check.Extract
{
    internal class Main_Form : DebtPlus.Data.Forms.DebtPlusForm
    {
        /// <summary>
        /// Main entry to the program
        /// </summary>
        internal Check.Extract.ArgParser ap;
        public Main_Form(ArgParser ap) : this()
		{
            this.ap = ap;
		}

        public Main_Form()
            : base()
        {
            InitializeComponent();

            Load += Form1_Load;
        }

        private void RegisterHandlers()
        {
            BankListLookupEdit.EditValueChanging += BankListLookupEdit_EditValueChanging;
            SimpleButton_OK.Click += SimpleButton_OK_Click;
        }

        private void UnRegisterHandlers()
        {
            BankListLookupEdit.EditValueChanging -= BankListLookupEdit_EditValueChanging;
            SimpleButton_OK.Click -= SimpleButton_OK_Click;
        }

        #region "Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            try
            {
                if (Disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(Disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        private DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
        internal DevExpress.XtraEditors.LookUpEdit BankListLookupEdit;
        public DevExpress.XtraEditors.LabelControl Label1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.BankListLookupEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();

            ((System.ComponentModel.ISupportInitialize)this.BankListLookupEdit.Properties).BeginInit();
            this.SuspendLayout();

            //
            //Label1
            //
            this.Label1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Appearance.Options.UseBackColor = true;
            this.Label1.Appearance.Options.UseFont = true;
            this.Label1.Appearance.Options.UseForeColor = true;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(24, 26);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(71, 14);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Bank Account:";
            //
            //BankListLookupEdit
            //
            this.BankListLookupEdit.Location = new System.Drawing.Point(112, 24);
            this.BankListLookupEdit.Name = "BankListLookupEdit";
            this.BankListLookupEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.BankListLookupEdit.Size = new System.Drawing.Size(192, 20);
            this.BankListLookupEdit.TabIndex = 4;
            //
            //SimpleButton_OK
            //
            this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.SimpleButton_OK.Location = new System.Drawing.Point(71, 64);
            this.SimpleButton_OK.Name = "SimpleButton_OK";
            this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton_OK.TabIndex = 5;
            this.SimpleButton_OK.Text = "&OK";
            //
            //SimpleButton_Cancel
            //
            this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(167, 64);
            this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
            this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton_Cancel.TabIndex = 6;
            this.SimpleButton_Cancel.Text = "&Cancel";
            //
            //Main_Form
            //
            this.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(312, 109);
            this.Controls.Add(this.SimpleButton_Cancel);
            this.Controls.Add(this.SimpleButton_OK);
            this.Controls.Add(this.BankListLookupEdit);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.Location = new System.Drawing.Point(4, 23);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main_Form";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "ARP Extract";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();

            ((System.ComponentModel.ISupportInitialize)this.BankListLookupEdit.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
        #endregion

        // ARP (Account Reconciliation Program) Extract Utility

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void Form1_Load(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                BankListLookupEdit_Load();
                Enable_OK();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Load the bank information
        /// </summary>
        private void BankListLookupEdit_Load()
        {
            try
            {
                // List of the banks which are for checks and active
                var col = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.ActiveFlag && s.type == "C");
                BankListLookupEdit.Properties.DataSource = col;

                BankListLookupEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[]
                {
					new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 25, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far),
					new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 50, DevExpress.Utils.FormatType.None, string.Empty, true, DevExpress.Utils.HorzAlignment.Near)
				});

                BankListLookupEdit.Properties.DisplayMember = "description";
                BankListLookupEdit.Properties.ValueMember = "Id";
                BankListLookupEdit.Properties.NullText = string.Empty;

                // Add some extra space for the dropdown width
                BankListLookupEdit.Properties.PopupWidth = BankListLookupEdit.Width + 100;

                // Take the row marked Default
                BankListLookupEdit.EditValue = DebtPlus.LINQ.Cache.bank.getDefault("C");
            }
            catch(System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error loading banks");
            }
        }

        /// <summary>
        /// Change in the bank location
        /// </summary>
        private void BankListLookupEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            Enable_OK();
        }

        /// <summary>
        /// Enable or disable the OK button
        /// </summary>
        private void Enable_OK()
        {
            SimpleButton_OK.Enabled = (BankListLookupEdit.EditValue != null);
        }

        /// <summary>
        /// Process the OK button click event
        /// </summary>
        private void SimpleButton_OK_Click(System.Object sender, System.EventArgs e)
        {
            ap.Bank = Convert.ToInt32(BankListLookupEdit.EditValue);
        }
    }
}
