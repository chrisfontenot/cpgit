#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;

namespace DebtPlus.UI.Desktop.CS.Check.Extract
{
    /// <summary>
    /// Argument Parsing Class
    /// </summary>
    internal class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// TRUE if we want to use a local file only and not use the output directory in the bank record.
        /// </summary>
        public bool LocalFile { get; set; }

        /// <summary>
        /// Bank reference ID
        /// </summary>
        public System.Int32 Bank { get; set; }

        /// <summary>
        /// Filename
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {"b","f","l"})
        {
            LocalFile = false;
            FileName = string.Empty;
            Bank = -1;
        }

        /// <summary>
        /// Process a switch on the run-line
        /// </summary>
        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            switch (switchSymbol)
            {
                case "b":
                    Int32 tempBank;
                    if (Int32.TryParse(switchValue, out tempBank))
                    {
                        Bank = tempBank;
                        break;
                    }
                    return DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;

                case "l":
                    LocalFile = true;
                    break;

                case "f":
                    FileName = switchValue;
                    break;

                case "?":
                    return DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;

                default:
                    return DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }

            return DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine(String.Format("Usage: {0} [-b#] [-f name]", fname))
            //AppendErrorLine("       [-b#]     : bank account number (#) to extract")
            //AppendErrorLine("       [-f name] : filename to be used for the extract")
        }

        /// <summary>
        /// Process the end of the parse operation
        /// </summary>
        protected override SwitchStatus OnDoneParse()
        {
            if (Bank <= 0)
            {
                using (var frm = new Main_Form(this))
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    }
                }
            }

            return DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
        }
    }
}
