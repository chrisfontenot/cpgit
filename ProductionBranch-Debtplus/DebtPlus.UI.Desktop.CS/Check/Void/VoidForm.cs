#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Diagnostics;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.Check.Void
{
    internal partial class VoidForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        internal ArgParser ap;

        // Allocate a context for the operation
        private DebtPlus.LINQ.BusinessContext bc          = new DebtPlus.LINQ.BusinessContext();
        private DebtPlus.LINQ.registers_trust trustRecord = null;
        private VoidTemplate CurrentMode                  = null;

        public VoidForm() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            this.Load                       += VoidForm_Load;
            Button_Cancel.Click             += Button_Cancel_Click;
            Button_OK.Click                 += Button_OK_Click;
            TextCheckNumber.Validating      += TextCheckNumber_Validating;
            LookupEditBank.EditValueChanged += LookupEditBank_EditValueChanged;

            if (CurrentMode != null)
            {
                CurrentMode.DataChanged += CurrentMode_DataChanged;
            }
        }

        private void UnRegisterHandlers()
        {
            this.Load                       -= VoidForm_Load;
            Button_Cancel.Click             -= Button_Cancel_Click;
            Button_OK.Click                 -= Button_OK_Click;
            TextCheckNumber.Validating      -= TextCheckNumber_Validating;
            LookupEditBank.EditValueChanged -= LookupEditBank_EditValueChanged;

            if (CurrentMode != null)
            {
                CurrentMode.DataChanged -= CurrentMode_DataChanged;
            }
        }

        public VoidForm(ArgParser ap)
            : this()
        {
            this.ap = ap;
        }

        /// <summary>
        /// Find the bank number for future access
        /// </summary>
        private Int32 bank
        {
            get
            {
                return DebtPlus.Utils.Nulls.v_Int32(LookupEditBank.EditValue).GetValueOrDefault();
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    if (components != null) components.Dispose();
                    if (bc != null) bc.Dispose();
                }

                // Clear the pointers to our objects
                components  = null;
                bc          = null;
                CurrentMode = null;
                trustRecord = null;
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.TextCheckNumber = new DevExpress.XtraEditors.ButtonEdit();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LookupEditBank = new DevExpress.XtraEditors.LookUpEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.CreditorVoid1 = new CreditorVoid();
            this.ClientVoid1 = new ClientVoid();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextCheckNumber.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBank.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "Caramel";
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(8, 48);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(69, 13);
            this.LabelControl1.TabIndex = 2;
            this.LabelControl1.Text = "&Check Number";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            // 
            // TextCheckNumber
            // 
            this.TextCheckNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextCheckNumber.Location = new System.Drawing.Point(96, 48);
            this.TextCheckNumber.Name = "TextCheckNumber";
            this.TextCheckNumber.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.TextCheckNumber.Properties.Appearance.Options.UseTextOptions = true;
            this.TextCheckNumber.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.TextCheckNumber.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, false)});
            this.TextCheckNumber.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.TextCheckNumber.Properties.Mask.BeepOnError = true;
            this.TextCheckNumber.Properties.Mask.EditMask = "[A-Z]?\\d+";
            this.TextCheckNumber.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.TextCheckNumber.Properties.Mask.SaveLiteral = false;
            this.TextCheckNumber.Properties.Mask.ShowPlaceHolders = false;
            this.TextCheckNumber.Properties.MaxLength = 50;
            this.TextCheckNumber.Properties.ValidateOnEnterKey = true;
            this.TextCheckNumber.Size = new System.Drawing.Size(104, 20);
            this.TextCheckNumber.TabIndex = 3;
            this.TextCheckNumber.ToolTip = "Enter the check number that you wish to void here.";
            this.TextCheckNumber.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl3
            // 
            this.LabelControl3.Location = new System.Drawing.Point(8, 16);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(23, 13);
            this.LabelControl3.TabIndex = 0;
            this.LabelControl3.Text = "&Bank";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            // 
            // LookupEditBank
            // 
            this.LookupEditBank.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookupEditBank.Location = new System.Drawing.Point(96, 16);
            this.LookupEditBank.Name = "LookupEditBank";
            this.LookupEditBank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookupEditBank.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LookupEditBank.Properties.DisplayMember = "description";
            this.LookupEditBank.Properties.NullText = "...Please choose one...";
            this.LookupEditBank.Properties.ShowFooter = false;
            this.LookupEditBank.Properties.ShowHeader = false;
            this.LookupEditBank.Properties.ValueMember = "Id";
            this.LookupEditBank.Size = new System.Drawing.Size(192, 20);
            this.LookupEditBank.TabIndex = 1;
            this.LookupEditBank.ToolTip = "You must first select the bank account against which the check is written.";
            this.LookupEditBank.ToolTipController = this.ToolTipController1;
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(328, 8);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 4;
            this.Button_OK.Text = "Void";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(328, 48);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "&Quit";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            // 
            // CreditorVoid1
            // 
            this.CreditorVoid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CreditorVoid1.Location = new System.Drawing.Point(8, 80);
            this.CreditorVoid1.Name = "CreditorVoid1";
            this.CreditorVoid1.Size = new System.Drawing.Size(400, 224);
            this.CreditorVoid1.TabIndex = 6;
            // 
            // ClientVoid1
            // 
            this.ClientVoid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ClientVoid1.Location = new System.Drawing.Point(8, 80);
            this.ClientVoid1.Name = "ClientVoid1";
            this.ClientVoid1.Size = new System.Drawing.Size(400, 224);
            this.ClientVoid1.TabIndex = 5;
            // 
            // VoidForm
            // 
            this.AcceptButton = this.Button_OK;
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(416, 310);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LookupEditBank);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.TextCheckNumber);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.CreditorVoid1);
            this.Controls.Add(this.ClientVoid1);
            this.Name = "VoidForm";
            this.Text = "Void Checks";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextCheckNumber.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookupEditBank.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.ButtonEdit TextCheckNumber;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LookUpEdit LookupEditBank;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal ClientVoid ClientVoid1;
        internal CreditorVoid CreditorVoid1;

        #endregion " Windows Form Designer generated code "

        private void VoidForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Remember the form location and size
                LoadPlacement("DebtPlus.Check.Void.MainForm");

                // Hide the client and creditor controls
                ClientVoid1.Visible   = false;
                CreditorVoid1.Visible = false;

                // Load the list of bank accounts
                LookupEditBank.Properties.DataSource = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.type == "C");
                LookupEditBank.EditValue             = DebtPlus.LINQ.Cache.bank.getDefault("C");

                // Enable the OK button if everything is valid.
                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                if (CurrentMode != null)
                {
                    CurrentMode.ProcessVoid(bc, trustRecord);
                    CurrentMode.Visible = false;
                    CurrentMode = null;
                    DoMessageBeep(MessageBeepEnum.OK);
                }

                TextCheckNumber.Text = string.Empty;
                Button_OK.Enabled = false;

                // Tab to the check number field again.
                TextCheckNumber.Focus();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void CurrentMode_DataChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            // If there is no error on the client/creditor mode then look at our own data.
            if (DxErrorProvider1.GetError(TextCheckNumber) != string.Empty)
            {
                return true;
            }

            if (DxErrorProvider1.GetError(LookupEditBank) != string.Empty)
            {
                return true;
            }

            if (TextCheckNumber.Text.Trim() == string.Empty)
            {
                return true;
            }

            // Look for the error condition in the displayed form
            return CurrentMode != null && CurrentMode.HasErrors;
        }

        /// <summary>
        /// Find the trust register for the check that we wish to void
        /// </summary>
        private DebtPlus.LINQ.registers_trust RegistersTrustRow(long CheckNumber, Int32 Bank)
        {
            if (CheckNumber <= 0 || Bank <= 0)
            {
                return null;
            }

            System.Collections.Generic.List<registers_trust> colItems = bc.registers_trusts.Where(s => s.checknum == CheckNumber && s.bank == Bank).OrderByDescending(s => s.date_created).ToList();
            if (colItems.Count == 0)
            {
                return null;
            }

            var answer = colItems.Find(s => s.cleared != 'D');
            if (answer == null)
            {
                answer = colItems[0];   // Take the first item, probably destroyed.
            }

            return answer;
        }

        private void TextCheckNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                Validate_CheckNumber();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Validate the entered check number field
        /// </summary>
        private void Validate_CheckNumber()
        {
            string ErrorMessage = null;
            trustRecord = null;

            long checknum = 0L;
            if (! long.TryParse(TextCheckNumber.Text.Trim(), out checknum))
            {
                checknum = 0L;
            }

            if (checknum <= 0L)
            {
                DxErrorProvider1.SetError(TextCheckNumber, "Invalid check number");
                ClientVoid1.Visible = false;
                CreditorVoid1.Visible = false;
                Button_OK.Enabled = false;
                return;
            }

            trustRecord = RegistersTrustRow(checknum, bank);
            if (trustRecord == null)
            {
                DxErrorProvider1.SetError(TextCheckNumber, "Invalid check number");
                ClientVoid1.Visible = false;
                CreditorVoid1.Visible = false;
                Button_OK.Enabled = false;
                return;
            }

            switch (trustRecord.cleared)
            {
                case 'V':
                    ErrorMessage = "The check was voided";
                    if (trustRecord.reconciled_date.HasValue)
                    {
                        ErrorMessage += " on " + trustRecord.reconciled_date.Value.ToShortDateString();
                    }

                    DxErrorProvider1.SetError(TextCheckNumber, ErrorMessage);
                    ClientVoid1.Visible = false;
                    CreditorVoid1.Visible = false;
                    Button_OK.Enabled = false;
                    return;

                case 'R':
                    ErrorMessage = "The check was reconciled";
                    if (trustRecord.reconciled_date.HasValue)
                    {
                        ErrorMessage += " on " + trustRecord.reconciled_date.Value.ToShortDateString();
                    }

                    DxErrorProvider1.SetError(TextCheckNumber, ErrorMessage);
                    ClientVoid1.Visible = false;
                    CreditorVoid1.Visible = false;
                    Button_OK.Enabled = false;
                    return;

                case 'D':
                    ErrorMessage = "The check was marked destroyed in printing";
                    DxErrorProvider1.SetError(TextCheckNumber, ErrorMessage);
                    ClientVoid1.Visible = false;
                    CreditorVoid1.Visible = false;
                    Button_OK.Enabled = false;
                    return;

                default:
                    break;
            }

            // Look for a client refund
            if (ClientVoid1.ValidTranType(trustRecord.tran_type))
            {
                CurrentMode = ClientVoid1;
            }

            // Look for a valid creditor check
            else if (CreditorVoid1.ValidTranType(trustRecord.tran_type))
            {
                CurrentMode = CreditorVoid1;
            }
            else
            {
                DxErrorProvider1.SetError(TextCheckNumber, "Check type invalid");
                ClientVoid1.Visible = false;
                CreditorVoid1.Visible = false;
                Button_OK.Enabled = false;
                return;
            }

            if (!CurrentMode.ValidCheck(bc, trustRecord))
            {
                DxErrorProvider1.SetError(TextCheckNumber, "Check invalid");
                ClientVoid1.Visible = false;
                CreditorVoid1.Visible = false;
                Button_OK.Enabled = false;
                return;
            }

            // Correct the display to match the type of check
            switch (CurrentMode.Type())
            {
                case "creditor":
                    ClientVoid1.Visible = false;
                    ClientVoid1.TabStop = false;
                    ClientVoid1.SendToBack();

                    CreditorVoid1.Visible = true;
                    CreditorVoid1.TabStop = true;
                    CreditorVoid1.BringToFront();
                    CreditorVoid1.Focus();
                    break;

                case "client":
                    CreditorVoid1.Visible = false;
                    CreditorVoid1.TabStop = false;
                    CreditorVoid1.SendToBack();

                    ClientVoid1.Visible = true;
                    ClientVoid1.TabStop = true;
                    ClientVoid1.BringToFront();
                    ClientVoid1.Focus();
                    break;

                default:
                    Debug.Assert(false);
                    break;
            }

            // Set the error information and accept the change if possible
            DxErrorProvider1.SetError(TextCheckNumber, null);
            Button_OK.Enabled = !HasErrors();
        }

        private void LookupEditBank_EditValueChanged(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                if (LookupEditBank.EditValue == null)
                {
                    DxErrorProvider1.SetError(LookupEditBank, "Value Required");
                    TextCheckNumber.Enabled = false;
                    Button_OK.Enabled = false;
                    return;
                }

                DxErrorProvider1.SetError(LookupEditBank, null);
                TextCheckNumber.Enabled = true;
                Validate_CheckNumber();
            }
            finally
            {
                RegisterHandlers();
            }
        }
    }
}