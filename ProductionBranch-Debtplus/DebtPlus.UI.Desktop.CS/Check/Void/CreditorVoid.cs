#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DebtPlus.UI.Common;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.Check.Void
{
    internal partial class CreditorVoid : VoidTemplate
    {
        private class CreditorTransactions
        {
            public Int32 Id { get; private set; }
            public Int32 clientID { get; private set; }
            public string clientName { get; private set; }
            public decimal debit_amt { get; private set; }
            public decimal fairshare_amt { get; private set; }
            public double fairshare_pct { get; private set; }
            public char creditor_type { get; private set; }
            public decimal deducted { get; private set; }
            public decimal billed { get; private set; }
            public decimal net { get; private set; }
            public string account_number { get; private set; }
            public string active_status { get; private set; }

            public CreditorTransactions()
            {
            }

            public CreditorTransactions(DebtPlus.LINQ.BusinessContext bc, registers_client_creditor clientCreditorTransaction) : this()
            {
                // Find the information from the registers_client_creditor transaction
                Id             = clientCreditorTransaction.Id;
                clientID       = clientCreditorTransaction.client.GetValueOrDefault();
                debit_amt      = clientCreditorTransaction.debit_amt.GetValueOrDefault();
                fairshare_amt  = clientCreditorTransaction.fairshare_amt.GetValueOrDefault();
                fairshare_pct  = clientCreditorTransaction.fairshare_pct.GetValueOrDefault();
                account_number = clientCreditorTransaction.account_number ?? string.Empty;
                creditor_type  = clientCreditorTransaction.creditor_type ?? 'N';
                deducted       = (creditor_type == 'D') ? fairshare_amt : 0M;
                billed         = (creditor_type == 'D' || creditor_type == 'N') ? 0M : fairshare_amt;
                net            = debit_amt - deducted;

                // Find the client information
                var client_reocord = bc.clients.Where(s => s.Id == clientID).FirstOrDefault();
                active_status = client_reocord == null ? "I" : client_reocord.active_status;

                // Find client's name
                var nameRecord = bc.peoples.Join(bc.Names, p => p.NameID, n => n.Id, (p, n) => new { p = p, n = n }).Where(x => x.p.Client == clientID && x.p.Relation == DebtPlus.LINQ.Cache.RelationType.Self).Select(x => x.n).FirstOrDefault();
                clientName     = nameRecord == null ? string.Empty : nameRecord.ToString();
            }
        }

        private System.Collections.Generic.List<CreditorTransactions> colRecords = null;

        public CreditorVoid()
            : base()
        {
            InitializeComponent();
            GridColumn_client.DisplayFormat.Format = new global::DebtPlus.Utils.Format.Client.CustomFormatter();
        }

        private void RegisterHandlers()
        {
            GridView1.Layout += LayoutChanged;
        }

        private void UnRegisterHandlers()
        {
            GridView1.Layout -= LayoutChanged;
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string layoutPathName()
        {
            string BasePath = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus";
            return System.IO.Path.Combine(BasePath, "Creditor.Billing.Payments");
        }

        private string layoutFileName()
        {
            return System.IO.Path.Combine(layoutPathName(), "CreditorVoid.Grid.xml");
        }

        private void LayoutChanged(object Sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = layoutPathName();
                if (!string.IsNullOrEmpty(PathName))
                {
                    if (!System.IO.Directory.Exists(PathName))
                    {
                        System.IO.Directory.CreateDirectory(PathName);
                    }
                    GridView1.SaveLayoutToXml(layoutFileName());
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl4;

        internal DevExpress.XtraEditors.LabelControl lbl_creditor_name;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ClientName;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Gross;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Deducted;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Billed;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Net;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Active_Status;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.GridColumn_Active_Status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lbl_creditor_name = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_ClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Gross = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Deducted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Billed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Net = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.Combo_Reason.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_Amount
            // 
            this.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            // 
            // Combo_Reason
            // 
            // 
            // GridColumn_Active_Status
            // 
            this.GridColumn_Active_Status.Caption = "Active Status";
            this.GridColumn_Active_Status.FieldName = "active_status";
            this.GridColumn_Active_Status.Name = "GridColumn_Active_Status";
            this.GridColumn_Active_Status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // lbl_creditor_name
            // 
            this.lbl_creditor_name.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_creditor_name.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbl_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.lbl_creditor_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lbl_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.lbl_creditor_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_creditor_name.Location = new System.Drawing.Point(88, 61);
            this.lbl_creditor_name.Name = "lbl_creditor_name";
            this.lbl_creditor_name.Size = new System.Drawing.Size(232, 13);
            this.lbl_creditor_name.TabIndex = 1;
            this.lbl_creditor_name.UseMnemonic = false;
            // 
            // LabelControl4
            // 
            this.LabelControl4.Location = new System.Drawing.Point(0, 61);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(43, 13);
            this.LabelControl4.TabIndex = 2;
            this.LabelControl4.Text = "Creditor:";
            this.LabelControl4.UseMnemonic = false;
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(0, 80);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(336, 128);
            this.GridControl1.TabIndex = 6;
            this.GridControl1.TabStop = false;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_client,
            this.GridColumn_ClientName,
            this.GridColumn_Gross,
            this.GridColumn_Deducted,
            this.GridColumn_Billed,
            this.GridColumn_Net,
            this.GridColumn_Active_Status});
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.Appearance.Options.UseBorderColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.GridColumn_Active_Status;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
            styleFormatCondition1.Value1 = "A";
            this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1});
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowFooter = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_client, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // GridColumn_client
            // 
            this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.Caption = "Client";
            this.GridColumn_client.DisplayFormat.FormatString = "0000000";
            this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.FieldName = "clientID";
            this.GridColumn_client.Name = "GridColumn_client";
            this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client.Visible = true;
            this.GridColumn_client.VisibleIndex = 0;
            // 
            // GridColumn_ClientName
            // 
            this.GridColumn_ClientName.Caption = "Name";
            this.GridColumn_ClientName.FieldName = "clientName";
            this.GridColumn_ClientName.Name = "GridColumn_ClientName";
            this.GridColumn_ClientName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_ClientName.Visible = true;
            this.GridColumn_ClientName.VisibleIndex = 1;
            // 
            // GridColumn_Gross
            // 
            this.GridColumn_Gross.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Gross.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Gross.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Gross.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Gross.Caption = "Gross";
            this.GridColumn_Gross.DisplayFormat.FormatString = "c2";
            this.GridColumn_Gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Gross.FieldName = "debit_amt";
            this.GridColumn_Gross.GroupFormat.FormatString = "c2";
            this.GridColumn_Gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Gross.Name = "GridColumn_Gross";
            this.GridColumn_Gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Gross.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "debit_amt", "{0:c}")});
            this.GridColumn_Gross.Visible = true;
            this.GridColumn_Gross.VisibleIndex = 2;
            // 
            // GridColumn_Deducted
            // 
            this.GridColumn_Deducted.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Deducted.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Deducted.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Deducted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Deducted.Caption = "Deducted";
            this.GridColumn_Deducted.DisplayFormat.FormatString = "c2";
            this.GridColumn_Deducted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Deducted.FieldName = "deducted";
            this.GridColumn_Deducted.GroupFormat.FormatString = "c2";
            this.GridColumn_Deducted.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Deducted.Name = "GridColumn_Deducted";
            this.GridColumn_Deducted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Deducted.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "deducted", "{0:c}")});
            this.GridColumn_Deducted.Visible = true;
            this.GridColumn_Deducted.VisibleIndex = 3;
            // 
            // GridColumn_Billed
            // 
            this.GridColumn_Billed.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Billed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Billed.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Billed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Billed.Caption = "Billed";
            this.GridColumn_Billed.DisplayFormat.FormatString = "c2";
            this.GridColumn_Billed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Billed.FieldName = "billed";
            this.GridColumn_Billed.GroupFormat.FormatString = "c2";
            this.GridColumn_Billed.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Billed.Name = "GridColumn_Billed";
            this.GridColumn_Billed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Billed.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "billed", "{0:c}")});
            this.GridColumn_Billed.Visible = true;
            this.GridColumn_Billed.VisibleIndex = 4;
            // 
            // GridColumn_Net
            // 
            this.GridColumn_Net.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Net.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Net.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Net.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Net.Caption = "Net";
            this.GridColumn_Net.DisplayFormat.FormatString = "c2";
            this.GridColumn_Net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Net.FieldName = "net";
            this.GridColumn_Net.GroupFormat.FormatString = "c2";
            this.GridColumn_Net.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_Net.Name = "GridColumn_Net";
            this.GridColumn_Net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Net.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "net", "{0:c}")});
            this.GridColumn_Net.Visible = true;
            this.GridColumn_Net.VisibleIndex = 5;
            // 
            // CreditorVoid
            // 
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.lbl_creditor_name);
            this.Name = "CreditorVoid";
            this.Controls.SetChildIndex(this.Combo_Reason, 0);
            this.Controls.SetChildIndex(this.Label_Date, 0);
            this.Controls.SetChildIndex(this.Label_Amount, 0);
            this.Controls.SetChildIndex(this.lbl_creditor_name, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.Combo_Reason.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion " Windows Form Designer generated code "

        public override void ProcessVoid(BusinessContext bc, registers_trust trustRecord)
        {
            base.ProcessVoid(bc, trustRecord);

            // Clear my display areas as well
            lbl_creditor_name.Text = string.Empty;

            // Empty the grid
            GridControl1.DataSource = null;
            GridControl1.RefreshDataSource();
        }

        [Description("Is the transaction a valid type for this processing function?"), Browsable(false)]
        protected internal override bool ValidTranType(string tran_type)
        {
            return (new string[] { "AD", "MD", "CM" }.Contains(tran_type.ToUpper()));
        }

        private string _creditor = string.Empty;

        [Description("Creditor ID associated with the check"), Browsable(false)]
        protected internal string creditor
        {
            get { return _creditor; }
            set { _creditor = value.ToUpper(); }
        }

        protected override void OnFirstLoad()
        {
            UnRegisterHandlers();
            try
            {
                base.OnFirstLoad();
                lbl_creditor_name.Text = string.Empty;

                // Restore the layout for the grid control
                string layoutName = layoutFileName();
                if (System.IO.File.Exists(layoutName))
                {
                    GridView1.RestoreLayoutFromXml(layoutName);
                }
            }
            catch { }
            finally
            {
                RegisterHandlers();
            }
        }

        public override string Type()
        {
            return "creditor";
        }

        [Description("Read the creditor information"), Browsable(false)]
        public override bool ValidCheck(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.registers_trust trustRegisterRecord)
        {
            // Pass the request along to the parent
            if (!base.ValidCheck(bc, trustRegisterRecord))
            {
                return false;
            }

            // Set the parameters for processing
            lbl_creditor_name.Text = string.Empty;
            creditor = trustRegisterRecord.creditor;

            // Read the name of the creditor. We ignore errors as they fold down to unknown name.
            var creditorRecord = bc.creditors.Where(s => s.Id == creditor).Select(s => new { creditor_name = s.creditor_name }).FirstOrDefault();
            if (creditorRecord == null)
            {
                DebtPlus.Data.Forms.MessageBox.Show("The creditor is no longer in the system", "Sorry, can not find the creditor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            // Format the creditor information
            if (!string.IsNullOrEmpty(creditorRecord.creditor_name))
            {
                lbl_creditor_name.Text = string.Format("[{0}] {1}", creditor, creditorRecord.creditor_name);
            }
            else
            {
                lbl_creditor_name.Text = creditor;
            }

            // Read the check contents
            //Dim frm As New WaitDialogForm("Reading check contents")
            //frm.Show()
            //TODO: We may wish to add a second variant on CursorManager that accepts a message and displays a dialog with that message.
            using (var cm = new CursorManager())
            {
                colRecords = bc.registers_client_creditors.Where(s => s.trust_register == trustRegisterRecord.Id && (new string[] { "AD", "MD", "CM", "BW" }).Contains(s.tran_type)).Select(s => new CreditorTransactions(bc, s)).ToList();
                GridControl1.DataSource = colRecords;
                GridControl1.RefreshDataSource();

                // Compare the transaction total to the check amount. They must agree.
                if (colRecords.Sum(s => s.net) != Amount)
                {
                    return false;
                }

                // Build the list of reasons why we are voiding the check
                var colItems = new System.Collections.ArrayList();
                DebtPlus.LINQ.Cache.message.getList().FindAll(s => s.item_type == "VD").ToList().ForEach(s => colItems.Add(new DebtPlus.Data.Controls.ComboboxItem(s.description, s.item_value, s.ActiveFlag)));

                Combo_Reason.Properties.Items.Clear();
                Combo_Reason.Properties.Sorted = true;
                Combo_Reason.Properties.Items.AddRange(colItems.ToArray());
                return true;
            }
        }
    }
}