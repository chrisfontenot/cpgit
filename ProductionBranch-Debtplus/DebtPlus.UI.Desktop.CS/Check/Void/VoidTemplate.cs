#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Interfaces;

namespace DebtPlus.UI.Desktop.CS.Check.Void
{
    internal partial class VoidTemplate : DevExpress.XtraEditors.XtraUserControl
    {
        // Event handler to indicate that the form was changed
        public event EventHandler DataChanged;

        protected virtual void OnDataChanged(System.EventArgs e)
        {
            if (DataChanged != null)
            {
                DataChanged(this, e);
            }
        }

        protected void RaiseDataChanged()
        {
            OnDataChanged(EventArgs.Empty);
        }

        public VoidTemplate()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        // Determine if an error is indicated in the form
        public virtual bool HasErrors
        {
            get
            {
                string ErrorString = string.Empty;
                if (Combo_Reason.Text.Trim() == string.Empty)
                {
                    ErrorString = "Entry Required";
                }
                Combo_Reason.ErrorText = ErrorString;
#if false
                // Set the error text
                var frm = (IForm)this.FindForm();
                frm.GetErrorProvider.SetError(Combo_Reason, ErrorString);
#endif

                return ErrorString != string.Empty;
            }
        }

        private decimal _Amount = 0m;

        protected decimal Amount
        {
            get
            {
                return _Amount;
            }
            set
            {
                _Amount = value;
                Label_Amount.Text = string.Format("{0:c}", value);
            }
        }

        private System.DateTime _CheckDate = System.DateTime.MinValue;

        protected System.DateTime CheckDate
        {
            get
            {
                return _CheckDate;
            }
            set
            {
                _CheckDate = value;
                if (value == System.DateTime.MinValue)
                {
                    Label_Date.Text = string.Empty;
                }
                else
                {
                    Label_Date.Text = value.ToShortDateString();
                }
            }
        }

        public virtual void ProcessVoid(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.registers_trust trustRegisterRecord)
        {
            UnRegisterHandlers();
            try
            {
                Int32 ChecksVoided = bc.xpr_void_check(trustRegisterRecord.Id, Combo_Reason.Text.Trim());
                if (ChecksVoided == 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The check was not voided because it is no longer a valid check to void", "Sorry, but I could not void the check", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }

                // Correct the trust register information from the database
                bc.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, trustRegisterRecord);

                // Clear the current controls
                CheckDate         = System.DateTime.MinValue;
                Amount            = 0m;
                Label_Amount.Text = string.Empty;
                Label_Date.Text   = string.Empty;

                // Change the combobox to indicate that it is empty (without tripping the error condition)
                Combo_Reason.SelectedIndex = -1;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error attempting to void the check");
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void RegisterHandlers()
        {
            Combo_Reason.SelectedIndexChanged += Combo_Reason_Changed;
            Combo_Reason.TextChanged          += Combo_Reason_Changed;
        }

        private void UnRegisterHandlers()
        {
            Combo_Reason.SelectedIndexChanged -= Combo_Reason_Changed;
            Combo_Reason.TextChanged          -= Combo_Reason_Changed;
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        protected internal DevExpress.XtraEditors.LabelControl Label_Amount;

        internal DevExpress.XtraEditors.LabelControl LabelControl8;
        protected internal DevExpress.XtraEditors.LabelControl Label_Date;
        protected internal DevExpress.XtraEditors.ComboBoxEdit Combo_Reason;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Label_Amount = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.Label_Date = new DevExpress.XtraEditors.LabelControl();
            this.Combo_Reason = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.Combo_Reason.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Label_Amount
            //
            this.Label_Amount.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Label_Amount.Appearance.Options.UseTextOptions = true;
            this.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Label_Amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Amount.Location = new System.Drawing.Point(215, 35);
            this.Label_Amount.Name = "Label_Amount";
            this.Label_Amount.Size = new System.Drawing.Size(105, 13);
            this.Label_Amount.TabIndex = 5;
            this.Label_Amount.UseMnemonic = false;
            //
            //LabelControl8
            //
            this.LabelControl8.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl8.Location = new System.Drawing.Point(168, 35);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(41, 13);
            this.LabelControl8.TabIndex = 4;
            this.LabelControl8.Text = "Amount:";
            this.LabelControl8.UseMnemonic = false;
            //
            //Label_Date
            //
            this.Label_Date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label_Date.Location = new System.Drawing.Point(88, 32);
            this.Label_Date.Name = "Label_Date";
            this.Label_Date.Size = new System.Drawing.Size(63, 19);
            this.Label_Date.TabIndex = 3;
            this.Label_Date.UseMnemonic = false;
            //
            //Combo_Reason
            //
            this.Combo_Reason.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.Combo_Reason.Location = new System.Drawing.Point(88, 4);
            this.Combo_Reason.Name = "Combo_Reason";
            this.Combo_Reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Combo_Reason.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Optimistic;
            this.Combo_Reason.Properties.Mask.BeepOnError = true;
            this.Combo_Reason.Properties.Mask.EditMask = "[^ ]+.*";
            this.Combo_Reason.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.Combo_Reason.Properties.MaxLength = 50;
            this.Combo_Reason.Size = new System.Drawing.Size(232, 20);
            this.Combo_Reason.TabIndex = 1;
            this.Combo_Reason.ToolTip = "Enter or choose a reason why you wish to void the check.";
            //
            //LabelControl5
            //
            this.LabelControl5.Location = new System.Drawing.Point(0, 7);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(40, 13);
            this.LabelControl5.TabIndex = 0;
            this.LabelControl5.Text = "&Reason:";
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(0, 35);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(59, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Check Date:";
            this.LabelControl2.UseMnemonic = false;
            //
            //VoidTemplate
            //
            this.Controls.Add(this.Label_Amount);
            this.Controls.Add(this.LabelControl8);
            this.Controls.Add(this.Label_Date);
            this.Controls.Add(this.Combo_Reason);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl2);
            this.Name = "VoidTemplate";
            this.Size = new System.Drawing.Size(336, 208);
            ((System.ComponentModel.ISupportInitialize)this.Combo_Reason.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        [Description("Is the transaction a valid type for this processing function?"), Browsable(false)]
        protected internal virtual bool ValidTranType(string tran_type)
        {
            return false;
        }

        private System.Int32 _trust_register = Int32.MinValue;

        [Description("Trust Register associated with the check"), Browsable(false)]
        protected internal virtual System.Int32 trust_register
        {
            get
            {
                return _trust_register;
            }
            set { _trust_register = value; }
        }
#if false
        protected override void OnFirstLoad()
        {
            base.OnFirstLoad();

            UnRegisterHandlers();
            try
            {
                DevExpress.Utils.ToolTipController tooltipcontroller = null;
                System.Windows.Forms.Form MyForm = this.FindForm();
                if (MyForm is IForm)
                {
                    tooltipcontroller = ((IForm)MyForm).GetToolTipController;
                }

                // Set the controllers
                foreach (Control ctl in Controls)
                {
                    if (ctl is DevExpress.XtraEditors.BaseControl)
                    {
                        ((DevExpress.XtraEditors.BaseControl)ctl).ToolTipController = tooltipcontroller;
                    }
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }
#endif
        [Description("Read the client information"), Browsable(false)]
        public virtual bool ValidCheck(DebtPlus.LINQ.BusinessContext bc, DebtPlus.LINQ.registers_trust trustRegisterRecord)
        {
            // If the check is missing information then blank the items
            if (trustRegisterRecord == null)
            {
                Amount         = 0m;
                CheckDate      = System.DateTime.MinValue;
                trust_register = 0;
                return false;
            }

            trust_register = trustRegisterRecord.Id;
            Amount         = trustRegisterRecord.amount;
            CheckDate      = trustRegisterRecord.date_created;
            return true;
        }

        public virtual string Type()
        {
            return "unknown";
        }

        private void Combo_Reason_Changed(object sender, System.EventArgs e)
        {
            RaiseDataChanged();
        }
    }
}