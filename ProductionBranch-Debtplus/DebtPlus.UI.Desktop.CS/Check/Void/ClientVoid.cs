#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.ComponentModel;
using DebtPlus.UI.Common;
using DebtPlus.Interfaces;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.Check.Void
{
    internal partial class ClientVoid : VoidTemplate
    {
        public ClientVoid() : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl lbl_client;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl lbl_client_name;
        internal DevExpress.XtraEditors.LabelControl lbl_client_addr;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_client_name = new DevExpress.XtraEditors.LabelControl();
            this.lbl_client = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_client_addr = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.Combo_Reason.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Label_Amount
            //
            this.Label_Amount.Appearance.Options.UseTextOptions = true;
            this.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            //
            //Combo_Reason
            //
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(0, 80);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(31, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Name:";
            this.LabelControl1.UseMnemonic = false;
            //
            //lbl_client_name
            //
            this.lbl_client_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.lbl_client_name.Appearance.Options.UseTextOptions = true;
            this.lbl_client_name.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbl_client_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.lbl_client_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lbl_client_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.lbl_client_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_client_name.Location = new System.Drawing.Point(88, 80);
            this.lbl_client_name.Name = "lbl_client_name";
            this.lbl_client_name.Size = new System.Drawing.Size(232, 13);
            this.lbl_client_name.TabIndex = 1;
            this.lbl_client_name.UseMnemonic = false;
            //
            //lbl_client
            //
            this.lbl_client.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.lbl_client.Appearance.Options.UseTextOptions = true;
            this.lbl_client.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbl_client.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.lbl_client.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lbl_client.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_client.Location = new System.Drawing.Point(88, 56);
            this.lbl_client.Name = "lbl_client";
            this.lbl_client.Size = new System.Drawing.Size(232, 13);
            this.lbl_client.TabIndex = 3;
            this.lbl_client.UseMnemonic = false;
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(0, 56);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(31, 13);
            this.LabelControl4.TabIndex = 2;
            this.LabelControl4.Text = "Client:";
            this.LabelControl4.UseMnemonic = false;
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(0, 104);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(27, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Addr:";
            this.LabelControl3.UseMnemonic = false;
            //
            //lbl_client_addr
            //
            this.lbl_client_addr.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.lbl_client_addr.Appearance.Options.UseTextOptions = true;
            this.lbl_client_addr.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbl_client_addr.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            this.lbl_client_addr.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.lbl_client_addr.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.lbl_client_addr.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_client_addr.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lbl_client_addr.CausesValidation = false;
            this.lbl_client_addr.Location = new System.Drawing.Point(88, 104);
            this.lbl_client_addr.Name = "lbl_client_addr";
            this.lbl_client_addr.Size = new System.Drawing.Size(232, 101);
            this.lbl_client_addr.TabIndex = 20;
            this.lbl_client_addr.ToolTip = "Creditor's payment address.";
            this.lbl_client_addr.UseMnemonic = false;
            //
            //ClientVoid
            //
            this.Controls.Add(this.lbl_client_addr);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.lbl_client);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.lbl_client_name);
            this.Controls.Add(this.LabelControl1);
            this.Name = "ClientVoid";
            this.Controls.SetChildIndex(this.Combo_Reason, 0);
            this.Controls.SetChildIndex(this.Label_Date, 0);
            this.Controls.SetChildIndex(this.Label_Amount, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.lbl_client_name, 0);
            this.Controls.SetChildIndex(this.LabelControl4, 0);
            this.Controls.SetChildIndex(this.lbl_client, 0);
            this.Controls.SetChildIndex(this.LabelControl3, 0);
            this.Controls.SetChildIndex(this.lbl_client_addr, 0);
            ((System.ComponentModel.ISupportInitialize)this.Combo_Reason.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        public override void ProcessVoid(BusinessContext bc, registers_trust trustRecord)
        {
            // Do the standard void processing
            base.ProcessVoid(bc, trustRecord);

            // Clear my display areas as well
            lbl_client.Text = string.Empty;
            lbl_client_name.Text = string.Empty;
            lbl_client_addr.Text = string.Empty;
        }

        [Description("Is the transaction a valid type for this processing function?"), Browsable(false)]
        protected internal override bool ValidTranType(string tran_type)
        {
            return (new string[] { "CR", "AR" }).Contains(tran_type.ToUpper());
        }

        private System.Int32 _client = Int32.MinValue;

        [Description("Client ID associated with the check"), Browsable(false)]
        protected internal Int32 client
        {
            get { return _client; }
            set
            {
                _client = value;
                lbl_client.Text = DebtPlus.Utils.Format.Client.FormatClientID(value);
            }
        }

        protected override void OnFirstLoad()
        {
            Form myForm = this.FindForm();
            base.OnFirstLoad();
#if false
            DevExpress.Utils.ToolTipController tooltipcontroller = null;

            if (myForm is IForm)
            {
                tooltipcontroller = ((IForm)myForm).GetToolTipController;
            }

            // Set the controllers
            foreach (Control ctl in Controls)
            {
                if (ctl is DevExpress.XtraEditors.BaseControl)
                {
                    ((DevExpress.XtraEditors.BaseControl)ctl).ToolTipController = tooltipcontroller;
                }
            }
#endif
            // Clear my display areas as well
            lbl_client.Text      = string.Empty;
            lbl_client_name.Text = string.Empty;
            lbl_client_addr.Text = string.Empty;
        }

        public override string Type()
        {
            return "client";
        }

        [Description("Read the client information"), Browsable(false)]
        public override bool ValidCheck(BusinessContext bc, registers_trust trustRecord)
        {
            // Retrieve the information from the trust register
            client = trustRecord.client.GetValueOrDefault(-1);

            // Clear the name field
            lbl_client_name.Text = System.String.Empty;
            lbl_client_addr.Text = System.String.Empty;
            lbl_client.Text = System.String.Empty;

            if (client < 0)
            {
                return false;
            }

            using (var cm = new CursorManager())
            {
                var r = bc.view_client_addresses.Where(s => s.client == client).Select(s => new {client = s.client, name = s.name, addr1 = s.addr1, addr2 = s.addr2, addr3 = s.addr3, activeStatus = s.active_status}).FirstOrDefault();

                // The client must exist.
                if (r == null)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(System.String.Format("The client {0} could not be found. A refund is not possible at this time.", System.String.Format("{0:0000000}", client)), "Invalid Client ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                // Double check the active status for the client
                if (r.activeStatus != "A" && r.activeStatus != "AR")
                {
                    if (DebtPlus.Data.Forms.MessageBox.Show(System.String.Format("Client {0} is not active.\r\n\r\nDo you still wish to continue to refund {1:c2} to this client?", client, Amount), "Client Not Active", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question, System.Windows.Forms.MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes)
                    {
                        return false;
                    }
                }

                // Format the address line block as needed.
                var sb = new System.Text.StringBuilder();
                foreach(string strng in new string[] {r.addr1, r.addr2, r.addr3})
                {
                    if (!System.String.IsNullOrWhiteSpace(strng))
                    {
                        sb.Append("\r\n");
                        sb.Append(strng);
                    }
                }

                if (sb.Length > 0)
                {
                    sb.Remove(0,2);
                }

                lbl_client_addr.Text = sb.ToString();
                lbl_client_name.Text = r.name;
                lbl_client.Text      = DebtPlus.Utils.Format.Client.FormatClientID(r.client);
            }

            // If successful, then populate the list of reasons
            var colItems = new System.Collections.ArrayList();
            DebtPlus.LINQ.Cache.message.getList().FindAll(s => s.item_type == "VR").ToList().ForEach(s => colItems.Add(new DebtPlus.Data.Controls.ComboboxItem(s.description, s.item_value, s.ActiveFlag)));

            base.Combo_Reason.Properties.Items.Clear();
            base.Combo_Reason.Properties.Sorted = true;
            base.Combo_Reason.Properties.Items.AddRange(colItems.ToArray());

            return true;
        }
    }
}