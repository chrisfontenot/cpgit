#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Proposals
{
    internal partial class StatusForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private System.ComponentModel.BackgroundWorker bt = new System.ComponentModel.BackgroundWorker();

        private ArgParser ap = null;

        public StatusForm(ArgParser ap) : this()
        {
            this.ap = ap;
            Button_OK.Click += cmdOK_Click;
            this.Load += StatusForm_Load;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
            bt.DoWork += bt_DoWork;
            Button_Print.Click += printButton_Click;
        }

        public StatusForm()
            : base()
        {
            InitializeComponent();
        }

        #region "Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Print;
        internal DevExpress.XtraEditors.LabelControl lbl_cdn_text;
        internal DevExpress.XtraEditors.LabelControl lbl_fbd_text;
        internal DevExpress.XtraEditors.LabelControl lbl_batch_count_text;
        internal DevExpress.XtraEditors.LabelControl lbl_text_records_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdv_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdd_text;
        internal DevExpress.XtraEditors.LabelControl lbl_fbc_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdp_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdn;
        internal DevExpress.XtraEditors.LabelControl lbl_fbd;
        internal DevExpress.XtraEditors.LabelControl lbl_batch_count;
        internal DevExpress.XtraEditors.LabelControl lbl_text_records;
        internal DevExpress.XtraEditors.LabelControl lbl_cdv;
        internal DevExpress.XtraEditors.LabelControl lbl_cdd;
        internal DevExpress.XtraEditors.LabelControl lbl_fbc;
        internal DevExpress.XtraEditors.LabelControl lbl_cdp;

        internal DevExpress.XtraEditors.MarqueeProgressBarControl MarqueeProgressBarControl1;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
            this.lbl_cdn_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_fbd_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_batch_count_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_text_records_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdv_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdd_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_fbc_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdp_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdn = new DevExpress.XtraEditors.LabelControl();
            this.lbl_fbd = new DevExpress.XtraEditors.LabelControl();
            this.lbl_batch_count = new DevExpress.XtraEditors.LabelControl();
            this.lbl_text_records = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdv = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdd = new DevExpress.XtraEditors.LabelControl();
            this.lbl_fbc = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdp = new DevExpress.XtraEditors.LabelControl();
            this.MarqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            //CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()

            ((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).BeginInit();
            this.SuspendLayout();

            //
            //Button_OK
            //
            this.Button_OK.Location = new System.Drawing.Point(63, 231);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 0;
            this.Button_OK.Text = "&Cancel";
            //
            //Button_Print
            //
            this.Button_Print.Enabled = false;
            this.Button_Print.Location = new System.Drawing.Point(153, 231);
            this.Button_Print.Name = "Button_Print";
            this.Button_Print.Size = new System.Drawing.Size(75, 23);
            this.Button_Print.TabIndex = 1;
            this.Button_Print.Text = "&Print";
            //
            //lbl_cdn_text
            //
            this.lbl_cdn_text.Location = new System.Drawing.Point(12, 30);
            this.lbl_cdn_text.Name = "lbl_cdn_text";
            this.lbl_cdn_text.Size = new System.Drawing.Size(88, 13);
            this.lbl_cdn_text.TabIndex = 2;
            this.lbl_cdn_text.Text = "Message Records:";
            //
            //lbl_fbd_text
            //
            this.lbl_fbd_text.Location = new System.Drawing.Point(12, 49);
            this.lbl_fbd_text.Name = "lbl_fbd_text";
            this.lbl_fbd_text.Size = new System.Drawing.Size(92, 13);
            this.lbl_fbd_text.TabIndex = 3;
            this.lbl_fbd_text.Text = "Full Budget Details:";
            //
            //lbl_batch_count_text
            //
            this.lbl_batch_count_text.Location = new System.Drawing.Point(12, 68);
            this.lbl_batch_count_text.Name = "lbl_batch_count_text";
            this.lbl_batch_count_text.Size = new System.Drawing.Size(63, 13);
            this.lbl_batch_count_text.TabIndex = 4;
            this.lbl_batch_count_text.Text = "Batch Count:";
            //
            //lbl_text_records_text
            //
            this.lbl_text_records_text.Location = new System.Drawing.Point(12, 87);
            this.lbl_text_records_text.Name = "lbl_text_records_text";
            this.lbl_text_records_text.Size = new System.Drawing.Size(105, 13);
            this.lbl_text_records_text.TabIndex = 5;
            this.lbl_text_records_text.Text = "Output Text Records:";
            //
            //lbl_cdv_text
            //
            this.lbl_cdv_text.Location = new System.Drawing.Point(12, 106);
            this.lbl_cdv_text.Name = "lbl_cdv_text";
            this.lbl_cdv_text.Size = new System.Drawing.Size(102, 13);
            this.lbl_cdv_text.TabIndex = 6;
            this.lbl_cdv_text.Text = "Balance Verifications:";
            //
            //lbl_cdd_text
            //
            this.lbl_cdd_text.Location = new System.Drawing.Point(12, 125);
            this.lbl_cdd_text.Name = "lbl_cdd_text";
            this.lbl_cdd_text.Size = new System.Drawing.Size(69, 13);
            this.lbl_cdd_text.TabIndex = 7;
            this.lbl_cdd_text.Text = "Drop Records:";
            //
            //lbl_fbc_text
            //
            this.lbl_fbc_text.Location = new System.Drawing.Point(12, 144);
            this.lbl_fbc_text.Name = "lbl_fbc_text";
            this.lbl_fbc_text.Size = new System.Drawing.Size(104, 13);
            this.lbl_fbc_text.TabIndex = 8;
            this.lbl_fbc_text.Text = "Full Budget Creditors:";
            //
            //lbl_cdp_text
            //
            this.lbl_cdp_text.Location = new System.Drawing.Point(12, 163);
            this.lbl_cdp_text.Name = "lbl_cdp_text";
            this.lbl_cdp_text.Size = new System.Drawing.Size(104, 13);
            this.lbl_cdp_text.TabIndex = 9;
            this.lbl_cdp_text.Text = "Proposals Generated:";
            //
            //lbl_cdn
            //
            this.lbl_cdn.Appearance.Options.UseTextOptions = true;
            this.lbl_cdn.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdn.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdn.Location = new System.Drawing.Point(230, 30);
            this.lbl_cdn.Name = "lbl_cdn";
            this.lbl_cdn.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdn.TabIndex = 10;
            this.lbl_cdn.Text = "0";
            //
            //lbl_fbd
            //
            this.lbl_fbd.Appearance.Options.UseTextOptions = true;
            this.lbl_fbd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_fbd.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_fbd.Location = new System.Drawing.Point(230, 49);
            this.lbl_fbd.Name = "lbl_fbd";
            this.lbl_fbd.Size = new System.Drawing.Size(50, 13);
            this.lbl_fbd.TabIndex = 11;
            this.lbl_fbd.Text = "0";
            //
            //lbl_batch_count
            //
            this.lbl_batch_count.Appearance.Options.UseTextOptions = true;
            this.lbl_batch_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_batch_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_batch_count.Location = new System.Drawing.Point(230, 68);
            this.lbl_batch_count.Name = "lbl_batch_count";
            this.lbl_batch_count.Size = new System.Drawing.Size(50, 13);
            this.lbl_batch_count.TabIndex = 12;
            this.lbl_batch_count.Text = "0";
            //
            //lbl_text_records
            //
            this.lbl_text_records.Appearance.Options.UseTextOptions = true;
            this.lbl_text_records.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_text_records.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_text_records.Location = new System.Drawing.Point(230, 87);
            this.lbl_text_records.Name = "lbl_text_records";
            this.lbl_text_records.Size = new System.Drawing.Size(50, 13);
            this.lbl_text_records.TabIndex = 13;
            this.lbl_text_records.Text = "0";
            //
            //lbl_cdv
            //
            this.lbl_cdv.Appearance.Options.UseTextOptions = true;
            this.lbl_cdv.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdv.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdv.Location = new System.Drawing.Point(230, 106);
            this.lbl_cdv.Name = "lbl_cdv";
            this.lbl_cdv.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdv.TabIndex = 14;
            this.lbl_cdv.Text = "0";
            //
            //lbl_cdd
            //
            this.lbl_cdd.Appearance.Options.UseTextOptions = true;
            this.lbl_cdd.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdd.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdd.Location = new System.Drawing.Point(230, 125);
            this.lbl_cdd.Name = "lbl_cdd";
            this.lbl_cdd.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdd.TabIndex = 15;
            this.lbl_cdd.Text = "0";
            //
            //lbl_fbc
            //
            this.lbl_fbc.Appearance.Options.UseTextOptions = true;
            this.lbl_fbc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_fbc.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_fbc.Location = new System.Drawing.Point(230, 144);
            this.lbl_fbc.Name = "lbl_fbc";
            this.lbl_fbc.Size = new System.Drawing.Size(50, 13);
            this.lbl_fbc.TabIndex = 16;
            this.lbl_fbc.Text = "0";
            //
            //lbl_cdp
            //
            this.lbl_cdp.Appearance.Options.UseTextOptions = true;
            this.lbl_cdp.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdp.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdp.Location = new System.Drawing.Point(230, 163);
            this.lbl_cdp.Name = "lbl_cdp";
            this.lbl_cdp.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdp.TabIndex = 17;
            this.lbl_cdp.Text = "0";
            //
            //MarqueeProgressBarControl1
            //
            this.MarqueeProgressBarControl1.EditValue = 0;
            this.MarqueeProgressBarControl1.Location = new System.Drawing.Point(12, 191);
            this.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1";
            this.MarqueeProgressBarControl1.Size = new System.Drawing.Size(268, 23);
            this.MarqueeProgressBarControl1.TabIndex = 18;
            //
            //StatusForm
            //
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.MarqueeProgressBarControl1);
            this.Controls.Add(this.lbl_cdp);
            this.Controls.Add(this.lbl_fbc);
            this.Controls.Add(this.lbl_cdd);
            this.Controls.Add(this.lbl_cdv);
            this.Controls.Add(this.lbl_text_records);
            this.Controls.Add(this.lbl_batch_count);
            this.Controls.Add(this.lbl_fbd);
            this.Controls.Add(this.lbl_cdn);
            this.Controls.Add(this.lbl_cdp_text);
            this.Controls.Add(this.lbl_fbc_text);
            this.Controls.Add(this.lbl_cdd_text);
            this.Controls.Add(this.lbl_cdv_text);
            this.Controls.Add(this.lbl_text_records_text);
            this.Controls.Add(this.lbl_batch_count_text);
            this.Controls.Add(this.lbl_fbd_text);
            this.Controls.Add(this.lbl_cdn_text);
            this.Controls.Add(this.Button_Print);
            this.Controls.Add(this.Button_OK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "StatusForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "RPPS Proposal File Generation";
            //CType(Me.DxErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()

            ((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion "Windows Form Designer generated code "

        private void cmdOK_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            Close();
        }

        private void Delegated_BatchID()
        {
        }

        private string _BatchID = string.Empty;

        public string BatchID
        {
            get { return _BatchID; }
            set
            {
                _BatchID = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_BatchID));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_BatchID();
                }
            }
        }

        private void Delegated_BillerID()
        {
        }

        private string _billerID = string.Empty;

        public string BillerID
        {
            get { return _billerID; }
            set
            {
                _billerID = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_BillerID));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_BillerID();
                }
            }
        }

        private void Delegated_BillerName()
        {
        }

        private string _BillerName = string.Empty;

        public string BillerName
        {
            get { return _BillerName; }
            set
            {
                _BillerName = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_BillerName));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_BillerName();
                }
            }
        }

        private void Delegated_BatchesProcessed()
        {
            lbl_batch_count.Text = string.Format("{0:n0}", BatchesProcessed);
        }

        private System.Int32 _BatchesProcessed = 0;

        public System.Int32 BatchesProcessed
        {
            get { return _BatchesProcessed; }
            set
            {
                _BatchesProcessed = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_BatchesProcessed));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_BatchesProcessed();
                }
            }
        }

        private void Delegated_TotalItems()
        {
        }

        private System.Int32 _TotalItems = 0;

        public System.Int32 TotalItems
        {
            get { return _TotalItems; }
            set
            {
                _TotalItems = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_TotalItems));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_TotalItems();
                }
            }
        }

        private void Delegated_PrenoteCount()
        {
        }

        private System.Int32 _PrenoteCount = 0;

        public System.Int32 PrenoteCount
        {
            get { return _PrenoteCount; }
            set
            {
                _PrenoteCount = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_PrenoteCount));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_PrenoteCount();
                }
            }
        }

        private void Delegated_DetailCount()
        {
            lbl_text_records.Text = string.Format("{0:n0}", DetailCount);
        }

        private System.Int32 _DetailCount = 0;

        public System.Int32 DetailCount
        {
            get { return _DetailCount; }
            set
            {
                _DetailCount = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_DetailCount));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_DetailCount();
                }
            }
        }

        private void Delegated_CountCDP()
        {
            lbl_cdp.Text = string.Format("{0:n0}", CountCDP);
        }

        private System.Int32 _CountCDP = 0;

        public System.Int32 CountCDP
        {
            get { return _CountCDP; }
            set
            {
                _CountCDP = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_CountCDP));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_CountCDP();
                }
            }
        }

        private void Delegated_CountCDD()
        {
            lbl_cdd.Text = string.Format("{0:n0}", CountCDD);
        }

        private System.Int32 _CountCDD = 0;

        public System.Int32 CountCDD
        {
            get { return _CountCDD; }
            set
            {
                _CountCDD = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_CountCDD));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_CountCDD();
                }
            }
        }

        private void Delegated_CountCDF()
        {
        }

        private System.Int32 _CountCDF = 0;

        public System.Int32 CountCDF
        {
            get { return _CountCDF; }
            set
            {
                _CountCDF = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_CountCDF));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_CountCDF();
                }
            }
        }

        private void Delegated_CountCDV()
        {
            lbl_cdv.Text = string.Format("{0:n0}", CountCDV);
        }

        private System.Int32 _CountCDV = 0;

        public System.Int32 CountCDV
        {
            get { return _CountCDV; }
            set
            {
                _CountCDV = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_CountCDV));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_CountCDV();
                }
            }
        }

        private void Delegated_CountCDN()
        {
            lbl_cdn.Text = string.Format("{0:n0}", CountCDN);
        }

        private System.Int32 _CountCDN = 0;

        public System.Int32 CountCDN
        {
            get { return _CountCDN; }
            set
            {
                _CountCDN = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_CountCDN));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_CountCDN();
                }
            }
        }

        private void Delegated_CountFBC()
        {
            lbl_fbc.Text = string.Format("{0:n0}", CountFBC);
        }

        private System.Int32 _CountFBC = 0;

        public System.Int32 CountFBC
        {
            get { return _CountFBC; }
            set
            {
                _CountFBC = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_CountFBC));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_CountFBD();
                }
            }
        }

        private void Delegated_CountFBD()
        {
            lbl_fbd.Text = string.Format("{0:n0}", CountFBD);
        }

        private System.Int32 _CountFBD = 0;

        public System.Int32 CountFBD
        {
            get { return _CountFBD; }
            set
            {
                _CountFBD = value;
                if (this.InvokeRequired)
                {
                    IAsyncResult ia = BeginInvoke(new MethodInvoker(Delegated_CountFBD));
                    EndInvoke(ia);
                }
                else
                {
                    Delegated_CountFBD();
                }
            }
        }

        private GenerateProposals ProcessingClass;

        private void StatusForm_Load(object sender, System.EventArgs e)
        {
            // Change the message text to indicate "CANCEL"
            Button_OK.Text = "&Cancel";
            Button_Print.Enabled = false;

            // Start the marque displaying
            var ctl = MarqueeProgressBarControl1;
            ctl.Properties.Stopped = false;
            ctl.Text = "OPERATION COMPLETED";
            ctl.Properties.ShowTitle = false;

            // Create the class to process the proposals
            ProcessingClass = new GenerateProposals(this, ap);

            // Start a thread to do the work. It runs on its own from here.
            bt.RunWorkerAsync();
        }

        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Change the message text to indicate "OK"
            Button_OK.Text = "&OK";
            Button_Print.Enabled = true;

            // Stop the marque from being displayed
            var ctl = MarqueeProgressBarControl1;
            ctl.Properties.Stopped = true;
            ctl.Text = "OPERATION COMPLETED";
            ctl.Properties.ShowTitle = true;
        }

        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                if (ProcessingClass.Process_Proposals())
                {
                    SaveResults();
                }
            }
#pragma warning disable 168
            catch (System.Threading.ThreadAbortException ex) { }
#pragma warning restore 1668
        }

        private void SaveResults()
        {
            if (this.InvokeRequired)
            {
                IAsyncResult ia = this.BeginInvoke(new MethodInvoker(SaveResults));
                EndInvoke(ia);
            }
            else
            {
                ProcessingClass.SaveOutputFile();
            }
        }

        // The Click event is raised when the user clicks the Print button.
        private System.Drawing.Font printFont = null;

        private void printButton_Click(object sender, EventArgs e)
        {
            printFont = new Font("Arial", 10);
            try
            {
                using (var pd = new System.Drawing.Printing.PrintDocument())
                {
                    pd.PrintPage += pd_PrintPage;

#if false // Direct to printer
	    			pd.Print();
#else // Use preview form first
                    var PreviewDialog = new PrintPreviewDialog();
                    PreviewDialog.ShowIcon = false;
                    PreviewDialog.Document = pd;
                    PreviewDialog.ShowDialog(this);
#endif
                }
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error printing document");
            }
            finally
            {
                printFont.Dispose();
                printFont = null;
            }
        }

        // The PrintPage event is raised for each page to be printed.
        private void pd_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs ev)
        {
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            float yPos = topMargin;
            System.Drawing.StringFormat fmt = new System.Drawing.StringFormat();
            System.Int32 LineNo = 0;

            // Generate the font for the header
            using (var HdrFont = new System.Drawing.Font(printFont.FontFamily, 20, FontStyle.Bold, GraphicsUnit.Point, Convert.ToByte(0)))
            {
                fmt.Alignment = StringAlignment.Near;
                ev.Graphics.DrawString(Text, HdrFont, Brushes.Black, leftMargin + 0, yPos, fmt);
                yPos += (HdrFont.GetHeight(ev.Graphics) * 2);
            }

            // Print each line of the file.
            ev.HasMorePages = true;

            while (ev.HasMorePages)
            {
                string TitleString = string.Empty;
                string ValueString = string.Empty;
                switch (LineNo)
                {
                    case 0:
                        TitleString = lbl_cdn_text.Text;
                        ValueString = lbl_cdn.Text;
                        break;

                    case 1:
                        TitleString = lbl_fbd_text.Text;
                        ValueString = lbl_fbd.Text;
                        break;

                    case 2:
                        TitleString = lbl_batch_count_text.Text;
                        ValueString = lbl_batch_count.Text;
                        break;

                    case 3:
                        TitleString = lbl_text_records_text.Text;
                        ValueString = lbl_text_records.Text;
                        break;

                    case 4:
                        TitleString = lbl_cdv_text.Text;
                        ValueString = lbl_cdv.Text;
                        break;

                    case 5:
                        TitleString = lbl_cdd_text.Text;
                        ValueString = lbl_cdd.Text;
                        break;

                    case 6:
                        TitleString = lbl_fbc_text.Text;
                        ValueString = lbl_fbc.Text;
                        break;

                    case 7:
                        TitleString = lbl_cdp_text.Text;
                        ValueString = lbl_cdp.Text;
                        ev.HasMorePages = false;
                        break;
                }

                // Generate the position on the form and print the line
                fmt.Alignment = StringAlignment.Near;
                ev.Graphics.DrawString(TitleString, printFont, Brushes.Black, leftMargin + 0, yPos, fmt);
                fmt.Alignment = StringAlignment.Far;
                ev.Graphics.DrawString(ValueString, printFont, Brushes.Black, leftMargin + 300, yPos, fmt);

                yPos += printFont.GetHeight(ev.Graphics);
                LineNo += 1;
            }
        }
    }
}