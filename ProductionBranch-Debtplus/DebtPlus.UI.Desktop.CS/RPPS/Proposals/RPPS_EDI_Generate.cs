﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.Utils;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Reports;
using DebtPlus.Interfaces.Reports;

namespace DebtPlus.UI.Desktop.CS.RPPS.Proposals
{
    internal class GenerateProposals
    {
        #region " Storage "
        public System.Text.StringBuilder OutputText;

        // Status form
        public StatusForm status_form       = null;
        internal ArgParser ap               = null;
        
        // Dataset for the transactions that we are going to process
        public DataSet ds                   = new DataSet("ds");
        
        // Bank number
        public Int32 batch_count            = 0;
        public System.DateTime dateNow      = System.DateTime.Now;
        public bool CIE_Format              = false;
        public bool Fake                    = true;
        public string FileName              = string.Empty;
        public short BillerType             = 0;
        public bool OrigTest                = false;
        public string OpenBatchID           = string.Empty;
        public string open_biller_id        = string.Empty;
        public string OpenBillerName        = string.Empty;
        public string OpenBillerClass       = string.Empty;
        public string OpenBillerDescription = string.Empty;
        
        // Information used to write the file control record at the end of the file
        public Int32 File_BatchCount        = 0;
        public Int32 File_DetailCount       = 0;
        public decimal File_Debit           = 0m;
        public decimal File_Credit          = 0m;
        
        // Information used to write the batch control record at the end of the batch
        public Int32 Batch_DetailCount      = 0;
        public decimal Batch_Debit          = 0m;
        public decimal Batch_Credit         = 0m;
        
        // Information used in the batches to reflect the company identification
        public string BillerCompanyID       = string.Empty;
        public string BillerCompanyName     = string.Empty;
        
        // Information from the banks table for the current file
        public string immediate_origin      = "00000000";
        public string immediate_origin_name = string.Empty;
        public string prefix_line           = string.Empty;
        public string suffix_line           = string.Empty;
        public string agency_name           = string.Empty;
        public string agency_address        = string.Empty;
        public string agency_city           = string.Empty;
        public string agency_state          = string.Empty;
        public string agency_zipcode        = string.Empty;
        public string agency_phone          = string.Empty;
        
        // ID information for the transaction sequences
        public Int32 transaction_id         = 0;     // Value for the current day's transactions
        public Int32 transaction_count      = 0;     // Number of transactions processed

        // ID information for the batch sequences
        public Int32 batch_id = 0;

        // Value for the current day's transactions
        System.Text.EncoderReplacementFallback erf;
        System.Text.DecoderReplacementFallback drf;
        #endregion
        System.Text.Encoding ae;

        #region " Initialization "

        /// <summary>
        /// Initialize the class
        /// </summary>
        public GenerateProposals(StatusForm StatusForm, ArgParser ap) : this()
        {
            this.status_form = StatusForm;
            this.ap = ap;

            // Create the encoding classes
            erf = new System.Text.EncoderReplacementFallback(" ");
            drf = new System.Text.DecoderReplacementFallback(" ");
            ae = System.Text.Encoding.GetEncoding("us-ascii", erf, drf);
        }

        public GenerateProposals() : base()
        {
            Int32 doy = default(Int32);
            Int32 Remainder = default(Int32);
            Int32 Quotient = default(Int32);

            // Seed values for the sequence numbers to ensure that the values increase for days
            doy = dateNow.DayOfYear;
            Quotient = System.Math.DivRem(doy, 98, out Remainder);
            transaction_id = (Remainder + 1) * 100000;

            // Create the encoding classes
            erf = new System.Text.EncoderReplacementFallback(" ");
            drf = new System.Text.DecoderReplacementFallback(" ");
            ae = System.Text.Encoding.GetEncoding("us-ascii", erf, drf);
        }

        #endregion

        #region " Banks Table Routines "

        /// <summary>
        /// Retrieve information about the agency
        /// </summary>
        public bool ReadProposalConfiguration()
        {
            bool answer = false;

            // Ensure that the table does not exist
            const string MyTableName = "proposal_configuration";
            DataTable tbl = ds.Tables[MyTableName];
            if (tbl != null)
            {
                tbl.Clear();
            }

            // Read the information
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandTimeout = System.Math.Min(300, cmd.CommandTimeout);
                    cmd.CommandText = "SELECT b.transaction_number as transaction_number, b.batch_number as batch_number, isnull(b.immediate_destination,'999900004') as immediate_destination, isnull(b.immediate_destination_name,'MC REMIT PROC CENT SITE') as immediate_destination_name, b.immediate_origin, b.immediate_origin_name, b.output_directory, b.prefix_line, b.suffix_line, cnf.name as agency_name, dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value) as agency_address, a.city as agency_city, left(st.mailingcode,2) as agency_state, left(a.postalcode,5) as agency_zipcode, dbo.Format_TelephoneNumber_BaseOnly(cnf.TelephoneID) as agency_phone FROM banks b WITH (NOLOCK) CROSS JOIN config cnf LEFT OUTER JOIN addresses a on cnf.AddressID = a.Address LEFT OUTER JOIN states st WITH (NOLOCK) ON a.state = st.state WHERE b.bank = @bank";
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = ap.Bank;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.FillLoadOption = LoadOption.OverwriteChanges;
                        da.Fill(ds, MyTableName);
                    }
                }

                tbl = ds.Tables[MyTableName];

                if (tbl.Rows.Count > 0)
                {
                    DataRow row = tbl.Rows[0];
                    foreach (DataColumn col in tbl.Columns)
                    {
                        if (row[col] != null && !object.ReferenceEquals(row[col], DBNull.Value))
                        {
                            switch (col.ColumnName.ToLower())
                            {
                                case "transaction_number":
                                    transaction_id = Convert.ToInt32(row[col]);
                                    break;
                                case "batch_number":
                                    batch_id = Convert.ToInt32(row[col]);
                                    break;
                                case "immediate_origin":
                                    immediate_origin = Convert.ToString(row[col]);
                                    break;
                                case "immediate_origin_name":
                                    immediate_origin_name = Convert.ToString(row[col]);
                                    break;
                                case "agency_name":
                                    agency_name = Convert.ToString(row[col]);
                                    break;
                                case "agency_address":
                                    agency_address = Convert.ToString(row[col]);
                                    break;
                                case "agency_city":
                                    agency_city = Convert.ToString(row[col]);
                                    break;
                                case "agency_state":
                                    agency_state = Convert.ToString(row[col]);
                                    break;
                                case "agency_zipcode":
                                    agency_zipcode = Convert.ToString(row[col]);
                                    break;
                                case "agency_phone":
                                    agency_phone = Convert.ToString(row[col]);
                                    break;
                                case "prefix_line":
                                    prefix_line = Convert.ToString(row[col]);
                                    break;
                                case "suffix_line":
                                    suffix_line = Convert.ToString(row[col]);
                                    break;
                            }
                        }
                    }
                }

                // If the RPS Origination ID is all zeros then do not generate the RPS file
                Fake = (immediate_origin == "00000000");

                // Convert the information once it was read
                answer = immediate_origin_name != string.Empty;

            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading proposal configuration information");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // Ask the user if they wish to continue with a fake extract
            if (answer && Fake)
            {
                if (DebtPlus.Data.Forms.MessageBox.Show("The RPS Biller ID is not configured properly." + Environment.NewLine + "This is configured in the configuration tool under the RPS tab." + Environment.NewLine + Environment.NewLine + "If you wish to continue then no output file will be generated but the RPS transactions " + Environment.NewLine + "will be allocated to a fake disbursement." + Environment.NewLine + Environment.NewLine + "If you don't continue now then please set the proper ORIGINATOR (8 digit) ID into the system." + Environment.NewLine + Environment.NewLine + "Do you wish to continue?", "RPS IS NOT CONFIGURED", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.No)
                {
                    answer = false;
                }
            }

            return answer;
        }

        /// <summary>
        /// Rewrite the bank information for the trace numbers
        /// </summary>
        public bool UpdateBanksTable(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            // Find the new transaction ID to be used
            Int32 new_transaction_id = transaction_count + transaction_id;
            if (new_transaction_id >= 10000000)
            {
                new_transaction_id -= 10000000;
            }

            // Do the same to the batch ID to be used
            Int32 new_batch_id = File_BatchCount + batch_id;
            if (new_batch_id >= 10000000)
            {
                new_batch_id -= 10000000;
            }

            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                using (var cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "UPDATE banks SET transaction_number=@new_transaction_number, batch_number=@new_batch_number WHERE isnull(transaction_number,0)=@transaction_number AND isnull(batch_number,0)=@batch_number AND bank=@bank";
                    cmd.Parameters.Add("@new_transaction_number", SqlDbType.Int).Value = new_transaction_id;
                    cmd.Parameters.Add("@transaction_number", SqlDbType.Int).Value = transaction_id;
                    cmd.Parameters.Add("@new_batch_number", SqlDbType.Int).Value = new_batch_id;
                    cmd.Parameters.Add("@batch_number", SqlDbType.Int).Value = batch_id;
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = ap.Bank;

                    Int32 RecordCount = cmd.ExecuteNonQuery();
                    return (RecordCount == 1);
                }
            }
        }
        #endregion

        #region " Text File Routines "

        /// <summary>
        /// Write a text record
        /// </summary>

        private void WriteRecord(string strRecord)
        {
            // Count this for the statistics
            status_form.DetailCount += 1;

            // Ensure that the record length is proper
            Debug.Assert(strRecord.Length == 94);

            // Place the record into the outbound queue for later file generation.
            OutputText.Append(strRecord);
            OutputText.Append(Environment.NewLine);
        }

        /// <summary>
        /// Local routine to obtain the next filename
        /// </summary>
        /// <param name="FileName">Current filename to the save function</param>
        /// <returns></returns>
        private System.Windows.Forms.DialogResult getNewName(ref string FileName)
        {
            // Conduct a new file save dialog to ask for a new name.
            string initDir = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (var dlg = new SaveFileDialog()
            {
                CheckFileExists = false,
                CheckPathExists = true,
                DefaultExt = ".txt",
                InitialDirectory = initDir,
                FileName = System.IO.Path.Combine(initDir, System.IO.Path.GetFileName(FileName)),
                Filter = "All Files (*.*)|*.*",
                OverwritePrompt = true,
                RestoreDirectory = true,
                ValidateNames = true,
                Title = "Proposals Output Text File"
            })
            {
                // Ask the user for a new file name
                System.Windows.Forms.DialogResult answer = dlg.ShowDialog();
                FileName = dlg.FileName;
                return answer;
            }
        }

        /// <summary>
        /// Save the output buffer to the indicated filname
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        private bool WriteOutputFile(string FileName)
        {
            try
            {
                // Open the output file
                using (var fs = new System.IO.StreamWriter(FileName))
                {
                    fs.Write(OutputText.ToString());
                    fs.Flush();
                    fs.Close();
                }
                return true;
            }
            catch (System.Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error saving proposals file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return false;
        }

        /// <summary>
        /// Save the output file to the disk
        /// </summary>
        public DialogResult SaveOutputFile()
        {
            // If this is a fake file then do not write anything to the disk.
            if (Fake)
            {
                return DialogResult.OK;
            }

            // Save the file to the default filename.
            if (WriteOutputFile(FileName))
            {
                return System.Windows.Forms.DialogResult.OK;
            }

            // Ask the user for a new filename. If one is given then use it for the save operation.
            while (getNewName(ref FileName) == System.Windows.Forms.DialogResult.OK)
            {
                // Write the output file to the disk system.
                if (WriteOutputFile(FileName))
                {
                    return System.Windows.Forms.DialogResult.OK;
                }
            }

            // Return the cancel event to indicate that we did not save the file.
            return System.Windows.Forms.DialogResult.Cancel;
        }

        /// <summary>
        /// Conduct the dialog to find the file name
        /// </summary>
        public bool request_file_name()
        {
            string directory_name = string.Empty;

            if (!ap.LocalFile)
            {
                // Read the output directory from the banks table
                DataTable tbl = ds.Tables["proposal_configuration"];
                if (tbl != null)
                {
                    if (tbl.Rows.Count > 0)
                    {
                        DataRow row = tbl.Rows[0];
                        Int32 col = tbl.Columns.IndexOf("output_directory");
                        if (col >= 0)
                        {
                            directory_name = DebtPlus.Utils.Nulls.DStr(row[col]).Trim();
                        }
                    }
                }
            }

            // Default to the documents folder
            if (string.IsNullOrEmpty(directory_name))
            {
                directory_name = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            }

            // The filename is the directory and the current timestamp.
            FileName = System.IO.Path.Combine(directory_name, string.Format("{0:yyyyMMddHHmm}_{1:0000}_RPPS.txt", dateNow, ap.Bank));
            return true;
        }
        #endregion

        #region " File Level Records "

        /// <summary>
        /// Write the file header record
        /// </summary>
        private void Write_File_Header()
        {
            System.Text.StringBuilder TextBuffer = new System.Text.StringBuilder(100);

            TextBuffer.Append("1");                                     // Record type
            TextBuffer.Append("01");                                    // Priority code
            TextBuffer.Append(" 999900004");                            // Immediate destination
            TextBuffer.AppendFormat(" {0:00000000}0", Convert.ToInt32(immediate_origin));   // Origin code
            TextBuffer.AppendFormat("{0:yyMMddHHmm}", dateNow);         // File creation date/time
            TextBuffer.Append("1");                                     // File ID modifier
            TextBuffer.Append("094");                                   // File Record Size
            TextBuffer.Append("10");                                    // Blocking factor
            TextBuffer.Append("1");                                     // File format code
            TextBuffer.Append("MC REMIT PROC CENT SITE");               // File destination
            TextBuffer.Append(FixedField(immediate_origin_name, 23));   // Source name

            // Include the proper "marker" as needed
            if (OrigTest)
            {
                TextBuffer.Append("ORIGTEST");                          // Testing string
            }
            else
            {
                TextBuffer.Append("        ");                          // Normal string
            }

            // Write the record
            WriteRecord(TextBuffer.ToString());
        }

        /// <summary>
        /// Write the file trailer record
        /// </summary>
        private void Write_File_Control()
        {
            Int32 Quotient = default(Int32);
            Int32 Remainder = default(Int32);

            // Find the number of blocks written to the file.
            Quotient = System.Math.DivRem(File_DetailCount + 9, 10, out Remainder);

            System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);
            TextRecord.Append("9");                                         // Record type
            TextRecord.AppendFormat("{0:000000}", File_BatchCount);         // Number of output batches
            TextRecord.AppendFormat("{0:000000}", Quotient);                // Number of output blocks
            TextRecord.AppendFormat("{0:00000000}", File_DetailCount);      // Number of output records
            TextRecord.Append('0', 10);                                     // Extra zeros
            TextRecord.Append(FormattedMoney(File_Debit).ToString("000000000000"));     // Total debit
            TextRecord.Append(FormattedMoney(File_Credit).ToString("000000000000"));    // Total credit
            TextRecord.Append(' ', 39);                                     // Extra spaces

            // Write the record to the disk file
            WriteRecord(TextRecord.ToString());
        }

        /// <summary>
        /// Generate a file number for the transactions
        /// </summary>
        public Int32 generate_rpps_file(string TempFileName, string file_type)
        {
            // Build the filename for the request
            if (TempFileName.Length > 50)
            {
                TempFileName = "..." + TempFileName.Substring(TempFileName.Length - 47);
            }

            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "xpr_rpps_create_file";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 300);
                            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                            cmd.Parameters.Add("@bank", SqlDbType.Int).Value = ap.Bank;
                            cmd.Parameters.Add("@filename", SqlDbType.VarChar, 80).Value = TempFileName;
                            cmd.Parameters.Add("@file_type", SqlDbType.VarChar, 10).Value = file_type;
                            cmd.ExecuteNonQuery();
                            return Convert.ToInt32(cmd.Parameters[0].Value);
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating RPPS file");
                return -1;
            }
        }
        #endregion

        #region " Batch Level Records "

        /// <summary>
        /// Write the batch header record
        /// </summary>
        private void Write_Batch_Header(string TraceNumber, DataRowView drv, string Description)
        {
            System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);

            TextRecord.Append("5");
            // Batch header record type
            TextRecord.Append(RPPS_Constants.SERVICE_CLASS_CREDIT);
            // Class identifier (normally 200)
            TextRecord.Append(FixedField(drv["biller_name"], 16));
            // Biller name
            TextRecord.Append(' ', 20);
            // 20 spaces
            TextRecord.Append(FixedField(drv["biller_id"], 10));
            // Biller ID number
            TextRecord.Append("CIE");
            // Batch entry class
            TextRecord.Append(FixedField(Description, 10));
            // Entry description
            TextRecord.Append(' ', 6);
            // Date for information
            TextRecord.AppendFormat("{0:yyMMdd}", dateNow);
            // Effective date
            TextRecord.Append(' ', 3);
            // 3 spaces
            TextRecord.Append("1");
            // Batch originator status code
            TextRecord.Append(TraceNumber);
            // Current batch ID

            // Write the record to the disk file
            WriteRecord(TextRecord.ToString());
        }

        /// <summary>
        /// Write the batch trailer record
        /// </summary>
        private void Write_Batch_Control(string TraceNumber, string BillerID)
        {
            System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);

            TextRecord.Append("8");
            // Record type
            TextRecord.Append(RPPS_Constants.SERVICE_CLASS_CREDIT);
            // Biller class
            TextRecord.AppendFormat("{0:000000}", Batch_DetailCount);
            // Number of output records
            TextRecord.Append('0', 10);
            // Extra zeros
            TextRecord.Append(FormattedMoney(Batch_Debit).ToString("000000000000"));
            // Total debit
            TextRecord.Append(FormattedMoney(Batch_Credit).ToString("000000000000"));
            // Total credit
            TextRecord.Append(FixedField(BillerID, 10));
            // Biller ID
            TextRecord.Append(' ', 25);
            // Extra spaces
            TextRecord.Append(TraceNumber);
            // Batch trace ID

            // Write the record to the disk file
            WriteRecord(TextRecord.ToString());

            // Update the totals for the file processing
            File_DetailCount += Batch_DetailCount;
            File_Debit += Batch_Debit;
            File_Credit += Batch_Credit;
        }

        /// <summary>
        /// Generate a RPPS Batch ID
        /// </summary>
        private Int32 Next_Batch(string TraceNumber, DataRowView drv)
        {
            Int32 answer = -1;

            // Bump the number of batches processed
            File_BatchCount += 1;

            // Clear the batch counters
            Batch_DetailCount = 0;
            Batch_Debit = 0m;
            Batch_Credit = 0m;

            // Pass the parameters to the status form
            status_form.BatchesProcessed = File_BatchCount;
            status_form.BatchID = TraceNumber;
            status_form.BillerID = Convert.ToString(drv["biller_id"]);
            status_form.BillerName = Convert.ToString(drv["biller_name"]);

            // Update the database with the new batch information
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.CommandText = "xpr_rpps_create_batch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@FileNumber", SqlDbType.Int).Value = ap.rpps_file;
                    cmd.Parameters.Add("@BillerID", SqlDbType.VarChar, 80).Value = Convert.ToString(drv["biller_id"]);
                    cmd.Parameters.Add("@TraceNumber", SqlDbType.VarChar, 80).Value = current_batch_trace();
                    cmd.Parameters.Add("@ServiceClass", SqlDbType.VarChar, 80).Value = RPPS_Constants.SERVICE_CLASS_CREDIT;
                    cmd.Parameters.Add("@BillerName", SqlDbType.VarChar, 80).Value = Convert.ToString(drv["biller_name"]);
                    cmd.ExecuteNonQuery();
                    answer = Convert.ToInt32(cmd.Parameters[0].Value);
                }

            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating RPPS batch");
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
                Cursor.Current = current_cursor;
            }

            return answer;
        }
        #endregion

        #region " Transaction Level Records "

        /// <summary>
        /// Rewrite the transaction trace number information
        /// </summary>
        public bool UpdateTransactionTraceNumbers(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "UPDATE rpps_transactions SET trace_number_first=@trace_number_first, trace_number_last=@trace_number_last, rpps_file=@rpps_file, rpps_batch=@rpps_batch WHERE rpps_transaction=@rpps_transaction";
                cmd.Parameters.Add("@trace_number_first", SqlDbType.VarChar, 16, "trace_number_first");
                cmd.Parameters.Add("@trace_number_last", SqlDbType.VarChar, 16, "trace_number_last");
                cmd.Parameters.Add("@rpps_file", SqlDbType.Int, 0, "rpps_file");
                cmd.Parameters.Add("@rpps_batch", SqlDbType.Int, 0, "rpps_batch");
                cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int, 0, "rpps_transaction");

                // Allocate a new data adapter to update the tables
                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                {
                    da.UpdateCommand = cmd;

                    // Do the two table updates. They both have the same datafields.
                    da.Update(ds.Tables["view_rpps_cdp_info"]);
                    da.Update(ds.Tables["view_rpps_cdn_info"]);
                }
            }
            return true;
        }
        #endregion

        #region " Batch Level Records "

        /// <summary>
        /// Rewrite the transaction trace number information
        /// </summary>
        public bool UpdateBatchTraceNumbers(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "UPDATE rpps_batches SET trace_number=@trace_number WHERE rpps_batch=@rpps_batch";
                cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 16, "trace_number");
                cmd.Parameters.Add("@rpps_batch", SqlDbType.Int, 0, "rpps_batch");

                // Allocate a new data adapter to update the tables
                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                {
                    da.InsertCommand = cmd;
                    da.UpdateCommand = cmd;
                    // (should not be needed since rows are "added" and not "updated")

                    // Do the table update.
                    da.Update(ds.Tables["rpps_batches"]);
                }
            }
            return true;
        }
        #endregion

        #region " Formatting "

        /// <summary>
        /// Format the currency value as a long value
        /// </summary>
        private Int64 FormattedWholeDollars(decimal curValue)
        {
            if (curValue < 0m)
            {
                curValue = 0m;
            }
            return Convert.ToInt64(curValue);
        }

        /// <summary>
        /// Format the currency value as a long value
        /// </summary>
        private Int64 FormattedMoney(decimal curValue)
        {
            if (curValue < 0m)
            {
                curValue = 0m;
            }
            return Convert.ToInt64(System.Math.Truncate(curValue * 100M));
        }

        /// <summary>
        /// Return a left-justified string of a specific size
        /// </summary>
        private string FixedField(object varName, Int32 length)
        {
            // Retrieve the value as a suitable string.
            string answer = DebtPlus.Utils.Nulls.DStr(varName);

            // Remove the junk characters from the string
            answer = clean_string(answer).Trim();

            // Make the field the exact number of spaces needed to generate the fixed format record
            answer = answer.PadRight(length);
            answer = answer.Substring(0, length);
            return answer;
        }

        /// <summary>
        /// Generate the account name value for the detail
        /// </summary>
        private string AccountName(object varName)
        {
            return AccountName(varName, 5);
        }

        private string AccountName(object varName, Int32 length)
        {
            string answer = null;

            // If the value is supplied then use the name.
            if (varName != null && !object.ReferenceEquals(varName, System.DBNull.Value))
            {
                answer = Convert.ToString(varName);
            }
            else
            {
                answer = string.Empty;
            }

            // Return the value as a five character field with trailing spaces
            return FixedField(answer, length);
        }

        /// <summary>
        /// Obtain the next trace number for a detail record
        /// </summary>
        private string Next_Detail_Trace()
        {
            // Bump the sequence number and display it on the status form
            transaction_count += 1;
            status_form.TotalItems = transaction_count;
            return current_detail_trace();
        }

        /// <summary>
        /// Return the current trace number for the transaction
        /// </summary>
        /// <returns></returns>
        private string current_detail_trace()
        {
            // Make the trace number the combined value of the RPS origination ID and the seven digit sequence number
            Int32 answer = transaction_count + transaction_id;
            if (answer >= 10000000)
            {
                answer -= 10000000;
            }

            return string.Format("{0}{1:0000000}", immediate_origin, answer);
        }

        /// <summary>
        /// Obtain the next trace number for a detail record
        /// </summary>
        private string current_batch_trace()
        {
            // Make the trace number the combined value of the RPS origination ID and the seven digit sequence number
            Int32 answer = File_BatchCount + batch_id;
            if (answer >= 10000000)
            {
                answer -= 10000000;
            }
            return string.Format("{0}{1:0000000}", immediate_origin, answer);
        }

        /// <summary>
        /// Generate detail information for a CDP request
        /// </summary>
        private void Write_Proposal_Addendum(string strTraceNumber, string strItem, ref Int32 nDetail)
        {
            strTraceNumber = Next_Detail_Trace();

            System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);
            TextRecord.Append(RPPS_Constants.RECORD_TYPE_ENTRY_DETAIL_ADDENDUM);
            TextRecord.Append(nDetail.ToString("00"));
            TextRecord.Append(FixedField(strItem, 76));
            TextRecord.Append(strTraceNumber);

            if (TextRecord.Length != 94)
            {
                throw new ApplicationException("Addendum record length <> 94 characters");
            }
            WriteRecord(TextRecord.ToString());

            // Count the type 7 record in the transaction counter as well
            Batch_DetailCount += 1;

            nDetail -= 1;
            if (nDetail == 91)
            {
                nDetail = 79;       // The list is discontinuous at this point
            }
        }

        /// <summary>
        /// Write prenotes and the additional type 7 records
        /// </summary>
        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2, string record_3)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2, record_3 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2, string record_3, string record_4)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2, record_3, record_4 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2, string record_3, string record_4, string record_5)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2, record_3, record_4, record_5 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2, string record_3, string record_4, string record_5, string record_6)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2, record_3, record_4, record_5, record_6 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2, string record_3, string record_4, string record_5, string record_6, string record_7)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2, record_3, record_4, record_5, record_6, record_7 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2, string record_3, string record_4, string record_5, string record_6, string record_7, string record_8)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2, record_3, record_4, record_5, record_6, record_7, record_8 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string record_1, string record_2, string record_3, string record_4, string record_5, string record_6, string record_7, string record_8, string record_9)
        {
            Generate_Proposal_Prenotes(strTraceNumber, drv, new string[] { record_1, record_2, record_3, record_4, record_5, record_6, record_7, record_8, record_9 });
        }

        private void Generate_Proposal_Prenotes(string strTraceNumber, DataRowView drv, string[] records)
        {
            // Determine the number of additional records
            Int32 nAdditional = 0;
            foreach (string rec in records)
            {
                if (!string.IsNullOrEmpty(rec))
                {
                    nAdditional += 1;
                }
            }

            // Count this pre-note record
            status_form.PrenoteCount += 1;

            // Generate a standard pre-note request
            System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);
            TextRecord.Append(RPPS_Constants.RECORD_TYPE_ENTRY_DETAIL);
            TextRecord.Append(RPPS_Constants.ENTRY_TRANSACTION_CODE_PRENOTE);
            TextRecord.Append(' ', 26);
            TextRecord.Append('0', 10);
            TextRecord.Append(AccountName(drv["account_name"], 15));
            TextRecord.Append(FixedField(drv["account_number"], 22));
            TextRecord.Append("  ");
            TextRecord.AppendFormat("{0:0}", nAdditional);
            TextRecord.Append(strTraceNumber);

            // Write the record
            WriteRecord(TextRecord.ToString());

            // Count the type 6 record
            Batch_DetailCount += 1;

            // Generate the addendum records
            Int32 nDetail = 95;
            foreach (string rec in records)
            {
                if (!string.IsNullOrEmpty(rec))
                {
                    Write_Proposal_Addendum(strTraceNumber, rec, ref nDetail);
                }
            }
        }

        /// <summary>
        /// Ensure message does not have funny characters
        /// </summary>
        private string clean_string(string message)
        {
            // Find the length of the input stream. It should be the number of bytes for the input.
            Int32 Length = ae.GetByteCount(message);
            if (Length < 1)
            {
                return string.Empty;
            }

            // Convert the string to a list of bytes. This removes the unicode non-ASCII characters.
            byte[] ab = new byte[Length];
            ae.GetBytes(message, 0, message.Length, ab, 0);

            // Convert the bytes back to a string as ASCII characters
            string Pass1 = ae.GetString(ab);

            // Now, remove any objectionable ASCII characters from the string. This is now a simple string, not unicode.
            string Result = System.Text.RegularExpressions.Regex.Replace(Pass1, "[\\x00-\\x1F\\x7F]", " ");
            return Result;
        }
        #endregion

        #region " Generate CDP records "

        /// <summary>
        /// Process the proposal requests (CDP, FBC, and FBD)
        /// </summary>
        public void Generate_CDP()
        {
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdp_generate";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = ap.rpps_file;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating CDP records");
            }
        }
        #endregion

        #region " Generate CDV records "

        /// <summary>
        /// Process the balance verification requests
        /// </summary>
        public void Generate_CDV()
        {
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdv_generate";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = ap.rpps_file;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating CDV records");
            }
        }
        #endregion

        #region " Generate CDF records "

        /// <summary>
        /// Process the account number verification requests
        /// </summary>
        public void Generate_CDF()
        {
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdf_generate";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = ap.rpps_file;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating CDF records");
            }
        }
        #endregion

        #region " Generate CDD records "

        /// <summary>
        /// Process the drop requests
        /// </summary>
        public void Generate_CDD()
        {
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdd_generate";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = ap.rpps_file;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating CDD records");
            }
        }
        #endregion

        #region " Generate CDN records "

        /// <summary>
        /// Process the message block requests for those who want it
        /// </summary>
        public void Generate_CDN()
        {
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdn_generate";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = ap.rpps_file;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error creating CDN records");
            }
        }
        #endregion

        #region " Write Standard Proposals "

        /// <summary>
        /// Generate detail information for a CDP request
        /// </summary>
        private void Generate_CDP_Proposal(string strTraceNumber, DataRowView drv)
        {
            decimal account_balance        = 0m;
            decimal living_expenses        = 0m;
            decimal proposed_payment       = 0m;
            decimal total_debt             = 0m;
            decimal total_debt_payments    = 0m;
            decimal total_income           = 0m;

            double bankruptcy_percent      = 0.0D;
            
            Int32 bankruptcy_term          = 0;
            Int32 creditor_count           = 0;
            Int32 proposal_count           = 0;
            
            string account_number          = string.Empty;
            string bankruptcy_flag         = string.Empty;
            string client                  = string.Empty;
            string home_phone              = string.Empty;
            string special_identifer       = string.Empty;
            string special_identifier      = string.Empty;
            string ssn                     = string.Empty;
            string ssn2                    = string.Empty;
            
            System.DateTime first_counsel  = dateNow;
            System.DateTime proposed_start = dateNow.AddDays(3);

            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdp_info";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];

                            using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
                            {
                                if (rd.Read())
                                {
                                    for (Int32 FldNo = 0; FldNo <= rd.FieldCount - 1; FldNo++)
                                    {
                                        if (!rd.IsDBNull(FldNo))
                                        {
                                            switch (rd.GetName(FldNo).ToLower())
                                            {
                                                case "client":
                                                    client = string.Format("{0:0000000}", Convert.ToInt32(rd.GetValue(FldNo)));
                                                    break;
                                                case "special_identifier":
                                                    special_identifier = rd.GetString(FldNo);
                                                    break;
                                                case "ssn":
                                                    ssn = rd.GetString(FldNo);
                                                    break;
                                                case "ssn2":
                                                    ssn2 = rd.GetString(FldNo);
                                                    break;
                                                case "proposal_count":
                                                    proposal_count = Convert.ToInt32(rd.GetValue(FldNo));
                                                    break;
                                                case "proposed_payment":
                                                    proposed_payment = Convert.ToDecimal(rd.GetValue(FldNo));
                                                    break;
                                                case "proposed_start":
                                                    proposed_start = Convert.ToDateTime(rd.GetValue(FldNo));
                                                    break;
                                                case "creditor_count":
                                                    creditor_count = Convert.ToInt32(rd.GetValue(FldNo));
                                                    break;
                                                case "first_counsel":
                                                    first_counsel = Convert.ToDateTime(rd.GetValue(FldNo));
                                                    break;
                                                case "account_balance":
                                                    account_balance = Convert.ToDecimal(rd.GetValue(FldNo));
                                                    break;
                                                case "total_debt":
                                                    total_debt = Convert.ToDecimal(rd.GetValue(FldNo));
                                                    break;
                                                case "total_income":
                                                    total_income = Convert.ToDecimal(rd.GetValue(FldNo));
                                                    break;
                                                case "total_debt_payments":
                                                    total_debt_payments = Convert.ToDecimal(rd.GetValue(FldNo));
                                                    break;
                                                case "living_expenses":
                                                    living_expenses = Convert.ToDecimal(rd.GetValue(FldNo));
                                                    break;
                                                case "home_phone":
                                                    home_phone = rd.GetString(FldNo);
                                                    break;
                                                case "account_number":
                                                    account_number = rd.GetString(FldNo);
                                                    break;
                                                case "bankruptcy_flag":
                                                    bankruptcy_flag = rd.GetString(FldNo);
                                                    break;
                                                case "percent_balance":
                                                case "bankruptcy_percent":
                                                    bankruptcy_percent = Convert.ToDouble(rd.GetValue(FldNo));
                                                    break;
                                                case "bankruptcy_term":
                                                    bankruptcy_term = Convert.ToInt32(rd.GetValue(FldNo));
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // First addendum record
                System.Text.StringBuilder Record1 = new System.Text.StringBuilder(100);
                Record1.Append("CDP");
                Record1.Append(FixedField(client, 15));
                Record1.Append(FixedField(special_identifier, 6));
                Record1.Append(FixedField(ssn, 9));
                Record1.Append(FixedField(ssn2, 9));
                Record1.Append(proposal_count.ToString("0"));
                Record1.Append(FixedField(agency_address, 28));
                Record1.Append(FixedField(agency_zipcode, 5));

                // Second addendum record
                System.Text.StringBuilder Record2 = new System.Text.StringBuilder(100);
                Record2.Append("CDP");
                Record2.Append(FormattedMoney(proposed_payment).ToString("0000000"));
                Record2.Append(proposed_start.ToString("MMddyyyy"));
                Record2.Append(creditor_count.ToString("00"));
                Record2.Append(FixedField(agency_city, 20));
                Record2.Append(first_counsel.ToString("MMddyyyy"));
                Record2.Append(FixedField(agency_name, 18));
                Record2.Append(FixedField(agency_phone, 10));

                // Third addendum record
                System.Text.StringBuilder Record3 = new System.Text.StringBuilder(100);

                // If bankruptcy records are indicated then generate the new format. Otherwise, use the old format.
                string Flag = bankruptcy_flag.ToUpper();
                string WorkTelephoneNumber = null;

                // Format the percentage figure. It must be less than 100% or we use blanks.
                if (bankruptcy_percent < 100.0)
                {
                    WorkTelephoneNumber = Convert.ToInt32(bankruptcy_percent * 1000.0).ToString("00000");
                }
                else
                {
                    WorkTelephoneNumber = "     ";
                }

                // Format the term of the payment plan. It should be less than 60 months.
                if (bankruptcy_term > 0 && bankruptcy_term < 100)
                {
                    WorkTelephoneNumber += bankruptcy_term.ToString("00");
                }
                else
                {
                    WorkTelephoneNumber += "  ";
                }
                WorkTelephoneNumber += "   ";

                // If the flag is blank or "B" then blank the field since it is the work telephone number which we don't give any longer
                if (Flag == "B" || Flag == " ")
                {
                    WorkTelephoneNumber = new string(' ', 10);
                }

                Record3.Append("CDP");
                Record3.Append(FormattedMoney(account_balance).ToString("0000000000"));
                Record3.Append(FormattedMoney(total_debt).ToString("0000000000"));
                Record3.Append(Flag);
                Record3.Append(FormattedMoney(total_income).ToString("0000000000"));
                Record3.Append(FormattedMoney(total_debt_payments).ToString("0000000000"));
                Record3.Append(FormattedMoney(living_expenses).ToString("0000000000"));
                Record3.Append(FixedField(home_phone, 10));
                Record3.Append(WorkTelephoneNumber);
                Record3.Append(FixedField(agency_state, 2));

                // Generate the record for the proposal item
                Generate_Proposal_Prenotes(strTraceNumber, drv, Record1.ToString(), Record2.ToString(), Record3.ToString());
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading CDP information");
            }
        }
        #endregion

        #region " Write Full Disclosure - Creditors "

        /// <summary>
        /// Generate detail information for a FBC request
        /// </summary>
        private void Generate_FBC_Proposal(string strTraceNumber, DataRowView drv)
        {
            string[] Records = new string[] {
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty
            };

            Int32 client            = 0;
            Int32 creditor_count    = 0;
            string creditor_message = string.Empty;

            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_fbc_info";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];
                            using (var rd = cmd.ExecuteReader(CommandBehavior.SingleRow))
                            {
                                if (rd.Read())
                                {
                                    client = DebtPlus.Utils.Nulls.DInt(rd["client"]);
                                    creditor_count = DebtPlus.Utils.Nulls.DInt(rd["creditor_count"]);
                                    creditor_message = DebtPlus.Utils.Nulls.DStr(rd["creditor_message"]).Trim();
                                }
                            }
                        }

                        // Read the detail information next
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_fbc_info_detail";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];
                            using (var rd = cmd.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection))
                            {
                                Int32 nIndex = 0;
                                System.Text.StringBuilder sb = null;

                                while (rd.Read())
                                {
                                    if (sb == null)
                                    {
                                        sb = new System.Text.StringBuilder();
                                    }

                                    if (sb.Length > 60)
                                    {
                                        if (nIndex == 6)
                                        {
                                            break;
                                        }
                                        sb.Insert(0, "FBC");
                                        Records[nIndex] = sb.ToString();
                                        nIndex += 1;
                                        sb = new System.Text.StringBuilder();
                                    }

                                    // Add the current creditor to the list
                                    decimal balance = DebtPlus.Utils.Nulls.DDec(rd["balance"]);
                                    decimal payment = DebtPlus.Utils.Nulls.DDec(rd["payment"]);
                                    string creditor_name = DebtPlus.Utils.Nulls.DStr(rd["creditor_name"]);

                                    sb.Append(FormattedWholeDollars(balance).ToString("000000"));
                                    sb.Append(FormattedMoney(payment).ToString("000000"));
                                    sb.Append(FixedField(creditor_name, 12));
                                }

                                // Process the record type for the last record.
                                if (sb != null)
                                {
                                    sb.Insert(0, "FBC");
                                    Records[nIndex] = sb.ToString();
                                }
                            }
                        }

                        // Generate the proposal records
                        creditor_message = FixedField(creditor_message, 128).ToUpper();
                        Generate_Proposal_Prenotes(strTraceNumber, drv, string.Format("FBC{0}{1:00}{2}", FixedField(client, 15), creditor_count, creditor_message.Substring(0, 56)), "FBC" + creditor_message.Substring(56, 72), Records[0], Records[1], Records[2], Records[3], Records[4], Records[5], Records[6]);
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading FBC information");
            }
        }
        #endregion

        #region " Write Full Disclosure - Detail "

        /// <summary>
        /// Generate detail information for a FBD request
        /// </summary>
        private void Generate_FBD_Proposal(string strTraceNumber, DataRowView drv)
        {
            string[] Records = new string[] {
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty,
                string.Empty
            };

            Int32 client            = 0;
            Int32 creditor_count    = 0;
            string creditor_message = string.Empty;
            Int32 nIndex            = default(Int32);

            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_fbc_info";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];
                            using (var rd1 = cmd.ExecuteReader(CommandBehavior.SingleRow))
                            {
                                if (rd1.Read())
                                {
                                    client           = DebtPlus.Utils.Nulls.DInt(rd1["client"]);
                                    creditor_count   = DebtPlus.Utils.Nulls.DInt(rd1["creditor_count"]);
                                    creditor_message = DebtPlus.Utils.Nulls.DStr(rd1["creditor_message"]).Trim();
                                }
                                rd1.Close();
                            }
                        }

                        // Read the detail information next
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_fbd_info_detail";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];
                            using (var rd2 = cmd.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection))
                            {
                                // We must add the client ID as the first 15 characters of the first record. Do it here.
                                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                                sb.Append(FixedField(string.Format("{0:0000000}", client), 15));

                                // Start a new buffer if the current one is full
                                while (rd2.Read())
                                {
                                    // this works for the first record as well as the others. It is in the middle of the last block.
                                    if (sb.Length > 67)
                                    {
                                        if (nIndex == 6)
                                        {
                                            break;
                                        }
                                        sb.Insert(0, "FBD");
                                        Records[nIndex] = sb.ToString();
                                        nIndex += 1;
                                        sb = new System.Text.StringBuilder();
                                    }

                                    Int32 value        = 0;
                                    string strCategory = DebtPlus.Utils.Nulls.DInt(rd2["category"]).ToString("0");
                                    try
                                    {
                                        value = Convert.ToInt32(rd2["value"]);
                                    }
                                    catch { }

                                    // Allow the value to be negative and represent that as a dash with five digits
                                    string strValue    = null;
                                    if (value < 0)
                                    {
                                        value = 0 - value;  // flip the sign of the number
                                        if (value > 99999)
                                        {
                                            value = 99999;  // limit the to five digits in size
                                        }
                                        strValue = string.Format("-{0:00000}", value);  // format it to a six character string.
                                    }
                                    else
                                    {
                                        // Positive values are six digits in length
                                        if (value > 999999)
                                        {
                                            value = 999999;
                                        }
                                        strValue = string.Format("{0:000000}", value);
                                    }
                                    if (strValue.Length != 6)
                                    {
                                        throw new ApplicationException("FBD: Value is not valid length");
                                    }

                                    // Convert the category and item numbers
                                    if (strCategory.Length != 1)
                                    {
                                        throw new ApplicationException("FBD: Category ID is not valid length");
                                    }

                                    string strItem = DebtPlus.Utils.Nulls.DInt(rd2["item"]).ToString("00");
                                    if (strItem.Length != 2)
                                    {
                                        throw new ApplicationException("FBD: Item ID is not valid length");
                                    }

                                    // Generate the output block
                                    sb.Append(strCategory);
                                    sb.Append(strItem);
                                    sb.Append(strValue);
                                }

                                // Emit the last record. The array "Records" is one extra to account for too many items.
                                sb.Insert(0, "FBD");
                                Records[nIndex] = sb.ToString().PadRight(75, '0');  // 72 characters plus the "FBD" makes 75.

                                // Generate the proposal records
                                creditor_message = FixedField(creditor_message, 144).ToUpper();
                                Generate_Proposal_Prenotes(strTraceNumber, drv, "FBD" + creditor_message.Substring(0, 72), "FBD" + creditor_message.Substring(72, 72), Records[0], Records[1], Records[2], Records[3], Records[4], Records[5], Records[6]);
                            }
                        }
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading FBD information");
            }
        }
        #endregion

        #region " Write Balance Verifications "

        /// <summary>
        /// Generate detail information for a CDV request
        /// </summary>
        private void Generate_CDV_Proposal(string strTraceNumber, DataRowView drv)
        {
            // Default the fields should the fields not be supplied as expected
            bool Prenote = false;
            decimal balance = 0m;
            string last_communication = string.Empty;
            string ssn = "000000000";
            Int32 client = 0;

            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdv_info";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];
                            using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
                            {
                                if (rd.Read())
                                {
                                    // Obtain the fields from the current record
                                    for (Int32 col = rd.FieldCount - 1; col >= 0; col += -1)
                                    {
                                        if (!rd.IsDBNull(col))
                                        {
                                            switch (rd.GetName(col).ToLower())
                                            {
                                                case "balance":
                                                    balance = System.Math.Truncate(Convert.ToDecimal(rd.GetValue(col)) * 100M) / 100M;
                                                    break;
                                                case "ssn":
                                                    ssn = Convert.ToString(rd.GetValue(col)).Trim().PadRight(9, '0').Substring(0, 9);
                                                    break;
                                                case "client":
                                                    client = Convert.ToInt32(rd.GetValue(col));
                                                    break;
                                                case "last_communication":
                                                    last_communication = Convert.ToString(rd.GetValue(col)).Trim();
                                                    break;
                                                case "prenote":
                                                    Prenote = Convert.ToInt32(rd.GetValue(col)) != 0;
                                                    break;
                                                default:
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Generate the record for the balance verification item
                System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);
                TextRecord.Append("CDV");
                if (balance <= 0m)
                {
                    TextRecord.Append(FormattedMoney(0m - balance).ToString("0000000000"));
                    TextRecord.Append(FixedField(last_communication, 15));
                    TextRecord.Append(ssn);
                    TextRecord.Append(FixedField(string.Format("{0:0000000}", client), 15));
                    TextRecord.Append("C");
                }
                else
                {
                    TextRecord.Append(FormattedMoney(balance).ToString("0000000000"));
                    TextRecord.Append(FixedField(last_communication, 15));
                    TextRecord.Append(ssn);
                    TextRecord.Append(FixedField(string.Format("{0:0000000}", client), 15));
                    TextRecord.Append("D");
                }

                // Include the prenote flag as needed
                TextRecord.Append(Prenote ? "Y" : " ");

                // Write the record with a prenote prefix
                Generate_Proposal_Prenotes(strTraceNumber, drv, TextRecord.ToString());
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading CDV information");
            }
        }
        #endregion

        #region " Write Account Number Change Requests "

        /// <summary>
        /// Generate detail information for a CDF request
        /// </summary>
        private void Generate_CDF_Proposal(string strTraceNumber, DataRowView drv)
        {
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdf_info";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];
                            using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
                            {
                                if (rd.Read())
                                {
                                    string client = DebtPlus.Utils.Nulls.DStr(rd["client"]);
                                    string company_identifier = DebtPlus.Utils.Nulls.DStr(rd["company_identifier"]);
                                    string ssn = DebtPlus.Utils.Nulls.DStr(rd["ssn"]);
                                    string new_account_number = DebtPlus.Utils.Nulls.DStr(rd["new_account_number"]);
                                    string exception_code = DebtPlus.Utils.Nulls.DStr(rd["exception_code"]);
                                    decimal monthly_payment_amount = DebtPlus.Utils.Nulls.DDec(rd["monthly_payment_amount"]);
                                    decimal balance = DebtPlus.Utils.Nulls.DDec(rd["balance"]);

                                    // Generate the record for the account number change item
                                    System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);
                                    TextRecord.Append("CDF");
                                    TextRecord.Append(FixedField(client, 15));
                                    TextRecord.Append(FixedField(company_identifier, 6));
                                    TextRecord.Append(FixedField(ssn, 9));
                                    TextRecord.Append(FixedField(new_account_number, 22));
                                    TextRecord.Append(FixedField(exception_code, 3));
                                    TextRecord.Append(FormattedMoney(monthly_payment_amount).ToString("0000000"));
                                    TextRecord.Append(FormattedMoney(balance).ToString("0000000000"));

                                    // Write the record with a prenote prefix
                                    Generate_Proposal_Prenotes(strTraceNumber, drv, TextRecord.ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading CDF information");
            }
        }
        #endregion

        #region " Write Drop Notices "

        /// <summary>
        /// Generate detail information for a CDD request
        /// </summary>
        private void Generate_CDD_Proposal(string strTraceNumber, DataRowView drv)
        {
            string ssn       = string.Empty;
            string client    = string.Empty;
            string rpps_code = string.Empty;

            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                            cmd.CommandText = "xpr_rpps_cdd_info";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = drv["rpps_transaction"];
                            using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
                            {
                                if (rd.Read())
                                {
                                    ssn       = DebtPlus.Utils.Nulls.DStr(rd["ssn"]);
                                    client    = DebtPlus.Utils.Nulls.DStr(rd["client"]);
                                    rpps_code = DebtPlus.Utils.Nulls.DStr(rd["rpps_code"]);
                                }
                            }
                        }
                    }
                }

                // Generate the record for the drop notice item
                System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);
                TextRecord.Append("CDD");
                TextRecord.Append(FixedField(immediate_origin, 6));
                TextRecord.Append(FixedField(ssn, 9));
                TextRecord.Append(FixedField(client, 15));
                TextRecord.Append(FixedField(rpps_code, 3));
                TextRecord.Append(' ', 40);

                // Write the record with a prenote prefix
                Generate_Proposal_Prenotes(strTraceNumber, drv, TextRecord.ToString());
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading CDD information");
            }
        }
        #endregion

        #region " Write message batches "

        /// <summary>
        /// Generate detail information for a CDN request
        /// </summary>
        private void Generate_CDN_Proposal(string TraceNumber, DataRowView drv)
        {
            string message        = string.Empty;
            decimal amount        = 0m;
            Int32 client          = 0;
            string strRecord      = string.Empty;
            string account_name   = string.Empty;
            string account_number = "MESSAGE";

            // Fetch the information for the main record
            message = Convert.ToString(drv["creditor_message"]);

            // Ensure that there are no strange characters.
            message = clean_string(message).Trim();

            // If there is no message then don't bother with this transaction
            if (message.Length == 0)
            {
                return;
            }

            client = DebtPlus.Utils.Nulls.DInt(drv["client"]);
            account_name = clean_string(DebtPlus.Utils.Nulls.DStr(drv["account_name"]));
            amount = DebtPlus.Utils.Nulls.DDec(drv["amount"]);
            account_number = clean_string(DebtPlus.Utils.Nulls.DStr(drv["account_number"]));

            // Determine the account name for the request
            if (account_name == string.Empty)
            {
                if (account_number == string.Empty)
                {
                    account_name = agency_name;
                    if (account_name == string.Empty)
                    {
                        account_name = "AGENCY NAME";
                    }
                }
                else if (client > 0)
                {
                    account_name = "CLIENT " + string.Format("{0:0000000}", client);
                }
            }

            // Count this prenote
            status_form.PrenoteCount += 1;

            // Count this record for the batch control
            Batch_DetailCount += 1;

            // Generate the first level message field
            System.Text.StringBuilder TextRecord = new System.Text.StringBuilder(100);
            TextRecord.Append(RPPS_Constants.RECORD_TYPE_ENTRY_DETAIL);
            TextRecord.Append(RPPS_Constants.ENTRY_TRANSACTION_CODE_PRENOTE);
            TextRecord.Append(' ', 26);
            TextRecord.Append(FormattedWholeDollars(amount).ToString("0000000000"));
            TextRecord.Append(FixedField(account_name, 15));
            TextRecord.Append(FixedField(account_number, 22));
            TextRecord.Append("  ");
            TextRecord.Append("1");
            TextRecord.Append(TraceNumber);

            // Write the prenote record to the buffer
            WriteRecord(TextRecord.ToString());

            // Split the message up into the appropriate number of additional lines
            Int32 addendum_sequence_number = 1;
            while (addendum_sequence_number < 100)
            {
                string LineText = null;

                // Stop at the end of the message.
                if (message == string.Empty)
                {
                    break;
                }

                // If the message fits then use the remainder of the message
                if (message.Length <= 77)
                {
                    LineText = message;
                    message = string.Empty;
                }
                else
                {
                    Int32 cols = 77;
                    while (cols > 0)
                    {
                        cols -= 1;
                        if (message[cols] == ' ')
                        {
                            break;
                        }
                    }

                    // Just break the line if we can't do a nice word wrap
                    if (cols <= 0)
                    {
                        cols = 77;
                    }

                    // Split the line into the pieces
                    LineText = message.Substring(0, cols).Trim();
                    message = message.Substring(cols).Trim();
                }

                // Write the message text record
                TextRecord = new System.Text.StringBuilder(100);
                TextRecord.Append(RPPS_Constants.RECORD_TYPE_ENTRY_DETAIL_ADDENDUM);
                TextRecord.Append(RPPS_Constants.ENTRY_TRANSACTION_MESSAGE);
                TextRecord.Append("CDN");
                TextRecord.Append(FixedField(LineText, 77));
                TextRecord.Append(addendum_sequence_number.ToString("0000"));
                TextRecord.Append(TraceNumber.Substring(8));

                WriteRecord(TextRecord.ToString());

                // Count this record for the batch control
                Batch_DetailCount += 1;
                addendum_sequence_number += 1;
            }
        }

        /// <summary>
        /// Process the Message requests
        /// </summary>
        public void WriteMessages()
        {
            DataRowView drv = null;

            // Retrieve the transactions to be processed
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
            cmd.CommandText = "SELECT [rpps_transaction],[rpps_file],[trace_number_first],[trace_number_last],[rpps_batch],[death_date],[service_class_or_purpose],[account_name],[biller_id],[biller_name],[account_number],[creditor_message],[client],[amount] FROM view_rpps_cdn_info WHERE rpps_file=@rpps_file";
            cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = ap.rpps_file;

            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                da.Fill(ds, "view_rpps_cdn_info");
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading pending CDN transactions");
            }

            finally
            {
                Cursor.Current = current_cursor;
            }

            // Process the transactions
            DataTable tbl1 = ds.Tables["view_rpps_cdn_info"];
            if (tbl1 != null)
            {
                DataView vue = new DataView(tbl1, string.Empty, "biller_id,account_number", DataViewRowState.CurrentRows);

                // Current batch information
                string last_rpps_biller_id = string.Empty;
                string BatchTraceNumber = string.Empty;
                Int32 rpps_batch = -1;

                // Process the proposals and other records in the batch
                foreach (DataRowView drvLoop in vue)
                {
                    drv = drvLoop;
                    drv.BeginEdit();

                    // Find the current biller id
                    string current_rpps_biller_id = DebtPlus.Utils.Nulls.DStr(drv["biller_id"]).Trim();

                    // Close the current biller if the fields differ
                    if (last_rpps_biller_id != string.Empty && last_rpps_biller_id != current_rpps_biller_id)
                    {
                        Write_Batch_Control(BatchTraceNumber, last_rpps_biller_id);
                        last_rpps_biller_id = string.Empty;
                    }

                    // If there is no biller then start one
                    if (last_rpps_biller_id == string.Empty)
                    {
                        last_rpps_biller_id = current_rpps_biller_id;

                        // Generate the batch trace number and add the batch to the tables
                        rpps_batch = Next_Batch(immediate_origin, drv);
                        BatchTraceNumber = current_batch_trace();

                        // Record the trace number for later update
                        var tbl2 = ds.Tables["rpps_batches"];
                        DataRow row = tbl2.NewRow();
                        row["rpps_batch"] = rpps_batch;
                        row["trace_number"] = BatchTraceNumber;
                        tbl2.Rows.Add(row);

                        // Write the batch header
                        Write_Batch_Header(BatchTraceNumber, drv, RPPS_Constants.ENTRY_DESCRIPTION_MESSAGE);
                    }

                    // Generate a trace number for the current record
                    string TransactionTraceNumber = Next_Detail_Trace();
                    drv["trace_number_first"] = TransactionTraceNumber;
                    drv["rpps_batch"] = rpps_batch;
                    drv["rpps_file"] = ap.rpps_file;

                    // Process the transactions
                    status_form.CountCDN += 1;

                    Generate_CDN_Proposal(TransactionTraceNumber, drv);

                    // Save the last trace number
                    drv["trace_number_last"] = current_detail_trace();

                    // Mark the edit operation as complete
                    drv.EndEdit();
                }

                // Close the current biller at the end of the last batch
                if (last_rpps_biller_id != string.Empty)
                {
                    Write_Batch_Control(BatchTraceNumber, last_rpps_biller_id);
                    last_rpps_biller_id = string.Empty;
                }
            }
        }
        #endregion

        #region " Write EDI items "

        /// <summary>
        /// Process the RPS proposal requests
        /// </summary>
        public void WriteProposals()
        {
            DataRowView drv = null;

            // Retrieve the transactions to be processed
            using (var cmd = new System.Data.SqlClient.SqlCommand())
            {
                cmd.Connection = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                cmd.CommandText = "SELECT [rpps_transaction],[rpps_file],[trace_number_first],[trace_number_last],[rpps_batch],[service_class_or_purpose],[death_date],[account_number],[account_name],[biller_id],[biller_name] FROM view_rpps_cdp_info WHERE rpps_file=@rpps_file";
                cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = ap.rpps_file;

                try
                {
                    using (var cm = new DebtPlus.UI.Common.CursorManager())
                    {
                        using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "view_rpps_cdp_info");
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading pending transactions");
                }
            }

            // Process the transactions
            DataTable tbl = ds.Tables["view_rpps_cdp_info"];
            if (tbl != null)
            {
                DataView vue = new DataView(tbl, "service_class_or_purpose<>'CDN'", "biller_id,service_class_or_purpose,account_number", DataViewRowState.CurrentRows);

                // Current batch information
                string last_rpps_biller_id = string.Empty;
                string BatchTraceNumber = string.Empty;
                Int32 rpps_batch = -1;

                // Process the proposals and other records in the batch
                foreach (DataRowView drvLoop in vue)
                {
                    drv = drvLoop;
                    drv.BeginEdit();

                    // Find the current biller id
                    string current_rpps_biller_id = DebtPlus.Utils.Nulls.DStr(drv["biller_id"]).Trim();

                    // Close the current biller if the fields differ
                    if (last_rpps_biller_id != string.Empty && last_rpps_biller_id != current_rpps_biller_id)
                    {
                        Write_Batch_Control(BatchTraceNumber, last_rpps_biller_id);
                        last_rpps_biller_id = string.Empty;
                    }

                    // If there is no biller then start one
                    if (last_rpps_biller_id == string.Empty)
                    {
                        last_rpps_biller_id = current_rpps_biller_id;

                        // Generate the batch trace number and add the batch to the tables
                        rpps_batch = Next_Batch(immediate_origin, drv);
                        BatchTraceNumber = current_batch_trace();

                        // Record the trace number for later update
                        var tblBatch = ds.Tables["rpps_batches"];
                        DataRow row = tblBatch.NewRow();
                        row["rpps_batch"] = rpps_batch;
                        row["trace_number"] = BatchTraceNumber;
                        tblBatch.Rows.Add(row);

                        // Write the batch header
                        Write_Batch_Header(BatchTraceNumber, drv, RPPS_Constants.ENTRY_DESCRIPTION_PAYMENT);
                    }

                    // Generate a trace number for the current record
                    string TransactionTraceNumber = Next_Detail_Trace();
                    drv["trace_number_first"] = TransactionTraceNumber;
                    drv["rpps_batch"] = rpps_batch;
                    drv["rpps_file"] = ap.rpps_file;

                    // Process the transactions
                    switch (Convert.ToString(drv["service_class_or_purpose"]).ToUpper())
                    {
                        case "CDP":
                            status_form.CountCDP += 1;
                            Generate_CDP_Proposal(TransactionTraceNumber, drv);
                            break;
                        case "CDD":
                            status_form.CountCDD += 1;
                            Generate_CDD_Proposal(TransactionTraceNumber, drv);
                            break;
                        case "CDF":
                            status_form.CountCDF += 1;
                            Generate_CDF_Proposal(TransactionTraceNumber, drv);
                            break;
                        case "CDV":
                            status_form.CountCDV += 1;
                            Generate_CDV_Proposal(TransactionTraceNumber, drv);
                            break;
                        case "FBC":
                            status_form.CountFBC += 1;
                            Generate_FBC_Proposal(TransactionTraceNumber, drv);
                            break;
                        case "FBD":
                            status_form.CountFBD += 1;
                            Generate_FBD_Proposal(TransactionTraceNumber, drv);
                            break;
                    }

                    // Save the last trace number
                    TransactionTraceNumber = current_detail_trace();
                    drv["trace_number_last"] = TransactionTraceNumber;

                    // Mark the edit operation as complete
                    drv.EndEdit();
                }

                // Close the current biller at the end of the last batch
                if (last_rpps_biller_id != string.Empty)
                {
                    Write_Batch_Control(BatchTraceNumber, last_rpps_biller_id);
                    last_rpps_biller_id = string.Empty;
                }
            }
        }
        #endregion

        #region " Dropped Proposals Report "

        /// <summary>
        /// Generate the report for dropped proposals
        /// </summary>
        private void PrintDroppedProposalsReport()
        {
            DebtPlus.Interfaces.Reports.IReports rpt = DebtPlus.Reports.RPPS.DroppedProposals.DroppedProposalsReport.CreateReport();
            rpt.RunReportInSeparateThread();
        }
        #endregion

        #region " Do the main routine for this class "

        /// <summary>
        /// Process the RPS proposal requests
        /// </summary>
        public bool Process_Proposals()
        {
            bool answer = false;

            // Create a table for the batch list
            DataTable tbl = ds.Tables["rpps_batches"];
            if (tbl != null)
            {
                tbl.Clear();
            }
            else
            {
                tbl = ds.Tables.Add("rpps_batches");
                tbl.Columns.Add("rpps_batch", typeof(Int32)).ReadOnly = false;
                tbl.Columns.Add("trace_number", typeof(string)).ReadOnly = false;
                tbl.PrimaryKey = new DataColumn[] { tbl.Columns[0] };
            }

            // We need the output directory from the configuration information to create the filename
            if (!ReadProposalConfiguration())
            {
                return false;
            }

            // Ask where we are to save the file. If canceled then abort the creation.
            if (!request_file_name())
            {
                return false;
            }

            // We need the remainder of the proposal information again once we have a file name.
            if (!ReadProposalConfiguration())
            {
                return false;
            }

            // Generate the data that we are going to process.
            if (ap.rpps_file <= 0)
            {
                // Start by creating the RPPS file.
                ap.rpps_file = generate_rpps_file(FileName, "EDI");

                // Include transactions into the proposal database
                Generate_CDV();         // Balance verifications
                Generate_CDP();         // Proposals
                Generate_CDD();         // Drop requests
                Generate_CDN();         // Additional message blocks

                // Display the proposals dropped from this batch
                PrintDroppedProposalsReport();
            }

            do
            {
                // Reset the values for the new file processing
                batch_count       = 0;
                transaction_count = 0;
                BillerType        = 0;
                File_BatchCount   = 0;
                File_DetailCount  = 0;
                File_Debit        = 0m;
                File_Credit       = 0m;

                // Reset the counters
                status_form.CountCDP     = 0;
                status_form.CountCDD     = 0;
                status_form.CountCDF     = 0;
                status_form.CountCDP     = 0;
                status_form.CountCDV     = 0;
                status_form.CountFBC     = 0;
                status_form.CountFBD     = 0;
                status_form.CountCDN     = 0;
                status_form.PrenoteCount = 0;
                status_form.DetailCount  = 0;

                // Empty the current output "file" contents
                OutputText = new System.Text.StringBuilder();

                // Write the prefix to the output text buffer
                if (prefix_line != string.Empty)
                {
                    OutputText.Append(prefix_line);
                    OutputText.Append(Environment.NewLine);
                }

                // Write the file header to the output text
                Write_File_Header();

                // Write the proposals to the file
                WriteProposals();

                // Write the message items
                WriteMessages();

                // Write the file trailer
                Write_File_Control();

                // Write the suffix to the output text buffer
                if (suffix_line != string.Empty)
                {
                    OutputText.Append(suffix_line);
                    OutputText.Append(Environment.NewLine);
                }

                // If the operation is not for real then just return a failure here.
                if (Fake)
                {
                    return false;
                }

                // Do the updates to the database. We are successful only if all works.
                try
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (var txn = cn.BeginTransaction(IsolationLevel.RepeatableRead))
                        {
                            // Try to update the banks table with the new trace numbers. If no one was doing this while we were writing then everything is good.
                            // However, if the trace numbers were changed then we need to restart the processsing so don't bother to set the trace numbers in the
                            // transactions as we need to regenerate the information again.
                            answer = UpdateBanksTable(cn, txn);

                            // If successful, update the trace numbers and commit the transaction. If failure, the transaction will be aborted.
                            if (answer)
                            {
                                UpdateTransactionTraceNumbers(cn, txn);
                                UpdateBatchTraceNumbers(cn, txn);

                                // Commit the transactions and return success to the caller.
                                txn.Commit();
                                return true;
                            }

                            // Rollback the updates and do it again. This may have an error as well. Ignore it.
                            try
                            {
                                txn.Rollback();
                            }
                            catch { }
                        }
                    }
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording trace information");
                    return false;
                }

                // Tell the user that the tables were changed and we are redoing the save operation.
                DebtPlus.Data.Forms.MessageBox.Show("The underlying database table has been changed by another program." + Environment.NewLine + "(Someone generated another RPPS file while you were do it.)" + Environment.NewLine + Environment.NewLine + "The output file save has been suppressed and the transactions will be regenerated.", "Database consistency check failed", MessageBoxButtons.OK, MessageBoxIcon.Error);

                // Re-read the proposal configuration information to get new trace numbers
                if (!ReadProposalConfiguration())
                {
                    return false;
                }
            } while (true);
        }
        #endregion
    }
}
