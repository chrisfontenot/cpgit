
#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.LINQ;
using System.Linq;

namespace DebtPlus.UI.Desktop.CS.RPPS.Proposals
{
    internal partial class frmBank : DebtPlus.Data.Forms.DebtPlusForm
    {
        private DebtPlus.UI.Desktop.CS.RPPS.Proposals.ArgParser ap;

        /// <summary>
        /// Initialize the new form
        /// </summary>
        public frmBank() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialize the new form
        /// </summary>
        /// <param name="ap"></param>
        public frmBank(DebtPlus.UI.Desktop.CS.RPPS.Proposals.ArgParser ap) : this()
        {
            this.ap = ap;
            RegisterHandlers();
        }

        /// <summary>
        /// Register the event handlers
        /// </summary>
        private void RegisterHandlers()
        {
            Load                                  += Form_Load;
            LookUpEdit_Bank.EditValueChanging     += DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_Bank.EditValueChanged      += checkEdit_ExistingFile_CheckedChanged;
            checkEdit_ExistingFile.CheckedChanged += checkEdit_ExistingFile_CheckedChanged;

            // There is no need to test the checkEdit_NewFile since it changes when the checkEdit_ExistingFile changes.
            checkEdit_ExistingFile.CheckedChanged += FormChanged;
            checkEdit_LocalFile.CheckedChanged    += FormChanged;
            LookUpEdit_Bank.EditValueChanged      += FormChanged;
            lookUpEdit_FileList.EditValueChanged  += FormChanged;

            Button_OK.Click                       += Button_OK_Click;
        }

        /// <summary>
        /// Remove the event handler registrations
        /// </summary>
        private void UnRegisterHandlers()
        {
            Load                                  -= Form_Load;
            LookUpEdit_Bank.EditValueChanging     -= DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit_Bank.EditValueChanged      -= checkEdit_ExistingFile_CheckedChanged;
            checkEdit_ExistingFile.CheckedChanged -= checkEdit_ExistingFile_CheckedChanged;

            checkEdit_ExistingFile.CheckedChanged -= FormChanged;
            checkEdit_LocalFile.CheckedChanged    -= FormChanged;
            LookUpEdit_Bank.EditValueChanged      -= FormChanged;
            lookUpEdit_FileList.EditValueChanged  -= FormChanged;

            Button_OK.Click                       -= Button_OK_Click;
        }

        #region "Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        public System.Windows.Forms.ToolTip ToolTip1;
        public DevExpress.XtraEditors.SimpleButton Button_Cancel;
        public DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_Bank;
        internal DevExpress.XtraEditors.LookUpEdit lookUpEdit_FileList;
        private DevExpress.XtraEditors.CheckEdit checkEdit_ExistingFile;
        private DevExpress.XtraEditors.CheckEdit checkEdit_NewFile;
        private DevExpress.XtraEditors.CheckEdit checkEdit_LocalFile;
        public DevExpress.XtraEditors.LabelControl Label1;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBank));
            this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.LookUpEdit_Bank = new DevExpress.XtraEditors.LookUpEdit();
            this.lookUpEdit_FileList = new DevExpress.XtraEditors.LookUpEdit();
            this.checkEdit_ExistingFile = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_NewFile = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit_LocalFile = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Bank.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_FileList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ExistingFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_NewFile.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_LocalFile.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_Cancel.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Button_Cancel.Appearance.Options.UseFont = true;
            this.Button_Cancel.Appearance.Options.UseForeColor = true;
            this.Button_Cancel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(190, 181);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Button_Cancel.Size = new System.Drawing.Size(73, 25);
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button_OK.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Button_OK.Appearance.Options.UseFont = true;
            this.Button_OK.Appearance.Options.UseForeColor = true;
            this.Button_OK.Cursor = System.Windows.Forms.Cursors.Default;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(111, 181);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Button_OK.Size = new System.Drawing.Size(73, 25);
            this.Button_OK.TabIndex = 6;
            this.Button_OK.Text = "&OK";
            // 
            // Label1
            // 
            this.Label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Label1.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.Label1.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.Label1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.Label1.Location = new System.Drawing.Point(16, 12);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(347, 50);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Please choose the appropriate RPPS bank account for the transactions. Each RPPS b" +
    "ank account holds the orignator ID and various other parameters that are needed " +
    "to properly build the RPPS file.";
            this.Label1.UseMnemonic = false;
            // 
            // LookUpEdit_Bank
            // 
            this.LookUpEdit_Bank.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LookUpEdit_Bank.Location = new System.Drawing.Point(18, 68);
            this.LookUpEdit_Bank.Name = "LookUpEdit_Bank";
            this.LookUpEdit_Bank.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LookUpEdit_Bank.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Id", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)});
            this.LookUpEdit_Bank.Properties.DisplayMember = "description";
            this.LookUpEdit_Bank.Properties.NullText = "";
            this.LookUpEdit_Bank.Properties.ShowFooter = false;
            this.LookUpEdit_Bank.Properties.ShowHeader = false;
            this.LookUpEdit_Bank.Properties.SortColumnIndex = 1;
            this.LookUpEdit_Bank.Properties.ValueMember = "Id";
            this.LookUpEdit_Bank.Size = new System.Drawing.Size(345, 20);
            this.LookUpEdit_Bank.TabIndex = 1;
            // 
            // lookUpEdit_FileList
            // 
            this.lookUpEdit_FileList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lookUpEdit_FileList.Location = new System.Drawing.Point(161, 119);
            this.lookUpEdit_FileList.Name = "lookUpEdit_FileList";
            this.lookUpEdit_FileList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit_FileList.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 10, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Descending, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date Created", 30, DevExpress.Utils.FormatType.DateTime, "MM/dd/yyyy h:mm tt", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "Creator", 30, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("filename", "FileName", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True)});
            this.lookUpEdit_FileList.Properties.DisplayFormat.FormatString = @"MM/dd/yyyy \a\t h:mm tt";
            this.lookUpEdit_FileList.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.lookUpEdit_FileList.Properties.DisplayMember = "date_created";
            this.lookUpEdit_FileList.Properties.NullText = "";
            this.lookUpEdit_FileList.Properties.ShowFooter = false;
            this.lookUpEdit_FileList.Properties.ShowHeader = true;
            this.lookUpEdit_FileList.Properties.SortColumnIndex = 1;
            this.lookUpEdit_FileList.Properties.ValueMember = "Id";
            this.lookUpEdit_FileList.Size = new System.Drawing.Size(202, 20);
            this.lookUpEdit_FileList.Properties.PopupWidth = 342;
            this.lookUpEdit_FileList.TabIndex = 4;
            // 
            // checkEdit_ExistingFile
            // 
            this.checkEdit_ExistingFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_ExistingFile.Location = new System.Drawing.Point(18, 119);
            this.checkEdit_ExistingFile.Name = "checkEdit_ExistingFile";
            this.checkEdit_ExistingFile.Properties.Caption = "Regenerate existing file";
            this.checkEdit_ExistingFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit_ExistingFile.Properties.RadioGroupIndex = 1;
            this.checkEdit_ExistingFile.Size = new System.Drawing.Size(137, 19);
            this.checkEdit_ExistingFile.TabIndex = 3;
            this.checkEdit_ExistingFile.TabStop = false;
            // 
            // checkEdit_NewFile
            // 
            this.checkEdit_NewFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_NewFile.EditValue = true;
            this.checkEdit_NewFile.Location = new System.Drawing.Point(18, 94);
            this.checkEdit_NewFile.Name = "checkEdit_NewFile";
            this.checkEdit_NewFile.Properties.Caption = "Generate a new file";
            this.checkEdit_NewFile.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio;
            this.checkEdit_NewFile.Properties.RadioGroupIndex = 1;
            this.checkEdit_NewFile.Size = new System.Drawing.Size(137, 19);
            this.checkEdit_NewFile.TabIndex = 2;
            // 
            // checkEdit_LocalFile
            // 
            this.checkEdit_LocalFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkEdit_LocalFile.Location = new System.Drawing.Point(20, 145);
            this.checkEdit_LocalFile.Name = "checkEdit_LocalFile";
            this.checkEdit_LocalFile.Properties.Caption = "Use your document folder rather than the share for the output file";
            this.checkEdit_LocalFile.Size = new System.Drawing.Size(343, 19);
            this.checkEdit_LocalFile.TabIndex = 5;
            // 
            // frmBank
            // 
            this.AcceptButton = this.Button_OK;
            this.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Appearance.Options.UseFont = true;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(375, 218);
            this.Controls.Add(this.checkEdit_LocalFile);
            this.Controls.Add(this.checkEdit_NewFile);
            this.Controls.Add(this.checkEdit_ExistingFile);
            this.Controls.Add(this.lookUpEdit_FileList);
            this.Controls.Add(this.LookUpEdit_Bank);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.Label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(4, 30);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBank";
            this.Text = "Bank Account Selection";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LookUpEdit_Bank.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit_FileList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_ExistingFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_NewFile.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit_LocalFile.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion "Windows Form Designer generated code "

        /// <summary>
        /// Process the LOAD event for the form
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        private void Form_Load(object Sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                LookUpEdit_Bank.Properties.DataSource = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.type == "R" && s.ActiveFlag);
                if (ap.Bank > 0)
                {
                    LookUpEdit_Bank.EditValue = ap.Bank;
                }

                // Load the RPPS file selection information
                checkEdit_NewFile.Checked = ap.rpps_file <= 0;
                checkEdit_ExistingFile.Checked = ap.rpps_file > 0;
                lookUpEdit_FileList.EditValue = ap.rpps_file;

                // Enable the list control if the radio button is selected
                lookUpEdit_FileList.Enabled = checkEdit_ExistingFile.Checked;
                if (checkEdit_ExistingFile.Checked)
                {
                    PopulateFileList(ap.Bank);
                }

                // The checked status for the local file comes from the flag.
                checkEdit_ExistingFile.Checked = ap.LocalFile;
                Button_OK.Enabled = !HasErrors();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Enable the list for the file when the user chooses the radio for the existing file.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkEdit_ExistingFile_CheckedChanged(object sender, EventArgs e)
        {
            lookUpEdit_FileList.Enabled = checkEdit_ExistingFile.Checked;
            if (checkEdit_ExistingFile.Checked)
            {
                PopulateFileList(DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Bank.EditValue));
            }
        }

        /// <summary>
        /// Populate the list of files associated with the designated bank ID
        /// </summary>
        /// <param name="bankID"></param>
        private void PopulateFileList(System.Nullable<Int32> bankID)
        {
            lookUpEdit_FileList.EditValue = null;
            Button_OK.Enabled = false;

            if (bankID.HasValue && bankID.Value > 0)
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    lookUpEdit_FileList.Properties.DataSource = bc.rpps_files.Where(s => s.file_type == "EDI" && s.bank == bankID.Value).OrderByDescending(s => s.date_created).Take(30).ToList();
                    return;
                }
            }
            lookUpEdit_FileList.Properties.DataSource = null;
        }

        /// <summary>
        /// Handle the change on a control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormChanged(object sender, EventArgs e)
        {
            Button_OK.Enabled = !HasErrors();
        }

        /// <summary>
        /// Determine if the input form has some errors in it
        /// </summary>
        /// <returns></returns>
        private bool HasErrors()
        {
            if (checkEdit_ExistingFile.Checked)
            {
                if (DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_FileList.EditValue).GetValueOrDefault() < 1)
                {
                    return true;
                }
            }

            if (LookUpEdit_Bank.EditValue == null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Handle the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            // Get the file identifier for the RPPS file
            if (checkEdit_NewFile.Checked)
            {
                ap.rpps_file = -1;
            }
            else
            {
                ap.rpps_file = DebtPlus.Utils.Nulls.v_Int32(lookUpEdit_FileList.EditValue).GetValueOrDefault();
            }

            // Find the bank and the local file flag.
            ap.Bank = DebtPlus.Utils.Nulls.v_Int32(LookUpEdit_Bank.EditValue).GetValueOrDefault();
            ap.LocalFile = checkEdit_LocalFile.Checked;
        }
    }
}