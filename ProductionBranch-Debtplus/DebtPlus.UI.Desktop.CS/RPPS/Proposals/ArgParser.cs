using System;

using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Proposals
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Bank to generate proposals
        /// </summary>
        public System.Int32 Bank { get; set; }

        /// <summary>
        /// Current output RPPS file identifier in the rpps_files table
        /// </summary>
        public System.Int32 rpps_file { get; set; }

        /// <summary>
        /// Override banks table and use "my documents"?
        /// (This is mainly for testing against a customer's site)
        /// </summary>
        public bool LocalFile { get; set; }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {"b", "f", "l" })
        {
            this.Bank = -1;
            this.rpps_file = -1;
            this.LocalFile = false;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            double TempDouble = 0;

            switch (switchSymbol.ToLower())
            {
                case "b":
                    if (double.TryParse(switchValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out TempDouble))
                    {
                        if (TempDouble > 0.0 && TempDouble <= Convert.ToDouble(Int32.MaxValue))
                        {
                            Bank = Convert.ToInt32(TempDouble);
                            break;
                        }
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "l":
                    LocalFile = true;
                    break;

                case "f":
                    if (double.TryParse(switchValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out TempDouble))
                    {
                        if (TempDouble > 0.0 && TempDouble <= Convert.ToDouble(Int32.MaxValue))
                        {
                            rpps_file = Convert.ToInt32(TempDouble);
                            break;
                        }
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Process the completion of the parse operation
        /// </summary>
        protected override SwitchStatus OnDoneParse()
        {
            // Default the bank if needed. We want an RPPS bank.
            if (Bank < 1)
            {
                Bank = DebtPlus.LINQ.Cache.bank.getDefault("R").GetValueOrDefault(-1);
            }

            // Ask the user for the current options.
            using (var frm = new frmBank(this))
            {
                if (frm.ShowDialog() != DialogResult.OK)
                {
                    return global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                }
            }

            return global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
        }
    }
}