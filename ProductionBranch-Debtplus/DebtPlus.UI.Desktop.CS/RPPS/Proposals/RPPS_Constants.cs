#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Proposals
{
    internal static class RPPS_Constants
    {
        // Length of each record in the database file

        public const short FILE_RECORD_LENGTH = 94;

        // File type codes for the various record formats
        public const string RECORD_TYPE_FILE_HEADER = "1";

        public const string RECORD_TYPE_BATCH_HEADER = "5";
        public const string RECORD_TYPE_ENTRY_DETAIL = "6";
        public const string RECORD_TYPE_ENTRY_DETAIL_ADDENDUM = "7";
        public const string RECORD_TYPE_BATCH_CONTROL = "8";

        public const string RECORD_TYPE_FILE_CONTROL = "9";

        public const string FILE_PRIORITY_CODE = "01";

        public const string FILE_IMMEDIATE_DESTINATION = " 999900004";

        public const string FILE_ID_MODIFIER_VALUE = "1";

        public const string FILE_RECORD_SIZE = "094";

        public const string FILE_BLOCKING_FACTOR = "10";

        public const string FILE_FORMAT_CODE = "1";

        public const string FILE_DESTINATION = "MC REMIT PROC CENT SITE";
        public const string FILE_REFERENCE_CODE_TESTING = "ORIGTEST";

        public const string FILE_REFERENCE_CODE_NORMAL = "        ";

        public const string BATCH_ORIGINATOR_STATUS_CODE = "1";
        public const string SERVICE_CLASS_CREDIT = "220";
        public const string SERVICE_CLASS_DEBIT = "225";

        public const string SERVICE_CLASS_CREDIT_DEBIT = "200";

        // Payments
        public const string ENTRY_CLASS_CIE = "CIE";

        // Proposals
        public const string ENTRY_CLASS_CDP = "CDP";

        // Balance verifications
        public const string ENTRY_CLASS_CDV = "CDV";

        // Drops
        public const string ENTRY_CLASS_CDD = "CDD";

        // Returns (for concentrators only)
        public const string ENTRY_CLASS_COR = "COR";

        public const string ENTRY_CLASS_BLANK = "   ";
        public const string ENTRY_DESCRIPTION_PAYMENT = "RPS PAYMNT";
        public const string ENTRY_DESCRIPTION_REVERSAL = "REVERSAL  ";
        public const string ENTRY_DESCRIPTION_RETURN = "RETURN    ";

        public const string ENTRY_DESCRIPTION_MESSAGE = "MESSAGE   ";
        public const string ENTRY_TRANSACTION_CODE_CREDIT = "21";
        public const string ENTRY_TRANSACTION_CODE_PAYMENT = "22";
        public const string ENTRY_TRANSACTION_CODE_PRENOTE = "23";
        public const string ENTRY_TRANSACTION_CODE_RETURN = "26";
        public const string ENTRY_TRANSACTION_CODE_REVERSAL = "27";

        public const string ENTRY_TRANSACTION_MESSAGE = "40";
        public const string ENTRY_ADDENDUM_UNUSED = "0";

        public const string ENTRY_ADDENDUM_USED = "1";
        public const string ADDENDUM_TYPE_CODE_RETURN = "99";
        public const string ADDENDUM_TYPE_CODE_NONFINANCIAL = "98";

        public const string ADDENDUM_TYPE_CODE_REVERSAL = "97";
        //     General layout of the records in the file
        //
        //     FILE HEADER RECORD
        //     BATCH HEADER RECORD             Batch number 1
        //     ENTRY DETAIL RECORD 1
        //     ...
        //     ENTRY DETAIL RECORD N
        //     BATCH CONTROL RECORD
        //
        //     BATCH HEADER RECORD             Batch number 2
        //     ENTRY DETAIL RECORD 1
        //     ...
        //     ENTRY DETAIL RECORD N
        //     BATCH CONTROL RECORD
        //
        //     BATCH HEADER RECORD             Batch number 3
        //     ENTRY DETAIL RECORD 1
        //     ...
        //     ENTRY DETAIL RECORD N
        //     BATCH CONTROL RECORD
        //     FILE CONTROL RECORD
        //
    }
}