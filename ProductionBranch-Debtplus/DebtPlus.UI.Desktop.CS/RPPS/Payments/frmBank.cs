#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Linq;
using DebtPlus.LINQ;

namespace DebtPlus.UI.Desktop.CS.RPPS.Payments
{
    [System.Runtime.InteropServices.ComVisible(false)]
    internal partial class frmBank
    {
        private System.Collections.Generic.List<DebtPlus.LINQ.bank> colBanks;

        private System.Int32 _bank = -1;

        public System.Int32 bank
        {
            get { return _bank; }
        }

        public frmBank(System.Collections.Generic.List<DebtPlus.LINQ.bank> colBanks) : base()
        {
            InitializeComponent();
            this.colBanks = colBanks;

            Load += Form_Load;
            LookUpEdit1.EditValueChanged += Bank_EditValueChanged;
        }

        /// <summary>
        /// The bank was changed
        /// </summary>
        private void Bank_EditValueChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (LookUpEdit1.EditValue != null)
            {
                _bank = Convert.ToInt32(LookUpEdit1.EditValue);
            }

            Button_OK.Enabled = (_bank > 0);
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        private void Form_Load(object Sender, System.EventArgs e)
        {
            BankListLookupEdit_Load(ref LookUpEdit1);
        }

        /// <summary>
        /// Load the bank information
        /// </summary>

        private void BankListLookupEdit_Load(ref DevExpress.XtraEditors.LookUpEdit ctl)
        {
            // Set the datasource for the collection
            ctl.Properties.DataSource = colBanks;

            // See if there is a default item. If so, choose it
            bank def = colBanks.Find(s => s.Default);
            if (def != null)
            {
                ctl.EditValue = def.Id;
                return;
            }

            // Set the item to the first one
            System.Nullable<Int32> defId = colBanks.Min(s => s.Id);
            if (defId.HasValue)
            {
                ctl.EditValue = defId;
                return;
            }
        }
    }
}