#Region "Copyright 2000-2012 DebtPlus, L.L.C."
'{*******************************************************************}
'{                                                                   }
'{       DebtPlus Debt Management System                             }
'{                                                                   }
'{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
'{       ALL RIGHTS RESERVED                                         }
'{                                                                   }
'{   The entire contents of this file is protected by U.S. and       }
'{   International Copyright Laws. Unauthorized reproduction,        }
'{   reverse-engineering, and distribution of all or any portion of  }
'{   the code contained in this file is strictly prohibited and may  }
'{   result in severe civil and criminal penalties and will be       }
'{   prosecuted to the maximum extent possible under the law.        }
'{                                                                   }
'{   RESTRICTIONS                                                    }
'{                                                                   }
'{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
'{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
'{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
'{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
'{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
'{                                                                   }
'{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
'{   ADDITIONAL RESTRICTIONS.                                        }
'{                                                                   }
'{*******************************************************************}
#End Region

Option Compare Binary
Option Strict On
Option Explicit On

Imports DebtPlus.Utils
Imports System.Data.SqlClient
Imports System.Windows.Forms
Imports DebtPlus.LINQ
Imports System.Linq

Namespace RPPS.Payments
    <System.Runtime.InteropServices.ComVisible(False)>
    Friend Class frmBank

        Private colBanks As System.Collections.Generic.List(Of DebtPlus.LINQ.bank)
        Private _bank As System.Int32 = -1

        Public ReadOnly Property bank() As System.Int32
            Get
                Return _bank
            End Get
        End Property

        Public Sub New(colBanks As System.Collections.Generic.List(Of DebtPlus.LINQ.bank))
            MyBase.New()
            InitializeComponent()
            Me.colBanks = colBanks

            AddHandler Load, AddressOf Form_Load
            AddHandler LookUpEdit1.EditValueChanged, AddressOf Bank_EditValueChanged
        End Sub

        ''' <summary>
        ''' The bank was changed
        ''' </summary>
        Private Sub Bank_EditValueChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
            If LookUpEdit1.EditValue IsNot Nothing Then
                _bank = Convert.ToInt32(LookUpEdit1.EditValue)
            End If

            Button_OK.Enabled = (_bank > 0)
        End Sub

        ''' <summary>
        ''' Process the FORM LOAD event
        ''' </summary>
        Private Sub Form_Load(ByVal Sender As Object, ByVal e As System.EventArgs)
            BankListLookupEdit_Load(LookUpEdit1)
        End Sub

        ''' <summary>
        ''' Load the bank information
        ''' </summary>
        Private Sub BankListLookupEdit_Load(ByRef ctl As DevExpress.XtraEditors.LookUpEdit)

            ' Set the datasource for the collection
            ctl.Properties.DataSource = colBanks

            ' See if there is a default item. If so, choose it
            Dim def As bank = colBanks.Find(Function(s) s.default = True)
            If def IsNot Nothing Then
                ctl.EditValue = def.Id
                Return
            End If

            ' Set the item to the first one
            Dim defId As Int32? = colBanks.Min(Function(s) s.Id)
            If defId.HasValue Then
                ctl.EditValue = defId
                Return
            End If
        End Sub
    End Class
End Namespace