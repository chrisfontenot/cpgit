using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Desktop.CS.Disbursement.Common;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Payments
{
    internal partial class form_RPS : Iform_RPS
    {
        /// <summary>
        /// Create instances of this form
        /// </summary>
        public form_RPS() : base()
        {
            InitializeComponent();
        }

        private ArgParser ap = null;

        public form_RPS(ArgParser ap) : this()
        {
            this.ap = ap;
            this.Load += form_RPS_Load;
            BackgroundWorker1.DoWork += BackgroundWorker1_DoWork;
            BackgroundWorker1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted;
            Button_OK.Click += Button_OK_Click;
            Button_Print.Click += Button_OK_Click;
        }

        /// <summary>
        /// Routine to hold the processing class info
        /// </summary>
        public item_info cls_item
        {
            // do nothing. We don't need anything done here.
            set { }
        }

        /// <summary>
        /// Determine the output file name
        /// </summary>
        public System.IO.StreamWriter Request_FileName(System.Int32 disbursement_register, System.Int32 Bank, string FileType)
        {
            return Request_FileName(disbursement_register, Bank, FileType, false);
        }

        public System.IO.StreamWriter Request_FileName(System.Int32 disbursement_register, System.Int32 Bank, string FileType, bool UseLocalFileOnly)
        {
            GenerateRPSPayments.FileName = ap.OutputFileName;
            return new System.IO.StreamWriter(new System.IO.FileStream(GenerateRPSPayments.FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.Read, 4096), System.Text.Encoding.ASCII, 4096);
        }

        private delegate void UpdateDecimalDelegate(decimal value);

        private delegate void UpdateIntegerDelegate(System.Int32 value);

        private delegate void UpdateStringDelegate(string value);

        #region " total_debit "

        private void update_total_debt(decimal value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateDecimalDelegate(update_total_debt), new object[] { value });
            }
            else
            {
                lbl_total_debit_amount.Text = string.Format("{0:c}", value);
            }
        }

        public decimal total_debt
        {
            set { update_total_debt(value); }
        }

        #endregion " total_debit "

        #region " total_credit "

        private void update_total_credit(decimal value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateDecimalDelegate(update_total_credit), new object[] { value });
            }
            else
            {
                lbl_total_credit_amount.Text = string.Format("{0:c}", value);
            }
        }

        public decimal total_credit
        {
            set { update_total_credit(value); }
        }

        #endregion " total_credit "

        #region " total_items "

        private void update_total_items(System.Int32 value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateIntegerDelegate(update_total_items), new object[] { value });
            }
            else
            {
                lbl_total_items.Text = string.Format("{0:n0}", value);
            }
        }

        public System.Int32 total_items
        {
            set { update_total_items(value); }
        }

        #endregion " total_items "

        #region " total_batches "

        private void update_total_batches(System.Int32 value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateIntegerDelegate(update_total_batches), new object[] { value });
            }
            else
            {
                lbl_total_batches.Text = string.Format("{0:n0}", value);
            }
        }

        public System.Int32 total_batches
        {
            set { update_total_batches(value); }
        }

        #endregion " total_batches "

        #region " total_billed "

        private void update_total_billed(decimal value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateDecimalDelegate(update_total_billed), new object[] { value });
            }
            else
            {
                lbl_billed_amounts.Text = string.Format("{0:c}", value);
            }
        }

        public decimal total_billed
        {
            set { update_total_billed(value); }
        }

        #endregion " total_billed "

        #region " total_gross "

        private void update_total_gross(decimal value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateDecimalDelegate(update_total_gross), new object[] { value });
            }
            else
            {
                lbl_gross_amounts.Text = string.Format("{0:c}", value);
            }
        }

        public decimal total_gross
        {
            set { update_total_gross(value); }
        }

        #endregion " total_gross "

        #region " total_net "

        private void update_total_net(decimal value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateDecimalDelegate(update_total_net), new object[] { value });
            }
            else
            {
                lbl_net_amounts.Text = string.Format("{0:c}", value);
            }
        }

        public decimal total_net
        {
            set { update_total_net(value); }
        }

        #endregion " total_net "

        #region " total_prenotes "

        private void update_total_prenotes(System.Int32 value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateIntegerDelegate(update_total_prenotes), new object[] { value });
            }
            else
            {
                lbl_prenote_count.Text = string.Format("{0:n0}", value);
            }
        }

        public System.Int32 total_prenotes
        {
            set { update_total_prenotes(value); }
        }

        #endregion " total_prenotes "

        #region " batch "

        private void update_batch(string value)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new UpdateStringDelegate(update_batch), new object[] { value });
            }
            else
            {
                lbl_batch_id.Text = value;
            }
        }

        public string batch
        {
            set { update_batch(value); }
        }

        #endregion " batch "

        private void form_RPS_Load(object sender, System.EventArgs e)
        {
            BackgroundWorker1.RunWorkerAsync();
        }

        private void BackgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            var _with1 = new GenerateRPSPayments();
            _with1.bank = ap.Bank;
            if (!_with1.Process_Payments(this))
            {
                DebtPlus.Data.Forms.MessageBox.Show("An error prevented the generation of the payment file. Please run the utility again.", "Sorry, an error occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BackgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Enable the buttons on the form
            var _with2 = Button_OK;
            _with2.Text = "&OK";
            _with2.Tag = "ok";
            _with2.Visible = true;

            Button_Print.Visible = true;
        }

        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            switch (Convert.ToString(((DevExpress.XtraEditors.SimpleButton)sender).Tag))
            {
                case "ok":
                    Close();
                    break;

                case "cancel":
                    BackgroundWorker1.CancelAsync();
                    Button_OK.Enabled = false;
                    break;

                case "print":
                    PrintForm();
                    break;
            }
        }

        private System.Drawing.Font printFont;

        private void PrintForm()
        {
            try
            {
                printFont = new Font("Arial", 10);
                System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
                pd.PrintPage += pd_PrintPage;

#if false // Direct to printer
				pd.Print();

#else // Use preview form first
                using (PrintPreviewDialog PreviewDialog = new PrintPreviewDialog())
                {
                    var _with3 = PreviewDialog;
                    _with3.ShowIcon = false;
                    _with3.Document = pd;
                    _with3.ShowDialog(this);
                }
#endif
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error printing document");
            }
        }

        /// <summary>
        /// Generate the printer document
        /// </summary>
        private void pd_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs ev)
        {
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            float yPos = topMargin;
            System.Drawing.StringFormat fmt = new System.Drawing.StringFormat();
            System.Int32 LineNo = 0;

            // Generate the font for the header
            System.Drawing.Font HdrFont = new System.Drawing.Font(printFont.FontFamily, 20, FontStyle.Bold, GraphicsUnit.Point, Convert.ToByte(0));
            fmt.Alignment = StringAlignment.Near;
            ev.Graphics.DrawString(Text, HdrFont, Brushes.Black, leftMargin + 0, yPos, fmt);
            yPos += (HdrFont.GetHeight(ev.Graphics) * 2);
            HdrFont.Dispose();

            // Print each line of the file.
            ev.HasMorePages = true;

            while (ev.HasMorePages)
            {
                string TitleString = string.Empty;
                string ValueString = string.Empty;

                switch (LineNo)
                {
                    case 0:
                        TitleString = "Output Text File";
                        ValueString = ap.OutputFileName;

                        // Try to measure the font width
                        fmt.Alignment = StringAlignment.Far;
                        float NormalHeight = printFont.GetHeight(ev.Graphics);
                        do
                        {
                            System.Drawing.SizeF WidthSize = ev.Graphics.MeasureString("..." + ValueString, printFont, new System.Drawing.SizeF(350, NormalHeight * 2), fmt);
                            if (WidthSize.Height != NormalHeight * 2)
                                break;
                            if (ValueString.Length <= 0)
                                break;
                            ValueString = ValueString.Substring(1);
                        } while (true);

                        if (ValueString != ap.OutputFileName)
                            ValueString = "..." + ValueString;
                        break;

                    case 1:
                        TitleString = "Date";
                        ValueString = string.Format("{0:MM/dd/yyyy HH:mm}", ap.CurrentTime);
                        break;

                    case 2:
                    case 3:
                    case 8:
                        TitleString = string.Empty;
                        ValueString = string.Empty;
                        break;

                    case 4:
                        TitleString = LabelControl_total_debit_amount.Text;
                        ValueString = lbl_total_debit_amount.Text;
                        break;

                    case 5:
                        TitleString = LabelControl_total_credit_amount.Text;
                        ValueString = lbl_total_credit_amount.Text;
                        break;

                    case 6:
                        TitleString = LabelControl_total_items.Text;
                        ValueString = lbl_total_items.Text;
                        break;

                    case 7:
                        TitleString = LabelControl_total_batches.Text;
                        ValueString = lbl_total_batches.Text;
                        break;

                    case 9:
                        TitleString = LabelControl_total_billed_amounts.Text;
                        ValueString = lbl_billed_amounts.Text;
                        break;

                    case 10:
                        TitleString = LabelControl_total_gross_amounts.Text;
                        ValueString = lbl_gross_amounts.Text;
                        break;

                    case 11:
                        TitleString = LabelControl_total_net_amounts.Text;
                        ValueString = lbl_net_amounts.Text;
                        break;

                    case 12:
                        TitleString = LabelControl_prenote_count.Text;
                        ValueString = lbl_prenote_count.Text;
                        ev.HasMorePages = false;
                        break;
                }

                // Generate the position on the form and print the line
                fmt.Alignment = StringAlignment.Near;
                ev.Graphics.DrawString(TitleString, printFont, Brushes.Black, leftMargin + 0, yPos, fmt);
                fmt.Alignment = StringAlignment.Far;
                ev.Graphics.DrawString(ValueString, printFont, Brushes.Black, leftMargin + 450, yPos, fmt);
                yPos += printFont.GetHeight(ev.Graphics);

                LineNo += 1;
            }
        }
    }
}