using System;

using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Payments
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        // The current time reported in the output files
        public System.DateTime CurrentTime = DateTime.Now;

        // Output file name
        public string OutputFileName = string.Empty;

        /// <summary>
        /// Bank to generate proposals
        /// </summary>
        private System.Int32 _bank = -1;

        public System.Int32 Bank
        {
            get { return _bank; }
        }

        /// <summary>
        /// Current output RPPS file identifier in the rpps_files table
        /// </summary>
        private System.Int32 _rpps_file = -1;

        public System.Int32 rpps_file
        {
            get { return _rpps_file; }
            set { _rpps_file = value; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {
            "b",
            "f"
        })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            switch (switchSymbol.ToLower())
            {
                case "b":
                    System.Int32 temp_b_Integer = 0;
                    if (Int32.TryParse(switchValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out temp_b_Integer))
                    {
                        if (temp_b_Integer > 0)
                        {
                            _bank = temp_b_Integer;
                            break;
                        }
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "f":
                    System.Int32 temp_f_Integer = 0;
                    if (Int32.TryParse(switchValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out temp_f_Integer))
                    {
                        if (temp_f_Integer > 0)
                        {
                            _rpps_file = temp_f_Integer;
                            break;
                        }
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Process the completion of the parse operation
        /// </summary>
        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            if (Bank < 0)
            {
                System.Collections.Generic.List<DebtPlus.LINQ.bank> colBanks = DebtPlus.LINQ.Cache.bank.getList().FindAll(s => s.type == "R" && s.ActiveFlag == true);
                if (colBanks.Count == 1)
                {
                    _bank = colBanks[0].Id;
                    return ss;
                }

                using (frmBank frm = new frmBank(colBanks))
                {
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        _bank = frm.bank;
                    }
                }

                if (Bank <= 0)
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }

            // If there is no name specified then ask for one.
            if (ss == global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError && OutputFileName == string.Empty)
            {
                var _with1 = new System.Windows.Forms.OpenFileDialog();
                _with1.AddExtension = true;
                _with1.CheckFileExists = false;
                _with1.CheckPathExists = true;
                _with1.DefaultExt = ".txt";
                _with1.FileName = "*.txt";
                _with1.Filter = "Text Files (*.txt)|*.txt|Data Files (*.dat)|*.dat|All Files (*.*)|*.*";
                _with1.FilterIndex = 0;
                _with1.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                _with1.RestoreDirectory = true;
                _with1.ShowReadOnly = false;

                if (_with1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    OutputFileName = _with1.FileName;
                }
                else
                {
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                }
                _with1.Dispose();
            }

            return ss;
        }
    }
}