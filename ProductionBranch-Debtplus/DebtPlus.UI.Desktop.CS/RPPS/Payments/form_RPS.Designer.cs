using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Payments
{
	partial class form_RPS : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl_total_debit_amount = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_total_credit_amount = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_total_items = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_total_batches = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_total_billed_amounts = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_total_gross_amounts = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_total_net_amounts = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_prenote_count = new DevExpress.XtraEditors.LabelControl();
			this.lbl_prenote_count = new DevExpress.XtraEditors.LabelControl();
			this.lbl_net_amounts = new DevExpress.XtraEditors.LabelControl();
			this.lbl_gross_amounts = new DevExpress.XtraEditors.LabelControl();
			this.lbl_billed_amounts = new DevExpress.XtraEditors.LabelControl();
			this.lbl_total_batches = new DevExpress.XtraEditors.LabelControl();
			this.lbl_total_items = new DevExpress.XtraEditors.LabelControl();
			this.lbl_total_credit_amount = new DevExpress.XtraEditors.LabelControl();
			this.lbl_total_debit_amount = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_batch_id = new DevExpress.XtraEditors.LabelControl();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
			this.BackgroundWorker1 = new System.ComponentModel.BackgroundWorker();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl_total_debit_amount
			//
			this.LabelControl_total_debit_amount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_total_debit_amount.Appearance.Options.UseFont = true;
			this.LabelControl_total_debit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total_debit_amount.Location = new System.Drawing.Point(12, 46);
			this.LabelControl_total_debit_amount.Name = "LabelControl_total_debit_amount";
			this.LabelControl_total_debit_amount.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_total_debit_amount.TabIndex = 0;
			this.LabelControl_total_debit_amount.Text = "Total Debit Amount:";
			this.LabelControl_total_debit_amount.UseMnemonic = false;
			//
			//LabelControl_total_credit_amount
			//
			this.LabelControl_total_credit_amount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_total_credit_amount.Appearance.Options.UseFont = true;
			this.LabelControl_total_credit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total_credit_amount.Location = new System.Drawing.Point(12, 65);
			this.LabelControl_total_credit_amount.Name = "LabelControl_total_credit_amount";
			this.LabelControl_total_credit_amount.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_total_credit_amount.TabIndex = 1;
			this.LabelControl_total_credit_amount.Text = "Total Credit Amount:";
			this.LabelControl_total_credit_amount.UseMnemonic = false;
			//
			//LabelControl_total_items
			//
			this.LabelControl_total_items.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_total_items.Appearance.Options.UseFont = true;
			this.LabelControl_total_items.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total_items.Location = new System.Drawing.Point(12, 84);
			this.LabelControl_total_items.Name = "LabelControl_total_items";
			this.LabelControl_total_items.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_total_items.TabIndex = 2;
			this.LabelControl_total_items.Text = "Total Items:";
			this.LabelControl_total_items.UseMnemonic = false;
			//
			//LabelControl_total_batches
			//
			this.LabelControl_total_batches.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_total_batches.Appearance.Options.UseFont = true;
			this.LabelControl_total_batches.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total_batches.Location = new System.Drawing.Point(12, 103);
			this.LabelControl_total_batches.Name = "LabelControl_total_batches";
			this.LabelControl_total_batches.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_total_batches.TabIndex = 3;
			this.LabelControl_total_batches.Text = "Total Batches:";
			this.LabelControl_total_batches.UseMnemonic = false;
			//
			//LabelControl_total_billed_amounts
			//
			this.LabelControl_total_billed_amounts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_total_billed_amounts.Appearance.Options.UseFont = true;
			this.LabelControl_total_billed_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total_billed_amounts.Location = new System.Drawing.Point(12, 143);
			this.LabelControl_total_billed_amounts.Name = "LabelControl_total_billed_amounts";
			this.LabelControl_total_billed_amounts.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_total_billed_amounts.TabIndex = 4;
			this.LabelControl_total_billed_amounts.Text = "Billed Amounts:";
			this.LabelControl_total_billed_amounts.UseMnemonic = false;
			//
			//LabelControl_total_gross_amounts
			//
			this.LabelControl_total_gross_amounts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_total_gross_amounts.Appearance.Options.UseFont = true;
			this.LabelControl_total_gross_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total_gross_amounts.Location = new System.Drawing.Point(12, 162);
			this.LabelControl_total_gross_amounts.Name = "LabelControl_total_gross_amounts";
			this.LabelControl_total_gross_amounts.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_total_gross_amounts.TabIndex = 5;
			this.LabelControl_total_gross_amounts.Text = "Gross Amounts:";
			this.LabelControl_total_gross_amounts.UseMnemonic = false;
			//
			//LabelControl_total_net_amounts
			//
			this.LabelControl_total_net_amounts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_total_net_amounts.Appearance.Options.UseFont = true;
			this.LabelControl_total_net_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_total_net_amounts.Location = new System.Drawing.Point(12, 181);
			this.LabelControl_total_net_amounts.Name = "LabelControl_total_net_amounts";
			this.LabelControl_total_net_amounts.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_total_net_amounts.TabIndex = 6;
			this.LabelControl_total_net_amounts.Text = "Net Amounts:";
			this.LabelControl_total_net_amounts.UseMnemonic = false;
			//
			//LabelControl_prenote_count
			//
			this.LabelControl_prenote_count.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.LabelControl_prenote_count.Appearance.Options.UseFont = true;
			this.LabelControl_prenote_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_prenote_count.Location = new System.Drawing.Point(12, 200);
			this.LabelControl_prenote_count.Name = "LabelControl_prenote_count";
			this.LabelControl_prenote_count.Size = new System.Drawing.Size(166, 13);
			this.LabelControl_prenote_count.TabIndex = 7;
			this.LabelControl_prenote_count.Text = "Prenote Count:";
			this.LabelControl_prenote_count.UseMnemonic = false;
			//
			//lbl_prenote_count
			//
			this.lbl_prenote_count.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_prenote_count.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_prenote_count.Appearance.Options.UseFont = true;
			this.lbl_prenote_count.Appearance.Options.UseTextOptions = true;
			this.lbl_prenote_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_prenote_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_prenote_count.Location = new System.Drawing.Point(264, 200);
			this.lbl_prenote_count.Name = "lbl_prenote_count";
			this.lbl_prenote_count.Size = new System.Drawing.Size(120, 13);
			this.lbl_prenote_count.TabIndex = 15;
			this.lbl_prenote_count.Text = "0";
			this.lbl_prenote_count.UseMnemonic = false;
			//
			//lbl_net_amounts
			//
			this.lbl_net_amounts.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_net_amounts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_net_amounts.Appearance.Options.UseFont = true;
			this.lbl_net_amounts.Appearance.Options.UseTextOptions = true;
			this.lbl_net_amounts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_net_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_net_amounts.Location = new System.Drawing.Point(264, 181);
			this.lbl_net_amounts.Name = "lbl_net_amounts";
			this.lbl_net_amounts.Size = new System.Drawing.Size(120, 13);
			this.lbl_net_amounts.TabIndex = 14;
			this.lbl_net_amounts.Text = "$0.00";
			this.lbl_net_amounts.UseMnemonic = false;
			//
			//lbl_gross_amounts
			//
			this.lbl_gross_amounts.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_gross_amounts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_gross_amounts.Appearance.Options.UseFont = true;
			this.lbl_gross_amounts.Appearance.Options.UseTextOptions = true;
			this.lbl_gross_amounts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_gross_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_gross_amounts.Location = new System.Drawing.Point(264, 162);
			this.lbl_gross_amounts.Name = "lbl_gross_amounts";
			this.lbl_gross_amounts.Size = new System.Drawing.Size(120, 13);
			this.lbl_gross_amounts.TabIndex = 13;
			this.lbl_gross_amounts.Text = "$0.00";
			this.lbl_gross_amounts.UseMnemonic = false;
			//
			//lbl_billed_amounts
			//
			this.lbl_billed_amounts.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_billed_amounts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_billed_amounts.Appearance.Options.UseFont = true;
			this.lbl_billed_amounts.Appearance.Options.UseTextOptions = true;
			this.lbl_billed_amounts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_billed_amounts.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_billed_amounts.Location = new System.Drawing.Point(264, 143);
			this.lbl_billed_amounts.Name = "lbl_billed_amounts";
			this.lbl_billed_amounts.Size = new System.Drawing.Size(120, 13);
			this.lbl_billed_amounts.TabIndex = 12;
			this.lbl_billed_amounts.Text = "$0.00";
			this.lbl_billed_amounts.UseMnemonic = false;
			//
			//lbl_total_batches
			//
			this.lbl_total_batches.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_total_batches.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_total_batches.Appearance.Options.UseFont = true;
			this.lbl_total_batches.Appearance.Options.UseTextOptions = true;
			this.lbl_total_batches.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_total_batches.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_total_batches.Location = new System.Drawing.Point(264, 103);
			this.lbl_total_batches.Name = "lbl_total_batches";
			this.lbl_total_batches.Size = new System.Drawing.Size(120, 13);
			this.lbl_total_batches.TabIndex = 11;
			this.lbl_total_batches.Text = "0";
			this.lbl_total_batches.UseMnemonic = false;
			//
			//lbl_total_items
			//
			this.lbl_total_items.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_total_items.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_total_items.Appearance.Options.UseFont = true;
			this.lbl_total_items.Appearance.Options.UseTextOptions = true;
			this.lbl_total_items.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_total_items.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_total_items.Location = new System.Drawing.Point(264, 84);
			this.lbl_total_items.Name = "lbl_total_items";
			this.lbl_total_items.Size = new System.Drawing.Size(120, 13);
			this.lbl_total_items.TabIndex = 10;
			this.lbl_total_items.Text = "0";
			this.lbl_total_items.UseMnemonic = false;
			//
			//lbl_total_credit_amount
			//
			this.lbl_total_credit_amount.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_total_credit_amount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_total_credit_amount.Appearance.Options.UseFont = true;
			this.lbl_total_credit_amount.Appearance.Options.UseTextOptions = true;
			this.lbl_total_credit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_total_credit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_total_credit_amount.Location = new System.Drawing.Point(264, 65);
			this.lbl_total_credit_amount.Name = "lbl_total_credit_amount";
			this.lbl_total_credit_amount.Size = new System.Drawing.Size(120, 13);
			this.lbl_total_credit_amount.TabIndex = 9;
			this.lbl_total_credit_amount.Text = "$0.00";
			this.lbl_total_credit_amount.UseMnemonic = false;
			//
			//lbl_total_debit_amount
			//
			this.lbl_total_debit_amount.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_total_debit_amount.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			this.lbl_total_debit_amount.Appearance.Options.UseFont = true;
			this.lbl_total_debit_amount.Appearance.Options.UseTextOptions = true;
			this.lbl_total_debit_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_total_debit_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_total_debit_amount.Location = new System.Drawing.Point(264, 46);
			this.lbl_total_debit_amount.Name = "lbl_total_debit_amount";
			this.lbl_total_debit_amount.Size = new System.Drawing.Size(120, 13);
			this.lbl_total_debit_amount.TabIndex = 8;
			this.lbl_total_debit_amount.Text = "$0.00";
			this.lbl_total_debit_amount.UseMnemonic = false;
			//
			//LabelControl9
			//
			this.LabelControl9.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 12f, System.Drawing.FontStyle.Bold);
			this.LabelControl9.Appearance.Options.UseFont = true;
			this.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl9.Location = new System.Drawing.Point(12, 12);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(225, 28);
			this.LabelControl9.TabIndex = 16;
			this.LabelControl9.Text = "Processing Batch Identifier:";
			this.LabelControl9.UseMnemonic = false;
			//
			//lbl_batch_id
			//
			this.lbl_batch_id.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_batch_id.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 12f, System.Drawing.FontStyle.Bold);
			this.lbl_batch_id.Appearance.Options.UseFont = true;
			this.lbl_batch_id.Appearance.Options.UseTextOptions = true;
			this.lbl_batch_id.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_batch_id.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_batch_id.Location = new System.Drawing.Point(243, 12);
			this.lbl_batch_id.Name = "lbl_batch_id";
			this.lbl_batch_id.Size = new System.Drawing.Size(141, 28);
			this.lbl_batch_id.TabIndex = 17;
			this.lbl_batch_id.Text = "000000000000000";
			this.lbl_batch_id.UseMnemonic = false;
			//
			//Button_OK
			//
			this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_OK.Location = new System.Drawing.Point(114, 231);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 18;
			this.Button_OK.Tag = "cancel";
			this.Button_OK.Text = "&Cancel";
			//
			//Button_Print
			//
			this.Button_Print.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Print.Location = new System.Drawing.Point(208, 231);
			this.Button_Print.Name = "Button_Print";
			this.Button_Print.Size = new System.Drawing.Size(75, 23);
			this.Button_Print.TabIndex = 19;
			this.Button_Print.Tag = "print";
			this.Button_Print.Text = "&Print...";
			this.Button_Print.Visible = false;
			//
			//BackgroundWorker1
			//
			this.BackgroundWorker1.WorkerSupportsCancellation = true;
			//
			//form_RPS
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(396, 266);
			this.Controls.Add(this.Button_Print);
			this.Controls.Add(this.Button_OK);
			this.Controls.Add(this.lbl_batch_id);
			this.Controls.Add(this.LabelControl9);
			this.Controls.Add(this.lbl_prenote_count);
			this.Controls.Add(this.lbl_net_amounts);
			this.Controls.Add(this.lbl_gross_amounts);
			this.Controls.Add(this.lbl_billed_amounts);
			this.Controls.Add(this.lbl_total_batches);
			this.Controls.Add(this.lbl_total_items);
			this.Controls.Add(this.lbl_total_credit_amount);
			this.Controls.Add(this.lbl_total_debit_amount);
			this.Controls.Add(this.LabelControl_prenote_count);
			this.Controls.Add(this.LabelControl_total_net_amounts);
			this.Controls.Add(this.LabelControl_total_gross_amounts);
			this.Controls.Add(this.LabelControl_total_billed_amounts);
			this.Controls.Add(this.LabelControl_total_batches);
			this.Controls.Add(this.LabelControl_total_items);
			this.Controls.Add(this.LabelControl_total_credit_amount);
			this.Controls.Add(this.LabelControl_total_debit_amount);
			this.Name = "form_RPS";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Generate RPS File for MasterCard";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total_debit_amount;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total_credit_amount;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total_items;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total_batches;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total_billed_amounts;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total_gross_amounts;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_total_net_amounts;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_prenote_count;
		protected internal DevExpress.XtraEditors.LabelControl lbl_prenote_count;
		protected internal DevExpress.XtraEditors.LabelControl lbl_net_amounts;
		protected internal DevExpress.XtraEditors.LabelControl lbl_gross_amounts;
		protected internal DevExpress.XtraEditors.LabelControl lbl_billed_amounts;
		protected internal DevExpress.XtraEditors.LabelControl lbl_total_batches;
		protected internal DevExpress.XtraEditors.LabelControl lbl_total_items;
		protected internal DevExpress.XtraEditors.LabelControl lbl_total_credit_amount;
		protected internal DevExpress.XtraEditors.LabelControl lbl_total_debit_amount;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl9;
		protected internal DevExpress.XtraEditors.LabelControl lbl_batch_id;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Print;
		protected internal System.ComponentModel.BackgroundWorker BackgroundWorker1;
	}
}
