using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.RPPS.Payments
{
	partial class frmBank : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;
		public System.Windows.Forms.ToolTip ToolTip1;
		public DevExpress.XtraEditors.SimpleButton Button_Cancel;
		public DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit1;
		public DevExpress.XtraEditors.LabelControl Label1;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.ToolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.Label1 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//DefaultLookAndFeel1
			//
			this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Button_Cancel.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Button_Cancel.Appearance.Options.UseFont = true;
			this.Button_Cancel.Appearance.Options.UseForeColor = true;
			this.Button_Cancel.Cursor = System.Windows.Forms.Cursors.Default;
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(199, 112);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Button_Cancel.Size = new System.Drawing.Size(73, 25);
			this.Button_Cancel.TabIndex = 3;
			this.Button_Cancel.Text = "&Cancel";
			//
			//Button_OK
			//
			this.Button_OK.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Button_OK.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Button_OK.Appearance.Options.UseFont = true;
			this.Button_OK.Appearance.Options.UseForeColor = true;
			this.Button_OK.Cursor = System.Windows.Forms.Cursors.Default;
			this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Button_OK.Location = new System.Drawing.Point(104, 112);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.Button_OK.Size = new System.Drawing.Size(73, 25);
			this.Button_OK.TabIndex = 2;
			this.Button_OK.Text = "&OK";
			//
			//Label1
			//
			this.Label1.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Label1.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Label1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.Label1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.Label1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label1.Cursor = System.Windows.Forms.Cursors.Default;
			this.Label1.Location = new System.Drawing.Point(16, 12);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(345, 54);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "Please choose the appropriate RPPS bank account for the transactions. Each RPPS b" + "ank account holds the originator ID and various other parameters that are needed" + " to properly build the RPPS file.";
			this.Label1.UseMnemonic = false;
			//
			//LookUpEdit1
			//
			this.LookUpEdit1.Location = new System.Drawing.Point(18, 72);
			this.LookUpEdit1.Name = "LookUpEdit1";
			this.LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)
			});
			this.LookUpEdit1.Properties.DisplayMember = "description";
			this.LookUpEdit1.Properties.NullText = "";
			this.LookUpEdit1.Properties.ShowFooter = false;
			this.LookUpEdit1.Properties.ShowHeader = false;
			this.LookUpEdit1.Properties.ValueMember = "Id";
			this.LookUpEdit1.Size = new System.Drawing.Size(345, 20);
			this.LookUpEdit1.TabIndex = 4;
			//
			//frmBank
			//
			this.AcceptButton = this.Button_OK;
			this.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Appearance.Options.UseFont = true;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(375, 151);
			this.Controls.Add(this.LookUpEdit1);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.Controls.Add(this.Label1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Location = new System.Drawing.Point(4, 30);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmBank";
			this.Text = "Bank Account Selection";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();
			this.ResumeLayout(false);
		}
	}
}
