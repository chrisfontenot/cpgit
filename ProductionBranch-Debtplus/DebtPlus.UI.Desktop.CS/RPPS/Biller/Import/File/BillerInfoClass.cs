#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class BillerInfoClass
    {
        public RPPS_Input input_class;

        public System.Collections.Generic.List<LineOfBusinessClass> col_lob = new System.Collections.Generic.List<LineOfBusinessClass>();
        public object Key = System.DBNull.Value;
        public string livedate = string.Empty;
        public string effdate = string.Empty;
        public string trnaba = string.Empty;
        public string privatebillername = string.Empty;
        public string ClassName = string.Empty;
        public string billertype = string.Empty;
        public string prenotes = string.Empty;
        public string guarpayonly = string.Empty;
        public string dmpprenote = string.Empty;
        public string dmppayonly = string.Empty;
        public string blroldname = string.Empty;
        public string pvtblr = string.Empty;

        public string blrnote = string.Empty;
        public string acceptFBD = "N";
        public string acceptFBC = "N";
        public string acceptCDN = "N";
        public string acceptCDF = "N";
        public string acceptExceptionPay = "N";
        public string returnCDR = "N";
        public string returnCDT = "N";
        public string returnCDA = "N";
        public string returnCDV = "N";
        public string returnCDC = "N";
        public string returnCDM = "N";
        public string ReqAdndaRev = "N";
        public string CheckDigit = "N";
        public string AvgResponseTime = string.Empty;
        public string Country = string.Empty;
        public string State = string.Empty;
        public string TerritoryCode = string.Empty;
        public string FileFormat = string.Empty;
        private string privateblrid = string.Empty;
        private string privateacceptCDD = "N";
        private string privateacceptCDV = "N";
        private string privateacceptCDP = "N";

        private string privateCurrency = "840";

        public string Currency
        {
            get { return privateCurrency; }
            set { privateCurrency = value; }
        }

        public string acceptCDP
        {
            get { return privateacceptCDP; }
            set { privateacceptCDP = value; }
        }

        public string acceptCDV
        {
            get { return privateacceptCDV; }
            set { privateacceptCDV = value; }
        }

        public string acceptCDD
        {
            get { return privateacceptCDD; }
            set { privateacceptCDD = value; }
        }

        public string blrid
        {
            get { return privateblrid; }
            set
            {
                privateblrid = value;
                input_class.Set_BillerID(privateblrid);
            }
        }

        public string billername
        {
            get { return privatebillername; }
            set
            {
                privatebillername = value;
                input_class.Set_BillerName(value);
            }
        }

        public void Parse(RPPS_XML xmlInput)
        {
            var _with1 = xmlInput;

            // Start with the content of the billerinfo area
            xmlInput.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with1.BadFile && _with1.XMLReader.Read())
            {
                string ElementName = _with1.XMLReader.Name.ToLower();

                switch (_with1.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == "billerinfo")
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "fileformat":
                                xmlInput.parse_Field(FileFormat);
                                break;

                            case "country":
                                xmlInput.parse_Field(Country);
                                break;

                            case "state":
                                xmlInput.parse_Field(State);
                                break;

                            case "territorycode":
                                xmlInput.parse_Field(TerritoryCode);
                                break;

                            case "effdate":
                                xmlInput.parse_Field(effdate);
                                break;

                            case "livedate":
                                xmlInput.parse_Field(livedate);
                                break;

                            case "lineofbusiness":
                                LineOfBusinessClass nxt = new LineOfBusinessClass();
                                var _with2 = nxt;
                                xmlInput.parse_Field(nxt.LOB);
                                col_lob.Add(nxt);
                                break;

                            case "trnaba":
                                xmlInput.parse_Field(trnaba);
                                break;

                            case "billerid":
                                xmlInput.parse_Field(blrid);
                                break;

                            case "billername":
                                xmlInput.parse_Field(billername);
                                break;

                            case "billerclass":
                                xmlInput.parse_Field(ClassName);
                                break;

                            case "billertype":
                                xmlInput.parse_Field(billertype);
                                break;

                            case "blroldname":
                            case "prevname":
                                xmlInput.parse_Field(blroldname);
                                break;

                            case "pvtblr":
                                xmlInput.parse_Field(pvtblr);
                                break;

                            case "blrnote":
                            case "billernote":
                                xmlInput.parse_Field(blrnote);
                                break;

                            case "currency":
                                xmlInput.parse_Field(Currency);
                                break;

                            case "prenotes":
                                xmlInput.parse_Field(prenotes);
                                break;

                            case "guarpayonly":
                                xmlInput.parse_Field(guarpayonly);
                                break;

                            case "dmpprenote":
                                xmlInput.parse_Field(dmpprenote);
                                break;

                            case "dmppayonly":
                                xmlInput.parse_Field(dmppayonly);
                                break;

                            case "acceptcdp":
                                xmlInput.parse_Field(acceptCDP);
                                break;

                            case "acceptcdv":
                                xmlInput.parse_Field(acceptCDV);
                                break;

                            case "acceptcdd":
                                xmlInput.parse_Field(acceptCDD);
                                break;

                            case "acceptfbd":
                                xmlInput.parse_Field(acceptFBD);
                                break;

                            case "acceptfbc":
                                xmlInput.parse_Field(acceptFBC);
                                break;

                            case "acceptcdn":
                                xmlInput.parse_Field(acceptCDN);
                                break;

                            case "acceptcdf":
                                xmlInput.parse_Field(acceptCDF);
                                break;

                            case "acceptexceptionpay":
                                xmlInput.parse_Field(acceptExceptionPay);
                                break;

                            case "returncdr":
                                xmlInput.parse_Field(returnCDR);
                                break;

                            case "returncdt":
                                xmlInput.parse_Field(returnCDT);
                                break;

                            case "returncda":
                                xmlInput.parse_Field(returnCDA);
                                break;

                            case "returncdv":
                                xmlInput.parse_Field(returnCDV);
                                break;

                            case "returncdc":
                                xmlInput.parse_Field(returnCDC);
                                break;

                            case "returncdm":
                                xmlInput.parse_Field(returnCDM);
                                break;

                            case "reqadndarev":
                                xmlInput.parse_Field(ReqAdndaRev);
                                break;

                            case "checkdigit":
                                xmlInput.parse_Field(CheckDigit);
                                break;

                            case "avgresponsetime":
                                xmlInput.parse_Field(AvgResponseTime);
                                break;

                            default:
                                xmlInput.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        private System.Int32 billertype_value()
        {
            System.Int32 answer = modConstants.BILLER_TYPE_OTHER;

            switch (billertype.ToUpper())
            {
                case "GROSS":
                    answer = modConstants.BILLER_TYPE_GROSS;
                    break;

                case "MODIFIED GROSS":
                    answer = modConstants.BILLER_TYPE_MODIFIED_GROSS;
                    break;

                case "NET":
                    answer = modConstants.BILLER_TYPE_NET;
                    break;
            }

            return answer;
        }

        public static System.Int32 bool_value(string input_string)
        {
            // Look for specific strings
            switch (input_string.ToLower())
            {
                case "y":
                case "yes":
                case "1":
                case "t":
                case "true":
                case "+":
                    return 1;

                case "n":
                case "no":
                case "0":
                case "f":
                case "false":
                case "-":
                    return 0;

                default:
                    return 0;
            }
        }

        internal void WriteInfo(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, BillerClass biller)
        {
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with3 = cmd;
                _with3.Connection = cn;
                _with3.Transaction = txn;
                _with3.CommandText = "UPDATE rpps_biller_ids SET [reqAdndaRev]=@reqAdndaRev,[returns_cdr]=@returns_cdr,[returns_cdt]=@returns_cdt,[returns_cda]=@returns_cda,[returns_cdv]=@returns_cdv,[returns_cdc]=@returns_cdc,[currency]=@currency,[effdate]=@effdate,[livedate]=@livedate,[Mastercard_key]=@Mastercard_key,[biller_name]=@biller_name,[biller_aba]=@biller_aba,[biller_class]=@biller_class,[biller_type]=@biller_type,[payment_prenote]=@payment_prenote,[guaranteed_funds]=@guaranteed_funds,[proposal_prenote]=@proposal_prenote,[dmppayonly]=@dmppayonly,[blroldname]=@blroldname,[pvt_biller_id]=@pvt_biller_id,[note]=@note,[send_cdp]=@send_cdp,[send_fbc]=@send_fbc,[send_fbd]=@send_fbd,[send_cdv]=@send_cdv,[send_cdd]=@send_cdd,[send_cdn]=@send_cdn,[send_cdf]=@send_cdf,[checkdigit]=@checkdigit WHERE [rpps_biller_id]=@rpps_biller_id";
                _with3.CommandType = CommandType.Text;

                _with3.Parameters.Add("@reqAdndaRev", SqlDbType.Bit).Value = bool_value(ReqAdndaRev);
                _with3.Parameters.Add("@returns_cdr", SqlDbType.Bit).Value = bool_value(returnCDR);
                _with3.Parameters.Add("@returns_cdt", SqlDbType.Bit).Value = bool_value(returnCDT);
                _with3.Parameters.Add("@returns_cda", SqlDbType.Bit).Value = bool_value(returnCDA);
                _with3.Parameters.Add("@returns_cdv", SqlDbType.Bit).Value = bool_value(returnCDV);
                _with3.Parameters.Add("@returns_cdc", SqlDbType.Bit).Value = bool_value(returnCDC);
                _with3.Parameters.Add("@currency", SqlDbType.VarChar, 50).Value = Currency;
                _with3.Parameters.Add("@effdate", SqlDbType.DateTime).Value = TypeUtilities.Date_Value(effdate);
                _with3.Parameters.Add("@livedate", SqlDbType.DateTime).Value = TypeUtilities.Date_Value(livedate);
                _with3.Parameters.Add("@Mastercard_key", SqlDbType.VarChar, 50).Value = Key;
                _with3.Parameters.Add("@biller_aba", SqlDbType.VarChar, 80).Value = trnaba;
                _with3.Parameters.Add("@biller_name", SqlDbType.VarChar, 80).Value = billername.PadLeft(16).Substring(0, 16).Trim();
                _with3.Parameters.Add("@biller_class", SqlDbType.VarChar, 80).Value = ClassName;
                _with3.Parameters.Add("@biller_type", SqlDbType.Int).Value = billertype_value();
                _with3.Parameters.Add("@payment_prenote", SqlDbType.Bit).Value = bool_value(prenotes);
                _with3.Parameters.Add("@guaranteed_funds", SqlDbType.Bit).Value = bool_value(guarpayonly);
                _with3.Parameters.Add("@proposal_prenote", SqlDbType.Bit).Value = bool_value(dmpprenote);
                _with3.Parameters.Add("@dmppayonly", SqlDbType.Bit).Value = bool_value(dmppayonly);
                _with3.Parameters.Add("@blroldname", SqlDbType.VarChar, 80).Value = blroldname;
                _with3.Parameters.Add("@pvt_biller_id", SqlDbType.Bit).Value = bool_value(pvtblr);
                _with3.Parameters.Add("@note", SqlDbType.VarChar, 80).Value = blrnote;
                _with3.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller.billerinfo.blrid;
                _with3.Parameters.Add("@send_cdp", SqlDbType.Bit).Value = bool_value(acceptCDP);
                _with3.Parameters.Add("@send_fbc", SqlDbType.Bit).Value = bool_value(acceptFBC);
                _with3.Parameters.Add("@send_fbd", SqlDbType.Bit).Value = bool_value(acceptFBD);
                _with3.Parameters.Add("@send_cdd", SqlDbType.Bit).Value = bool_value(acceptCDD);
                _with3.Parameters.Add("@send_cdv", SqlDbType.Bit).Value = bool_value(acceptCDV);
                _with3.Parameters.Add("@send_cdn", SqlDbType.Bit).Value = bool_value(acceptCDN);
                _with3.Parameters.Add("@send_cdf", SqlDbType.Bit).Value = bool_value(acceptCDF);
                _with3.Parameters.Add("@checkdigit", SqlDbType.Bit).Value = bool_value(CheckDigit);

                _with3.ExecuteNonQuery();
            }

            // Create the LOB items
            IEnumerator ie = col_lob.GetEnumerator();
            while (ie.MoveNext())
            {
                LineOfBusinessClass lob_class = ie.Current as LineOfBusinessClass;
                lob_class.WriteInfo(cn, txn, ref biller);
            }
        }
    }
}