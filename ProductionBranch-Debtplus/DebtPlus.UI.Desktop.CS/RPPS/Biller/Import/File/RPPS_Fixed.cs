#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class RPPS_Fixed : RPPS_Input
    {
        public RPPS_Fixed() : base()
        {
        }

        private System.Int32 strCol;

        public RPPS_Fixed(StatusForm sts) : base(sts)
        {
        }

        private string Field(System.Int32 size)
        {
            string answer = LineString.Substring(strCol - 1, size).Trim();

            // Debug.Print "'" + answer + "'"
            strCol = strCol + size;
            return answer;
        }

        private bool ParseBiller()
        {
            bool functionReturnValue = false;
            System.Int32 count_addresses = 0;
            System.Int32 count_masks = 0;
            System.Int32 index = 0;

            string strField = null;

            BillerClass biller = default(BillerClass);
            //UPGRADE_NOTE: Object biller may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
            biller = null;

            // Assume that the biller is proper.
            functionReturnValue = true;
            strCol = 1;

            // If this record is simply "there are no changes" then just terminate processing
            strField = Field(2);
            if (strField == "X0")
            {
                functionReturnValue = false;
                return functionReturnValue;
                // Simulate EOF
            }

            string strLob = null;
            LineOfBusinessClass cls_lob = default(LineOfBusinessClass);
            string strAKA = null;
            AkaClass cls_aka = default(AkaClass);
            string Filler = null;
            string addr1 = null;
            string addr2 = null;
            string city = null;
            string state = null;
            string zipcode = null;
            string country = null;
            AddressClass addr_class = default(AddressClass);
            string strLen = null;
            string strMask = null;
            string strDesc = null;
            MaskClass mask_class = default(MaskClass);

            do
            {
                switch (strField.Substring(1, 1))
                {
                    case "0":
                        biller = new BillerClass();
                        biller.input_class = this;
                        biller.acdind = strField.Substring(0, 1);
                        biller.billerinfo.blrid = Field(10);
                        biller.billerinfo.effdate = Field(8);
                        biller.billerinfo.trnaba = Field(10);
                        biller.billerinfo.billername = Field(128);
                        biller.billerinfo.ClassName = Field(50);
                        biller.billerinfo.billertype = Field(50);

                        // Line of business are the next group of fields
                        for (index = 1; index <= 10; index++)
                        {
                            strLob = Field(3);
                            if (strLob != string.Empty)
                            {
                                cls_lob = new LineOfBusinessClass();
                                cls_lob.LOB = strLob;
                                biller.billerinfo.col_lob.Add(cls_lob);
                            }
                        }

                        // Parse the other "flags"
                        biller.billerinfo.prenotes = Field(1);
                        biller.billerinfo.guarpayonly = Field(1);
                        biller.billerinfo.dmpprenote = Field(1);
                        biller.billerinfo.guarpayonly = Field(1);
                        biller.billerinfo.pvtblr = Field(1);
                        biller.billerinfo.blroldname = Field(128);
                        biller.billerinfo.blrnote = Field(1000);

                        // Parse the AKA list
                        for (index = 1; index <= 8; index++)
                        {
                            strAKA = Field(128);
                            if (strAKA != string.Empty)
                            {
                                cls_aka = new AkaClass();
                                cls_aka.Name = strAKA;
                                biller.col_akas.Add(cls_aka);
                            }
                        }

                        // The count of items follows this field
                        if (!Int32.TryParse(Field(6), out count_addresses))
                        {
                            count_addresses = 0;
                        }

                        if (!Int32.TryParse(Field(7), out count_masks))
                        {
                            count_masks = 0;
                        }
                        break;

                    case "1":
                        strCol = strCol + 10;
                        // Ignore the biller ID
                        if ((biller != null))
                        {
                            while (strCol < (LineString.Length - 20))
                            {
                                // Obtain the record for the current address information
                                Filler = Field(50);
                                addr1 = Field(50);
                                addr2 = Field(50);
                                city = Field(50);
                                state = Field(6);
                                zipcode = Field(12);
                                country = Field(3);

                                // A "valid" address has a line for the address, city, and state. Others are "optional".
                                if (addr1 != string.Empty && city != string.Empty & state != string.Empty)
                                {
                                    addr_class = new AddressClass();
                                    addr_class.addr1 = addr1;
                                    addr_class.addr2 = addr2;
                                    addr_class.city = city;
                                    addr_class.state = state;
                                    addr_class.zipcode = zipcode;
                                    addr_class.country = country;
                                    biller.col_addresses.Add(addr_class);
                                    count_addresses = count_addresses - 1;
                                }
                            }
                        }
                        break;

                    case "2":
                        strCol = strCol + 10;
                        // Ignore the biller ID
                        if ((biller != null))
                        {
                            while (strCol < LineString.Length - 72)
                            {
                                strLen = Field(2);
                                strMask = Field(22);
                                strDesc = Field(50);

                                if (strMask != string.Empty && strLen != string.Empty)
                                {
                                    mask_class = new MaskClass();
                                    mask_class.MaskInfo.mask = strMask + string.Empty;
                                    mask_class.MaskInfo.masklen = strLen + string.Empty;
                                    mask_class.MaskInfo.maskdesc = strDesc + string.Empty;
                                    biller.col_masks.Add(mask_class);
                                    count_masks = count_masks - 1;
                                }
                            }
                        }
                        break;
                }

                // Read the next record
                if (!NextLine())
                {
                    functionReturnValue = false;
                    break;
                }

                // Split the record into the fields
                strCol = 1;
                strField = LineString.Substring(2, 10);

                // If this is for a different biller then the current one is complete.
                if ((biller != null))
                {
                    if (strField != biller.billerinfo.blrid)
                        break;
                }
                strField = Field(2);
            } while (true);

            // If we have reached this point then add the biller to the list. All of the additional data should be present.
            if ((biller != null) && (count_masks == 0) && (count_addresses == 0))
            {
                biller.WriteInformation();
            }
            return functionReturnValue;
        }

        public override bool ParseFile()
        {
            // Read the first line of the input
            if (!NextLine())
            {
                return false;
            }

            while (ParseBiller()) ;
            return true;
        }
    }
}