using System;

using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        public enum FileTypes
        {
            xml = 0,
            Fixed = 1,
            Tab = 2
        }

        /// <summary>
        /// Is this a full download?
        /// </summary>
        private DevExpress.Utils.DefaultBoolean privateFullDownload = DevExpress.Utils.DefaultBoolean.Default;

        public DevExpress.Utils.DefaultBoolean FullDownload
        {
            get { return privateFullDownload; }
            set { privateFullDownload = value; }
        }

        /// <summary>
        /// Type of the input file
        /// </summary>
        public FileTypes FileType
        {
            get
            {
                switch (System.IO.Path.GetExtension(FileName).ToLower())
                {
                    case ".xml":
                        return FileTypes.xml;

                    case ".fixed":
                        return FileTypes.Fixed;

                    case ".tab":
                        return FileTypes.Tab;

                    default:
                        return FileTypes.xml;
                }
            }
        }

        protected override SwitchStatus OnNonSwitch(string value)
        {
            privateFileName = value;
            return global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
        }

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            switch (switchSymbol)
            {
                case "p":
                    privateFullDownload = DevExpress.Utils.DefaultBoolean.False;
                    break;

                case "f":
                    privateFullDownload = DevExpress.Utils.DefaultBoolean.True;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }

            return ss;
        }

        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus answer = base.OnDoneParse();

            if (answer == global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError && FileName == string.Empty)
            {
                var _with1 = new System.Windows.Forms.OpenFileDialog();
                _with1.CheckFileExists = true;
                _with1.CheckPathExists = true;
                _with1.Filter = "Fixed Format (*.fixed)|*.fixed|Tab Format (*.tab)|*.tab|XML Format (*.xml)|*.xml|All Files (*.*) - XML format only|*.*";
                _with1.FilterIndex = 2;
                _with1.Multiselect = false;
                _with1.RestoreDirectory = true;
                _with1.FileName = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "*.xml");
                _with1.ValidateNames = true;
                if (_with1.ShowDialog() == DialogResult.OK)
                {
                    privateFileName = _with1.FileName;
                }
                _with1.Dispose();

                // If there is no filename then terminate
                if (FileName == string.Empty)
                    answer = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                return answer;
            }

            return answer;
        }

        /// <summary>
        /// Name of the input file
        /// </summary>
        private string privateFileName = string.Empty;

        public string FileName
        {
            get { return privateFileName; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {
            "f",
            "p"
        })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}