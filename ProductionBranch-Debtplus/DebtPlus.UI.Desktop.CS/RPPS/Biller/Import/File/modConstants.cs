#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal static class modConstants
    {
        // Set to "1" for a special version for the Citrix server
        public const System.Int32 CITRIX_SERVER = 1;

        // Information for the resource strings
        public const string RESOURCE_APP = "DebtPlus";

        // At this point the monthly amounts get a warning that it is too much for a month.
        public const decimal MAXIMUM_MONTHLY_AMOUNT = (90000.0M / 12.0M);

        // Appointment states
        // Appointment is yet to be kept
        public const string APPT_STATUS_PENDING = "P";

        // The appointment was made and kept
        public const string APPT_STATUS_KEPT = "K";

        // The appointment was cancelled without reschedule
        public const string APPT_STATUS_CANCELLED = "C";

        // The appointment was made and never kept
        public const string APPT_STATUS_MISSED = "M";

        // The appointment was rescheduled to a different time
        public const string APPT_STATUS_RESCHEDULED = "R";

        // Appointment Result types

        // Debt Management Program
        public const string APPT_RESULT_DMP = "DMP";

        // Financial Counseling Only (no program, just talk)
        public const string APPT_RESULT_FCO = "FCO";

        // Referred to Other Agency (sent client elsewhere -- outside of our agency)
        public const string APPT_RESULT_ROA = "ROA";

        // The result is pending to be assigned
        public const string APPT_RESULT_PND = "PND";

        // We referred the client to a workshop for further help
        public const string APPT_RESULT_WKS = "WKS";

        // Resrouce registery strings
        public const string RESOURCE_REGISTRY_DATABASE = "DebtPlus_SQL.CDatabase";

        public const string RESOURCE_REGISTRY_SQLFIELD = "DebtPlus_SQL.CSQLfield";
        public const string RESOURCE_REGISTRY_OPTIONS = "DebtPlus_SQL.COptions";
        public const string RESOURCE_REGISTRY_LETTERFIELD = "DebtPlus_Letters.CLetterField";
        public const string RESOURCE_REGISTRY_LETTERFIELDS = "DebtPlus_Letters.CLetterFields";
        public const string RESOURCE_REGISTRY_LETTER = "DebtPlus_Letters.CLetter";
        public const string RESOURCE_REGISTRY_RESOURCEREPORT = "DebtPlus_ResourceGuide.ResourceReport";
        public const string RESOURCE_REGISTRY_VOID = "DebtPlus_Void.CVoid";
        public const string RESOURCE_REGISTRY_SELLDEBT = "DebtPlus_SellDebt.SellDebt";
        public const string RESOURCE_REGISTRY_CLIENTUPDATE = "DebtPlus_Client.CClientUpdate";
        public const string RESOURCE_REGISTRY_CREDITORUPDATE = "DebtPlus_Creditor.CCreditorUpdate";
        public const string RESOURCE_REGISTRY_APPOINTMENTS = "DP_Appointment.CAppointments";
        public const string RESOURCE_REGISTRY_SOUNDS = "Sounds.CSounds";
        public const string RESOURCE_REGISTRY_CHECK = "DP_PrintChecks.CCheckWriter";
        public const string RESOURCE_REGISTRY_SALES_TOOL = "DP_SalesTool.Sales_Tool";
        public const string RESOURCE_REGISTRY_REPORTS = "DP_Reports.CReports";

        // Proposal status information

        // A proposal letter has not been sent
        public const System.Int32 PROPOSAL_STATUS_NONE = 0;

        // A proposal letter was sent and a reply is pending
        public const System.Int32 PROPOSAL_STATUS_PENDING = 1;

        // A proposal letter was accepted by default
        public const System.Int32 PROPOSAL_STATUS_DEFAULT = 2;

        // A proposal letter was accepted by a person
        public const System.Int32 PROPOSAL_STATUS_ACCEPTED = 3;

        // A proposal letter was rejected
        public const System.Int32 PROPOSAL_STATUS_REJECTED = 4;

        // Values for the "cleared" indiction in the registers_trust and registers_invoices tables
        // Print is pending
        public const string REGISTER_STATE_PRINT = "P";

        // Item has been printed and is now waiting to be cleared
        public const string REGISTER_STATE_PENDING = " ";

        // Item has been voided
        public const string REGISTER_STATE_VOID = "V";

        // Item has been chashed but no statement has been received
        public const string REGISTER_STATE_CASHED = "E";

        // Item has been cleared but not reconciled
        public const string REGISTER_STATE_MARKED = "C";

        // Item has been reconciled against a statement
        public const string REGISTER_STATE_RECONSILED = "R";

        // Notes are stored in the database with these types
        public const System.Int32 NOTE_TYPE_PERM = 1;

        public const System.Int32 NOTE_TYPE_TEMP = 2;
        public const System.Int32 NOTE_TYPE_SYSTEM = 3;
        public const System.Int32 NOTE_TYPE_ALERT = 4;
        public const System.Int32 NOTE_TYPE_DISBURSEMENT = 5;
        public const System.Int32 NOTE_TYPE_RESEARCH = 6;

        // These are the type markers in the contact_types
        public const string CONTACT_TYPE_FAIRSHARE = "I";

        public const string CONTACT_TYPE_CORRESPONDENCE = "L";
        public const string CONTACT_TYPE_TERMINATIONS = "T";
        public const string CONTACT_TYPE_PAYMENTS = "P";
        public const string CONTACT_TYPE_PROPOSALS = "R";

        // These are the type codes in the creditor address table
        public const string CREDITOR_ADDRESS_INVOICE = "I";

        public const string CREDITOR_ADDRESS_PAYMENT = "P";
        public const string CREDITOR_ADDRESS_CORRESPONDENCE = "C";
        public const string CREDITOR_ADDRESS_PROPOSALS = "L";

        // Type of disbursment required for the creditor
        public const System.Int32 CREDITOR_DISBURSEMENT_CHECK = 0;

        public const System.Int32 CREDITOR_DISBURSEMENT_RPS = 1;
        public const System.Int32 CREDITOR_DISBURSEMENT_EPAY = 2;

        // Types of billing information required by the item
        public const System.Int32 BILLER_TYPE_NET = 1;

        public const System.Int32 BILLER_TYPE_GROSS = 2;
        public const System.Int32 BILLER_TYPE_MODIFIED_GROSS = 3;
        public const System.Int32 BILLER_TYPE_OTHER = 4;

        // Format strings for the various types
        public const string DATE_FORMAT = "MM/dd/yy";

        // Visual Basic F5 key
        public const System.Int32 VB_F5 = 116;
    }
}