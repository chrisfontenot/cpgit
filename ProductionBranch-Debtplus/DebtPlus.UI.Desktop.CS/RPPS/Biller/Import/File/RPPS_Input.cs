using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal abstract class RPPS_Input
    {
        internal RPPS_Input() : base()
        {
        }

        private string pushback = string.Empty;
        private System.Int32 LineCol = 0;
        private System.Int32 MaxCol = 0;

        public string LineString = string.Empty;

        /// <summary>
        /// Creation of the input class
        /// </summary>
        protected StatusForm privatestatus_form = null;

        public RPPS_Input(StatusForm sts)
        {
            privatestatus_form = sts;
        }

        internal StatusForm status_form
        {
            get { return privatestatus_form; }
        }

        internal void Set_BillerID(string biller_id)
        {
            status_form.SetBillerID(biller_id);
        }

        internal void Set_BillerName(string biller_name)
        {
            status_form.SetBillerName(biller_name);
        }

        private System.IO.FileStream iStream = null;

        protected System.IO.StreamReader txtStream = null;

        internal virtual bool OpenFile()
        {
            bool answer = false;

            try
            {
                iStream = new System.IO.FileStream(status_form.ap.FileName, System.IO.FileMode.Open);
                txtStream = new System.IO.StreamReader(iStream);
                answer = true;
            }
            catch (System.IO.FileNotFoundException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (System.IO.PathTooLongException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return answer;
        }

        public bool NextLine()
        {
            // At the end of the file, return failure
            if (txtStream.EndOfStream)
            {
                return false;
            }

            // Otherwise, return success
            LineString = txtStream.ReadLine();
            return true;
        }

        public string NextChar()
        {
            string functionReturnValue = null;

            // If there is a pending "returned to input stream" character, return it.
            if (pushback.Length == 1)
            {
                functionReturnValue = pushback;
                pushback = string.Empty;
                return functionReturnValue;
            }

            // If there are more than one "returned to input stream" characters, return the next character
            if (pushback.Length > 0)
            {
                functionReturnValue = pushback.Substring(0, 1);
                pushback = pushback.Substring(1);
                return functionReturnValue;
            }

            // If the input is at EOF then return the magic marker
            do
            {
                if (LineCol < 0)
                {
                    if (!NextLine())
                    {
                        functionReturnValue = "EOF";
                        return functionReturnValue;
                    }
                    LineCol = 0;
                    MaxCol = LineString.Length;
                }

                // Retrieve the next input character from the input stream
                if (LineCol >= MaxCol)
                {
                    LineCol = -1;
                }
                else
                {
                    functionReturnValue = LineString.Substring(LineCol, 1);
                    LineCol = LineCol + 1;
                    return functionReturnValue;
                }
            } while (true);
        }

        public void PushBackString(string InputString)
        {
            // Do not attempt to return the EOF sequence to the input.
            if (InputString != "EOF")
            {
                pushback += InputString;
            }
        }

        public virtual void Complete()
        {
            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_rpps_download_cleanup";
                        cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error running the RPPS cleanup procedure");
            }
        }

        public void DeleteTables(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "DELETE rpps_masks";
                cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }

            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "DELETE rpps_addresses";
                cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }

            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "DELETE rpps_lobs";
                cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }

            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "DELETE rpps_akas";
                cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }

            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "DELETE rpps_biller_ids";
                cmd.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, 600);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
            }
        }

        public virtual bool ParseFile()
        {
            return false;
        }

        public bool BadFile = false;
    }
}