using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    public static class TypeUtilities
    {
        static internal System.DateTime Date_Value(string inputString)
        {
            // Try to find a match to the simple formats
            Match matchList = Regex.Match(inputString, "^(?<year>\\d\\d\\d\\d)-(?<month>\\d\\d)-(?<day>\\d\\d)$");
            if (!matchList.Success)
            {
                matchList = Regex.Match(inputString, "^(?<year>\\d\\d\\d\\d)(?<month>\\d\\d)(?<day>\\d\\d)$");
            }

            // If there is a match then try to find the dates
            if (matchList.Success)
            {
                try
                {
                    DateTime answer = new DateTime(Int32.Parse(matchList.Groups["year"].Value), Int32.Parse(matchList.Groups["month"].Value), Int32.Parse(matchList.Groups["day"].Value));
                    if (answer.Year >= 1900)
                    {
                        return answer;
                    }
                }
                catch
                {
                }
            }

            return new DateTime(1900, 1, 1);
        }

        static internal Int32 Int_Value(string inputString)
        {
            Int32 answer = 0;
            if (!Int32.TryParse(inputString, NumberStyles.Integer, NumberFormatInfo.InvariantInfo, out answer))
                answer = 0;
            return answer;
        }
    }
}