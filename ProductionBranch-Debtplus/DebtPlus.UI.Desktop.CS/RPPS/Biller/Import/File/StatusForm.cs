using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class StatusForm
    {
        private System.DateTime date_start;
        internal ArgParser ap = null;

        private System.ComponentModel.BackgroundWorker bt = new System.ComponentModel.BackgroundWorker();

        public StatusForm() : base()
        {
            InitializeComponent();
        }

        public StatusForm(ArgParser ap) : base()
        {
            InitializeComponent();

            this.ap = ap;
            SimpleButton_Cancel.Click += cmd_Cancel_Click;
            this.Load += StatusForm_Load;
            Timer1.Tick += Timer1_Tick;
            bt.DoWork += bt_Worker;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
        }

        private void cmd_Cancel_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            Close();
        }

        private RPPS_Input cls = null;

        private void StatusForm_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            // Determine the class
            switch (ap.FileType)
            {
                case ArgParser.FileTypes.Fixed:
                    cls = new RPPS_Fixed(this);
                    break;

                case ArgParser.FileTypes.Tab:
                    cls = new RPPS_Tab(this);
                    break;

                case ArgParser.FileTypes.xml:
                    cls = new RPPS_XML(this);
                    break;
            }

            // Attempt to open the input file. If failed then cancel the program.
            if (cls.OpenFile())
            {
                if (ap.FullDownload == DevExpress.Utils.DefaultBoolean.Default)
                {
                    ap.FullDownload = (DevExpress.Utils.DefaultBoolean)(DebtPlus.Data.Forms.MessageBox.Show("Was this a full download of the entire biller database?", "Clear the existing tables", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes ? DevExpress.Utils.DefaultBoolean.True : DevExpress.Utils.DefaultBoolean.False);
                }

                bt.RunWorkerAsync();
                date_start = DateTime.Now;
                Timer1.Interval = 1000;
                Timer1.Enabled = true;

                SimpleButton_Cancel.Enabled = true;
                SimpleButton_Cancel.Visible = true;
            }
        }

        public void StopTimer()
        {
            Timer1.Enabled = false;
            SimpleButton_Cancel.Enabled = false;
            SimpleButton_Cancel.Visible = false;
            this.Close();
        }

        private void Timer1_Tick(System.Object eventSender, System.EventArgs eventArgs)
        {
            System.Int32 nMin = 0;
            System.Int32 nHr = 0;
            TimeSpan lElapsed = DateTime.Now.Subtract(date_start);
            System.Int32 nSec = System.Convert.ToInt32(lElapsed.TotalSeconds);

            nHr = nSec / 3600;
            nSec = nSec % 3600;

            nMin = nSec / 60;
            nSec = nSec % 60;

            lblElapsedTime.Text = string.Format("{0:00}:{1:00}:{2:00}", nHr, nMin, nSec);
        }

        public void bt_Worker(object Sender, System.EventArgs e)
        {
            // Determine if this is a full download. If so indicated then clear the tables
            if (ap.FullDownload == DevExpress.Utils.DefaultBoolean.True)
            {
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                System.Data.SqlClient.SqlTransaction txn = null;

                try
                {
                    cn.Open();
                    txn = cn.BeginTransaction();
                    cls.DeleteTables(cn, txn);
                    txn.Commit();
                    txn = null;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error deleting database tables");
                }
                finally
                {
                    if (txn != null)
                        txn.Commit();
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                }
            }

            // Do the parse of the input file
            cls.ParseFile();
            if (!cls.BadFile)
                cls.Complete();
        }

        internal delegate void BillerDelegate(string InputString);

        internal void SetBillerID(string BillerID)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new BillerDelegate(SetBillerID), new object[] { BillerID });
            }
            else
            {
                lblBillerID.Text = BillerID;
            }
        }

        internal void SetBillerName(string BillerName)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new BillerDelegate(SetBillerName), new object[] { BillerName });
            }
            else
            {
                lblBillerName.Text = BillerName;
            }
        }

        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Timer1.Enabled = false;
            SimpleButton_Cancel.Text = "OK";

            lblBillerID.Text = "Load Completed";

            LabelControl2.Text = string.Empty;
            LabelControl3.Text = string.Empty;

            lblBillerName.Text = Environment.NewLine + "Press OK to terminate";
        }
    }
}