using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Xml;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class AkaClass
    {
        private object _key = DBNull.Value;
        private string _privatename = string.Empty;

        private string _privateeffdate = string.Empty;

        public string Name
        {
            get { return _privatename; }
            set
            {
                _privatename = value.Trim();
                if (_privatename.Length > 80)
                    _privatename = _privatename.Substring(0, 80);
            }
        }

        public string Effdate
        {
            get { return _privateeffdate; }
            set { _privateeffdate = value; }
        }

        internal void Parse(RPPS_XML xmlInput)
        {
            string sectionName = xmlInput.XMLReader.Name.ToLower();

            // Find the key to the biller if there is one.
            if (xmlInput.XMLReader.HasAttributes)
            {
                for (Int32 attributeID = 0; attributeID <= xmlInput.XMLReader.AttributeCount - 1; attributeID++)
                {
                    xmlInput.XMLReader.MoveToAttribute(attributeID);
                    switch (xmlInput.XMLReader.Name.ToLower())
                    {
                        case "key":
                            xmlInput.XMLReader.ReadAttributeValue();
                            _key = xmlInput.XMLReader.Value;
                            break;
                    }
                }
            }

            xmlInput.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!xmlInput.BadFile && xmlInput.XMLReader.Read())
            {
                string elementName = xmlInput.XMLReader.Name.ToLower();

                switch (xmlInput.XMLReader.NodeType)
                {
                    case XmlNodeType.EndElement:
                        if (elementName == sectionName)
                        {
                            return;
                        }
                        break;

                    case XmlNodeType.Element:
                        switch (elementName)
                        {
                            case "name":
                                string tempName = null;
                                xmlInput.parse_Field(tempName);
                                Name = tempName;
                                break;

                            case "effdate":
                                string tempEffdate = null;
                                xmlInput.parse_Field(tempEffdate);
                                Effdate = tempEffdate;
                                break;

                            default:
                                xmlInput.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        public void WriteInfo(SqlConnection cn, SqlTransaction txn, BillerClass billerClass)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                var _with2 = cmd;
                _with2.Connection = cn;
                _with2.Transaction = txn;
                _with2.CommandText = "INSERT INTO rpps_akas(Mastercard_key,effdate,rpps_biller_id,name) VALUES (@Mastercard_key,@effdate,@rpps_biller_id,@name)";
                _with2.CommandType = CommandType.Text;
                _with2.Parameters.Add("@Mastercard_key", SqlDbType.VarChar, 50).Value = _key;
                _with2.Parameters.Add("@EffDate", SqlDbType.DateTime).Value = TypeUtilities.Date_Value(Effdate);
                _with2.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = billerClass.billerinfo.blrid;
                _with2.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = Name;
                _with2.ExecuteNonQuery();
            }
        }
    }
}