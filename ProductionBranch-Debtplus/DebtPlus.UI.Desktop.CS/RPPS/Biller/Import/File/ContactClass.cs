using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class PhoneClass
    {
        public object Key = System.DBNull.Value;
        public string PhoneType = string.Empty;
        public string PhoneNum = string.Empty;

        public string effdate = string.Empty;

        public void Parse(RPPS_XML XmlInput)
        {
            var _with1 = XmlInput;
            string SectionName = _with1.XMLReader.Name.ToLower();

            // Find the key to the biller if there is one.
            if (_with1.XMLReader.HasAttributes)
            {
                for (System.Int32 AttributeID = 0; AttributeID <= XmlInput.XMLReader.AttributeCount - 1; AttributeID++)
                {
                    XmlInput.XMLReader.MoveToAttribute(AttributeID);
                    switch (XmlInput.XMLReader.Name.ToLower())
                    {
                        case "key":
                            XmlInput.XMLReader.ReadAttributeValue();
                            Key = XmlInput.XMLReader.Value;
                            break;
                    }
                }
            }

            _with1.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with1.BadFile && _with1.XMLReader.Read())
            {
                string ElementName = _with1.XMLReader.Name.ToLower();

                switch (_with1.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "phonetype":
                                XmlInput.parse_Field(PhoneType);
                                break;

                            case "phonenum":
                                XmlInput.parse_Field(PhoneNum);
                                break;

                            case "effdate":
                                XmlInput.parse_Field(effdate);
                                break;

                            default:
                                _with1.SkipField();
                                break;
                        }
                        break;
                }
            }
        }
    }

    internal partial class ContactInfoClass
    {
        public object Key = System.DBNull.Value;
        public string effdate = string.Empty;
        public string ContactInfoType = string.Empty;
        public string Organization = string.Empty;
        public string FirstName = string.Empty;
        public string LastName = string.Empty;
        public string Address1 = string.Empty;
        public string Address2 = string.Empty;
        public string City = string.Empty;
        public string State = string.Empty;
        public string Country = string.Empty;
        public string PostalCode = string.Empty;
        public string Email = string.Empty;

        public string Title = string.Empty;

        public void Parse(ref RPPS_XML XmlInput)
        {
            var _with2 = XmlInput;
            string SectionName = _with2.XMLReader.Name.ToLower();

            // Find the key to the biller if there is one.
            if (_with2.XMLReader.HasAttributes)
            {
                for (System.Int32 AttributeID = 0; AttributeID <= XmlInput.XMLReader.AttributeCount - 1; AttributeID++)
                {
                    XmlInput.XMLReader.MoveToAttribute(AttributeID);
                    switch (XmlInput.XMLReader.Name.ToLower())
                    {
                        case "key":
                            XmlInput.XMLReader.ReadAttributeValue();
                            Key = XmlInput.XMLReader.Value;
                            break;
                    }
                }
            }

            _with2.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with2.BadFile && _with2.XMLReader.Read())
            {
                string ElementName = _with2.XMLReader.Name.ToLower();

                switch (_with2.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "effdate":
                                XmlInput.parse_Field(effdate);
                                break;

                            case "type":
                                XmlInput.parse_Field(ContactInfoType);
                                break;

                            case "title":
                                XmlInput.parse_Field(Title);
                                break;

                            case "organization":
                                XmlInput.parse_Field(Organization);
                                break;

                            case "firstname":
                                XmlInput.parse_Field(FirstName);
                                break;

                            case "lastname":
                                XmlInput.parse_Field(LastName);
                                break;

                            case "address1":
                                XmlInput.parse_Field(Address1);
                                break;

                            case "address2":
                                XmlInput.parse_Field(Address2);
                                break;

                            case "city":
                                XmlInput.parse_Field(City);
                                break;

                            case "state":
                                XmlInput.parse_Field(State);
                                break;

                            case "postalcode":
                                XmlInput.parse_Field(PostalCode);
                                break;

                            case "country":
                                XmlInput.parse_Field(Country);
                                break;

                            case "email":
                                XmlInput.parse_Field(Email);
                                break;

                            default:
                                _with2.SkipField();
                                break;
                        }
                        break;
                }
            }
        }
    }

    internal partial class ContactClass : IDisposable
    {
        public ContactClass() : base()
        {
        }

        public System.Collections.Generic.List<PhoneClass> col_phones = new System.Collections.Generic.List<PhoneClass>();

        public ContactInfoClass ContactInfo = new ContactInfoClass();

        internal void parse(ref RPPS_XML xmlInput)
        {
            var _with3 = xmlInput;
            string SectionName = _with3.XMLReader.Name.ToLower();
            _with3.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with3.BadFile && _with3.XMLReader.Read())
            {
                string ElementName = _with3.XMLReader.Name.ToLower();

                switch (_with3.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "contactinfo":
                                ContactInfo.Parse(ref xmlInput);
                                break;

                            case "phones":
                                Parse_Phones(xmlInput);
                                break;

                            default:
                                _with3.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        private void Parse_Phones(RPPS_XML XMLInput)
        {
            var _with4 = XMLInput;
            string SectionName = _with4.XMLReader.Name.ToLower();
            _with4.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with4.BadFile && _with4.XMLReader.Read())
            {
                string ElementName = _with4.XMLReader.Name.ToLower();

                switch (_with4.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "phone":
                                PhoneClass nxt = new PhoneClass();
                                nxt.Parse(XMLInput);
                                col_phones.Add(nxt);
                                break;

                            default:
                                _with4.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        // To detect redundant calls
        private bool disposedValue = false;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    col_phones.Clear();
                }
            }
            this.disposedValue = true;
        }

        #region " IDisposable Support "

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion " IDisposable Support "
    }
}