using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class MaskDescriptionClass
    {
        public object Key = System.DBNull.Value;
        public string MaskDesc = string.Empty;

        public string effdate = string.Empty;

        public void Parse(RPPS_XML XmlInput)
        {
            var _with1 = XmlInput;
            string SectionName = _with1.XMLReader.Name.ToLower();

            // Find the key to the biller if there is one.
            if (_with1.XMLReader.HasAttributes)
            {
                for (System.Int32 AttributeID = 0; AttributeID <= XmlInput.XMLReader.AttributeCount - 1; AttributeID++)
                {
                    XmlInput.XMLReader.MoveToAttribute(AttributeID);
                    switch (XmlInput.XMLReader.Name.ToLower())
                    {
                        case "key":
                            XmlInput.XMLReader.ReadAttributeValue();
                            Key = XmlInput.XMLReader.Value;
                            break;
                    }
                }
            }

            _with1.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with1.BadFile && _with1.XMLReader.Read())
            {
                string ElementName = _with1.XMLReader.Name.ToLower();

                switch (_with1.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "maskdesc":
                                XmlInput.parse_Field(MaskDesc);
                                break;

                            case "effdate":
                                XmlInput.parse_Field(effdate);
                                break;

                            default:
                                _with1.SkipField();
                                break;
                        }
                        break;
                }
            }
        }
    }

    internal partial class MaskInfoClass
    {
        private string privateMaskLen = "0";

        public string masklen
        {
            get { return privateMaskLen; }
            set { privateMaskLen = value; }
        }

        private string privateMask = string.Empty;

        public string mask
        {
            get { return privateMask; }
            set { privateMask = value.Trim(); }
        }

        private string privateMaskDesc = string.Empty;

        public string maskdesc
        {
            get { return privateMaskDesc; }
            set { privateMaskDesc = value.Trim(); }
        }

        private string privateeffdate = string.Empty;

        public string effdate
        {
            get { return privateeffdate; }
            set { privateeffdate = value; }
        }

        private string privateexceptionmask = "N";

        public string exceptionmask
        {
            get { return privateexceptionmask; }
            set { privateexceptionmask = value; }
        }

        private string privatecheckdigit = "N";

        public string checkdigit
        {
            get { return privatecheckdigit; }
            set { privatecheckdigit = value; }
        }

        internal void parse(RPPS_XML xmlInput)
        {
            var _with2 = xmlInput;
            string SectionName = _with2.XMLReader.Name.ToLower();
            _with2.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with2.BadFile && _with2.XMLReader.Read())
            {
                string ElementName = _with2.XMLReader.Name.ToLower();

                switch (_with2.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "mask":
                            case "maskformat":
                                _with2.parse_Field(mask);
                                break;

                            case "masklen":
                            case "masklength":
                                _with2.parse_Field(masklen);
                                break;

                            case "maskdesc":
                                _with2.parse_Field(maskdesc);
                                break;

                            case "effdate":
                                _with2.parse_Field(effdate);
                                break;

                            case "exceptionmask":
                                _with2.parse_Field(exceptionmask);
                                break;

                            case "checkdigit":
                                _with2.parse_Field(checkdigit);
                                break;

                            default:
                                _with2.SkipField();
                                break;
                        }
                        break;
                }
            }
        }
    }

    internal partial class MaskClass : IDisposable
    {
        public MaskClass() : base()
        {
        }

        public object Key = System.DBNull.Value;
        public MaskInfoClass MaskInfo = new MaskInfoClass();

        public System.Collections.Generic.List<MaskDescriptionClass> col_MaskDescriptions = new System.Collections.Generic.List<MaskDescriptionClass>();

        internal void Parse(RPPS_XML xmlInput)
        {
            string SectionName = xmlInput.XMLReader.Name.ToLower();

            // Find the key to the biller if there is one.
            if (xmlInput.XMLReader.HasAttributes)
            {
                for (System.Int32 AttributeID = 0; AttributeID <= xmlInput.XMLReader.AttributeCount - 1; AttributeID++)
                {
                    xmlInput.XMLReader.MoveToAttribute(AttributeID);
                    switch (xmlInput.XMLReader.Name.ToLower())
                    {
                        case "key":
                            xmlInput.XMLReader.ReadAttributeValue();
                            Key = xmlInput.XMLReader.Value;
                            break;
                    }
                }
            }

            xmlInput.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!xmlInput.BadFile && xmlInput.XMLReader.Read())
            {
                string ElementName = xmlInput.XMLReader.Name.ToLower();

                switch (xmlInput.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                        {
                            return;
                        }
                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "mask":
                            case "maskformat":
                                xmlInput.parse_Field(MaskInfo.mask);
                                break;

                            case "masklen":
                            case "masklength":
                                xmlInput.parse_Field(MaskInfo.masklen);
                                break;

                            case "maskdesc":
                                MaskDescriptionClass nxt = new MaskDescriptionClass();
                                var _with4 = nxt;
                                xmlInput.parse_Field(_with4.MaskDesc);
                                col_MaskDescriptions.Add(nxt);
                                break;

                            case "maskdescriptions":
                                Parse_MaskDescriptions(xmlInput);
                                break;

                            case "effdate":
                                xmlInput.parse_Field(MaskInfo.effdate);
                                break;

                            case "exceptionmask":
                                xmlInput.parse_Field(MaskInfo.exceptionmask);
                                break;

                            case "checkdigit":
                                xmlInput.parse_Field(MaskInfo.checkdigit);
                                break;

                            case "maskinfo":
                                MaskInfo.parse(xmlInput);
                                break;

                            default:
                                xmlInput.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        private void Parse_MaskDescriptions(RPPS_XML xmlInput)
        {
            string SectionName = xmlInput.XMLReader.Name.ToLower();

            xmlInput.XMLReader.MoveToContent();

            while (!xmlInput.BadFile && xmlInput.XMLReader.Read())
            {
                string ElementName = xmlInput.XMLReader.Name.ToLower();

                switch (xmlInput.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                        {
                            return;
                        }

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "maskdescription":
                                MaskDescriptionClass nxt = new MaskDescriptionClass();
                                nxt.Parse(xmlInput);
                                col_MaskDescriptions.Add(nxt);
                                break;

                            default:
                                xmlInput.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        public void WriteInfo(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, BillerClass biller_class)
        {
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "INSERT INTO rpps_masks(ExceptionMask,CheckDigit,Mastercard_key,rpps_biller_id,length,mask,description,effdate) VALUES (@ExceptionMask,@CheckDigit,@Mastercard_key,@rpps_biller_id,@length,@mask,@description,@effdate)";
                cmd.CommandType = CommandType.Text;

                cmd.Parameters.Add("@ExceptionMask", SqlDbType.Bit).Value = BillerInfoClass.bool_value(MaskInfo.exceptionmask);
                cmd.Parameters.Add("@CheckDigit", SqlDbType.Bit).Value = BillerInfoClass.bool_value(MaskInfo.checkdigit);
                cmd.Parameters.Add("@Mastercard_key", SqlDbType.Int).Value = Key;
                cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller_class.billerinfo.blrid;
                cmd.Parameters.Add("@length", SqlDbType.Int).Value = DebtPlus.Utils.Nulls.v_Int32(MaskInfo.masklen).GetValueOrDefault();
                cmd.Parameters.Add("@mask", SqlDbType.VarChar, 80).Value = MaskInfo.mask;
                cmd.Parameters.Add("@description", SqlDbType.VarChar, 80).Value = MaskInfo.maskdesc;
                cmd.Parameters.Add("@effdate", SqlDbType.DateTime, 0).Value = TypeUtilities.Date_Value(MaskInfo.effdate);
                cmd.ExecuteNonQuery();
            }
        }

        // To detect redundant calls
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    MaskInfo = null;
                }

                // Release the storage
            }

            this.disposedValue = true;
        }

        #region " IDisposable Support "

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion " IDisposable Support "
    }
}