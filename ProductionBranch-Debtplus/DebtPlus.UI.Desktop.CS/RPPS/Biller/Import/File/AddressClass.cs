#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class AddressClass : IDisposable
    {
        public AddressClass() : base()
        {
        }

        public string AddressType = "Standard";
        public string effdate = string.Empty;
        private object Key = System.DBNull.Value;
        private string privateaddr1 = string.Empty;
        private string privateaddr2 = string.Empty;
        private string privatecity = string.Empty;
        private string privatestate = string.Empty;
        private string privatezipcode = string.Empty;

        private string privatecountry = string.Empty;

        public string zipcode
        {
            get
            {
                return privatezipcode;
            }
            set
            {
                privatezipcode = value.Trim();
                if (privatezipcode.Length > 14)
                {
                    privatezipcode = privatezipcode.Substring(0, 14);
                }
            }
        }

        public string state
        {
            get
            {
                return privatestate;
            }
            set
            {
                privatestate = value.Trim();

                System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("^[a-zA-Z]{2}$", System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Singleline);
                if (!rx.IsMatch(state))
                {
                    privatestate = string.Empty;
                }
            }
        }

        public string addr1
        {
            get
            {
                return privateaddr1;
            }
            set
            {
                privateaddr1 = value.Trim();
                if (privateaddr1.Length > 80)
                {
                    privateaddr1 = privateaddr1.Substring(0, 80);
                }
            }
        }

        public string addr2
        {
            get
            {
                return privateaddr2;
            }
            set
            {
                privateaddr2 = value.Trim();
                if (privateaddr2.Length > 80)
                {
                    privateaddr2 = privateaddr2.Substring(0, 80);
                }
            }
        }

        public string city
        {
            get
            {
                return privatecity;
            }
            set
            {
                privatecity = value.Trim();
                if (privatecity.Length > 60)
                {
                    privatecity = privatecity.Substring(0, 60);
                }
            }
        }

        public string country
        {
            get
            {
                return privatecountry;
            }
            set
            {
                privatecountry = value.Trim();
                if (privatecountry.Length > 10)
                {
                    privatecountry = privatecountry.Substring(0, 10);
                }
            }
        }

        internal void Parse(RPPS_XML xmlInput)
        {
            var _with1 = xmlInput;

            // Find the key to the biller if there is one.
            if (_with1.XMLReader.HasAttributes)
            {
                for (System.Int32 AttributeID = 0; AttributeID <= xmlInput.XMLReader.AttributeCount - 1; AttributeID++)
                {
                    xmlInput.XMLReader.MoveToAttribute(AttributeID);
                    switch (xmlInput.XMLReader.Name.ToLower())
                    {
                        case "key":
                            xmlInput.XMLReader.ReadAttributeValue();
                            Key = xmlInput.XMLReader.Value;
                            break;
                    }
                }
            }

            _with1.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with1.BadFile && _with1.XMLReader.Read())
            {
                string ElementName = _with1.XMLReader.Name.ToLower();

                switch (_with1.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == "address")
                        {
                            return;
                        }
                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "address1":
                            case "addr1":
                                _with1.parse_Field(addr1);
                                break;

                            case "address2":
                            case "addr2":
                                _with1.parse_Field(addr2);
                                break;

                            case "city":
                                _with1.parse_Field(city);
                                break;

                            case "state":
                                _with1.parse_Field(state);
                                break;

                            case "zipcode":
                            case "postalcode":
                                _with1.parse_Field(zipcode);
                                break;

                            case "country":
                                _with1.parse_Field(country);
                                break;

                            case "effdate":
                                _with1.parse_Field(effdate);
                                break;

                            case "type":
                                _with1.parse_Field(AddressType);
                                break;

                            default:
                                _with1.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        private object Nullify(string InputString)
        {
            object answer = System.DBNull.Value;
            if (!string.IsNullOrEmpty(InputString))
            {
                answer = InputString;
            }
            return answer;
        }

        public void WriteInfo(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, BillerClass biller_class)
        {
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with2 = cmd;
                _with2.Connection = cn;
                _with2.Transaction = txn;
                _with2.CommandText = "INSERT INTO rpps_addresses(rpps_biller_id,biller_addr1,biller_addr2,biller_city,biller_state,biller_zipcode,biller_country) VALUES (@rpps_biller_id,@biller_addr1,@biller_addr2,@biller_city,@biller_state,@biller_zipcode,@biller_country)";
                _with2.CommandType = CommandType.Text;

                _with2.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller_class.billerinfo.blrid;
                _with2.Parameters.Add("@biller_addr1", SqlDbType.VarChar, 80).Value = Nullify(addr1);
                _with2.Parameters.Add("@biller_addr2", SqlDbType.VarChar, 80).Value = Nullify(addr2);
                _with2.Parameters.Add("@biller_city", SqlDbType.VarChar, 80).Value = Nullify(city);
                _with2.Parameters.Add("@biller_state", SqlDbType.VarChar, 10).Value = Nullify(state);
                _with2.Parameters.Add("@biller_zipcode", SqlDbType.VarChar, 14).Value = Nullify(zipcode);
                _with2.Parameters.Add("@biller_country", SqlDbType.VarChar, 80).Value = Nullify(country);
                _with2.ExecuteNonQuery();
            }
        }

        // To detect redundant calls
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    // free other state (managed objects).
                }

                // Release the storage
                privateaddr1 = null;
                privateaddr2 = null;
                privatecity = null;
                privatestate = null;
                privatezipcode = null;
                country = null;
            }

            this.disposedValue = true;
        }

        #region " IDisposable Support "

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion " IDisposable Support "
    }
}