using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
	partial class StatusForm : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.lblBillerName = new DevExpress.XtraEditors.LabelControl();
			this.lblBillerID = new DevExpress.XtraEditors.LabelControl();
			this.lblElapsedTime = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.Timer1 = new System.Windows.Forms.Timer(this.components);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Appearance.Options.UseTextOptions = true;
			this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl1.Location = new System.Drawing.Point(13, 13);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(66, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Elapsed Time:";
			//
			//LabelControl2
			//
			this.LabelControl2.Appearance.Options.UseTextOptions = true;
			this.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl2.Location = new System.Drawing.Point(13, 32);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(94, 13);
			this.LabelControl2.TabIndex = 1;
			this.LabelControl2.Text = "Processing Biller ID:";
			//
			//LabelControl3
			//
			this.LabelControl3.Appearance.Options.UseTextOptions = true;
			this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl3.Location = new System.Drawing.Point(13, 51);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(56, 13);
			this.LabelControl3.TabIndex = 2;
			this.LabelControl3.Text = "Biller Name:";
			//
			//lblBillerName
			//
			this.lblBillerName.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lblBillerName.Appearance.Options.UseTextOptions = true;
			this.lblBillerName.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.lblBillerName.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lblBillerName.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.lblBillerName.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lblBillerName.Location = new System.Drawing.Point(113, 51);
			this.lblBillerName.Name = "lblBillerName";
			this.lblBillerName.Size = new System.Drawing.Size(167, 55);
			this.lblBillerName.TabIndex = 5;
			//
			//lblBillerID
			//
			this.lblBillerID.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lblBillerID.Appearance.Options.UseTextOptions = true;
			this.lblBillerID.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.lblBillerID.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
			this.lblBillerID.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lblBillerID.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.lblBillerID.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lblBillerID.Location = new System.Drawing.Point(113, 32);
			this.lblBillerID.Name = "lblBillerID";
			this.lblBillerID.Size = new System.Drawing.Size(167, 13);
			this.lblBillerID.TabIndex = 4;
			//
			//lblElapsedTime
			//
			this.lblElapsedTime.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lblElapsedTime.Appearance.Options.UseTextOptions = true;
			this.lblElapsedTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.lblElapsedTime.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lblElapsedTime.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.lblElapsedTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lblElapsedTime.Location = new System.Drawing.Point(113, 13);
			this.lblElapsedTime.Name = "lblElapsedTime";
			this.lblElapsedTime.Size = new System.Drawing.Size(167, 13);
			this.lblElapsedTime.TabIndex = 3;
			//
			//SimpleButton_Cancel
			//
			this.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_Cancel.Location = new System.Drawing.Point(109, 112);
			this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
			this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cancel.TabIndex = 6;
			this.SimpleButton_Cancel.Text = "&Cancel";
			//
			//Timer1
			//
			//
			//StatusForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(292, 148);
			this.Controls.Add(this.SimpleButton_Cancel);
			this.Controls.Add(this.lblBillerName);
			this.Controls.Add(this.lblBillerID);
			this.Controls.Add(this.lblElapsedTime);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.Name = "StatusForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation;
			this.Text = "RPS Table Import Function";
			this.TopMost = true;
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl lblBillerName;
		protected internal DevExpress.XtraEditors.LabelControl lblBillerID;
		protected internal DevExpress.XtraEditors.LabelControl lblElapsedTime;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
		protected internal System.Windows.Forms.Timer Timer1;
	}
}
