using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class RPPS_XML : RPPS_Input
    {
        public RPPS_XML() : base()
        {
        }

        public RPPS_XML(StatusForm sts) : base(sts)
        {
        }

        protected internal void SkipField()
        {
            XMLReader.Skip();
        }

        protected internal string TagModifier = string.Empty;

        internal System.Xml.XmlReader XMLReader;

        internal override bool OpenFile()
        {
            bool answer = false;

            // If the file is open, try to use it as an XML document.
            try
            {
                System.Xml.XmlReaderSettings settings = new System.Xml.XmlReaderSettings()
														{
															ConformanceLevel = System.Xml.ConformanceLevel.Document,
															IgnoreWhitespace = true,
															IgnoreComments   = true
														};

                // Open the file directly without the stream. Let it manage it as-is.
                XMLReader = System.Xml.XmlReader.Create(status_form.ap.FileName, settings);
                answer = true;
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error opening XML document", MessageBoxButtons.OK, MessageBoxIcon.Error);
                answer = false;
            }

            // Generate an error condition if there is no reader
            if (XMLReader == null)
			{
                answer = false;
			}
			
            return answer;
        }

        public override bool ParseFile()
        {
            try
            {
                // Go to the first node
                if (!XMLReader.Read())
				{
                    return false;
				}
                do
                {
                    string ElementName = XMLReader.Name.ToLower();

                    switch (XMLReader.NodeType)
                    {
                        case System.Xml.XmlNodeType.Element:
                            switch (ElementName)
                            {
                                case "billers":
                                    parse_billers();
                                    break;

                                default:
                                    SkipField();
                                    break;
                            }
                            break;
                    }
                } while (!(BadFile || !XMLReader.Read()));

                return !BadFile;
            }
            catch (System.Xml.XmlException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error parsing bad XML file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        protected internal void parse_Field(string Field)
        {
            // Go to the biller content to read the fields
            XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!BadFile && XMLReader.Read())
            {
                switch (XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        return;

                    case System.Xml.XmlNodeType.Text:
                        Field = XMLReader.Value;
                        break;

                    default:
                        break;
                }
            }
        }

        public void parse_billers()
        {
            string SectionName = XMLReader.Name.ToLower();

            // Go to the billers content. Skip the attributes.
            XMLReader.MoveToContent();

            while (!BadFile && XMLReader.Read())
            {
                string ElementName = XMLReader.Name.ToLower();

                switch (XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
						{
                            return;
						}
                        break;

                    case System.Xml.XmlNodeType.Element:
                        if (ElementName == "biller")
                        {
                            using (var biller = new BillerClass())
                            {
                                biller.input_class = this;
                                biller.Parse(this);
                                if (!BadFile)
                                {
                                    biller.WriteInformation();
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        public override void Complete()
        {
            // Do nothing since the XML format has the flags that we want
        }
    }
}