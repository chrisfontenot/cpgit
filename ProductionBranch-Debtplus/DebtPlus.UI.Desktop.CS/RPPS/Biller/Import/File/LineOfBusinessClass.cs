using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class LineOfBusinessClass : IDisposable
    {
        public LineOfBusinessClass() : base()
        {
        }

        private string privateLOB = string.Empty;

        public string LOB
        {
            get { return privateLOB; }
            set
            {
                privateLOB = value.Trim();
                if (privateLOB.Length > 60)
                    privateLOB = privateLOB.Substring(0, 60);
            }
        }

        internal void Parse(ref RPPS_XML xmlInput)
        {
            xmlInput.parse_Field(LOB);
        }

        public void WriteInfo(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, ref BillerClass biller_class)
        {
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with1 = cmd;
                _with1.Connection = cn;
                _with1.Transaction = txn;
                _with1.CommandText = "INSERT INTO rpps_lobs(rpps_biller_id,biller_lob) VALUES (@rpps_biller_id,@biller_lob)";
                _with1.CommandType = CommandType.Text;

                _with1.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80).Value = biller_class.billerinfo.blrid;
                _with1.Parameters.Add("@biller_lob", SqlDbType.VarChar, 80).Value = LOB;
                _with1.ExecuteNonQuery();
            }
        }

        // To detect redundant calls
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    // free other state (managed objects).
                }

                privateLOB = null;
            }
            this.disposedValue = true;
        }

        #region " IDisposable Support "

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion " IDisposable Support "
    }
}