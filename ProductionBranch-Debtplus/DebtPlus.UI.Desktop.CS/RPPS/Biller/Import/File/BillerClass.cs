using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class BillerClass : IDisposable
    {
        public BillerClass() : base()
        {
        }

        public RPPS_Input input_class = null;
        public System.Collections.Generic.List<AddressClass> col_addresses = new System.Collections.Generic.List<AddressClass>();
        public System.Collections.Generic.List<AkaClass> col_akas = new System.Collections.Generic.List<AkaClass>();
        public System.Collections.Generic.List<MaskClass> col_masks = new System.Collections.Generic.List<MaskClass>();

        public System.Collections.Generic.List<ContactClass> col_contacts = new System.Collections.Generic.List<ContactClass>();
        public string acdind = "A";

        private object Key = System.DBNull.Value;
        public BillerInfoClass privatebillerinfo = null;

        public BillerInfoClass billerinfo
        {
            get
            {
                if (privatebillerinfo == null)
                {
                    privatebillerinfo = new BillerInfoClass();
                    privatebillerinfo.input_class = input_class;
                }
                return privatebillerinfo;
            }
            set { privatebillerinfo = value; }
        }

        internal void Parse(RPPS_XML xmlInput)
        {
            var _with1 = xmlInput;

            // Find the key to the biller if there is one.
            if (_with1.XMLReader.HasAttributes)
            {
                for (System.Int32 AttributeID = 0; AttributeID <= xmlInput.XMLReader.AttributeCount - 1; AttributeID++)
                {
                    xmlInput.XMLReader.MoveToAttribute(AttributeID);
                    switch (xmlInput.XMLReader.Name.ToLower())
                    {
                        case "key":
                            xmlInput.XMLReader.ReadAttributeValue();
                            billerinfo.Key = xmlInput.XMLReader.Value;
                            break;
                    }
                }
            }

            // Go to the biller content to read the fields
            _with1.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with1.BadFile && _with1.XMLReader.Read())
            {
                string ElementName = _with1.XMLReader.Name.ToLower();

                switch (_with1.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == "biller")
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "acdind":
                                xmlInput.parse_Field(acdind);
                                break;

                            case "effdate":
                                xmlInput.parse_Field(billerinfo.effdate);
                                break;

                            case "blrid":
                                xmlInput.parse_Field(billerinfo.blrid);
                                break;

                            case "billerinfo":
                                billerinfo.Parse(xmlInput);
                                break;

                            case "addresses":
                                Parse_Addresses(xmlInput);
                                break;

                            case "contacts":
                                Parse_Contacts(xmlInput);
                                break;

                            case "lineofbusiness":
                                Parse_LineOfBusinesses(xmlInput);
                                break;

                            case "akas":
                                Parse_BlrAKA(xmlInput);
                                break;

                            case "billermasks":
                            case "masks":
                                Parse_Masks(xmlInput);
                                break;

                            default:
                                _with1.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        private void Parse_Addresses(RPPS_XML xmlInput)
        {
            var _with2 = xmlInput;
            string SectionName = _with2.XMLReader.Name.ToLower();

            _with2.XMLReader.MoveToContent();

            while (!_with2.BadFile && _with2.XMLReader.Read())
            {
                string ElementName = _with2.XMLReader.Name.ToLower();

                switch (_with2.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        if (ElementName == "address")
                        {
                            AddressClass nxt = new AddressClass();
                            var _with3 = nxt;
                            _with3.Parse(xmlInput);
                            col_addresses.Add(nxt);
                        }
                        break;
                }
            }
        }

        private void Parse_LineOfBusinesses(RPPS_XML xmlInput)
        {
            var _with4 = xmlInput;
            string SectionName = _with4.XMLReader.Name.ToLower();

            _with4.XMLReader.MoveToContent();

            while (!_with4.BadFile && _with4.XMLReader.Read())
            {
                string ElementName = _with4.XMLReader.Name.ToLower();
                switch (_with4.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        if (ElementName == "lineofbusiness")
                        {
                            LineOfBusinessClass nxt = new LineOfBusinessClass();
                            var _with5 = nxt;
                            _with5.Parse(ref xmlInput);
                            billerinfo.col_lob.Add(nxt);
                        }
                        break;
                }
            }
        }

        private void Parse_BlrAKA(RPPS_XML xmlInput)
        {
            var _with6 = xmlInput;
            string SectionName = _with6.XMLReader.Name.ToLower();

            _with6.XMLReader.MoveToContent();

            while (!_with6.BadFile && _with6.XMLReader.Read())
            {
                string ElementName = _with6.XMLReader.Name.ToLower();
                switch (_with6.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        if (ElementName == "aka")
                        {
                            AkaClass nxt = new AkaClass();
                            var _with7 = nxt;
                            _with7.Parse(xmlInput);
                            col_akas.Add(nxt);
                            if (_with6.BadFile)
                                return;
                        }
                        break;
                }
            }
        }

        private void Parse_Masks(RPPS_XML xmlInput)
        {
            var _with8 = xmlInput;
            string SectionName = xmlInput.XMLReader.Name.ToLower();

            _with8.XMLReader.MoveToContent();

            while (!_with8.BadFile && _with8.XMLReader.Read())
            {
                string ElementName = _with8.XMLReader.Name.ToLower();

                switch (_with8.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        if (ElementName == "mask" || ElementName == "billermask")
                        {
                            MaskClass nxt = new MaskClass();
                            var _with9 = nxt;
                            _with9.Parse(xmlInput);
                            col_masks.Add(nxt);
                        }
                        break;
                }
            }
        }

        private void Parse_Contacts(RPPS_XML XMLInput)
        {
            var _with10 = XMLInput;
            string SectionName = _with10.XMLReader.Name.ToLower();
            _with10.XMLReader.MoveToContent();

            // Process the fields until we are at the end of the biller
            while (!_with10.BadFile && _with10.XMLReader.Read())
            {
                string ElementName = _with10.XMLReader.Name.ToLower();

                switch (_with10.XMLReader.NodeType)
                {
                    case System.Xml.XmlNodeType.EndElement:
                        if (ElementName == SectionName)
                            break;

                        break;

                    case System.Xml.XmlNodeType.Element:
                        switch (ElementName)
                        {
                            case "contact":
                                ContactClass nxt = new ContactClass();
                                nxt.parse(ref XMLInput);
                                col_contacts.Add(nxt);
                                break;

                            default:
                                _with10.SkipField();
                                break;
                        }
                        break;
                }
            }
        }

        // Create the biller ID information

        // Update the checkdigit to show "reality"

        // Find the LOB being a CC for a credit card

        // Create the ADDRESS items

        // Create the MASK items

        // Determine if the mask has a LUHN checkdigit.
        // List of credit card masks, first is mastercard, then visa, then amex, then discover. These are all standard LUHN checkdigits.
        public void WriteInformation()
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlTransaction txn = null;
            try
            {
                cn.Open();
                txn = cn.BeginTransaction();
                DeleteItems(cn, txn);
                switch (acdind.ToUpper())
                {
                    case "D":
                        break;

                    default:
                        Write_Biller_ID(cn, txn);
                        bool isCC = false;
                        var _with11 = billerinfo;
                        _with11.WriteInfo(cn, txn, this);
                        foreach (LineOfBusinessClass item in _with11.col_lob)
                        {
                            if (string.Compare(item.LOB, "cc", true) == 0)
                                isCC = true;
                        }

                        foreach (AddressClass addr in col_addresses)
                        {
                            addr.WriteInfo(cn, txn, this);
                        }

                        string WriteInformation_RegexMask = "^(5[1-5][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#]|4([0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#]|[0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#])|(34|37)[0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#]|6011[0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#][0-9#])$";
                        foreach (MaskClass mask in col_masks)
                        {
                            mask.MaskInfo.checkdigit = isCC && System.Text.RegularExpressions.Regex.IsMatch(mask.MaskInfo.mask, WriteInformation_RegexMask) ? "Y" : "N";
                            mask.WriteInfo(cn, txn, this);
                        }

                        // Add the AKA for the original name of the creditor.
                        AkaClass aka_class = new AkaClass();
                        aka_class.Name = billerinfo.billername;
                        col_akas.Add(aka_class);

                        // Create the AKA items
                        foreach (AkaClass aka in col_akas)
                        {
                            aka.WriteInfo(cn, txn, this);
                        }
                        break;
                }

                // Commit the changes at this point
                txn.Commit();
                txn = null;
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch
                    {
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();
            }
        }

        private void DeleteItems(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            DeleteItems_biller_akas(cn, txn);
            DeleteItems_biller_addresses(cn, txn);
            DeleteItems_biller_lobs(cn, txn);
            DeleteItems_biller_masks(cn, txn);
            DeleteItems_biller_id(cn, txn);
        }

        private void DeleteItems_biller_akas(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            // Delete all of the AKA items for this specific biller id
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with12 = cmd;
                _with12.Connection = cn;
                _with12.Transaction = txn;
                _with12.CommandType = CommandType.Text;
                _with12.CommandText = "DELETE FROM rpps_akas WHERE rpps_biller_id=@rpps_biller_id";
                _with12.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid;
                _with12.ExecuteNonQuery();
            }
        }

        private void DeleteItems_biller_addresses(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            // Delete all of the ADDRESS items for this specific biller id
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with13 = cmd;
                _with13.Connection = cn;
                _with13.Transaction = txn;
                _with13.CommandType = CommandType.Text;
                _with13.CommandText = "DELETE FROM rpps_addresses WHERE rpps_biller_id=@rpps_biller_id";
                _with13.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid;
                _with13.ExecuteNonQuery();
            }
        }

        private void DeleteItems_biller_lobs(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            // Delete all of the LOB items for this specific biller id
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with14 = cmd;
                _with14.Connection = cn;
                _with14.Transaction = txn;
                _with14.CommandType = CommandType.Text;
                _with14.CommandText = "DELETE FROM rpps_lobs WHERE rpps_biller_id=@rpps_biller_id";
                _with14.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid;
                _with14.ExecuteNonQuery();
            }
        }

        private void DeleteItems_biller_masks(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            // Delete all of the MASK items for this specific biller id
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with15 = cmd;
                _with15.Connection = cn;
                _with15.Transaction = txn;
                _with15.CommandType = CommandType.Text;
                _with15.CommandText = "DELETE FROM rpps_masks WHERE rpps_biller_id=@rpps_biller_id";
                _with15.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid;
                _with15.ExecuteNonQuery();
            }
        }

        private void DeleteItems_biller_id(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            // Delete all of the biller information item for this specific biller id
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with16 = cmd;
                _with16.Connection = cn;
                _with16.Transaction = txn;
                _with16.CommandType = CommandType.Text;
                string strSQL = "DELETE FROM rpps_biller_ids WHERE rpps_biller_id=@rpps_biller_id";
                _with16.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid;

                if (billerinfo.trnaba != string.Empty)
                {
                    strSQL += " OR biller_aba=@rpps_biller_aba";
                    _with16.Parameters.Add("@rpps_biller_aba", SqlDbType.VarChar, 20).Value = billerinfo.trnaba;
                }

                _with16.CommandText = strSQL;
                _with16.ExecuteNonQuery();
            }
        }

        private void Write_Biller_ID(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            try
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with17 = cmd;
                    _with17.Connection = cn;
                    _with17.Transaction = txn;
                    _with17.CommandText = "INSERT INTO rpps_biller_ids(rpps_biller_id, effdate, biller_aba) VALUES (@rpps_biller_id, @effdate, @biller_aba)";
                    _with17.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = billerinfo.blrid;
                    _with17.Parameters.Add("@effdate", SqlDbType.DateTime).Value = TypeUtilities.Date_Value(billerinfo.effdate);
                    _with17.Parameters.Add("@biller_aba", SqlDbType.VarChar, 20).Value = billerinfo.trnaba;
                    _with17.ExecuteNonQuery();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                // Ignore a duplicate key for now. It is not really important here.
                if (ex.Message.IndexOf(" duplicate key row ") < 0)
                    throw ex;
            }
        }

        // To detect redundant calls
        private bool disposedValue = false;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    col_addresses.Clear();
                    col_akas.Clear();
                    col_masks.Clear();
                }

                col_addresses = null;
                col_akas = null;
                col_masks = null;

                privatebillerinfo = null;
                input_class = null;

                acdind = string.Empty;
            }

            this.disposedValue = true;
        }

        #region " IDisposable Support "

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion " IDisposable Support "
    }
}