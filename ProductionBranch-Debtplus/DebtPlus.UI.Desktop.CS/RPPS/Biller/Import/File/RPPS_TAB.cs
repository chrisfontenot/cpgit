#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Import.File
{
    internal partial class RPPS_Tab : RPPS_Input
    {
        public RPPS_Tab() : base()
        {
        }

        public RPPS_Tab(StatusForm sts) : base(sts)
        {
        }

        private bool ParseBiller()
        {
            bool functionReturnValue     = false;
            System.Int32 count_addresses = 0;
            System.Int32 count_masks     = 0;
            System.Int32 index           = 0;
            string[] flds                = null;

            BillerClass biller = null;

            // Assume that the biller is proper.
            functionReturnValue = true;

            // Split the input record into the proper types.
            flds = LineString.Split('\t');

            // If this record is simply "there are no changes" then just terminate processing
            if (flds[0] == "X0")
            {
                functionReturnValue = false;
                return functionReturnValue;
            }

            string strLob               = null;
            LineOfBusinessClass cls_lob = default(LineOfBusinessClass);
            string strAKA               = null;
            AkaClass cls_aka            = default(AkaClass);
            AddressClass addr_class     = default(AddressClass);
            MaskClass mask_class        = default(MaskClass);
            string strLen               = null;
            string strMask              = null;
            string strDesc              = null;
            System.Int32 BoundLimit     = 0;

            do
            {
                switch (flds[0][1])
                {
                    case '0':
                        biller                       = new BillerClass();
                        biller.input_class           = this;
                        biller.acdind                = flds[0].Substring(0, 1);
                        biller.billerinfo.blrid      = flds[1];
                        biller.billerinfo.effdate    = flds[2];
                        biller.billerinfo.trnaba     = flds[3];
                        biller.billerinfo.billername = flds[4];
                        biller.billerinfo.ClassName  = flds[5];
                        biller.billerinfo.billertype = flds[6];

                        // Line of business are the next group of fields
                        for (index = 1; index <= 10; index++)
                        {
                            strLob = flds[index + 6].Trim();
                            if (strLob != string.Empty)
                            {
                                cls_lob = new LineOfBusinessClass();
                                cls_lob.LOB = strLob;
                                biller.billerinfo.col_lob.Add(cls_lob);
                            }
                        }

                        // Parse the other "flags"
                        biller.billerinfo.prenotes    = flds[17];
                        biller.billerinfo.guarpayonly = flds[18];
                        biller.billerinfo.dmpprenote  = flds[19];
                        biller.billerinfo.guarpayonly = flds[20];
                        biller.billerinfo.pvtblr      = flds[21];
                        biller.billerinfo.blroldname  = flds[22];
                        biller.billerinfo.blrnote     = flds[23];

                        // Parse the AKA list
                        for (index = 1; index <= 8; index++)
                        {
                            strAKA = flds[index + 23].Trim();
                            if (strAKA != string.Empty)
                            {
                                cls_aka = new AkaClass();
                                cls_aka.Name = strAKA;
                                biller.col_akas.Add(cls_aka);
                            }
                        }

                        // The count of items follows this field
                        if (!Int32.TryParse(flds[32], out count_addresses))
                        {
                            count_addresses = 0;
                        }

                        if (!Int32.TryParse(flds[33], out count_masks))
                        {
                            count_masks = 0;
                        }
                        break;

                    case '1':
                        if (biller != null)
                        {
                            BoundLimit = flds.GetUpperBound(0);
                            for (index = 2; index <= BoundLimit; index += 6)
                            {
                                addr_class = new AddressClass();

                                // Ignore the "reserved for future use field" at "index+0"
								
                                if (index + 0 <= BoundLimit)
								{
                                     addr_class.addr1 = flds[index + 1];
								}
								if (index + 1 <= BoundLimit)
								{
									addr_class.addr2 = flds[index + 2];
								}
                                if (index + 2 <= BoundLimit)
								{
                                    addr_class.city = flds[index + 3];
								}
                                if (index + 3 <= BoundLimit)
								{
                                    addr_class.state = flds[index + 4];
								}
                                if (index + 4 <= BoundLimit)
								{
                                    addr_class.zipcode = flds[index + 5];
								}
                                if (index + 5 <= BoundLimit)
								{
                                    addr_class.country = flds[index + 6];
								}
                                biller.col_addresses.Add(addr_class);
                                count_addresses = count_addresses - 1;
                                if (count_addresses == 0)
								{
                                    break;
								}
                            }
                        }
                        break;

                    case '2':
                        if (biller != null)
                        {
                            for (index = 2; index <= flds.GetUpperBound(0); index += 3)
                            {
                                strLen  = flds[index];
                                strMask = string.Empty;
                                strDesc = string.Empty;
                                if (index <= flds.GetUpperBound(0))
								{
                                    strMask = flds[index + 1];
								}
                                if ((index + 1) <= flds.GetUpperBound(0))
								{
                                    strDesc = flds[index + 2];
								}

                                if (strMask != string.Empty && strLen != string.Empty)
                                {
                                    mask_class                   = new MaskClass();
                                    mask_class.MaskInfo.mask     = strMask + string.Empty;
                                    mask_class.MaskInfo.maskdesc = strDesc + string.Empty;
                                    mask_class.MaskInfo.masklen  = strLen + string.Empty;
                                    biller.col_masks.Add(mask_class);
                                    count_masks                  = count_masks - 1;
                                }
                            }
                        }
                        break;
                }

                // Read the next record
                if (!NextLine())
                {
                    functionReturnValue = false;
                    break;
                }

                // Split the record into the fields
                flds = LineString.Split('\t');

                // If this is for a different biller then the current one is complete.
                if ((biller != null))
                {
                    if (flds[1] != biller.billerinfo.blrid)
					{
                        break;
					}
                }
            } while (true);

            // If we have reached this point then add the biller to the list. All of the additional data should be present.
            System.Diagnostics.Debug.Assert(((biller != null)) && (count_masks == 0) && (count_addresses == 0), string.Empty);
            if ((biller != null) && (count_masks == 0) && (count_addresses == 0))
            {
                biller.WriteInformation();
            }
            return functionReturnValue;
        }

        public override bool ParseFile()
        {
            // Read the first line of the input
            if (!NextLine())
            {
                return false;
            }

            // Build a new collection of biller information
            while (ParseBiller())
            {
            }

            return true;
        }
    }
}