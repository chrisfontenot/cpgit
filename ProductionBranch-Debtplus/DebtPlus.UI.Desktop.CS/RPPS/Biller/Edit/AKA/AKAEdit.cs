#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.AKA
{
    public partial class AKAEdit
    {
        public AKAEdit() : base()
        {
            InitializeComponent();
            this.Load += Form_Load;
        }

        private System.Data.DataRowView drv = null;

        public AKAEdit(System.Data.DataRowView drv) : this()
        {
            this.drv = drv;
        }

        private void Form_Load(object sender, System.EventArgs e)
        {
            var _with1 = TextEdit1;
            _with1.DataBindings.Clear();
            _with1.DataBindings.Add("EditValue", drv, "name");
            _with1.EditValueChanged += Form_Changed;
            SimpleButton_OK.Enabled = !HasErrors();
        }

        private void Form_Changed(object Sender, System.EventArgs e)
        {
            SimpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return false;
        }
    }
}