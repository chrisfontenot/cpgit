using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.AKA
{
	partial class AKAEdit : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Cance = new DevExpress.XtraEditors.SimpleButton();
			this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.EmptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.SimpleButton_OK.Enabled = false;
			this.SimpleButton_OK.Location = new System.Drawing.Point(66, 53);
			this.SimpleButton_OK.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.Name = "SimpleButton_OK";
			this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.StyleController = this.LayoutControl1;
			this.SimpleButton_OK.TabIndex = 2;
			this.SimpleButton_OK.Text = "&OK";
			//
			//SimpleButton_Cance
			//
			this.SimpleButton_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_Cance.Location = new System.Drawing.Point(152, 53);
			this.SimpleButton_Cance.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cance.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cance.Name = "SimpleButton_Cance";
			this.SimpleButton_Cance.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cance.StyleController = this.LayoutControl1;
			this.SimpleButton_Cance.TabIndex = 3;
			this.SimpleButton_Cance.Text = "&Cancel";
			//
			//TextEdit1
			//
			this.TextEdit1.Location = new System.Drawing.Point(39, 7);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Properties.MaxLength = 50;
			this.TextEdit1.Size = new System.Drawing.Size(247, 20);
			this.TextEdit1.StyleController = this.LayoutControl1;
			this.TextEdit1.TabIndex = 1;
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.TextEdit1);
			this.LayoutControl1.Controls.Add(this.SimpleButton_OK);
			this.LayoutControl1.Controls.Add(this.SimpleButton_Cance);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(292, 97);
			this.LayoutControl1.TabIndex = 4;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.EmptySpaceItem1,
				this.EmptySpaceItem2,
				this.EmptySpaceItem3,
				this.EmptySpaceItem4
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(292, 97);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.TextEdit1;
			this.LayoutControlItem1.CustomizationFormText = "Name";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(290, 31);
			this.LayoutControlItem1.Text = "Name";
			this.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(27, 20);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.SimpleButton_OK;
			this.LayoutControlItem2.CustomizationFormText = "LayoutControlItem2";
			this.LayoutControlItem2.Location = new System.Drawing.Point(59, 46);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(86, 34);
			this.LayoutControlItem2.Text = "LayoutControlItem2";
			this.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem2.TextToControlDistance = 0;
			this.LayoutControlItem2.TextVisible = false;
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.SimpleButton_Cance;
			this.LayoutControlItem3.CustomizationFormText = "LayoutControlItem3";
			this.LayoutControlItem3.Location = new System.Drawing.Point(145, 46);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(86, 34);
			this.LayoutControlItem3.Text = "LayoutControlItem3";
			this.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem3.TextToControlDistance = 0;
			this.LayoutControlItem3.TextVisible = false;
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 46);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(59, 34);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(231, 46);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(59, 34);
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem3
			//
			this.EmptySpaceItem3.CustomizationFormText = "EmptySpaceItem3";
			this.EmptySpaceItem3.Location = new System.Drawing.Point(0, 31);
			this.EmptySpaceItem3.Name = "EmptySpaceItem3";
			this.EmptySpaceItem3.Size = new System.Drawing.Size(290, 15);
			this.EmptySpaceItem3.Text = "EmptySpaceItem3";
			this.EmptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
			//
			//EmptySpaceItem4
			//
			this.EmptySpaceItem4.CustomizationFormText = "EmptySpaceItem4";
			this.EmptySpaceItem4.Location = new System.Drawing.Point(0, 80);
			this.EmptySpaceItem4.Name = "EmptySpaceItem4";
			this.EmptySpaceItem4.Size = new System.Drawing.Size(290, 15);
			this.EmptySpaceItem4.Text = "EmptySpaceItem4";
			this.EmptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
			//
			//AKAEdit
			//
			this.AcceptButton = this.SimpleButton_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButton_Cance;
			this.ClientSize = new System.Drawing.Size(292, 97);
			this.Controls.Add(this.LayoutControl1);
			this.MaximizeBox = false;
			this.Name = "AKAEdit";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "RPPS AKA";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem4).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cance;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit1;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem3;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem4;
	}
}
