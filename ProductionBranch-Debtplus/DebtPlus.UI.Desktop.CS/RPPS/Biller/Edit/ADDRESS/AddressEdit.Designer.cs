using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.ADDRESS
{
	partial class AddressEdit : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Cance = new DevExpress.XtraEditors.SimpleButton();
			this.AddressParts1 = new DebtPlus.Data.Controls.AddressParts();
			this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.SimpleButton_OK.Enabled = false;
			this.SimpleButton_OK.Location = new System.Drawing.Point(81, 136);
			this.SimpleButton_OK.Name = "SimpleButton_OK";
			this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.TabIndex = 2;
			this.SimpleButton_OK.Text = "&OK";
			//
			//SimpleButton_Cance
			//
			this.SimpleButton_Cance.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_Cance.Location = new System.Drawing.Point(173, 136);
			this.SimpleButton_Cance.Name = "SimpleButton_Cance";
			this.SimpleButton_Cance.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cance.TabIndex = 3;
			this.SimpleButton_Cance.Text = "&Cancel";
			//
			//AddressParts1
			//
			this.AddressParts1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.AddressParts1.Location = new System.Drawing.Point(32, 13);
			this.AddressParts1.Name = "AddressParts1";
			this.AddressParts1.Size = new System.Drawing.Size(285, 72);
			this.ToolTipController1.SetSuperTip(this.AddressParts1, null);
			this.AddressParts1.TabIndex = 0;
			//
			//TextEdit1
			//
			this.TextEdit1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.TextEdit1.Location = new System.Drawing.Point(32, 88);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Size = new System.Drawing.Size(285, 20);
			this.TextEdit1.TabIndex = 1;
			//
			//AddressEdit
			//
			this.AcceptButton = this.SimpleButton_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButton_Cance;
			this.ClientSize = new System.Drawing.Size(329, 171);
			this.Controls.Add(this.TextEdit1);
			this.Controls.Add(this.AddressParts1);
			this.Controls.Add(this.SimpleButton_Cance);
			this.Controls.Add(this.SimpleButton_OK);
			this.MaximizeBox = false;
			this.Name = "AddressEdit";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "RPPS Address";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cance;
		protected internal DebtPlus.Data.Controls.AddressParts AddressParts1;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit1;
	}
}
