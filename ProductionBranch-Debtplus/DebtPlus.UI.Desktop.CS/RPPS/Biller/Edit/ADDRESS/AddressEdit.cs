#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.ADDRESS
{
    public partial class AddressEdit
    {
        public AddressEdit() : base()
        {
            InitializeComponent();
        }

        private System.Data.DataRowView drv = null;

        public AddressEdit(System.Data.DataRowView drv) : this()
        {
            this.drv = drv;
            SimpleButton_OK.Click += SimpleButton_OK_Click;
            Load += Form_Load;
        }

        private void Form_Load(object sender, System.EventArgs e)
        {
            AddressParts1.BeginInit();
            try
            {
                AddressParts1.Line1.DataBindings.Clear();
                AddressParts1.Line1.DataBindings.Add("EditValue", drv, "biller_addr1");
                AddressParts1.Line1.EditValueChanged += Form_Changed;

                AddressParts1.Line2.DataBindings.Clear();
                AddressParts1.Line2.DataBindings.Add("EditValue", drv, "biller_addr2");
                AddressParts1.Line2.EditValueChanged += Form_Changed;

                AddressParts1.PostalCode.DataBindings.Clear();
                AddressParts1.PostalCode.DataBindings.Add("EditValue", drv, "biller_zipcode");
                AddressParts1.PostalCode.EditValueChanged += Form_Changed;

                AddressParts1.City.DataBindings.Clear();
                AddressParts1.City.DataBindings.Add("EditValue", drv, "biller_city");
                AddressParts1.City.EditValueChanged += Form_Changed;

                AddressParts1.States.DataBindings.Clear();
                AddressParts1.States.DataBindings.Add("Text", drv, "biller_state");
                AddressParts1.States.EditValueChanged += Form_Changed;
            }
            finally
            {
                AddressParts1.EndInit();
            }

            TextEdit1.DataBindings.Clear();
            TextEdit1.DataBindings.Add("EditValue", drv, "biller_country");
            TextEdit1.EditValueChanged += Form_Changed;

            SimpleButton_OK.Enabled = !HasErrors();
        }

        private void Form_Changed(object Sender, System.EventArgs e)
        {
            SimpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return false;
        }

        private void SimpleButton_OK_Click(object sender, System.EventArgs e)
        {
            drv["biller_state"] = AddressParts1.States.Text;
        }
    }
}