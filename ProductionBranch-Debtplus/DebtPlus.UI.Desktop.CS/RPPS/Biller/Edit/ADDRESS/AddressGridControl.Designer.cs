using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.ADDRESS
{
	partial class AddressGridControl
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.GridColumn_address = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_addr1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_addr1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.SuspendLayout();
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_address,
				this.GridColumn_addr1,
				this.GridColumn1,
				this.GridColumn2,
				this.GridColumn3,
				this.GridColumn4,
				this.GridColumn5
			});
			//
			//GridColumn_address
			//
			this.GridColumn_address.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_address.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_address.Caption = "ID";
			this.GridColumn_address.CustomizationCaption = "Record ID";
			this.GridColumn_address.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_address.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_address.FieldName = "rpps_biller_address";
			this.GridColumn_address.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_address.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_address.Name = "GridColumn_address";
			//
			//GridColumn_addr1
			//
			this.GridColumn_addr1.Caption = "Addr1";
			this.GridColumn_addr1.FieldName = "biller_addr1";
			this.GridColumn_addr1.Name = "GridColumn_addr1";
			this.GridColumn_addr1.Visible = true;
			this.GridColumn_addr1.VisibleIndex = 0;
			//
			//GridColumn_counselor
			//
			this.GridColumn1.Caption = "Addr2";
			this.GridColumn1.FieldName = "biller_addr2";
			this.GridColumn1.Name = "GridColumn_counselor";
			//
			//GridColumn_subject
			//
			this.GridColumn2.Caption = "City";
			this.GridColumn2.FieldName = "biller_city";
			this.GridColumn2.Name = "GridColumn_subject";
			this.GridColumn2.Visible = true;
			this.GridColumn2.VisibleIndex = 1;
			//
			//GridColumn_Id
			//
			this.GridColumn3.Caption = "State";
			this.GridColumn3.FieldName = "biller_state";
			this.GridColumn3.Name = "GridColumn_Id";
			this.GridColumn3.Visible = true;
			this.GridColumn3.VisibleIndex = 2;
			//
			//GridColumn_dont_delete
			//
			this.GridColumn4.Caption = "Postal Code";
			this.GridColumn4.FieldName = "biller_zipcode";
			this.GridColumn4.Name = "GridColumn_dont_delete";
			//
			//GridColumn_dont_print
			//
			this.GridColumn5.Caption = "Country";
			this.GridColumn5.FieldName = "biller_country";
			this.GridColumn5.Name = "GridColumn_dont_print";
			//
			//AddressGridControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "AddressGridControl";
			this.Size = new System.Drawing.Size(426, 355);
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_address;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_addr1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn2;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn3;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn4;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn5;
	}
}
