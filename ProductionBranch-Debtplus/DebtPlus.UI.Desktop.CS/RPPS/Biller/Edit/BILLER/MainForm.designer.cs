using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.BILLER
{
	partial class MainForm : DebtPlus.UI.Common.Templates.ADO.Net.MainTemplateForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
			DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
			this.GridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn6.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_id = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn5.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn7.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn8.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn9.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn10.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn11.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn12.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn13.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn14.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn15.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn16.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn17.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn18.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//GridControl1
			//
			this.GridControl1.EmbeddedNavigator.Name = "";
			//
			//GridView1
			//
			this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
			this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.Empty.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
			this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
			this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
			this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
			this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
			this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
			this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
			this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.Preview.Options.UseFont = true;
			this.GridView1.Appearance.Preview.Options.UseForeColor = true;
			this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.Row.Options.UseBackColor = true;
			this.GridView1.Appearance.Row.Options.UseForeColor = true;
			this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
			this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_id,
				this.GridColumn5,
				this.GridColumn6,
				this.GridColumn4,
				this.GridColumn1,
				this.GridColumn2,
				this.GridColumn3,
				this.GridColumn7,
				this.GridColumn8,
				this.GridColumn9,
				this.GridColumn10,
				this.GridColumn11,
				this.GridColumn12,
				this.GridColumn13,
				this.GridColumn14,
				this.GridColumn15,
				this.GridColumn16,
				this.GridColumn17,
				this.GridColumn18
			});
			StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(128));
			StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(128));
			StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.Yellow;
			StyleFormatCondition1.Appearance.Options.UseBackColor = true;
			StyleFormatCondition1.Appearance.Options.UseBorderColor = true;
			StyleFormatCondition1.ApplyToRow = true;
			StyleFormatCondition1.Column = this.GridColumn6;
			StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
			StyleFormatCondition1.Value1 = 4;
			StyleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			StyleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			StyleFormatCondition2.Appearance.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			StyleFormatCondition2.Appearance.Options.UseBackColor = true;
			StyleFormatCondition2.Appearance.Options.UseBorderColor = true;
			StyleFormatCondition2.ApplyToRow = true;
			StyleFormatCondition2.Column = this.GridColumn6;
			StyleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Between;
			StyleFormatCondition2.Value1 = 10;
			StyleFormatCondition2.Value2 = 13;
			this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
				StyleFormatCondition1,
				StyleFormatCondition2
			});
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsNavigation.AutoFocusNewRow = true;
			this.GridView1.OptionsSelection.InvertSelection = true;
			this.GridView1.OptionsView.RowAutoHeight = true;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			//
			//GridColumn6
			//
			this.GridColumn6.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn6.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn6.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn6.Caption = "Biller Type";
			this.GridColumn6.DisplayFormat.FormatString = "f0";
			this.GridColumn6.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.GridColumn6.FieldName = "biller_type";
			this.GridColumn6.GroupFormat.FormatString = "f0";
			this.GridColumn6.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.GridColumn6.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DisplayText;
			this.GridColumn6.Name = "GridColumn6";
			this.GridColumn6.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
			this.GridColumn6.Visible = true;
			this.GridColumn6.VisibleIndex = 3;
			//
			//GridColumn_id
			//
			this.GridColumn_id.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_id.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_id.Caption = "ID";
			this.GridColumn_id.FieldName = "rpps_biller_id";
			this.GridColumn_id.Name = "GridColumn_id";
			this.GridColumn_id.Visible = true;
			this.GridColumn_id.VisibleIndex = 0;
			//
			//GridColumn_counselor
			//
			this.GridColumn1.Caption = "Rev Biller ID";
			this.GridColumn1.FieldName = "reversal_biller_id";
			this.GridColumn1.Name = "GridColumn_counselor";
			//
			//GridColumn_subject
			//
			this.GridColumn2.Caption = "EFF Date";
			this.GridColumn2.FieldName = "effdate";
			this.GridColumn2.Name = "GridColumn_subject";
			//
			//GridColumn_Id
			//
			this.GridColumn3.Caption = "ABA";
			this.GridColumn3.FieldName = "biller_aba";
			this.GridColumn3.Name = "GridColumn_Id";
			this.GridColumn3.Visible = true;
			this.GridColumn3.VisibleIndex = 1;
			//
			//GridColumn_dont_delete
			//
			this.GridColumn4.Caption = "Name";
			this.GridColumn4.FieldName = "biller_name";
			this.GridColumn4.Name = "GridColumn_dont_delete";
			this.GridColumn4.Visible = true;
			this.GridColumn4.VisibleIndex = 2;
			//
			//GridColumn_dont_print
			//
			this.GridColumn5.Caption = "Biller Class";
			this.GridColumn5.FieldName = "biller_class";
			this.GridColumn5.Name = "GridColumn_dont_print";
			this.GridColumn5.Visible = true;
			this.GridColumn5.VisibleIndex = 3;
			//
			//GridColumn7
			//
			this.GridColumn7.Caption = "Payment Pnote";
			this.GridColumn7.FieldName = "payment_prenote";
			this.GridColumn7.Name = "GridColumn7";
			//
			//GridColumn8
			//
			this.GridColumn8.Caption = "Proposal Pnote";
			this.GridColumn8.FieldName = "proposal_prenote";
			this.GridColumn8.Name = "GridColumn8";
			//
			//GridColumn9
			//
			this.GridColumn9.Caption = "DMP Pay Only";
			this.GridColumn9.FieldName = "dmppayonly";
			this.GridColumn9.Name = "GridColumn9";
			//
			//GridColumn10
			//
			this.GridColumn10.Caption = "Gntd Funds";
			this.GridColumn10.FieldName = "guaranteed_funds";
			this.GridColumn10.Name = "GridColumn10";
			//
			//GridColumn11
			//
			this.GridColumn11.Caption = "PVT Biller";
			this.GridColumn11.FieldName = "pvt_biller_id";
			this.GridColumn11.Name = "GridColumn11";
			//
			//GridColumn12
			//
			this.GridColumn12.Caption = "Note";
			this.GridColumn12.FieldName = "note";
			this.GridColumn12.Name = "GridColumn12";
			//
			//GridColumn13
			//
			this.GridColumn13.Caption = "CDP";
			this.GridColumn13.FieldName = "send_cdp";
			this.GridColumn13.Name = "GridColumn13";
			//
			//GridColumn14
			//
			this.GridColumn14.Caption = "FBC";
			this.GridColumn14.FieldName = "send_fbc";
			this.GridColumn14.Name = "GridColumn14";
			//
			//GridColumn15
			//
			this.GridColumn15.Caption = "FBD";
			this.GridColumn15.FieldName = "send_fbd";
			this.GridColumn15.Name = "GridColumn15";
			//
			//GridColumn16
			//
			this.GridColumn16.Caption = "CDV";
			this.GridColumn16.FieldName = "send_cdv";
			this.GridColumn16.Name = "GridColumn16";
			//
			//GridColumn17
			//
			this.GridColumn17.Caption = "CDD";
			this.GridColumn17.FieldName = "send_cdd";
			this.GridColumn17.Name = "GridColumn17";
			//
			//GridColumn18
			//
			this.GridColumn18.Caption = "blroldname";
			this.GridColumn18.FieldName = "blroldname";
			this.GridColumn18.Name = "GridColumn18";
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 294);
			this.Name = "MainForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "RPPS Biller IDs";
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_id;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn2;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn3;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn4;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn5;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn6;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn7;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn8;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn9;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn10;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn11;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn12;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn13;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn14;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn15;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn16;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn17;

		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn18;
	}
}
