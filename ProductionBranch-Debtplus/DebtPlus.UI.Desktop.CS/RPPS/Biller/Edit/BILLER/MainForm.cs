#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.BILLER
{
    internal partial class MainForm
    {
        // Public Property ctx As Context = New Context()

        public MainForm() : base()
        {
            ctx = new Context();
            InitializeComponent();
            this.Load += MainForm_Load;
            this.Load += MyGridControl_Load;

            GridColumn6.GroupFormat.Format = new FormatBillerType();
            GridColumn6.DisplayFormat.Format = new FormatBillerType();
        }

        /// <summary>
        /// Format the contact method to the suiltable text string
        /// </summary>
        private class FormatBillerType : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(System.Type formatType)
            {
                return this;
            }

            public string Format(string format1, object arg, System.IFormatProvider formatProvider)
            {
                if (object.ReferenceEquals(arg, System.DBNull.Value) || arg == null)
                    arg = 0;
                switch (Convert.ToInt32(arg))
                {
                    case 1:
                        return "NET";

                    case 2:
                        return "GROSS";

                    case 3:
                        return "MODIFIED GROSS";

                    case 4:
                        return "DMP";

                    case 11:
                        return "DMP/NET";

                    case 12:
                        return "DMP/GROSS";

                    case 13:
                        return "DMP/MODIFIED GROSS";

                    default:
                        return Convert.ToInt32(arg).ToString();
                }
            }
        }

        /// <summary>
        /// Process the load event on the form
        /// </summary>
        private void MainForm_Load(object sender, System.EventArgs e)
        {
            base.LoadPlacement("Tables.RPPS.Billers.MainForm");
            RefreshInformation();
        }

        /// <summary>
        /// Refresh the data on the form
        /// </summary>

        protected void RefreshInformation()
        {
            // Commit any pending changes before refresing the form
            if (ctx.ds.Tables.Count > 0)
                CommitChanges();

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                ctx.ds.Clear();

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(SelectCommand());
                da.Fill(ctx.ds, "rpps_biller_ids");
                RecordTable = ctx.ds.Tables["rpps_biller_ids"];

                if (RecordTable.PrimaryKey.GetUpperBound(0) < 0)
                    RecordTable.PrimaryKey = new System.Data.DataColumn[] { RecordTable.Columns["rpps_biller_id"] };

                RecordTable.Columns["biller_type"].DefaultValue = 1;
                RecordTable.Columns["payment_prenote"].DefaultValue = false;
                RecordTable.Columns["proposal_prenote"].DefaultValue = false;
                RecordTable.Columns["dmppayonly"].DefaultValue = false;
                RecordTable.Columns["guaranteed_funds"].DefaultValue = false;
                RecordTable.Columns["pvt_biller_id"].DefaultValue = false;

                RecordTable.Columns["send_cdd"].DefaultValue = false;
                RecordTable.Columns["send_cdp"].DefaultValue = false;
                RecordTable.Columns["send_fbc"].DefaultValue = false;
                RecordTable.Columns["send_fbd"].DefaultValue = false;
                RecordTable.Columns["send_cdv"].DefaultValue = false;
                RecordTable.Columns["send_cdn"].DefaultValue = false;
                RecordTable.Columns["send_cdf"].DefaultValue = false;
                RecordTable.Columns["send_exception_pay"].DefaultValue = false;

                RecordTable.Columns["returns_cdr"].DefaultValue = false;
                RecordTable.Columns["returns_cdt"].DefaultValue = false;
                RecordTable.Columns["returns_cda"].DefaultValue = false;
                RecordTable.Columns["returns_cdv"].DefaultValue = false;
                RecordTable.Columns["returns_cdc"].DefaultValue = false;
                RecordTable.Columns["returns_cdm"].DefaultValue = false;
                RecordTable.Columns["reqAdndaRev"].DefaultValue = false;
                RecordTable.Columns["checkdigit"].DefaultValue = false;

                RecordTable.Columns["reversal_biller_id"].DefaultValue = "0000000000";

                // Bind the dataset to the control
                GridControl1.DataSource = RecordTable.DefaultView;
                GridControl1.RefreshDataSource();
                GridView1.BestFitColumns();
            }
            catch (Exception ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error retrieving database information");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Handle the edit of the information on the form
        /// </summary>
        protected override System.Windows.Forms.DialogResult EditRow(System.Data.DataRowView drv)
        {
            System.Windows.Forms.DialogResult answer = System.Windows.Forms.DialogResult.Cancel;
            EditForm frm = null;

            try
            {
                // Start with the row in an "edit" mode
                frm = new EditForm(ctx, drv);
                drv.BeginEdit();
                answer = frm.ShowDialog();

                // If the dialog was successful then write the changes to the database
                if (answer == System.Windows.Forms.DialogResult.OK)
                {
                    drv.EndEdit();
                    SimpleButton_SaveChanges.Enabled = !RecordTable.HasErrors;
                }
            }
#pragma warning disable 168
            catch (System.Data.ConstraintException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show("The ID is already defined and may not be duplicated. Please use a new ID.", "Duplicated ID", MessageBoxButtons.OK, MessageBoxIcon.Error);
                answer = System.Windows.Forms.DialogResult.Cancel;
            }
#pragma warning restore 168
            catch (System.Data.DataException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Data Exception", MessageBoxButtons.OK, MessageBoxIcon.Error);
                answer = System.Windows.Forms.DialogResult.Cancel;
            }
            finally
            {
                if (drv.IsEdit)
                    drv.CancelEdit();
                if (frm != null)
                {
                    if (frm.Visible)
                        frm.Close();
                    if (!frm.IsDisposed)
                        frm.Dispose();
                }
            }

            return answer;
        }

        /// <summary>
        /// Return the selection command block
        /// </summary>
        private System.Data.SqlClient.SqlCommand SelectCommand()
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            cmd.CommandText = "SELECT [rpps_biller_id],[Mastercard_Key],[reversal_biller_id],[effdate],[livedate],[biller_aba],[biller_name],[biller_class],[biller_type],[currency],[payment_prenote],[proposal_prenote],[dmppayonly],[guaranteed_funds],[pvt_biller_id],[note],[send_cdp],[send_fbc],[send_fbd],[send_cdv],[send_cdd],[send_cdn],[send_cdf],[send_exception_pay],[returns_cdr],[returns_cdt],[returns_cda],[returns_cdv],[returns_cdc],[returns_cdm],[reqAdndaRev],[checkdigit],[blroldname],[date_created],[created_by] FROM rpps_biller_ids";
            cmd.CommandType = CommandType.Text;
            return cmd;
        }

        /// <summary>
        /// Commit the changes to the database
        /// </summary>
        protected override void CommitChanges()
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlTransaction txn = null;

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                txn = cn.BeginTransaction();

                Commit_Billers_Changes(cn, txn);
                Commit_akas_Changes(cn, txn);
                Commit_lobs_Changes(cn, txn);
                Commit_masks_Changes(cn, txn);
                Commit_addresses_Changes(cn, txn);

                txn.Commit();
                txn = null;

                SimpleButton_SaveChanges.Enabled = false;
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch
                    {
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();

                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        #region " rpps_biller_ids "

        /// <summary>
        /// Commit the changes to the biller table
        /// </summary>

        private void Commit_Billers_Changes(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
            try
            {
                da.UpdateCommand = Update_Billers_Command(cn, txn);
                da.DeleteCommand = Delete_Billers_Command(cn, txn);
                da.InsertCommand = Insert_Billers_Command(cn, txn);

                System.Int32 UpdateCount = da.Update(ctx.ds.Tables["rpps_biller_ids"]);
                UpdateCount += 0;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Return the insert command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Insert_Billers_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "INSERT INTO rpps_biller_ids ([rpps_biller_id],[Mastercard_Key],[reversal_biller_id],[effdate],[livedate],[biller_aba],[biller_name],[biller_class],[biller_type],[currency],[payment_prenote],[proposal_prenote],[dmppayonly],[guaranteed_funds],[pvt_biller_id],[note],[send_cdp],[send_fbc],[send_fbd],[send_cdv],[send_cdd],[send_cdn],[send_cdf],[send_exception_pay],[returns_cdr],[returns_cdt],[returns_cda],[returns_cdv],[returns_cdc],[returns_cdm],[reqAdndaRev],[checkdigit],[blroldname]) VALUES (@rpps_biller_id,@Mastercard_Key,@reversal_biller_id,@effdate,@livedate,@biller_aba,@biller_name,@biller_class,@biller_type,@currency,@payment_prenote,@proposal_prenote,@dmppayonly,@guaranteed_funds,@pvt_biller_id,@note,@send_cdp,@send_fbc,@send_fbd,@send_cdv,@send_cdd,@send_cdn,@send_cdf,@send_exception_pay,@returns_cdr,@returns_cdt,@returns_cda,@returns_cdv,@returns_cdc,@returns_cdm,@reqAdndaRev,@checkdigit,@blroldname)";
            cmd.CommandType = CommandType.Text;

            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20, "rpps_biller_id");
            cmd.Parameters.Add("@Mastercard_Key", SqlDbType.Int, 0, "Mastercard_Key");
            cmd.Parameters.Add("@reversal_biller_id", SqlDbType.VarChar, 20, "reversal_biller_id");
            cmd.Parameters.Add("@effdate", SqlDbType.DateTime, 0, "effdate");
            cmd.Parameters.Add("@livedate", SqlDbType.DateTime, 0, "livedate");
            cmd.Parameters.Add("@biller_aba", SqlDbType.VarChar, 20, "biller_aba");
            cmd.Parameters.Add("@biller_name", SqlDbType.VarChar, 80, "biller_name");
            cmd.Parameters.Add("@biller_class", SqlDbType.VarChar, 50, "biller_class");
            cmd.Parameters.Add("@biller_type", SqlDbType.Int, 0, "biller_type");
            cmd.Parameters.Add("@currency", SqlDbType.Int, 0, "currency");
            cmd.Parameters.Add("@payment_prenote", SqlDbType.Bit, 0, "payment_prenote");
            cmd.Parameters.Add("@proposal_prenote", SqlDbType.Bit, 0, "proposal_prenote");
            cmd.Parameters.Add("@dmppayonly", SqlDbType.Bit, 0, "dmppayonly");
            cmd.Parameters.Add("@guaranteed_funds", SqlDbType.Bit, 0, "guaranteed_funds");
            cmd.Parameters.Add("@pvt_biller_id", SqlDbType.Bit, 0, "pvt_biller_id");
            cmd.Parameters.Add("@note", SqlDbType.VarChar, 1024, "note");
            cmd.Parameters.Add("@send_cdp", SqlDbType.Bit, 0, "send_cdp");
            cmd.Parameters.Add("@send_fbc", SqlDbType.Bit, 0, "send_fbc");
            cmd.Parameters.Add("@send_fbd", SqlDbType.Bit, 0, "send_fbd");
            cmd.Parameters.Add("@send_cdv", SqlDbType.Bit, 0, "send_cdv");
            cmd.Parameters.Add("@send_cdd", SqlDbType.Bit, 0, "send_cdd");
            cmd.Parameters.Add("@send_cdn", SqlDbType.Bit, 0, "send_cdn");
            cmd.Parameters.Add("@send_cdf", SqlDbType.Bit, 0, "send_cdf");
            cmd.Parameters.Add("@send_exception_pay", SqlDbType.Bit, 0, "send_exception_pay");
            cmd.Parameters.Add("@returns_cdr", SqlDbType.Bit, 0, "returns_cdr");
            cmd.Parameters.Add("@returns_cdt", SqlDbType.Bit, 0, "returns_cdt");
            cmd.Parameters.Add("@returns_cda", SqlDbType.Bit, 0, "returns_cda");
            cmd.Parameters.Add("@returns_cdv", SqlDbType.Bit, 0, "returns_cdv");
            cmd.Parameters.Add("@returns_cdc", SqlDbType.Bit, 0, "returns_cdc");
            cmd.Parameters.Add("@returns_cdm", SqlDbType.Bit, 0, "returns_cdm");
            cmd.Parameters.Add("@reqAdndaRev", SqlDbType.Bit, 0, "reqAdndaRev");
            cmd.Parameters.Add("@checkdigit", SqlDbType.Bit, 0, "checkdigit");
            cmd.Parameters.Add("@blroldname", SqlDbType.VarChar, 1024, "blroldname");
            return cmd;
        }

        /// <summary>
        /// Return the update command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Update_Billers_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "UPDATE rpps_biller_ids SET [rpps_biller_id]=@rpps_biller_id,[Mastercard_Key]=@Mastercard_Key,[reversal_biller_id]=@reversal_biller_id,[effdate]=@effdate,[livedate]=@livedate,[biller_aba]=@biller_aba,[biller_name]=@biller_name,[biller_class]=@biller_class,[biller_type]=@biller_type,[currency]=@currency,[payment_prenote]=@payment_prenote,[proposal_prenote]=@proposal_prenote,[dmppayonly]=@dmppayonly,[guaranteed_funds]=@guaranteed_funds,[pvt_biller_id]=@pvt_biller_id,[note]=@note,[send_cdp]=@send_cdp,[send_fbc]=@send_fbc,[send_fbd]=@send_fbd,[send_cdv]=@send_cdv,[send_cdd]=@send_cdd,[send_cdn]=@send_cdn,[send_cdf]=@send_cdf,[send_exception_pay]=@send_exception_pay,[returns_cdr]=@returns_cdr,[returns_cdt]=@returns_cdt,[returns_cda]=@returns_cda,[returns_cdv]=@returns_cdv,[returns_cdc]=@returns_cdc,[returns_cdm]=@returns_cdm,[reqAdndaRev]=@reqAdndaRev,[checkdigit]=@checkdigit,[blroldname]=@blroldname WHERE [rpps_biller_id]=@old_rpps_biller_id";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20, "rpps_biller_id");
            cmd.Parameters.Add("@Mastercard_Key", SqlDbType.Int, 0, "Mastercard_Key");
            cmd.Parameters.Add("@reversal_biller_id", SqlDbType.VarChar, 20, "reversal_biller_id");
            cmd.Parameters.Add("@effdate", SqlDbType.DateTime, 0, "effdate");
            cmd.Parameters.Add("@livedate", SqlDbType.DateTime, 0, "livedate");
            cmd.Parameters.Add("@biller_aba", SqlDbType.VarChar, 20, "biller_aba");
            cmd.Parameters.Add("@biller_name", SqlDbType.VarChar, 80, "biller_name");
            cmd.Parameters.Add("@biller_class", SqlDbType.VarChar, 50, "biller_class");
            cmd.Parameters.Add("@biller_type", SqlDbType.Int, 0, "biller_type");
            cmd.Parameters.Add("@currency", SqlDbType.Int, 0, "currency");
            cmd.Parameters.Add("@payment_prenote", SqlDbType.Bit, 0, "payment_prenote");
            cmd.Parameters.Add("@proposal_prenote", SqlDbType.Bit, 0, "proposal_prenote");
            cmd.Parameters.Add("@dmppayonly", SqlDbType.Bit, 0, "dmppayonly");
            cmd.Parameters.Add("@guaranteed_funds", SqlDbType.Bit, 0, "guaranteed_funds");
            cmd.Parameters.Add("@pvt_biller_id", SqlDbType.Bit, 0, "pvt_biller_id");
            cmd.Parameters.Add("@note", SqlDbType.VarChar, 1024, "note");
            cmd.Parameters.Add("@send_cdp", SqlDbType.Bit, 0, "send_cdp");
            cmd.Parameters.Add("@send_fbc", SqlDbType.Bit, 0, "send_fbc");
            cmd.Parameters.Add("@send_fbd", SqlDbType.Bit, 0, "send_fbd");
            cmd.Parameters.Add("@send_cdv", SqlDbType.Bit, 0, "send_cdv");
            cmd.Parameters.Add("@send_cdd", SqlDbType.Bit, 0, "send_cdd");
            cmd.Parameters.Add("@send_cdn", SqlDbType.Bit, 0, "send_cdn");
            cmd.Parameters.Add("@send_cdf", SqlDbType.Bit, 0, "send_cdf");
            cmd.Parameters.Add("@send_exception_pay", SqlDbType.Bit, 0, "send_exception_pay");
            cmd.Parameters.Add("@returns_cdr", SqlDbType.Bit, 0, "returns_cdr");
            cmd.Parameters.Add("@returns_cdt", SqlDbType.Bit, 0, "returns_cdt");
            cmd.Parameters.Add("@returns_cda", SqlDbType.Bit, 0, "returns_cda");
            cmd.Parameters.Add("@returns_cdv", SqlDbType.Bit, 0, "returns_cdv");
            cmd.Parameters.Add("@returns_cdc", SqlDbType.Bit, 0, "returns_cdc");
            cmd.Parameters.Add("@returns_cdm", SqlDbType.Bit, 0, "returns_cdm");
            cmd.Parameters.Add("@reqAdndaRev", SqlDbType.Bit, 0, "reqAdndaRev");
            cmd.Parameters.Add("@checkdigit", SqlDbType.Bit, 0, "checkdigit");
            cmd.Parameters.Add("@blroldname", SqlDbType.VarChar, 1024, "blroldname");

            cmd.Parameters.Add(new System.Data.SqlClient.SqlParameter("@old_rpps_biller_id", SqlDbType.VarChar, 80, ParameterDirection.Input, false, Convert.ToByte(0), Convert.ToByte(0), "rpps_biller_id", DataRowVersion.Original, null));
            return cmd;
        }

        /// <summary>
        /// Return the delete command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Delete_Billers_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "DELETE FROM rpps_biller_ids WHERE rpps_biller_id=@rpps_biller_id;";
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            return cmd;
        }

        #endregion " rpps_biller_ids "

        #region " rpps_akas "

        /// <summary>
        /// Commit the changes to the biller table
        /// </summary>

        private void Commit_akas_Changes(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            if (ctx.ds.Tables["rpps_akas"] != null)
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();

                da.UpdateCommand = Update_akas_Command(cn, txn);
                da.DeleteCommand = Delete_akas_Command(cn, txn);
                da.InsertCommand = Insert_akas_Command(cn, txn);

                try
                {
                    System.Int32 UpdateCount = da.Update(ctx.ds.Tables["rpps_akas"]);
                    UpdateCount += 0;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_akas table");
                }
            }
        }

        /// <summary>
        /// Return the insert command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Insert_akas_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "xpr_insert_rpps_aka";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "rpps_aka").Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 80, "name");
            return cmd;
        }

        /// <summary>
        /// Return the update command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Update_akas_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "UPDATE rpps_akas SET name=@name, rpps_biller_id=@rpps_biller_id WHERE rpps_aka=@rpps_aka";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 80, "name");
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@rpps_aka", SqlDbType.Int, 0, "rpps_aka");
            return cmd;
        }

        /// <summary>
        /// Return the delete command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Delete_akas_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "DELETE FROM rpps_akas WHERE rpps_aka=@rpps_aka";
            cmd.Parameters.Add("@rpps_aka", SqlDbType.Int, 0, "rpps_aka");
            return cmd;
        }

        #endregion " rpps_akas "

        #region " rpps_lobs "

        /// <summary>
        /// Commit the changes to the biller table
        /// </summary>

        private void Commit_lobs_Changes(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            if (ctx.ds.Tables["rpps_lobs"] != null)
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();

                da.UpdateCommand = Update_lobs_Command(cn, txn);
                da.DeleteCommand = Delete_lobs_Command(cn, txn);
                da.InsertCommand = Insert_lobs_Command(cn, txn);

                try
                {
                    da.Update(ctx.ds.Tables["rpps_lobs"]);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_lobs table");
                }
            }
        }

        /// <summary>
        /// Return the insert command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Insert_lobs_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "xpr_insert_rpps_lob";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "rpps_lob").Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@biller_lob", SqlDbType.VarChar, 80, "biller_lob");
            return cmd;
        }

        /// <summary>
        /// Return the update command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Update_lobs_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "UPDATE rpps_lobs SET biller_lob=@biller_lob, rpps_biller_id=@rpps_biller_id WHERE rpps_biller_lob=@rpps_biller_lob";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@biller_lob", SqlDbType.VarChar, 80, "biller_lob");
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@rpps_biller_lob", SqlDbType.Int, 0, "rpps_biller_lob");
            return cmd;
        }

        /// <summary>
        /// Return the delete command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Delete_lobs_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "DELETE FROM rpps_lobs WHERE rpps_biller_lob=@rpps_biller_lob";
            cmd.Parameters.Add("@rpps_biller_lob", SqlDbType.Int, 0, "rpps_biller_lob");
            return cmd;
        }

        #endregion " rpps_lobs "

        #region " rpps_masks "

        /// <summary>
        /// Commit the changes to the biller table
        /// </summary>

        private void Commit_masks_Changes(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            if (ctx.ds.Tables["rpps_masks"] != null)
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();

                da.UpdateCommand = Update_masks_Command(cn, txn);
                da.DeleteCommand = Delete_masks_Command(cn, txn);
                da.InsertCommand = Insert_masks_Command(cn, txn);

                try
                {
                    System.Int32 UpdateCount = da.Update(ctx.ds.Tables["rpps_masks"]);
                    UpdateCount += 0;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_masks table");
                }
            }
        }

        /// <summary>
        /// Return the insert command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Insert_masks_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "xpr_insert_rpps_mask";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "rpps_mask").Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@length", SqlDbType.Int, 0, "length");
            cmd.Parameters.Add("@mask", SqlDbType.VarChar, 80, "mask");
            cmd.Parameters.Add("@ExceptionMask", SqlDbType.Bit, 0, "ExceptionMask");
            cmd.Parameters.Add("@CheckDigit", SqlDbType.Bit, 0, "CheckDigit");
            cmd.Parameters.Add("@description", SqlDbType.VarChar, 80, "description");
            return cmd;
        }

        /// <summary>
        /// Return the update command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Update_masks_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "UPDATE rpps_masks SET [length]=@length, [mask]=@mask, [description]=@description, [rpps_biller_id]=@rpps_biller_id, [ExceptionMask]=@ExceptionMask, [CheckDigit]=@CheckDigit WHERE [rpps_mask]=@rpps_mask";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@length", SqlDbType.Int, 0, "length");
            cmd.Parameters.Add("@mask", SqlDbType.VarChar, 80, "mask");
            cmd.Parameters.Add("@description", SqlDbType.VarChar, 80, "description");
            cmd.Parameters.Add("@ExceptionMask", SqlDbType.Bit, 0, "ExceptionMask");
            cmd.Parameters.Add("@CheckDigit", SqlDbType.Bit, 0, "CheckDigit");
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@rpps_mask", SqlDbType.Int, 0, "rpps_mask");
            return cmd;
        }

        /// <summary>
        /// Return the delete command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Delete_masks_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "DELETE FROM rpps_masks WHERE rpps_mask=@rpps_mask";
            cmd.Parameters.Add("@rpps_mask", SqlDbType.Int, 0, "rpps_mask");
            return cmd;
        }

        #endregion " rpps_masks "

        #region " rpps_addresses "

        /// <summary>
        /// Commit the changes to the biller table
        /// </summary>

        private void Commit_addresses_Changes(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            if (ctx.ds.Tables["rpps_addresses"] != null)
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();

                da.UpdateCommand = Update_addresses_Command(cn, txn);
                da.DeleteCommand = Delete_addresses_Command(cn, txn);
                da.InsertCommand = Insert_addresses_Command(cn, txn);

                try
                {
                    da.Update(ctx.ds.Tables["rpps_addresses"]);
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating rpps_addresses table");
                }
            }
        }

        /// <summary>
        /// Return the insert command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Insert_addresses_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "INSERT INTO rpps_addresses (rpps_biller_id, biller_addr1, biller_addr2, biller_city, biller_state, biller_zipcode, biller_country) VALUES (@rpps_biller_id, @biller_addr1, @biller_addr2, @biller_city, @biller_state, @biller_zipcode, @biller_country);";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@biller_addr1", SqlDbType.VarChar, 80, "biller_addr1");
            cmd.Parameters.Add("@biller_addr2", SqlDbType.VarChar, 80, "biller_addr2");
            cmd.Parameters.Add("@biller_city", SqlDbType.VarChar, 80, "biller_city");
            cmd.Parameters.Add("@biller_state", SqlDbType.VarChar, 80, "biller_state");
            cmd.Parameters.Add("@biller_zipcode", SqlDbType.VarChar, 80, "biller_zipcode");
            cmd.Parameters.Add("@biller_country", SqlDbType.VarChar, 80, "biller_country");
            return cmd;
        }

        /// <summary>
        /// Return the update command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Update_addresses_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "UPDATE rpps_addresses SET biller_addr1=@biller_addr1, biller_addr2=@biller_addr2, biller_city=@biller_city, biller_state=@biller_state, biller_zipcode=@biller_zipcode, biller_country=@biller_country, rpps_biller_id=@rpps_biller_id WHERE rpps_biller_address=@rpps_biller_address";
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add("@biller_addr1", SqlDbType.VarChar, 80, "biller_addr1");
            cmd.Parameters.Add("@biller_addr2", SqlDbType.VarChar, 80, "biller_addr2");
            cmd.Parameters.Add("@biller_city", SqlDbType.VarChar, 80, "biller_city");
            cmd.Parameters.Add("@biller_state", SqlDbType.VarChar, 80, "biller_state");
            cmd.Parameters.Add("@biller_zipcode", SqlDbType.VarChar, 80, "biller_zipcode");
            cmd.Parameters.Add("@biller_country", SqlDbType.VarChar, 80, "biller_country");
            cmd.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 80, "rpps_biller_id");
            cmd.Parameters.Add("@rpps_biller_address", SqlDbType.Int, 0, "rpps_biller_address");
            return cmd;
        }

        /// <summary>
        /// Return the delete command block
        /// </summary>
        protected System.Data.SqlClient.SqlCommand Delete_addresses_Command(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.Transaction = txn;
            cmd.CommandText = "DELETE FROM rpps_addresses WHERE rpps_biller_address=@rpps_biller_address";
            cmd.Parameters.Add("@rpps_biller_address", SqlDbType.Int, 0, "rpps_biller_address");
            return cmd;
        }

        #endregion " rpps_addresses "

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string BasePath = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus";
            return System.IO.Path.Combine(BasePath, "Tables.RPPS.Billers.MainForm");
        }

        /// <summary>
        /// When loading the control, restore the layout from the file
        /// </summary>
        private void MyGridControl_Load(object sender, System.EventArgs e)
        {
            string PathName = XMLBasePath();
            string FileName = System.IO.Path.Combine(PathName, GridView1.Name + ".Grid.xml");

            try
            {
                if (System.IO.File.Exists(FileName))
                {
                    GridView1.RestoreLayoutFromXml(FileName);
                }
            }
#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex)
            {
            }
            catch (System.IO.FileNotFoundException ex)
            {
            }
#pragma warning restore 168

            // Hook into the changes of the layout from this point
            GridView1.Layout += LayoutChanged;
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, System.EventArgs e)
        {
            string PathName = XMLBasePath();
            if (!System.IO.Directory.Exists(PathName))
            {
                System.IO.Directory.CreateDirectory(PathName);
            }

            string FileName = System.IO.Path.Combine(PathName, GridView1.Name + ".Grid.xml");
            GridView1.SaveLayoutToXml(FileName);
        }

        /// <summary>
        /// Do not allow the sub-form to ask for saving changes
        /// </summary>
        protected override void MainTemplateForm_Closing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            CommitChanges();
            // Just save the changes.
            base.MainTemplateForm_Closing(sender, e);
        }
    }
}