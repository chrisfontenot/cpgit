using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.ADDRESS;
using DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.AKA;
using DebtPlus.UI.Common.Templates;
using DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.LOB;
using DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.MASK;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.BILLER
{

	partial class EditForm : DebtPlus.UI.Common.Templates.ADO.Net.EditTemplateForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LobGridControl1 = new LOBGridControl();
			this.AkaGridControl1 = new AKAGridControl();
			this.AddressGridControl1 = new AddressGridControl();
			this.MaskGridControl1 = new MaskGridControl();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.CheckEdit_send_cdf = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_send_cdn = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_checkdigit = new DevExpress.XtraEditors.CheckEdit();
			this.DateEdit_eff_date = new DevExpress.XtraEditors.DateEdit();
			this.CheckEdit_payment_prenote = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_send_cdv = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_dmppayonly = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_proposal_prenote = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_send_fbd = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_send_cdp = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_send_cdd = new DevExpress.XtraEditors.CheckEdit();
			this.XtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
			this.XtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
			this.XtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
			this.XtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
			this.XtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
			this.MemoEdit_note = new DevExpress.XtraEditors.MemoEdit();
			this.LookUpEdit_biller_type = new DevExpress.XtraEditors.LookUpEdit();
			this.TextEdit_biller_class = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_biller_name = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_reversal_biller_id = new DevExpress.XtraEditors.TextEdit();
			this.CheckEdit_send_fbc = new DevExpress.XtraEditors.CheckEdit();
			this.TextEdit_rpps_biller_id = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit_biller_aba = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_send_exception_pay = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_returns_CDR = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_reqAdndaRev = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_returns_cdm = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_returns_cdv = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_returns_cda = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_returns_cdt = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.TextEdit_blroldname = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
			this.CheckEdit_guaranteed_funds = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
			this.TextEdit_currency = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
			this.DateEdit_live_date = new DevExpress.XtraEditors.DateEdit();
			this.LayoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdf.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdn.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_checkdigit.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_eff_date.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_eff_date.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_payment_prenote.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdv.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_dmppayonly.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_proposal_prenote.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_fbd.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdp.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdd.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).BeginInit();
			this.XtraTabControl1.SuspendLayout();
			this.XtraTabPage1.SuspendLayout();
			this.XtraTabPage2.SuspendLayout();
			this.XtraTabPage3.SuspendLayout();
			this.XtraTabPage4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.MemoEdit_note.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_biller_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_biller_class.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_biller_name.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_reversal_biller_id.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_fbc.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_rpps_biller_id.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_biller_aba.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem15).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem16).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem17).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem18).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem19).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem20).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_exception_pay.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem21).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_CDR.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem22).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_reqAdndaRev.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem23).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cdm.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem24).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cdv.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem25).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cda.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem26).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cdt.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem27).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_blroldname.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem28).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_guaranteed_funds.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem29).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_currency.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem30).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_live_date.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_live_date.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem31).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_OK.Location = new System.Drawing.Point(156, 442);
			this.SimpleButton_OK.TabIndex = 14;
			//
			//SimpleButton_Cancel
			//
			this.SimpleButton_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton_Cancel.Location = new System.Drawing.Point(258, 442);
			this.SimpleButton_Cancel.TabIndex = 15;
			//
			//LobGridControl1
			//
			this.LobGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LobGridControl1.Location = new System.Drawing.Point(0, 0);
			this.LobGridControl1.Name = "LobGridControl1";
			this.LobGridControl1.Size = new System.Drawing.Size(432, 108);
			this.LobGridControl1.TabIndex = 16;
			//
			//AkaGridControl1
			//
			this.AkaGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AkaGridControl1.Location = new System.Drawing.Point(0, 0);
			this.AkaGridControl1.Name = "AkaGridControl1";
			this.AkaGridControl1.Size = new System.Drawing.Size(432, 118);
			this.AkaGridControl1.TabIndex = 17;
			//
			//AddressGridControl1
			//
			this.AddressGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.AddressGridControl1.Location = new System.Drawing.Point(0, 0);
			this.AddressGridControl1.Name = "AddressGridControl1";
			this.AddressGridControl1.Size = new System.Drawing.Size(432, 108);
			this.AddressGridControl1.TabIndex = 18;
			//
			//MaskGridControl1
			//
			this.MaskGridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.MaskGridControl1.Location = new System.Drawing.Point(0, 0);
			this.MaskGridControl1.Name = "MaskGridControl1";
			this.MaskGridControl1.Size = new System.Drawing.Size(432, 108);
			this.MaskGridControl1.TabIndex = 19;
			//
			//LayoutControl1
			//
			this.LayoutControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LayoutControl1.Appearance.Control.Options.UseTextOptions = true;
			this.LayoutControl1.Appearance.Control.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LayoutControl1.Appearance.Control.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LayoutControl1.Controls.Add(this.DateEdit_live_date);
			this.LayoutControl1.Controls.Add(this.TextEdit_currency);
			this.LayoutControl1.Controls.Add(this.CheckEdit_guaranteed_funds);
			this.LayoutControl1.Controls.Add(this.TextEdit_blroldname);
			this.LayoutControl1.Controls.Add(this.CheckEdit_returns_cdt);
			this.LayoutControl1.Controls.Add(this.CheckEdit_returns_cda);
			this.LayoutControl1.Controls.Add(this.CheckEdit_returns_cdv);
			this.LayoutControl1.Controls.Add(this.CheckEdit_returns_cdm);
			this.LayoutControl1.Controls.Add(this.CheckEdit_returns_CDR);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_exception_pay);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_cdf);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_cdn);
			this.LayoutControl1.Controls.Add(this.CheckEdit_reqAdndaRev);
			this.LayoutControl1.Controls.Add(this.CheckEdit_checkdigit);
			this.LayoutControl1.Controls.Add(this.DateEdit_eff_date);
			this.LayoutControl1.Controls.Add(this.CheckEdit_payment_prenote);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_cdv);
			this.LayoutControl1.Controls.Add(this.CheckEdit_dmppayonly);
			this.LayoutControl1.Controls.Add(this.CheckEdit_proposal_prenote);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_fbd);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_cdp);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_cdd);
			this.LayoutControl1.Controls.Add(this.XtraTabControl1);
			this.LayoutControl1.Controls.Add(this.MemoEdit_note);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_biller_type);
			this.LayoutControl1.Controls.Add(this.TextEdit_biller_class);
			this.LayoutControl1.Controls.Add(this.TextEdit_biller_name);
			this.LayoutControl1.Controls.Add(this.TextEdit_reversal_biller_id);
			this.LayoutControl1.Controls.Add(this.CheckEdit_send_fbc);
			this.LayoutControl1.Controls.Add(this.TextEdit_rpps_biller_id);
			this.LayoutControl1.Controls.Add(this.TextEdit_biller_aba);
			this.LayoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem21,
				this.LayoutControlItem24,
				this.LayoutControlItem25,
				this.LayoutControlItem26,
				this.LayoutControlItem23,
				this.LayoutControlItem27,
				this.LayoutControlItem22,
				this.LayoutControlItem28,
				this.LayoutControlItem29,
				this.LayoutControlItem30,
				this.LayoutControlItem31
			});
			this.LayoutControl1.Location = new System.Drawing.Point(12, 12);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.OptionsCustomizationForm.ShowPropertyGrid = true;
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(463, 416);
			this.LayoutControl1.TabIndex = 20;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//CheckEdit_send_cdf
			//
			this.CheckEdit_send_cdf.Location = new System.Drawing.Point(200, 362);
			this.CheckEdit_send_cdf.Name = "CheckEdit_send_cdf";
			this.CheckEdit_send_cdf.Properties.Caption = "Send CDF";
			this.CheckEdit_send_cdf.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_send_cdf.StyleController = this.LayoutControl1;
			this.CheckEdit_send_cdf.TabIndex = 32;
			//
			//CheckEdit_send_cdn
			//
			this.CheckEdit_send_cdn.Location = new System.Drawing.Point(106, 385);
			this.CheckEdit_send_cdn.Name = "CheckEdit_send_cdn";
			this.CheckEdit_send_cdn.Properties.Caption = "Send CDN";
			this.CheckEdit_send_cdn.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_send_cdn.StyleController = this.LayoutControl1;
			this.CheckEdit_send_cdn.TabIndex = 31;
			//
			//CheckEdit_checkdigit
			//
			this.CheckEdit_checkdigit.Location = new System.Drawing.Point(12, 385);
			this.CheckEdit_checkdigit.Name = "CheckEdit_checkdigit";
			this.CheckEdit_checkdigit.Properties.Caption = "Checkdigit";
			this.CheckEdit_checkdigit.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_checkdigit.StyleController = this.LayoutControl1;
			this.CheckEdit_checkdigit.TabIndex = 30;
			//
			//DateEdit_eff_date
			//
			this.DateEdit_eff_date.EditValue = null;
			this.DateEdit_eff_date.Location = new System.Drawing.Point(312, 36);
			this.DateEdit_eff_date.Name = "DateEdit_eff_date";
			this.DateEdit_eff_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit_eff_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit_eff_date.Size = new System.Drawing.Size(139, 20);
			this.DateEdit_eff_date.StyleController = this.LayoutControl1;
			this.DateEdit_eff_date.TabIndex = 29;
			//
			//CheckEdit_payment_prenote
			//
			this.CheckEdit_payment_prenote.Location = new System.Drawing.Point(294, 362);
			this.CheckEdit_payment_prenote.Name = "CheckEdit_payment_prenote";
			this.CheckEdit_payment_prenote.Properties.Caption = "Paymt Prenote";
			this.CheckEdit_payment_prenote.Size = new System.Drawing.Size(157, 19);
			this.CheckEdit_payment_prenote.StyleController = this.LayoutControl1;
			this.CheckEdit_payment_prenote.TabIndex = 28;
			//
			//CheckEdit_send_cdv
			//
			this.CheckEdit_send_cdv.Location = new System.Drawing.Point(106, 362);
			this.CheckEdit_send_cdv.Name = "CheckEdit_send_cdv";
			this.CheckEdit_send_cdv.Properties.Caption = "Send CDV";
			this.CheckEdit_send_cdv.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_send_cdv.StyleController = this.LayoutControl1;
			this.CheckEdit_send_cdv.TabIndex = 27;
			//
			//CheckEdit_dmppayonly
			//
			this.CheckEdit_dmppayonly.Location = new System.Drawing.Point(294, 385);
			this.CheckEdit_dmppayonly.Name = "CheckEdit_dmppayonly";
			this.CheckEdit_dmppayonly.Properties.Caption = "DMP Only";
			this.CheckEdit_dmppayonly.Size = new System.Drawing.Size(157, 19);
			this.CheckEdit_dmppayonly.StyleController = this.LayoutControl1;
			this.CheckEdit_dmppayonly.TabIndex = 26;
			//
			//CheckEdit_proposal_prenote
			//
			this.CheckEdit_proposal_prenote.Location = new System.Drawing.Point(294, 339);
			this.CheckEdit_proposal_prenote.Name = "CheckEdit_proposal_prenote";
			this.CheckEdit_proposal_prenote.Properties.Caption = "Propsl Prenote";
			this.CheckEdit_proposal_prenote.Size = new System.Drawing.Size(157, 19);
			this.CheckEdit_proposal_prenote.StyleController = this.LayoutControl1;
			this.CheckEdit_proposal_prenote.TabIndex = 25;
			//
			//CheckEdit_send_fbd
			//
			this.CheckEdit_send_fbd.Location = new System.Drawing.Point(106, 339);
			this.CheckEdit_send_fbd.Name = "CheckEdit_send_fbd";
			this.CheckEdit_send_fbd.Properties.Caption = "Send FBD";
			this.CheckEdit_send_fbd.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_send_fbd.StyleController = this.LayoutControl1;
			this.CheckEdit_send_fbd.TabIndex = 24;
			//
			//CheckEdit_send_cdp
			//
			this.CheckEdit_send_cdp.Location = new System.Drawing.Point(12, 339);
			this.CheckEdit_send_cdp.Name = "CheckEdit_send_cdp";
			this.CheckEdit_send_cdp.Properties.Caption = "Send CDP";
			this.CheckEdit_send_cdp.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_send_cdp.StyleController = this.LayoutControl1;
			this.CheckEdit_send_cdp.TabIndex = 22;
			//
			//CheckEdit_send_cdd
			//
			this.CheckEdit_send_cdd.Location = new System.Drawing.Point(200, 339);
			this.CheckEdit_send_cdd.Name = "CheckEdit_send_cdd";
			this.CheckEdit_send_cdd.Properties.Caption = "Send CDD";
			this.CheckEdit_send_cdd.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_send_cdd.StyleController = this.LayoutControl1;
			this.CheckEdit_send_cdd.TabIndex = 4;
			//
			//XtraTabControl1
			//
			this.XtraTabControl1.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
			this.XtraTabControl1.Location = new System.Drawing.Point(12, 188);
			this.XtraTabControl1.Name = "XtraTabControl1";
			this.XtraTabControl1.SelectedTabPage = this.XtraTabPage1;
			this.XtraTabControl1.Size = new System.Drawing.Size(439, 147);
			this.XtraTabControl1.TabIndex = 21;
			this.XtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
				this.XtraTabPage1,
				this.XtraTabPage2,
				this.XtraTabPage3,
				this.XtraTabPage4
			});
			//
			//XtraTabPage1
			//
			this.XtraTabPage1.Controls.Add(this.AkaGridControl1);
			this.XtraTabPage1.Name = "XtraTabPage1";
			this.XtraTabPage1.Size = new System.Drawing.Size(432, 118);
			this.XtraTabPage1.Text = "Aliases";
			//
			//XtraTabPage2
			//
			this.XtraTabPage2.Controls.Add(this.MaskGridControl1);
			this.XtraTabPage2.Name = "XtraTabPage2";
			this.XtraTabPage2.Size = new System.Drawing.Size(432, 108);
			this.XtraTabPage2.Text = "Masks";
			//
			//XtraTabPage3
			//
			this.XtraTabPage3.Controls.Add(this.LobGridControl1);
			this.XtraTabPage3.Name = "XtraTabPage3";
			this.XtraTabPage3.Size = new System.Drawing.Size(432, 108);
			this.XtraTabPage3.Text = "Lines of Business";
			//
			//XtraTabPage4
			//
			this.XtraTabPage4.Controls.Add(this.AddressGridControl1);
			this.XtraTabPage4.Name = "XtraTabPage4";
			this.XtraTabPage4.Size = new System.Drawing.Size(432, 108);
			this.XtraTabPage4.Text = "Addresses";
			//
			//MemoEdit_note
			//
			this.MemoEdit_note.Location = new System.Drawing.Point(97, 132);
			this.MemoEdit_note.Name = "MemoEdit_note";
			this.MemoEdit_note.Properties.MaxLength = 1024;
			this.MemoEdit_note.Size = new System.Drawing.Size(354, 52);
			this.MemoEdit_note.StyleController = this.LayoutControl1;
			this.MemoEdit_note.TabIndex = 20;
			//
			//LookUpEdit_biller_type
			//
			this.LookUpEdit_biller_type.Location = new System.Drawing.Point(97, 108);
			this.LookUpEdit_biller_type.Name = "LookUpEdit_biller_type";
			this.LookUpEdit_biller_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_biller_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_biller_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("biller_type", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_biller_type.Properties.DisplayMember = "description";
			this.LookUpEdit_biller_type.Properties.NullText = "";
			this.LookUpEdit_biller_type.Properties.ShowFooter = false;
			this.LookUpEdit_biller_type.Properties.ShowHeader = false;
			this.LookUpEdit_biller_type.Properties.ValueMember = "biller_type";
			this.LookUpEdit_biller_type.Size = new System.Drawing.Size(354, 20);
			this.LookUpEdit_biller_type.StyleController = this.LayoutControl1;
			this.LookUpEdit_biller_type.TabIndex = 9;
			this.LookUpEdit_biller_type.Properties.SortColumnIndex = 1;
			//
			//TextEdit_biller_class
			//
			this.TextEdit_biller_class.Location = new System.Drawing.Point(97, 84);
			this.TextEdit_biller_class.Name = "TextEdit_biller_class";
			this.TextEdit_biller_class.Properties.MaxLength = 50;
			this.TextEdit_biller_class.Size = new System.Drawing.Size(354, 20);
			this.TextEdit_biller_class.StyleController = this.LayoutControl1;
			this.TextEdit_biller_class.TabIndex = 8;
			//
			//TextEdit_biller_name
			//
			this.TextEdit_biller_name.Location = new System.Drawing.Point(97, 60);
			this.TextEdit_biller_name.Name = "TextEdit_biller_name";
			this.TextEdit_biller_name.Properties.MaxLength = 16;
			this.TextEdit_biller_name.Size = new System.Drawing.Size(354, 20);
			this.TextEdit_biller_name.StyleController = this.LayoutControl1;
			this.TextEdit_biller_name.TabIndex = 7;
			//
			//TextEdit_reversal_biller_id
			//
			this.TextEdit_reversal_biller_id.Location = new System.Drawing.Point(97, 36);
			this.TextEdit_reversal_biller_id.Name = "TextEdit_reversal_biller_id";
			this.TextEdit_reversal_biller_id.Properties.Mask.BeepOnError = true;
			this.TextEdit_reversal_biller_id.Properties.Mask.EditMask = "\\d{10}";
			this.TextEdit_reversal_biller_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_reversal_biller_id.Properties.MaxLength = 10;
			this.TextEdit_reversal_biller_id.Size = new System.Drawing.Size(126, 20);
			this.TextEdit_reversal_biller_id.StyleController = this.LayoutControl1;
			this.TextEdit_reversal_biller_id.TabIndex = 6;
			//
			//CheckEdit_send_fbc
			//
			this.CheckEdit_send_fbc.Location = new System.Drawing.Point(12, 362);
			this.CheckEdit_send_fbc.Name = "CheckEdit_send_fbc";
			this.CheckEdit_send_fbc.Properties.Caption = "Send FBC";
			this.CheckEdit_send_fbc.Size = new System.Drawing.Size(90, 19);
			this.CheckEdit_send_fbc.StyleController = this.LayoutControl1;
			this.CheckEdit_send_fbc.TabIndex = 23;
			//
			//TextEdit_rpps_biller_id
			//
			this.TextEdit_rpps_biller_id.Location = new System.Drawing.Point(97, 12);
			this.TextEdit_rpps_biller_id.Name = "TextEdit_rpps_biller_id";
			this.TextEdit_rpps_biller_id.Properties.Mask.BeepOnError = true;
			this.TextEdit_rpps_biller_id.Properties.Mask.EditMask = "\\d{10}";
			this.TextEdit_rpps_biller_id.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_rpps_biller_id.Properties.MaxLength = 10;
			this.TextEdit_rpps_biller_id.Size = new System.Drawing.Size(126, 20);
			this.TextEdit_rpps_biller_id.StyleController = this.LayoutControl1;
			this.TextEdit_rpps_biller_id.TabIndex = 4;
			//
			//TextEdit_biller_aba
			//
			this.TextEdit_biller_aba.Location = new System.Drawing.Point(312, 12);
			this.TextEdit_biller_aba.Name = "TextEdit_biller_aba";
			this.TextEdit_biller_aba.Properties.Mask.BeepOnError = true;
			this.TextEdit_biller_aba.Properties.Mask.EditMask = "\\d{10}";
			this.TextEdit_biller_aba.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit_biller_aba.Properties.MaxLength = 10;
			this.TextEdit_biller_aba.Size = new System.Drawing.Size(139, 20);
			this.TextEdit_biller_aba.StyleController = this.LayoutControl1;
			this.TextEdit_biller_aba.TabIndex = 5;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem2,
				this.LayoutControlItem5,
				this.LayoutControlItem6,
				this.LayoutControlItem9,
				this.LayoutControlItem12,
				this.LayoutControlItem7,
				this.LayoutControlItem8,
				this.LayoutControlItem10,
				this.LayoutControlItem11,
				this.LayoutControlItem13,
				this.LayoutControlItem15,
				this.LayoutControlItem16,
				this.LayoutControlItem17,
				this.LayoutControlItem18,
				this.LayoutControlItem19,
				this.LayoutControlItem14,
				this.LayoutControlItem20,
				this.EmptySpaceItem1
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(463, 416);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.TextEdit_rpps_biller_id;
			this.LayoutControlItem1.CustomizationFormText = "Biller ID";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(215, 24);
			this.LayoutControlItem1.Text = "Biller ID";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.TextEdit_reversal_biller_id;
			this.LayoutControlItem3.CustomizationFormText = "Reversal Biller ID";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(215, 24);
			this.LayoutControlItem3.Text = "Reversal Biller ID";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.TextEdit_biller_name;
			this.LayoutControlItem4.CustomizationFormText = "Biller Name";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(443, 24);
			this.LayoutControlItem4.Text = "Biller Name";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.TextEdit_biller_aba;
			this.LayoutControlItem2.CustomizationFormText = "Biller ABA";
			this.LayoutControlItem2.Location = new System.Drawing.Point(215, 0);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(228, 24);
			this.LayoutControlItem2.Text = "Biller ABA";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.TextEdit_biller_class;
			this.LayoutControlItem5.CustomizationFormText = "Biller Class";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(443, 24);
			this.LayoutControlItem5.Text = "Biller Class";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.LookUpEdit_biller_type;
			this.LayoutControlItem6.CustomizationFormText = "Biller Type";
			this.LayoutControlItem6.Location = new System.Drawing.Point(0, 96);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(443, 24);
			this.LayoutControlItem6.Text = "Biller Type";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem9
			//
			this.LayoutControlItem9.AppearanceItemCaption.Options.UseTextOptions = true;
			this.LayoutControlItem9.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LayoutControlItem9.Control = this.MemoEdit_note;
			this.LayoutControlItem9.CustomizationFormText = "Note";
			this.LayoutControlItem9.Location = new System.Drawing.Point(0, 120);
			this.LayoutControlItem9.Name = "LayoutControlItem9";
			this.LayoutControlItem9.Size = new System.Drawing.Size(443, 56);
			this.LayoutControlItem9.Text = "Note";
			this.LayoutControlItem9.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem12
			//
			this.LayoutControlItem12.Control = this.XtraTabControl1;
			this.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12";
			this.LayoutControlItem12.Location = new System.Drawing.Point(0, 176);
			this.LayoutControlItem12.Name = "LayoutControlItem12";
			this.LayoutControlItem12.Size = new System.Drawing.Size(443, 151);
			this.LayoutControlItem12.Text = "LayoutControlItem12";
			this.LayoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem12.TextToControlDistance = 0;
			this.LayoutControlItem12.TextVisible = false;
			//
			//LayoutControlItem7
			//
			this.LayoutControlItem7.Control = this.CheckEdit_send_cdd;
			this.LayoutControlItem7.CustomizationFormText = "LayoutControlItem7";
			this.LayoutControlItem7.Location = new System.Drawing.Point(188, 327);
			this.LayoutControlItem7.Name = "LayoutControlItem7";
			this.LayoutControlItem7.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem7.Text = "LayoutControlItem7";
			this.LayoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem7.TextToControlDistance = 0;
			this.LayoutControlItem7.TextVisible = false;
			//
			//LayoutControlItem8
			//
			this.LayoutControlItem8.Control = this.CheckEdit_send_cdp;
			this.LayoutControlItem8.CustomizationFormText = "LayoutControlItem8";
			this.LayoutControlItem8.Location = new System.Drawing.Point(0, 327);
			this.LayoutControlItem8.Name = "LayoutControlItem8";
			this.LayoutControlItem8.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem8.Text = "LayoutControlItem8";
			this.LayoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem8.TextToControlDistance = 0;
			this.LayoutControlItem8.TextVisible = false;
			//
			//LayoutControlItem10
			//
			this.LayoutControlItem10.Control = this.CheckEdit_send_fbc;
			this.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10";
			this.LayoutControlItem10.Location = new System.Drawing.Point(0, 350);
			this.LayoutControlItem10.Name = "LayoutControlItem10";
			this.LayoutControlItem10.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem10.Text = "LayoutControlItem10";
			this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem10.TextToControlDistance = 0;
			this.LayoutControlItem10.TextVisible = false;
			//
			//LayoutControlItem11
			//
			this.LayoutControlItem11.Control = this.CheckEdit_send_fbd;
			this.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11";
			this.LayoutControlItem11.Location = new System.Drawing.Point(94, 327);
			this.LayoutControlItem11.Name = "LayoutControlItem11";
			this.LayoutControlItem11.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem11.Text = "LayoutControlItem11";
			this.LayoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem11.TextToControlDistance = 0;
			this.LayoutControlItem11.TextVisible = false;
			//
			//LayoutControlItem13
			//
			this.LayoutControlItem13.Control = this.CheckEdit_proposal_prenote;
			this.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13";
			this.LayoutControlItem13.Location = new System.Drawing.Point(282, 327);
			this.LayoutControlItem13.Name = "LayoutControlItem13";
			this.LayoutControlItem13.Size = new System.Drawing.Size(161, 23);
			this.LayoutControlItem13.Text = "LayoutControlItem13";
			this.LayoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem13.TextToControlDistance = 0;
			this.LayoutControlItem13.TextVisible = false;
			//
			//LayoutControlItem14
			//
			this.LayoutControlItem14.Control = this.CheckEdit_dmppayonly;
			this.LayoutControlItem14.CustomizationFormText = "LayoutControlItem14";
			this.LayoutControlItem14.Location = new System.Drawing.Point(282, 373);
			this.LayoutControlItem14.Name = "LayoutControlItem14";
			this.LayoutControlItem14.Size = new System.Drawing.Size(161, 23);
			this.LayoutControlItem14.Text = "LayoutControlItem14";
			this.LayoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem14.TextToControlDistance = 0;
			this.LayoutControlItem14.TextVisible = false;
			//
			//LayoutControlItem15
			//
			this.LayoutControlItem15.Control = this.CheckEdit_send_cdv;
			this.LayoutControlItem15.CustomizationFormText = "LayoutControlItem15";
			this.LayoutControlItem15.Location = new System.Drawing.Point(94, 350);
			this.LayoutControlItem15.Name = "LayoutControlItem15";
			this.LayoutControlItem15.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem15.Text = "LayoutControlItem15";
			this.LayoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem15.TextToControlDistance = 0;
			this.LayoutControlItem15.TextVisible = false;
			//
			//LayoutControlItem16
			//
			this.LayoutControlItem16.Control = this.CheckEdit_payment_prenote;
			this.LayoutControlItem16.CustomizationFormText = "LayoutControlItem16";
			this.LayoutControlItem16.Location = new System.Drawing.Point(282, 350);
			this.LayoutControlItem16.Name = "LayoutControlItem16";
			this.LayoutControlItem16.Size = new System.Drawing.Size(161, 23);
			this.LayoutControlItem16.Text = "LayoutControlItem16";
			this.LayoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem16.TextToControlDistance = 0;
			this.LayoutControlItem16.TextVisible = false;
			//
			//LayoutControlItem17
			//
			this.LayoutControlItem17.Control = this.DateEdit_eff_date;
			this.LayoutControlItem17.CustomizationFormText = "Effective Date";
			this.LayoutControlItem17.Location = new System.Drawing.Point(215, 24);
			this.LayoutControlItem17.Name = "LayoutControlItem17";
			this.LayoutControlItem17.Size = new System.Drawing.Size(228, 24);
			this.LayoutControlItem17.Text = "Effective";
			this.LayoutControlItem17.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem18
			//
			this.LayoutControlItem18.Control = this.CheckEdit_checkdigit;
			this.LayoutControlItem18.CustomizationFormText = "LayoutControlItem18";
			this.LayoutControlItem18.Location = new System.Drawing.Point(0, 373);
			this.LayoutControlItem18.Name = "LayoutControlItem18";
			this.LayoutControlItem18.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem18.Text = "LayoutControlItem18";
			this.LayoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem18.TextToControlDistance = 0;
			this.LayoutControlItem18.TextVisible = false;
			this.LayoutControlItem18.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
			//
			//LayoutControlItem19
			//
			this.LayoutControlItem19.Control = this.CheckEdit_send_cdn;
			this.LayoutControlItem19.CustomizationFormText = "LayoutControlItem19";
			this.LayoutControlItem19.Location = new System.Drawing.Point(94, 373);
			this.LayoutControlItem19.Name = "LayoutControlItem19";
			this.LayoutControlItem19.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem19.Text = "LayoutControlItem19";
			this.LayoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem19.TextToControlDistance = 0;
			this.LayoutControlItem19.TextVisible = false;
			//
			//LayoutControlItem20
			//
			this.LayoutControlItem20.Control = this.CheckEdit_send_cdf;
			this.LayoutControlItem20.CustomizationFormText = "LayoutControlItem20";
			this.LayoutControlItem20.Location = new System.Drawing.Point(188, 350);
			this.LayoutControlItem20.Name = "LayoutControlItem20";
			this.LayoutControlItem20.Size = new System.Drawing.Size(94, 23);
			this.LayoutControlItem20.Text = "LayoutControlItem20";
			this.LayoutControlItem20.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem20.TextToControlDistance = 0;
			this.LayoutControlItem20.TextVisible = false;
			//
			//CheckEdit_send_exception_pay
			//
			this.CheckEdit_send_exception_pay.Location = new System.Drawing.Point(257, 345);
			this.CheckEdit_send_exception_pay.Name = "CheckEdit_send_exception_pay";
			this.CheckEdit_send_exception_pay.Properties.Caption = "Exception Pay";
			this.CheckEdit_send_exception_pay.Size = new System.Drawing.Size(104, 19);
			this.CheckEdit_send_exception_pay.StyleController = this.LayoutControl1;
			this.CheckEdit_send_exception_pay.TabIndex = 33;
			//
			//LayoutControlItem21
			//
			this.LayoutControlItem21.Control = this.CheckEdit_send_exception_pay;
			this.LayoutControlItem21.CustomizationFormText = "Send Exception Pay";
			this.LayoutControlItem21.Location = new System.Drawing.Point(338, 333);
			this.LayoutControlItem21.Name = "LayoutControlItem21";
			this.LayoutControlItem21.Size = new System.Drawing.Size(108, 23);
			this.LayoutControlItem21.Text = "Send Exception Pay";
			this.LayoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem21.TextToControlDistance = 0;
			this.LayoutControlItem21.TextVisible = false;
			//
			//CheckEdit_returns_CDR
			//
			this.CheckEdit_returns_CDR.Location = new System.Drawing.Point(12, 385);
			this.CheckEdit_returns_CDR.Name = "CheckEdit_returns_CDR";
			this.CheckEdit_returns_CDR.Properties.Caption = "Returns CDR";
			this.CheckEdit_returns_CDR.Size = new System.Drawing.Size(439, 19);
			this.CheckEdit_returns_CDR.StyleController = this.LayoutControl1;
			this.CheckEdit_returns_CDR.TabIndex = 34;
			this.CheckEdit_returns_CDR.Visible = false;
			//
			//LayoutControlItem22
			//
			this.LayoutControlItem22.Control = this.CheckEdit_returns_CDR;
			this.LayoutControlItem22.CustomizationFormText = "Returns CDR record";
			this.LayoutControlItem22.Location = new System.Drawing.Point(0, 373);
			this.LayoutControlItem22.Name = "LayoutControlItem22";
			this.LayoutControlItem22.Size = new System.Drawing.Size(443, 23);
			this.LayoutControlItem22.Text = "Returns CDR record";
			this.LayoutControlItem22.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem22.TextToControlDistance = 0;
			this.LayoutControlItem22.TextVisible = false;
			//
			//CheckEdit_reqAdndaRev
			//
			this.CheckEdit_reqAdndaRev.Location = new System.Drawing.Point(189, 385);
			this.CheckEdit_reqAdndaRev.Name = "CheckEdit_reqAdndaRev";
			this.CheckEdit_reqAdndaRev.Properties.Caption = "Req Addenda";
			this.CheckEdit_reqAdndaRev.Size = new System.Drawing.Size(262, 19);
			this.CheckEdit_reqAdndaRev.StyleController = this.LayoutControl1;
			this.CheckEdit_reqAdndaRev.TabIndex = 35;
			this.CheckEdit_reqAdndaRev.Visible = false;
			//
			//LayoutControlItem23
			//
			this.LayoutControlItem23.Control = this.CheckEdit_reqAdndaRev;
			this.LayoutControlItem23.CustomizationFormText = "Requires Addenda Revision";
			this.LayoutControlItem23.Location = new System.Drawing.Point(177, 373);
			this.LayoutControlItem23.Name = "LayoutControlItem23";
			this.LayoutControlItem23.Size = new System.Drawing.Size(266, 23);
			this.LayoutControlItem23.Text = "Requires Addenda Revision";
			this.LayoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem23.TextToControlDistance = 0;
			this.LayoutControlItem23.TextVisible = false;
			//
			//CheckEdit_returns_cdm
			//
			this.CheckEdit_returns_cdm.Location = new System.Drawing.Point(273, 368);
			this.CheckEdit_returns_cdm.Name = "CheckEdit_returns_cdm";
			this.CheckEdit_returns_cdm.Properties.Caption = "Returns CDM";
			this.CheckEdit_returns_cdm.Size = new System.Drawing.Size(86, 19);
			this.CheckEdit_returns_cdm.StyleController = this.LayoutControl1;
			this.CheckEdit_returns_cdm.TabIndex = 36;
			this.CheckEdit_returns_cdm.Visible = false;
			//
			//LayoutControlItem24
			//
			this.LayoutControlItem24.Control = this.CheckEdit_returns_cdm;
			this.LayoutControlItem24.CustomizationFormText = "Returns CDM record";
			this.LayoutControlItem24.Location = new System.Drawing.Point(354, 356);
			this.LayoutControlItem24.Name = "LayoutControlItem24";
			this.LayoutControlItem24.Size = new System.Drawing.Size(90, 23);
			this.LayoutControlItem24.Text = "Returns CDM record";
			this.LayoutControlItem24.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem24.TextToControlDistance = 0;
			this.LayoutControlItem24.TextVisible = false;
			//
			//CheckEdit_returns_cdv
			//
			this.CheckEdit_returns_cdv.Location = new System.Drawing.Point(275, 368);
			this.CheckEdit_returns_cdv.Name = "CheckEdit_returns_cdv";
			this.CheckEdit_returns_cdv.Properties.Caption = "Returns CDV";
			this.CheckEdit_returns_cdv.Size = new System.Drawing.Size(84, 19);
			this.CheckEdit_returns_cdv.StyleController = this.LayoutControl1;
			this.CheckEdit_returns_cdv.TabIndex = 37;
			this.CheckEdit_returns_cdv.Visible = false;
			//
			//LayoutControlItem25
			//
			this.LayoutControlItem25.Control = this.CheckEdit_returns_cdv;
			this.LayoutControlItem25.CustomizationFormText = "Returns CDV record";
			this.LayoutControlItem25.Location = new System.Drawing.Point(266, 356);
			this.LayoutControlItem25.Name = "LayoutControlItem25";
			this.LayoutControlItem25.Size = new System.Drawing.Size(88, 23);
			this.LayoutControlItem25.Text = "Returns CDV record";
			this.LayoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem25.TextToControlDistance = 0;
			this.LayoutControlItem25.TextVisible = false;
			//
			//CheckEdit_returns_cda
			//
			this.CheckEdit_returns_cda.Location = new System.Drawing.Point(189, 385);
			this.CheckEdit_returns_cda.Name = "CheckEdit_returns_cda";
			this.CheckEdit_returns_cda.Properties.Caption = "Returns CDA";
			this.CheckEdit_returns_cda.Size = new System.Drawing.Size(85, 19);
			this.CheckEdit_returns_cda.StyleController = this.LayoutControl1;
			this.CheckEdit_returns_cda.TabIndex = 38;
			this.CheckEdit_returns_cda.Visible = false;
			//
			//LayoutControlItem26
			//
			this.LayoutControlItem26.Control = this.CheckEdit_returns_cda;
			this.LayoutControlItem26.CustomizationFormText = "Returns CDA record";
			this.LayoutControlItem26.Location = new System.Drawing.Point(177, 373);
			this.LayoutControlItem26.Name = "LayoutControlItem26";
			this.LayoutControlItem26.Size = new System.Drawing.Size(89, 23);
			this.LayoutControlItem26.Text = "Returns CDA record";
			this.LayoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem26.TextToControlDistance = 0;
			this.LayoutControlItem26.TextVisible = false;
			//
			//CheckEdit_returns_cdt
			//
			this.CheckEdit_returns_cdt.Location = new System.Drawing.Point(101, 385);
			this.CheckEdit_returns_cdt.Name = "CheckEdit_returns_cdt";
			this.CheckEdit_returns_cdt.Properties.Caption = "Returns CDT";
			this.CheckEdit_returns_cdt.Size = new System.Drawing.Size(350, 19);
			this.CheckEdit_returns_cdt.StyleController = this.LayoutControl1;
			this.CheckEdit_returns_cdt.TabIndex = 39;
			this.CheckEdit_returns_cdt.Visible = false;
			//
			//LayoutControlItem27
			//
			this.LayoutControlItem27.Control = this.CheckEdit_returns_cdt;
			this.LayoutControlItem27.CustomizationFormText = "Returns CDT record";
			this.LayoutControlItem27.Location = new System.Drawing.Point(89, 373);
			this.LayoutControlItem27.Name = "LayoutControlItem27";
			this.LayoutControlItem27.Size = new System.Drawing.Size(354, 23);
			this.LayoutControlItem27.Text = "Returns CDT record";
			this.LayoutControlItem27.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem27.TextToControlDistance = 0;
			this.LayoutControlItem27.TextVisible = false;
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(188, 373);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(94, 23);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//TextEdit_blroldname
			//
			this.TextEdit_blroldname.Location = new System.Drawing.Point(97, 84);
			this.TextEdit_blroldname.Name = "TextEdit_blroldname";
			this.TextEdit_blroldname.Properties.MaxLength = 1024;
			this.TextEdit_blroldname.Size = new System.Drawing.Size(354, 20);
			this.TextEdit_blroldname.StyleController = this.LayoutControl1;
			this.TextEdit_blroldname.TabIndex = 40;
			//
			//LayoutControlItem28
			//
			this.LayoutControlItem28.Control = this.TextEdit_blroldname;
			this.LayoutControlItem28.CustomizationFormText = "Biller Old Name";
			this.LayoutControlItem28.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem28.Name = "LayoutControlItem28";
			this.LayoutControlItem28.Size = new System.Drawing.Size(443, 24);
			this.LayoutControlItem28.Text = "Biller Old Name";
			this.LayoutControlItem28.TextSize = new System.Drawing.Size(50, 20);
			this.LayoutControlItem28.TextToControlDistance = 5;
			//
			//CheckEdit_guaranteed_funds
			//
			this.CheckEdit_guaranteed_funds.Location = new System.Drawing.Point(12, 60);
			this.CheckEdit_guaranteed_funds.Name = "CheckEdit_guaranteed_funds";
			this.CheckEdit_guaranteed_funds.Properties.Caption = "Guaranteed Funds";
			this.CheckEdit_guaranteed_funds.Size = new System.Drawing.Size(217, 19);
			this.CheckEdit_guaranteed_funds.StyleController = this.LayoutControl1;
			this.CheckEdit_guaranteed_funds.TabIndex = 41;
			//
			//LayoutControlItem29
			//
			this.LayoutControlItem29.Control = this.CheckEdit_guaranteed_funds;
			this.LayoutControlItem29.CustomizationFormText = "Guaranteed Funds";
			this.LayoutControlItem29.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem29.Name = "LayoutControlItem29";
			this.LayoutControlItem29.Size = new System.Drawing.Size(221, 24);
			this.LayoutControlItem29.Text = "Guaranteed Funds";
			this.LayoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem29.TextToControlDistance = 0;
			this.LayoutControlItem29.TextVisible = false;
			//
			//TextEdit_currency
			//
			this.TextEdit_currency.Location = new System.Drawing.Point(97, 60);
			this.TextEdit_currency.Name = "TextEdit_currency";
			this.TextEdit_currency.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit_currency.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit_currency.Properties.DisplayFormat.FormatString = "{0:f0}";
			this.TextEdit_currency.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_currency.Properties.EditFormat.FormatString = "{0:f0}";
			this.TextEdit_currency.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit_currency.Properties.Mask.BeepOnError = true;
			this.TextEdit_currency.Properties.Mask.EditMask = "f0";
			this.TextEdit_currency.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.TextEdit_currency.Size = new System.Drawing.Size(354, 20);
			this.TextEdit_currency.StyleController = this.LayoutControl1;
			this.TextEdit_currency.TabIndex = 42;
			//
			//LayoutControlItem30
			//
			this.LayoutControlItem30.Control = this.TextEdit_currency;
			this.LayoutControlItem30.CustomizationFormText = "Currency";
			this.LayoutControlItem30.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem30.Name = "LayoutControlItem30";
			this.LayoutControlItem30.Size = new System.Drawing.Size(443, 24);
			this.LayoutControlItem30.Text = "Currency";
			this.LayoutControlItem30.TextSize = new System.Drawing.Size(50, 20);
			this.LayoutControlItem30.TextToControlDistance = 5;
			//
			//DateEdit_live_date
			//
			this.DateEdit_live_date.EditValue = null;
			this.DateEdit_live_date.Location = new System.Drawing.Point(312, 60);
			this.DateEdit_live_date.Name = "DateEdit_live_date";
			this.DateEdit_live_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit_live_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit_live_date.Size = new System.Drawing.Size(139, 20);
			this.DateEdit_live_date.StyleController = this.LayoutControl1;
			this.DateEdit_live_date.TabIndex = 43;
			//
			//LayoutControlItem31
			//
			this.LayoutControlItem31.Control = this.DateEdit_live_date;
			this.LayoutControlItem31.CustomizationFormText = "Live Date";
			this.LayoutControlItem31.Location = new System.Drawing.Point(215, 48);
			this.LayoutControlItem31.Name = "LayoutControlItem31";
			this.LayoutControlItem31.Size = new System.Drawing.Size(228, 24);
			this.LayoutControlItem31.Text = "Live";
			this.LayoutControlItem31.TextSize = new System.Drawing.Size(50, 20);
			this.LayoutControlItem31.TextToControlDistance = 5;
			//
			//EditForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(486, 480);
			this.Controls.Add(this.LayoutControl1);
			this.Name = "EditForm";
			this.Text = "RPPS Biller";
			this.Controls.SetChildIndex(this.SimpleButton_OK, 0);
			this.Controls.SetChildIndex(this.SimpleButton_Cancel, 0);
			this.Controls.SetChildIndex(this.LayoutControl1, 0);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdf.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdn.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_checkdigit.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_eff_date.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_eff_date.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_payment_prenote.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdv.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_dmppayonly.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_proposal_prenote.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_fbd.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdp.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_cdd.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.XtraTabControl1).EndInit();
			this.XtraTabControl1.ResumeLayout(false);
			this.XtraTabPage1.ResumeLayout(false);
			this.XtraTabPage2.ResumeLayout(false);
			this.XtraTabPage3.ResumeLayout(false);
			this.XtraTabPage4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.MemoEdit_note.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_biller_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_biller_class.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_biller_name.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_reversal_biller_id.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_fbc.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_rpps_biller_id.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_biller_aba.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem15).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem16).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem17).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem18).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem19).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem20).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_send_exception_pay.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem21).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_CDR.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem22).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_reqAdndaRev.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem23).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cdm.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem24).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cdv.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem25).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cda.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem26).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_returns_cdt.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem27).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_blroldname.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem28).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_guaranteed_funds.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem29).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit_currency.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem30).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_live_date.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit_live_date.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem31).EndInit();
			this.ResumeLayout(false);

		}
		protected internal LOBGridControl LobGridControl1;
		protected internal AKAGridControl AkaGridControl1;
		protected internal AddressGridControl AddressGridControl1;
		protected internal MaskGridControl MaskGridControl1;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_biller_class;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_biller_name;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_reversal_biller_id;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_rpps_biller_id;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_biller_aba;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		protected internal DevExpress.XtraEditors.MemoEdit MemoEdit_note;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_biller_type;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		protected internal DevExpress.XtraTab.XtraTabControl XtraTabControl1;
		protected internal DevExpress.XtraTab.XtraTabPage XtraTabPage1;
		protected internal DevExpress.XtraTab.XtraTabPage XtraTabPage2;
		protected internal DevExpress.XtraTab.XtraTabPage XtraTabPage3;
		protected internal DevExpress.XtraTab.XtraTabPage XtraTabPage4;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_payment_prenote;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_cdv;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_dmppayonly;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_proposal_prenote;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_fbd;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_cdp;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_cdd;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_fbc;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem15;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem16;
		protected internal DevExpress.XtraEditors.DateEdit DateEdit_eff_date;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem17;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_cdn;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_checkdigit;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem18;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem19;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_cdf;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem20;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_send_exception_pay;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem21;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_returns_cdt;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_returns_cda;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_returns_cdv;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_returns_cdm;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_reqAdndaRev;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_returns_CDR;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem22;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem23;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem24;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem25;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem26;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem27;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_blroldname;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem28;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit_currency;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_guaranteed_funds;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem29;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem30;
		protected internal DevExpress.XtraEditors.DateEdit DateEdit_live_date;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem31;
	}
}
