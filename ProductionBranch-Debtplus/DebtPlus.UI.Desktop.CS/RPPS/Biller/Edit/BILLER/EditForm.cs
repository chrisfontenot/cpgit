using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Common;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.BILLER
{
    internal partial class EditForm
    {
        public EditForm() : base()
        {
            InitializeComponent();
            this.Load += EditForm_Load;
        }

        public EditForm(Context ctx, System.Data.DataRowView drv) : base(ctx, drv)
        {
            InitializeComponent();
            this.Load += EditForm_Load;
        }

        private string privateOldBiller = "0000000000";

        internal string OldBiller
        {
            get { return privateOldBiller; }
            set { privateOldBiller = value; }
        }

        private void EditForm_Load(object sender, System.EventArgs e)
        {
            MaskGridControl1.LoadControl(ctx);
            AddressGridControl1.LoadControl(ctx);
            AkaGridControl1.LoadControl(ctx);
            LobGridControl1.LoadControl(ctx);

            // If there is a row then obtain the current row setting for the biller id
            if (drv["rpps_biller_id"] != null && !object.ReferenceEquals(drv["rpps_biller_id"], System.DBNull.Value))
                OldBiller = Convert.ToString(drv["rpps_biller_id"]);

            // Set the various control lists
            MaskGridControl1.ReadForm(OldBiller);
            AddressGridControl1.ReadForm(OldBiller);
            AkaGridControl1.ReadForm(OldBiller);
            LobGridControl1.ReadForm(OldBiller);

            var _with1 = TextEdit_rpps_biller_id;
            _with1.DataBindings.Clear();
            _with1.DataBindings.Add("EditValue", drv, "rpps_biller_id");
            _with1.EditValueChanged += FormChanged;

            var _with2 = TextEdit_biller_aba;
            _with2.DataBindings.Clear();
            _with2.DataBindings.Add("EditValue", drv, "biller_aba");
            _with2.EditValueChanged += FormChanged;

            var _with3 = TextEdit_biller_class;
            _with3.DataBindings.Clear();
            _with3.DataBindings.Add("EditValue", drv, "biller_class");
            _with3.EditValueChanged += FormChanged;

            var _with4 = TextEdit_reversal_biller_id;
            _with4.DataBindings.Clear();
            _with4.DataBindings.Add("EditValue", drv, "reversal_biller_id");
            _with4.EditValueChanged += FormChanged;

            var _with5 = TextEdit_biller_name;
            _with5.DataBindings.Clear();
            _with5.DataBindings.Add("EditValue", drv, "biller_name");
            _with5.EditValueChanged += FormChanged;

            var _with6 = MemoEdit_note;
            _with6.DataBindings.Clear();
            _with6.DataBindings.Add("EditValue", drv, "note");
            _with6.EditValueChanged += FormChanged;

            var _with7 = DateEdit_eff_date;
            _with7.DataBindings.Clear();
            _with7.DataBindings.Add("EditValue", drv, "effdate");
            _with7.EditValueChanged += FormChanged;

            var _with8 = DateEdit_live_date;
            _with8.DataBindings.Clear();
            _with8.DataBindings.Add("EditValue", drv, "livedate");
            _with8.EditValueChanged += FormChanged;

            var _with9 = LookUpEdit_biller_type;
            _with9.Properties.DataSource = EditTable.BillerTypeTable();
            _with9.DataBindings.Clear();
            _with9.DataBindings.Add("EditValue", drv, "biller_type");
            _with9.EditValueChanged += FormChanged;

            var _with10 = CheckEdit_dmppayonly;
            _with10.DataBindings.Clear();
            _with10.DataBindings.Add("EditValue", drv, "dmppayonly");
            _with10.EditValueChanged += FormChanged;

            var _with11 = CheckEdit_payment_prenote;
            _with11.DataBindings.Clear();
            _with11.DataBindings.Add("EditValue", drv, "payment_prenote");
            _with11.EditValueChanged += FormChanged;

            var _with12 = CheckEdit_proposal_prenote;
            _with12.DataBindings.Clear();
            _with12.DataBindings.Add("EditValue", drv, "proposal_prenote");
            _with12.EditValueChanged += FormChanged;

            var _with13 = CheckEdit_send_cdd;
            _with13.DataBindings.Clear();
            _with13.DataBindings.Add("EditValue", drv, "send_cdd");
            _with13.EditValueChanged += FormChanged;

            var _with14 = CheckEdit_send_cdp;
            _with14.DataBindings.Clear();
            _with14.DataBindings.Add("EditValue", drv, "send_cdp");
            _with14.EditValueChanged += FormChanged;

            var _with15 = CheckEdit_send_cdv;
            _with15.DataBindings.Clear();
            _with15.DataBindings.Add("EditValue", drv, "send_cdv");
            _with15.EditValueChanged += FormChanged;

            var _with16 = CheckEdit_send_fbc;
            _with16.DataBindings.Clear();
            _with16.DataBindings.Add("EditValue", drv, "send_fbc");
            _with16.EditValueChanged += FormChanged;

            var _with17 = CheckEdit_send_fbd;
            _with17.DataBindings.Clear();
            _with17.DataBindings.Add("EditValue", drv, "send_fbd");
            _with17.EditValueChanged += FormChanged;

            var _with18 = CheckEdit_checkdigit;
            _with18.DataBindings.Clear();
            _with18.DataBindings.Add("EditValue", drv, "checkdigit");
            _with18.EditValueChanged += FormChanged;

            var _with19 = CheckEdit_send_cdn;
            _with19.DataBindings.Clear();
            _with19.DataBindings.Add("EditValue", drv, "send_cdn");
            _with19.EditValueChanged += FormChanged;

            var _with20 = CheckEdit_send_cdf;
            _with20.DataBindings.Clear();
            _with20.DataBindings.Add("EditValue", drv, "send_cdf");
            _with20.EditValueChanged += FormChanged;

            var _with21 = CheckEdit_guaranteed_funds;
            _with21.DataBindings.Clear();
            _with21.DataBindings.Add("EditValue", drv, "guaranteed_funds");
            _with21.EditValueChanged += FormChanged;

            var _with22 = CheckEdit_reqAdndaRev;
            _with22.DataBindings.Clear();
            _with22.DataBindings.Add("EditValue", drv, "reqAdndaRev");
            _with22.EditValueChanged += FormChanged;

            var _with23 = CheckEdit_returns_cda;
            _with23.DataBindings.Clear();
            _with23.DataBindings.Add("EditValue", drv, "returns_cda");
            _with23.EditValueChanged += FormChanged;

            var _with24 = CheckEdit_returns_cdm;
            _with24.DataBindings.Clear();
            _with24.DataBindings.Add("EditValue", drv, "returns_cdm");
            _with24.EditValueChanged += FormChanged;

            var _with25 = CheckEdit_returns_CDR;
            _with25.DataBindings.Clear();
            _with25.DataBindings.Add("EditValue", drv, "returns_cdr");
            _with25.EditValueChanged += FormChanged;

            var _with26 = CheckEdit_returns_cdt;
            _with26.DataBindings.Clear();
            _with26.DataBindings.Add("EditValue", drv, "returns_cdt");
            _with26.EditValueChanged += FormChanged;

            var _with27 = CheckEdit_returns_cdv;
            _with27.DataBindings.Clear();
            _with27.DataBindings.Add("EditValue", drv, "returns_cdv");
            _with27.EditValueChanged += FormChanged;

            var _with28 = CheckEdit_send_exception_pay;
            _with28.DataBindings.Clear();
            _with28.DataBindings.Add("EditValue", drv, "send_exception_pay");
            _with28.EditValueChanged += FormChanged;

            var _with29 = TextEdit_currency;
            _with29.DataBindings.Clear();
            _with29.DataBindings.Add("EditValue", drv, "currency");
            _with29.EditValueChanged += FormChanged;

            var _with30 = TextEdit_blroldname;
            _with30.DataBindings.Clear();
            _with30.DataBindings.Add("EditValue", drv, "blroldname");
            _with30.EditValueChanged += FormChanged;

            SimpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (TextEdit_rpps_biller_id.EditValue == null || object.ReferenceEquals(TextEdit_rpps_biller_id.EditValue, System.DBNull.Value))
                return true;
            return Convert.ToString(TextEdit_rpps_biller_id.EditValue) == "0000000000" || Convert.ToString(TextEdit_rpps_biller_id.EditValue).Length != 10;
        }

        private void FormChanged(object Sender, System.EventArgs e)
        {
            SimpleButton_OK.Enabled = !HasErrors();
        }

        protected override void SimpleButton_OK_Click(object Sender, System.EventArgs e)
        {
            base.SimpleButton_OK_Click(Sender, e);

            System.Data.DataSet ds = drv.Row.Table.DataSet;

            // Find the previous items in the tables and change them to the new biller id
            string NewBiller = "0000000000";
            if (drv["rpps_biller_id"] != null && !object.ReferenceEquals(drv["rpps_biller_id"], System.DBNull.Value))
                NewBiller = Convert.ToString(drv["rpps_biller_id"]);

            if (NewBiller != OldBiller)
            {
                // Update the LOB table
                System.Data.DataView vue = new System.Data.DataView(ds.Tables["rpps_lobs"], string.Format("[rpps_biller_id]='{0}'", OldBiller), string.Empty, DataViewRowState.CurrentRows);
                foreach (System.Data.DataRowView Updatedrv in vue)
                {
                    Updatedrv.BeginEdit();
                    Updatedrv["rpps_biller_id"] = NewBiller;
                    Updatedrv.EndEdit();
                }

                // Update the ADDRESSES table
                vue = new System.Data.DataView(ds.Tables["rpps_addresses"], string.Format("[rpps_biller_id]='{0}'", OldBiller), string.Empty, DataViewRowState.CurrentRows);
                foreach (System.Data.DataRowView Updatedrv in vue)
                {
                    Updatedrv.BeginEdit();
                    Updatedrv["rpps_biller_id"] = NewBiller;
                    Updatedrv.EndEdit();
                }

                // Update the MASKS table
                vue = new System.Data.DataView(ds.Tables["rpps_masks"], string.Format("[rpps_biller_id]='{0}'", OldBiller), string.Empty, DataViewRowState.CurrentRows);
                foreach (System.Data.DataRowView Updatedrv in vue)
                {
                    Updatedrv.BeginEdit();
                    Updatedrv["rpps_biller_id"] = NewBiller;
                    Updatedrv.EndEdit();
                }

                // Update the AKAS table
                vue = new System.Data.DataView(ds.Tables["rpps_akas"], string.Format("[rpps_biller_id]='{0}'", OldBiller), string.Empty, DataViewRowState.CurrentRows);
                foreach (System.Data.DataRowView Updatedrv in vue)
                {
                    Updatedrv.BeginEdit();
                    Updatedrv["rpps_biller_id"] = NewBiller;
                    Updatedrv.EndEdit();
                }
            }
        }
    }

    static internal partial class EditTable
    {
        static internal System.Data.DataTable BillerTypeTable()
        {
            System.Data.DataTable tbl = new System.Data.DataTable("biller_types");
            var _with31 = tbl;
            _with31.Columns.Add("biller_type", typeof(System.Int32));
            _with31.Columns.Add("description", typeof(string));
            _with31.PrimaryKey = new System.Data.DataColumn[] { _with31.Columns[0] };

            _with31.Rows.Add(1, "NET");
            _with31.Rows.Add(2, "GROSS");
            _with31.Rows.Add(3, "MODIFIED GROSS");
            _with31.Rows.Add(4, "DMP");
            _with31.Rows.Add(11, "DMP/NET");
            _with31.Rows.Add(12, "DMP/GROSS");
            _with31.Rows.Add(13, "DMP/MODIFIED GROSS");

            return tbl;
        }
    }
}