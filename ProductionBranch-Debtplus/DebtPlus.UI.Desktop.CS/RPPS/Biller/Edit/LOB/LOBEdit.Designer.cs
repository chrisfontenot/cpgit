using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.LOB
{
	partial class LOBEdit : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Cance = new DevExpress.XtraEditors.SimpleButton();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.SimpleButton_OK.Enabled = false;
			this.SimpleButton_OK.Location = new System.Drawing.Point(63, 57);
			this.SimpleButton_OK.Name = "SimpleButton_OK";
			this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.TabIndex = 2;
			this.SimpleButton_OK.Text = "&OK";
			//
			//SimpleButton_Cance
			//
			this.SimpleButton_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_Cance.Location = new System.Drawing.Point(155, 57);
			this.SimpleButton_Cance.Name = "SimpleButton_Cance";
			this.SimpleButton_Cance.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cance.TabIndex = 3;
			this.SimpleButton_Cance.Text = "&Cancel";
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(12, 16);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(76, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Line of Business";
			//
			//TextEdit1
			//
			this.TextEdit1.Location = new System.Drawing.Point(94, 13);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Properties.MaxLength = 3;
			this.TextEdit1.Size = new System.Drawing.Size(65, 20);
			this.TextEdit1.TabIndex = 1;
			//
			//LOBEdit
			//
			this.AcceptButton = this.SimpleButton_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButton_Cance;
			this.ClientSize = new System.Drawing.Size(292, 98);
			this.Controls.Add(this.TextEdit1);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.SimpleButton_Cance);
			this.Controls.Add(this.SimpleButton_OK);
			this.MaximizeBox = false;
			this.Name = "LOBEdit";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "RPPS Line Of Buisness";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cance;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit1;
	}
}
