using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.Templates;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.LOB
{
    internal partial class LOBGridControl : EditRecordControl
    {
        public LOBGridControl() : base()
        {
            InitializeComponent();
        }

        private string OriginalRPPSBiller;
        private System.Data.DataTable RecordTable = null;

        public void ReadForm(string RppsBillerID)
        {
            RecordTable = ReadDataset(RppsBillerID);
            GridControl1.DataSource = new System.Data.DataView(RecordTable, string.Format("[rpps_biller_id]='{0}'", RppsBillerID), string.Empty, DataViewRowState.CurrentRows);
            GridControl1.RefreshDataSource();
            GridView1.BestFitColumns();
        }

        private System.Data.DataTable ReadDataset(string RppsBillerID)
        {
            OriginalRPPSBiller = RppsBillerID;

            System.Data.DataTable tbl = ctx.ds.Tables["rpps_lobs"];
            if (tbl != null)
            {
                System.Data.DataRow[] rows = tbl.Select(string.Format("[rpps_biller_id]='{0}'", RppsBillerID));
                if (rows.Length <= 0)
                    tbl = null;
            }

            // Load the dataset
            if (tbl == null)
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with1.CommandText = "SELECT [rpps_biller_lob],[rpps_biller_id],[biller_lob],[date_created],[created_by] FROM rpps_lobs WHERE rpps_biller_id=@rpps_biller_id";
                    _with1.CommandType = CommandType.Text;
                    _with1.Parameters.Add("@rpps_biller_id", SqlDbType.VarChar, 20).Value = OriginalRPPSBiller;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.AcceptChangesDuringFill = true;
                        da.FillLoadOption = LoadOption.PreserveChanges;
                        da.Fill(ctx.ds, "rpps_lobs");
                    }
                }

                tbl = ctx.ds.Tables["rpps_lobs"];

                // Supplement the schema with the known values that are not read from the database
                var _with2 = tbl;
                if (_with2.PrimaryKey.GetUpperBound(0) < 0)
                {
                    _with2.PrimaryKey = new System.Data.DataColumn[] { _with2.Columns["rpps_biller_lob"] };
                    var _with3 = _with2.Columns["rpps_biller_lob"];
                    _with3.AutoIncrement = true;
                    _with3.AutoIncrementSeed = -1;
                    _with3.AutoIncrementStep = -1;
                }
            }

            return tbl;
        }

        public void SaveForm()
        {
        }

        /// <summary>
        /// Edit the values for the current row
        /// </summary>
        protected override void EditRow(System.Data.DataRowView EditDrv)
        {
            EditDrv.BeginEdit();
            var _with4 = new LOBEdit(EditDrv);
            if (_with4.ShowDialog() == DialogResult.OK)
            {
                EditDrv.EndEdit();
            }
            else
            {
                EditDrv.CancelEdit();
            }
            _with4.Dispose();
        }

        /// <summary>
        /// Create a new blank row in the database
        /// </summary>
        protected override void CreateRow(System.Data.DataView vue)
        {
            System.Data.DataRowView EditDrv = vue.AddNew();
            EditDrv["rpps_biller_id"] = OriginalRPPSBiller;
            EditDrv["date_created"] = DateTime.Now;
            EditDrv["created_by"] = "Me";
            EditRow(EditDrv);
        }

        /// <summary>
        /// Delete the current row from the database
        /// </summary>
        protected override void DeleteRow(System.Data.DataRowView EditDrv)
        {
            EditDrv.Delete();
        }
    }
}