using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.LOB
{

	partial class LOBGridControl
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.GridColumn_lob = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_lob.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.SuspendLayout();
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_lob,
				this.GridColumn_description
			});
			//
			//GridColumn_lob
			//
			this.GridColumn_lob.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_lob.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_lob.Caption = "ID";
			this.GridColumn_lob.CustomizationCaption = "Record ID";
			this.GridColumn_lob.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_lob.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_lob.FieldName = "rpps_biller_lob";
			this.GridColumn_lob.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_lob.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_lob.Name = "GridColumn_lob";
			//
			//GridColumn_description
			//
			this.GridColumn_description.Caption = "Line Of Business";
			this.GridColumn_description.FieldName = "biller_lob";
			this.GridColumn_description.Name = "GridColumn_description";
			this.GridColumn_description.Visible = true;
			this.GridColumn_description.VisibleIndex = 0;
			//
			//LOBGridControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "LOBGridControl";
			this.Size = new System.Drawing.Size(426, 355);
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_lob;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_description;
	}
}
