using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.MASK
{
    public partial class MaskEdit
    {
        public MaskEdit() : base()
        {
            InitializeComponent();
        }

        private System.Data.DataRowView drv = null;

        public MaskEdit(System.Data.DataRowView drv) : this()
        {
            this.drv = drv;

            this.Load += Form_Load;
            SpinEdit1.EditValueChanging += SpinEdit1_EditValueChanging;
        }

        private void Form_Load(object sender, System.EventArgs e)
        {
            var _with1 = TextEdit1;
            _with1.DataBindings.Clear();
            _with1.DataBindings.Add("EditValue", drv, "mask");
            _with1.EditValueChanged += Form_Changed;

            var _with2 = TextEdit2;
            _with2.DataBindings.Clear();
            _with2.DataBindings.Add("EditValue", drv, "description");
            _with2.EditValueChanged += Form_Changed;

            var _with3 = CheckEdit_ExceptionMask;
            _with3.DataBindings.Clear();
            _with3.DataBindings.Add("EditValue", drv, "ExceptionMask");
            _with3.EditValueChanged += Form_Changed;

            var _with4 = CheckEdit_checkdigit;
            _with4.DataBindings.Clear();
            _with4.DataBindings.Add("EditValue", drv, "CheckDigit");
            _with4.EditValueChanged += Form_Changed;

            var _with5 = SpinEdit1;
            _with5.DataBindings.Clear();
            _with5.DataBindings.Add("EditValue", drv, "length");
            _with5.EditValueChanged += Form_Changed;

            SimpleButton_OK.Enabled = !HasErrors();
        }

        private void Form_Changed(object Sender, System.EventArgs e)
        {
            SimpleButton_OK.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            return false;
        }

        private void SpinEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            object value = SpinEdit1.EditValue;
            if (value == null || object.ReferenceEquals(value, System.DBNull.Value))
                value = 0;
            e.Cancel = Convert.ToInt32(value) < 0;
        }
    }
}