using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.Templates;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.MASK
{
	partial class MaskGridControl : EditRecordControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.GridColumn_address = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn3.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn4.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.SuspendLayout();
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_address,
				this.GridColumn2,
				this.GridColumn1,
				this.GridColumn3,
				this.GridColumn4
			});
			//
			//GridColumn_address
			//
			this.GridColumn_address.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_address.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_address.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_address.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_address.Caption = "ID";
			this.GridColumn_address.CustomizationCaption = "Record ID";
			this.GridColumn_address.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_address.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_address.FieldName = "rpps_mask";
			this.GridColumn_address.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_address.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_address.Name = "GridColumn_address";
			//
			//GridColumn_subject
			//
			this.GridColumn2.Caption = "Mask";
			this.GridColumn2.FieldName = "Mask";
			this.GridColumn2.Name = "GridColumn_subject";
			this.GridColumn2.Visible = true;
			this.GridColumn2.VisibleIndex = 0;
			//
			//GridColumn_counselor
			//
			this.GridColumn1.Caption = "Length";
			this.GridColumn1.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn1.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn1.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn1.FieldName = "Length";
			this.GridColumn1.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn1.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn1.Name = "GridColumn_counselor";
			this.GridColumn1.Visible = true;
			this.GridColumn1.VisibleIndex = 1;
			//
			//GridColumn_Id
			//
			this.GridColumn3.Caption = "Description";
			this.GridColumn3.FieldName = "Description";
			this.GridColumn3.Name = "GridColumn_Id";
			//
			//GridColumn_dont_delete
			//
			this.GridColumn4.Caption = "Checksum";
			this.GridColumn4.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn4.FieldName = "CheckDigit";
			this.GridColumn4.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn4.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn4.Name = "GridColumn_dont_delete";
			//
			//MaskGridControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Name = "MaskGridControl";
			this.Size = new System.Drawing.Size(426, 355);
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_address;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn2;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn3;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn4;
	}
}
