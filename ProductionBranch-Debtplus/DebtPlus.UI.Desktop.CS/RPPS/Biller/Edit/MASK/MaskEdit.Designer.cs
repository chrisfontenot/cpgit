using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.MASK
{
	partial class MaskEdit : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton_OK = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Cance = new DevExpress.XtraEditors.SimpleButton();
			this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.TextEdit2 = new DevExpress.XtraEditors.TextEdit();
			this.SpinEdit1 = new DevExpress.XtraEditors.SpinEdit();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.CheckEdit_ExceptionMask = new DevExpress.XtraEditors.CheckEdit();
			this.CheckEdit_checkdigit = new DevExpress.XtraEditors.CheckEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit2.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.SpinEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ExceptionMask.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_checkdigit.Properties).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton_OK
			//
			this.SimpleButton_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.SimpleButton_OK.Enabled = false;
			this.SimpleButton_OK.Location = new System.Drawing.Point(242, 86);
			this.SimpleButton_OK.Name = "SimpleButton_OK";
			this.SimpleButton_OK.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_OK.TabIndex = 8;
			this.SimpleButton_OK.Text = "&OK";
			//
			//SimpleButton_Cance
			//
			this.SimpleButton_Cance.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_Cance.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.SimpleButton_Cance.Location = new System.Drawing.Point(242, 126);
			this.SimpleButton_Cance.Name = "SimpleButton_Cance";
			this.SimpleButton_Cance.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cance.TabIndex = 9;
			this.SimpleButton_Cance.Text = "&Cancel";
			//
			//TextEdit1
			//
			this.TextEdit1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.TextEdit1.Location = new System.Drawing.Point(81, 12);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Properties.MaxLength = 50;
			this.TextEdit1.Size = new System.Drawing.Size(241, 20);
			this.TextEdit1.TabIndex = 1;
			//
			//TextEdit2
			//
			this.TextEdit2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.TextEdit2.Location = new System.Drawing.Point(81, 38);
			this.TextEdit2.Name = "TextEdit2";
			this.TextEdit2.Properties.MaxLength = 22;
			this.TextEdit2.Size = new System.Drawing.Size(241, 20);
			this.TextEdit2.TabIndex = 3;
			//
			//SpinEdit1
			//
			this.SpinEdit1.EditValue = new decimal(new Int32[] {
				0,
				0,
				0,
				0
			});
			this.SpinEdit1.Location = new System.Drawing.Point(81, 64);
			this.SpinEdit1.Name = "SpinEdit1";
			this.SpinEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.SpinEdit1.Properties.IsFloatValue = false;
			this.SpinEdit1.Properties.Mask.EditMask = "N00";
			this.SpinEdit1.Properties.MaxValue = new decimal(new Int32[] {
				22,
				0,
				0,
				0
			});
			this.SpinEdit1.Size = new System.Drawing.Size(75, 20);
			this.SpinEdit1.TabIndex = 5;
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(13, 15);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(24, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Mask";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 41);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(23, 13);
			this.LabelControl2.TabIndex = 2;
			this.LabelControl2.Text = "Note";
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 67);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(33, 13);
			this.LabelControl3.TabIndex = 4;
			this.LabelControl3.Text = "Length";
			//
			//CheckEdit_ExceptionMask
			//
			this.CheckEdit_ExceptionMask.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.CheckEdit_ExceptionMask.Location = new System.Drawing.Point(10, 100);
			this.CheckEdit_ExceptionMask.Name = "CheckEdit_ExceptionMask";
			this.CheckEdit_ExceptionMask.Properties.Caption = "Exception Mask";
			this.CheckEdit_ExceptionMask.Size = new System.Drawing.Size(105, 19);
			this.CheckEdit_ExceptionMask.TabIndex = 6;
			//
			//CheckEdit_checkdigit
			//
			this.CheckEdit_checkdigit.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.CheckEdit_checkdigit.Location = new System.Drawing.Point(11, 125);
			this.CheckEdit_checkdigit.Name = "CheckEdit_checkdigit";
			this.CheckEdit_checkdigit.Properties.Caption = "Has Check Digit";
			this.CheckEdit_checkdigit.Size = new System.Drawing.Size(105, 19);
			this.CheckEdit_checkdigit.TabIndex = 7;
			//
			//MaskEdit
			//
			this.AcceptButton = this.SimpleButton_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.SimpleButton_Cance;
			this.ClientSize = new System.Drawing.Size(329, 175);
			this.Controls.Add(this.CheckEdit_checkdigit);
			this.Controls.Add(this.CheckEdit_ExceptionMask);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.SpinEdit1);
			this.Controls.Add(this.TextEdit2);
			this.Controls.Add(this.TextEdit1);
			this.Controls.Add(this.SimpleButton_Cance);
			this.Controls.Add(this.SimpleButton_OK);
			this.MaximizeBox = false;
			this.Name = "MaskEdit";
			this.Text = "RPPS Mask";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit2.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.SpinEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ExceptionMask.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_checkdigit.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_OK;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cance;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit1;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit2;
		protected internal DevExpress.XtraEditors.SpinEdit SpinEdit1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_ExceptionMask;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_checkdigit;
	}
}
