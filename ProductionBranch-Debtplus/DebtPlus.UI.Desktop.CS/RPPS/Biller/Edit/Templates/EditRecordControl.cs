using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.BILLER;
using DebtPlus.UI.Common;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.Templates
{
    internal partial class EditRecordControl
    {
        public EditRecordControl() : base()
        {
            InitializeComponent();
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.Click += GridView1_Click;
            BarButtonItem_Change.ItemClick += BarButtonItemChange_ItemClick;
            BarButtonItem_Delete.ItemClick += BarButtonItem_Delete_ItemClick;
            BarButtonItem_Add.ItemClick += BarButtonItem_Add_ItemClick;
            GridView1.MouseUp += GridView1_MouseUp;
            this.Load += MyGridControl_Load;
        }

        protected Context ctx = null;

        public void LoadControl(Context ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Find a pointer to the data row view that is being edited
        /// </summary>
        protected internal DataRowView drv
        {
            get { return ((EditForm)ParentForm).drv; }
        }

        /// <summary>
        /// Retrieve the rpps biller id from the edit record
        /// </summary>
        protected internal string rpps_biller_id
        {
            get { return global::DebtPlus.Utils.Nulls.DStr(drv["rpps_biller_id"]); }
        }

        /// <summary>
        /// Biller to be used in newly created rows
        /// </summary>
        protected internal string OldBiller
        {
            get { return ((EditForm)ParentForm).OldBiller; }
        }

        /// <summary>
        /// Edit the values for the current row
        /// </summary>
        protected virtual void EditRow(DataRowView EditDrv)
        {
        }

        /// <summary>
        /// Create a new blank row in the database
        /// </summary>
        protected virtual void CreateRow(DataView vue)
        {
        }

        /// <summary>
        /// Delete the current row from the database
        /// </summary>
        protected virtual void DeleteRow(DataRowView EditDrv)
        {
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.GridControl ctl = gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            Int32 RowHandle = -1;
            if (hi.InRow || hi.InRowCell)
            {
                RowHandle = hi.RowHandle;
            }

            // Find the row view in the dataview object for this row.
            DataRowView drv = null;
            if (RowHandle >= 0)
            {
                gv.FocusedRowHandle = RowHandle;
                drv = (DataRowView)gv.GetRow(RowHandle);
            }

            // If the row is defined then edit the row.
            if (drv != null)
                EditRow(drv);
        }

        /// <summary>
        /// Double click event on the list
        /// </summary>
        private Int32 RowHandle = -1;

        private void GridView1_Click(object sender, EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.GridControl ctl = gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            RowHandle = -1;
            if (hi.InRow || hi.InRowCell)
            {
                RowHandle = hi.RowHandle;
            }

            if (RowHandle >= 0 && GridView1.GetRow(RowHandle) != null)
                RowHandle = -1;

            // Enable the popup menu items according to the position clicked
            BarButtonItem_Change.Enabled = (RowHandle >= 0);
            BarButtonItem_Delete.Enabled = (RowHandle >= 0);
        }

        /// <summary>
        /// Process the change item in the popup menu
        /// </summary>

        private void BarButtonItemChange_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRowView drv = null;
            if (RowHandle >= 0)
            {
                drv = (DataRowView)GridView1.GetRow(RowHandle);
            }

            // If the row is defined then edit the row.
            if (drv != null)
                EditRow(drv);
        }

        /// <summary>
        /// Process the delete item in the popup menu
        /// </summary>
        private void BarButtonItem_Delete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DataRowView drv = null;
            if (RowHandle >= 0)
            {
                drv = (DataRowView)GridView1.GetRow(RowHandle);
            }

            // If the row is defined then edit the row.
            if (drv != null)
                DeleteRow(drv);
        }

        /// <summary>
        /// Process the add item in the popup menu
        /// </summary>
        private void BarButtonItem_Add_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CreateRow((DataView)GridControl1.DataSource);
        }

        /// <summary>
        /// Show the popup menu when the button is released
        /// </summary>
        private void GridView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                PopupMenu1.ShowPopup(System.Windows.Forms.Control.MousePosition);
            }
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string BasePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus";
            return System.IO.Path.Combine(BasePath, "Tables.RPPS.Billers" + System.IO.Path.PathSeparator + ParentForm.Name);
        }

        /// <summary>
        /// When loading the control, restore the layout from the file
        /// </summary>
        private void MyGridControl_Load(object sender, EventArgs e)
        {
            string PathName = XMLBasePath();
            string FileName = System.IO.Path.Combine(PathName, Name + ".Grid.xml");

            try
            {
                if (System.IO.File.Exists(FileName))
                {
                    GridView1.RestoreLayoutFromXml(FileName);
                }
            }
#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168

            // Hook into the changes of the layout from this point
            GridView1.Layout += LayoutChanged;
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            string PathName = XMLBasePath();
            if (!System.IO.Directory.Exists(PathName))
            {
                System.IO.Directory.CreateDirectory(PathName);
            }

            string FileName = System.IO.Path.Combine(PathName, Name + ".Grid.xml");
            GridView1.SaveLayoutToXml(FileName);
        }
    }
}