using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.RPPS.Biller.Edit.Templates
{
	partial class EditRecordControl : DevExpress.XtraEditors.XtraUserControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;
		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.BarButtonItem_Change = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_Add = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem_Delete = new DevExpress.XtraBars.BarButtonItem();
			this.PopupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.PopupMenu1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			this.SuspendLayout();
			//
			//barManager1
			//
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarButtonItem_Change,
				this.BarButtonItem_Add,
				this.BarButtonItem_Delete
			});
			this.barManager1.MaxItemId = 3;
			//
			//BarButtonItem_Change
			//
			this.BarButtonItem_Change.Caption = "&Change...";
			this.BarButtonItem_Change.Id = 0;
			this.BarButtonItem_Change.Name = "BarButtonItem_Change";
			//
			//BarButtonItem_Add
			//
			this.BarButtonItem_Add.Caption = "&Add...";
			this.BarButtonItem_Add.Id = 1;
			this.BarButtonItem_Add.Name = "BarButtonItem_Add";
			//
			//BarButtonItem_Delete
			//
			this.BarButtonItem_Delete.Caption = "&Delete";
			this.BarButtonItem_Delete.Id = 2;
			this.BarButtonItem_Delete.Name = "BarButtonItem_Delete";
			//
			//PopupMenu1
			//
			this.PopupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Change),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Add),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem_Delete)
			});
			this.PopupMenu1.Manager = this.barManager1;
			this.PopupMenu1.Name = "PopupMenu1";
			//
			//GridControl1
			//
			this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GridControl1.EmbeddedNavigator.Name = "";
			this.GridControl1.Location = new System.Drawing.Point(0, 0);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.Size = new System.Drawing.Size(415, 260);
			this.GridControl1.TabIndex = 4;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
			//
			//EditRecordControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GridControl1);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "EditRecordControl";
			this.Size = new System.Drawing.Size(415, 260);
			((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.PopupMenu1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			this.ResumeLayout(false);
		}
		protected internal DevExpress.XtraBars.BarManager barManager1;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlTop;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlRight;
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraBars.PopupMenu PopupMenu1;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Change;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Add;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem_Delete;
	}
}
