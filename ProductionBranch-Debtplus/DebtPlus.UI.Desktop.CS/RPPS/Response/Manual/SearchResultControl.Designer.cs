using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
	partial class SearchResultControl : DevExpress.XtraEditors.XtraUserControl
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton2
			//
			this.SimpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton2.Location = new System.Drawing.Point(192, 246);
			this.SimpleButton2.Name = "SimpleButton2";
			this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton2.TabIndex = 2;
			this.SimpleButton2.Text = "&Cancel";
			//
			//SimpleButton1
			//
			this.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton1.Location = new System.Drawing.Point(96, 246);
			this.SimpleButton1.Name = "SimpleButton1";
			this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton1.TabIndex = 1;
			this.SimpleButton1.Text = "&Select";
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.Location = new System.Drawing.Point(4, 4);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.Size = new System.Drawing.Size(355, 226);
			this.GridControl1.TabIndex = 0;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			//
			//SearchResultControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GridControl1);
			this.Controls.Add(this.SimpleButton2);
			this.Controls.Add(this.SimpleButton1);
			this.Name = "SearchResultControl";
			this.Size = new System.Drawing.Size(362, 291);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		public DevExpress.XtraGrid.GridControl GridControl1;
	}
}
