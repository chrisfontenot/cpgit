using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
	partial class SelectTransaction : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.col_client = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_account_number = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_gross = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_net = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();

			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			this.SuspendLayout();

			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.EmbeddedNavigator.Name = "";
			this.GridControl1.Location = new System.Drawing.Point(2, 63);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.Size = new System.Drawing.Size(291, 162);
			this.GridControl1.TabIndex = 0;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.col_client,
				this.col_creditor,
				this.col_account_number,
				this.col_gross,
				this.col_net
			});
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsSelection.InvertSelection = true;
			this.GridView1.OptionsSelection.UseIndicatorForSelection = false;
			this.GridView1.OptionsView.ShowDetailButtons = false;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			//
			//col_client
			//
			this.col_client.Caption = "Client";
			this.col_client.CustomizationCaption = "Client ID";
			this.col_client.FieldName = "client";
			this.col_client.Name = "col_client";
			this.col_client.Visible = true;
			this.col_client.VisibleIndex = 0;
			//
			//col_creditor
			//
			this.col_creditor.Caption = "Creditor";
			this.col_creditor.CustomizationCaption = "Creditor ID";
			this.col_creditor.FieldName = "creditor";
			this.col_creditor.Name = "col_creditor";
			this.col_creditor.Visible = true;
			this.col_creditor.VisibleIndex = 1;
			//
			//col_account_number
			//
			this.col_account_number.Caption = "Account #";
			this.col_account_number.CustomizationCaption = "Account Number";
			this.col_account_number.FieldName = "account_number";
			this.col_account_number.Name = "col_account_number";
			this.col_account_number.Visible = true;
			this.col_account_number.VisibleIndex = 2;
			//
			//col_gross
			//
			this.col_gross.Caption = "Gross";
			this.col_gross.CustomizationCaption = "Gross Amount";
			this.col_gross.DisplayFormat.FormatString = "c";
			this.col_gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_gross.FieldName = "gross";
			this.col_gross.GroupFormat.FormatString = "c";
			this.col_gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_gross.Name = "col_gross";
			this.col_gross.Visible = true;
			this.col_gross.VisibleIndex = 3;
			//
			//col_net
			//
			this.col_net.Caption = "Net";
			this.col_net.CustomizationCaption = "Net Amount";
			this.col_net.DisplayFormat.FormatString = "c";
			this.col_net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_net.FieldName = "net";
			this.col_net.GroupFormat.FormatString = "c";
			this.col_net.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_net.Name = "col_net";
			this.col_net.Visible = true;
			this.col_net.VisibleIndex = 4;
			//
			//Button_OK
			//
			this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.Button_OK.Location = new System.Drawing.Point(60, 231);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 1;
			this.Button_OK.Text = "&OK";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(158, 231);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 2;
			this.Button_Cancel.Text = "&Cancel";
			//
			//LabelControl1
			//
			this.LabelControl1.Appearance.Options.UseTextOptions = true;
			this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl1.Location = new System.Drawing.Point(11, 12);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(269, 39);
			this.LabelControl1.TabIndex = 3;
			this.LabelControl1.Text = "There are multiple items which match the trace number that you provided. Please s" + "elect the item to be refunded from the following list.";
			this.LabelControl1.UseMnemonic = false;
			//
			//SelectTransaction
			//
			this.AcceptButton = this.Button_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.Controls.Add(this.GridControl1);
			this.Name = "SelectTransaction";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Select Transaction";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();

			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		protected internal DevExpress.XtraGrid.Columns.GridColumn col_client;
		protected internal DevExpress.XtraGrid.Columns.GridColumn col_creditor;
		protected internal DevExpress.XtraGrid.Columns.GridColumn col_account_number;
		protected internal DevExpress.XtraGrid.Columns.GridColumn col_gross;
		protected internal DevExpress.XtraGrid.Columns.GridColumn col_net;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
	}
}
