#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
    public partial class SearchInputControl
    {
        public SearchInputControl() : base()
        {
            InitializeComponent();
            LookupEdit3.Resize += LookupEdit3_Resize;
            LookupEdit3.ProcessNewValue += LookupEdit3_ProcessNewValue;
            LookupEdit3.EditValueChanged += TextEdit1_EditValueChanged;
            LookupEdit2.EditValueChanged += LookupEdit2_EditValueChanged;
            LookUpEdit1.EditValueChanged += LookUpEdit1_EditValueChanged;
            SimpleButton2.Click += SimpleButton2_Click;
            SimpleButton1.Click += SimpleButton1_Click;
            this.Load += SearchInputControl_Load;
        }

        public partial class SelectedArgs : System.EventArgs
        {
            public SelectedArgs() : base()
            {
            }

            public Int32 rpps_transaction;
            public Int32 client;
            public string account_number;
            public string creditor;
            public decimal gross;
            public decimal deduct;

            public string creditor_type;

            public decimal Net
            {
                get
                {
                    if (creditor_type == "D")
                    {
                        return gross - deduct;
                    }
                    return gross;
                }
            }
        }

        public delegate void SelectedEventHandler(object Sender, SelectedArgs e);

        public event EventHandler Cancelled;

        public event SelectedEventHandler Selected;

        public string TraceNumber = string.Empty;
        // If we are entering an account number then we need to have it defined in the table. So, here it is.

        private System.Data.DataRowView AccountNumberRow;

        private void SimpleButton1_Click(System.Object sender, System.EventArgs e)
        {
            System.Data.DataRowView drv = (System.Data.DataRowView)LookupEdit3.GetSelectedDataRow();
            SelectedArgs args = new SelectedArgs();
            args.rpps_transaction = Convert.ToInt32(drv["rpps_transaction"]);
            args.account_number = Convert.ToString(drv["account_number"]);
            args.client = Convert.ToInt32(drv["client"]);
            args.creditor = Convert.ToString(drv["creditor"]);
            args.creditor_type = Convert.ToString(drv["creditor_type"]);
            args.deduct = Convert.ToDecimal(drv["fairshare_amt"]);
            args.gross = Convert.ToDecimal(drv["debit_amt"]);
            if (Selected != null)
            {
                Selected(this, args);
            }
        }

        private void SimpleButton2_Click(object sender, System.EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        public void RefreshData()
        {
            // Restore the previously selected file
            if (LookUpEdit1.EditValue == null || object.ReferenceEquals(LookUpEdit1.EditValue, System.DBNull.Value))
            {
                LookUpEdit1.EditValue = Storage.LastFile;

                LookUpEdit2_LoadItems();
                LookupEdit2.EditValue = Storage.LastBatch;
            }

            SimpleButton1.Enabled = !HasErrors();
        }

        private bool HasErrors()
        {
            if (LookUpEdit1.EditValue == null || object.ReferenceEquals(LookUpEdit1.EditValue, System.DBNull.Value))
            {
                return true;
            }

            if (LookupEdit2.EditValue == null || object.ReferenceEquals(LookupEdit2.EditValue, System.DBNull.Value))
            {
                return true;
            }

            if (LookupEdit3.Text.Trim().Length == 0)
            {
                return true;
            }

            return false;
        }

        private void SearchInputControl_Load(object sender, System.EventArgs e)
        {
            // Load the waiting cursor before reading the database
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                System.Data.DataTable FilesTable = rpps_files();
                System.Data.DataTable BatchesTable = rpps_batches();

                // Load the data relation between the batches/files if it is missing
                System.Data.DataRelation rel = Storage.ds.Relations["rpps_files_rpps_batches"];
                if (rel == null)
                {
                    rel = new System.Data.DataRelation("rpps_files_rpps_batches", FilesTable.Columns["rpps_file"], BatchesTable.Columns["rpps_file"]);
                    Storage.ds.Relations.Add(rel);
                }

                // Load the control with the list of rpps files
                LookUpEdit1.Properties.DataSource = new System.Data.DataView(FilesTable, string.Empty, "[date_created] desc, [rpps_file] desc", DataViewRowState.CurrentRows);
                LookUpEdit2_LoadItems();
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        private System.Data.DataTable rpps_files()
        {
            System.Data.DataTable tbl = Storage.ds.Tables["rpps_files"];
            if (tbl == null)
            {
                try
                {
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "SELECT [rpps_file],[bank],[date_created],[created_by] FROM rpps_files WHERE [file_type]='EFT'";
                        cmd.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.AcceptChangesDuringFill = true;
                            da.Fill(Storage.ds, "rpps_files");
                        }
                    }

                    tbl = Storage.ds.Tables["rpps_files"];
                    tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["rpps_file"] };
                }
                catch (Exception ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_files table");
                }
            }

            return tbl;
        }

        private System.Data.DataTable rpps_batches()
        {
            System.Data.DataTable tbl = Storage.ds.Tables["rpps_batches"];
            if (tbl == null)
            {
                try
                {
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "SELECT b.[rpps_batch],b.[rpps_file],b.[rpps_biller_id],b.[biller_name] FROM rpps_batches b INNER JOIN rpps_files f on f.rpps_file = b.rpps_file WHERE f.[file_type]='EFT'";
                        cmd.CommandType = CommandType.Text;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.AcceptChangesDuringFill = true;
                            da.Fill(Storage.ds, "rpps_batches");
                        }

                        tbl = Storage.ds.Tables["rpps_batches"];
                        tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["rpps_batch"] };
                    }
                }
                catch (Exception ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_batches table");
                }
            }

            return tbl;
        }

        private System.Data.DataView rpps_transactions(Int32 rpps_batch)
        {
            System.Data.DataTable tbl = Storage.ds.Tables["rpps_transactions"];

            // If there is a table then determine if there are transactions for this batch. If so, we have been here before....
            if (tbl != null)
            {
                System.Data.DataView vue = new System.Data.DataView(tbl, string.Format("[rpps_batch]={0:f0}", rpps_batch), "account_number", DataViewRowState.CurrentRows);
                if (vue.Count > 0)
                    return vue;
                vue.Dispose();
            }

            // Read the transactions for that batch
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT t.[rpps_transaction], t.[rpps_batch], t.[rpps_file], rrc.[account_number], t.[trace_number_first], rrc.client, rrc.creditor, rrc.debit_amt, rrc.creditor_type, rrc.fairshare_amt FROM rpps_transactions t INNER JOIN registers_client_creditor rrc on t.client_creditor_register = rrc.client_creditor_register WHERE t.[rpps_batch]=@rpps_batch";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@rpps_batch", SqlDbType.Int).Value = rpps_batch;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.AcceptChangesDuringFill = true;
                        da.Fill(Storage.ds, "rpps_transactions");
                    }
                }

                tbl = Storage.ds.Tables["rpps_transactions"];
                tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["rpps_transaction"] };
                if (!tbl.Columns.Contains("deduct"))
                {
                    tbl.Columns.Add("deduct", typeof(decimal), "iif([creditor_type]='D',fairshare_amt,0)");
                }

                if (!tbl.Columns.Contains("net"))
                {
                    tbl.Columns.Add("net", typeof(decimal), "[debit_amt]-[deduct]");
                }
            }
            catch (Exception ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_transactions table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            // Return the view to the data
            return new System.Data.DataView(tbl, string.Format("[rpps_batch]={0:f0}", rpps_batch), "account_number", DataViewRowState.CurrentRows);
        }

        private void LookUpEdit1_EditValueChanged(object sender, System.EventArgs e)
        {
            Storage.LastFile = LookUpEdit1.EditValue;
            LookUpEdit2_LoadItems();
        }

        private void LookUpEdit2_LoadItems()
        {
            System.Data.DataView vue = null;

            if (LookUpEdit1.EditValue == null || object.ReferenceEquals(LookUpEdit1.EditValue, System.DBNull.Value))
            {
                vue = null;
            }
            else
            {
                vue = new System.Data.DataView(rpps_batches(), string.Format("[rpps_file]='{0}'", LookUpEdit1.EditValue), "rpps_biller_id", DataViewRowState.CurrentRows);
            }

            LookupEdit2.Properties.DataSource = vue;
            SimpleButton1.Enabled = !HasErrors();
        }

        private void TextEdit1_EditValueChanged(System.Object sender, System.EventArgs e)
        {
            CalcEdit1.EditValue = LookupEdit3.GetColumnValue("debit_amt");
            CalcEdit2.EditValue = LookupEdit3.GetColumnValue("net");
            TraceNumber = global::DebtPlus.Utils.Nulls.DStr(LookupEdit3.GetColumnValue("trace_number_first"));

            SimpleButton1.Enabled = !HasErrors();
        }

        private void LookupEdit2_EditValueChanged(object sender, System.EventArgs e)
        {
            Storage.LastBatch = LookupEdit2.EditValue;
            LookupEdit3_LoadItems();
            SimpleButton1.Enabled = !HasErrors();
        }

        private void LookupEdit3_LoadItems()
        {
            System.Int32 BatchID = 0;
            if (LookupEdit2.EditValue != null && !object.ReferenceEquals(LookupEdit2.EditValue, System.DBNull.Value))
            {
                BatchID = Convert.ToInt32(LookupEdit2.GetColumnValue("rpps_batch"));
            }
            else
            {
                BatchID = -1;
            }

            if (BatchID <= 0)
            {
                LookupEdit3.EditValue = null;
                LookupEdit3.Properties.DataSource = null;
            }
            else
            {
                System.Data.DataView vue = rpps_transactions(BatchID);
                LookupEdit3.Properties.DataSource = vue;
            }
        }

        private void LookupEdit3_ProcessNewValue(object sender, DevExpress.XtraEditors.Controls.ProcessNewValueEventArgs e)
        {
            // If there is a batch then you must choose one from the list of items. You can't just type a value in.
            if (LookupEdit2.EditValue == null || object.ReferenceEquals(LookupEdit2.EditValue, System.DBNull.Value))
            {
                System.Data.DataView vue = (System.Data.DataView)LookupEdit3.Properties.DataSource;

                if (vue == null)
                {
                    // There is no pending view. Is there a table that we can use?
                    System.Data.DataTable tbl = Storage.ds.Tables["rpps_transactions"];
                    if (tbl == null)
                    {
                        try
                        {
                            // No table either. Define the schema for the table but leave it empty.
                            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                                cmd.CommandText = "SELECT t.[rpps_transaction], t.[rpps_batch], t.[rpps_file], rrc.[account_number], t.[trace_number_first], rrc.client, rrc.debit_amt, rrc.creditor_type, rrc.fairshare_amt FROM rpps_transactions t INNER JOIN registers_client_creditor rrc on t.client_creditor_register = rrc.client_creditor_register";
                                cmd.CommandType = CommandType.Text;

                                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                                {
                                    da.AcceptChangesDuringFill = true;
                                    da.FillSchema(Storage.ds, SchemaType.Source, "rpps_transactions");
                                }
                            }

                            tbl = Storage.ds.Tables["rpps_transactions"];
                            tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["rpps_transaction"] };
                            if (!tbl.Columns.Contains("deduct"))
                            {
                                tbl.Columns.Add("deduct", typeof(decimal), "iif([creditor_type]='D',fairshare_amt,0)");
                            }

                            if (!tbl.Columns.Contains("net"))
                            {
                                tbl.Columns.Add("net", typeof(decimal), "[debit_amt]-[deduct]");
                            }
                        }
                        catch (Exception ex)
                        {
                            global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading rpps_transactions table");
                        }
                    }

                    vue = new System.Data.DataView(tbl, "[rpps_batch] IS NULL", "account_number", DataViewRowState.CurrentRows);
                    LookupEdit3.Properties.DataSource = vue;
                }

                // If there is no added row then add a new row to the list.
                if (AccountNumberRow == null)
                {
                    AccountNumberRow = vue.AddNew();
                    AccountNumberRow.BeginEdit();

                    // Specify default values for the dummy row
                    AccountNumberRow["rpps_transaction"] = -1;
                    AccountNumberRow["trace_number_first"] = string.Empty;
                    AccountNumberRow["client"] = -1;
                    AccountNumberRow["debit_amt"] = 0m;
                    AccountNumberRow["creditor_type"] = "N";
                    AccountNumberRow["fairshare_amt"] = 0m;
                    AccountNumberRow["rpps_batch"] = System.DBNull.Value;
                }

                // Set the account number in the new row and return success to the caller.
                AccountNumberRow["account_number"] = e.DisplayValue;
                e.Handled = true;
            }
        }

        private void LookupEdit3_Resize(object sender, System.EventArgs e)
        {
            LookupEdit3.Properties.PopupWidth = LookupEdit3.Width + 280;
        }
    }
}