#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
    internal partial class MainForm
    {
        protected ArgParser ap;
        private System.Int32 rpps_transaction = -1;

        private bool PendingTransactions;

        public MainForm() : this((ArgParser)null)
        {
        }

        public MainForm(ArgParser ap) : base()
        {
            InitializeComponent();

            this.ap = ap;

            FormClosing                  += Form_FormClosing;
            Load                         += Form_Load;
            LookUpEdit1.EditValueChanged += LookUpEdit1_EditValueChanged;
            LookUpEdit2.EditValueChanged += TextEdit1_Validated;
            TextEdit1.Validated          += TextEdit1_Validated;
            SimpleButton2.Click          += SimpleButton2_Click;
            SimpleButton1.Click          += SimpleButton1_Click;
            TextEdit1.ButtonPressed      += TextEdit1_ButtonPressed;
            TextEdit1.EditValueChanged   += FieldChanged;
            LookUpEdit1.EditValueChanged += FieldChanged;
            LookUpEdit2.EditValueChanged += FieldChanged;
        }

        private void SimpleButton2_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private readonly System.Data.DataSet dsErrors = new System.Data.DataSet("dsErrors");

        private void Form_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (PendingTransactions)
            {
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_rpps_response_process_post";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                        cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = ap.rpps_response_file;
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error posting the response file");
                }
                finally
                {
                    if (cn != null)
                        cn.Dispose();
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }
        }

        private void Form_Load(object sender, System.EventArgs e)
        {
            // Invalidate the fields
            client.Text         = string.Empty;
            creditor.Text       = string.Empty;
            account_number.Text = string.Empty;
            gross.Text          = string.Empty;
            net.Text            = string.Empty;

            // Invalidate the transaction at the start
            rpps_transaction = -1;

            // Load the list of error codes
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT [rpps_reject_code],[description],[resend_funds] FROM rpps_reject_codes WITH (NOLOCK)";
                    cmd.CommandType = CommandType.Text;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(dsErrors, "rpps_reject_codes");
                        var tbl = dsErrors.Tables["rpps_reject_codes"];
                        tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["rpps_reject_code"] };
                        if (!tbl.Columns.Contains("formatted_rpps_reject_code"))
                        {
                            tbl.Columns.Add("formatted_rpps_reject_code", typeof(string), "[rpps_reject_code] + isnull(' - ' + [description],'')");
                        }
                    }
                }

                LookUpEdit1.Properties.Columns.Clear();
                LookUpEdit1.Properties.DataSource = dsErrors.Tables["rpps_reject_codes"].DefaultView;
                this.LookUpEdit1.Properties.ShowFooter = false;
                this.LookUpEdit1.Properties.DisplayMember = "formatted_rpps_reject_code";
                this.LookUpEdit1.Properties.ValueMember = "rpps_reject_code";
                this.LookUpEdit1.Properties.PopupWidth = LookUpEdit1.Width + 130;
                this.LookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_reject_code", 20, "Error"), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 60, "Description") });

                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT [bank],[description],[immediate_origin],[Default],[ActiveFlag] FROM banks WITH (NOLOCK) WHERE type = 'R'";
                    cmd.CommandType = CommandType.Text;
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(dsErrors, "banks");
                    }

                    var tbl = dsErrors.Tables["banks"];
                    tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["bank"] };
                    if (!tbl.Columns.Contains("formatted_description"))
                    {
                        tbl.Columns.Add("formatted_description", typeof(string), "isnull([immediate_origin],'') + isnull(' - ' + [description],'')");
                    }
                }

                LookUpEdit2.Properties.Columns.Clear();
                LookUpEdit2.Properties.DataSource = dsErrors.Tables["banks"].DefaultView;
                this.LookUpEdit2.Properties.ShowFooter = false;
                this.LookUpEdit2.Properties.DisplayMember = "formatted_description";
                this.LookUpEdit2.Properties.ValueMember = "bank";
                this.LookUpEdit2.Properties.PopupWidth = LookUpEdit1.Width + 130;
                this.LookUpEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("bank", 20, "ID"), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 60, "Description"), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("immediate_origin", 20, "Prefix Code") });
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading RPPS reject codes");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        private void LookUpEdit1_EditValueChanged(object sender, System.EventArgs e)
        {
            SimpleButton1.Enabled = IsValid();
        }

        private void TextEdit1_ButtonPressed(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            using (TraceNumberSearchForm frm = new TraceNumberSearchForm())
            {
                frm.SearchInputControl1.Selected += SelectedHandler;
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    TextEdit1.Text = frm.TraceNumber;
                }
                frm.SearchInputControl1.Selected -= SelectedHandler;
            }
        }

        private void SelectedHandler(object Sender, SearchInputControl.SelectedArgs e)
        {
            rpps_transaction      = e.rpps_transaction;
            client.Text           = string.Format("{0:0000000}", e.client);
            account_number.Text   = e.account_number;
            creditor.Text         = e.creditor;
            gross.Text            = string.Format("{0:c}", e.gross);
            net.Text              = string.Format("{0:c}", e.Net);
            SimpleButton1.Enabled = IsValid();
        }

        private void TextEdit1_Validated(object sender, System.EventArgs e)
        {
            // Do validation only when we have not found the transaction

            if (rpps_transaction <= 0)
            {
                // Invalidate the fields
                client.Text         = string.Empty;
                creditor.Text       = string.Empty;
                account_number.Text = string.Empty;
                gross.Text          = string.Empty;
                net.Text            = string.Empty;

                // Find the prefix to the trace number
                System.Int32 bank = -1;
                if (LookUpEdit2.EditValue != null && !object.ReferenceEquals(LookUpEdit2.EditValue, System.DBNull.Value))
                    bank = Convert.ToInt32(LookUpEdit2.EditValue);
                if (bank <= 0)
                    return;

                // If there is no trace number then terminate
                string trace_number = global::DebtPlus.Utils.Nulls.DStr(TextEdit1.Text.Trim());
                if (trace_number == string.Empty)
                    return;

                // Take only the last seven digits of the trace number.
                trace_number = trace_number.PadLeft(7, '0');
                if (trace_number.Length > 7)
                {
                    trace_number = trace_number.Substring(trace_number.Length - 7);
                }

                // Lookup the trace number in the system.
                using (System.Data.DataSet ds = new System.Data.DataSet("ds"))
                {
                    System.Data.DataTable tbl = null;
                    System.Data.DataRowView drv = null;

                    System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                    try
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            cmd.CommandText = "SELECT t.rpps_transaction, t.bank, t.client, t.creditor, t.client_creditor, t.biller_id, t.trace_number_first, t.trace_number_last, t.rpps_file, t.rpps_batch, t.transaction_code, t.service_class_or_purpose, t.return_code, t.client_creditor_register, t.client_creditor_proposal, t.death_date, t.old_account_number, t.new_account_number, t.old_balance, t.response_batch_id, t.prenote_status, t.debt_note, t.date_created, t.created_by, rcc.debit_amt as gross, case rcc.creditor_type when 'D' then rcc.debit_amt - rcc.fairshare_amt else rcc.debit_amt end as net, rcc.creditor, rcc.client, rcc.account_number, rcc.date_created as disbursement_date, v.name, cr.creditor_name FROM rpps_transactions t WITH (NOLOCK) LEFT OUTER JOIN registers_client_creditor rcc WITH (NOLOCK) on t.client_creditor_register = rcc.client_creditor_register LEFT OUTER JOIN view_client_address v WITH (NOLOCK) ON rcc.client = v.client LEFT OUTER JOIN creditors cr WITH (NOLOCK) ON rcc.creditor = cr.creditor WHERE t.trace_number_first like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]' AND t.bank = @bank AND @trace_number BETWEEN right(t.trace_number_first,7) AND right(t.trace_number_last, 7) AND t.service_class_or_purpose = 'CIE'";
                            cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank;
                            cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 7).Value = trace_number;
                            cmd.CommandType = CommandType.Text;

                            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.Fill(ds, "transactions");
                                tbl = ds.Tables[0];
                            }
                        }

                        // Look for obvious error conditions
                        string ErrorMessage = string.Empty;
                        using (System.Data.DataView vue = new System.Data.DataView(tbl, "[return_code] IS NULL", string.Empty, DataViewRowState.CurrentRows))
                        {
                            // If there are no rows then there is no trace number
                            if (tbl.Rows.Count == 0)
                            {
                                ErrorMessage = "The trace number does not exist in the system";

                                // If the view is empty then the trace number has been rejected.
                            }
                            else if (vue.Count == 0)
                            {
                                ErrorMessage = "The item has already been rejected.";

                                // If only one trustRegister then we have the trustRegister
                            }
                            else if (vue.Count == 1)
                            {
                                drv = vue[0];
                            }
                            else
                            {
                                // Since there are more than one, ask the user for the specific item.
                                using (SelectTransaction frm = new SelectTransaction(vue))
                                {
                                    if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                    {
                                        drv = frm.drv;
                                    }
                                }
                            }

                            // Supply the current values for the gross and net figures
                            if (drv != null)
                            {
                                rpps_transaction = Convert.ToInt32(drv["rpps_transaction"]);

                                if (!object.ReferenceEquals(drv["gross"], System.DBNull.Value))
                                    gross.Text = string.Format("{0:c}", Convert.ToDecimal(drv["gross"]));
                                if (!object.ReferenceEquals(drv["net"], System.DBNull.Value))
                                    net.Text = string.Format("{0:c}", Convert.ToDecimal(drv["net"]));
                                if (!object.ReferenceEquals(drv["account_number"], System.DBNull.Value))
                                    account_number.Text = Convert.ToString(drv["account_number"]);

                                // Add the creditor to the form
                                string string_creditor = string.Empty;
                                string string_creditor_name = string.Empty;
                                if (!object.ReferenceEquals(drv["creditor"], System.DBNull.Value))
                                    string_creditor = Convert.ToString(drv["creditor"]);
                                if (!object.ReferenceEquals(drv["creditor_name"], System.DBNull.Value))
                                    string_creditor_name = Convert.ToString(drv["creditor_name"]);
                                if (string_creditor != string.Empty && string_creditor_name != string.Empty)
                                {
                                    creditor.Text = string.Format("{0} {1}", string_creditor, string_creditor_name);
                                }
                                else
                                {
                                    creditor.Text = string_creditor + string_creditor_name;
                                }

                                // Add the client name and id to the form
                                string string_client = string.Empty;
                                string string_name = string.Empty;
                                if (!object.ReferenceEquals(drv["client"], System.DBNull.Value))
                                    string_client = string.Format("{0:0000000}", Convert.ToInt32(drv["ClientId"]));
                                if (!object.ReferenceEquals(drv["name"], System.DBNull.Value))
                                    string_name = Convert.ToString(drv["name"]);
                                if (string_client != string.Empty && string_name != string.Empty)
                                {
                                    client.Text = string.Format("{0} {1}", string_client, string_name);
                                }
                                else
                                {
                                    client.Text = string_client + string_name;
                                }
                            }
                        }

                        TextEdit1.ErrorText = ErrorMessage;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading RPPS transactions");
                        System.Windows.Forms.Cursor.Current = current_cursor;
                    }
                }
            }
        }

        private void FieldChanged(object Sender, System.EventArgs e)
        {
            SimpleButton1.Enabled = IsValid();
        }

        private bool IsValid()
        {
            // Ensure that there is an error code
            string ErrorCode = string.Empty;
            if (LookUpEdit1.ErrorText != string.Empty)
            {
                return false;
            }
            if (LookUpEdit1.EditValue != null)
                ErrorCode = Convert.ToString(LookUpEdit1.EditValue);
            if (ErrorCode == string.Empty)
            {
                return false;
            }

            // Ensure that there is a bank
            System.Int32 bank = -1;
            if (LookUpEdit2.ErrorText != string.Empty)
            {
                return false;
            }
            if (LookUpEdit2.EditValue != null)
                bank = Convert.ToInt32(LookUpEdit2.EditValue);
            if (bank <= 0)
            {
                return false;
            }

            // There should be a transaction
            if (rpps_transaction <= 0)
            {
                return false;
            }

            // Finally, things are valid.
            return true;
        }

        private void SimpleButton1_Click(object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;

            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                cn.Open();

                // Generate the transaction to void the item
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_Manual";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = ap.rpps_response_file;
                    cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int).Value = rpps_transaction;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 4).Value = Convert.ToString(LookUpEdit1.EditValue);
                    cmd.Parameters.Add("@disbursement_message", SqlDbType.Int).Value = 0;
                    cmd.ExecuteNonQuery();

                    // Enable the "Do you Wish to post?" function.
                    PendingTransactions = true;
                }

                // Invalidate the fields
                client.Text           = string.Empty;
                creditor.Text         = string.Empty;
                account_number.Text   = string.Empty;
                gross.Text            = string.Empty;
                net.Text              = string.Empty;

                TextEdit1.EditValue   = null;
                rpps_transaction      = -1;
                SimpleButton1.Enabled = false;

                // Set the focus to the trace number
                TextEdit1.Focus();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing transaction");
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }
    }
}