#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
    public partial class SelectTransaction
    {
        private readonly System.Data.DataView vue;
        public System.Data.DataRowView drv;

        public SelectTransaction(System.Data.DataView vue) : base()
        {
            InitializeComponent();
            this.Load += SelectTransaction_Load;

            this.vue = vue;
        }

        private void SelectTransaction_Load(object sender, System.EventArgs e)
        {
            // Load the grid with the possible responses
            var _with1 = GridControl1;
            _with1.DataSource = vue;
            _with1.Refresh();

            // Add the handlers to the processing logic
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridControl1.DoubleClick += GridControl_DoubleClick;

            // Enable processing to find the appropriate item
            GridView1.FocusedRowHandle = GridView1.GetRowHandle(0);
            Perform_FocusedRowHandleChanged(GridView1.FocusedRowHandle);
        }

        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            Perform_FocusedRowHandleChanged(e.FocusedRowHandle);
        }

        private void Perform_FocusedRowHandleChanged(System.Int32 NewRowHandle)
        {
            try
            {
                // If there is a row then process the double-click event. Ignore it if there is no row.
                if (NewRowHandle >= 0)
                {
                    drv = (System.Data.DataRowView)GridView1.GetRow(NewRowHandle);
                }
            }
            catch
            {
                drv = null;
            }

            // Enable the OK button if the creditor is specified
            Button_OK.Enabled = drv != null;
        }

        private void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            if (Button_OK.Enabled)
            {
                // Find the row targetted as the double-click item
                DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
                DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
                DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

                // If there is a row then process the double-click event. Ignore it if there is no row.
                System.Int32 ControlRow = hi.RowHandle;
                if (ControlRow >= 0)
                {
                    drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
                }

                // From the datasource, we can find the client in the table.
                if (drv != null)
                {
                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
            }
        }

        private void GridControl_DoubleClick(object sender, System.EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)sender;
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)ctl.DefaultView;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));

            // If there is a row then process the double-click event. Ignore it if there is no row.
            System.Int32 ControlRow = hi.RowHandle;
            if (ControlRow >= 0)
            {
                drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
            }

            // From the datasource, we can find the client in the table.
            if (drv != null)
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}