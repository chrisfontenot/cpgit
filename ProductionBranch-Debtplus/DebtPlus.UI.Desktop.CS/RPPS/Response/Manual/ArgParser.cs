using System;
using System.Collections;
using System.Windows.Forms;
using DebtPlus.UI.FormLib.RPPS.New;

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        private ArrayList pathnames = new ArrayList();

        /// <summary>
        /// RPPS Response file
        /// </summary>
        private System.Int32 _rpps_response_file = -1;

        public System.Int32 rpps_response_file
        {
            get { return _rpps_response_file; }
            set { _rpps_response_file = value; }
        }

        /// <summary>
        /// Process an option
        /// </summary>
        protected override global::DebtPlus.Utils.ArgParserBase.SwitchStatus OnSwitch(string switchName, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            switch (switchName)
            {
                case "b":
                    System.Int32 tempInteger = 0;
                    if (Int32.TryParse(switchValue, out tempInteger))
                    {
                        if (tempInteger > 0)
                        {
                            rpps_response_file = tempInteger;
                            ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
                        }
                    }
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;
            }

            return ss;
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] { "b" })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        /// <summary>
        /// Called after all of the arguments have been parsed
        /// </summary>
        protected override global::DebtPlus.Utils.ArgParserBase.SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            // Ask for the response batch to use
            if (rpps_response_file <= 0)
            {
                using (BatchForm frm = new BatchForm())
                {
                    var _with1 = frm;
                    if (_with1.ShowDialog() == DialogResult.OK)
                        rpps_response_file = _with1.rpps_response_file;
                }

                if (rpps_response_file <= 0)
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }

            return ss;
        }

        //OnDoneParse
    }
}