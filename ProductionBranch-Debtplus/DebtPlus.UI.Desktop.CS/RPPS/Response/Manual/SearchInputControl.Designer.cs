using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
	partial class SearchInputControl : DevExpress.XtraEditors.XtraUserControl
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.CalcEdit2 = new DevExpress.XtraEditors.CalcEdit();
			this.CalcEdit1 = new DevExpress.XtraEditors.CalcEdit();
			this.LookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
			this.LookupEdit2 = new DevExpress.XtraEditors.LookUpEdit();
			this.LookupEdit3 = new DevExpress.XtraEditors.LookUpEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit2.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit2.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit3.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton1
			//
			this.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton1.Enabled = false;
			this.SimpleButton1.Location = new System.Drawing.Point(96, 246);
			this.SimpleButton1.Name = "SimpleButton1";
			this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton1.TabIndex = 2;
			this.SimpleButton1.Text = "&OK";
			//
			//SimpleButton2
			//
			this.SimpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton2.Location = new System.Drawing.Point(192, 246);
			this.SimpleButton2.Name = "SimpleButton2";
			this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton2.TabIndex = 3;
			this.SimpleButton2.Text = "&Cancel";
			//
			//LayoutControl1
			//
			this.LayoutControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LayoutControl1.Controls.Add(this.CalcEdit2);
			this.LayoutControl1.Controls.Add(this.CalcEdit1);
			this.LayoutControl1.Controls.Add(this.LookUpEdit1);
			this.LayoutControl1.Controls.Add(this.LookupEdit2);
			this.LayoutControl1.Controls.Add(this.LookupEdit3);
			this.LayoutControl1.Location = new System.Drawing.Point(4, 4);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(353, 224);
			this.LayoutControl1.TabIndex = 4;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//CalcEdit2
			//
			this.CalcEdit2.Location = new System.Drawing.Point(97, 108);
			this.CalcEdit2.Name = "CalcEdit2";
			this.CalcEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit2.Properties.Mask.BeepOnError = true;
			this.CalcEdit2.Properties.Mask.EditMask = "c";
			this.CalcEdit2.Properties.Precision = 2;
			this.CalcEdit2.Size = new System.Drawing.Size(244, 20);
			this.CalcEdit2.StyleController = this.LayoutControl1;
			this.CalcEdit2.TabIndex = 7;
			//
			//CalcEdit1
			//
			this.CalcEdit1.Location = new System.Drawing.Point(97, 84);
			this.CalcEdit1.Name = "CalcEdit1";
			this.CalcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit1.Properties.Mask.BeepOnError = true;
			this.CalcEdit1.Properties.Mask.EditMask = "c";
			this.CalcEdit1.Properties.Precision = 2;
			this.CalcEdit1.Size = new System.Drawing.Size(244, 20);
			this.CalcEdit1.StyleController = this.LayoutControl1;
			this.CalcEdit1.TabIndex = 6;
			//
			//LookUpEdit1
			//
			this.LookUpEdit1.Location = new System.Drawing.Point(97, 12);
			this.LookUpEdit1.Name = "LookUpEdit1";
			this.LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_file", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("date_created", "Date", 20, DevExpress.Utils.FormatType.DateTime, "d", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Descending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("created_by", "Creator", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit1.Properties.DisplayFormat.FormatString = "d";
			this.LookUpEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.LookUpEdit1.Properties.DisplayMember = "date_created";
			this.LookUpEdit1.Properties.NullText = "";
			this.LookUpEdit1.Properties.ShowFooter = false;
			this.LookUpEdit1.Properties.ValueMember = "rpps_file";
			this.LookUpEdit1.Size = new System.Drawing.Size(244, 20);
			this.LookUpEdit1.StyleController = this.LayoutControl1;
			this.LookUpEdit1.TabIndex = 4;
			this.LookUpEdit1.Properties.SortColumnIndex = 1;
			//
			//LookupEdit2
			//
			this.LookupEdit2.Location = new System.Drawing.Point(97, 36);
			this.LookupEdit2.Name = "LookupEdit2";
			this.LookupEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookupEdit2.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_biller_id", "ID", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("biller_name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookupEdit2.Properties.DisplayMember = "rpps_biller_id";
			this.LookupEdit2.Properties.MaxLength = 15;
			this.LookupEdit2.Properties.NullText = "";
			this.LookupEdit2.Properties.ShowFooter = false;
			this.LookupEdit2.Properties.ValueMember = "rpps_biller_id";
			this.LookupEdit2.Size = new System.Drawing.Size(244, 20);
			this.LookupEdit2.StyleController = this.LayoutControl1;
			this.LookupEdit2.TabIndex = 8;
			this.LookupEdit2.TabStop = false;
			this.LookupEdit2.Properties.SortColumnIndex = 1;
			//
			//LookupEdit3
			//
			this.LookupEdit3.Location = new System.Drawing.Point(97, 60);
			this.LookupEdit3.Name = "LookupEdit3";
			this.LookupEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookupEdit3.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("trace_number_first", "Trace #", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("client", "Client", 20, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("account_number", "Account #", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("debit_amt", "Gross", 20, DevExpress.Utils.FormatType.Numeric, "c", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("net", "Net", 20, DevExpress.Utils.FormatType.Numeric, "c", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("rpps_transaction", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default)
			});
			this.LookupEdit3.Properties.DisplayMember = "account_number";
			this.LookupEdit3.Properties.MaxLength = 22;
			this.LookupEdit3.Properties.NullText = "";
			this.LookupEdit3.Properties.ShowFooter = false;
			this.LookupEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
			this.LookupEdit3.Properties.ValueMember = "rpps_transaction";
			this.LookupEdit3.Size = new System.Drawing.Size(244, 20);
			this.LookupEdit3.StyleController = this.LayoutControl1;
			this.LookupEdit3.TabIndex = 5;
			this.LookupEdit3.TabStop = false;
			this.LookupEdit3.Properties.SortColumnIndex = 1;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(353, 224);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.LookUpEdit1;
			this.LayoutControlItem1.CustomizationFormText = "Output RPPS File";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(333, 24);
			this.LayoutControlItem1.Text = "Output RPPS File";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.LookupEdit3;
			this.LayoutControlItem2.CustomizationFormText = "Account Number";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(333, 24);
			this.LayoutControlItem2.Text = "Account Number";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.CalcEdit1;
			this.LayoutControlItem3.CustomizationFormText = "Gross Amount";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 72);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(333, 24);
			this.LayoutControlItem3.Text = "Gross Amount";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.CalcEdit2;
			this.LayoutControlItem4.CustomizationFormText = "Net Amount";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 96);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(333, 108);
			this.LayoutControlItem4.Text = "Net Amount";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(81, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.LookupEdit2;
			this.LayoutControlItem5.CustomizationFormText = "RPPS Biller ID";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(333, 24);
			this.LayoutControlItem5.Text = "RPPS Biller ID";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(81, 13);
			//
			//SearchInputControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.LayoutControl1);
			this.Controls.Add(this.SimpleButton2);
			this.Controls.Add(this.SimpleButton1);
			this.Name = "SearchInputControl";
			this.Size = new System.Drawing.Size(362, 291);
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.CalcEdit2.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit2.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit3.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		public DevExpress.XtraEditors.CalcEdit CalcEdit2;
		public DevExpress.XtraEditors.CalcEdit CalcEdit1;
		public DevExpress.XtraEditors.LookUpEdit LookUpEdit1;
		protected internal DevExpress.XtraEditors.LookUpEdit LookupEdit2;
		protected internal DevExpress.XtraEditors.LookUpEdit LookupEdit3;
	}
}
