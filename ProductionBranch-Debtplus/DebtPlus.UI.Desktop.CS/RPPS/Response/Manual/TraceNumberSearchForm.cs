#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
    public partial class TraceNumberSearchForm
    {
        public TraceNumberSearchForm() : base()
        {
            InitializeComponent();
            this.Load += TraceNumberSearchForm_Load;
            SearchInputControl1.Cancelled += SearchInputControl1_Cancelled;
            SearchResultControl1.Cancelled += SearchResultControl1_Cancelled;
            SearchInputControl1.Selected += SearchInputControl1_Selected;
        }

        private string privateTraceNumber = string.Empty;

        public string TraceNumber
        {
            get { return privateTraceNumber; }
            set { privateTraceNumber = value; }
        }

        private void TraceNumberSearchForm_Load(object sender, System.EventArgs e)
        {
            SwitchToInput();
        }

        private void SearchInputControl1_Cancelled(object sender, System.EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void SearchResultControl1_Cancelled(object sender, System.EventArgs e)
        {
            SwitchToInput();
        }

        private void SwitchToInput()
        {
            // Hide the result
            SearchResultControl1.Visible = false;
            SearchResultControl1.TabStop = false;
            SearchResultControl1.SendToBack();

            // Enable the input control
            SearchInputControl1.Visible = true;
            SearchInputControl1.TabStop = true;
            SearchInputControl1.BringToFront();
            SearchInputControl1.TraceNumber = string.Empty;
            SearchInputControl1.RefreshData();
        }

        private void SearchInputControl1_Selected(object sender, System.EventArgs e)
        {
            if (SearchInputControl1.TraceNumber != string.Empty)
            {
                TraceNumber = SearchInputControl1.TraceNumber;
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }
    }
}