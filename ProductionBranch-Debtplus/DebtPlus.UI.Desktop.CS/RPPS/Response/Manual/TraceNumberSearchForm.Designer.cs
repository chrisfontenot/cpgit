using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
	partial class TraceNumberSearchForm : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SearchInputControl1 = new SearchInputControl();
			this.SearchResultControl1 = new SearchResultControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//SearchInputControl1
			//
			this.SearchInputControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SearchInputControl1.Location = new System.Drawing.Point(0, 0);
			this.SearchInputControl1.Name = "SearchInputControl1";
			this.SearchInputControl1.Size = new System.Drawing.Size(342, 266);
			this.SearchInputControl1.TabIndex = 2;
			//
			//SearchResultControl1
			//
			this.SearchResultControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SearchResultControl1.Location = new System.Drawing.Point(0, 0);
			this.SearchResultControl1.Name = "SearchResultControl1";
			this.SearchResultControl1.Size = new System.Drawing.Size(342, 266);
			this.SearchResultControl1.TabIndex = 3;
			this.SearchResultControl1.Visible = false;
			//
			//TraceNumberSearchForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(342, 266);
			this.Controls.Add(this.SearchInputControl1);
			this.Controls.Add(this.SearchResultControl1);
			this.Name = "TraceNumberSearchForm";
			this.Text = "Trace Number Search";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal SearchInputControl SearchInputControl1;
		protected internal SearchResultControl SearchResultControl1;
	}
}
