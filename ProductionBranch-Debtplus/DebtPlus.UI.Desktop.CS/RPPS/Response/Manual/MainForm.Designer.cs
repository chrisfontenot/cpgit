using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.Manual
{
	partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
			this.LookUpEdit2 = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.gross = new DevExpress.XtraEditors.LabelControl();
			this.net = new DevExpress.XtraEditors.LabelControl();
			this.creditor = new DevExpress.XtraEditors.LabelControl();
			this.client = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.account_number = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
			this.TextEdit1 = new DevExpress.XtraEditors.ButtonEdit();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit2.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(12, 40);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(67, 13);
			this.LabelControl1.TabIndex = 2;
			this.LabelControl1.Text = "Trace Number";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(13, 66);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(52, 13);
			this.LabelControl2.TabIndex = 4;
			this.LabelControl2.Text = "Error Code";
			//
			//LookUpEdit1
			//
			this.LookUpEdit1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LookUpEdit1.Location = new System.Drawing.Point(122, 63);
			this.LookUpEdit1.Name = "LookUpEdit1";
			this.LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit1.Properties.NullText = "Please Choose One...";
			this.LookUpEdit1.Size = new System.Drawing.Size(254, 20);
			this.LookUpEdit1.TabIndex = 5;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 146);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(27, 13);
			this.LabelControl3.TabIndex = 12;
			this.LabelControl3.Text = "Gross";
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(12, 165);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(17, 13);
			this.LabelControl4.TabIndex = 14;
			this.LabelControl4.Text = "Net";
			//
			//SimpleButton1
			//
			this.SimpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton1.Enabled = false;
			this.SimpleButton1.Location = new System.Drawing.Point(122, 194);
			this.SimpleButton1.Name = "SimpleButton1";
			this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton1.TabIndex = 16;
			this.SimpleButton1.Text = "Refund";
			//
			//SimpleButton2
			//
			this.SimpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.SimpleButton2.CausesValidation = false;
			this.SimpleButton2.Location = new System.Drawing.Point(222, 194);
			this.SimpleButton2.Name = "SimpleButton2";
			this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton2.TabIndex = 17;
			this.SimpleButton2.Text = "&Quit";
			//
			//LookUpEdit2
			//
			this.LookUpEdit2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LookUpEdit2.Location = new System.Drawing.Point(121, 11);
			this.LookUpEdit2.Name = "LookUpEdit2";
			this.LookUpEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit2.Properties.NullText = "Please Choose One...";
			this.LookUpEdit2.Size = new System.Drawing.Size(254, 20);
			this.LookUpEdit2.TabIndex = 1;
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(12, 12);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(23, 13);
			this.LabelControl5.TabIndex = 0;
			this.LabelControl5.Text = "Bank";
			//
			//gross
			//
			this.gross.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.gross.Location = new System.Drawing.Point(121, 146);
			this.gross.Name = "gross";
			this.gross.Size = new System.Drawing.Size(176, 13);
			this.gross.TabIndex = 13;
			this.gross.Text = "$0.00";
			this.gross.UseMnemonic = false;
			//
			//net
			//
			this.net.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.net.Location = new System.Drawing.Point(121, 165);
			this.net.Name = "net";
			this.net.Size = new System.Drawing.Size(176, 13);
			this.net.TabIndex = 15;
			this.net.Text = "$0.00";
			this.net.UseMnemonic = false;
			//
			//creditor
			//
			this.creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.creditor.Location = new System.Drawing.Point(122, 108);
			this.creditor.Name = "creditor";
			this.creditor.Size = new System.Drawing.Size(253, 13);
			this.creditor.TabIndex = 9;
			this.creditor.UseMnemonic = false;
			//
			//Client
			//
			this.client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.client.Location = new System.Drawing.Point(122, 89);
			this.client.Name = "client";
			this.client.Size = new System.Drawing.Size(253, 13);
			this.client.TabIndex = 7;
			this.client.UseMnemonic = false;
			//
			//LabelControl8
			//
			this.LabelControl8.Location = new System.Drawing.Point(13, 108);
			this.LabelControl8.Name = "LabelControl8";
			this.LabelControl8.Size = new System.Drawing.Size(39, 13);
			this.LabelControl8.TabIndex = 8;
			this.LabelControl8.Text = "Creditor";
			//
			//LabelControl9
			//
			this.LabelControl9.Location = new System.Drawing.Point(13, 89);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(27, 13);
			this.LabelControl9.TabIndex = 6;
			this.LabelControl9.Text = "Client";
			//
			//account_number
			//
			this.account_number.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.account_number.Location = new System.Drawing.Point(121, 127);
			this.account_number.Name = "account_number";
			this.account_number.Size = new System.Drawing.Size(254, 13);
			this.account_number.TabIndex = 11;
			this.account_number.UseMnemonic = false;
			//
			//LabelControl11
			//
			this.LabelControl11.Location = new System.Drawing.Point(12, 127);
			this.LabelControl11.Name = "LabelControl11";
			this.LabelControl11.Size = new System.Drawing.Size(79, 13);
			this.LabelControl11.TabIndex = 10;
			this.LabelControl11.Text = "Account Number";
			//
			//TextEdit1
			//
			this.TextEdit1.EditValue = "";
			this.TextEdit1.Location = new System.Drawing.Point(122, 37);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.TextEdit1.Properties.DisplayFormat.FormatString = "0000000";
			this.TextEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.TextEdit1.Properties.Mask.EditMask = "\\d{0,7}";
			this.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit1.Properties.Mask.ShowPlaceHolders = false;
			this.TextEdit1.Properties.MaxLength = 7;
			this.TextEdit1.Properties.NullValuePrompt = "Value Required";
			this.TextEdit1.Properties.NullValuePromptShowForEmptyValue = true;
			this.TextEdit1.Size = new System.Drawing.Size(121, 20);
			this.TextEdit1.TabIndex = 3;
			this.TextEdit1.TabStop = false;
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(387, 229);
			this.Controls.Add(this.account_number);
			this.Controls.Add(this.LabelControl11);
			this.Controls.Add(this.creditor);
			this.Controls.Add(this.client);
			this.Controls.Add(this.LabelControl8);
			this.Controls.Add(this.LabelControl9);
			this.Controls.Add(this.net);
			this.Controls.Add(this.gross);
			this.Controls.Add(this.LookUpEdit2);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.SimpleButton2);
			this.Controls.Add(this.SimpleButton1);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.LookUpEdit1);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.TextEdit1);
			this.LookAndFeel.UseDefaultLookAndFeel = true;
			this.Name = "MainForm";
			this.Text = "RPPS Refunds";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit2.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl5;
		protected internal DevExpress.XtraEditors.LabelControl gross;
		protected internal DevExpress.XtraEditors.LabelControl net;
		protected internal DevExpress.XtraEditors.LabelControl creditor;
		protected internal DevExpress.XtraEditors.LabelControl client;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl8;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl9;
		protected internal DevExpress.XtraEditors.LabelControl account_number;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl11;
		protected internal DevExpress.XtraEditors.ButtonEdit TextEdit1;
	}
}
