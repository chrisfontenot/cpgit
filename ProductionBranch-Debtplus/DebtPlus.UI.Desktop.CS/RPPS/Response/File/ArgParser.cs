using System;
using System.Collections;
using System.IO;

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.File
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] { "b", "n0", "n1", "?" }) { }

        public ArrayList pathnames = new ArrayList();

        /// <summary>
        /// RPPS Response file
        /// </summary>
        private System.Int32 _rpps_response_file = -1;

        public System.Int32 rpps_response_file
        {
            get { return _rpps_response_file; }
            set { _rpps_response_file = value; }
        }

        /// <summary>
        /// Desire disbursement notes
        /// </summary>
        private System.Int32 _notes = 0;

        public bool notes
        {
            get { return _notes == 1; }
        }

        /// <summary>
        /// Process an option
        /// </summary>
        protected override global::DebtPlus.Utils.ArgParserBase.SwitchStatus OnSwitch(string switchName, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchName)
            {
                case "n1":
                    _notes = 1;
                    break;

                case "n0":
                    _notes = 2;
                    break;

                case "b":
                    System.Int32 TempInteger = 0;
                    if (Int32.TryParse(switchValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out TempInteger) && TempInteger > 0)
                    {
                        rpps_response_file = TempInteger;
                        break; // TODO: might not be correct. Was : Exit Select
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }

            return ss;
        }

        /// <summary>
        /// Input file to be read
        /// </summary>
        protected override global::DebtPlus.Utils.ArgParserBase.SwitchStatus OnNonSwitch(string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            // Add command-line argument to array of pathnames.
            // Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            try
            {
                string d = Path.GetDirectoryName(switchValue);
                DirectoryInfo dir = null;
                if ((d.Length == 0))
                {
                    dir = new DirectoryInfo(".");
                }
                else
                {
                    dir = new DirectoryInfo(d);
                }

                FileInfo f = null;
                foreach (FileInfo f_loopVariable in dir.GetFiles(Path.GetFileName(switchValue)))
                {
                    f = f_loopVariable;
                    pathnames.Add(f.FullName);
                }
            }
#pragma warning disable 168
            catch (System.Security.SecurityException SecEx)
            {
                throw;
            }
            catch (System.Exception ex)
            {
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }
#pragma warning restore 168

            if (pathnames.Count == 0)
            {
                AppendErrorLine("None of the specified files exists.");
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }

            return ss;
        }

        //OnNonSwitch

        /// <summary>
        /// Return the list of input file name
        /// </summary>
        public IEnumerator GetPathnameEnumerator()
        {
            return pathnames.GetEnumerator(0, pathnames.Count);
        }

        //GetPathnameEnumerator

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}