using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DebtPlus.UI.FormLib.RPPS.New;
using System.Windows.Forms;
using System.Drawing;

namespace DebtPlus.UI.Desktop.CS.RPPS.Response.File
{
    internal class Status_Form : DebtPlus.Data.Forms.DebtPlusForm
    {
        private System.ComponentModel.BackgroundWorker bt = new System.ComponentModel.BackgroundWorker();

        internal ArgParser ap;

        public Status_Form() : base()
        {
            privateInstance = this;
            InitializeComponent();
        }

        public Status_Form(ArgParser ap) : this()
        {
            this.ap = ap;
            this.Load += Status_Form_Load;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
            bt.DoWork += bt_DoWork;
            Button_OK.Click += cmdOK_Click;
            Button_Print.Click += printButton_Click;
        }

        /// <summary>
        /// Class to read the input file
        /// </summary>
        private class Reader : IDisposable
        {
            private System.IO.FileStream fs;

            private System.IO.StreamReader InputStream;

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public Reader()
            {
            }

            public Reader(string FileName) : this()
            {
                this.FileName = FileName;
            }

            /// <summary>
            /// Input file name
            /// </summary>
            public string FileName { get; set; }

            /// <summary>
            /// Flag to indicate that the file is at the end
            /// </summary>
            private bool privateAtEnd;

            public bool AtEnd
            {
                get { return privateAtEnd; }
            }

            /// <summary>
            /// Line buffer for the input file
            /// </summary>
            private string privateNextLine = string.Empty;

            public string NextLine
            {
                get { return privateNextLine; }
            }

            /// <summary>
            /// Access the next line in the input file
            /// </summary>
            public bool ReadNextLine()
            {
                if (!AtEnd && InputStream.EndOfStream)
                {
                    privateAtEnd = true;
                }
                if (AtEnd)
                    return false;

                privateNextLine = InputStream.ReadLine().PadRight(94);
                RecordType = privateNextLine.Substring(0, 1);
                return true;
            }

            // The first character of the record is the type

            private string RecordType = string.Empty;

            /// <summary>
            /// Look for a type 1 record
            /// </summary>
            public bool IsType1
            {
                get { return RecordType == "1"; }
            }

            /// <summary>
            /// Look for a type 5 record
            /// </summary>
            public bool IsType5
            {
                get { return RecordType == "5"; }
            }

            /// <summary>
            /// Look for a type 6 record
            /// </summary>
            public bool IsType6
            {
                get { return RecordType == "6"; }
            }

            /// <summary>
            /// Look for a type 7 record
            /// </summary>
            public bool IsType7
            {
                get { return RecordType == "7"; }
            }

            /// <summary>
            /// Look for a type 8 record
            /// </summary>
            public bool IsType8
            {
                get { return RecordType == "8"; }
            }

            /// <summary>
            /// Look for a type 9 record
            /// </summary>
            public bool IsType9
            {
                get { return RecordType == "9"; }
            }

            /// <summary>
            /// Open the input file
            /// </summary>
            public bool OpenFile()
            {
                bool answer = false;

                try
                {
                    fs = new System.IO.FileStream(FileName, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 4096);
                    InputStream = new System.IO.StreamReader(fs);
                    answer = true;
                }
#pragma warning disable 168
                catch (System.IO.FileNotFoundException ex)
                {
                }
                catch (System.IO.DirectoryNotFoundException ex)
                {
                }
                catch (System.IO.PathTooLongException ex)
                {
                }
#pragma warning restore 168

                return answer;
            }

            /// <summary>
            /// Close the input file
            /// </summary>
            public void CloseFile()
            {
                if (InputStream != null)
                    InputStream.Close();
                if (fs != null)
                    fs.Close();
            }

            /// <summary>
            /// Dispose of the class
            /// </summary>
            // To detect redundant calls
            private bool disposedValue;

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        CloseFile();
                    }

                    // set large fields to null.
                    InputStream = null;
                    fs = null;
                }

                disposedValue = true;
            }

            #region " IDisposable Support "

            // This code added by Visual Basic to correctly implement the disposable pattern.
            public void Dispose()
            {
                // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion " IDisposable Support "
        }

        /// <summary>
        /// Generic class for files, batches, and transactions
        /// </summary>
        private class Responses
        {
            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public Responses() : base()
            {
            }

            public Responses(Reader rdr) : this()
            {
                this.rdr = rdr;
            }

            /// <summary>
            /// Linkage to the file reader class
            /// </summary>
            public Reader rdr { get; set; }

            /// <summary>
            /// Set the error code value
            /// </summary>
            public string ErrorCode { get; set; }

            /// <summary>
            /// Return the base error code
            /// </summary>
            public virtual string GetError()
            {
                return ErrorCode;
            }

            /// <summary>
            /// Record the transactions to the database
            /// </summary>
            public virtual void UpdateDatabase()
            {
            }
        }

        /// <summary>
        /// A transaction is a representation of the following class
        /// </summary>
        private class StandardTransactionProcessing : Responses
        {
            protected BatchProcessing Batch;

            // List of addendum records
            protected System.Collections.Specialized.StringCollection AddendumCollection = new System.Collections.Specialized.StringCollection();

            protected string TransactionLine = string.Empty;

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public StandardTransactionProcessing() : base()
            {
            }

            public StandardTransactionProcessing(Reader rdr, BatchProcessing Batch) : base(rdr)
            {
                this.Batch = Batch;
            }

            public StandardTransactionProcessing(Reader rdr) : base(rdr)
            {
            }

            /// <summary>
            /// Parse the type 6 (Transaction Header) record
            /// </summary>
            public virtual bool ParseType6()
            {
                bool answer = false;

                // Save the transaction line for later examination
                TransactionLine = rdr.NextLine;

                // Gather all of the addendum records for this line. We don't look at the count
                // since the note buffers don't have a valid count.
                while (rdr.ReadNextLine())
                {
                    if (rdr.IsType7)
                    {
                        AddendumCollection.Add(rdr.NextLine);
                    }
                    else
                    {
                        answer = true;
                        break;
                    }
                }

                return answer;
            }

            /// <summary>
            /// Obtain the error status for the file
            /// </summary>
            public override string GetError()
            {
                string answer = Batch.GetError();
                if (answer == string.Empty)
                    answer = ErrorCode;
                return answer;
            }

            /// <summary>
            /// Process a CDP record
            /// </summary>

            protected void process_transaction_CDP(string ClientName, string account_number, string trace_number, decimal net)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CDP += 1;

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CDP";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDP response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// This is a proposal record
            /// </summary>
            protected void process_transaction_FBC(string ClientName, string account_number, string trace_number, decimal net)
            {
                process_transaction_CDP(ClientName, account_number, trace_number, net);
            }

            /// <summary>
            /// This is a proposal record
            /// </summary>
            protected void process_transaction_FBD(string ClientName, string account_number, string trace_number, decimal net)
            {
                process_transaction_CDP(ClientName, account_number, trace_number, net);
            }

            /// <summary>
            /// This is a drop record
            /// </summary>

            protected void process_transaction_CDD(string ClientName, string account_number, string trace_number, decimal net)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CDD += 1;

                // Fetch the fields from the input record
                //Dim CompanyIdentifier As String = AddendumCollection.Item(0).Substring(6, 6)
                //Dim SSN As String = AddendumCollection.Item(0).Substring(12, 9)
                //Dim Client As Int32 = IntegerValue(AddendumCollection.Item(0), 21, 15)

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CDD";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDD response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// This is a change request
            /// </summary>

            protected void process_transaction_CDC(string ClientName, string account_number, string trace_number, decimal net)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CDC += 1;

                // Find the information for the record
                //Dim client As Int32 = IntegerValue(AddendumCollection.Item(0), 21, 15)
                string new_account_number = AddendumCollection[0].Substring(36, 22).Trim();
                decimal Payment = Status_Form.MoneyValue(AddendumCollection[0], 61, 7);
                decimal Balance = Status_Form.MoneyValue(AddendumCollection[0], 68, 10);

                // Override the location for the error condition
                ErrorCode = AddendumCollection[0].Substring(58, 3);

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CDC";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@old_account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.Parameters.Add("@new_account_number", SqlDbType.VarChar, 80).Value = new_account_number;
                    cmd.Parameters.Add("@monthly_payment_amount", SqlDbType.Decimal).Value = Payment;
                    cmd.Parameters.Add("@balance", SqlDbType.Decimal).Value = Balance;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDC response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// This is a change request
            /// </summary>

            protected void process_transaction_ADV(string ClientName, string account_number, string trace_number, decimal net, decimal Gross)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.ADV += 1;

                // Look for the addendum lines to determine the processing required
                string AddendumCode = AddendumCollection[0].Substring(1, 2);
                string AddendumDate = AddendumCollection[0].Substring(21, 6);
                string AddendumRoutingNumber = AddendumCollection[0].Substring(27, 8);
                string AddendumInformation = AddendumCollection[0].Substring(35, 44);

                // Open the database connection
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_ADV";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.Parameters.Add("@gross", SqlDbType.Decimal).Value = Gross;

                    cmd.Parameters.Add("@addendum_code", SqlDbType.VarChar, 2).Value = AddendumCode;
                    cmd.Parameters.Add("@addendum_date", SqlDbType.VarChar, 8).Value = AddendumDate;
                    cmd.Parameters.Add("@addendum_routing", SqlDbType.VarChar, 8).Value = AddendumRoutingNumber;
                    cmd.Parameters.Add("@addendum_information", SqlDbType.VarChar, 50).Value = AddendumInformation;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording ADV response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// Parse the input string into a suitable date object. If the date is not valid, NULL is returned.
            /// </summary>
            /// <param name="InputString">The date string. It may be either 6 or 8 digits in length.</param>
            /// <returns></returns>
            /// <remarks></remarks>
            protected object ParseNullDate(string InputString)
            {
                object answer = DBNull.Value;

                // Split the fields so that we can use a language independant date format
                Int32 Year = -1;
                Int32 Month = -1;
                Int32 Day = -1;

                try
                {
                    // Convert the year and try to fold in the centurary. We use dates from 00 through 80 as being 2000 through 2080. Dates from 81 to 99 are 1981 through 1999.
                    if (Int32.TryParse(InputString.Substring(0, 2), out Year))
                    {
                        Year += 1900;
                        if (Year <= 1980)
                        {
                            Year += 100;
                        }
                    }

                    Int32.TryParse(InputString.Substring(2, 2), out Month);
                    Int32.TryParse(InputString.Substring(4, 2), out Day);

                    // Convert the date fields to a system date.
                    if (Year > 1900 && Month > 0 && Day > 0)
                    {
                        answer = new DateTime(Year, Month, Day);
                    }
                }
                catch
                {
                    // do nothing about an exception here. It will be good to return the null value.
                }

                return answer;
            }

            /// <summary>
            /// Parse the input string into a suitable date object. If the date is not valid, NULL is returned.
            /// </summary>
            /// <param name="InputString">The date string. It may be either 6 or 8 digits in length.</param>
            /// <returns></returns>
            /// <remarks></remarks>
            protected object ParseNullInt(string InputString)
            {
                object answer = DBNull.Value;

                // Convert the number to a suitable value
                try
                {
                    Int32 Value = 0;
                    if (Int32.TryParse(InputString, out Value))
                    {
                        answer = Value;
                    }
                }
                catch
                {
                    // do nothing about an exception here. It will be good to return the null value.
                }

                return answer;
            }

            /// <summary>
            /// Parse the input string into a suitable date object. If the date is not valid, NULL is returned.
            /// </summary>
            /// <param name="InputString">The date string. It may be either 6 or 8 digits in length.</param>
            /// <returns></returns>
            /// <remarks></remarks>
            protected object ParseNullFloat(string InputString)
            {
                object answer = DBNull.Value;

                // Convert the number to a suitable value
                try
                {
                    double Value = 0.0;

                    // Make the floating point number "legal" to our system. We use from 0.0 to 1.0 (exclusive).
                    string TempString = "0." + InputString.Trim();
                    if (double.TryParse(TempString, out Value))
                    {
                        answer = Value;
                    }
                }
                catch
                {
                    // do nothing about an exception here. It will be good to return the null value.
                }

                return answer;
            }

            /// <summary>
            /// Parse the input string into a suitable decimal object. If the money is not valid, NULL is returned.
            /// </summary>
            /// <param name="InputString">The money string.</param>
            /// <returns></returns>
            /// <remarks></remarks>
            protected object ParseNullMoney(string InputString)
            {
                object answer = DBNull.Value;

                // Convert the number to a suitable value
                try
                {
                    decimal Value = 0m;

                    // Insert a specific radix point two places to the left
                    string TempString = InputString;
                    if (TempString.Length > 2)
                    {
                        TempString = TempString.Substring(0, TempString.Length - 2) + "." + TempString.Substring(TempString.Length - 2);
                    }

                    if (decimal.TryParse(TempString, out Value))
                    {
                        answer = Value;
                    }
                }
                catch
                {
                    // do nothing about an exception here. It will be good to return the null value.
                }

                return answer;
            }

            /// <summary>
            /// This is a proposal reject record
            /// </summary>

            protected void process_transaction_CDR(string ClientName, string account_number, string trace_number, decimal net)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CDR += 1;

                // Fetch the fields from the input record
                string CompanyIdentifier = AddendumCollection[0].Substring(21, 6);
                string SSN = AddendumCollection[0].Substring(27, 9);
                Int32 Client = Status_Form.IntegerValue(AddendumCollection[0], 36, 15);
                string ReasonString = AddendumCollection[0].Substring(51, 3).Trim();
                decimal CounterAmount = 0m;
                string InfoRequired = string.Empty;

                // These fields vary depending upon the reject code.
                object resubmit_date = DBNull.Value;
                object cycle_months_remaining = DBNull.Value;
                object forgiven_pct = DBNull.Value;
                object first_payment_date = DBNull.Value;
                object good_thru_date = DBNull.Value;
                object ineligible_reason = DBNull.Value;
                object internal_program_ends_date = DBNull.Value;
                object interest_rate = DBNull.Value;
                object third_party_detail = DBNull.Value;
                object third_party_contact = DBNull.Value;

                // The counter amount does not seem to conflict with other fields. So, just include it on all items
                CounterAmount = Status_Form.MoneyValue(AddendumCollection[0], 54, 7);

                // Determine the fields based upon the error code. Only some have formatted fields, so just include those in the parsing logic.
                switch (ReasonString)
                {
                    case "AO":
                        resubmit_date = ParseNullDate(AddendumCollection[0].Substring(61, 6));

                        break;

                    case "FA":
                        cycle_months_remaining = ParseNullInt(AddendumCollection[0].Substring(61, 2));
                        forgiven_pct = ParseNullFloat(AddendumCollection[0].Substring(63, 5));

                        break;

                    case "IP":
                        first_payment_date = ParseNullDate(AddendumCollection[0].Substring(61, 6));
                        good_thru_date = ParseNullDate(AddendumCollection[0].Substring(67, 6));

                        break;

                    case "IE":
                        string TempString = AddendumCollection[0].Substring(61, 2).Trim();
                        if (TempString != string.Empty)
                        {
                            ineligible_reason = TempString;
                        }

                        break;

                    case "PI":
                        internal_program_ends_date = ParseNullDate(AddendumCollection[0].Substring(61, 6));
                        interest_rate = ParseNullFloat(AddendumCollection[0].Substring(67, 2));
                        good_thru_date = ParseNullDate(AddendumCollection[0].Substring(69, 6));

                        break;

                    case "PO":
                        resubmit_date = ParseNullDate(AddendumCollection[0].Substring(61, 6));

                        break;

                    case "PT":
                        first_payment_date = ParseNullDate(AddendumCollection[0].Substring(61, 6));
                        good_thru_date = ParseNullDate(AddendumCollection[0].Substring(67, 6));

                        break;

                    case "RA":
                        resubmit_date = ParseNullDate(AddendumCollection[0].Substring(61, 6));

                        break;

                    case "TP":
                        third_party_detail = AddendumCollection[0].Substring(61, 2).Trim();
                        third_party_contact = AddendumCollection[0].Substring(63, 16).Trim();

                        break;

                    default:
                        // This is the general case. It is just "information required"
                        InfoRequired = AddendumCollection[0].Substring(61, 18).Trim();
                        break;
                }

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CDR";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Client;
                    cmd.Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN;
                    cmd.Parameters.Add("@company_identification", SqlDbType.VarChar, 80).Value = CompanyIdentifier;
                    cmd.Parameters.Add("@reason", SqlDbType.VarChar, 80).Value = ReasonString;
                    cmd.Parameters.Add("@counter_offer", SqlDbType.Decimal).Value = CounterAmount;
                    cmd.Parameters.Add("@information_required", SqlDbType.VarChar, 80).Value = InfoRequired;
                    cmd.Parameters.Add("@resubmit_date", System.Data.SqlDbType.DateTime, 8).Value = resubmit_date;
                    cmd.Parameters.Add("@cycle_months_remaining", System.Data.SqlDbType.Int, 4).Value = cycle_months_remaining;
                    cmd.Parameters.Add("@forgiven_pct", System.Data.SqlDbType.Float, 8).Value = forgiven_pct;
                    cmd.Parameters.Add("@first_payment_date", System.Data.SqlDbType.DateTime, 8).Value = first_payment_date;
                    cmd.Parameters.Add("@good_thru_date", System.Data.SqlDbType.DateTime, 8).Value = good_thru_date;
                    cmd.Parameters.Add("@ineligible_reason", System.Data.SqlDbType.VarChar, 80).Value = ineligible_reason;
                    cmd.Parameters.Add("@internal_program_ends_date", System.Data.SqlDbType.DateTime, 8).Value = internal_program_ends_date;
                    cmd.Parameters.Add("@interest_rate", System.Data.SqlDbType.Float, 8).Value = interest_rate;
                    cmd.Parameters.Add("@third_party_detail", System.Data.SqlDbType.VarChar, 80).Value = third_party_detail;
                    cmd.Parameters.Add("@third_party_contact", System.Data.SqlDbType.VarChar, 80).Value = third_party_contact;

                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDR response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// This is a termination record
            /// </summary>

            protected void process_transaction_CDT(string ClientName, string account_number, string trace_number, decimal net)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CDT += 1;

                // Fetch the fields from the input record
                string CompanyIdentifier = AddendumCollection[0].Substring(21, 6);
                string SSN = AddendumCollection[0].Substring(27, 9);
                Int32 Client = Status_Form.IntegerValue(AddendumCollection[0], 36, 15);

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CDT";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Client;
                    cmd.Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN;
                    cmd.Parameters.Add("@company_identification", SqlDbType.VarChar, 80).Value = CompanyIdentifier;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDT response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// This is a proposal acceptance record
            /// </summary>

            protected void process_transaction_CDA(string ClientName, string account_number, string trace_number, decimal net)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CDA += 1;

                // Fetch the fields from the input record
                string CompanyIdentifier = AddendumCollection[0].Substring(21, 6);
                string SSN = AddendumCollection[0].Substring(27, 9);
                Int32 Client = Status_Form.IntegerValue(AddendumCollection[0], 36, 15);
                string LastCommunication = AddendumCollection[0].Substring(6, 10);
                decimal Balance = Status_Form.IntegerValue(AddendumCollection[0], 51, 10);
                Int32 APR = Status_Form.IntegerValue(AddendumCollection[0], 61, 5);
                object payment_amount = ParseNullMoney(AddendumCollection[0].Substring(66, 7));
                object start_date = ParseNullDate(AddendumCollection[0].Substring(73, 6));

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CDA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Client;
                    cmd.Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN;
                    cmd.Parameters.Add("@company_identification", SqlDbType.VarChar, 80).Value = CompanyIdentifier;
                    cmd.Parameters.Add("@last_communication", SqlDbType.VarChar, 80).Value = LastCommunication;
                    cmd.Parameters.Add("@balance", SqlDbType.Decimal).Value = Balance;
                    cmd.Parameters.Add("@apr", SqlDbType.Int).Value = APR;
                    cmd.Parameters.Add("@payment_amount", System.Data.SqlDbType.Decimal).Value = payment_amount;
                    cmd.Parameters.Add("@start_date", System.Data.SqlDbType.DateTime, 8).Value = start_date;

                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDA response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// This is a balance verification record
            /// </summary>

            protected void process_transaction_CDV(string ClientName, string account_number, string trace_number, decimal net)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CDV += 1;

                // Fetch the fields from the input record
                decimal Balance = Status_Form.MoneyValue(AddendumCollection[0], 21, 10);
                string LastCommunication = AddendumCollection[0].Substring(31, 15);
                string SSN = AddendumCollection[0].Substring(46, 9);
                Int32 Client = Status_Form.IntegerValue(AddendumCollection[0], 55, 15);

                // Reverse the sign of the balance if the amount should be negative. These are "C"redit balances.
                string Status = AddendumCollection[0].Substring(70, 1);
                if (Status == "C" && Balance > 0)
                {
                    Balance = 0 - Balance;
                }

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CDV";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = Client;
                    cmd.Parameters.Add("@ssn", SqlDbType.VarChar, 80).Value = SSN;
                    cmd.Parameters.Add("@balance", SqlDbType.Decimal).Value = Balance;
                    cmd.Parameters.Add("@last_communication", SqlDbType.VarChar, 80).Value = LastCommunication;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDV response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// There are no detail records on this response
            /// </summary>

            protected void process_transaction_CIE(string ClientName, string account_number, string trace_number, decimal net, decimal gross)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CIE += 1;

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_CIE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = GetError();
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                    cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = net;
                    cmd.Parameters.Add("@gross", SqlDbType.Decimal).Value = gross;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CIE response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// Record the transactions to the database
            /// </summary>
            public override void UpdateDatabase()
            {
                string trace_number = null;

                // Parse the first record. These are common for all record types.
                string ClientName = TransactionLine.Substring(39, 15).Trim();
                string account_number = TransactionLine.Substring(54, 22).Trim();
                decimal net = Status_Form.MoneyValue(TransactionLine, 29, 10);
                decimal Gross = 0m;

                // The gross amount is part of the client name field if this is a payment request
                // 10 digits followed by a non-blank and then anything.
                if (System.Text.RegularExpressions.Regex.IsMatch(@"\d{10}\S.*", ClientName))
                {
                    Gross = Status_Form.MoneyValue(ClientName.Substring(0, 10));
                    ClientName = ClientName.Substring(11).Trim();
                }

                // If there are no addendum records then this must be a payment reject
                if (AddendumCollection.Count == 0)
                {
                    ErrorCode = TransactionLine.Substring(12, 3);
                    trace_number = TransactionLine.Substring(79, 15);
                    process_transaction_CIE(ClientName, account_number, trace_number, net, Gross);
                }
                else
                {
                    // Otherwise, look at the batch type to find the error and trace numbers
                    switch (Batch.CompanyEntryDescription)
                    {
                        case "RETURN    ":
                        case "CONVERSION":
                            ErrorCode = AddendumCollection[0].Substring(3, 3);
                            trace_number = AddendumCollection[0].Substring(6, 15);

                            break;

                        default:
                            // "REJECT ENT", "REJECT BAT", "REJECT FIL", "ACCEPT FIL"
                            ErrorCode = TransactionLine.Substring(12, 3);
                            trace_number = TransactionLine.Substring(79, 15);
                            break;
                    }

                    // Look at the record to determine the subtype processing desired
                    switch (AddendumCollection[0].Substring(3, 3))
                    {
                        case "C09":
                        case "C11":
                        case "C50":
                            process_transaction_ADV(ClientName, account_number, trace_number, net, Gross);

                            break;

                        case "CDA":
                            process_transaction_CDA(ClientName, account_number, trace_number, net);

                            break;

                        case "CDR":
                            process_transaction_CDR(ClientName, account_number, trace_number, net);

                            break;

                        case "CDC":
                            process_transaction_CDC(ClientName, account_number, trace_number, net);

                            break;

                        case "CDD":
                            process_transaction_CDD(ClientName, account_number, trace_number, net);

                            break;

                        case "CDT":
                            process_transaction_CDT(ClientName, account_number, trace_number, net);

                            break;

                        case "CDV":
                            process_transaction_CDV(ClientName, account_number, trace_number, net);

                            break;

                        case "CDP":
                            process_transaction_CDP(ClientName, account_number, trace_number, net);

                            break;

                        case "FBC":
                            process_transaction_FBC(ClientName, account_number, trace_number, net);

                            break;

                        case "FBD":
                            process_transaction_FBD(ClientName, account_number, trace_number, net);

                            break;

                        default:
                            process_transaction_CIE(ClientName, account_number, trace_number, net, Gross);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Process a message return transaction for a MESSAGE batch type
        /// </summary>
        private class MessageTransactionProcessing : StandardTransactionProcessing
        {
            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public MessageTransactionProcessing(Reader rdr, BatchProcessing Batch) : base(rdr, Batch)
            {
            }

            public MessageTransactionProcessing() : base()
            {
            }

            public MessageTransactionProcessing(Reader rdr) : base(rdr)
            {
            }

            /// <summary>
            /// Record the transactions to the database
            /// </summary>

            public override void UpdateDatabase()
            {
                Int32 LastSequence = 0;
                decimal Net = Status_Form.MoneyValue(TransactionLine, 29, 10) / 100m;
                string account_number = TransactionLine.Substring(54, 22).Trim();
                string ClientName = TransactionLine.Substring(39, 15).Trim();
                string trace_number = string.Empty;
                string TextMessage = string.Empty;

                // Count an instance of our record type
                Status_Form.privateInstance.CDM += 1;

                // The error code is always CDM for now.
                ErrorCode = "CDM";

                // Process the addendum lines with the line text
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();

                    foreach (string CurrentInputLine in AddendumCollection)
                    {
                        Int32 current_sequence = 0;
                        if (!Int32.TryParse(CurrentInputLine.Substring(83, 4), System.Globalization.NumberStyles.AllowLeadingWhite | System.Globalization.NumberStyles.AllowTrailingWhite, System.Globalization.CultureInfo.InvariantCulture, out current_sequence) || current_sequence < 0)
                            current_sequence = 0;

                        if (current_sequence == 1)
                        {
                            trace_number = CurrentInputLine.Substring(6, 15);
                            TextMessage = CurrentInputLine.Substring(21, 62).Trim();
                        }
                        else
                        {
                            TextMessage = CurrentInputLine.Substring(6, 77).Trim();
                        }

                        // Count the sequence number
                        LastSequence += 1;

                        var cmd = new SqlCommand();
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_rpps_response_CDM";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);

                        cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                        cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number;
                        cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                        cmd.Parameters.Add("@error_code", SqlDbType.VarChar, 80).Value = GetError();
                        cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                        cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = account_number;
                        cmd.Parameters.Add("@net", SqlDbType.Decimal).Value = Net;
                        cmd.Parameters.Add("@line_number", SqlDbType.Int).Value = current_sequence;
                        cmd.Parameters.Add("@message", SqlDbType.VarChar, 256).Value = TextMessage;

                        cmd.ExecuteNonQuery();
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording CDM response");
                }
                finally
                {
                    if (cn != null && cn.State != ConnectionState.Closed)
                        cn.Close();
                    Cursor.Current = current_cursor;
                }
            }
        }

        /// <summary>
        /// Process a message return transaction for a MESSAGE batch type
        /// </summary>
        private class NotificationOfChangeTransactionProcessing : StandardTransactionProcessing
        {
            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="rdr">Pointer to the data reader</param>
            /// <param name="Batch">Batch ID associated with this RPPS batch</param>
            /// <remarks></remarks>
            public NotificationOfChangeTransactionProcessing(Reader rdr, BatchProcessing Batch) : base(rdr, Batch)
            {
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <remarks></remarks>
            public NotificationOfChangeTransactionProcessing() : base()
            {
            }

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            /// <param name="rdr">Pointer to the data reader</param>
            /// <remarks></remarks>
            public NotificationOfChangeTransactionProcessing(Reader rdr) : base(rdr)
            {
            }

            /// <summary>
            /// Process an advisory change in the account numbers
            /// </summary>
            /// <param name="ChangeAccountNumber">TRUE if there is a change in the account number</param>
            /// <param name="ChangeBillerID">TRUE if there is a change in the biller ID number</param>
            /// <param name="ErrorCode">Error code asssociated with the transaction</param>
            /// <param name="OriginalEntryTraceNumber">Original trace number to find the transaction</param>
            /// <param name="OldAccountNumber">Old account number if there is a change</param>
            /// <param name="NewAccountNumber">New account number if there is a change</param>
            /// <param name="NewBillerID">New biller id for the creditor</param>
            /// <param name="ClientName">Client name</param>
            /// <remarks></remarks>

            protected void process_transaction_COA(bool ChangeAccountNumber, bool ChangeBillerID, string ErrorCode, string OriginalEntryTraceNumber, string OldAccountNumber, string NewAccountNumber, string NewBillerID, string ClientName)
            {
                // Count an instance of our record type
                Status_Form.privateInstance.CIE += 1;

                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    var cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_COA";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);

                    cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = Batch.File.ResponseFileID;
                    cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = OriginalEntryTraceNumber;
                    cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = Batch.BillerID;
                    cmd.Parameters.Add("@return_code", SqlDbType.VarChar, 80).Value = ErrorCode;
                    cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = ClientName;
                    cmd.Parameters.Add("@OldAccountNumber", SqlDbType.VarChar, 80).Value = OldAccountNumber;
                    cmd.Parameters.Add("@ChangeAccountNumber", SqlDbType.Bit, 0).Value = ChangeAccountNumber;
                    cmd.Parameters.Add("@NewAccountNumber", SqlDbType.VarChar, 80).Value = NewAccountNumber;
                    cmd.Parameters.Add("@ChangeBillerID", SqlDbType.Bit, 0).Value = ChangeBillerID;
                    cmd.Parameters.Add("@NewBillerID", SqlDbType.VarChar, 80).Value = NewBillerID;
                    cmd.ExecuteNonQuery();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error recording COA response");
                }
                finally
                {
                    if (cn != null)
                        cn.Dispose();

                    Cursor.Current = current_cursor;
                }
            }

            /// <summary>
            /// Change in the account number
            /// </summary>
            /// <param name="OriginalEntryTraceNumber">Original trace number</param>
            /// <param name="OldAccountNumber">Old account number value</param>
            /// <param name="NewAccountNumber">New account number value</param>
            /// <param name="NewBillerID">New biller id value</param>
            /// <param name="ClientName">Client name</param>
            /// <remarks></remarks>
            protected void process_transaction_C50(string OriginalEntryTraceNumber, string OldAccountNumber, string NewAccountNumber, string NewBillerID, string ClientName)
            {
                process_transaction_COA(true, false, "C50", OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName);
            }

            /// <summary>
            /// Change in the biller ID number
            /// </summary>
            /// <param name="OriginalEntryTraceNumber">Original trace number</param>
            /// <param name="OldAccountNumber">Old account number value</param>
            /// <param name="NewAccountNumber">New account number value</param>
            /// <param name="NewBillerID">New biller id value</param>
            /// <param name="ClientName">Client name</param>
            /// <remarks></remarks>
            protected void process_transaction_C51(string OriginalEntryTraceNumber, string OldAccountNumber, string NewAccountNumber, string NewBillerID, string ClientName)
            {
                process_transaction_COA(false, true, "C51", OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName);
            }

            /// <summary>
            /// Change in the account number and biller ID number
            /// </summary>
            /// <param name="OriginalEntryTraceNumber">Original trace number</param>
            /// <param name="OldAccountNumber">Old account number value</param>
            /// <param name="NewAccountNumber">New account number value</param>
            /// <param name="NewBillerID">New biller id value</param>
            /// <param name="ClientName">Client name</param>
            /// <remarks></remarks>
            protected void process_transaction_C52(string OriginalEntryTraceNumber, string OldAccountNumber, string NewAccountNumber, string NewBillerID, string ClientName)
            {
                process_transaction_COA(true, true, "C52", OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName);
            }

            /// <summary>
            /// Record the transactions to the database
            /// </summary>

            public override void UpdateDatabase()
            {
                // Parse the first record. These are common for all record types.
                string ClientName = TransactionLine.Substring(39, 15).Trim();
                string OldAccountNumber = TransactionLine.Substring(54, 22).Trim();

                // These are in the first addendum record
                string ErrorCode = string.Empty;
                string OriginalEntryTraceNumber = string.Empty;
                string OriginalDFI = string.Empty;
                string NewAccountNumber = string.Empty;
                string NewBillerID = string.Empty;

                // Find the information from the addendum records
                if (AddendumCollection.Count > 0)
                {
                    string Addendum = AddendumCollection[0];
                    if (Addendum.Substring(0, 3) == "798")
                    {
                        ErrorCode = Addendum.Substring(3, 3).Trim();
                        OriginalEntryTraceNumber = Addendum.Substring(6, 15);
                        OriginalDFI = Addendum.Substring(27, 8);
                        NewAccountNumber = Addendum.Substring(35, 22).Trim();
                        NewBillerID = Addendum.Substring(57, 10);
                    }
                }

                // Look at the reason for the transaction and handle accordingly.
                switch (ErrorCode)
                {
                    case "C50":
                        process_transaction_C50(OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName);
                        break;

                    case "C51":
                        process_transaction_C51(OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName);
                        break;

                    case "C52":
                        process_transaction_C52(OriginalEntryTraceNumber, OldAccountNumber, NewAccountNumber, NewBillerID, ClientName);
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// A batch is a representation of the following class
        /// </summary>
        private class BatchProcessing : Responses
        {
            // List of transactions for this batch
            private readonly LinkedList<Responses> TranCollection = new LinkedList<Responses>();

            public FileProcessing File;

            // Information from the batch header
            public string BatchHeader = string.Empty;

            public string BillerID = string.Empty;
            public string CompanyEntryDescription = string.Empty;
            public string ServiceClassCode = string.Empty;

            public string StandardEntryClass = string.Empty;

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public BatchProcessing() : base()
            {
            }

            public BatchProcessing(Reader rdr, FileProcessing File) : base(rdr)
            {
                this.File = File;
            }

            public BatchProcessing(Reader rdr) : base(rdr)
            {
            }

            /// <summary>
            /// Obtain the error status for the file
            /// </summary>
            public override string GetError()
            {
                string answer = File.GetError();
                if (answer == string.Empty)
                    answer = ErrorCode;
                return answer;
            }

            /// <summary>
            /// Process a batch from the creditor
            /// </summary>
            public bool ParseType5()
            {
                bool answer = false;

                // Determine the biller id for the response
                BatchHeader = rdr.NextLine;
                BillerID = BatchHeader.Substring(40, 10).Trim();

                // Save the fields from the batch header
                ServiceClassCode = BatchHeader.Substring(1, 3).Trim();
                StandardEntryClass = BatchHeader.Substring(50, 3).Trim();
                CompanyEntryDescription = BatchHeader.Substring(53, 10);

                // Read the first transaction line which should follow the batch header
                if (!rdr.ReadNextLine())
                {
                    return false;
                }

                // If this is the end of the batch then stop processing
                do
                {
                    if (rdr.IsType8)
                    {
                        ParseType8();
                        answer = true;
                        break;
                    }

                    // If this is the end of file record then we missed the batch EOF
                    if (rdr.IsType9 || rdr.IsType1)
                    {
                        answer = false;
                        break;
                    }

                    // If this is a type 6 record then process the batch

                    if (rdr.IsType6)
                    {
                        // If this is a correction batch then process it seperately.
                        if (StandardEntryClass == "CIE")
                        {
                            switch (CompanyEntryDescription)
                            {
                                case "CONVERSION":
                                    NotificationOfChangeTransactionProcessing convTran = new NotificationOfChangeTransactionProcessing(rdr, this);
                                    if (convTran.ParseType6())
                                    {
                                        TranCollection.AddLast(convTran);
                                    }

                                    break;
                                // Look for message lines
                                case "MESSAGE   ":
                                case "MSG RETURN":
                                    MessageTransactionProcessing msgTran = new MessageTransactionProcessing(rdr, this);
                                    if (msgTran.ParseType6())
                                    {
                                        TranCollection.AddLast(msgTran);
                                    }

                                    break;
                                // Take all others as payment and proposal rejects
                                default:
                                    StandardTransactionProcessing defTran = new StandardTransactionProcessing(rdr, this);
                                    if (defTran.ParseType6())
                                    {
                                        TranCollection.AddLast(defTran);
                                    }
                                    break;
                            }
                        }

                        // If this is not the expected type, flush the record until we find one.
                    }
                    else if (!rdr.ReadNextLine())
                    {
                        answer = false;
                        break;
                    }
                } while (true);

                return answer;
            }

            /// <summary>
            /// Parse the type 8 (EOB) record for batch trailer
            /// </summary>
            public bool ParseType8()
            {
                // Save the error code associated with the batch
                ErrorCode = rdr.NextLine.Substring(54, 3).Trim();

                return false;
            }

            /// <summary>
            /// Record the transactions to the database
            /// </summary>

            public override void UpdateDatabase()
            {
                // Count an instance of our record type
                Status_Form.privateInstance.batch_count += 1;

                foreach (Responses tran in TranCollection)
                {
                    tran.UpdateDatabase();
                }
            }
        }

        /// <summary>
        /// A file is a representation of the following class
        /// </summary>
        private class FileProcessing : Responses
        {
            public Int32 ResponseFileID = -1;

            /// <summary>
            /// Create an instance of our class
            /// </summary>
            public FileProcessing() : base()
            {
            }

            public FileProcessing(Reader rdr, Int32 ResponseFileID) : base(rdr)
            {
                this.ResponseFileID = ResponseFileID;
            }

            public FileProcessing(Reader rdr) : base(rdr)
            {
            }

            // List of batches in this file
            private readonly LinkedList<BatchProcessing> BatchCollection = new LinkedList<BatchProcessing>();

            public string FileHeader = string.Empty;

            public string FileTrailer = string.Empty;

            /// <summary>
            /// Examine the type 1 (HEADER) record which starts the file
            /// </summary>
            public bool ParseType1()
            {
                bool answer = false;

                // Examine the type 1 start of file record
                FileHeader = rdr.NextLine;
                if (!FileHeader.StartsWith("101") || string.Compare(FileHeader.Substring(34, 3), "094", false) != 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The file does not have a valid RPPS reject header near line 1");
                    return false;
                }

                // Ensure that the file is not a response file
                if (string.Compare(FileHeader.Substring(63, 23), "MC REMIT PROC CENT SITE", true) != 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The file is not an RPPS response file. It is missing the proper source marker in columns 64 to 87.");
                    return false;
                }

                // Read the first line for the file contents
                if (!rdr.ReadNextLine())
                {
                    return false;
                }

                do
                {
                    // If this is the end of the file then stop processing
                    if (rdr.IsType9)
                    {
                        ParseType9();
                        answer = true;
                        break;
                    }

                    // If this is a new type 1 record then we missed the type 9
                    if (rdr.IsType1)
                    {
                        answer = false;
                        break;
                    }

                    // If this is a type 5 record then process the batch
                    if (rdr.IsType5)
                    {
                        BatchProcessing batch = new BatchProcessing(rdr, this);
                        if (batch.ParseType5())
                        {
                            BatchCollection.AddLast(batch);
                        }
                    }
                    else if (!rdr.ReadNextLine())
                    {
                        answer = false;
                        break;
                    }
                } while (true);

                return answer;
            }

            /// <summary>
            /// Examine the type 9 (EOF) record which terminates the file
            /// </summary>
            public bool ParseType9()
            {
                // Save the file trailer line
                FileTrailer = rdr.NextLine;

                // Isolate the error code from the file trailer
                ErrorCode = FileTrailer.Substring(56, 3).Trim();

                return false;
            }

            /// <summary>
            /// Record the transactions to the database
            /// </summary>

            public override void UpdateDatabase()
            {
                // Count an instance of our record type
                Status_Form.privateInstance.file_count += 1;

                foreach (BatchProcessing batch in BatchCollection)
                {
                    batch.UpdateDatabase();
                }
            }
        }

        /// <summary>
        /// Class to manage the processing of multiple files in a dataset
        /// </summary>
        private class InputProcessing
        {
            /// <summary>
            /// Create an instance of our class
            /// </summary>
            private readonly Int32 ResponseFileID = -1;

            public InputProcessing(Int32 ResponseFileID)
            {
                this.ResponseFileID = ResponseFileID;
            }

            /// <summary>
            /// Parse the input file
            /// </summary>
            public void ParseFile(string FileName)
            {
                Reader rdr = new Reader(FileName);

                // Open and read the first line in the file
                if (rdr.OpenFile())
                {
                    if (rdr.ReadNextLine())
                    {
                        do
                        {
                            // If the record is a type 1 then process the new file
                            if (rdr.IsType1)
                            {
                                var fileOp = new FileProcessing(rdr, ResponseFileID);
                                fileOp.ParseType1();
                                fileOp.UpdateDatabase();
                            }

                            // Flush records until we reach the end or we find a type 1 record again.
                            else if (!rdr.ReadNextLine())
                            {
                                break;
                            }
                        } while (true);
                    }

                    // Close the opened file
                    rdr.CloseFile();
                }
            }
        }

        /// <summary>
        /// Post the response file
        /// </summary>
        public void PostChanges()
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_response_process_post";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.Parameters.Add("@response_file", SqlDbType.Int).Value = ap.rpps_response_file;
                    cmd.ExecuteNonQuery();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error posting the response file");
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        #region "Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool Disposing)
        {
            if (Disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(Disposing);
        }

        internal DevExpress.XtraEditors.LabelControl lbl_ADV_count;
        internal DevExpress.XtraEditors.LabelControl lbl_ADV_text;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Print;
        internal DevExpress.XtraEditors.LabelControl lbl_cdm_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cie_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cda_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdr_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdv_text;
        internal DevExpress.XtraEditors.LabelControl lbl_batch_count_text;
        internal DevExpress.XtraEditors.LabelControl lbl_file_count_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdc_text;
        internal DevExpress.XtraEditors.LabelControl lbl_cdm;
        internal DevExpress.XtraEditors.LabelControl lbl_cie;
        internal DevExpress.XtraEditors.LabelControl lbl_cda;
        internal DevExpress.XtraEditors.LabelControl lbl_cdr;
        internal DevExpress.XtraEditors.LabelControl lbl_cdv;
        internal DevExpress.XtraEditors.LabelControl lbl_batch_count;
        internal DevExpress.XtraEditors.LabelControl lbl_file_count;
        internal DevExpress.XtraEditors.LabelControl lbl_cdc;

        internal DevExpress.XtraEditors.MarqueeProgressBarControl MarqueeProgressBarControl1;

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough()]
        private void InitializeComponent()
        {
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
            this.lbl_cdm_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cie_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cda_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdr_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdv_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_batch_count_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_file_count_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdc_text = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdm = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cie = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cda = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdr = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdv = new DevExpress.XtraEditors.LabelControl();
            this.lbl_batch_count = new DevExpress.XtraEditors.LabelControl();
            this.lbl_file_count = new DevExpress.XtraEditors.LabelControl();
            this.lbl_cdc = new DevExpress.XtraEditors.LabelControl();
            this.MarqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            this.lbl_ADV_count = new DevExpress.XtraEditors.LabelControl();
            this.lbl_ADV_text = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //Button_OK
            //
            this.Button_OK.Location = new System.Drawing.Point(63, 230);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 0;
            this.Button_OK.Text = "&Cancel";
            //
            //Button_Print
            //
            this.Button_Print.Enabled = false;
            this.Button_Print.Location = new System.Drawing.Point(153, 230);
            this.Button_Print.Name = "Button_Print";
            this.Button_Print.Size = new System.Drawing.Size(75, 23);
            this.Button_Print.TabIndex = 1;
            this.Button_Print.Text = "&Print";
            //
            //lbl_cdm_text
            //
            this.lbl_cdm_text.Location = new System.Drawing.Point(12, 12);
            this.lbl_cdm_text.Name = "lbl_cdm_text";
            this.lbl_cdm_text.Size = new System.Drawing.Size(84, 13);
            this.lbl_cdm_text.TabIndex = 2;
            this.lbl_cdm_text.Text = "Message Records";
            //
            //lbl_cie_text
            //
            this.lbl_cie_text.Location = new System.Drawing.Point(12, 31);
            this.lbl_cie_text.Name = "lbl_cie_text";
            this.lbl_cie_text.Size = new System.Drawing.Size(93, 13);
            this.lbl_cie_text.TabIndex = 3;
            this.lbl_cie_text.Text = "All Others (Payments?)";
            //
            //lbl_cda_text
            //
            this.lbl_cda_text.Location = new System.Drawing.Point(12, 50);
            this.lbl_cda_text.Name = "lbl_cda_text";
            this.lbl_cda_text.Size = new System.Drawing.Size(94, 13);
            this.lbl_cda_text.TabIndex = 4;
            this.lbl_cda_text.Text = "Proposals Accepted";
            //
            //lbl_cdr_text
            //
            this.lbl_cdr_text.Location = new System.Drawing.Point(12, 69);
            this.lbl_cdr_text.Name = "lbl_cdr_text";
            this.lbl_cdr_text.Size = new System.Drawing.Size(92, 13);
            this.lbl_cdr_text.TabIndex = 5;
            this.lbl_cdr_text.Text = "Proposals Rejected";
            //
            //lbl_cdv_text
            //
            this.lbl_cdv_text.Location = new System.Drawing.Point(12, 88);
            this.lbl_cdv_text.Name = "lbl_cdv_text";
            this.lbl_cdv_text.Size = new System.Drawing.Size(98, 13);
            this.lbl_cdv_text.TabIndex = 6;
            this.lbl_cdv_text.Text = "Balance Verifications";
            //
            //lbl_batch_count_text
            //
            this.lbl_batch_count_text.Location = new System.Drawing.Point(12, 107);
            this.lbl_batch_count_text.Name = "lbl_batch_count_text";
            this.lbl_batch_count_text.Size = new System.Drawing.Size(90, 13);
            this.lbl_batch_count_text.TabIndex = 7;
            this.lbl_batch_count_text.Text = "Batches Processed";
            //
            //lbl_file_count_text
            //
            this.lbl_file_count_text.Location = new System.Drawing.Point(12, 126);
            this.lbl_file_count_text.Name = "lbl_file_count_text";
            this.lbl_file_count_text.Size = new System.Drawing.Size(73, 13);
            this.lbl_file_count_text.TabIndex = 8;
            this.lbl_file_count_text.Text = "Files Processed";
            //
            //lbl_cdc_text
            //
            this.lbl_cdc_text.Location = new System.Drawing.Point(12, 145);
            this.lbl_cdc_text.Name = "lbl_cdc_text";
            this.lbl_cdc_text.Size = new System.Drawing.Size(124, 13);
            this.lbl_cdc_text.TabIndex = 9;
            this.lbl_cdc_text.Text = "Account Number Changes";
            //
            //lbl_cdm
            //
            this.lbl_cdm.Appearance.Options.UseTextOptions = true;
            this.lbl_cdm.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdm.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdm.Location = new System.Drawing.Point(230, 12);
            this.lbl_cdm.Name = "lbl_cdm";
            this.lbl_cdm.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdm.TabIndex = 10;
            this.lbl_cdm.Text = "0";
            //
            //lbl_cie
            //
            this.lbl_cie.Appearance.Options.UseTextOptions = true;
            this.lbl_cie.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cie.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cie.Location = new System.Drawing.Point(230, 31);
            this.lbl_cie.Name = "lbl_cie";
            this.lbl_cie.Size = new System.Drawing.Size(50, 13);
            this.lbl_cie.TabIndex = 11;
            this.lbl_cie.Text = "0";
            //
            //lbl_cda
            //
            this.lbl_cda.Appearance.Options.UseTextOptions = true;
            this.lbl_cda.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cda.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cda.Location = new System.Drawing.Point(230, 50);
            this.lbl_cda.Name = "lbl_cda";
            this.lbl_cda.Size = new System.Drawing.Size(50, 13);
            this.lbl_cda.TabIndex = 12;
            this.lbl_cda.Text = "0";
            //
            //lbl_cdr
            //
            this.lbl_cdr.Appearance.Options.UseTextOptions = true;
            this.lbl_cdr.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdr.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdr.Location = new System.Drawing.Point(230, 69);
            this.lbl_cdr.Name = "lbl_cdr";
            this.lbl_cdr.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdr.TabIndex = 13;
            this.lbl_cdr.Text = "0";
            //
            //lbl_cdv
            //
            this.lbl_cdv.Appearance.Options.UseTextOptions = true;
            this.lbl_cdv.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdv.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdv.Location = new System.Drawing.Point(230, 88);
            this.lbl_cdv.Name = "lbl_cdv";
            this.lbl_cdv.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdv.TabIndex = 14;
            this.lbl_cdv.Text = "0";
            //
            //lbl_batch_count
            //
            this.lbl_batch_count.Appearance.Options.UseTextOptions = true;
            this.lbl_batch_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_batch_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_batch_count.Location = new System.Drawing.Point(230, 107);
            this.lbl_batch_count.Name = "lbl_batch_count";
            this.lbl_batch_count.Size = new System.Drawing.Size(50, 13);
            this.lbl_batch_count.TabIndex = 15;
            this.lbl_batch_count.Text = "0";
            //
            //lbl_file_count
            //
            this.lbl_file_count.Appearance.Options.UseTextOptions = true;
            this.lbl_file_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_file_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_file_count.Location = new System.Drawing.Point(230, 126);
            this.lbl_file_count.Name = "lbl_file_count";
            this.lbl_file_count.Size = new System.Drawing.Size(50, 13);
            this.lbl_file_count.TabIndex = 16;
            this.lbl_file_count.Text = "0";
            //
            //lbl_cdc
            //
            this.lbl_cdc.Appearance.Options.UseTextOptions = true;
            this.lbl_cdc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_cdc.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_cdc.Location = new System.Drawing.Point(230, 145);
            this.lbl_cdc.Name = "lbl_cdc";
            this.lbl_cdc.Size = new System.Drawing.Size(50, 13);
            this.lbl_cdc.TabIndex = 17;
            this.lbl_cdc.Text = "0";
            //
            //MarqueeProgressBarControl1
            //
            this.MarqueeProgressBarControl1.EditValue = "Initializing for Processing";
            this.MarqueeProgressBarControl1.Location = new System.Drawing.Point(12, 190);
            this.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1";
            this.MarqueeProgressBarControl1.Size = new System.Drawing.Size(268, 23);
            this.MarqueeProgressBarControl1.TabIndex = 18;
            //
            //lbl_ADV_count
            //
            this.lbl_ADV_count.Appearance.Options.UseTextOptions = true;
            this.lbl_ADV_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbl_ADV_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbl_ADV_count.Location = new System.Drawing.Point(230, 164);
            this.lbl_ADV_count.Name = "lbl_ADV_count";
            this.lbl_ADV_count.Size = new System.Drawing.Size(50, 13);
            this.lbl_ADV_count.TabIndex = 20;
            this.lbl_ADV_count.Text = "0";
            //
            //lbl_ADV_text
            //
            this.lbl_ADV_text.Location = new System.Drawing.Point(12, 164);
            this.lbl_ADV_text.Name = "lbl_ADV_text";
            this.lbl_ADV_text.Size = new System.Drawing.Size(84, 13);
            this.lbl_ADV_text.TabIndex = 19;
            this.lbl_ADV_text.Text = "Advisory Records";
            //
            //Status_Form
            //
            this.ToolTipController1.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.False);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.lbl_ADV_count);
            this.Controls.Add(this.lbl_ADV_text);
            this.Controls.Add(this.MarqueeProgressBarControl1);
            this.Controls.Add(this.lbl_cdc);
            this.Controls.Add(this.lbl_file_count);
            this.Controls.Add(this.lbl_batch_count);
            this.Controls.Add(this.lbl_cdv);
            this.Controls.Add(this.lbl_cdr);
            this.Controls.Add(this.lbl_cda);
            this.Controls.Add(this.lbl_cie);
            this.Controls.Add(this.lbl_cdm);
            this.Controls.Add(this.lbl_cdc_text);
            this.Controls.Add(this.lbl_file_count_text);
            this.Controls.Add(this.lbl_batch_count_text);
            this.Controls.Add(this.lbl_cdv_text);
            this.Controls.Add(this.lbl_cdr_text);
            this.Controls.Add(this.lbl_cda_text);
            this.Controls.Add(this.lbl_cie_text);
            this.Controls.Add(this.lbl_cdm_text);
            this.Controls.Add(this.Button_Print);
            this.Controls.Add(this.Button_OK);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(300, 300);
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "Status_Form";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "RPPS Response File Processing";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion "Windows Form Designer generated code "

        #region " Counters "

        /// <summary>
        /// Count of CDD records
        /// </summary>
        private Int32 _CIE;

        private void delegated_CIE()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_CIE));
            }
            else
            {
                lbl_cie.Text = string.Format("{0:n0}", CIE);
            }
        }

        public Int32 CIE
        {
            get { return _CIE; }
            set
            {
                _CIE = value;
                delegated_CIE();
            }
        }

        /// <summary>
        /// Count of CDD records
        /// </summary>
        private Int32 _CDD;

        private void delegated_CDD()
        {
        }

        public Int32 CDD
        {
            get { return _CDD; }
            set
            {
                _CDD = value;
                delegated_CDD();
            }
        }

        /// <summary>
        /// Count of CDT records
        /// </summary>
        private Int32 _CDT = 0;

        private void delegated_CDT()
        {
        }

        public Int32 CDT
        {
            get { return _CDT; }
            set
            {
                _CDT = value;
                delegated_CDT();
            }
        }

        /// <summary>
        /// Count of CDR records
        /// </summary>
        private Int32 _CDR;

        private void delegated_CDR()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_CDR));
            }
            else
            {
                lbl_cdr.Text = string.Format("{0:n0}", CDR);
            }
        }

        public Int32 CDR
        {
            get { return _CDR; }
            set
            {
                _CDR = value;
                delegated_CDR();
            }
        }

        /// <summary>
        /// Count of CDA records
        /// </summary>
        private Int32 _CDA;

        private void delegated_CDA()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_CDA));
            }
            else
            {
                lbl_cda.Text = string.Format("{0:n0}", CDA);
            }
        }

        public Int32 CDA
        {
            get { return _CDA; }
            set
            {
                _CDA = value;
                delegated_CDA();
            }
        }

        /// <summary>
        /// Count of CDV records
        /// </summary>
        private Int32 _CDV;

        private void delegated_CDV()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_CDV));
            }
            else
            {
                lbl_cdv.Text = string.Format("{0:n0}", CDV);
            }
        }

        public Int32 CDV
        {
            get { return _CDV; }
            set
            {
                _CDV = value;
                delegated_CDV();
            }
        }

        /// <summary>
        /// Count of CDM records
        /// </summary>
        private Int32 _CDM;

        private void delegated_CDM()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_CDM));
            }
            else
            {
                lbl_cdm.Text = string.Format("{0:n0}", CDM);
            }
        }

        public Int32 CDM
        {
            get { return _CDM; }
            set
            {
                _CDM = value;
                delegated_CDM();
            }
        }

        /// <summary>
        /// Count of CDC records
        /// </summary>
        private Int32 _CDC;

        private void delegated_CDC()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_CDC));
            }
            else
            {
                lbl_cdc.Text = string.Format("{0:n0}", _CDC);
            }
        }

        public Int32 CDC
        {
            get { return _CDC; }
            set
            {
                _CDC = value;
                delegated_CDC();
            }
        }

        /// <summary>
        /// Count of proposals
        /// </summary>
        private Int32 _CDP = 0;

        private void delegated_CDP()
        {
        }

        public Int32 CDP
        {
            get { return _CDP; }
            set
            {
                _CDP = value;
                delegated_CDP();
            }
        }

        /// <summary>
        /// Batch Count
        /// </summary>
        private Int32 _batch_count = 0;

        private void delegated_batch_count()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_batch_count));
            }
            else
            {
                lbl_batch_count.Text = string.Format("{0:n0}", _batch_count);
            }
        }

        public Int32 batch_count
        {
            get { return _batch_count; }
            set
            {
                _batch_count = value;
                delegated_batch_count();
            }
        }

        /// <summary>
        /// File Count
        /// </summary>
        private Int32 _file_count;

        private void delegated_file_count()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_file_count));
            }
            else
            {
                lbl_file_count.Text = string.Format("{0:n0}", _file_count);
            }
        }

        public Int32 file_count
        {
            get { return _file_count; }
            set
            {
                _file_count = value;
                delegated_file_count();
            }
        }

        /// <summary>
        /// Advisorary Count
        /// </summary>
        private Int32 _ADV;

        private void delegated_ADV()
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegated_ADV));
            }
            else
            {
                lbl_ADV_count.Text = string.Format("{0:n0}", _ADV);
            }
        }

        public Int32 ADV
        {
            get { return _ADV; }
            set
            {
                _ADV = value;
                delegated_ADV();
            }
        }

        #endregion " Counters "

        #region " Form Processing "

        /// <summary>
        /// Process the form update logic once the form is displayed
        /// </summary>
        private void Status_Form_Load(object sender, EventArgs e)
        {
            Activated += Form1_Activated;

            MarqueeProgressBarControl1.Properties.Stopped = true;
            MarqueeProgressBarControl1.Text = "INITIALIZING FOR PROCESSING";
            MarqueeProgressBarControl1.Properties.ShowTitle = true;
        }

        /// <summary>
        /// Process the form update logic once the form is displayed
        /// </summary>
        private void Form1_Activated(object sender, EventArgs e)
        {
            bool Enabled = true;

            // Do this logic only on the first time that we are activated
            Activated -= Form1_Activated;

            // Ask for the response batch to use
            if (ap.rpps_response_file <= 0)
            {
                using (BatchForm frm = new BatchForm())
                {
                    frm.Owner = this;
                    if (frm.ShowDialog() == DialogResult.OK)
                    {
                        ap.rpps_response_file = frm.rpps_response_file;
                    }
                }

                if (ap.rpps_response_file <= 0)
                {
                    Enabled = false;
                }
            }

            // Sort all the pathnames in the list
            if (Enabled && ap.pathnames.Count == 0)
            {
                DialogResult answer = default(DialogResult);
                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.FileOk += dlg_FileOk;

                    dlg.Title = "Open RPPS Input response file";
                    dlg.ShowReadOnly = false;
                    dlg.ShowHelp = false;
                    dlg.RestoreDirectory = true;
                    dlg.Multiselect = true;
                    dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                    dlg.DereferenceLinks = true;
                    dlg.CheckPathExists = true;
                    dlg.CheckFileExists = true;
                    dlg.Filter = "All Files (*.*)|*.*";
                    answer = dlg.ShowDialog();

                    // Add the filenames to the list
                    if (answer == System.Windows.Forms.DialogResult.OK)
                    {
                        foreach (string f in dlg.FileNames)
                        {
                            ap.pathnames.Add(f);
                        }
                    }
                }
            }

            // If there are no names then return an error. Otherwise, sort them.
            if (Enabled && ap.pathnames.Count == 0)
                Enabled = false;

            // If the form is not valid then close it. This will kill the application.
            if (!Enabled)
            {
                Close();
            }
            else
            {
                ap.pathnames.Sort(0, ap.pathnames.Count, null);

                // Change the message text to indicate "CANCEL"
                Button_OK.Text = "&Cancel";
                Button_Print.Enabled = false;

                // Show the marquee
                MarqueeProgressBarControl1.Properties.Stopped = false;
                MarqueeProgressBarControl1.Properties.ShowTitle = false;

                // Start the background thread to process the file(s).
                bt.WorkerSupportsCancellation = true;
                bt.WorkerReportsProgress = false;
                bt.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Validation routine for the file open dialog
        /// </summary>
        private void dlg_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            bool Canceled = true;

            // Process the selected file names
            foreach (string fname in ((OpenFileDialog)sender).FileNames)
            {
                Canceled = true;
                if (fname == null)
                {
                    break;
                }

                // Read the first part of the file. It should start with the string "101"
                try
                {
                    string FirstPart = string.Empty;
                    System.IO.StreamReader fs = new System.IO.StreamReader(new System.IO.FileStream(fname, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read, 4096));
                    FirstPart = fs.ReadLine();
                    fs.Close();

                    // Look for a valid response file
                    if (FirstPart.Length > 80 && FirstPart.StartsWith("101"))
                    {
                        Canceled = false;
                    }
                }
#pragma warning disable 168
                catch (System.IO.FileNotFoundException ex)
                {
                }
                catch (System.IO.DirectoryNotFoundException ex)
                {
                }
                catch (System.IO.EndOfStreamException ex)
                {
                    // These are ignored at this point
                }
#pragma warning restore 168

                // If the entry is not valid, set the cancel status
                if (Canceled)
                {
                    break;
                }
            }

            e.Cancel = Canceled;
        }

        /// <summary>
        /// Complete processing for the thread
        /// </summary>

        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            // Change the message text to indicate "OK"
            Button_OK.Text = "&OK";
            Button_Print.Enabled = true;

            // Stop the marque from being displayed
            MarqueeProgressBarControl1.Properties.Stopped = true;
            MarqueeProgressBarControl1.Text = "OPERATION COMPLETED";
            MarqueeProgressBarControl1.Properties.ShowTitle = true;
        }

        /// <summary>
        /// Process the cancel/ok button
        /// </summary>

        private void cmdOK_Click(object eventSender, EventArgs eventArgs)
        {
            // Closing the main form will terminate the background thread and the program.
            // There is no need to "CANCEL" the thread since it is stopped automatically when we stop.
            Close();
        }

        #endregion " Form Processing "

        #region " Thread Processing "

        /// <summary>
        /// Handle the thread procedure
        /// </summary>
        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            try
            {
                // Do the application here
                IEnumerator ie = ap.GetPathnameEnumerator();
                while (ie.MoveNext())
                {
                    var inputOp = new InputProcessing(ap.rpps_response_file);
                    inputOp.ParseFile(Convert.ToString(ie.Current));
                }

                // If we have something to process then post the changes to the database
                PostChanges();
            }
            catch (System.Threading.ThreadAbortException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        #endregion " Thread Processing "

        #region " Parsing routines "

        /// <summary>
        /// Decode the numerical value of a fixed point number string
        /// </summary>
        static internal Int32 IntegerValue(string input_string)
        {
            Int32 answer = 0;

            if (!Int32.TryParse(input_string, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out answer) || answer < 0)
                answer = 0;

            return answer;
        }

        static internal Int32 IntegerValue(string input_string, Int32 StartingIndex, Int32 Length)
        {
            return IntegerValue(input_string.Substring(StartingIndex, Length));
        }

        /// <summary>
        /// Retrieve the monitary value for the column
        /// </summary>
        static internal decimal MoneyValue(string input_string)
        {
            decimal answer = 0m;

            if (!decimal.TryParse(input_string, System.Globalization.NumberStyles.Currency, System.Globalization.CultureInfo.InvariantCulture, out answer) || answer < 0m)
                answer = 0m;
            answer = System.Math.Truncate(answer) / 100m;

            return answer;
        }

        static internal decimal MoneyValue(string input_string, Int32 StartingIndex, Int32 Length)
        {
            return MoneyValue(input_string.Substring(StartingIndex, Length));
        }

        #endregion " Parsing routines "

        #region " Print Form "

        /// <summary>
        /// Print the form on the default printer
        /// </summary>
        private Font printFont;

        private void printButton_Click(object sender, EventArgs e)
        {
            try
            {
                printFont = new Font("Arial", 10);
                System.Drawing.Printing.PrintDocument pd = new System.Drawing.Printing.PrintDocument();
                pd.PrintPage += pd_PrintPage;

#if false // Direct to printer
				pd.Print();
#else // Use preview form first
                PrintPreviewDialog PreviewDialog = new PrintPreviewDialog();
                PreviewDialog.ShowIcon = false;
                PreviewDialog.Document = pd;
                PreviewDialog.ShowDialog(this);
#endif
            }
            catch (Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error printing document");
            }
        }

        /// <summary>
        /// Event handler to put data on the page for the printer
        /// </summary>
        private void pd_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs ev)
        {
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            float yPos = topMargin;
            StringFormat fmt = new StringFormat();
            Int32 LineNo = 0;

            // Generate the font for the header
            Font HdrFont = new Font(printFont.FontFamily, 20, FontStyle.Bold, GraphicsUnit.Point, Convert.ToByte(0));
            fmt.Alignment = StringAlignment.Near;
            ev.Graphics.DrawString(Text, HdrFont, Brushes.Black, leftMargin + 0, yPos, fmt);
            yPos += (HdrFont.GetHeight(ev.Graphics) * 2);
            HdrFont.Dispose();

            // Print each line of the file.
            ev.HasMorePages = true;

            while (ev.HasMorePages)
            {
                string TitleString = string.Empty;
                string ValueString = string.Empty;
                switch (LineNo)
                {
                    case 0:
                        TitleString = lbl_cdm_text.Text;
                        ValueString = lbl_cdm.Text;
                        break;

                    case 1:
                        TitleString = lbl_cie_text.Text;
                        ValueString = lbl_cie.Text;
                        break;

                    case 2:
                        TitleString = lbl_cda_text.Text;
                        ValueString = lbl_cda.Text;
                        break;

                    case 3:
                        TitleString = lbl_cdr_text.Text;
                        ValueString = lbl_cdr.Text;
                        break;

                    case 4:
                        TitleString = lbl_cdv_text.Text;
                        ValueString = lbl_cdv.Text;
                        break;

                    case 5:
                        TitleString = lbl_batch_count_text.Text;
                        ValueString = lbl_batch_count.Text;
                        break;

                    case 6:
                        TitleString = lbl_file_count_text.Text;
                        ValueString = lbl_file_count.Text;
                        break;

                    case 7:
                        TitleString = lbl_cdc_text.Text;
                        ValueString = lbl_cdc.Text;
                        break;

                    case 8:
                        TitleString = lbl_ADV_text.Text;
                        ValueString = lbl_ADV_count.Text;
                        ev.HasMorePages = false;
                        break;

                    default:
                        break;
                }

                // Generate the position on the form and print the line
                fmt.Alignment = StringAlignment.Near;
                ev.Graphics.DrawString(TitleString, printFont, Brushes.Black, leftMargin + 0, yPos, fmt);
                fmt.Alignment = StringAlignment.Far;
                ev.Graphics.DrawString(ValueString, printFont, Brushes.Black, leftMargin + 300, yPos, fmt);
                yPos += printFont.GetHeight(ev.Graphics);

                LineNo += 1;
            }
        }

        #endregion " Print Form "

        protected internal static Status_Form privateInstance;
    }
}