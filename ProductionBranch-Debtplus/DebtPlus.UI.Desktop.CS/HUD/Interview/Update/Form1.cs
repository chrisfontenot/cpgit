using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Columns;
using DebtPlus.Data.Forms;
using DevExpress.XtraEditors.Repository;
using System.Data.SqlClient;
using System.Text;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.IO;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.HUD.Interview.Update
{
    internal partial class Form1 : DebtPlusForm
    {
        private DataSet ds = new DataSet("ds");

        /// <summary>
        /// Normal way to create my class
        /// </summary>
        public Form1() : base()
        {
            InitializeComponent();

            this.Load += MyGridControl_Load;
            ContextMenu1.Popup += ContextMenu1_Popup;
            GridView1.Click += GridView1_Click;
            this.FormClosing += Form1_FormClosing;
            this.Load += Form1_Load;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.CellValueChanged += GridView1_CellValueChanged;
            MenuItem_delete.Click += MenuItem_delete_Click;
            txt_client.Validated += txt_client_Validated;
        }

        internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID txt_client;
        internal GridColumn col_HUD_Grant;
        internal RepositoryItemLookUpEdit LookUpEdit_HUD_Grant;
        internal GridColumn col_HousingFeeAmount;

        internal RepositoryItemCalcEdit CalcEdit_HousingFeeAmount;

        private ArgParser ap;

        public Form1(ArgParser ap) : this()
        {
            this.ap = ap;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl lbl_client_name;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn col_hud_interview;
        internal DevExpress.XtraGrid.Columns.GridColumn col_interview_date;
        internal DevExpress.XtraGrid.Columns.GridColumn col_result_date;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookupEdit_Interview;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookupEdit_Result;
        internal DevExpress.XtraGrid.Columns.GridColumn col_interview_counselor;
        internal DevExpress.XtraGrid.Columns.GridColumn col_result_counselor;
        internal DevExpress.XtraGrid.Columns.GridColumn col_hud_result;
        internal DevExpress.XtraGrid.Columns.GridColumn col_interview_type;
        internal DevExpress.XtraGrid.Columns.GridColumn col_termination_reason;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit LookupEdit_Termination;
        internal DevExpress.XtraGrid.Columns.GridColumn col_termination_date;
        internal ContextMenu ContextMenu1;

        internal MenuItem MenuItem_delete;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lbl_client_name = new DevExpress.XtraEditors.LabelControl();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.ContextMenu1 = new System.Windows.Forms.ContextMenu();
            this.MenuItem_delete = new System.Windows.Forms.MenuItem();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.col_hud_interview = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_interview_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookupEdit_Interview = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col_interview_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_interview_counselor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_hud_result = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookupEdit_Result = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col_result_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_result_counselor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_termination_reason = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookupEdit_Termination = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.col_termination_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col_HUD_Grant = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LookUpEdit_HUD_Grant = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.txt_client = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.col_HousingFeeAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CalcEdit_HousingFeeAmount = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookupEdit_Interview).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookupEdit_Result).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookupEdit_Termination).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_HUD_Grant).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.txt_client.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit_HousingFeeAmount).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 11);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(27, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Client";
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 40);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(27, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Name";
            //
            //lbl_client_name
            //
            this.lbl_client_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.lbl_client_name.Location = new System.Drawing.Point(96, 40);
            this.lbl_client_name.Name = "lbl_client_name";
            this.lbl_client_name.Size = new System.Drawing.Size(0, 13);
            this.lbl_client_name.TabIndex = 3;
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.ContextMenu = this.ContextMenu1;
            this.GridControl1.Location = new System.Drawing.Point(0, 66);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
                this.LookupEdit_Interview,
                this.LookupEdit_Result,
                this.LookupEdit_Termination,
                this.LookUpEdit_HUD_Grant,
                this.CalcEdit_HousingFeeAmount
            });
            this.GridControl1.Size = new System.Drawing.Size(580, 200);
            this.GridControl1.TabIndex = 4;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //ContextMenu1
            //
            this.ContextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] { this.MenuItem_delete });
            //
            //MenuItem_delete
            //
            this.MenuItem_delete.Index = 0;
            this.MenuItem_delete.Text = "&Delete...";
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.col_hud_interview,
                this.col_interview_type,
                this.col_interview_date,
                this.col_interview_counselor,
                this.col_hud_result,
                this.col_result_date,
                this.col_result_counselor,
                this.col_termination_reason,
                this.col_termination_date,
                this.col_HUD_Grant,
                this.col_HousingFeeAmount
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //col_hud_interview
            //
            this.col_hud_interview.AppearanceCell.Options.UseTextOptions = true;
            this.col_hud_interview.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_hud_interview.AppearanceHeader.Options.UseTextOptions = true;
            this.col_hud_interview.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_hud_interview.Caption = "Record ID";
            this.col_hud_interview.CustomizationCaption = "Record Number";
            this.col_hud_interview.FieldName = "hud_interview";
            this.col_hud_interview.Name = "col_hud_interview";
            this.col_hud_interview.OptionsColumn.AllowEdit = false;
            this.col_hud_interview.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_hud_interview.ToolTip = "Interview Record Number";
            //
            //col_interview_type
            //
            this.col_interview_type.Caption = "Interview";
            this.col_interview_type.ColumnEdit = this.LookupEdit_Interview;
            this.col_interview_type.CustomizationCaption = "Interview Description";
            this.col_interview_type.FieldName = "interview_type";
            this.col_interview_type.Name = "col_interview_type";
            this.col_interview_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_interview_type.Visible = true;
            this.col_interview_type.VisibleIndex = 1;
            this.col_interview_type.Width = 140;
            //
            //LookupEdit_Interview
            //
            this.LookupEdit_Interview.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookupEdit_Interview.AutoHeight = false;
            this.LookupEdit_Interview.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookupEdit_Interview.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", 60, "Description")
            });
            this.LookupEdit_Interview.Name = "LookupEdit_Interview";
            this.LookupEdit_Interview.NullText = "";
            this.LookupEdit_Interview.ShowFooter = false;
            this.LookupEdit_Interview.ShowHeader = false;
            this.LookupEdit_Interview.ShowLines = false;
            //
            //col_interview_date
            //
            this.col_interview_date.AppearanceCell.Options.UseTextOptions = true;
            this.col_interview_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_interview_date.AppearanceHeader.Options.UseTextOptions = true;
            this.col_interview_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_interview_date.Caption = "Interview Date";
            this.col_interview_date.CustomizationCaption = "Interview Date";
            this.col_interview_date.DisplayFormat.FormatString = "d";
            this.col_interview_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col_interview_date.FieldName = "interview_date";
            this.col_interview_date.GroupFormat.FormatString = "d";
            this.col_interview_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col_interview_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth;
            this.col_interview_date.Name = "col_interview_date";
            this.col_interview_date.OptionsColumn.AllowEdit = false;
            this.col_interview_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_interview_date.Visible = true;
            this.col_interview_date.VisibleIndex = 0;
            this.col_interview_date.Width = 80;
            //
            //col_interview_counselor
            //
            this.col_interview_counselor.Caption = "Interview Counselor";
            this.col_interview_counselor.CustomizationCaption = "Interview Counselor";
            this.col_interview_counselor.FieldName = "interview_counselor";
            this.col_interview_counselor.Name = "col_interview_counselor";
            this.col_interview_counselor.OptionsColumn.AllowEdit = false;
            this.col_interview_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //col_hud_result
            //
            this.col_hud_result.Caption = "Result";
            this.col_hud_result.ColumnEdit = this.LookupEdit_Result;
            this.col_hud_result.CustomizationCaption = "Result";
            this.col_hud_result.FieldName = "hud_result";
            this.col_hud_result.Name = "col_hud_result";
            this.col_hud_result.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_hud_result.Visible = true;
            this.col_hud_result.VisibleIndex = 3;
            this.col_hud_result.Width = 173;
            //
            //LookupEdit_Result
            //
            this.LookupEdit_Result.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookupEdit_Result.AutoHeight = false;
            this.LookupEdit_Result.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookupEdit_Result.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("result", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.LookupEdit_Result.Name = "LookupEdit_Result";
            this.LookupEdit_Result.NullText = "";
            this.LookupEdit_Result.ShowFooter = false;
            this.LookupEdit_Result.ShowHeader = false;
            this.LookupEdit_Result.ShowLines = false;
            //
            //col_result_date
            //
            this.col_result_date.AppearanceCell.Options.UseTextOptions = true;
            this.col_result_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_result_date.AppearanceHeader.Options.UseTextOptions = true;
            this.col_result_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_result_date.Caption = "Result Date";
            this.col_result_date.CustomizationCaption = "Result Date";
            this.col_result_date.DisplayFormat.FormatString = "d";
            this.col_result_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col_result_date.FieldName = "result_date";
            this.col_result_date.GroupFormat.FormatString = "d";
            this.col_result_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.col_result_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth;
            this.col_result_date.Name = "col_result_date";
            this.col_result_date.OptionsColumn.AllowEdit = false;
            this.col_result_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_result_date.Visible = true;
            this.col_result_date.VisibleIndex = 2;
            this.col_result_date.Width = 148;
            //
            //col_result_counselor
            //
            this.col_result_counselor.Caption = "Result Counselor";
            this.col_result_counselor.CustomizationCaption = "Result Counselor";
            this.col_result_counselor.FieldName = "result_counselor";
            this.col_result_counselor.Name = "col_result_counselor";
            this.col_result_counselor.OptionsColumn.AllowEdit = false;
            this.col_result_counselor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            //
            //col_termination_reason
            //
            this.col_termination_reason.Caption = "Termination";
            this.col_termination_reason.ColumnEdit = this.LookupEdit_Termination;
            this.col_termination_reason.CustomizationCaption = "Termination Reason";
            this.col_termination_reason.FieldName = "termination_reason";
            this.col_termination_reason.Name = "col_termination_reason";
            this.col_termination_reason.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_termination_reason.Visible = true;
            this.col_termination_reason.VisibleIndex = 5;
            this.col_termination_reason.Width = 187;
            //
            //LookupEdit_Termination
            //
            this.LookupEdit_Termination.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.LookupEdit_Termination.AutoHeight = false;
            this.LookupEdit_Termination.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo),
                new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
            });
            this.LookupEdit_Termination.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Default),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.LookupEdit_Termination.DisplayMember = "(None)";
            this.LookupEdit_Termination.Name = "LookupEdit_Termination";
            this.LookupEdit_Termination.NullText = "[Not Terminated - Still Pending]";
            this.LookupEdit_Termination.ShowFooter = false;
            this.LookupEdit_Termination.ShowHeader = false;
            //
            //col_termination_date
            //
            this.col_termination_date.Caption = "Termination Date";
            this.col_termination_date.CustomizationCaption = "Termination Date";
            this.col_termination_date.FieldName = "termination_date";
            this.col_termination_date.Name = "col_termination_date";
            this.col_termination_date.OptionsColumn.AllowEdit = false;
            this.col_termination_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.col_termination_date.Visible = true;
            this.col_termination_date.VisibleIndex = 4;
            //
            //col_HUD_Grant
            //
            this.col_HUD_Grant.AppearanceCell.Options.UseTextOptions = true;
            this.col_HUD_Grant.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.col_HUD_Grant.AppearanceHeader.Options.UseTextOptions = true;
            this.col_HUD_Grant.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.col_HUD_Grant.Caption = "Grant";
            this.col_HUD_Grant.ColumnEdit = this.LookUpEdit_HUD_Grant;
            this.col_HUD_Grant.CustomizationCaption = "HUD Grant ID";
            this.col_HUD_Grant.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.col_HUD_Grant.FieldName = "HUD_Grant";
            this.col_HUD_Grant.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.col_HUD_Grant.Name = "col_HUD_Grant";
            this.col_HUD_Grant.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this.col_HUD_Grant.ToolTip = "HUD Grant";
            this.col_HUD_Grant.Visible = true;
            this.col_HUD_Grant.VisibleIndex = 6;
            //
            //LookUpEdit_HUD_Grant
            //
            this.LookUpEdit_HUD_Grant.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.LookUpEdit_HUD_Grant.AutoHeight = false;
            this.LookUpEdit_HUD_Grant.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.LookUpEdit_HUD_Grant.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("oID", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)
            });
            this.LookUpEdit_HUD_Grant.DisplayMember = "description";
            this.LookUpEdit_HUD_Grant.Name = "LookUpEdit_HUD_Grant";
            this.LookUpEdit_HUD_Grant.NullText = "";
            this.LookUpEdit_HUD_Grant.ShowFooter = false;
            this.LookUpEdit_HUD_Grant.ShowHeader = false;
            this.LookUpEdit_HUD_Grant.ValueMember = "oID";
            //
            //txt_client
            //
            this.txt_client.AllowDrop = true;
            this.txt_client.Location = new System.Drawing.Point(96, 8);
            this.txt_client.Name = "txt_client";
            this.txt_client.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.txt_client.Properties.Appearance.Options.UseTextOptions = true;
            this.txt_client.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.txt_client.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("txt_client.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
            "Click here to search for a client ID", "search", null, true) });
            this.txt_client.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txt_client.Properties.DisplayFormat.FormatString = "0000000";
            this.txt_client.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.txt_client.Properties.EditFormat.FormatString = "f0";
            this.txt_client.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txt_client.Properties.Mask.BeepOnError = true;
            this.txt_client.Properties.Mask.EditMask = "\\d*";
            this.txt_client.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txt_client.Properties.ValidateOnEnterKey = true;
            this.txt_client.Size = new System.Drawing.Size(100, 20);
            this.txt_client.TabIndex = 1;
            //
            //col_HousingFeeAmount
            //
            this.col_HousingFeeAmount.AppearanceCell.Options.UseTextOptions = true;
            this.col_HousingFeeAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_HousingFeeAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.col_HousingFeeAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.col_HousingFeeAmount.Caption = "Fee";
            this.col_HousingFeeAmount.ColumnEdit = this.CalcEdit_HousingFeeAmount;
            this.col_HousingFeeAmount.CustomizationCaption = "Housing Fee Amount";
            this.col_HousingFeeAmount.DisplayFormat.FormatString = "c";
            this.col_HousingFeeAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_HousingFeeAmount.FieldName = "HousingFeeAmount";
            this.col_HousingFeeAmount.GroupFormat.FormatString = "c";
            this.col_HousingFeeAmount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.col_HousingFeeAmount.Name = "col_HousingFeeAmount";
            this.col_HousingFeeAmount.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.col_HousingFeeAmount.ToolTip = "Amount changed for this interview";
            this.col_HousingFeeAmount.Visible = true;
            this.col_HousingFeeAmount.VisibleIndex = 7;
            //
            //CalcEdit_HousingFeeAmount
            //
            this.CalcEdit_HousingFeeAmount.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.CalcEdit_HousingFeeAmount.AutoHeight = false;
            this.CalcEdit_HousingFeeAmount.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit_HousingFeeAmount.DisplayFormat.FormatString = "c";
            this.CalcEdit_HousingFeeAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_HousingFeeAmount.EditFormat.FormatString = "c";
            this.CalcEdit_HousingFeeAmount.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit_HousingFeeAmount.Mask.BeepOnError = true;
            this.CalcEdit_HousingFeeAmount.Mask.EditMask = "c";
            this.CalcEdit_HousingFeeAmount.Name = "CalcEdit_HousingFeeAmount";
            this.CalcEdit_HousingFeeAmount.Precision = 2;
            //
            //Form1
            //
            this.ClientSize = new System.Drawing.Size(580, 266);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.lbl_client_name);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.txt_client);
            this.Name = "Form1";
            this.Text = "Alter HUD Interview / Result Transactions";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookupEdit_Interview).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookupEdit_Result).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookupEdit_Termination).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.LookUpEdit_HUD_Grant).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.txt_client.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit_HousingFeeAmount).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Record the changed table to the database
        /// </summary>
        private bool SaveChanges()
        {
            DataTable tbl = ds.Tables["hud_interviews"];
            if (tbl == null)
            {
                return true;
            }

            using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
            {
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    using (SqlCommand UpdateCmd = new SqlCommand())
                    {
                        UpdateCmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        UpdateCmd.CommandText = "UPDATE hud_interviews SET [interview_type]=@interview_type, [hud_result]=@hud_result, [termination_reason]=@termination_reason,[HUD_grant]=@HUD_Grant,[HousingFeeAmount]=@HousingFeeAmount WHERE hud_interview=@hud_interview";
                        UpdateCmd.Parameters.Add("@interview_type", SqlDbType.Int, 4, "interview_type");
                        UpdateCmd.Parameters.Add("@hud_result", SqlDbType.Int, 4, "hud_result");
                        UpdateCmd.Parameters.Add("@termination_reason", SqlDbType.Int, 4, "termination_reason");
                        UpdateCmd.Parameters.Add("@HUD_Grant", SqlDbType.Int, 4, "HUD_Grant");
                        UpdateCmd.Parameters.Add("@HousingFeeAmount", SqlDbType.Decimal, 0, "HousingFeeAmount");
                        UpdateCmd.Parameters.Add("@hud_interview", SqlDbType.Int, 4, "hud_interview");
                        da.UpdateCommand = UpdateCmd;

                        using (SqlCommand DeleteCmd = new SqlCommand())
                        {
                            DeleteCmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            DeleteCmd.CommandText = "DELETE update_hud_interviews WHERE hud_interview=@hud_interview";
                            DeleteCmd.Parameters.Add("@hud_interview", SqlDbType.Int, 4, "hud_interview");
                            da.DeleteCommand = DeleteCmd;

                            try
                            {
                                da.Update(tbl);
                                return true;
                            }
                            catch (SqlException ex)
                            {
                                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating hud interviews");
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Process a change in the client ID
        /// </summary>

        private void txt_client_Validated(object sender, EventArgs e)
        {
            // Save the changes to the client.

            if (SaveChanges())
            {
                // Read the client name
                string client_name = string.Empty;

                // Attempt to read the new client's transactions into the list
                Int32 clientID = txt_client.EditValue.GetValueOrDefault(-1);
                if (clientID >= 0)
                {
                    client_name = ClientName(clientID);
                    lbl_client_name.Text = client_name;

                    using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
                    {
                        try
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                                cmd.CommandText = "SELECT [hud_interview],[client],[HUD_Grant],[interview_type],[interview_date],[interview_counselor],[hud_result],[result_date],[result_counselor],[termination_reason],[termination_date],[termination_counselor],[HousingFeeAmount] FROM hud_interviews WHERE client = @client";
                                cmd.Parameters.Add("@client", SqlDbType.Int).Value = clientID;

                                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                                {
                                    da.Fill(ds, "hud_interviews");
                                }
                            }

                            DataTable tbl = ds.Tables["hud_interviews"];
                            var _with1 = tbl;
                            if (_with1.PrimaryKey.GetUpperBound(0) < 0)
                            {
                                _with1.PrimaryKey = new DataColumn[] { _with1.Columns["hud_interview"] };
                            }

                            // If there are no items then say so
                            DataView vue = new DataView(tbl, string.Format("[client]={0:f0}", clientID), "interview_date", DataViewRowState.CurrentRows);
                            if (vue.Count <= 0)
                                txt_client.ErrorText = "There are no items for this client";

                            GridControl1.DataSource = vue;
                            GridControl1.RefreshDataSource();
                        }
                        catch (SqlException ex)
                        {
                            global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud transactions");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Read the client name from the database
        /// </summary>
        private string ClientName(Int32 ClientID)
        {
            if (ClientID <= 0)
            {
                return string.Empty;
            }

            using (SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
                {
                    try
                    {
                        cn.Open();
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT [name] FROM [view_client_address] WITH (NOLOCK) WHERE [client] = @client";
                            cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientID;
                            return (global::DebtPlus.Utils.Nulls.v_String(cmd.ExecuteScalar()) ?? string.Empty).Trim();
                        }
                    }
                    catch (SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client name");
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Save the changes when the form is closed
        /// </summary>

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Ask if the changes are to be made to the database
            if (GridControl1.DataSource != null && ((DataView)GridControl1.DataSource).Count > 0)
            {
                switch (DebtPlus.Data.Forms.MessageBox.Show("Do you wish to update the database with the changes before you quit?", "Are you sure?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.No:
                        break;

                    case DialogResult.Yes:
                        if (!SaveChanges())
                            e.Cancel = true;
                        break;

                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }

        /// <summary>
        /// Process a load event on the form
        /// </summary>

        private void Form1_Load(object sender, EventArgs e)
        {
            // Restore the previous size
            base.LoadPlacement("HUD.Interview.update");

            LookupEdit_Interview.DataSource = Housing_PurposeOfVisitTypes().DefaultView;
            LookupEdit_Interview.DisplayMember = "description";
            LookupEdit_Interview.ValueMember = "oID";
            LookupEdit_Interview.PopupWidth = col_hud_interview.Width + 200;

            LookupEdit_Result.DataSource = null;
            LookupEdit_Result.DisplayMember = "description";
            LookupEdit_Result.ValueMember = "oID";

            LookupEdit_Termination.DataSource = Housing_TerminationReasonTypes().DefaultView;
            LookupEdit_Termination.DisplayMember = "description";
            LookupEdit_Termination.ValueMember = "oID";
            LookupEdit_Termination.PopupWidth = col_hud_interview.Width + 200;

            LookUpEdit_HUD_Grant.DataSource = Housing_GrantTypes().DefaultView;
            LookUpEdit_HUD_Grant.DisplayMember = "description";
            LookUpEdit_HUD_Grant.ValueMember = "oID";
            LookUpEdit_HUD_Grant.PopupWidth = col_hud_interview.Width + 200;
        }

        /// <summary>
        /// Return a pointer to the interview types table
        /// </summary>
        private DataTable Housing_PurposeOfVisitTypes()
        {
            // Attempt to read the new client's transactions into the list
            DataTable tbl = ds.Tables["Housing_PurposeOfVisitTypes"];

            if (tbl != null)
            {
                return tbl;
            }

            using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "SELECT item_value AS oID, description as description FROM messages WITH (NOLOCK) WHERE 'HUD INTERVIEW TYPE' = item_type ORDER BY 1";
                        cmd.CommandType = CommandType.Text;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "Housing_PurposeOfVisitTypes");
                        }
                    }

                    return ds.Tables["Housing_PurposeOfVisitTypes"];
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud interview types");
                }
            }

            return null;
        }

        /// <summary>
        /// Return a pointer to the Grant types table
        /// </summary>
        private DataTable Housing_GrantTypes()
        {
            // Attempt to read the new client's transactions into the list
            DataTable tbl = ds.Tables["Housing_GrantTypes"];

            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with2 = cmd;
                        _with2.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with2.CommandText = "SELECT item_value AS oID, description as description FROM messages WITH (NOLOCK) WHERE 'HUD GRANT' = item_type ORDER BY 1";

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "Housing_GrantTypes");
                        }
                    }

                    tbl = ds.Tables["Housing_GrantTypes"];
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Housing_GrantTypes");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        /// <summary>
        /// Return a pointer to the result types table
        /// </summary>
        private DataTable Housing_VisitOutcomeTypes()
        {
            // Attempt to read the new client's transactions into the list
            DataTable tbl = ds.Tables["Housing_VisitOutcomeTypes"];

            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with3 = cmd;
                        _with3.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with3.CommandText = "SELECT item_value AS oID, description as description FROM messages WITH (NOLOCK) WHERE 'HUD RESULT' = item_type ORDER BY 1";

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "Housing_VisitOutcomeTypes");
                        }
                    }

                    tbl = ds.Tables["Housing_VisitOutcomeTypes"];
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud result types");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        /// <summary>
        /// Return a pointer to the interview types table
        /// </summary>
        private DataTable Housing_TerminationReasonTypes()
        {
            // Attempt to read the new client's transactions into the list
            DataTable tbl = ds.Tables["Housing_TerminationReasonTypes"];

            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with4 = cmd;
                        _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with4.CommandText = "SELECT item_value as oID, description FROM messages where item_type = 'HUD TERM REASON'";
                        _with4.CommandType = CommandType.Text;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "Housing_TerminationReasonTypes");
                        }
                    }

                    tbl = ds.Tables["Housing_TerminationReasonTypes"];
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading hud termination reasons");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        /// <summary>
        /// Return a pointer to the table listing valid results for types
        /// </summary>
        private DataTable Housing_AllowedVisitOutcomeTypesTable()
        {
            // Attempt to read the new client's transactions into the list
            DataTable tbl = ds.Tables["Housing_AllowedVisitOutcomeTypes"];

            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with5 = cmd;
                        _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with5.CommandText = "SELECT PurposeOfVisit AS hud_interview, Outcome AS hud_result FROM Housing_AllowedVisitOutcomeTypes WITH (NOLOCK) WHERE PurposeOfVisit IS NOT NULL AND Outcome IS NOT NULL ORDER BY 1, 2";

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "Housing_AllowedVisitOutcomeTypes");
                        }
                    }

                    tbl = ds.Tables["Housing_AllowedVisitOutcomeTypes"];
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Housing_AllowedVisitOutcomeTypes table");
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        /// <summary>
        /// When the row changes, reload the list of valid results
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            ReloadResultList(e.FocusedRowHandle);
        }

        /// <summary>
        /// When the interview is changed, reload the list of results
        /// </summary>
        private void GridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName == "interview_type")
            {
                ReloadResultList(e.RowHandle);
            }
        }

        /// <summary>
        /// reload the list of valid results
        /// </summary>
        private void ReloadResultList(Int32 Handle)
        {
            DataRow row = GridView1.GetDataRow(Handle);
            DataView DataSource = null;

            // From the row, find the interview id
            if (row != null && row["interview_type"] != null && !object.ReferenceEquals(row["interview_type"], DBNull.Value))
            {
                Int32 interview = Convert.ToInt32(row["interview_type"]);

                // Build a list of the valid results for this interview type
                StringBuilder sb = new StringBuilder();
                using (DataView vue = new DataView(Housing_AllowedVisitOutcomeTypesTable(), string.Format("[hud_interview]={0:f0}", interview), "hud_result", DataViewRowState.CurrentRows))
                {
                    foreach (DataRowView drv in vue)
                    {
                        sb.AppendFormat(",{0:f0}", drv["hud_result"]);
                    }
                }

                // If there are results then build the proper selection clause
                if (sb.Length > 0)
                {
                    sb.Remove(0, 1);
                    sb.Insert(0, "oID in (");
                    sb.Append(")");

                    // Create a view to select only those items that match the interview type
                    DataSource = new DataView(Housing_VisitOutcomeTypes(), sb.ToString(), "description", DataViewRowState.CurrentRows);
                }
            }

            var _with6 = LookupEdit_Result;
            _with6.DataSource = DataSource;
            _with6.PopupWidth = col_hud_result.Width + 200;
            _with6.BestFit();
        }

        /// <summary>
        /// Process the DELETE menu item click event
        /// </summary>
        private void MenuItem_delete_Click(object sender, EventArgs e)
        {
            DataRow row = GridView1.GetDataRow(GridView1.FocusedRowHandle);

            if (row != null)
            {
                if (DebtPlus.Data.Prompts.RequestConfirmation_Delete("Are you sure that you want to delete this item?") == DialogResult.Yes)
                {
                    row.Delete();
                    GridControl1.RefreshDataSource();
                }
            }
        }

        /// <summary>
        /// Find the row selected when the click event occurs
        /// </summary>

        private void GridView1_Click(object sender, EventArgs e)
        {
            // Find the row targeted as the double-click item
            GridView gv = (GridView)sender;
            GridHitInfo hi = gv.CalcHitInfo((gv.GridControl.PointToClient(MousePosition)));

            if (hi.InRow && hi.IsValid)
            {
                GridView1.FocusedRowHandle = hi.RowHandle;
            }
            else
            {
                GridView1.FocusedRowHandle = -1;
            }
        }

        /// <summary>
        /// Do the POPUP event for the right-click menu
        /// </summary>
        private void ContextMenu1_Popup(object sender, EventArgs e)
        {
            DataRow row = GridView1.GetDataRow(GridView1.FocusedRowHandle);
            MenuItem_delete.Enabled = row != null;
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string BasePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "DebtPlus";
            return Path.Combine(BasePath, "HUD.Interview.Update");
        }

        /// <summary>
        /// When loading the control, restore the layout from the file
        /// </summary>
        private void MyGridControl_Load(object sender, EventArgs e)
        {
            string PathName = XMLBasePath();
            string FileName = Path.Combine(PathName, Name + ".Grid.xml");

            try
            {
                if (File.Exists(FileName))
                {
                    GridView1.RestoreLayoutFromXml(FileName);
                }
            }
#pragma warning disable 168
            catch (DirectoryNotFoundException ex)
            {
            }
            catch (FileNotFoundException ex)
            {
            }
#pragma warning restore 168

            // Hook into the changes of the layout from this point
            GridView1.Layout += LayoutChanged;
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            string PathName = XMLBasePath();
            if (!Directory.Exists(PathName))
            {
                Directory.CreateDirectory(PathName);
            }

            string FileName = Path.Combine(PathName, Name + ".Grid.xml");
            GridView1.SaveLayoutToXml(FileName);
        }
    }
}