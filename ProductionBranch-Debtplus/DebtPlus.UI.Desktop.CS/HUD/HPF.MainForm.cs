﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.HUD.HPF
{
    public partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public MainForm()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MainForm_Load;
            simpleButton1.Click += new EventHandler(simpleButton1_Click);
            simpleButton2.Click += new EventHandler(simpleButton2_Click);
        }

        private void UnRegisterHandlers()
        {
            Load -= MainForm_Load;
            simpleButton1.Click -= new EventHandler(simpleButton1_Click);
            simpleButton2.Click -= new EventHandler(simpleButton2_Click);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
            }
            finally
            {
                RegisterHandlers();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            using (var cls = new DebtPlus.UI.Housing.HPF.SubmitCase())
            {
                // If successful submission then complete the dialog.
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    var clientRecord = bc.clients.Where(s => s.Id == Convert.ToInt32(clientID1.EditValue)).FirstOrDefault();
                    if (clientRecord != null)
                    {
                        if (cls.SendByClient(clientRecord).HasValue)
                        {
                            Close();
                        }
                    }
                }
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
