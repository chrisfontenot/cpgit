﻿namespace DebtPlus.UI.Desktop.CS.HUD.HPF
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientID1 = new DebtPlus.UI.Client.Widgets.Controls.ClientID();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientID1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // clientID1
            // 
            this.clientID1.AllowDrop = true;
            this.clientID1.Location = new System.Drawing.Point(116, 24);
            this.clientID1.Name = "clientID1";
            this.clientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.clientID1.Properties.Appearance.Options.UseTextOptions = true;
            this.clientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.clientID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DebtPlus.UI.Common.Controls.SearchButton()});
            this.clientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.clientID1.Properties.DisplayFormat.FormatString = "0000000";
            this.clientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.clientID1.Properties.EditFormat.FormatString = "f0";
            this.clientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.clientID1.Properties.Mask.BeepOnError = true;
            this.clientID1.Properties.Mask.EditMask = "\\d*";
            this.clientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.clientID1.Properties.ValidateOnEnterKey = true;
            this.clientID1.Size = new System.Drawing.Size(100, 20);
            this.clientID1.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton1.Location = new System.Drawing.Point(59, 65);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "&OK";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(69, 27);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(41, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Client ID";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.simpleButton2.Location = new System.Drawing.Point(151, 65);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "&Cancel";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 100);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.clientID1);
            this.Name = "MainForm";
            this.Text = "HPF Main Submission";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientID1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UI.Client.Widgets.Controls.ClientID clientID1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}