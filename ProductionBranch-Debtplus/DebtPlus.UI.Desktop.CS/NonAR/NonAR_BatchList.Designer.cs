using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.NonAR
{
	partial class NonAR_BatchList
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.ListBoxControl1 = new DevExpress.XtraEditors.ListBoxControl();
			((System.ComponentModel.ISupportInitialize)this.ListBoxControl1).BeginInit();
			this.SuspendLayout();
			//
			//ListBoxControl1
			//
			this.ListBoxControl1.Appearance.Options.UseTextOptions = true;
			this.ListBoxControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ListBoxControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ListBoxControl1.HotTrackSelectMode = DevExpress.XtraEditors.HotTrackSelectMode.SelectItemOnClick;
			this.ListBoxControl1.Location = new System.Drawing.Point(0, 0);
			this.ListBoxControl1.Name = "ListBoxControl1";
			this.ListBoxControl1.Size = new System.Drawing.Size(150, 150);
			this.ListBoxControl1.TabIndex = 0;
			this.ListBoxControl1.TabStop = false;
			//
			//NonAR_BatchList
			//
			this.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Appearance.Options.UseBackColor = true;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.ListBoxControl1);
			this.Name = "NonAR_BatchList";
			((System.ComponentModel.ISupportInitialize)this.ListBoxControl1).EndInit();
			this.ResumeLayout(false);
		}
		protected internal DevExpress.XtraEditors.ListBoxControl ListBoxControl1;
	}
}
