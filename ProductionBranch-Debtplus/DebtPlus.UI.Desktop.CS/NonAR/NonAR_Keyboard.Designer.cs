using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
	partial class NonAR_Keyboard
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try
            {
                if (disposing)
                {
                    if (components != null)
                    {
                        components.Dispose();
                    }
                }
			} 
            finally
            {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.DateEdit1 = new DevExpress.XtraEditors.DateEdit();
			this.TextEdit1 = new global::DebtPlus.UI.Desktop.CS.NonAR.Keypad_TextEdit();
			this.LookUpEdit1 = new global::DebtPlus.UI.Desktop.CS.NonAR.Keypad_LookupEdit();
			this.CalcEdit1 = new global::DebtPlus.UI.Desktop.CS.NonAR.Keypad_CalcEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			this.SuspendLayout();
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.DateEdit1);
			this.LayoutControl1.Controls.Add(this.TextEdit1);
			this.LayoutControl1.Controls.Add(this.LookUpEdit1);
			this.LayoutControl1.Controls.Add(this.CalcEdit1);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(339, 127);
			this.LayoutControl1.TabIndex = 0;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//DateEdit1
			//
			this.DateEdit1.EditValue = null;
			this.DateEdit1.Location = new System.Drawing.Point(87, 100);
			this.DateEdit1.Name = "DateEdit1";
			this.DateEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.DateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.DateEdit1.Properties.NullText = "[Please enter a date here]";
			this.DateEdit1.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.DateEdit1.Size = new System.Drawing.Size(246, 20);
			this.DateEdit1.StyleController = this.LayoutControl1;
			this.DateEdit1.TabIndex = 7;
			this.DateEdit1.ToolTip = "Enter or select the date for the draft. This is the date of the check, not today'" + "s date.";
			//
			//TextEdit1
			//
			this.TextEdit1.Location = new System.Drawing.Point(87, 69);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Properties.MaxLength = 50;
			this.TextEdit1.Size = new System.Drawing.Size(246, 20);
			this.TextEdit1.StyleController = this.LayoutControl1;
			this.TextEdit1.TabIndex = 6;
			this.TextEdit1.ToolTip = "Enter the id corresponding to the bank draft";
			//
			//LookUpEdit1
			//
			this.LookUpEdit1.Location = new System.Drawing.Point(87, 7);
			this.LookUpEdit1.Name = "LookUpEdit1";
			this.LookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit1.Properties.NullText = "[Please choose an item here]";
			this.LookUpEdit1.Size = new System.Drawing.Size(246, 20);
			this.LookUpEdit1.StyleController = this.LayoutControl1;
			this.LookUpEdit1.TabIndex = 4;
			this.LookUpEdit1.ToolTip = "Choose the apporpriate ledger account for the deposit";
			//
			//CalcEdit1
			//
			this.CalcEdit1.Location = new System.Drawing.Point(87, 38);
			this.CalcEdit1.Name = "CalcEdit1";
			this.CalcEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.CalcEdit1.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit1.Properties.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.CalcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit1.Properties.DisplayFormat.FormatString = "c2";
			this.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit1.Properties.EditFormat.FormatString = "f2";
			this.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit1.Properties.Mask.BeepOnError = true;
			this.CalcEdit1.Properties.Mask.EditMask = "c2";
			this.CalcEdit1.Size = new System.Drawing.Size(246, 20);
			this.CalcEdit1.StyleController = this.LayoutControl1;
			this.CalcEdit1.TabIndex = 5;
			this.CalcEdit1.ToolTip = "Enter the dollar amount of the item";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem2,
				this.LayoutControlItem1,
				this.LayoutControlItem3,
				this.LayoutControlItem4
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(339, 127);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.CalcEdit1;
			this.LayoutControlItem2.CustomizationFormText = "Dollar &Amount";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 31);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(337, 31);
			this.LayoutControlItem2.Text = "Dollar &Amount";
			this.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(75, 20);
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.LookUpEdit1;
			this.LayoutControlItem1.CustomizationFormText = "&Ledger Account";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(337, 31);
			this.LayoutControlItem1.Text = "&Ledger Account";
			this.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(75, 20);
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.TextEdit1;
			this.LayoutControlItem3.CustomizationFormText = "Check &ID";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 62);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(337, 31);
			this.LayoutControlItem3.Text = "Check &ID";
			this.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(75, 20);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.DateEdit1;
			this.LayoutControlItem4.CustomizationFormText = "Date of draft";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 93);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(337, 32);
			this.LayoutControlItem4.Text = "&Date of draft";
			this.LayoutControlItem4.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(75, 20);
			//
			//NonAR_Keyboard
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.LayoutControl1);
			this.Name = "NonAR_Keyboard";
			this.Size = new System.Drawing.Size(339, 127);
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.DateEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			this.ResumeLayout(false);
		}
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal Keypad_TextEdit TextEdit1;
		protected internal Keypad_LookupEdit LookUpEdit1;
		protected internal Keypad_CalcEdit CalcEdit1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraEditors.DateEdit DateEdit1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
	}
}
