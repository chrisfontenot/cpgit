using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    internal partial class NonAR_BatchList : SubForms
    {
        public NonAR_BatchList() : base()
        {
            InitializeComponent();
            ListBoxControl1.DrawItem += ListBoxControl1_DrawItem;
            ListBoxControl1.DoubleClick += ListBoxControl1_DoubleClick;
        }

        /// <summary>
        /// Do any pre-processing and load any unbound fields
        /// </summary>
        public override void ReadForm()
        {
            ListBoxControl1.DataSource = ((Form_Modal_NonAR)this.ParentForm).registers_non_ar.DefaultView;
            ListBoxControl1.DisplayMember = "credit_amt";
            ListBoxControl1.ValueMember = "non_ar_register";
        }

        /// <summary>
        /// Save any fields to the record before it is closed
        /// </summary>
        public override void SaveForm()
        {
        }

        /// <summary>
        /// Is the SAVE mode valid now?
        /// </summary>
        public override bool SaveValid
        {
            get { return true; }
        }

        /// <summary>
        /// Draw the items with a dollar sign
        /// </summary>
        private void ListBoxControl1_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            var ctl = (ListBoxControl)sender;

            // Put the background in the object
            e.Appearance.DrawBackground(e.Cache, e.Bounds);

            // Find the item for the display

            if (e.Index >= 0)
            {
                // From the index, find the row.
                DataRow row = ((DataView)ctl.DataSource)[e.Index].Row;

                // From the row, find the amount to be displayed
                decimal CreditAmt = 0m;
                if (row[ctl.DisplayMember] != null && !object.ReferenceEquals(row[ctl.DisplayMember], DBNull.Value))
                {
                    CreditAmt = Convert.ToDecimal(row[ctl.DisplayMember]);
                }

                // Format the display string and draw it
                string txt = string.Format("{0:c}", CreditAmt);
                e.Appearance.DrawString(e.Cache, txt, e.Bounds);
            }

            // Indicate that we displayed the row and do not do normal processing
            e.Handled = true;
        }

        /// <summary>
        /// DOUBLE-CLICK is "EDIT"
        /// </summary>
        private void ListBoxControl1_DoubleClick(object sender, EventArgs e)
        {
            var ctl = (ListBoxControl)sender;

            // Find where the use clicked
            Int32 HitIndex = ctl.IndexFromPoint(ctl.PointToClient(MousePosition));

            // If the user clicked on an item then it should be zero or positive. Blank area = negative
            if (HitIndex >= 0)
            {
                // From the index, find the row being clicked.
                DataView vue = (DataView)ctl.DataSource;
                DataRowView new_drv = vue[HitIndex];

                // The row can not be the one that we are just creating
                if (!new_drv.IsNew)
                {
                    // Run the edit form as a new modal dialog to edit the indicated row.
                    Form_NonAR parent = (Form_NonAR)ParentForm;
                    using (var frm = new Form_Modal_NonAR(parent.ap, new_drv))
                    {
                        new_drv.BeginEdit();
                        if (frm.ShowDialog() == DialogResult.OK)
                        {
                            new_drv.EndEdit();
                        }
                        else
                        {
                            new_drv.CancelEdit();
                        }
                    }
                }
            }
        }
    }
}