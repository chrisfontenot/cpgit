using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.NonAR
{
	partial class Form_Modal_NonAR
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
			this.NonAR_Source1 = new global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_Source();
			this.NonAR_BatchTotals1 = new global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_BatchTotals();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.Bar2 = new DevExpress.XtraBars.Bar();
			this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
			this.BarButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
			this.BarSubItem2 = new DevExpress.XtraBars.BarSubItem();
			this.BarButtonItem2 = new DevExpress.XtraBars.BarCheckItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
			this.NonAR_keyboard1 = new global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_Keyboard();
			this.NonAR_keypad1 = new global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_Keypad();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).BeginInit();
			this.PanelControl1.SuspendLayout();
			this.SuspendLayout();
			//
			//SimpleButton1
			//
			this.SimpleButton1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton1.Location = new System.Drawing.Point(386, 42);
			this.SimpleButton1.Name = "SimpleButton1";
			this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton1.TabIndex = 3;
			this.SimpleButton1.Text = "&Save";
			//
			//SimpleButton2
			//
			this.SimpleButton2.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton2.Location = new System.Drawing.Point(386, 71);
			this.SimpleButton2.Name = "SimpleButton2";
			this.SimpleButton2.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton2.TabIndex = 4;
			this.SimpleButton2.TabStop = false;
			this.SimpleButton2.Text = "&Cancel";
			//
			//NonAR_Source1
			//
			this.NonAR_Source1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.NonAR_Source1.Location = new System.Drawing.Point(13, 100);
			this.NonAR_Source1.Name = "NonAR_Source1";
			this.NonAR_Source1.Size = new System.Drawing.Size(357, 150);
			this.NonAR_Source1.TabIndex = 1;
			//
			//NonAR_BatchTotals1
			//
			this.NonAR_BatchTotals1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.NonAR_BatchTotals1.Location = new System.Drawing.Point(13, 27);
			this.NonAR_BatchTotals1.Name = "NonAR_BatchTotals1";
			this.NonAR_BatchTotals1.Size = new System.Drawing.Size(357, 67);
			this.NonAR_BatchTotals1.TabIndex = 0;
			this.NonAR_BatchTotals1.TabStop = false;
			//
			//barManager1
			//
			this.barManager1.AllowShowToolbarsPopup = false;
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarSubItem1,
				this.BarButtonItem1,
				this.BarSubItem2,
				this.BarButtonItem2
			});
			this.barManager1.MainMenu = this.Bar2;
			this.barManager1.MaxItemId = 4;
			//
			//Bar2
			//
			this.Bar2.BarName = "Main menu";
			this.Bar2.DockCol = 0;
			this.Bar2.DockRow = 0;
			this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
			this.Bar2.FloatLocation = new System.Drawing.Point(227, 144);
			this.Bar2.FloatSize = new System.Drawing.Size(105, 24);
			this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1) });
			this.Bar2.OptionsBar.MultiLine = true;
			this.Bar2.OptionsBar.UseWholeRow = true;
			this.Bar2.Text = "Main menu";
			//
			//BarSubItem1
			//
			this.BarSubItem1.Caption = "&File";
			this.BarSubItem1.Id = 0;
			this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem1) });
			this.BarSubItem1.Name = "BarSubItem1";
			//
			//BarButtonItem1
			//
			this.BarButtonItem1.Caption = "&Exit";
			this.BarButtonItem1.Id = 1;
			this.BarButtonItem1.Name = "BarButtonItem1";
			//
			//BarSubItem2
			//
			this.BarSubItem2.Caption = "&Options";
			this.BarSubItem2.Id = 2;
			this.BarSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem2) });
			this.BarSubItem2.Name = "BarSubItem2";
			//
			//BarButtonItem2
			//
			this.BarButtonItem2.Caption = "&Use Keypad Enter";
			this.BarButtonItem2.Id = 3;
			this.BarButtonItem2.Name = "BarButtonItem2";
			//
			//barDockControlTop
			//
			this.barDockControlTop.CausesValidation = false;
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(473, 22);
			//
			//barDockControlBottom
			//
			this.barDockControlBottom.CausesValidation = false;
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 402);
			this.barDockControlBottom.Size = new System.Drawing.Size(473, 0);
			//
			//barDockControlLeft
			//
			this.barDockControlLeft.CausesValidation = false;
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 380);
			//
			//barDockControlRight
			//
			this.barDockControlRight.CausesValidation = false;
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(473, 22);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 380);
			//
			//PanelControl1
			//
			this.PanelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.PanelControl1.Controls.Add(this.NonAR_keyboard1);
			this.PanelControl1.Controls.Add(this.NonAR_keypad1);
			this.PanelControl1.Location = new System.Drawing.Point(13, 256);
			this.PanelControl1.Name = "PanelControl1";
			this.PanelControl1.Size = new System.Drawing.Size(358, 134);
			this.PanelControl1.TabIndex = 2;
			//
			//NonAR_keyboard1
			//
			this.NonAR_keyboard1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NonAR_keyboard1.Location = new System.Drawing.Point(0, 0);
			this.NonAR_keyboard1.Name = "NonAR_keyboard1";
			this.NonAR_keyboard1.Size = new System.Drawing.Size(358, 134);
			this.NonAR_keyboard1.TabIndex = 1;
			this.NonAR_keyboard1.TabStop = false;
			//
			//NonAR_keypad1
			//
			this.NonAR_keypad1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NonAR_keypad1.Location = new System.Drawing.Point(0, 0);
			this.NonAR_keypad1.Name = "NonAR_keypad1";
			this.NonAR_keypad1.Size = new System.Drawing.Size(358, 134);
			this.NonAR_keypad1.TabIndex = 0;
			this.NonAR_keypad1.TabStop = false;
			//
			//Form_Modal_NonAR
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(473, 402);
			this.Controls.Add(this.PanelControl1);
			this.Controls.Add(this.SimpleButton2);
			this.Controls.Add(this.SimpleButton1);
			this.Controls.Add(this.NonAR_Source1);
			this.Controls.Add(this.NonAR_BatchTotals1);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "Form_Modal_NonAR";
			this.Text = "Add Non-AR Deposits to Ledger Accounts";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).EndInit();
			this.PanelControl1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		protected internal global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_BatchTotals NonAR_BatchTotals1;
		protected internal global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_Source NonAR_Source1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
		protected internal DevExpress.XtraBars.BarManager barManager1;
		protected internal DevExpress.XtraBars.Bar Bar2;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlTop;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlRight;
		protected internal DevExpress.XtraEditors.PanelControl PanelControl1;
		protected internal global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_Keypad NonAR_keypad1;
		protected internal global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_Keyboard NonAR_keyboard1;
		protected internal DevExpress.XtraBars.BarSubItem BarSubItem1;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem1;
		protected internal DevExpress.XtraBars.BarSubItem BarSubItem2;
		protected internal DevExpress.XtraBars.BarCheckItem BarButtonItem2;
	}
}
