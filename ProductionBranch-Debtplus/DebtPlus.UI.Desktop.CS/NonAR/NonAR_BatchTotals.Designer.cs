using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.NonAR
{
	partial class NonAR_BatchTotals
    {

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.edit_item_total = new DevExpress.XtraEditors.TextEdit();
			this.edit_item_count = new DevExpress.XtraEditors.TextEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.edit_item_total.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.edit_item_count.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			this.SuspendLayout();
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.edit_item_total);
			this.LayoutControl1.Controls.Add(this.edit_item_count);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(184, 67);
			this.LayoutControl1.TabIndex = 0;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//edit_item_total
			//
			this.edit_item_total.EditValue = "$0.00";
			this.edit_item_total.Location = new System.Drawing.Point(66, 38);
			this.edit_item_total.Name = "edit_item_total";
			this.edit_item_total.Properties.AllowFocused = false;
			this.edit_item_total.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.edit_item_total.Properties.Appearance.Options.UseTextOptions = true;
			this.edit_item_total.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.edit_item_total.Properties.ReadOnly = true;
			this.edit_item_total.Size = new System.Drawing.Size(112, 20);
			this.edit_item_total.StyleController = this.LayoutControl1;
			this.edit_item_total.TabIndex = 5;
			//
			//edit_item_count
			//
			this.edit_item_count.EditValue = "0";
			this.edit_item_count.Location = new System.Drawing.Point(66, 7);
			this.edit_item_count.Name = "edit_item_count";
			this.edit_item_count.Properties.AllowFocused = false;
			this.edit_item_count.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.edit_item_count.Properties.Appearance.Options.UseTextOptions = true;
			this.edit_item_count.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.edit_item_count.Properties.ReadOnly = true;
			this.edit_item_count.Size = new System.Drawing.Size(112, 20);
			this.edit_item_count.StyleController = this.LayoutControl1;
			this.edit_item_count.TabIndex = 4;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(184, 67);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.edit_item_count;
			this.LayoutControlItem1.CustomizationFormText = "Item Count";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(182, 31);
			this.LayoutControlItem1.Text = "Item Count";
			this.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(54, 20);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.edit_item_total;
			this.LayoutControlItem2.CustomizationFormText = "Item Total";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 31);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(182, 34);
			this.LayoutControlItem2.Text = "Item Total";
			this.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(54, 20);
			//
			//NonAR_BatchTotals
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.LayoutControl1);
			this.Name = "NonAR_BatchTotals";
			this.Size = new System.Drawing.Size(184, 67);
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.edit_item_total.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.edit_item_count.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraEditors.TextEdit edit_item_total;
		protected internal DevExpress.XtraEditors.TextEdit edit_item_count;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
	}
}
