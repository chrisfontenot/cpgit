using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    internal partial class Form_NonAR : Form_Modal_NonAR
    {
        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public Form_NonAR() : base()
        {
            InitializeComponent();
        }

        public Form_NonAR(ArgParser ap) : base(ap)
        {
            InitializeComponent();
            this.Load += Form_NonAR_Load;
        }

        /// <summary>
        /// Process the loading of the form
        /// </summary>

        private void Form_NonAR_Load(object sender, System.EventArgs e)
        {
            // Read the list of transactions for this batch
            ReadRegistersNonArTable(-1);
            registers_non_ar = Globals.ds.Tables["registers_non_ar"];
            var _with1 = registers_non_ar;
            _with1.PrimaryKey = new DataColumn[] { _with1.Columns["non_ar_register"] };
            var _with2 = _with1.Columns["non_ar_register"];
            _with2.AllowDBNull = false;
            _with2.AutoIncrement = true;
            _with2.AutoIncrementSeed = -1;
            _with2.AutoIncrementStep = -1;

            var _with3 = _with1.Columns["tran_type"];
            _with3.AllowDBNull = false;
            _with3.DefaultValue = "NA";
            _with3.MaxLength = 2;

            var _with4 = _with1.Columns["non_ar_source"];
            _with4.AllowDBNull = true;

            var _with5 = _with1.Columns["client"];
            _with5.AllowDBNull = true;

            var _with6 = _with1.Columns["creditor"];
            _with6.AllowDBNull = true;
            _with6.MaxLength = 10;

            var _with7 = _with1.Columns["non_ar_source"];
            _with7.AllowDBNull = true;
            _with7.DefaultValue = DBNull.Value;

            var _with8 = _with1.Columns["credit_amt"];
            _with8.DefaultValue = 0m;
            _with8.AllowDBNull = false;

            var _with9 = _with1.Columns["debit_amt"];
            _with9.DefaultValue = 0m;
            _with9.AllowDBNull = false;

            var _with10 = _with1.Columns["date_created"];
            _with10.DefaultValue = DateTime.Now.Date;
            _with10.AllowDBNull = false;

            var _with11 = _with1.Columns["item_date"];
            _with11.DefaultValue = DateTime.Now.Date;
            _with11.AllowDBNull = false;

            var _with12 = _with1.Columns["reference"];
            _with12.AllowDBNull = true;
            _with12.MaxLength = 50;

            var _with13 = _with1.Columns["created_by"];
            _with13.AllowDBNull = false;
            _with13.DefaultValue = "ME";

            var _with14 = _with1.Columns["src_ledger_account"];
            _with14.AllowDBNull = true;
            _with14.MaxLength = 10;

            var _with15 = _with1.Columns["dst_ledger_account"];
            _with15.AllowDBNull = true;
            _with15.MaxLength = 10;

            var _with16 = _with1.Columns["message"];
            _with16.AllowDBNull = true;
            _with16.MaxLength = 256;

            // Create the first record
            System.Data.DataView vue = registers_non_ar.DefaultView;
            drv = vue.AddNew();
            drv.BeginEdit();

            // Bind the new items to a new record
            ItemEdit.ReadForm();
            NonAR_Source1.ReadForm();
            NonAR_BatchList1.ReadForm();
            NonAR_BatchTotals1.ReadForm();

            // Add the handlers once the initialization is complete
            FormClosing += Form_NonAR_FormClosing;
            SimpleButton3.Click += SimpleButton3_Click;

            // Hook into the changes in the table to recalculate the totals
            registers_non_ar.RowChanged += TableChanged;
            registers_non_ar.RowDeleted += TableChanged;
            registers_non_ar.TableNewRow += TableNewRow;
        }

        /// <summary>
        /// Update the totals when the form is changed
        /// </summary>
        protected void TableChanged(object sender, System.Data.DataRowChangeEventArgs e)
        {
            NonAR_BatchTotals1.ReadForm();
        }

        protected void TableNewRow(object sender, System.Data.DataTableNewRowEventArgs e)
        {
            NonAR_BatchTotals1.ReadForm();
        }

        /// <summary>
        /// Save the changes when the SAVE button is pressed
        /// This also creates a new row for the edit and starts the process again
        /// </summary>

        protected override void SimpleButton1_Click(System.Object sender, System.EventArgs e)
        {
            if (SimpleButton1.Enabled)
            {
                // Save the current row
                ItemEdit.SaveForm();
                NonAR_Source1.SaveForm();
                drv.EndEdit();

                // Create a new row for the new item
                drv = drv.DataView.AddNew();
                ItemEdit.ReadForm();
                NonAR_Source1.ReadForm();
                NonAR_BatchTotals1.ReadForm();
            }
        }

        /// <summary>
        /// When a field is changed, update the button status
        /// </summary>
        protected override void Form_Changed(object sender, System.EventArgs e)
        {
            base.Form_Changed(sender, e);
            SimpleButton3.Enabled = SimpleButton1.Enabled;
        }

        /// <summary>
        /// Close the form when the menu item is choosen
        /// </summary>
        protected override void BarButtonItem1_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Close the form when the CANCEL button is choosen
        /// </summary>
        protected override void SimpleButton2_Click(System.Object sender, System.EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Read the table definitions for the updates
        /// </summary>
        protected void ReadRegistersNonArTable(System.Int32 non_ar_register)
        {
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(SelectCommand());
                da.Fill(Globals.ds, "registers_non_ar");
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading registers_non_ar table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        protected internal System.Data.DataRow ReadRegistersNonAr(System.Int32 non_ar_register)
        {
            System.Data.DataRow answer = null;
            System.Data.DataTable tbl = Globals.ds.Tables["registers_non_ar"];
            if (tbl != null)
            {
                answer = tbl.Rows.Find(non_ar_register);
            }

            // If the row is not present then read it.
            if (answer == null)
            {
                ReadRegistersNonArTable(non_ar_register);
                tbl = Globals.ds.Tables["registers_non_ar"];
                if (tbl != null)
                {
                    answer = tbl.Rows.Find(non_ar_register);
                }
            }

            return answer;
        }

        /// <summary>
        /// SELECT command to read the table
        /// </summary>
        protected System.Data.SqlClient.SqlCommand SelectCommand()
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            var _with17 = cmd;
            _with17.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with17.CommandText = "SELECT non_ar_register,tran_type,non_ar_source,client,creditor,credit_amt,debit_amt,date_created,item_date,reference,created_by,src_ledger_account,dst_ledger_account,message FROM registers_non_ar WHERE non_ar_register=@non_ar_register";
            _with17.CommandType = CommandType.Text;
            _with17.Parameters.Add("@non_ar_register", SqlDbType.Int).Value = -1;
            return cmd;
        }

        /// <summary>
        /// INSERT command to add to the table
        /// </summary>
        protected System.Data.SqlClient.SqlCommand InsertCommand()
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();

            // Generate the insert command to insert new rows
            var _with18 = cmd;
            _with18.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with18.CommandText = "INSERT INTO registers_non_ar(tran_type,non_ar_source,client,creditor,credit_amt,debit_amt,item_date,reference,src_ledger_account,dst_ledger_account,message) values (@tran_type,@non_ar_source,@client,@creditor,@credit_amt,@debit_amt,@item_date,@reference,@src_ledger_account,@dst_ledger_account,@message)";
            _with18.CommandType = CommandType.Text;
            _with18.Parameters.Add("@tran_type", SqlDbType.VarChar, 10, "tran_type");
            _with18.Parameters.Add("@non_ar_source", SqlDbType.Int, 0, "non_ar_source");
            _with18.Parameters.Add("@client", SqlDbType.Int, 0, "client");
            _with18.Parameters.Add("@creditor", SqlDbType.VarChar, 10, "creditor");
            _with18.Parameters.Add("@credit_amt", SqlDbType.Money, 0, "credit_amt");
            _with18.Parameters.Add("@debit_amt", SqlDbType.Money, 0, "debit_amt");
            _with18.Parameters.Add("@item_date", SqlDbType.DateTime, 8, "item_date");
            _with18.Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference");
            _with18.Parameters.Add("@src_ledger_account", SqlDbType.VarChar, 10, "src_ledger_account");
            _with18.Parameters.Add("@dst_ledger_account", SqlDbType.VarChar, 10, "dst_ledger_account");
            _with18.Parameters.Add("@message", SqlDbType.VarChar, 256, "message");

            return cmd;
        }

        /// <summary>
        /// UPDATE command to change the table
        /// </summary>
        protected System.Data.SqlClient.SqlCommand UpdateCommand()
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();

            // Generate the insert command to insert new rows
            var _with19 = cmd;
            _with19.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with19.CommandText = "update registers_non_ar set tran_type=@tran_type,non_ar_source=@non_ar_source,client=@client,creditor=@creditor,credit_amt=@credit_amt,debit_amt=@debit_amt,item_date=@item_date,reference=@reference,src_ledger_account=@src_ledger_account,dst_ledger_account=@dst_ledger_account,dst_ledger_account,message=@message WHERE non_ar_register=@non_ar_register";
            _with19.CommandType = CommandType.Text;
            _with19.Parameters.Add("@tran_type", SqlDbType.VarChar, 10, "tran_type");
            _with19.Parameters.Add("@non_ar_source", SqlDbType.Int, 0, "non_ar_source");
            _with19.Parameters.Add("@client", SqlDbType.Int, 0, "client");
            _with19.Parameters.Add("@creditor", SqlDbType.VarChar, 10, "creditor");
            _with19.Parameters.Add("@credit_amt", SqlDbType.Money, 0, "credit_amt");
            _with19.Parameters.Add("@debit_amt", SqlDbType.Money, 0, "debit_amt");
            _with19.Parameters.Add("@item_date", SqlDbType.DateTime, 8, "item_date");
            _with19.Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference");
            _with19.Parameters.Add("@src_ledger_account", SqlDbType.VarChar, 10, "src_ledger_account");
            _with19.Parameters.Add("@dst_ledger_account", SqlDbType.VarChar, 10, "dst_ledger_account");
            _with19.Parameters.Add("@message", SqlDbType.VarChar, 256, "message");
            _with19.Parameters.Add("@non_ar_register", SqlDbType.Int, 0, "non_ar_register");

            return cmd;
        }

        /// <summary>
        /// DELETE command to delete rows from the table
        /// </summary>
        protected System.Data.SqlClient.SqlCommand DeleteCommand()
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();

            // Generate the insert command to insert new rows
            var _with20 = cmd;
            _with20.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with20.CommandText = "DELETE FROM registers_non_ar WHERE non_ar_register=@non_ar_register";
            _with20.CommandType = CommandType.Text;
            _with20.Parameters.Add("@non_ar_register", SqlDbType.Int, 0, "non_ar_register");

            return cmd;
        }

        /// <summary>
        /// Determine if there are pending items for the update
        /// </summary>
        protected bool PendingItems()
        {
            bool answer = false;
            System.Data.DataTable tbl = Globals.ds.Tables["registers_non_ar"];
            System.Data.DataView vue = null;

            if (tbl != null)
            {
                vue = new System.Data.DataView(tbl, string.Empty, string.Empty, DataViewRowState.Added);
                if (vue.Count > 0)
                    answer = true;

                if (!answer)
                {
                    vue = new System.Data.DataView(tbl, string.Empty, string.Empty, DataViewRowState.ModifiedCurrent);
                    if (vue.Count > 0)
                        answer = true;
                }

                if (!answer)
                {
                    vue = new System.Data.DataView(tbl, string.Empty, string.Empty, DataViewRowState.Deleted);
                    if (vue.Count > 0)
                        answer = true;
                }
            }

            return answer;
        }

        /// <summary>
        /// Ask if the update is desired when the form is closed
        /// </summary>

        protected void Form_NonAR_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (PendingItems())
            {
                switch (DebtPlus.Data.Forms.MessageBox.Show("You have items that have not been saved to the database." + Environment.NewLine + Environment.NewLine + "Do you wish to save them now?", "Pending Items", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1))
                {
                    case System.Windows.Forms.DialogResult.No:
                        break;

                    case System.Windows.Forms.DialogResult.Yes:
                        if (!ApplyChanges())
                            e.Cancel = true;
                        break;

                    default:
                        e.Cancel = true;
                        return;
                }
            }
        }

        /// <summary>
        /// Do the apply function to save and update the table
        /// </summary>
        private void SimpleButton3_Click(System.Object sender, System.EventArgs e)
        {
            if (drv != null)
            {
                // Save the current changes
                ItemEdit.SaveForm();
                NonAR_Source1.SaveForm();
                drv.EndEdit();

                // Update the database with the new changes and clear the pending table
                if (ApplyChanges())
                {
                    registers_non_ar.Clear();
                    drv = registers_non_ar.DefaultView.AddNew();
                }

                // Create a new row for the new item
                ItemEdit.ReadForm();
                NonAR_Source1.ReadForm();
                NonAR_BatchTotals1.ReadForm();
                drv.BeginEdit();
            }
        }

        /// <summary>
        /// update the table
        /// </summary>
        private bool ApplyChanges()
        {
            bool Result = false;
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter();
                da.InsertCommand = InsertCommand();
                da.UpdateCommand = UpdateCommand();
                da.DeleteCommand = DeleteCommand();

                System.Data.DataTable tbl = Globals.ds.Tables["registers_non_ar"];
                da.Update(tbl);
                Result = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the database");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            return Result;
        }
    }
}