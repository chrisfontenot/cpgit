#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    internal partial class NonAR_BatchTotals : SubForms
    {
        public NonAR_BatchTotals() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Is the SAVE mode valid now?
        /// </summary>
        public override bool SaveValid
        {
            get { return true; }
        }

        /// <summary>
        /// Do any pre-processing and load any unbound fields
        /// </summary>
        public override void ReadForm()
        {
            System.Int32 Items = ((Form_Modal_NonAR)this.ParentForm).registers_non_ar.Rows.Count;
            object Total = ((Form_Modal_NonAR)this.ParentForm).registers_non_ar.Compute("sum(credit_amt)", string.Empty);
            if (Total == null || object.ReferenceEquals(Total, System.DBNull.Value))
                Total = 0m;

            // Update the display counts correctly
            edit_item_count.Text = string.Format("{0:n0}", Items);
            edit_item_total.Text = string.Format("{0:c}", Convert.ToDecimal(Total));
        }

        /// <summary>
        /// Save any fields to the record before it is closed
        /// </summary>
        public override void SaveForm()
        {
        }
    }
}