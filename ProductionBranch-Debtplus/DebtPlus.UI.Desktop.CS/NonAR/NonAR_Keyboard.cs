#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    internal partial class NonAR_Keyboard : SubForms
    {
        public NonAR_Keyboard() : base()
        {
            InitializeComponent();
            CalcEdit1.EditValueChanged += DateEdit1_EditValueChanged;
            this.Load += NonAR_Keyboard_Load;
            this.TabStopChanged += NonAR_Keyboard_TabStopChanged;
            LookUpEdit1.EditValueChanged += DateEdit1_EditValueChanged;
            TextEdit1.EditValueChanged += DateEdit1_EditValueChanged;
            DateEdit1.EditValueChanged += DateEdit1_EditValueChanged;
        }

        /// <summary>
        /// Is the SAVE mode valid now?
        /// </summary>
        public override bool SaveValid
        {
            get { return TextEdit1.ErrorText == string.Empty && CalcEdit1.ErrorText == string.Empty && DateEdit1.ErrorText == string.Empty && LookUpEdit1.ErrorText == string.Empty; }
        }

        /// <summary>
        /// Interfaces to the various fields of this record
        /// </summary>
        public override string CheckId
        {
            get { return Convert.ToString(TextEdit1.EditValue).Trim(); }
        }

        public override decimal DollarAmount
        {
            get { return Convert.ToDecimal(CalcEdit1.EditValue); }
        }

        public override object ItemDate
        {
            get { return DateEdit1.EditValue; }
        }

        public override string LedgerAccount
        {
            get { return Convert.ToString(LookUpEdit1.EditValue); }
        }

        /// <summary>
        /// Do any pre-processing and load any unbound fields
        /// </summary>
        public override void ReadForm()
        {
            // Find the source items from the list of ar sources
            DataView source_view = (DataView)LookUpEdit1.Properties.DataSource;
            DataRowView drv = ((Form_Modal_NonAR)this.ParentForm).drv;

            if (source_view == null)
            {
                DataTable tbl = Globals.ds.Tables["ledger_codes"];
                if (tbl == null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "select ledger_code,description FROM ledger_codes ORDER BY description";
                    cmd.CommandType = CommandType.Text;

                    Cursor current_cursor = Cursor.Current;
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(Globals.ds, "ledger_codes");
                        tbl = Globals.ds.Tables["ledger_codes"];
                    }
                    catch (SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading ledger_codes from database");
                    }
                    finally
                    {
                        Cursor.Current = current_cursor;
                    }
                }

                source_view = new DataView(tbl, string.Empty, "description", DataViewRowState.CurrentRows);
                LookUpEdit1.Properties.DataSource = source_view;
                LookUpEdit1.Properties.DisplayMember = "description";
                LookUpEdit1.Properties.ValueMember = "ledger_code";

                LookUpEdit1.Properties.ShowFooter = false;
                LookUpEdit1.Properties.ShowHeader = false;
                LookUpEdit1.Properties.ShowLines = true;
                LookUpEdit1.Properties.Columns.AddRange(new LookUpColumnInfo[] {
                    new LookUpColumnInfo("ledger_code", "ledger_code", 25, FormatType.None, string.Empty, false, HorzAlignment.Far),
                    new LookUpColumnInfo("description", "description", 75, FormatType.None, string.Empty, true, HorzAlignment.Near)
                });
            }

            // Bind the data members to the items
            LookUpEdit1.DataBindings.Clear();
            LookUpEdit1.DataBindings.Add("EditValue", drv, "dst_ledger_account");
            LookUpEdit1.EditValueChanging += global::DebtPlus.Data.Validation.LookUpEdit_ActiveTest;
            LookUpEdit1.EditValueChanging += LookupEdit1_EditValueChanging;

            CalcEdit1.DataBindings.Clear();
            CalcEdit1.DataBindings.Add("EditValue", drv, "credit_amt");
            CalcEdit1.EditValueChanging += CalcEdit1_EditValueChanging;

            TextEdit1.DataBindings.Clear();
            TextEdit1.DataBindings.Add("EditValue", drv, "reference");

            DateEdit1.DataBindings.Clear();
            DateEdit1.DataBindings.Add("EditValue", drv, "item_date");
            DateEdit1.EditValueChanging += DateEdit1_EditValueChanging;
        }

        private void DateEdit1_EditValueChanged(object sender, EventArgs e)
        {
            RaiseChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Handle the change in the amount field
        /// </summary>
        private void CalcEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string ErrorText = string.Empty;
            if (e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value) || Convert.ToDecimal(e.NewValue) <= 0m)
            {
                ErrorText = Globals.MisingText;
            }
            CalcEdit1.ErrorText = ErrorText;
        }

        /// <summary>
        /// Handle the change in the ledger account field
        /// </summary>
        private void LookupEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string ErrorText = string.Empty;
            if (e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value))
            {
                ErrorText = Globals.MisingText;
            }
            LookUpEdit1.ErrorText = ErrorText;
        }

        /// <summary>
        /// Handle the change in the date field
        /// </summary>
        private void DateEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string ErrorText = string.Empty;
            if (e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value))
            {
                ErrorText = Globals.MisingText;
            }
            DateEdit1.ErrorText = ErrorText;
        }

        /// <summary>
        /// Process the load event for our control
        /// </summary>
        private void NonAR_Keyboard_Load(object sender, EventArgs e)
        {
            LookUpEdit1.TabStop = false;
            CalcEdit1.TabStop = false;
            TextEdit1.TabStop = false;
            DateEdit1.TabStop = false;
        }

        /// <summary>
        /// Process the change in tab stop for our control.
        /// </summary>

        private void NonAR_Keyboard_TabStopChanged(object sender, EventArgs e)
        {
            // Enable or disable the tab stop processing as the control is switched
            LookUpEdit1.TabStop = TabStop;
            CalcEdit1.TabStop = TabStop;
            TextEdit1.TabStop = TabStop;
            DateEdit1.TabStop = TabStop;
        }
    }
}