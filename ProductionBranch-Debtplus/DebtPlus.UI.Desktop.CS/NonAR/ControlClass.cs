﻿using System;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    public delegate void ChangedEventHandler(object sender, EventArgs e);

    internal class SubForms : DevExpress.XtraEditors.XtraUserControl
    {
        public event ChangedEventHandler Changed;

        protected void RaiseChanged(EventArgs e)
        {
            var evt = Changed;
            if (evt != null)
            {
                evt(this, e);
            }
        }

        protected virtual void OnChanged(EventArgs e)
        {
            RaiseChanged(e);
        }

        public virtual void ReadForm()
        {
        }

        public virtual void SaveForm()
        {
        }

        public virtual bool SaveValid
        {
            get
            {
                return false;
            }
        }

        public virtual string LedgerAccount
        {
            get
            {
                return null;
            }
        }

        public virtual decimal DollarAmount
        {
            get
            {
                return 0M;
            }
        }

        public virtual string CheckId
        {
            get
            {
                return null;
            }
        }

        public virtual object ItemDate
        {
            get
            {
                return System.DBNull.Value;
            }
        }
    }
}