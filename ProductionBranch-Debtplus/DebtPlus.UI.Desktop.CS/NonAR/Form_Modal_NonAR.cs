#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    internal partial class Form_Modal_NonAR : DebtPlus.Data.Forms.DebtPlusForm
    {
        // This is the item being used for the edit format. It is either a keypad or keyboard interface.
        protected SubForms ItemEdit;

        protected internal DataTable registers_non_ar = null;

        protected internal DataRowView drv = null;

        public Form_Modal_NonAR() : base()
        {
            InitializeComponent();
            this.Load += Form_NonAR_Load;
        }

        internal ArgParser ap = null;

        public Form_Modal_NonAR(ArgParser ap) : this()
        {
            this.ap = ap;
        }

        public Form_Modal_NonAR(ArgParser ap, DataRowView drv) : this(ap)
        {
            this.drv = drv;
        }

        /// <summary>
        /// Change the keyboard input control as needed
        /// </summary>

        protected void SetItemEditControl(SubForms Editor)
        {
            if (ItemEdit != null)
            {
                if (drv != null)
                    ItemEdit.SaveForm();
                var _with1 = (XtraUserControl)ItemEdit;
                _with1.Enabled = false;
                _with1.Visible = false;
                _with1.TabStop = false;
                _with1.SendToBack();
            }

            ItemEdit = Editor;
            var _with2 = (XtraUserControl)ItemEdit;
            _with2.Enabled = true;
            _with2.Visible = true;
            _with2.TabStop = true;
            _with2.BringToFront();
            if (drv != null)
                ItemEdit.ReadForm();
        }

        private void Form_NonAR_Load(object sender, EventArgs e)
        {
            // Read the list of transactions for this batch
            registers_non_ar = Globals.ds.Tables["registers_non_ar"];

            // Use the keypad if indicated
            if (DebtPlus.UI.Desktop.CS.Deposit.Manual.Controls.Defaults.EnterMoveNextControl())
            {
                SetItemEditControl(NonAR_keypad1);
                BarButtonItem2.Checked = true;
            }
            else
            {
                SetItemEditControl(NonAR_keyboard1);
            }

            // Bind the new items to a new record
            if (drv != null)
            {
                ItemEdit.ReadForm();
                NonAR_Source1.ReadForm();
                NonAR_BatchTotals1.ReadForm();
            }

            // Add the handlers once the initialization is complete
            SimpleButton1.Click += SimpleButton1_Click;
            SimpleButton2.Click += SimpleButton2_Click;
            BarButtonItem1.ItemClick += BarButtonItem1_ItemClick;

            // These are the change events in the sub-forms
            ItemEdit.Changed += Form_Changed;
            NonAR_Source1.Changed += Form_Changed;
        }

        protected virtual void SimpleButton1_Click(object sender, EventArgs e)
        {
            if (SimpleButton1.Enabled)
            {
                ItemEdit.SaveForm();
                NonAR_Source1.SaveForm();
                DialogResult = DialogResult.OK;
            }
        }

        protected virtual void SimpleButton2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual void BarButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        protected virtual void Form_Changed(object sender, EventArgs e)
        {
            SimpleButton1.Enabled = ItemEdit.SaveValid && NonAR_Source1.SaveValid;
        }
    }

    static internal partial class Globals
    {
        static internal DataSet ds = new DataSet("ds");
        static internal readonly string MisingText = "A value is required";
    }
}