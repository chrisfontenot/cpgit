#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    internal partial class NonAR_Keypad : SubForms
    {
        public NonAR_Keypad() : base()
        {
            InitializeComponent();
            TextEdit2.Validating += TextEdit2_Validating;
            TextEdit2.EditValueChanged += TextEdit2_EditValueChanged;
            CalcEdit1.EditValueChanging += CalcEdit1_EditValueChanging;
            LookUpEdit1.EditValueChanging += LookupEdit1_EditValueChanging;
            this.Load += NonAR_Keypad_Load;
            this.TabStopChanged += NonAR_Keypad_TabStopChanged;
            CalcEdit1.EditValueChanged += TextEdit2_EditValueChanged;
            LookUpEdit1.EditValueChanged += TextEdit2_EditValueChanged;
            TextEdit1.EditValueChanged += TextEdit2_EditValueChanged;
        }

        /// <summary>
        /// Is the SAVE mode valid now?
        /// </summary>
        public override bool SaveValid
        {
            get { return TextEdit1.ErrorText == string.Empty && CalcEdit1.ErrorText == string.Empty && TextEdit2.ErrorText == string.Empty && LookUpEdit1.ErrorText == string.Empty; }
        }

        public override string CheckId
        {
            get { return Convert.ToString(TextEdit1.EditValue).Trim(); }
        }

        public override decimal DollarAmount
        {
            get { return Convert.ToDecimal(CalcEdit1.EditValue); }
        }

        public override object ItemDate
        {
            get
            {
                System.DateTime value = default(System.DateTime);
                if (!System.DateTime.TryParse(TextEdit2.Text.Trim(), out value))
                    value = System.DateTime.Now;
                return value;
            }
        }

        public override string LedgerAccount
        {
            get { return Convert.ToString(LookUpEdit1.EditValue); }
        }

        /// <summary>
        /// Do any pre-processing and load any unbound fields
        /// </summary>
        public override void ReadForm()
        {
            // Find the source items from the list of ar sources
            DataView source_view = (DataView)LookUpEdit1.Properties.DataSource;
            DataRowView drv = ((Form_Modal_NonAR)this.ParentForm).drv;

            if (source_view == null)
            {
                DataTable tbl = Globals.ds.Tables["ledger_codes"];
                if (tbl == null)
                {
                    SqlCommand cmd = new SqlCommand();
                    var _with1 = cmd;
                    _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with1.CommandText = "select ledger_code,description FROM ledger_codes ORDER BY description";
                    _with1.CommandType = CommandType.Text;

                    Cursor current_cursor = Cursor.Current;
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(Globals.ds, "ledger_codes");
                        tbl = Globals.ds.Tables["ledger_codes"];
                    }
                    catch (SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading ledger_codes from database");
                    }
                    finally
                    {
                        Cursor.Current = current_cursor;
                    }
                }

                source_view = new DataView(tbl, string.Empty, "description", DataViewRowState.CurrentRows);
                var _with2 = LookUpEdit1.Properties;
                _with2.DataSource = source_view;
                _with2.DisplayMember = "description";
                _with2.ValueMember = "ledger_code";

                _with2.ShowFooter = false;
                _with2.ShowHeader = false;
                _with2.ShowLines = true;
                _with2.Columns.AddRange(new LookUpColumnInfo[] {
                    new LookUpColumnInfo("ledger_code", "ledger_code", 25, FormatType.None, string.Empty, false, HorzAlignment.Far),
                    new LookUpColumnInfo("description", "description", 75, FormatType.None, string.Empty, true, HorzAlignment.Near)
                });
            }

            // Bind the data members to the items
            var _with3 = LookUpEdit1;
            _with3.DataBindings.Clear();
            _with3.DataBindings.Add("EditValue", drv, "dst_ledger_account");
            _with3.EditValueChanging += global::DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            var _with4 = CalcEdit1;
            _with4.DataBindings.Clear();
            _with4.DataBindings.Add("EditValue", drv, "credit_amt");

            var _with5 = TextEdit1;
            _with5.DataBindings.Clear();
            _with5.DataBindings.Add("EditValue", drv, "reference");

            var _with6 = TextEdit2;
            _with6.Text = string.Empty;
            if (drv["item_date"] != null && !object.ReferenceEquals(drv["item_date"], DBNull.Value))
            {
                _with6.Text = string.Format("{0:d}", Convert.ToDateTime(drv["item_date"]));
            }
        }

        /// <summary>
        /// Save any fields to the record before it is closed
        /// </summary>
        public override void SaveForm()
        {
            DataRowView drv = ((Form_Modal_NonAR)this.ParentForm).drv;
            drv["item_date"] = ItemDate;
        }

        /// <summary>
        /// Validate and update the date field when it is changed
        /// </summary>
        private void TextEdit2_Validating(object sender, CancelEventArgs e)
        {
            System.DateTime Value = DateTime.Now.Date;
            string DateText = Convert.ToString(TextEdit2.Text).Trim();

            // Try to parse the date as it is.
            if (!System.DateTime.TryParse(DateText, out Value))
            {
                switch (DateText.Length)
                {
                    case 3:
                        DateText = DateText.Substring(0, 1) + "/" + DateText.Substring(1, 2);
                        break;

                    case 4:
                        DateText = DateText.Substring(0, 2) + "/" + DateText.Substring(2, 2);
                        break;

                    case 5:
                        DateText = DateText.Substring(0, 1) + "/" + DateText.Substring(1, 2) + "/" + DateText.Substring(3, 2);
                        break;

                    case 6:
                        DateText = DateText.Substring(0, 2) + "/" + DateText.Substring(2, 2) + "/" + DateText.Substring(4, 2);
                        break;

                    case 7:
                        DateText = DateText.Substring(0, 1) + "/" + DateText.Substring(1, 2) + "/" + DateText.Substring(3, 4);
                        break;

                    case 8:
                        DateText = DateText.Substring(0, 2) + "/" + DateText.Substring(2, 2) + "/" + DateText.Substring(4, 4);
                        break;

                    default:
                        TextEdit2.Text = string.Empty;
                        return;
                }
                System.DateTime.TryParse(DateText, out Value);
            }

            // Update the date format and return its value
            TextEdit2.Text = string.Format("{0:d}", Value);
        }

        /// <summary>
        /// Process the change in the date field
        /// </summary>
        private void TextEdit2_EditValueChanged(object sender, EventArgs e)
        {
            OnChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Process the change in the amount field
        /// </summary>
        private void CalcEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string ErrorText = string.Empty;
            if (e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value) || Convert.ToDecimal(e.NewValue) <= 0m)
            {
                ErrorText = Globals.MisingText;
            }
            CalcEdit1.ErrorText = ErrorText;
        }

        /// <summary>
        /// Process the change in the ledger account field
        /// </summary>
        private void LookupEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string ErrorText = string.Empty;
            if (e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value))
            {
                ErrorText = Globals.MisingText;
            }
            LookUpEdit1.ErrorText = ErrorText;
        }

        /// <summary>
        /// Handle the loading of our control
        /// </summary>
        private void NonAR_Keypad_Load(object sender, EventArgs e)
        {
            LookUpEdit1.TabStop = false;
            CalcEdit1.TabStop = false;
            TextEdit1.TabStop = false;
            TextEdit2.TabStop = false;
        }

        /// <summary>
        /// Handle the change in the tab stop setting for our control
        /// </summary>

        private void NonAR_Keypad_TabStopChanged(object sender, EventArgs e)
        {
            // Enable or disable the tab stop processing as the control is switched
            LookUpEdit1.TabStop = TabStop;
            CalcEdit1.TabStop = TabStop;
            TextEdit1.TabStop = TabStop;
            TextEdit2.TabStop = TabStop;
        }
    }

    /// <summary>
    /// Override class to allow for the translation of CR to TAB
    /// </summary>
    internal partial class Keypad_TextEdit : TextEdit
    {
        public Keypad_TextEdit() : base()
        {
        }

        private Control FocusedControl = null;

        private bool FindFocusedControl(Control ctl)
        {
            bool answer = false;
            if (ctl.Focused)
            {
                FocusedControl = ctl;
                answer = true;
            }
            else
            {
                foreach (Control SubCtl in ctl.Controls)
                {
                    answer = FindFocusedControl(SubCtl);
                    if (answer)
                        break;
                }
            }
            return answer;
        }

        private bool GenerateTabEvent(bool Forward)
        {
            bool answer = false;

            // Find the top level form. It is the one without a parent.
            Control ParentForm = this.Parent;
            while (ParentForm.Parent != null)
            {
                ParentForm = ParentForm.Parent;
            }

            // Find the control with the focus. It should be us.
            FocusedControl = null;
            if (FindFocusedControl(ParentForm))
            {
                ParentForm.SelectNextControl(FocusedControl, Forward, true, true, true);
                answer = true;
            }
            return answer;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return && GenerateTabEvent(!e.Shift))
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            base.OnKeyDown(e);
        }
    }

    /// <summary>
    /// Override class to allow for the translation of CR to TAB
    /// </summary>
    internal partial class Keypad_CalcEdit : CalcEdit
    {
        public Keypad_CalcEdit() : base()
        {
        }

        private Control FocusedControl = null;

        private bool FindFocusedControl(Control ctl)
        {
            bool answer = false;
            if (ctl.Focused)
            {
                FocusedControl = ctl;
                answer = true;
            }
            else
            {
                foreach (Control SubCtl in ctl.Controls)
                {
                    answer = FindFocusedControl(SubCtl);
                    if (answer)
                        break;
                }
            }
            return answer;
        }

        private bool GenerateTabEvent(bool Forward)
        {
            bool answer = false;

            // Find the top level form. It is the one without a parent.
            Control ParentForm = this.Parent;
            while (ParentForm.Parent != null)
            {
                ParentForm = ParentForm.Parent;
            }

            // Find the control with the focus. It should be us.
            FocusedControl = null;
            if (FindFocusedControl(ParentForm))
            {
                ParentForm.SelectNextControl(FocusedControl, Forward, true, true, true);
                answer = true;
            }
            return answer;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return && GenerateTabEvent(!e.Shift))
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            base.OnKeyDown(e);
        }
    }

    /// <summary>
    /// Override class to allow for the translation of CR to TAB
    /// </summary>
    internal partial class Keypad_LookupEdit : LookUpEdit
    {
        public Keypad_LookupEdit() : base()
        {
        }

        private Control FocusedControl = null;

        private bool FindFocusedControl(Control ctl)
        {
            bool answer = false;
            if (ctl.Focused)
            {
                FocusedControl = ctl;
                answer = true;
            }
            else
            {
                foreach (Control SubCtl in ctl.Controls)
                {
                    answer = FindFocusedControl(SubCtl);
                    if (answer)
                        break;
                }
            }
            return answer;
        }

        private bool GenerateTabEvent(bool Forward)
        {
            bool answer = false;

            // Find the top level form. It is the one without a parent.
            Control ParentForm = this.Parent;
            while (ParentForm.Parent != null)
            {
                ParentForm = ParentForm.Parent;
            }

            // Find the control with the focus. It should be us.
            FocusedControl = null;
            if (FindFocusedControl(ParentForm))
            {
                ParentForm.SelectNextControl(FocusedControl, Forward, true, true, true);
                answer = true;
            }
            return answer;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Return && GenerateTabEvent(!e.Shift))
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
            base.OnKeyDown(e);
        }
    }
}