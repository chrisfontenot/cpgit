using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
	partial class Form_NonAR
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.NonAR_BatchList1 = new global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_BatchList();
			this.SimpleButton3 = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).BeginInit();
			this.PanelControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//NonAR_BatchTotals1
			//
			this.ToolTipController1.SetSuperTip(this.NonAR_BatchTotals1, null);
			//
			//NonAR_Source1
			//
			this.ToolTipController1.SetSuperTip(this.NonAR_Source1, null);
			//
			//SimpleButton1
			//
			this.SimpleButton1.Location = new System.Drawing.Point(386, 30);
			//
			//SimpleButton2
			//
			this.SimpleButton2.Location = new System.Drawing.Point(386, 100);
			this.SimpleButton2.Text = "&Close";
			//
			//PanelControl1
			//
			this.ToolTipController1.SetSuperTip(this.PanelControl1, null);
			//
			//NonAR_keypad1
			//
			this.ToolTipController1.SetSuperTip(this.NonAR_keypad1, null);
			//
			//NonAR_keyboard1
			//
			this.ToolTipController1.SetSuperTip(this.NonAR_keyboard1, null);
			this.NonAR_keyboard1.TabStop = true;
			//
			//NonAR_BatchList1
			//
			this.NonAR_BatchList1.Location = new System.Drawing.Point(386, 141);
			this.NonAR_BatchList1.Name = "NonAR_BatchList1";
			this.NonAR_BatchList1.Size = new System.Drawing.Size(75, 249);
			this.ToolTipController1.SetSuperTip(this.NonAR_BatchList1, null);
			this.NonAR_BatchList1.TabIndex = 9;
			//
			//SimpleButton3
			//
			this.SimpleButton3.Location = new System.Drawing.Point(386, 65);
			this.SimpleButton3.Name = "SimpleButton3";
			this.SimpleButton3.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton3.TabIndex = 10;
			this.SimpleButton3.TabStop = false;
			this.SimpleButton3.Text = "Apply";
			//
			//Form_NonAR
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.ClientSize = new System.Drawing.Size(473, 402);
			this.Controls.Add(this.NonAR_BatchList1);
			this.Controls.Add(this.SimpleButton3);
			this.Name = "Form_NonAR";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Controls.SetChildIndex(this.SimpleButton3, 0);
			this.Controls.SetChildIndex(this.SimpleButton1, 0);
			this.Controls.SetChildIndex(this.SimpleButton2, 0);
			this.Controls.SetChildIndex(this.NonAR_BatchList1, 0);
			this.Controls.SetChildIndex(this.NonAR_BatchTotals1, 0);
			this.Controls.SetChildIndex(this.NonAR_Source1, 0);
			this.Controls.SetChildIndex(this.PanelControl1, 0);
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).EndInit();
			this.PanelControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal global::DebtPlus.UI.Desktop.CS.NonAR.NonAR_BatchList NonAR_BatchList1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton3;
	}
}
