using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.NonAR
{
	partial class NonAR_Source
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.MemoEdit_note = new DevExpress.XtraEditors.MemoEdit();
			this.LookUpEdit_source = new DevExpress.XtraEditors.LookUpEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.MemoEdit_note.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_source.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			this.SuspendLayout();
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.MemoEdit_note);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_source);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(418, 150);
			this.LayoutControl1.TabIndex = 0;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//MemoEdit_note
			//
			this.MemoEdit_note.Location = new System.Drawing.Point(39, 38);
			this.MemoEdit_note.Name = "MemoEdit_note";
			this.MemoEdit_note.Properties.MaxLength = 256;
			this.MemoEdit_note.Size = new System.Drawing.Size(373, 106);
			this.MemoEdit_note.StyleController = this.LayoutControl1;
			this.MemoEdit_note.TabIndex = 5;
			//
			//LookUpEdit_source
			//
			this.LookUpEdit_source.Location = new System.Drawing.Point(39, 7);
			this.LookUpEdit_source.Name = "LookUpEdit_source";
			this.LookUpEdit_source.Properties.ActionButtonIndex = 1;
			this.LookUpEdit_source.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, true, false, DevExpress.Utils.HorzAlignment.Center, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Add a new item to the list",
				"add"),
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
			});
			this.LookUpEdit_source.Properties.NullText = "[Please choose an item here]";
			this.LookUpEdit_source.Size = new System.Drawing.Size(373, 20);
			this.LookUpEdit_source.StyleController = this.LayoutControl1;
			this.LookUpEdit_source.TabIndex = 4;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(418, 150);
			this.LayoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.LookUpEdit_source;
			this.LayoutControlItem1.CustomizationFormText = "Name";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(416, 31);
			this.LayoutControlItem1.Text = "Name";
			this.LayoutControlItem1.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(27, 20);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
			this.LayoutControlItem2.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LayoutControlItem2.Control = this.MemoEdit_note;
			this.LayoutControlItem2.CustomizationFormText = "Note";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 31);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(416, 117);
			this.LayoutControlItem2.Text = "Note";
			this.LayoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(27, 20);
			//
			//NonAR_Source
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.LayoutControl1);
			this.Name = "NonAR_Source";
			this.Size = new System.Drawing.Size(418, 150);
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.MemoEdit_note.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_source.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_source;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraEditors.MemoEdit MemoEdit_note;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
	}
}
