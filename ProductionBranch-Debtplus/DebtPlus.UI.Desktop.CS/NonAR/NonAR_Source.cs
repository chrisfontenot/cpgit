#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Desktop.CS.NonAR
{
    internal partial class NonAR_Source : SubForms
    {
        public NonAR_Source() : base()
        {
            InitializeComponent();
            LookUpEdit_source.ButtonPressed += LookUpEdit_source_ButtonPressed;
            LookUpEdit_source.EditValueChanged += LookUpEdit_source_EditValueChanged;
            LookUpEdit_source.EditValueChanging += LookUpEdit_source_EditValueChanging;
            MemoEdit_note.EditValueChanged += MemoEdit_note_EditValueChanged;
        }

        /// <summary>
        /// Is the SAVE mode valid now?
        /// </summary>
        public override bool SaveValid
        {
            get
            {
                return LookUpEdit_source.ErrorText == string.Empty && MemoEdit_note.ErrorText == string.Empty;
            }
        }

        /// <summary>
        /// Do any pre-processing and load any unbound fields
        /// </summary>
        public override void ReadForm()
        {
            DataRowView drv = ((Form_Modal_NonAR)ParentForm).drv;

            // Find the source items from the list of ar sources
            DataView source_view = (DataView)LookUpEdit_source.Properties.DataSource;

            if (source_view == null)
            {
                DataTable tbl = Globals.ds.Tables["ar_sources"];
                if (tbl == null)
                {
                    SqlCommand cmd = new SqlCommand();
                    var _with1 = cmd;
                    _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with1.CommandText = "select non_ar_source,description FROM non_ar_sources ORDER BY description";
                    _with1.CommandType = CommandType.Text;

                    Cursor current_cursor = Cursor.Current;
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(Globals.ds, "ar_sources");
                        tbl = Globals.ds.Tables["ar_sources"];
                    }
                    catch (SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading AR sources from database");
                    }
                    finally
                    {
                        Cursor.Current = current_cursor;
                    }
                }

                source_view = new DataView(tbl, string.Empty, "description", DataViewRowState.CurrentRows);
                var _with2 = LookUpEdit_source.Properties;
                _with2.DataSource = source_view;
                _with2.DisplayMember = "description";
                _with2.ValueMember = "non_ar_source";

                _with2.ShowFooter = false;
                _with2.ShowHeader = false;
                _with2.ShowLines = true;
                _with2.Columns.AddRange(new LookUpColumnInfo[] {
                    new LookUpColumnInfo("non_ar_source", "non_ar_source", 25, FormatType.None, string.Empty, false, HorzAlignment.Far),
                    new LookUpColumnInfo("description", "description", 75, FormatType.None, string.Empty, true, HorzAlignment.Near)
                });
            }

            // Bind the data members to the items
            var _with3 = LookUpEdit_source;
            _with3.DataBindings.Clear();
            _with3.DataBindings.Add("EditValue", drv, "non_ar_source");
            _with3.EditValueChanging += global::DebtPlus.Data.Validation.LookUpEdit_ActiveTest;

            var _with4 = MemoEdit_note;
            _with4.DataBindings.Clear();
            _with4.DataBindings.Add("EditValue", drv, "message");
        }

        /// <summary>
        /// Process the ADD button for the source
        /// </summary>
        private void LookUpEdit_source_ButtonPressed(object sender, ButtonPressedEventArgs e)
        {
            string ButtonID = Convert.ToString(e.Button.Tag);

            if (ButtonID == "add")
            {
                string SourceName = string.Empty;
                if (global::DebtPlus.Data.Forms.InputBox.Show("What name do you wish to add to the list of Non-AR sources", ref SourceName, "Non-AR Source", "", 50) == DialogResult.OK && SourceName.Length > 0)
                {
                    DataTable tbl = Globals.ds.Tables["ar_sources"];
                    DataRow[] rows = tbl.Select(string.Format("[description]='{0}'", SourceName.Replace("'", "''")));
                    Int32 NewItem = -1;
                    if (rows.Length > 0)
                        NewItem = Convert.ToInt32(rows[0]["item_key"]);

                    // If the current dataset does not have the new item, add it.

                    if (NewItem < 0)
                    {
                        // Update the table with the new row
                        SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        Cursor current_cursor = Cursor.Current;
                        try
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            cn.Open();
                            var _with5 = new SqlCommand();
                            _with5.Connection = cn;
                            _with5.CommandText = "INSERT INTO non_ar_sources(description) VALUES (@description); SELECT scope_identity() as non_ar_source;";
                            _with5.CommandType = CommandType.Text;
                            _with5.Parameters.Add("@description", SqlDbType.VarChar, 80).Value = SourceName;
                            object objAnswer = _with5.ExecuteScalar();
                            if (objAnswer != null && !object.ReferenceEquals(objAnswer, DBNull.Value))
                                NewItem = Convert.ToInt32(objAnswer);
                        }
                        catch (SqlException ex)
                        {
                            global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding Non-AR source");
                        }
                        finally
                        {
                            if (cn != null && cn.State != ConnectionState.Closed)
                                cn.Close();
                            Cursor.Current = current_cursor;
                        }

                        DataRow row = tbl.NewRow();
                        row.BeginEdit();
                        row["description"] = SourceName;
                        row["non_ar_source"] = NewItem;
                        row.EndEdit();
                        tbl.Rows.Add(row);
                        tbl.AcceptChanges();
                    }

                    // Set the view to the new list of items
                    DataView source_view = new DataView(tbl, string.Empty, "description", DataViewRowState.CurrentRows);
                    var _with6 = LookUpEdit_source;
                    var _with7 = _with6.Properties;
                    _with7.DataSource = source_view;

                    _with6.EditValue = NewItem;
                    _with6.Refresh();
                }
            }
        }

        /// <summary>
        /// When the source changes, tell our parent
        /// </summary>
        private void LookUpEdit_source_EditValueChanged(object sender, EventArgs e)
        {
            RaiseChanged(EventArgs.Empty);
        }

        /// <summary>
        /// Process a change in the source field
        /// </summary>
        private void LookUpEdit_source_EditValueChanging(object sender, ChangingEventArgs e)
        {
            string ErrorText = string.Empty;
            if (e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value))
            {
                ErrorText = Globals.MisingText;
            }
            LookUpEdit_source.ErrorText = ErrorText;
        }

        /// <summary>
        /// Process a change in the MEMO field
        /// </summary>
        private void MemoEdit_note_EditValueChanged(object sender, EventArgs e)
        {
            RaiseChanged(EventArgs.Empty);
        }
    }
}