using System.Collections;


namespace Reports.Print
{
    internal class ArgsParser : DebtPlus.Utils.ArgParserBase
    {
        private readonly ArrayList _params = new ArrayList();

        public ArrayList Params
        {
            get { return _params; }
        }

        public string Report { get; set; }

        private readonly System.Collections.Specialized.NameValueCollection _conf = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ReportsPrint);

        public System.Collections.Specialized.NameValueCollection Config
        {
            get { return _conf; }
        }

        public ArgsParser() : base(new string[] { "p" })
        {
        }

        protected override SwitchStatus OnSwitch(string switchName, string switchValue)
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            switch (switchName)
            {
                case "p":
                    string paramValue = switchValue.Trim();
                    if (paramValue != string.Empty)
                    {
                        string[] splitValues = paramValue.Split('=');
                        if (splitValues.GetUpperBound(0) == 1 && splitValues[0].Length > 0 && splitValues[1].Length > 0)
                        {
                            Params.Add(new Parameters(splitValues[0], splitValues[1]));
                            break; // TODO: might not be correct. Was : Exit Select
                        }
                    }
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;

                    break;

                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;

                    break;

                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }

            return ss;
        }

        protected override SwitchStatus OnNonSwitch(string switchValue)
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            if (Report != string.Empty)
            {
                AppendErrorLine("Only one report may be specified on the command line");
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }
            else
            {
                Report = switchValue;
            }

            return ss;
        }

        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine(String.Format("Usage: {0} [-o Filename] [-dDirectoryName ...] [-pNAME=VALUE ...] [-e] [report]", fname))
            //AppendErrorLine("       -o : Open the report file for editing")
            //AppendErrorLine("       -e : Edit the report definition")
            //AppendErrorLine("       -p : Specifiy a parameter for the report")
        }

        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            if (Report == string.Empty && ss != DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit)
            {
                AppendErrorLine("A report file must be specified on the command line or entered on the dialog.");
                ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
            }

            return ss;
        }
    }
}