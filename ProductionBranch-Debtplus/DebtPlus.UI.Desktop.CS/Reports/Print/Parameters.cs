namespace Reports.Print
{
    internal class Parameters
    {
        public string name;
        public string value;

        public Parameters(string name, string value)
        {
            this.name = name;
            this.value = value;
        }
    }
}