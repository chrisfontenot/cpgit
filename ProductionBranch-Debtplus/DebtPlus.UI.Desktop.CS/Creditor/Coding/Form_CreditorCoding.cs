#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Svc.Common;
using DebtPlus.UI.Creditor.Widgets.Controls;

namespace DebtPlus.UI.Desktop.CS.Creditor.Coding
{
    internal partial class Form_CreditorCoding : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Form_CreditorCoding() : base()
        {
            InitializeComponent();

            // Register the handlers for the first time if possible.
            if (!DebtPlus.Configuration.DesignMode.IsInDesignMode())
            {
                RegiserHandlers();
            }

            // Do not do these in the RegisterHandlers. They need to be statically set before the program starts.
            bt.DoWork += bt_DoWork;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
        }

        private void RegiserHandlers()
        {
            GridView1.KeyPress            += GridView1_KeyPress;
            Button_Update.Click           += Button_Update_Click;
            CheckedItem.EditValueChanging += CheckedItem_EditValueChanging;
            Load                          += Form_CreditorCoding_Load;
            CreditorID1.Validating        += CreditorID1_Validating;
            GridView1.Layout              += LayoutChanged;
            GridView1.DoubleClick         += GridView1_DoubleClick;

            if (tbl != null)
            {
                tbl.ColumnChanged += tbl_ColumnChanged;
            }
        }

        private void UnRegiserHandlers()
        {
            GridView1.KeyPress            -= GridView1_KeyPress;
            Button_Update.Click           -= Button_Update_Click;
            CheckedItem.EditValueChanging -= CheckedItem_EditValueChanging;
            Load                          -= Form_CreditorCoding_Load;
            CreditorID1.Validating        -= CreditorID1_Validating;
            GridView1.Layout              -= LayoutChanged;
            GridView1.DoubleClick         -= GridView1_DoubleClick;

            if (tbl != null)
            {
                tbl.ColumnChanged -= tbl_ColumnChanged;
            }
        }

        private const string STR_CreditorCoding = "Creditor.Coding";
        private readonly DataSet ds = new DataSet("ds");
        private DataTable tbl { get; set; }

        private System.ComponentModel.BackgroundWorker bt = new System.ComponentModel.BackgroundWorker();

        private DevExpress.Utils.WaitDialogForm dlg;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn_balance;
        private readonly ArgParser ap;

        public Form_CreditorCoding(ArgParser ap) : this()
        {
            this.ap = ap;
        }

        #region Windows Form Designer generated code

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.Repository.RepositoryItemTextEdit AccountNumberEdit;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_status;
        internal CreditorID CreditorID1;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_checked;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Client;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_held_in_trust;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_active_status;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_account;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_message;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_start_date;
        internal DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit CheckedItem;
        internal DevExpress.XtraEditors.Repository.RepositoryItemTextEdit MessageEdit;
        internal DevExpress.XtraEditors.CheckEdit EraseMessageField;
        internal DevExpress.XtraEditors.SimpleButton Button_Update;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_CreditorCoding));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_checked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CheckedItem = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.GridColumn_Client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_held_in_trust = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_active_status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_account = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AccountNumberEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.GridColumn_message = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MessageEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.GridColumn_start_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client_status = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EraseMessageField = new DevExpress.XtraEditors.CheckEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_Update = new DevExpress.XtraEditors.SimpleButton();
            this.CreditorID1 = new DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
            this.gridColumn_balance = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckedItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountNumberEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MessageEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EraseMessageField.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // DefaultLookAndFeel1
            // 
            this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
            // 
            // GridControl1
            // 
            this.GridControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridControl1.Location = new System.Drawing.Point(8, 40);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.CheckedItem,
            this.MessageEdit,
            this.AccountNumberEdit});
            this.GridControl1.Size = new System.Drawing.Size(705, 200);
            this.GridControl1.TabIndex = 3;
            this.GridControl1.ToolTipController = this.ToolTipController1;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.GridView1});
            // 
            // GridView1
            // 
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(133)))), ((int)(((byte)(195)))));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(109)))), ((int)(((byte)(189)))));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(59)))), ((int)(((byte)(139)))), ((int)(((byte)(206)))));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(216)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(139)))), ((int)(((byte)(201)))), ((int)(((byte)(254)))));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(170)))), ((int)(((byte)(225)))));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(246)))), ((int)(((byte)(255)))));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(155)))), ((int)(((byte)(215)))));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(104)))), ((int)(((byte)(184)))), ((int)(((byte)(251)))));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.GridColumn_checked,
            this.GridColumn_Client,
            this.GridColumn_held_in_trust,
            this.GridColumn_active_status,
            this.GridColumn_creditor,
            this.GridColumn_account,
            this.GridColumn_message,
            this.gridColumn_balance,
            this.GridColumn_start_date,
            this.GridColumn_client_status});
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsDetail.AllowZoomDetail = false;
            this.GridView1.OptionsDetail.EnableMasterViewMode = false;
            this.GridView1.OptionsDetail.ShowDetailTabs = false;
            this.GridView1.OptionsDetail.SmartDetailExpand = false;
            this.GridView1.OptionsSelection.InvertSelection = true;
            this.GridView1.OptionsSelection.MultiSelect = true;
            this.GridView1.OptionsSelection.UseIndicatorForSelection = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_Client, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // GridColumn_checked
            // 
            this.GridColumn_checked.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_checked.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_checked.ColumnEdit = this.CheckedItem;
            this.GridColumn_checked.CustomizationCaption = "Selected Item";
            this.GridColumn_checked.FieldName = "checked";
            this.GridColumn_checked.Name = "GridColumn_checked";
            this.GridColumn_checked.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_checked.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridColumn_checked.ToolTip = "Check this box if you wish to assign the creditor to this debt.";
            this.GridColumn_checked.Visible = true;
            this.GridColumn_checked.VisibleIndex = 0;
            this.GridColumn_checked.Width = 20;
            // 
            // CheckedItem
            // 
            this.CheckedItem.AutoHeight = false;
            this.CheckedItem.Name = "CheckedItem";
            // 
            // GridColumn_Client
            // 
            this.GridColumn_Client.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Client.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Client.Caption = "Client";
            this.GridColumn_Client.CustomizationCaption = "Client ID";
            this.GridColumn_Client.DisplayFormat.FormatString = "f0";
            this.GridColumn_Client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Client.FieldName = "client";
            this.GridColumn_Client.GroupFormat.FormatString = "f0";
            this.GridColumn_Client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Client.Name = "GridColumn_Client";
            this.GridColumn_Client.OptionsColumn.AllowEdit = false;
            this.GridColumn_Client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Client.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_Client.ToolTip = "Client ID associated with the debt";
            this.GridColumn_Client.Visible = true;
            this.GridColumn_Client.VisibleIndex = 1;
            this.GridColumn_Client.Width = 63;
            // 
            // GridColumn_held_in_trust
            // 
            this.GridColumn_held_in_trust.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_held_in_trust.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_held_in_trust.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_held_in_trust.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_held_in_trust.Caption = "Trust";
            this.GridColumn_held_in_trust.CustomizationCaption = "Amount in the trust";
            this.GridColumn_held_in_trust.DisplayFormat.FormatString = "c2";
            this.GridColumn_held_in_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_held_in_trust.FieldName = "held_in_trust";
            this.GridColumn_held_in_trust.GroupFormat.FormatString = "c2";
            this.GridColumn_held_in_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_held_in_trust.Name = "GridColumn_held_in_trust";
            this.GridColumn_held_in_trust.OptionsColumn.AllowEdit = false;
            this.GridColumn_held_in_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_held_in_trust.ToolTip = "Client\'s current trust balance";
            this.GridColumn_held_in_trust.Visible = true;
            this.GridColumn_held_in_trust.VisibleIndex = 2;
            this.GridColumn_held_in_trust.Width = 59;
            // 
            // GridColumn_active_status
            // 
            this.GridColumn_active_status.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_active_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_active_status.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_active_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_active_status.Caption = "Status";
            this.GridColumn_active_status.CustomizationCaption = "Client Active Status";
            this.GridColumn_active_status.FieldName = "active_status";
            this.GridColumn_active_status.Name = "GridColumn_active_status";
            this.GridColumn_active_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_active_status.ToolTip = "Client\'s active status";
            this.GridColumn_active_status.Visible = true;
            this.GridColumn_active_status.VisibleIndex = 3;
            this.GridColumn_active_status.Width = 54;
            // 
            // GridColumn_creditor
            // 
            this.GridColumn_creditor.Caption = "Creditor";
            this.GridColumn_creditor.CustomizationCaption = "Creditor";
            this.GridColumn_creditor.FieldName = "creditor_name";
            this.GridColumn_creditor.Name = "GridColumn_creditor";
            this.GridColumn_creditor.OptionsColumn.AllowEdit = false;
            this.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor.ToolTip = "The name assigned by the counselor when the debt was created.";
            this.GridColumn_creditor.Visible = true;
            this.GridColumn_creditor.VisibleIndex = 4;
            this.GridColumn_creditor.Width = 58;
            // 
            // GridColumn_account
            // 
            this.GridColumn_account.Caption = "Account#";
            this.GridColumn_account.ColumnEdit = this.AccountNumberEdit;
            this.GridColumn_account.CustomizationCaption = "Account Number";
            this.GridColumn_account.FieldName = "account_number";
            this.GridColumn_account.Name = "GridColumn_account";
            this.GridColumn_account.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_account.ToolTip = "Account number for the client\'s debt";
            this.GridColumn_account.Visible = true;
            this.GridColumn_account.VisibleIndex = 5;
            this.GridColumn_account.Width = 83;
            // 
            // AccountNumberEdit
            // 
            this.AccountNumberEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.AccountNumberEdit.AutoHeight = false;
            this.AccountNumberEdit.MaxLength = 22;
            this.AccountNumberEdit.Name = "AccountNumberEdit";
            // 
            // GridColumn_message
            // 
            this.GridColumn_message.Caption = "Message";
            this.GridColumn_message.ColumnEdit = this.MessageEdit;
            this.GridColumn_message.CustomizationCaption = "Message on Account";
            this.GridColumn_message.FieldName = "message";
            this.GridColumn_message.Name = "GridColumn_message";
            this.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_message.ToolTip = "Message field. Type a new value here if you wish to change this item.";
            this.GridColumn_message.Visible = true;
            this.GridColumn_message.VisibleIndex = 6;
            this.GridColumn_message.Width = 123;
            // 
            // MessageEdit
            // 
            this.MessageEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.MessageEdit.AutoHeight = false;
            this.MessageEdit.MaxLength = 50;
            this.MessageEdit.Name = "MessageEdit";
            // 
            // GridColumn_start_date
            // 
            this.GridColumn_start_date.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_start_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_start_date.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_start_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_start_date.Caption = "Start Date";
            this.GridColumn_start_date.CustomizationCaption = "Start Date";
            this.GridColumn_start_date.DisplayFormat.FormatString = "d";
            this.GridColumn_start_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_start_date.FieldName = "start_date";
            this.GridColumn_start_date.GroupFormat.FormatString = "d";
            this.GridColumn_start_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_start_date.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.DateMonth;
            this.GridColumn_start_date.Name = "GridColumn_start_date";
            this.GridColumn_start_date.OptionsColumn.AllowEdit = false;
            this.GridColumn_start_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_start_date.ToolTip = "Start date for the client.";
            this.GridColumn_start_date.Visible = true;
            this.GridColumn_start_date.VisibleIndex = 7;
            this.GridColumn_start_date.Width = 64;
            // 
            // GridColumn_client_status
            // 
            this.GridColumn_client_status.Caption = "Client Status";
            this.GridColumn_client_status.CustomizationCaption = "Client status";
            this.GridColumn_client_status.FieldName = "client_status";
            this.GridColumn_client_status.Name = "GridColumn_client_status";
            this.GridColumn_client_status.OptionsColumn.AllowEdit = false;
            this.GridColumn_client_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_client_status.ToolTip = "Client status from the Client form";
            // 
            // EraseMessageField
            // 
            this.EraseMessageField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.EraseMessageField.Location = new System.Drawing.Point(8, 248);
            this.EraseMessageField.Name = "EraseMessageField";
            this.EraseMessageField.Properties.Caption = "Clear the message once the debt has been updated.";
            this.EraseMessageField.Size = new System.Drawing.Size(280, 19);
            this.EraseMessageField.TabIndex = 4;
            this.EraseMessageField.ToolTip = "SHould the message field be cleared when the debts are assigned?";
            this.EraseMessageField.ToolTipController = this.ToolTipController1;
            // 
            // LabelControl1
            // 
            this.LabelControl1.Location = new System.Drawing.Point(16, 11);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(43, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Creditor:";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            // 
            // Button_Update
            // 
            this.Button_Update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Update.Enabled = false;
            this.Button_Update.Location = new System.Drawing.Point(641, 8);
            this.Button_Update.Name = "Button_Update";
            this.Button_Update.Size = new System.Drawing.Size(75, 23);
            this.Button_Update.TabIndex = 2;
            this.Button_Update.Text = "Update";
            this.Button_Update.ToolTip = "Click here to assign the creditor to the checked debts.";
            this.Button_Update.ToolTipController = this.ToolTipController1;
            // 
            // CreditorID1
            // 
            this.CreditorID1.AllowDrop = true;
            this.CreditorID1.EditValue = null;
            this.CreditorID1.Location = new System.Drawing.Point(65, 8);
            this.CreditorID1.Name = "CreditorID1";
            this.CreditorID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("CreditorID1.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Click here to search for a creditor ID", "search", null, true)});
            this.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.CreditorID1.Properties.Mask.BeepOnError = true;
            this.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditorID1.Properties.MaxLength = 10;
            this.CreditorID1.Size = new System.Drawing.Size(100, 20);
            this.CreditorID1.TabIndex = 1;
            // 
            // gridColumn_balance
            // 
            this.gridColumn_balance.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_balance.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.gridColumn_balance.Caption = "Balance";
            this.gridColumn_balance.DisplayFormat.FormatString = "{0:c}";
            this.gridColumn_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_balance.FieldName = "balance";
            this.gridColumn_balance.GroupFormat.FormatString = "{0:c}";
            this.gridColumn_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn_balance.Name = "gridColumn_balance";
            this.gridColumn_balance.OptionsColumn.AllowEdit = false;
            this.gridColumn_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.gridColumn_balance.Visible = true;
            this.gridColumn_balance.VisibleIndex = 8;
            // 
            // Form_CreditorCoding
            // 
            this.AcceptButton = this.Button_Update;
            this.ClientSize = new System.Drawing.Size(721, 278);
            this.Controls.Add(this.CreditorID1);
            this.Controls.Add(this.Button_Update);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.EraseMessageField);
            this.Controls.Add(this.GridControl1);
            this.Name = "Form_CreditorCoding";
            this.Text = "Code the debt with the creditor ID";
            ((System.ComponentModel.ISupportInitialize)(this.DxErrorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckedItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountNumberEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MessageEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EraseMessageField.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditorID1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// General mainline to start our process
        /// </summary>
        private void Form_CreditorCoding_Load(object sender, EventArgs e)
        {
            UnRegiserHandlers();
            try
            {
                // Create a validation routine for this creditor
                ValidateRoutine = new AccountNumberValidation();

                LoadPlacement(STR_CreditorCoding);

                // Tell the user that this will take some time and don't worry
                dlg = new DevExpress.Utils.WaitDialogForm("Loading list of un-coded debts");
                dlg.Show();

                bt.WorkerSupportsCancellation = true;
                bt.WorkerReportsProgress = false;
                bt.RunWorkerAsync();

                string Filename = System.IO.Path.Combine(XMLDirectoryName(), GridView1.Name + "_2.xml");
                if (System.IO.File.Exists(Filename))
                {
                    GridView1.RestoreLayoutFromXml(Filename);
                }
            }
#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168
            finally
            {
                RegiserHandlers();
            }
        }

        /// <summary>
        /// Filename to the disk storage for this program
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private static string XMLDirectoryName()
        {
            string answer = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), string.Format("DebtPlus{0}{1}", System.IO.Path.DirectorySeparatorChar, STR_CreditorCoding)) + System.IO.Path.DirectorySeparatorChar;
            return answer;
        }

        /// <summary>
        /// Save the layout when it is changed to the disk
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            UnRegiserHandlers();
            try
            {
                string PathName = XMLDirectoryName();
                string Filename = System.IO.Path.Combine(PathName, ((DevExpress.XtraGrid.Views.Base.BaseView)Sender).Name + "_2.xml");

                if (!System.IO.Directory.Exists(PathName))
                {
                    System.IO.Directory.CreateDirectory(PathName);
                }
                GridView1.SaveLayoutToXml(Filename);
            }
#pragma warning disable 168
            catch (System.IO.FileLoadException ex) { }
            catch (System.IO.DirectoryNotFoundException ex) { }
#pragma warning restore 168
            finally
            {
                RegiserHandlers();
            }
        }

        /// <summary>
        /// Do the work to read the debt list in a separate thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Clear the tables in the dataset
            ds.Clear();

            // Ask the system for the list of un-coded debts
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();

            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            cmd.CommandText = "xpr_debt_coding";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;

            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
            try
            {
                da.Fill(ds, "uncoded_debts");
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
        }

        /// <summary>
        /// Back at the main thread -- process the completion logic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            UnRegiserHandlers();
            try
            {
                tbl = ds.Tables["uncoded_debts"];
                if (tbl == null)
                {
                    return;
                }

                // Remove any column called "checked" from the table
                if (tbl.Columns.Contains("checked"))
                {
                    tbl.Columns.Remove(tbl.Columns["checked"]);
                }

                // Add the checked column to the table so that the table has a column called "checked"
                // It is normally not there. It is the status of our checkbox. For some reason, it seems to
                // come and go.
                DataColumn col   = new DataColumn("checked", typeof(bool));
                col.AllowDBNull  = false;
                col.Caption      = "Checked";
                col.DefaultValue = false;
                col.ReadOnly     = false;
                tbl.Columns.Add(col);

                // Bind the data to the grid
                GridControl1.DataSource = tbl.DefaultView;
                GridControl1.RefreshDataSource();
            }
            finally
            {
                // Dispose of the waiting dialog
                if (dlg != null)
                {
                    dlg.Close();
                    dlg.Dispose();
                    dlg = null;
                }

                RegiserHandlers();
            }
        }

        /// <summary>
        /// Return the current creditor ID value
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        private string creditor
        {
            get
            {
                if (CreditorID1.EditValue == null)
                {
                    return string.Empty;
                }
                return Convert.ToString(CreditorID1.EditValue);
            }
        }

        /// <summary>
        /// Validate the Creditor ID
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        private void CreditorID1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string ErrorMessage = string.Empty;
            string read_creditor = string.Empty;
            bool read_prohibit_use = true;

            if (creditor == string.Empty)
            {
                ErrorMessage = "This Field Is Required";
            }
            else
            {
                try
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "SELECT creditor, prohibit_use FROM creditors WHERE creditor = @creditor";
                            cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;
                            using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow))
                            {
                                if (rd.Read())
                                {
                                    if (!rd.IsDBNull(0))
                                    {
                                        read_creditor = rd.GetString(0);
                                    }
                                    if (!rd.IsDBNull(1))
                                    {
                                        read_prohibit_use = Convert.ToInt32(rd.GetValue(1)) != 0;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }

                // Do not allow the user to leave the field if the valid is invalid
                if (read_creditor == string.Empty)
                {
                    ErrorMessage = "Invalid Creditor";
                }
                else if (read_prohibit_use)
                {
                    ErrorMessage = "Marked Prohibit Use";
                }
            }

            // Set the error message text but don't cancel the update or you can't leave the field to close the form.
            CreditorID1.ErrorText = ErrorMessage;
            Button_Update.Enabled = (CreditorID1.ErrorText == string.Empty) && (creditor != string.Empty) && (CheckedItems > 0);
        }

        /// <summary>
        /// Process a checked status change in the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void CheckedItem_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            Button_Update.Enabled = (CreditorID1.ErrorText == string.Empty) && (creditor != string.Empty) && (Convert.ToBoolean(e.NewValue) || (CheckedItems > 1));
        }

        /// <summary>
        /// Return the number of checked debts in the list
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        private Int32 CheckedItems
        {
            get
            {
                object obj = tbl.Compute("count(checked)", "[checked]<>0");
                if (obj == null || object.ReferenceEquals(obj, DBNull.Value))
                {
                    obj = 0;
                }
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// Handle a change in the debt message column
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void tbl_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            Int32 client_creditor = default(Int32);
            if (e.Row["client_creditor"] != null && !object.ReferenceEquals(e.Row["client_creditor"], DBNull.Value))
            {
                client_creditor = Convert.ToInt32(e.Row["client_creditor"]);
            }
            else
            {
                client_creditor = -1;
            }

            // Look for a change to the message field
            if (client_creditor >= 0)
            {
                switch (e.Column.ColumnName.ToLower())
                {
                    case "message":
                        // Obtain the new message value from the change. Change an empty message to be a NULL value.
                        string message = DebtPlus.Utils.Nulls.v_String(e.ProposedValue);
                        if (string.IsNullOrWhiteSpace(message))
                        {
                            message = null;
                        }

                        // There must be a debt record to continue
                        using (var cm = new DebtPlus.UI.Common.CursorManager())
                        {
                            try
                            {
                                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                                {
                                    cn.Open();
                                    using (var cmd = new SqlCommand())
                                    {
                                        cmd.Connection = cn;
                                        cmd.CommandText = "UPDATE update_client_creditor SET message=@message WHERE client_creditor=@client_creditor";
                                        cmd.Parameters.Add("@message", SqlDbType.VarChar, 50).Value = DebtPlus.Utils.Nulls.ToDbNull(message);
                                        cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = client_creditor;
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                            catch (System.Data.SqlClient.SqlException ex)
                            {
                                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                            }
                        }
                        break;

                    case "account_number":
                        // Obtain the new message value from the change. Change an empty message to be a NULL value.
                        string account_number = DebtPlus.Utils.Nulls.v_String(e.ProposedValue) ?? string.Empty;

                        using (var cm = new DebtPlus.UI.Common.CursorManager())
                        {
                            try
                            {
                                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                                {
                                    cn.Open();
                                    using (var cmd = new SqlCommand())
                                    {
                                        cmd.Connection = cn;
                                        cmd.CommandText = "UPDATE update_client_creditor SET account_number=@account_number WHERE client_creditor=@client_creditor";
                                        cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 22).Value = DebtPlus.Utils.Nulls.ToDbNull(account_number);
                                        cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = client_creditor;
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                            catch (System.Data.SqlClient.SqlException ex)
                            {
                                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// When the update button is clicked, update the debts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        private void Button_Update_Click(object sender, EventArgs e)
        {
            // Set the waiting cursor
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            Int32 LastFocusedRow = GridView1.FocusedRowHandle;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                using (DataView vue = new DataView(tbl, "[checked]<>0", string.Empty, DataViewRowState.CurrentRows))
                {
                    foreach (DataRowView drv in vue)
                    {
                        UpdateDebt(drv, EraseMessageField.Checked);
                    }
                }

                // If all is good then clear the creditor ID
                CreditorID1.EditValue = null;

                // Try to position the input the creditor field
                CreditorID1.Focus();
            }
            finally
            {
                tbl.AcceptChanges();
                GridView1.FocusedRowHandle = LastFocusedRow;
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        private AccountNumberValidation ValidateRoutine;

        /// <summary>
        /// Process the debt update
        /// </summary>
        /// <param name="drv"></param>
        /// <param name="EraseMessage"></param>
        /// <remarks></remarks>
        private void UpdateDebt(DataRowView drv, bool EraseMessage)
        {
            Int32 clientCreditor = DebtPlus.Utils.Nulls.v_Int32(drv["client_creditor"]).GetValueOrDefault(-1);
            if (clientCreditor <= 0)
            {
                return;
            }

            // Try to update the debt
            string AccountNumber = DebtPlus.Utils.Nulls.v_String(drv["account_number"]) ?? string.Empty;
            while (true)
            {
                // Validate the account number against the list of valid creditor masks
                if (!ValidateRoutine.IsValidAccount(creditor, AccountNumber))
                {
                    string NewAccountNumber = string.Empty;
                    if (DebtPlus.Data.Forms.InputBox.Show(string.Format("The account number is invalid. Please enter a new account number\r\nfor the debt associated with account '{0}' or press CANCEL\r\nto not update this debt.", AccountNumber), ref NewAccountNumber, "Invalid Account Number", AccountNumber) != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }

                    AccountNumber = NewAccountNumber;
                    continue;
                }
                AccountNumber = ValidateRoutine.ValidatedAccountNumber;

                switch (DoUpdate(clientCreditor, EraseMessage, AccountNumber))
                {
                    case 0:
                        drv.Delete();   // The account was changed. Remove it from the list.
                        return;

                    case 1:
                        string NewAccountNumber = string.Empty;
                        if (DebtPlus.Data.Forms.InputBox.Show(string.Format("The account number is invalid. Please enter a new account number\r\nfor the debt associated with account '{0}' or press CANCEL\r\nto not update this debt.", AccountNumber), ref NewAccountNumber, "Invalid Account Number", AccountNumber) != System.Windows.Forms.DialogResult.OK)
                        {
                            return;
                        }
                        AccountNumber = NewAccountNumber;
                        break;

                    default:
                        return;  // Some other error occurred. Leave the trustRegister alone.
                }
            }
        }

        /// <summary>
        /// Perform an update on the debt with the account number
        /// </summary>
        /// <param name="ClientCreditor"></param>
        /// <param name="EraseMessage"></param>
        /// <param name="AccountNumber"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private Int32 DoUpdate(Int32 ClientCreditor, bool EraseMessage, string AccountNumber)
        {
            // Missing account numbers are always invalid.
            if (AccountNumber.Length == 0)
            {
                return 1;
            }

            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();

                    //Correct the debt on the database and allow for a bad account number
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_debt_creditor";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = ClientCreditor;
                        cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;
                        cmd.Parameters.Add("@clear_msg", SqlDbType.Bit).Value = EraseMessage;
                        cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 50).Value = AccountNumber;
                        cmd.ExecuteNonQuery();
                        return 0;
                    }
                }
            }

            catch (System.Data.SqlClient.SqlException ex)
            {
                if (ex.Message.Contains("account number is not valid"))
                {
                    return 1;
                }

                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }

            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }

            return 2;
        }

        /// <summary>
        /// Process a keypress event on the grid to select the rows
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void GridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == ' ')
            {
                Int32[] RowHandles = GridView1.GetSelectedRows();

                // Determine if a selection is desired for the rows.
                // If none are selected then we want to select the items.
                // Otherwise, deselect them.
                bool ShouldSelectRow = true;
                foreach (Int32 RowHandle in RowHandles)
                {
                    DataRow row = GridView1.GetDataRow(RowHandle);
                    if (Convert.ToBoolean(row["checked"]))
                    {
                        ShouldSelectRow = false;
                        break;
                    }
                }

                // Select or deselect the rows.
                foreach (Int32 rowhandle in RowHandles)
                {
                    DataRow row = GridView1.GetDataRow(rowhandle);
                    row["checked"] = ShouldSelectRow;
                }
                GridView1.ClearSelection();
            }

            // If the new status is to select the trustRegister then enable the update button
            Button_Update.Enabled = CheckedItems > 1;
        }

        /// <summary>
        /// Handle a double-click event on the grid control
        /// </summary>
        private void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 RowHandle = hi.RowHandle;

            // Find the record to be edited from the list
            if (RowHandle >= 0)
            {
                DataRowView drv = GridView1.GetRow(RowHandle) as DataRowView;
                if (drv == null)
                {
                    return;
                }

                // If we are in a cell then we can look for the cell
                if (hi.InRowCell && !hi.InColumnPanel)
                {
                    string fieldName = hi.Column.FieldName ?? string.Empty;
                    if (fieldName == "client")
                    {

                        // Find the client from the dataset
                        Int32? clientID = DebtPlus.Utils.Nulls.v_Int32(drv["client"]);
                        if (!clientID.HasValue)
                        {
                            return;
                        }

                        // Display the client information
                        ShowClient(clientID.Value);
                    }
                }
            }
        }

        /// <summary>
        /// Display the indicated client to the user
        /// </summary>
        /// <param name="ClientID"></param>
        private void ShowClient(Int32 ClientID)
        {
            // Start a thread to show the client
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(ShowClientThread));
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Name = "ClientUpdate";
            thrd.Start(ClientID);
        }

        /// <summary>
        /// Display the client to the user
        /// </summary>
        private void ShowClientThread(object obj)
        {
            // Display the alert notes first
            ShowAlertNotesThread(obj);

            Int32 ClientID = (Int32)obj;
            if (ClientID > 0)
            {
                using (var cuc = new DebtPlus.UI.Client.Service.ClientUpdateClass())
                {
                    cuc.ShowEditDialog(ClientID, false);
                }
            }
        }

        /// <summary>
        /// Display the alert notes to the user
        /// </summary>
        private void ShowAlertNotesThread(object obj)
        {
            Int32 ClientID = (Int32)obj;
            if (ClientID >= 0)
            {
                using (var notes = new DebtPlus.Notes.AlertNotes.Client())
                {
                    notes.ShowAlerts(ClientID);
                }
            }
        }
    }
}