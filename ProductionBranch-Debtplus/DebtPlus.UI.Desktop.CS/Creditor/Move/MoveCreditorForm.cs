#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Creditor.Widgets.Controls;

namespace DebtPlus.UI.Desktop.CS.Creditor.Move
{
    internal partial class MoveCreditorForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public MoveCreditorForm() : base()
        {
            InitializeComponent();

            Load += MoveCreditorForm_Load;
            Closing += MoveCreditorForm_Closing;
            CreditorControl1.CreditorChanged += CreditorControl1_CreditorChanged;
            CreditorControl2.CreditorChanged += CreditorControl1_CreditorChanged;

            //Add any initialization after the InitializeComponent() call
            CreditorControl1.Text = "Source (old) Creditor ID";
            CreditorControl2.Text = "Target (new) Creditor ID";
        }

        private ArgParser ap = null;

        public MoveCreditorForm(ArgParser ap) : this()
        {
            this.ap = ap;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if ((components != null))
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal global::DebtPlus.UI.Desktop.CS.Creditor.Move.CreditorControl CreditorControl1;
        internal global::DebtPlus.UI.Desktop.CS.Creditor.Move.CreditorControl CreditorControl2;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.CheckEdit chkDelete;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MoveCreditorForm));
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.chkDelete = new DevExpress.XtraEditors.CheckEdit();
            this.CreditorControl2 = new global::DebtPlus.UI.Desktop.CS.Creditor.Move.CreditorControl();
            this.CreditorControl1 = new global::DebtPlus.UI.Desktop.CS.Creditor.Move.CreditorControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();

            ((System.ComponentModel.ISupportInitialize)this.chkDelete.Properties).BeginInit();
            this.SuspendLayout();

            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.LabelControl1.Location = new System.Drawing.Point(0, 0);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Padding = new System.Windows.Forms.Padding(5);
            this.LabelControl1.Size = new System.Drawing.Size(512, 50);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = resources.GetString("LabelControl1.Text");
            this.LabelControl1.UseMnemonic = false;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(424, 72);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 4;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(424, 104);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "&Cancel";
            //
            //chkDelete
            //
            this.chkDelete.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.chkDelete.Location = new System.Drawing.Point(8, 336);
            this.chkDelete.Name = "chkDelete";
            this.chkDelete.Properties.Caption = "Also permanently delete the source creditor from the system.";
            this.chkDelete.Size = new System.Drawing.Size(392, 19);
            this.chkDelete.TabIndex = 3;
            //
            //CreditorControl2
            //
            this.CreditorControl2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.CreditorControl2.Location = new System.Drawing.Point(8, 200);
            this.CreditorControl2.Name = "CreditorControl2";
            this.CreditorControl2.Size = new System.Drawing.Size(408, 128);
            this.ToolTipController1.SetSuperTip(this.CreditorControl2, null);
            this.CreditorControl2.TabIndex = 2;
            this.CreditorControl2.ValidationLevel = CreditorID.ValidationLevelEnum.NotProhibitUse;
            //
            //CreditorControl1
            //
            this.CreditorControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.CreditorControl1.Location = new System.Drawing.Point(8, 64);
            this.CreditorControl1.Name = "CreditorControl1";
            this.CreditorControl1.Size = new System.Drawing.Size(408, 128);
            this.ToolTipController1.SetSuperTip(this.CreditorControl1, null);
            this.CreditorControl1.TabIndex = 1;
            this.CreditorControl1.ValidationLevel = CreditorID.ValidationLevelEnum.Exists;
            //
            //MoveCreditorForm
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(512, 366);
            this.Controls.Add(this.CreditorControl2);
            this.Controls.Add(this.CreditorControl1);
            this.Controls.Add(this.chkDelete);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl1);
            this.Name = "MoveCreditorForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Move Creditor Information to an NEW Creditor";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();

            ((System.ComponentModel.ISupportInitialize)this.chkDelete.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void MoveCreditorForm_Load(System.Object sender, System.EventArgs e)
        {
            // Define the current values from the argument parser
            chkDelete.Checked = ap.DeleteCreditor;
            CreditorControl1.CreditorID1.EditValue = ap.SourceCreditor;
            CreditorControl2.CreditorID1.EditValue = ap.DestinationCreditor;

            // Enable or disable the OK button based upon the creditor information
            EnableOK();
        }

        private void CreditorControl1_CreditorChanged(object sender, System.EventArgs e)
        {
            EnableOK();
        }

        private void EnableOK()
        {
            if (CreditorControl1.CreditorID1.EditValue == string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            if (CreditorControl1.CreditorID1.ErrorText != string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            if (CreditorControl2.CreditorID1.EditValue == string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            if (CreditorControl2.CreditorID1.ErrorText == "The creditor IDs may not be the same")
            {
                CreditorControl2.CreditorID1.ErrorText = string.Empty;
            }

            if (string.Compare(CreditorControl1.CreditorID1.EditValue, CreditorControl2.CreditorID1.EditValue, true) == 0)
            {
                CreditorControl2.CreditorID1.ErrorText = "The creditor IDs may not be the same";
            }

            if (CreditorControl2.CreditorID1.ErrorText != string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            Button_OK.Enabled = true;
        }

        private void MoveCreditorForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ap.DeleteCreditor = chkDelete.Checked;
            ap.SourceCreditor = CreditorControl1.CreditorID1.EditValue;
            ap.DestinationCreditor = CreditorControl2.CreditorID1.EditValue;
        }
    }
}