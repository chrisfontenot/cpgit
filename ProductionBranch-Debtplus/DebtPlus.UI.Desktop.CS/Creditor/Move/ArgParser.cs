using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Move
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Delete the source creditor when complete
        /// </summary>
        private bool _DeleteCreditor = false;

        public bool DeleteCreditor
        {
            get { return _DeleteCreditor; }
            set { _DeleteCreditor = value; }
        }

        /// <summary>
        /// Source (old) creditor
        /// </summary>
        private string _SourceCreditor = string.Empty;

        public string SourceCreditor
        {
            get { return _SourceCreditor; }
            set { _SourceCreditor = value; }
        }

        /// <summary>
        /// New (Target) creditor
        /// </summary>
        private string _DestinationCreditor = string.Empty;

        public string DestinationCreditor
        {
            get { return _DestinationCreditor; }
            set { _DestinationCreditor = value; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {
            "f",
            "t",
            "d"
        })
        {
        }

        /// <summary>
        /// Process a switch condition
        /// </summary>
        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            global::DebtPlus.Utils.ArgParserBase.SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            switch (switchSymbol)
            {
                case "f":
                    SourceCreditor = switchValue;
                    break;

                case "t":
                    DestinationCreditor = switchValue;
                    break;

                case "d":
                    DeleteCreditor = true;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }

            return ss;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override global::DebtPlus.Utils.ArgParserBase.SwitchStatus OnDoneParse()
        {
            global::DebtPlus.Utils.ArgParserBase.SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            if (SourceCreditor == string.Empty || DestinationCreditor == string.Empty)
            {
                var _with1 = new MoveCreditorForm(this);
                if (_with1.ShowDialog() != DialogResult.OK)
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                _with1.Dispose();
            }

            return ss;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}