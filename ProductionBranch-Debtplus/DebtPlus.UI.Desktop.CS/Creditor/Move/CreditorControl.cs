using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Creditor.Widgets.Controls;
using System.Data.SqlClient;
using DebtPlus.Interfaces;

namespace DebtPlus.UI.Desktop.CS.Creditor.Move
{
    internal partial class CreditorControl : DevExpress.XtraEditors.XtraUserControl
    {
        public CreditorControl() : base()
        {
            InitializeComponent();
            CreditorID1.Validated += CreditorID1_Validated;
#if false
            this.Load += CreditorControl_Load;
#endif
        }

        public event System.EventHandler CreditorChanged;

        public CreditorID.ValidationLevelEnum ValidationLevel
        {
            get { return CreditorID1.ValidationLevel; }
            set { CreditorID1.ValidationLevel = value; }
        }

        public override string Text
        {
            get { return GroupControl1.Text; }
            set { GroupControl1.Text = value; }
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if ((components != null))
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        public DevExpress.XtraEditors.GroupControl GroupControl1;

        public CreditorID CreditorID1;
        private DevExpress.XtraEditors.LabelControl LabelControl2;
        private DevExpress.XtraEditors.LabelControl LabelControl1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditorControl));
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.CreditorID1 = new CreditorID();
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            this.SuspendLayout();
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 32);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(53, 13);
            this.LabelControl2.TabIndex = 0;
            this.LabelControl2.Text = "Creditor ID";
            //
            //CreditorID1
            //
            this.CreditorID1.AllowDrop = true;
            this.CreditorID1.EditValue = null;
            this.CreditorID1.Location = new System.Drawing.Point(72, 28);
            this.CreditorID1.Name = "CreditorID1";
            this.CreditorID1.Properties.Appearance.Options.UseTextOptions = true;
            this.CreditorID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CreditorID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.Utils.HorzAlignment.Center, (System.Drawing.Image)resources.GetObject("CreditorID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), "Click here to search for a creditor ID",
            "search") });
            this.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.CreditorID1.Properties.Mask.BeepOnError = true;
            this.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditorID1.Properties.MaxLength = 10;
            this.CreditorID1.Size = new System.Drawing.Size(100, 20);
            this.CreditorID1.TabIndex = 1;
            this.CreditorID1.ValidationLevel = CreditorID.ValidationLevelEnum.Exists;
            //
            //GroupControl1
            //
            this.GroupControl1.Controls.Add(this.LabelControl1);
            this.GroupControl1.Controls.Add(this.LabelControl2);
            this.GroupControl1.Controls.Add(this.CreditorID1);
            this.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GroupControl1.Location = new System.Drawing.Point(0, 0);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(480, 150);
            this.GroupControl1.TabIndex = 0;
            this.GroupControl1.Text = "GroupControl1";
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(72, 56);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(403, 89);
            this.LabelControl1.TabIndex = 2;
            this.LabelControl1.UseMnemonic = false;
            //
            //CreditorControl
            //
            this.Controls.Add(this.GroupControl1);
            this.Name = "CreditorControl";
            this.Size = new System.Drawing.Size(480, 150);
            ((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            this.GroupControl1.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void CreditorID1_Validated(object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlDataReader rd = null;
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // Do not attempt to lookup empty creditor names
            if (string.IsNullOrEmpty(CreditorID1.EditValue))
                return;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                cn.Open();
                var _with1 = new SqlCommand();
                _with1.Connection = cn;
                _with1.CommandText = "xpr_creditor_address_merged";
                _with1.CommandType = CommandType.StoredProcedure;
                _with1.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = CreditorID1.EditValue;
                _with1.Parameters.Add("@type", SqlDbType.VarChar, 1).Value = "P";
                rd = _with1.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);

                if (rd != null && rd.Read())
                {
                    foreach (string strName in new string[] {
                        "addr1",
                        "addr2",
                        "addr3",
                        "addr4",
                        "addr5",
                        "addr6",
                        "addr7"
                    })
                    {
                        string strValue = string.Empty;
                        if (!rd.IsDBNull(rd.GetOrdinal(strName)))
                            strValue = rd.GetString(rd.GetOrdinal(strName));
                        if (strValue != string.Empty)
                        {
                            sb.Append(Environment.NewLine);
                            sb.Append(strValue);
                        }
                    }

                    if (sb.Length > 0)
                        sb.Remove(0, 2);
                }

                // Save the address information
                LabelControl1.Text = sb.ToString();
                if (CreditorChanged != null)
                {
                    CreditorChanged(this, EventArgs.Empty);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor address");
            }
            finally
            {
                if (rd != null && !rd.IsClosed)
                    rd.Close();
                if (cn != null && cn.State != ConnectionState.Closed)
                    cn.Close();
            }
        }
#if false
        private void CreditorControl_Load(object sender, System.EventArgs e)
        {
            DevExpress.Utils.ToolTipController ToolTipController = ((IForm)this.ParentForm).GetToolTipController;

            CreditorID1.ToolTipController = ToolTipController;
            LabelControl2.ToolTipController = ToolTipController;
            LabelControl1.ToolTipController = ToolTipController;
        }
#endif
    }
}