using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Create.Utility
{
    internal partial class CreditorCreateMain : System.IDisposable
    {
        public CreditorCreateMain() : base()
        {
        }

        public CreditorCreateMain(ArgParser ap) : base()
        {
        }

        /// <summary>
        /// Display the creditor update dialog.
        /// </summary>
        public void ShowDialog()
        {
            string CreditorID = null;

            // Do the create creditor function. It will create the creditor on the database at this point.
            using (var createClass = new DebtPlus.UI.Creditor.Create.CreateCreditorClass())
            {
                if (createClass.ShowDialog() != DialogResult.OK)
                {
                    return;
                }

                // This is the ID of the new creditor.
                CreditorID = createClass.Creditor;
                if (string.IsNullOrEmpty(CreditorID))
                {
                    return;
                }
            }

            // Add it to the MRU for the subsequent search
            using (var mru = new DebtPlus.Data.MRU("Creditors"))
            {
                mru.InsertTopMost(CreditorID);
                mru.SaveKey();
            }

            // Determine if the creditor still exists and load the creditor table entry
            var bc = new DebtPlus.LINQ.BusinessContext();
            try
            {
                var creditorRecord = bc.creditors.Where(s => s.Id == CreditorID).FirstOrDefault();
                if (creditorRecord == null)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The creditor is no longer in the system. The edit operation is cancelled.", "Sorry, can not find the creditor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Hook into the submit changes function to record the system note.
                bc.BeforeSubmitChanges += bc.RecordSystemNoteHandler;

                // Edit the creditor
                using (var frm = new DebtPlus.UI.Creditor.Update.Forms.Form_CreditorUpdate(bc, creditorRecord, true))
                {
                    frm.ShowDialog();
                }

                // Submit the final set of changes to the creditor
                bc.SubmitChanges();
            }

            // Handle any error condition that still occurs in the creditor operation
            catch (System.Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor information");
            }

            finally
            {
                bc.BeforeSubmitChanges -= bc.RecordSystemNoteHandler;
                bc.Dispose();
            }
        }

        #region "IDisposable Support"

        // To detect redundant calls
        private bool disposedValue;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                // TODO: set large fields to null.
            }
            this.disposedValue = true;
        }

        // TODO: override Finalize() only if Dispose(ByVal disposing As Boolean) above has code to free unmanaged resources.
        //Protected Overrides Sub Finalize()
        //    ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        //    Dispose(False)
        //    MyBase.Finalize()
        //End Sub

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion "IDisposable Support"
    }
}