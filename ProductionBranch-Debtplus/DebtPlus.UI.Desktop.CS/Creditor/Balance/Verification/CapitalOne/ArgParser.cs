using System;
using System.Collections;
using System.IO;
using System.Windows.Forms;
using DebtPlus.Reports.Template.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Balance.Verification.CapitalOne
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        private ArrayList pathnames = new ArrayList();

        private string privateCreditor = string.Empty;

        /// <summary>
        /// Creditor ID for the update event
        /// </summary>
        public string Creditor
        {
            get { return privateCreditor; }
        }

        /// <summary>
        /// Input file to be read
        /// </summary>
        protected override global::DebtPlus.Utils.ArgParserBase.SwitchStatus OnNonSwitch(string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;

            // Add command-line argument to array of pathnames.
            // Convert switchValue to set of pathnames, add each pathname to the pathnames ArrayList.
            try
            {
                string d = Path.GetDirectoryName(switchValue);
                DirectoryInfo dir = null;
                if ((d.Length == 0))
                {
                    dir = new DirectoryInfo(".");
                }
                else
                {
                    dir = new DirectoryInfo(d);
                }
                FileInfo f = null;
                foreach (FileInfo f_loopVariable in dir.GetFiles(Path.GetFileName(switchValue)))
                {
                    f = f_loopVariable;
                    pathnames.Add(f.FullName);
                }
            }
#pragma warning disable 168
            catch (System.Security.SecurityException SecEx)
            {
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }
#pragma warning restore 168

            if (pathnames.Count == 0)
            {
                AppendErrorLine("None of the specified files exists.");
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }

            return ss;
        }

        //OnNonSwitch

        private bool privateVisible;

        public bool Visible
        {
            get { return privateVisible; }
            set { privateVisible = value; }
        }

        /// <summary>
        /// Return the list of input file name
        /// </summary>
        public IEnumerator GetPathnameEnumerator()
        {
            return pathnames.GetEnumerator(0, pathnames.Count);
        }

        //GetPathnameEnumerator

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "v":
                    Visible = true;
                    break;

                case "c":
                    privateCreditor = switchValue.Trim();
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {
            "v",
            "c"
        })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }

        /// <summary>
        /// Called after all of the arguments have been parsed
        /// </summary>
        protected override global::DebtPlus.Utils.ArgParserBase.SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = default(SwitchStatus);

            // Ask for the creditor ID from the list of valid ids.
            if (Creditor == string.Empty)
            {
                DialogResult answer = default(DialogResult);
                using (CreditorParametersForm frm = new CreditorParametersForm())
                {
                    var _with1 = frm;
                    _with1.Text = "Creditor ID for the update";
                    answer = _with1.ShowDialog();
                    privateCreditor = _with1.Parameter_Creditor;
                }

                if (answer != DialogResult.OK)
                {
                    return global::DebtPlus.Utils.ArgParserBase.SwitchStatus.Quit;
                }
            }

            // Ask for the input file(s) to use
            if (pathnames.Count <= 0)
            {
                var _with2 = new OpenFileDialog();
                _with2.AddExtension = true;
                _with2.CheckFileExists = true;
                _with2.CheckPathExists = true;
                _with2.DereferenceLinks = true;
                _with2.FileName = "*.xls";
                _with2.DefaultExt = ".xls";
                _with2.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                _with2.Filter = "Excel Files (*.xls)|*.xls";
                _with2.FilterIndex = 0;
                _with2.Multiselect = true;
                _with2.ReadOnlyChecked = true;
                _with2.RestoreDirectory = true;
                _with2.Title = "Open Excel Sheet Input";
                if (_with2.ShowDialog() == DialogResult.OK)
                {
                    foreach (string FileName in _with2.FileNames)
                    {
                        pathnames.Add(FileName);
                    }
                }
                _with2.Dispose();
            }

            // Cancel if there are no file names now.
            if (pathnames.Count <= 0)
            {
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
            }
            else
            {
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            }

            // If there are no names then return an error. Otherwise, sort them.
            if (ss == global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError)
            {
                pathnames.Sort(0, pathnames.Count, null);
            }

            return ss;
        }

        //OnDoneParse
    }
}