using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Creditor.Balance.Verification.CapitalOne
{
	partial class Form1 : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_client = new DevExpress.XtraEditors.LabelControl();
			this.lbl_account_number = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_disbursement_amt = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_balance = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_last_disb_date = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_last_disb_amt = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_payments_1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl14 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_payments_2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl16 = new DevExpress.XtraEditors.LabelControl();
			this.MarqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(12, 12);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(27, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Client";
			//
			//lbl_client
			//
			this.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_client.Location = new System.Drawing.Point(117, 12);
			this.lbl_client.Name = "lbl_client";
			this.lbl_client.Size = new System.Drawing.Size(163, 13);
			this.lbl_client.TabIndex = 1;
			this.lbl_client.UseMnemonic = false;
			//
			//lbl_account_number
			//
			this.lbl_account_number.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_account_number.Location = new System.Drawing.Point(117, 31);
			this.lbl_account_number.Name = "lbl_account_number";
			this.lbl_account_number.Size = new System.Drawing.Size(163, 13);
			this.lbl_account_number.TabIndex = 3;
			this.lbl_account_number.UseMnemonic = false;
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(12, 31);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(79, 13);
			this.LabelControl4.TabIndex = 2;
			this.LabelControl4.Text = "Account Number";
			//
			//lbl_disbursement_amt
			//
			this.lbl_disbursement_amt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_disbursement_amt.Location = new System.Drawing.Point(117, 50);
			this.lbl_disbursement_amt.Name = "lbl_disbursement_amt";
			this.lbl_disbursement_amt.Size = new System.Drawing.Size(163, 13);
			this.lbl_disbursement_amt.TabIndex = 5;
			this.lbl_disbursement_amt.UseMnemonic = false;
			//
			//LabelControl6
			//
			this.LabelControl6.Location = new System.Drawing.Point(12, 50);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(87, 13);
			this.LabelControl6.TabIndex = 4;
			this.LabelControl6.Text = "Disbursement Amt";
			//
			//lbl_balance
			//
			this.lbl_balance.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_balance.Location = new System.Drawing.Point(117, 69);
			this.lbl_balance.Name = "lbl_balance";
			this.lbl_balance.Size = new System.Drawing.Size(163, 13);
			this.lbl_balance.TabIndex = 7;
			this.lbl_balance.UseMnemonic = false;
			//
			//LabelControl8
			//
			this.LabelControl8.Location = new System.Drawing.Point(12, 69);
			this.LabelControl8.Name = "LabelControl8";
			this.LabelControl8.Size = new System.Drawing.Size(37, 13);
			this.LabelControl8.TabIndex = 6;
			this.LabelControl8.Text = "Balance";
			//
			//lbl_last_disb_date
			//
			this.lbl_last_disb_date.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_last_disb_date.Location = new System.Drawing.Point(117, 88);
			this.lbl_last_disb_date.Name = "lbl_last_disb_date";
			this.lbl_last_disb_date.Size = new System.Drawing.Size(163, 13);
			this.lbl_last_disb_date.TabIndex = 9;
			this.lbl_last_disb_date.UseMnemonic = false;
			//
			//LabelControl10
			//
			this.LabelControl10.Location = new System.Drawing.Point(12, 88);
			this.LabelControl10.Name = "LabelControl10";
			this.LabelControl10.Size = new System.Drawing.Size(69, 13);
			this.LabelControl10.TabIndex = 8;
			this.LabelControl10.Text = "Last Disb Date";
			//
			//lbl_last_disb_amt
			//
			this.lbl_last_disb_amt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_last_disb_amt.Location = new System.Drawing.Point(117, 107);
			this.lbl_last_disb_amt.Name = "lbl_last_disb_amt";
			this.lbl_last_disb_amt.Size = new System.Drawing.Size(163, 13);
			this.lbl_last_disb_amt.TabIndex = 11;
			this.lbl_last_disb_amt.UseMnemonic = false;
			//
			//LabelControl12
			//
			this.LabelControl12.Location = new System.Drawing.Point(12, 107);
			this.LabelControl12.Name = "LabelControl12";
			this.LabelControl12.Size = new System.Drawing.Size(65, 13);
			this.LabelControl12.TabIndex = 10;
			this.LabelControl12.Text = "Last Disb Amt";
			//
			//lbl_payments_1
			//
			this.lbl_payments_1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_payments_1.Location = new System.Drawing.Point(117, 126);
			this.lbl_payments_1.Name = "lbl_payments_1";
			this.lbl_payments_1.Size = new System.Drawing.Size(163, 13);
			this.lbl_payments_1.TabIndex = 13;
			this.lbl_payments_1.UseMnemonic = false;
			//
			//LabelControl14
			//
			this.LabelControl14.Location = new System.Drawing.Point(12, 126);
			this.LabelControl14.Name = "LabelControl14";
			this.LabelControl14.Size = new System.Drawing.Size(99, 13);
			this.LabelControl14.TabIndex = 12;
			this.LabelControl14.Text = "Payments Remaining";
			//
			//lbl_payments_2
			//
			this.lbl_payments_2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_payments_2.Location = new System.Drawing.Point(117, 145);
			this.lbl_payments_2.Name = "lbl_payments_2";
			this.lbl_payments_2.Size = new System.Drawing.Size(163, 13);
			this.lbl_payments_2.TabIndex = 15;
			this.lbl_payments_2.UseMnemonic = false;
			//
			//LabelControl16
			//
			this.LabelControl16.Location = new System.Drawing.Point(12, 145);
			this.LabelControl16.Name = "LabelControl16";
			this.LabelControl16.Size = new System.Drawing.Size(99, 13);
			this.LabelControl16.TabIndex = 14;
			this.LabelControl16.Text = "Payments Remaining";
			//
			//MarqueeProgressBarControl1
			//
			this.MarqueeProgressBarControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.MarqueeProgressBarControl1.EditValue = 0;
			this.MarqueeProgressBarControl1.Location = new System.Drawing.Point(12, 164);
			this.MarqueeProgressBarControl1.Name = "MarqueeProgressBarControl1";
			this.MarqueeProgressBarControl1.Size = new System.Drawing.Size(268, 18);
			this.MarqueeProgressBarControl1.TabIndex = 16;
			//
			//Form1
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(292, 192);
			this.Controls.Add(this.MarqueeProgressBarControl1);
			this.Controls.Add(this.lbl_payments_2);
			this.Controls.Add(this.LabelControl16);
			this.Controls.Add(this.lbl_payments_1);
			this.Controls.Add(this.LabelControl14);
			this.Controls.Add(this.lbl_last_disb_amt);
			this.Controls.Add(this.LabelControl12);
			this.Controls.Add(this.lbl_last_disb_date);
			this.Controls.Add(this.LabelControl10);
			this.Controls.Add(this.lbl_balance);
			this.Controls.Add(this.LabelControl8);
			this.Controls.Add(this.lbl_disbursement_amt);
			this.Controls.Add(this.LabelControl6);
			this.Controls.Add(this.lbl_account_number);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.lbl_client);
			this.Controls.Add(this.LabelControl1);
			this.LookAndFeel.UseDefaultLookAndFeel = true;
			this.Name = "Form1";
			this.Text = "Setting Peregrin Balances";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.MarqueeProgressBarControl1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl lbl_client;
		protected internal DevExpress.XtraEditors.LabelControl lbl_account_number;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.LabelControl lbl_disbursement_amt;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl6;
		protected internal DevExpress.XtraEditors.LabelControl lbl_balance;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl8;
		protected internal DevExpress.XtraEditors.LabelControl lbl_last_disb_date;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl10;
		protected internal DevExpress.XtraEditors.LabelControl lbl_last_disb_amt;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl12;
		protected internal DevExpress.XtraEditors.LabelControl lbl_payments_1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl14;
		protected internal DevExpress.XtraEditors.LabelControl lbl_payments_2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl16;
		protected internal DevExpress.XtraEditors.MarqueeProgressBarControl MarqueeProgressBarControl1;
	}
}
