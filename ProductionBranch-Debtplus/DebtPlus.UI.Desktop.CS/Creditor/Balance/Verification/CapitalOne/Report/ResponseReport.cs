#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.IO;
using DebtPlus.Reports.Template;

namespace DebtPlus.UI.Desktop.CS.Creditor.Balance.Verification.CapitalOne
{
    internal partial class ResponseReport : TemplateXtraReportClass
    {
        public ResponseReport() : base()
        {
            // Name of the report file
            const string ReportName = "DebtPlus.Reports.Creditor.Balance.Verification.CapitalOne.Responses.repx";

            // See if there is a report reference that we can use
            bool UseDefault = true;
            string Fname = System.IO.Path.Combine(DebtPlus.Configuration.Config.ReportsDirectory, ReportName);
            try
            {
                if (System.IO.File.Exists(Fname))
                {
                    LoadLayout(Fname);
                    UseDefault = false;
                }
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }

            // Load the standard report definition if we need a new item
            if (UseDefault)
            {
                System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
                // changed
                using (Stream ios = asm.GetManifestResourceStream(string.Format("global::DebtPlus.UI.Desktop.CS.{0}", ReportName)))
                {
                    // changed
                    if (ios != null)
                        LoadLayout(ios);
                }
            }

            // Set the script references as needed
            ScriptReferences = global::DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();

            // Update the subreports as well
            foreach (DevExpress.XtraReports.UI.Band BandPtr in Bands)
            {
                foreach (DevExpress.XtraReports.UI.XRControl CtlPtr in BandPtr.Controls)
                {
                    DevExpress.XtraReports.UI.XRSubreport Rpt = CtlPtr as DevExpress.XtraReports.UI.XRSubreport;
                    if (Rpt != null)
                    {
                        DevExpress.XtraReports.UI.XtraReport RptSrc = Rpt.ReportSource as DevExpress.XtraReports.UI.XtraReport;
                        if (RptSrc != null)
                        {
                            RptSrc.ScriptReferences = global::DebtPlus.Utils.AssemblyTools.GetAllSiblingAssemblies();
                        }
                    }
                }
            }

            // Indicate that we support the report filter
            ReportFilter.IsEnabled = true;
        }

        /// <summary>
        /// Report title
        /// </summary>
        public override string ReportTitle
        {
            get
            {
                return "Balance Verifications";
            }
        }

        /// <summary>
        /// Report sub-title
        /// </summary>
        public override string ReportSubTitle
        {
            get
            {
                return global::DebtPlus.Utils.Nulls.DStr(Parameters["ParameterSubtitle"].Value);
            }
        }
    }
}