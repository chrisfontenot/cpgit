using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Creditor.Balance.Verification.CapitalOne
{
    partial class ResponseReport
    {
        //XtraReport overrides dispose to clean up the component list.
        [System.Diagnostics.DebuggerNonUserCode]
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        //Required by the Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Designer
        //It can be modified using the Designer.  
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.XrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.XrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.XrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_result = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_account_number = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_balance = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_disbursement_factor = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_last_disb_date = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_pmts_remaining_1 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_pmts_remaining_2 = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_client = new DevExpress.XtraReports.UI.XRLabel();
            this.XrLabel_last_disb_amt = new DevExpress.XtraReports.UI.XRLabel();
            this.XrPanel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.XrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.ParameterSubtitle = new DevExpress.XtraReports.Parameters.Parameter();
            ((System.ComponentModel.ISupportInitialize)this).BeginInit();
            //
            //Detail
            //
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
			this.XrLabel_last_disb_amt,
			this.XrLabel_pmts_remaining_1,
			this.XrLabel_last_disb_date,
			this.XrLabel_client,
			this.XrLabel_pmts_remaining_2,
			this.XrLabel_account_number,
			this.XrLabel_result,
			this.XrLabel_disbursement_factor,
			this.XrLabel_balance
		});
            this.Detail.HeightF = 15f;
            //
            //PageHeader
            //
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
			this.XrPanel3,
			this.XrPanel2,
			this.XrPanel1
		});
            this.PageHeader.HeightF = 160.25f;
            this.PageHeader.Controls.SetChildIndex(this.XrPanel_AgencyAddress, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrLabel_Title, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrLine_Title, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrLabel_Subtitle, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrPageInfo1, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrPanel1, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrPanel2, 0);
            this.PageHeader.Controls.SetChildIndex(this.XrPanel3, 0);
            //
            //XrLabel_Title
            //
            this.XrLabel_Title.StylePriority.UseFont = false;
            //
            //XrLabel_Subtitle
            //
            this.XrLabel_Subtitle.LocationFloat = new DevExpress.Utils.PointFloat(7.999992f, 85.00001f);
            this.XrLabel_Subtitle.SizeF = new System.Drawing.SizeF(1034f, 17f);
            //
            //XrPageInfo_PageNumber
            //
            this.XrPageInfo_PageNumber.LocationFloat = new DevExpress.Utils.PointFloat(917f, 8f);
            this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
            //
            //XrPanel_AgencyAddress
            //
            this.XrPanel_AgencyAddress.LocationFloat = new DevExpress.Utils.PointFloat(742f, 0f);
            //
            //XRLabel_Agency_Name
            //
            this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address3
            //
            this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address1
            //
            this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Phone
            //
            this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
            this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
            //
            //XrLabel_Agency_Address2
            //
            this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
            this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
            //
            //XrPanel1
            //
            this.XrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
			this.XrLabel9,
			this.XrLabel6,
			this.XrLabel5,
			this.XrLabel1,
			this.XrLabel7,
			this.XrLabel2,
			this.XrLabel8,
			this.XrLabel4,
			this.XrLabel3
		});
            this.XrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 137f);
            this.XrPanel1.Name = "XrPanel1";
            this.XrPanel1.SizeF = new System.Drawing.SizeF(1040f, 17.00001f);
            this.XrPanel1.StyleName = "XrControlStyle_HeaderPannel";
            //
            //XrLabel9
            //
            this.XrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(393.75f, 0.9999084f);
            this.XrLabel9.Name = "XrLabel9";
            this.XrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel9.SizeF = new System.Drawing.SizeF(100f, 15.00002f);
            this.XrLabel9.Text = "AMT";
            //
            //XrLabel6
            //
            this.XrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(669.7916f, 0.9999593f);
            this.XrLabel6.Name = "XrLabel6";
            this.XrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel6.SizeF = new System.Drawing.SizeF(53.125f, 15f);
            this.XrLabel6.Text = "DISB";
            //
            //XrLabel5
            //
            this.XrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(493.7501f, 0.9999847f);
            this.XrLabel5.Name = "XrLabel5";
            this.XrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel5.SizeF = new System.Drawing.SizeF(76.04163f, 15f);
            this.XrLabel5.Text = "DATE";
            //
            //XrLabel1
            //
            this.XrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 1f);
            this.XrLabel1.Name = "XrLabel1";
            this.XrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100f);
            this.XrLabel1.SizeF = new System.Drawing.SizeF(100f, 15f);
            this.XrLabel1.StylePriority.UsePadding = false;
            this.XrLabel1.Text = "CLIENT";
            //
            //XrLabel7
            //
            this.XrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(722.9166f, 0.9999847f);
            this.XrLabel7.Name = "XrLabel7";
            this.XrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel7.SizeF = new System.Drawing.SizeF(53.125f, 15f);
            this.XrLabel7.Text = "AMT";
            //
            //XrLabel2
            //
            this.XrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(100f, 0.9999847f);
            this.XrLabel2.Name = "XrLabel2";
            this.XrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100f);
            this.XrLabel2.SizeF = new System.Drawing.SizeF(193.75f, 15f);
            this.XrLabel2.StylePriority.UsePadding = false;
            this.XrLabel2.StylePriority.UseTextAlignment = false;
            this.XrLabel2.Text = "ACCOUNT";
            this.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //XrLabel8
            //
            this.XrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(776.0417f, 0.9999847f);
            this.XrLabel8.Name = "XrLabel8";
            this.XrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100f);
            this.XrLabel8.SizeF = new System.Drawing.SizeF(178.5417f, 15f);
            this.XrLabel8.StylePriority.UsePadding = false;
            this.XrLabel8.StylePriority.UseTextAlignment = false;
            this.XrLabel8.Text = "STATUS";
            this.XrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //XrLabel4
            //
            this.XrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(569.7916f, 1f);
            this.XrLabel4.Name = "XrLabel4";
            this.XrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel4.SizeF = new System.Drawing.SizeF(100f, 15f);
            this.XrLabel4.Text = "DISB FACTOR";
            //
            //XrLabel3
            //
            this.XrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(293.75f, 1f);
            this.XrLabel3.Name = "XrLabel3";
            this.XrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel3.SizeF = new System.Drawing.SizeF(100f, 15f);
            this.XrLabel3.Text = "BALANCE";
            //
            //XrPanel2
            //
            this.XrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] { this.XrLabel10 });
            this.XrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(669.7916f, 120f);
            this.XrPanel2.Name = "XrPanel2";
            this.XrPanel2.SizeF = new System.Drawing.SizeF(106.2501f, 17f);
            this.XrPanel2.StyleName = "XrControlStyle_HeaderPannel";
            //
            //XrLabel10
            //
            this.XrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(0f, 1f);
            this.XrLabel10.Name = "XrLabel10";
            this.XrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel10.SizeF = new System.Drawing.SizeF(106.2501f, 15.00001f);
            this.XrLabel10.StylePriority.UseTextAlignment = false;
            this.XrLabel10.Text = "PAYMENTS";
            this.XrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            //
            //XrLabel_result
            //
            this.XrLabel_result.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "result") });
            this.XrLabel_result.LocationFloat = new DevExpress.Utils.PointFloat(776.0417f, 0f);
            this.XrLabel_result.Name = "XrLabel_result";
            this.XrLabel_result.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100f);
            this.XrLabel_result.SizeF = new System.Drawing.SizeF(263.9583f, 15f);
            this.XrLabel_result.StylePriority.UsePadding = false;
            this.XrLabel_result.StylePriority.UseTextAlignment = false;
            this.XrLabel_result.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //XrLabel_account_number
            //
            this.XrLabel_account_number.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "account_number") });
            this.XrLabel_account_number.LocationFloat = new DevExpress.Utils.PointFloat(100f, 0f);
            this.XrLabel_account_number.Name = "XrLabel_account_number";
            this.XrLabel_account_number.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100f);
            this.XrLabel_account_number.SizeF = new System.Drawing.SizeF(193.75f, 15f);
            this.XrLabel_account_number.StylePriority.UsePadding = false;
            this.XrLabel_account_number.StylePriority.UseTextAlignment = false;
            this.XrLabel_account_number.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            //
            //XrLabel_balance
            //
            this.XrLabel_balance.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "balance", "{0:c}") });
            this.XrLabel_balance.LocationFloat = new DevExpress.Utils.PointFloat(293.75f, 0f);
            this.XrLabel_balance.Name = "XrLabel_balance";
            this.XrLabel_balance.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_balance.SizeF = new System.Drawing.SizeF(100f, 15f);
            this.XrLabel_balance.StylePriority.UseTextAlignment = false;
            this.XrLabel_balance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_disbursement_factor
            //
            this.XrLabel_disbursement_factor.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "disbursement_factor", "{0:c}") });
            this.XrLabel_disbursement_factor.LocationFloat = new DevExpress.Utils.PointFloat(569.7916f, 0f);
            this.XrLabel_disbursement_factor.Name = "XrLabel_disbursement_factor";
            this.XrLabel_disbursement_factor.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_disbursement_factor.SizeF = new System.Drawing.SizeF(100f, 15f);
            this.XrLabel_disbursement_factor.StylePriority.UseTextAlignment = false;
            this.XrLabel_disbursement_factor.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_last_disb_date
            //
            this.XrLabel_last_disb_date.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "last_disbursement_date", "{0:d}") });
            this.XrLabel_last_disb_date.LocationFloat = new DevExpress.Utils.PointFloat(493.75f, 0f);
            this.XrLabel_last_disb_date.Name = "XrLabel_last_disb_date";
            this.XrLabel_last_disb_date.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_last_disb_date.SizeF = new System.Drawing.SizeF(76.04163f, 15f);
            this.XrLabel_last_disb_date.StylePriority.UseTextAlignment = false;
            this.XrLabel_last_disb_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_pmts_remaining_1
            //
            this.XrLabel_pmts_remaining_1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "remaining_payments_1", "{0:f0}") });
            this.XrLabel_pmts_remaining_1.LocationFloat = new DevExpress.Utils.PointFloat(669.7916f, 0f);
            this.XrLabel_pmts_remaining_1.Name = "XrLabel_pmts_remaining_1";
            this.XrLabel_pmts_remaining_1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_pmts_remaining_1.SizeF = new System.Drawing.SizeF(53.125f, 15f);
            this.XrLabel_pmts_remaining_1.StylePriority.UseTextAlignment = false;
            this.XrLabel_pmts_remaining_1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_pmts_remaining_2
            //
            this.XrLabel_pmts_remaining_2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "remaining_payments_2", "{0:f0}") });
            this.XrLabel_pmts_remaining_2.LocationFloat = new DevExpress.Utils.PointFloat(722.9166f, 0f);
            this.XrLabel_pmts_remaining_2.Name = "XrLabel_pmts_remaining_2";
            this.XrLabel_pmts_remaining_2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_pmts_remaining_2.SizeF = new System.Drawing.SizeF(53.125f, 15f);
            this.XrLabel_pmts_remaining_2.StylePriority.UseTextAlignment = false;
            this.XrLabel_pmts_remaining_2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_client
            //
            this.XrLabel_client.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "client", "{0:0000000}") });
            this.XrLabel_client.LocationFloat = new DevExpress.Utils.PointFloat(0f, 0f);
            this.XrLabel_client.Name = "XrLabel_client";
            this.XrLabel_client.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 5, 0, 0, 100f);
            this.XrLabel_client.SizeF = new System.Drawing.SizeF(100f, 15f);
            this.XrLabel_client.StylePriority.UsePadding = false;
            this.XrLabel_client.StylePriority.UseTextAlignment = false;
            this.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrLabel_last_disb_amt
            //
            this.XrLabel_last_disb_amt.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "last_disbursement_amount", "{0:c}") });
            this.XrLabel_last_disb_amt.LocationFloat = new DevExpress.Utils.PointFloat(393.75f, 0f);
            this.XrLabel_last_disb_amt.Name = "XrLabel_last_disb_amt";
            this.XrLabel_last_disb_amt.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel_last_disb_amt.SizeF = new System.Drawing.SizeF(100f, 15f);
            this.XrLabel_last_disb_amt.StylePriority.UseTextAlignment = false;
            this.XrLabel_last_disb_amt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            //
            //XrPanel3
            //
            this.XrPanel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] { this.XrLabel12 });
            this.XrPanel3.LocationFloat = new DevExpress.Utils.PointFloat(393.75f, 120f);
            this.XrPanel3.Name = "XrPanel3";
            this.XrPanel3.SizeF = new System.Drawing.SizeF(176.0417f, 17f);
            this.XrPanel3.StyleName = "XrControlStyle_HeaderPannel";
            //
            //XrLabel12
            //
            this.XrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(0f, 0.9999924f);
            this.XrLabel12.Name = "XrLabel12";
            this.XrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100f);
            this.XrLabel12.SizeF = new System.Drawing.SizeF(176.0416f, 15.00002f);
            this.XrLabel12.StylePriority.UseTextAlignment = false;
            this.XrLabel12.Text = "LAST DISBURSEMENT";
            this.XrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            //
            //ParameterSubtitle
            //
            this.ParameterSubtitle.Description = "Subtitle String";
            this.ParameterSubtitle.Name = "ParameterSubtitle";
            this.ParameterSubtitle.Visible = false;
            //
            //ResponseReport
            //
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
			this.Detail,
			this.PageHeader,
			this.PageFooter,
			this.TopMarginBand1,
			this.BottomMarginBand1
		});
            this.Landscape = true;
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Parameters.AddRange(new DevExpress.XtraReports.Parameters.Parameter[] { this.ParameterSubtitle });
            this.RequestParameters = false;
            this.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
			this.XrControlStyle_Header,
			this.XrControlStyle_Totals,
			this.XrControlStyle_HeaderPannel,
			this.XrControlStyle_GroupHeader
		});
            this.Version = "11.2";
            ((System.ComponentModel.ISupportInitialize)this).EndInit();

        }
        protected internal DevExpress.XtraReports.UI.XRPanel XrPanel1;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel6;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel5;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel1;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel7;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel2;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel8;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel4;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel3;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_pmts_remaining_1;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_last_disb_date;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_client;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_pmts_remaining_2;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_account_number;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_result;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_disbursement_factor;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_balance;
        protected internal DevExpress.XtraReports.UI.XRPanel XrPanel2;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel10;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_last_disb_amt;
        protected internal DevExpress.XtraReports.UI.XRPanel XrPanel3;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel12;
        protected internal DevExpress.XtraReports.UI.XRLabel XrLabel9;
        protected internal DevExpress.XtraReports.Parameters.Parameter ParameterSubtitle;
    }
}
