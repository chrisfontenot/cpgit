#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion

using System;
using System.Collections;
using System.Data;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Balance.Verification.CapitalOne
{
	internal partial class Form1
	{
        public Form1() : base()
		{
			InitializeComponent();
			this.Load += Form1_Load;
		}

		private readonly ArgParser ap;
		private readonly DataSet ds = new DataSet("ds");
		private DataTable tbl;

		// Regular expression for the account number
		private readonly System.Text.RegularExpressions.Regex rx_account_number = new System.Text.RegularExpressions.Regex("\\d{11,}", System.Text.RegularExpressions.RegexOptions.Singleline);

		// Information shared from the parser to the display logic
		private decimal Balance;
		private decimal DisbursementFactor;
		private decimal DisbursementAmount;
		private Int32 Client;
		private string AccountNumber;

		private DateTime DisbursementDate;
		public Form1(ArgParser ap) : this()
		{
			this.ap = ap;
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			System.Threading.Thread thrd = new System.Threading.Thread(ThreadProcedure);
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.IsBackground = true;
            thrd.Name = "ProcessingThread";
            thrd.Start();
		}

		private delegate void FormCloseDelegate();
		private void FormClose()
		{
			if (InvokeRequired)
            {
				BeginInvoke(new FormCloseDelegate(FormClose));
			}
            else
            {
				Close();
			}
		}

		private void ThreadProcedure()
		{
			// Find the defaults for the items from the app.config file
			System.Collections.Specialized.NameValueCollection configKeys = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CreditorBalanceVerificationCapitalOne);

			// Create the update table to hold the update information
			tbl = new DataTable("updates");
			tbl.Columns.Add("client", typeof(Int32));
			tbl.Columns.Add("account_number", typeof(string));
			tbl.Columns.Add("balance", typeof(decimal));
			tbl.Columns.Add("disbursement_factor", typeof(decimal));
			tbl.Columns.Add("last_disbursement_date", typeof(DateTime));
			tbl.Columns.Add("last_disbursement_amount", typeof(decimal));

			tbl.Columns.Add("result", typeof(string));
            tbl.Columns["result"].AllowDBNull = true;

			tbl.Columns.Add("id", typeof(Int32));
			var col = tbl.Columns["id"];
			col.AutoIncrement = true;
			col.AutoIncrementSeed = 1;
			col.AutoIncrementStep = 1;
			tbl.PrimaryKey = new DataColumn[] { col };

			ds.Tables.Add(tbl);

			try
            {
				// Open the excel application
				if (OpenApplication())
                {
					try
                    {
						SetVisible(ap.Visible);

						// Process the files and the clients
						IEnumerator ie = ap.GetPathnameEnumerator();
						while (ie.MoveNext())
                        {
							string Filename = Convert.ToString(ie.Current);
							ProcessFile(Filename);
						}

						// Update the database
						UpdateDatabase();
						PrintReport();
					}
                    
                    catch (Exception ex)
                    {
						DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
					}
                    
                    finally
                    {
                        // Terminate the EXCEL application
                        QuitApplication();
					}
				}
			}
            
            catch (Exception ex)
            {
				DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
			}

			// Close the form when we are completed
			FormClose();
		}

		private Int32 MapColumn(string strName)
		{
			// If there is no string then return the "it is missing" special value
			if (string.IsNullOrWhiteSpace(strName))
            {
				return -1;
			}

			string key = strName.ToUpper();
			Int32 value = 0;

			// Handle the case where the column is "aa" ect. The columns need to handle values above 26.
			for (var charIndex = 0; charIndex <= key.Length - 1; charIndex++) {
				char chr = key[charIndex];
				Int32 id = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".IndexOf(chr) + 1;
				if (id < 1)
                {
					return -2;
				}
				value = (value * 26) + id;
			}

			// Return the column "a" as 0, "b" as 1, etc.
			return value - 1;
		}

		private void ProcessFile(string Filename)
		{
			// Find the defaults for the items from the app.config file
			System.Collections.Specialized.NameValueCollection configKeys = DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CreditorBalanceVerificationCapitalOne);

			// Find the columns for the fields
			Int32 colClientID        = MapColumn(configKeys["ClientID"]);
			Int32 colAccountNumber   = MapColumn(configKeys["AccoutNumber"]);
			Int32 colProposalAmt     = MapColumn(configKeys["ProposalAmt"]);
			Int32 colBalance         = MapColumn(configKeys["Balance"]);
			Int32 colLastPaymentDate = MapColumn(configKeys["LastPaymentDate"]);
			Int32 colLastPaymentAmt  = MapColumn(configKeys["LastPaymentAmt"]);

			if (colClientID < 0 || colAccountNumber < 0 || colProposalAmt < -1 || colBalance < 0 || colLastPaymentAmt < -1 || colLastPaymentDate < -1)
            {
				DebtPlus.Data.Forms.MessageBox.Show("Invalid configuration file Config/DebtPlus.Creditor.Balance.Verification.CapitalOne.Default.config", "Configuration Error");
				return;
			}

			// Open the excel document
			if (OpenWorkbook(Filename) && GetSheet(1) && ActivateSheet())
            {
				// Process the rows in the sheet
				bool Previous = false;
				Int32 RowCount = Convert.ToInt32(ThisSheet.Rows.Count);
				Client = 0;

				for (Int32 RowNumber = 0; RowNumber <= RowCount - 1; RowNumber++)
                {
					// If this row is not processed but the last one was then we have handled all of the items.
					bool Current = UpdateBalances(CellValue(RowNumber, colClientID), CellValue(RowNumber, colAccountNumber), CellValue(RowNumber, colProposalAmt), CellValue(RowNumber, colBalance), CellValue(RowNumber, colLastPaymentDate), CellValue(RowNumber, colLastPaymentAmt));
                    if (Previous && !Current)
                    {
                        break;
                    }
					Previous = Current;
				}
			}
		}

		private void DisplayInformation()
		{
			if (InvokeRequired)
            {
				IAsyncResult ia = BeginInvoke(new MethodInvoker(DisplayInformation));
				EndInvoke(ia);
			}
            
            else
            {
				// Display the information on the form
				lbl_client.Text           = string.Format("{0:0000000}", Client);
				lbl_account_number.Text   = AccountNumber;
				lbl_balance.Text          = string.Format("{0:c}", Balance);
				lbl_disbursement_amt.Text = string.Format("{0:c}", DisbursementFactor);
				lbl_last_disb_date.Text   = DisbursementDate.ToShortDateString();
				lbl_last_disb_amt.Text    = string.Format("{0:c}", DisbursementAmount);

				Invalidate();
			}
		}

		private bool UpdateBalances(string colClientID, string colAccountNumber, string colDisbursementFactor, string colBalance, string colDisbursementDate, string colDisbursementAmount)
		{
			// The account number is just a string. It must be present for all valid rows.
			AccountNumber = colAccountNumber;
            if (string.IsNullOrEmpty(AccountNumber) || !rx_account_number.IsMatch(AccountNumber))
            {
                return false;
            }

			// Save the last parsed client
			if (!string.IsNullOrEmpty(colClientID))
            {
				Int32 CurrentClient;
                if (Int32.TryParse(colClientID, out CurrentClient) && CurrentClient > 0)
                {
                    Client = CurrentClient;
                }
			}

            if (Client <= 0)
            {
                return false;
            }

			// Parse the other fields to suitable numeric values
            if (!decimal.TryParse(colDisbursementFactor, out DisbursementFactor))
            {
                return false;
            }
            if (!decimal.TryParse(colBalance, out Balance))
            {
                return false;
            }
            if (!decimal.TryParse(colDisbursementAmount, out DisbursementAmount))
            {
                return false;
            }

			// These are defaulted to reasonable figures
            if (!System.DateTime.TryParse(colDisbursementDate, out DisbursementDate))
            {
                DisbursementDate = new DateTime(1900, 1, 1);
            }

			// Update the information with the parameters
			DisplayInformation();

			// Add the row to the table
			tbl.Rows.Add(new object[]
                {
				    Client,
				    AccountNumber,
				    Balance,
				    DisbursementFactor,
				    DisbursementDate,
				    DisbursementAmount
			    });
			return true;
		}

		private delegate void UpdateCompletedDelegate(Int32 ItemCount);
		private void UpdateCompleted(Int32 ItemCount)
		{
			if (InvokeRequired)
            {
				Invoke(new UpdateCompletedDelegate(UpdateCompleted), new object[] { ItemCount });
			}
            
            else
            {
				MarqueeProgressBarControl1.Properties.Stopped = true;
				// DebtPlus.Data.Forms.MessageBox.Show(String.Format("{0:n0} debts have been updated.", ItemCount), "Operation Successful", MessageBoxButtons.OK, MessageBoxIcon.Information)
			}
		}

		private void UpdateDatabase()
		{
			// Generate the update event
			try
            {
				using (DebtPlus.UI.Common.CursorManager cm = new DebtPlus.UI.Common.CursorManager())
                {
					using (System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand())
                    {
						cmd.Connection = new System.Data.SqlClient.SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
						cmd.CommandText = "xpr_Peregrin_Balance_Update";
						cmd.CommandType = System.Data.CommandType.StoredProcedure;

						cmd.Parameters.Add("@client_text", System.Data.SqlDbType.Int, 0, "client");
						cmd.Parameters.Add("@account_number", System.Data.SqlDbType.VarChar, 80, "account_number");
						cmd.Parameters.Add("@disbursement_factor_text", System.Data.SqlDbType.Decimal, 0, "disbursement_factor");
						cmd.Parameters.Add("@balance_text", System.Data.SqlDbType.Decimal, 0, "balance");
						cmd.Parameters.Add("@last_disbursement_date_text", System.Data.SqlDbType.DateTime, 0, "last_disbursement_date");
						cmd.Parameters.Add("@last_disbursement_amount_text", System.Data.SqlDbType.Decimal, 0, "last_disbursement_amount");
						cmd.Parameters.Add("@result", System.Data.SqlDbType.VarChar, 80, "result").Direction = ParameterDirection.Output;
						cmd.Parameters.Add("@creditor", System.Data.SqlDbType.VarChar, 10).Value = ap.Creditor;

						using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter { InsertCommand = cmd })
                        {
							UpdateCompleted(da.Update(tbl));
						}
					}
				}
			}
            
            catch (System.Data.SqlClient.SqlException ex)
            {
				DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error inserting balance updates");
			}
		}

		/// <summary>
		/// Generate and display the resulting report
		/// </summary>
		/// <remarks></remarks>
		private void PrintReport()
		{
			using (var rpt = new ResponseReport())
            {
				rpt.BeforePrint       += SetReportDataSource;
				rpt.GetSubTitleString += GetSubTitleString;
				using (var frm = new DebtPlus.Reports.Template.PrintPreviewForm(rpt))
                {
					frm.ShowDialog();
				}
				rpt.BeforePrint       -= SetReportDataSource;
				rpt.GetSubTitleString -= GetSubTitleString;
			}
		}

		private void GetSubTitleString(object sender, DebtPlus.Reports.Template.TitleStringEventArgs e)
		{
			e.Title = string.Format("Items for creditor ID {0}", ap.Creditor);
		}

		private void SetReportDataSource(object Sender, System.EventArgs e)
		{
			DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport) Sender;
			rpt.DataSource = new System.Data.DataView(tbl, ((DebtPlus.Interfaces.Reports.IReportFilter) rpt).ReportFilter.ViewFilter, string.Empty, DataViewRowState.CurrentRows);
			foreach (DevExpress.XtraReports.UI.CalculatedField calc in rpt.CalculatedFields)
            {
				calc.Assign(rpt.DataSource, rpt.DataMember);
			}
		}
	}
}
