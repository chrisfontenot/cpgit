﻿using System;
using System.Reflection;
using System.Windows.Forms;
using System.Diagnostics;

#pragma warning disable 1974    // We know that we use Debug.Assert with dynamic fields.

// ///////////////////////////////////////////////////////////////////////////////
// //                                                                           //
// //     WARNING: INCLUDE THIS FILE OR THE ONE CALLED "PIA" BUT NOT BOTH       //
// //                                                                           //
// ///////////////////////////////////////////////////////////////////////////////

namespace DebtPlus.UI.Desktop.CS.Creditor.Balance.Verification.CapitalOne
{
	partial class Form1
	{
		private dynamic ThisApplication;
        private dynamic ThisWorkbook;
        private dynamic ThisSheet;

		[System.Security.SecuritySafeCritical()]
		private bool OpenApplication()
		{
			Type objClassType = Type.GetTypeFromProgID("Excel.Application");
            Debug.Assert(objClassType != null, "Excel.Application is not defined");
            
			ThisApplication = Activator.CreateInstance(objClassType);
            Debug.Assert(ThisApplication != null, "Unable to create instance of application");

			return true;
		}

		private bool QuitApplication()
		{
			bool answer = false;

			if (ThisApplication != null)
            {
				try
                {
					InvokeMethod(ThisApplication, "Quit");
					ThisApplication = null;
					answer = true;
				}
                
                catch (Exception ex)
                {
					DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
					DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error quitting application", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}

			return answer;
		}

		private bool ActivateSheet()
		{
			try
            {
                Debug.Assert(ThisSheet != null, "ActivateSheet does not have a sheet pointer");
				InvokeMethod(ThisSheet, "Activate");
			}
            
            catch (Exception ex)
            {
				DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
				DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error activating sheet", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}
			return true;
		}

		private bool GetSheet(object Name)
		{
			try
            {
                Debug.Assert(ThisWorkbook != null, "GetSheet does not have a workbook pointer");

				dynamic Sheets = GetProperty(ThisWorkbook, "Sheets");
                Debug.Assert(Sheets != null, "Unable to locate sheet collection");

				ThisSheet = GetProperty(Sheets, "Item", Name);
                Debug.Assert(ThisSheet, "Unable to locate named sheet instance");
			}
            
            catch (Exception ex)
            {
				DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
				DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error finding sheet", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			return true;
		}

		private void SetVisible(bool Value)
		{
            Debug.Assert(ThisApplication != null, "SetVisible does not have an application");
			SetProperty(ThisApplication, "Visible", Value);
		}

		private bool OpenWorkbook(string Fname)
		{
			try
            {
                Debug.Assert(ThisApplication != null, "OpenWorkbook does not have an application");

				dynamic Workbooks = GetProperty(ThisApplication, "Workbooks");
                Debug.Assert(Workbooks, "Unable to locate workbook collection");

				ThisWorkbook = InvokeMethod(Workbooks, "Open", Fname);
                Debug.Assert(ThisWorkbook != null, "Unable to open workbook");
			}
            
            catch (Exception ex)
            {
				DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
				DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return false;
			}

			return true;
		}

        private string CellValue(Int32 RowID, Int32 ColId)
        {
            // If there is no location then there is no value.
            if (RowID < 0 || ColId < 0)
            {
                return string.Empty;
            }

            // Create a range to hold the desired cell. We just want one cell in the range.
            try
            {
                dynamic rangeValue = ThisSheet.Cells(RowID + 1, ColId + 1);
                if (rangeValue == null)
                {
                    return string.Empty;
                }

                // Return the string value of the cell.
                return Convert.ToString(rangeValue.value);
            }

            catch
            {
                return string.Empty;
            }
        }

        [System.Security.SecuritySafeCritical()]
		private static void SetProperty(object obj, string sProperty, object oValue)
		{
			object[] oParam = new object[1];
			oParam[0] = oValue;
			obj.GetType().InvokeMember(sProperty, BindingFlags.SetProperty, null, obj, oParam);
		}

		[System.Security.SecuritySafeCritical()]
		private static object GetProperty(object obj, string sProperty, object oValue)
		{
			object[] oParam = new object[1];
            oParam[0] = oValue;
			return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, null, obj, oParam);
		}

		[System.Security.SecuritySafeCritical()]
		private static object GetProperty(object obj, string sProperty)
		{
			return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, null, obj, null);
		}

		[System.Security.SecuritySafeCritical()]
		private static object InvokeMethod(object obj, string sProperty, object oValue)
		{
			object[] oParam = new object[1];
            oParam[0] = oValue;
			return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, null, obj, oParam);
		}

		[System.Security.SecuritySafeCritical()]
		private static object InvokeMethod(object obj, string sProperty)
		{
			return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, null, obj, null);
		}
    }
}
