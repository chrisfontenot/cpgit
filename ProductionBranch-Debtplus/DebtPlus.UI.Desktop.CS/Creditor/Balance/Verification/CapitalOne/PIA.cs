﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

// ///////////////////////////////////////////////////////////////////////////////
// //                                                                           //
// //     WARNING: INCLUDE THIS FILE OR THE ONE CALLED "ACTIVEX" BUT NOT BOTH   //
// //                                                                           //
// ///////////////////////////////////////////////////////////////////////////////

namespace DebtPlus.UI.Desktop.CS.Creditor.Balance.Verification.CapitalOne
{
    partial class Form1
    {
        private Microsoft.Office.Interop.Excel.ApplicationClass ThisApplication = null;
        private Microsoft.Office.Interop.Excel.WorkbookClass ThisWorkbook = null;
        private Microsoft.Office.Interop.Excel._Worksheet ThisSheet = null;
        private Microsoft.Office.Interop.Excel.Range ThisRange = null;

        private bool OpenApplication()
        {
            ThisApplication = new Microsoft.Office.Interop.Excel.ApplicationClass();
            return true;
        }

        private void CloseApplication()
        {
            if (ThisApplication != null)
            {
                ThisApplication.UserControl = true;
                ThisApplication = null;
            }
        }

        private bool QuitApplication()
        {
            ThisApplication.Quit();
            ThisApplication = null;
            return true;
        }

        private System.Int32 SheetCount()
        {
            return ThisWorkbook.Sheets.Count;
        }

        private bool CreateSheet()
        {
            ThisWorkbook.Sheets.Add();
            return true;
        }

        private bool ActivateSheet()
        {
            ThisSheet.Activate();
            return true;
        }

        private bool GetSheet(object Name)
        {
            try
            {
                ThisSheet = ThisWorkbook.Sheets.Item(Name);
            }

            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                return false;
            }

            return true;
        }

        private void SetVisible(bool Value)
        {
            ThisApplication.Visible = Value;
        }

        private Microsoft.Office.Interop.Excel.Range GetRange(string CellName)
        {
            return ThisApplication.Range(CellName);
        }

        private bool SetValue(ref Microsoft.Office.Interop.Excel.Range Rng, object Value)
        {
            if (Information.IsNumeric(Value))
            {
                Rng.Value = Convert.ToDouble(Value);
            }
            else
            {
                Rng.Value = Value;
            }
            return true;
        }

        private bool OpenWorkbook(string Fname)
        {
            try
            {
                ThisWorkbook = ThisApplication.Workbooks.Open(Fname);
            }

            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool CreateWorkbook()
        {
            ThisWorkbook = ThisApplication.Workbooks.Add;
            return true;
        }

        private bool SaveWorkbook(ref Microsoft.Office.Interop.Excel.WorkbookClass WkBk, string Fname)
        {
            try
            {
                WkBk.SaveAs(Fname);
            }

            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DevExpress.XtraEditors.XtraMessageBox.Show(ex.ToString(), "Error saving workbook", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private string CellValue(Int32 RowID, Int32 ColId)
        {
            // If there is no location then there is no value.
            if (RowID < 0 || ColId < 0)
            {
                return string.Empty;
            }

            // Create a range to hold the desired cell. We just want one cell in the range.
            try
            {
                Microsoft.Office.Interop.Excel.Range rangeValue = ThisSheet.Cells(RowID + 1, ColId + 1);
                if (rangeValue == null)
                {
                    return string.Empty;
                }

                // Return the string value of the cell.
                return Convert.ToString(rangeValue.value);
            }

            catch
            {
                return string.Empty;
            }
        }
    }
}
