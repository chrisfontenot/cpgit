using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Data.Forms;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.PolicyMatrix.Update
{
    internal partial class Form_EditItem : DebtPlusForm
    {
        private DataRowView drv = null;

        private string creditor_label = string.Empty;

        public Form_EditItem() : base()
        {
            InitializeComponent();
            this.Load += Form_EditItem_Load;
        }

        public Form_EditItem(DataRowView drv, string creditor_label) : this()
        {
            this.drv = drv;
            this.creditor_label = creditor_label;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.MemoEdit message;
        internal DevExpress.XtraEditors.CheckEdit CopyToSIC;
        internal DevExpress.XtraEditors.TextEdit value;
        internal DevExpress.XtraEditors.LabelControl label_description;

        internal DevExpress.XtraEditors.LabelControl label_creditor;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.message = new DevExpress.XtraEditors.MemoEdit();
            this.CopyToSIC = new DevExpress.XtraEditors.CheckEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.value = new DevExpress.XtraEditors.TextEdit();
            this.label_description = new DevExpress.XtraEditors.LabelControl();
            this.label_creditor = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CopyToSIC.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.value.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(39, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Creditor";
            //
            //LabelControl2
            //
            this.LabelControl2.Location = new System.Drawing.Point(8, 24);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(53, 13);
            this.LabelControl2.TabIndex = 2;
            this.LabelControl2.Text = "Description";
            //
            //LabelControl3
            //
            this.LabelControl3.Location = new System.Drawing.Point(8, 48);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(26, 13);
            this.LabelControl3.TabIndex = 4;
            this.LabelControl3.Text = "Value";
            //
            //LabelControl4
            //
            this.LabelControl4.Location = new System.Drawing.Point(8, 72);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(23, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Note";
            //
            //message
            //
            this.message.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.message.Location = new System.Drawing.Point(88, 72);
            this.message.Name = "message";
            this.message.Properties.MaxLength = 1024;
            this.message.Size = new System.Drawing.Size(288, 96);
            this.message.TabIndex = 7;
            //
            //CopyToSIC
            //
            this.CopyToSIC.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
            this.CopyToSIC.Location = new System.Drawing.Point(8, 176);
            this.CopyToSIC.Name = "CopyToSIC";
            this.CopyToSIC.Properties.Caption = "Copy to all creditors of the same SIC code?";
            this.CopyToSIC.Size = new System.Drawing.Size(232, 19);
            this.CopyToSIC.TabIndex = 8;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(304, 8);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 9;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(304, 40);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 10;
            this.Button_Cancel.Text = "&Cancel";
            //
            //value
            //
            this.value.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.value.Location = new System.Drawing.Point(88, 48);
            this.value.Name = "value";
            this.value.Properties.MaxLength = 1024;
            this.value.Size = new System.Drawing.Size(200, 20);
            this.value.TabIndex = 5;
            //
            //label_description
            //
            this.label_description.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_description.Location = new System.Drawing.Point(88, 24);
            this.label_description.Name = "label_description";
            this.label_description.Size = new System.Drawing.Size(0, 13);
            this.label_description.TabIndex = 3;
            //
            //label_creditor
            //
            this.label_creditor.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.label_creditor.Location = new System.Drawing.Point(88, 8);
            this.label_creditor.Name = "label_creditor";
            this.label_creditor.Size = new System.Drawing.Size(0, 13);
            this.label_creditor.TabIndex = 1;
            //
            //Form_EditItem
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(392, 206);
            this.Controls.Add(this.label_description);
            this.Controls.Add(this.label_creditor);
            this.Controls.Add(this.value);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.CopyToSIC);
            this.Controls.Add(this.message);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Form_EditItem";
            this.Text = "Policy Matrix - Policy Information";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.message.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CopyToSIC.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.value.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void Form_EditItem_Load(object sender, EventArgs e)
        {
            // Copy the creditor name from the parent
            label_creditor.Text = creditor_label;

            // Bind the controls and let it run
            label_description.DataBindings.Add(new Binding("Text", drv, "description"));
            value.DataBindings.Add(new Binding("EditValue", drv, "item_value"));
            message.DataBindings.Add(new Binding("EditValue", drv, "message"));
            CopyToSIC.DataBindings.Add(new Binding("EditValue", drv, "CopyToSIC"));
        }
    }
}