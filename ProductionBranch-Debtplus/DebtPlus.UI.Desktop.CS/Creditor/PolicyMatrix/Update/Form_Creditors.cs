using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraGrid.Views.Base;
using DebtPlus.Data.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.PolicyMatrix.Update
{
    internal partial class Form_Creditors : DebtPlusForm
    {
        public Form_Creditors() : base()
        {
            InitializeComponent();
        }

        private readonly DataSet _ds;

        public Form_Creditors(DataSet ds) : base()
        {
            InitializeComponent();
            SimpleButton_Cancel.Click += SimpleButton_Cancel_Click;
            SimpleButton_Edit.Click += SimpleButton_Edit_Click;
            GridView1.MouseDown += GridView1_MouseDown;
            this.Load += MyGridControl_Load;

            _ds = ds;

            //Add any initialization after the InitializeComponent() call
            GridColumn_sic.GroupFormat.Format = new CreditorPolicyMatrixFormatter();
            GridColumn_sic.DisplayFormat.Format = new CreditorPolicyMatrixFormatter();
        }

        #region " Windows Form Designer generated code "

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_Edit;

        internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_sic;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_CreditorName;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.SimpleButton_Edit = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_sic = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_sic.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_CreditorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_CreditorName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;

            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();

            //
            //SimpleButton_Edit
            //
            this.SimpleButton_Edit.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.SimpleButton_Edit.Location = new System.Drawing.Point(416, 8);
            this.SimpleButton_Edit.Name = "SimpleButton_Edit";
            this.SimpleButton_Edit.TabIndex = 0;
            this.SimpleButton_Edit.Text = "&Edit...";
            //
            //SimpleButton_Cancel
            //
            this.SimpleButton_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(416, 48);
            this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
            this.SimpleButton_Cancel.TabIndex = 1;
            this.SimpleButton_Cancel.Text = "&Cancel";
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(8, 8);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(400, 256);
            this.GridControl1.TabIndex = 2;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_sic,
                this.GridColumn_creditor,
                this.GridColumn_CreditorName
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_creditor, DevExpress.Data.ColumnSortOrder.Ascending) });
            //
            //GridColumn_sic
            //
            this.GridColumn_sic.Caption = "SIC";
            this.GridColumn_sic.CustomizationCaption = "S.I.C.";
            this.GridColumn_sic.DisplayFormat.FormatString = "A0000-00-0000";
            this.GridColumn_sic.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_sic.FieldName = "sic";
            this.GridColumn_sic.GroupFormat.FormatString = "A0000-00-0000";
            this.GridColumn_sic.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_sic.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical;
            this.GridColumn_sic.Name = "GridColumn_sic";
            this.GridColumn_sic.Visible = true;
            this.GridColumn_sic.VisibleIndex = 0;
            this.GridColumn_sic.Width = 101;
            //
            //GridColumn_creditor
            //
            this.GridColumn_creditor.Caption = "Creditor";
            this.GridColumn_creditor.CustomizationCaption = "Creditor ID";
            this.GridColumn_creditor.FieldName = "creditor";
            this.GridColumn_creditor.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Alphabetical;
            this.GridColumn_creditor.Name = "GridColumn_creditor";
            this.GridColumn_creditor.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_creditor.Visible = true;
            this.GridColumn_creditor.VisibleIndex = 1;
            this.GridColumn_creditor.Width = 92;
            //
            //GridColumn_CreditorName
            //
            this.GridColumn_CreditorName.Caption = "Name";
            this.GridColumn_CreditorName.CustomizationCaption = "Creditor Name";
            this.GridColumn_CreditorName.FieldName = "creditor_name";
            this.GridColumn_CreditorName.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Value;
            this.GridColumn_CreditorName.Name = "GridColumn_CreditorName";
            this.GridColumn_CreditorName.Visible = true;
            this.GridColumn_CreditorName.VisibleIndex = 2;
            this.GridColumn_CreditorName.Width = 203;
            //
            //Form_Creditors
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(504, 266);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.SimpleButton_Cancel);
            this.Controls.Add(this.SimpleButton_Edit);
            this.Name = "Form_Creditors";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Policy Matrix - Creditor Listing";

            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void SimpleButton_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SimpleButton_Edit_Click(object sender, EventArgs e)
        {
            DataRowView drv = null;

            if (GridView1.FocusedRowHandle >= 0)
            {
                DataView vue = (DataView)GridControl1.DataSource;
                drv = (DataRowView)GridView1.GetRow(GridView1.FocusedRowHandle);
            }

            // If there is a row then perform the edit operation. If successful, complete the changes else roll them back
            if (drv != null)
                EditItem(drv);
        }

        /// <summary>
        /// Process a change in the current row for the control
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView gv = (GridView)sender;
            GridControl ctl = gv.GridControl;
            Int32 ControlRow = e.FocusedRowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (DataRowView)gv.GetRow(ControlRow);
            }
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targetted as the double-click item
            GridView gv = (GridView)sender;
            GridControl ctl = (GridControl)gv.GridControl;
            GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(MousePosition)));
            Int32 ControlRow = hi.RowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (DataRowView)gv.GetRow(ControlRow);
            }

            // If there is a row then perform the edit operation. If successful, complete the changes else roll them back
            if (drv != null)
                EditItem(drv);
        }

        private void GridView1_MouseDown(object sender, MouseEventArgs e)
        {
            //Dim gv As DevExpress.XtraGrid.Views.Grid.GridView = GridView1
            //Dim ctl As DevExpress.XtraGrid.GridControl = CType(gv.GridControl, DevExpress.XtraGrid.GridControl)
            //Dim hi As DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo = gv.CalcHitInfo(New Point(e.X, e.Y))
        }

        private void MyGridControl_Load(object sender, EventArgs e)
        {
            // Define the form information
            SqlCommand cmd = new SqlCommand();
            var _with1 = cmd;
            _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with1.CommandText = "SELECT creditor_id, creditor, sic, creditor_name FROM creditors WITH (NOLOCK)";

            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(_ds, "creditors");

                var _with2 = GridControl1;
                _with2.DataSource = _ds.Tables["creditors"].DefaultView;
                _with2.RefreshDataSource();

                // Disable the edit operation if there are no creditors to edit???
                if (_ds.Tables["creditors"].Rows.Count <= 0)
                    this.SimpleButton_Edit.Enabled = false;

                var _with3 = GridView1;
                _with3.BestFitColumns();
                _with3.FocusedRowHandle = -1;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor list");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // Add the handlers for the events
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
        }

        private void EditItem(DataRowView drv)
        {
            var _with4 = new Form_PolicyMatrix(_ds, drv);
            _with4.ShowDialog();
            _with4.Dispose();
        }

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
    }
}