using System;

using System.Text.RegularExpressions;

namespace DebtPlus.UI.Desktop.CS.Creditor.PolicyMatrix.Update
{
    public partial class CreditorPolicyMatrixFormatter : object, ICustomFormatter, IFormatProvider
    {
        public CreditorPolicyMatrixFormatter() : base()
        {
        }

        public string Format(string FormatCode, object arg, IFormatProvider formatProvider)
        {
            if (arg == null || object.ReferenceEquals(arg, DBNull.Value))
                return string.Empty;
            string argString = Convert.ToString(arg);

            // If the value is un-formatted then format it.
            Regex rx = new Regex("^[A-Z]\\d{10}$", RegexOptions.Singleline | RegexOptions.IgnoreCase);
            if (rx.IsMatch(argString))
            {
                argString = argString.Substring(0, 5) + "-" + argString.Substring(5, 2) + "-" + argString.Substring(7, 4);
            }

            return argString;
        }

        public object GetFormat(Type formatType)
        {
            return this;
        }
    }
}