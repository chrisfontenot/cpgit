using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using DevExpress.XtraGrid.Views.Base;
using DebtPlus.Data.Forms;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.PolicyMatrix.Update
{
    internal partial class Form_PolicyMatrix : DebtPlusForm
    {
        public Form_PolicyMatrix() : base()
        {
            InitializeComponent();
        }

        private readonly DataSet _ds;
        private readonly DataRowView _drv = null;

        private const string TableName = "policy_matrix_policies";

        /// <summary>
        /// Find the creditor that we are editing
        /// </summary>
        private string creditor
        {
            get
            {
                if (_drv == null || _drv["creditor"] == null || object.ReferenceEquals(_drv["creditor"], DBNull.Value))
                    return string.Empty;
                return Convert.ToString(_drv["creditor"]).Trim();
            }
        }

        /// <summary>
        /// Find the creditor name that we are editing
        /// </summary>
        private string creditor_name
        {
            get
            {
                string result = string.Empty;
                if (_drv != null && _drv["creditor_name"] != null && !object.ReferenceEquals(_drv["creditor_name"], DBNull.Value))
                    result = Convert.ToString(_drv["creditor_name"]).Trim();
                return result;
            }
        }

        /// <summary>
        /// Value to be used for the editing form as the creditor
        /// </summary>
        private string creditor_label
        {
            get
            {
                string lbl_creditor = creditor;
                string lbl_name = creditor_name;
                if (lbl_creditor != string.Empty && lbl_name != string.Empty)
                    return "[" + lbl_creditor + "] " + lbl_name;
                return lbl_creditor + lbl_name;
            }
        }

        public Form_PolicyMatrix(DataSet ds, DataRowView drv) : base()
        {
            InitializeComponent();
            this.Load += Form_PolicyMatrix_Load;
            base.Closing += Form_PolicyMatrix_Closing;
            SimpleButton_Edit.Click += SimpleButton_Edit_Click;
            _drv = drv;
            _ds = ds;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraGrid.GridControl GridControl1;

        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_Edit;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_policy_matrix_policy;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Creditor;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_policy_matrix_policy_type;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_changed_date;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_item_value;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_message;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_formatted_policy_matrix_policy_type;

        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_description;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_item_value = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_item_value.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_message = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_message.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_policy_matrix_policy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_policy_matrix_policy.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Creditor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_policy_matrix_policy_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_policy_matrix_policy_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_changed_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_changed_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_formatted_policy_matrix_policy_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_formatted_policy_matrix_policy_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.SimpleButton_Edit = new DevExpress.XtraEditors.SimpleButton();

            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();

            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(8, 8);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(400, 256);
            this.GridControl1.TabIndex = 5;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_description,
                this.GridColumn_item_value,
                this.GridColumn_message,
                this.GridColumn_policy_matrix_policy,
                this.GridColumn_Creditor,
                this.GridColumn_policy_matrix_policy_type,
                this.GridColumn_changed_date,
                this.GridColumn_date_created,
                this.GridColumn_created_by,
                this.GridColumn_formatted_policy_matrix_policy_type
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //GridColumn_description
            //
            this.GridColumn_description.Caption = "Policy";
            this.GridColumn_description.CustomizationCaption = "Policy";
            this.GridColumn_description.FieldName = "description";
            this.GridColumn_description.Name = "GridColumn_description";
            this.GridColumn_description.Visible = true;
            this.GridColumn_description.VisibleIndex = 0;
            this.GridColumn_description.Width = 148;
            //
            //GridColumn_item_value
            //
            this.GridColumn_item_value.Caption = "Value";
            this.GridColumn_item_value.CustomizationCaption = "Value";
            this.GridColumn_item_value.FieldName = "item_value";
            this.GridColumn_item_value.Name = "GridColumn_item_value";
            this.GridColumn_item_value.Visible = true;
            this.GridColumn_item_value.VisibleIndex = 1;
            this.GridColumn_item_value.Width = 67;
            //
            //GridColumn_message
            //
            this.GridColumn_message.Caption = "Note";
            this.GridColumn_message.CustomizationCaption = "Note";
            this.GridColumn_message.FieldName = "message";
            this.GridColumn_message.Name = "GridColumn_message";
            this.GridColumn_message.Visible = true;
            this.GridColumn_message.VisibleIndex = 2;
            this.GridColumn_message.Width = 181;
            //
            //GridColumn_policy_matrix_policy
            //
            this.GridColumn_policy_matrix_policy.Caption = "policy_matrix_policy";
            this.GridColumn_policy_matrix_policy.CustomizationCaption = "policy_matrix_policy";
            this.GridColumn_policy_matrix_policy.FieldName = "policy_matrix_policy";
            this.GridColumn_policy_matrix_policy.Name = "GridColumn_policy_matrix_policy";
            //
            //GridColumn_Creditor
            //
            this.GridColumn_Creditor.Caption = "Creditor";
            this.GridColumn_Creditor.CustomizationCaption = "Creditor";
            this.GridColumn_Creditor.FieldName = "creditor";
            this.GridColumn_Creditor.Name = "GridColumn_Creditor";
            //
            //GridColumn_policy_matrix_policy_type
            //
            this.GridColumn_policy_matrix_policy_type.Caption = "policy_matrix_policy_type";
            this.GridColumn_policy_matrix_policy_type.CustomizationCaption = "policy_matrix_policy_type";
            this.GridColumn_policy_matrix_policy_type.FieldName = "policy_matrix_policy_type";
            this.GridColumn_policy_matrix_policy_type.Name = "GridColumn_policy_matrix_policy_type";
            //
            //GridColumn_changed_date
            //
            this.GridColumn_changed_date.Caption = "Date Changed";
            this.GridColumn_changed_date.CustomizationCaption = "Date Changed";
            this.GridColumn_changed_date.FieldName = "changed_date";
            this.GridColumn_changed_date.Name = "GridColumn_changed_date";
            //
            //GridColumn_date_created
            //
            this.GridColumn_date_created.Caption = "Date Created";
            this.GridColumn_date_created.CustomizationCaption = "Date Created";
            this.GridColumn_date_created.FieldName = "date_created";
            this.GridColumn_date_created.Name = "GridColumn_date_created";
            //
            //GridColumn_created_by
            //
            this.GridColumn_created_by.Caption = "Created By";
            this.GridColumn_created_by.CustomizationCaption = "Created By";
            this.GridColumn_created_by.FieldName = "created_by";
            this.GridColumn_created_by.Name = "GridColumn_created_by";
            //
            //GridColumn_formatted_policy_matrix_policy_type
            //
            this.GridColumn_formatted_policy_matrix_policy_type.Caption = "formatted_policy_matrix_policy_type";
            this.GridColumn_formatted_policy_matrix_policy_type.CustomizationCaption = "formatted_policy_matrix_policy_type";
            this.GridColumn_formatted_policy_matrix_policy_type.FieldName = "formatted_policy_matrix_policy_type";
            this.GridColumn_formatted_policy_matrix_policy_type.Name = "GridColumn_formatted_policy_matrix_policy_type";
            //
            //SimpleButton_Cancel
            //
            this.SimpleButton_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.SimpleButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SimpleButton_Cancel.Location = new System.Drawing.Point(416, 48);
            this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
            this.SimpleButton_Cancel.TabIndex = 4;
            this.SimpleButton_Cancel.Text = "&Cancel";
            //
            //SimpleButton_Edit
            //
            this.SimpleButton_Edit.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.SimpleButton_Edit.Location = new System.Drawing.Point(416, 8);
            this.SimpleButton_Edit.Name = "SimpleButton_Edit";
            this.SimpleButton_Edit.TabIndex = 3;
            this.SimpleButton_Edit.Text = "&Edit...";
            //
            //Form_PolicyMatrix
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.SimpleButton_Cancel;
            this.ClientSize = new System.Drawing.Size(504, 270);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.SimpleButton_Cancel);
            this.Controls.Add(this.SimpleButton_Edit);
            this.Name = "Form_PolicyMatrix";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Policy Matrix - Policy Values";

            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void Form_PolicyMatrix_Load(object sender, EventArgs e)
        {
            // Replace our title with the creditor name if given
            if (creditor_name != string.Empty)
                this.Text = creditor_name + " Policy Matrix";

            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                DataTable tbl = _ds.Tables[TableName];
                if (tbl != null)
                    tbl.Clear();

                // Load the list of policies for this creditor
                using (SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with1.CommandText = "SELECT p.policy_matrix_policy, p.creditor, p.policy_matrix_policy_type, p.changed_date, p.item_value, p.message, p.date_created, p.created_by, t.policy_matrix_policy_type as formatted_policy_matrix_policy_type, t.description, convert(bit,0) AS CopyToSIC FROM policy_matrix_policy_types t WITH (NOLOCK) LEFT OUTER JOIN policy_matrix_policies p WITH (NOLOCK) ON t.policy_matrix_policy_type = p.policy_matrix_policy_type AND p.creditor = @creditor";
                    _with1.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(_ds, TableName);
                    }
                }

                tbl = _ds.Tables[TableName];

                var _with2 = GridControl1;
                _with2.DataSource = tbl.DefaultView;
                _with2.RefreshDataSource();

                var _with3 = GridView1;
                _with3.BestFitColumns();

                // Disable the edit operation if there are no policies to edit???
                if (tbl.Rows.Count <= 0)
                    this.SimpleButton_Edit.Enabled = false;
            }
            catch (Exception ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading policy_matrix tables");
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            // Add the handlers for the events
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
        }

        /// <summary>
        /// Process a change in the current trustRegister for the control
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            GridView gv = (GridView)sender;
            GridControl ctl = gv.GridControl;
            Int32 controlRow = e.FocusedRowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (controlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (DataRowView)gv.GetRow(controlRow);
            }
        }

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the trustRegister targetted as the double-click item
            GridView gv = (GridView)sender;
            GridControl ctl = (GridControl)gv.GridControl;
            GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(MousePosition)));
            Int32 controlRow = hi.RowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (controlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (DataRowView)gv.GetRow(controlRow);
            }

            // If there is a trustRegister then perform the edit operation. If successful, complete the changes else roll them back
            if (drv != null)
                EditItem(drv);
        }

        private void EditItem(DataRowView drv)
        {
            drv.BeginEdit();
            using (Form_EditItem frm = new Form_EditItem(drv, creditor_label))
            {
                if (frm.ShowDialog() == DialogResult.OK)
                {
                    drv.EndEdit();
                }
                else
                {
                    drv.CancelEdit();
                }
            }
        }

        /// <summary>
        /// Update the table of policy matrix values when the form is closed
        /// </summary>
        private void Form_PolicyMatrix_Closing(object sender, CancelEventArgs e)
        {
            DataTable tbl = _ds.Tables[TableName];

            // If there is a table to update then build the update command.
            // We don't have an insert nor delete since we don't do these operations.

            if (tbl != null)
            {
                // Do the update operation for any rows that are changed
                try
                {
                    using (global::DebtPlus.UI.Common.CursorManager cur = new global::DebtPlus.UI.Common.CursorManager())
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            var _with4 = cmd;
                            _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            _with4.CommandText = "xpr_insert_policy_matrix_policies";
                            _with4.CommandType = CommandType.StoredProcedure;
                            _with4.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;
                            _with4.Parameters.Add("@policy_matrix_policy_type", SqlDbType.VarChar, 50, "formatted_policy_matrix_policy_type");
                            _with4.Parameters.Add("@item_value", SqlDbType.VarChar, 1024, "item_value");
                            _with4.Parameters.Add("@message", SqlDbType.VarChar, 1024, "message");
                            _with4.Parameters.Add("@use_sic", SqlDbType.Bit, 0, "CopyToSIC");

                            using (SqlDataAdapter da = new SqlDataAdapter())
                            {
                                da.UpdateCommand = cmd;
                                da.Update(tbl);
                            }
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating creditor policy matrix");
                }
            }
        }

        private void SimpleButton_Edit_Click(object sender, EventArgs e)
        {
            DataRowView drv = null;

            if (GridView1.FocusedRowHandle >= 0)
            {
                drv = (DataRowView)GridView1.GetRow(GridView1.FocusedRowHandle);
            }

            // If there is a trustRegister then perform the edit operation. If successful, complete the changes else roll them back
            if (drv != null)
                EditItem(drv);
        }
    }
}