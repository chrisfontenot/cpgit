using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing
{
	partial class FormMain : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.Check_Annual = new DevExpress.XtraEditors.CheckEdit();
			this.Check_SemiAnnual = new DevExpress.XtraEditors.CheckEdit();
			this.Check_Quarterly = new DevExpress.XtraEditors.CheckEdit();
			this.Check_Monthly = new DevExpress.XtraEditors.CheckEdit();
			this.Check_Weekly = new DevExpress.XtraEditors.CheckEdit();
			this.Check_Ignore = new DevExpress.XtraEditors.CheckEdit();
			this.Period_Annually = new DevExpress.XtraEditors.TextEdit();
			this.Period_SemiAnnually = new DevExpress.XtraEditors.TextEdit();
			this.Period_Quarterly = new DevExpress.XtraEditors.TextEdit();
			this.Period_Monthly = new DevExpress.XtraEditors.TextEdit();
			this.Period_Weekly = new DevExpress.XtraEditors.TextEdit();
			this.Label1 = new DevExpress.XtraEditors.LabelControl();
			this.GroupBox1 = new System.Windows.Forms.GroupBox();
			this.GroupBox2 = new System.Windows.Forms.GroupBox();
			this.Label3 = new DevExpress.XtraEditors.LabelControl();
			this.Button_Print = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();

			((System.ComponentModel.ISupportInitialize)this.Check_Annual.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Check_SemiAnnual.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Quarterly.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Monthly.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Weekly.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Ignore.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Annually.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Period_SemiAnnually.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Quarterly.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Monthly.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Weekly.Properties).BeginInit();
			this.SuspendLayout();

			//
			//Check_Annual
			//
			this.Check_Annual.Location = new System.Drawing.Point(16, 52);
			this.Check_Annual.Name = "Check_Annual";
			this.Check_Annual.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Check_Annual.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Check_Annual.Properties.Appearance.Options.UseBackColor = true;
			this.Check_Annual.Properties.Appearance.Options.UseForeColor = true;
			this.Check_Annual.Properties.Caption = "Generate invoices for creditors billed annually";
			this.Check_Annual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5;
			this.Check_Annual.Size = new System.Drawing.Size(328, 22);

			this.Check_Annual.TabIndex = 0;
			this.Check_Annual.ToolTip = "Generate creditor invoices for transactions that are billed ANNUALLY";
			this.Check_Annual.ToolTipController = this.ToolTipController1;
			//
			//Check_SemiAnnual
			//
			this.Check_SemiAnnual.Location = new System.Drawing.Point(16, 78);
			this.Check_SemiAnnual.Name = "Check_SemiAnnual";
			this.Check_SemiAnnual.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Check_SemiAnnual.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Check_SemiAnnual.Properties.Appearance.Options.UseBackColor = true;
			this.Check_SemiAnnual.Properties.Appearance.Options.UseForeColor = true;
			this.Check_SemiAnnual.Properties.Caption = "Generate invoices for creditors billed semi-annually";
			this.Check_SemiAnnual.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5;
			this.Check_SemiAnnual.Size = new System.Drawing.Size(328, 22);

			this.Check_SemiAnnual.TabIndex = 1;
			this.Check_SemiAnnual.ToolTip = "Generate creditor invoices for transactions that are billed SEMI-ANNUALLY";
			this.Check_SemiAnnual.ToolTipController = this.ToolTipController1;
			//
			//Check_Quarterly
			//
			this.Check_Quarterly.Location = new System.Drawing.Point(16, 103);
			this.Check_Quarterly.Name = "Check_Quarterly";
			this.Check_Quarterly.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Check_Quarterly.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Check_Quarterly.Properties.Appearance.Options.UseBackColor = true;
			this.Check_Quarterly.Properties.Appearance.Options.UseForeColor = true;
			this.Check_Quarterly.Properties.Caption = "Generate invoices for creditors billed quarterly";
			this.Check_Quarterly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5;
			this.Check_Quarterly.Size = new System.Drawing.Size(328, 22);

			this.Check_Quarterly.TabIndex = 2;
			this.Check_Quarterly.ToolTip = "Generate creditor invoices for transactions that are billed QUARTERLY";
			this.Check_Quarterly.ToolTipController = this.ToolTipController1;
			//
			//Check_Monthly
			//
			this.Check_Monthly.Location = new System.Drawing.Point(16, 129);
			this.Check_Monthly.Name = "Check_Monthly";
			this.Check_Monthly.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Check_Monthly.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Check_Monthly.Properties.Appearance.Options.UseBackColor = true;
			this.Check_Monthly.Properties.Appearance.Options.UseForeColor = true;
			this.Check_Monthly.Properties.Caption = "Generate invoices for creditors billed monthly";
			this.Check_Monthly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5;
			this.Check_Monthly.Size = new System.Drawing.Size(328, 22);

			this.Check_Monthly.TabIndex = 3;
			this.Check_Monthly.ToolTip = "Generate creditor invoices for transactions that are billed MONTHLY";
			this.Check_Monthly.ToolTipController = this.ToolTipController1;
			//
			//Check_Weekly
			//
			this.Check_Weekly.Location = new System.Drawing.Point(16, 155);
			this.Check_Weekly.Name = "Check_Weekly";
			this.Check_Weekly.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Check_Weekly.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Check_Weekly.Properties.Appearance.Options.UseBackColor = true;
			this.Check_Weekly.Properties.Appearance.Options.UseForeColor = true;
			this.Check_Weekly.Properties.Caption = "Generate invoices for creditors billed weekly";
			this.Check_Weekly.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5;
			this.Check_Weekly.Size = new System.Drawing.Size(328, 22);

			this.Check_Weekly.TabIndex = 4;
			this.Check_Weekly.ToolTip = "Generate creditor invoices for transactions that are billed WEEKLY";
			this.Check_Weekly.ToolTipController = this.ToolTipController1;
			//
			//Check_Ignore
			//
			this.Check_Ignore.Location = new System.Drawing.Point(16, 198);
			this.Check_Ignore.Name = "Check_Ignore";
			this.Check_Ignore.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.Check_Ignore.Properties.Appearance.ForeColor = System.Drawing.SystemColors.WindowText;
			this.Check_Ignore.Properties.Appearance.Options.UseBackColor = true;
			this.Check_Ignore.Properties.Appearance.Options.UseForeColor = true;
			this.Check_Ignore.Properties.Caption = "Ignore creditor minimum amount to be billed and generate bill";
			this.Check_Ignore.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style5;
			this.Check_Ignore.Size = new System.Drawing.Size(384, 22);

			this.Check_Ignore.TabIndex = 5;
			this.Check_Ignore.ToolTip = "At the end of the year, do you want to override the minimum bill amount and gener" + "ate the invoices for the current year?";
			this.Check_Ignore.ToolTipController = this.ToolTipController1;
			//
			//Period_Annually
			//
			this.Period_Annually.Location = new System.Drawing.Point(352, 52);
			this.Period_Annually.Name = "Period_Annually";
			this.Period_Annually.Properties.Appearance.Options.UseTextOptions = true;
			this.Period_Annually.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Period_Annually.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
			this.Period_Annually.Size = new System.Drawing.Size(48, 22);

			this.Period_Annually.TabIndex = 6;
			this.Period_Annually.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" + "UTION: Do not enter a value here unless you wish to generate only specific credi" + "tor's invoices.";
			this.Period_Annually.ToolTipController = this.ToolTipController1;
			//
			//Period_SemiAnnually
			//
			this.Period_SemiAnnually.Location = new System.Drawing.Point(352, 78);
			this.Period_SemiAnnually.Name = "Period_SemiAnnually";
			this.Period_SemiAnnually.Properties.Appearance.Options.UseTextOptions = true;
			this.Period_SemiAnnually.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Period_SemiAnnually.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
			this.Period_SemiAnnually.Size = new System.Drawing.Size(48, 22);

			this.Period_SemiAnnually.TabIndex = 7;
			this.Period_SemiAnnually.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" + "UTION: Do not enter a value here unless you wish to generate only specific credi" + "tor's invoices.";
			this.Period_SemiAnnually.ToolTipController = this.ToolTipController1;
			//
			//Period_Quarterly
			//
			this.Period_Quarterly.Location = new System.Drawing.Point(352, 103);
			this.Period_Quarterly.Name = "Period_Quarterly";
			this.Period_Quarterly.Properties.Appearance.Options.UseTextOptions = true;
			this.Period_Quarterly.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Period_Quarterly.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
			this.Period_Quarterly.Size = new System.Drawing.Size(48, 22);

			this.Period_Quarterly.TabIndex = 8;
			this.Period_Quarterly.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" + "UTION: Do not enter a value here unless you wish to generate only specific credi" + "tor's invoices.";
			this.Period_Quarterly.ToolTipController = this.ToolTipController1;
			//
			//Period_Monthly
			//
			this.Period_Monthly.Location = new System.Drawing.Point(352, 129);
			this.Period_Monthly.Name = "Period_Monthly";
			this.Period_Monthly.Properties.Appearance.Options.UseTextOptions = true;
			this.Period_Monthly.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Period_Monthly.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
			this.Period_Monthly.Size = new System.Drawing.Size(48, 22);

			this.Period_Monthly.TabIndex = 9;
			this.Period_Monthly.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" + "UTION: Do not enter a value here unless you wish to generate only specific credi" + "tor's invoices.";
			this.Period_Monthly.ToolTipController = this.ToolTipController1;
			//
			//Period_Weekly
			//
			this.Period_Weekly.Location = new System.Drawing.Point(352, 155);
			this.Period_Weekly.Name = "Period_Weekly";
			this.Period_Weekly.Properties.Appearance.Options.UseTextOptions = true;
			this.Period_Weekly.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Period_Weekly.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Style3D;
			this.Period_Weekly.Size = new System.Drawing.Size(48, 22);

			this.Period_Weekly.TabIndex = 10;
			this.Period_Weekly.ToolTip = "Restrict the generation of invoices to only creditors with this billing month. CA" + "UTION: Do not enter a value here unless you wish to generate only specific credi" + "tor's invoices.";
			this.Period_Weekly.ToolTipController = this.ToolTipController1;
			//
			//Label1
			//
			this.Label1.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Label1.Appearance.Options.UseFont = true;
			this.Label1.Appearance.Options.UseTextOptions = true;
			this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
			this.Label1.Location = new System.Drawing.Point(16, 9);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(437, 13);

			this.Label1.TabIndex = 11;
			this.Label1.Text = "Select the billing periods to generate invoices for creditors in the billing tabl" + "e";
			this.Label1.ToolTipController = this.ToolTipController1;
			this.Label1.UseMnemonic = false;
			//
			//GroupBox1
			//
			this.GroupBox1.Location = new System.Drawing.Point(16, 181);
			this.GroupBox1.Name = "GroupBox1";
			this.GroupBox1.Size = new System.Drawing.Size(384, 9);
			this.ToolTipController1.SetSuperTip(this.GroupBox1, null);
			this.GroupBox1.TabIndex = 12;
			this.GroupBox1.TabStop = false;
			//
			//GroupBox2
			//
			this.GroupBox2.Location = new System.Drawing.Point(16, 224);
			this.GroupBox2.Name = "GroupBox2";
			this.GroupBox2.Size = new System.Drawing.Size(384, 9);
			this.ToolTipController1.SetSuperTip(this.GroupBox2, null);
			this.GroupBox2.TabIndex = 13;
			this.GroupBox2.TabStop = false;
			//
			//Label3
			//
			this.Label3.Appearance.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Label3.Appearance.Options.UseFont = true;
			this.Label3.Appearance.Options.UseTextOptions = true;
			this.Label3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Label3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
			this.Label3.Location = new System.Drawing.Point(352, 28);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(48, 13);

			this.Label3.TabIndex = 15;
			this.Label3.Text = "Period #";
			this.Label3.ToolTipController = this.ToolTipController1;
			this.Label3.UseMnemonic = false;
			//
			//Button_Print
			//
			this.Button_Print.Location = new System.Drawing.Point(123, 250);
			this.Button_Print.Name = "Button_Print";
			this.Button_Print.Size = new System.Drawing.Size(75, 25);

			this.Button_Print.TabIndex = 16;
			this.Button_Print.Text = "&Print...";
			this.Button_Print.ToolTip = "Generate and print the invoices";
			this.Button_Print.ToolTipController = this.ToolTipController1;
			//
			//Button_Cancel
			//
			this.Button_Cancel.Location = new System.Drawing.Point(219, 250);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 25);

			this.Button_Cancel.TabIndex = 17;
			this.Button_Cancel.Text = "&Cancel";
			this.Button_Cancel.ToolTip = "Generate and print the invoices";
			this.Button_Cancel.ToolTipController = this.ToolTipController1;
			//
			//FormMain
			//
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(464, 292);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_Print);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.GroupBox2);
			this.Controls.Add(this.GroupBox1);
			this.Controls.Add(this.Label1);
			this.Controls.Add(this.Period_Weekly);
			this.Controls.Add(this.Period_Monthly);
			this.Controls.Add(this.Period_Quarterly);
			this.Controls.Add(this.Period_SemiAnnually);
			this.Controls.Add(this.Period_Annually);
			this.Controls.Add(this.Check_Ignore);
			this.Controls.Add(this.Check_Weekly);
			this.Controls.Add(this.Check_Monthly);
			this.Controls.Add(this.Check_Quarterly);
			this.Controls.Add(this.Check_SemiAnnual);
			this.Controls.Add(this.Check_Annual);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.Name = "FormMain";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Select Creditor Billing Periods to Generate Invoices";

			((System.ComponentModel.ISupportInitialize)this.Check_Annual.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Check_SemiAnnual.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Quarterly.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Monthly.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Weekly.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Check_Ignore.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Annually.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Period_SemiAnnually.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Quarterly.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Monthly.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.Period_Weekly.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		protected internal DevExpress.XtraEditors.LabelControl Label1;
		protected internal System.Windows.Forms.GroupBox GroupBox1;
		protected internal System.Windows.Forms.GroupBox GroupBox2;
		protected internal DevExpress.XtraEditors.LabelControl Label3;
		protected internal DevExpress.XtraEditors.CheckEdit Check_Annual;
		protected internal DevExpress.XtraEditors.CheckEdit Check_SemiAnnual;
		protected internal DevExpress.XtraEditors.CheckEdit Check_Quarterly;
		protected internal DevExpress.XtraEditors.CheckEdit Check_Monthly;
		protected internal DevExpress.XtraEditors.CheckEdit Check_Weekly;
		protected internal DevExpress.XtraEditors.CheckEdit Check_Ignore;
		protected internal DevExpress.XtraEditors.TextEdit Period_Annually;
		protected internal DevExpress.XtraEditors.TextEdit Period_SemiAnnually;
		protected internal DevExpress.XtraEditors.TextEdit Period_Quarterly;
		protected internal DevExpress.XtraEditors.TextEdit Period_Monthly;
		protected internal DevExpress.XtraEditors.TextEdit Period_Weekly;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Print;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
	}
}
