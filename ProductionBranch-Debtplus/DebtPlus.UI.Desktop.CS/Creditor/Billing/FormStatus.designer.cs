using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.Creditor.Billing
{
	partial class FormStatus : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		protected internal System.Windows.Forms.Timer Timer1;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		protected internal DevExpress.XtraEditors.LabelControl Label_ElapsedTime;
		protected internal DevExpress.XtraEditors.LabelControl Label_Amount;
		protected internal DevExpress.XtraEditors.LabelControl Label_Invoice;
		protected internal DevExpress.XtraEditors.LabelControl Label_Creditor;
		protected internal DevExpress.XtraEditors.LabelControl label_Caption;
		protected internal DevExpress.XtraEditors.LabelControl Label4;
		protected internal DevExpress.XtraEditors.LabelControl Label3;
		protected internal DevExpress.XtraEditors.LabelControl Label2;
		protected internal DevExpress.XtraEditors.LabelControl Label1;
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStatus));
			this.Timer1 = new System.Windows.Forms.Timer(this.components);
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.Label_ElapsedTime = new DevExpress.XtraEditors.LabelControl();
			this.Label_Amount = new DevExpress.XtraEditors.LabelControl();
			this.Label_Invoice = new DevExpress.XtraEditors.LabelControl();
			this.Label_Creditor = new DevExpress.XtraEditors.LabelControl();
			this.label_Caption = new DevExpress.XtraEditors.LabelControl();
			this.Label4 = new DevExpress.XtraEditors.LabelControl();
			this.Label3 = new DevExpress.XtraEditors.LabelControl();
			this.Label2 = new DevExpress.XtraEditors.LabelControl();
			this.Label1 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//Timer1
			//
			//
			//Button_Cancel
			//
			this.Button_Cancel.Location = new System.Drawing.Point(116, 144);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(80, 26);
			this.Button_Cancel.TabIndex = 24;
			this.Button_Cancel.Text = "&Cancel";
			this.Button_Cancel.ToolTip = "Cancel the invoice creation at the next most convienient point";
			this.Button_Cancel.ToolTipController = this.ToolTipController1;
			//
			//Label_ElapsedTime
			//
			this.Label_ElapsedTime.Appearance.Options.UseTextOptions = true;
			this.Label_ElapsedTime.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Label_ElapsedTime.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label_ElapsedTime.Location = new System.Drawing.Point(148, 103);
			this.Label_ElapsedTime.Name = "Label_ElapsedTime";
			this.Label_ElapsedTime.Size = new System.Drawing.Size(112, 13);
			this.Label_ElapsedTime.TabIndex = 23;
			this.Label_ElapsedTime.ToolTipController = this.ToolTipController1;
			this.Label_ElapsedTime.UseMnemonic = false;
			//
			//Label_Amount
			//
			this.Label_Amount.Appearance.Options.UseTextOptions = true;
			this.Label_Amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Label_Amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label_Amount.Location = new System.Drawing.Point(148, 77);
			this.Label_Amount.Name = "Label_Amount";
			this.Label_Amount.Size = new System.Drawing.Size(112, 13);
			this.Label_Amount.TabIndex = 22;
			this.Label_Amount.Text = "$0.00";
			this.Label_Amount.ToolTipController = this.ToolTipController1;
			this.Label_Amount.UseMnemonic = false;
			//
			//Label_Invoice
			//
			this.Label_Invoice.Appearance.Options.UseTextOptions = true;
			this.Label_Invoice.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Label_Invoice.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label_Invoice.Location = new System.Drawing.Point(148, 90);
			this.Label_Invoice.Name = "Label_Invoice";
			this.Label_Invoice.Size = new System.Drawing.Size(112, 13);
			this.Label_Invoice.TabIndex = 21;
			this.Label_Invoice.Text = "00000000";
			this.Label_Invoice.ToolTipController = this.ToolTipController1;
			this.Label_Invoice.UseMnemonic = false;
			//
			//Label_Creditor
			//
			this.Label_Creditor.Appearance.Options.UseTextOptions = true;
			this.Label_Creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Label_Creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Label_Creditor.Location = new System.Drawing.Point(148, 64);
			this.Label_Creditor.Name = "Label_Creditor";
			this.Label_Creditor.Size = new System.Drawing.Size(112, 13);
			this.Label_Creditor.TabIndex = 20;
			this.Label_Creditor.ToolTipController = this.ToolTipController1;
			this.Label_Creditor.UseMnemonic = false;
			//
			//label_Caption
			//
			this.label_Caption.Appearance.Options.UseTextOptions = true;
			this.label_Caption.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.label_Caption.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.label_Caption.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.label_Caption.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.label_Caption.Location = new System.Drawing.Point(16, 12);
			this.label_Caption.Name = "label_Caption";
			this.label_Caption.Size = new System.Drawing.Size(284, 46);
			this.label_Caption.TabIndex = 19;
			this.label_Caption.Text = "The database connections are being established. Please wait for the operation to " + "be completed since it may not be cancelled until the process actually starts.";
			this.label_Caption.ToolTipController = this.ToolTipController1;
			this.label_Caption.UseMnemonic = false;
			//
			//Label4
			//
			this.Label4.Location = new System.Drawing.Point(16, 103);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(66, 13);
			this.Label4.TabIndex = 18;
			this.Label4.Text = "Elapsed Time:";
			this.Label4.ToolTipController = this.ToolTipController1;
			this.Label4.UseMnemonic = false;
			//
			//Label3
			//
			this.Label3.Location = new System.Drawing.Point(16, 77);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(79, 13);
			this.Label3.TabIndex = 17;
			this.Label3.Text = "Invoice Amount:";
			this.Label3.ToolTipController = this.ToolTipController1;
			this.Label3.UseMnemonic = false;
			//
			//Label2
			//
			this.Label2.Location = new System.Drawing.Point(16, 90);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(93, 13);
			this.Label2.TabIndex = 16;
			this.Label2.Text = "Processing Invoice:";
			this.Label2.ToolTipController = this.ToolTipController1;
			this.Label2.UseMnemonic = false;
			//
			//Label1
			//
			this.Label1.Location = new System.Drawing.Point(16, 64);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(97, 13);
			this.Label1.TabIndex = 15;
			this.Label1.Text = "Processing Creditor:";
			this.Label1.ToolTipController = this.ToolTipController1;
			this.Label1.UseMnemonic = false;
			//
			//FormStatus
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.ClientSize = new System.Drawing.Size(312, 182);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Label_ElapsedTime);
			this.Controls.Add(this.Label_Amount);
			this.Controls.Add(this.Label_Invoice);
			this.Controls.Add(this.Label_Creditor);
			this.Controls.Add(this.label_Caption);
			this.Controls.Add(this.Label4);
			this.Controls.Add(this.Label3);
			this.Controls.Add(this.Label2);
			this.Controls.Add(this.Label1);
			this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			this.MaximizeBox = false;
			this.MaximumSize = new System.Drawing.Size(320, 216);
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(320, 216);
			this.Name = "FormStatus";
			this.Text = "Status";
			this.TopMost = true;
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
	}
}
