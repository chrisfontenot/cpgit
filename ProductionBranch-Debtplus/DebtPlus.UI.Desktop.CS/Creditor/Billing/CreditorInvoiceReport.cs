using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Interfaces.Creditor;
using DebtPlus.Reports.Template;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing
{
    public partial class CreditorInvoiceReport : BaseXtraReportClass
    {
        public CreditorInvoiceReport() : base()
        {
            InitializeComponent();
            XrSubreportInvoiceReport.BeforePrint += XrSubreportInvoiceReport_BeforePrint;

            // Load the invoice report as the sub-report
            try
            {
                DebtPlus.Reports.Creditor.Invoice.CreditorInvoiceReport InvoiceReport = new DebtPlus.Reports.Creditor.Invoice.CreditorInvoiceReport();
                InvoiceReport.RequestParameters = false;
                foreach (DevExpress.XtraReports.Parameters.Parameter parm in InvoiceReport.Parameters)
                {
                    parm.Visible = false;
                }

                // Suppress showing the page numbering. It is for the whole report and not a single Creditor Invoice
                DevExpress.XtraReports.UI.PageFooterBand band = InvoiceReport.Bands["PageFooter"] as DevExpress.XtraReports.UI.PageFooterBand;
                if (band != null)
                {
                    DevExpress.XtraReports.UI.XRControl ctl = ((DevExpress.XtraReports.UI.XtraReport)InvoiceReport).FindControl("XrPageInfo1", true);
                    if (ctl != null)
                    {
                        ctl.Visible = false;
                    }
                }

                // Finally, the report is the sub-report source
                XrSubreportInvoiceReport.ReportSource = (DevExpress.XtraReports.UI.XtraReport)InvoiceReport;
            }
            catch (System.Exception ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format("{0}: {3}{1}{1}{2}", "Error loading report", Environment.NewLine, ex.Message, "DebtPlus.Reports.Creditor.Invoice.dll"), "Failure to load invoice report", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void XrSubreportInvoiceReport_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            var _with1 = (DevExpress.XtraReports.UI.XRSubreport)sender;
            string creditor = Convert.ToString(GetCurrentColumnValue("creditor"));

            DevExpress.XtraReports.UI.XtraReport rpt = (DevExpress.XtraReports.UI.XtraReport)_with1.ReportSource;
            DebtPlus.Interfaces.Reports.IReports DebtPlusReport = rpt as DebtPlus.Interfaces.Reports.IReports;
            if (DebtPlusReport != null)
            {
                if (DebtPlusReport is ICreditor)
                {
                    ((ICreditor)DebtPlusReport).Creditor = creditor;
                }
                else
                {
                    DebtPlusReport.SetReportParameter("Creditor", creditor);
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        public override string ReportTitle
        {
            get { return "Creditor Invoices"; }
        }
    }
}