using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
namespace DebtPlus.UI.Desktop.CS.Creditor.Billing
{
	partial class CreditorInvoiceReport
	{
		//XtraReport overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Designer
		//It can be modified using the Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.XrSubreportInvoiceReport = new DevExpress.XtraReports.UI.XRSubreport();
			((System.ComponentModel.ISupportInitialize)this).BeginInit();
			//
			//Detail
			//
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] { this.XrSubreportInvoiceReport });
			this.Detail.HeightF = 23f;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100f);
			this.Detail.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			//
			//XrSubreportInvoiceReport
			//
			this.XrSubreportInvoiceReport.LocationFloat = new DevExpress.Utils.PointFloat(0f, 0f);
			this.XrSubreportInvoiceReport.Name = "XrSubreportInvoiceReport";
			this.XrSubreportInvoiceReport.SizeF = new System.Drawing.SizeF(799.9999f, 23f);
			//
			//CreditorInvoiceReport
			//
			this.Version = "10.2";
			((System.ComponentModel.ISupportInitialize)this).EndInit();
		}
		protected internal DevExpress.XtraReports.UI.XRSubreport XrSubreportInvoiceReport;
	}
}
