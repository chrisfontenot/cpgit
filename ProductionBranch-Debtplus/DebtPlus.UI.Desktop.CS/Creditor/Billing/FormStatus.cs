using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing
{
    internal partial class FormStatus
    {
        private FormMain.Billing cls_billing;

        private System.DateTime BaseTime;

        /// <summary>
        /// Create a new instance of our form
        /// </summary>
        public FormStatus() : base()
        {
            InitializeComponent();
            Timer1.Tick += Timer1_Tick;
            this.GotFocus += FormStatus_GotFocus;
            this.Load += FormStatus_Load;
            base.Closing += FormStatus_Closing;
        }

        public FormStatus(FormMain.Billing bill) : this()
        {
            cls_billing = bill;
        }

        /// <summary>
        /// Set the CANCEL status if the cancel button is pressed
        /// </summary>
        private void Button_Cancel_Click(System.Object sender, System.EventArgs e)
        {
            cls_billing.Cancelled = true;
            Button_Cancel.Enabled = false;
            label_Caption.Text = "The operation is being cancelled. Please wait.";
            cls_billing.Cancelled = true;
        }

        /// <summary>
        /// Handle the tick of the background timer
        /// </summary>

        private void Timer1_Tick(object sender, System.EventArgs e)
        {
            // Convert the time to a formatted string

            try
            {
                // Find the elapsed time
                System.TimeSpan ElapsedTime = System.DateTime.UtcNow.Subtract(BaseTime);
                System.Int32 hours = ElapsedTime.Hours;
                System.Int32 Minutes = ElapsedTime.Minutes;
                System.Int32 Seconds = ElapsedTime.Seconds;
                Label_ElapsedTime.Text = System.String.Format("{0:0}:{1:00}:{2:00}", hours, Minutes, Seconds);
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
            }

            // Restart the timer at one second
            var _with1 = Timer1;
            _with1.Interval = 1000;
            _with1.Start();
        }

        /// <summary>
        /// Process the condition when the window receives the input focus
        /// </summary>
        private void FormStatus_GotFocus(object sender, System.EventArgs e)
        {
            TopMost = false;
        }

        /// <summary>
        /// One-Time intialization of our form
        /// </summary>

        private void FormStatus_Load(object sender, System.EventArgs e)
        {
            // Start the timer at one second
            BaseTime = System.DateTime.UtcNow;
            var _with2 = Timer1;
            _with2.Enabled = true;
            _with2.Interval = 1000;
            _with2.Start();
        }

        /// <summary>
        /// The form is being closed. Set the CANCEL status
        /// </summary>
        private void FormStatus_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (cls_billing != null)
                cls_billing.Cancelled = true;
            Timer1.Stop();
            Timer1.Enabled = false;
        }
    }
}