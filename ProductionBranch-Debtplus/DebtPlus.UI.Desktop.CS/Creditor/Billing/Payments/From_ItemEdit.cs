using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing.Payments
{
    internal partial class From_ItemEdit : DebtPlus.Data.Forms.DebtPlusForm
    {
        private System.Data.DataRow row = null;
        private decimal AvailableFunds  = 0m;
        private decimal inv_amount      = 0m;
        private decimal adj_amount      = 0m;
        private decimal pmt_amount      = 0m;

        public From_ItemEdit(decimal Available, System.Data.DataRow EditRow) : this()
        {
            row = EditRow;
            AvailableFunds = Available;
        }

        public From_ItemEdit() : base()
        {
            InitializeComponent();
            this.Load += From_ItemEdit_Load;
            CalcEdit1.Validating += DebtPlus.Data.Validation.NonNegative;
            CalcEdit2.Validating += DebtPlus.Data.Validation.NonNegative;
            CalcEdit3.Validating += DebtPlus.Data.Validation.NonNegative;
            CalcEdit4.Validating += DebtPlus.Data.Validation.NonNegative;
            CalcEdit5.Validating += DebtPlus.Data.Validation.NonNegative;
            CalcEdit6.Validating += DebtPlus.Data.Validation.NonNegative;
            CalcEdit7.Validating += DebtPlus.Data.Validation.NonNegative;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl LabelControl6;
        internal DevExpress.XtraEditors.LabelControl LabelControl7;
        internal DevExpress.XtraEditors.LabelControl LabelControl8;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit1;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit2;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit3;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit4;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit5;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit6;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit7;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.CalcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit2 = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit3 = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit4 = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit5 = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit6 = new DevExpress.XtraEditors.CalcEdit();
            this.CalcEdit7 = new DevExpress.XtraEditors.CalcEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit2.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit3.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit4.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit5.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit6.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit7.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(16, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(355, 38);
            this.LabelControl1.TabIndex = 6;
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            this.LabelControl1.UseMnemonic = false;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 60);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(114, 13);
            this.LabelControl2.TabIndex = 7;
            this.LabelControl2.Text = "Original Invoice Amount";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 84);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(107, 13);
            this.LabelControl3.TabIndex = 9;
            this.LabelControl3.Text = "Less Amount Adjusted";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(8, 108);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(136, 13);
            this.LabelControl4.TabIndex = 11;
            this.LabelControl4.Text = "Less Amount Previously Paid";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            //
            //LabelControl5
            //
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl5.Location = new System.Drawing.Point(8, 140);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(99, 13);
            this.LabelControl5.TabIndex = 13;
            this.LabelControl5.Text = "Outstanding Balance";
            this.LabelControl5.ToolTipController = this.ToolTipController1;
            //
            //LabelControl6
            //
            this.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl6.Location = new System.Drawing.Point(8, 164);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(138, 13);
            this.LabelControl6.TabIndex = 0;
            this.LabelControl6.Text = "Less Amount to pay this time";
            this.LabelControl6.ToolTipController = this.ToolTipController1;
            //
            //LabelControl7
            //
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(8, 188);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(150, 13);
            this.LabelControl7.TabIndex = 2;
            this.LabelControl7.Text = "Less Amount to adjust this time";
            this.LabelControl7.ToolTipController = this.ToolTipController1;
            //
            //LabelControl8
            //
            this.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl8.Location = new System.Drawing.Point(8, 228);
            this.LabelControl8.Name = "LabelControl8";
            this.LabelControl8.Size = new System.Drawing.Size(129, 13);
            this.LabelControl8.TabIndex = 15;
            this.LabelControl8.Text = "Yields New Invoice Balance";
            this.LabelControl8.ToolTipController = this.ToolTipController1;
            //
            //Button_OK
            //
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(296, 56);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 4;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(296, 88);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit1
            //
            this.CalcEdit1.Enabled = false;
            this.CalcEdit1.Location = new System.Drawing.Point(184, 56);
            this.CalcEdit1.Name = "CalcEdit1";
            this.CalcEdit1.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit1.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit1.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit1.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.CalcEdit1.Properties.Appearance.Options.UseBorderColor = true;
            this.CalcEdit1.Properties.Appearance.Options.UseForeColor = true;
            this.CalcEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit1.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit1.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit1.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit1.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit1.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.CalcEdit1.Properties.AppearanceDisabled.Options.UseBorderColor = true;
            this.CalcEdit1.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.CalcEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit1.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit1.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit1.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit1.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit1.Properties.EditFormat.FormatString = "f2";
            this.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit1.Properties.Precision = 2;
            this.CalcEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CalcEdit1.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit1.TabIndex = 8;
            this.CalcEdit1.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit2
            //
            this.CalcEdit2.Enabled = false;
            this.CalcEdit2.Location = new System.Drawing.Point(184, 80);
            this.CalcEdit2.Name = "CalcEdit2";
            this.CalcEdit2.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit2.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit2.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit2.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit2.Properties.Appearance.Options.UseBackColor = true;
            this.CalcEdit2.Properties.Appearance.Options.UseBorderColor = true;
            this.CalcEdit2.Properties.Appearance.Options.UseForeColor = true;
            this.CalcEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit2.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit2.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit2.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit2.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit2.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.CalcEdit2.Properties.AppearanceDisabled.Options.UseBorderColor = true;
            this.CalcEdit2.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.CalcEdit2.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit2.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit2.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit2.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit2.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit2.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit2.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit2.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit2.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit2.Properties.EditFormat.FormatString = "f2";
            this.CalcEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit2.Properties.Precision = 2;
            this.CalcEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CalcEdit2.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit2.TabIndex = 10;
            this.CalcEdit2.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit3
            //
            this.CalcEdit3.Enabled = false;
            this.CalcEdit3.Location = new System.Drawing.Point(184, 104);
            this.CalcEdit3.Name = "CalcEdit3";
            this.CalcEdit3.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit3.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit3.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit3.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit3.Properties.Appearance.Options.UseBackColor = true;
            this.CalcEdit3.Properties.Appearance.Options.UseBorderColor = true;
            this.CalcEdit3.Properties.Appearance.Options.UseForeColor = true;
            this.CalcEdit3.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit3.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit3.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit3.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit3.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit3.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit3.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.CalcEdit3.Properties.AppearanceDisabled.Options.UseBorderColor = true;
            this.CalcEdit3.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.CalcEdit3.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit3.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit3.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit3.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit3.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit3.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit3.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit3.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit3.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit3.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit3.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit3.Properties.EditFormat.FormatString = "f2";
            this.CalcEdit3.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit3.Properties.Precision = 2;
            this.CalcEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CalcEdit3.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit3.TabIndex = 12;
            this.CalcEdit3.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit4
            //
            this.CalcEdit4.Enabled = false;
            this.CalcEdit4.Location = new System.Drawing.Point(184, 136);
            this.CalcEdit4.Name = "CalcEdit4";
            this.CalcEdit4.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit4.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit4.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit4.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit4.Properties.Appearance.Options.UseBackColor = true;
            this.CalcEdit4.Properties.Appearance.Options.UseBorderColor = true;
            this.CalcEdit4.Properties.Appearance.Options.UseForeColor = true;
            this.CalcEdit4.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit4.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit4.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit4.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit4.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit4.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit4.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.CalcEdit4.Properties.AppearanceDisabled.Options.UseBorderColor = true;
            this.CalcEdit4.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.CalcEdit4.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit4.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit4.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit4.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit4.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit4.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit4.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit4.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit4.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit4.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit4.Properties.EditFormat.FormatString = "f2";
            this.CalcEdit4.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit4.Properties.Precision = 2;
            this.CalcEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CalcEdit4.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit4.TabIndex = 14;
            this.CalcEdit4.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit5
            //
            this.CalcEdit5.Location = new System.Drawing.Point(184, 160);
            this.CalcEdit5.Name = "CalcEdit5";
            this.CalcEdit5.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit5.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit5.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit5.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit5.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit5.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit5.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit5.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit5.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit5.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit5.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit5.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit5.Properties.EditFormat.FormatString = "f2";
            this.CalcEdit5.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit5.Properties.Precision = 2;
            this.CalcEdit5.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit5.TabIndex = 1;
            this.CalcEdit5.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit6
            //
            this.CalcEdit6.Location = new System.Drawing.Point(184, 184);
            this.CalcEdit6.Name = "CalcEdit6";
            this.CalcEdit6.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit6.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit6.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit6.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit6.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit6.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit6.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit6.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit6.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit6.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit6.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit6.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit6.Properties.EditFormat.FormatString = "f2";
            this.CalcEdit6.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit6.Properties.Precision = 2;
            this.CalcEdit6.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit6.TabIndex = 3;
            this.CalcEdit6.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit7
            //
            this.CalcEdit7.Enabled = false;
            this.CalcEdit7.Location = new System.Drawing.Point(184, 224);
            this.CalcEdit7.Name = "CalcEdit7";
            this.CalcEdit7.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit7.Properties.Appearance.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit7.Properties.Appearance.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit7.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit7.Properties.Appearance.Options.UseBackColor = true;
            this.CalcEdit7.Properties.Appearance.Options.UseBorderColor = true;
            this.CalcEdit7.Properties.Appearance.Options.UseForeColor = true;
            this.CalcEdit7.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit7.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit7.Properties.AppearanceDisabled.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit7.Properties.AppearanceDisabled.BackColor2 = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit7.Properties.AppearanceDisabled.BorderColor = System.Drawing.SystemColors.InactiveBorder;
            this.CalcEdit7.Properties.AppearanceDisabled.ForeColor = System.Drawing.Color.Black;
            this.CalcEdit7.Properties.AppearanceDisabled.Options.UseBackColor = true;
            this.CalcEdit7.Properties.AppearanceDisabled.Options.UseBorderColor = true;
            this.CalcEdit7.Properties.AppearanceDisabled.Options.UseForeColor = true;
            this.CalcEdit7.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.CalcEdit7.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit7.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.CalcEdit7.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit7.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.CalcEdit7.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit7.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.CalcEdit7.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit7.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit7.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit7.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit7.Properties.EditFormat.FormatString = "f2";
            this.CalcEdit7.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit7.Properties.Precision = 2;
            this.CalcEdit7.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.CalcEdit7.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit7.TabIndex = 16;
            this.CalcEdit7.ToolTipController = this.ToolTipController1;
            //
            //From_ItemEdit
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(384, 262);
            this.Controls.Add(this.CalcEdit7);
            this.Controls.Add(this.CalcEdit6);
            this.Controls.Add(this.CalcEdit5);
            this.Controls.Add(this.CalcEdit4);
            this.Controls.Add(this.CalcEdit3);
            this.Controls.Add(this.CalcEdit2);
            this.Controls.Add(this.CalcEdit1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl8);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "From_ItemEdit";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Invoice Information";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit2.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit3.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit4.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit5.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit6.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit7.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void From_ItemEdit_Load(object sender, System.EventArgs e)
        {
            // IF there is an input row then obtain the values from the row
            if (row != null)
            {
                if (!object.ReferenceEquals(row["inv_amount"], System.DBNull.Value))
                    inv_amount = Convert.ToDecimal(row["inv_amount"]);
                if (!object.ReferenceEquals(row["pmt_amount"], System.DBNull.Value))
                    pmt_amount = Convert.ToDecimal(row["pmt_amount"]);
                if (!object.ReferenceEquals(row["adj_amount"], System.DBNull.Value))
                    adj_amount = Convert.ToDecimal(row["adj_amount"]);
            }

            // Generate the information for the display.
            CalcEdit1.EditValue = inv_amount;
            CalcEdit2.EditValue = adj_amount;
            CalcEdit3.EditValue = pmt_amount;
            CalcEdit4.EditValue = inv_amount - adj_amount - pmt_amount;
            CalcEdit5.EditValue = System.Math.Min(AvailableFunds, inv_amount - adj_amount - pmt_amount);
            CalcEdit6.EditValue = 0m;
            CalcEdit7.EditValue = inv_amount - adj_amount - pmt_amount - Convert.ToDecimal(CalcEdit5.EditValue);

            // Register the two event handlers
            CalcEdit5.Validated += Form_Changed;
            CalcEdit6.Validated += Form_Changed;

            // Change the display text according the money available
            if (AvailableFunds <= 0m)
            {
                LabelControl1.Text = "There is no money to pay this invoice. However, you are permitted to adjust the invoice. Or you may click on the cancel button to return to the previous form.";
                CalcEdit5.Enabled = false;
            }
            else
            {
                LabelControl1.Text = System.String.Format("There is {0:c} to pay this invoice. Either pay some (or all) of this and/or adjust the invoice.", AvailableFunds);
            }

            // Set the OK button status
            EnableOK();
        }

        private void Form_Changed(object Sender, System.EventArgs e)
        {
            EnableOK();
        }

        private void EnableOK()
        {
            // Update the information on the display and enable the OK button if possible.
            CalcEdit7.EditValue = inv_amount - adj_amount - pmt_amount - Convert.ToDecimal(CalcEdit5.EditValue) - Convert.ToDecimal(CalcEdit6.EditValue);
            Button_OK.Enabled = (Convert.ToDecimal(CalcEdit5.EditValue) > 0m) || (Convert.ToDecimal(CalcEdit6.EditValue) > 0m);

            // DO not allow the invoice to be paid with funds more than available in the check.
            if (Convert.ToDecimal(CalcEdit5.EditValue) > AvailableFunds && AvailableFunds > 0)
            {
                DxErrorProvider1.SetError(CalcEdit5, System.String.Format("You have only {0:c2} to use on this invoice. Please limit it to that amount.", AvailableFunds));
                Button_OK.Enabled = false;
            }
            else
            {
                DxErrorProvider1.SetError(CalcEdit5, System.String.Empty);
            }

            // Do not allow the invoice to go negative.
            if (Convert.ToDecimal(CalcEdit7.EditValue) < 0m)
                Button_OK.Enabled = false;
        }
    }
}