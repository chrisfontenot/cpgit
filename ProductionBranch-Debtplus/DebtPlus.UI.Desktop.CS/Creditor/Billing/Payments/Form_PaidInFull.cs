using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing.Payments
{
    public partial class Form_PaidInFull : DebtPlus.Data.Forms.DebtPlusForm
    {
        private decimal MaximumAvailable = 0m;

        public Form_PaidInFull() : base()
        {
            InitializeComponent();
            CalcEdit1.EditValueChanging += CalcEdit1_EditValueChanging;
            Load += Form_PaidInFull_Load;
        }

        public Form_PaidInFull(decimal MaxFunds) : this()
        {
            MaximumAvailable = MaxFunds;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl label_header;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.CalcEdit CalcEdit1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.label_header = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.CalcEdit1 = new DevExpress.XtraEditors.CalcEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //label_header
            //
            this.label_header.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.label_header.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.label_header.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.label_header.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.label_header.Location = new System.Drawing.Point(12, 12);
            this.label_header.Name = "label_header";
            this.label_header.Size = new System.Drawing.Size(274, 40);
            this.label_header.TabIndex = 0;
            this.label_header.ToolTipController = this.ToolTipController1;
            //
            //LabelControl2
            //
            this.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl2.Location = new System.Drawing.Point(8, 61);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(149, 31);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "What is the amount that you wish to pay?";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //CalcEdit1
            //
            this.CalcEdit1.Location = new System.Drawing.Point(163, 58);
            this.CalcEdit1.Name = "CalcEdit1";
            this.CalcEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.CalcEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.CalcEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.CalcEdit1.Properties.DisplayFormat.FormatString = "c2";
            this.CalcEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit1.Properties.EditFormat.FormatString = "d2";
            this.CalcEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CalcEdit1.Properties.Precision = 2;
            this.CalcEdit1.Size = new System.Drawing.Size(100, 20);
            this.CalcEdit1.TabIndex = 2;
            this.CalcEdit1.ToolTipController = this.ToolTipController1;
            //
            //Button_OK
            //
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(60, 106);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 3;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(163, 106);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 4;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //Form_PaidInFull
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(294, 141);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.CalcEdit1);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.label_header);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.MaximizeBox = false;
            this.Name = "Form_PaidInFull";
            this.Text = "Pay Oldest Invoices First";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.CalcEdit1.Properties).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void CalcEdit1_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            var _with1 = CalcEdit1;
            Button_OK.Enabled = Convert.ToDecimal(_with1.EditValue) >= 0m & Convert.ToDecimal(_with1.EditValue) <= MaximumAvailable;
        }

        private void Form_PaidInFull_Load(object sender, System.EventArgs e)
        {
            label_header.Text = System.String.Format("There is {0:c2} available to pay the invoices for this creditor. Please indicate the amount that is to be used to pay the invoices in the space below.", MaximumAvailable);
        }
    }
}