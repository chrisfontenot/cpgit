#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using DebtPlus.UI.Creditor.Widgets.Controls;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing.Payments
{
    internal partial class Form_InvoicePayments : DebtPlus.Data.Forms.DebtPlusForm
    {
        internal System.Data.DataSet ds = new System.Data.DataSet("ds");
        internal CreditorID Text_CreditorID;

        private ArgParser ap = null;

        public Form_InvoicePayments(ArgParser ap) : this()
        {
            this.ap = ap;
        }

        public Form_InvoicePayments() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Text_Amount.EditValueChanging += Text_Amount_EditValueChanging;
            Load                          += Form_InvoicePayments_Load;
            Text_Amount.EditValueChanged  += txt_check_amount_TextChanged;
            GridView1.DoubleClick         += GridView1_DoubleClick;
            GridView1.FocusedRowChanged   += GridView1_FocusedRowChanged;
            Text_CreditorID.Validated     += Text_CreditorID_Validated;
            Button_Select.Click           += Button_Select_Click;
            Button_NonAR.Click            += Button_NonAR_Click;
            Button_PayOldest.Click        += Button_PayOldest_Click;
            GridView1.Layout              += LayoutChanged;
        }

        private void UnRegisterHandlers()
        {
            Text_Amount.EditValueChanging -= Text_Amount_EditValueChanging;
            Load                          -= Form_InvoicePayments_Load;
            Text_Amount.EditValueChanged  -= txt_check_amount_TextChanged;
            GridView1.DoubleClick         -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged   -= GridView1_FocusedRowChanged;
            Text_CreditorID.Validated     -= Text_CreditorID_Validated;
            Button_Select.Click           -= Button_Select_Click;
            Button_NonAR.Click            -= Button_NonAR_Click;
            Button_PayOldest.Click        -= Button_PayOldest_Click;
            GridView1.Layout              -= LayoutChanged;
        }

        /// <summary>
        /// A/R payments
        /// </summary>
        private decimal ar_payments
        {
            get
            {
                if (label_ar.Text.Length == 0)
                {
                    return 0m;
                }
                return System.Decimal.Parse(label_ar.Text, System.Globalization.NumberStyles.Currency);
            }
            set
            {
                label_ar.Text = string.Format("{0:c}", value);
                unspent_funds = check_amount - ar_payments - non_ar_payments;
            }
        }

        /// <summary>
        /// Non-A/R payments
        /// </summary>
        private decimal non_ar_payments
        {
            get
            {
                if (label_non_ar.Text.Length == 0)
                {
                    return 0m;
                }
                    return System.Decimal.Parse(label_non_ar.Text, System.Globalization.NumberStyles.Currency);
            }
            set
            {
                label_non_ar.Text = string.Format("{0:c}", value);
                unspent_funds = check_amount - ar_payments - non_ar_payments;
            }
        }

        /// <summary>
        /// Check amount
        /// </summary>
        private decimal check_amount
        {
            get
            {
                return Convert.ToDecimal(Text_Amount.Value);
            }
            set
            {
                Text_Amount.Value = value; 
            }
        }

        /// <summary>
        /// Unspent funds from the check
        /// </summary>
        private decimal unspent_funds
        {
            get
            {
                if (label_unspent_funds.Text.Length == 0)
                {
                    return 0m;
                }
                return System.Decimal.Parse(label_unspent_funds.Text, System.Globalization.NumberStyles.Currency);
            }
            set
            {
                label_unspent_funds.Text = string.Format("{0:c}", value);
                EnableOK();
            }
        }

        /// <summary>
        /// Enable or Disable the buttons
        /// </summary>
        private void EnableOK()
        {
            Button_NonAR.Enabled = (unspent_funds > 0m) && Label_CreditorAddress.Text != string.Empty;
            Button_PayOldest.Enabled = Button_NonAR.Enabled && ds.Tables.Contains("invoices") && (ds.Tables["invoices"].Rows != null);

            // Look for a row on the grid to enable the select button
            System.Data.DataRow EditRow = (System.Data.DataRow)GridView1.GetDataRow(GridView1.FocusedRowHandle);
            Button_Select.Enabled = EditRow != null && Convert.ToDecimal(EditRow["balance"]) > 0m;
        }

        /// <summary>
        /// Current creditor ID
        /// </summary>
        private string creditor
        {
            get
            {
                return Text_CreditorID.Text.Trim();
            }
            set
            {
                Text_CreditorID.Text = value;
            }
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl LabelControl6;
        internal DevExpress.XtraEditors.LabelControl LabelControl7;
        internal DevExpress.XtraEditors.CalcEdit Text_Amount;
        internal DevExpress.XtraEditors.TextEdit Text_ReferenceInfo;
        internal DevExpress.XtraEditors.SimpleButton Button_Select;
        internal DevExpress.XtraEditors.SimpleButton Button_PayOldest;
        internal DevExpress.XtraEditors.SimpleButton Button_NonAR;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraEditors.LabelControl Label_CreditorAddress;
        internal DevExpress.XtraEditors.LabelControl label_ar;
        internal DevExpress.XtraEditors.LabelControl label_non_ar;
        internal DevExpress.XtraEditors.LabelControl label_unspent_funds;
        internal DevExpress.XtraEditors.LabelControl label_net_invoice_amt;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Invoice;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_CheckNumber;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_InvDate;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_InvAmount;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_PmtAmt;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_PmtDate;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_AdjAmt;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_AdjDate;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Balance;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_InvoicePayments));
            DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.Text_Amount = new DevExpress.XtraEditors.CalcEdit();
            this.label_ar = new DevExpress.XtraEditors.LabelControl();
            this.label_non_ar = new DevExpress.XtraEditors.LabelControl();
            this.label_unspent_funds = new DevExpress.XtraEditors.LabelControl();
            this.label_net_invoice_amt = new DevExpress.XtraEditors.LabelControl();
            this.Text_ReferenceInfo = new DevExpress.XtraEditors.TextEdit();
            this.Button_Select = new DevExpress.XtraEditors.SimpleButton();
            this.Button_PayOldest = new DevExpress.XtraEditors.SimpleButton();
            this.Button_NonAR = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_Invoice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Invoice.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_CheckNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_CheckNumber.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_InvDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_InvDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_InvAmount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_InvAmount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_PmtAmt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_PmtAmt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_PmtDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_PmtDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_AdjAmt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_AdjAmt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_AdjDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_AdjDate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_Balance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_Balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Label_CreditorAddress = new DevExpress.XtraEditors.LabelControl();
            this.Text_CreditorID = new CreditorID();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Amount.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_ReferenceInfo.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_CreditorID.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl1.Location = new System.Drawing.Point(59, 9);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(57, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Creditor &ID:";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(43, 34);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(73, 13);
            this.LabelControl2.TabIndex = 3;
            this.LabelControl2.Text = "Check &Amount:";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(44, 60);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(72, 13);
            this.LabelControl3.TabIndex = 5;
            this.LabelControl3.Text = "A/R Payments:";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            this.LabelControl3.UseMnemonic = false;
            //
            //LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(21, 73);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(95, 13);
            this.LabelControl4.TabIndex = 7;
            this.LabelControl4.Text = "Non-A/R Payments:";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            this.LabelControl4.UseMnemonic = false;
            //
            //LabelControl5
            //
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl5.Location = new System.Drawing.Point(36, 86);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(80, 13);
            this.LabelControl5.TabIndex = 9;
            this.LabelControl5.Text = "Un-spent Funds:";
            this.LabelControl5.ToolTipController = this.ToolTipController1;
            this.LabelControl5.UseMnemonic = false;
            //
            //LabelControl6
            //
            this.LabelControl6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl6.Location = new System.Drawing.Point(8, 99);
            this.LabelControl6.Name = "LabelControl6";
            this.LabelControl6.Size = new System.Drawing.Size(108, 13);
            this.LabelControl6.TabIndex = 11;
            this.LabelControl6.Text = "Total Net Invoice Amt:";
            this.LabelControl6.ToolTipController = this.ToolTipController1;
            this.LabelControl6.UseMnemonic = false;
            //
            //LabelControl7
            //
            this.LabelControl7.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl7.Location = new System.Drawing.Point(264, 99);
            this.LabelControl7.Name = "LabelControl7";
            this.LabelControl7.Size = new System.Drawing.Size(77, 13);
            this.LabelControl7.TabIndex = 13;
            this.LabelControl7.Text = "&Reference Info:";
            this.LabelControl7.ToolTipController = this.ToolTipController1;
            //
            //Text_Amount
            //
            this.Text_Amount.Location = new System.Drawing.Point(136, 30);
            this.Text_Amount.Name = "Text_Amount";
            this.Text_Amount.Properties.Appearance.Options.UseTextOptions = true;
            this.Text_Amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Text_Amount.Properties.AppearanceDisabled.Options.UseTextOptions = true;
            this.Text_Amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Text_Amount.Properties.AppearanceDropDown.Options.UseTextOptions = true;
            this.Text_Amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Text_Amount.Properties.AppearanceFocused.Options.UseTextOptions = true;
            this.Text_Amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Text_Amount.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.Text_Amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Text_Amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.Text_Amount.Properties.DisplayFormat.FormatString = "{0:c}";
            this.Text_Amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Text_Amount.Properties.EditFormat.FormatString = "{0:f2}";
            this.Text_Amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Text_Amount.Properties.Mask.BeepOnError = true;
            this.Text_Amount.Properties.Mask.EditMask = "c";
            this.Text_Amount.Properties.Precision = 2;
            this.Text_Amount.Size = new System.Drawing.Size(100, 20);
            this.Text_Amount.TabIndex = 4;
            this.Text_Amount.ToolTip = "This is the total amount of the check";
            this.Text_Amount.ToolTipController = this.ToolTipController1;
            //
            //label_ar
            //
            this.label_ar.Appearance.Options.UseTextOptions = true;
            this.label_ar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.label_ar.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.label_ar.Location = new System.Drawing.Point(136, 60);
            this.label_ar.Name = "label_ar";
            this.label_ar.Size = new System.Drawing.Size(100, 13);
            this.label_ar.TabIndex = 6;
            this.label_ar.Text = "$0.00";
            this.label_ar.ToolTipController = this.ToolTipController1;
            //
            //label_non_ar
            //
            this.label_non_ar.Appearance.Options.UseTextOptions = true;
            this.label_non_ar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.label_non_ar.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.label_non_ar.Location = new System.Drawing.Point(136, 73);
            this.label_non_ar.Name = "label_non_ar";
            this.label_non_ar.Size = new System.Drawing.Size(100, 13);
            this.label_non_ar.TabIndex = 8;
            this.label_non_ar.Text = "$0.00";
            this.label_non_ar.ToolTipController = this.ToolTipController1;
            //
            //label_unspent_funds
            //
            this.label_unspent_funds.Appearance.Options.UseTextOptions = true;
            this.label_unspent_funds.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.label_unspent_funds.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.label_unspent_funds.Location = new System.Drawing.Point(136, 86);
            this.label_unspent_funds.Name = "label_unspent_funds";
            this.label_unspent_funds.Size = new System.Drawing.Size(100, 13);
            this.label_unspent_funds.TabIndex = 10;
            this.label_unspent_funds.Text = "$0.00";
            this.label_unspent_funds.ToolTipController = this.ToolTipController1;
            //
            //label_net_invoice_amt
            //
            this.label_net_invoice_amt.Appearance.Options.UseTextOptions = true;
            this.label_net_invoice_amt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.label_net_invoice_amt.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.label_net_invoice_amt.Location = new System.Drawing.Point(136, 99);
            this.label_net_invoice_amt.Name = "label_net_invoice_amt";
            this.label_net_invoice_amt.Size = new System.Drawing.Size(100, 13);
            this.label_net_invoice_amt.TabIndex = 12;
            this.label_net_invoice_amt.Text = "$0.00";
            this.label_net_invoice_amt.ToolTipController = this.ToolTipController1;
            //
            //Text_ReferenceInfo
            //
            this.Text_ReferenceInfo.Location = new System.Drawing.Point(352, 95);
            this.Text_ReferenceInfo.Name = "Text_ReferenceInfo";
            this.Text_ReferenceInfo.Size = new System.Drawing.Size(264, 20);
            this.Text_ReferenceInfo.TabIndex = 14;
            this.Text_ReferenceInfo.ToolTip = "Include any reference information associated with the payment. For example, the c" + "heck number.";
            this.Text_ReferenceInfo.ToolTipController = this.ToolTipController1;
            //
            //Button_Select
            //
            this.Button_Select.Location = new System.Drawing.Point(544, 8);
            this.Button_Select.Name = "Button_Select";
            this.Button_Select.Size = new System.Drawing.Size(75, 23);
            this.Button_Select.TabIndex = 15;
            this.Button_Select.Text = "&Select";
            this.Button_Select.ToolTip = "Pay the indicated invoice";
            this.Button_Select.ToolTipController = this.ToolTipController1;
            //
            //Button_PayOldest
            //
            this.Button_PayOldest.Location = new System.Drawing.Point(544, 39);
            this.Button_PayOldest.Name = "Button_PayOldest";
            this.Button_PayOldest.Size = new System.Drawing.Size(75, 23);
            this.Button_PayOldest.TabIndex = 16;
            this.Button_PayOldest.Text = "&Pay Oldest";
            this.Button_PayOldest.ToolTip = "Automatically pay the oldest invoice or invoices until the balance is paid";
            this.Button_PayOldest.ToolTipController = this.ToolTipController1;
            //
            //Button_NonAR
            //
            this.Button_NonAR.Location = new System.Drawing.Point(544, 70);
            this.Button_NonAR.Name = "Button_NonAR";
            this.Button_NonAR.Size = new System.Drawing.Size(75, 23);
            this.Button_NonAR.TabIndex = 17;
            this.Button_NonAR.Text = "&Non-A/R";
            this.Button_NonAR.ToolTip = "If you wish to make a non-AR payment for this creditor, click here.";
            this.Button_NonAR.ToolTipController = this.ToolTipController1;
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Location = new System.Drawing.Point(8, 128);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(624, 200);
            this.GridControl1.TabIndex = 18;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_Invoice,
                this.GridColumn_CheckNumber,
                this.GridColumn_InvDate,
                this.GridColumn_InvAmount,
                this.GridColumn_PmtAmt,
                this.GridColumn_PmtDate,
                this.GridColumn_AdjAmt,
                this.GridColumn_AdjDate,
                this.GridColumn_Balance
            });
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] { new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "balance", null, "") });
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.AutoCalcPreviewLineCount = true;
            this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.GridView1.OptionsView.EnableAppearanceOddRow = true;
            this.GridView1.OptionsView.RowAutoHeight = true;
            this.GridView1.OptionsView.ShowFooter = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowForFocusedRow;
            //
            //GridColumn_Invoice
            //
            this.GridColumn_Invoice.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Invoice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Invoice.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Invoice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Invoice.Caption = "Invoice";
            this.GridColumn_Invoice.CustomizationCaption = "InvoiceNumber";
            this.GridColumn_Invoice.DisplayFormat.FormatString = "d10";
            this.GridColumn_Invoice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Invoice.FieldName = "invoice_register";
            this.GridColumn_Invoice.Name = "GridColumn_Invoice";
            this.GridColumn_Invoice.Visible = true;
            this.GridColumn_Invoice.VisibleIndex = 0;
            //
            //GridColumn_CheckNumber
            //
            this.GridColumn_CheckNumber.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_CheckNumber.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_CheckNumber.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_CheckNumber.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_CheckNumber.Caption = "Check #";
            this.GridColumn_CheckNumber.CustomizationCaption = "CheckNumber";
            this.GridColumn_CheckNumber.FieldName = "checknum";
            this.GridColumn_CheckNumber.Name = "GridColumn_CheckNumber";
            this.GridColumn_CheckNumber.Visible = true;
            this.GridColumn_CheckNumber.VisibleIndex = 1;
            //
            //GridColumn_InvDate
            //
            this.GridColumn_InvDate.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_InvDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_InvDate.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_InvDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_InvDate.Caption = "Inv Date";
            this.GridColumn_InvDate.CustomizationCaption = "InvoiceDate";
            this.GridColumn_InvDate.DisplayFormat.FormatString = "d";
            this.GridColumn_InvDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_InvDate.FieldName = "inv_date";
            this.GridColumn_InvDate.Name = "GridColumn_InvDate";
            this.GridColumn_InvDate.Visible = true;
            this.GridColumn_InvDate.VisibleIndex = 2;
            //
            //GridColumn_InvAmount
            //
            this.GridColumn_InvAmount.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_InvAmount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_InvAmount.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_InvAmount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_InvAmount.Caption = "Inv Amt";
            this.GridColumn_InvAmount.CustomizationCaption = "InvoiceAmount";
            this.GridColumn_InvAmount.DisplayFormat.FormatString = "c2";
            this.GridColumn_InvAmount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_InvAmount.FieldName = "inv_amount";
            this.GridColumn_InvAmount.GroupFormat.FormatString = "c2";
            this.GridColumn_InvAmount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_InvAmount.Name = "GridColumn_InvAmount";
            this.GridColumn_InvAmount.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_InvAmount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.GridColumn_InvAmount.Visible = true;
            this.GridColumn_InvAmount.VisibleIndex = 3;
            //
            //GridColumn_PmtAmt
            //
            this.GridColumn_PmtAmt.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_PmtAmt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_PmtAmt.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_PmtAmt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_PmtAmt.Caption = "Pmt Amt";
            this.GridColumn_PmtAmt.CustomizationCaption = "PaymentAmount";
            this.GridColumn_PmtAmt.DisplayFormat.FormatString = "c2";
            this.GridColumn_PmtAmt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_PmtAmt.FieldName = "pmt_amount";
            this.GridColumn_PmtAmt.GroupFormat.FormatString = "c2";
            this.GridColumn_PmtAmt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_PmtAmt.Name = "GridColumn_PmtAmt";
            this.GridColumn_PmtAmt.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_PmtAmt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.GridColumn_PmtAmt.Visible = true;
            this.GridColumn_PmtAmt.VisibleIndex = 4;
            //
            //GridColumn_PmtDate
            //
            this.GridColumn_PmtDate.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_PmtDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_PmtDate.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_PmtDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_PmtDate.Caption = "Pmt Date";
            this.GridColumn_PmtDate.CustomizationCaption = "PaymentDate";
            this.GridColumn_PmtDate.DisplayFormat.FormatString = "d";
            this.GridColumn_PmtDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_PmtDate.FieldName = "pmt_date";
            this.GridColumn_PmtDate.Name = "GridColumn_PmtDate";
            //
            //GridColumn_AdjAmt
            //
            this.GridColumn_AdjAmt.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_AdjAmt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_AdjAmt.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_AdjAmt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_AdjAmt.Caption = "Adj Amt";
            this.GridColumn_AdjAmt.CustomizationCaption = "AdjustmentAmount";
            this.GridColumn_AdjAmt.DisplayFormat.FormatString = "c2";
            this.GridColumn_AdjAmt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_AdjAmt.FieldName = "adj_amount";
            this.GridColumn_AdjAmt.GroupFormat.FormatString = "c2";
            this.GridColumn_AdjAmt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_AdjAmt.Name = "GridColumn_AdjAmt";
            this.GridColumn_AdjAmt.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_AdjAmt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.GridColumn_AdjAmt.Visible = true;
            this.GridColumn_AdjAmt.VisibleIndex = 5;
            //
            //GridColumn_AdjDate
            //
            this.GridColumn_AdjDate.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_AdjDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_AdjDate.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_AdjDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_AdjDate.Caption = "Adj Date";
            this.GridColumn_AdjDate.CustomizationCaption = "AdjustmentDate";
            this.GridColumn_AdjDate.DisplayFormat.FormatString = "d";
            this.GridColumn_AdjDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_AdjDate.FieldName = "adj_date";
            this.GridColumn_AdjDate.Name = "GridColumn_AdjDate";
            //
            //GridColumn_Balance
            //
            this.GridColumn_Balance.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_Balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Balance.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_Balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_Balance.Caption = "Balance";
            this.GridColumn_Balance.CustomizationCaption = "Balance";
            this.GridColumn_Balance.DisplayFormat.FormatString = "c2";
            this.GridColumn_Balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Balance.FieldName = "balance";
            this.GridColumn_Balance.GroupFormat.FormatString = "c2";
            this.GridColumn_Balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_Balance.Name = "GridColumn_Balance";
            this.GridColumn_Balance.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_Balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.GridColumn_Balance.Visible = true;
            this.GridColumn_Balance.VisibleIndex = 6;
            //
            //Label_CreditorAddress
            //
            this.Label_CreditorAddress.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.Label_CreditorAddress.Location = new System.Drawing.Point(256, 8);
            this.Label_CreditorAddress.Name = "Label_CreditorAddress";
            this.Label_CreditorAddress.Size = new System.Drawing.Size(0, 13);
            this.Label_CreditorAddress.TabIndex = 2;
            this.Label_CreditorAddress.ToolTipController = this.ToolTipController1;
            this.Label_CreditorAddress.UseMnemonic = false;
            //
            //Text_CreditorID
            //
            this.Text_CreditorID.EditValue = null;
            this.Text_CreditorID.Location = new System.Drawing.Point(136, 5);
            this.Text_CreditorID.Name = "Text_CreditorID";
            this.Text_CreditorID.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("CreditorID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
            "Click here to search for a creditor ID", "search", null, true) });
            this.Text_CreditorID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Text_CreditorID.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
            this.Text_CreditorID.Properties.Mask.BeepOnError = true;
            this.Text_CreditorID.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
            this.Text_CreditorID.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.Text_CreditorID.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.Text_CreditorID.Properties.MaxLength = 10;
            this.Text_CreditorID.Size = new System.Drawing.Size(100, 20);
            this.Text_CreditorID.TabIndex = 1;
            //
            //Form_InvoicePayments
            //
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.ClientSize = new System.Drawing.Size(640, 334);
            this.Controls.Add(this.Text_CreditorID);
            this.Controls.Add(this.Label_CreditorAddress);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.Button_NonAR);
            this.Controls.Add(this.Button_PayOldest);
            this.Controls.Add(this.Button_Select);
            this.Controls.Add(this.Text_ReferenceInfo);
            this.Controls.Add(this.label_net_invoice_amt);
            this.Controls.Add(this.label_unspent_funds);
            this.Controls.Add(this.label_non_ar);
            this.Controls.Add(this.label_ar);
            this.Controls.Add(this.Text_Amount);
            this.Controls.Add(this.LabelControl7);
            this.Controls.Add(this.LabelControl6);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Form_InvoicePayments";
            this.Text = "Invoice Payments";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Amount.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_ReferenceInfo.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_CreditorID.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Perform a Non-A/R transaction for this creditor
        /// </summary>
        private void Button_NonAR_Click(System.Object Sender, System.EventArgs e)
        {
            decimal Amount    = 0m;
            string LedgerCode = string.Empty;
            string Message    = string.Empty;

            using (var frm = new Form_NonAR(unspent_funds))
            {
                if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                {
                    return;
                }
                Amount     = Convert.ToDecimal(frm.txt_contribution.EditValue);
                LedgerCode = Convert.ToString(frm.lst_LedgerCode.EditValue);
                Message    = frm.MemoEdit1.Text.Trim();
            }

            try
            {
                // Process the non-ar information
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_creditor_contribution_non_ar";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;
                        cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = Amount;
                        cmd.Parameters.Add("@Account", SqlDbType.VarChar, 50).Value = LedgerCode;
                        cmd.Parameters.Add("@Message", SqlDbType.VarChar, 1024).Value = Message;
                        cmd.ExecuteNonQuery();
                    }
                }

                non_ar_payments += Amount;
                EnableOK();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting Non-AR Amount");
            }
        }

        /// <summary>
        /// Pay invoices until there is no more money.
        /// </summary>
        private void Button_PayOldest_Click(System.Object Sender, System.EventArgs e)
        {
            decimal FundsChosen = 0m;

            // If there are unspent funds then we may apply it to the invoices.
            // Ask how much is desired.
            if (unspent_funds > 0m)
            {
                using (var frm = new Form_PaidInFull(unspent_funds))
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }
                    FundsChosen = Convert.ToDecimal(frm.CalcEdit1.EditValue);
                    if (FundsChosen <= 0m)
                    {
                        RefreshInvoiceList();
                        EnableOK();
                        return;
                    }
                }

                try
                {
                    using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "xpr_creditor_contribution_pif";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = creditor;
                            cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = FundsChosen;
                            cmd.Parameters.Add("@message", SqlDbType.VarChar, 256).Value = Text_ReferenceInfo.Text.Trim();

                            object objPaid = cmd.ExecuteScalar();
                            if (objPaid != null && !object.ReferenceEquals(objPaid, System.DBNull.Value))
                            {
                                ar_payments += FundsChosen - Convert.ToDecimal(objPaid);
                            }
                        }
                    }

                    // Since the stored procedure may have changed multiple items, refresh the whole list.
                    RefreshInvoiceList();
                    EnableOK();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error setting Non-AR Amount");
                }
            }
        }

        #region " Grid Control "

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string BasePath = System.Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + System.IO.Path.DirectorySeparatorChar + "DebtPlus";
            return System.IO.Path.Combine(BasePath, "Creditor.Billing.Payments");
        }

        /// <summary>
        /// Reload the layout of the grid control if needed
        /// </summary>
        protected void ReloadGridControlLayout()
        {
            // Find the base path to the saved file location
            string PathName = XMLBasePath();
            UnRegisterHandlers();
            try
            {
                string FileName = System.IO.Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                if (System.IO.File.Exists(FileName))
                {
                    GridView1.RestoreLayoutFromXml(FileName);
                }
            }
#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                string PathName = XMLBasePath();
                if (!string.IsNullOrEmpty(PathName))
                {
                    if (!System.IO.Directory.Exists(PathName))
                    {
                        System.IO.Directory.CreateDirectory(PathName);
                    }
                    string FileName = System.IO.Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                    GridView1.SaveLayoutToXml(FileName);
                }
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Find the outstanding balance for the invoices
        /// </summary>
        private decimal BalanceTotal()
        {
            System.Data.DataTable tbl = (System.Data.DataTable)GridControl1.DataSource;
            object answer = tbl.Compute("sum(balance)", string.Empty);
            if (object.ReferenceEquals(answer, System.DBNull.Value))
                answer = 0m;
            return Convert.ToDecimal(answer);
        }

        /// <summary>
        /// Rebuild the list of pending invoices
        /// </summary>
        private void RefreshInvoiceList()
        {
            const string TableName = "invoices";

            // Clear the invoice table
            System.Data.DataTable tbl = ds.Tables[TableName];
            if (tbl != null)
                ds.Tables.Remove(tbl);

            // Set the wait cursor to indicate that we are busy
            System.Windows.Forms.Cursor prior_cursor = System.Windows.Forms.Cursor.Current;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            try
            {
                using (System.Data.SqlClient.SqlCommand cmd = SelectCommand())
                {
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, TableName);
                    }
                }

                tbl = ds.Tables[TableName];

                if (tbl != null)
                {
                    //Under some rare conditions, the check number may be null. This would fail as a primary key. So, since we don't do a "find" on the rows
                    //and the table is deleted before it is reloaded, we don't need a primary key. For now, leave it off.
                    //If .PrimaryKey.Length <= 0 Then .PrimaryKey = New System.Data.DataColumn() {.Columns["invoice_register"], .Columns["checknum"]}

                    if (!tbl.Columns.Contains("balance"))
                    {
                        var col = tbl.Columns.Add("balance", typeof(decimal), "[inv_amount]-[pmt_amount]-[adj_amount]");
                        col.DefaultValue = 0m;
                        col.ReadOnly = true;
                        col.Caption = "Balance";
                    }

                    GridControl1.DataSource = tbl;
                    GridControl1.RefreshDataSource();

                    // Correct the outstanding balance information
                    label_net_invoice_amt.Text = System.String.Format("{0:c}", BalanceTotal());
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = prior_cursor;
            }
        }

        /// <summary>
        /// Generate the select command to retrieve the invoice data
        /// </summary>
        private System.Data.SqlClient.SqlCommand SelectCommand()
        {
            var cmd = new SqlCommand("SELECT ri.invoice_register, tr.checknum, isnull(ri.inv_amount,0) as inv_amount, ri.inv_date, isnull(ri.pmt_amount,0) as pmt_amount, ri.pmt_date, isnull(ri.adj_amount,0) as adj_amount, ri.adj_date FROM registers_invoices ri WITH (NOLOCK) RIGHT OUTER JOIN registers_creditor rc WITH (NOLOCK) ON rc.invoice_register = ri.invoice_register AND rc.tran_type IN ('AD','BW','MD','CM') LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON tr.trust_register = rc.trust_register WHERE ri.creditor=@creditor AND round(isnull(ri.inv_amount,0)-isnull(ri.adj_amount,0)-isnull(ri.pmt_amount,0), 2) > 0 ORDER BY ri.inv_date, ri.invoice_register, tr.checknum", new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString)) { CommandType = System.Data.CommandType.Text };
            cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;
            return cmd;
        }

        /// <summary>
        /// A row was clicked to change the focus
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            EnableOK();
        }

        /// <summary>
        /// Double Click on an invoice -- pay it
        /// </summary>

        private void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv             = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl                    = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 ControlRow                                = hi.RowHandle;

            // If there is a row then process the double-click event. Ignore it if there is no row.
            System.Data.DataRow EditRow = null;
            if (ControlRow >= 0)
            {
                DataTable tbl = (DataTable)ctl.DataSource;
                EditRow = (System.Data.DataRow)gv.GetDataRow(ControlRow);
            }

            // From the datasource, we can find the client in the table.
            if (EditRow != null)
            {
                if (Convert.ToDecimal(EditRow["balance"]) > 0m)
                {
                    PayInvoice(EditRow);
                    EnableOK();
                }
            }
        }

        /// <summary>
        /// Process the edit of the invoice data
        /// </summary>
        private void Button_Select_Click(object sender, System.EventArgs e)
        {
            PayInvoice((System.Data.DataRow)GridView1.GetDataRow(GridView1.FocusedRowHandle));
            EnableOK();
        }

        #endregion " Grid Control "

        /// <summary>
        /// Initialize the form
        /// </summary>

        private void Form_InvoicePayments_Load(object sender, System.EventArgs e)
        {
            // Try to restore the layout from the registry settings.
            ReloadGridControlLayout();

            // Disable the buttons until a creditor is entered
            EnableOK();
        }

        /// <summary>
        /// Process a change in the check amount
        /// </summary>
        private void txt_check_amount_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            UnRegisterHandlers();
            try
            {
                Text_ReferenceInfo.Text = System.String.Empty;
                non_ar_payments = 0m;
                ar_payments = 0m;
            }
            finally
            {
                RegisterHandlers();
            }
            EnableOK();
        }

        /// <summary>
        /// Find the balance owed on an invoice
        /// </summary>
        private decimal invoice_balance(System.Int32 invoice_register)
        {
            decimal answer = 0m;

            try
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "SELECT round(isnull(inv_amount,0)-isnull(adj_amount,0)-isnull(pmt_amount,0),2) as 'balance' FROM registers_invoices WITH (NOLOCK) WHERE invoice_register=@invoice_register";
                        cmd.Parameters.Add("@invoice_register", SqlDbType.Int).Value = invoice_register;
                        object objAnswer = cmd.ExecuteScalar();
                        if (objAnswer != null && !object.ReferenceEquals(objAnswer, System.DBNull.Value))
                            answer = Convert.ToDecimal(objAnswer);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading invoice balance");
            }

            return answer;
        }

        /// <summary>
        /// Update the net_invoice amount to reflect the current total
        /// </summary>
        private void PayInvoice(System.Data.DataRow row)
        {
            System.Int32 invoice_register = Convert.ToInt32(row["invoice_register"]);

            // Find the balance to pay on the invoice
            decimal Balance = invoice_balance(invoice_register);
            if (Balance <= 0m)
            {
                DebtPlus.Data.Forms.MessageBox.Show("That invoice is paid. Please choose a different one.", "Data Entry Error", MessageBoxButtons.OK);
            }
            else
            {
                // Determine the information for the invoice
                decimal amt_paid = 0m;
                decimal amt_adj = 0m;
                using (var frm = new From_ItemEdit(unspent_funds, row))
                {
                    if (frm.ShowDialog() != System.Windows.Forms.DialogResult.OK)
                    {
                        return;
                    }
                    amt_paid = Convert.ToDecimal(frm.CalcEdit5.EditValue);
                    amt_adj = Convert.ToDecimal(frm.CalcEdit6.EditValue);
                }

                // Update the invoice with the changed information
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                System.Data.SqlClient.SqlTransaction txn = null;

                try
                {
                    cn.Open();
                    txn = cn.BeginTransaction();

                    row.BeginEdit();

                    // Pay on this invoice
                    if (amt_paid > 0m)
                    {
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.Transaction = txn;
                            cmd.CommandText = "xpr_creditor_contribution_invoice";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@invoice_register", SqlDbType.Int).Value = invoice_register;
                            cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = amt_paid;
                            cmd.Parameters.Add("@message", SqlDbType.VarChar, 80).Value = Text_ReferenceInfo.Text.Trim();
                            cmd.ExecuteNonQuery();
                        }

                        // Correct the invoice parameters
                        row["pmt_amount"] = Convert.ToDecimal(row["pmt_amount"]) + amt_paid;
                        row["pmt_date"] = DateTime.Now;
                        ar_payments += amt_paid;
                    }

                    // Adjust the invoice as needed
                    if (amt_adj > 0m)
                    {
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.Transaction = txn;
                            cmd.CommandText = "xpr_creditor_contribution_adj";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@invoice_register", SqlDbType.Int).Value = invoice_register;
                            cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = amt_adj;
                            cmd.Parameters.Add("@reference", SqlDbType.VarChar, 80).Value = Text_ReferenceInfo.Text.Trim();
                            cmd.ExecuteNonQuery();
                        }

                        // Correct the invoice parameters
                        row["adj_amount"] = Convert.ToDecimal(row["adj_amount"]) + amt_adj;
                        row["adj_date"] = DateTime.Now;
                    }

                    // Commit the update of the row
                    row.EndEdit();
                    row.AcceptChanges();

                    label_net_invoice_amt.Text = System.String.Format("{0:c}", BalanceTotal());
                    GridControl1.RefreshDataSource();

                    // Commit the changes to the database
                    txn.Commit();
                    txn.Dispose();
                    txn = null;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating invoice information");
                }
                finally
                {
                    if (row != null)
                    {
                        row.CancelEdit();
                    }

                    if (txn != null)
                    {
                        try
                        {
                            txn.Rollback();
                        }
                        catch { }
                        txn.Dispose();
                        txn = null;
                    }

                    if (cn != null)
                    {
                        cn.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// Handle the condition where the creditor ID was changed
        /// </summary>
        private void Text_CreditorID_Validated(object sender, System.EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                Text_Amount.EditValue = 0m;
                Text_ReferenceInfo.EditValue = null;
            }
            finally
            {
                RegisterHandlers();
            }

            ReadCreditor();
            EnableOK();
        }

        /// <summary>
        /// Fetch the creditor information from the creditor_id field
        /// </summary>

        private void ReadCreditor()
        {
            string CreditorAddress = ReadCreditor_Address();
            if (CreditorAddress == string.Empty)
            {
                CreditorAddress = ReadCreditor_Name();
            }

            Label_CreditorAddress.Text = CreditorAddress;
            if (CreditorAddress != string.Empty)
            {
                RefreshInvoiceList();
            }
            else
            {
                GridControl1.DataSource = null;
                GridControl1.RefreshDataSource();
                GridView1.FocusedRowHandle = -1;
            }
        }

        private string ReadCreditor_Name()
        {
            string answer = string.Empty;
            System.Data.DataTable tbl = ds.Tables["creditors"];
            System.Data.DataRow row = null;

            if (tbl != null)
            {
                row = tbl.Rows.Find(creditor);
                if (row != null)
                {
                    if ((!object.ReferenceEquals(row["creditor_name"], System.DBNull.Value)))
                        answer = Convert.ToString(row["creditor_name"]).Trim();
                    return answer;
                }
            }

            try
            {
                // Attempt to read the creditor from the list.
                System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                cmd.CommandText = "SELECT creditor, creditor_name FROM creditors WITH (NOLOCK) WHERE creditor = @creditor";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                da.Fill(ds, "creditors");
                tbl = ds.Tables["creditors"];
                if (tbl.PrimaryKey.Length <= 0)
                    tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["creditor"] };
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditors table");
            }

            row = tbl.Rows.Find(creditor);
            if (row != null)
            {
                if ((!object.ReferenceEquals(row["creditor_name"], System.DBNull.Value)))
                    answer = Convert.ToString(row["creditor_name"]).Trim();
            }

            return answer;
        }

        private string ReadCreditor_Address()
        {
            string answer = string.Empty;
            System.Data.DataTable tbl = ds.Tables["creditor_addresses"];
            System.Data.DataRow row = null;

            // If the entry is loaded from previous use, then reuse the information
            if (tbl != null)
            {
                row = tbl.Rows.Find(creditor);
                if (row != null)
                {
                    if ((!object.ReferenceEquals(row["creditor_address"], System.DBNull.Value)))
                        answer = Convert.ToString(row["creditor_address"]).Trim();
                }
            }

            if (answer == string.Empty)
            {
                try
                {
                    // Attempt to read the creditor from the list.
                    System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT creditor, dbo.address_block(attn, addr1, addr2, addr3, addr6) as creditor_address FROM view_creditor_addresses WITH (NOLOCK) WHERE creditor = @creditor AND type = 'P'";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;

                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                    da.Fill(ds, "creditor_addresses");
                    tbl = ds.Tables["creditor_addresses"];
                    if (tbl.PrimaryKey.Length <= 0)
                        tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["creditor"] };
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditor addresss table");
                }

                row = tbl.Rows.Find(creditor);
                if (row != null)
                {
                    if ((!object.ReferenceEquals(row["creditor_address"], System.DBNull.Value)))
                        answer = Convert.ToString(row["creditor_address"]).Trim();
                }
            }

            return answer;
        }

        private void Text_Amount_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            e.Cancel = e.NewValue == null || object.ReferenceEquals(e.NewValue, System.DBNull.Value) || Convert.ToDecimal(e.NewValue) <= 0m;
        }
    }
}