using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing.Payments
{
    public partial class Form_NonAR : DebtPlus.Data.Forms.DebtPlusForm
    {
        private decimal MaximumAvailable = 0m;

        public Form_NonAR() : base()
        {
            InitializeComponent();
            this.Load += Form_NonAR_Load;
        }

        public Form_NonAR(decimal MaxFunds) : this()
        {
            MaximumAvailable = MaxFunds;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl LabelControl5;
        internal DevExpress.XtraEditors.LabelControl label_amt_available;
        internal DevExpress.XtraEditors.CalcEdit txt_contribution;
        internal DevExpress.XtraEditors.MemoEdit MemoEdit1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraEditors.LookUpEdit lst_LedgerCode;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_NonAR));
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.label_amt_available = new DevExpress.XtraEditors.LabelControl();
            this.txt_contribution = new DevExpress.XtraEditors.CalcEdit();
            this.lst_LedgerCode = new DevExpress.XtraEditors.LookUpEdit();
            this.MemoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.txt_contribution.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.lst_LedgerCode.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.MemoEdit1.Properties).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LabelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.LabelControl1.Location = new System.Drawing.Point(0, 0);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Padding = new System.Windows.Forms.Padding(8);
            this.LabelControl1.Size = new System.Drawing.Size(340, 29);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Enter the information to make a contribution to the non-AR system.";
            this.LabelControl1.ToolTipController = this.ToolTipController1;
            //
            //LabelControl2
            //
            this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl2.Location = new System.Drawing.Point(8, 32);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(83, 13);
            this.LabelControl2.TabIndex = 1;
            this.LabelControl2.Text = "Amount Available";
            this.LabelControl2.ToolTipController = this.ToolTipController1;
            //
            //LabelControl3
            //
            this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl3.Location = new System.Drawing.Point(8, 60);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(106, 13);
            this.LabelControl3.TabIndex = 3;
            this.LabelControl3.Text = "Amount To Contribute";
            this.LabelControl3.ToolTipController = this.ToolTipController1;
            //
            //LabelControl4
            //
            this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl4.Location = new System.Drawing.Point(8, 92);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(75, 13);
            this.LabelControl4.TabIndex = 5;
            this.LabelControl4.Text = "Ledger Account";
            this.LabelControl4.ToolTipController = this.ToolTipController1;
            //
            //LabelControl5
            //
            this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
            this.LabelControl5.Location = new System.Drawing.Point(8, 120);
            this.LabelControl5.Name = "LabelControl5";
            this.LabelControl5.Size = new System.Drawing.Size(36, 13);
            this.LabelControl5.TabIndex = 7;
            this.LabelControl5.Text = "Reason";
            this.LabelControl5.ToolTipController = this.ToolTipController1;
            //
            //label_amt_available
            //
            this.label_amt_available.Appearance.Options.UseTextOptions = true;
            this.label_amt_available.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.label_amt_available.Location = new System.Drawing.Point(128, 32);
            this.label_amt_available.Name = "label_amt_available";
            this.label_amt_available.Size = new System.Drawing.Size(28, 13);
            this.label_amt_available.TabIndex = 2;
            this.label_amt_available.Text = "$0.00";
            this.label_amt_available.ToolTip = "This is the available (upper limit) that you may enter for the contribution amoun" + "t.";
            this.label_amt_available.ToolTipController = this.ToolTipController1;
            //
            //txt_contribution
            //
            this.txt_contribution.Location = new System.Drawing.Point(128, 56);
            this.txt_contribution.Name = "txt_contribution";
            this.txt_contribution.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.txt_contribution.Size = new System.Drawing.Size(93, 20);
            this.txt_contribution.TabIndex = 4;
            this.txt_contribution.ToolTip = "Enter the dollar amount that you wish to accept. Valid entries are from $0.01 to " + "the amount available (as shown above)";
            this.txt_contribution.ToolTipController = this.ToolTipController1;
            //
            //lst_LedgerCode
            //
            this.lst_LedgerCode.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.lst_LedgerCode.Location = new System.Drawing.Point(128, 88);
            this.lst_LedgerCode.Name = "lst_LedgerCode";
            this.lst_LedgerCode.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.lst_LedgerCode.Properties.NullText = "Please choose a ledger account...";
            this.lst_LedgerCode.Size = new System.Drawing.Size(200, 20);
            this.lst_LedgerCode.TabIndex = 6;
            this.lst_LedgerCode.ToolTip = "Choose the ledger account associated with the contribution from this list";
            this.lst_LedgerCode.ToolTipController = this.ToolTipController1;
            //
            //MemoEdit1
            //
            this.MemoEdit1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.MemoEdit1.Location = new System.Drawing.Point(128, 120);
            this.MemoEdit1.Name = "MemoEdit1";
            this.MemoEdit1.Size = new System.Drawing.Size(200, 96);
            this.MemoEdit1.TabIndex = 8;
            this.MemoEdit1.ToolTip = "Enter a textual description here. You have 512 characters to make an entry";
            this.MemoEdit1.ToolTipController = this.ToolTipController1;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Button_OK.Location = new System.Drawing.Point(87, 232);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 9;
            this.Button_OK.Text = "&OK";
            this.Button_OK.ToolTip = "Click here to submit the changes to the database";
            this.Button_OK.ToolTipController = this.ToolTipController1;
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Cancel.Location = new System.Drawing.Point(191, 232);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 10;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel this form and return to the previous display";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            //
            //Form_NonAR
            //
            this.AcceptButton = this.Button_OK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Cancel;
            this.ClientSize = new System.Drawing.Size(354, 268);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.MemoEdit1);
            this.Controls.Add(this.lst_LedgerCode);
            this.Controls.Add(this.txt_contribution);
            this.Controls.Add(this.label_amt_available);
            this.Controls.Add(this.LabelControl5);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(360, 300);
            this.MinimumSize = new System.Drawing.Size(360, 300);
            this.Name = "Form_NonAR";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Non-A/R Creditor Contributions";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.txt_contribution.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.lst_LedgerCode.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.MemoEdit1.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private void Form_NonAR_Load(object sender, System.EventArgs e)
        {
            // Format the amount that we have to use
            label_amt_available.Text = System.String.Format("{0:c2}", MaximumAvailable);

            // Load the list of ledger codes
            Load_LedgerCode();

            // Define the contribution information
            txt_contribution.EditValue = 0m;
            txt_contribution.EditValueChanged += Form_Changed;

            // Add the handler when the text value is changed
            label_amt_available.TextChanged += Form_Changed;

            // Complete the processing by enabling the OK button
            Enable_OK();
        }

        private void Form_Changed(object sender, System.EventArgs e)
        {
            Enable_OK();
        }

        private void Enable_OK()
        {
            Button_OK.Enabled = true;

            // Check the range for the decimal input
            if (Convert.ToDecimal(txt_contribution.EditValue) <= 0m || Convert.ToDecimal(txt_contribution.EditValue) > MaximumAvailable)
            {
                DxErrorProvider1.SetError(txt_contribution, System.String.Format("The value is out of the allowed range. It must be between $0.01 and {0:c2}", MaximumAvailable));
                Button_OK.Enabled = false;
            }
            else
            {
                DxErrorProvider1.SetError(txt_contribution, "");
            }

            // There should be a ledger account
            if (lst_LedgerCode.EditValue == null)
            {
                DxErrorProvider1.SetError(lst_LedgerCode, "A ledger account is required");
                Button_OK.Enabled = false;
            }
            else
            {
                DxErrorProvider1.SetError(lst_LedgerCode, "");
            }
        }

        #region " lst_ledger_code "

        private void Load_LedgerCode()
        {
            // Load the source ledger code table
            System.Data.DataView ledger_vue = LedgerCodesTableView();
            var _with1 = lst_LedgerCode;
            var _with2 = _with1.Properties;
            _with2.DataSource = ledger_vue;
            _with2.PopulateColumns();
            _with2.Columns["Default"].Visible = false;
            // Do not display the default column
            _with2.Columns["ActiveFlag"].Visible = false;
            // Do not display the active flag
            _with2.DisplayMember = "Description";
            // Show the description
            _with2.ValueMember = "Ledger Code";
            // but the ledger code is what we want
            _with2.NullText = "";
            // do not show anything if there is no item

            // Try to select the default item
            using (System.Data.DataView vue = new System.Data.DataView(ledger_vue.Table, "Default<>0", "", DataViewRowState.CurrentRows))
            {
                if (vue.Count > 0)
                    _with1.EditValue = vue[0]["Ledger Code"];
            }

            // Bind the controls
            _with1.EditValueChanging += lst_LedgerCode_EditValueChanging;
            _with1.EditValueChanged += Form_Changed;
        }

        private void lst_LedgerCode_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            // If the new value is defined then make sure that the office is still active.
            var _with3 = (DevExpress.XtraEditors.LookUpEdit)sender;

            if (e.NewValue != null)
            {
                // Find the new office being selected
                string IDKey = Convert.ToString(e.NewValue);
                DataRowView drv = (System.Data.DataRowView)_with3.Properties.GetDataSourceRowByKeyValue(IDKey);

                // From the office, ensure that the active flag is set
                if (!Convert.ToBoolean(drv["ActiveFlag"]))
                {
                    e.Cancel = true;
                    DebtPlus.Data.Forms.MessageBox.Show("The ledger account is no longer active. Please do not use this account.", "Ledger code selection is not valid", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                }
            }
        }

        private System.Data.DataView LedgerCodesTableView()
        {
            System.Data.DataView answer = null;
            System.Data.DataSet ds = new System.Data.DataSet("ledger_codes");

            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            var _with4 = cmd;
            _with4.Connection = cn;
            _with4.CommandText = "SELECT [ledger_code] as [Ledger Code],[description] as Description, 1 as [ActiveFlag], 0 as [Default] FROM ledger_codes WITH (NOLOCK) ORDER BY description";

            // Fill the ledger codes
            try
            {
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                da.Fill(ds, "ledger_codes");
                answer = ds.Tables[0].DefaultView;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.Message, "Error loading ledger codes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }

            return answer;
        }

        #endregion " lst_ledger_code "
    }
}