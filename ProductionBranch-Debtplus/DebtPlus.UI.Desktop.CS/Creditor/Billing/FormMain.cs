using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Threading;
using System.Text;
using DevExpress.Utils;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace DebtPlus.UI.Desktop.CS.Creditor.Billing
{
    internal partial class FormMain
    {
        private BackgroundWorker bt = new BackgroundWorker();
        private readonly Billing bill = new Billing();
        private Int32 _checksProcessed = 0;

        private readonly CreditorBillingContext ctx = null;

        public FormMain(CreditorBillingContext ctx) : base()
        {
            this.ctx = ctx;
            InitializeComponent();

            bt.DoWork += bt_DoWork;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
            Button_Print.Click += Button_Print_Click;
            Button_Cancel.Click += Button_Cancel_Click;
        }

        /// <summary>
        /// Information used to talk to the billing status form
        /// </summary>
        internal partial class Billing
        {
            public bool Cancelled = false;

            // Status for the various groups of creditors
            public bool Check_Annually;

            public bool Check_SemiAnnually;
            public bool Check_Quarterly;
            public bool Check_Monthly;
            public bool Check_Weekly;

            public bool Check_Ignore;
            public string Period_Annually;
            public string Period_SemiAnnually;
            public string Period_Quarterly;
            public string Period_Monthly;

            public string Period_Weekly;
            private FormStatus privateStatusForm = null;
            private Int32 privateInvoiceRegister = 0;
            private decimal privateBilled = 0m;

            private string privateLastCreditor = string.Empty;

            public FormStatus StatusForm
            {
                get
                {
                    if (privateStatusForm == null || privateStatusForm.IsDisposed)
                    {
                        privateStatusForm = new FormStatus(this);
                        privateStatusForm.Show();
                    }
                    return privateStatusForm;
                }
            }

            internal Int32 InvoiceRegister
            {
                get { return privateInvoiceRegister; }
                set
                {
                    privateInvoiceRegister = value;
                    SetLabelInvoice();
                }
            }

            private void SetLabelInvoice()
            {
                if (StatusForm.InvokeRequired)
                {
                    StatusForm.Invoke(new MethodInvoker(SetLabelInvoice));
                }
                else
                {
                    StatusForm.Label_Invoice.Text = string.Format("{0:d8}", InvoiceRegister);
                }
            }

            internal decimal BilledAmount
            {
                get { return privateBilled; }
                set
                {
                    privateBilled = value;
                    SetLabelAmount();
                }
            }

            private void SetLabelAmount()
            {
                if (StatusForm.InvokeRequired)
                {
                    StatusForm.Invoke(new MethodInvoker(SetLabelAmount));
                }
                else
                {
                    StatusForm.Label_Amount.Text = string.Format("{0:c}", BilledAmount);
                }
            }

            internal string LastCreditor
            {
                get { return privateLastCreditor; }
                set
                {
                    privateLastCreditor = value;
                    SetLastCreditor();
                }
            }

            private void SetLastCreditor()
            {
                if (StatusForm.InvokeRequired)
                {
                    StatusForm.Invoke(new MethodInvoker(SetLastCreditor));
                }
                else
                {
                    StatusForm.Label_Creditor.Text = LastCreditor;
                }
            }
        }

        /// <summary>
        /// Handle the GO button to start the operation
        /// </summary>
        private void Button_Print_Click(object sender, EventArgs e)
        {
            Button_Print.Enabled = false;
            UseWaitCursor = true;

            // Create the billing class and supply the members
            bill.Check_Annually = (Check_Annual.CheckState != CheckState.Unchecked);
            bill.Check_SemiAnnually = (Check_SemiAnnual.CheckState != CheckState.Unchecked);
            bill.Check_Quarterly = (Check_Quarterly.CheckState != CheckState.Unchecked);
            bill.Check_Monthly = (Check_Monthly.CheckState != CheckState.Unchecked);
            bill.Check_Weekly = (Check_Weekly.CheckState != CheckState.Unchecked);
            bill.Check_Ignore = (Check_Ignore.CheckState != CheckState.Unchecked);

            bill.Period_Annually = Period_Annually.Text.Trim();
            bill.Period_SemiAnnually = Period_SemiAnnually.Text.Trim();
            bill.Period_Quarterly = Period_Quarterly.Text.Trim();
            bill.Period_Monthly = Period_Monthly.Text.Trim();
            bill.Period_Weekly = Period_Weekly.Text.Trim();

            // Load the status form in the main thread
            bill.StatusForm.Show();

            // Start the backgroun thread to process the generation logic
            bt.RunWorkerAsync(bill);
        }

        /// <summary>
        /// The invoices have been created.
        /// </summary>
        private void bt_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bill.StatusForm.Close();
            UseWaitCursor = false;
            Close();
        }

        /// <summary>
        /// Background thread to do the work of creating invoices
        /// </summary>

        private void bt_DoWork(object sender, DoWorkEventArgs e)
        {
            // Set the pointer to the billing class
            Billing bill = e.Argument as Billing;
            if (bill == null)
            {
                e.Cancel = true;
                return;
            }

            // Determine if we need to create a set of invoices first.
            if (bill.Check_Annually || bill.Check_SemiAnnually || bill.Check_Quarterly || bill.Check_Monthly || bill.Check_Weekly)
            {
                // Generate a database connection to process the outer objects
                using (SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    if (Create_Invoice_list(cn, null))
                    {
                        Process_Creditor_List(cn, null);
                    }
                    else
                    {
                        // Print the report operation if it is not cancelled
                        if (bill.Cancelled)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }

            e.Cancel = false;

            var thrd = new Thread(PrintInvoices_Thread);
            thrd.SetApartmentState(ApartmentState.STA);
            thrd.Name = "PrintInvoices_Thread";
            thrd.Start();
        }

        /// <summary>
        /// Stop the program if the CANCEL button is pressed
        /// </summary>
        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Allocate a new invoice number from the system
        /// </summary>
        private Int32 new_invoice(ref SqlConnection cn, ref SqlTransaction txn, string creditor)
        {
            Int32 result = 0;

            // Build the command block to create the invoice
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "xpr_insert_registers_invoice";
                cmd.CommandType = CommandType.StoredProcedure;

                // Include the parameters for the request
                cmd.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;
                cmd.Parameters.Add("@inv_amount", SqlDbType.Decimal).Value = 0m;
                cmd.Parameters.Add("@adj_amount", SqlDbType.Decimal).Value = 0m;
                cmd.Parameters.Add("@pmt_amount", SqlDbType.Decimal).Value = 0m;

                // Execute the request
                cmd.ExecuteNonQuery();
                object obj = cmd.Parameters[0].Value;
                if (obj != null)
                {
                    result = Convert.ToInt32(obj);
                }
            }

            return result;
        }

        /// <summary>
        /// Update the invoice information for later printing
        /// </summary>
        private void CloseInvoice(ref SqlConnection cn, ref SqlTransaction txn, Int32 InvoiceRegister)
        {
            // Build the command block to complete the invoice
            string strSql = "UPDATE registers_invoices SET inv_amount=@inv_amount,inv_date=getdate() WHERE invoice_register=@invoice_register";
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = strSql;

                // Include the parameters for the request
                cmd.Parameters.Add("@inv_amount", SqlDbType.Decimal).Value = bill.BilledAmount;
                cmd.Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister;

                // Update the invoice
                cmd.ExecuteNonQuery();
            }

            // Update the mtd statistics for the creditor
            strSql = "UPDATE creditors SET contrib_mtd_billed=contrib_mtd_billed + @billed_amount WHERE creditor=@creditor";

            // Build the command block
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = strSql;

                // Include the parameters for the request
                cmd.Parameters.Add("@billed_amount", SqlDbType.Decimal).Value = bill.BilledAmount;
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = bill.LastCreditor;

                // Update the invoice
                cmd.ExecuteNonQuery();
            }
        }

        private SqlDataReader rd = null;

        /// <summary>
        /// Create a table showing the creditors with pending invoices
        /// </summary>
        public void Process_Creditor_List(SqlConnection main_cn, SqlTransaction main_txn)
        {
            // Open a new connection to process the items for the invoices
            using (SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                cn.Open();

                // Allocate a cursor to process the transaction
                StringBuilder commandString = new StringBuilder();

                commandString.Append("SELECT isnull(d.creditor_register,0) as CreditorRegister, isnull(d.client_creditor_register,0) as DebtRegister, isnull(d.creditor,'') as Creditor, isnull(d.fairshare_amt,0) as FairshareAmt, isnull(d.trust_register,0) as TrustRegister, isnull(t.ChecksPerInvoice,0) as ChecksPerInvoice");
                commandString.Append(" FROM registers_client_creditor d");
                commandString.Append(" INNER JOIN #invoice_list i ON d.creditor = i.creditor AND d.creditor_type in ('B', i.creditor_type)");
                commandString.Append(" INNER JOIN #creditor_list t ON d.creditor = t.creditor");
                commandString.Append(" WHERE d.tran_type in ('AD', 'BW', 'MD', 'CM')");
                commandString.Append(" AND d.invoice_register IS NULL");
                commandString.Append(" AND d.fairshare_amt > 0");
                commandString.Append(" AND d.void = 0");
                commandString.Append(" ORDER BY d.creditor, d.trust_register;");

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandTimeout = 0;
                    cmd.Connection = main_cn;
                    cmd.Transaction = main_txn;

                    cmd.CommandText = commandString.ToString();

                    using (rd = cmd.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.CloseConnection))
                    {
                        if (rd.Read())
                        {
                            // Process all invoices, one at a time until we have no more to generate
                            do { } while (ProcessCurrentInvoice(cn));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Process a single creditor from the list of creditors
        /// </summary>
        private bool ProcessCurrentInvoice(SqlConnection cn)
        {
            bool answer = true;

            // Input parameters
            Int32 LastCreditorRegister = default(Int32);

            // Parameters for the current invoice
            Int32 LastTrustRegister = default(Int32);
            string CommandString = null;

            // Pending transaction
            SqlTransaction txn = null;

            // Process the records until we need to start a new invoice
            string Creditor = rd.GetString(rd.GetOrdinal("Creditor"));
            Int32 TrustRegister = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("TrustRegister")));
            Int32 ChecksPerInvoice = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("ChecksPerInvoice")));

            // Start a transaction for the operations
            try
            {
                txn = cn.BeginTransaction();

                // Allocate a new invoice for the operation
                bill.InvoiceRegister = new_invoice(ref cn, ref txn, Creditor);
                if (bill.InvoiceRegister <= 0)
                    bill.Cancelled = true;

                // Save the transaction information
                bill.LastCreditor = Creditor;
                bill.BilledAmount = 0m;
                LastTrustRegister = 0;
                LastCreditorRegister = 0;
                _checksProcessed = 0;

                do
                {
                    // Retrieve the current parameters from the recordset. Stop when the creditor changes.
                    Creditor = rd.GetString(rd.GetOrdinal("Creditor"));
                    if (string.Compare(Creditor, bill.LastCreditor, true) != 0)
                        break;

                    // Process the details for this invoice
                    TrustRegister = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("TrustRegister")));
                    Int32 DebtRegister = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("DebtRegister")));
                    Int32 CreditorRegister = Convert.ToInt32(rd.GetValue(rd.GetOrdinal("CreditorRegister")));
                    decimal FairshareAmt = rd.GetDecimal(rd.GetOrdinal("FairshareAmt"));

                    Application.DoEvents();
                    if (bill.Cancelled)
                    {
                        try
                        {
                            txn.Rollback();
                        }
                        catch (Exception exRollback)
                        {
                            DebtPlus.Svc.Common.ErrorHandling.HandleErrors(exRollback);
                        }

                        txn.Dispose();
                        txn = null;
                        return false;
                    }

                    // If the trust register is different then count the checks.
                    if (LastTrustRegister != TrustRegister)
                    {
                        _checksProcessed += 1;
                        if (ChecksPerInvoice > 0 && ChecksPerInvoice < _checksProcessed)
                            break;
                        LastTrustRegister = TrustRegister;

                        // Set the invoice pointer in the trust register.
                        CommandString = "UPDATE registers_trust SET invoice_register=@invoice_register WHERE trust_register=@trust_register";
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.Transaction = txn;
                            cmd.CommandText = CommandString;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister;
                            cmd.Parameters.Add("@trust_register", SqlDbType.Int).Value = LastTrustRegister;
                            cmd.ExecuteNonQuery();
                        }
                    }

                    // Update the invoice register in the creditor register for this invoice
                    if (CreditorRegister > 0 && CreditorRegister != LastCreditorRegister)
                    {
                        LastCreditorRegister = CreditorRegister;
                        CommandString = "UPDATE registers_creditor SET invoice_register=@invoice_register WHERE creditor_register=@creditor_register";
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.Transaction = txn;
                            cmd.CommandText = CommandString;
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister;
                            cmd.Parameters.Add("@creditor_register", SqlDbType.Int).Value = LastCreditorRegister;
                            cmd.ExecuteNonQuery();
                        }
                    }

                    // Set the invoice register into the transaction. This will mark the transaction as being processed
                    CommandString = "UPDATE registers_client_creditor SET invoice_register=@invoice_register WHERE client_creditor_register=@client_creditor_register";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = CommandString;
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@invoice_register", SqlDbType.Int).Value = bill.InvoiceRegister;
                        cmd.Parameters.Add("@client_creditor_register", SqlDbType.Int).Value = DebtRegister;
                        cmd.ExecuteNonQuery();
                    }

                    // Add to the total invoice amount the figure for the current transaction.
                    bill.BilledAmount += FairshareAmt;

                    // Read the next detail record. At the end, stop the loop
                    if (!rd.Read())
                    {
                        answer = false;
                        break;
                    }
                } while (true);

                // Close out the current invoice. If we stopped in middle processing, then
                // the next input record is left as the starting values for the new invoice.
                CloseInvoice(ref cn, ref txn, bill.InvoiceRegister);
                txn.Commit();
                txn.Dispose();
                txn = null;
            }
            finally
            {
                if (txn != null)
                {
                    txn.Rollback();
                    txn.Dispose();
                }
            }

            return answer;
        }

        /// <summary>
        /// Do any followup cleanup operations
        /// </summary>
        public void Cleanup_Processing(SqlConnection cn, SqlTransaction txn)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;

                    cmd.CommandText = "DROP TABLE #creditor_list";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                // we don't care about these erorrs
            }

            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "DROP TABLE #invoice_list";
                    cmd.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                // we don't care about these erorrs
            }
        }

        /// <summary>
        /// Create the list of creditors for billing
        /// </summary>

        public void CreateBillingCreditors(SqlConnection cn, SqlTransaction txn)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "CREATE TABLE #creditor_list (creditor varchar(10), contrib_bill_month int, ChecksPerInvoice int, min_accept_per_bill Money, special_processing int);";
                cmd.ExecuteNonQuery();
            }

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "CREATE UNIQUE CLUSTERED INDEX ix2_t_billing_creditors on #creditor_list ( creditor );";
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Create the list of creditor invoices that are outstanding
        /// </summary>

        public void CreateBillingCounts(SqlConnection cn, SqlTransaction txn)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "create table #invoice_list ( creditor varchar(10), creditor_type varchar(1), outstanding_bills money );";
                cmd.ExecuteNonQuery();
            }

            // The first index is the creditor and type
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "create unique clustered index ix2_t_billing_counts on #invoice_list ( creditor, creditor_type );";
                cmd.ExecuteNonQuery();
            }

            // Augment this with an index for the creditor only
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "create index ix3_t_billing_counts on #invoice_list ( creditor );";
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Toss creditors from the list when they are not on the proper cycle
        /// </summary>
        private void RemoveCreditors(SqlConnection cn, SqlTransaction txn, string CreditorType, bool chk, string txt)
        {
            string CommandString = null;

            if (chk)
            {
                // If a period is given then delete all creditors which are not in the desired period
                if (txt != string.Empty)
                {
                    Int32 ContribBillMonth = Convert.ToInt32(txt);

                    if (ContribBillMonth > 0)
                    {
                        CommandString = "DELETE #invoice_list FROM #invoice_list i INNER JOIN #creditor_list cr ON cr.creditor = i.creditor WHERE cr.contrib_bill_month <> @contrib_bill_month AND i.creditor_type = @creditor_type;";
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.Transaction = txn;
                            cmd.CommandText = CommandString;
                            cmd.Parameters.Add("@contrib_bill_month", SqlDbType.Int).Value = ContribBillMonth;
                            cmd.Parameters.Add("@creditor_type", SqlDbType.VarChar, 1).Value = CreditorType;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            else
            {
                // The creditor type is not to be processed. Ignore the invoices for this creditor
                CommandString = "DELETE FROM #invoice_list WHERE creditor_type=@creditor_type;";
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = CommandString;
                    cmd.Parameters.Add("@creditor_type", SqlDbType.VarChar, 1).Value = CreditorType;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Insert the creditors for later processing
        /// </summary>

        private void InsertCreditors(SqlConnection cn, SqlTransaction txn)
        {
            // Insert the creditor into the list of possible creditors
            StringBuilder CommandString = new StringBuilder();

            CommandString.Append("INSERT INTO #creditor_list (creditor, contrib_bill_month, ChecksPerInvoice, min_accept_per_bill, special_processing)");
            CommandString.Append(" SELECT cr.creditor as 'creditor', cr.contrib_bill_month,");
            CommandString.Append(" coalesce(cr.chks_per_invoice,cnf.chks_per_invoice,1) as 'ChecksPerInvoice',");
            CommandString.Append(" isnull(cr.min_accept_per_bill,0) as 'min_accept_per_bill',");
            CommandString.Append(" 0 as 'special_processing'");
            CommandString.Append(" FROM creditors cr WITH (NOLOCK)");
            CommandString.Append(" CROSS JOIN config cnf WITH (NOLOCK)");

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandTimeout = 0;
                cmd.CommandText = CommandString.ToString();
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Insert the invoices for later processing
        /// </summary>

        private void InsertInvoices(SqlConnection cn, SqlTransaction txn)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                StringBuilder CommandString = new StringBuilder();

                // Find the list of creditors that have outstanding invoices for the various billing periods
                CommandString.Append("INSERT INTO #invoice_list (creditor, creditor_type, outstanding_bills)");
                CommandString.Append(" SELECT d.creditor, creditor_type, SUM(d.fairshare_amt) AS 'outstanding_bills'");
                CommandString.Append(" FROM registers_client_creditor d");
                CommandString.Append(" WHERE d.tran_type in ('AD', 'BW', 'MD', 'CM')");
                CommandString.Append(" AND d.invoice_register IS NULL");
                CommandString.Append(" AND d.creditor_type NOT IN ('D','N')");
                CommandString.Append(" AND d.fairshare_amt > 0");
                CommandString.Append(" AND d.void = 0");
                CommandString.Append(" GROUP BY creditor, creditor_type;");

                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandTimeout = 0;
                cmd.CommandText = CommandString.ToString();
                cmd.ExecuteNonQuery();
            }

            // Correct the old "B" creditor types to be the billing period.
            using (SqlCommand cmd = new SqlCommand())
            {
                StringBuilder CommandString = new StringBuilder();

                // Find the list of creditors that have outstanding invoices for the various billing periods
                CommandString.Append("UPDATE #invoice_list ");
                CommandString.Append("SET [creditor_type] = cr.[contrib_cycle] ");
                CommandString.Append("FROM #invoice_list i ");
                CommandString.Append("INNER JOIN [creditors] cr ON i.[creditor] = cr.[creditor] ");
                CommandString.Append("WHERE i.[creditor_type] = 'B'");

                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandTimeout = 0;
                cmd.CommandText = CommandString.ToString();
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Create a list of invoices
        /// </summary>
        public bool Create_Invoice_list(SqlConnection cn, SqlTransaction txn)
        {
            bool answer = false;
            Int32 lRecords = 0;
            string strSQL = null;

            // Indicate that we are busy
            Cursor CurrentCursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                // Create the two working tables
                CreateBillingCreditors(cn, txn);
                CreateBillingCounts(cn, txn);

                // Load the list of creditors and the invoices
                InsertCreditors(cn, txn);
                InsertInvoices(cn, txn);

                // Remove the creditors that we don't want to invoice at this time.
                RemoveCreditors(cn, txn, "A", bill.Check_Annually, bill.Period_Annually.Trim());
                RemoveCreditors(cn, txn, "S", bill.Check_SemiAnnually, bill.Period_SemiAnnually.Trim());
                RemoveCreditors(cn, txn, "Q", bill.Check_Quarterly, bill.Period_Quarterly.Trim());
                RemoveCreditors(cn, txn, "M", bill.Check_Monthly, bill.Period_Monthly.Trim());
                RemoveCreditors(cn, txn, "W", bill.Check_Weekly, bill.Period_Weekly.Trim());

                // Discard the creditors that are too small for an invoice
                if (!bill.Check_Ignore)
                {
                    strSQL = "DELETE #invoice_list FROM #invoice_list i INNER JOIN #creditor_list cr ON i.creditor = cr.creditor WHERE IsNull(cr.min_accept_per_bill, 0) > 0 AND IsNull(cr.min_accept_per_bill,0) > IsNull(i.outstanding_bills,0)";
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = strSQL;
                        cmd.ExecuteNonQuery();
                    }
                }

                // Complete the processing with the cleanup exception handling
                try
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = "xpr_creditor_invoice_special";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                }
                catch (SqlException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                    // Do nothing with the exception. I want to ignore any error here.
                }

                // Ensure that there are some records to be processed
                strSQL = "SELECT count(*) FROM #invoice_list";
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = strSQL;
                    object objRecords = cmd.ExecuteScalar();

                    lRecords = 0;
                    if (objRecords != null && !object.ReferenceEquals(objRecords, DBNull.Value))
                        lRecords = Convert.ToInt32(objRecords);
                }
            }
            finally
            {
                Cursor.Current = CurrentCursor;
            }

            if (lRecords < 1)
            {
                DebtPlus.Data.Forms.MessageBox.Show("There are no creditors which match the criteria for billing. The existing bills will still be printed.", "Database Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
            else
            {
                answer = true;
            }

            return answer;
        }

        /// <summary>
        /// Print the invoice reports
        /// </summary>

        private DataSet ds;

        private void PrintInvoices_Thread()
        {
            using (CreditorInvoiceReport rpt = new CreditorInvoiceReport())
            {
                ds = new DataSet("ds");
                try
                {
                    rpt.BeforePrint += ReportBeforePrint;
                    rpt.DisplayPreviewDialog();
                    rpt.BeforePrint -= ReportBeforePrint;
                }
                finally
                {
                    ds.Dispose();
                }
            }
        }

        private void ReportBeforePrint(object sender, PrintEventArgs e)
        {
            CreditorInvoiceReport rpt = (CreditorInvoiceReport)sender;

            // Process the list of creditors that have pending invoices
            using (WaitDialogForm dlg = new WaitDialogForm("Reading creditor list", "Printing Invoices"))
            {
                dlg.ShowInTaskbar = true;
                dlg.Show();

                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

                    // Generate a list of the creditors, in zipcode order, that have invoices.
                    StringBuilder cmdText = new StringBuilder();
                    cmdText.Append("SELECT distinct ri.creditor, coalesce(left(a1.PostalCode,5), left(a2.PostalCode,5), '99999') as PostalCode2 ");
                    cmdText.Append("FROM registers_invoices ri ");
                    cmdText.Append("INNER JOIN creditors cr WITH (NOLOCK) ON ri.creditor = cr.creditor ");
                    cmdText.Append("LEFT OUTER JOIN creditor_addresses ca1 WITH (NOLOCK) ON cr.creditor = ca1.creditor AND 'I' = ca1.type ");
                    cmdText.Append("LEFT OUTER JOIN addresses a1 WITH (NOLOCK) ON ca1.AddressID = a1.Address ");
                    cmdText.Append("LEFT OUTER JOIN creditor_addresses ca2 WITH (NOLOCK) ON cr.creditor = ca2.creditor AND 'P' = ca2.type ");
                    cmdText.Append("LEFT OUTER JOIN addresses a2 WITH (NOLOCK) ON ca2.AddressID = a2.Address ");
                    cmdText.Append("WHERE isnull(ri.inv_amount,0) > isnull(ri.pmt_amount,0) + isnull(ri.adj_amount,0) AND isnull(cr.suppress_invoice,0) = 0 ");
                    cmdText.Append("ORDER BY 2, 1");

                    cmd.CommandText = cmdText.ToString();
                    cmd.CommandType = CommandType.Text;

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "creditors");
                    }
                }

                rpt.DataSource = ds.Tables["creditors"];
                dlg.Close();
            }
        }
    }
}