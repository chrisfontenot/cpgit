using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraBars;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds
{
    // Singleton
    internal partial class Form_CreditorRefund
    {
        private readonly CreditorRefundsContext ctx = null;

        private Form_CreditorRefund() : base()
        {
            InitializeComponent();
        }

        public Form_CreditorRefund(CreditorRefundsContext ctx) : base()
        {
            this.ctx = ctx;
            InitializeComponent();
            this.Load += Form_CreditorRefund_Load;
            BarButtonItem1.ItemClick += BarButtonItem1_ItemClick;
            this.FormClosing += Form_FormClosing;
        }

        /// <summary>
        /// Process the one-time load event for the form
        /// </summary>

        private void Form_CreditorRefund_Load(object sender, EventArgs e)
        {
            // If there is no batch then use the form realestate to ask for a batch.

            if (ctx.ap.Batch <= 0)
            {
                var _with1 = CreditorRefundControl1;
                _with1.SendToBack();
                _with1.Visible = false;

                var _with2 = NewCreditorRefundBatch1;
                _with2.Visible = true;
                _with2.BringToFront();
                _with2.ReadForm();
                _with2.Focus();
                _with2.Cancelled += NewCreditorRefundBatch1_Cancelled;
                _with2.Selected += NewCreditorRefundBatch1_Selected;
            }
            else
            {
                // Otherwise, do the deposits for this batch
                PerformDeposits();
            }
        }

        /// <summary>
        /// If the entry is to cancel the form then close it
        /// </summary>
        private void NewCreditorRefundBatch1_Cancelled(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// When a new batch is selected then process the batch updates
        /// </summary>
        private void NewCreditorRefundBatch1_Selected(object sender, EventArgs e)
        {
            ctx.ap.Batch = NewCreditorRefundBatch1.DepositBatchID;
            PerformDeposits();
        }

        /// <summary>
        /// Come here once we have a valid batch ID to start the updates
        /// </summary>

        private void PerformDeposits()
        {
            // Make the batch selection control to the back
            var _with3 = NewCreditorRefundBatch1;
            _with3.SendToBack();
            _with3.Visible = false;

            // Bring forward the batch update control. It will take it from here.
            var _with4 = CreditorRefundControl1;
            _with4.BringToFront();
            _with4.Visible = true;
            _with4.ReadForm();
            _with4.Focus();
        }

        /// <summary>
        /// Process the MENU FILE -> CLOSE operation
        /// </summary>
        private void BarButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        /// <summary>
        /// If the form is closing then check for lost updates
        /// </summary>

        private void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            // If there is no batch then simply return to close the form
            if (ctx.ap.Batch <= 0)
                return;

            // Look to see if some transactions were made. If so, ask what to do.
            if (PendingTransactions())
            {
                switch (DebtPlus.Data.Forms.MessageBox.Show("Warning: You have made changes to this batch." + Environment.NewLine + "Do you wish to save these transactions now?", "Are you sure", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1))
                {
                    case DialogResult.Cancel:
                        e.Cancel = true;

                        return;

                    case DialogResult.Yes:
                        if (SaveTransactions() < 0)
                        {
                            e.Cancel = true;
                            return;
                        }

                        if (DebtPlus.Data.Forms.MessageBox.Show("Do you also wish to close/post the batch?" + Environment.NewLine + Environment.NewLine + "(Once the batch is closed you may not make any changes to the batch. It must be closed to be posted.)", "Close the batch?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            CloseBatch();
                        }
                        break;

                    default:
                        break;
                        // Assume "no" as the response.
                }
            }
            else
            {
                if (DebtPlus.Data.Forms.MessageBox.Show("You did not make any changes to this batch" + Environment.NewLine + "Do you wish to close/post the batch anyway?" + Environment.NewLine + Environment.NewLine + "(Once the batch is closed you may not make any changes to the batch. It must be closed to be posted.)", "Close the batch?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    CloseBatch();
                }
            }

            // Ensure that re-entry calls do nothing.
            ctx.ap.Batch = 0;
        }

        /// <summary>
        /// Determine if there are transactions still pending to be recorded
        /// </summary>
        internal bool PendingTransactions()
        {
            if (ctx.ds.Tables["deposit_batch_details"] != null && ctx.ds.Tables["deposit_batch_details"].GetChanges() != null)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Save the transactions to the database
        /// </summary>
        internal Int32 SaveTransactions()
        {
            Int32 rowsUpdated = -1;
            DataTable tbl = ctx.ds.Tables["deposit_batch_details"];

            // Update the database with the changed items
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlTransaction txn = null;
            try
            {
                UseWaitCursor = true;
                cn.Open();
                txn = cn.BeginTransaction();

                SqlDataAdapter da = new SqlDataAdapter();
                da.UpdateCommand = UpdateCommand(ref cn, ref txn);
                da.InsertCommand = InsertCommand(ref cn, ref txn);
                da.DeleteCommand = DeleteCommand(ref cn, ref txn);

                // Perform the pending updates on the database
                rowsUpdated = da.Update(tbl);

                // Commit the changes
                txn.Commit();
                txn = null;
            }
#pragma warning disable 168
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show("The transaction(s) are no longer valid for this batch. The most common cause is that the batch has been posted and edits are no longer permitted.", "Database Consistency Check Violation", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
#pragma warning restore 168
            catch (Exception ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating the deposit batch details");
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch
                    {
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();
                UseWaitCursor = false;
            }

            return rowsUpdated;
        }

        /// <summary>
        /// Generate the update statement
        /// </summary>
        private SqlCommand UpdateCommand(ref SqlConnection cn, ref SqlTransaction txn)
        {
            SqlCommand cmd = new SqlCommand();
            var _with5 = cmd;
            _with5.Connection = cn;
            _with5.Transaction = txn;
            _with5.CommandText = "UPDATE deposit_batch_details SET client=cc.client, creditor=cc.creditor, client_creditor=cc.client_creditor, amount=@amount, fairshare_pct=@fairshare_pct, fairshare_amt=@fairshare_amt, creditor_type=@creditor_type, reference=@reference FROM deposit_batch_details d INNER JOIN client_creditor cc ON cc.client_creditor=@client_creditor WHERE deposit_batch_detail=@deposit_batch_detail";
            _with5.CommandType = CommandType.Text;
            _with5.Parameters.Add("@client_creditor", SqlDbType.Int, 0, "client_creditor");
            _with5.Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount");
            _with5.Parameters.Add("@fairshare_pct", SqlDbType.Float, 0, "fairshare_pct");
            _with5.Parameters.Add("@fairshare_amt", SqlDbType.Decimal, 0, "fairshare_amt");
            _with5.Parameters.Add("@creditor_type", SqlDbType.VarChar, 10, "creditor_type");
            _with5.Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference");
            _with5.Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail");
            return cmd;
        }

        /// <summary>
        /// Generate the insert statement
        /// </summary>
        private SqlCommand InsertCommand(ref SqlConnection cn, ref SqlTransaction txn)
        {
            SqlCommand cmd = new SqlCommand();
            var _with6 = cmd;
            _with6.Connection = cn;
            _with6.Transaction = txn;
            _with6.CommandText = "INSERT INTO deposit_batch_details (deposit_batch_id, client, creditor, client_creditor, amount, fairshare_pct, fairshare_amt, creditor_type, reference) SELECT @deposit_batch_id, cc.client, cc.creditor, cc.client_creditor, @amount, @fairshare_pct, @fairshare_amt, @creditor_type, @reference FROM client_creditor cc WHERE cc.client_creditor = @client_creditor";
            _with6.CommandType = CommandType.Text;
            _with6.Parameters.Add("@client_creditor", SqlDbType.Int, 0, "client_creditor");
            _with6.Parameters.Add("@amount", SqlDbType.Decimal, 0, "amount");
            _with6.Parameters.Add("@fairshare_amt", SqlDbType.Decimal, 0, "fairshare_amt");
            _with6.Parameters.Add("@fairshare_pct", SqlDbType.Float, 0, "fairshare_pct");
            _with6.Parameters.Add("@creditor_type", SqlDbType.VarChar, 10, "creditor_type");
            _with6.Parameters.Add("@reference", SqlDbType.VarChar, 50, "reference");
            _with6.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch;
            return cmd;
        }

        /// <summary>
        /// Generate the delete statement
        /// </summary>
        private SqlCommand DeleteCommand(ref SqlConnection cn, ref SqlTransaction txn)
        {
            SqlCommand cmd = new SqlCommand();
            var _with7 = cmd;
            _with7.Connection = cn;
            _with7.Transaction = txn;
            _with7.CommandText = "DELETE FROM deposit_batch_details WHERE deposit_batch_detail = @deposit_batch_detail AND deposit_batch_id=@deposit_batch_id";
            _with7.CommandType = CommandType.Text;
            _with7.Parameters.Add("@deposit_batch_detail", SqlDbType.Int, 0, "deposit_batch_detail");
            _with7.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch;
            return cmd;
        }

        /// <summary>
        /// Close the deposit batch
        /// </summary>
        private void CloseBatch()
        {
            SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlTransaction txn = null;
            try
            {
                cn.Open();
                txn = cn.BeginTransaction();

                // First, close the batch
                var _with8 = new SqlCommand();
                _with8.Connection = cn;
                _with8.Transaction = txn;
                _with8.CommandText = "xpr_creditor_refund_batch_close";
                _with8.CommandType = CommandType.StoredProcedure;
                _with8.Parameters.Add("@deposit_batch_id", SqlDbType.Int).Value = ctx.ap.Batch;
                _with8.ExecuteNonQuery();

                // Commit the transactions
                txn.Commit();
                txn = null;
            }
            catch (SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error closing/posting the deposit batch");
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch
                    {
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();
            }
        }
    }
}