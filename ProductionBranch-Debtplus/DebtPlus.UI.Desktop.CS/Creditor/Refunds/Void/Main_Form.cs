#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Void
{
    internal partial class Main_Form : DebtPlus.Data.Forms.DebtPlusForm
    {
        public Main_Form() : base()
        {
            InitializeComponent();
        }

        internal CreditorRefundsVoidArgParser ap = null;

        public Main_Form(CreditorRefundsVoidArgParser ap) : this()
        {
            this.ap = ap;

            VoidCreditorList1.Cancelled += VoidCreditorList1_Cancelled;
            VoidCreditorList1.Selected += VoidCreditorList1_Selected;
            RefundCheckDetails1.Cancelled += RefundCheckDetails1_Cancelled;
            this.Load += Main_Form_Load;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        protected global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Void.VoidCreditorList VoidCreditorList1;

        internal global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Void.RefundCheckDetails RefundCheckDetails1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.VoidCreditorList1 = new global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Void.VoidCreditorList();
            this.RefundCheckDetails1 = new global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Void.RefundCheckDetails();

            this.SuspendLayout();

            //
            //VoidCreditorList1
            //
            this.VoidCreditorList1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VoidCreditorList1.Location = new System.Drawing.Point(0, 0);
            this.VoidCreditorList1.Name = "VoidCreditorList1";
            this.VoidCreditorList1.Size = new System.Drawing.Size(536, 382);
            this.ToolTipController1.SetSuperTip(this.VoidCreditorList1, null);
            this.VoidCreditorList1.TabIndex = 0;
            //
            //RefundCheckDetails1
            //
            this.RefundCheckDetails1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RefundCheckDetails1.Location = new System.Drawing.Point(0, 0);
            this.RefundCheckDetails1.Name = "RefundCheckDetails1";
            this.RefundCheckDetails1.Size = new System.Drawing.Size(536, 382);
            this.ToolTipController1.SetSuperTip(this.RefundCheckDetails1, null);
            this.RefundCheckDetails1.TabIndex = 1;
            //
            //Main_Form
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(536, 382);
            this.Controls.Add(this.VoidCreditorList1);
            this.Controls.Add(this.RefundCheckDetails1);
            this.Name = "Main_Form";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Void Creditor Refund Checks";

            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void Main_Form_Load(object sender, System.EventArgs e)
        {
            // Hide the refund information
            RefundCheckDetails1.Visible = false;
            RefundCheckDetails1.SendToBack();

            // Initialize the information for the creditor display
            VoidCreditorList1.Reload();
            VoidCreditorList1.Visible = true;
        }

        private void VoidCreditorList1_Cancelled(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void VoidCreditorList1_Selected(object Sender, System.Int32 CreditorRegister)
        {
            VoidCreditorList1.Visible = false;
            VoidCreditorList1.SendToBack();

            RefundCheckDetails1.Visible = true;
            RefundCheckDetails1.Reload(CreditorRegister);
        }

        private void RefundCheckDetails1_Cancelled(object sender, System.EventArgs e)
        {
            // Hide the refund information
            RefundCheckDetails1.Visible = false;
            RefundCheckDetails1.SendToBack();

            // Initialize the information for the creditor display
            VoidCreditorList1.Reload();
            VoidCreditorList1.Visible = true;
        }
    }
}