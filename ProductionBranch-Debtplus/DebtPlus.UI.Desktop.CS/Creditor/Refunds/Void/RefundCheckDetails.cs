using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Void
{
    internal partial class RefundCheckDetails : DevExpress.XtraEditors.XtraUserControl
    {
        protected System.Data.DataSet ds = new System.Data.DataSet("registers_client_creditor");

        public RefundCheckDetails() : base()
        {
            InitializeComponent();
            Button_Cancel.Click += Button_Cancel_Click;
            Button_OK.Click += Button_OK_Click;
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        protected DevExpress.XtraGrid.GridControl GridControl1;

        protected DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        protected DevExpress.XtraEditors.LabelControl LabelControl1;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_name;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_gross;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_deducted;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_net;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_type;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_fairshare_amt;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_fairshare_pct;
        protected DevExpress.XtraGrid.Columns.GridColumn GridColumn_void;
        protected DevExpress.XtraEditors.SimpleButton Button_OK;
        protected DevExpress.XtraEditors.SimpleButton Button_Cancel;
        protected DevExpress.XtraEditors.LookUpEdit reason;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_net = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_deducted = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_deducted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_gross = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_creditor_type = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_fairshare_amt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_fairshare_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_fairshare_pct = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_fairshare_pct.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_void = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_void.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.reason = new DevExpress.XtraEditors.LookUpEdit();
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.reason.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(0, 40);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(440, 312);
            this.GridControl1.TabIndex = 2;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_client,
                this.GridColumn_name,
                this.GridColumn_net,
                this.GridColumn_deducted,
                this.GridColumn_gross,
                this.GridColumn_creditor_type,
                this.GridColumn_fairshare_amt,
                this.GridColumn_fairshare_pct,
                this.GridColumn_void
            });
            this.GridView1.CustomizationFormBounds = new System.Drawing.Rectangle(806, 518, 208, 170);
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowFooter = true;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            //
            //GridColumn_client
            //
            this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.Caption = "Client ID";
            this.GridColumn_client.DisplayFormat.FormatString = "0000000";
            this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.FieldName = "client";
            this.GridColumn_client.GroupFormat.FormatString = "0000000";
            this.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.Name = "GridColumn_client";
            this.GridColumn_client.SummaryItem.DisplayFormat = "{0:d} item(s)";
            this.GridColumn_client.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            this.GridColumn_client.Visible = true;
            this.GridColumn_client.VisibleIndex = 0;
            this.GridColumn_client.Width = 45;
            //
            //GridColumn_name
            //
            this.GridColumn_name.Caption = "Name";
            this.GridColumn_name.FieldName = "name";
            this.GridColumn_name.Name = "GridColumn_name";
            this.GridColumn_name.Visible = true;
            this.GridColumn_name.VisibleIndex = 1;
            this.GridColumn_name.Width = 210;
            //
            //GridColumn_net
            //
            this.GridColumn_net.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_net.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_net.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_net.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_net.Caption = "Net";
            this.GridColumn_net.DisplayFormat.FormatString = "c";
            this.GridColumn_net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_net.FieldName = "net";
            this.GridColumn_net.GroupFormat.FormatString = "c";
            this.GridColumn_net.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_net.Name = "GridColumn_net";
            this.GridColumn_net.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_net.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.GridColumn_net.Visible = true;
            this.GridColumn_net.VisibleIndex = 2;
            this.GridColumn_net.Width = 70;
            //
            //GridColumn_deducted
            //
            this.GridColumn_deducted.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_deducted.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_deducted.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_deducted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_deducted.Caption = "Deducted";
            this.GridColumn_deducted.DisplayFormat.FormatString = "c";
            this.GridColumn_deducted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_deducted.FieldName = "deducted";
            this.GridColumn_deducted.GroupFormat.FormatString = "c";
            this.GridColumn_deducted.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_deducted.Name = "GridColumn_deducted";
            this.GridColumn_deducted.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_deducted.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.GridColumn_deducted.Visible = true;
            this.GridColumn_deducted.VisibleIndex = 3;
            this.GridColumn_deducted.Width = 60;
            //
            //GridColumn_gross
            //
            this.GridColumn_gross.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_gross.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_gross.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_gross.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_gross.Caption = "Gross";
            this.GridColumn_gross.DisplayFormat.FormatString = "c";
            this.GridColumn_gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_gross.FieldName = "credit_amt";
            this.GridColumn_gross.GroupFormat.FormatString = "c";
            this.GridColumn_gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_gross.Name = "GridColumn_gross";
            this.GridColumn_gross.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_gross.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.GridColumn_gross.Visible = true;
            this.GridColumn_gross.VisibleIndex = 4;
            this.GridColumn_gross.Width = 70;
            //
            //GridColumn_creditor_type
            //
            this.GridColumn_creditor_type.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_creditor_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_creditor_type.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_creditor_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_creditor_type.Caption = "Creditor Type";
            this.GridColumn_creditor_type.FieldName = "creditor_type";
            this.GridColumn_creditor_type.Name = "GridColumn_creditor_type";
            //
            //GridColumn_fairshare_amt
            //
            this.GridColumn_fairshare_amt.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_fairshare_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_fairshare_amt.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_fairshare_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_fairshare_amt.Caption = "Fairshare";
            this.GridColumn_fairshare_amt.DisplayFormat.FormatString = "c";
            this.GridColumn_fairshare_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_fairshare_amt.FieldName = "fairshare_amt";
            this.GridColumn_fairshare_amt.GroupFormat.FormatString = "c";
            this.GridColumn_fairshare_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_fairshare_amt.Name = "GridColumn_fairshare_amt";
            this.GridColumn_fairshare_amt.SummaryItem.DisplayFormat = "{0:c}";
            this.GridColumn_fairshare_amt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            //
            //GridColumn_fairshare_pct
            //
            this.GridColumn_fairshare_pct.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_fairshare_pct.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_fairshare_pct.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_fairshare_pct.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_fairshare_pct.Caption = "Fairshare Pct";
            this.GridColumn_fairshare_pct.DisplayFormat.FormatString = "p";
            this.GridColumn_fairshare_pct.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_fairshare_pct.FieldName = "fairshare_pct";
            this.GridColumn_fairshare_pct.GroupFormat.FormatString = "p";
            this.GridColumn_fairshare_pct.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_fairshare_pct.Name = "GridColumn_fairshare_pct";
            //
            //GridColumn_void
            //
            this.GridColumn_void.Caption = "Voided";
            this.GridColumn_void.FieldName = "void";
            this.GridColumn_void.Name = "GridColumn_void";
            //
            //reason
            //
            this.reason.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.reason.Location = new System.Drawing.Point(104, 8);
            this.reason.Name = "reason";
            this.reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            this.reason.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("item_key", "ID", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None),
                new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending)
            });
            this.reason.Properties.NullText = "";
            this.reason.Properties.ShowFooter = false;
            this.reason.Properties.ShowHeader = false;
            this.reason.Properties.ShowLines = false;
            this.reason.Size = new System.Drawing.Size(336, 20);
            this.reason.TabIndex = 1;
            this.reason.Properties.SortColumnIndex = 1;
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(16, 11);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(36, 13);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Reason";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_OK.Enabled = false;
            this.Button_OK.Location = new System.Drawing.Point(456, 8);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 3;
            this.Button_OK.Text = "&OK";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
            this.Button_Cancel.CausesValidation = false;
            this.Button_Cancel.Location = new System.Drawing.Point(456, 56);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 4;
            this.Button_Cancel.Text = "&Cancel";
            //
            //RefundCheckDetails
            //
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.reason);
            this.Controls.Add(this.GridControl1);
            this.Name = "RefundCheckDetails";
            this.Size = new System.Drawing.Size(544, 352);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.reason.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Intialize the control with the values
        /// </summary>
        private System.Int32 creditor_register = -1;

        public void Reload(System.Int32 CreditorRegister)
        {
            creditor_register = CreditorRegister;

            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            var _with1 = cmd;
            _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with1.CommandText = "SELECT rrc.client_creditor_register, rrc.client_creditor, rrc.tran_type, rrc.client, rrc.creditor, rrc.creditor_type, rrc.disbursement_register, rrc.trust_register, rrc.invoice_register, rrc.creditor_register, rrc.credit_amt, rrc.debit_amt, rrc.fairshare_amt, rrc.fairshare_pct, rrc.account_number, rrc.void, rrc.date_created, rrc.created_by, dbo.format_normal_name(p.prefix, p.first, p.middle, p.last, p.suffix) AS name, isnull(rrc.fairshare_amt,0) AS deducted, isnull(rrc.credit_amt,0) - isnull(rrc.fairshare_amt,0) AS net FROM registers_client_creditor rrc LEFT OUTER JOIN people p ON rrc.client = p.client AND 1 = p.relation WHERE tran_type = 'RF' AND creditor_register=@creditor_register";
            _with1.Parameters.Add("@creditor_register", SqlDbType.Int).Value = CreditorRegister;

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                ds.Clear();
                da.Fill(ds, "registers_client_creditor");

                var _with2 = GridControl1;
                _with2.DataSource = ds.Tables["registers_client_creditor"];
                _with2.RefreshDataSource();

                // Try to fit the columns as best that we can
                GridView1.BestFitColumns();

                // Load the reason list as well
                var _with3 = reason;
                System.Data.DataTable tbl = MessagesTable();
                var _with4 = _with3.Properties;
                _with4.DataSource = tbl.DefaultView;
                _with4.DisplayMember = "description";
                _with4.ValueMember = "item_key";
                if (tbl.Columns.IndexOf("Default") >= 0)
                {
                    System.Data.DataView vue = new System.Data.DataView(tbl, "[default]=True", string.Empty, DataViewRowState.CurrentRows);
                    if (vue.Count > 0)
                    {
                        _with3.EditValue = vue[0]["item_key"];
                    }
                }

                if (_with3.EditValue == null && tbl.Rows.Count > 0)
                {
                    _with3.EditValue = tbl.Rows[0]["item_key"];
                }
                System.Windows.Forms.Cursor.Current = current_cursor;

                // Enable the OK button as needed
                Button_OK.Enabled = (GridView1.RowCount > 0) && reason.EditValue != null;

                // If the check contains voided items then complain.
                object VoidCount = ds.Tables[0].Compute("count(void)", "void<>0");
                if (VoidCount != null && Convert.ToInt32(VoidCount) > 0)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(string.Format("{0:d} item(s) have been previously voided on this refund check." + Environment.NewLine + Environment.NewLine + "This check may not be voided again.", Convert.ToInt32(VoidCount)), "Sorry, but the check is voided", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    Button_OK.Enabled = false;
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading registers_client_creditor table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Retrieve the list of void reasons for a creditor check
        /// </summary>
        internal System.Data.DataTable MessagesTable()
        {
            System.Data.DataTable tbl = null;
            System.Data.DataSet ds = new System.Data.DataSet("messages");
            ds.Clear();

            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            var _with5 = cmd;
            _with5.CommandText = "lst_descriptions_vf";
            _with5.CommandType = CommandType.StoredProcedure;
            _with5.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                da.Fill(ds, "reasons");
                tbl = ds.Tables[0];
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading messages table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            return tbl;
        }

        /// <summary>
        /// Process a click on the CANCEL button
        /// </summary>
        public event System.EventHandler Cancelled;

        private void Button_Cancel_Click(object sender, System.EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Process a click on the OK button
        /// </summary>
        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = System.Windows.Forms.Cursor.Current;

            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                cn.Open();

                var _with6 = new SqlCommand();
                _with6.Connection = cn;
                _with6.CommandText = "xpr_void_creditor_refund";
                _with6.CommandType = CommandType.StoredProcedure;
                _with6.Parameters.Add("@creditor_register", SqlDbType.Int).Value = creditor_register;
                _with6.Parameters.Add("@reason", SqlDbType.VarChar, 50).Value = reason.Text;
                _with6.ExecuteNonQuery();

                // All is well with the world
                System.Windows.Forms.Cursor.Current = current_cursor;
                DebtPlus.Data.Forms.MessageBox.Show("The creditor refund check was successfully voided.", "Operation Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error voiding creditor refund");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            // Trip the cancelled function to return to the previous screen.
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }
    }
}