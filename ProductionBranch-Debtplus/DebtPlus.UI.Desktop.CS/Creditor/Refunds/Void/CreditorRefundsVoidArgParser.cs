using System;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Void
{
    internal partial class CreditorRefundsVoidArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public CreditorRefundsVoidArgParser() : base(new string[] { })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo));
            }

            AppendErrorLine("Usage: DebtPlus.Creditor.Refunds.Void.exe");
        }
    }
}