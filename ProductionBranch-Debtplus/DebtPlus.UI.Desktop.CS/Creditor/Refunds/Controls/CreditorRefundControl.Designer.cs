using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
	partial class CreditorRefundControl : BaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.NewRefundItemControl1 = new global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls.RefundItemNewControl(ctx);
			this.BatchListControl1 = new global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls.BatchListControl();
			this.RefundItemControl1 = new global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls.RefundItemOldControl(ctx);
			this.SuspendLayout();
			//
			//NewRefundItemControl1
			//
			this.NewRefundItemControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.NewRefundItemControl1.Location = new System.Drawing.Point(0, 0);
			this.NewRefundItemControl1.Name = "NewRefundItemControl1";
			this.NewRefundItemControl1.Size = new System.Drawing.Size(458, 292);
			this.NewRefundItemControl1.TabIndex = 1;
			//
			//BatchListControl1
			//
			this.BatchListControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.BatchListControl1.Location = new System.Drawing.Point(0, 289);
			this.BatchListControl1.Name = "BatchListControl1";
			this.BatchListControl1.Padding = new System.Windows.Forms.Padding(4);
			this.BatchListControl1.Size = new System.Drawing.Size(458, 154);
			this.BatchListControl1.TabIndex = 3;
			this.BatchListControl1.TabStop = false;
			//
			//RefundItemControl1
			//
			this.RefundItemControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.RefundItemControl1.Location = new System.Drawing.Point(0, 0);
			this.RefundItemControl1.Name = "RefundItemControl1";
			this.RefundItemControl1.Size = new System.Drawing.Size(458, 256);
			this.RefundItemControl1.TabIndex = 2;
			//
			//CreditorRefundControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.BatchListControl1);
			this.Controls.Add(this.NewRefundItemControl1);
			this.Controls.Add(this.RefundItemControl1);
			this.Name = "CreditorRefundControl";
			this.Size = new System.Drawing.Size(458, 438);
			this.ResumeLayout(false);

		}
		protected internal global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls.BatchListControl BatchListControl1;
		protected internal global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls.RefundItemNewControl NewRefundItemControl1;
		protected internal global::DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls.RefundItemOldControl RefundItemControl1;
	}
}
