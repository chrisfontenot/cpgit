using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Creditor.Widgets.Controls;
using DebtPlus.UI.Client.Widgets.Controls;
using DebtPlus.UI.Creditor.Widgets;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
	partial class RefundItemBaseControl : BaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;
		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefundItemBaseControl));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
			this.LabelControl_client_name = new DevExpress.XtraEditors.LabelControl();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.CreditorID1 = new global::DebtPlus.UI.Creditor.Widgets.Controls.CreditorID();
			this.ClientID1 = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
			this.LookUpEdit_creditor_type = new DevExpress.XtraEditors.LookUpEdit();
			this.CalcEdit_fairshare_rate = new DevExpress.XtraEditors.CalcEdit();
			this.CalcEdit_gross = new DevExpress.XtraEditors.CalcEdit();
			this.SimpleButton_ok = new DevExpress.XtraEditors.SimpleButton();
			this.CalcEdit_fairshare_amt = new DevExpress.XtraEditors.CalcEdit();
			this.ComboBoxEdit_message = new DevExpress.XtraEditors.ComboBoxEdit();
			this.CalcEdit_net = new DevExpress.XtraEditors.CalcEdit();
			this.LookUpEdit_client_creditor = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl_creditor_name = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton_cancel = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_creditor_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_fairshare_rate.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_gross.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_fairshare_amt.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_message.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_net.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_client_creditor.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl_client_name
			//
			this.LabelControl_client_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_client_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
			this.LabelControl_client_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.LabelControl_client_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl_client_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_client_name.Location = new System.Drawing.Point(204, 67);
			this.LabelControl_client_name.Name = "LabelControl_client_name";
			this.LabelControl_client_name.Padding = new System.Windows.Forms.Padding(3);
			this.LabelControl_client_name.Size = new System.Drawing.Size(209, 19);
			this.LabelControl_client_name.StyleController = this.LayoutControl1;
			this.LabelControl_client_name.TabIndex = 3;
			this.LabelControl_client_name.UseMnemonic = false;
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.CreditorID1);
			this.LayoutControl1.Controls.Add(this.ClientID1);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_creditor_type);
			this.LayoutControl1.Controls.Add(this.LabelControl_client_name);
			this.LayoutControl1.Controls.Add(this.CalcEdit_fairshare_rate);
			this.LayoutControl1.Controls.Add(this.CalcEdit_gross);
			this.LayoutControl1.Controls.Add(this.SimpleButton_ok);
			this.LayoutControl1.Controls.Add(this.CalcEdit_fairshare_amt);
			this.LayoutControl1.Controls.Add(this.ComboBoxEdit_message);
			this.LayoutControl1.Controls.Add(this.CalcEdit_net);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_client_creditor);
			this.LayoutControl1.Controls.Add(this.LabelControl_creditor_name);
			this.LayoutControl1.Controls.Add(this.SimpleButton_cancel);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(437, 294);
			this.LayoutControl1.TabIndex = 2;
			this.LayoutControl1.OptionsFocus.EnableAutoTabOrder = false;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//CreditorID1
			//
			this.CreditorID1.AllowDrop = true;
			this.CreditorID1.EditValue = null;
			this.CreditorID1.Location = new System.Drawing.Point(114, 43);
			this.CreditorID1.Name = "CreditorID1";
			this.CreditorID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("CreditorID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a creditor ID", "search", null, true) });
			this.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.CreditorID1.Properties.Mask.BeepOnError = true;
			this.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
			this.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.CreditorID1.Properties.MaxLength = 10;
			this.CreditorID1.Size = new System.Drawing.Size(86, 20);
			this.CreditorID1.StyleController = this.LayoutControl1;
			this.CreditorID1.TabIndex = 0;
			//
			//ClientID1
			//
			this.ClientID1.AllowDrop = true;
			this.ClientID1.Location = new System.Drawing.Point(114, 67);
			this.ClientID1.Name = "ClientID1";
			this.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.ClientID1.Properties.Appearance.Options.UseTextOptions = true;
			this.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ClientID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("ClientID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject2,
			"Click here to search for a client ID", "search", null, true) });
			this.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.ClientID1.Properties.DisplayFormat.FormatString = "0000000";
			this.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ClientID1.Properties.EditFormat.FormatString = "f0";
			this.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.ClientID1.Properties.Mask.BeepOnError = true;
			this.ClientID1.Properties.Mask.EditMask = "\\d*";
			this.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.ClientID1.Properties.ValidateOnEnterKey = true;
			this.ClientID1.Size = new System.Drawing.Size(86, 20);
			this.ClientID1.StyleController = this.LayoutControl1;
			this.ClientID1.TabIndex = 2;
			//
			//LookUpEdit_creditor_type
			//
			this.LookUpEdit_creditor_type.Location = new System.Drawing.Point(275, 175);
			this.LookUpEdit_creditor_type.Name = "LookUpEdit_creditor_type";
			this.LookUpEdit_creditor_type.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.LookUpEdit_creditor_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_creditor_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Id", "Type", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_creditor_type.Properties.DisplayMember = "description";
			this.LookUpEdit_creditor_type.Properties.NullText = "";
			this.LookUpEdit_creditor_type.Properties.ShowFooter = false;
			this.LookUpEdit_creditor_type.Properties.ShowHeader = false;
			this.LookUpEdit_creditor_type.Properties.SortColumnIndex = 1;
			this.LookUpEdit_creditor_type.Properties.ValueMember = "Id";
			this.LookUpEdit_creditor_type.Size = new System.Drawing.Size(59, 20);
			this.LookUpEdit_creditor_type.StyleController = this.LayoutControl1;
			this.LookUpEdit_creditor_type.TabIndex = 6;
			//
			//CalcEdit_fairshare_rate
			//
			this.CalcEdit_fairshare_rate.Location = new System.Drawing.Point(275, 199);
			this.CalcEdit_fairshare_rate.Name = "CalcEdit_fairshare_rate";
			this.CalcEdit_fairshare_rate.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.CalcEdit_fairshare_rate.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_rate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_rate.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_rate.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_rate.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_rate.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_rate.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_rate.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_rate.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_rate.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_rate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_fairshare_rate.Properties.DisplayFormat.FormatString = "p";
			this.CalcEdit_fairshare_rate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_fairshare_rate.Properties.EditFormat.FormatString = "f6";
			this.CalcEdit_fairshare_rate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_fairshare_rate.Properties.Mask.BeepOnError = true;
			this.CalcEdit_fairshare_rate.Properties.Mask.EditMask = "f6";
			this.CalcEdit_fairshare_rate.Size = new System.Drawing.Size(59, 20);
			this.CalcEdit_fairshare_rate.StyleController = this.LayoutControl1;
			this.CalcEdit_fairshare_rate.TabIndex = 8;
			//
			//CalcEdit_gross
			//
			this.CalcEdit_gross.Location = new System.Drawing.Point(114, 223);
			this.CalcEdit_gross.Name = "CalcEdit_gross";
			this.CalcEdit_gross.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_gross.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_gross.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_gross.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_gross.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_gross.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_gross.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_gross.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_gross.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_gross.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_gross.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_gross.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit_gross.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_gross.Properties.EditFormat.FormatString = "{0:f2}";
			this.CalcEdit_gross.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_gross.Properties.Mask.EditMask = "c";
			this.CalcEdit_gross.Properties.Precision = 2;
			this.CalcEdit_gross.Properties.ReadOnly = true;
			this.CalcEdit_gross.Size = new System.Drawing.Size(67, 20);
			this.CalcEdit_gross.StyleController = this.LayoutControl1;
			this.CalcEdit_gross.TabIndex = 9;
			this.CalcEdit_gross.TabStop = false;
			//
			//SimpleButton_ok
			//
			this.SimpleButton_ok.Location = new System.Drawing.Point(338, 175);
			this.SimpleButton_ok.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.Name = "SimpleButton_ok";
			this.SimpleButton_ok.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.StyleController = this.LayoutControl1;
			this.SimpleButton_ok.TabIndex = 11;
			this.SimpleButton_ok.Text = "&OK";
			//
			//CalcEdit_fairshare_amt
			//
			this.CalcEdit_fairshare_amt.Location = new System.Drawing.Point(114, 199);
			this.CalcEdit_fairshare_amt.Name = "CalcEdit_fairshare_amt";
			this.CalcEdit_fairshare_amt.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.CalcEdit_fairshare_amt.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_amt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_amt.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_amt.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_amt.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_amt.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_amt.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_amt.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_amt.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_fairshare_amt.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_fairshare_amt.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_fairshare_amt.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit_fairshare_amt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.CalcEdit_fairshare_amt.Properties.EditFormat.FormatString = "{0:f2}";
			this.CalcEdit_fairshare_amt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_fairshare_amt.Properties.Mask.EditMask = "c";
			this.CalcEdit_fairshare_amt.Properties.Precision = 2;
			this.CalcEdit_fairshare_amt.Size = new System.Drawing.Size(67, 20);
			this.CalcEdit_fairshare_amt.StyleController = this.LayoutControl1;
			this.CalcEdit_fairshare_amt.TabIndex = 7;
			//
			//ComboBoxEdit_message
			//
			this.ComboBoxEdit_message.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.ComboBoxEdit_message.Location = new System.Drawing.Point(114, 250);
			this.ComboBoxEdit_message.Name = "ComboBoxEdit_message";
			this.ComboBoxEdit_message.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit_message.Size = new System.Drawing.Size(299, 20);
			this.ComboBoxEdit_message.StyleController = this.LayoutControl1;
			this.ComboBoxEdit_message.TabIndex = 10;
			//
			//CalcEdit_net
			//
			this.CalcEdit_net.Location = new System.Drawing.Point(114, 175);
			this.CalcEdit_net.Name = "CalcEdit_net";
			this.CalcEdit_net.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.CalcEdit_net.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_net.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_net.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_net.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_net.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_net.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_net.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_net.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_net.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_net.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_net.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_net.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit_net.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_net.Properties.EditFormat.FormatString = "{0:f2}";
			this.CalcEdit_net.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_net.Properties.Mask.EditMask = "c";
			this.CalcEdit_net.Properties.Precision = 2;
			this.CalcEdit_net.Size = new System.Drawing.Size(67, 20);
			this.CalcEdit_net.StyleController = this.LayoutControl1;
			this.CalcEdit_net.TabIndex = 5;
			//
			//LookUpEdit_client_creditor
			//
			this.LookUpEdit_client_creditor.Location = new System.Drawing.Point(114, 91);
			this.LookUpEdit_client_creditor.Name = "LookUpEdit_client_creditor";
			this.LookUpEdit_client_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_client_creditor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("client_creditor", "Debt", 20, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("account_number", "Account Number", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_client_creditor.Properties.DisplayMember = "account_number";
			this.LookUpEdit_client_creditor.Properties.NullText = "REQUIRED";
			this.LookUpEdit_client_creditor.Properties.ValueMember = "client_creditor";
			this.LookUpEdit_client_creditor.Size = new System.Drawing.Size(299, 20);
			this.LookUpEdit_client_creditor.StyleController = this.LayoutControl1;
			this.LookUpEdit_client_creditor.TabIndex = 4;
			//
			//LabelControl_creditor_name
			//
			this.LabelControl_creditor_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_creditor_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
			this.LabelControl_creditor_name.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.LabelControl_creditor_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl_creditor_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_creditor_name.Location = new System.Drawing.Point(204, 43);
			this.LabelControl_creditor_name.Name = "LabelControl_creditor_name";
			this.LabelControl_creditor_name.Padding = new System.Windows.Forms.Padding(3);
			this.LabelControl_creditor_name.Size = new System.Drawing.Size(209, 19);
			this.LabelControl_creditor_name.StyleController = this.LayoutControl1;
			this.LabelControl_creditor_name.TabIndex = 1;
			this.LabelControl_creditor_name.UseMnemonic = false;
			//
			//SimpleButton_cancel
			//
			this.SimpleButton_cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_cancel.Location = new System.Drawing.Point(338, 223);
			this.SimpleButton_cancel.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.Name = "SimpleButton_cancel";
			this.SimpleButton_cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.StyleController = this.LayoutControl1;
			this.SimpleButton_cancel.TabIndex = 12;
			this.SimpleButton_cancel.Text = "&Cancel";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlGroup3,
				this.LayoutControlGroup2
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(437, 294);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlGroup3
			//
			this.LayoutControlGroup3.CustomizationFormText = "Refund Information";
			this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem8,
				this.LayoutControlItem7,
				this.LayoutControlItem6,
				this.LayoutControlItem10,
				this.LayoutControlItem11,
				this.LayoutControlItem12,
				this.LayoutControlItem9,
				this.EmptySpaceItem1,
				this.LayoutControlItem13
			});
			this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 132);
			this.LayoutControlGroup3.Name = "LayoutControlGroup3";
			this.LayoutControlGroup3.Size = new System.Drawing.Size(417, 142);
			this.LayoutControlGroup3.Text = "Refund Information";
			//
			//LayoutControlItem8
			//
			this.LayoutControlItem8.Control = this.CalcEdit_gross;
			this.LayoutControlItem8.CustomizationFormText = "Gross";
			this.LayoutControlItem8.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem8.Name = "LayoutControlItem8";
			this.LayoutControlItem8.Size = new System.Drawing.Size(161, 27);
			this.LayoutControlItem8.Text = "Gross";
			this.LayoutControlItem8.TextSize = new System.Drawing.Size(86, 13);
			//
			//LayoutControlItem7
			//
			this.LayoutControlItem7.Control = this.CalcEdit_fairshare_amt;
			this.LayoutControlItem7.CustomizationFormText = "Fairshare";
			this.LayoutControlItem7.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem7.Name = "LayoutControlItem7";
			this.LayoutControlItem7.Size = new System.Drawing.Size(161, 24);
			this.LayoutControlItem7.Text = "Fairshare";
			this.LayoutControlItem7.TextSize = new System.Drawing.Size(86, 13);
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.CalcEdit_net;
			this.LayoutControlItem6.CustomizationFormText = "Net";
			this.LayoutControlItem6.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(161, 24);
			this.LayoutControlItem6.Text = "Net";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(86, 13);
			//
			//LayoutControlItem10
			//
			this.LayoutControlItem10.Control = this.LookUpEdit_creditor_type;
			this.LayoutControlItem10.CustomizationFormText = "Contribution Type";
			this.LayoutControlItem10.Location = new System.Drawing.Point(161, 0);
			this.LayoutControlItem10.Name = "LayoutControlItem10";
			this.LayoutControlItem10.Size = new System.Drawing.Size(153, 24);
			this.LayoutControlItem10.Text = "Contribution Type";
			this.LayoutControlItem10.TextSize = new System.Drawing.Size(86, 13);
			//
			//LayoutControlItem11
			//
			this.LayoutControlItem11.Control = this.CalcEdit_fairshare_rate;
			this.LayoutControlItem11.CustomizationFormText = "Fairshare Rate";
			this.LayoutControlItem11.Location = new System.Drawing.Point(161, 24);
			this.LayoutControlItem11.Name = "LayoutControlItem11";
			this.LayoutControlItem11.Size = new System.Drawing.Size(153, 24);
			this.LayoutControlItem11.Text = "Fairshare Rate";
			this.LayoutControlItem11.TextSize = new System.Drawing.Size(86, 13);
			//
			//LayoutControlItem12
			//
			this.LayoutControlItem12.Control = this.SimpleButton_ok;
			this.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12";
			this.LayoutControlItem12.Location = new System.Drawing.Point(314, 0);
			this.LayoutControlItem12.Name = "LayoutControlItem12";
			this.LayoutControlItem12.Size = new System.Drawing.Size(79, 48);
			this.LayoutControlItem12.Text = "LayoutControlItem12";
			this.LayoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem12.TextToControlDistance = 0;
			this.LayoutControlItem12.TextVisible = false;
			//
			//LayoutControlItem9
			//
			this.LayoutControlItem9.Control = this.ComboBoxEdit_message;
			this.LayoutControlItem9.CustomizationFormText = "Message";
			this.LayoutControlItem9.Location = new System.Drawing.Point(0, 75);
			this.LayoutControlItem9.Name = "LayoutControlItem9";
			this.LayoutControlItem9.Size = new System.Drawing.Size(393, 24);
			this.LayoutControlItem9.Text = "Message";
			this.LayoutControlItem9.TextSize = new System.Drawing.Size(86, 13);
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.AllowHotTrack = false;
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(161, 48);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(153, 27);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//LayoutControlItem13
			//
			this.LayoutControlItem13.Control = this.SimpleButton_cancel;
			this.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13";
			this.LayoutControlItem13.Location = new System.Drawing.Point(314, 48);
			this.LayoutControlItem13.Name = "LayoutControlItem13";
			this.LayoutControlItem13.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem13.Text = "LayoutControlItem13";
			this.LayoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem13.TextToControlDistance = 0;
			this.LayoutControlItem13.TextVisible = false;
			//
			//LayoutControlGroup2
			//
			this.LayoutControlGroup2.CustomizationFormText = "Client Information";
			this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.LayoutControlItem5,
				this.EmptySpaceItem2,
				this.LayoutControlItem1,
				this.LayoutControlItem2
			});
			this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup2.Name = "LayoutControlGroup2";
			this.LayoutControlGroup2.Size = new System.Drawing.Size(417, 132);
			this.LayoutControlGroup2.Text = "Client Information";
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.LookUpEdit_client_creditor;
			this.LayoutControlItem3.CustomizationFormText = "Account Number";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(393, 24);
			this.LayoutControlItem3.Text = "Account Number";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(86, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.LabelControl_creditor_name;
			this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem4";
			this.LayoutControlItem4.Location = new System.Drawing.Point(180, 0);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(213, 24);
			this.LayoutControlItem4.Text = "LayoutControlItem4";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem4.TextToControlDistance = 0;
			this.LayoutControlItem4.TextVisible = false;
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.LabelControl_client_name;
			this.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5";
			this.LayoutControlItem5.Location = new System.Drawing.Point(180, 24);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(213, 24);
			this.LayoutControlItem5.Text = "LayoutControlItem5";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem5.TextToControlDistance = 0;
			this.LayoutControlItem5.TextVisible = false;
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.AllowHotTrack = false;
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 72);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(393, 17);
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.ClientID1;
			this.LayoutControlItem1.CustomizationFormText = "Client";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(180, 24);
			this.LayoutControlItem1.Text = "Client";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(86, 13);
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.CreditorID1;
			this.LayoutControlItem2.CustomizationFormText = "Creditor";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(180, 24);
			this.LayoutControlItem2.Text = "Creditor";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(86, 13);
			//
			//RefundItemBaseControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.LayoutControl1);
			this.Name = "RefundItemBaseControl";
			this.Size = new System.Drawing.Size(437, 294);
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_creditor_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_fairshare_rate.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_gross.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_fairshare_amt.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_message.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_net.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_client_creditor.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_client_name;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_creditor_name;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_client_creditor;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_fairshare_rate;
		protected internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_message;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_cancel;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_ok;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_fairshare_amt;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_net;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_gross;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_creditor_type;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
		protected internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal CreditorID CreditorID1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
	}
}
