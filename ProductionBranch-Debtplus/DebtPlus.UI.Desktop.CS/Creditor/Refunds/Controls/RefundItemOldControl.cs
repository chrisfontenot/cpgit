using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
    internal partial class RefundItemOldControl
    {
        public RefundItemOldControl(CreditorRefundsContext ctx) : base(ctx)
        {
            InitializeComponent();
            SimpleButton_ok.Click += SimpleButton_ok_Click;
            SimpleButton_cancel.Click += SimpleButton_cancel_Click;
        }

        private DataRowView EditDrv = null;

        public void ReadRecord(DataRowView drv)
        {
            EditDrv = drv;

            // Do not do update events until the record is properly defined
            UnRegisterHandlers();

            // Set the values into the controls
            ClientID1.EditValue = global::DebtPlus.Utils.Nulls.v_Int32(drv["client"]);
            ReadClientName(ClientID1.EditValue);

            CreditorID1.EditValue = null;
            if (!object.ReferenceEquals(drv["creditor"], DBNull.Value))
                CreditorID1.EditValue = Convert.ToString(drv["creditor"]);
            ReadCreditorName(CreditorID1.EditValue);

            LookUpEdit_client_creditor.EditValue = drv["client_creditor"];
            CalcEdit_fairshare_amt.EditValue = drv["fairshare_amt"];
            CalcEdit_gross.EditValue = drv["amount"];
            CalcEdit_net.EditValue = drv["net_amt"];
            ComboBoxEdit_message.Text = Convert.ToString(drv["reference"]);

            // Go to the standard processing logic
            ReadRecord();

            // Since these are changed when we lookup the creditor, put them back.
            LookUpEdit_creditor_type.EditValue = drv["creditor_type"];
            CalcEdit_fairshare_rate.EditValue = drv["fairshare_pct"];

            // Enable the update events to track the changes
            RegisterHandlers();
        }

        public void TakeFocus()
        {
            var _with1 = (CreditorRefundControl)Parent;
            _with1.BatchListControl1.Enabled = false;

            var _with2 = this;
            _with2.BringToFront();
            _with2.Visible = true;
            _with2.TabStop = true;

            // Go to the creditor ID information
            CreditorID1.Focus();
        }

        public void GiveFocus()
        {
            var _with3 = this;
            _with3.SendToBack();
            _with3.Visible = false;
            _with3.TabStop = false;
        }

        private void SimpleButton_ok_Click(object sender, EventArgs e)
        {
            // Start the edit operation on the trustRegister
            EditDrv.BeginEdit();

            // Update the fields for the trustRegister
            EditDrv["client"] = global::DebtPlus.Utils.Nulls.ToDbNull(ClientID1.EditValue);
            EditDrv["creditor"] = CreditorID1.EditValue;
            EditDrv["amount"] = CalcEdit_gross.EditValue;
            EditDrv["fairshare_amt"] = CalcEdit_fairshare_amt.EditValue;
            EditDrv["fairshare_pct"] = CalcEdit_fairshare_rate.EditValue;
            EditDrv["creditor_type"] = LookUpEdit_creditor_type.EditValue;
            EditDrv["reference"] = ComboBoxEdit_message.Text.Trim();
            EditDrv["client_creditor"] = LookUpEdit_client_creditor.EditValue;

            EditDrv.EndEdit();

            var _with4 = (CreditorRefundControl)this.Parent;
            _with4.RefundItemControl1.GiveFocus();
            _with4.NewRefundItemControl1.TakeFocus();
        }

        private void SimpleButton_cancel_Click(object sender, EventArgs e)
        {
            var _with5 = (CreditorRefundControl)this.Parent;
            _with5.RefundItemControl1.GiveFocus();
            _with5.NewRefundItemControl1.TakeFocus();
        }
    }
}