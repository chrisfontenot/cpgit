using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.IO;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
    internal partial class BatchListControl
    {
        public BatchListControl() : base()
        {
            InitializeComponent();

            this.Load += MyGridControl_Load;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.Layout += LayoutChanged;
        }

        /// <summary>
        /// Load the batch list on the first call
        /// </summary>

        public void LoadList(DataTable tbl)
        {
            // Update the list with the display information
            var _with1 = GridControl1;
            _with1.DataSource = tbl.DefaultView;
        }

        /// <summary>
        /// Find the item to edit
        /// </summary>

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            // Find the row targeted as the double-click item
            GridView gv = (GridView)sender;
            GridControl ctl = (GridControl)gv.GridControl;
            GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(MousePosition)));
            Int32 ControlRow = hi.RowHandle;

            // Find the datarowview from the input tables.
            DataRowView drv = null;
            if (hi.InRow && ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (DataRowView)gv.GetRow(ControlRow);

                // If this is not the new row then edit it with the edit control.
                if (!drv.IsNew)
                {
                    var _with2 = (CreditorRefundControl)Parent;
                    _with2.NewRefundItemControl1.GiveFocus();
                    _with2.RefundItemControl1.TakeFocus();
                    _with2.RefundItemControl1.ReadRecord(drv);
                }
            }
        }

        private bool InInit = true;

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            if (DesignMode || ParentForm == null)
                return string.Empty;
            string BasePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "DebtPlus";
            return Path.Combine(BasePath, "Creditor.Refunds");
        }

        /// <summary>
        /// When loading the control, restore the layout from the file
        /// </summary>
        private void MyGridControl_Load(object sender, EventArgs e)
        {
            ReloadGridControlLayout();
            InInit = false;
        }

        /// <summary>
        /// Reload the layout of the grid control if needed
        /// </summary>

        protected void ReloadGridControlLayout()
        {
            // Find the base path to the saved file location
            string PathName = XMLBasePath();
            if (!string.IsNullOrEmpty(PathName))
            {
                InInit = true;
                try
                {
                    string FileName = Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
                    if (File.Exists(FileName))
                    {
                        GridView1.RestoreLayoutFromXml(FileName);
                    }
                }
#pragma warning disable 168
                catch (DirectoryNotFoundException ex)
                {
                }
                catch (FileNotFoundException ex)
                {
                }
#pragma warning restore 168
                finally
                {
                    InInit = false;
                }
            }
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            string PathName = XMLBasePath();
            if (!InInit && !string.IsNullOrEmpty(PathName))
            {
                if (!Directory.Exists(PathName))
                {
                    Directory.CreateDirectory(PathName);
                }

                string FileName = Path.Combine(PathName, string.Format("{0}.Grid.xml", Name));
#if false // Save all options so that it may be imported into the designer
				GridView1.OptionsLayout.StoreAllOptions = true;
				GridView1.OptionsLayout.StoreAppearance = true;
				GridView1.OptionsLayout.StoreDataSettings = true;
				GridView1.OptionsLayout.StoreVisualOptions = true;
				GridView1.OptionsLayout.Columns.StoreAllOptions = true;
				GridView1.OptionsLayout.Columns.StoreAppearance = true;
				GridView1.OptionsLayout.Columns.StoreLayout = true;
#endif
                GridView1.SaveLayoutToXml(FileName);
            }
        }
    }
}