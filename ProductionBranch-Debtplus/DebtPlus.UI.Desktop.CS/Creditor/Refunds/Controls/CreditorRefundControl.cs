using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraBars;
using System.Data.SqlClient;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Drawing;
using System.Drawing;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
    internal partial class CreditorRefundControl
    {
        private readonly CreditorRefundsContext ctx = null;

        private DataTable _tbl = null;

        private Form_CreditorRefund parentFrm;

        public CreditorRefundControl(Form_CreditorRefund p, CreditorRefundsContext ctx) : base()
        {
            this.ctx = ctx;
            parentFrm = p;
            InitializeComponent();
            RefundItemControl1.GiveFocus();
            NewRefundItemControl1.TakeFocus();
            _tbl = ctx.ds.Tables["deposit_batch_details"];
        }

        public void ReadForm()
        {
            // Add a handler to the menu for printing the report
            var _with1 = parentFrm;
            BarManager mgr = _with1.barManager1;
            BarButtonItem newItem = new BarButtonItem(mgr, "&Print...");
            var _with2 = newItem;
            _with2.Name = "PrintReport";
            _with2.ItemClick += PrintReport;
            _with1.BarSubItem_File.InsertItem(_with1.BarSubItem_File.ItemLinks[0], newItem);

            // Read the list of pending items for the deposit details
            if (_tbl == null)
            {
                SqlCommand cmd = new SqlCommand();
                var _with3 = cmd;
                _with3.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with3.CommandType = CommandType.Text;
                _with3.CommandText = "SELECT [deposit_batch_detail],[deposit_batch_id],[tran_subtype],[ok_to_post],[scanned_client],[client],[creditor],[client_creditor],[amount],[fairshare_amt],[fairshare_pct],[creditor_type],[item_date],[reference],[message],[ach_transaction_code],[ach_routing_number],[ach_account_number],[ach_authentication_code],[ach_response_batch_id],[date_posted],[created_by],[date_created] FROM deposit_batch_details WHERE deposit_batch_id = @deposit_batch ORDER BY deposit_batch_detail";
                _with3.Parameters.Add("@deposit_batch", SqlDbType.Int).Value = ctx.ap.Batch;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ctx.ds, "deposit_batch_details");
                _tbl = ctx.ds.Tables["deposit_batch_details"];
            }

            // Supply the necessary default values for the columns
            var _with4 = _tbl;
            _with4.PrimaryKey = new DataColumn[] { _with4.Columns["deposit_batch_detail"] };

            var _with5 = _with4.Columns["deposit_batch_detail"];
            object objAnswer = _tbl.Compute("max(deposit_batch_detail)", string.Empty);
            if (objAnswer == null || object.ReferenceEquals(objAnswer, DBNull.Value))
                objAnswer = 0;

            _with5.AutoIncrement = true;
            _with5.AutoIncrementSeed = Convert.ToInt32(objAnswer) + 1;
            _with5.AutoIncrementStep = 1;

            var _with6 = _with4.Columns["creditor_type"];
            _with6.DefaultValue = "N";

            var _with7 = _with4.Columns["fairshare_pct"];
            _with7.DefaultValue = 0.0;

            var _with8 = _with4.Columns["fairshare_amt"];
            // fairshare
            _with8.DefaultValue = 0m;

            var _with9 = _with4.Columns["amount"];
            // gross
            _with9.DefaultValue = 0m;

            var _with10 = _with4.Columns.Add("deduct_amt", typeof(decimal), "iif([creditor_type]='D',[fairshare_amt],0)");
            _with10.DefaultValue = 0m;

            var _with11 = _with4.Columns.Add("net_amt", typeof(decimal), "[amount]-[deduct_amt]");
            // net
            _with11.DefaultValue = 0m;

            // Set the controls in the proper order
            RefundItemControl1.GiveFocus();
            NewRefundItemControl1.TakeFocus();

            // Bind the table to the various controls
            BatchListControl1.LoadList(_tbl);

            // Add a new trustRegister to the view
            NewRefundItemControl1.ReadRecord();
        }

        private void PrintReport(object sender, ItemClickEventArgs e)
        {
            DebtPlus.Reports.Deposits.Batch.DepositBatchReport rpt = new DebtPlus.Reports.Deposits.Batch.DepositBatchReport();
            rpt.Parameter_DepositBatchID = ctx.ap.Batch;
            rpt.Parameter_SortString = "client";

            if (rpt.RequestReportParameters() == System.Windows.Forms.DialogResult.OK)
            {
                DataTable updatedTable = _tbl.GetChanges();
                if (updatedTable != null && updatedTable.Rows.Count > 0)
                {
                    var _with12 = ((XtraReport)rpt).Watermark;
                    _with12.Font = new Font("Arial", 60, FontStyle.Bold, GraphicsUnit.Point, 0);
                    _with12.ImageViewMode = ImageViewMode.Stretch;
                    _with12.ShowBehind = true;
                    _with12.Text = string.Format("DATABASE IS{0}NOT UPDATED", Environment.NewLine);
                    _with12.TextDirection = DirectionMode.BackwardDiagonal;
                    _with12.TextTransparency = 180;
                    _with12.ForeColor = Color.DarkGreen;
                }
                rpt.RunReportInSeparateThread();
            }
        }
    }
}