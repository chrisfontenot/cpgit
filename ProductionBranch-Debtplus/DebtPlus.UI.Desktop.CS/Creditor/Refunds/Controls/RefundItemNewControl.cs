using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
    internal partial class RefundItemNewControl
    {
        public RefundItemNewControl(CreditorRefundsContext ctx) : base(ctx)
        {
            InitializeComponent();

            // Add the standard handlers when the control is created
            SimpleButton_cancel.Click += SimpleButton_cancel_Click;
            SimpleButton_ok.Click += SimpleButton_ok_Click;
        }

        // Save the previous values for the input
        private object _lastCreditorType = "N";

        private object _lastFairsharePct = 0.0;

        public override void ReadRecord()
        {
            // Do not do update events until the record is properly defined
            UnRegisterHandlers();

            // Set the values into the controls
            ClientID1.EditValue = null;
            CreditorID1.EditValue = null;
            LookUpEdit_client_creditor.EditValue = null;
            CalcEdit_fairshare_amt.EditValue = 0m;
            CalcEdit_net.EditValue = 0m;
            CalcEdit_gross.EditValue = 0m;
            CalcEdit_fairshare_rate.EditValue = _lastFairsharePct;
            LookUpEdit_creditor_type.EditValue = _lastCreditorType;

            // Go to the standard processing logic
            base.ReadRecord();

            // Enable the update events to track the changes
            RegisterHandlers();
        }

        public void TakeFocus()
        {
            var _with1 = (CreditorRefundControl)Parent;
            _with1.BatchListControl1.Enabled = true;

            var _with2 = this;
            _with2.BringToFront();
            _with2.Visible = true;
            _with2.TabStop = true;

            // Go to the creditor ID information
            CreditorID1.Focus();
        }

        public void GiveFocus()
        {
            var _with3 = this;
            _with3.SendToBack();
            _with3.Visible = false;
            _with3.TabStop = false;
        }

        private void SimpleButton_ok_Click(object sender, EventArgs e)
        {
            // Save the previous values for the input
            _lastCreditorType = LookUpEdit_creditor_type.EditValue;
            _lastFairsharePct = CalcEdit_fairshare_rate.EditValue;

            // Add a new record to the list and populate the controls with the values
            DataRowView drv = ctx.ds.Tables["deposit_batch_details"].DefaultView.AddNew();
            drv.BeginEdit();

            // Update the fields for the trustRegister
            drv["client"] = global::DebtPlus.Utils.Nulls.ToDbNull(ClientID1.EditValue);
            drv["creditor"] = CreditorID1.EditValue;
            drv["amount"] = CalcEdit_gross.EditValue;
            drv["fairshare_amt"] = CalcEdit_fairshare_amt.EditValue;
            drv["fairshare_pct"] = CalcEdit_fairshare_rate.EditValue;
            drv["creditor_type"] = LookUpEdit_creditor_type.EditValue;
            drv["reference"] = ComboBoxEdit_message.Text.Trim();
            drv["client_creditor"] = LookUpEdit_client_creditor.EditValue;

            // End the edit operation
            drv.EndEdit();

            // Load the control with the new record
            ReadRecord();
        }

        private void SimpleButton_cancel_Click(object sender, EventArgs e)
        {
            // Complete the transactions
            var _with4 = (Form_CreditorRefund)ParentForm;
            _with4.Close();
        }
    }
}