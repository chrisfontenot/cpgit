using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
	partial class BatchListControl : BaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_net = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_fairshare = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_gross = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_creditor_type = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_deduct_amt = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_reference = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			this.SuspendLayout();
			//
			//GroupControl1
			//
			this.GroupControl1.Controls.Add(this.GridControl1);
			this.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GroupControl1.Location = new System.Drawing.Point(0, 0);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(439, 261);
			this.GroupControl1.TabIndex = 0;
			this.GroupControl1.Text = "Batch Items";
			//
			//GridControl1
			//
			this.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.GridControl1.Location = new System.Drawing.Point(2, 21);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.Size = new System.Drawing.Size(435, 238);
			this.GridControl1.TabIndex = 0;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
			this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.Empty.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
			this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
			this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
			this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
			this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
			this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
			this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
			this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.Preview.Options.UseFont = true;
			this.GridView1.Appearance.Preview.Options.UseForeColor = true;
			this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.Row.Options.UseBackColor = true;
			this.GridView1.Appearance.Row.Options.UseForeColor = true;
			this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
			this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_client,
				this.GridColumn_creditor,
				this.GridColumn_net,
				this.GridColumn_fairshare,
				this.GridColumn_gross,
				this.GridColumn_creditor_type,
				this.GridColumn_deduct_amt,
				this.GridColumn_reference,
				this.GridColumn_ID
			});
			this.GridView1.CustomizationFormBounds = new System.Drawing.Rectangle(579, 424, 208, 170);
			this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsLayout.Columns.StoreAllOptions = true;
			this.GridView1.OptionsLayout.Columns.StoreAppearance = true;
			this.GridView1.OptionsLayout.StoreAllOptions = true;
			this.GridView1.OptionsLayout.StoreAppearance = true;
			this.GridView1.OptionsView.ShowFooter = true;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowOnlyInEditor;
			//
			//GridColumn_client
			//
			this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client.Caption = "Client";
			this.GridColumn_client.CustomizationCaption = "Client";
			this.GridColumn_client.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client.FieldName = "client";
			this.GridColumn_client.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client.Name = "GridColumn_client";
			this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_client.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] { new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "client", "{0:n0} items") });
			this.GridColumn_client.Visible = true;
			this.GridColumn_client.VisibleIndex = 0;
			this.GridColumn_client.Width = 94;
			//
			//GridColumn_creditor
			//
			this.GridColumn_creditor.Caption = "Creditor";
			this.GridColumn_creditor.CustomizationCaption = "Creditor ID";
			this.GridColumn_creditor.FieldName = "creditor";
			this.GridColumn_creditor.Name = "GridColumn_creditor";
			this.GridColumn_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_creditor.Visible = true;
			this.GridColumn_creditor.VisibleIndex = 1;
			this.GridColumn_creditor.Width = 238;
			//
			//GridColumn_net
			//
			this.GridColumn_net.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_net.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_net.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_net.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_net.Caption = "Net";
			this.GridColumn_net.CustomizationCaption = "Net Amount";
			this.GridColumn_net.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_net.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_net.FieldName = "net_amt";
			this.GridColumn_net.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_net.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_net.Name = "GridColumn_net";
			this.GridColumn_net.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_net.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] { new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "net_amt", "{0:c}") });
			this.GridColumn_net.Visible = true;
			this.GridColumn_net.VisibleIndex = 2;
			this.GridColumn_net.Width = 74;
			//
			//GridColumn_fairshare
			//
			this.GridColumn_fairshare.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_fairshare.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_fairshare.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_fairshare.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_fairshare.Caption = "Fairshare";
			this.GridColumn_fairshare.CustomizationCaption = "Fairshare Amount";
			this.GridColumn_fairshare.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_fairshare.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_fairshare.FieldName = "fairshare_amt";
			this.GridColumn_fairshare.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_fairshare.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_fairshare.Name = "GridColumn_fairshare";
			this.GridColumn_fairshare.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_fairshare.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] { new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "fairshare_amt", "{0:c}") });
			this.GridColumn_fairshare.Visible = true;
			this.GridColumn_fairshare.VisibleIndex = 3;
			this.GridColumn_fairshare.Width = 65;
			//
			//GridColumn_gross
			//
			this.GridColumn_gross.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_gross.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_gross.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_gross.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_gross.Caption = "Gross";
			this.GridColumn_gross.CustomizationCaption = "Gross Amount";
			this.GridColumn_gross.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_gross.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_gross.FieldName = "amount";
			this.GridColumn_gross.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_gross.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_gross.Name = "GridColumn_gross";
			this.GridColumn_gross.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_gross.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] { new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "amount", "{0:c}") });
			this.GridColumn_gross.Visible = true;
			this.GridColumn_gross.VisibleIndex = 4;
			this.GridColumn_gross.Width = 70;
			//
			//GridColumn_creditor_type
			//
			this.GridColumn_creditor_type.Caption = "Type";
			this.GridColumn_creditor_type.CustomizationCaption = "Bill/Deduct/None";
			this.GridColumn_creditor_type.FieldName = "creditor_type";
			this.GridColumn_creditor_type.Name = "GridColumn_creditor_type";
			this.GridColumn_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//GridColumn_deduct_amt
			//
			this.GridColumn_deduct_amt.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_deduct_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_deduct_amt.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_deduct_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_deduct_amt.Caption = "Deducted";
			this.GridColumn_deduct_amt.CustomizationCaption = "Deduct";
			this.GridColumn_deduct_amt.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_deduct_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_deduct_amt.FieldName = "deduct_amt";
			this.GridColumn_deduct_amt.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_deduct_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_deduct_amt.Name = "GridColumn_deduct_amt";
			this.GridColumn_deduct_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_deduct_amt.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] { new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "deduct_amt", "{0:c}") });
			//
			//GridColumn_reference
			//
			this.GridColumn_reference.Caption = "Reference";
			this.GridColumn_reference.CustomizationCaption = "Message Field";
			this.GridColumn_reference.FieldName = "reference";
			this.GridColumn_reference.Name = "GridColumn_reference";
			this.GridColumn_reference.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//GridColumn_ID
			//
			this.GridColumn_ID.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_ID.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_ID.Caption = "ID";
			this.GridColumn_ID.CustomizationCaption = "Deposit Record ID";
			this.GridColumn_ID.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_ID.FieldName = "deposit_batch_id";
			this.GridColumn_ID.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_ID.Name = "GridColumn_ID";
			this.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//BatchListControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GroupControl1);
			this.Name = "BatchListControl";
			this.Size = new System.Drawing.Size(439, 261);
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.GroupControl GroupControl1;
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_net;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_fairshare;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_gross;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_type;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_deduct_amt;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_reference;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ID;
	}
}
