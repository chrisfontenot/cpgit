#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds.Controls
{
    internal partial class RefundItemBaseControl
    {
        protected readonly CreditorRefundsContext ctx = null;

        public event EventHandler Cancelled;

        public event EventHandler Completed;

        public RefundItemBaseControl(CreditorRefundsContext ctx) : base()
        {
            this.ctx = ctx;
            InitializeComponent();
            Load += RefundItemBaseControl_Load;
            LookUpEdit_client_creditor.EditValueChanged += LookUpEdit_client_creditor_EditValueChanged;
            CalcEdit_net.EditValueChanged += EnableOK;
            CreditorID1.Enter += CreditorID1_Enter;
            ClientID1.EditValueChanged += EnableOK;
            CreditorID1.EditValueChanged += EnableOK;
            LookUpEdit_client_creditor.EditValueChanged += EnableOK;
            ClientID1.Enter += CreditorID1_Enter;
            CalcEdit_fairshare_amt.Enter += CreditorID1_Enter;
            CalcEdit_fairshare_rate.Enter += CreditorID1_Enter;
            CalcEdit_gross.Enter += CreditorID1_Enter;
            CalcEdit_net.Enter += CreditorID1_Enter;
            CalcEdit_fairshare_amt.Properties.DisplayFormat.Format = new DeductFormatter(this);
        }

        protected void RegisterHandlers()
        {
            CreditorID1.Validating += CreditorID1_Validating;
            CreditorID1.EditValueChanged += EnableOK;
            ClientID1.Validating += ClientID1_Validating;
            ClientID1.EditValueChanged += EnableOK;
            CalcEdit_net.EditValueChanged += CalcEdit_net_EditValueChanged;
            CalcEdit_fairshare_amt.EditValueChanged += CalcEdit_net_EditValueChanged;
            CalcEdit_fairshare_rate.EditValueChanged += CalcEdit_fairshare_rate_EditValueChanged;
            CalcEdit_fairshare_rate.EditValueChanging += CalcEdit_net_EditValueChanging;
            CalcEdit_net.EditValueChanging += CalcEdit_net_EditValueChanging;
            CalcEdit_net.EditValueChanged += CalcEdit_fairshare_rate_EditValueChanged;
            CalcEdit_net.EditValueChanged += EnableOK;
            CalcEdit_gross.EditValueChanging += CalcEdit_net_EditValueChanging;
            LookUpEdit_creditor_type.EditValueChanged += CalcEdit_fairshare_rate_EditValueChanged;
            LookUpEdit_creditor_type.EditValueChanged += CalcEdit_net_EditValueChanged;
            LookUpEdit_client_creditor.EditValueChanged += LookUpEdit_client_creditor_EditValueChanged;
            LookUpEdit_client_creditor.EditValueChanged += EnableOK;
        }

        protected void UnRegisterHandlers()
        {
            CreditorID1.Validating -= CreditorID1_Validating;
            CreditorID1.EditValueChanged -= EnableOK;
            ClientID1.Validating -= ClientID1_Validating;
            ClientID1.EditValueChanged -= EnableOK;
            CalcEdit_net.EditValueChanged -= CalcEdit_net_EditValueChanged;
            CalcEdit_fairshare_amt.EditValueChanged -= CalcEdit_net_EditValueChanged;
            CalcEdit_fairshare_rate.EditValueChanged -= CalcEdit_fairshare_rate_EditValueChanged;
            CalcEdit_fairshare_rate.EditValueChanging -= CalcEdit_net_EditValueChanging;
            CalcEdit_net.EditValueChanging -= CalcEdit_net_EditValueChanging;
            CalcEdit_net.EditValueChanged -= CalcEdit_fairshare_rate_EditValueChanged;
            CalcEdit_net.EditValueChanged -= EnableOK;
            CalcEdit_gross.EditValueChanging -= CalcEdit_net_EditValueChanging;
            LookUpEdit_creditor_type.EditValueChanged -= CalcEdit_fairshare_rate_EditValueChanged;
            LookUpEdit_creditor_type.EditValueChanged -= CalcEdit_net_EditValueChanged;
            LookUpEdit_client_creditor.EditValueChanged -= LookUpEdit_client_creditor_EditValueChanged;
            LookUpEdit_client_creditor.EditValueChanged -= EnableOK;
        }

        protected virtual void OnCancelled(EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, e);
            }
        }

        protected void RaiseCancelled()
        {
            OnCancelled(EventArgs.Empty);
        }

        protected virtual void OnCompleted(EventArgs e)
        {
            if (Completed != null)
            {
                Completed(this, e);
            }
        }

        protected void RaiseCompleted()
        {
            OnCompleted(EventArgs.Empty);
        }

        private class DeductFormatter : IFormatProvider, ICustomFormatter
        {
            public RefundItemBaseControl Parent;

            public DeductFormatter(RefundItemBaseControl Parent)
            {
                this.Parent = Parent;
            }

            public object GetFormat(Type formatType)
            {
                return this;
            }

            public string Format(string format1, object arg, IFormatProvider formatProvider)
            {
                string DeductType = string.Empty;
                if (Parent.LookUpEdit_creditor_type.EditValue != null && !object.ReferenceEquals(Parent.LookUpEdit_creditor_type.EditValue, DBNull.Value))
                {
                    DeductType = Convert.ToString(Parent.LookUpEdit_creditor_type.EditValue);
                }
                if (DeductType != "D")
                {
                    arg = 0m;
                }
                if (arg == null || object.ReferenceEquals(arg, DBNull.Value))
                    arg = 0m;
                if (format1 == string.Empty)
                {
                    return Convert.ToDecimal(arg).ToString();
                }
                if (!format1.StartsWith("{"))
                    format1 = "{0:" + format1 + "}";
                return string.Format(format1, arg);
            }
        }

        protected virtual bool HasErrors()
        {
            bool answer = !(ValidClient() && ValidCreditor() && ValidAmount());
            return answer;
        }

        public virtual void ReadRecord()
        {
            // Define the client and creditor references
            ReadClientName(ClientID1.EditValue);
            ReadCreditorName(CreditorID1.EditValue);

            // Read the debt list if possible
            if (ValidClient() && ValidCreditor())
            {
                ReadDebtList(ClientID1.EditValue);
            }

            // Determine if there are pending errors
            SimpleButton_ok.Enabled = !HasErrors();
            CreditorID1.Focus();
        }

        private void RefundItemBaseControl_Load(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                // Set the controls to be the proper setting for the keypad enter key
                CalcEdit_fairshare_amt.EnterMoveNextControl = ctx.EnterMoveNextControl;
                CalcEdit_fairshare_rate.EnterMoveNextControl = ctx.EnterMoveNextControl;
                CalcEdit_gross.EnterMoveNextControl = ctx.EnterMoveNextControl;
                CalcEdit_net.EnterMoveNextControl = ctx.EnterMoveNextControl;
                ClientID1.EnterMoveNextControl = ctx.EnterMoveNextControl;
                CreditorID1.EnterMoveNextControl = ctx.EnterMoveNextControl;
                LookUpEdit_client_creditor.EnterMoveNextControl = ctx.EnterMoveNextControl;
                LookUpEdit_creditor_type.EnterMoveNextControl = ctx.EnterMoveNextControl;
                ComboBoxEdit_message.EnterMoveNextControl = ctx.EnterMoveNextControl;

                // Load the list of creditor types
                LookUpEdit_creditor_type.Properties.DataSource = DebtPlus.LINQ.InMemory.CreditorBillDeductTypes.getList();

                // Load the list of message types
                if (ComboBoxEdit_message.Properties.Items.Count == 0)
                {
                    ComboBoxEdit_message.Properties.Sorted = true;
                    var _with1 = ComboBoxEdit_message.Properties.Items;
                    _with1.Clear();
                    foreach (DebtPlus.LINQ.message item in DebtPlus.LINQ.Cache.message.getList().FindAll(s => s.item_type == "RF"))
                    {
                        _with1.Add(new DebtPlus.Data.Controls.ComboboxItem(item.description, item.Id, item.ActiveFlag));
                    }
                }
            }
        }

        private void CreditorID1_Validating(object sender, CancelEventArgs e)
        {
            ReadCreditorName(CreditorID1.EditValue);
            if (ValidClient() && ValidCreditor())
                LoadDebtList();
        }

        protected void ReadCreditorName(object Creditor)
        {
            LabelControl_creditor_name.Text = string.Empty;

            if (Creditor != null && !object.ReferenceEquals(Creditor, DBNull.Value))
            {
                DataTable tbl = ctx.ds.Tables["creditors"];
                DataRow row = null;

                if (tbl != null)
                {
                    row = tbl.Rows.Find(Creditor);
                    if (row != null)
                    {
                        CreditorID1.ErrorText = string.Empty;
                        if (row["creditor_name"] != null && !object.ReferenceEquals(row["creditor_name"], DBNull.Value))
                            LabelControl_creditor_name.Text = Convert.ToString(row["creditor_name"]);
                        CalcEdit_fairshare_rate.EditValue = row["fairshare_pct_eft"];
                        LookUpEdit_creditor_type.EditValue = row["creditor_type"];
                        return;
                    }
                }

                // Attempt to read the creditor id from the record
                SqlCommand cmd = new SqlCommand();
                var _with2 = cmd;
                _with2.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with2.CommandText = "SELECT cr.creditor, cr.creditor_name, pct.creditor_type_eft as creditor_type, pct.fairshare_pct_eft, pct.fairshare_pct_check FROM creditors cr LEFT OUTER JOIN creditor_contribution_pcts pct ON cr.creditor_contribution_pct = pct.creditor_contribution_pct WHERE cr.creditor = @creditor";
                _with2.CommandType = CommandType.Text;
                _with2.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor;

                // Read the information from the creditor tables
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ctx.ds, "creditors");
                da.Dispose();
                tbl = ctx.ds.Tables["creditors"];
                if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns[0] };

                // Locate the creditor information
                row = tbl.Rows.Find(Creditor);
                if (row != null)
                {
                    CreditorID1.ErrorText = string.Empty;
                    if (row["creditor_name"] != null && !object.ReferenceEquals(row["creditor_name"], DBNull.Value))
                        LabelControl_creditor_name.Text = Convert.ToString(row["creditor_name"]);
                    CalcEdit_fairshare_rate.EditValue = row["fairshare_pct_eft"];
                    LookUpEdit_creditor_type.EditValue = row["creditor_type"];
                    return;
                }
            }

            // The creditor is not valid
            CreditorID1.ErrorText = "Invalid creditor ID";
        }

        protected bool ValidCreditor()
        {
            return CreditorID1.EditValue != null && !object.ReferenceEquals(CreditorID1.EditValue, DBNull.Value) && CreditorID1.ErrorText == string.Empty;
        }

        private void ClientID1_Validating(object sender, CancelEventArgs e)
        {
            ReadClientName(ClientID1.EditValue);
            if (ValidClient() && ValidCreditor())
                LoadDebtList();
        }

        protected void ReadClientName(System.Nullable<Int32> client)
        {
            LabelControl_client_name.Text = string.Empty;
            if (client != null)
            {
                DataTable tbl = ctx.ds.Tables["clients"];
                DataRow row = null;

                if (tbl != null)
                {
                    row = tbl.Rows.Find(client.Value);
                    if (row != null)
                    {
                        ClientID1.ErrorText = string.Empty;
                        if (row["client_name"] != null && !object.ReferenceEquals(row["client_name"], DBNull.Value))
                            LabelControl_client_name.Text = Convert.ToString(row["client_name"]);
                        return;
                    }
                }

                // Attempt to read the creditor id from the record
                SqlCommand cmd = new SqlCommand();
                var _with3 = cmd;
                _with3.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with3.CommandText = "SELECT c.client, dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as client_name FROM clients c LEFT OUTER JOIN people p on c.client = p.client and 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name WHERE c.client = @client";
                _with3.CommandType = CommandType.Text;
                _with3.Parameters.Add("@client", SqlDbType.Int).Value = client.Value;

                // Read the information from the creditor tables
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ctx.ds, "clients");
                da.Dispose();
                tbl = ctx.ds.Tables["clients"];
                if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns[0] };

                // Locate the creditor information
                row = tbl.Rows.Find(client.Value);
                if (row != null)
                {
                    CreditorID1.ErrorText = string.Empty;
                    if (row["client_name"] != null && !object.ReferenceEquals(row["client_name"], DBNull.Value))
                        LabelControl_client_name.Text = Convert.ToString(row["client_name"]);
                    return;
                }
            }

            // The client is not valid
            ClientID1.ErrorText = "Invalid Client ID";
        }

        protected bool ValidClient()
        {
            return ClientID1.EditValue.GetValueOrDefault(-1) >= 0 && ClientID1.ErrorText == string.Empty;
        }

        protected void LoadDebtList()
        {
            // Find the table
            DataTable tbl = ctx.ds.Tables["client_creditor"];
            if (tbl == null)
            {
                ReadDebtList(ClientID1.EditValue);
                tbl = ctx.ds.Tables["client_creditor"];
            }

            // Look to see if we have any debts for this client in the list.
            DataView vue = new DataView(tbl, string.Format("[client]={0:f0}", ClientID1.EditValue.Value), string.Empty, DataViewRowState.CurrentRows);
            if (vue.Count == 0)
            {
                ReadDebtList(ClientID1.EditValue);
                tbl = ctx.ds.Tables["client_creditor"];
            }

            // Find the proper view for the client and creditor reference
            vue = new DataView(tbl, string.Format("[creditor]='{0}' AND [client]={1:f0}", Convert.ToString(CreditorID1.EditValue), ClientID1.EditValue.Value), "client_creditor", DataViewRowState.CurrentRows);

            // Load the view into the list of debts
            LookUpEdit_client_creditor.Properties.DataSource = vue;

            // If there are debts then clear the error else set it.
            if (vue.Count <= 0)
            {
                LookUpEdit_client_creditor.ErrorText = "This client has no debt for this creditor. Wrong Client? Wrong creditor?";
            }
            else
            {
                LookUpEdit_client_creditor.ErrorText = string.Empty;

                // If there is only one debt then select that debt
                if (vue.Count == 1)
                    LookUpEdit_client_creditor.EditValue = vue[0]["client_creditor"];
            }
        }

        protected void ReadDebtList(System.Nullable<Int32> Client)
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                var _with4 = cmd;
                _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with4.CommandType = CommandType.Text;
                _with4.CommandText = "SELECT cc.client, cc.creditor, cc.client_creditor, cc.account_number, cc.fairshare_pct_eft, cc.fairshare_pct_check FROM client_creditor cc WITH (NOLOCK) WHERE cc.client=@client";
                _with4.Parameters.Add("@client", SqlDbType.Int).Value = Client.GetValueOrDefault(-1);

                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ctx.ds, "client_creditor");
                }
            }

            // Create the primary key to the table if required
            DataTable tbl = ctx.ds.Tables["client_creditor"];
            if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                tbl.PrimaryKey = new DataColumn[] { tbl.Columns["client_creditor"] };
        }

        private void CalcEdit_net_EditValueChanged(object sender, EventArgs e)
        {
            decimal DeductAmount = 0m;
            if (Convert.ToString(LookUpEdit_creditor_type.EditValue) == "D")
                DeductAmount = Convert.ToDecimal(CalcEdit_fairshare_amt.EditValue);
            CalcEdit_gross.EditValue = Convert.ToDecimal(CalcEdit_net.EditValue) + DeductAmount;
        }

        protected bool ValidAmount()
        {
            return CalcEdit_net.EditValue != null && !object.ReferenceEquals(CalcEdit_net.EditValue, DBNull.Value) && Convert.ToDecimal(CalcEdit_net.EditValue) > 0m && CalcEdit_net.ErrorText == string.Empty;
        }

        private void CalcEdit_fairshare_rate_EditValueChanged(object sender, EventArgs e)
        {
            // Find the net amount
            decimal Net = 0m;
            if (CalcEdit_net.EditValue != null && !object.ReferenceEquals(CalcEdit_net.EditValue, DBNull.Value))
                Net = Convert.ToDecimal(CalcEdit_net.EditValue);

            // Find the rate for the discount
            double Rate = 0.0;
            if (CalcEdit_fairshare_rate.EditValue != null && !object.ReferenceEquals(CalcEdit_fairshare_rate, DBNull.Value))
                Rate = Convert.ToDouble(CalcEdit_fairshare_rate.EditValue);

            // Force the rate to be zero if it is NOT "deduct"
            string CreditorType = Convert.ToString(LookUpEdit_creditor_type.EditValue);
            if (CreditorType != "D")
                Rate = 0.0;
            while (Rate >= 1.0)
            {
                Rate /= 100.0;
            }

            // If the rate is zero then the fairshare is zero.
            if (Rate <= 0.0)
            {
                CalcEdit_fairshare_amt.EditValue = 0m;
            }
            else
            {
                // Otherwise, calculate it as a new gross figure to find the fairshare amount
                decimal NewGross = System.Math.Truncate(Convert.ToDecimal(Convert.ToDouble(Net) / (1.0 - Rate)) * 100M) / 100M;
                CalcEdit_fairshare_amt.EditValue = NewGross - Net;
            }
        }

        private void CalcEdit_net_EditValueChanging(object sender, ChangingEventArgs e)
        {
            // If this is the fairshare rate then do additional logic
            if (object.ReferenceEquals(sender, CalcEdit_fairshare_rate))
            {
                if (e.NewValue != null && !object.ReferenceEquals(e.NewValue, DBNull.Value))
                {
                    double Rate = Convert.ToDouble(e.NewValue);
                    if (Rate < 0.0)
                    {
                        e.Cancel = true;
                        return;
                    }

                    if (Rate >= 1.0)
                    {
                        while (Rate >= 1.0)
                        {
                            Rate /= 100.0;
                        }

                        // Reduce the rate to the proper value
                        CalcEdit_fairshare_rate.EditValue = Rate;
                        e.Cancel = true;
                        return;
                    }
                }
            }

            e.Cancel = e.NewValue == null || object.ReferenceEquals(e.NewValue, DBNull.Value) || Convert.ToDecimal(e.NewValue) < 0m;
        }

        private void LookUpEdit_client_creditor_EditValueChanged(object sender, EventArgs e)
        {
            Int32 client_creditor = -1;
            if (LookUpEdit_client_creditor.EditValue != null && !object.ReferenceEquals(LookUpEdit_client_creditor.EditValue, DBNull.Value))
                client_creditor = Convert.ToInt32(LookUpEdit_client_creditor.EditValue);
            if (client_creditor > 0)
            {
                DataRow row = ctx.ds.Tables["client_creditor"].Rows.Find(client_creditor);
                if (row != null)
                {
                    if (row["fairshare_pct_eft"] != null && !object.ReferenceEquals(row["fairshare_pct_eft"], DBNull.Value))
                        CalcEdit_fairshare_rate.EditValue = Convert.ToDouble(row["fairshare_pct_eft"]);
                }
            }
        }

        private void EnableOK(object sender, EventArgs e)
        {
            SimpleButton_ok.Enabled = !HasErrors();
        }

        private void CreditorID1_Enter(object sender, EventArgs e)
        {
            ((TextEdit)sender).SelectAll();
        }
    }
}