using System.Data;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds
{
    public partial class CreditorRefundsContext
    {
        public DepositArgParser ap = new DepositArgParser();

        public DataSet ds = new DataSet("ds");
        private bool privateEnterMoveNextControl = false;

        public bool EnterMoveNextControl
        {
            get { return privateEnterMoveNextControl; }
            private set { privateEnterMoveNextControl = value; }
        }

        public CreditorRefundsContext()
        {
            bool tempEnterMoveNextControl = false;

            // Find the flag for the mapping of the kepad tab to the enter button sequence.
            string TempString = DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.DepositManual), "EnterMoveNextControl");
            if (!bool.TryParse(TempString, out tempEnterMoveNextControl))
            {
                tempEnterMoveNextControl = false;
            }
            EnterMoveNextControl = tempEnterMoveNextControl;
        }
    }
}