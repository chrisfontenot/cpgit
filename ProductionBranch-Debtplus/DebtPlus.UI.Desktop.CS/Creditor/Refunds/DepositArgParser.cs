using System;

using System.Globalization;

namespace DebtPlus.UI.Desktop.CS.Creditor.Refunds
{
    public partial class DepositArgParser : DebtPlus.Utils.ArgParserBase
    {
        private Int32 _batch = -1;

        public Int32 Batch
        {
            get { return _batch; }
            set { _batch = value; }
        }

        public DepositArgParser() : base(new string[] { "b" })
        {
            Batch = -1;
        }

        protected override Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = SwitchStatus.NoError;
            switch (switchSymbol.ToLower())
            {
                case "b":
                    Int32 tempBatch = 0;
                    if (Int32.TryParse(switchValue, NumberStyles.Integer, CultureInfo.CurrentCulture, out tempBatch) && tempBatch <= 0)
                    {
                        ss = SwitchStatus.YesError;
                    }
                    Batch = tempBatch;
                    break;

                case "?":
                    ss = SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = SwitchStatus.YesError;
                    break;
            }
            return ss;
        }
    }
}