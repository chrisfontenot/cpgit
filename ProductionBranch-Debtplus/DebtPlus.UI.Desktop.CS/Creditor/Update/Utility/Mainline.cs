#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.Interfaces.Creditor;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.UI.Creditor.Widgets.Search;

namespace DebtPlus.UI.Desktop.CS.Creditor.Update.Utility
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new CreditorUpdateArgParser();
                    if (ap.Parse((string[])param))
                    {
                        ProcessUpdate(ap);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "CreditorUpdate"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }

        /// <summary>
        /// Do the creditor update routine.
        /// </summary>
        /// <param name="ap">Pointer to the decomposed argument string</param>
        private void ProcessUpdate(CreditorUpdateArgParser ap)
        {
            string currentCreditor = ap.Creditor;

            // Find the creditor if there is not one specified
            if (string.IsNullOrWhiteSpace(currentCreditor))
            {
                using (var search = new CreditorSearchClass())
                {
                    if (search.PerformCreditorSearch() != DialogResult.OK)
                    {
                        return;
                    }
                    currentCreditor = search.Creditor;
                }
            }

            // If no creditor can be located then do not do the update.
            if (string.IsNullOrWhiteSpace(currentCreditor))
            {
                return;
            }

            var bc = new DebtPlus.LINQ.BusinessContext();
            try
            {
                // Retrieve the creditor record from the database
                var creditorRecord = bc.creditors.Where(s => s.Id == currentCreditor).FirstOrDefault();
                if (creditorRecord == null)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The creditor is no longer in the system. The edit operation is cancelled.", "Sorry, can not find the creditor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Do the alert processing if needed
                if (ap.ShowAlerts)
                {
                    var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object objRecord)
                    {
                        var record = objRecord as DebtPlus.LINQ.creditor;
                        try
                        {
                            using (var search = new DebtPlus.Notes.AlertNotes.Creditor())
                            {
                                search.ShowAlerts(record.Id);
                            }
                        }

                        catch (System.Exception ex)
                        {
                            // Handle the error conditions
                            DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                        }
                    }))
                    {
                        IsBackground = true,
                        Name = "CreditorAlerts"
                    };

                    // Start the thread to display the alert messages
                    thrd.SetApartmentState(System.Threading.ApartmentState.STA);
                    thrd.Start((object)creditorRecord);
                }

                // Add it to the MRU for the subsequent search
                using (var mru = new DebtPlus.Data.MRU("Creditors"))
                {
                    mru.InsertTopMost(creditorRecord.Id);
                    mru.SaveKey();
                }

                // Hook into the submit changes function so that we can create the system note.
                bc.BeforeSubmitChanges += bc.RecordSystemNoteHandler;

                // Edit the current creditor
                using (var frm = new DebtPlus.UI.Creditor.Update.Forms.Form_CreditorUpdate(bc, creditorRecord, false))
                {
                    frm.ShowDialog();
                }

                // Submit the final changes to the creditor record
                bc.SubmitChanges();
            }

            finally
            {
                bc.BeforeSubmitChanges -= bc.RecordSystemNoteHandler;
                bc.Dispose();
            }
        }
    }
}