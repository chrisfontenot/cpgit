namespace DebtPlus.UI.Desktop.CS.Creditor.Update.Utility
{
    internal partial class CreditorUpdateArgParser : DebtPlus.Utils.ArgParserBase
    {
        private bool _ShowAlerts = true;
        private bool _Search = false;

        private string _Creditor = string.Empty;

        /// <summary>
        /// Return the flag for alert notes
        /// </summary>
        public bool ShowAlerts
        {
            get { return _ShowAlerts; }
        }

        /// <summary>
        /// Return the search status
        /// </summary>
        public bool Search
        {
            get { return _Search; }
        }

        /// <summary>
        /// Return the creidtor ID
        /// </summary>
        public string Creditor
        {
            get { return _Creditor; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public CreditorUpdateArgParser() : base(new string[] {
            "a",
            "s",
            "c"
        })
        {
        }

        /// <summary>
        /// Display error information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine("Usage: " + Fname + " [-a] [-s] [-c creditor]")
            //AppendErrorLine("       -a   Supress alert notes")
            //AppendErrorLine("       -s   Search for the creditor. Result written to console.")
            //AppendErrorLine("       -c   Creditor ID to be updated")
        }

        /// <summary>
        /// Handle an option switch
        /// </summary>
        protected override Utils.ArgParserBase.SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "?":
                    // User wants to see Usage
                    ss = SwitchStatus.ShowUsage;
                    break;

                case "a":
                    // supress alert notes
                    _ShowAlerts = false;
                    break;

                case "s":
                    // search
                    _Search = true;
                    break;

                case "c":
                    // creditor
                    ss = SetCreditor(switchValue);
                    break;

                default:
                    ss = SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        /// <summary>
        /// Handle a non-switched option
        /// </summary>
        protected override SwitchStatus OnNonSwitch(string Value)
        {
            return SetCreditor(Value);
        }

        /// <summary>
        /// Set the creditor for processing
        /// </summary>
        private SwitchStatus SetCreditor(string Value)
        {
            SwitchStatus ss = SwitchStatus.NoError;
            System.Text.RegularExpressions.Regex rx = new System.Text.RegularExpressions.Regex("[A-Za-z][0-9]{4,}");
            if (rx.IsMatch(Value))
            {
                _Creditor = Value;
            }
            else
            {
                AppendErrorLine("Invalid creditor ID: " + Value);
                ss = SwitchStatus.ShowUsage;
            }
            return ss;
        }
    }
}