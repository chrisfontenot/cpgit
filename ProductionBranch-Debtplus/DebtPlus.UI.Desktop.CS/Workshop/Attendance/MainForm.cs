#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Workshop.Attendance
{
    internal partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        public MainForm() : base()
        {
            InitializeComponent();
        }

        internal ArgParser ap = null;

        public MainForm(ArgParser ap) : this()
        {
            this.ap = ap;
            this.Load += MainForm_Load;
            Page_Select1.Completed += SelectWorkshop1_Completed;
            Page_Attendance1.PreviousPage += Page_Attendance1_PreviousPage;
        }

        #region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal Page_Select Page_Select1;

        internal Page_Attendance Page_Attendance1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.Page_Select1 = new Page_Select();
            this.Page_Attendance1 = new Page_Attendance();

            this.SuspendLayout();

            //
            //Page_Select1
            //
            this.Page_Select1.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.Page_Select1.Appearance.Options.UseBackColor = true;
            this.Page_Select1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Select1.Location = new System.Drawing.Point(0, 0);
            this.Page_Select1.Name = "Page_Select1";
            this.Page_Select1.Size = new System.Drawing.Size(496, 310);
            this.ToolTipController1.SetSuperTip(this.Page_Select1, null);
            this.Page_Select1.TabIndex = 0;
            //
            //Page_Attendance1
            //
            this.Page_Attendance1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Page_Attendance1.Location = new System.Drawing.Point(0, 0);
            this.Page_Attendance1.Name = "Page_Attendance1";
            this.Page_Attendance1.Size = new System.Drawing.Size(496, 310);
            this.ToolTipController1.SetSuperTip(this.Page_Attendance1, null);
            this.Page_Attendance1.TabIndex = 1;
            this.Page_Attendance1.Workshop = -1;
            //
            //MainForm
            //
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(496, 310);
            this.Controls.Add(this.Page_Select1);
            this.Controls.Add(this.Page_Attendance1);
            this.Name = "MainForm";
            this.ToolTipController1.SetSuperTip(this, null);
            this.Text = "Update Workshop Attendance";

            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            var _with1 = Page_Attendance1;
            _with1.Visible = false;

            var _with2 = Page_Select1;
            _with2.Visible = true;
            _with2.Process();
        }

        private void SelectWorkshop1_Completed(object Sender, System.Int32 Workshop)
        {
            var _with3 = Page_Select1;
            _with3.Visible = false;

            var _with4 = Page_Attendance1;
            _with4.Workshop = Workshop;
            _with4.Visible = true;
            _with4.Process();
        }

        private void Page_Attendance1_PreviousPage(object sender, System.EventArgs e)
        {
            var _with5 = Page_Attendance1;
            _with5.Visible = false;

            var _with6 = Page_Select1;
            _with6.Visible = true;
            _with6.Process();
        }
    }
}