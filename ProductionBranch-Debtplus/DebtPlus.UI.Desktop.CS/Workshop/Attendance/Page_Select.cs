using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DebtPlus.Interfaces;
using System.Windows.Forms;
using System.Drawing;

namespace DebtPlus.UI.Desktop.CS.Workshop.Attendance
{
    internal partial class Page_Select : DevExpress.XtraEditors.XtraUserControl
    {
        public event CompletedEventHandler Completed;

        public delegate void CompletedEventHandler(object Sender, System.Int32 Workshop);

        private System.Int32 Workshop = -1;

        public Page_Select() : base()
        {
            InitializeComponent();
            Button_Cancel.Click += Button_Cancel_Click;
            Button_OK.Click += Button_OK_Click;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.MouseDown += GridView1_MouseDown;
#if false
            this.Load += SelectWorkshop_Load;
#endif
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_workshop;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_time;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_location;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_description;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_workshop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_workshop.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_time.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_location = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_location.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_description.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            this.SuspendLayout();
            //
            //LabelControl1
            //
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(384, 14);
            this.LabelControl1.TabIndex = 1;
            this.LabelControl1.Text = "Select the workshop to be used";
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(0, 32);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(400, 144);
            this.GridControl1.TabIndex = 1;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(133), Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(38), Convert.ToByte(109), Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(59), Convert.ToByte(139), Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(170), Convert.ToByte(216), Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(139), Convert.ToByte(201), Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(105), Convert.ToByte(170), Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(247), Convert.ToByte(251), Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(236), Convert.ToByte(246), Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(83), Convert.ToByte(155), Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(Convert.ToByte(104), Convert.ToByte(184), Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_workshop,
                this.GridColumn_time,
                this.GridColumn_location,
                this.GridColumn_description
            });
            this.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsBehavior.Editable = false;
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_workshop, DevExpress.Data.ColumnSortOrder.Descending) });
            //
            //GridColumn_workshop
            //
            this.GridColumn_workshop.Caption = "ID";
            this.GridColumn_workshop.CustomizationCaption = "Workshop ID";
            this.GridColumn_workshop.FieldName = "workshop";
            this.GridColumn_workshop.Name = "GridColumn_workshop";
            //
            //GridColumn_time
            //
            this.GridColumn_time.Caption = "Date/Time";
            this.GridColumn_time.CustomizationCaption = "Date for workshop";
            this.GridColumn_time.DisplayFormat.FormatString = "M/dd/yyyy h:mm tt";
            this.GridColumn_time.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_time.FieldName = "start_time";
            this.GridColumn_time.GroupFormat.FormatString = "d";
            this.GridColumn_time.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.GridColumn_time.Name = "GridColumn_time";
            this.GridColumn_time.Visible = true;
            this.GridColumn_time.VisibleIndex = 0;
            this.GridColumn_time.Width = 95;
            //
            //GridColumn_location
            //
            this.GridColumn_location.Caption = "Location";
            this.GridColumn_location.CustomizationCaption = "Location";
            this.GridColumn_location.FieldName = "location";
            this.GridColumn_location.Name = "GridColumn_location";
            this.GridColumn_location.Visible = true;
            this.GridColumn_location.VisibleIndex = 1;
            this.GridColumn_location.Width = 150;
            //
            //GridColumn_description
            //
            this.GridColumn_description.Caption = "Description";
            this.GridColumn_description.CustomizationCaption = "Description";
            this.GridColumn_description.FieldName = "description";
            this.GridColumn_description.Name = "GridColumn_description";
            this.GridColumn_description.Visible = true;
            this.GridColumn_description.VisibleIndex = 2;
            this.GridColumn_description.Width = 151;
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(211, 184);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.TabIndex = 2;
            this.Button_OK.Text = "Next >";
            this.Button_OK.ToolTip = "Click here to select the workshop";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Location = new System.Drawing.Point(115, 184);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.TabIndex = 3;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Click here to cancel the form and return to the previous screen";
            //
            //Page_Select
            //
            this.Appearance.BackColor = System.Drawing.SystemColors.Window;
            this.Appearance.Options.UseBackColor = true;
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.LabelControl1);
            this.Name = "Page_Select";
            this.Size = new System.Drawing.Size(400, 224);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "

        /// <summary>
        /// Process the CANCEL button on the form
        /// </summary>
        private void Button_Cancel_Click(System.Object sender, System.EventArgs e)
        {
            FindForm().Close();
        }

        /// <summary>
        /// Process the OK button on the form
        /// </summary>
        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            if (Completed != null)
            {
                Completed(this, Workshop);
            }
        }

        /// <summary>
        /// Load the list of workshops to select
        /// </summary>
        public void Process()
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            var _with1 = cmd;
            _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with1.CommandText = "lst_workshops_pending";
            _with1.CommandType = CommandType.StoredProcedure;

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            System.Data.DataSet ds = new System.Data.DataSet();
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                da.Fill(ds, "workshops");

                var _with2 = GridControl1;
                _with2.DataSource = ds.Tables[0].DefaultView;
                _with2.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading Workshop IDs");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// Process a change in the current row for the control
        /// </summary>
        private void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = gv.GridControl;
            System.Int32 ControlRow = e.FocusedRowHandle;

            // Find the datarowview from the input tables.
            System.Data.DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
            }

            // Ask the user to do something with the selected row
            Workshop = -1;
            if (drv["item_key"] != null && !object.ReferenceEquals(drv["item_key"], System.DBNull.Value))
                Workshop = Convert.ToInt32(drv["item_key"]);
            Button_OK.Enabled = (Workshop > 0);
        }

        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>

        private void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            // Find the row targetted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 ControlRow = hi.RowHandle;

            // Find the datarowview from the input tables.
            System.Data.DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
            }

            // If there is a row then select it
            Workshop = -1;
            if (drv != null)
            {
                if (drv["item_key"] != null && !object.ReferenceEquals(drv["item_key"], System.DBNull.Value))
                    Workshop = Convert.ToInt32(drv["item_key"]);
            }

            // If there is a workshop then select it
            if (Workshop > 0)
                Button_OK.PerformClick();
        }

        private void GridView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo(new Point(e.X, e.Y));

            // Remember the position for the popup menu handler.
            System.Int32 ControlRow = -1;
            if (hi.InRow)
            {
                ControlRow = hi.RowHandle;
            }

            Workshop = -1;
            if (ControlRow >= 0)
            {
                System.Data.DataRowView drv = (System.Data.DataRowView)GridView1.GetRow(ControlRow);
                if (drv != null)
                {
                    if (drv["item_key"] != null && !object.ReferenceEquals(drv["item_key"], System.DBNull.Value))
                        Workshop = Convert.ToInt32(drv["item_key"]);
                }
                Button_OK.Enabled = (Workshop > 0);
            }
        }
#if false
        private void SelectWorkshop_Load(System.Object sender, System.EventArgs e)
        {
            Form MyForm = this.FindForm();

            // Assign the controllers to the controls for this control.
            if (MyForm != null && MyForm is IForm)
            {
                var _with3 = Button_Cancel;
                _with3.ToolTipController = ((IForm)MyForm).GetToolTipController;

                var _with4 = Button_OK;
                _with4.ToolTipController = ((IForm)MyForm).GetToolTipController;

                var _with5 = GridControl1;
                _with5.ToolTipController = ((IForm)MyForm).GetToolTipController;
            }
        }
#endif
    }
}