using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Workshop.Attendance
{
    public partial class Page_Attendance : DevExpress.XtraEditors.XtraUserControl
    {
        public event EventHandler PreviousPage;

        public Int32 Workshop { get; set; }

        public Page_Attendance() : base()
        {
            InitializeComponent();
            SimpleButton_Prev.Click += SimpleButton_Prev_Click;
            Button_OK.Click += Button_OK_Click;

            //Add any initialization after the InitializeComponent() call
            GridColumn_client.DisplayFormat.Format = new global::DebtPlus.Utils.Format.Client.CustomFormatter();
            GridColumn_client.GroupFormat.Format = new global::DebtPlus.Utils.Format.Client.CustomFormatter();
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.SimpleButton SimpleButton_Prev;

        internal DevExpress.XtraEditors.SimpleButton Button_OK;
        internal DevExpress.XtraGrid.GridControl GridControl1;
        internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_kept;
        internal DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit Status_Kept;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_StatusMissed;
        internal DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit Status_Missed;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ClientName;
        internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_seats;
        internal DevExpress.XtraEditors.LabelControl LabelControl1;
        internal DevExpress.XtraEditors.LabelControl LabelControl2;
        internal DevExpress.XtraEditors.LabelControl LabelControl3;
        internal DevExpress.XtraEditors.LabelControl LabelControl4;
        internal DevExpress.XtraEditors.LabelControl workshop_name;
        internal DevExpress.XtraEditors.LabelControl workshop_date;
        internal DevExpress.XtraEditors.LabelControl workshop_location;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.SimpleButton_Prev = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.GridControl1 = new DevExpress.XtraGrid.GridControl();
            this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GridColumn_kept = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_kept.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Status_Kept = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.GridColumn_StatusMissed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_StatusMissed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Status_Missed = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_seats = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_seats.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.GridColumn_ClientName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GridColumn_ClientName.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.workshop_name = new DevExpress.XtraEditors.LabelControl();
            this.workshop_date = new DevExpress.XtraEditors.LabelControl();
            this.workshop_location = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Status_Kept).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Status_Missed).BeginInit();
            this.SuspendLayout();
            //
            //SimpleButton_Prev
            //
            this.SimpleButton_Prev.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SimpleButton_Prev.Location = new System.Drawing.Point(115, 264);
            this.SimpleButton_Prev.Name = "SimpleButton_Prev";
            this.SimpleButton_Prev.Size = new System.Drawing.Size(75, 23);
            this.SimpleButton_Prev.TabIndex = 0;
            this.SimpleButton_Prev.Text = "< Prev";
            //
            //Button_OK
            //
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(211, 264);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.TabIndex = 1;
            this.Button_OK.Text = "Finish";
            //
            //GridControl1
            //
            this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.GridControl1.Location = new System.Drawing.Point(0, 112);
            this.GridControl1.MainView = this.GridView1;
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
                this.Status_Kept,
                this.Status_Missed
            });
            this.GridControl1.Size = new System.Drawing.Size(400, 144);
            this.GridControl1.TabIndex = 2;
            this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
            //
            //GridView1
            //
            this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.Empty.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
            this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
            this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
            this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
            this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
            this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
            this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
            this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
            this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
            this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.Preview.Options.UseFont = true;
            this.GridView1.Appearance.Preview.Options.UseForeColor = true;
            this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.GridView1.Appearance.Row.Options.UseBackColor = true;
            this.GridView1.Appearance.Row.Options.UseForeColor = true;
            this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
            this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
            this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
            this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
            this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
            this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
                this.GridColumn_kept,
                this.GridColumn_StatusMissed,
                this.GridColumn_client,
                this.GridColumn_seats,
                this.GridColumn_ClientName
            });
            this.GridView1.GridControl = this.GridControl1;
            this.GridView1.Name = "GridView1";
            this.GridView1.OptionsView.ShowGroupPanel = false;
            this.GridView1.OptionsView.ShowIndicator = false;
            this.GridView1.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_client, DevExpress.Data.ColumnSortOrder.Ascending) });
            //
            //GridColumn_kept
            //
            this.GridColumn_kept.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_kept.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_kept.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_kept.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_kept.Caption = "Attended";
            this.GridColumn_kept.ColumnEdit = this.Status_Kept;
            this.GridColumn_kept.CustomizationCaption = "Client Kept Appt";
            this.GridColumn_kept.FieldName = "status_kept";
            this.GridColumn_kept.Name = "GridColumn_kept";
            this.GridColumn_kept.Visible = true;
            this.GridColumn_kept.VisibleIndex = 0;
            this.GridColumn_kept.Width = 59;
            //
            //Status_Kept
            //
            this.Status_Kept.AutoHeight = false;
            this.Status_Kept.Name = "Status_Kept";
            this.Status_Kept.Tag = "K";
            //
            //GridColumn_StatusMissed
            //
            this.GridColumn_StatusMissed.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_StatusMissed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_StatusMissed.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_StatusMissed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GridColumn_StatusMissed.Caption = "No Show";
            this.GridColumn_StatusMissed.ColumnEdit = this.Status_Missed;
            this.GridColumn_StatusMissed.CustomizationCaption = "Client missed the appointment";
            this.GridColumn_StatusMissed.FieldName = "status_missed";
            this.GridColumn_StatusMissed.Name = "GridColumn_StatusMissed";
            this.GridColumn_StatusMissed.Visible = true;
            this.GridColumn_StatusMissed.VisibleIndex = 1;
            this.GridColumn_StatusMissed.Width = 59;
            //
            //Status_Missed
            //
            this.Status_Missed.AutoHeight = false;
            this.Status_Missed.Name = "Status_Missed";
            this.Status_Missed.Tag = "M";
            //
            //GridColumn_client
            //
            this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_client.Caption = "Client";
            this.GridColumn_client.CustomizationCaption = "Client ID";
            this.GridColumn_client.DisplayFormat.FormatString = "f0";
            this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.FieldName = "client";
            this.GridColumn_client.GroupFormat.FormatString = "f0";
            this.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.GridColumn_client.Name = "GridColumn_client";
            this.GridColumn_client.OptionsColumn.AllowEdit = false;
            this.GridColumn_client.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
            this.GridColumn_client.Visible = true;
            this.GridColumn_client.VisibleIndex = 2;
            this.GridColumn_client.Width = 58;
            //
            //GridColumn_seats
            //
            this.GridColumn_seats.AppearanceCell.Options.UseTextOptions = true;
            this.GridColumn_seats.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_seats.AppearanceHeader.Options.UseTextOptions = true;
            this.GridColumn_seats.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.GridColumn_seats.Caption = "Seats";
            this.GridColumn_seats.CustomizationCaption = "Seats";
            this.GridColumn_seats.DisplayFormat.FormatString = "f0";
            this.GridColumn_seats.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_seats.FieldName = "workshop_people";
            this.GridColumn_seats.GroupFormat.FormatString = "f0";
            this.GridColumn_seats.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.GridColumn_seats.Name = "GridColumn_seats";
            this.GridColumn_seats.OptionsColumn.AllowEdit = false;
            this.GridColumn_seats.Visible = true;
            this.GridColumn_seats.VisibleIndex = 3;
            this.GridColumn_seats.Width = 45;
            //
            //GridColumn_ClientName
            //
            this.GridColumn_ClientName.Caption = "Name";
            this.GridColumn_ClientName.CustomizationCaption = "Client Name";
            this.GridColumn_ClientName.FieldName = "client1_name";
            this.GridColumn_ClientName.Name = "GridColumn_ClientName";
            this.GridColumn_ClientName.OptionsColumn.AllowEdit = false;
            this.GridColumn_ClientName.Visible = true;
            this.GridColumn_ClientName.VisibleIndex = 4;
            this.GridColumn_ClientName.Width = 175;
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(11, 3);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(379, 47);
            this.LabelControl1.TabIndex = 3;
            this.LabelControl1.Text = "This function will permit you to mark the clients who attended a workshop. Check " + "the clients who attended the workshop or not and press the FINISH button.";
            //
            //LabelControl2
            //
            this.LabelControl2.Appearance.Options.UseTextOptions = true;
            this.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl2.Location = new System.Drawing.Point(8, 56);
            this.LabelControl2.Name = "LabelControl2";
            this.LabelControl2.Size = new System.Drawing.Size(82, 13);
            this.LabelControl2.TabIndex = 4;
            this.LabelControl2.Text = "Workshop Name:";
            this.LabelControl2.UseMnemonic = false;
            //
            //LabelControl3
            //
            this.LabelControl3.Appearance.Options.UseTextOptions = true;
            this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl3.Location = new System.Drawing.Point(8, 72);
            this.LabelControl3.Name = "LabelControl3";
            this.LabelControl3.Size = new System.Drawing.Size(27, 13);
            this.LabelControl3.TabIndex = 5;
            this.LabelControl3.Text = "Date:";
            this.LabelControl3.UseMnemonic = false;
            //
            //LabelControl4
            //
            this.LabelControl4.Appearance.Options.UseTextOptions = true;
            this.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.LabelControl4.Location = new System.Drawing.Point(8, 88);
            this.LabelControl4.Name = "LabelControl4";
            this.LabelControl4.Size = new System.Drawing.Size(44, 13);
            this.LabelControl4.TabIndex = 6;
            this.LabelControl4.Text = "Location:";
            this.LabelControl4.UseMnemonic = false;
            //
            //workshop_name
            //
            this.workshop_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.workshop_name.Appearance.Options.UseTextOptions = true;
            this.workshop_name.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.workshop_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.workshop_name.Location = new System.Drawing.Point(104, 56);
            this.workshop_name.Name = "workshop_name";
            this.workshop_name.Size = new System.Drawing.Size(0, 13);
            this.workshop_name.TabIndex = 7;
            this.workshop_name.UseMnemonic = false;
            //
            //workshop_date
            //
            this.workshop_date.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.workshop_date.Appearance.Options.UseTextOptions = true;
            this.workshop_date.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.workshop_date.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.workshop_date.Location = new System.Drawing.Point(104, 72);
            this.workshop_date.Name = "workshop_date";
            this.workshop_date.Size = new System.Drawing.Size(0, 13);
            this.workshop_date.TabIndex = 8;
            this.workshop_date.UseMnemonic = false;
            //
            //workshop_location
            //
            this.workshop_location.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.workshop_location.Appearance.Options.UseTextOptions = true;
            this.workshop_location.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.workshop_location.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.workshop_location.Location = new System.Drawing.Point(104, 88);
            this.workshop_location.Name = "workshop_location";
            this.workshop_location.Size = new System.Drawing.Size(0, 13);
            this.workshop_location.TabIndex = 9;
            this.workshop_location.UseMnemonic = false;
            //
            //Page_Attendance
            //
            this.Controls.Add(this.workshop_location);
            this.Controls.Add(this.workshop_date);
            this.Controls.Add(this.workshop_name);
            this.Controls.Add(this.LabelControl4);
            this.Controls.Add(this.LabelControl3);
            this.Controls.Add(this.LabelControl2);
            this.Controls.Add(this.LabelControl1);
            this.Controls.Add(this.GridControl1);
            this.Controls.Add(this.Button_OK);
            this.Controls.Add(this.SimpleButton_Prev);
            this.Name = "Page_Attendance";
            this.Size = new System.Drawing.Size(400, 304);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Status_Kept).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Status_Missed).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion " Windows Form Designer generated code "

        private readonly DataSet ds = new DataSet("ds");
        private DataTable _tbl;

        private DataTable tbl
        {
            get { return _tbl; }
            set
            {
                if (_tbl != null)
                {
                    _tbl.ColumnChanged -= tbl_ColumnChanged;
                }
                _tbl = value;
                if (_tbl != null)
                {
                    _tbl.ColumnChanged += tbl_ColumnChanged;
                }
            }
        }

        /// <summary>
        /// Load the list of clients who have pending appointments
        /// </summary>
        private void ReadWorkshopInfo()
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlDataReader rd = null;
            try
            {
                cn.Open();
                var _with1 = new SqlCommand();
                _with1.Connection = cn;
                _with1.CommandText = "SELECT wn.description as workshop_name, w.start_time, wl.name as workshop_location FROM workshops w with (nolock) LEFT OUTER JOIN workshop_types wn WITH (NOLOCK) ON w.workshop_type = wn.workshop_type LEFT OUTER JOIN workshop_locations wl WITH (NOLOCK) ON w.workshop_location = wl.workshop_location WHERE w.workshop=@workshop";
                _with1.Parameters.Add("@workshop", SqlDbType.Int).Value = Workshop;
                rd = _with1.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);

                if (rd.Read())
                {
                    if (!rd.IsDBNull(0))
                        workshop_name.Text = rd.GetString(0).Trim();
                    if (!rd.IsDBNull(1))
                        workshop_date.Text = string.Format("{0:MM/dd/yyyy hh:mm tt}", rd.GetDateTime(1));
                    if (!rd.IsDBNull(2))
                        workshop_location.Text = rd.GetString(2);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop information");
            }
            finally
            {
                if (rd != null && !rd.IsClosed)
                    rd.Close();
                if (cn != null && cn.State != ConnectionState.Closed)
                    cn.Close();
            }
        }

        /// <summary>
        /// Load the list of clients who have pending appointments
        /// </summary>
        public void Process()
        {
            ds.Clear();

            // Read the information for the workshop

            Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                // Read the workshop information first
                ReadWorkshopInfo();

                // Try to fill the workshop details next
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with2.CommandText = "SELECT ca.client_appointment, ca.client, ca.appt_time, ca.counselor, ca.start_time, ca.end_time, ca.office, ca.workshop, ca.workshop_people, ca.appt_type, ca.status, ca.result, ca.previous_appointment, ca.referred_to, ca.referred_by, ca.partner, ca.bankruptcy_class, ca.priority, ca.housing, ca.post_purchase, ca.credit, ca.callback_ph, ca.HousingFeeAmount, ca.confirmation_status, ca.date_confirmed, ca.date_updated, ca.date_created, ca.created_by, convert(bit,0) as status_kept, convert(bit,0) as status_missed, dbo.format_normal_name(p1n.prefix,p1n.first,p1n.middle,p1n.last,p1n.suffix) as client1_name FROM client_appointments ca WITH (NOLOCK) LEFT OUTER JOIN people p1 ON ca.client = p1.client AND 1 = p1.relation LEFT OUTER JOIN names p1n WITH (NOLOCK) ON p1.nameid = p1n.name WHERE ca.office IS NULL AND ca.workshop=@workshop AND ca.status='P'";
                    _with2.Parameters.Add("@workshop", SqlDbType.Int).Value = Workshop;
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }
                tbl = ds.Tables[0];

                var _with3 = GridControl1;
                _with3.DataSource = tbl.DefaultView;
                _with3.RefreshDataSource();
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client appointments");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }

        /// <summary>
        /// The previous button was clicked
        /// </summary>
        private void SimpleButton_Prev_Click(object sender, EventArgs e)
        {
            SaveChanges();
            if (PreviousPage != null)
            {
                PreviousPage(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// The Next button was clicked
        /// </summary>
        private void Button_OK_Click(object sender, EventArgs e)
        {
            FindForm().Close();
        }

        /// <summary>
        /// Hook into the form closing event when we load
        /// </summary>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            FindForm().Closing += Form_Closing;
        }

        /// <summary>
        /// The form is being closed. Save the changes.
        /// </summary>
        private void Form_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveChanges();
        }

        /// <summary>
        /// Routine to save the changed data
        /// </summary>

        private void SaveChanges()
        {
            if (tbl != null)
            {
                Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                    using (System.Data.SqlClient.SqlCommand update_cmd = new SqlCommand())
                    {
                        var _with4 = update_cmd;
                        _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with4.CommandText = "UPDATE client_appointments SET status=@status WHERE client_appointment=@client_appointment";
                        _with4.Parameters.Add("@status", SqlDbType.VarChar, 2, "status");
                        _with4.Parameters.Add("@client_appointment", SqlDbType.Int, 0, "client_appointment");

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                        {
                            da.UpdateCommand = update_cmd;
                            da.Update(tbl);
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client appointments");
                }
                finally
                {
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }
        }

        /// <summary>
        /// Process a change on the status of the appointment
        /// </summary>

        private void tbl_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            // Look for a change in our checkbox items to update the current appointment status
            switch (e.Column.ColumnName)
            {
                case "status_kept":
                    if (Convert.ToBoolean(e.ProposedValue))
                        e.Row["status_missed"] = false;
                    break;

                case "status_missed":
                    if (Convert.ToBoolean(e.ProposedValue))
                        e.Row["status_kept"] = false;
                    break;

                default:
                    return;
            }

            // Set the proper status for the trustRegister
            if (Convert.ToBoolean(e.Row["status_missed"]))
            {
                e.Row["status"] = "M";
            }
            else if (Convert.ToBoolean(e.Row["status_kept"]))
            {
                e.Row["status"] = "K";
            }
            else
            {
                e.Row["status"] = "P";
            }
        }
    }
}