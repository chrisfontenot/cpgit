using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client
{
	partial class MainForm : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.NewWorkshopSelectionControl1 = new NewWorkshopSelectionControl();
			this.ClientSelectionControl1 = new ClientSelectionControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//NewWorkshopSelectionControl1
			//
			this.NewWorkshopSelectionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.NewWorkshopSelectionControl1.Location = new System.Drawing.Point(0, 0);
			this.NewWorkshopSelectionControl1.Name = "NewWorkshopSelectionControl1";
			this.NewWorkshopSelectionControl1.Size = new System.Drawing.Size(617, 341);
			this.ToolTipController1.SetSuperTip(this.NewWorkshopSelectionControl1, null);
			this.NewWorkshopSelectionControl1.TabIndex = 0;
			//
			//ClientSelectionControl1
			//
			this.ClientSelectionControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ClientSelectionControl1.Location = new System.Drawing.Point(0, 0);
			this.ClientSelectionControl1.Name = "ClientSelectionControl1";
			this.ClientSelectionControl1.Size = new System.Drawing.Size(617, 341);
			this.ToolTipController1.SetSuperTip(this.ClientSelectionControl1, null);
			this.ClientSelectionControl1.TabIndex = 1;
			this.ClientSelectionControl1.Visible = false;
			//
			//MainForm
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(617, 341);
			this.Controls.Add(this.NewWorkshopSelectionControl1);
			this.Controls.Add(this.ClientSelectionControl1);
			this.Name = "MainForm";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Workshop Clients";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
		}
		protected internal NewWorkshopSelectionControl NewWorkshopSelectionControl1;
		protected internal ClientSelectionControl ClientSelectionControl1;
	}
}
