using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client
{
	partial class ClientSelectionControl : DevExpress.XtraEditors.XtraUserControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_client_appointment = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_client_appointment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.RepositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
			this.GridColumn_name = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_active_status = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_active_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_workshop_people = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_workshop_people.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.RepositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
			this.GridColumn_status = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.RepositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
			this.GridColumn_result = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_result.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_housing = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_housing.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_post_purchase = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_post_purchase.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_credit = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_credit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_callback_ph = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_callback_ph.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_confirmation_status = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_confirmation_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_date_confirmed = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_date_confirmed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_date_updated = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_date_updated.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_address = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_address.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemButtonEdit1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemSpinEdit1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemLookUpEdit1).BeginInit();
			this.SuspendLayout();
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Cancel.Location = new System.Drawing.Point(195, 152);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 5;
			this.Button_Cancel.Text = "&Cancel";
			//
			//Button_OK
			//
			this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_OK.Location = new System.Drawing.Point(99, 152);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 4;
			this.Button_OK.Text = "&OK";
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.EmbeddedNavigator.Name = "";
			this.GridControl1.Location = new System.Drawing.Point(0, 0);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
				this.RepositoryItemLookUpEdit1,
				this.RepositoryItemButtonEdit1,
				this.RepositoryItemSpinEdit1
			});
			this.GridControl1.Size = new System.Drawing.Size(369, 146);
			this.GridControl1.TabIndex = 6;
			this.GridControl1.UseEmbeddedNavigator = true;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
			this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.Empty.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
			this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
			this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
			this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
			this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
			this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
			this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
			this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.Preview.Options.UseFont = true;
			this.GridView1.Appearance.Preview.Options.UseForeColor = true;
			this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.Row.Options.UseBackColor = true;
			this.GridView1.Appearance.Row.Options.UseForeColor = true;
			this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
			this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_client_appointment,
				this.GridColumn_client,
				this.GridColumn_name,
				this.GridColumn_active_status,
				this.GridColumn_workshop_people,
				this.GridColumn_status,
				this.GridColumn_result,
				this.GridColumn_housing,
				this.GridColumn_post_purchase,
				this.GridColumn_credit,
				this.GridColumn_callback_ph,
				this.GridColumn_confirmation_status,
				this.GridColumn_date_confirmed,
				this.GridColumn_date_updated,
				this.GridColumn_date_created,
				this.GridColumn_created_by,
				this.GridColumn_address
			});
			this.GridView1.CustomizationFormBounds = new System.Drawing.Rectangle(638, 264, 208, 170);
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.NewItemRowText = "Add new items here...";
			this.GridView1.OptionsCustomization.AllowRowSizing = true;
			this.GridView1.OptionsDetail.AutoZoomDetail = true;
			this.GridView1.OptionsFilter.UseNewCustomFilterDialog = true;
			this.GridView1.OptionsNavigation.EnterMoveNextColumn = true;
			this.GridView1.OptionsView.EnableAppearanceEvenRow = true;
			this.GridView1.OptionsView.EnableAppearanceOddRow = true;
			this.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
			this.GridView1.OptionsView.RowAutoHeight = true;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			//
			//GridColumn_client_appointment
			//
			this.GridColumn_client_appointment.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_client_appointment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client_appointment.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_client_appointment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client_appointment.Caption = "ID";
			this.GridColumn_client_appointment.CustomizationCaption = "Record ID";
			this.GridColumn_client_appointment.DisplayFormat.FormatString = "f0";
			this.GridColumn_client_appointment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client_appointment.FieldName = "client_appointment";
			this.GridColumn_client_appointment.GroupFormat.FormatString = "f0";
			this.GridColumn_client_appointment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client_appointment.Name = "GridColumn_client_appointment";
			this.GridColumn_client_appointment.OptionsColumn.AllowEdit = false;
			this.GridColumn_client_appointment.ToolTip = "ID of record in database";
			//
			//GridColumn_client
			//
			this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client.Caption = "Client";
			this.GridColumn_client.ColumnEdit = this.RepositoryItemButtonEdit1;
			this.GridColumn_client.CustomizationCaption = "Client ID";
			this.GridColumn_client.DisplayFormat.FormatString = "f0";
			this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client.FieldName = "client";
			this.GridColumn_client.GroupFormat.FormatString = "f0";
			this.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client.Name = "GridColumn_client";
			this.GridColumn_client.ToolTip = "Client ID number";
			this.GridColumn_client.Visible = true;
			this.GridColumn_client.VisibleIndex = 0;
			this.GridColumn_client.Width = 76;
			//
			//RepositoryItemButtonEdit1
			//
			this.RepositoryItemButtonEdit1.AutoHeight = false;
			this.RepositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.RepositoryItemButtonEdit1.DisplayFormat.FormatString = "f0";
			this.RepositoryItemButtonEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.RepositoryItemButtonEdit1.EditFormat.FormatString = "f0";
			this.RepositoryItemButtonEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemButtonEdit1.Name = "RepositoryItemButtonEdit1";
			//
			//GridColumn_name
			//
			this.GridColumn_name.Caption = "Name";
			this.GridColumn_name.CustomizationCaption = "Client Name";
			this.GridColumn_name.FieldName = "name";
			this.GridColumn_name.Name = "GridColumn_name";
			this.GridColumn_name.OptionsColumn.AllowEdit = false;
			this.GridColumn_name.ToolTip = "Client Name";
			this.GridColumn_name.Visible = true;
			this.GridColumn_name.VisibleIndex = 3;
			this.GridColumn_name.Width = 255;
			//
			//GridColumn_active_status
			//
			this.GridColumn_active_status.Caption = "Active Status";
			this.GridColumn_active_status.CustomizationCaption = "Active Status";
			this.GridColumn_active_status.FieldName = "active_status";
			this.GridColumn_active_status.Name = "GridColumn_active_status";
			this.GridColumn_active_status.OptionsColumn.AllowEdit = false;
			this.GridColumn_active_status.ToolTip = "Active Status";
			//
			//GridColumn_workshop_people
			//
			this.GridColumn_workshop_people.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_workshop_people.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_workshop_people.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_workshop_people.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_workshop_people.Caption = "People";
			this.GridColumn_workshop_people.ColumnEdit = this.RepositoryItemSpinEdit1;
			this.GridColumn_workshop_people.CustomizationCaption = "Number of people attending";
			this.GridColumn_workshop_people.DisplayFormat.FormatString = "f0";
			this.GridColumn_workshop_people.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_workshop_people.FieldName = "workshop_people";
			this.GridColumn_workshop_people.GroupFormat.FormatString = "f0";
			this.GridColumn_workshop_people.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_workshop_people.Name = "GridColumn_workshop_people";
			this.GridColumn_workshop_people.ToolTip = "Number of people attending";
			this.GridColumn_workshop_people.Visible = true;
			this.GridColumn_workshop_people.VisibleIndex = 1;
			this.GridColumn_workshop_people.Width = 83;
			//
			//RepositoryItemSpinEdit1
			//
			this.RepositoryItemSpinEdit1.AutoHeight = false;
			this.RepositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.RepositoryItemSpinEdit1.DisplayFormat.FormatString = "f0";
			this.RepositoryItemSpinEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemSpinEdit1.EditFormat.FormatString = "f0";
			this.RepositoryItemSpinEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemSpinEdit1.IsFloatValue = false;
			this.RepositoryItemSpinEdit1.Mask.EditMask = "N00";
			this.RepositoryItemSpinEdit1.MaxLength = 3;
			this.RepositoryItemSpinEdit1.MaxValue = new decimal(new Int32[] {
				100,
				0,
				0,
				0
			});
			this.RepositoryItemSpinEdit1.MinValue = new decimal(new Int32[] {
				1,
				0,
				0,
				0
			});
			this.RepositoryItemSpinEdit1.Name = "RepositoryItemSpinEdit1";
			//
			//GridColumn_status
			//
			this.GridColumn_status.Caption = "Status";
			this.GridColumn_status.ColumnEdit = this.RepositoryItemLookUpEdit1;
			this.GridColumn_status.CustomizationCaption = "Appointment Status";
			this.GridColumn_status.FieldName = "status";
			this.GridColumn_status.Name = "GridColumn_status";
			this.GridColumn_status.ToolTip = "Appointment Status";
			this.GridColumn_status.Visible = true;
			this.GridColumn_status.VisibleIndex = 2;
			this.GridColumn_status.Width = 103;
			//
			//RepositoryItemLookUpEdit1
			//
			this.RepositoryItemLookUpEdit1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.RepositoryItemLookUpEdit1.AutoHeight = false;
			this.RepositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.RepositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("status", "Status", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None)
			});
			this.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1";
			this.RepositoryItemLookUpEdit1.ShowFooter = false;
			//
			//GridColumn_result
			//
			this.GridColumn_result.Caption = "Result";
			this.GridColumn_result.CustomizationCaption = "Appointment Result";
			this.GridColumn_result.FieldName = "result";
			this.GridColumn_result.Name = "GridColumn_result";
			this.GridColumn_result.OptionsColumn.AllowEdit = false;
			this.GridColumn_result.ToolTip = "Appointment Result";
			//
			//GridColumn_housing
			//
			this.GridColumn_housing.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_housing.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_housing.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_housing.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_housing.Caption = "Housing";
			this.GridColumn_housing.CustomizationCaption = "Housing Appointment?";
			this.GridColumn_housing.FieldName = "housing";
			this.GridColumn_housing.Name = "GridColumn_housing";
			this.GridColumn_housing.OptionsColumn.AllowEdit = false;
			this.GridColumn_housing.ToolTip = "Is this a housing appointment?";
			//
			//GridColumn_post_purchase
			//
			this.GridColumn_post_purchase.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_post_purchase.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_post_purchase.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_post_purchase.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_post_purchase.Caption = "Post";
			this.GridColumn_post_purchase.CustomizationCaption = "Post Purchase Appointment?";
			this.GridColumn_post_purchase.FieldName = "post_purchase";
			this.GridColumn_post_purchase.Name = "GridColumn_post_purchase";
			this.GridColumn_post_purchase.OptionsColumn.AllowEdit = false;
			this.GridColumn_post_purchase.ToolTip = "Is this a post-purchase appointment?";
			//
			//GridColumn_credit
			//
			this.GridColumn_credit.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_credit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_credit.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_credit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_credit.Caption = "Credit";
			this.GridColumn_credit.CustomizationCaption = "Credit Review Appointment?";
			this.GridColumn_credit.FieldName = "credit";
			this.GridColumn_credit.Name = "GridColumn_credit";
			this.GridColumn_credit.OptionsColumn.AllowEdit = false;
			this.GridColumn_credit.ToolTip = "Is this a credit review appointment?";
			//
			//GridColumn_callback_ph
			//
			this.GridColumn_callback_ph.Caption = "Callback #";
			this.GridColumn_callback_ph.CustomizationCaption = "Callback Telephone No.";
			this.GridColumn_callback_ph.FieldName = "callback_ph";
			this.GridColumn_callback_ph.Name = "GridColumn_callback_ph";
			this.GridColumn_callback_ph.OptionsColumn.AllowEdit = false;
			this.GridColumn_callback_ph.ToolTip = "Callback telephone number";
			//
			//GridColumn_confirmation_status
			//
			this.GridColumn_confirmation_status.Caption = "Confirmed";
			this.GridColumn_confirmation_status.CustomizationCaption = "Confirmation Status";
			this.GridColumn_confirmation_status.FieldName = "confirmation_status";
			this.GridColumn_confirmation_status.Name = "GridColumn_confirmation_status";
			this.GridColumn_confirmation_status.OptionsColumn.AllowEdit = false;
			this.GridColumn_confirmation_status.ToolTip = "Appointment Confirmation Status";
			//
			//GridColumn_date_confirmed
			//
			this.GridColumn_date_confirmed.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_date_confirmed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_confirmed.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_date_confirmed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_confirmed.Caption = "date_confirmed";
			this.GridColumn_date_confirmed.CustomizationCaption = "date_confirmed";
			this.GridColumn_date_confirmed.DisplayFormat.FormatString = "d";
			this.GridColumn_date_confirmed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_confirmed.FieldName = "date_confirmed";
			this.GridColumn_date_confirmed.GroupFormat.FormatString = "d";
			this.GridColumn_date_confirmed.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_confirmed.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
			this.GridColumn_date_confirmed.Name = "GridColumn_date_confirmed";
			this.GridColumn_date_confirmed.OptionsColumn.AllowEdit = false;
			this.GridColumn_date_confirmed.ToolTip = "Date appointment was confirmed";
			//
			//GridColumn_date_updated
			//
			this.GridColumn_date_updated.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_date_updated.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_updated.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_date_updated.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_updated.Caption = "Date Updated";
			this.GridColumn_date_updated.CustomizationCaption = "Date Record Updated";
			this.GridColumn_date_updated.DisplayFormat.FormatString = "d";
			this.GridColumn_date_updated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_updated.FieldName = "date_updated";
			this.GridColumn_date_updated.GroupFormat.FormatString = "d";
			this.GridColumn_date_updated.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_updated.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
			this.GridColumn_date_updated.Name = "GridColumn_date_updated";
			this.GridColumn_date_updated.OptionsColumn.AllowEdit = false;
			this.GridColumn_date_updated.ToolTip = "Date appointment was last changed";
			//
			//GridColumn_date_created
			//
			this.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_created.Caption = "Created";
			this.GridColumn_date_created.CustomizationCaption = "Date Record Created";
			this.GridColumn_date_created.DisplayFormat.FormatString = "d";
			this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_created.FieldName = "date_created";
			this.GridColumn_date_created.GroupFormat.FormatString = "d";
			this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_created.GroupInterval = DevExpress.XtraGrid.ColumnGroupInterval.Date;
			this.GridColumn_date_created.Name = "GridColumn_date_created";
			this.GridColumn_date_created.OptionsColumn.AllowEdit = false;
			this.GridColumn_date_created.ToolTip = "Date/Time when the appointment was created";
			//
			//GridColumn_created_by
			//
			this.GridColumn_created_by.Caption = "Counselor";
			this.GridColumn_created_by.CustomizationCaption = "Person who created the record";
			this.GridColumn_created_by.FieldName = "created_by";
			this.GridColumn_created_by.Name = "GridColumn_created_by";
			this.GridColumn_created_by.OptionsColumn.AllowEdit = false;
			this.GridColumn_created_by.ToolTip = "Name of the person who created the appointment";
			//
			//GridColumn_address
			//
			this.GridColumn_address.Caption = "Address";
			this.GridColumn_address.CustomizationCaption = "Client Address";
			this.GridColumn_address.FieldName = "address";
			this.GridColumn_address.Name = "GridColumn_address";
			this.GridColumn_address.OptionsColumn.AllowEdit = false;
			this.GridColumn_address.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.False;
			this.GridColumn_address.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
			this.GridColumn_address.ToolTip = "Client's Address";
			//
			//ClientSelectionControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GridControl1);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.Name = "ClientSelectionControl";
			this.Size = new System.Drawing.Size(369, 189);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemButtonEdit1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemSpinEdit1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemLookUpEdit1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_appointment;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_name;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_active_status;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_workshop_people;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_status;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_result;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_housing;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_post_purchase;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_credit;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_callback_ph;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_confirmation_status;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_confirmed;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_updated;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit RepositoryItemLookUpEdit1;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit RepositoryItemButtonEdit1;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit RepositoryItemSpinEdit1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_address;
	}
}
