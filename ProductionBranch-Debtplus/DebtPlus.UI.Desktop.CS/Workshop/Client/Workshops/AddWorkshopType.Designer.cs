using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
	partial class AddWorkshopType : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.tx_description = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.sp_duration = new DevExpress.XtraEditors.SpinEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.cbl_contents = new DevExpress.XtraEditors.CheckedListBoxControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.tx_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.sp_duration.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.cbl_contents).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(12, 18);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(53, 13);
			this.LabelControl2.TabIndex = 0;
			this.LabelControl2.Text = "Description";
			//
			//tx_description
			//
			this.tx_description.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.tx_description.Location = new System.Drawing.Point(73, 15);
			this.tx_description.Name = "tx_description";
			this.tx_description.Properties.MaxLength = 50;
			this.tx_description.Size = new System.Drawing.Size(230, 20);
			this.tx_description.TabIndex = 1;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 44);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(41, 13);
			this.LabelControl3.TabIndex = 2;
			this.LabelControl3.Text = "Duration";
			//
			//sp_duration
			//
			this.sp_duration.EditValue = new decimal(new Int32[] {15, 0, 0, 0});
			this.sp_duration.Location = new System.Drawing.Point(73, 41);
			this.sp_duration.Name = "sp_duration";
			this.sp_duration.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.sp_duration.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.sp_duration.Properties.Increment = new decimal(new Int32[] {
				15,
				0,
				0,
				0
			});
			this.sp_duration.Properties.IsFloatValue = false;
			this.sp_duration.Properties.Mask.BeepOnError = true;
			this.sp_duration.Properties.Mask.EditMask = "n0";
			this.sp_duration.Properties.MaxValue = new decimal(new Int32[] {
				1440,
				0,
				0,
				0
			});
			this.sp_duration.Properties.NullText = "0";
			this.sp_duration.Size = new System.Drawing.Size(57, 20);
			this.sp_duration.TabIndex = 3;
			this.sp_duration.ToolTip = "Number of minutes that this workshop should take.";
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(136, 44);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(37, 13);
			this.LabelControl4.TabIndex = 4;
			this.LabelControl4.Text = "minutes";
			//
			//cbl_contents
			//
			this.cbl_contents.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.cbl_contents.Location = new System.Drawing.Point(12, 97);
			this.cbl_contents.Name = "cbl_contents";
			this.cbl_contents.Size = new System.Drawing.Size(372, 160);
			this.cbl_contents.TabIndex = 6;
			//
			//LabelControl5
			//
			this.LabelControl5.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl5.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255));
			this.LabelControl5.Appearance.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255));
			this.LabelControl5.Appearance.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(192), (Int32)Convert.ToByte(192), (Int32)Convert.ToByte(255));
			this.LabelControl5.Appearance.Options.UseBackColor = true;
			this.LabelControl5.Appearance.Options.UseBorderColor = true;
			this.LabelControl5.Appearance.Options.UseTextOptions = true;
			this.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.LabelControl5.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.LabelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl5.Location = new System.Drawing.Point(12, 78);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(372, 13);
			this.LabelControl5.TabIndex = 5;
			this.LabelControl5.Text = "Contents";
			//
			//Button_OK
			//
			this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.Button_OK.Enabled = false;
			this.Button_OK.Location = new System.Drawing.Point(309, 12);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 7;
			this.Button_OK.Text = "&OK";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(309, 41);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 8;
			this.Button_Cancel.Text = "&Cancel";
			//
			//AddWorkshopType
			//
			this.AcceptButton = this.Button_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(396, 269);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.cbl_contents);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.sp_duration);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.tx_description);
			this.Controls.Add(this.LabelControl2);
			this.Name = "AddWorkshopType";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Add New Workshop Type";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.tx_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.sp_duration.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.cbl_contents).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.TextEdit tx_description;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.SpinEdit sp_duration;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.CheckedListBoxControl cbl_contents;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl5;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
	}
}
