#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Svc.DataSets;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
    public partial class NewWorkshop
    {
        private System.Data.DataRowView drv = null;

        public NewWorkshop(System.Data.DataRowView drv) : base()
        {
            this.InitializeComponent();
            this.Load += NewWorkshop_Load;
            Button_OK.Click += Button_OK_Click;
            this.drv = drv;
        }

        private void NewWorkshop_Load(object sender, System.EventArgs e)
        {
            // Load the workshop types
            DataTable typTbl = WorkshopTypesTable();
            lk_workshop.Properties.DataSource = new System.Data.DataView(typTbl, string.Empty, "description", DataViewRowState.CurrentRows);
            lk_workshop.Properties.DisplayMember = "description";
            lk_workshop.Properties.ValueMember = "workshop_type";

            // Bind the data to the control
            lk_workshop.DataBindings.Clear();
            lk_workshop.DataBindings.Add(new Binding("EditValue", drv, "workshop_type"));

            // If there is no workshop type then create one
            if (drv.IsNew)
            {
                System.Data.DataView vue = new System.Data.DataView(typTbl, string.Empty, "workshop_type", DataViewRowState.CurrentRows);
                if (vue.Count > 0)
                {
                    drv["workshop_type"] = vue[0]["workshop_type"];
                }
            }
            lk_workshop.ButtonClick += lk_workshop_ButtonClick;
            lk_workshop.EditValueChanged += EditValueChanged;

            // Set the location
            DataTable wksTbl = WorkshopLocationsTable();
            lk_location.Properties.DataSource = new System.Data.DataView(wksTbl, string.Empty, "name", DataViewRowState.CurrentRows);
            lk_location.Properties.DisplayMember = "name";
            lk_location.Properties.ValueMember = "workshop_location";

            // Bind the data to the control
            lk_location.DataBindings.Clear();
            lk_location.DataBindings.Add(new Binding("EditValue", drv, "workshop_location"));

            // If there is no workshop type then create one
            if (drv.IsNew)
            {
                System.Data.DataView vue = new System.Data.DataView(wksTbl, string.Empty, "workshop_location", DataViewRowState.CurrentRows);
                if (vue.Count > 0)
                {
                    drv["workshop_location"] = vue[0]["workshop_location"];
                }
            }

            lk_location.ButtonClick += lk_location_ButtonClick;
            lk_location.EditValueChanged += EditValueChanged;

            // Set the counselor
            using (var DataClass = new ClientDataSet())
            {
                System.Nullable<Int32> counselorID = global::DebtPlus.Utils.Nulls.v_Int32(drv["counselor"]);
                System.Collections.Generic.List<DebtPlus.LINQ.counselor> lst = DebtPlus.LINQ.Cache.counselor.CounselorsList(counselorID);

                lk_counselor.Properties.DataSource = lst;
                lk_counselor.Properties.DisplayMember = "Name";
                lk_counselor.Properties.ValueMember = "Person";

                // If there is no counselor for this workshop then find the default item
                if (object.ReferenceEquals(drv["counselor"], System.DBNull.Value))
                {
                    DebtPlus.LINQ.counselor defItem = lst.Find(s => s.Default);
                    if (defItem != null)
                    {
                        drv["counselor"] = defItem.Id;
                    }
                }

                // Bind the data to the control
                lk_counselor.DataBindings.Clear();
                lk_counselor.DataBindings.Add(new Binding("EditValue", drv, "counselor"));

                lk_counselor.EditValueChanged += EditValueChanged;
            }

            // Set the starting date/time values
            if (drv["start_time"] != null && !object.ReferenceEquals(drv["start_time"], System.DBNull.Value))
            {
                dt_date.EditValue = Convert.ToDateTime(drv["start_time"]);
            }

            // Start with no seats. We will add attendees later when we have completed the processing
            if (drv.IsNew)
            {
                drv["seats_available"] = 0;
            }

            // Add the handlers to detect changes
            dt_date.EditValueChanged += dt_date_EditValueChanged;
            dt_date.EditValueChanging += dt_date_EditValueChanging;

            // Enable/Disable the OK button
            EnableOK();
        }

        private System.Data.DataTable WorkshopTypesTable()
        {
            System.Data.DataTable tbl = GlobalValues.ds.Tables["workshop_types"];

            if (tbl == null)
            {
                System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                cmd.CommandText = "SELECT [workshop_type],[description],[duration],[HUD_Grant],[HousingFeeAmount],[ActiveFlag],[date_created],[created_by] FROM workshop_types";
                cmd.CommandType = CommandType.Text;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);

                System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    da.Fill(GlobalValues.ds, "workshop_types");
                    tbl = GlobalValues.ds.Tables["workshop_types"];
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["workshop_type"] };
                    var col = tbl.Columns["workshop_type"];
                    col.AutoIncrement = true;
                    col.AutoIncrementSeed = -1;
                    col.AutoIncrementStep = -1;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_types table");
                }
                finally
                {
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        private System.Data.DataTable WorkshopLocationsTable()
        {
            System.Data.DataTable tbl = GlobalValues.ds.Tables["workshop_locations"];

            if (tbl == null)
            {
                System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                cmd.CommandText = "SELECT [workshop_location],[name],[organization],[type],[AddressID],[TelephoneID],[directions],[milage],[ActiveFlag],[hcs_id],[date_created],[created_by] FROM workshop_locations";
                cmd.CommandType = CommandType.Text;
                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);

                System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    da.Fill(GlobalValues.ds, "workshop_locations");
                    tbl = GlobalValues.ds.Tables["workshop_locations"];
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["workshop_location"] };
                    var col = tbl.Columns["workshop_location"];
                    col.AutoIncrement = true;
                    col.AutoIncrementSeed = -1;
                    col.AutoIncrementStep = -1;
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_locations table");
                }
                finally
                {
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        private void lk_workshop_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                DataTable tbl = WorkshopTypesTable();
                DataView vue = tbl.DefaultView;
                DataRowView edit_drv = vue.AddNew();
                var frm = new AddWorkshopType(edit_drv);
                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    edit_drv.EndEdit();
                    drv["workshop_type"] = edit_drv["workshop_type"];
                    lk_workshop.EditValue = edit_drv["workshop_type"];
                }
                else
                {
                    edit_drv.CancelEdit();
                }
            }
        }

        private void lk_location_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Plus)
            {
                DataTable tbl = WorkshopLocationsTable();
                DataView vue = tbl.DefaultView;
                DataRowView edit_drv = vue.AddNew();

                var frm = new AddWorkshopLocation(edit_drv);

                if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    edit_drv.EndEdit();
                    drv["workshop_location"] = edit_drv["workshop_location"];
                    lk_location.EditValue = edit_drv["workshop_location"];
                }
                else
                {
                    edit_drv.CancelEdit();
                }
            }
        }

        private void dt_date_EditValueChanged(object sender, System.EventArgs e)
        {
            System.DateTime dt = new System.DateTime(Convert.ToDateTime(dt_date.EditValue).Year, Convert.ToDateTime(dt_date.EditValue).Month, Convert.ToDateTime(dt_date.EditValue).Day, Convert.ToDateTime(dt_date.EditValue).Hour, Convert.ToDateTime(dt_date.EditValue).Minute, 0);
            drv["start_time"] = dt;
            EnableOK();
        }

        private void dt_date_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            System.DateTime dt = Convert.ToDateTime(e.NewValue);
            System.Int32 days = (System.DateTime.Now - dt).Days;
            if (days > 60)
            {
                days = (System.DateTime.Now - Convert.ToDateTime(e.OldValue)).Days;
                if (days < 60)
                {
                    e.Cancel = global::DebtPlus.Data.Forms.MessageBox.Show("You are attempting to create a workshop that is more than 60 days ago." + Environment.NewLine + "Are you really sure that you want to do this?" + Environment.NewLine + Environment.NewLine + "It is a very old workshop and that may effect existing reports.", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) != System.Windows.Forms.DialogResult.Yes;
                }
            }
        }

        private void EnableOK()
        {
            if (lk_counselor.EditValue == null || object.ReferenceEquals(lk_counselor.EditValue, System.DBNull.Value))
            {
                Button_OK.Enabled = false;
                return;
            }

            if (lk_location.EditValue == null || object.ReferenceEquals(lk_location.EditValue, System.DBNull.Value))
            {
                Button_OK.Enabled = false;
                return;
            }

            if (lk_workshop.EditValue == null || object.ReferenceEquals(lk_workshop.EditValue, System.DBNull.Value))
            {
                Button_OK.Enabled = false;
                return;
            }

            Button_OK.Enabled = true;
        }

        private void EditValueChanged(object sender, System.EventArgs e)
        {
            EnableOK();
        }

        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
            {
                using (var cm = new global::DebtPlus.UI.Common.CursorManager())
                {
                    System.Int32 workshop = -1;
                    try
                    {
                        cn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "INSERT INTO workshops (workshop_type, start_time, workshop_location, seats_available, counselor) values (@workshop_type, @start_time, @workshop_location, @seats_available, @counselor); SELECT scope_identity() as workshop;";
                            cmd.Parameters.Add("@workshop_type", SqlDbType.Int).Value = drv["workshop_type"];
                            cmd.Parameters.Add("@start_time", SqlDbType.DateTime).Value = drv["start_time"];
                            cmd.Parameters.Add("@workshop_location", SqlDbType.Int).Value = drv["workshop_location"];
                            cmd.Parameters.Add("@seats_available", SqlDbType.Int).Value = drv["seats_available"];
                            cmd.Parameters.Add("@counselor", SqlDbType.VarChar, 80).Value = drv["counselor"];

                            object objAnswer = cmd.ExecuteScalar();
                            if (objAnswer != null && !object.ReferenceEquals(objAnswer, System.DBNull.Value))
                            {
                                workshop = Convert.ToInt32(objAnswer);
                            }
                        }

                        if (workshop < 0)
                        {
                            throw new ApplicationException("No workshop for new record");
                        }
                        drv["workshop"] = workshop;
                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding workshop");
                    }
                }
            }
        }
    }
}