using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
    internal partial class NewWorkshopSelectionControl : WorkshopSelectionControl
    {
        public NewWorkshopSelectionControl() : base()
        {
            InitializeComponent();
            Button_New.Click += Button_New_Click;
        }

        private void Button_New_Click(object sender, System.EventArgs e)
        {
            System.Data.DataView vue = (System.Data.DataView)GridControl1.DataSource;
            System.Data.DataRowView drv = vue.AddNew();

            drv.BeginEdit();

            // Find a good "start time" for the workshop
            DateTime StartingTime = DateTime.Now.AddHours(1);
            StartingTime = new DateTime(StartingTime.Year, StartingTime.Month, StartingTime.Day, StartingTime.Hour, 0, 0);
            drv["start_time"] = StartingTime;

            var _with1 = new NewWorkshop(drv);
            if (_with1.ShowDialog() == DialogResult.OK)
            {
                drv.EndEdit();
                GlobalValues.workshop = Convert.ToInt32(drv["workshop"]);
                RaiseCompleted();
            }
            else
            {
                drv.CancelEdit();
            }
        }
    }
}