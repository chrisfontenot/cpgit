#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
    public partial class AddWorkshopType
    {
        private System.Data.DataRowView drv = null;

        public AddWorkshopType(System.Data.DataRowView drv) : base()
        {
            this.InitializeComponent();
            this.drv = drv;
            this.Load += AddWorkshopType_Load;
            cbl_contents.ItemCheck += cbl_contents_ItemCheck;
            tx_description.EditValueChanged += tx_description_EditValueChanged;
            Button_OK.Click += Button_OK_Click;
        }

        #region " MyCheckedControl "

        /// <summary>
        /// Class to handle the checked status of our indicators
        /// </summary>
        private class MyCheckedControl : DevExpress.XtraEditors.Controls.CheckedListBoxItem
        {
            public override string Description
            {
                get { return Convert.ToString(this.Value); }
                set { this.Value = value; }
            }

            private System.Int32 _content_type = -1;

            public System.Int32 content_type
            {
                get { return _content_type; }
                set { _content_type = value; }
            }

            public MyCheckedControl(string Description, System.Int32 content_type, CheckState CheckState) : base()
            {
                this.Description = Description;
                this.content_type = content_type;
                this.CheckState = CheckState;
            }
        }

        #endregion " MyCheckedControl "

        private void AddWorkshopType_Load(System.Object sender, System.EventArgs e)
        {
            if (drv["description"] == null || object.ReferenceEquals(drv["description"], System.DBNull.Value))
            {
                drv["description"] = string.Empty;
            }
            tx_description.DataBindings.Clear();
            tx_description.DataBindings.Add(new Binding("EditValue", drv, "description"));
            tx_description.EditValueChanged += tx_description_EditValueChanged;

            if (drv["duration"] == null || object.ReferenceEquals(drv["duration"], System.DBNull.Value))
            {
                drv["duration"] = 15;
            }
            sp_duration.DataBindings.Clear();
            sp_duration.DataBindings.Add(new Binding("EditValue", drv, "duration"));
            sp_duration.EditValueChanging += sp_duration_EditValueChanging;
            sp_duration.EditValueChanged += tx_description_EditValueChanged;

            DataTable tbl = WorkshopContentsTable();
            DataView vue = new DataView(tbl, string.Empty, "description", DataViewRowState.CurrentRows);
            cbl_contents.Items.Clear();
            foreach (DataRowView vueDrv in vue)
            {
                string description = Convert.ToString(vueDrv["description"]);
                System.Int32 content_type = Convert.ToInt32(vueDrv["content_type"]);
                CheckState @checked = (CheckState)vueDrv["checked"];
                cbl_contents.Items.Add(new MyCheckedControl(description, content_type, @checked));
            }

            // Enable/Disable the OK button
            EnableOK();
        }

        private DataTable WorkshopContentsTable()
        {
            DataTable tbl = null;
            System.Data.DataSet ds = new System.Data.DataSet("ds");

            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            cmd.CommandType = CommandType.Text;
            if (drv.IsNew)
            {
                cmd.CommandText = "select wt.content_type, wt.name as description, convert(int, 0) as checked from workshop_content_types wt";
            }
            else
            {
                cmd.CommandText = "select wt.content_type, wt.name as description, convert(int, case when wc.content_type is not null then 1 else 0 end) as checked from workshop_content_types wt left outer join workshop_contents wc ON wt.content_type = wc.content_type AND wc.workshop_type = @workshop_type";
                cmd.Parameters.Add("@workshop_type", SqlDbType.Int).Value = drv["workshop_type"];
            }

            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;

            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                da.Fill(ds, "workshop_content_types");
                tbl = ds.Tables["workshop_content_types"];
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_content_types table");
            }
            finally
            {
                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            return tbl;
        }

        private void sp_duration_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            System.Int32 NewValue = 0;
            if (e.NewValue != null && Int32.TryParse(Convert.ToString(e.NewValue), out NewValue))
            {
                if (NewValue > 0)
                {
                    return;
                }
            }
            e.Cancel = true;
        }

        private void cbl_contents_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            EnableOK();
        }

        private void tx_description_EditValueChanged(object sender, System.EventArgs e)
        {
            EnableOK();
        }

        private void EnableOK()
        {
            if (Convert.ToString(tx_description.EditValue).Trim() == string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            if (Convert.ToInt32(sp_duration.EditValue) <= 0)
            {
                Button_OK.Enabled = false;
                return;
            }

            foreach (MyCheckedControl item in cbl_contents.Items)
            {
                if (item.CheckState == CheckState.Checked)
                {
                    Button_OK.Enabled = true;
                    return;
                }
            }

            Button_OK.Enabled = false;
        }

        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlTransaction txn = null;
            System.Data.SqlClient.SqlCommand cmd = null;

            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
            try
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;

                // Open the connection and make a transaction
                cn.Open();
                txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted);

                // Start by ensuring that we have a workshop_type
                System.Int32 workshop_type = -1;
                if (!drv.IsNew && drv["workshop_type"] != null && object.ReferenceEquals(drv["workshop_type"], System.DBNull.Value))
                    workshop_type = Convert.ToInt32(drv["workshop_type"]);

                if (workshop_type < 0)
                {
                    cmd = new SqlCommand();
                    cmd.Connection = cn;
                    cmd.Transaction = txn;
                    cmd.CommandText = "INSERT INTO workshop_types (description,duration) VALUES (@description, @duration); SELECT scope_identity() AS workshop_type";
                    cmd.Parameters.Add("@description", SqlDbType.VarChar, 50).Value = Convert.ToString(tx_description.EditValue).Trim();
                    cmd.Parameters.Add("@duration", SqlDbType.Int).Value = Convert.ToInt32(sp_duration.EditValue);

                    object objAnswer = cmd.ExecuteScalar();
                    if (objAnswer != null && !object.ReferenceEquals(objAnswer, System.DBNull.Value))
                        workshop_type = Convert.ToInt32(objAnswer);

                    if (workshop_type < 0)
                        throw new ApplicationException("Error attempting to create a workshop_type");
                    drv["workshop_type"] = workshop_type;
                }

                // Discard all checked items for this workshop type
                cmd = new SqlCommand();
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "DELETE FROM workshop_contents WHERE workshop_type=@workshop_type";
                cmd.Parameters.Add("@workshop_type", SqlDbType.Int).Value = workshop_type;
                cmd.ExecuteNonQuery();

                // Add any checked items to the list
                foreach (MyCheckedControl item in cbl_contents.Items)
                {
                    if (item.CheckState == CheckState.Checked)
                    {
                        cmd = new SqlCommand();
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = "INSERT INTO workshop_contents(workshop_type, content_type) VALUES (@workshop_type, @content_type)";
                        cmd.Parameters.Add("@workshop_type", SqlDbType.Int).Value = workshop_type;
                        cmd.Parameters.Add("@content_type", SqlDbType.Int).Value = item.content_type;
                        cmd.ExecuteNonQuery();
                    }
                }

                // Commit the transaction
                txn.Commit();
                txn = null;

                // Complete the dialog properly
                DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating workshop_types information");
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch
                    {
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();
                System.Windows.Forms.Cursor.Current = current_cursor;
            }
        }
    }
}