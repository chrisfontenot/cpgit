using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
	partial class NewWorkshopSelectionControl : WorkshopSelectionControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Button_New = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Text = "Select the workshop that you wish to update or click New to create a new workshop" + "";
			//
			//Button_OK
			//
			this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.Button_OK.Location = new System.Drawing.Point(218, 182);
			this.Button_OK.TabIndex = 3;
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.Button_Cancel.Location = new System.Drawing.Point(309, 182);
			this.Button_Cancel.TabIndex = 4;
			//
			//Button_New
			//
			this.Button_New.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.Button_New.Location = new System.Drawing.Point(13, 182);
			this.Button_New.Name = "Button_New";
			this.Button_New.Size = new System.Drawing.Size(75, 23);
			this.Button_New.TabIndex = 2;
			this.Button_New.Text = "&New...";
			this.Button_New.ToolTip = "Create a new workshop";
			//
			//NewWorkshopSelectionControl
			//
			this.Controls.Add(this.Button_New);
			this.Name = "NewWorkshopSelectionControl";
			this.Controls.SetChildIndex(this.Button_Cancel, 0);
			this.Controls.SetChildIndex(this.Button_OK, 0);
			this.Controls.SetChildIndex(this.Button_New, 0);
			this.Controls.SetChildIndex(this.LabelControl1, 0);
			this.Controls.SetChildIndex(this.GridControl1, 0);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			this.ResumeLayout(false);
		}
		protected internal DevExpress.XtraEditors.SimpleButton Button_New;
	}
}
