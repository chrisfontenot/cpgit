using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
	partial class NewWorkshop : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			DevExpress.Utils.SuperToolTip SuperToolTip3 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipTitleItem ToolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
			DevExpress.Utils.ToolTipItem ToolTipItem3 = new DevExpress.Utils.ToolTipItem();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewWorkshop));
			DevExpress.Utils.SuperToolTip SuperToolTip1 = new DevExpress.Utils.SuperToolTip();
			DevExpress.Utils.ToolTipTitleItem ToolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
			DevExpress.Utils.ToolTipItem ToolTipItem1 = new DevExpress.Utils.ToolTipItem();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.lk_workshop = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.dt_date = new DevExpress.XtraEditors.DateEdit();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.lk_location = new DevExpress.XtraEditors.LookUpEdit();
			this.lk_counselor = new DevExpress.XtraEditors.LookUpEdit();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.lk_workshop.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.dt_date.Properties.VistaTimeProperties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.dt_date.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.lk_location.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.lk_counselor.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl1.Location = new System.Drawing.Point(12, 12);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(307, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Please enter the information to create a new workshop instance";
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(12, 51);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(48, 13);
			this.LabelControl2.TabIndex = 1;
			this.LabelControl2.Text = "&Workshop";
			//
			//lk_workshop
			//
			this.lk_workshop.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lk_workshop.Location = new System.Drawing.Point(84, 48);
			this.lk_workshop.Name = "lk_workshop";
			this.lk_workshop.Properties.ActionButtonIndex = 1;
			this.lk_workshop.Properties.AllowFocused = false;
			this.lk_workshop.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
			});
			this.lk_workshop.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 200, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near) });
			this.lk_workshop.Properties.NullText = "";
			this.lk_workshop.Properties.ShowFooter = false;
			this.lk_workshop.Properties.ShowHeader = false;
			this.lk_workshop.Size = new System.Drawing.Size(281, 20);
			ToolTipTitleItem3.Text = "Workshop Type";
			ToolTipItem3.LeftIndent = 6;
			ToolTipItem3.Text = resources.GetString("ToolTipItem3.Text");
			SuperToolTip3.Items.Add(ToolTipTitleItem3);
			SuperToolTip3.Items.Add(ToolTipItem3);
			this.lk_workshop.SuperTip = SuperToolTip3;
			this.lk_workshop.TabIndex = 2;
			this.lk_workshop.Properties.SortColumnIndex = 0;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 77);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(40, 13);
			this.LabelControl3.TabIndex = 3;
			this.LabelControl3.Text = "&Location";
			//
			//dt_date
			//
			this.dt_date.EditValue = null;
			this.dt_date.Location = new System.Drawing.Point(84, 126);
			this.dt_date.Name = "dt_date";
			this.dt_date.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.dt_date.Properties.DisplayFormat.FormatString = "g";
			this.dt_date.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.dt_date.Properties.EditFormat.FormatString = "g";
			this.dt_date.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.dt_date.Properties.Mask.BeepOnError = true;
			this.dt_date.Properties.Mask.EditMask = "g";
			this.dt_date.Properties.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
			this.dt_date.Properties.VistaEditTime = DevExpress.Utils.DefaultBoolean.True;
			this.dt_date.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.dt_date.Size = new System.Drawing.Size(281, 20);
			this.dt_date.TabIndex = 8;
			this.dt_date.ToolTip = "Date for the workshop";
			//
			//LabelControl4
			//
			this.LabelControl4.Location = new System.Drawing.Point(12, 103);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(47, 13);
			this.LabelControl4.TabIndex = 5;
			this.LabelControl4.Text = "&Presenter";
			//
			//lk_location
			//
			this.lk_location.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lk_location.Location = new System.Drawing.Point(84, 74);
			this.lk_location.Name = "lk_location";
			this.lk_location.Properties.ActionButtonIndex = 1;
			this.lk_location.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus),
				new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)
			});
			this.lk_location.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "ID", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None) });
			this.lk_location.Properties.NullText = "";
			this.lk_location.Properties.ShowFooter = false;
			this.lk_location.Properties.ShowHeader = false;
			this.lk_location.Size = new System.Drawing.Size(281, 20);
			ToolTipTitleItem1.Text = "Workshop Location";
			ToolTipItem1.LeftIndent = 6;
			ToolTipItem1.Text = resources.GetString("ToolTipItem1.Text");
			SuperToolTip1.Items.Add(ToolTipTitleItem1);
			SuperToolTip1.Items.Add(ToolTipItem1);
			this.lk_location.SuperTip = SuperToolTip1;
			this.lk_location.TabIndex = 4;
			this.lk_location.Properties.SortColumnIndex = 0;
			//
			//lk_counselor
			//
			this.lk_counselor.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lk_counselor.Location = new System.Drawing.Point(84, 100);
			this.lk_counselor.Name = "lk_counselor";
			this.lk_counselor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.lk_counselor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "ID", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.None) });
			this.lk_counselor.Properties.NullText = "";
			this.lk_counselor.Properties.ShowFooter = false;
			this.lk_counselor.Properties.ShowHeader = false;
			this.lk_counselor.Size = new System.Drawing.Size(281, 20);
			this.lk_counselor.TabIndex = 6;
			this.lk_counselor.ToolTip = "Name of the counselor/instructor who is conducting the workshop";
			this.lk_counselor.Properties.SortColumnIndex = 0;
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(12, 129);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(49, 13);
			this.LabelControl5.TabIndex = 7;
			this.LabelControl5.Text = "&Date/Time";
			//
			//Button_OK
			//
			this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_OK.Enabled = false;
			this.Button_OK.Location = new System.Drawing.Point(110, 162);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 10;
			this.Button_OK.Text = "&OK";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(191, 162);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 11;
			this.Button_Cancel.Text = "&Cancel";
			//
			//NewWorkshop
			//
			this.AcceptButton = this.Button_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(377, 197);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.lk_counselor);
			this.Controls.Add(this.lk_location);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.dt_date);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.lk_workshop);
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.LabelControl1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "NewWorkshop";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "New Workshop";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.lk_workshop.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.dt_date.Properties.VistaTimeProperties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.dt_date.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.lk_location.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.lk_counselor.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.LookUpEdit lk_workshop;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.DateEdit dt_date;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.LookUpEdit lk_location;
		protected internal DevExpress.XtraEditors.LookUpEdit lk_counselor;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl5;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
	}
}
