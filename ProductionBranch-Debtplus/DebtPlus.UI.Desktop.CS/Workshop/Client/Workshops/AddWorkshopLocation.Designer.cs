using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
	partial class AddWorkshopLocation : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;

		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.tx_description = new DevExpress.XtraEditors.TextEdit();
			this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.lk_type = new DevExpress.XtraEditors.LookUpEdit();
			this.sp_milage = new DevExpress.XtraEditors.SpinEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.me_directions = new DevExpress.XtraEditors.MemoExEdit();
			this.adr = new DebtPlus.Data.Controls.AddressParts();
			this.ph_phone = new DebtPlus.Data.Controls.PhoneNumber();
			this.tx_name = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl7 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.tx_description.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.lk_type.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.sp_milage.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.me_directions.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ph_phone.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.tx_name.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl2
			//
			this.LabelControl2.Location = new System.Drawing.Point(12, 18);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(27, 13);
			this.LabelControl2.TabIndex = 0;
			this.LabelControl2.Text = "Name";
			//
			//tx_description
			//
			this.tx_description.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.tx_description.Location = new System.Drawing.Point(85, 15);
			this.tx_description.Name = "tx_description";
			this.tx_description.Properties.MaxLength = 50;
			this.tx_description.Size = new System.Drawing.Size(218, 20);
			this.tx_description.TabIndex = 1;
			//
			//Button_OK
			//
			this.Button_OK.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.Button_OK.Enabled = false;
			this.Button_OK.Location = new System.Drawing.Point(309, 12);
			this.Button_OK.Name = "Button_OK";
			this.Button_OK.Size = new System.Drawing.Size(75, 23);
			this.Button_OK.TabIndex = 14;
			this.Button_OK.Text = "&OK";
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.Button_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.Button_Cancel.Location = new System.Drawing.Point(309, 41);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 15;
			this.Button_Cancel.Text = "&Cancel";
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(12, 41);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(24, 13);
			this.LabelControl1.TabIndex = 2;
			this.LabelControl1.Text = "Type";
			//
			//lk_type
			//
			this.lk_type.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lk_type.Location = new System.Drawing.Point(85, 38);
			this.lk_type.Name = "lk_type";
			this.lk_type.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.lk_type.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("name", "Name", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending) });
			this.lk_type.Properties.NullText = "";
			this.lk_type.Properties.ShowFooter = false;
			this.lk_type.Properties.ShowHeader = false;
			this.lk_type.Size = new System.Drawing.Size(218, 20);
			this.lk_type.TabIndex = 3;
			this.lk_type.Properties.SortColumnIndex = 0;
			//
			//sp_milage
			//
			this.sp_milage.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.sp_milage.EditValue = new decimal(new Int32[] {
				0,
				0,
				0,
				0
			});
			this.sp_milage.Location = new System.Drawing.Point(323, 170);
			this.sp_milage.Name = "sp_milage";
			this.sp_milage.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
			this.sp_milage.Size = new System.Drawing.Size(61, 20);
			this.sp_milage.TabIndex = 11;
			//
			//LabelControl3
			//
			this.LabelControl3.Location = new System.Drawing.Point(12, 199);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(47, 13);
			this.LabelControl3.TabIndex = 12;
			this.LabelControl3.Text = "Directions";
			//
			//LabelControl4
			//
			this.LabelControl4.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl4.Location = new System.Drawing.Point(287, 173);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(30, 13);
			this.LabelControl4.TabIndex = 10;
			this.LabelControl4.Text = "Milage";
			//
			//LabelControl5
			//
			this.LabelControl5.Location = new System.Drawing.Point(12, 173);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(30, 13);
			this.LabelControl5.TabIndex = 8;
			this.LabelControl5.Text = "Phone";
			//
			//LabelControl6
			//
			this.LabelControl6.Location = new System.Drawing.Point(12, 99);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(39, 13);
			this.LabelControl6.TabIndex = 6;
			this.LabelControl6.Text = "Address";
			//
			//me_directions
			//
			this.me_directions.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.me_directions.Location = new System.Drawing.Point(85, 196);
			this.me_directions.Name = "me_directions";
			this.me_directions.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.me_directions.Properties.MaxLength = 1024;
			this.me_directions.Properties.ShowIcon = false;
			this.me_directions.Size = new System.Drawing.Size(299, 20);
			this.me_directions.TabIndex = 13;
			//
			//adr
			//
			this.adr.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.adr.Location = new System.Drawing.Point(85, 96);
			this.adr.Name = "adr";
			this.adr.Size = new System.Drawing.Size(299, 72);
			this.ToolTipController1.SetSuperTip(this.adr, null);
			this.adr.TabIndex = 7;
			//
			//ph_phone
			//
			this.ph_phone.Location = new System.Drawing.Point(85, 170);
			this.ph_phone.Name = "ph_phone";
			this.ph_phone.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.ph_phone.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ph_phone.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.ph_phone.Properties.Mask.BeepOnError = true;
			this.ph_phone.Properties.Mask.EditMask = "(999) 000-0000";
			this.ph_phone.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
			this.ph_phone.Properties.Mask.SaveLiteral = false;
			this.ph_phone.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.ph_phone.Size = new System.Drawing.Size(100, 20);
			this.ph_phone.TabIndex = 9;
			this.ph_phone.ToolTip = "This is the telephone number";
			//
			//tx_name
			//
			this.tx_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.tx_name.Location = new System.Drawing.Point(85, 70);
			this.tx_name.Name = "tx_name";
			this.tx_name.Properties.MaxLength = 50;
			this.tx_name.Size = new System.Drawing.Size(299, 20);
			this.tx_name.TabIndex = 5;
			//
			//LabelControl7
			//
			this.LabelControl7.Location = new System.Drawing.Point(12, 73);
			this.LabelControl7.Name = "LabelControl7";
			this.LabelControl7.Size = new System.Drawing.Size(61, 13);
			this.LabelControl7.TabIndex = 4;
			this.LabelControl7.Text = "Organization";
			//
			//AddWorkshopLocation
			//
			this.AcceptButton = this.Button_OK;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.Button_Cancel;
			this.ClientSize = new System.Drawing.Size(396, 229);
			this.Controls.Add(this.tx_name);
			this.Controls.Add(this.LabelControl7);
			this.Controls.Add(this.ph_phone);
			this.Controls.Add(this.adr);
			this.Controls.Add(this.me_directions);
			this.Controls.Add(this.LabelControl6);
			this.Controls.Add(this.LabelControl5);
			this.Controls.Add(this.LabelControl4);
			this.Controls.Add(this.LabelControl3);
			this.Controls.Add(this.sp_milage);
			this.Controls.Add(this.lk_type);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_OK);
			this.Controls.Add(this.tx_description);
			this.Controls.Add(this.LabelControl2);
			this.Name = "AddWorkshopLocation";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Add New Workshop Location";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.tx_description.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.lk_type.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.sp_milage.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.me_directions.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ph_phone.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.tx_name.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.TextEdit tx_description;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LookUpEdit lk_type;
		protected internal DevExpress.XtraEditors.SpinEdit sp_milage;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl5;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl6;
		protected internal DevExpress.XtraEditors.MemoExEdit me_directions;
		protected internal DebtPlus.Data.Controls.AddressParts adr;
		protected internal DebtPlus.Data.Controls.PhoneNumber ph_phone;
		protected internal DevExpress.XtraEditors.TextEdit tx_name;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl7;
	}
}
