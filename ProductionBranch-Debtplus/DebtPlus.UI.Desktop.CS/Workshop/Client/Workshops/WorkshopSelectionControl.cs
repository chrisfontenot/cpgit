#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
    internal partial class WorkshopSelectionControl : DevExpress.XtraEditors.XtraUserControl
    {
        public event EventHandler Completed;

        public event EventHandler Cancelled;

        public WorkshopSelectionControl() : base()
        {
            InitializeComponent();

            // Add the "Handles" clauses here
            Button_Cancel.Click += Button_Cancel_Click;
            Button_OK.Click += Button_OK_Click;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.MouseDown += GridView1_MouseDown;
        }

        /// <summary>
        /// Process the CANCEL button on the form
        /// </summary>
        protected void RaiseCancelled()
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        protected void Button_Cancel_Click(System.Object sender, System.EventArgs e)
        {
            RaiseCancelled();
        }

        /// <summary>
        /// Process the OK button on the form
        /// </summary>
        protected void RaiseCompleted()
        {
            if (Completed != null)
            {
                Completed(this, EventArgs.Empty);
            }
        }

        protected void Button_OK_Click(object sender, System.EventArgs e)
        {
            RaiseCompleted();
        }

        /// <summary>
        /// Save the ACH files
        /// </summary>
        protected internal bool Save()
        {
            return true;
        }

        protected internal void Process()
        {
            DataTable tbl = GlobalValues.ds.Tables["workshops"];
            if (tbl == null)
            {
                System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
                var _with1 = cmd;
                _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                _with1.CommandText = "SELECT w.workshop, w.workshop_type, w.start_time, w.workshop_location, w.seats_available, w.CounselorID, w.HUD_Grant, w.HousingFeeAmount, w.Guest, w.date_created, w.created_by, w.counselor, dbo.format_counselor_name(w.counselor) AS formatted_counselor, wl.name AS formatted_workshop_location, wt.description AS formatted_workshop_type FROM workshops w WITH (NOLOCK) LEFT OUTER JOIN workshop_types wt WITH (NOLOCK) ON w.workshop_type = wt.workshop_type LEFT OUTER JOIN workshop_locations wl WITH (NOLOCK) ON w.workshop_location = wl.workshop_location WHERE w.start_time BETWEEN dbo.date_only (dateadd(d, -30, getdate())) AND dbo.date_only(dateadd(d, 31, getdate()))";
                _with1.CommandType = CommandType.Text;

                System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                Cursor current_cursor = System.Windows.Forms.Cursor.Current;

                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    da.Fill(GlobalValues.ds, "workshops");
                    tbl = GlobalValues.ds.Tables["workshops"];
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshops");
                }
                finally
                {
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }

            var _with2 = GridControl1;
            _with2.DataSource = tbl.DefaultView;
            _with2.RefreshDataSource();
        }

        /// <summary>
        /// Process a change in the current trustRegister for the control
        /// </summary>
        protected void GridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = gv.GridControl;
            System.Int32 ControlRow = e.FocusedRowHandle;

            // Find the datarowview from the input tables.
            System.Data.DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
            }

            // Ask the user to do something with the selected trustRegister
            Int32 workshop = -1;
            if (drv != null)
            {
                if (drv["workshop"] != null && !object.ReferenceEquals(drv["workshop"], System.DBNull.Value))
                {
                    workshop = Convert.ToInt32(drv["workshop"]);
                }
            }
            Button_OK.Enabled = (workshop > 0);
        }

        /// <summary>
        /// Double Click on an item -- edit it
        /// </summary>

        protected void GridView1_DoubleClick(object sender, System.EventArgs e)
        {
            // Find the trustRegister targeted as the double-click item
            DevExpress.XtraGrid.Views.Grid.GridView gv = (DevExpress.XtraGrid.Views.Grid.GridView)sender;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo((ctl.PointToClient(System.Windows.Forms.Control.MousePosition)));
            System.Int32 ControlRow = hi.RowHandle;

            // Find the datarowview from the input tables.
            System.Data.DataRowView drv = null;
            if (ControlRow >= 0)
            {
                DataView vue = (DataView)ctl.DataSource;
                drv = (System.Data.DataRowView)gv.GetRow(ControlRow);
            }

            // If there is a trustRegister then select it
            var workshop = -1;
            if (drv != null)
            {
                if (drv["workshop"] != null && !object.ReferenceEquals(drv["workshop"], System.DBNull.Value))
                    workshop = Convert.ToInt32(drv["workshop"]);
            }

            // If there is a workshop then select it
            if (workshop > 0)
                Button_OK.PerformClick();
        }

        /// <summary>
        /// Handle a mouse click on the control
        /// </summary>
        protected void GridView1_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            DevExpress.XtraGrid.Views.Grid.GridView gv = GridView1;
            DevExpress.XtraGrid.GridControl ctl = (DevExpress.XtraGrid.GridControl)gv.GridControl;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = gv.CalcHitInfo(new Point(e.X, e.Y));

            // Remember the position for the popup menu handler.
            System.Int32 ControlRow = -1;
            if (hi.InRow)
            {
                ControlRow = hi.RowHandle;
            }

            var workshop = -1;
            if (ControlRow >= 0)
            {
                System.Data.DataRowView drv = (System.Data.DataRowView)GridView1.GetRow(ControlRow);
                if (drv != null)
                {
                    if (drv["workshop"] != null && !object.ReferenceEquals(drv["workshop"], System.DBNull.Value))
                        workshop = Convert.ToInt32(drv["workshop"]);
                }
                Button_OK.Enabled = (workshop > 0);
            }
        }
    }
}