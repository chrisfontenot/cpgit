#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client.Workshops
{
    public partial class AddWorkshopLocation
    {
        private System.Data.DataRowView drv = null;

        public AddWorkshopLocation(System.Data.DataRowView drv) : base()
        {
            this.InitializeComponent();
            this.drv = drv;
            this.Load += AddWorkshopLocation_Load;
            Button_OK.Click += Button_OK_Click;
        }

        private void AddWorkshopLocation_Load(System.Object sender, System.EventArgs e)
        {
            tx_description.DataBindings.Clear();
            tx_description.DataBindings.Add("EditValue", drv, "name");
            tx_description.EditValueChanged += tx_description_EditValueChanged;

            tx_name.DataBindings.Clear();
            tx_name.DataBindings.Add("EditValue", drv, "organization");
            tx_name.EditValueChanged += tx_description_EditValueChanged;

            DataTable tbl = WorkshopLocationTypesTable();
            lk_type.Properties.DataSource = tbl.DefaultView;
            lk_type.Properties.ValueMember = "organization_type";
            lk_type.Properties.DisplayMember = "name";

            lk_type.DataBindings.Clear();
            lk_type.DataBindings.Add(new Binding("EditValue", drv, "type"));
            lk_type.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["address1"], System.DBNull.Value))
            {
                drv["address1"] = string.Empty;
            }
            adr.Line1.DataBindings.Clear();
            adr.Line1.DataBindings.Add(new Binding("EditValue", drv, "address1"));
            adr.Line1.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["address2"], System.DBNull.Value))
            {
                drv["address2"] = string.Empty;
            }
            adr.Line2.DataBindings.Clear();
            adr.Line2.DataBindings.Add(new Binding("EditValue", drv, "address2"));
            adr.Line2.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["city"], System.DBNull.Value))
            {
                drv["city"] = string.Empty;
            }
            adr.City.DataBindings.Clear();
            adr.City.DataBindings.Add(new Binding("EditValue", drv, "city"));
            adr.City.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["postalcode"], System.DBNull.Value))
            {
                drv["postalcode"] = string.Empty;
            }
            adr.PostalCode.DataBindings.Clear();
            adr.PostalCode.DataBindings.Add(new Binding("EditValue", drv, "postalcode"));
            adr.PostalCode.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["state"], System.DBNull.Value))
            {
                drv["state"] = adr.States.EditValue;
            }
            adr.States.DataBindings.Clear();
            adr.States.DataBindings.Add(new Binding("EditValue", drv, "state"));
            adr.States.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["phone"], System.DBNull.Value))
            {
                drv["phone"] = ph_phone.EditValue;
            }
            ph_phone.DataBindings.Clear();
            ph_phone.DataBindings.Add(new Binding("EditValue", drv, "phone"));
            ph_phone.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["milage"], System.DBNull.Value))
            {
                drv["milage"] = sp_milage.EditValue;
            }
            sp_milage.DataBindings.Clear();
            sp_milage.DataBindings.Add(new Binding("EditValue", drv, "milage"));
            sp_milage.EditValueChanged += tx_description_EditValueChanged;

            if (drv.IsNew || object.ReferenceEquals(drv["directions"], System.DBNull.Value))
            {
                drv["directions"] = me_directions.EditValue;
            }
            me_directions.DataBindings.Clear();
            me_directions.DataBindings.Add(new Binding("EditValue", drv, "directions"));
            me_directions.EditValueChanged += tx_description_EditValueChanged;

            EnableOK();
        }

        private DataTable WorkshopLocationTypesTable()
        {
            DataTable tbl = GlobalValues.ds.Tables["workshop_organization_types"];

            if (tbl != null)
            {
                return tbl;
            }
            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "SELECT [organization_type],[name],[ActiveFlag],[date_created],[created_by] FROM workshop_organization_types";
                        cmd.CommandType = CommandType.Text;
                        using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(GlobalValues.ds, "workshop_organization_types");
                            tbl = GlobalValues.ds.Tables["workshop_organization_types"];
                            tbl.PrimaryKey = new DataColumn[] { tbl.Columns["organization_type"] };
                            var col = tbl.Columns["organization_type"];
                            col.AutoIncrement = true;
                            col.AutoIncrementSeed = -1;
                            col.AutoIncrementStep = -1;
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading workshop_organization_types table");
            }

            return tbl;
        }

        private void EnableOK()
        {
            if (tx_description.Text.Trim() == string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            if (lk_type.EditValue == null || object.ReferenceEquals(lk_type.EditValue, System.DBNull.Value))
            {
                Button_OK.Enabled = false;
                return;
            }

            if (tx_name.Text.Trim() == string.Empty)
            {
                Button_OK.Enabled = false;
                return;
            }

            System.Int32 Milage = 0;
            if (Int32.TryParse(sp_milage.Text, out Milage) && Milage >= 0)
            {
                Button_OK.Enabled = true;
                return;
            }

            Button_OK.Enabled = false;
        }

        private void tx_description_EditValueChanged(object sender, System.EventArgs e)
        {
            EnableOK();
        }

        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            System.Int32 workshop_location = -1;
            try
            {
                using (var cm = new DebtPlus.UI.Common.CursorManager())
                {
                    using (System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                    {
                        cn.Open();
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = cn;
                            cmd.CommandText = "INSERT INTO workshop_locations (name,organization,type,address1,address2,city,state,postalcode,phone,directions,milage) values (@name,@organization,@type,@address1,@address2,@city,@state,@postalcode,@phone,@directions,@milage); SELECT scope_identity() as workshop_location;";
                            cmd.Parameters.Add("@name", SqlDbType.VarChar, 80).Value = drv["name"];
                            cmd.Parameters.Add("@organization", SqlDbType.VarChar, 80).Value = drv["organization"];
                            cmd.Parameters.Add("@type", SqlDbType.Int).Value = drv["type"];
                            cmd.Parameters.Add("@address1", SqlDbType.VarChar, 80).Value = drv["address1"];
                            cmd.Parameters.Add("@address2", SqlDbType.VarChar, 80).Value = drv["address2"];
                            cmd.Parameters.Add("@city", SqlDbType.VarChar, 80).Value = drv["city"];
                            cmd.Parameters.Add("@state", SqlDbType.Int).Value = drv["state"];
                            cmd.Parameters.Add("@postalcode", SqlDbType.VarChar, 80).Value = drv["postalcode"];
                            cmd.Parameters.Add("@phone", SqlDbType.VarChar, 50).Value = drv["phone"];
                            cmd.Parameters.Add("@directions", SqlDbType.VarChar, 1024).Value = drv["directions"];
                            cmd.Parameters.Add("@milage", SqlDbType.Float).Value = drv["milage"];

                            object objAnswer = cmd.ExecuteScalar();
                            if (objAnswer != null && !object.ReferenceEquals(objAnswer, System.DBNull.Value))
                            {
                                workshop_location = Convert.ToInt32(objAnswer);
                            }

                            if (workshop_location < 0)
                            {
                                throw new ApplicationException("No workshop_location for new record");
                            }
                            drv["workshop_location"] = workshop_location;
                            DialogResult = System.Windows.Forms.DialogResult.OK;
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error adding workshop_location");
            }
        }
    }
}