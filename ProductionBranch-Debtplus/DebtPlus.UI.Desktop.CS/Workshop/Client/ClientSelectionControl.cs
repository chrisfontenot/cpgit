using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data.SqlClient;
using DebtPlus.Interfaces.Client;
using DebtPlus.UI.Client.Widgets.Search;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Workshop.Client
{
    public partial class ClientSelectionControl
    {
        public event EventHandler Completed;

        public event EventHandler Cancelled;

        // Table for the updates of the clients
        private System.Data.DataTable _EditTable = null;

        private DataTable EditTable
        {
            get { return _EditTable; }
            set
            {
                if (_EditTable != null)
                {
                    _EditTable.ColumnChanged -= EditTable_ColumnChanged;
                    _EditTable.ColumnChanging -= EditTable_ColumnChanging;
                    _EditTable.TableNewRow -= EditTable_TableNewRow;
                }
                _EditTable = value;
                if (_EditTable != null)
                {
                    _EditTable.ColumnChanged += EditTable_ColumnChanged;
                    _EditTable.ColumnChanging += EditTable_ColumnChanging;
                    _EditTable.TableNewRow += EditTable_TableNewRow;
                }
            }
        }

        private System.Data.DataColumn ClientColumn = null;

        public ClientSelectionControl() : base()
        {
            InitializeComponent();

            Button_Cancel.Click += Button_Cancel_Click;
            Button_OK.Click += Button_OK_Click;
            RepositoryItemButtonEdit1.ButtonClick += RepositoryItemButtonEdit1_Click;
        }

        /// <summary>
        /// Process the CANCEL button on the form
        /// </summary>
        protected void RaiseCancelled()
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        protected void Button_Cancel_Click(System.Object sender, System.EventArgs e)
        {
            RaiseCancelled();
        }

        /// <summary>
        /// Process the OK button on the form
        /// </summary>
        protected void RaiseCompleted()
        {
            if (Completed != null)
            {
                Completed(this, EventArgs.Empty);
            }
        }

        protected void Button_OK_Click(object sender, System.EventArgs e)
        {
            RaiseCompleted();
        }

        protected internal void Process()
        {
            System.Data.SqlClient.SqlCommand cmd = new SqlCommand();
            var _with1 = cmd;
            _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            _with1.CommandText = "select ca.client_appointment, ca.client, v.name, v.active_status, dbo.address_block(v.addr1, v.addr2, v.addr3, default, default) as address, ca.workshop_people, ca.status, ca.result, ca.housing, ca.post_purchase, ca.credit, ca.callback_ph, ca.confirmation_status, ca.date_confirmed, ca.date_updated, ca.date_created, ca.created_by from client_appointments ca left outer join view_client_address v on ca.client = v.client where ca.office is null and ca.workshop = @workshop ORDER BY ca.client";
            _with1.CommandType = CommandType.Text;
            _with1.Parameters.Add("@workshop", SqlDbType.Int).Value = GlobalValues.workshop;

            System.Data.DataSet ds = new System.Data.DataSet("ds");
            System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
            da.Fill(ds, "client_appointments");

            EditTable = ds.Tables["client_appointments"];
            var _with2 = EditTable;
            var _with3 = _with2.Columns["client_appointment"];
            _with3.AutoIncrement = true;
            _with3.AutoIncrementSeed = -1;
            _with3.AutoIncrementStep = -1;

            ClientColumn = _with2.Columns["client"];
            ClientColumn.DefaultValue = 0;

            _with2.Columns["date_created"].DefaultValue = DateTime.Now.Date;
            _with2.Columns["created_by"].DefaultValue = "Me";
            _with2.Columns["workshop_people"].DefaultValue = 1;
            _with2.Columns["name"].DefaultValue = string.Empty;
            _with2.Columns["housing"].DefaultValue = false;
            _with2.Columns["credit"].DefaultValue = false;
            _with2.Columns["post_purchase"].DefaultValue = false;
            _with2.Columns["confirmation_status"].DefaultValue = 0;
            _with2.Columns["status"].DefaultValue = "K";

            _with2.PrimaryKey = new DataColumn[] { _with2.Columns["client_appointment"] };
            _with2.Constraints.Add("UniqueClient", _with2.Columns["client"], false);

            System.Data.DataView vue = EditTable.DefaultView;
            var _with4 = GridControl1;
            _with4.DataSource = vue;
            _with4.RefreshDataSource();

            System.Data.DataTable tbl = new System.Data.DataTable("appointment_status_types");
            var _with5 = tbl;
            _with5.Columns.Add(new System.Data.DataColumn("status", typeof(string)));
            _with5.Columns.Add(new System.Data.DataColumn("description", typeof(string)));
            var _with6 = _with5.Rows;
            _with6.Add(new object[] {
                "M",
                "No Show"
            });
            _with6.Add(new object[] {
                "K",
                "Show"
            });
            _with5.PrimaryKey = new DataColumn[] { _with5.Columns[0] };
            ds.Tables.Add(tbl);

            var _with7 = RepositoryItemLookUpEdit1;
            _with7.DataSource = tbl;
            _with7.DisplayMember = "description";
            _with7.ValueMember = "status";
            _with7.NullText = "Show";

            var _with8 = RepositoryItemButtonEdit1;
            _with8.NullText = System.String.Empty;
            _with8.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;

            var _with9 = _with8.DisplayFormat;
            _with9.FormatString = "0000000";
            _with9.FormatType = DevExpress.Utils.FormatType.Custom;
            _with9.Format = new global::DebtPlus.Utils.Format.Client.CustomFormatter();

            var _with10 = _with8.EditFormat;
            _with10.FormatString = "f0";
            _with10.FormatType = DevExpress.Utils.FormatType.Numeric;

            // Numbers only, please.
            var _with11 = _with8.Mask;
            _with11.BeepOnError = true;
            _with11.EditMask = "f0";
            _with11.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;

            _with8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;

            var _with12 = _with8.Buttons[0];
            _with12.Tag = "search";
            _with12.ToolTip = "Click here to search for a client ID";

            // Load the search bitmap and attach it to the button
            System.Reflection.Assembly _assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.Stream ios = _assembly.GetManifestResourceStream(_assembly.GetName().Name + ".Find.bmp");
            _with12.Image = new System.Drawing.Bitmap(ios);
            ios.Close();

            _with12.Kind = DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph;
        }

        private void RepositoryItemButtonEdit1_Click(object sender, EventArgs e)
        {
            IClientSearch searchClass = new ClientSearchClass();
            if (searchClass != null)
            {
                var _with13 = searchClass;
                if (_with13.ShowDialog() == DialogResult.OK)
                {
                    GridView1.SetFocusedRowCellValue(GridColumn_client, _with13.ClientId);
                }
            }
        }

        private void EditTable_ColumnChanged(object sender, DataColumnChangeEventArgs e)
        {
            System.Data.DataTable tbl = (DataTable)sender;

            var _with14 = tbl;
            if (object.ReferenceEquals(e.Column, ClientColumn))
            {
                bool ValidClient = false;
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                System.Data.SqlClient.SqlDataReader rd = null;
                System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;

                if (global::DebtPlus.Utils.Nulls.DInt(e.ProposedValue) > 0)
                {
                    try
                    {
                        cn.Open();
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with15 = cmd;
                            _with15.Connection = cn;
                            _with15.CommandText = "SELECT c.active_status, dbo.format_normal_name(p.prefix, p.first, p.middle, p.last, p.suffix) as name, dbo.address_block(c.address1, c.address2, dbo.format_city_state_zip(c.city, c.state, c.postalcode), default, default) as address FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p ON p.client = c.client and 1 = p.relation WHERE c.client = @client";
                            _with15.CommandType = CommandType.Text;
                            _with15.Parameters.Add("@client", SqlDbType.Int).Value = e.ProposedValue;
                            rd = _with15.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);
                        }

                        if (rd.Read())
                        {
                            e.Row["active_status"] = rd.GetValue(0);
                            e.Row["name"] = rd.GetValue(1);
                            e.Row["address"] = rd.GetValue(2);
                            ValidClient = true;
                        }
                    }
                    finally
                    {
                        if (rd != null)
                            rd.Dispose();
                        if (cn != null)
                            cn.Dispose();
                        System.Windows.Forms.Cursor.Current = current_cursor;
                    }
                }

                if (!ValidClient)
                {
                    throw new ArgumentException("Invalid Client");
                }
            }
        }

        private void EditTable_ColumnChanging(object sender, System.Data.DataColumnChangeEventArgs e)
        {
        }

        private void EditTable_TableNewRow(object sender, System.Data.DataTableNewRowEventArgs e)
        {
            //Dim row As System.Data.DataRow = e.Row
            //With row
            //.Item("status") = "K"
            //.Item("workshop_people") = 1
            //.Item("client") = 0
            //End With
        }

        protected internal bool Save()
        {
            bool answer = false;
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlTransaction txn = null;
            System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;

            // Ignore the save if there is no workshop
            if (GlobalValues.workshop <= 0 || EditTable == null)
                return true;

            try
            {
                cn.Open();
                txn = cn.BeginTransaction();

                // Add the rows to the database
                System.Data.DataView AddView = new System.Data.DataView(EditTable, string.Empty, string.Empty, DataViewRowState.Added);
                if (AddView.Count > 0)
                {
                    AddRows(AddView, cn, txn);
                }

                // Update the rows that have been changed
                System.Data.DataView ChangeView = new System.Data.DataView(EditTable, string.Empty, string.Empty, DataViewRowState.ModifiedCurrent);
                if (ChangeView.Count > 0)
                {
                    ChangeRows(ChangeView, cn, txn);
                }

                // Delete the rows that have been deleted
                System.Data.DataView DeleteView = new System.Data.DataView(EditTable, string.Empty, string.Empty, DataViewRowState.Deleted);
                if (DeleteView.Count > 0)
                {
                    DeleteRows(DeleteView, cn, txn);
                }

                // Commit the changes
                txn.Commit();
                txn = null;
                answer = true;
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating client appointments table");
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch
                    {
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();

                System.Windows.Forms.Cursor.Current = current_cursor;
            }

            return answer;
        }

        private void AddRows(System.Data.DataView vue, System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            foreach (System.Data.DataRowView drv in vue)
            {
                if (drv["status"] == null || object.ReferenceEquals(drv["status"], System.DBNull.Value))
                    drv["status"] = "K";

                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with16 = cmd;
                    _with16.Connection = cn;
                    _with16.Transaction = txn;

                    // Add the workshop appointment to the client_appointments
                    _with16.CommandText = "SET NOCOUNT ON; INSERT INTO client_appointments(client, workshop, workshop_people, status, start_time) SELECT @client, workshop, @workshop_people, @status, start_time FROM workshops WHERE workshop = @workshop; SELECT scope_identity() as client_appointment; SET NOCOUNT OFF;";
                    _with16.Parameters.Add("@client", SqlDbType.Int).Value = drv["ClientId"];
                    _with16.Parameters.Add("@workshop_people", SqlDbType.Int).Value = drv["workshop_people"];
                    _with16.Parameters.Add("@status", SqlDbType.VarChar, 10).Value = drv["status"];
                    _with16.Parameters.Add("@workshop", SqlDbType.Int).Value = GlobalValues.workshop;

                    // Save the workshop appointment
                    drv["client_appointment"] = _with16.ExecuteScalar();
                }

                // Change the client
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with17 = cmd;
                    _with17.Connection = cn;
                    _with17.Transaction = txn;
                    _with17.CommandText = "UPDATE update_clients SET active_status = 'WKS' WHERE client = @client AND active_status = 'CRE';";
                    _with17.Parameters.Add("@client", SqlDbType.Int).Value = drv["client"];
                    _with17.ExecuteNonQuery();
                }
            }
        }

        private void ChangeRows(System.Data.DataView vue, System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            foreach (System.Data.DataRowView drv in vue)
            {
                var _with18 = new SqlCommand();
                _with18.Connection = cn;
                _with18.Transaction = txn;

                // Add the workshop appointment to the client_appointments
                _with18.CommandText = "UPDATE client_appointments SET client = @client, workshop_people = @workshop_people, status = @status WHERE client_appointment = @client_appointment;";
                _with18.Parameters.Add("@client", SqlDbType.Int).Value = drv["client"];
                _with18.Parameters.Add("@workshop_people", SqlDbType.Int).Value = drv["workshop_people"];
                _with18.Parameters.Add("@status", SqlDbType.VarChar, 10).Value = drv["status"];
                _with18.Parameters.Add("@client_appointment", SqlDbType.Int).Value = drv["client_appointment"];

                _with18.ExecuteNonQuery();
            }
        }

        private void DeleteRows(System.Data.DataView vue, System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            foreach (System.Data.DataRowView drv in vue)
            {
                var _with19 = new SqlCommand();
                _with19.Connection = cn;
                _with19.Transaction = txn;

                // Add the workshop appointment to the client_appointments
                _with19.CommandText = "DELETE FROM client_appointments WHERE client_appointment = @client_appointment;";
                _with19.Parameters.Add("@client_appointment", SqlDbType.Int).Value = drv["client_appointment"];

                _with19.ExecuteNonQuery();
            }
        }
    }
}