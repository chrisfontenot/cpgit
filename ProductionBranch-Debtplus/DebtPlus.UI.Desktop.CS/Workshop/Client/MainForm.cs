#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Workshop.Client
{
    internal partial class MainForm
    {
        internal ArgParser ap = null;

        public MainForm(ArgParser ap) : base()
        {
            this.InitializeComponent();
            this.ap = ap;

            this.FormClosing += MainForm_FormClosing;
            this.Load += MainForm_Load;
            NewWorkshopSelectionControl1.Cancelled += NewWorkshopSelectionControl1_Cancelled;
            NewWorkshopSelectionControl1.Completed += NewWorkshopSelectionControl1_Completed;
            ClientSelectionControl1.Cancelled += ClientSelectionControl1_Cancelled;
            ClientSelectionControl1.Completed += ClientSelectionControl1_Completed;
        }

        private void MainForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (ClientSelectionControl1.Visible)
            {
                if (!ClientSelectionControl1.Save())
                {
                    e.Cancel = true;
                }
            }
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            var _with1 = ClientSelectionControl1;
            _with1.Visible = false;

            var _with2 = NewWorkshopSelectionControl1;
            _with2.Visible = true;
            _with2.Process();
        }

        private void NewWorkshopSelectionControl1_Cancelled(object sender, System.EventArgs e)
        {
            this.Close();
        }

        private void NewWorkshopSelectionControl1_Completed(object sender, System.EventArgs e)
        {
            var _with3 = NewWorkshopSelectionControl1;
            _with3.Visible = false;

            var _with4 = ClientSelectionControl1;
            _with4.Visible = true;
            _with4.Process();
        }

        private void ClientSelectionControl1_Cancelled(object sender, System.EventArgs e)
        {
            var _with5 = ClientSelectionControl1;
            _with5.Visible = false;

            GlobalValues.workshop = -1;
            var _with6 = NewWorkshopSelectionControl1;
            _with6.Visible = true;
            _with6.Process();
        }

        private void ClientSelectionControl1_Completed(object sender, System.EventArgs e)
        {
            var _with7 = ClientSelectionControl1;
            if (_with7.Save())
            {
                _with7.Visible = false;

                GlobalValues.workshop = -1;
                var _with8 = NewWorkshopSelectionControl1;
                _with8.Visible = true;
                _with8.Process();
            }
        }
    }
}