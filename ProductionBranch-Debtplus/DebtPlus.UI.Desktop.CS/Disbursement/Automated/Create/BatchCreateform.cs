#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Create
{
    public partial class BatchCreateForm : DebtPlus.Data.Forms.DebtPlusForm
    {
        private ArgParser ap;
        private DevExpress.Utils.WaitDialogForm dlg = null;
        public delegate void CompletedCallbackDelegate(System.Int32 BatchID);

        public BatchCreateForm() : base()
        {
            InitializeComponent();
        }

        internal BatchCreateForm(ArgParser ap) : this()
        {
            this.ap = ap;
			RegisterHandlers();
        }

		private void RegisterHandlers()
		{
            Load += BatchCreateForm_Load;
            Text_Label.KeyPress += Text_Label_KeyPress;
            Text_Label.Enter += Text_Label_Enter;
            Button_Create.Click += btn_ok_Click;
            Button_Cancel.Click += btn_cancel_Click;
            Button_cycle.Click += Button_cycle_Click;
            Button_Region.Click += Button_cycle_Click;
		}
		
		private void UnRegisterHandlers()
		{
            Load -= BatchCreateForm_Load;
            Text_Label.KeyPress -= Text_Label_KeyPress;
            Text_Label.Enter -= Text_Label_Enter;
            Button_Create.Click -= btn_ok_Click;
            Button_Cancel.Click -= btn_cancel_Click;
            Button_cycle.Click -= Button_cycle_Click;
            Button_Region.Click -= Button_cycle_Click;
		}
		
#region " Windows Form Designer generated code "

        //Form overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer
        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.CheckedListBox_Cycle = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.Button_cycle = new DevExpress.XtraEditors.SimpleButton();
            this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.CheckedListBox_Region = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.Button_Region = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Create = new DevExpress.XtraEditors.SimpleButton();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Label1 = new DevExpress.XtraEditors.LabelControl();
            this.Text_Label = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
            this.GroupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBox_Cycle).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
            this.GroupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBox_Region).BeginInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Label.Properties).BeginInit();
            this.SuspendLayout();
            //
            //GroupControl1
            //
            this.GroupControl1.Controls.Add(this.CheckedListBox_Cycle);
            this.GroupControl1.Controls.Add(this.Button_cycle);
            this.GroupControl1.Location = new System.Drawing.Point(8, 40);
            this.GroupControl1.Name = "GroupControl1";
            this.GroupControl1.Size = new System.Drawing.Size(160, 200);
            this.GroupControl1.TabIndex = 2;
            this.GroupControl1.Text = " Disbursement Cycle(s) ";
            //
            //CheckedListBox_Cycle
            //
            this.CheckedListBox_Cycle.CheckOnClick = true;
            this.CheckedListBox_Cycle.ItemHeight = 16;
            this.CheckedListBox_Cycle.Items.AddRange(new DevExpress.XtraEditors.Controls.CheckedListBoxItem[] {
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("1"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("2"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("3"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("4"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("5"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("6"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("7"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("8"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("9"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("10"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("11"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("12"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("13"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("14"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("15"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("16"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("17"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("18"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("19"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("20"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("21"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("22"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("23"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("24"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("25"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("26"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("27"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("28"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("29"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("30"),
                new DevExpress.XtraEditors.Controls.CheckedListBoxItem("31")
            });
            this.CheckedListBox_Cycle.Location = new System.Drawing.Point(8, 16);
            this.CheckedListBox_Cycle.Name = "CheckedListBox_Cycle";
            this.CheckedListBox_Cycle.Size = new System.Drawing.Size(144, 144);
            this.CheckedListBox_Cycle.TabIndex = 0;
            this.CheckedListBox_Cycle.ToolTip = "Choose the client's disbursement cycle for those clients that you wish to disburs" + "e in this batch";
            this.CheckedListBox_Cycle.ToolTipController = this.ToolTipController1;
            //
            //Button_cycle
            //
            this.Button_cycle.Location = new System.Drawing.Point(48, 168);
            this.Button_cycle.Name = "Button_cycle";
            this.Button_cycle.Size = new System.Drawing.Size(75, 23);
            this.Button_cycle.TabIndex = 1;
            this.Button_cycle.Tag = "1";
            this.Button_cycle.Text = "Select &All";
            this.Button_cycle.ToolTip = "Click here to select all of the items in this list";
            this.Button_cycle.ToolTipController = this.ToolTipController1;
            this.Button_cycle.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            //
            //GroupControl2
            //
            this.GroupControl2.Controls.Add(this.CheckedListBox_Region);
            this.GroupControl2.Controls.Add(this.Button_Region);
            this.GroupControl2.Location = new System.Drawing.Point(192, 40);
            this.GroupControl2.Name = "GroupControl2";
            this.GroupControl2.Size = new System.Drawing.Size(160, 200);
            this.GroupControl2.TabIndex = 3;
            this.GroupControl2.Text = " Region(s) ";
            //
            //CheckedListBox_Region
            //
            this.CheckedListBox_Region.CheckOnClick = true;
            this.CheckedListBox_Region.ItemHeight = 16;
            this.CheckedListBox_Region.Location = new System.Drawing.Point(8, 16);
            this.CheckedListBox_Region.Name = "CheckedListBox_Region";
            this.CheckedListBox_Region.Size = new System.Drawing.Size(144, 144);
            this.CheckedListBox_Region.TabIndex = 0;
            this.CheckedListBox_Region.ToolTip = "Choose the region or regions that you wish to disburse in this batch";
            this.CheckedListBox_Region.ToolTipController = this.ToolTipController1;
            //
            //Button_Region
            //
            this.Button_Region.Location = new System.Drawing.Point(48, 168);
            this.Button_Region.Name = "Button_Region";
            this.Button_Region.Size = new System.Drawing.Size(75, 23);
            this.Button_Region.TabIndex = 1;
            this.Button_Region.Tag = "1";
            this.Button_Region.Text = "Select &All";
            this.Button_Region.ToolTip = "Click here to select all of the items in this list";
            this.Button_Region.ToolTipController = this.ToolTipController1;
            this.Button_Region.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            //
            //Button_Create
            //
            this.Button_Create.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Button_Create.Location = new System.Drawing.Point(56, 248);
            this.Button_Create.Name = "Button_Create";
            this.Button_Create.Size = new System.Drawing.Size(75, 23);
            this.Button_Create.TabIndex = 4;
            this.Button_Create.Text = "C&reate";
            this.Button_Create.ToolTip = "Click here to create the batch";
            this.Button_Create.ToolTipController = this.ToolTipController1;
            this.Button_Create.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            //
            //Button_Cancel
            //
            this.Button_Cancel.Location = new System.Drawing.Point(240, 248);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.TabIndex = 5;
            this.Button_Cancel.Text = "&Cancel";
            this.Button_Cancel.ToolTip = "Cancel this form and return to the previous condition";
            this.Button_Cancel.ToolTipController = this.ToolTipController1;
            this.Button_Cancel.ToolTipIconType = DevExpress.Utils.ToolTipIconType.Hand;
            //
            //Label1
            //
            this.Label1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.Label1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Label1.Location = new System.Drawing.Point(16, 8);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(29, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Label:";
            this.Label1.ToolTipController = this.ToolTipController1;
            //
            //Text_Label
            //
            this.Text_Label.Location = new System.Drawing.Point(80, 8);
            this.Text_Label.Name = "Text_Label";
            this.Text_Label.Properties.NullValuePrompt = "... The optional label for the batch goes here ...";
            this.Text_Label.Size = new System.Drawing.Size(272, 20);
            this.Text_Label.TabIndex = 1;
            this.Text_Label.ToolTipController = this.ToolTipController1;
            //
            //BatchCreateForm
            //
            this.AcceptButton = this.Button_Create;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
            this.CancelButton = this.Button_Create;
            this.ClientSize = new System.Drawing.Size(368, 286);
            this.Controls.Add(this.Text_Label);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.Button_Cancel);
            this.Controls.Add(this.Button_Create);
            this.Controls.Add(this.GroupControl2);
            this.Controls.Add(this.GroupControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BatchCreateForm";
            this.Text = "Create Auto Disbursement Batch";
            ((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
            this.GroupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBox_Cycle).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
            this.GroupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)this.CheckedListBox_Region).EndInit();
            ((System.ComponentModel.ISupportInitialize)this.Text_Label.Properties).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
        }
		
        private DevExpress.XtraEditors.GroupControl GroupControl1;
        private DevExpress.XtraEditors.GroupControl GroupControl2;
        private DevExpress.XtraEditors.LabelControl Label1;
        private DevExpress.XtraEditors.CheckedListBoxControl CheckedListBox_Cycle;
        private DevExpress.XtraEditors.SimpleButton Button_cycle;
        private DevExpress.XtraEditors.CheckedListBoxControl CheckedListBox_Region;
        private DevExpress.XtraEditors.SimpleButton Button_Region;
        private DevExpress.XtraEditors.SimpleButton Button_Create;
        private DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraEditors.TextEdit Text_Label;
		
#endregion " Windows Form Designer generated code "

        private string CycleString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool SelectedAll = true;

            // Find the list of checked cycle dates. If all are selected then treat it as "none"
            // and this will process all of the dates
            for (System.Int32 Index = 0; Index <= CheckedListBox_Cycle.ItemCount - 1; Index++)
            {
                if (CheckedListBox_Cycle.Items[Index].CheckState == CheckState.Checked)
                {
                    sb.AppendFormat(",{0}", CheckedListBox_Cycle.Items[Index].Value);
                }
                else
                {
                    SelectedAll = false;       // Since we found one that was not selected then not all are selected
                }
            }

            // Return the empty string if all are selected.
            if (SelectedAll)
            {
                return System.String.Empty;
            }
            else
            {
                if (sb.Length > 0)
				{
                    sb.Remove(0, 1);
				}
                return sb.ToString();
            }
        }

        private string RegionString()
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool SelectedAll = true;

            // Find the list of checked cycle dates. If all are selected then treat it as "none"
            // and this will process all of the dates
            for (System.Int32 Index = 0; Index <= CheckedListBox_Region.ItemCount - 1; Index++)
            {
                if (CheckedListBox_Region.Items[Index].CheckState == CheckState.Checked)
                {
                    sb.AppendFormat(",{0:f0}", Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)CheckedListBox_Region.Items[Index].Value).value));
                }
                else
                {
                    SelectedAll = false;  // Since we found one that was not selected then not all are selected
                }
            }

            // Return the empty string if all are selected.
            if (SelectedAll)
            {
                return System.String.Empty;
            }
            else
            {
                if (sb.Length > 0)
				{
                    sb.Remove(0, 1);
				}
                return sb.ToString();
            }
        }

        private void Button_cycle_Click(object sender, System.EventArgs e)
        {
            // Translate the button to the associated control
            var btn = (DevExpress.XtraEditors.SimpleButton) sender;
            DevExpress.XtraEditors.CheckedListBoxControl ctl = object.ReferenceEquals(sender, Button_cycle) ? CheckedListBox_Cycle : CheckedListBox_Region;

            // Process the check status for the button
            System.Windows.Forms.CheckState CheckedState = CheckState.Checked;
            if (Convert.ToInt32(btn.Tag) == 0)
			{
                CheckedState = CheckState.Unchecked;
			}
			
            // Select all of the items in the list
            for (System.Int32 ItemCount = 0; ItemCount <= ctl.ItemCount - 1; ItemCount++)
            {
                ctl.Items[ItemCount].CheckState = CheckedState;
            }

            // Change the legend to match the tag
            if (CheckedState == CheckState.Checked)
            {
                btn.Tag = 0;
                btn.Text = "&Clear All";
            }
            else
            {
                btn.Tag = 1;
                btn.Text = "&Set All";
            }
        }

        private void load_lst_regions()
        {
            CheckedListBox_Region.Items.Clear();

            try
            {
				using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
				{
					cn.Open();
					using (var cmd = new SqlCommand())
					{
						cmd.Connection = cn;
						cmd.CommandText = "SELECT isnull(region,0) as region,isnull(description,'') as description FROM regions WITH (NOLOCK) ORDER BY description";
						using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult))
						{
							while (rd.Read())
							{
								CheckedListBox_Region.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(rd["description"]), Convert.ToInt32(rd["region"])), CheckState.Unchecked, true);
							}
						}
					}
				}
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Loading the region list", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BatchCreateForm_Load(System.Object sender, System.EventArgs e)
        {
            load_lst_regions();
        }

        private void Text_Label_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (Text_Label.Text == System.String.Empty && e.KeyChar == ' ')
			{
                e.Handled = true;
			}
        }

        private void Text_Label_Enter(object sender, System.EventArgs e)
        {
            Text_Label.SelectAll();
        }

        private void btn_ok_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            Button_Create.Enabled = false;

            ThreadClass cls = new ThreadClass(new CompletedCallbackDelegate(Completed), CycleString(), RegionString(), Text_Label.Text.Trim());
            System.Threading.Thread thrd = new System.Threading.Thread(cls.DoThread);
            thrd.IsBackground = true;
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Name = "CreateBatch";
            thrd.Start();

            // Put put the waiting dialog
            dlg = new DevExpress.Utils.WaitDialogForm("Creating the disbursement batch");
            dlg.Show();
        }

        private void Completed(System.Int32 BatchID)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new CompletedCallbackDelegate(Completed), new object[] { BatchID });
            }
            else
            {
                if (dlg != null)
                {
                    dlg.Close();
                    dlg.Dispose();
                    dlg = null;
                }

                DebtPlus.Data.Forms.MessageBox.Show(System.String.Format("The disbursement batch {0:f0} was created successfully.", BatchID), "Operation Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Close();
            }
        }

        private void btn_cancel_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            Close();
        }

        private class ThreadClass
        {
            private CompletedCallbackDelegate CompletedDelegate;
            private string CycleString;
            private string RegionString;

            private string LabelString;

            public ThreadClass(CompletedCallbackDelegate CompletedDelegate, string CycleString, string RegionString, string LabelString)
            {
                this.CompletedDelegate = CompletedDelegate;
                this.CycleString = CycleString;
                this.RegionString = RegionString;
                this.LabelString = LabelString;
            }

            public void DoThread()
            {
                try
                {
					using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
					{
						cn.Open();
						using (var cmd = new SqlCommand())
						{
							cmd.Connection = cn;
							cmd.CommandTimeout = System.Math.Max(1200, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
							cmd.CommandText = "xpr_disbursement_create";
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
							cmd.Parameters.Add("@disbursement_date", SqlDbType.VarChar, 512).Value = CycleString;
							cmd.Parameters.Add("@region", SqlDbType.VarChar, 512).Value = RegionString;
							cmd.Parameters.Add("@note", SqlDbType.VarChar, 50).Value = LabelString;

							cmd.ExecuteNonQuery();
							CompletedDelegate.Invoke(Convert.ToInt32(cmd.Parameters[0].Value));
						}
					}
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error Loading the region list", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}