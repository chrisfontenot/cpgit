namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Create
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] { })
        {
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}