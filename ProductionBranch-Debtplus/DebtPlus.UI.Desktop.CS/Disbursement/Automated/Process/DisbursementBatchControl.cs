#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using DebtPlus.UI.FormLib.Disbursement;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
    internal partial class DisbursementBatchControl : NewDisbursementRegisterControl
    {
        protected Disbursement.Common.IDisbursementContext context
        {
            get { return ((Disbursement.Common.ISupportContext)ParentForm).Context; }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DisbursementBatchControl() : base()
        {
            InitializeComponent();
        }

        #region " Windows Form Designer generated code "

        //UserControl overrides dispose to clean up the component list.
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        //Required by the Windows Form Designer

        private System.ComponentModel.IContainer components = null;

        //NOTE: The following procedure is required by the Windows Form Designer
        //It can be modified using the Windows Form Designer.
        //Do not modify it using the code editor.
        internal DevExpress.XtraEditors.LabelControl LabelControl1;

        [System.Diagnostics.DebuggerStepThrough]
        private void InitializeComponent()
        {
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
            this.SuspendLayout();
            //
            //Button_New
            //
            this.Button_New.Location = new System.Drawing.Point(432, 64);
            this.Button_New.Name = "Button_New";
            //
            //GridControl1
            //
            //
            //GridControl1.EmbeddedNavigator
            //
            this.GridControl1.EmbeddedNavigator.Name = "";
            this.GridControl1.Location = new System.Drawing.Point(8, 32);
            this.GridControl1.Name = "GridControl1";
            this.GridControl1.Size = new System.Drawing.Size(416, 168);
            //
            //Button_Select
            //
            this.Button_Select.Location = new System.Drawing.Point(432, 32);
            this.Button_Select.Name = "Button_Select";
            //
            //Button_Cancel
            //
            this.Button_Cancel.Location = new System.Drawing.Point(432, 96);
            this.Button_Cancel.Name = "Button_Cancel";
            //
            //LabelControl1
            //
            this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
            this.LabelControl1.Appearance.Options.UseTextOptions = true;
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.LabelControl1.Location = new System.Drawing.Point(8, 8);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(496, 14);
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "Please select the batch that you wish to update or choose New to create a new bat" + "ch.";
            //
            //DisbursementBatchControl
            //
            this.Controls.Add(this.LabelControl1);
            this.Name = "DisbursementBatchControl";
            this.Size = new System.Drawing.Size(512, 208);
            this.Controls.SetChildIndex(this.Button_New, 0);
            this.Controls.SetChildIndex(this.LabelControl1, 0);
            this.Controls.SetChildIndex(this.GridControl1, 0);
            this.Controls.SetChildIndex(this.Button_Select, 0);
            this.Controls.SetChildIndex(this.Button_Cancel, 0);
            ((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
            this.ResumeLayout(false);
        }

        #endregion " Windows Form Designer generated code "
    }
}