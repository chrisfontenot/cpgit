using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
	partial class Main_Form : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.ProcessControl1 = new global::DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process.ProcessControl();
			this.DisbursementBatchControl1 = new global::DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process.DisbursementBatchControl();
			this.ClientSelection1 = new global::DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process.ClientSelection();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//DefaultLookAndFeel1
			//
			this.DefaultLookAndFeel1.LookAndFeel.SkinName = "McSkin";
			//
			//ProcessControl1
			//
			this.ProcessControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ProcessControl1.Location = new System.Drawing.Point(0, 0);
			this.ProcessControl1.Name = "ProcessControl1";
			this.ProcessControl1.Size = new System.Drawing.Size(472, 430);
			this.ProcessControl1.TabIndex = 3;
			//
			//DisbursementBatchControl1
			//
			this.DisbursementBatchControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.DisbursementBatchControl1.Location = new System.Drawing.Point(0, 0);
			this.DisbursementBatchControl1.Name = "DisbursementBatchControl1";
			this.DisbursementBatchControl1.Size = new System.Drawing.Size(472, 430);
			this.DisbursementBatchControl1.TabIndex = 4;
			//
			//ClientSelection1
			//
			this.ClientSelection1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ClientSelection1.Location = new System.Drawing.Point(0, 0);
			this.ClientSelection1.Name = "ClientSelection1";
			this.ClientSelection1.Size = new System.Drawing.Size(472, 430);
			this.ClientSelection1.TabIndex = 5;
			//
			//Main_Form
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(472, 430);
			this.Controls.Add(this.ProcessControl1);
			this.Controls.Add(this.ClientSelection1);
			this.Controls.Add(this.DisbursementBatchControl1);
			this.Name = "Main_Form";
			this.Text = "Auto Disbursement";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal ProcessControl ProcessControl1;
		protected internal DisbursementBatchControl DisbursementBatchControl1;
		protected internal ClientSelection ClientSelection1;
	}
}
