#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.UI.Desktop.CS.Disbursement.Common;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
    internal partial class ProcessControl
    {
        internal DataSet ds = new DataSet("ds");

        public event EventHandler Cancelled;

        protected Disbursement.Common.IDisbursementContext context
        {
            get
            {
                ISupportContext ictx = ParentForm as Disbursement.Common.ISupportContext;
                if (ictx != null)
                {
                    return ictx.Context;
                }
                return null;
            }
            //Throw New NotImplementedException()
            set { }
        }

        public ProcessControl() : base()
        {
            InitializeComponent();

            normal_no.Tag  = "1";
            normal_yes.Tag = "2";
            more_no.Tag    = "3";
            more_yes.Tag   = "4";
            less_no.Tag    = "5";
            less_yes.Tag   = "6";

            normal_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show));
            normal_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay));
            normal_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay));
            normal_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip));

            normal_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show));
            normal_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay));
            normal_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay));
            normal_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip));

            more_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show));
            more_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay));
            more_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay));
            more_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_ProrateExcess, Modes.prorate));
            more_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip));

            more_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show));
            more_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay));
            more_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Normal, Modes.pay));
            more_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_ProrateExcess, Modes.prorate));
            more_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip));

            less_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show));
            less_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay));
            less_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_ProrateLess, Modes.prorate));
            less_no.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip));

            less_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Manual, Modes.show));
            less_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_NoPay, Modes.nopay));
            less_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_ProrateLess, Modes.prorate));
            less_yes.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem(Const_Skip, Modes.skip));

            RegisterHandlers();
            ProgressBarControl1.Properties.DisplayFormat.Format = new ProgressBarCustomFormatter();
            ProgressBarControl1.Properties.EditFormat.Format = new ProgressBarCustomFormatter();
        }

        #region "OnCompleted"

        /// <summary>
        /// Generated when the disbursement operation is complete and we should quit.
        /// </summary>
        public event EventHandler Completed;

        /// <summary>
        /// Triggers the Completed event.
        /// </summary>
        protected virtual void OnCompleted(EventArgs ea)
        {
            if (Completed != null)
            {
                Completed(this, ea);
            }
        }

        #endregion "OnCompleted"

        /// <summary>
        /// Format the progress bar to include the percentage in the display
        /// </summary>
        private class ProgressBarCustomFormatter : IFormatProvider, ICustomFormatter
        {
            public object GetFormat(Type formatType)
            {
                return this;
            }

            public string Format(string FormatString, object arg, IFormatProvider formatProvider)
            {
                string answer = string.Empty;
                if (arg != null && !object.ReferenceEquals(arg, DBNull.Value))
                {
                    try
                    {
                        double pct = Convert.ToDouble(arg) / 100.0;
                        answer = string.Format("{0:p0}", pct);
                    }
                    catch { }
                }
                return answer;
            }
        }

        /// <summary>
        /// Modes used to determine what to do based upon the disbursement information
        /// </summary>
        internal enum Modes
        {
            quit    = 0,
            skip    = 1,

            pay     = 2,
            nopay   = 3,
            prorate = 4,
            show    = 5,

            slop    = 20,
            slop_skip,
            slop_pay,
            slop_nopay,
            slop_prorate,
            slop_show
        }

        // Strings for the combo-box literals
        private string Const_Manual = "Manually examine the client";
        private string Const_NoPay = "Do not pay anything.";
        private string Const_Normal = "Pay the creditors the normal amount.";
        private string Const_Skip = "Skip this client. Go to the next.";
        private string Const_ProrateExcess = "Prorate the excess amount among the creditors.";
        private string Const_ProrateLess = "Prorate the available funds among the creditors.";

        private static bool IsLocked
        {
            get { return DebtPlus.Configuration.Config.DisbursementLocked; }
        }

        private void RegisterHandlers()
        {
            Load                            += ProcessControl_Load;
            Button_Cancel.Click             += Button_Cancel_Click;
            Button_Start.Click              += Button_Start_Click;
            normal_no.SelectedIndexChanged  += SelectedIndexChanged;
            normal_yes.SelectedIndexChanged += SelectedIndexChanged;
            less_no.SelectedIndexChanged    += SelectedIndexChanged;
            less_yes.SelectedIndexChanged   += SelectedIndexChanged;
            more_no.SelectedIndexChanged    += SelectedIndexChanged;
            more_yes.SelectedIndexChanged   += SelectedIndexChanged;
        }

        private void UnRegisterHandlers()
        {
            Load                            -= ProcessControl_Load;
            Button_Cancel.Click             -= Button_Cancel_Click;
            Button_Start.Click              -= Button_Start_Click;
            normal_no.SelectedIndexChanged  -= SelectedIndexChanged;
            normal_yes.SelectedIndexChanged -= SelectedIndexChanged;
            less_no.SelectedIndexChanged    -= SelectedIndexChanged;
            less_yes.SelectedIndexChanged   -= SelectedIndexChanged;
            more_no.SelectedIndexChanged    -= SelectedIndexChanged;
            more_yes.SelectedIndexChanged   -= SelectedIndexChanged;
        }

        /// <summary>
        /// Handle the LOAD event for our control
        /// </summary>
        private void ProcessControl_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                SetSelectedItem(ref normal_no, DebtPlus.Configuration.Config.DisbursementDefault1);
                SetSelectedItem(ref normal_yes, DebtPlus.Configuration.Config.DisbursementDefault2);
                SetSelectedItem(ref more_no, DebtPlus.Configuration.Config.DisbursementDefault3);
                SetSelectedItem(ref more_yes, DebtPlus.Configuration.Config.DisbursementDefault4);
                SetSelectedItem(ref less_no, DebtPlus.Configuration.Config.DisbursementDefault5);
                SetSelectedItem(ref less_yes, DebtPlus.Configuration.Config.DisbursementDefault6);

                // Clear the status information
                lbl_client.Text = string.Empty;
                lbl_amount.Text = string.Format("{0:c}", 0m);  // localized $0.00 string.

                lbl_count.Text = string.Empty;
                ProgressBarControl1.Visible = false;

                if (IsLocked)
                {
                    normal_no.Properties.ReadOnly  = GetSelectedItem(ref normal_no) >= 0;
                    normal_yes.Properties.ReadOnly = GetSelectedItem(ref normal_yes) >= 0;
                    more_no.Properties.ReadOnly    = GetSelectedItem(ref more_no) >= 0;
                    more_yes.Properties.ReadOnly   = GetSelectedItem(ref more_yes) >= 0;
                    less_no.Properties.ReadOnly    = GetSelectedItem(ref less_no) >= 0;
                    less_yes.Properties.ReadOnly   = GetSelectedItem(ref less_yes) >= 0;
                }
                else
                {
                    normal_no.Properties.ReadOnly  = false;
                    normal_yes.Properties.ReadOnly = false;
                    more_no.Properties.ReadOnly    = false;
                    more_yes.Properties.ReadOnly   = false;
                    less_no.Properties.ReadOnly    = false;
                    less_yes.Properties.ReadOnly   = false;
                }
            }
            finally
            {
                RegisterHandlers();
            }

            // Enable the OK button if the values are properly set
            Button_Start.Enabled = !HasErrors();
        }

        private void Button_Cancel_Click(object sender, EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// We are now selected. Read the information and "do our thing."
        /// </summary>
        public bool ReadForm()
        {
            bool answer = false;

            try
            {
                using (global::DebtPlus.UI.Common.WaitCursor cm = new global::DebtPlus.UI.Common.WaitCursor())
                {
                    // Read the information for the common processing
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = "xpr_disbursement_general_info";
                        cmd.CommandType = CommandType.StoredProcedure;

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            da.Fill(ds, "xpr_disbursement_general_info");
                        }
                    }

                    // Retrieve the disbursement information for the client fields
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        cmd.CommandText = string.Format("SELECT [oID],[disbursement_register],[client],[held_in_trust],[reserved_in_trust],[disbursement_factor],[note_count],[client_register],[active_status],[start_date],[region],[counselor],[csr],[office],[last_name],[date_created],[created_by] FROM disbursement_clients WHERE disbursement_register=@disbursement_register {0}", ((ArgParser)context.ap).SelectionClause);
                        cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = ((ArgParser)context.ap).BatchID;

                        using (var da = new SqlDataAdapter(cmd))
                        {
                            Cursor.Current = Cursors.WaitCursor;
                            da.Fill(ds, "disbursement_clients");
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading disbursement configuration information");
            }

            // Complain if there are no records to process
            DataTable tbl = ds.Tables["disbursement_clients"];
            if (tbl == null || tbl.Rows.Count == 0)
            {
                DebtPlus.Data.Forms.MessageBox.Show("There are no clients that match your selection criteria", "Sorry, there is nothing to disburse");
            }
            else
            {
                // Format the count of items
                Int32 TotalClientCount = tbl.Rows.Count;
                lbl_total_count.Text = string.Format("out of {0:n0}", TotalClientCount);
                lbl_count.Text = "0";

                // Set the maximum value for the progress bar to the client count
                ProgressBarControl1.Properties.Maximum = TotalClientCount;
                ProgressBarControl1.Properties.Minimum = 0;
                ProgressBarControl1.Properties.PercentView = true;
                ProgressBarControl1.Properties.ShowTitle = true;
                ProgressBarControl1.EditValue = 0;
                ProgressBarControl1.Visible = true;
                answer = true;

                // Enable/Disable the OK button
                Button_Start.Enabled = !HasErrors();
            }

            return answer;
        }

        /// <summary>
        /// Update the statistics client count
        /// </summary>
        private delegate void ProcessClientCountDelegate(Int32 Count);

        private void ProcessClientCount(Int32 Count)
        {
            if (ProgressBarControl1.InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ProcessClientCountDelegate(ProcessClientCount), new object[] { Count });
                EndInvoke(ia);
            }
            else
            {
                ProgressBarControl1.EditValue = Convert.ToInt32(ProgressBarControl1.EditValue) + Count;
                lbl_count.Text = Convert.ToInt32(ProgressBarControl1.EditValue).ToString();
            }
        }

        /// <summary>
        /// Update the statistics total dollars disbursed
        /// </summary>

        private decimal TotalDollarsDisbursed = 0m;

        private delegate void ProcessClientAmountDelegate(decimal Amount);

        private void ProcessClientAmount(decimal Amount = 0m)
        {
            if (InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ProcessClientAmountDelegate(ProcessClientAmount), new object[] { Amount });
                EndInvoke(ia);
            }
            else
            {
                TotalDollarsDisbursed += Amount;
                lbl_amount.Text = string.Format("{0:c}", TotalDollarsDisbursed);
            }
        }

        /// <summary>
        /// Update the statistics current client ID
        /// </summary>
        private delegate void ProcessClientDelegate(Int32 Client);

        private void ProcessClient(Int32 Client)
        {
            if (InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new ProcessClientDelegate(ProcessClient), new object[] { Client });
                EndInvoke(ia);
            }
            else
            {
                lbl_client.Text = string.Format("{0:0000000}", Client);
            }
        }

        private Thread ProcessingThread = null;
        private Int32 CurrentClientIndex = 0;

        /// <summary>
        /// Process the OK button to start the disbursement
        /// </summary>
        private void Button_Start_Click(object sender, EventArgs e)
        {
            // Do not allow the start button again.
            Button_Start.Enabled = false;

            // Start a thread to process the disbursements. It will take it from there
            ProcessingThread = new Thread(ProcessDisbursement);
            ProcessingThread.Name = "ProcessDisbursement";
            ProcessingThread.SetApartmentState(ApartmentState.STA);
            ProcessingThread.Start();
        }

        /// <summary>
        /// The disbursement operation is complete
        /// </summary>
        private void CompletedDisbursement()
        {
            // Do the thread switch if needed
            if (InvokeRequired)
            {
                IAsyncResult ia = BeginInvoke(new MethodInvoker(CompletedDisbursement));
                EndInvoke(ia);
            }
            else
            {
                try
                {
                    lbl_client.Text = string.Empty;
                    ProgressBarControl1.EditValue = ProgressBarControl1.Properties.Maximum;
                }
                catch (Exception ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                }

                // If there is no parent form then just quit the thread. We are complete anyway.
                if (ParentForm != null && !context.MasterQuit)
                {
                    DebtPlus.Data.Forms.MessageBox.Show("The disbursement operation is complete.", "Completed");
                }

                // We should be able to close our parent form at this point and complete the application.
                OnCompleted(EventArgs.Empty);
            }
        }

        // Thread to handle the display of the client information
        private Thread bt = null;

        /// <summary>
        /// Process the disbursement operation
        /// </summary>
        private void ProcessDisbursement()
        {
            DataTable tbl = ds.Tables["disbursement_clients"];
            using (DataView vue = new DataView(tbl, string.Empty, "client", DataViewRowState.CurrentRows))
            {
                Int32 TotalClients = vue.Count;

                // Last client examined when "previous" button is pressed
                ClientClass LastExaminedClient = null;

                // Process the disbursement for this thread
                while ((!context.MasterQuit) && (CurrentClientIndex < TotalClients))
                {
                    DataRowView drv = vue[CurrentClientIndex];
                    CurrentClientIndex += 1;

                    // Find the current client ID
                    Int32 Client = Convert.ToInt32(drv["client"]);
                    ProcessClient(Client);

                    // Read the list of debts
                    DisbursementDebtList DebtList = new DisbursementDebtList();
                    DebtList.ReadDebtsTable(Client, ((ArgParser)((ISupportContext)ParentForm).Context.ap).BatchID);

                    // Recalculate the fee amount before we determine the variance
                    ClientClass ProcessingClass = new ClientClass(context, drv.Row, ref DebtList);
                    ProcessingClass.RecalulateMonthlyFee();

                    // Compute the amount that is needed for the disbursement
                    decimal DisbursementAmount = DebtList.TotalPayments();
                    if (DisbursementAmount > 0m)
                    {
                        DisbursementAmount += DebtList.TotalFees();
                    }
                    DisbursementAmount += DebtList.TotalFixedFeePayments();

                    // Find the relative position for this client in the list of classifications
                    decimal Difference = Convert.ToDecimal(drv["held_in_trust"]) - DisbursementAmount;
                    bool HasNotes = Convert.ToInt32(drv["note_count"]) != 0;

                    // Determine which control is to process this item
                    Modes ProcessingMode = default(Modes);

                    // The expected normal deposit amount
                    if (Difference == 0m)
                    {
                        if (HasNotes)
                        {
                            ProcessingMode = (Modes)GetSelectedItem(ref normal_yes);
                        }
                        else
                        {
                            ProcessingMode = (Modes)GetSelectedItem(ref normal_no);
                        }

                        // More funds than needed
                    }
                    else if (Difference > 0m)
                    {
                        if (HasNotes)
                        {
                            ProcessingMode = (Modes)GetSelectedItem(ref more_yes);
                        }
                        else
                        {
                            ProcessingMode = (Modes)GetSelectedItem(ref more_no);
                        }

                        decimal PaddGreater = Convert.ToDecimal(ds.Tables["xpr_disbursement_general_info"].Rows[0]["payments_pad_greater"]);
                        if (PaddGreater > 0m && Difference <= PaddGreater)
                        {
                            ProcessingMode = (Modes)Convert.ToInt32(ProcessingMode) + Convert.ToInt32(Modes.slop);
                        }
                    }
                    else
                    {
                        if (HasNotes)
                        {
                            ProcessingMode = (Modes)GetSelectedItem(ref less_yes);
                        }
                        else
                        {
                            ProcessingMode = (Modes)GetSelectedItem(ref less_no);
                        }
                        decimal PaddLess = Convert.ToDecimal(ds.Tables["xpr_disbursement_general_info"].Rows[0]["payments_pad_less"]);

                        Difference = 0m - Difference;
                        if (PaddLess > 0m && Difference < PaddLess)
                        {
                            ProcessingMode = (Modes)Convert.ToInt32(ProcessingMode) + Convert.ToInt32(Modes.slop);
                        }
                    }

                    decimal debit_amt = 0M;

                    // Do the major processing cycle for the type. The "examine" has its own modified copy.
                    switch (ProcessingMode)
                    {
                        case Modes.quit:
                            break;

                        case Modes.nopay:
                        case Modes.slop_nopay:
                            ProcessingClass.Do_NoPay();
                            break;

                        case Modes.pay:
                            debit_amt = ProcessingClass.Do_Pay();
                            ProcessClientAmount(debit_amt);
                            break;

                        case Modes.skip:
                        case Modes.slop_skip:
                            break;

                        // Do nothing
                        case Modes.show:
                        case Modes.slop_show:

                            // Join the previous thread if the thread is still busy.
                            // This will cause us to hold until the thread terminates.
                            if (bt != null && bt.IsAlive)
                            {
                                bt.Join();
                            }

                            // Set the handler for the previous button and save the value for next time.
                            if (context == null)
                            {
                                break;
                            }

                            if (!context.MasterQuit)
                            {
                                ProcessingClass.Previous = LastExaminedClient;
                                LastExaminedClient = ProcessingClass;

                                // Create a new thread to handle the client update
                                bt = new Thread(ProcessingClass.Do_Examine_ThreadProcedure);
                                bt.SetApartmentState(ApartmentState.STA);
                                bt.Name = "SHOW_CLIENT_" + Guid.NewGuid().ToString();
                                bt.Start();
                            }
                            break;

                        case Modes.prorate:
                            ProcessingClass.Do_Prorate();
                            debit_amt = ProcessingClass.Do_Pay();
                            ProcessClientAmount(debit_amt);
                            break;

                        case Modes.slop_prorate:
                        case Modes.slop_pay:
                            ProcessingClass.Do_Prorate();
                            debit_amt = ProcessingClass.Do_Pay();
                            ProcessClientAmount(debit_amt);
                            break;

                        default:
                            break;
                    }

                    // Count this client
                    ProcessClientCount(1);
                }

                // At the very end, wait for the last examined client
                if (bt != null && bt.IsAlive)
                {
                    bt.Join();
                }

                // At the end, tell the parent thread that we have completed
                if (context != null)
                {
                    CompletedDisbursement();
                }
            }
        }

        /// <summary>
        /// A drop-down index was changed. Enable the START button as needed
        /// </summary>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            Button_Start.Enabled = !HasErrors();

            // Save the values accordingly
            DebtPlus.Configuration.Config.DisbursementDefault1 = GetSelectedItem(ref normal_no);
            DebtPlus.Configuration.Config.DisbursementDefault2 = GetSelectedItem(ref normal_yes);
            DebtPlus.Configuration.Config.DisbursementDefault3 = GetSelectedItem(ref more_no);
            DebtPlus.Configuration.Config.DisbursementDefault4 = GetSelectedItem(ref more_yes);
            DebtPlus.Configuration.Config.DisbursementDefault5 = GetSelectedItem(ref less_no);
            DebtPlus.Configuration.Config.DisbursementDefault6 = GetSelectedItem(ref less_yes);
        }

        /// <summary>
        /// Determine if the form has error conditions
        /// </summary>
        private bool HasErrors()
        {
            if (GetSelectedItem(ref normal_yes) < 0)
                return true;
            if (GetSelectedItem(ref normal_yes) < 0)
                return true;
            if (GetSelectedItem(ref normal_no) < 0)
                return true;
            if (GetSelectedItem(ref less_yes) < 0)
                return true;
            if (GetSelectedItem(ref less_no) < 0)
                return true;
            if (GetSelectedItem(ref more_yes) < 0)
                return true;
            if (GetSelectedItem(ref more_no) < 0)
                return true;

            return false;
        }

        private Int32 GetSelectedItem(ref DevExpress.XtraEditors.ComboBoxEdit ctl)
        {
            DebtPlus.Data.Controls.ComboboxItem item = ctl.SelectedItem as DebtPlus.Data.Controls.ComboboxItem;
            if (item != null)
            {
                return Convert.ToInt32(item.value);
            }
            return -1;
        }

        private void SetSelectedItem(ref DevExpress.XtraEditors.ComboBoxEdit ctl, Int32 value)
        {
            for (var idx = 0; idx <= ctl.Properties.Items.Count - 1; idx++)
            {
                DebtPlus.Data.Controls.ComboboxItem item = ctl.Properties.Items[idx] as DebtPlus.Data.Controls.ComboboxItem;
                if (item != null)
                {
                    if ((int)item.value == value)
                    {
                        ctl.SelectedIndex = idx;
                        return;
                    }
                }
            }

            ctl.SelectedIndex = -1;
        }
    }
}