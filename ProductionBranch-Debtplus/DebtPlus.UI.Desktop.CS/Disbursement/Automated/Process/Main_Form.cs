#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
    internal partial class Main_Form : Disbursement.Common.ISupportContext
    {
        public Main_Form() : base()
        {
            InitializeComponent();
        }

        public Disbursement.Common.IDisbursementContext context { get; set; }

        Disbursement.Common.IDisbursementContext Disbursement.Common.ISupportContext.Context
        {
            get { return context; }
            set { context = value; }
        }

        public Main_Form(Disbursement.Common.IDisbursementContext context) : this()
        {
            this.context = context;
            Load += Form_Load;
            DisbursementBatchControl1.Cancelled += DisbursementRegisterControl1_Cancelled;
            DisbursementBatchControl1.Selected += DisbursementRegisterControl1_Selected;
            ClientSelection1.Selected += ClientSelection1_Selected;
            ProcessControl1.Completed += ProcessControl1_Completed;
            ClientSelection1.Cancelled += DisbursementRegisterControl1_Cancelled;
            ProcessControl1.Cancelled += DisbursementRegisterControl1_Cancelled;
        }

        private void Form_Load(object sender, System.EventArgs e)
        {
            ClientSelection1.Visible = false;
            ProcessControl1.Visible = false;

            if (((ArgParser)context.ap).BatchID <= 0)
            {
                DisbursementBatchControl1.Visible = true;
                DisbursementBatchControl1.ReadForm();
            }
            else
            {
                ClientSelection1.Visible = true;
                ClientSelection1.ReadForm();
            }
        }

        private void DisbursementRegisterControl1_Cancelled(object sender, System.EventArgs e)
        {
            context.MasterQuit = true;
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void DisbursementRegisterControl1_Selected(object sender, System.EventArgs e)
        {
            DisbursementBatchControl1.Visible = false;
            ((ArgParser)context.ap).BatchID = DisbursementBatchControl1.BatchID;

            ClientSelection1.Visible = true;
            ClientSelection1.ReadForm();
        }

        private void ClientSelection1_Selected(object sender, System.EventArgs e)
        {
            ((ArgParser)context.ap).SelectionClause = ClientSelection1.SelectionClause;
            if (ProcessControl1.ReadForm())
            {
                ClientSelection1.Visible = false;
                ProcessControl1.Visible = true;
            }
        }

        private void ProcessControl1_Completed(object sender, System.EventArgs e)
        {
            context.MasterQuit = true;
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}