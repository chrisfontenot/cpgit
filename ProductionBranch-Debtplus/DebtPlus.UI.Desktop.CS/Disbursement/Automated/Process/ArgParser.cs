using System;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        public Int32 BatchID { get; set; }
        public string SelectionClause { get; set; }

        public ArgParser() : base(new string[] { "b" })
        {
        }

        public ArgParser(string[] switchSymbols) : base(switchSymbols)
        {
        }

        public ArgParser(string[] switchSymbols, bool caseSensitiveSwitches) : base(switchSymbols, caseSensitiveSwitches)
        {
        }

        public ArgParser(string[] switchSymbols, bool caseSensitiveSwitches, string[] switchChars) : base(switchSymbols, caseSensitiveSwitches, switchChars)
        {
        }

        protected override SwitchStatus OnSwitch(string switchName, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchName)
            {
                case "b":
                    double dblTemp = 0;
                    if (double.TryParse(switchValue, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out dblTemp))
                    {
                        if (dblTemp >= 0 && dblTemp < Convert.ToDouble(Int32.MaxValue))
                        {
                            BatchID = Convert.ToInt32(dblTemp);
                            break; // TODO: might not be correct. Was : Exit Select
                        }
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }

            return ss;
        }

        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}