using System;
using System.Linq;
using DebtPlus.LINQ;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
    internal partial class ClientSelection
    {
        public ClientSelection() : base()
        {
            InitializeComponent();
        }

        protected Disbursement.Common.IDisbursementContext context
        {
            get { return ((Disbursement.Common.ISupportContext)ParentForm).Context; }
            set
            {
                throw new NotImplementedException();
            }
        }

        private string _SelectionClause = string.Empty;

        public string SelectionClause
        {
            get { return _SelectionClause; }
        }

        public enum SelectionCriteria : int
        {
            All_Clients,
            Client_ID,
            Client_LastName,
            TrustBalance,
            Counselor,
            Office,
            CSR
        }

        public event EventHandler Cancelled;

        public event EventHandler Selected;

        private void RegisterHandlers()
        {
            Button_OK.Click                    += Button_OK_Click;
            Button_Cancel.Click                += Button_Cancel_Click;
            SelectionList.SelectedIndexChanged += SelectionList_SelectedIndexChanged;
            Resize += ClientSelection_Resize;
        }

        private void UnRegisterHandlers()
        {
            Button_OK.Click                    -= Button_OK_Click;
            Button_Cancel.Click                -= Button_Cancel_Click;
            SelectionList.SelectedIndexChanged -= SelectionList_SelectedIndexChanged;
            Resize -= ClientSelection_Resize;
        }

        /// <summary>
        /// Handle the form resize event
        /// </summary>
        private void ClientSelection_Resize(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                HandleResize();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Center the buttons at the bottom of the form
        /// </summary>
        private void HandleResize()
        {
            emptySpaceItem_Left.Width = (emptySpaceItem_Left.Width + emptySpaceItem_Right.Width) >> 1;
        }

        /// <summary>
        /// Load the controls with the selection criteria
        /// </summary>
        public void ReadForm()
        {
            UnRegisterHandlers();
            try
            {
                // Populate the list of items
                SelectionList.Properties.Items.Clear();
                SelectionList.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("All Clients", SelectionCriteria.All_Clients));
                SelectionList.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client IDs in the range between", SelectionCriteria.Client_ID));
                SelectionList.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client last names between", SelectionCriteria.Client_LastName));
                SelectionList.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Client trust balances between", SelectionCriteria.TrustBalance));
                SelectionList.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Counselor is equal to", SelectionCriteria.Counselor));
                SelectionList.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("Office is equal to", SelectionCriteria.Office));
                SelectionList.Properties.Items.Add(new DebtPlus.Data.Controls.ComboboxItem("CSR is equal to", SelectionCriteria.CSR));
                SelectionList.SelectedIndex = 0;

                LoadParameters();
                HandleResize();
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the CLICK event on the CANCEL button
        /// </summary>
        private void Button_Cancel_Click(object sender, System.EventArgs e)
        {
            RaiseCancelled();
        }

        /// <summary>
        /// Raise the CANCEL event
        /// </summary>
        protected void RaiseCancelled()
        {
            var evt = Cancelled;
            if (evt != null)
            {
                evt(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Process the CLICK event on the OK button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_OK_Click(object sender, System.EventArgs e)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            // Determine the selection criteria
            switch ((SelectionCriteria)((DebtPlus.Data.Controls.ComboboxItem)SelectionList.SelectedItem).value)
            {
                case SelectionCriteria.All_Clients:
                    break;  // do nothing

                case SelectionCriteria.Client_ID:
                    System.Int32 ClientID = 0;
                    if (Int32.TryParse(Parameter1.Text.Trim(), out ClientID) && ClientID >= 0)
                    {
                        sb.AppendFormat(" AND client>={0:f0}", ClientID);
                    }
                    if (Int32.TryParse(Parameter2.Text.Trim(), out ClientID) && ClientID >= 0)
                    {
                        sb.AppendFormat(" AND client<={0:f0}", ClientID);
                    }
                    break;

                case SelectionCriteria.Client_LastName:
                    if (Parameter1.SelectedIndex >= 0)
                    {
                        sb.AppendFormat(" AND last_name>='{0}'", Parameter1.Text);
                    }

                    if (Parameter2.SelectedIndex >= 0)
                    {
                        sb.AppendFormat(" AND last_name<='{0}'", Parameter2.Text);
                    }
                    break;

                case SelectionCriteria.TrustBalance:
                    if (Parameter1.SelectedIndex >= 0)
                    {
                        sb.AppendFormat(" AND held_in_trust>={0:f2}", Convert.ToDecimal(Parameter1.EditValue));
                    }

                    if (Parameter2.SelectedIndex >= 0)
                    {
                        sb.AppendFormat(" AND held_in_trust<={0:f2}", Convert.ToDecimal(Parameter2.EditValue));
                    }
                    break;

                case SelectionCriteria.Counselor:
                    sb.AppendFormat(" AND counselor={0:f0}", Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)Parameter1.SelectedItem).value));
                    break;

                case SelectionCriteria.Office:
                    sb.AppendFormat(" AND office={0:f0}", Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)Parameter1.SelectedItem).value));
                    break;

                case SelectionCriteria.CSR:
                    sb.AppendFormat(" AND csr={0:f0}", Convert.ToInt32(((DebtPlus.Data.Controls.ComboboxItem)Parameter1.SelectedItem).value));
                    break;
            }

            // If the checkbox is set then include only items which have no disbursement register
            if (OnlyNonDisbursedClients.Checked)
            {
                sb.Append(" AND client_register is null");
            }

            _SelectionClause = sb.ToString();

            RaiseSelected();
        }

        /// <summary>
        /// Raise the selected event
        /// </summary>
        protected void RaiseSelected()
        {
            var evt = Selected;
            if (evt != null)
            {
                evt(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Process a change in the selected item list
        /// </summary>
        private void SelectionList_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            LoadParameters();
        }

        /// <summary>
        /// Load the Parameter2 and Parameter3 controls with the proper items
        /// </summary>
        private void LoadParameters()
        {
            Parameter1.SelectedIndex = -1;
            Parameter1.Text = string.Empty;
            Parameter1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;

            Parameter2.SelectedIndex = -1;
            Parameter2.Text = string.Empty;
            Parameter2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.None;

            switch ((SelectionCriteria)((DebtPlus.Data.Controls.ComboboxItem)SelectionList.SelectedItem).value)
            {
                case SelectionCriteria.All_Clients:
                    layoutControlItem_Parameter1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
                    layoutControlItem_Parameter2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
                    layoutControlItem_And.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
                    break;

                case SelectionCriteria.Client_ID:
                    layoutControlItem_Parameter1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_Parameter2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_And.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

                    Parameter1.Properties.Items.Clear();
                    Parameter1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    Parameter1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                    Parameter1.Properties.Mask.EditMask = "f0";
                    Parameter1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                    Parameter1.Properties.Mask.BeepOnError = true;

                    Parameter1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    Parameter1.Properties.DisplayFormat.Format = new DebtPlus.Utils.Format.Client.CustomFormatter();
                    Parameter1.Properties.DisplayFormat.FormatString = "0000000";

                    Parameter2.Properties.Items.Clear();
                    Parameter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    Parameter2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                    Parameter2.Properties.Mask.EditMask = "f0";
                    Parameter2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                    Parameter2.Properties.Mask.BeepOnError = true;

                    Parameter2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
                    Parameter2.Properties.DisplayFormat.Format = new DebtPlus.Utils.Format.Client.CustomFormatter();
                    Parameter2.Properties.DisplayFormat.FormatString = "0000000";
                    break;

                case SelectionCriteria.Client_LastName:
                    layoutControlItem_Parameter1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_Parameter2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_And.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

                    Parameter1.Properties.Items.Clear();
                    Parameter1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    Parameter1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                    Parameter1.Properties.Items.AddRange("ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray());
                    Parameter1.SelectedIndex = 0;

                    Parameter2.Properties.Items.Clear();
                    Parameter2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    Parameter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                    Parameter2.Properties.Items.AddRange("ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray());
                    Parameter2.SelectedIndex = Parameter2.Properties.Items.Count - 1;
                    break;

                case SelectionCriteria.Counselor:
                    layoutControlItem_Parameter1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_Parameter2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
                    layoutControlItem_And.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;

                    Parameter1.Properties.Items.Clear();
                    Parameter1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    Parameter1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;

                    Parameter1.Properties.Sorted = false;
                    Parameter1.Properties.Items.AddRange(DebtPlus.LINQ.Cache.counselor.CounselorsList().AsQueryable<DebtPlus.LINQ.counselor>().Select(s => new DebtPlus.Data.Controls.ComboboxItem(s.Name == null ? string.Empty : s.Name.ToString(), s.Id, s.ActiveFlag)).ToArray());
                    Parameter1.Properties.Sorted = true;

                    Parameter1.SelectedIndex = 0;
                    break;

                case SelectionCriteria.CSR:
                    layoutControlItem_Parameter1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_Parameter2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
                    layoutControlItem_And.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;

                    Parameter1.Properties.Items.Clear();
                    Parameter1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    Parameter1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;

                    Parameter1.Properties.Sorted = false;
                    Parameter1.Properties.Items.AddRange(DebtPlus.LINQ.Cache.counselor.CSRsList().AsQueryable<DebtPlus.LINQ.counselor>().Select(s => new DebtPlus.Data.Controls.ComboboxItem(s.Name == null ? string.Empty : s.Name.ToString(), s.Id, s.ActiveFlag)).ToArray());
                    Parameter1.Properties.Sorted = true;

                    Parameter1.SelectedIndex = 0;
                    break;

                case SelectionCriteria.Office:
                    layoutControlItem_Parameter1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_Parameter2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;
                    layoutControlItem_And.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.OnlyInCustomization;

                    Parameter1.Properties.Items.Clear();
                    Parameter1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
                    Parameter1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;

                    Parameter1.Properties.Sorted = false;
                    Parameter1.Properties.Items.AddRange(DebtPlus.LINQ.Cache.office.getList().AsQueryable<DebtPlus.LINQ.office>().Select(s => new DebtPlus.Data.Controls.ComboboxItem(s.name, s.Id, s.ActiveFlag)).ToArray());
                    Parameter1.Properties.Sorted = true;

                    Parameter1.SelectedIndex = 0;
                    break;

                case SelectionCriteria.TrustBalance:
                    layoutControlItem_Parameter1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_Parameter2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                    layoutControlItem_And.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;

                    Parameter1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                    Parameter1.Properties.Items.Clear();
                    Parameter1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    Parameter1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                    Parameter1.Properties.Mask.EditMask = "n2";
                    Parameter1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                    Parameter1.Properties.Mask.BeepOnError = true;
                    Parameter1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    Parameter1.Properties.DisplayFormat.FormatString = "c";

                    Parameter2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                    Parameter2.Properties.Items.Clear();
                    Parameter2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
                    Parameter2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
                    Parameter2.Properties.Mask.EditMask = "n2";
                    Parameter2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
                    Parameter2.Properties.Mask.BeepOnError = true;
                    Parameter2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
                    Parameter2.Properties.DisplayFormat.FormatString = "c";
                    break;
            }
        }
    }
}