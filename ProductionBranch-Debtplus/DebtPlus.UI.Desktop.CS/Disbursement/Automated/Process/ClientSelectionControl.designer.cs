using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
	partial class ClientSelection : DevExpress.XtraEditors.XtraUserControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
            this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
            this.Button_OK = new DevExpress.XtraEditors.SimpleButton();
            this.OnlyNonDisbursedClients = new DevExpress.XtraEditors.CheckEdit();
            this.Parameter2 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.LabelParameter2 = new DevExpress.XtraEditors.LabelControl();
            this.Parameter1 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.SelectionList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem_Button_Cancel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_LabelControl1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_SelectionList = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_And = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_OnlyNonDisbursed = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Button_OK = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem_Left = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem_Right = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem_Parameter1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem_Parameter2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.OnlyNonDisbursedClients.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectionList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_LabelControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SelectionList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_And)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_OnlyNonDisbursed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_OK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Parameter1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Parameter2)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelControl1
            // 
            this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
            this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelControl1.Location = new System.Drawing.Point(12, 12);
            this.LabelControl1.Name = "LabelControl1";
            this.LabelControl1.Size = new System.Drawing.Size(435, 47);
            this.LabelControl1.StyleController = this.layoutControl1;
            this.LabelControl1.TabIndex = 0;
            this.LabelControl1.Text = "The next step is to decide which group of clients that you wish to include in you" +
    "r selection now. The choice of clients should not overlap between workstations p" +
    "rocessing this disbursement.";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.Button_Cancel);
            this.layoutControl1.Controls.Add(this.Button_OK);
            this.layoutControl1.Controls.Add(this.OnlyNonDisbursedClients);
            this.layoutControl1.Controls.Add(this.Parameter2);
            this.layoutControl1.Controls.Add(this.LabelControl1);
            this.layoutControl1.Controls.Add(this.LabelParameter2);
            this.layoutControl1.Controls.Add(this.Parameter1);
            this.layoutControl1.Controls.Add(this.SelectionList);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(459, 203);
            this.layoutControl1.TabIndex = 8;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // Button_Cancel
            // 
            this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_Cancel.Location = new System.Drawing.Point(239, 168);
            this.Button_Cancel.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_Cancel.Name = "Button_Cancel";
            this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
            this.Button_Cancel.StyleController = this.layoutControl1;
            this.Button_Cancel.TabIndex = 7;
            this.Button_Cancel.Text = "&Cancel";
            // 
            // Button_OK
            // 
            this.Button_OK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Button_OK.Location = new System.Drawing.Point(160, 168);
            this.Button_OK.MaximumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.MinimumSize = new System.Drawing.Size(75, 23);
            this.Button_OK.Name = "Button_OK";
            this.Button_OK.Size = new System.Drawing.Size(75, 23);
            this.Button_OK.StyleController = this.layoutControl1;
            this.Button_OK.TabIndex = 6;
            this.Button_OK.Text = "&OK";
            // 
            // OnlyNonDisbursedClients
            // 
            this.OnlyNonDisbursedClients.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.OnlyNonDisbursedClients.EditValue = true;
            this.OnlyNonDisbursedClients.Location = new System.Drawing.Point(12, 144);
            this.OnlyNonDisbursedClients.Name = "OnlyNonDisbursedClients";
            this.OnlyNonDisbursedClients.Properties.Caption = "Only include clients who have not been disbursed in this batch";
            this.OnlyNonDisbursedClients.Size = new System.Drawing.Size(435, 20);
            this.OnlyNonDisbursedClients.StyleController = this.layoutControl1;
            this.OnlyNonDisbursedClients.TabIndex = 5;
            // 
            // Parameter2
            // 
            this.Parameter2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Parameter2.Location = new System.Drawing.Point(339, 63);
            this.Parameter2.Name = "Parameter2";
            this.Parameter2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Parameter2.Size = new System.Drawing.Size(108, 20);
            this.Parameter2.StyleController = this.layoutControl1;
            this.Parameter2.TabIndex = 4;
            this.Parameter2.Visible = false;
            // 
            // LabelParameter2
            // 
            this.LabelParameter2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelParameter2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelParameter2.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.LabelParameter2.Location = new System.Drawing.Point(317, 63);
            this.LabelParameter2.Name = "LabelParameter2";
            this.LabelParameter2.Size = new System.Drawing.Size(18, 13);
            this.LabelParameter2.StyleController = this.layoutControl1;
            this.LabelParameter2.TabIndex = 1;
            this.LabelParameter2.Text = "and";
            this.LabelParameter2.Visible = false;
            // 
            // Parameter1
            // 
            this.Parameter1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Parameter1.Location = new System.Drawing.Point(234, 63);
            this.Parameter1.Name = "Parameter1";
            this.Parameter1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Parameter1.Size = new System.Drawing.Size(79, 20);
            this.Parameter1.StyleController = this.layoutControl1;
            this.Parameter1.TabIndex = 3;
            this.Parameter1.Visible = false;
            // 
            // SelectionList
            // 
            this.SelectionList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SelectionList.Location = new System.Drawing.Point(12, 63);
            this.SelectionList.Name = "SelectionList";
            this.SelectionList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SelectionList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.SelectionList.Size = new System.Drawing.Size(218, 20);
            this.SelectionList.StyleController = this.layoutControl1;
            this.SelectionList.TabIndex = 2;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlGroup1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.layoutControlGroup1.AppearanceItemCaption.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem_Button_Cancel,
            this.layoutControlItem_SelectionList,
            this.layoutControlItem_And,
            this.layoutControlItem_OnlyNonDisbursed,
            this.layoutControlItem_Button_OK,
            this.emptySpaceItem_Left,
            this.emptySpaceItem_Right,
            this.emptySpaceItem3,
            this.layoutControlItem_Parameter1,
            this.layoutControlItem_Parameter2,
            this.layoutControlItem_LabelControl1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(459, 203);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            // 
            // layoutControlItem_Button_Cancel
            // 
            this.layoutControlItem_Button_Cancel.Control = this.Button_Cancel;
            this.layoutControlItem_Button_Cancel.CustomizationFormText = "layoutControlItem_Button_Cancel";
            this.layoutControlItem_Button_Cancel.Location = new System.Drawing.Point(227, 156);
            this.layoutControlItem_Button_Cancel.Name = "layoutControlItem_Button_Cancel";
            this.layoutControlItem_Button_Cancel.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem_Button_Cancel.Text = "layoutControlItem_Button_Cancel";
            this.layoutControlItem_Button_Cancel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Button_Cancel.TextToControlDistance = 0;
            this.layoutControlItem_Button_Cancel.TextVisible = false;
            // 
            // layoutControlItem_LabelControl1
            // 
            this.layoutControlItem_LabelControl1.Control = this.LabelControl1;
            this.layoutControlItem_LabelControl1.CustomizationFormText = "layoutControlItem_LabelControl1";
            this.layoutControlItem_LabelControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem_LabelControl1.MaxSize = new System.Drawing.Size(0, 51);
            this.layoutControlItem_LabelControl1.MinSize = new System.Drawing.Size(176, 51);
            this.layoutControlItem_LabelControl1.Name = "layoutControlItem_LabelControl1";
            this.layoutControlItem_LabelControl1.Size = new System.Drawing.Size(439, 51);
            this.layoutControlItem_LabelControl1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem_LabelControl1.Text = "layoutControlItem_LabelControl1";
            this.layoutControlItem_LabelControl1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_LabelControl1.TextToControlDistance = 0;
            this.layoutControlItem_LabelControl1.TextVisible = false;
            // 
            // layoutControlItem_SelectionList
            // 
            this.layoutControlItem_SelectionList.Control = this.SelectionList;
            this.layoutControlItem_SelectionList.CustomizationFormText = "layoutControlItem_Parameter1";
            this.layoutControlItem_SelectionList.Location = new System.Drawing.Point(0, 51);
            this.layoutControlItem_SelectionList.MaxSize = new System.Drawing.Size(222, 24);
            this.layoutControlItem_SelectionList.MinSize = new System.Drawing.Size(222, 24);
            this.layoutControlItem_SelectionList.Name = "layoutControlItem_SelectionList";
            this.layoutControlItem_SelectionList.Size = new System.Drawing.Size(222, 24);
            this.layoutControlItem_SelectionList.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem_SelectionList.Text = "layoutControlItem_SelectionList";
            this.layoutControlItem_SelectionList.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_SelectionList.TextToControlDistance = 0;
            this.layoutControlItem_SelectionList.TextVisible = false;
            // 
            // layoutControlItem_And
            // 
            this.layoutControlItem_And.Control = this.LabelParameter2;
            this.layoutControlItem_And.ControlAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.layoutControlItem_And.CustomizationFormText = "layoutControlItem_And";
            this.layoutControlItem_And.Location = new System.Drawing.Point(305, 51);
            this.layoutControlItem_And.Name = "layoutControlItem_And";
            this.layoutControlItem_And.Size = new System.Drawing.Size(22, 24);
            this.layoutControlItem_And.Text = "layoutControlItem_And";
            this.layoutControlItem_And.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem_And.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_And.TextToControlDistance = 0;
            this.layoutControlItem_And.TextVisible = false;
            // 
            // layoutControlItem_OnlyNonDisbursed
            // 
            this.layoutControlItem_OnlyNonDisbursed.Control = this.OnlyNonDisbursedClients;
            this.layoutControlItem_OnlyNonDisbursed.CustomizationFormText = "layoutControlItem_OnlyNonDisbursed";
            this.layoutControlItem_OnlyNonDisbursed.Location = new System.Drawing.Point(0, 132);
            this.layoutControlItem_OnlyNonDisbursed.Name = "layoutControlItem_OnlyNonDisbursed";
            this.layoutControlItem_OnlyNonDisbursed.Size = new System.Drawing.Size(439, 24);
            this.layoutControlItem_OnlyNonDisbursed.Text = "layoutControlItem_OnlyNonDisbursed";
            this.layoutControlItem_OnlyNonDisbursed.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_OnlyNonDisbursed.TextToControlDistance = 0;
            this.layoutControlItem_OnlyNonDisbursed.TextVisible = false;
            // 
            // layoutControlItem_Button_OK
            // 
            this.layoutControlItem_Button_OK.Control = this.Button_OK;
            this.layoutControlItem_Button_OK.CustomizationFormText = "layoutControlItem_Button_OK";
            this.layoutControlItem_Button_OK.Location = new System.Drawing.Point(148, 156);
            this.layoutControlItem_Button_OK.Name = "layoutControlItem_Button_OK";
            this.layoutControlItem_Button_OK.Size = new System.Drawing.Size(79, 27);
            this.layoutControlItem_Button_OK.Text = "layoutControlItem_Button_OK";
            this.layoutControlItem_Button_OK.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Button_OK.TextToControlDistance = 0;
            this.layoutControlItem_Button_OK.TextVisible = false;
            // 
            // emptySpaceItem_Left
            // 
            this.emptySpaceItem_Left.AllowHotTrack = false;
            this.emptySpaceItem_Left.CustomizationFormText = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Location = new System.Drawing.Point(0, 156);
            this.emptySpaceItem_Left.Name = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.Size = new System.Drawing.Size(213, 27);
            this.emptySpaceItem_Left.Text = "emptySpaceItem_Left";
            this.emptySpaceItem_Left.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem_Right
            // 
            this.emptySpaceItem_Right.AllowHotTrack = false;
            this.emptySpaceItem_Right.CustomizationFormText = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Location = new System.Drawing.Point(371, 156);
            this.emptySpaceItem_Right.Name = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.Size = new System.Drawing.Size(259, 27);
            this.emptySpaceItem_Right.Text = "emptySpaceItem_Right";
            this.emptySpaceItem_Right.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 75);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(630, 57);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem_Parameter1
            // 
            this.layoutControlItem_Parameter1.Control = this.Parameter1;
            this.layoutControlItem_Parameter1.CustomizationFormText = "layoutControlItem_Parameter3";
            this.layoutControlItem_Parameter1.Location = new System.Drawing.Point(222, 51);
            this.layoutControlItem_Parameter1.Name = "layoutControlItem_Parameter1";
            this.layoutControlItem_Parameter1.Size = new System.Drawing.Size(83, 24);
            this.layoutControlItem_Parameter1.Text = "layoutControlItem_Parameter1";
            this.layoutControlItem_Parameter1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Parameter1.TextToControlDistance = 0;
            this.layoutControlItem_Parameter1.TextVisible = false;
            // 
            // layoutControlItem_Parameter2
            // 
            this.layoutControlItem_Parameter2.Control = this.Parameter2;
            this.layoutControlItem_Parameter2.CustomizationFormText = "layoutControlItem_Parameter2";
            this.layoutControlItem_Parameter2.Location = new System.Drawing.Point(327, 51);
            this.layoutControlItem_Parameter2.Name = "layoutControlItem_Parameter2";
            this.layoutControlItem_Parameter2.Size = new System.Drawing.Size(112, 24);
            this.layoutControlItem_Parameter2.Text = "layoutControlItem_Parameter2";
            this.layoutControlItem_Parameter2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem_Parameter2.TextToControlDistance = 0;
            this.layoutControlItem_Parameter2.TextVisible = false;
            // 
            // ClientSelection
            // 
            this.Controls.Add(this.layoutControl1);
            this.Name = "ClientSelection";
            this.Size = new System.Drawing.Size(459, 203);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.OnlyNonDisbursedClients.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Parameter1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SelectionList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_LabelControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_SelectionList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_And)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_OnlyNonDisbursed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Button_OK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Parameter1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem_Parameter2)).EndInit();
            this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl LabelParameter2;
		protected internal DevExpress.XtraEditors.ComboBoxEdit SelectionList;
		protected internal DevExpress.XtraEditors.ComboBoxEdit Parameter1;
		protected internal DevExpress.XtraEditors.ComboBoxEdit Parameter2;
		protected internal DevExpress.XtraEditors.CheckEdit OnlyNonDisbursedClients;
		protected internal DevExpress.XtraEditors.SimpleButton Button_OK;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Button_Cancel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_SelectionList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Parameter2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_And;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Parameter1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_OnlyNonDisbursed;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_Button_OK;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Left;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem_Right;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem_LabelControl1;
	}
}
