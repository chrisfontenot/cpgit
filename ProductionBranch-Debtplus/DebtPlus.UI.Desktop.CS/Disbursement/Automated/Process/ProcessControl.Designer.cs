using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Process
{
	partial class ProcessControl : DevExpress.XtraEditors.XtraUserControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing) {
					if (components != null)
						components.Dispose();
					ds.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Button_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.Button_Start = new DevExpress.XtraEditors.SimpleButton();
			this.GroupControl4 = new DevExpress.XtraEditors.GroupControl();
			this.less_yes = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl12 = new DevExpress.XtraEditors.LabelControl();
			this.less_no = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
			this.GroupControl3 = new DevExpress.XtraEditors.GroupControl();
			this.more_yes = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
			this.more_no = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl11 = new DevExpress.XtraEditors.LabelControl();
			this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
			this.normal_yes = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl9 = new DevExpress.XtraEditors.LabelControl();
			this.normal_no = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl8 = new DevExpress.XtraEditors.LabelControl();
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.lbl_total_count = new DevExpress.XtraEditors.LabelControl();
			this.ProgressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
			this.lbl_amount = new DevExpress.XtraEditors.LabelControl();
			this.lbl_count = new DevExpress.XtraEditors.LabelControl();
			this.lbl_client = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.GroupControl4).BeginInit();
			this.GroupControl4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.less_yes.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.less_no.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl3).BeginInit();
			this.GroupControl3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.more_yes.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.more_no.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
			this.GroupControl2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.normal_yes.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.normal_no.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//Button_Cancel
			//
			this.Button_Cancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Cancel.Location = new System.Drawing.Point(264, 393);
			this.Button_Cancel.Name = "Button_Cancel";
			this.Button_Cancel.Size = new System.Drawing.Size(75, 23);
			this.Button_Cancel.TabIndex = 13;
			this.Button_Cancel.Text = "&Cancel";
			//
			//Button_Start
			//
			this.Button_Start.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.Button_Start.Enabled = false;
			this.Button_Start.Location = new System.Drawing.Point(139, 393);
			this.Button_Start.Name = "Button_Start";
			this.Button_Start.Size = new System.Drawing.Size(75, 23);
			this.Button_Start.TabIndex = 12;
			this.Button_Start.Text = "&Start";
			//
			//GroupControl4
			//
			this.GroupControl4.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GroupControl4.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.GroupControl4.Appearance.Options.UseForeColor = true;
			this.GroupControl4.Controls.Add(this.less_yes);
			this.GroupControl4.Controls.Add(this.LabelControl12);
			this.GroupControl4.Controls.Add(this.less_no);
			this.GroupControl4.Controls.Add(this.LabelControl13);
			this.GroupControl4.Location = new System.Drawing.Point(12, 305);
			this.GroupControl4.Name = "GroupControl4";
			this.GroupControl4.Size = new System.Drawing.Size(440, 72);
			this.GroupControl4.TabIndex = 11;
			this.GroupControl4.Text = "If there is less money in the trust than what is normally needed";
			//
			//less_yes
			//
			this.less_yes.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.less_yes.Location = new System.Drawing.Point(208, 48);
			this.less_yes.Name = "less_yes";
			this.less_yes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.less_yes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.less_yes.Size = new System.Drawing.Size(224, 20);
			this.less_yes.TabIndex = 3;
			//
			//LabelControl12
			//
			this.LabelControl12.Appearance.ForeColor = System.Drawing.Color.Red;
			this.LabelControl12.Appearance.Options.UseForeColor = true;
			this.LabelControl12.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl12.Location = new System.Drawing.Point(24, 52);
			this.LabelControl12.Name = "LabelControl12";
			this.LabelControl12.Size = new System.Drawing.Size(164, 13);
			this.LabelControl12.TabIndex = 2;
			this.LabelControl12.Text = "And there are disbursement notes";
			//
			//less_no
			//
			this.less_no.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.less_no.Location = new System.Drawing.Point(208, 24);
			this.less_no.Name = "less_no";
			this.less_no.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.less_no.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.less_no.Size = new System.Drawing.Size(224, 20);
			this.less_no.TabIndex = 1;
			//
			//LabelControl13
			//
			this.LabelControl13.Appearance.ForeColor = System.Drawing.Color.Green;
			this.LabelControl13.Appearance.Options.UseForeColor = true;
			this.LabelControl13.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl13.Location = new System.Drawing.Point(24, 28);
			this.LabelControl13.Name = "LabelControl13";
			this.LabelControl13.Size = new System.Drawing.Size(179, 13);
			this.LabelControl13.TabIndex = 0;
			this.LabelControl13.Text = "And there are no disbursement notes";
			//
			//GroupControl3
			//
			this.GroupControl3.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GroupControl3.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.GroupControl3.Appearance.Options.UseForeColor = true;
			this.GroupControl3.Controls.Add(this.more_yes);
			this.GroupControl3.Controls.Add(this.LabelControl10);
			this.GroupControl3.Controls.Add(this.more_no);
			this.GroupControl3.Controls.Add(this.LabelControl11);
			this.GroupControl3.Location = new System.Drawing.Point(12, 225);
			this.GroupControl3.Name = "GroupControl3";
			this.GroupControl3.Size = new System.Drawing.Size(440, 72);
			this.GroupControl3.TabIndex = 10;
			this.GroupControl3.Text = "If there is more money in the trust than what is normal";
			//
			//more_yes
			//
			this.more_yes.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.more_yes.Location = new System.Drawing.Point(208, 48);
			this.more_yes.Name = "more_yes";
			this.more_yes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.more_yes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.more_yes.Size = new System.Drawing.Size(224, 20);
			this.more_yes.TabIndex = 3;
			//
			//LabelControl10
			//
			this.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Red;
			this.LabelControl10.Appearance.Options.UseForeColor = true;
			this.LabelControl10.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl10.Location = new System.Drawing.Point(24, 52);
			this.LabelControl10.Name = "LabelControl10";
			this.LabelControl10.Size = new System.Drawing.Size(164, 13);
			this.LabelControl10.TabIndex = 2;
			this.LabelControl10.Text = "And there are disbursement notes";
			//
			//more_no
			//
			this.more_no.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.more_no.Location = new System.Drawing.Point(208, 24);
			this.more_no.Name = "more_no";
			this.more_no.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.more_no.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.more_no.Size = new System.Drawing.Size(224, 20);
			this.more_no.TabIndex = 1;
			//
			//LabelControl11
			//
			this.LabelControl11.Appearance.ForeColor = System.Drawing.Color.Green;
			this.LabelControl11.Appearance.Options.UseForeColor = true;
			this.LabelControl11.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl11.Location = new System.Drawing.Point(24, 28);
			this.LabelControl11.Name = "LabelControl11";
			this.LabelControl11.Size = new System.Drawing.Size(179, 13);
			this.LabelControl11.TabIndex = 0;
			this.LabelControl11.Text = "And there are no disbursement notes";
			//
			//GroupControl2
			//
			this.GroupControl2.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GroupControl2.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.GroupControl2.Appearance.BackColor2 = System.Drawing.Color.PaleGreen;
			this.GroupControl2.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.GroupControl2.Appearance.Options.UseBackColor = true;
			this.GroupControl2.Appearance.Options.UseForeColor = true;
			this.GroupControl2.AppearanceCaption.BackColor = System.Drawing.Color.Transparent;
			this.GroupControl2.AppearanceCaption.BackColor2 = System.Drawing.Color.Transparent;
			this.GroupControl2.AppearanceCaption.Options.UseBackColor = true;
			this.GroupControl2.Controls.Add(this.normal_yes);
			this.GroupControl2.Controls.Add(this.LabelControl9);
			this.GroupControl2.Controls.Add(this.normal_no);
			this.GroupControl2.Controls.Add(this.LabelControl8);
			this.GroupControl2.Location = new System.Drawing.Point(12, 145);
			this.GroupControl2.Name = "GroupControl2";
			this.GroupControl2.Size = new System.Drawing.Size(440, 72);
			this.GroupControl2.TabIndex = 9;
			this.GroupControl2.Text = "If the client paid the normal amount";
			//
			//normal_yes
			//
			this.normal_yes.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.normal_yes.Location = new System.Drawing.Point(208, 48);
			this.normal_yes.Name = "normal_yes";
			this.normal_yes.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.normal_yes.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.normal_yes.Size = new System.Drawing.Size(224, 20);
			this.normal_yes.TabIndex = 3;
			//
			//LabelControl9
			//
			this.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Red;
			this.LabelControl9.Appearance.Options.UseForeColor = true;
			this.LabelControl9.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl9.Location = new System.Drawing.Point(24, 52);
			this.LabelControl9.Name = "LabelControl9";
			this.LabelControl9.Size = new System.Drawing.Size(164, 13);
			this.LabelControl9.TabIndex = 2;
			this.LabelControl9.Text = "And there are disbursement notes";
			//
			//normal_no
			//
			this.normal_no.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.normal_no.Location = new System.Drawing.Point(208, 24);
			this.normal_no.Name = "normal_no";
			this.normal_no.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.normal_no.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.normal_no.Size = new System.Drawing.Size(224, 20);
			this.normal_no.TabIndex = 1;
			//
			//LabelControl8
			//
			this.LabelControl8.Appearance.ForeColor = System.Drawing.Color.Green;
			this.LabelControl8.Appearance.Options.UseForeColor = true;
			this.LabelControl8.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl8.Location = new System.Drawing.Point(24, 28);
			this.LabelControl8.Name = "LabelControl8";
			this.LabelControl8.Size = new System.Drawing.Size(179, 13);
			this.LabelControl8.TabIndex = 0;
			this.LabelControl8.Text = "And there are no disbursement notes";
			//
			//GroupControl1
			//
			this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GroupControl1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.GroupControl1.Appearance.Options.UseForeColor = true;
			this.GroupControl1.Controls.Add(this.lbl_total_count);
			this.GroupControl1.Controls.Add(this.ProgressBarControl1);
			this.GroupControl1.Controls.Add(this.lbl_amount);
			this.GroupControl1.Controls.Add(this.lbl_count);
			this.GroupControl1.Controls.Add(this.lbl_client);
			this.GroupControl1.Controls.Add(this.LabelControl4);
			this.GroupControl1.Controls.Add(this.LabelControl3);
			this.GroupControl1.Controls.Add(this.LabelControl2);
			this.GroupControl1.Location = new System.Drawing.Point(12, 33);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(440, 104);
			this.GroupControl1.TabIndex = 8;
			this.GroupControl1.Text = "Disbursement Identification";
			//
			//lbl_total_count
			//
			this.lbl_total_count.Appearance.Options.UseTextOptions = true;
			this.lbl_total_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.lbl_total_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_total_count.Location = new System.Drawing.Point(278, 40);
			this.lbl_total_count.Name = "lbl_total_count";
			this.lbl_total_count.Size = new System.Drawing.Size(154, 13);
			this.lbl_total_count.TabIndex = 7;
			this.lbl_total_count.Text = "out of 0,000,000,000";
			this.lbl_total_count.UseMnemonic = false;
			//
			//ProgressBarControl1
			//
			this.ProgressBarControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.ProgressBarControl1.Location = new System.Drawing.Point(2, 78);
			this.ProgressBarControl1.Name = "ProgressBarControl1";
			this.ProgressBarControl1.Properties.DisplayFormat.FormatString = "P0";
			this.ProgressBarControl1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ProgressBarControl1.Properties.EditFormat.FormatString = "P0";
			this.ProgressBarControl1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ProgressBarControl1.Size = new System.Drawing.Size(436, 24);
			this.ProgressBarControl1.TabIndex = 6;
			//
			//lbl_amount
			//
			this.lbl_amount.Appearance.Options.UseTextOptions = true;
			this.lbl_amount.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_amount.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_amount.Location = new System.Drawing.Point(200, 56);
			this.lbl_amount.Name = "lbl_amount";
			this.lbl_amount.Size = new System.Drawing.Size(72, 13);
			this.lbl_amount.TabIndex = 5;
			this.lbl_amount.Text = "$0,000,000.00";
			this.lbl_amount.UseMnemonic = false;
			//
			//lbl_count
			//
			this.lbl_count.Appearance.Options.UseTextOptions = true;
			this.lbl_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_count.Location = new System.Drawing.Point(200, 40);
			this.lbl_count.Name = "lbl_count";
			this.lbl_count.Size = new System.Drawing.Size(72, 13);
			this.lbl_count.TabIndex = 4;
			this.lbl_count.Text = "0,000,000,000";
			this.lbl_count.UseMnemonic = false;
			//
			//lbl_client
			//
			this.lbl_client.Appearance.Options.UseTextOptions = true;
			this.lbl_client.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_client.Location = new System.Drawing.Point(200, 24);
			this.lbl_client.Name = "lbl_client";
			this.lbl_client.Size = new System.Drawing.Size(72, 13);
			this.lbl_client.TabIndex = 3;
			this.lbl_client.Text = "0000000";
			this.lbl_client.UseMnemonic = false;
			//
			//LabelControl4
			//
			this.LabelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl4.Location = new System.Drawing.Point(48, 56);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(121, 13);
			this.LabelControl4.TabIndex = 2;
			this.LabelControl4.Text = "Dollar Amount Disbursed:";
			this.LabelControl4.UseMnemonic = false;
			//
			//LabelControl3
			//
			this.LabelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl3.Location = new System.Drawing.Point(48, 40);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(139, 13);
			this.LabelControl3.TabIndex = 1;
			this.LabelControl3.Text = "Number of clients processed:";
			this.LabelControl3.UseMnemonic = false;
			//
			//LabelControl2
			//
			this.LabelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Horizontal;
			this.LabelControl2.Location = new System.Drawing.Point(48, 24);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(131, 13);
			this.LabelControl2.TabIndex = 0;
			this.LabelControl2.Text = "Currently processing client:";
			this.LabelControl2.UseMnemonic = false;
			//
			//LabelControl1
			//
			this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl1.Appearance.Font = new System.Drawing.Font("Comic Sans MS", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.LabelControl1.Appearance.Options.UseFont = true;
			this.LabelControl1.Appearance.Options.UseTextOptions = true;
			this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl1.Location = new System.Drawing.Point(14, 8);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(135, 19);
			this.LabelControl1.TabIndex = 7;
			this.LabelControl1.Text = "Disbursement Process";
			//
			//ProcessControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.Button_Cancel);
			this.Controls.Add(this.Button_Start);
			this.Controls.Add(this.GroupControl4);
			this.Controls.Add(this.GroupControl3);
			this.Controls.Add(this.GroupControl2);
			this.Controls.Add(this.GroupControl1);
			this.Controls.Add(this.LabelControl1);
			this.Name = "ProcessControl";
			this.Size = new System.Drawing.Size(464, 424);
			((System.ComponentModel.ISupportInitialize)this.GroupControl4).EndInit();
			this.GroupControl4.ResumeLayout(false);
			this.GroupControl4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.less_yes.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.less_no.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl3).EndInit();
			this.GroupControl3.ResumeLayout(false);
			this.GroupControl3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.more_yes.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.more_no.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
			this.GroupControl2.ResumeLayout(false);
			this.GroupControl2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.normal_yes.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.normal_no.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.SimpleButton Button_Cancel;
		protected internal DevExpress.XtraEditors.SimpleButton Button_Start;
		protected internal DevExpress.XtraEditors.GroupControl GroupControl4;
		protected internal DevExpress.XtraEditors.ComboBoxEdit less_yes;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl12;
		protected internal DevExpress.XtraEditors.ComboBoxEdit less_no;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl13;
		protected internal DevExpress.XtraEditors.GroupControl GroupControl3;
		protected internal DevExpress.XtraEditors.ComboBoxEdit more_yes;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl10;
		protected internal DevExpress.XtraEditors.ComboBoxEdit more_no;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl11;
		protected internal DevExpress.XtraEditors.GroupControl GroupControl2;
		protected internal DevExpress.XtraEditors.ComboBoxEdit normal_yes;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl9;
		protected internal DevExpress.XtraEditors.ComboBoxEdit normal_no;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl8;
		protected internal DevExpress.XtraEditors.GroupControl GroupControl1;
		protected internal DevExpress.XtraEditors.ProgressBarControl ProgressBarControl1;
		protected internal DevExpress.XtraEditors.LabelControl lbl_amount;
		protected internal DevExpress.XtraEditors.LabelControl lbl_count;
		protected internal DevExpress.XtraEditors.LabelControl lbl_client;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl4;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.LabelControl lbl_total_count;
	}
}
