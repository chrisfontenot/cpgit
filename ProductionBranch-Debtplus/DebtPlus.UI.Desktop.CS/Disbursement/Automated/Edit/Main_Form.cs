using System;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Edit
{
    internal partial class Main_Form : DebtPlus.UI.Desktop.CS.Disbursement.Common.ISupportContext
    {
        public Main_Form() : base()
        {
            InitializeComponent();
        }

        public global::DebtPlus.UI.Desktop.CS.Disbursement.Common.IDisbursementContext Context { get; set; }

        public Main_Form(DisbursementAutoEditContext Context) : base()
        {
            this.Context = Context;
            InitializeComponent();

            DisbursementRegisterControl1.Selected += DisbursementRegisterControl1_Selected;
            Load += Form1_Load;
            Panel_Process1.Cancelled += Panel_process1_Cancel;
            DisbursementRegisterControl1.Cancelled += DisbursementRegisterControl1_Cancelled;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Reload the placement and size
            LoadPlacement("Disbursement.Auto.Edit");

            if (((ArgParser)Context.ap).BatchID <= 0)
            {
                PanelControl1.Visible = true;
                PanelControl1.TabStop = true;
                PanelControl1.BringToFront();

                DisbursementRegisterControl1.ReadForm();
            }
            else
            {
                ProcessBatch();
            }
        }

        private void Panel_process1_Cancel(object sender, EventArgs e)
        {
            Panel_Process1.Visible = false;
            Panel_Process1.TabStop = false;
            Panel_Process1.SendToBack();

            PanelControl1.Visible = true;
            PanelControl1.TabStop = true;
            PanelControl1.BringToFront();

            DisbursementRegisterControl1.ReadForm();
        }

        private void DisbursementRegisterControl1_Cancelled(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void DisbursementRegisterControl1_Selected(object sender, EventArgs e)
        {
            ((ArgParser)Context.ap).BatchID = DisbursementRegisterControl1.BatchID;
            ProcessBatch();
        }

        private void ProcessBatch()
        {
            PanelControl1.Visible = false;
            PanelControl1.TabStop = false;
            PanelControl1.SendToBack();

            Panel_Process1.Visible = true;
            Panel_Process1.TabStop = true;
            Panel_Process1.BringToFront();
            Panel_Process1.ReadForm();
        }
    }
}