using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.FormLib.Disbursement;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Edit
{
	partial class Main_Form : DebtPlus.Data.Forms.DebtPlusForm
	{

		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.DisbursementRegisterControl1 = new DisbursementRegisterControl();
			this.Panel_Process1 = new Panel_Process();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).BeginInit();
			this.PanelControl1.SuspendLayout();
			this.SuspendLayout();
			//
			//PanelControl1
			//
			this.PanelControl1.Controls.Add(this.LabelControl1);
			this.PanelControl1.Controls.Add(this.DisbursementRegisterControl1);
			this.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.PanelControl1.Location = new System.Drawing.Point(0, 0);
			this.PanelControl1.Name = "PanelControl1";
			this.PanelControl1.Size = new System.Drawing.Size(497, 331);
			this.ToolTipController1.SetSuperTip(this.PanelControl1, null);
			this.PanelControl1.TabIndex = 0;
			//
			//LabelControl1
			//
			this.LabelControl1.Appearance.Options.UseTextOptions = true;
			this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl1.Location = new System.Drawing.Point(12, 0);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Padding = new System.Windows.Forms.Padding(4);
			this.LabelControl1.Size = new System.Drawing.Size(472, 29);
			this.LabelControl1.TabIndex = 1;
			this.LabelControl1.Text = "Please choose the disbursement batch that you wish to update or press CANCEL to t" + "erminate";
			//
			//DisbursementRegisterControl1
			//
			this.DisbursementRegisterControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.DisbursementRegisterControl1.Location = new System.Drawing.Point(6, 33);
			this.DisbursementRegisterControl1.Name = "DisbursementRegisterControl1";
			this.DisbursementRegisterControl1.Size = new System.Drawing.Size(486, 286);
			this.ToolTipController1.SetSuperTip(this.DisbursementRegisterControl1, null);
			this.DisbursementRegisterControl1.TabIndex = 0;
			//
			//Panel_Process1
			//
			this.Panel_Process1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Panel_Process1.Location = new System.Drawing.Point(0, 0);
			this.Panel_Process1.Name = "Panel_Process1";
			this.Panel_Process1.Size = new System.Drawing.Size(497, 331);
			this.ToolTipController1.SetSuperTip(this.Panel_Process1, null);
			this.Panel_Process1.TabIndex = 1;
			//
			//Form1
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(497, 331);
			this.Controls.Add(this.Panel_Process1);
			this.Controls.Add(this.PanelControl1);
			this.Name = "Form1";
			this.ToolTipController1.SetSuperTip(this, null);
			this.Text = "Disbursement Review";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).EndInit();
			this.PanelControl1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.PanelControl PanelControl1;
		protected internal DisbursementRegisterControl DisbursementRegisterControl1;
		protected internal Panel_Process Panel_Process1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
	}
}
