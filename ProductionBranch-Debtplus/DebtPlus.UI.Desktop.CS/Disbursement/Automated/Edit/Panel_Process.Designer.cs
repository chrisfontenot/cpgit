using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Edit
{
	partial class Panel_Process : DevExpress.XtraEditors.XtraUserControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing) {
					if (components != null)
						components.Dispose();
					ds.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
			this.GridColumn_ending_trust = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_ending_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_disbursed = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_disbursed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_client = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_client_name = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_client_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_held_in_trust = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_held_in_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_reserved_in_trust = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_reserved_in_trust.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_disbursement_factor = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_note_count = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_note_count.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_client_register = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_client_register.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_active_status = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_active_status.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_start_date = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_start_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_region = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_region.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_created_by = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_created_by.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_date_created = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_date_created.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_office_name = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_office_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_counselor_name = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_counselor_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.SimpleButton_Select = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_Cancel = new DevExpress.XtraEditors.SimpleButton();
			this.ComboBoxEdit_filter = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_filter.Properties).BeginInit();
			this.SuspendLayout();
			//
			//GridColumn_ending_trust
			//
			this.GridColumn_ending_trust.Caption = "Ending Trust";
			this.GridColumn_ending_trust.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_ending_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_ending_trust.FieldName = "ending_trust";
			this.GridColumn_ending_trust.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_ending_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_ending_trust.Name = "GridColumn_ending_trust";
			//
			//GridColumn_disbursed
			//
			this.GridColumn_disbursed.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_disbursed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_disbursed.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_disbursed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_disbursed.Caption = "Disbursed";
			this.GridColumn_disbursed.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_disbursed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_disbursed.FieldName = "disbursed";
			this.GridColumn_disbursed.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_disbursed.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_disbursed.Name = "GridColumn_disbursed";
			this.GridColumn_disbursed.Visible = true;
			this.GridColumn_disbursed.VisibleIndex = 4;
			//
			//LabelControl1
			//
			this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl1.Appearance.Options.UseTextOptions = true;
			this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
			this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl1.Location = new System.Drawing.Point(4, 3);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(482, 39);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "This is a list of the clients involved in the disbursement for this batch. Double" + "-click on a client or click on the SELECT button to the right to edit the client" + " information.";
			this.LabelControl1.UseMnemonic = false;
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.Location = new System.Drawing.Point(4, 49);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.Size = new System.Drawing.Size(400, 166);
			this.GridControl1.TabIndex = 1;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
			this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.Empty.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
			this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
			this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
			this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
			this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
			this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
			this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
			this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.Preview.Options.UseFont = true;
			this.GridView1.Appearance.Preview.Options.UseForeColor = true;
			this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.Row.Options.UseBackColor = true;
			this.GridView1.Appearance.Row.Options.UseForeColor = true;
			this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
			this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_client,
				this.GridColumn_client_name,
				this.GridColumn_held_in_trust,
				this.GridColumn_ending_trust,
				this.GridColumn_reserved_in_trust,
				this.GridColumn_disbursement_factor,
				this.GridColumn_note_count,
				this.GridColumn_client_register,
				this.GridColumn_active_status,
				this.GridColumn_start_date,
				this.GridColumn_region,
				this.GridColumn_disbursed,
				this.GridColumn_created_by,
				this.GridColumn_date_created,
				this.GridColumn_office_name,
				this.GridColumn_counselor_name
			});
			StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(255), (Int32)Convert.ToByte(192));
			StyleFormatCondition1.Appearance.Options.UseBackColor = true;
			StyleFormatCondition1.ApplyToRow = true;
			StyleFormatCondition1.Column = this.GridColumn_ending_trust;
			StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Greater;
			StyleFormatCondition1.Value1 = new decimal(new Int32[] {
				0,
				0,
				0,
				0
			});
			this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] { StyleFormatCondition1 });
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			this.GridView1.SortInfo.Add(GridColumn_client, DevExpress.Data.ColumnSortOrder.Ascending);
			//
			//GridColumn_client
			//
			this.GridColumn_client.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client.Caption = "Client";
			this.GridColumn_client.DisplayFormat.FormatString = "f0";
			this.GridColumn_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.GridColumn_client.FieldName = "client";
			this.GridColumn_client.GroupFormat.FormatString = "f0";
			this.GridColumn_client.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.GridColumn_client.Name = "GridColumn_client";
			this.GridColumn_client.Visible = true;
			this.GridColumn_client.VisibleIndex = 0;
			this.GridColumn_client.SortOrder = DevExpress.Data.ColumnSortOrder.Ascending;
			//
			//GridColumn_client_name
			//
			this.GridColumn_client_name.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_client_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_client_name.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_client_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_client_name.Caption = "Name";
			this.GridColumn_client_name.FieldName = "client_name";
			this.GridColumn_client_name.Name = "GridColumn_client_name";
			this.GridColumn_client_name.Visible = true;
			this.GridColumn_client_name.VisibleIndex = 1;
			//
			//GridColumn_held_in_trust
			//
			this.GridColumn_held_in_trust.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_held_in_trust.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_held_in_trust.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_held_in_trust.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_held_in_trust.Caption = "Starting Trust";
			this.GridColumn_held_in_trust.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_held_in_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_held_in_trust.FieldName = "starting_trust";
			this.GridColumn_held_in_trust.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_held_in_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_held_in_trust.Name = "GridColumn_held_in_trust";
			this.GridColumn_held_in_trust.Visible = true;
			this.GridColumn_held_in_trust.VisibleIndex = 2;
			//
			//GridColumn_reserved_in_trust
			//
			this.GridColumn_reserved_in_trust.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_reserved_in_trust.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_reserved_in_trust.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_reserved_in_trust.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_reserved_in_trust.Caption = "Reserved $";
			this.GridColumn_reserved_in_trust.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_reserved_in_trust.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_reserved_in_trust.FieldName = "reserved_in_trust";
			this.GridColumn_reserved_in_trust.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_reserved_in_trust.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_reserved_in_trust.Name = "GridColumn_reserved_in_trust";
			//
			//GridColumn_disbursement_factor
			//
			this.GridColumn_disbursement_factor.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_disbursement_factor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_disbursement_factor.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_disbursement_factor.Caption = "Disbursement";
			this.GridColumn_disbursement_factor.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_disbursement_factor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_disbursement_factor.FieldName = "disbursement_factor";
			this.GridColumn_disbursement_factor.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_disbursement_factor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_disbursement_factor.Name = "GridColumn_disbursement_factor";
			this.GridColumn_disbursement_factor.Visible = true;
			this.GridColumn_disbursement_factor.VisibleIndex = 3;
			//
			//GridColumn_note_count
			//
			this.GridColumn_note_count.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_note_count.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_note_count.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_note_count.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_note_count.Caption = "Notes";
			this.GridColumn_note_count.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_note_count.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_note_count.FieldName = "note_count";
			this.GridColumn_note_count.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_note_count.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_note_count.Name = "GridColumn_note_count";
			//
			//GridColumn_client_register
			//
			this.GridColumn_client_register.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_client_register.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client_register.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_client_register.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_client_register.Caption = "ClientRegister";
			this.GridColumn_client_register.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_client_register.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client_register.FieldName = "client_register";
			this.GridColumn_client_register.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_client_register.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_client_register.Name = "GridColumn_client_register";
			//
			//GridColumn_active_status
			//
			this.GridColumn_active_status.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_active_status.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_active_status.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_active_status.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_active_status.Caption = "Active Status";
			this.GridColumn_active_status.FieldName = "active_status";
			this.GridColumn_active_status.Name = "GridColumn_active_status";
			//
			//GridColumn_start_date
			//
			this.GridColumn_start_date.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_start_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_start_date.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_start_date.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_start_date.Caption = "Start Date";
			this.GridColumn_start_date.DisplayFormat.FormatString = "d";
			this.GridColumn_start_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_start_date.FieldName = "start_date";
			this.GridColumn_start_date.GroupFormat.FormatString = "d";
			this.GridColumn_start_date.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_start_date.Name = "GridColumn_start_date";
			//
			//GridColumn_region
			//
			this.GridColumn_region.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_region.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_region.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_region.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_region.Caption = "Region";
			this.GridColumn_region.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_region.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_region.FieldName = "region";
			this.GridColumn_region.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_region.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_region.Name = "GridColumn_region";
			//
			//GridColumn_created_by
			//
			this.GridColumn_created_by.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_created_by.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_created_by.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_created_by.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_created_by.Caption = "Disbursed By";
			this.GridColumn_created_by.FieldName = "created_by";
			this.GridColumn_created_by.Name = "GridColumn_created_by";
			//
			//GridColumn_date_created
			//
			this.GridColumn_date_created.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_date_created.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_created.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_date_created.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_date_created.Caption = "Disbused Time";
			this.GridColumn_date_created.DisplayFormat.FormatString = "d";
			this.GridColumn_date_created.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_created.FieldName = "date_created";
			this.GridColumn_date_created.GroupFormat.FormatString = "d";
			this.GridColumn_date_created.GroupFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_date_created.Name = "GridColumn_date_created";
			//
			//GridColumn_office_name
			//
			this.GridColumn_office_name.Caption = "Office";
			this.GridColumn_office_name.FieldName = "office_name";
			this.GridColumn_office_name.Name = "GridColumn_office_name";
			//
			//GridColumn_counselor_name
			//
			this.GridColumn_counselor_name.Caption = "Counselor";
			this.GridColumn_counselor_name.FieldName = "counselor_name";
			this.GridColumn_counselor_name.Name = "GridColumn_counselor_name";
			//
			//SimpleButton_Select
			//
			this.SimpleButton_Select.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_Select.Location = new System.Drawing.Point(411, 49);
			this.SimpleButton_Select.Name = "SimpleButton_Select";
			this.SimpleButton_Select.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Select.TabIndex = 2;
			this.SimpleButton_Select.Text = "&Select...";
			//
			//SimpleButton_Cancel
			//
			this.SimpleButton_Cancel.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_Cancel.Location = new System.Drawing.Point(410, 78);
			this.SimpleButton_Cancel.Name = "SimpleButton_Cancel";
			this.SimpleButton_Cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Cancel.TabIndex = 3;
			this.SimpleButton_Cancel.Text = "&Cancel";
			//
			//ComboBoxEdit_filter
			//
			this.ComboBoxEdit_filter.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.ComboBoxEdit_filter.Location = new System.Drawing.Point(34, 221);
			this.ComboBoxEdit_filter.Name = "ComboBoxEdit_filter";
			this.ComboBoxEdit_filter.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit_filter.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.ComboBoxEdit_filter.Size = new System.Drawing.Size(370, 20);
			this.ComboBoxEdit_filter.TabIndex = 4;
			//
			//LabelControl2
			//
			this.LabelControl2.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.LabelControl2.Location = new System.Drawing.Point(4, 224);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(24, 13);
			this.LabelControl2.TabIndex = 5;
			this.LabelControl2.Text = "Filter";
			//
			//Panel_Process
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.LabelControl2);
			this.Controls.Add(this.ComboBoxEdit_filter);
			this.Controls.Add(this.SimpleButton_Cancel);
			this.Controls.Add(this.SimpleButton_Select);
			this.Controls.Add(this.GridControl1);
			this.Controls.Add(this.LabelControl1);
			this.Name = "Panel_Process";
			this.Size = new System.Drawing.Size(497, 244);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_filter.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Select;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Cancel;
		protected internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_filter;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_held_in_trust;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_reserved_in_trust;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_disbursement_factor;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_note_count;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_register;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_active_status;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_start_date;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_region;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_client_name;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_disbursed;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_created_by;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_date_created;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl2;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_office_name;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_counselor_name;

		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ending_trust;
	}
}
