using System;
using System.Data;
using System.Diagnostics;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Desktop.CS.Disbursement.Common;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Edit
{
    internal partial class ReviewClientClass : ClientClass
    {
        public ReviewClientClass() : base(null)
        {
        }

        // Tripped at the end of the update to refresh the row in the main display
        public event EventHandler RefreshRow;

        protected internal DataRowView drv = null;

        /// <summary>
        /// Initialize the class instance
        /// </summary>
        /// <param name="Context">Pointer to the global context information</param>
        /// <remarks></remarks>
        public ReviewClientClass(IDisbursementContext Context) : base(Context)
        {
        }

        /// <summary>
        /// Initialize the class instance
        /// </summary>
        /// <param name="Context">Pointer to the global context information</param>
        /// <param name="drv">Row that represents the client in the disbursement</param>
        /// <remarks></remarks>
        public ReviewClientClass(IDisbursementContext Context, DataRowView drv) : this(Context)
        {
            this.drv = drv;
            ClientROW = drv.Row;
        }

        /// <summary>
        /// Thread procedure to do the review of the client
        /// </summary>
        /// <remarks></remarks>

        public void ReviewDisbursement()
        {
            // Build the list of debts in the separate thread for appearance performance enhancment
            DebtList = new DisbursementDebtList();
            DebtList.ReadDebtsTable(Convert.ToInt32(drv["client"]), ((ArgParser)Context.ap).BatchID);
            // $$$ EDIT $$$ MyBase.RecalulateMonthlyFee()

            // Conduct the dialog and tell the parent that we have completed the form.

            if (Do_Examine() == DialogResult.OK)
            {
                // Empty the information before we do the update
                drv["client_register"] = DBNull.Value;
                drv["disbursed"] = 0m;
                drv["created_by"] = DBNull.Value;
                drv["date_created"] = DBNull.Value;

                // Update the current row with the disbursement information once we have done the operation
                SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                object ClientRegister = null;
                SqlDataReader rd = null;
                try
                {
                    cn.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with1 = cmd;
                        _with1.Connection = cn;
                        _with1.CommandText = "SELECT client_register FROM disbursement_clients WHERE client = @client AND disbursement_register = @disbursement_register";
                        _with1.CommandType = CommandType.Text;
                        _with1.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
                        _with1.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                        ClientRegister = _with1.ExecuteScalar();
                    }
                    drv["client_register"] = ClientRegister;

                    // From the client register, get the disbursed dollar amount and the various other information
                    if (ClientRegister != null && !object.ReferenceEquals(ClientRegister, DBNull.Value))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            var _with2 = cmd;
                            _with2.Connection = cn;
                            _with2.CommandText = "SELECT debit_amt, created_by, date_created FROM registers_client WHERE client_register = @client_register";
                            _with2.CommandType = CommandType.Text;
                            _with2.Parameters.Add("@client_register", SqlDbType.Int).Value = ClientRegister;
                            rd = _with2.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);
                        }

                        // Read the field information
                        if (rd != null && rd.Read())
                        {
                            drv["disbursed"] = rd.GetValue(0);
                            drv["created_by"] = rd.GetValue(1);
                            drv["date_created"] = rd.GetValue(2);
                        }
                    }
                }
                catch (SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading disbursement information");
                }
                finally
                {
                    if (cn != null)
                        cn.Dispose();
                }

                // Refresh the form with the new information
                if (RefreshRow != null)
                {
                    RefreshRow(this, EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// Handle the EXAMINE PREVIOUS button event differently in the review
        /// </summary>
        /// <remarks></remarks>
        public override void Do_ExaminePrevious()
        {
            Debug.Assert(false, "ExaminePrevious called when it has no previous pointer");
        }

        /// <summary>
        /// Handle the QUIT button event differently in the review
        /// </summary>
        /// <remarks></remarks>
        public override void Do_Quit()
        {
            CurrentForm.DialogResult = DialogResult.Cancel;
        }

        /// <summary>
        /// Process the EXAMINE button event
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public override DialogResult Do_Examine()
        {
            CurrentForm = new Examine_Form(Context, this);
            try
            {
                // For the review operation we want the QUIT button to be CANCEL and there is no
                // need for a PREVIOUS button since there is no concept of a PREVIOUS client.
                CurrentForm.ButtonQuit.Text = "&Cancel";
                CurrentForm.ButtonPrevious.Visible = false;

                // Do the standard logic from here.
                return CurrentForm.ShowDialog();
            }
            finally
            {
                CurrentForm.Dispose();
                CurrentForm = null;
            }
        }
    }
}