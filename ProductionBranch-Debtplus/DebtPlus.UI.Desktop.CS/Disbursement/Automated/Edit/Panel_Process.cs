using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraEditors;
using DevExpress.Utils;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using System.Threading;
using System.IO;
using System.Windows.Forms;
using System.Drawing;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Edit
{
    internal partial class Panel_Process
    {
        /// <summary>
        /// Event raised when the CANCEL button is pressed
        /// </summary>
        /// <remarks></remarks>
        public event EventHandler Cancelled;

        private DataRowView FocusedDRV = null;
        /// <summary>
        /// Dataset for the local use
        /// </summary>
        /// <remarks></remarks>

        private DataSet ds = new DataSet("ds");

        /// <summary>
        /// Context (static) information
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        protected Disbursement.Common.IDisbursementContext context
        {
            get { return ((Disbursement.Common.ISupportContext)ParentForm).Context; }
            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Initialize the class
        /// </summary>
        /// <remarks></remarks>
        public Panel_Process() : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        /// <summary>
        /// Register the events
        /// </summary>
        /// <remarks></remarks>
        private void RegisterHandlers()
        {
            Load += Control_Load;
            SimpleButton_Select.Click += SimpleButton_Select_Click;
            SimpleButton_Cancel.Click += SimpleButton_Cancel_Click;
            GridView1.FilterEditorCreated += GridView1_FilterEditorCreated;
            GridView1.DoubleClick += GridView1_DoubleClick;
            GridView1.FocusedRowChanged += GridView1_FocusedRowChanged;
            GridView1.Layout += LayoutChanged;
            ComboBoxEdit_filter.SelectedIndexChanged += ComboBoxEdit_filter_SelectedIndexChanged;
        }

        /// <summary>
        /// Remove the event registration
        /// </summary>
        /// <remarks></remarks>
        private void UnRegisterHandlers()
        {
            Load -= Control_Load;
            SimpleButton_Cancel.Click -= SimpleButton_Cancel_Click;
            SimpleButton_Select.Click -= SimpleButton_Select_Click;
            GridView1.FilterEditorCreated -= GridView1_FilterEditorCreated;
            GridView1.DoubleClick -= GridView1_DoubleClick;
            GridView1.FocusedRowChanged -= GridView1_FocusedRowChanged;
            GridView1.Layout -= LayoutChanged;
            ComboBoxEdit_filter.SelectedIndexChanged -= ComboBoxEdit_filter_SelectedIndexChanged;
        }

        /// <summary>
        /// Process the load of the control. Here is where the initialization is performed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void Control_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();

            try
            {
                // Load the layout for the grid control
                string pathName = XMLBasePath();
                string fileName = Path.Combine(pathName, "Process.Grid.xml");

                try
                {
                    if (File.Exists(fileName))
                    {
                        GridView1.RestoreLayoutFromXml(fileName);
                    }
                }
#pragma warning disable 168
                catch (DirectoryNotFoundException ex)
                {
                }
                catch (FileNotFoundException ex)
                {
                }
#pragma warning restore 168

                var _with1 = GridColumn_client;
                var _with2 = _with1.DisplayFormat;
                _with2.Format = new global::DebtPlus.Utils.Format.Client.CustomFormatter();

                var _with3 = _with1.GroupFormat;
                _with3.Format = new global::DebtPlus.Utils.Format.Client.CustomFormatter();

                var _with4 = ComboBoxEdit_filter;
                var _with5 = _with4.Properties;
                var _with6 = _with5.Items;
                _with6.Clear();
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem("All clients in the batch", ""));
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem("Only disbursed clients in the batch", "[disbursed]>0.0"));
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem("Only clients who were NOT disbursed in the batch", "[disbursed]=0.0"));
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem("Has trust but not disbursed", "[held_in_trust]>0.0 AND [disbursed]=0.0"));
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem("Not Disbused but with disbursement notes", "[disbursed]=0.0 AND [note_count]>0"));
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem("Disbursement Factor differs from Amount Disbursed", "[disbursed]<>[disbursement_factor]"));
                _with6.Add(new DebtPlus.Data.Controls.ComboboxItem("Did not disburse whole trust amount", "[disbursed]<>[held_in_trust]"));
                _with4.SelectedIndex = 0;
            }
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Process the CANCEL button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void SimpleButton_Cancel_Click(object sender, EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Read the list of clients disbursed in the batch
        /// </summary>
        /// <remarks></remarks>
        public void ReadForm()
        {
            UnRegisterHandlers();
            Cursor current_cursor = Cursor.Current;

            // Put a big notice over the form that we are reading the data for the time being
            try
            {
                using (LabelControl lbl = new LabelControl())
                {
                    var _with7 = lbl;
                    _with7.Location = GridControl1.Location;
                    _with7.Size = GridControl1.Size;
                    _with7.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
                    _with7.AutoSizeMode = LabelAutoSizeMode.None;
                    _with7.AutoEllipsis = false;
                    _with7.UseMnemonic = false;
                    _with7.Cursor = Cursors.WaitCursor;

                    // Set the color
                    _with7.BackColor = Color.FromKnownColor(KnownColor.Window);
                    _with7.ForeColor = Color.FromKnownColor(KnownColor.WindowText);
                    _with7.Appearance.BackColor = _with7.BackColor;
                    _with7.Appearance.ForeColor = _with7.ForeColor;

                    var _with8 = _with7.Appearance;

                    // Force the text to the middle of the area
                    var _with9 = _with8.TextOptions;
                    _with9.HAlignment = HorzAlignment.Near;
                    _with9.VAlignment = VertAlignment.Center;
                    _with9.WordWrap = WordWrap.Wrap;

                    var _with10 = _with8.Options;
                    _with10.UseTextOptions = true;
                    _with10.UseBackColor = true;
                    _with10.UseForeColor = true;

                    _with7.Font = new Font(_with7.Font, FontStyle.Bold);
                    _with7.Padding = new Padding(20);
                    _with7.Text = "The system is currently reading in the list of clients that where placed into this disbursement batch. This may take a few minutes. It depends upon the number of clients in the batch. When the reading operation is completed this notice will go away and you will have a grid showing the clients. Until then please be patient. It is working as fast as possible. Thank you.";

                    // Add the item to the control and make it overlay the grid control
                    Controls.Add(lbl);
                    lbl.BringToFront();

                    // Do the painting operation for the controls
                    Application.DoEvents();
                    Cursor.Current = Cursors.WaitCursor;

                    using (SqlCommand cmd = new SqlCommand())
                    {
                        var _with11 = cmd;
                        _with11.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with11.CommandText = "SELECT v.disbursement_register, v.client, v.held_in_trust, v.reserved_in_trust, v.disbursement_factor, v.note_count, v.client_register, v.active_status, v.start_date, v.region, v.client_name, v.disbursed, v.created_by, v.date_created, dbo.format_normal_name(default,cox.first,cox.middle,cox.last,cox.suffix) as counselor_name, o.name as office_name FROM [view_disbursement_review] v WITH (NOLOCK) INNER JOIN clients c ON v.client = c.client LEFT OUTER JOIN offices o ON c.office = o.office LEFT OUTER JOIN counselors co ON c.counselor = co.counselor LEFT OUTER JOIN names cox WITH (NOLOCK) ON co.NameID = cox.Name WHERE disbursement_register=@disbursement_register";
                        _with11.CommandType = CommandType.Text;
                        _with11.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = ((ArgParser)context.ap).BatchID;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            ds.Clear();
                            da.Fill(ds, "view_disbursement_review");
                        }
                    }

                    DataTable tbl = ds.Tables["view_disbursement_review"];
                    var _with12 = tbl;
                    if (_with12.PrimaryKey.GetUpperBound(0) < 0)
                        _with12.PrimaryKey = new DataColumn[] { _with12.Columns["client_creditor"] };
                    if (!_with12.Columns.Contains("starting_trust"))
                        _with12.Columns.Add("starting_trust", typeof(decimal), "[held_in_trust]");
                    if (!_with12.Columns.Contains("ending_trust"))
                        _with12.Columns.Add("ending_trust", typeof(decimal), "[held_in_trust]-[disbursed]");

                    // Update the grid information
                    var _with13 = GridControl1;
                    _with13.DataSource = tbl.DefaultView;
                    _with13.RefreshDataSource();

                    // Put in the selected filter
                    AdjustFilter();

                    // Remove the label control once we have the data
                    lbl.SendToBack();
                    Controls.Remove(lbl);
                }
            }
            finally
            {
                Cursor.Current = current_cursor;
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Set the filter into the grid
        /// </summary>
        /// <remarks></remarks>

        private void AdjustFilter()
        {
            var _with14 = GridView1;
            _with14.BeginUpdate();
            // Suspend the update of the grid for a moment
            _with14.ActiveFilter.Clear();
            // Remove the existing filter from the list

            // Set the filter to the expression
            if (CurrentFilterMode != string.Empty)
            {
                _with14.ActiveFilterString = CurrentFilterMode;
                _with14.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
            }

            _with14.EndUpdate();
            // Complete the update event
        }

        /// <summary>
        /// Find the current filter string
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        private string CurrentFilterMode
        {
            get
            {
                if (ComboBoxEdit_filter.SelectedIndex < 0)
                    return string.Empty;
                return Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_filter.SelectedItem).value);
            }
        }

        /// <summary>
        /// When the combobox changes, update the filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void ComboBoxEdit_filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            AdjustFilter();
        }

        /// <summary>
        /// Process an update on the filter for the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void GridView1_FilterEditorCreated(object sender, FilterControlEventArgs e)
        {
            GridView1.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Default;
        }

        /// <summary>
        /// Handle a double-click event on the grid view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        private void GridView1_DoubleClick(object sender, EventArgs e)
        {
            GridHitInfo hi = GridView1.CalcHitInfo((GridControl1.PointToClient(MousePosition)));

            if (hi.IsValid && (hi.InRow || hi.InRowCell))
            {
                // Find the row from the hit test routine
                Int32 controlRow = hi.RowHandle;

                // If there is a row then edit that trustRegister
                if (controlRow >= 0)
                {
                    DataView vue = (DataView)GridControl1.DataSource;
                    DataRowView drv = (DataRowView)GridView1.GetRow(controlRow);
                    EditClient(drv);
                }
            }
        }

        /// <summary>
        /// When the grid control focus trustRegister changes, enable the SELECT button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            FocusedDRV = null;
            if (!DesignMode)
            {
                Int32 controlRow = e.FocusedRowHandle;
                if (controlRow >= 0)
                {
                    DataView vue = (DataView)GridControl1.DataSource;
                    FocusedDRV = (DataRowView)GridView1.GetRow(controlRow);
                }
            }

            SimpleButton_Select.Enabled = FocusedDRV != null;
        }

        /// <summary>
        /// Handle the SELECT button click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void SimpleButton_Select_Click(object sender, EventArgs e)
        {
            EditClient(FocusedDRV);
            FocusedDRV = null;
            SimpleButton_Select.Enabled = false;
        }

        /// <summary>
        /// Edit the client row
        /// </summary>
        /// <param name="drv"></param>
        /// <remarks></remarks>

        private void EditClient(DataRowView drv)
        {
            // Launch a thread to handle the dialog
            ReviewClientClass reviewClass = new ReviewClientClass(context, drv);
            reviewClass.RefreshRow += RefreshRow;

            var _with15 = new Thread(reviewClass.ReviewDisbursement);
            _with15.SetApartmentState(ApartmentState.STA);
            _with15.Name = "Edit_Thread";
            _with15.IsBackground = true;
            _with15.Start();
        }

        /// <summary>
        /// Perform a refresh on the grid trustRegister (thread safe)
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void RefreshRow(object Sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventHandler(RefreshRow), new object[] {
                    Sender,
                    e
                });
            }
            else
            {
                GridView1.RefreshData();
            }
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        protected virtual string XMLBasePath()
        {
            string basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Path.DirectorySeparatorChar + "DebtPlus";
            return Path.Combine(basePath, "Disbursement.Auto.Edit");
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            string pathName = XMLBasePath();
            if (!Directory.Exists(pathName))
            {
                Directory.CreateDirectory(pathName);
            }

            string fileName = Path.Combine(pathName, "Process.Grid.xml");
            GridView1.SaveLayoutToXml(fileName);
        }
    }
}