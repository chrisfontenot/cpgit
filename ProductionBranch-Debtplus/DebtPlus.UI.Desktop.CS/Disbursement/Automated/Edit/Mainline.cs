#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Data;
using System.Data.SqlClient;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Edit
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ArgParser();
                    if (ap.Parse((string[])param))
                    {
                        using (var context = new DisbursementAutoEditContext())
                        {
                            context.ap = ap;
                            ThreadProcedure(context);
                        }
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "DisbursementEdit"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }

        /// <summary>
        /// Thread processing procedure for the disbursement.
        /// </summary>
        /// <param name="context">Pointer to the allocated processing context structure</param>
        private void ThreadProcedure(DisbursementAutoEditContext context)
        {
            var sqlInfo = DebtPlus.LINQ.SQLInfoClass.getDefault();
            using (var cn = new SqlConnection(sqlInfo.ConnectionString))
            {
                SqlTransaction txn = null;
                try
                {
                    cn.Open();
                    txn = cn.BeginTransaction(IsolationLevel.ReadCommitted, "AutoDisbursement");

                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = "SELECT TOP 1 current_status FROM disbursement_lock WITH (HOLDLOCK, ROWLOCK)";
                        cmd.CommandType = CommandType.Text;
                        using (var rd = cmd.ExecuteReader(CommandBehavior.SingleRow))
                        {
                            rd.Read();
                            rd.Close();
                        }
                    }

                    // Now, with the transaction still open, run the program. It will process the data
                    // and allow full operation. When the form terminates, we will return to complete
                    // the transaction and release the shared lock.
                    using (var mainForm = new Main_Form(context))
                    {
                        mainForm.ShowDialog();
                    }
                }

                finally
                {
                    // Commit the transaction before closing the connection
                    if (txn != null)
                    {
                        txn.Commit();
                        txn = null;
                    }
                }
            }
        }
    }
}