using System;
using System.Globalization;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Edit
{
    public partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        public Int32 BatchID { get; set; }
        public string SelectionClause { get; set; }

        public ArgParser() : base(new string[] { "b" })
        {
        }

        protected override SwitchStatus OnSwitch(string switchName, string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchName)
            {
                case "b":
                    Int32 IntTemp = default(Int32);
                    if (Int32.TryParse(switchValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out IntTemp))
                    {
                        if (IntTemp >= 0)
                        {
                            BatchID = IntTemp;
                            break; // TODO: might not be correct. Was : Exit Select
                        }
                    }
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;

                case "?":
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
                    break;

                default:
                    ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }

            return ss;
        }

        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}