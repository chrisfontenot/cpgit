#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Windows.Forms;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
    public class Mainline : IDesktopMainline
    {
        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ArgParser();
                    if (ap.Parse((string[])param))
                    {
                        ProcessOperation(ap);
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "DisbursementPost"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }

        /// <summary>
        /// Process the requested operation
        /// </summary>
        private void ProcessOperation(ArgParser ap)
        {
            // Find the number of open disbursement batches
            System.Int32 openClients = default(System.Int32);
            try
            {
                using (var bc = new DebtPlus.LINQ.BusinessContext())
                {
                    openClients = bc.xpr_DisbursementLockCount();
                }
            }
#pragma warning disable 168
            catch (System.Data.SqlClient.SqlException ex)
            {
                openClients = 0;
            }
#pragma warning restore 168

            // If there are open clients then ask if the user still wants to continue.
            if (openClients > 0)
            {
                string errorMessage = null;
                if (openClients > 1)
                {
                    errorMessage = string.Format("There are {0:n0} clients still open and processing this batch.", openClients);
                }
                else
                {
                    errorMessage = "There is one client still open and processing this batch.";
                }

                if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("{0}\r\n\r\nDo you still wish to continue?", errorMessage), "Open Clients", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return;
                }
            }

            // Conduct the mode-less form
            var frm = new Main_Form(ap);
            System.Windows.Forms.Application.Run(frm);
        }
    }
}