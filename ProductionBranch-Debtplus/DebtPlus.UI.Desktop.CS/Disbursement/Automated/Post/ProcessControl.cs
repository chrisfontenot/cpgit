﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
    internal partial class ProcessControl
    {
        /// <summary>
        /// Raised when the CANCEL button is pressed
        /// </summary>
        /// <remarks></remarks>
        public event EventHandler Completed;

        /// <summary>
        /// Current arguments to the procedure
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        private ArgParser ap
        {
            get
            {
                return ((Main_Form)ParentForm).ap;
            }
        }

        /// <summary>
        /// Time when the process was started to find the elapsed time
        /// </summary>
        /// <remarks></remarks>

        private DateTime startTime;

        /// <summary>
        /// Initialize the new context for the class
        /// </summary>
        /// <remarks></remarks>
        public ProcessControl()
            : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Process the background thread to do the posting operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        private void bt_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            // Allocate a new class to hold the work
            using (CDisbursement_Post cls = new CDisbursement_Post())
            {
                cls.PropertyChanged += cls_PropertyChanged;
                var _with1 = cls;
                cls.UseLocalFile = ap.UseLocalFile;
                cls.disbursement_register = ap.Batch;
                cls.ProcessBatch();
                cls.PropertyChanged -= cls_PropertyChanged;
            }
        }

        /// <summary>
        /// Look for changes in the properties of the control. These effect the status fields.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        private void cls_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // Pointer to the class with the data
            CDisbursement_Post cls = (CDisbursement_Post)sender;

            // Marshal the thread to the main window thread to avoid cross-thread problems
            if (InvokeRequired)
            {
                EndInvoke(BeginInvoke(new System.ComponentModel.PropertyChangedEventHandler(cls_PropertyChanged), new object[] { sender, e }));
            }
            else
            {
                switch (e.PropertyName)
                {
                    case "Label":
                        lbl_Status.Text = cls.Label;
                        break;

                    case "Client":
                        lbl_client.Text = string.Format("{0:0000000}", cls.Client);
                        break;

                    case "Creditor":
                        lbl_creditor.Text = cls.Creditor;
                        break;

                    case "CreditorsProcessed":
                        lbl_creditor_count.Text = string.Format("{0:n0}", cls.CreditorsProcessed);
                        break;

                    case "gross":
                        lbl_gross.Text = string.Format("{0:c}", cls.gross);
                        lbl_net.Text = string.Format("{0:c}", cls.gross - cls.deducted);
                        break;

                    case "deducted":
                        lbl_deducted.Text = string.Format("{0:c}", cls.deducted);
                        lbl_net.Text = string.Format("{0:c}", cls.gross - cls.deducted);
                        break;

                    case "billed":
                        lbl_billed.Text = string.Format("{0:c}", cls.billed);
                        break;

                    case "RowNumber":
                    case "RowCount":
                        Int32 Count = cls.RowCount;
                        Int32 Number = cls.RowNumber;
                        if (Count > 0 && Number > 0)
                        {
                            bt.ReportProgress(Count * 100 / Number);
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private void bt_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Timer1.Enabled = false;

            lbl_creditor.Text = "Completed";
            lbl_client.Text = "Completed";

            lbl_Status.Visible = false;
            lbl_Complete.Visible = true;
            Button_Close.Visible = true;

            ProgressBarControl1.EditValue = 100;
        }

        private void cmd_Close_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            Timer1.Enabled = false;
            if (Completed != null)
            {
                Completed(this, System.EventArgs.Empty);
            }
        }

        public void Start()
        {
            bt.DoWork += bt_DoWork;
            bt.RunWorkerCompleted += bt_RunWorkerCompleted;
            bt.ProgressChanged += bt_ProgressChanged;
            Button_Close.Click += cmd_Close_Click;
            Timer1.Tick += Timer1_Tick;

            lbl_creditor.Text = "Reading Payment Data";
            bt.WorkerReportsProgress = true;
            bt.RunWorkerAsync();

            startTime = DateTime.UtcNow;
            var _with2 = Timer1;
            _with2.Interval = 1000;
            _with2.Enabled = true;
        }

        private void Timer1_Tick(System.Object eventSender, System.EventArgs eventArgs)
        {
            // Find the number of seconds since we started
            System.TimeSpan ts = DateTime.UtcNow.Subtract(startTime);

            // Format the time value
            lbl_elapsed.Text = System.String.Format("{0:0}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);
        }

        private void bt_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            ProgressBarControl1.EditValue = e.ProgressPercentage;
        }
    }
}