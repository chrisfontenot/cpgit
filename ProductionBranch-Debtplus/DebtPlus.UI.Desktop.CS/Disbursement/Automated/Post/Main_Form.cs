﻿#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
    internal partial class Main_Form
    {
        internal ArgParser ap { get; set; }

        public Main_Form()
            : base()
        {
            Load += Main_Form_Load;
            InitializeComponent();
        }

        public Main_Form(ArgParser ap)
            : this()
        {
            this.ap = ap;
        }

        private void Main_Form_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (ap.Batch <= 0)
            {
                ProcessControl1.Visible = false;
                ProcessControl1.SendToBack();

                BatchControl1.BringToFront();
                BatchControl1.Visible = true;
                BatchControl1.ReadForm();
            }
            else
            {
                StartProcessing();
            }
        }

        private void StartProcessing()
        {
            BatchControl1.SendToBack();
            BatchControl1.Visible = false;

            ProcessControl1.BringToFront();
            ProcessControl1.Visible = true;
            ProcessControl1.Start();
        }

        private void BatchControl1_Cancelled(object sender, System.EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            if (!Modal)
                Close();
        }

        private void BatchControl1_Selected(object sender, System.EventArgs e)
        {
            // Save the batch ID for processing later
            ap.Batch = BatchControl1.BatchID;

            // Do the start processing function once the batch is known.
            StartProcessing();
        }

        private void ProcessControl1_Completed(object sender, System.EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            if (!Modal)
                Close();
        }
    }
}