﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
	partial class ProcessControl : DebtPlus.Data.Controls.UserControl
	{
		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool Disposing)
		{
			if (Disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(Disposing);
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;
		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessControl));
			this.Timer1 = new System.Windows.Forms.Timer(this.components);
			this.lbl_Status = new DevExpress.XtraEditors.LabelControl();
			this.lbl_Complete = new DevExpress.XtraEditors.LabelControl();
			this.lbl_creditor = new DevExpress.XtraEditors.LabelControl();
			this._Label1_7 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_creditor_count = new DevExpress.XtraEditors.LabelControl();
			this._Label1_6 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_billed = new DevExpress.XtraEditors.LabelControl();
			this._Label1_5 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_net = new DevExpress.XtraEditors.LabelControl();
			this._Label1_4 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_deducted = new DevExpress.XtraEditors.LabelControl();
			this._Label1_3 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_gross = new DevExpress.XtraEditors.LabelControl();
			this._Label1_2 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_client = new DevExpress.XtraEditors.LabelControl();
			this._Label1_1 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_elapsed = new DevExpress.XtraEditors.LabelControl();
			this._Label1_0 = new DevExpress.XtraEditors.LabelControl();
			this.Button_Close = new DevExpress.XtraEditors.SimpleButton();
			this.ProgressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
			((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//Timer1
			//
			this.Timer1.Interval = 1000;
			//
			//lbl_Status
			//
			this.lbl_Status.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_Status.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_Status.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_Status.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_Status.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.lbl_Status.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_Status.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.lbl_Status.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_Status.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_Status.Location = new System.Drawing.Point(8, 8);
			this.lbl_Status.Name = "lbl_Status";
			this.lbl_Status.Size = new System.Drawing.Size(299, 66);
			this.lbl_Status.TabIndex = 0;
			this.lbl_Status.Text = resources.GetString("lbl_Status.Text");
			this.lbl_Status.UseMnemonic = false;
			//
			//lbl_Complete
			//
			this.lbl_Complete.Appearance.BackColor = System.Drawing.SystemColors.Control;
			this.lbl_Complete.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_Complete.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_Complete.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.lbl_Complete.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_Complete.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.lbl_Complete.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_Complete.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_Complete.Location = new System.Drawing.Point(8, 8);
			this.lbl_Complete.Name = "lbl_Complete";
			this.lbl_Complete.Size = new System.Drawing.Size(218, 66);
			this.lbl_Complete.TabIndex = 1;
			this.lbl_Complete.Text = "The processing is complete. Please close this form by pressing the button to the " + "right.";
			this.lbl_Complete.Visible = false;
			//
			//lbl_creditor
			//
			this.lbl_creditor.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_creditor.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_creditor.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_creditor.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_creditor.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_creditor.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_creditor.Location = new System.Drawing.Point(147, 141);
			this.lbl_creditor.Name = "lbl_creditor";
			this.lbl_creditor.Size = new System.Drawing.Size(153, 16);
			this.lbl_creditor.TabIndex = 7;
			this.lbl_creditor.UseMnemonic = false;
			//
			//_Label1_7
			//
			this._Label1_7.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_7.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_7.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_7.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_7.Location = new System.Drawing.Point(13, 141);
			this._Label1_7.Name = "_Label1_7";
			this._Label1_7.Size = new System.Drawing.Size(128, 16);
			this._Label1_7.TabIndex = 6;
			this._Label1_7.Text = "Processing Creditor:";
			this._Label1_7.UseMnemonic = false;
			//
			//lbl_creditor_count
			//
			this.lbl_creditor_count.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_creditor_count.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_creditor_count.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_creditor_count.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_creditor_count.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_creditor_count.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_creditor_count.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_creditor_count.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_creditor_count.Location = new System.Drawing.Point(184, 118);
			this.lbl_creditor_count.Name = "lbl_creditor_count";
			this.lbl_creditor_count.Size = new System.Drawing.Size(116, 16);
			this.lbl_creditor_count.TabIndex = 5;
			this.lbl_creditor_count.Text = "0";
			this.lbl_creditor_count.UseMnemonic = false;
			//
			//_Label1_6
			//
			this._Label1_6.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_6.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_6.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_6.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_6.Location = new System.Drawing.Point(13, 118);
			this._Label1_6.Name = "_Label1_6";
			this._Label1_6.Size = new System.Drawing.Size(130, 16);
			this._Label1_6.TabIndex = 4;
			this._Label1_6.Text = "Creditors Processed:";
			this._Label1_6.UseMnemonic = false;
			//
			//lbl_billed
			//
			this.lbl_billed.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_billed.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_billed.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_billed.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_billed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_billed.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_billed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_billed.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_billed.Location = new System.Drawing.Point(184, 256);
			this.lbl_billed.Name = "lbl_billed";
			this.lbl_billed.Size = new System.Drawing.Size(116, 16);
			this.lbl_billed.TabIndex = 17;
			this.lbl_billed.Text = "$0.00";
			this.lbl_billed.UseMnemonic = false;
			//
			//_Label1_5
			//
			this._Label1_5.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_5.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_5.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_5.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_5.Location = new System.Drawing.Point(13, 256);
			this._Label1_5.Name = "_Label1_5";
			this._Label1_5.Size = new System.Drawing.Size(77, 16);
			this._Label1_5.TabIndex = 16;
			this._Label1_5.Text = "Total Billed:";
			this._Label1_5.UseMnemonic = false;
			//
			//lbl_net
			//
			this.lbl_net.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_net.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_net.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_net.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_net.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_net.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_net.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_net.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_net.Location = new System.Drawing.Point(184, 233);
			this.lbl_net.Name = "lbl_net";
			this.lbl_net.Size = new System.Drawing.Size(116, 16);
			this.lbl_net.TabIndex = 15;
			this.lbl_net.Text = "$0.00";
			this.lbl_net.UseMnemonic = false;
			//
			//_Label1_4
			//
			this._Label1_4.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_4.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_4.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_4.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_4.Location = new System.Drawing.Point(13, 233);
			this._Label1_4.Name = "_Label1_4";
			this._Label1_4.Size = new System.Drawing.Size(115, 16);
			this._Label1_4.TabIndex = 14;
			this._Label1_4.Text = "Net Disbursement:";
			this._Label1_4.UseMnemonic = false;
			//
			//lbl_deducted
			//
			this.lbl_deducted.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_deducted.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_deducted.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_deducted.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_deducted.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_deducted.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_deducted.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_deducted.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_deducted.Location = new System.Drawing.Point(184, 210);
			this.lbl_deducted.Name = "lbl_deducted";
			this.lbl_deducted.Size = new System.Drawing.Size(116, 16);
			this.lbl_deducted.TabIndex = 13;
			this.lbl_deducted.Text = "$0.00";
			this.lbl_deducted.UseMnemonic = false;
			//
			//_Label1_3
			//
			this._Label1_3.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_3.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_3.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_3.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_3.Location = new System.Drawing.Point(13, 210);
			this._Label1_3.Name = "_Label1_3";
			this._Label1_3.Size = new System.Drawing.Size(100, 16);
			this._Label1_3.TabIndex = 12;
			this._Label1_3.Text = "Total Deducted:";
			this._Label1_3.UseMnemonic = false;
			//
			//lbl_gross
			//
			this.lbl_gross.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_gross.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_gross.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_gross.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_gross.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_gross.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_gross.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_gross.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_gross.Location = new System.Drawing.Point(184, 187);
			this.lbl_gross.Name = "lbl_gross";
			this.lbl_gross.Size = new System.Drawing.Size(116, 16);
			this.lbl_gross.TabIndex = 11;
			this.lbl_gross.Text = "$0.00";
			this.lbl_gross.UseMnemonic = false;
			//
			//_Label1_2
			//
			this._Label1_2.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_2.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_2.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_2.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_2.Location = new System.Drawing.Point(12, 187);
			this._Label1_2.Name = "_Label1_2";
			this._Label1_2.Size = new System.Drawing.Size(129, 16);
			this._Label1_2.TabIndex = 10;
			this._Label1_2.Text = "Gross Disbursement:";
			this._Label1_2.UseMnemonic = false;
			//
			//lbl_client
			//
			this.lbl_client.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_client.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_client.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_client.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_client.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_client.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_client.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_client.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_client.Location = new System.Drawing.Point(184, 164);
			this.lbl_client.Name = "lbl_client";
			this.lbl_client.Size = new System.Drawing.Size(116, 16);
			this.lbl_client.TabIndex = 9;
			this.lbl_client.Text = "0000000";
			this.lbl_client.UseMnemonic = false;
			//
			//_Label1_1
			//
			this._Label1_1.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_1.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_1.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_1.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_1.Location = new System.Drawing.Point(13, 164);
			this._Label1_1.Name = "_Label1_1";
			this._Label1_1.Size = new System.Drawing.Size(114, 16);
			this._Label1_1.TabIndex = 8;
			this._Label1_1.Text = "Processing Client:";
			this._Label1_1.UseMnemonic = false;
			//
			//lbl_elapsed
			//
			this.lbl_elapsed.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_elapsed.Appearance.BackColor = System.Drawing.Color.Transparent;
			this.lbl_elapsed.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.lbl_elapsed.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this.lbl_elapsed.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.lbl_elapsed.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.lbl_elapsed.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.lbl_elapsed.Cursor = System.Windows.Forms.Cursors.Default;
			this.lbl_elapsed.Location = new System.Drawing.Point(184, 95);
			this.lbl_elapsed.Name = "lbl_elapsed";
			this.lbl_elapsed.Size = new System.Drawing.Size(116, 16);
			this.lbl_elapsed.TabIndex = 3;
			this.lbl_elapsed.Text = "00:00:00";
			this.lbl_elapsed.UseMnemonic = false;
			//
			//_Label1_0
			//
			this._Label1_0.Appearance.BackColor = System.Drawing.Color.Transparent;
			this._Label1_0.Appearance.Font = new System.Drawing.Font("Arial", 10f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this._Label1_0.Appearance.ForeColor = System.Drawing.SystemColors.ControlText;
			this._Label1_0.Cursor = System.Windows.Forms.Cursors.Default;
			this._Label1_0.Location = new System.Drawing.Point(13, 95);
			this._Label1_0.Name = "_Label1_0";
			this._Label1_0.Size = new System.Drawing.Size(90, 16);
			this._Label1_0.TabIndex = 2;
			this._Label1_0.Text = "Elapsed Time:";
			this._Label1_0.UseMnemonic = false;
			//
			//Button_Close
			//
			this.Button_Close.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.Button_Close.Location = new System.Drawing.Point(232, 8);
			this.Button_Close.Name = "Button_Close";
			this.Button_Close.Size = new System.Drawing.Size(75, 23);
			this.Button_Close.TabIndex = 18;
			this.Button_Close.Text = "&Close";
			this.Button_Close.Visible = false;
			//
			//ProgressBarControl1
			//
			this.ProgressBarControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.ProgressBarControl1.Location = new System.Drawing.Point(13, 288);
			this.ProgressBarControl1.Name = "ProgressBarControl1";
			this.ProgressBarControl1.Properties.Appearance.ForeColor = System.Drawing.Color.DodgerBlue;
			this.ProgressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
			this.ProgressBarControl1.Properties.Step = 5;
			this.ProgressBarControl1.Size = new System.Drawing.Size(287, 18);
			this.ProgressBarControl1.TabIndex = 19;
			//
			//ProcessControl
			//
			this.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Appearance.Options.UseFont = true;
			this.Controls.Add(this.lbl_Status);
			this.Controls.Add(this.ProgressBarControl1);
			this.Controls.Add(this.lbl_creditor);
			this.Controls.Add(this._Label1_7);
			this.Controls.Add(this.lbl_creditor_count);
			this.Controls.Add(this._Label1_6);
			this.Controls.Add(this.lbl_billed);
			this.Controls.Add(this._Label1_5);
			this.Controls.Add(this.lbl_net);
			this.Controls.Add(this._Label1_4);
			this.Controls.Add(this.lbl_deducted);
			this.Controls.Add(this._Label1_3);
			this.Controls.Add(this.lbl_gross);
			this.Controls.Add(this._Label1_2);
			this.Controls.Add(this.lbl_client);
			this.Controls.Add(this._Label1_1);
			this.Controls.Add(this.lbl_elapsed);
			this.Controls.Add(this._Label1_0);
			this.Controls.Add(this.lbl_Complete);
			this.Controls.Add(this.Button_Close);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Name = "ProcessControl";
			this.Size = new System.Drawing.Size(312, 318);
			((System.ComponentModel.ISupportInitialize)this.ProgressBarControl1.Properties).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		internal System.Windows.Forms.Timer Timer1;
		internal DevExpress.XtraEditors.LabelControl lbl_Status;
		internal DevExpress.XtraEditors.LabelControl lbl_Complete;
		internal DevExpress.XtraEditors.LabelControl lbl_creditor;
		internal DevExpress.XtraEditors.LabelControl _Label1_7;
		internal DevExpress.XtraEditors.LabelControl lbl_creditor_count;
		internal DevExpress.XtraEditors.LabelControl _Label1_6;
		internal DevExpress.XtraEditors.LabelControl lbl_billed;
		internal DevExpress.XtraEditors.LabelControl _Label1_5;
		internal DevExpress.XtraEditors.LabelControl lbl_net;
		internal DevExpress.XtraEditors.LabelControl _Label1_4;
		internal DevExpress.XtraEditors.LabelControl lbl_deducted;
		internal DevExpress.XtraEditors.LabelControl _Label1_3;
		internal DevExpress.XtraEditors.LabelControl lbl_gross;
		internal DevExpress.XtraEditors.LabelControl _Label1_2;
		internal DevExpress.XtraEditors.LabelControl lbl_client;
		internal DevExpress.XtraEditors.LabelControl _Label1_1;
		internal DevExpress.XtraEditors.LabelControl lbl_elapsed;
		internal DevExpress.XtraEditors.LabelControl _Label1_0;
		internal DevExpress.XtraEditors.SimpleButton Button_Close;
		internal System.ComponentModel.BackgroundWorker bt = new System.ComponentModel.BackgroundWorker();
		internal DevExpress.XtraEditors.ProgressBarControl ProgressBarControl1;
	}
}