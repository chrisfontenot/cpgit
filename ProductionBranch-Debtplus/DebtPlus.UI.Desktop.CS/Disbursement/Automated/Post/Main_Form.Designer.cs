﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
	partial class Main_Form : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		protected override void Dispose(bool Disposing)
		{
			if (Disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(Disposing);
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;
		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.
		//Do not modify it using the code editor.
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
			this.BatchControl1 = new DebtPlus.UI.FormLib.Disbursement.DisbursementRegisterControl();
			this.ProcessControl1 = new ProcessControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//BatchControl1
			//
			this.BatchControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.BatchControl1.Location = new System.Drawing.Point(0, 0);
			this.BatchControl1.Name = "BatchControl1";
			this.BatchControl1.Size = new System.Drawing.Size(424, 318);
			this.BatchControl1.TabIndex = 0;
			//
			//ProcessControl1
			//
			this.ProcessControl1.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.ProcessControl1.Appearance.Options.UseFont = true;
			this.ProcessControl1.Cursor = System.Windows.Forms.Cursors.Default;
			this.ProcessControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ProcessControl1.Location = new System.Drawing.Point(0, 0);
			this.ProcessControl1.Name = "ProcessControl1";
			this.ProcessControl1.Size = new System.Drawing.Size(424, 318);
			this.ProcessControl1.TabIndex = 1;
			//
			//Main_Form
			//
			this.Appearance.Font = new System.Drawing.Font("Arial", 8f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Convert.ToByte(0));
			this.Appearance.Options.UseFont = true;
			this.ClientSize = new System.Drawing.Size(424, 318);
			this.ControlBox = false;
			this.Controls.Add(this.ProcessControl1);
			this.Controls.Add(this.BatchControl1);
			this.Cursor = System.Windows.Forms.Cursors.Default;
			this.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
			this.Location = new System.Drawing.Point(3, 22);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Main_Form";
			this.Text = "Posting Disbursement Status";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);
		}
		internal DebtPlus.UI.FormLib.Disbursement.DisbursementRegisterControl BatchControl1;
		internal ProcessControl ProcessControl1;
	}
}