#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
    internal class RPPS_Processing : IDisposable, DebtPlus.UI.Desktop.CS.Disbursement.Common.Iform_RPS
    {
        public RPPS_Processing()
            : base()
        {
        }

        /// <summary>
        /// This "form" is a fake. It is present so that the
        /// payment class may have something to send events.
        /// </summary>
        public string OutputDirectory = string.Empty;

        /// <summary>
        /// Fake routine to simulate the form display
        /// </summary>
        public void Show()
        {
        }

        #region " IDisposable Support "

        // To detect redundant calls
        private bool disposedValue = false;

        /// <summary>
        /// Support the dispose item for the "form"
        /// </summary>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                }
            }
            this.disposedValue = true;
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion " IDisposable Support "

        /// <summary>
        /// Determine the output file name
        /// (In the "real" form, this housed the CommonDialog control.
        /// so it had to be moved here to ask for the output file.)
        /// </summary>
        public StreamWriter Request_FileName(Int32 disbursement_register, Int32 Bank, string FileType)
        {
            return Request_FileName(disbursement_register, Bank, FileType, false);
        }

        public StreamWriter Request_FileName(Int32 disbursement_register, Int32 Bank, string FileType, bool UseLocalFileOnly)
        {
            FileStream ios = null;
            StreamWriter Result = null;
            string ErrorMessage = string.Empty;

            // Create the base file name that we are going to use
            string CreatedName = string.Format("{0:00000000}_{1:0000}_{2}.TXT", disbursement_register, Bank, FileType);
            string PathName = null;

            // If we can create a remote file then do so now.
            if (!UseLocalFileOnly)
            {
                PathName = DefaultFolder(Bank);
                if (PathName != string.Empty)
                {
                    DebtPlus.UI.Desktop.CS.Disbursement.Common.GenerateRPSPayments.FileName = Path.Combine(PathName, CreatedName);
                    try
                    {
                        ios = new FileStream(DebtPlus.UI.Desktop.CS.Disbursement.Common.GenerateRPSPayments.FileName, FileMode.Create, FileAccess.Write, FileShare.Read, 4096);
                    }
                    catch (Exception ex)
                    {
                        ErrorMessage = string.Format("{0}{1}{1}An error prevented the file from being written to the default directory.{1}The system will try to use your local documents directory instead.", ex.Message, Environment.NewLine);
                    }
                }
            }

            // Use the current documents directory for the file.
            if (ios == null)
            {
                PathName = DocumentsFolder();
                if (PathName == string.Empty)
                {
                    ErrorMessage = "The system did not return a path to the My Documents folder so the file can not be written.";
                }
                else
                {
                    DebtPlus.UI.Desktop.CS.Disbursement.Common.GenerateRPSPayments.FileName = Path.Combine(PathName, CreatedName);
                    try
                    {
                        ios = new FileStream(DebtPlus.UI.Desktop.CS.Disbursement.Common.GenerateRPSPayments.FileName, FileMode.Create, FileAccess.Write, FileShare.Read, 4096);
                    }
                    catch (Exception ex)
                    {
                        ErrorMessage = string.Format("{0}{1}{1}An error prevented the file from being written to documents directory.{1}The file has been skipped.", ex.Message, Environment.NewLine);
                    }
                }
            }

            // If there is a pending error then display it to the user
            if (ErrorMessage != string.Empty)
            {
                DebtPlus.Data.Forms.MessageBox.Show(ErrorMessage, "Error creating RPPS output file", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            // Return the file pointer or null if none available
            if (ios != null)
            {
                Result = new StreamWriter(ios, Encoding.ASCII, 4096);
            }

            return Result;
        }

        private string DocumentsFolder()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        }

        private string DefaultFolder(Int32 Bank)
        {
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "SELECT output_directory FROM banks WHERE bank = @bank";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = Bank;
                    object obj = cmd.ExecuteScalar();
                    if (obj != null && !object.ReferenceEquals(obj, DBNull.Value))
                    {
                        return Convert.ToString(obj).Trim();
                    }
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading banks table");
            }
            finally
            {
                if (cn != null)
                {
                    cn.Dispose();
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public decimal total_debt { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public decimal total_credit { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public decimal total_billed { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public decimal total_gross { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public decimal total_net { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public Int32 total_items { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public Int32 total_prenotes { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public Int32 total_batches { set { } }

        /// <summary>
        /// Not Implemented
        /// </summary>
        public string Batch { set { } }

        string DebtPlus.UI.Desktop.CS.Disbursement.Common.Iform_RPS.batch
        {
            set
            {
                Batch = value;
            }
        }
    }
}