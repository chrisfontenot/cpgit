using System;
using System.Globalization;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        private Int32 _batch = -1;

        public Int32 Batch
        {
            get { return _batch; }
            set { _batch = value; }
        }

        private bool _useLocalFile;

        public bool UseLocalFile
        {
            get { return _useLocalFile; }
        }

        public ArgParser()
            : base(new string[] { "b", "l" })
        {
        }

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            SwitchStatus ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            switch (switchSymbol)
            {
                case "b":
                    double TempBatch = 0;
                    if ((double.TryParse(switchValue, NumberStyles.Integer, CultureInfo.InvariantCulture, out TempBatch)))
                    {
                        if (TempBatch > 0 && TempBatch <= Convert.ToDouble(Int32.MaxValue))
                        {
                            _batch = Convert.ToInt32(TempBatch);
                            break; // TODO: might not be correct. Was : Exit Select
                        }
                    }
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;

                    break;

                case "l":
                    _useLocalFile = true;

                    break;

                case "?":
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;

                    break;

                default:
                    ss = DebtPlus.Utils.ArgParserBase.SwitchStatus.YesError;
                    break;
            }
            return ss;
        }

        protected override void OnUsage(string errorInfo)
        {
            //AppendErrorLine(String.Format("Usage: {0} [-b#]", fname))
            //AppendErrorLine("       -b : Disbursement batch to be posted")
            //AppendErrorLine("       -l : Generate EFT files locally and not according to the banks table")
        }
    }
}