#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post
{
    internal partial class CDisbursement_Post : IDisposable, System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// Event to notify the caller that a field was changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public delegate void PropertyChangedEventHandler(object sender, System.ComponentModel.PropertyChangedEventArgs e);

        // Items that need to be disposed
        private readonly DataSet ds = new DataSet("ds");

        // Collection of items for payments.
        private ArrayList col_item_info = new ArrayList();

        private DataTable tbl;
        private Int32 _Client = 0;
        private string _Creditor = string.Empty;
        private Int32 _CreditorsProcessed = 0;
        private decimal _gross = 0m;
        private decimal _deducted = 0m;

        private decimal _billed = 0m;

        /// <summary>
        /// Batch Number to process
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public Int32 disbursement_register { get; set; }

        /// <summary>
        /// Override to use the local documents directory rather than the config entry for RPPS files
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool UseLocalFile { get; set; }

        /// <summary>
        /// Change the display label on the status form
        /// </summary>
        private string privateLabel = string.Empty;

        public string Label
        {
            get { return privateLabel; }
            set
            {
                if (value != privateLabel)
                {
                    privateLabel = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("Label"));
                    }
                }
            }
        }

        public Int32 Client
        {
            get { return _Client; }
            set
            {
                if (_Client != value)
                {
                    _Client = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("Client"));
                    }
                }
            }
        }

        /// <summary>
        /// Current creditor ID
        /// </summary>
        public string Creditor
        {
            get { return _Creditor; }
            set
            {
                if (_Creditor != value)
                {
                    _Creditor = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("Creditor"));
                    }
                }
            }
        }

        /// <summary>
        /// Number of processed creditors
        /// </summary>
        public Int32 CreditorsProcessed
        {
            get { return _CreditorsProcessed; }
            set
            {
                if (_CreditorsProcessed != value)
                {
                    _CreditorsProcessed = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("CreditorsProcessed"));
                    }
                }
            }
        }

        /// <summary>
        /// Dollar amount deducted from gross
        /// </summary>
        public decimal gross
        {
            get { return _gross; }
            set
            {
                if (_gross != value)
                {
                    _gross = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("gross"));
                    }
                }
            }
        }

        /// <summary>
        /// Dollar amount deducted from gross
        /// </summary>
        public decimal deducted
        {
            get { return _deducted; }
            set
            {
                if (_deducted != value)
                {
                    _deducted = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("deducted"));
                    }
                }
            }
        }

        /// <summary>
        /// Dollar amount billed to creditors
        /// </summary>
        public decimal billed
        {
            get { return _billed; }
            set
            {
                if (_billed != value)
                {
                    _billed = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("billed"));
                    }
                }
            }
        }

        private Int32 _RowNumber = 0;

        public Int32 RowNumber
        {
            get { return _RowNumber; }
            set
            {
                if (_RowNumber != value)
                {
                    _RowNumber = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("RowNumber"));
                    }
                }
            }
        }

        private Int32 _RowCount = 0;

        public Int32 RowCount
        {
            get { return _RowCount; }
            set
            {
                if (_RowCount != value)
                {
                    _RowCount = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs("RowCount"));
                    }
                }
            }
        }

        /// <summary>
        /// Find the payment item the is corresponding to the bank
        /// </summary>
        public DebtPlus.UI.Desktop.CS.Disbursement.Common.item_info get_item_info(Int32 bank)
        {
            DebtPlus.UI.Desktop.CS.Disbursement.Common.item_info result = null;

            // Try to find the bank account in the collection of items
            foreach (DebtPlus.UI.Desktop.CS.Disbursement.Common.item_info item in col_item_info)
            {
                if (item.bank == bank)
                {
                    result = item;
                    break;
                }
            }

            // If there is no match then create a new item.
            if (result == null)
            {
                result = new DebtPlus.UI.Desktop.CS.Disbursement.Common.item_info(this);
                var _with1 = result;
                _with1.bank = bank;
                _with1.read_bank(bank);
                col_item_info.Add(result);
            }

            // Return the resulting structure pointer
            return result;
        }

        /// <summary>
        /// Process the posting operation
        /// </summary>

        public void ProcessBatch()
        {
            // Find the list of payments to process
            tbl = InputTable();
            if (tbl != null)
            {
                PayBatches();
                CalculateDeductCheck();
                GenerateEFTFiles();
            }
        }

        /// <summary>
        /// Find a list of the transactions to be processed
        /// </summary>
        private DataTable InputTable()
        {
            DataTable tbl = null;
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = cn;
                    _with2.CommandTimeout = 0;
                    _with2.CommandText = "xpr_disbursement_list_post";
                    _with2.CommandType = CommandType.StoredProcedure;
                    _with2.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;

                    using (var da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "xpr_disbursement_list_post");
                        tbl = ds.Tables["xpr_disbursement_list_post"];
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading disbursement information");
            }
            finally
            {
                if (cn != null)
                {
                    cn.Dispose();
                }
            }

            return tbl;
        }

        /// <summary>
        /// Pay all of the creditors in the batch
        /// </summary>

        private void PayBatches()
        {
            // Obtain the transactions for the batch
            Label = "Processing creditor payments";
            RowCount = tbl.Rows.Count;
            RowNumber = 0;
            CreditorsProcessed = 0;

            // Process the items until there are no more to process.
            // The ProcessCredior returns at the end of each creditor.
            while (RowNumber < RowCount)
            {
                CreditorsProcessed += 1;
                ProcessCreditor();
            }
        }

        /// <summary>
        /// Calculate the Z9999 deduction check
        /// </summary>
        private void CalculateDeductCheck()
        {
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

            // Generate the deduction check(s) and close the batch
            Label = "Creating deduct check information";
            try
            {
                cn.Open();

                using (var cmd = new SqlCommand())
                {
                    var _with3 = cmd;
                    _with3.Connection = cn;
                    _with3.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    _with3.CommandText = "xpr_disbursement_deduct";
                    _with3.CommandType = CommandType.StoredProcedure;
                    _with3.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                    _with3.ExecuteNonQuery();
                }
            }
            finally
            {
                if (cn != null)
                {
                    cn.Dispose();
                }
            }
        }

        /// <summary>
        /// Generate the EFT files for the disbursement
        /// </summary>

        private void GenerateEFTFiles()
        {
            // Process the RPPS files once the creditors have been generated
            foreach (DebtPlus.UI.Desktop.CS.Disbursement.Common.item_info item in col_item_info)
            {
                switch (item.bank_type)
                {
                    case "R":
                        Label = string.Format("Generating Payment File for bank # {0:f0}.", item.bank);
                        item.Write_RPPS(UseLocalFile);
                        break;
                }
            }
        }

        /// <summary>
        /// A creditor was found. Process all records for this creditor
        /// </summary>

        private void ProcessCreditor()
        {
            // Information about our current creditor trust register
            DebtPlus.UI.Desktop.CS.Disbursement.Common.item_info pmt_info = null;

            // Information for the current debt
            double debt_fairshare_pct_check = 0.0;
            double debt_fairshare_pct_eft = 0.0;

            string creditor_type = string.Empty;
            double fairshare_pct = 0.0;
            decimal fairshare_amt = 0m;
            decimal debt_gross = 0m;

            // Information from the creditor table
            Int32 creditor_voucher_spacing = 0;
            decimal creditor_max_amt_per_check = 0m;
            Int32 creditor_max_clients_per_check = -1;
            decimal creditor_max_fairshare_per_debt = 0;
            Int32 bank = 0;
            string rpps_biller_id = string.Empty;

            // Information for the current transaction
            decimal fairshare_deducted = 0m;
            decimal fairshare_billed = 0m;
            Int32 client_creditor_register = 0;

            // Number of items permitted for this creditor
            Int32 creditor_item_limit = 0;
            decimal creditor_dollar_limit = 0m;

            // Find the information for this creditor
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlTransaction txn = null;
            System.Data.SqlClient.SqlDataReader rd = null;

            DataRow row = tbl.Rows[RowNumber];
            Creditor = Convert.ToString(row["creditor"]);

            // Find the bank for this specific creditor.
            // The bank does not change for each transaction since the debt is not tied to a bank. The creditor has a bank, not the debt.
            bank = Convert.ToInt32(row["bank"]);
            pmt_info = get_item_info(bank);

            // Ensure that we start with a new creditor register even if this is a bank wire transaction.
            pmt_info.creditor_register = 0;

            try
            {
                cn.Open();
                txn = cn.BeginTransaction();

                using (var cmd = new SqlCommand())
                {
                    var _with4 = cmd;
                    _with4.Transaction = txn;
                    _with4.Connection = cn;
                    _with4.CommandText = "SELECT [creditor],[voucher_spacing],[max_amt_per_check],[max_fairshare_per_debt],[max_clients_per_check] FROM view_disbursement_post_creditor_info WHERE creditor=@creditor";
                    _with4.Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = Creditor;
                    rd = _with4.ExecuteReader(CommandBehavior.SingleRow);

                    if (rd != null)
                    {
                        if (rd.Read())
                        {
                            for (Int32 FieldNo = 0; FieldNo <= rd.FieldCount - 1; FieldNo++)
                            {
                                if (!rd.IsDBNull(FieldNo))
                                {
                                    switch (rd.GetName(FieldNo).ToLower())
                                    {
                                        case "voucher_spacing":
                                            creditor_voucher_spacing = Convert.ToInt32(rd.GetValue(FieldNo));
                                            break;

                                        case "max_amt_per_check":
                                            creditor_max_amt_per_check = rd.GetDecimal(FieldNo);
                                            break;

                                        case "max_clients_per_check":
                                            creditor_max_clients_per_check = Convert.ToInt32(rd.GetValue(FieldNo));
                                            break;

                                        case "max_fairshare_per_debt":
                                            creditor_max_fairshare_per_debt = rd.GetDecimal(FieldNo);
                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }
                            rd.Close();
                        }
                        rd = null;
                    }
                }

                // Stop processing when the creditor changes

                while (RowNumber < RowCount)
                {
                    // Stop processing when the creditor ID changes.
                    row = tbl.Rows[RowNumber];
                    if (Creditor != Convert.ToString(row["creditor"]))
                    {
                        break;
                    }

                    // Find the item limit for the creditor checks
                    creditor_item_limit = creditor_max_clients_per_check;
                    if (creditor_item_limit <= 0)
                    {
                        creditor_item_limit = pmt_info.max_items;
                    }

                    if (creditor_voucher_spacing == 2)
                    {
                        creditor_item_limit = 0;
                    }

                    if (creditor_item_limit > 0)
                    {
                        if (creditor_voucher_spacing == 1)
                        {
                            creditor_item_limit = (creditor_item_limit + 1) / 2;
                        }
                    }

                    // Apply a maximum limit of 40 items.
                    if (creditor_item_limit > 40)
                    {
                        creditor_item_limit = 40;
                    }

                    // Find the item limit for the creditor checks
                    creditor_dollar_limit = creditor_max_amt_per_check;
                    if (creditor_dollar_limit <= 0m)
                    {
                        creditor_dollar_limit = pmt_info.max_amount;
                    }

                    // Save the client for later.
                    Client = Convert.ToInt32(row["client"]);

                    // Save the ids
                    rpps_biller_id = DebtPlus.Utils.Nulls.DStr(row["rpps_biller_id"]);

                    creditor_type = DebtPlus.Utils.Nulls.DStr(row["creditor_type"], "N");

                    // Save the gross amount to be paid for later
                    debt_gross = DebtPlus.Utils.Nulls.DDec(row["debit_amt"]);

                    // Calculate the parameters for the client/creditor information
                    debt_fairshare_pct_eft = DebtPlus.Utils.Nulls.DDbl(row["fairshare_pct_eft"]);
                    debt_fairshare_pct_check = DebtPlus.Utils.Nulls.DDbl(row["fairshare_pct_check"]);

                    // Find the creditor type and the corresponding fairshare percentage
                    if (pmt_info.bank_type == "C")
                    {
                        fairshare_pct = debt_fairshare_pct_check;
                    }
                    else
                    {
                        fairshare_pct = debt_fairshare_pct_eft;
                    }

                    // Look at the type of the creditor for the proper fairshare information
                    if (creditor_type == "N")
                    {
                        fairshare_pct = 0.0;
                        fairshare_amt = 0m;
                    }
                    else
                    {
                        fairshare_amt = System.Decimal.Truncate(Convert.ToDecimal(Convert.ToDouble(debt_gross) * fairshare_pct * 100.0)) / 100M;

                        // If the fairshare is too much then limit it to the maximum
                        if (creditor_max_fairshare_per_debt > 0m)
                        {
                            if (creditor_max_fairshare_per_debt < fairshare_amt)
                            {
                                fairshare_amt = creditor_max_fairshare_per_debt;
                            }
                        }
                    }

                    if ((pmt_info.bank_type == "C") && (pmt_info.trust_register > 0))
                    {
                        // Limit the check to the maximum number of clients
                        if (creditor_item_limit > 0 && pmt_info.item_count >= creditor_item_limit)
                        {
                            pmt_info.Save_Trust_Register(cn, txn);
                        }

                        // Limit the check to the maximum dollar amount
                        else if ((creditor_dollar_limit > 0m) && (((pmt_info.gross - pmt_info.deducted) + (debt_gross - fairshare_deducted)) >= creditor_dollar_limit))
                        {
                            pmt_info.Save_Trust_Register(cn, txn);
                        }
                    }

                    fairshare_deducted = 0m;
                    fairshare_billed = 0m;

                    // Determine if the amount is billed or deducted
                    if (creditor_type == "D")
                    {
                        fairshare_deducted = fairshare_amt;
                    }
                    else if (creditor_type != "N")
                    {
                        fairshare_billed = fairshare_amt;
                    }

                    // Create a new trust register for the check if there is not one presently open
                    if (pmt_info.trust_register <= 0)
                    {
                        pmt_info.New_Check();
                        pmt_info.New_Trust_Register(cn, txn);
                    }

                    // We need a creditor register entry to complete the processing. Get one if we
                    // don't currently have one.
                    if (pmt_info.creditor_register <= 0)
                    {
                        pmt_info.New_Creditor_Register(cn, txn);
                    }

                    // Update the statistics for the current item
                    var _with5 = pmt_info;
                    _with5.gross += debt_gross;
                    _with5.deducted += fairshare_deducted;
                    _with5.billed += fairshare_billed;
                    _with5.item_count += 1;

                    gross += debt_gross;
                    deducted += fairshare_deducted;
                    billed += fairshare_billed;

                    // Insert the transaction into the registers_client_creditor table
                    // and deduct the amount paid from the balance of the debt.
                    using (var cmd = new SqlCommand())
                    {
                        var _with6 = cmd;
                        _with6.Connection = cn;
                        _with6.Transaction = txn;
                        _with6.CommandText = "xpr_disbursement_cc_trans";
                        _with6.CommandType = CommandType.StoredProcedure;

                        _with6.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                        _with6.Parameters.Add("@tran_type", SqlDbType.VarChar, 2).Value = pmt_info.tran_type;
                        _with6.Parameters.Add("@client_creditor", SqlDbType.Int).Value = Convert.ToInt32(row["client_creditor"]);
                        _with6.Parameters.Add("@creditor_type", SqlDbType.VarChar, 2).Value = creditor_type;
                        _with6.Parameters.Add("@debit_amt", SqlDbType.Decimal).Value = debt_gross;
                        _with6.Parameters.Add("@fairshare_pct", SqlDbType.Float).Value = fairshare_pct;
                        _with6.Parameters.Add("@fairshare_amt", SqlDbType.Decimal).Value = fairshare_amt;
                        _with6.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                        _with6.Parameters.Add("@trust_register", SqlDbType.Int).Value = pmt_info.trust_register;
                        _with6.Parameters.Add("@account_number", SqlDbType.VarChar, 256).Value = Convert.ToString(row["account_number"]);
                        _with6.Parameters.Add("@creditor_register", SqlDbType.Int).Value = pmt_info.creditor_register;

                        _with6.ExecuteNonQuery();
                        client_creditor_register = Convert.ToInt32(_with6.Parameters["RETURN"].Value);
                    }

                    // If this is an RPPS transaction then find the file number for the transactions.

                    if (pmt_info.bank_type == "R")
                    {
                        // Attach the transaction to the file.
                        using (var cmd = new SqlCommand())
                        {
                            var _with7 = cmd;
                            _with7.Connection = cn;
                            _with7.Transaction = txn;
                            _with7.CommandText = "xpr_rpps_cie_payment";
                            _with7.CommandType = CommandType.StoredProcedure;

                            _with7.Parameters.Add("@client_creditor", SqlDbType.Int).Value = Convert.ToInt32(row["client_creditor"]);
                            _with7.Parameters.Add("@client_creditor_register", SqlDbType.Int).Value = client_creditor_register;
                            _with7.Parameters.Add("@BillerID", SqlDbType.VarChar, 80).Value = rpps_biller_id;
                            _with7.Parameters.Add("@bank", SqlDbType.Int).Value = pmt_info.bank;
                            _with7.ExecuteNonQuery();
                        }
                    }
                    RowNumber += 1;
                }

                // Close out the creditor for this is no longer the proper creditor.
                pmt_info.Save_Trust_Register(cn, txn);

                // Commit the changes to the database
                txn.Commit();
                txn.Dispose();
                txn = null;
            }
            catch (Exception ex)
            {
                DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }
                    catch (Exception exRollback)
                    {
                        DebtPlus.Svc.Common.ErrorHandling.HandleErrors(exRollback);
                    }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                {
                    cn.Dispose();
                }
            }
        }

        #region "IDisposable Support"

        // To detect redundant calls
        private bool disposedValue;

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    ds.Dispose();
                    if (col_item_info != null)
                    {
                        col_item_info.Clear();
                    }
                }

                col_item_info = null;
                tbl = null;
            }

            this.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion "IDisposable Support"
    }
}