#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Common;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual
{
    internal partial class Form_ManualDisbursement
    {
        // Information set by the copy form and passed to the main entry form later.
        internal string Creditor = string.Empty;

        internal System.Int32 TrustRegister = -1;

        private Context ctx = null;

        public Form_ManualDisbursement() : base()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create an instance of our form
        /// </summary>
        public Form_ManualDisbursement(Context ctx) : this()
        {
            this.ctx = ctx;

            Main_Copy1.Cancelled += Main_Copy1_Cancelled;
            Main_Copy1.Completed += Main_Copy1_Completed;
            Load += Form_ManualDisbursement_Load;
            Main_Entry1.Cancelled += Main_Copy1_Cancelled;
        }

        /// <summary>
        /// Process the cancel event from the form
        /// </summary>
        private void Main_Copy1_Cancelled(object sender, System.EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Process the OK event from the check copy screen
        /// </summary>

        private void Main_Copy1_Completed(object sender, System.EventArgs e)
        {
            // Move the copy frame to the background
            Main_Copy1.SendToBack();
            Main_Copy1.Visible = false;
            Main_Copy1.TabStop = false;

            // Make the main entry form go to the background
            Main_Entry1.TabStop = true;
            Main_Entry1.Visible = true;
            Main_Entry1.BringToFront();
            Main_Entry1.Focus();
            Main_Entry1.ReadForm();
        }

        /// <summary>
        /// Display the copy information when we are started
        /// </summary>

        private void Form_ManualDisbursement_Load(object sender, System.EventArgs e)
        {
            // Restore the location and size of the form
            base.LoadPlacement("Disbursement.Manual");

            // Make the main entry form go to the background
            Main_Entry1.SendToBack();
            Main_Entry1.Visible = false;
            Main_Entry1.TabStop = false;

            // Move the copy frame to the foreground
            Main_Copy1.TabStop = true;
            Main_Copy1.Visible = true;
            Main_Copy1.BringToFront();
            Main_Copy1.Focus();
            Main_Copy1.ReadForm();
        }
    }
}