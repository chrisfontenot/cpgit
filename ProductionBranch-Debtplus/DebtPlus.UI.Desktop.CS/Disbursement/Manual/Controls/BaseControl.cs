using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Common;
using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls
{
    internal partial class BaseControl
    {
        protected Context ctx = null;

        public BaseControl(Context ctx)
        {
            this.ctx = ctx;
            InitializeComponent();
        }

        /// <summary>
        /// Called to do the equivalent of "one-time" initialization
        /// when the program is stablized. Equivalent to the LOAD event
        /// </summary>
        public virtual void ReadForm()
        {
        }

        /// <summary>
        /// Define the creditors table
        /// </summary>
        protected System.Data.DataRow ReadCreditor(string creditor)
        {
            System.Data.DataRow answer = null;

            // If the table is not defined then read it from the database.
            System.Data.DataTable tbl = ctx.ds.Tables["creditors"];
            if (tbl != null)
                answer = tbl.Rows.Find(creditor);

            if (answer == null)
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with1.CommandText = "SELECT cr.creditor, cr.creditor_id, cr.type, cr.sic, cr.creditor_name, cr.comment, cr.division, cr.creditor_class, cr.voucher_spacing, cr.mail_priority, cr.payment_balance, cr.prohibit_use, cr.full_disclosure, cr.suppress_invoice, cr.proposal_budget_info, cr.proposal_income_info, cr.contrib_cycle, cr.contrib_bill_month, cr.pledge_amt, cr.pledge_cycle, cr.pledge_bill_month, cr.min_accept_amt, cr.min_accept_pct, cr.min_accept_per_bill, cr.lowest_apr_pct, cr.medium_apr_pct, cr.highest_apr_pct, cr.medium_apr_amt, cr.highest_apr_amt, cr.max_clients_per_check, cr.max_amt_per_check, cr.max_fairshare_per_debt, cr.chks_per_invoice, cr.returned_mail, cr.po_number, cr.usual_priority, cr.percent_balance, cr.distrib_mtd, cr.distrib_ytd, cr.contrib_mtd_billed, cr.contrib_ytd_billed, cr.contrib_mtd_received, cr.contrib_ytd_received, cr.first_payment, cr.last_payment, cr.acceptance_days, cr.check_payments, cr.check_bank, cr.creditor_contribution_pct, cr.date_created, cr.created_by, pct.creditor_type_check, pct.fairshare_pct_check FROM creditors cr WITH (NOLOCK) LEFT OUTER JOIN creditor_contribution_pcts pct ON cr.creditor_contribution_pct = pct.creditor_contribution_pct WHERE cr.creditor = @creditor";
                    _with1.CommandType = CommandType.Text;
                    _with1.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = creditor;

                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                    try
                    {
                        da.Fill(ctx.ds, "creditors");
                        tbl = ctx.ds.Tables["creditors"];
                        if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                        {
                            var _with2 = tbl;
                            _with2.PrimaryKey = new System.Data.DataColumn[] { _with2.Columns["creditor"] };
                        }

                        answer = tbl.Rows.Find(creditor);
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading creditors");
                    }
                    finally
                    {
                        da.Dispose();
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// Define the clients table
        /// </summary>
        protected System.Data.DataRow ReadClient(System.Int32 client)
        {
            System.Data.DataRow answer = null;

            // If the table is not defined then read it from the database.
            System.Data.DataTable tbl = ctx.ds.Tables["clients"];
            if (tbl != null)
            {
                answer = tbl.Rows.Find(client);
            }

            if (answer == null)
            {
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    cmd.CommandText = "SELECT c.client, c.salutation, c.AddressID, c.HomeTelephoneID, c.MsgTelephoneID, c.county, c.region, c.active_status, c.active_status_date, c.dmp_status_date, c.client_status, c.client_status_date, c.disbursement_date, c.mail_error_date, c.start_date, c.restart_date, c.drop_date, c.drop_reason, c.drop_reason_other, c.referred_by, c.cause_fin_problem1, c.cause_fin_problem2, c.cause_fin_problem3, c.cause_fin_problem4, c.office, c.counselor, c.csr, c.hold_disbursements, c.personal_checks, c.ach_active, c.stack_proration, c.mortgage_problems, c.intake_agreement, c.ElectronicCorrespondence, c.ElectronicStatements, c.bankruptcy_class, c.satisfaction_score, c.fed_tax_owed, c.state_tax_owed, c.local_tax_owed, c.fed_tax_months, c.state_tax_months, c.local_tax_months, c.held_in_trust, c.deposit_in_trust, c.reserved_in_trust, c.reserved_in_trust_cutoff, c.marital_status, c.language, c.dependents, c.household, c.method_first_contact, c.preferred_contact, c.config_fee, c.first_appt, c.first_kept_appt, c.first_resched_appt, c.program_months, c.first_deposit_date, c.last_deposit_date, c.last_deposit_amount, c.payout_total_debt, c.payout_months_to_payout, c.payout_total_fees, c.payout_total_interest, c.payout_total_payments, c.payout_deposit_amount, c.payout_monthly_fee, c.payout_cushion_amount, c.payout_termination_date, c.created_by, c.date_created, c.housing_status, c.housing_type, c.people, c.client_guid, dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name FROM clients c WITH (NOLOCK) LEFT OUTER JOIN people p ON c.client = p.client AND 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name WHERE c.client = @client";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@client", SqlDbType.Int).Value = client;

                    System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                    try
                    {
                        da.Fill(ctx.ds, "clients");
                        tbl = ctx.ds.Tables["clients"];
                        if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                        {
                            tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["client"] };
                        }

                        answer = tbl.Rows.Find(client);
                    }
                    catch (System.Data.SqlClient.SqlException ex)
                    {
                        global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading clients");
                    }
                    finally
                    {
                        da.Dispose();
                    }
                }
            }

            return answer;
        }

        protected System.Data.DataTable CreditorTypes()
        {
            const string TableName = "creditor_types";
            System.Data.DataTable tbl = ctx.ds.Tables[TableName];

            if (tbl == null)
            {
                tbl = new System.Data.DataTable("creditor_types");
                var _with5 = tbl;
                _with5.Columns.Add("creditor_type", typeof(string));
                _with5.Columns.Add("description", typeof(string));
                _with5.PrimaryKey = new System.Data.DataColumn[] { _with5.Columns[0] };
                _with5.Rows.Add(new object[] {
                    "N",
                    "None"
                });
                _with5.Rows.Add(new object[] {
                    "D",
                    "Deduct"
                });
                _with5.Rows.Add(new object[] {
                    "M",
                    "Bill Monthly"
                });
                _with5.Rows.Add(new object[] {
                    "Q",
                    "Bill Quarterly"
                });
                _with5.Rows.Add(new object[] {
                    "S",
                    "Bill Semi-Annually"
                });
                _with5.Rows.Add(new object[] {
                    "A",
                    "Bill Annually"
                });
                ctx.ds.Tables.Add(tbl);
            }

            return tbl;
        }
    }
}