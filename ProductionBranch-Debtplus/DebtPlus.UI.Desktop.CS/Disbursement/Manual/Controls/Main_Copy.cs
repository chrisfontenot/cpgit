using System;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.UI.Common;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls
{
    internal partial class Main_Copy
    {
        /// <summary>
        /// Creditor ID used for the form
        /// </summary>
        private string Creditor
        {
            get { return ((Form_ManualDisbursement)this.FindForm()).Creditor; }
            set { ((Form_ManualDisbursement)this.FindForm()).Creditor = value; }
        }

        /// <summary>
        /// Trust Register used for the form
        /// </summary>
        private System.Int32 TrustRegister
        {
            get { return ((Form_ManualDisbursement)this.FindForm()).TrustRegister; }
            set { ((Form_ManualDisbursement)this.FindForm()).TrustRegister = value; }
        }

        /// <summary>
        /// Generate the CANCEL event to the caller
        /// </summary>
        public event EventHandler Cancelled;

        protected void OnCancelled(System.EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, e);
            }
        }

        protected void RaiseCancelled()
        {
            OnCancelled(EventArgs.Empty);
        }

        /// <summary>
        /// Generate the OK event to the caller
        /// </summary>
        public event EventHandler Completed;

        protected void OnCompleted(System.EventArgs e)
        {
            if (Completed != null)
            {
                Completed(this, e);
            }
        }

        protected void RaiseCompleted()
        {
            OnCompleted(EventArgs.Empty);
        }

        /// <summary>
        /// Create the instance of our control
        /// </summary>
        public Main_Copy(Context ctx) : base(ctx)
        {
            InitializeComponent();
            TextEdit1.Validating += TextEdit1_Validating;
            CreditorID1.Validating += CreditorID1_Validating;
            SimpleButton_ok.Click += SimpleButton_OK_Click;
            SimpleButton_cancel.Click += SimpleButton_Cancel_Click;
        }

        /// <summary>
        /// Handle the OK button
        /// </summary>
        private void SimpleButton_OK_Click(object Sender, System.EventArgs e)
        {
            Creditor = CreditorID1.EditValue;
            if (TextEdit1.Text == string.Empty)
                TrustRegister = -1;
            RaiseCompleted();
        }

        /// <summary>
        /// Handle the CANCEL button
        /// </summary>
        private void SimpleButton_Cancel_Click(object Sender, System.EventArgs e)
        {
            RaiseCancelled();
        }

        /// <summary>
        /// Look for errors on the check number entry
        /// </summary>
        private void TextEdit1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string ErrorText = string.Empty;

            // Find the new check number
            string NewCheckNumber = string.Empty;
            if (TextEdit1.EditValue != null && !object.ReferenceEquals(TextEdit1.EditValue, System.DBNull.Value))
                NewCheckNumber = Convert.ToString(TextEdit1.EditValue);

            if (NewCheckNumber != string.Empty)
            {
                // Supress the creditor if a check is given. It will be defined later when it is valid.
                CreditorID1.EditValue = null;

                // The check number was specified. Look it up in the database.
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                System.Data.SqlClient.SqlDataReader rd = null;
                System.Windows.Forms.Cursor current_cursor = System.Windows.Forms.Cursor.Current;
                try
                {
                    System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with1 = cmd;
                        _with1.Connection = cn;
                        _with1.CommandText = "SELECT TOP 1 tr.tran_type, tr.trust_register, tr.creditor FROM registers_trust tr WITH (NOLOCK) INNER JOIN banks b ON tr.bank = b.bank WHERE tr.checknum = @checknum AND tr.cleared = 'V' AND tr.tran_type in ('AD','MD','CR','CM') AND b.type = 'C' ORDER BY tr.date_created desc";
                        _with1.CommandType = CommandType.Text;
                        _with1.Parameters.Add("@checknum", SqlDbType.VarChar, 50).Value = NewCheckNumber;
                        rd = _with1.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);
                    }

                    if (rd == null || !rd.Read())
                    {
                        ErrorText = "check is not VOIDED";
                    }
                    else
                    {
                        string tran_type = string.Empty;
                        System.Int32 trust_register = -1;
                        string creditor = string.Empty;

                        if (!rd.IsDBNull(0))
                            tran_type = Convert.ToString(rd.GetValue(0));
                        if (!rd.IsDBNull(1))
                            trust_register = Convert.ToInt32(rd.GetValue(1));
                        if (!rd.IsDBNull(2))
                            creditor = Convert.ToString(rd.GetValue(2));

                        if (tran_type == "CR")
                        {
                            ErrorText = "do not use a client REFUND check";
                        }
                        else
                        {
                            TrustRegister = trust_register;
                            this.Creditor = creditor;
                            CreditorID1.EditValue = creditor;
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading trust register");
                }
                finally
                {
                    if (rd != null)
                        rd.Dispose();
                    if (cn != null)
                        cn.Dispose();
                    System.Windows.Forms.Cursor.Current = current_cursor;
                }
            }

            // Set the error condition
            TextEdit1.ErrorText = ErrorText;
            SimpleButton_ok.Enabled = this.Creditor != string.Empty && ErrorText == string.Empty;
        }

        private void CreditorID1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string ErrorText = string.Empty;

            // Clear the creditor information
            this.Creditor = string.Empty;

            // Find the new check number
            string NewCreditor = string.Empty;
            if (CreditorID1.EditValue != null && !object.ReferenceEquals(CreditorID1.EditValue, System.DBNull.Value))
                NewCreditor = Convert.ToString(CreditorID1.EditValue);
            if (NewCreditor != string.Empty)
            {
                System.Data.DataRow row = ReadCreditor(NewCreditor);
                if (row != null)
                {
                    this.Creditor = NewCreditor;
                }
            }

            // If there is no creditor then reject the entry
            if (this.Creditor == string.Empty)
                ErrorText = "Invalid Creditor";

            // Set the error condition
            CreditorID1.ErrorText = ErrorText;
            SimpleButton_ok.Enabled = this.Creditor != string.Empty && ErrorText == string.Empty;
        }
    }
}