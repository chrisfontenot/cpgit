using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls
{
    public partial class Transaction : object, System.ComponentModel.INotifyPropertyChanged, System.ComponentModel.ISupportInitialize, System.ComponentModel.IEditableObject
    {
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public Transaction() : base()
        {
        }

        private struct StorageTemplate
        {
            public Int32 client_creditor_register;
            public Int32 client;
            public Int32 client_creditor;
            public string name;
            public decimal debit_amt;
            public string creditor;
            public string account_number;
            public decimal fairshare_amt;
            public double fairshare_pct;
            public string creditor_type;
            public Int32 @void;
        }

        private StorageTemplate privateStorage = new StorageTemplate();
        private StorageTemplate privateUndoStorage = new StorageTemplate();
        private bool InEdit;
        private bool InInit;

        private Context ctx = null;

        public Transaction(Context ctx) : base()
        {
            this.ctx = ctx;
        }

        protected void DoSetProperty(string PropertyName)
        {
            if (!InInit)
            {
                var evt = PropertyChanged;
                if (evt != null)
                {
                    evt(this, new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
                }
            }
        }

        public void BeginInit()
        {
            InInit = true;
        }

        public void EndInit()
        {
            InInit = false;
        }

        public Int32 @void
        {
            get
            {
                return privateStorage.@void;
            }
            set
            {
                if (value != privateStorage.@void)
                {
                    privateStorage.@void = value;
                    DoSetProperty("void");
                }
            }
        }

        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(creditor))
                {
                    return false;
                }

                if (client <= 0)
                {
                    return false;
                }

                if (client_creditor <= 0)
                {
                    return false;
                }

                if (debit_amt <= 0m)
                {
                    return false;
                }

                if (fairshare_pct >= 1.0)
                {
                    return false;
                }

                return true;
            }
        }

        public Int32 client_creditor_register
        {
            get
            {
                return privateStorage.client_creditor_register;
            }
            set
            {
                if (value != privateStorage.client_creditor_register)
                {
                    privateStorage.client_creditor_register = value;
                    DoSetProperty("client_creditor_register");
                }
            }
        }

        public Int32 client
        {
            get { return privateStorage.client; }
            set
            {
                if (value != privateStorage.client)
                {
                    privateStorage.client = value;
                    DoSetProperty("client");

                    // Set the client name when we have a ClientId
                    if (!InInit)
                    {
                        ReadClientName();
                    }
                }
            }
        }

        public Int32 client_creditor
        {
            get { return privateStorage.client_creditor; }
            set
            {
                if (value != privateStorage.client_creditor)
                {
                    privateStorage.client_creditor = value;
                    DoSetProperty("client_creditor");

                    // Change the account number if needed
                    if (!InInit)
                    {
                        const string TableName = "client_creditor";
                        DataTable tbl = ctx.ds.Tables[TableName];
                        if (tbl != null)
                        {
                            DataRow row = tbl.Rows.Find(client_creditor);
                            if (row != null)
                            {
                                account_number = global::DebtPlus.Utils.Nulls.DStr(row["account_number"]);
                            }
                        }
                    }
                }
            }
        }

        public string name
        {
            get { return privateStorage.name; }
            set
            {
                if (name != value)
                {
                    privateStorage.name = value;
                    DoSetProperty("name");
                }
            }
        }

        public decimal debit_amt
        {
            get { return privateStorage.debit_amt; }
            set
            {
                if (value != privateStorage.debit_amt)
                {
                    if (!InInit && value <= 0m)
                    {
                        throw new DataException("The amount must be greater than $0.00");
                    }

                    privateStorage.debit_amt = value;
                    DoSetProperty("debit_amt");
                    RecalculateFairshareAmt();
                }
            }
        }

        public string creditor
        {
            get { return privateStorage.creditor; }
            set
            {
                if (value != privateStorage.creditor)
                {
                    privateStorage.creditor = value;
                    DoSetProperty("creditor");
                }
            }
        }

        public string account_number
        {
            get { return privateStorage.account_number; }
            set
            {
                if (value != privateStorage.account_number)
                {
                    privateStorage.account_number = value;
                    DoSetProperty("account_number");
                }
            }
        }

        public decimal fairshare_amt
        {
            get { return privateStorage.fairshare_amt; }
            set
            {
                if (value != privateStorage.fairshare_amt)
                {
                    privateStorage.fairshare_amt = value;
                    DoSetProperty("fairshare_amt");
                }
            }
        }

        public double fairshare_pct
        {
            get { return privateStorage.fairshare_pct; }
            set
            {
                if (value != privateStorage.fairshare_pct)
                {
                    privateStorage.fairshare_pct = value;
                    DoSetProperty("fairshare_pct");
                    RecalculateFairshareAmt();
                }
            }
        }

        public double Recorded_farishare_pct
        {
            get
            {
                if (creditor_type == "N")
                    return 0.0;
                return fairshare_pct;
            }
        }

        public decimal deducted
        {
            get
            {
                if (creditor_type == "D")
                {
                    return fairshare_amt;
                }
                return 0m;
            }
        }

        public decimal billed
        {
            get
            {
                if (creditor_type == "D" || creditor_type == "N")
                {
                    return 0m;
                }
                return fairshare_amt;
            }
        }

        public decimal net_amount
        {
            get { return debit_amt - deducted; }
        }

        public string creditor_type
        {
            get { return privateStorage.creditor_type; }
            set
            {
                if (value != privateStorage.creditor_type)
                {
                    privateStorage.creditor_type = value;
                    DoSetProperty("creditor_type");
                    RecalculateFairshareAmt();
                }
            }
        }

        public string Recorded_creditor_type
        {
            get
            {
                if (creditor_type != "N" && fairshare_pct <= 0.0)
                    return "N";
                return creditor_type;
            }
        }

        public void BeginEdit()
        {
            privateUndoStorage = privateStorage;
            InEdit = true;
        }

        public void CancelEdit()
        {
            if (InEdit)
            {
                privateStorage = privateUndoStorage;
                InEdit = false;
            }
        }

        public void EndEdit()
        {
            InEdit = false;
        }

        public void RecalculateFairshareAmt()
        {
            if (!InInit)
            {
                if (creditor_type == "N")
                {
                    fairshare_amt = 0m;
                }
                else
                {
                    fairshare_amt = System.Decimal.Truncate(Convert.ToDecimal(Convert.ToDouble(debit_amt) * fairshare_pct) * 100M) / 100M;
                }
            }
        }

        public void ReadClientName()
        {
            string newName = string.Empty;
            if (client > 0)
            {
                Int32 nameID = -1;
                DataRow row = ReadApplicant();
                if (row != null)
                {
                    if (row["NameID"] != null && !object.ReferenceEquals(row["NameID"], DBNull.Value))
                    {
                        nameID = Convert.ToInt32(row["NameID"], System.Globalization.CultureInfo.InvariantCulture);
                    }
                }

                if (nameID > 0)
                {
                    row = ReadName(nameID);
                    if (row != null)
                    {
                        newName = DebtPlus.LINQ.Name.FormatNormalName(row["prefix"], row["first"], row["middle"], row["last"], row["suffix"]);
                    }
                }
            }
            name = newName;
        }

        public DataRow ReadName(Int32 NameID)
        {
            const string tableName = "names";
            DataRow row = null;

            // If there is a table then look for the row. If found, return it.
            DataTable tbl = ctx.ds.Tables[tableName];
            if (tbl != null)
            {
                row = tbl.Rows.Find(NameID);
                if (row != null)
                {
                    return row;
                }
            }

            // Read the row from the database
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = cn;
                    _with1.CommandText = "SELECT [name],[prefix],[first],[middle],[last],[suffix] FROM [names] WHERE [name]=@name";
                    _with1.CommandType = CommandType.Text;
                    _with1.Parameters.Add("@name", SqlDbType.Int).Value = NameID;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ctx.ds, tableName);
                    }

                    tbl = ctx.ds.Tables[tableName];
                    var _with2 = tbl;
                    if (_with2.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        _with2.PrimaryKey = new DataColumn[] { _with2.Columns["name"] };
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }

            // Now, find the row. If the row is still not there then there is no name.
            // If there is no name then record the fact that the name is blank so that we don't do this again.
            row = tbl.Rows.Find(NameID);
            if (row == null)
            {
                row = tbl.NewRow();
                row["name"] = NameID;
                tbl.Rows.Add(row);
                row.AcceptChanges();
                // Prevent the row from being written should we mistakenly update the table.
            }

            return row;
        }

        public DataRow ReadApplicant()
        {
            const string TableName = "people";
            DataRow[] rows = null;

            // If there is a table then look for the row. If found, return it.
            DataTable tbl = ctx.ds.Tables[TableName];
            if (tbl != null)
            {
                rows = tbl.Select(string.Format("[client]={0:f0} AND [relation]=1", client), string.Empty);
                if (rows.GetUpperBound(0) >= 0)
                {
                    return rows[0];
                }
            }

            // Read the row from the database
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with3 = cmd;
                    _with3.Connection = cn;
                    _with3.CommandText = "SELECT [person],[client],[relation],[NameID] FROM [people] WHERE [client]=@client AND [relation]=1";
                    _with3.CommandType = CommandType.Text;
                    _with3.Parameters.Add("@client", SqlDbType.Int).Value = client;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ctx.ds, TableName);
                    }

                    tbl = ctx.ds.Tables[TableName];
                    var _with4 = tbl;
                    if (_with4.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        _with4.PrimaryKey = new DataColumn[] { _with4.Columns["person"] };
                    }

                    var _with5 = _with4.Columns["person"];
                    if (!_with5.AutoIncrement)
                    {
                        _with5.AutoIncrement = true;
                        _with5.AutoIncrementSeed = -1;
                        _with5.AutoIncrementStep = -1;
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }

            // Now, find the row. If the row is still not there then there is no name.
            // If there is no name then record the fact that the name is blank so that we don't do this again.
            rows = tbl.Select(string.Format("[client]={0:f0} AND [relation]=1", client), string.Empty);
            if (rows.GetUpperBound(0) >= 0)
            {
                return rows[0];
            }

            // Add a blank row to the table so that we don't look again for this person.
            DataRow row = tbl.NewRow();
            row["client"] = client;
            row["relation"] = 1;
            row["NameID"] = DBNull.Value;
            tbl.Rows.Add(row);
            row.AcceptChanges();

            return row;
        }
    }
}