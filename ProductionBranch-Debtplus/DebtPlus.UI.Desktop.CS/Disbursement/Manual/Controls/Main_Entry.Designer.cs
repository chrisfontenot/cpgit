using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using DebtPlus.UI.Client.Widgets.Controls;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls
{
	partial class Main_Entry : BaseControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Entry));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.GridColumn_IsValid = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_void = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_Client = new DevExpress.XtraGrid.Columns.GridColumn();
			this.RepositoryItemTextEdit_client = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
			this.GridColumn_Name = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_Debit_Amt = new DevExpress.XtraGrid.Columns.GridColumn();
			this.RepositoryItemCalcEdit_debit_amt = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
			this.GridColumn_Contribution = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_account_number = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_Rate = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_ID = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_Deducted = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_Billed = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_Net_Amount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_creditor_type = new DevExpress.XtraGrid.Columns.GridColumn();
			this.RepositoryItemLookUpEdit_creditor_type = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
			this.LookUpEdit_client_creditor = new DevExpress.XtraEditors.LookUpEdit();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.ClientID1 = new global::DebtPlus.UI.Client.Widgets.Controls.ClientID();
			this.LabelControl_client_name = new DevExpress.XtraEditors.LabelControl();
			this.SimpleButton_Post = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_add = new DevExpress.XtraEditors.SimpleButton();
			this.CalcEdit_amount = new DevExpress.XtraEditors.CalcEdit();
			this.SimpleButton_del = new DevExpress.XtraEditors.SimpleButton();
			this.ComboBoxEdit_reason = new DevExpress.XtraEditors.ComboBoxEdit();
			this.CalcEdit_rate = new DevExpress.XtraEditors.CalcEdit();
			this.ComboBoxEdit_disposition = new DevExpress.XtraEditors.ComboBoxEdit();
			this.LabelControl_creditor = new DevExpress.XtraEditors.LabelControl();
			this.LookupEdit_contribution = new DevExpress.XtraEditors.LookUpEdit();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.LayoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemTextEdit_client).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemCalcEdit_debit_amt).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemLookUpEdit_creditor_type).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_client_creditor.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_amount.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_reason.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_rate.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_disposition.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit_contribution.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).BeginInit();
			this.SuspendLayout();
			//
			//GridColumn_IsValid
			//
			this.GridColumn_IsValid.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_IsValid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_IsValid.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_IsValid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_IsValid.Caption = "Valid?";
			this.GridColumn_IsValid.FieldName = "IsValid";
			this.GridColumn_IsValid.Name = "GridColumn_IsValid";
			this.GridColumn_IsValid.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//GridColumn_void
			//
			this.GridColumn_void.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_void.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_void.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_void.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_void.Caption = "Deleted";
			this.GridColumn_void.CustomizationCaption = "Item Deleted";
			this.GridColumn_void.FieldName = "void";
			this.GridColumn_void.Name = "GridColumn_void";
			this.GridColumn_void.OptionsColumn.AllowEdit = false;
			this.GridColumn_void.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_void.OptionsFilter.AllowFilter = false;
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.Location = new System.Drawing.Point(12, 133);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
				this.RepositoryItemTextEdit_client,
				this.RepositoryItemCalcEdit_debit_amt,
				this.RepositoryItemLookUpEdit_creditor_type
			});
			this.GridControl1.Size = new System.Drawing.Size(559, 248);
			this.GridControl1.TabIndex = 19;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
			this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.Empty.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
			this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
			this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
			this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
			this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
			this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
			this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
			this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.Preview.Options.UseFont = true;
			this.GridView1.Appearance.Preview.Options.UseForeColor = true;
			this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.Row.Options.UseBackColor = true;
			this.GridView1.Appearance.Row.Options.UseForeColor = true;
			this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
			this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_Client,
				this.GridColumn_Name,
				this.GridColumn_Debit_Amt,
				this.GridColumn_Contribution,
				this.GridColumn_account_number,
				this.GridColumn_Rate,
				this.GridColumn_ID,
				this.GridColumn_Deducted,
				this.GridColumn_Billed,
				this.GridColumn_Net_Amount,
				this.GridColumn_creditor_type,
				this.GridColumn_void,
				this.GridColumn_IsValid
			});
			StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(128), (Int32)Convert.ToByte(128));
			StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(128), (Int32)Convert.ToByte(128));
			StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(255), (Int32)Convert.ToByte(128), (Int32)Convert.ToByte(128));
			StyleFormatCondition1.Appearance.Options.UseBackColor = true;
			StyleFormatCondition1.Appearance.Options.UseBorderColor = true;
			StyleFormatCondition1.ApplyToRow = true;
			StyleFormatCondition1.Column = this.GridColumn_IsValid;
			StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Equal;
			StyleFormatCondition1.Value1 = false;
			this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] { StyleFormatCondition1 });
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsView.ShowFooter = true;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			//
			//GridColumn_Client
			//
			this.GridColumn_Client.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Client.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Client.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Client.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Client.Caption = "Client";
			this.GridColumn_Client.ColumnEdit = this.RepositoryItemTextEdit_client;
			this.GridColumn_Client.FieldName = "client";
			this.GridColumn_Client.Name = "GridColumn_Client";
			this.GridColumn_Client.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_Client.SummaryItem.DisplayFormat = "{0:n0} items";
			this.GridColumn_Client.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
			this.GridColumn_Client.Visible = true;
			this.GridColumn_Client.VisibleIndex = 0;
			this.GridColumn_Client.Width = 51;
			//
			//RepositoryItemTextEdit_client
			//
			this.RepositoryItemTextEdit_client.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.RepositoryItemTextEdit_client.AutoHeight = false;
			this.RepositoryItemTextEdit_client.DisplayFormat.FormatString = "f0";
			this.RepositoryItemTextEdit_client.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.RepositoryItemTextEdit_client.EditFormat.FormatString = "f0";
			this.RepositoryItemTextEdit_client.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemTextEdit_client.Mask.BeepOnError = true;
			this.RepositoryItemTextEdit_client.Mask.EditMask = "\\d+";
			this.RepositoryItemTextEdit_client.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.RepositoryItemTextEdit_client.Name = "RepositoryItemTextEdit_client";
			//
			//GridColumn_Name
			//
			this.GridColumn_Name.Caption = "Name";
			this.GridColumn_Name.FieldName = "name";
			this.GridColumn_Name.Name = "GridColumn_Name";
			this.GridColumn_Name.OptionsColumn.AllowEdit = false;
			this.GridColumn_Name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_Name.Visible = true;
			this.GridColumn_Name.VisibleIndex = 1;
			this.GridColumn_Name.Width = 151;
			//
			//GridColumn_Debit_Amt
			//
			this.GridColumn_Debit_Amt.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Debit_Amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Debit_Amt.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Debit_Amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Debit_Amt.Caption = "Gross";
			this.GridColumn_Debit_Amt.ColumnEdit = this.RepositoryItemCalcEdit_debit_amt;
			this.GridColumn_Debit_Amt.DisplayFormat.FormatString = "c";
			this.GridColumn_Debit_Amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Debit_Amt.FieldName = "debit_amt";
			this.GridColumn_Debit_Amt.GroupFormat.FormatString = "c";
			this.GridColumn_Debit_Amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Debit_Amt.Name = "GridColumn_Debit_Amt";
			this.GridColumn_Debit_Amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_Debit_Amt.SummaryItem.DisplayFormat = "{0:c}";
			this.GridColumn_Debit_Amt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.GridColumn_Debit_Amt.Visible = true;
			this.GridColumn_Debit_Amt.VisibleIndex = 3;
			this.GridColumn_Debit_Amt.Width = 72;
			//
			//RepositoryItemCalcEdit_debit_amt
			//
			this.RepositoryItemCalcEdit_debit_amt.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.RepositoryItemCalcEdit_debit_amt.AutoHeight = false;
			this.RepositoryItemCalcEdit_debit_amt.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.RepositoryItemCalcEdit_debit_amt.DisplayFormat.FormatString = "c2";
			this.RepositoryItemCalcEdit_debit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemCalcEdit_debit_amt.EditFormat.FormatString = "c2";
			this.RepositoryItemCalcEdit_debit_amt.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemCalcEdit_debit_amt.Mask.BeepOnError = true;
			this.RepositoryItemCalcEdit_debit_amt.Mask.EditMask = "c2";
			this.RepositoryItemCalcEdit_debit_amt.Name = "RepositoryItemCalcEdit_debit_amt";
			this.RepositoryItemCalcEdit_debit_amt.Precision = 2;
			//
			//GridColumn_Contribution
			//
			this.GridColumn_Contribution.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Contribution.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Contribution.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Contribution.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Contribution.Caption = "Fairshare";
			this.GridColumn_Contribution.DisplayFormat.FormatString = "c";
			this.GridColumn_Contribution.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Contribution.FieldName = "fairshare_amt";
			this.GridColumn_Contribution.GroupFormat.FormatString = "c";
			this.GridColumn_Contribution.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Contribution.Name = "GridColumn_Contribution";
			this.GridColumn_Contribution.OptionsColumn.AllowEdit = false;
			this.GridColumn_Contribution.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_Contribution.SummaryItem.DisplayFormat = "{0:c}";
			this.GridColumn_Contribution.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.GridColumn_Contribution.Visible = true;
			this.GridColumn_Contribution.VisibleIndex = 4;
			this.GridColumn_Contribution.Width = 84;
			//
			//GridColumn_account_number
			//
			this.GridColumn_account_number.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_account_number.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_account_number.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_account_number.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_account_number.Caption = "Acct Num";
			this.GridColumn_account_number.CustomizationCaption = "Account Number";
			this.GridColumn_account_number.FieldName = "account_number";
			this.GridColumn_account_number.Name = "GridColumn_account_number";
			this.GridColumn_account_number.OptionsColumn.AllowEdit = false;
			this.GridColumn_account_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_account_number.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
			this.GridColumn_account_number.Visible = true;
			this.GridColumn_account_number.VisibleIndex = 2;
			this.GridColumn_account_number.Width = 114;
			//
			//GridColumn_Rate
			//
			this.GridColumn_Rate.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Rate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Rate.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Rate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Rate.Caption = "Rate";
			this.GridColumn_Rate.DisplayFormat.FormatString = "{0:p}";
			this.GridColumn_Rate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Rate.FieldName = "fairshare_pct";
			this.GridColumn_Rate.GroupFormat.FormatString = "{0:p}";
			this.GridColumn_Rate.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Rate.Name = "GridColumn_Rate";
			this.GridColumn_Rate.OptionsColumn.AllowEdit = false;
			this.GridColumn_Rate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//GridColumn_ID
			//
			this.GridColumn_ID.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_ID.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_ID.Caption = "ID";
			this.GridColumn_ID.DisplayFormat.FormatString = "{0:f0}";
			this.GridColumn_ID.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_ID.FieldName = "client_creditor_register";
			this.GridColumn_ID.GroupFormat.FormatString = "{0:f0}";
			this.GridColumn_ID.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_ID.Name = "GridColumn_ID";
			this.GridColumn_ID.OptionsColumn.AllowEdit = false;
			this.GridColumn_ID.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//GridColumn_Deducted
			//
			this.GridColumn_Deducted.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Deducted.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Deducted.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Deducted.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Deducted.Caption = "Deducted";
			this.GridColumn_Deducted.CustomizationCaption = "Deducted Contributions only";
			this.GridColumn_Deducted.DisplayFormat.FormatString = "c";
			this.GridColumn_Deducted.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Deducted.FieldName = "deducted";
			this.GridColumn_Deducted.GroupFormat.FormatString = "c";
			this.GridColumn_Deducted.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Deducted.Name = "GridColumn_Deducted";
			this.GridColumn_Deducted.OptionsColumn.AllowEdit = false;
			this.GridColumn_Deducted.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_Deducted.SummaryItem.DisplayFormat = "{0:c}";
			this.GridColumn_Deducted.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			//
			//GridColumn_Billed
			//
			this.GridColumn_Billed.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Billed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Billed.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Billed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Billed.Caption = "Billed";
			this.GridColumn_Billed.CustomizationCaption = "Billed Amount";
			this.GridColumn_Billed.DisplayFormat.FormatString = "c";
			this.GridColumn_Billed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Billed.FieldName = "billed";
			this.GridColumn_Billed.GroupFormat.FormatString = "c";
			this.GridColumn_Billed.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Billed.Name = "GridColumn_Billed";
			this.GridColumn_Billed.OptionsColumn.AllowEdit = false;
			this.GridColumn_Billed.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_Billed.SummaryItem.DisplayFormat = "{0:c}";
			this.GridColumn_Billed.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			//
			//GridColumn_Net_Amount
			//
			this.GridColumn_Net_Amount.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Net_Amount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Net_Amount.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Net_Amount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Net_Amount.Caption = "Net";
			this.GridColumn_Net_Amount.CustomizationCaption = "Net amount for this debt";
			this.GridColumn_Net_Amount.DisplayFormat.FormatString = "{0:c}";
			this.GridColumn_Net_Amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Net_Amount.FieldName = "net_amount";
			this.GridColumn_Net_Amount.GroupFormat.FormatString = "{0:c}";
			this.GridColumn_Net_Amount.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Net_Amount.Name = "GridColumn_Net_Amount";
			this.GridColumn_Net_Amount.OptionsColumn.AllowEdit = false;
			this.GridColumn_Net_Amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_Net_Amount.SummaryItem.DisplayFormat = "{0:c}";
			this.GridColumn_Net_Amount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.GridColumn_Net_Amount.Visible = true;
			this.GridColumn_Net_Amount.VisibleIndex = 5;
			this.GridColumn_Net_Amount.Width = 90;
			//
			//GridColumn_creditor_type
			//
			this.GridColumn_creditor_type.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_creditor_type.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_creditor_type.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_creditor_type.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.GridColumn_creditor_type.Caption = "Type";
			this.GridColumn_creditor_type.ColumnEdit = this.RepositoryItemLookUpEdit_creditor_type;
			this.GridColumn_creditor_type.CustomizationCaption = "Bill/Deduct/None";
			this.GridColumn_creditor_type.FieldName = "creditor_type";
			this.GridColumn_creditor_type.Name = "GridColumn_creditor_type";
			this.GridColumn_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//RepositoryItemLookUpEdit_creditor_type
			//
			this.RepositoryItemLookUpEdit_creditor_type.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.RepositoryItemLookUpEdit_creditor_type.AutoHeight = false;
			this.RepositoryItemLookUpEdit_creditor_type.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.RepositoryItemLookUpEdit_creditor_type.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("creditor_type", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Descripiton", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.RepositoryItemLookUpEdit_creditor_type.DisplayMember = "description";
			this.RepositoryItemLookUpEdit_creditor_type.Name = "RepositoryItemLookUpEdit_creditor_type";
			this.RepositoryItemLookUpEdit_creditor_type.NullText = "";
			this.RepositoryItemLookUpEdit_creditor_type.ShowFooter = false;
			this.RepositoryItemLookUpEdit_creditor_type.ShowHeader = false;
			this.RepositoryItemLookUpEdit_creditor_type.ValueMember = "creditor_type";
			//
			//LookUpEdit_client_creditor
			//
			this.LookUpEdit_client_creditor.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LookUpEdit_client_creditor.Location = new System.Drawing.Point(99, 85);
			this.LookUpEdit_client_creditor.Name = "LookUpEdit_client_creditor";
			this.LookUpEdit_client_creditor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_client_creditor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("client_creditor", "Debt", 20, DevExpress.Utils.FormatType.Numeric, "f0", false, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.Ascending),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("account_number", "Account Number", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookUpEdit_client_creditor.Properties.DisplayMember = "account_number";
			this.LookUpEdit_client_creditor.Properties.NullText = "";
			this.LookUpEdit_client_creditor.Properties.PopupWidth = 140;
			this.LookUpEdit_client_creditor.Properties.ShowFooter = false;
			this.LookUpEdit_client_creditor.Properties.ShowHeader = false;
			this.LookUpEdit_client_creditor.Properties.ValueMember = "client_creditor";
			this.LookUpEdit_client_creditor.Size = new System.Drawing.Size(266, 20);
			this.LookUpEdit_client_creditor.StyleController = this.LayoutControl1;
			this.LookUpEdit_client_creditor.TabIndex = 39;
			this.LookUpEdit_client_creditor.Properties.SortColumnIndex = 1;
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.ClientID1);
			this.LayoutControl1.Controls.Add(this.LabelControl_client_name);
			this.LayoutControl1.Controls.Add(this.SimpleButton_Post);
			this.LayoutControl1.Controls.Add(this.SimpleButton_add);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_client_creditor);
			this.LayoutControl1.Controls.Add(this.CalcEdit_amount);
			this.LayoutControl1.Controls.Add(this.SimpleButton_del);
			this.LayoutControl1.Controls.Add(this.ComboBoxEdit_reason);
			this.LayoutControl1.Controls.Add(this.CalcEdit_rate);
			this.LayoutControl1.Controls.Add(this.ComboBoxEdit_disposition);
			this.LayoutControl1.Controls.Add(this.GridControl1);
			this.LayoutControl1.Controls.Add(this.LabelControl_creditor);
			this.LayoutControl1.Controls.Add(this.LookupEdit_contribution);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(583, 393);
			this.LayoutControl1.TabIndex = 21;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//ClientID1
			//
			this.ClientID1.Location = new System.Drawing.Point(99, 60);
			this.ClientID1.Name = "ClientID1";
			this.ClientID1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.ClientID1.Properties.Appearance.Options.UseTextOptions = true;
			this.ClientID1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.ClientID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("ClientID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a client ID", "search", null, true) });
			this.ClientID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.ClientID1.Properties.DisplayFormat.FormatString = "0000000";
			this.ClientID1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom;
			this.ClientID1.Properties.EditFormat.FormatString = "f0";
			this.ClientID1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.ClientID1.Properties.Mask.BeepOnError = true;
			this.ClientID1.Properties.Mask.EditMask = "\\d*";
			this.ClientID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.ClientID1.Properties.ValidateOnEnterKey = true;
			this.ClientID1.Size = new System.Drawing.Size(109, 20);
			this.ClientID1.StyleController = this.LayoutControl1;
			this.ClientID1.TabIndex = 41;
			//
			//LabelControl_client_name
			//
			this.LabelControl_client_name.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl_client_name.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_client_name.Location = new System.Drawing.Point(216, 64);
			this.LabelControl_client_name.Name = "LabelControl_client_name";
			this.LabelControl_client_name.Size = new System.Drawing.Size(272, 13);
			this.LabelControl_client_name.StyleController = this.LayoutControl1;
			this.LabelControl_client_name.TabIndex = 40;
			//
			//SimpleButton_Post
			//
			this.SimpleButton_Post.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_Post.Location = new System.Drawing.Point(496, 106);
			this.SimpleButton_Post.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_Post.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_Post.Name = "SimpleButton_Post";
			this.SimpleButton_Post.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_Post.StyleController = this.LayoutControl1;
			this.SimpleButton_Post.TabIndex = 37;
			this.SimpleButton_Post.Text = "&Cut Check";
			//
			//SimpleButton_add
			//
			this.SimpleButton_add.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_add.Location = new System.Drawing.Point(496, 12);
			this.SimpleButton_add.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_add.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_add.Name = "SimpleButton_add";
			this.SimpleButton_add.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_add.StyleController = this.LayoutControl1;
			this.SimpleButton_add.TabIndex = 35;
			this.SimpleButton_add.Text = "&New Item";
			//
			//CalcEdit_amount
			//
			this.CalcEdit_amount.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.CalcEdit_amount.Location = new System.Drawing.Point(99, 36);
			this.CalcEdit_amount.Name = "CalcEdit_amount";
			this.CalcEdit_amount.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_amount.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_amount.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_amount.Properties.DisplayFormat.FormatString = "{0:c}";
			this.CalcEdit_amount.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_amount.Properties.EditFormat.FormatString = "{0:f0}";
			this.CalcEdit_amount.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_amount.Properties.Mask.BeepOnError = true;
			this.CalcEdit_amount.Properties.Mask.EditMask = "c";
			this.CalcEdit_amount.Properties.Precision = 2;
			this.CalcEdit_amount.Size = new System.Drawing.Size(109, 20);
			this.CalcEdit_amount.StyleController = this.LayoutControl1;
			this.CalcEdit_amount.TabIndex = 33;
			//
			//SimpleButton_del
			//
			this.SimpleButton_del.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.SimpleButton_del.Location = new System.Drawing.Point(496, 39);
			this.SimpleButton_del.MaximumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_del.MinimumSize = new System.Drawing.Size(75, 23);
			this.SimpleButton_del.Name = "SimpleButton_del";
			this.SimpleButton_del.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_del.StyleController = this.LayoutControl1;
			this.SimpleButton_del.TabIndex = 36;
			this.SimpleButton_del.Text = "&Remove Item";
			//
			//ComboBoxEdit_reason
			//
			this.ComboBoxEdit_reason.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.ComboBoxEdit_reason.Location = new System.Drawing.Point(99, 109);
			this.ComboBoxEdit_reason.Name = "ComboBoxEdit_reason";
			this.ComboBoxEdit_reason.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit_reason.Size = new System.Drawing.Size(393, 20);
			this.ComboBoxEdit_reason.StyleController = this.LayoutControl1;
			this.ComboBoxEdit_reason.TabIndex = 26;
			//
			//CalcEdit_rate
			//
			this.CalcEdit_rate.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.CalcEdit_rate.Location = new System.Drawing.Point(99, 12);
			this.CalcEdit_rate.Name = "CalcEdit_rate";
			this.CalcEdit_rate.Properties.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_rate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_rate.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.CalcEdit_rate.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_rate.Properties.AppearanceDropDown.Options.UseTextOptions = true;
			this.CalcEdit_rate.Properties.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_rate.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.CalcEdit_rate.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_rate.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.CalcEdit_rate.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_rate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_rate.Properties.DisplayFormat.FormatString = "{0:p}";
			this.CalcEdit_rate.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_rate.Properties.EditFormat.FormatString = "{0:f4}";
			this.CalcEdit_rate.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_rate.Properties.Mask.BeepOnError = true;
			this.CalcEdit_rate.Properties.Mask.EditMask = "p";
			this.CalcEdit_rate.Properties.Precision = 4;
			this.CalcEdit_rate.Size = new System.Drawing.Size(109, 20);
			this.CalcEdit_rate.StyleController = this.LayoutControl1;
			this.CalcEdit_rate.TabIndex = 22;
			//
			//ComboBoxEdit_disposition
			//
			this.ComboBoxEdit_disposition.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.ComboBoxEdit_disposition.Location = new System.Drawing.Point(299, 36);
			this.ComboBoxEdit_disposition.Name = "ComboBoxEdit_disposition";
			this.ComboBoxEdit_disposition.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.ComboBoxEdit_disposition.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.ComboBoxEdit_disposition.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
			this.ComboBoxEdit_disposition.Size = new System.Drawing.Size(193, 20);
			this.ComboBoxEdit_disposition.StyleController = this.LayoutControl1;
			this.ComboBoxEdit_disposition.TabIndex = 24;
			//
			//LabelControl_creditor
			//
			this.LabelControl_creditor.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl_creditor.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl_creditor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl_creditor.Location = new System.Drawing.Point(369, 85);
			this.LabelControl_creditor.Name = "LabelControl_creditor";
			this.LabelControl_creditor.Size = new System.Drawing.Size(123, 13);
			this.LabelControl_creditor.StyleController = this.LayoutControl1;
			this.LabelControl_creditor.TabIndex = 34;
			//
			//LookupEdit_contribution
			//
			this.LookupEdit_contribution.Location = new System.Drawing.Point(299, 12);
			this.LookupEdit_contribution.Name = "LookupEdit_contribution";
			this.LookupEdit_contribution.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookupEdit_contribution.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("creditor_type", "ID", 20, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Default),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("description", "Description", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)
			});
			this.LookupEdit_contribution.Properties.DisplayMember = "description";
			this.LookupEdit_contribution.Properties.NullText = "";
			this.LookupEdit_contribution.Properties.PopupSizeable = false;
			this.LookupEdit_contribution.Properties.ShowFooter = false;
			this.LookupEdit_contribution.Properties.ShowHeader = false;
			this.LookupEdit_contribution.Properties.ValueMember = "creditor_type";
			this.LookupEdit_contribution.Size = new System.Drawing.Size(193, 20);
			this.LookupEdit_contribution.StyleController = this.LayoutControl1;
			this.LookupEdit_contribution.TabIndex = 20;
			this.LookupEdit_contribution.Properties.SortColumnIndex = 1;
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem6,
				this.LayoutControlItem8,
				this.LayoutControlItem9,
				this.LayoutControlItem11,
				this.LayoutControlItem12,
				this.LayoutControlItem10,
				this.EmptySpaceItem1,
				this.LayoutControlItem7,
				this.LayoutControlItem13,
				this.LayoutControlItem3,
				this.LayoutControlItem5,
				this.LayoutControlItem14
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(583, 393);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.GridControl1;
			this.LayoutControlItem1.CustomizationFormText = "LayoutControlItem1";
			this.LayoutControlItem1.Location = new System.Drawing.Point(0, 121);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(563, 252);
			this.LayoutControlItem1.Text = "LayoutControlItem1";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem1.TextToControlDistance = 0;
			this.LayoutControlItem1.TextVisible = false;
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.LookUpEdit_client_creditor;
			this.LayoutControlItem2.CustomizationFormText = "Account Number";
			this.LayoutControlItem2.Location = new System.Drawing.Point(0, 73);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(357, 24);
			this.LayoutControlItem2.Text = "Account Number";
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(83, 13);
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.ComboBoxEdit_disposition;
			this.LayoutControlItem6.CustomizationFormText = "Check Disposition";
			this.LayoutControlItem6.Location = new System.Drawing.Point(200, 24);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(284, 24);
			this.LayoutControlItem6.Text = "Check Disposition";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(83, 13);
			//
			//LayoutControlItem8
			//
			this.LayoutControlItem8.Control = this.CalcEdit_rate;
			this.LayoutControlItem8.CustomizationFormText = "Rate";
			this.LayoutControlItem8.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem8.Name = "LayoutControlItem8";
			this.LayoutControlItem8.Size = new System.Drawing.Size(200, 24);
			this.LayoutControlItem8.Text = "Rate";
			this.LayoutControlItem8.TextSize = new System.Drawing.Size(83, 13);
			//
			//LayoutControlItem9
			//
			this.LayoutControlItem9.Control = this.LabelControl_creditor;
			this.LayoutControlItem9.CustomizationFormText = "LayoutControlItem9";
			this.LayoutControlItem9.Location = new System.Drawing.Point(357, 73);
			this.LayoutControlItem9.Name = "LayoutControlItem9";
			this.LayoutControlItem9.Size = new System.Drawing.Size(127, 24);
			this.LayoutControlItem9.Text = "LayoutControlItem9";
			this.LayoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem9.TextToControlDistance = 0;
			this.LayoutControlItem9.TextVisible = false;
			//
			//LayoutControlItem11
			//
			this.LayoutControlItem11.Control = this.SimpleButton_add;
			this.LayoutControlItem11.CustomizationFormText = "LayoutControlItem11";
			this.LayoutControlItem11.Location = new System.Drawing.Point(484, 0);
			this.LayoutControlItem11.Name = "LayoutControlItem11";
			this.LayoutControlItem11.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem11.Text = "LayoutControlItem11";
			this.LayoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem11.TextToControlDistance = 0;
			this.LayoutControlItem11.TextVisible = false;
			//
			//LayoutControlItem12
			//
			this.LayoutControlItem12.Control = this.SimpleButton_del;
			this.LayoutControlItem12.CustomizationFormText = "LayoutControlItem12";
			this.LayoutControlItem12.Location = new System.Drawing.Point(484, 27);
			this.LayoutControlItem12.Name = "LayoutControlItem12";
			this.LayoutControlItem12.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem12.Text = "LayoutControlItem12";
			this.LayoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem12.TextToControlDistance = 0;
			this.LayoutControlItem12.TextVisible = false;
			//
			//LayoutControlItem10
			//
			this.LayoutControlItem10.Control = this.SimpleButton_Post;
			this.LayoutControlItem10.CustomizationFormText = "LayoutControlItem10";
			this.LayoutControlItem10.Location = new System.Drawing.Point(484, 94);
			this.LayoutControlItem10.Name = "LayoutControlItem10";
			this.LayoutControlItem10.Size = new System.Drawing.Size(79, 27);
			this.LayoutControlItem10.Text = "LayoutControlItem10";
			this.LayoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem10.TextToControlDistance = 0;
			this.LayoutControlItem10.TextVisible = false;
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(484, 54);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(79, 40);
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//LayoutControlItem7
			//
			this.LayoutControlItem7.Control = this.LookupEdit_contribution;
			this.LayoutControlItem7.CustomizationFormText = "Contribution";
			this.LayoutControlItem7.Location = new System.Drawing.Point(200, 0);
			this.LayoutControlItem7.Name = "LayoutControlItem7";
			this.LayoutControlItem7.Size = new System.Drawing.Size(284, 24);
			this.LayoutControlItem7.Text = "Contribution";
			this.LayoutControlItem7.TextSize = new System.Drawing.Size(83, 13);
			//
			//LayoutControlItem13
			//
			this.LayoutControlItem13.Control = this.LabelControl_client_name;
			this.LayoutControlItem13.CustomizationFormText = "LayoutControlItem13";
			this.LayoutControlItem13.Location = new System.Drawing.Point(200, 48);
			this.LayoutControlItem13.Name = "LayoutControlItem13";
			this.LayoutControlItem13.Size = new System.Drawing.Size(284, 25);
			this.LayoutControlItem13.Spacing = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
			this.LayoutControlItem13.Text = "LayoutControlItem13";
			this.LayoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem13.TextToControlDistance = 0;
			this.LayoutControlItem13.TextVisible = false;
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.CalcEdit_amount;
			this.LayoutControlItem3.CustomizationFormText = "Amount";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 24);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(200, 24);
			this.LayoutControlItem3.Text = "Amount";
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(83, 13);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.ComboBoxEdit_reason;
			this.LayoutControlItem5.CustomizationFormText = "Reason";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 97);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(484, 24);
			this.LayoutControlItem5.Text = "Reason";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(83, 13);
			//
			//LayoutControlItem14
			//
			this.LayoutControlItem14.Control = this.ClientID1;
			this.LayoutControlItem14.CustomizationFormText = "Client";
			this.LayoutControlItem14.Location = new System.Drawing.Point(0, 48);
			this.LayoutControlItem14.Name = "LayoutControlItem14";
			this.LayoutControlItem14.Size = new System.Drawing.Size(200, 25);
			this.LayoutControlItem14.Text = "Client";
			this.LayoutControlItem14.TextSize = new System.Drawing.Size(83, 13);
			//
			//Main_Entry
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.LayoutControl1);
			this.Name = "Main_Entry";
			this.Size = new System.Drawing.Size(583, 393);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemTextEdit_client).EndInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemCalcEdit_debit_amt).EndInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemLookUpEdit_creditor_type).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_client_creditor.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.ClientID1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_amount.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_reason.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_rate.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.ComboBoxEdit_disposition.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookupEdit_contribution.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem8).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem9).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem11).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem12).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem10).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem7).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem13).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem14).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Client;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Name;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Debit_Amt;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Contribution;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_account_number;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Rate;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_ID;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Deducted;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Net_Amount;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_creditor_type;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_Post;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_amount;
		protected internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_reason;
		protected internal DevExpress.XtraEditors.ComboBoxEdit ComboBoxEdit_disposition;
		protected internal DevExpress.XtraEditors.CalcEdit CalcEdit_rate;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_del;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_add;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_creditor;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_client_creditor;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Billed;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem7;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem8;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem9;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem10;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem11;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem12;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_client_name;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem13;
		protected internal global::DebtPlus.UI.Client.Widgets.Controls.ClientID ClientID1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem14;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_void;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_IsValid;
		protected internal DevExpress.XtraEditors.LookUpEdit LookupEdit_contribution;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemTextEdit RepositoryItemTextEdit_client;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit RepositoryItemCalcEdit_debit_amt;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit RepositoryItemLookUpEdit_creditor_type;
	}
}
