#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Windows.Forms;
using DebtPlus.UI.Common;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls
{
    internal partial class Main_Entry
    {
        // List of the transactions for the current disbursement batch
        public List<Transaction> TransactionsTable = new List<Transaction>();

        /// <summary>
        /// Handle the new event creation
        /// </summary>
        public Main_Entry(Context ctx) : base(ctx)
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            Load += MyGridControl_Load;
            ClientID1.EditValueChanged += ClientID1_EditValueChanged;
            SimpleButton_add.Click += SimpleButton_Add_Click;
            SimpleButton_del.Click += SimpleButton_Delete_Click;
            SimpleButton_Post.Click += SimpleButton_Post_Click;
            GridView1.Layout += LayoutChanged;
            GridView1.FocusedRowChanged += BindEntryControls;
        }

        private void DeRegisterHandlers()
        {
            Load -= MyGridControl_Load;
            ClientID1.EditValueChanged -= ClientID1_EditValueChanged;
            SimpleButton_add.Click -= SimpleButton_Add_Click;
            SimpleButton_del.Click -= SimpleButton_Delete_Click;
            SimpleButton_Post.Click -= SimpleButton_Post_Click;
            GridView1.Layout -= LayoutChanged;
            GridView1.FocusedRowChanged -= BindEntryControls;
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string basePath = string.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar);
            return System.IO.Path.Combine(basePath, "Disbursement.Manual");
        }

        /// <summary>
        /// When loading the control, restore the layout from the file
        /// </summary>
        private void MyGridControl_Load(object sender, EventArgs e)
        {
            string pathName = XMLBasePath();
            string fileName = System.IO.Path.Combine(pathName, Name + ".Grid.xml");

            DeRegisterHandlers();
            try
            {
                if (System.IO.File.Exists(fileName))
                {
                    GridView1.RestoreLayoutFromXml(fileName);
                }
            }

#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168
            finally
            {
                GridView1.ActiveFilterString = "[void] = 0";
                GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
                RegisterHandlers();
            }
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            string pathName = XMLBasePath();
            if (!System.IO.Directory.Exists(pathName))
            {
                System.IO.Directory.CreateDirectory(pathName);
            }

            string fileName = System.IO.Path.Combine(pathName, Name + ".Grid.xml");
            GridView1.SaveLayoutToXml(fileName);
        }

        /// <summary>
        /// Generate the CANCEL event to the caller
        /// </summary>
        public event EventHandler Cancelled;

        protected void OnCancelled(EventArgs e)
        {
            if (Cancelled != null)
            {
                Cancelled(this, e);
            }
        }

        protected void RaiseCancelled()
        {
            OnCancelled(EventArgs.Empty);
        }

        /// <summary>
        /// Generate the OK event to the caller
        /// </summary>
        public event EventHandler Completed;

        protected void OnCompleted(EventArgs e)
        {
            if (Completed != null)
            {
                Completed(this, e);
            }
        }

        protected void RaiseCompleted()
        {
            OnCompleted(EventArgs.Empty);
        }

        /// <summary>
        /// Creditor ID used for the form
        /// </summary>
        private string Creditor
        {
            get { return ((Form_ManualDisbursement)FindForm()).Creditor; }
        }

        /// <summary>
        /// Trust Register used for the form
        /// </summary>
        private Int32 TrustRegister
        {
            get { return ((Form_ManualDisbursement)FindForm()).TrustRegister; }
        }

        private void Load_ComboboxEdit_contribution()
        {
            var _with1 = LookupEdit_contribution;
            _with1.Properties.DataSource = CreditorTypes().DefaultView;
        }

        private void Load_ComboBoxEdit_disposition()
        {
            var _with2 = ComboBoxEdit_disposition;
            var _with3 = _with2.Properties;
            var _with4 = _with3.Items;
            _with4.Clear();
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Save the check for the next print run", "QUEUE"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Print the check immediately", "PRINT"));
            _with4.Add(new DebtPlus.Data.Controls.ComboboxItem("Enter the information for a previously printed check", "MANUAL"));
            _with2.SelectedIndex = 0;
        }

        private void Load_ComboBoxEdit_reason()
        {
            var _with5 = ComboBoxEdit_reason;
            var _with6 = _with5.Properties;
            var _with7 = _with6.Items;
            _with7.Clear();
            foreach (DataRow row in ReasonsTable().Rows)
            {
                _with7.Add(new DebtPlus.Data.Controls.ComboboxItem(Convert.ToString(row["description"]), row["item_key"]));
            }
            _with5.SelectedIndex = 0;
        }

        /// <summary>
        /// Do the CONTROL LOAD event processing once things are stable
        /// </summary>

        public override void ReadForm()
        {
            // Set the creditor for display
            LabelControl_creditor.Text = "Creditor: " + Creditor;

            Load_ComboboxEdit_contribution();
            Load_ComboBoxEdit_disposition();
            Load_ComboBoxEdit_reason();

            // Determine if the warning dialog is to be used
            CheckChangeStatus = 0;
            if (TrustRegister > 0)
            {
                CheckChangeStatus = 1;
                ReadTransactions();
            }

            // Bind the data list to the grid and select the first item in the list
            var _with8 = GridControl1;
            _with8.DataSource = TransactionsTable;
            _with8.RefreshDataSource();
            if (TransactionsTable.Count > 0)
            {
                GridView1.FocusedRowHandle = 0;
            }

            var _with9 = RepositoryItemLookUpEdit_creditor_type;
            _with9.DataSource = CreditorTypes();

            // Trip the first change in the FocusedRow
            BindEntryControls(null, null);
        }

        private void BindEntryControls(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            Transaction CurrentTransaction = (Transaction)GridView1.GetFocusedRow();
            var _with10 = ClientID1;
            _with10.DataBindings.Clear();
            if (CurrentTransaction != null)
            {
                _with10.DataBindings.Add("EditValue", CurrentTransaction, "client", false, DataSourceUpdateMode.OnValidation);
                _with10.Enabled = true;
            }
            else
            {
                _with10.Enabled = false;
            }

            var _with11 = CalcEdit_amount;
            _with11.DataBindings.Clear();
            if (CurrentTransaction != null)
            {
                _with11.DataBindings.Add("EditValue", CurrentTransaction, "debit_amt", false, DataSourceUpdateMode.OnPropertyChanged);
                _with11.Enabled = true;
            }
            else
            {
                _with11.Enabled = false;
            }

            var _with12 = CalcEdit_rate;
            _with12.DataBindings.Clear();
            if (CurrentTransaction != null)
            {
                _with12.DataBindings.Add("EditValue", CurrentTransaction, "fairshare_pct", false, DataSourceUpdateMode.OnPropertyChanged);
                _with12.Enabled = true;
            }
            else
            {
                _with12.Enabled = false;
            }

            var _with13 = LabelControl_client_name;
            _with13.DataBindings.Clear();
            if (CurrentTransaction != null)
            {
                _with13.DataBindings.Add("Text", CurrentTransaction, "name", false, DataSourceUpdateMode.OnPropertyChanged);
            }
            else
            {
                _with13.Text = string.Empty;
            }

            var _with14 = LookUpEdit_client_creditor;
            _with14.DataBindings.Clear();
            if (CurrentTransaction != null)
            {
                _with14.Properties.DataSource = ClientCreditorView(CurrentTransaction);
                _with14.DataBindings.Add("EditValue", CurrentTransaction, "client_creditor", false, DataSourceUpdateMode.OnPropertyChanged);
                _with14.Enabled = true;
            }
            else
            {
                _with14.Enabled = false;
            }

            var _with15 = LookupEdit_contribution;
            _with15.DataBindings.Clear();
            if (CurrentTransaction != null)
            {
                _with15.DataBindings.Add("EditValue", CurrentTransaction, "creditor_type", false, DataSourceUpdateMode.OnPropertyChanged);
                _with15.Enabled = true;
            }
            else
            {
                _with15.Enabled = false;
            }

            // If there is a transaction then enable the delete button
            SimpleButton_del.Enabled = (CurrentTransaction != null);

            // Enable the post button if possible
            EnablePostButton();
        }

        private DataView ClientCreditorView(Transaction CurrentTransaction)
        {
            const string tableName = "client_creditor";
            DataTable tbl = ctx.ds.Tables[tableName];

            if (tbl != null)
            {
                DataView vue = new DataView(tbl, string.Format("[client]={0:f0} AND [creditor]='{1}'", CurrentTransaction.client, Creditor), "account_number", DataViewRowState.CurrentRows);
                if (vue.Count > 0)
                {
                    return vue;
                }
                vue.Dispose();
            }

            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            try
            {
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with16 = cmd;
                    _with16.Connection = cn;
                    _with16.CommandText = "SELECT client_creditor, client, creditor, account_number, reassigned_debt from client_creditor WHERE client=@client and creditor=@creditor";
                    _with16.CommandType = CommandType.Text;
                    _with16.Parameters.Add("@client", SqlDbType.Int).Value = CurrentTransaction.client;
                    _with16.Parameters.Add("@creditor", SqlDbType.VarChar).Value = Creditor;

                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.Fill(ctx.ds, tableName);
                    }

                    tbl = ctx.ds.Tables[tableName];
                    var _with17 = tbl;
                    if (_with17.PrimaryKey.GetUpperBound(0) < 0)
                    {
                        _with17.PrimaryKey = new DataColumn[] { _with17.Columns["client_creditor"] };
                    }
                }
            }
            catch (Exception ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
            }

            if (tbl != null)
            {
                return new DataView(tbl, string.Format("[client]={0:f0} AND [creditor]='{1}'", CurrentTransaction.client, Creditor), "account_number", DataViewRowState.CurrentRows);
            }

            return null;
        }

        private DataTable ReasonsTable()
        {
            const string TableName = "lst_descriptions_MD";
            DataTable tbl = ctx.ds.Tables[TableName];

            if (tbl == null)
            {
                try
                {
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with18 = cmd;
                        _with18.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with18.CommandText = "lst_descriptions_MD";
                        _with18.CommandType = CommandType.StoredProcedure;

                        using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                        {
                            da.Fill(ctx.ds, TableName);
                        }

                        tbl = ctx.ds.Tables[TableName];
                        var _with19 = tbl;
                        if (_with19.PrimaryKey.GetUpperBound(0) < 0)
                        {
                            _with19.PrimaryKey = new DataColumn[] { _with19.Columns["client"] };
                        }
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading lst_descripitons_MD");
                }
            }

            return tbl;
        }

        private string DefaultCreditorType()
        {
            DataRow row = ReadCreditor(Creditor);
            return global::DebtPlus.Utils.Nulls.DStr(row["creditor_type_check"]);
        }

        private double DefaultFairsharePct()
        {
            DataRow row = ReadCreditor(Creditor);
            return global::DebtPlus.Utils.Nulls.DDbl(row["fairshare_pct_check"]);
        }

        /// <summary>
        /// Process the ADD event
        /// </summary>
        private void SimpleButton_Add_Click(object Sender, EventArgs e)
        {
            if (AllowUpdate())
            {
                Transaction CurrentTransaction = new Transaction(ctx);
                CurrentTransaction.PropertyChanged += TransactionPropertyChanged;

                var _with20 = CurrentTransaction;
                _with20.BeginInit();
                _with20.creditor = Creditor;
                _with20.creditor_type = DefaultCreditorType();
                _with20.fairshare_pct = DefaultFairsharePct();
                _with20.EndInit();
                TransactionsTable.Add(CurrentTransaction);
                GridControl1.RefreshDataSource();

                // Find the new row in the grid
                GridView1.FocusedRowHandle = GridView1.GetDataSourceRowIndex(TransactionsTable.Count - 1);
            }
        }

        /// <summary>
        /// Process the DELETE event
        /// </summary>

        private void SimpleButton_Delete_Click(object Sender, EventArgs e)
        {
            Transaction CurrentTransaction = (Transaction)GridView1.GetFocusedRow();
            if (CurrentTransaction != null)
            {
                if (AllowUpdate())
                {
                    CurrentTransaction.@void = 1;
                    GridControl1.RefreshDataSource();
                }
            }

            // If there are no more transactions then clear the focused row
            bool HasTransactions = false;
            foreach (Transaction tran in TransactionsTable)
            {
                if (tran.@void == 0)
                {
                    HasTransactions = true;
                }
            }

            // If there are no transactions then there is no focused row and we can not delete it.
            if (!HasTransactions)
            {
                GridView1.FocusedRowHandle = -1;
                SimpleButton_del.Enabled = false;
            }
        }

        /// <summary>
        /// Define the transactions table
        /// </summary>
        private void ReadTransactions()
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlDataReader rd = null;
            try
            {
                cn.Open();
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with21 = cmd;
                    _with21.Connection = cn;
                    _with21.CommandText = "SELECT  dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name, rcc.client, rcc.creditor, rcc.client_creditor, rcc.debit_amt, rcc.fairshare_amt, rcc.fairshare_pct, rcc.creditor_type, rcc.account_number FROM registers_client_creditor rcc with (nolock) left outer join people p with (nolock) on rcc.client = p.client and 1 = p.relation LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name WHERE trust_register = @trust_register AND rcc.tran_type in ('AD','MD','CM') AND rcc.creditor = @creditor";
                    _with21.CommandType = CommandType.Text;
                    _with21.Parameters.Add("@trust_register", SqlDbType.Int).Value = TrustRegister;
                    _with21.Parameters.Add("@creditor", SqlDbType.VarChar).Value = Creditor;
                    rd = _with21.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);
                }

                if (rd != null)
                {
                    while (rd.Read())
                    {
                        // Copy the results from the database into a list of transaction records.
                        Transaction NewTransaction = new Transaction(ctx);
                        NewTransaction.PropertyChanged += TransactionPropertyChanged;
                        var _with22 = NewTransaction;
                        _with22.BeginInit();
                        for (Int32 col = 0; col <= rd.FieldCount - 1; col++)
                        {
                            if (!rd.IsDBNull(col))
                            {
                                string FieldName = rd.GetName(col).ToLower();
                                System.Reflection.PropertyInfo pi = typeof(Transaction).GetProperty(FieldName);
                                if (pi == null)
                                {
                                    Debug.WriteLine(string.Format("Missing field property '{0}'", FieldName));
                                }
                                else
                                {
                                    pi.SetValue(NewTransaction, rd[col], null);
                                }
                            }
                        }
                        _with22.EndInit();
                        TransactionsTable.Add(NewTransaction);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                if (rd != null)
                    rd.Dispose();
                if (cn != null)
                    cn.Dispose();
            }
        }

        public Int32 CheckChangeStatus;

        private bool AllowUpdate()
        {
            // Generate an error if the operation is not acceptable
            if (CheckChangeStatus == 1)
            {
                if (DebtPlus.Data.Forms.MessageBox.Show("Warning, changing a copy of a check is not advisable. Are you really sure that you want to do this?", "Are you sure", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    CheckChangeStatus = 3;
                }
                else
                {
                    CheckChangeStatus = 2;
                }
            }

            // Generate the error if this is the case
            return CheckChangeStatus != 2;
        }

        private DataView DebtView(System.Nullable<Int32> ClientObject)
        {
            DataView answer = null;
            Int32 Client = ClientObject.GetValueOrDefault(-1);
            if (Client >= 0)
            {
                DataTable tbl = ctx.ds.Tables["client_creditor"];
                if (tbl != null)
                {
                    answer = new DataView(tbl, string.Format("[ClientId]={0:f0} AND creditor='{1}'", Client, Creditor), "client_creditor", DataViewRowState.CurrentRows);
                }

                if (answer == null || answer.Count == 0)
                {
                    using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                    {
                        var _with23 = cmd;
                        _with23.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                        _with23.CommandText = "SELECT cc.client_creditor, cc.client_creditor_balance, cc.client, cc.creditor, cc.creditor_name, cc.line_number, cc.priority, cc.drop_date, cc.drop_reason, cc.drop_reason_sent, cc.person, cc.client_name, cc.non_dmp_payment, cc.non_dmp_interest, cc.disbursement_factor, cc.date_disp_changed, cc.orig_dmp_payment, cc.months_delinquent, cc.start_date, cc.dmp_interest, cc.dmp_payout_interest, cc.last_stmt_date, cc.irs_form_on_file, cc.student_loan_release, cc.balance_verification_release, cc.last_payment_date_b4_dmp, cc.expected_payout_date, cc.rpps_client_type_indicator, cc.terms, cc.percent_balance, cc.client_creditor_proposal, cc.contact_name, cc.payments_this_creditor, cc.returns_this_creditor, cc.interest_this_creditor, cc.sched_payment, cc.proposal_prenote_date, cc.send_bal_verify, cc.verify_request_date, cc.balance_verify_date, cc.balance_verify_by, cc.first_payment, cc.last_payment, cc.account_number, cc.message, cc.hold_disbursements, cc.send_drop_notice, cc.last_communication, cc.reassigned_debt, cc.fairshare_pct_check, cc.fairshare_pct_eft, cc.prenote_date, cc.check_payments, cc.payment_rpps_mask, cc.payment_rpps_mask_timestamp, cc.rpps_mask, cc.rpps_mask_timestamp, cc.created_by, cc.date_created, b.orig_balance, b.orig_balance+b.orig_balance_adjustment+b.total_interest-b.total_payments as current_balance FROM client_creditor cc WITH (NOLOCK) LEFT OUTER JOIN client_creditor_balances b ON cc.client_creditor_balance = b.client_creditor_balance WHERE cc.client=@client AND cc.creditor = @creditor";
                        _with23.CommandType = CommandType.Text;
                        _with23.Parameters.Add("@client", SqlDbType.Int).Value = Client;
                        _with23.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor;

                        System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                        try
                        {
                            da.Fill(ctx.ds, "client_creditor");
                            tbl = ctx.ds.Tables["client_creditor"];
                            if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                            {
                                var _with24 = tbl;
                                _with24.PrimaryKey = new DataColumn[] { _with24.Columns["client_creditor"] };
                            }
                        }
                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading debts");
                        }
                        finally
                        {
                            da.Dispose();
                        }
                    }
                }

                if (tbl != null)
                {
                    answer = new DataView(tbl, string.Format("[client]={0:f0} AND creditor='{1}'", Client, Creditor), "client_creditor", DataViewRowState.CurrentRows);
                }
            }

            return answer;
        }

        private void ClientID1_EditValueChanged(object sender, EventArgs e)
        {
            // Read the list of debts that match the client/creditor combination
            DataView vue = DebtView(ClientID1.EditValue);
            LookUpEdit_client_creditor.Properties.DataSource = vue;
        }

        private void SimpleButton_Post_Click(object sender, EventArgs e)
        {
            // Find the trust register for the new check.
            Int32 trust_register = PostManualDisbursement();

            if (trust_register > 0)
            {
                // Determine the post processing for the check
                switch (Convert.ToString(((DebtPlus.Data.Controls.ComboboxItem)ComboBoxEdit_disposition.SelectedItem).value))
                {
                    case "MANUAL":
                        DebtPlus.Check.Print.Library.PrintChecksClass manLibry = new DebtPlus.Check.Print.Library.PrintChecksClass();
                        manLibry.RecordSingleCheck(trust_register);
                        break;

                    case "QUEUE":
                        DebtPlus.Data.Forms.MessageBox.Show(string.Format("The manual disbursement is complete{0}{0}The check is left pending to be printed later", Environment.NewLine), "Operation completed", MessageBoxButtons.OK);
                        break;

                    case "PRINT":
                        DebtPlus.Check.Print.Library.PrintChecksClass printLibry = new DebtPlus.Check.Print.Library.PrintChecksClass();
                        printLibry.PrintSingleCheck(trust_register);
                        break;

                    default:
                        break;
                }

                // Complete the processing for the form.
                RaiseCancelled();
            }
        }

        private Int32 PostManualDisbursement()
        {
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            System.Data.SqlClient.SqlTransaction txn = null;
            Cursor current_cursor = Cursor.Current;
            Int32 trust_register = -1;
            Int32 creditor_register = -1;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                txn = cn.BeginTransaction();

                // Create a disbursement batch and trust_register
                Int32 disbursement_register = NewManualDisbursement(cn, txn);
                trust_register = NewTrustRegister(cn, txn, Creditor);

                foreach (var tranItem in TransactionsTable)
                {
                    if (tranItem.@void == 0 && tranItem.IsValid)
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with27 = cmd;
                            _with27.Connection = cn;
                            _with27.Transaction = txn;
                            _with27.CommandText = "xpr_disbursement_manual_detail";
                            _with27.CommandType = CommandType.StoredProcedure;

                            _with27.Parameters.Add("@RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                            _with27.Parameters.Add("@trust_register", SqlDbType.Int).Value = trust_register;
                            _with27.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                            _with27.Parameters.Add("@client_creditor", SqlDbType.Int).Value = tranItem.client_creditor;
                            _with27.Parameters.Add("@amount", SqlDbType.Decimal).Value = tranItem.debit_amt;
                            _with27.Parameters.Add("@fairshare_amt", SqlDbType.Decimal).Value = tranItem.fairshare_amt;
                            _with27.Parameters.Add("@creditor_type", SqlDbType.VarChar, 10).Value = tranItem.Recorded_creditor_type;

                            _with27.ExecuteNonQuery();
                            if (Convert.ToInt32(_with27.Parameters[0].Value) <= 0)
                            {
                                throw new DataException("Error creating disbursement detail");
                            }
                        }
                    }
                }

                // Close out the check
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with28 = cmd;
                    _with28.Connection = cn;
                    _with28.Transaction = txn;
                    _with28.CommandText = "xpr_disbursement_manual_close";
                    _with28.CommandType = CommandType.StoredProcedure;
                    _with28.Parameters.Add("@RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    _with28.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                    _with28.Parameters.Add("@trust_register", SqlDbType.Int).Value = trust_register;
                    _with28.Parameters.Add("@message", SqlDbType.VarChar, 80).Value = ComboBoxEdit_reason.Text.Trim();
                    _with28.ExecuteNonQuery();

                    creditor_register = Convert.ToInt32(_with28.Parameters[0].Value);
                    if (creditor_register <= 0)
                    {
                        throw new DataException("Error creating disbursement detail");
                    }
                }

                txn.Commit();
                txn.Dispose();
                txn = null;
            }
#pragma warning disable 168
            catch (DataException ex)
            {
                trust_register = -1;
            }
#pragma warning restore 168
            catch (System.Data.SqlClient.SqlException ex)
            {
                string Msg = ex.Message;
                string[] Msgs = Msg.Replace("\r", "").Split('\n');
                string FirstMsg = Msgs[0];

                DebtPlus.Data.Forms.MessageBox.Show(FirstMsg, "Error disbursing money", MessageBoxButtons.OK, MessageBoxIcon.Error);
                trust_register = -1;
            }
            finally
            {
                if (txn != null)
                {
                    try
                    {
                        txn.Rollback();
                    }

#pragma warning disable 168
                    catch (Exception exRollback) { }
#pragma warning restore 168

                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();
                Cursor.Current = current_cursor;
            }

            // Return the pointer to the new check.
            return trust_register;
        }

        private Int32 NewManualDisbursement(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            Int32 answer = -1;

            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with29 = cmd;
                _with29.Connection = cn;
                _with29.Transaction = txn;
                _with29.CommandText = "xpr_disbursement_manual_create";
                _with29.CommandType = CommandType.StoredProcedure;
                _with29.Parameters.Add("RETURN", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                _with29.Parameters.Add("@type", SqlDbType.VarChar, 10).Value = "MD";
                _with29.ExecuteNonQuery();
                answer = Convert.ToInt32(_with29.Parameters[0].Value);
            }
            return answer;
        }

        private Int32 NewTrustRegister(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, string Creditor)
        {
            Int32 answer = -1;
            Int32 bank = default(Int32);

            // Find the bank number used for the creditor checks
            DataRow row = ctx.ds.Tables["creditors"].Rows.Find(Creditor);
            if (row != null && !object.ReferenceEquals(row["check_bank"], DBNull.Value))
            {
                bank = Convert.ToInt32(row["check_bank"]);
            }
            else
            {
                bank = -1;
            }

            // Create the item in the trust register
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with30 = cmd;
                _with30.Connection = cn;
                _with30.Transaction = txn;
                _with30.CommandText = "INSERT INTO registers_trust(tran_type,creditor,bank,amount,cleared) VALUES (@tran_type,@creditor,@bank,0,'P'); SELECT scope_identity() AS trust_register";
                _with30.CommandType = CommandType.Text;
                _with30.Parameters.Add("@tran_type", SqlDbType.VarChar, 10).Value = "MD";
                _with30.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = Creditor;
                _with30.Parameters.Add("@bank", SqlDbType.Int).Value = bank;

                object objAnswer = _with30.ExecuteScalar();
                if (objAnswer != null && !object.ReferenceEquals(objAnswer, DBNull.Value))
                    answer = Convert.ToInt32(objAnswer);
            }
            return answer;
        }

        private void TransactionPropertyChanged(object Sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            // Refresh the row contining the field that was last changed.
            Transaction CurrentTransaction = (Transaction)Sender;
            Int32 RowInTable = TransactionsTable.IndexOf(CurrentTransaction);
            if (RowInTable >= 0)
            {
                Int32 RowHandle = GridView1.GetRowHandle(RowInTable);
                if (RowHandle >= 0)
                {
                    GridView1.RefreshRow(RowHandle);
                    GridView1.UpdateTotalSummary();
                }
            }

            // Enable the post button if possible
            EnablePostButton();
        }

        private void EnablePostButton()
        {
            // Determine if there are any errors and there is at least one Transaction
            bool HasError = false;
            bool HasTransactions = false;
            foreach (Transaction Tran in TransactionsTable)
            {
                if (Tran.@void == 0)
                {
                    HasTransactions = true;
                }

                if (!Tran.IsValid)
                {
                    HasError = true;
                }
            }

            // We can post if there is a transaction and there are no errors
            SimpleButton_Post.Enabled = HasTransactions && (!HasError);
        }
    }
}