using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

using DebtPlus.UI.Creditor.Widgets.Controls;
using DebtPlus.UI.Creditor.Widgets;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls
{
	partial class Main_Copy : BaseControl
	{

		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Copy));
			DevExpress.Utils.SerializableAppearanceObject SerializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.PanelControl1 = new DevExpress.XtraEditors.PanelControl();
			this.CreditorID1 = new CreditorID();
			this.SimpleButton_cancel = new DevExpress.XtraEditors.SimpleButton();
			this.SimpleButton_ok = new DevExpress.XtraEditors.SimpleButton();
			this.TextEdit1 = new DevExpress.XtraEditors.TextEdit();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl_creditor = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).BeginInit();
			this.PanelControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).BeginInit();
			this.SuspendLayout();
			//
			//LabelControl1
			//
			this.LabelControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.LabelControl1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
			this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
			this.LabelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LabelControl1.Location = new System.Drawing.Point(4, 13);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Padding = new System.Windows.Forms.Padding(4);
			this.LabelControl1.Size = new System.Drawing.Size(468, 60);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = resources.GetString("LabelControl1.Text");
			this.LabelControl1.UseMnemonic = false;
			//
			//PanelControl1
			//
			this.PanelControl1.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom);
			this.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
			this.PanelControl1.Controls.Add(this.CreditorID1);
			this.PanelControl1.Controls.Add(this.SimpleButton_cancel);
			this.PanelControl1.Controls.Add(this.SimpleButton_ok);
			this.PanelControl1.Controls.Add(this.TextEdit1);
			this.PanelControl1.Controls.Add(this.LabelControl3);
			this.PanelControl1.Controls.Add(this.LabelControl_creditor);
			this.PanelControl1.Location = new System.Drawing.Point(48, 79);
			this.PanelControl1.Name = "PanelControl1";
			this.PanelControl1.Size = new System.Drawing.Size(378, 109);
			this.PanelControl1.TabIndex = 1;
			//
			//CreditorID1
			//
			this.CreditorID1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.CreditorID1.EditValue = null;
			this.CreditorID1.Location = new System.Drawing.Point(139, 31);
			this.CreditorID1.Name = "CreditorID1";
			this.CreditorID1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, (System.Drawing.Image)resources.GetObject("CreditorID1.Properties.Buttons"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), SerializableAppearanceObject1,
			"Click here to search for a creditor ID", "search", null, true) });
			this.CreditorID1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.CreditorID1.Properties.Mask.AutoComplete = DevExpress.XtraEditors.Mask.AutoCompleteType.Strong;
			this.CreditorID1.Properties.Mask.BeepOnError = true;
			this.CreditorID1.Properties.Mask.EditMask = "[A-Z]{1,2}[0-9]{4,}";
			this.CreditorID1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.CreditorID1.Properties.Mask.UseMaskAsDisplayFormat = true;
			this.CreditorID1.Properties.MaxLength = 10;
			this.CreditorID1.Size = new System.Drawing.Size(100, 20);
			this.CreditorID1.TabIndex = 1;
			//
			//SimpleButton_cancel
			//
			this.SimpleButton_cancel.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.SimpleButton_cancel.Location = new System.Drawing.Point(270, 61);
			this.SimpleButton_cancel.Name = "SimpleButton_cancel";
			this.SimpleButton_cancel.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_cancel.TabIndex = 5;
			this.SimpleButton_cancel.Text = "&Cancel";
			//
			//SimpleButton_ok
			//
			this.SimpleButton_ok.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.SimpleButton_ok.Enabled = false;
			this.SimpleButton_ok.Location = new System.Drawing.Point(270, 24);
			this.SimpleButton_ok.Name = "SimpleButton_ok";
			this.SimpleButton_ok.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton_ok.TabIndex = 4;
			this.SimpleButton_ok.Text = "&Next";
			//
			//TextEdit1
			//
			this.TextEdit1.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.TextEdit1.Location = new System.Drawing.Point(139, 58);
			this.TextEdit1.Name = "TextEdit1";
			this.TextEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.TextEdit1.Properties.Appearance.Options.UseTextOptions = true;
			this.TextEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit1.Properties.AppearanceDisabled.Options.UseTextOptions = true;
			this.TextEdit1.Properties.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit1.Properties.AppearanceFocused.Options.UseTextOptions = true;
			this.TextEdit1.Properties.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit1.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
			this.TextEdit1.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.TextEdit1.Properties.Mask.BeepOnError = true;
			this.TextEdit1.Properties.Mask.EditMask = "[A-Z]?[0-9]+";
			this.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
			this.TextEdit1.Size = new System.Drawing.Size(100, 20);
			this.TextEdit1.TabIndex = 3;
			//
			//LabelControl3
			//
			this.LabelControl3.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LabelControl3.Location = new System.Drawing.Point(23, 61);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(110, 13);
			this.LabelControl3.TabIndex = 2;
			this.LabelControl3.Text = "Check Number to Copy";
			//
			//LabelControl_creditor
			//
			this.LabelControl_creditor.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.LabelControl_creditor.Location = new System.Drawing.Point(23, 34);
			this.LabelControl_creditor.Name = "LabelControl_creditor";
			this.LabelControl_creditor.Size = new System.Drawing.Size(39, 13);
			this.LabelControl_creditor.TabIndex = 0;
			this.LabelControl_creditor.Text = "Creditor";
			//
			//Main_Copy
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.PanelControl1);
			this.Controls.Add(this.LabelControl1);
			this.Name = "Main_Copy";
			this.Size = new System.Drawing.Size(475, 217);
			((System.ComponentModel.ISupportInitialize)this.PanelControl1).EndInit();
			this.PanelControl1.ResumeLayout(false);
			this.PanelControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.CreditorID1.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.TextEdit1.Properties).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraEditors.LabelControl LabelControl1;
		protected internal DevExpress.XtraEditors.PanelControl PanelControl1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_cancel;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton_ok;
		protected internal DevExpress.XtraEditors.TextEdit TextEdit1;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl3;
		protected internal DevExpress.XtraEditors.LabelControl LabelControl_creditor;
		protected internal CreditorID CreditorID1;
	}
}
