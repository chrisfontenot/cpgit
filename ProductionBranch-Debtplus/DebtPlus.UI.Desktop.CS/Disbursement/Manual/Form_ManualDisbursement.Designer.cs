using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Manual
{
	partial class Form_ManualDisbursement : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.Main_Entry1 = new global::DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls.Main_Entry(ctx);
			this.Main_Copy1 = new global::DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls.Main_Copy(ctx);
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			this.SuspendLayout();
			//
			//Main_Entry1
			//
			this.Main_Entry1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Main_Entry1.Location = new System.Drawing.Point(0, 0);
			this.Main_Entry1.Name = "Main_Entry1";
			this.Main_Entry1.Padding = new System.Windows.Forms.Padding(4);
			this.Main_Entry1.Size = new System.Drawing.Size(598, 351);
			this.Main_Entry1.TabIndex = 2;
			//
			//Main_Copy1
			//
			this.Main_Copy1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.Main_Copy1.Location = new System.Drawing.Point(0, 0);
			this.Main_Copy1.Name = "Main_Copy1";
			this.Main_Copy1.Size = new System.Drawing.Size(598, 351);
			this.Main_Copy1.TabIndex = 0;
			//
			//Form_ManualDisbursement
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(598, 351);
			this.Controls.Add(this.Main_Entry1);
			this.Controls.Add(this.Main_Copy1);
			this.LookAndFeel.UseDefaultLookAndFeel = true;
			this.Name = "Form_ManualDisbursement";
			this.Text = "Manual Disbursement";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			this.ResumeLayout(false);

		}
		protected internal global::DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls.Main_Copy Main_Copy1;
		protected internal global::DebtPlus.UI.Desktop.CS.Disbursement.Manual.Controls.Main_Entry Main_Entry1;
	}
}
