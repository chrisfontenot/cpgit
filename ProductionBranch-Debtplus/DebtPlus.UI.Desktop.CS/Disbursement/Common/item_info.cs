#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.Linq;
using DebtPlus.LINQ;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.UI.Desktop.CS.Disbursement.Automated.Post;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal partial class item_info
    {
        /// <summary>
        /// Class of processing for payments. All payments are vectored through here.
        /// </summary>
        private CDisbursement_Post cls_post;    // Pointer to our parent class
        private char cleared_status;  			// Used when the item is created
        public Int32 bank;						// Bank number
        public string tran_type;				// "AD" or "BW"
        public Int32 item_count;				// Number of items currently on the draft
        public Int32 max_items;					// Maximum number of items per check
        public decimal max_amount;				// Maximum dollar amount per check
        public string output_directory;		    // If EFT, this is the output directory for the file
        public Int32 trust_register;			// current trust register
        public decimal gross;					// gross amount
        public decimal deducted;				// total deducted amount
        public decimal billed;					// total billed amount
        public Int32 creditor_register;			// Pointer to the creditor register entry

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public item_info() : base()
        {
            cls_post          = null;			// Pointer to our parent class
            cleared_status    = 'P';  			// Used when the item is created
            bank              = -1;				// Bank number
            tran_type         = "AD";			// "AD" or "BW"
            item_count        = 0;				// Number of items currently on the draft
            max_items         = 0;				// Maximum number of items per check
            max_amount        = 0m;				// Maximum dollar amount per check
            output_directory  = string.Empty;	// If EFT, this is the output directory for the file
            trust_register    = 0;				// current trust register
            gross             = 0m;				// gross amount
            deducted          = 0m;				// total deducted amount
            billed            = 0m;				// total billed amount
            creditor_register = 0;				// Pointer to the creditor register entry
        }

        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public item_info(CDisbursement_Post Parent) : this()
        {
            cls_post = Parent;
        }

        /// <summary>
        /// Return the type of the bank account
        /// </summary>
        // Type of account
        public string bank_type { get; private set; }

        /// <summary>
        /// Initialize the item data
        /// </summary>
        public void New_Check()
        {
            billed            = 0m;
            deducted          = 0m;
            gross             = 0m;
            item_count        = 0;
            creditor_register = 0;
        }

        /// <summary>
        /// Initialize some of the information about the bank
        /// </summary>
        public void read_bank(Int32 BankNumber)
        {
            // Find the bank information in the cache
            var q = DebtPlus.LINQ.Cache.bank.getList().Find(s => s.Id == BankNumber);
            if (q == null)
            {
                // If there is no entry then make up one.
                q = DebtPlus.LINQ.Factory.Manufacture_bank();
                q.type = "C";
            }

            // Save the information from the bank record.
            bank             = BankNumber;
            output_directory = q.output_directory;
            bank_type        = q.type;

            // Do some post-processing of the items from the banks table
            switch (bank_type)
            {
                case "R":
                case "V":
                case "E":
                    // EFT types have no limits and are created "pending to be reconciled"
                    tran_type      = "BW";
                    cleared_status = ' ';
                    max_items      = 0;
                    max_amount     = 0m;
                    break;

                default:
                    // Checks have a limit on the number of items and amount.
                    tran_type      = "AD";
                    cleared_status = 'P';
                    max_amount     = q.max_amt_per_check;
                    max_items      = q.max_clients_per_check;
                    break;
            }
        }

        /// <summary>
        /// Complete the processing of all check or rps creditors
        /// </summary>
        public void Save_Trust_Register(SqlConnection cn, SqlTransaction txn)
        {
            using (var cmd = new SqlCommand())
            {
                cmd.Connection = cn;
                cmd.Transaction = txn;
                cmd.CommandText = "xpr_disbursement_cr_trans";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@tran_type", SqlDbType.VarChar, 2).Value = tran_type;
                cmd.Parameters.Add("@trust_register", SqlDbType.Int).Value = trust_register;
                cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = cls_post.disbursement_register;
                cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 10).Value = cls_post.Creditor;
                cmd.Parameters.Add("@gross", SqlDbType.Decimal).Value = gross;
                cmd.Parameters.Add("@deducted", SqlDbType.Decimal).Value = deducted;
                cmd.Parameters.Add("@billed", SqlDbType.Decimal).Value = billed;
                cmd.Parameters.Add("@creditor_register", SqlDbType.Int).Value = creditor_register;

                cmd.ExecuteNonQuery();
            }

            // Start with new check information
            New_Check();

            // We no longer have an open trust register if this is a check. We do for EFT, but not a check.
            if (bank_type == "C")
            {
                trust_register = 0;
            }
        }

        /// <summary>
        /// Complete the processing of all check or rps creditors
        /// </summary>
        public void New_Trust_Register(SqlConnection cn, SqlTransaction txn)
        {
            // Create the blank trust register record.
            var record       = DebtPlus.LINQ.Factory.Manufacture_registers_trust();
            record.tran_type = tran_type;
            record.creditor  = cls_post.Creditor;
            record.cleared   = cleared_status;
            record.bank      = bank;

            using (var bc = new BusinessContext(cn))
            {
                bc.Transaction = txn;
                bc.registers_trusts.InsertOnSubmit(record);
                bc.SubmitChanges();
                trust_register = record.Id;
            }
        }

        /// <summary>
        /// Create the new creditor register entry
        /// </summary>
        public void New_Creditor_Register(SqlConnection cn, SqlTransaction txn)
        {
            using (var bc = new BusinessContext(cn))
            {
                bc.Transaction = txn;

                // Create the blank creditor register record.
                var record                   = DebtPlus.LINQ.Factory.Manufacture_registers_creditor(cls_post.Creditor, tran_type);
                record.disbursement_register = cls_post.disbursement_register;
                record.trust_register        = trust_register;

                // We need the ID for the new record next.
                bc.registers_creditors.InsertOnSubmit(record);
                bc.SubmitChanges();
                creditor_register = record.Id;

                // Update the first and last payment information for the creditor
                var q = bc.creditors.Where(s => s.Id == cls_post.Creditor).FirstOrDefault();
                if (q != null)
                {
                    if (q.first_payment == null)
                    {
                        q.first_payment = creditor_register;
                    }
                    q.last_payment = creditor_register;
                    bc.SubmitChanges();
                }
            }
        }

        /// <summary>
        /// Generate the RPPS output file if this is an RPPS bank
        /// </summary>
        public void Write_RPPS(bool useLocalFile = false)
        {
            // If this bank is not RPPS then just leave.
            // so that processing is not needed now.
            if (bank_type != "R")
            {
                return;
            }

            // Allocate a processing form that is needed for later
            using (var frmRPS = new RPPS_Processing())
            {
                do
                {
                    // Generate the PRENOTE file if there is one
                    var clsRPPS = new GenerateRPSPayments()
                        {
                            disbursement_register = cls_post.disbursement_register,
                            bank = bank
                        };

                    if (clsRPPS.Process_Prenotes(frmRPS, useLocalFile))
                    {
                        break;
                    }

                    DebtPlus.Data.Forms.MessageBox.Show("Someone else was generating an RPPS file. They have changed the trace numbers so we need to regenerate the RPPS file.", "Sorry, but we need to redo the RPPS file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                while (true);

                do
                {
                    // Generate the PAYMENT file. There should be one or we would not be here.
                    var clsRPPS = new GenerateRPSPayments()
                    {
                        disbursement_register = cls_post.disbursement_register,
                        bank = bank
                    };

                    if (clsRPPS.Process_Payments(frmRPS, useLocalFile))
                    {
                        break;
                    }

                    DebtPlus.Data.Forms.MessageBox.Show("Someone else was generating an RPPS file. They have changed the trace numbers so we need to regenerate the RPPS file.", "Sorry, but we need to redo the RPPS file", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                while (true);
            }
        }
    }
}