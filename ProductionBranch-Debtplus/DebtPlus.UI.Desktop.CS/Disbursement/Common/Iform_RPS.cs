namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal interface Iform_RPS
    {
        System.IO.StreamWriter Request_FileName(System.Int32 disbursement_register, System.Int32 Bank, string FileType);

        System.IO.StreamWriter Request_FileName(System.Int32 disbursement_register, System.Int32 Bank, string FileType, bool UseLocalFileOnly);

        decimal total_debt { set; }

        decimal total_credit { set; }

        System.Int32 total_items { set; }

        System.Int32 total_batches { set; }

        decimal total_billed { set; }

        decimal total_gross { set; }

        decimal total_net { set; }

        System.Int32 total_prenotes { set; }

        string batch { set; }
    }
}