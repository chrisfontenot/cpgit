#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.Interfaces.Debt;
using DebtPlus.Svc.Debt;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    /// <summary>
    /// Debts for the disbursement operation come from the disbursement
    /// tables and not the debts table. As such, they don't have as many
    /// fields as the normal debts and some of the fields that are read-only
    /// on the normal debts are read/write here. But, these debts need to be
    /// proratable as well as able to calculate the monthly fees.
    /// </summary>
    partial class DisbursementDebtRecord : DebtPlus.Interfaces.Client.IClient, DebtPlus.Interfaces.Creditor.ICreditor, DebtPlus.Interfaces.Debt.IDebtRecord, DebtPlus.Interfaces.Debt.INotifyDataChanged, DebtPlus.Interfaces.Debt.IProratable, System.IDisposable, System.IComparable, System.IComparable<DisbursementDebtRecord>, System.ComponentModel.INotifyPropertyChanged, System.ComponentModel.ISupportInitialize
    {
        // Track the BeginInit / EndInit sequences
        protected bool InInit = false;

        /// <summary>
        /// Client ID
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public int ClientId
        {
            get { return 0; }
            set { }
        }

        public string m_Creditor;

        /// <summary>
        /// Creditor ID
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [DebtPlus.Svc.Debt.DatabaseField("Creditor", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)]
        public string Creditor
        {
            get { return m_Creditor; }
            set
            {
                m_Creditor = value;
                RaiseDataChanged("Creditor");
            }
        }

        /// <summary>
        /// System.ComponentModel.INotifyPropertyChanged
        /// </summary>
        /// <remarks></remarks>
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raise the property changed event
        /// </summary>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void RaisePropertyChanged(System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (!InInit)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, e);
                }
            }
        }

        /// <summary>
        /// Raise the property changed event
        /// </summary>
        /// <param name="PropertyName"></param>
        /// <remarks></remarks>
        public void RaisePropertyChanged(string PropertyName)
        {
            RaisePropertyChanged(new System.ComponentModel.PropertyChangedEventArgs(PropertyName));
        }

        private Int32 m_OID = 0;

        /// <summary>
        /// ID of the debt record
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        [DebtPlus.Svc.Debt.DatabaseField("oID", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)]
        public Int32 oid
        {
            get { return m_OID; }
            set
            {
                m_OID = value;
                RaiseDataChanged("oid");
            }
        }

        private Int32 m_ClientCreditor = 0;

        /// <summary>
        /// ID of the record associated with the payment information. This may change over the life of the debt.
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("client_creditor", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)]
        public Int32 client_creditor
        {
            get { return m_ClientCreditor; }
            set
            {
                m_ClientCreditor = value;
                RaiseDataChanged("client_creditor");
            }
        }

        /// <summary>
        /// Primary key to the debt table. This is the ID for the debt.
        /// </summary>
        public Int32 DebtId
        {
            get { return client_creditor; }
            set { client_creditor = value; }
        }

        private decimal m_SchedPayment;

        /// <summary>
        /// Scheduled payment for THIS MONTH
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("sched_payment", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public decimal sched_payment
        {
            get { return m_SchedPayment; }
            set
            {
                m_SchedPayment = value;
                RaiseDataChanged("sched_payment");
            }
        }

        private decimal m_DisbursementFactor;

        /// <summary>
        /// Disbursement factor
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("disbursement_factor", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public decimal disbursement_factor
        {
            get { return m_DisbursementFactor; }
            set
            {
                m_DisbursementFactor = value;
                RaiseDataChanged("disbursement_factor");
            }
        }

        public virtual decimal display_disbursement_factor
        {
            get { return disbursement_factor; }
            set { disbursement_factor = value; }
        }

        private decimal m_PaymentsMonth0 = 0m;

        /// <summary>
        /// Total amount paid THIS MONTH on the debt
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("current_month_disbursement", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public decimal payments_month_0
        {
            get { return m_PaymentsMonth0; }
            set
            {
                m_PaymentsMonth0 = value;
                RaiseDataChanged("payments_month_0");
            }
        }

        private decimal m_OrigBalance = 0m;

        /// <summary>
        /// Original balance information
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("orig_balance", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public decimal orig_balance
        {
            get { return m_OrigBalance; }
            set
            {
                m_OrigBalance = value;
                RaiseDataChanged("orig_balance");
            }
        }

        /// <summary>
        /// Adjusted original balance. Used in the payout report.
        /// </summary>
        public decimal adjusted_original_balance
        {
            get
            {
                return orig_balance + orig_balance_adjustment;
            }
        }

        private decimal m_OrigDMPPayment = 0m;

        /// <summary>
        /// Amount on the proposal that was accepted by the creditor
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("orig_dmp_payment", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public decimal orig_dmp_payment
        {
            get { return m_OrigDMPPayment; }
            set
            {
                m_OrigDMPPayment = value;
                RaiseDataChanged("orig_dmp_payment");
            }
        }

        private object m_DMPInterst = DBNull.Value;

        /// <summary>
        /// DMP Interest rate
        /// </summary>
        public object dmp_interest
        {
            get { return m_DMPInterst; }
            set
            {
                m_DMPInterst = value;
                RaiseDataChanged("dmp_interest");
            }
        }

        private decimal m_OrigBalanceAdjustment = 0m;

        /// <summary>
        /// Adjusted original balance
        /// </summary>
        public decimal orig_balance_adjustment
        {
            get { return m_OrigBalanceAdjustment; }
            set
            {
                if (m_OrigBalanceAdjustment != value)
                {
                    m_OrigBalanceAdjustment = value;
                    RaiseDataChanged("orig_balance_adjustment");
                }
            }
        }

        private decimal m_TotalPayments = 0m;

        /// <summary>
        /// Total payments
        /// </summary>
        public decimal total_payments
        {
            get { return m_TotalPayments; }
            set
            {
                if (m_TotalPayments != value)
                {
                    m_TotalPayments = value;
                    RaiseDataChanged("total_payments");
                }
            }
        }

        private decimal m_DisbursementCurrentBalance = 0m;

        /// <summary>
        /// Current balance. This is only for the load operation. For general use, see "current_balance".
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("current_balance", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public decimal disbursement_current_balance
        {
            get { return m_DisbursementCurrentBalance; }
            set
            {
                if (m_DisbursementCurrentBalance != value)
                {
                    m_DisbursementCurrentBalance = value;
                    RaiseDataChanged("current_balance");
                }
            }
        }

        /// <summary>
        /// Current balance
        /// </summary>
        public decimal current_balance
        {
            get { return disbursement_current_balance; }
        }

        /// <summary>
        /// Balance for the display grid
        /// </summary>
        public virtual decimal display_current_balance
        {
            get
            {
                if (!IsActive)
                {
                    return 0m;
                }

                if (ccl_zero_balance)
                {
                    return 0m;
                }

                if (current_balance < 0m)
                {
                    return 0m;
                }
                return current_balance;
            }
        }

        private decimal m_DebitAmt = 0m;

        /// <summary>
        /// Amount to be disbursed in this disbursement operation
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("debit_amt", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal)]
        public decimal debit_amt
        {
            get { return m_DebitAmt; }
            set
            {
                value = System.Math.Truncate(value * 100m) / 100m;
                // Remove any fractional cents

                // If we are not loading the information then do some range checking on the data
                // to prevent amounts in excess of what are allowed.
                if (!InInit)
                {
                    if (value < 0m)
                    {
                        value = 0m;
                    }
                    else if (!ccl_zero_balance && value > current_balance)
                    {
                        value = current_balance;
                    }
                }

                m_DebitAmt = value;
                RaiseDataChanged("debit_amt");
            }
        }

        private bool m_ZeroBalance = false;

        /// <summary>
        /// Does the debt have a condition where the balance is always zero?
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("zero_balance", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public bool ccl_zero_balance
        {
            get { return m_ZeroBalance; }
            set
            {
                m_ZeroBalance = value;
                RaiseDataChanged("ccl_zero_balance");
            }
        }

        private Int32 m_Priority = 9;

        /// <summary>
        /// Relative importance of paying this debt to others
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("priority", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)]
        public Int32 priority
        {
            get { return m_Priority; }
            set
            {
                m_Priority = value;
                RaiseDataChanged("Priority");
            }
        }

        private string m_CreditorName = string.Empty;

        /// <summary>
        /// Name of the creditor
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("creditor_name", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)]
        public string creditor_name
        {
            get { return m_CreditorName; }
            set
            {
                m_CreditorName = value;
                RaiseDataChanged("creditor_name");
            }
        }

        private string m_AccountNumber;

        /// <summary>
        /// Creditor's account number
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("account_number", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)]
        public string account_number
        {
            get { return m_AccountNumber; }
            set
            {
                m_AccountNumber = value;
                RaiseDataChanged("account_number");
            }
        }

        private Int32 m_LineNumber = 0;

        /// <summary>
        /// Relative stacking order for the debts
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("line_number", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32)]
        public Int32 line_number
        {
            get { return m_LineNumber; }
            set
            {
                m_LineNumber = value;
                RaiseDataChanged("line_number");
            }
        }

        private bool m_Prorate = false;

        /// <summary>
        /// Should we Prorate the debt?
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("prorate", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public bool ccl_prorate
        {
            get { return m_Prorate; }
            set
            {
                m_Prorate = value;
                RaiseDataChanged("ccl_prorate");
            }
        }

        private string m_CreditorType = "N";

        /// <summary>
        /// Type of the fairshare for this creditor
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("creditor_type", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString)]
        public string creditor_type
        {
            get { return m_CreditorType; }
            set
            {
                m_CreditorType = value;
                RaiseDataChanged("creditor_type");
            }
        }

        private DebtPlus.Interfaces.Debt.DebtTypeEnum m_DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal;

        /// <summary>
        /// Type of the debt
        /// </summary>
        public DebtPlus.Interfaces.Debt.DebtTypeEnum DebtType
        {
            get { return m_DebtType; }
            set
            {
                m_DebtType = value;
                RaiseDataChanged("DebtType");
            }
        }

        /// <summary>
        /// Routine to set the debttype when loading
        /// </summary>
        /// <param name="SpecialCreditorList"></param>
        public void SetDebtType(DebtPlus.Interfaces.Debt.SpecialCreditors SpecialCreditorList)
        {
            // Process the normal record list (Disbursement debts don't have agencyaccount)
            DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal;

            // Look at active items only from this point to find the other types
            if (IsActive)
            {
                if (string.Compare(Creditor, SpecialCreditorList.MonthlyFeeCreditor, true) == 0)
                {
                    DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee;
                }
                else if (string.Compare(Creditor, SpecialCreditorList.DeductCreditor, true) == 0)
                {
                    DebtType = DebtPlus.Interfaces.Debt.DebtTypeEnum.Fairshare;
                }
            }
        }

        /// <summary>
        /// Is this a fee creditor?
        /// </summary>
        public bool IsFee
        {
            get
            {
                bool Answer = false;

                // If the item is a payoff for the debt then it is a fee amount and not proratable.
                // we want the money to pay off the debt taken off the top first since the disbursement
                // factor is reduced to the payoff amount.
                if (IsPayoff)
                {
                    Answer = true;
                }
                else
                {
                    Answer = false;

                    // Otherwise, look that the other flags and return false if the value is not a valid debt.
                    if (!hold_disbursements && IsActive)
                    {
                        if (DebtType == DebtPlus.Interfaces.Debt.DebtTypeEnum.FixedAgencyFee || DebtType == DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee || DebtType == DebtPlus.Interfaces.Debt.DebtTypeEnum.SetupFee)
                        {
                            Answer = true;
                        }
                    }
                }

                return Answer;
            }
        }

        private bool m_IsPayoff = false;

        /// <summary>
        /// Is the debt being paid off such that balance is less than or equal to the debit_amt?
        /// </summary>
        public bool IsPayoff
        {
            get { return m_IsPayoff; }
            set
            {
                m_IsPayoff = value;
                RaiseDataChanged("IsPayoff");
            }
        }

        /// <summary>
        /// Are we allowed to prorate this debt's amount?
        /// </summary>
        public bool IsProratable
        {
            get
            {
                if (!IsPayoff && !hold_disbursements && IsActive)
                {
                    switch (DebtType)
                    {
                        case DebtPlus.Interfaces.Debt.DebtTypeEnum.SetupFee:
                            return false;

                        case DebtPlus.Interfaces.Debt.DebtTypeEnum.AgencyFee:
                        case DebtPlus.Interfaces.Debt.DebtTypeEnum.Normal:
                            return true;

                        default:
                            break;
                    }
                }

                return false;
            }
        }

        private decimal m_TotalInterest = 0m;

        /// <summary>
        /// Total amount of interest paid on this debt
        /// </summary>
        public decimal total_interest
        {
            get { return m_TotalInterest; }
            set
            {
                m_TotalInterest = value;
                RaiseDataChanged("total_interest");
            }
        }

        private bool m_IsActive = true;

        /// <summary>
        /// Is this debt active or not?
        /// </summary>
        public bool IsActive
        {
            get { return m_IsActive; }
            set
            {
                m_IsActive = value;
                RaiseDataChanged("IsActive");
            }
        }

        /// <summary>
        /// Creditor division name
        /// </summary>
        public string cr_division_name
        {
            get { return string.Empty; }
            set { }
        }

        private object m_apr = System.DBNull.Value;

        /// <summary>
        /// Creditor interest rate for debts
        /// </summary>
        [DatabaseField("apr")]
        public object display_intererst_rate
        {
            get { return m_apr; }
            set
            {
                m_apr = value;
                RaiseDataChanged("apr");
            }
        }

        private DebtPlus.Interfaces.Debt.IDebtRecordList m_Parent = null;

        /// <summary>
        /// Linkage to the list of debts in which this is an item
        /// </summary>
        public DebtPlus.Interfaces.Debt.IDebtRecordList Parent
        {
            get { return m_Parent; }
            set
            {
                m_Parent = value;
                RaiseDataChanged("Parent");
            }
        }

        private bool m_HoldDisbursements = false;

        /// <summary>
        /// Is the debt marked "hold disbursements"?
        /// </summary>
        [DebtPlus.Svc.Debt.DatabaseField("held", DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean)]
        public bool hold_disbursements
        {
            get { return m_HoldDisbursements; }
            set
            {
                m_HoldDisbursements = value;
                RaiseDataChanged("hold_disbursements");
            }
        }

        bool DebtPlus.Interfaces.Disbursement.IDisburseable.IsHeld
        {
            get { return hold_disbursements; }
            set { hold_disbursements = value; }
        }

        /// <summary>
        /// Compare the items for the sort function
        /// </summary>
        /// <param name="CmpObj"></param>
        /// <returns></returns>
        public Int32 CompareTo(object CmpObj)
        {
            if (!(CmpObj is DisbursementDebtRecord))
            {
                throw new ArgumentException("Comparison type of DisbursementDebtRecord is expected", "obj");
            }
            return CompareTo((DisbursementDebtRecord)CmpObj);
        }

        /// <summary>
        /// Compare the items for the sort function
        /// </summary>
        /// <param name="CmpObj"></param>
        /// <returns></returns>
        public Int32 CompareTo(DisbursementDebtRecord CmpObj)
        {
            Int32 Order = DebtPlus.Utils.Nulls.DInt(CmpObj.line_number).CompareTo(DebtPlus.Utils.Nulls.DInt(line_number));
            if (Order == 0)
                Order = DebtPlus.Utils.Nulls.DDbl(CmpObj.dmp_interest).CompareTo(DebtPlus.Utils.Nulls.DDbl(dmp_interest));
            if (Order == 0)
                Order = CmpObj.display_current_balance.CompareTo(display_current_balance);
            if (Order == 0)
                Order = DebtPlus.Utils.Nulls.DDec(CmpObj.total_interest).CompareTo(total_interest);

            return Order;
        }

        #region "ISupportInitialize"

        /// <summary>
        /// Class initialization
        /// </summary>
        public void BeginInit()
        {
            InInit = true;
        }

        /// <summary>
        /// Class initialization
        /// </summary>
        public void EndInit()
        {
            if (InInit)
            {
                // Default the amount to be paid to the amount scheduled
                if (debit_amt == 0m && sched_payment > 0m)
                {
                    debit_amt = sched_payment;
                }

                // Limit the amount to the balance
                if (debit_amt > current_balance)
                {
                    debit_amt = current_balance;
                }

                // If the debt is on hold then the amount is, by definition, zero.
                if (hold_disbursements)
                {
                    debit_amt = 0m;
                }

                InInit = false;
            }
        }

        #endregion "ISupportInitialize"

        public event DebtPlus.Events.DataChangedEventHandler DataChanged;

        protected virtual void OnDataChanged(DebtPlus.Events.DataChangedEventArgs e)
        {
            if (!InInit)
            {
                if (DataChanged != null)
                {
                    DataChanged(this, e);
                }
            }
        }

        protected void RaiseDataChanged(string DataName)
        {
            if (!InInit)
            {
                OnDataChanged(new DebtPlus.Events.DataChangedEventArgs(DataName));
            }
        }

        #region "IDisposable Support"

        private bool disposedValue;

        // To detect redundant calls
        /// <summary>
        /// Dispose of local storage
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                }
                this.disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~DisbursementDebtRecord()
        {
            Dispose(false);
        }

        #endregion "IDisposable Support"

        #region "Items not used"

        // These items are not used for the Disbursement Debt record. They are, however, required
        // to complete the definition of a "debt record". So, just define them but throw a not-
        // implemented exception should they be refrenced.

        public int balance_client_creditor
        {
            get { return 0; }
            set { }
        }

        public object balance_verification_release
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public string balance_verify_by
        {
            get { return string.Empty; }
            set { }
        }

        public object balance_verify_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public bool ccl_agency_account
        {
            get { return false; }
            set { }
        }

        public bool ccl_always_disburse
        {
            get { return false; }
            set { }
        }

        public int check_payments
        {
            get { return 0; }
            set { }
        }

        public int client_creditor_balance
        {
            get { return 0; }
            set { }
        }

        public int client_creditor_proposal
        {
            get { return 0; }
            set { }
        }

        public string client_name
        {
            get { return string.Empty; }
            set { }
        }

        public string contact_name
        {
            get { return string.Empty; }
            set { }
        }

        public string cr_creditor_name
        {
            get { return string.Empty; }
            set { }
        }

        public decimal cr_min_accept_amt
        {
            get { return 0m; }
            set { }
        }

        public double cr_min_accept_pct
        {
            get { return 0.0; }
            set { }
        }

        public string cr_payment_balance
        {
            get { return string.Empty; }
            set { }
        }

        public string created_by
        {
            get { return string.Empty; }
            set { }
        }

        public decimal current_sched_payment
        {
            get { return 0m; }
            set { }
        }

        public System.DateTime date_created
        {
            get { return DateTime.MinValue; }
            set { }
        }

        public object date_disp_changed
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object date_proposal_prenoted
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object dmp_payout_interest
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object drop_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object drop_reason
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object drop_reason_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object drop_reason_sent
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object expected_payout_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object fairshare_pct_check
        {
            get { return 0.0; }
            set { }
        }

        public object fairshare_pct_eft
        {
            get { return 0.0; }
            set { }
        }

        public int first_payment
        {
            get { return 0; }
            set { }
        }

        public decimal first_payment_amt
        {
            get { return 0m; }
            set { }
        }

        public object first_payment_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public string first_payment_type
        {
            get { return string.Empty; }
            set { }
        }

        public decimal interest_this_creditor
        {
            get { return 0m; }
            set { }
        }

        public bool irs_form_on_file
        {
            get { return false; }
            set { }
        }

        public string last_communication
        {
            get { return string.Empty; }
            set { }
        }

        public int last_payment
        {
            get { return 0; }
            set { }
        }

        public decimal last_payment_amt
        {
            get { return 0m; }
            set { }
        }

        public object last_payment_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object last_payment_date_b4_dmp
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public string last_payment_type
        {
            get { return string.Empty; }
            set { }
        }

        public decimal last_stmt_balance
        {
            get { return 0m; }
            set { }
        }

        public object last_stmt_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public decimal MaximumPayment()
        {
            return MaximumPayment(current_balance);
        }

        public decimal MaximumPayment(decimal CurrentBalance)
        {
            return 0m;
        }

        public decimal MinimumPayment()
        {
            return MinimumPayment(current_balance);
        }

        public decimal MinimumPayment(decimal CurrentBalance)
        {
            return 0m;
        }

        public object Message
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public int months_delinquent
        {
            get { return 0; }
            set { }
        }

        public double non_dmp_interest
        {
            get { return 0.0; }
            set { }
        }

        public decimal non_dmp_payment
        {
            get { return 0m; }
            set { }
        }

        public object payment_rpps_mask
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public decimal payments_month_1
        {
            get { return 0m; }
            set { }
        }

        public decimal payments_this_creditor
        {
            get { return 0m; }
            set { }
        }

        public double percent_balance
        {
            get { return 0.0; }
            set { }
        }

        public int person
        {
            get { return 0; }
            set { }
        }

        public object prenote_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public decimal proposal_balance
        {
            get { return 0m; }
            set { }
        }

        public int proposal_status
        {
            get { return 0; }
            set { }
        }

        public decimal returns_this_creditor
        {
            get { return 0m; }
            set { }
        }

        public object rpps_client_type_indicator
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object rpps_mask
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public int send_bal_verify
        {
            get { return 0; }
            set { }
        }

        public bool send_drop_notice
        {
            get { return false; }
            set { }
        }

        public object start_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public bool student_loan_release
        {
            get { return false; }
            set { }
        }

        public int terms
        {
            get { return 60; }
            set { }
        }

        public decimal total_sched_payment
        {
            get { return 0m; }
            set { }
        }

        public object verify_request_date
        {
            get { return System.DBNull.Value; }
            set { }
        }

        public object creditor_interest
        {
            get { return System.DBNull.Value; }
            set { }
        }

        #endregion "Items not used"
    }

    partial class DisbursementDebtList : DebtPlus.Svc.Debt.DebtRecordList
    {
        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public DisbursementDebtList()
            : base(typeof(DisbursementDebtRecord))
        {
        }

        public DisbursementDebtList(Type ItemTypes)
            : base(ItemTypes)
        {
        }

        /// <summary>
        /// Answer questions from the routine to calculate the monthly fee
        /// </summary>
        public override object QueryFeeValue(string Item)
        {
            object Answer = null;

            switch (Item)
            {
                case "debit_amt":
                    DebtPlus.Interfaces.Debt.IProratable fee_creditor = MonthlyFeeDebt;
                    if (fee_creditor != null)
                    {
                        Answer = fee_creditor.debit_amt;
                    }
                    break;
                // Enable all of the limit values
                case DebtPlus.Events.ParameterValueEventArgs.name_LimitDebtBalance:
                    Answer = true;
                    break;

                case DebtPlus.Events.ParameterValueEventArgs.name_LimitMonthlyMax:
                    Answer = true;
                    break;

                case DebtPlus.Events.ParameterValueEventArgs.name_LimitDisbMax:
                    Answer = true;
                    break;

                default:
                    Answer = base.QueryFeeValue(Item);
                    break;
            }

            return Answer;
        }

        /// <summary>
        /// Calculate the number of creditors for the disbursement
        /// </summary>
        /// <returns>The number of creditors that are not held, are not a fee account, and have a disbursement amount and balance</returns>
        public override Int32 TotalCreditors()
        {
            Int32 answer = 0;
            foreach (IProratable record in this)
            {
                var _with1 = record;
                if (!_with1.IsHeld && !_with1.IsFee && _with1.debit_amt > 0m && _with1.current_balance > 0m)
                {
                    answer += 1;
                }
            }
            return answer;
        }

        /// <summary>
        /// Read the list of client debts for the disbursement operation
        /// </summary>
        protected Int32 disbursement_register;

        public void ReadDebtsTable(Int32 Client, Int32 disbursement_register)
        {
            this.disbursement_register = disbursement_register;
            base.ReadDebtsTable(Client);
        }

        /// <summary>
        /// Select command to read the debt information
        /// </summary>
        protected override SqlCommand view_debt_info_Select(Int32 ClientId)
        {
            var cmd = new System.Data.SqlClient.SqlCommand();
            cmd.CommandText = "xpr_disbursement_creditor_info";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
            cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;

            return cmd;
        }

        /// <summary>
        /// These functions are not implemented in the disbursement debt list
        /// </summary>
        public override void RefreshData()
        {
            throw new NotImplementedException();
        }
    }
}