#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using DebtPlus.Interfaces.Debt;
using DebtPlus.Svc.Common;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal partial class ClientClass : IDisposable, ISupportContext
    {
        public IDisbursementContext Context { get; set; }

        // Local storage for the client information
        protected internal DataSet ClientDS = new DataSet("ClientDS");

        protected internal DataRow LastExaminedClient = null;
        protected internal DataRow ClientROW = null;
        protected internal DisbursementDebtList DebtList = null;
        protected Examine_Form CurrentForm = null;

        /// <summary>
        /// Override fee calculation so that when debts are initially loaded the fees are
        /// calculated for percentage based fees only.
        /// </summary>
        public partial class FeeCalculation : MonthlyFeeCalculation
        {
            public FeeCalculation()
                : base()
            {
            }

            /// <summary>
            /// Use the debit_amt for the scheduled payment when we are working a disbursement batch
            /// </summary>
            protected override void OnQueryValue(DebtPlus.Events.ParameterValueEventArgs e)
            {
                // Look for a debit_amt figure if we want the scheduled pay.

                if (e.Name == DebtPlus.Events.ParameterValueEventArgs.name_SchedPayment)
                {
                    // If so, use the debit_amt instead. This is the normal amount being paid.
                    object NewValue = GetValue("debit_amt");
                    if (NewValue != null && !object.ReferenceEquals(NewValue, DBNull.Value))
                    {
                        decimal DecimalValue = Convert.ToDecimal(NewValue);
                        if (DecimalValue > 0m)
                        {
                            e.Value = DecimalValue;
                            return;
                        }
                    }
                }

                // Everything else is simply passed along.
                base.OnQueryValue(e);
            }
        }

        /// <summary>
        /// Pointer to the routine to calculate the monthly client fee
        /// </summary>

        private FeeCalculation privateFeeInfo;

        public FeeCalculation FeeInfo
        {
            get
            {
                if (privateFeeInfo == null)
                {
                    privateFeeInfo = new FeeCalculation();
                }
                return privateFeeInfo;
            }
        }

        /// <summary>
        /// Suppress the update events for the process
        /// </summary>
        private Int32 UpdateCount;

        public void BeginUpdate()
        {
            UpdateCount += 1;
        }

        /// <summary>
        /// Complete the update operation
        /// </summary>
        public void EndUpdate()
        {
            if (UpdateCount < 2)
            {
                UpdateCount = 0;
            }
            else
            {
                UpdateCount -= 1;
            }
        }

        /// <summary>
        /// Return TRUE if the update flag is set
        /// </summary>
        public bool InUpdate
        {
            get { return UpdateCount > 0; }
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        private ClientClass()
            : base()
        {
        }

        public ClientClass(IDisbursementContext Context)
            : this()
        {
            this.Context = Context;
        }

        public ClientClass(IDisbursementContext Context, DataRow row, ref DisbursementDebtList DebtList)
            : this(Context)
        {
            // Save the parameters for processing later
            ClientROW = row;

            // Save the list of debts that we are to process
            this.DebtList = DebtList;
        }

        /// <summary>
        /// Pointer to the previous client
        /// </summary>
        public ClientClass Previous { get; set; }

        /// <summary>
        /// Determine the disbursement register
        /// </summary>
        protected internal Int32 disbursement_register
        {
            get { return Convert.ToInt32(ClientROW["disbursement_register"]); }
        }

        /// <summary>
        /// Determine the client ID
        /// </summary>
        protected internal Int32 ClientId
        {
            get { return Convert.ToInt32(ClientROW["client"]); }
        }

        /// <summary>
        /// Determine the number of disbursement notes for the client
        /// </summary>
        protected internal Int32 note_count
        {
            get { return Convert.ToInt32(ClientROW["note_count"]); }
        }

        /// <summary>
        /// Determine the trust funds available for the client
        /// </summary>
        protected internal virtual decimal held_in_trust
        {
            get
            {
                decimal answer = Convert.ToDecimal(ClientROW["held_in_trust"]);
                return answer;
            }
        }

        /// <summary>
        /// Determine the trust funds on hold for the client
        /// </summary>
        protected internal decimal trust_funds_on_hold
        {
            get
            {
                decimal answer = Convert.ToDecimal(ClientROW["reserved_in_trust"]);
                return answer;
            }
        }

        /// <summary>
        /// Process the SKIP button event
        /// </summary>
        public virtual void Do_Skip()
        {
            if (CurrentForm != null)
            {
                CurrentForm.DialogResult = DialogResult.Cancel;
            }
        }

        /// <summary>
        /// Process the NO PAY button event
        /// </summary>
        public virtual void Do_NoPay()
        {
            // Reset any pending information on the prior disbursement of this client
            // This will reset the amounts paid to each of the creditors in the database.
            DialogResult RetryStatus = default(DialogResult);

            do
            {
                RetryStatus = DialogResult.Ignore;

                var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                SqlTransaction txn = null;
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted);

                    // First, remove any payments made by this client
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = "xpr_disbursement_transaction_start";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                        cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
                        cmd.ExecuteNonQuery();
                    }

                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = "xpr_disbursement_transaction_end";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                        cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
                        cmd.Parameters.Add("@debit_amt", SqlDbType.Decimal).Value = 0m;
                        cmd.ExecuteNonQuery();
                    }

                    // Commit the changes to the database now
                    txn.Commit();
                    txn.Dispose();
                    txn = null;

                    // Terminate the form normally with OK status
                    if (CurrentForm != null)
                    {
                        CurrentForm.DialogResult = DialogResult.OK;
                    }
                }
                catch (SqlException ex)
                {
                    const string ErrorTitle = "Error doing the NO-PAY operation";
                    RetryStatus = DebtPlus.Data.Forms.MessageBox.Show(ex.Message, ErrorTitle, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                finally
                {
                    if (txn != null)
                    {
                        try { txn.Rollback(); }
                        catch { }
                        txn.Dispose();
                        txn = null;
                    }

                    if (cn != null)
                        cn.Dispose();

                    Cursor.Current = current_cursor;
                }
            } while (!(RetryStatus != DialogResult.Retry));
        }

        /// <summary>
        /// Process the PAY button event
        /// </summary>
        public virtual decimal Do_Pay()
        {
            decimal debit_amt = default(decimal);

            // Reset any pending information on the prior disbursement of this client
            // This will reset the amounts paid to each of the creditors in the database.
            DialogResult RetryStatus = default(DialogResult);

            do
            {
                debit_amt = 0m;
                RetryStatus = DialogResult.Ignore;

                var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                SqlTransaction txn = null;
                Cursor current_cursor = Cursor.Current;

                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();
                    txn = cn.BeginTransaction(IsolationLevel.ReadUncommitted);

                    // First, remove any payments made by this client
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = "xpr_disbursement_transaction_start";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                        cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
                        cmd.ExecuteNonQuery();
                    }

                    // Process the payments made by the debt list
                    foreach (DisbursementDebtRecord DebtInfo in DebtList)
                    {
                        // If an amount is paid, then pay the debt.
                        if (DebtInfo.debit_amt > 0m)
                        {
                            debit_amt += DebtInfo.debit_amt;

                            // Pay the debt.
                            using (var cmd = new SqlCommand())
                            {
                                cmd.Connection = cn;
                                cmd.Transaction = txn;
                                cmd.CommandText = "xpr_disbursement_transaction_trans";
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                                cmd.Parameters.Add("@client_creditor", SqlDbType.Int).Value = DebtInfo.client_creditor;
                                cmd.Parameters.Add("@debit_amt", SqlDbType.Decimal).Value = DebtInfo.debit_amt;
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }

                    // Complete the payments on this client
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.Transaction = txn;
                        cmd.CommandText = "xpr_disbursement_transaction_end";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = disbursement_register;
                        cmd.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
                        cmd.Parameters.Add("@debit_amt", SqlDbType.Decimal).Value = debit_amt;
                        cmd.ExecuteNonQuery();
                    }

                    // Commit the changes to the database now
                    txn.Commit();
                    txn.Dispose();
                    txn = null;

                    // Terminate the form normally with OK status
                    if (CurrentForm != null)
                    {
                        CurrentForm.DialogResult = DialogResult.OK;
                    }
                }
                catch (SqlException ex)
                {
                    const string ErrorTitle = "Error doing the PAY operation";
                    RetryStatus = DebtPlus.Data.Forms.MessageBox.Show(ex.Message, ErrorTitle, MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                finally
                {
                    if (txn != null)
                    {
                        try { txn.Rollback(); }
                        catch { }
                        txn.Dispose();
                        txn = null;
                    }

                    if (cn != null)
                        cn.Dispose();
                    Cursor.Current = current_cursor;
                }
            } while (!(RetryStatus != DialogResult.Retry));

            // Tell the caller the total dollar amount paid
            return debit_amt;
        }

        /// <summary>
        /// Process the QUIT button event
        /// </summary>
        public virtual void Do_Quit()
        {
            if (DebtPlus.Data.Forms.MessageBox.Show(string.Format("WARNING: This will terminate the disbursement and stop all further processing for clients.{0}The current client will NOT be paid.{0}You must reexecute the disbursement processing to return to this disbursement.{0}{0}Are you sure that you wish to stop processing immediately?", Environment.NewLine), "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                // Set the top most "let's bug out" flag
                Context.MasterQuit = true;

                // Force the dialog to terminate with an abort status if indicated
                if (CurrentForm != null)
                {
                    CurrentForm.DialogResult = DialogResult.Abort;
                }
            }
        }

        /// <summary>
        /// Process the PRORATE button event
        /// </summary>
        public virtual void Do_Prorate()
        {
            Prorate(held_in_trust);
        }

        /// <summary>
        /// Process the EXAMINE button event
        /// </summary>
        public virtual DialogResult Do_Examine()
        {
            using (Examine_Form CurrentForm = new Examine_Form(Context, this))
            {
                return CurrentForm.ShowDialog();
            }
        }

        /// <summary>
        /// Local routine so that it may be invoked by a thread
        /// </summary>
        public void Do_Examine_ThreadProcedure(object obj)
        {
            Do_Examine();
        }

        /// <summary>
        /// Process the EXAMINE PREVIOUS button event
        /// </summary>
        public virtual void Do_ExaminePrevious()
        {
            if ((!Context.MasterQuit) && (Previous != null))
            {
                if (Previous.Do_Examine() == DialogResult.Abort && (CurrentForm != null))
                {
                    CurrentForm.DialogResult = DialogResult.Abort;
                }
            }
        }

        /// <summary>
        /// Determine if we can show the previous client information
        /// </summary>
        public bool CanExaminePrevious
        {
            get { return (Previous != null) && (!Context.MasterQuit); }
        }

        /// <summary>
        /// Determine if we can show the client notes
        /// </summary>
        public bool CanShowNotes
        {
            //Always show notes, we say "yes".
            get { return true; }
        }

        /// <summary>
        /// Determine if we can do the prorate function
        /// </summary>
        public bool CanProrate
        {
            // to always allow prorate, say yes.
            get { return true; }
        }

        /// <summary>
        /// Do the prorate operation on the debts list
        /// </summary>
        private void Prorate(decimal TrustBalance)
        {
            using (DisbursementProrate cls = new DisbursementProrate((IProratableList)DebtList))
            {
                cls.Prorate(TrustBalance);
            }
        }

        /// <summary>
        /// Recalculate the monthly fee amount from the disbursement information
        /// </summary>

        public void RecalulateMonthlyFee()
        {
            // Recalculate the fee amount for the debt based upon the payment information
            IProratable FeeDebt = DebtList.MonthlyFeeDebt as IProratable;

            if (FeeDebt != null)
            {
                // Find the new fee figure for the debt and update it.
                try
                {
                    FeeInfo.QueryValue += FeeInfo_QueryValue;
                    decimal FeeAmount = FeeInfo.FeeAmount();
                    if (FeeDebt.debit_amt != FeeAmount)
                    {
                        FeeDebt.debit_amt = FeeAmount;
                    }
                }
                finally
                {
                    FeeInfo.QueryValue -= FeeInfo_QueryValue;
                }
            }
        }

        /// <summary>
        /// Hook into the fee query process
        /// </summary>

        public virtual void FeeInfo_QueryValue(object Sender, DebtPlus.Events.ParameterValueEventArgs e)
        {
            // We can handle the ClientId as a request here.
            if (string.Compare(e.Name, DebtPlus.Events.ParameterValueEventArgs.name_client, true) == 0)
            {
                e.Value = ClientId;
                return;
            }

            // Others are simply passed up the chain
            e.Value = ((IFeeable)DebtList).QueryFeeValue(e.Name);
        }

        // To detect redundant calls
        private bool disposedValue;

        /// <summary>
        /// Handle the dispose method
        /// </summary>

        protected virtual void Dispose(bool disposing)
        {
            // If we have not disposed of the storage then do so now.
            if (!disposedValue)
            {
                if (disposing)
                {
                    ClientDS.Dispose();
                    DebtList.Clear();
                }

                // Release the storage pointers once the items have been removed
                Context = null;
                Previous = null;
                ClientROW = null;
                ClientDS = null;
                DebtList = null;
            }
            disposedValue = true;
        }

        /// <summary>
        /// Dispose of the class
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ClientClass()
        {
            Dispose(false);
        }
    }
}