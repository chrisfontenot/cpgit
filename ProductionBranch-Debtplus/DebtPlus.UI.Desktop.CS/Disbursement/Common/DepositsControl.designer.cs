using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
	partial class DepositsControl : DevExpress.XtraEditors.XtraUserControl
	{
		//UserControl overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing) {
					if (components != null)
						components.Dispose();
					ds.Dispose();
					UnRegisterHandlers();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer
		private System.ComponentModel.IContainer components = null;
		
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.LastDeposit = new DevExpress.XtraEditors.LabelControl();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_deposit_amount = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_deposit_amount.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.GridColumn_deposit_date = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_deposit_date.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			this.SuspendLayout();
			//
			//SimpleButton1
			//
			this.SimpleButton1.Location = new System.Drawing.Point(0, 0);
			this.SimpleButton1.Name = "SimpleButton1";
			this.SimpleButton1.Size = new System.Drawing.Size(75, 23);
			this.SimpleButton1.TabIndex = 1;
			this.SimpleButton1.Text = "Deposits...";
			//
			//LabelControl1
			//
			this.LabelControl1.Location = new System.Drawing.Point(3, 29);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(20, 13);
			this.LabelControl1.TabIndex = 2;
			this.LabelControl1.Text = "Last";
			//
			//LastDeposit
			//
			this.LastDeposit.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.LastDeposit.Appearance.Options.UseTextOptions = true;
			this.LastDeposit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LastDeposit.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.LastDeposit.Location = new System.Drawing.Point(29, 29);
			this.LastDeposit.Name = "LastDeposit";
			this.LastDeposit.Size = new System.Drawing.Size(116, 13);
			this.LastDeposit.TabIndex = 3;
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.EmbeddedNavigator.Name = "";
			this.GridControl1.Location = new System.Drawing.Point(0, 51);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.Size = new System.Drawing.Size(148, 50);
			this.GridControl1.TabIndex = 4;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_deposit_amount,
				this.GridColumn_deposit_date
			});
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
			this.GridView1.OptionsBehavior.AutoSelectAllInEditor = false;
			this.GridView1.OptionsBehavior.AutoUpdateTotalSummary = false;
			this.GridView1.OptionsBehavior.Editable = false;
			this.GridView1.OptionsCustomization.AllowColumnMoving = false;
			this.GridView1.OptionsCustomization.AllowFilter = false;
			this.GridView1.OptionsCustomization.AllowGroup = false;
			this.GridView1.OptionsCustomization.AllowSort = false;
			this.GridView1.OptionsDetail.AllowZoomDetail = false;
			this.GridView1.OptionsDetail.EnableMasterViewMode = false;
			this.GridView1.OptionsDetail.ShowDetailTabs = false;
			this.GridView1.OptionsDetail.SmartDetailExpand = false;
			this.GridView1.OptionsFilter.AllowColumnMRUFilterList = false;
			this.GridView1.OptionsFilter.AllowFilterEditor = false;
			this.GridView1.OptionsFilter.AllowMRUFilterList = false;
			this.GridView1.OptionsHint.ShowCellHints = false;
			this.GridView1.OptionsHint.ShowColumnHeaderHints = false;
			this.GridView1.OptionsLayout.StoreDataSettings = false;
			this.GridView1.OptionsLayout.StoreVisualOptions = false;
			this.GridView1.OptionsMenu.EnableColumnMenu = false;
			this.GridView1.OptionsMenu.EnableFooterMenu = false;
			this.GridView1.OptionsMenu.EnableGroupPanelMenu = false;
			this.GridView1.OptionsNavigation.AutoMoveRowFocus = false;
			this.GridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
			this.GridView1.OptionsSelection.EnableAppearanceFocusedRow = false;
			this.GridView1.OptionsSelection.EnableAppearanceHideSelection = false;
			this.GridView1.OptionsSelection.UseIndicatorForSelection = false;
			this.GridView1.OptionsView.ShowColumnHeaders = false;
			this.GridView1.OptionsView.ShowDetailButtons = false;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowHorizontalLines = DevExpress.Utils.DefaultBoolean.True;
			this.GridView1.OptionsView.ShowIndicator = false;
			this.GridView1.OptionsView.ShowPreviewRowLines = DevExpress.Utils.DefaultBoolean.True;
			this.GridView1.OptionsView.ShowVerticalLines = DevExpress.Utils.DefaultBoolean.False;
			//
			//GridColumn_deposit_amount
			//
			this.GridColumn_deposit_amount.Caption = "Amount";
			this.GridColumn_deposit_amount.CustomizationCaption = "Deposit Amount";
			this.GridColumn_deposit_amount.DisplayFormat.FormatString = "c2";
			this.GridColumn_deposit_amount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_deposit_amount.FieldName = "deposit_amount";
			this.GridColumn_deposit_amount.Name = "GridColumn_deposit_amount";
			this.GridColumn_deposit_amount.Visible = true;
			this.GridColumn_deposit_amount.VisibleIndex = 0;
			//
			//GridColumn_deposit_date
			//
			this.GridColumn_deposit_date.Caption = "Date";
			this.GridColumn_deposit_date.CustomizationCaption = "Deposit Date";
			this.GridColumn_deposit_date.DisplayFormat.FormatString = "d";
			this.GridColumn_deposit_date.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
			this.GridColumn_deposit_date.FieldName = "deposit_date";
			this.GridColumn_deposit_date.Name = "GridColumn_deposit_date";
			this.GridColumn_deposit_date.Visible = true;
			this.GridColumn_deposit_date.VisibleIndex = 1;
			//
			//DepositsControl
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.GridControl1);
			this.Controls.Add(this.LastDeposit);
			this.Controls.Add(this.LabelControl1);
			this.Controls.Add(this.SimpleButton1);
			this.MaximumSize = new System.Drawing.Size(148, 101);
			this.Name = "DepositsControl";
			this.Size = new System.Drawing.Size(148, 101);
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();
		}

		internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
		internal DevExpress.XtraEditors.LabelControl LabelControl1;
		internal DevExpress.XtraEditors.LabelControl LastDeposit;
		internal DevExpress.XtraGrid.GridControl GridControl1;
		internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_deposit_amount;
		internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_deposit_date;
	}
}
