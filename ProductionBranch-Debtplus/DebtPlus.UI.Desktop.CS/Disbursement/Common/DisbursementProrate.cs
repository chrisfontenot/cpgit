#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using DebtPlus.Interfaces.Debt;
using DebtPlus.Svc.Common;
using DebtPlus.Svc.Debt;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    /// <summary>
    /// Override prorate to handle special cases for disbursement
    /// </summary>
    public class DisbursementProrate : ProrateDebts
    {
        /// <summary>
        /// Create a new instance of our class
        /// </summary>
        public DisbursementProrate()
            : base()
        {
        }

        public DisbursementProrate(IProratableList Debts)
            : base(Debts)
        {
        }

        /// <summary>
        /// Do the prorate operation
        /// </summary>
        private decimal LocalTrustBalance = 0m;

        private bool FirstTimeAtProrate;

        public override void Prorate(decimal TrustBalance)
        {
            // Save the values for the calculations
            MonthlyFeeCalculation FeeClass = null;
            LocalTrustBalance = TrustBalance;
            availableFunds = TrustBalance;
            FirstTimeAtProrate = true;

            try
            {
                FeeClass = new MonthlyFeeCalculation();
                FeeClass.QueryValue += fees_QueryValue;

                // Adjust the PAF creditor's scheduled payment with the correct amount
                IProratable Record = DebtList.MonthlyFeeDebt;
                if (Record != null)
                {
                    Record.debit_amt     = FeeClass.FeeAmount();
                    Record.sched_payment = Record.debit_amt;
                }

                // Use the new fee class for the debts
                FirstTimeAtProrate = false;

                // Find the amount of money that needs to be "taken off the top" of the payments and not prorated.
                decimal Reserved = DebtList.TotalFees();

                // Do an initial stab at the proration procedure.
                TryProrate(TrustBalance - Reserved);

                // Limit the disbursements to the debt balances where they are needed.
                decimal LastPayments = default(decimal);
                decimal CurrentPayments = DebtList.TotalPayments();
                do
                {
                    LastPayments = CurrentPayments;

                    // If the payments match the expected or there are no payments then we have nothing left to do
                    if (LastPayments + Reserved == TrustBalance)
                    {
                        break;
                    }

                    if (LastPayments == 0m)
                    {
                        break;
                    }

                    // Adjust the PAF creditor's scheduled payment with the correct amount
                    Record = DebtList.MonthlyFeeDebt;
                    if (Record != null)
                    {
                        Record.debit_amt     = FeeClass.FeeAmount();
                        Record.sched_payment = Record.debit_amt;
                    }

                    // Do the prorate function again once the values have changed
                    TryProrate(TrustBalance - Reserved);

                    // If the value did not change then we have reached equilibrium and stop
                    CurrentPayments = DebtList.TotalPayments();
                } while (!(CurrentPayments == LastPayments));
            }
            finally
            {
                if (FeeClass != null)
                    FeeClass.Dispose();
            }
        }

        /// <summary>
        /// Determine the value if we know for the requested item
        /// </summary>
        protected override void fees_QueryValue(object sender, Events.ParameterValueEventArgs e)
        {
            switch (e.Name)
            {
                case DebtPlus.Events.ParameterValueEventArgs.name_DollarsDisbursed:
                    if (FirstTimeAtProrate)
                    {
                        e.Value = LocalTrustBalance;
                    }
                    else
                    {
                        e.Value = DebtList.TotalPayments();
                    }

                    break;

                default:
                    base.fees_QueryValue(sender, e);
                    break;
            }
        }
    }
}