using System;
using System.Data;
using System.Reflection;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    //     Debts for the disbursement operation come from the disbursement
    //     tables and not the debts table. As such, they don't have as many
    //     fields as the normal debts and some of the fields that are read-only
    //     on the normal debts are read/write here. But, these debts need to be
    //     proratable as well as able to calculate the monthly fees.
    partial class DisbursementDebtRecord : DebtPlus.Interfaces.Debt.IDebtRecordLoad
    {
        /// <summary>
        /// Load the record
        /// </summary>
        /// <param name="rdr"></param>
        public void LoadRecord(System.Type RecordType, ref IDataReader rdr)
        {
            for (Int32 indx = rdr.FieldCount - 1; indx >= 0; indx += -1)
            {
                if (!rdr.IsDBNull(indx))
                {
                    ProcessField(RecordType, rdr.GetName(indx), rdr.GetValue(indx));
                }
            }
        }

        /// <summary>
        /// Load the record
        /// </summary>
        /// <param name="row"></param>
        public void LoadRecord(System.Type RecordType, ref DataRow row)
        {
            DataTable tbl = row.Table;
            foreach (DataColumn col in tbl.Columns)
            {
                if (!row.IsNull(col))
                {
                    ProcessField(RecordType, col.ColumnName, row[col]);
                }
            }
        }

        /// <summary>
        /// Set the field value in the debt record to the passed parameter.
        /// </summary>
        /// <param name="RecordType"></param>
        /// <param name="FieldName"></param>
        /// <param name="objValue"></param>
        private void ProcessField(System.Type RecordType, string FieldName, object objValue)
        {
            PropertyInfo[] properties = RecordType.GetProperties();

            // Process the properties to set the current field to its value
            foreach (PropertyInfo propertyField in properties)
            {
                object[] propertyAttributeList = propertyField.GetCustomAttributes(typeof(DebtPlus.Svc.Debt.DatabaseFieldAttribute), true);

                if (propertyAttributeList != null && propertyAttributeList.Length == 1)
                {
                    DebtPlus.Svc.Debt.DatabaseFieldAttribute customAttr = propertyAttributeList[0] as DebtPlus.Svc.Debt.DatabaseFieldAttribute;
                    if (customAttr != null)
                    {
                        string propertyFieldName = customAttr.ColumnName;
                        DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType TypeOfField = customAttr.TypeOfField;

                        // Per SQL, ignore the case of the column name. If the database is case sensitive, we need to change this.

                        if (string.Compare(FieldName, propertyFieldName, true, System.Globalization.CultureInfo.InvariantCulture) == 0)
                        {
                            // If the item is not an object then convert the data to the appropriate type
                            switch (TypeOfField)
                            {
                                case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeObject:
                                    break;

                                case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeInt32:
                                    objValue = Convert.ToInt32(objValue);
                                    break;

                                case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeBoolean:
                                    objValue = Convert.ToBoolean(objValue);
                                    break;

                                case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDate:
                                    objValue = Convert.ToDateTime(objValue);
                                    break;

                                case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDecimal:
                                    objValue = Convert.ToDecimal(objValue);
                                    break;

                                case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeDouble:
                                    objValue = Convert.ToDouble(objValue);
                                    break;

                                case DebtPlus.Svc.Debt.DatabaseFieldAttribute.FieldType.TypeString:
                                    objValue = Convert.ToString(objValue);
                                    break;

                                default:
                                    break;
                            }

                            // Set the propertyField to the current column value
                            propertyField.SetValue(this, objValue, null);
                            break;
                        }
                    }
                }
            }
        }
    }
}