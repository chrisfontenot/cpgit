#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal partial class GenerateRPSPayments : object
    {
        /// <summary>
        /// Number of batch records written to the file
        /// </summary>
        private Int32 m_TotalFile_BatchCount = 0;

        private Int32 TotalFile_BatchCount
        {
            get { return m_TotalFile_BatchCount; }
            set
            {
                m_TotalFile_BatchCount = value;
                if (status_form != null)
                {
                    status_form.total_batches = value;
                }
            }
        }

        /// <summary>
        /// Number of blocks written to the file
        /// </summary>
        private Int32 m_BlockCount = 0;

        private Int32 BlockCount
        {
            get { return m_BlockCount; }
            set { m_BlockCount = value; }
        }

        /// <summary>
        /// Number of records written to the text file
        /// </summary>
        private Int32 m_TotalFile_DetailCount = 0;

        private Int32 TotalFile_DetailCount
        {
            get { return m_TotalFile_DetailCount; }
            set
            {
                m_TotalFile_DetailCount = value;
                if (status_form != null)
                {
                    status_form.total_items = value;
                }
            }
        }

        /// <summary>
        /// Debit (paid) dollars in the file
        /// </summary>
        private Int32 m_TotalFile_Debit = 0;

        private Int32 TotalFile_Debit
        {
            get { return m_TotalFile_Debit; }
            set
            {
                m_TotalFile_Debit = value;
                if (status_form != null)
                {
                    status_form.total_debt = Convert.ToDecimal(value) / 100m;
                }
            }
        }

        /// <summary>
        /// Credit (returned) dollars in the file
        /// </summary>
        private Int32 m_TotalFile_Credit = 0;

        private Int32 TotalFile_Credit
        {
            get { return m_TotalFile_Credit; }
            set
            {
                m_TotalFile_Credit = value;
                if (status_form != null)
                {
                    status_form.total_credit = Convert.ToDecimal(value) / 100m;
                }
            }
        }

        /// <summary>
        /// Debit amount for the batch
        /// </summary>
        private Int32 m_Batch_Debit = 0;

        private Int32 Batch_Debit
        {
            get { return m_Batch_Debit; }
            set
            {
                m_Batch_Debit = value;
                if (status_form != null)
                {
                    status_form.total_debt = Convert.ToDecimal(value + TotalFile_Debit) / 100m;
                }
            }
        }

        /// <summary>
        /// Credit amount for the batch
        /// </summary>
        private Int32 m_Batch_Credit = 0;

        private Int32 Batch_Credit
        {
            get { return m_Batch_Credit; }
            set
            {
                m_Batch_Credit = value;
                if (status_form != null)
                {
                    status_form.total_credit = Convert.ToDecimal(value + TotalFile_Credit) / 100m;
                }
            }
        }

        /// <summary>
        /// Number of detail records for this batch
        /// </summary>
        private Int32 m_Batch_DetailCount = 0;

        private Int32 Batch_DetailCount
        {
            get { return m_Batch_DetailCount; }
            set
            {
                m_Batch_DetailCount = value;
                if (status_form != null)
                {
                    status_form.total_items = value + TotalFile_DetailCount;
                }
            }
        }

        /// <summary>
        /// Total billed amount
        /// </summary>
        private Int32 m_TotalFile_Billed = 0;

        private Int32 TotalFile_Billed
        {
            get { return m_TotalFile_Billed; }
            set
            {
                m_TotalFile_Billed = value;
                if (status_form != null)
                {
                    status_form.total_billed = Convert.ToDecimal(value) / 100m;
                }
            }
        }

        /// <summary>
        /// Total gross dollars amount
        /// </summary>
        private Int32 m_TotalFile_Gross = 0;

        private Int32 TotalFile_Gross
        {
            get { return m_TotalFile_Gross; }
            set
            {
                m_TotalFile_Gross = value;
                if (status_form != null)
                {
                    status_form.total_gross = Convert.ToDecimal(value) / 100m;
                }
            }
        }

        /// <summary>
        /// Total net dollars amount
        /// </summary>
        private Int32 m_TotalFile_Net = 0;

        private Int32 TotalFile_Net
        {
            get { return m_TotalFile_Net; }
            set
            {
                m_TotalFile_Net = value;
                if (status_form != null)
                {
                    status_form.total_net = Convert.ToDecimal(value) / 100m;
                }
            }
        }

        /// <summary>
        /// Number of prenote records
        /// </summary>
        private Int32 m_Prenote_Count = 0;

        private Int32 Prenote_Count
        {
            get { return m_Prenote_Count; }
            set
            {
                m_Prenote_Count = value;
                if (status_form != null)
                {
                    status_form.total_prenotes = value;
                }
            }
        }

        // Pointer to our status form

        private Iform_RPS status_form = null;
        // Current date for the batch

        private System.DateTime CurrentDate = System.DateTime.Now.Date;

        // Public information
        public Int32 bank = 0;

        public Int32 disbursement_register = 0;
        public static string FileName = string.Empty;
        private StreamWriter OutputTextFile = null;

        private Int32 eft_file = 0;
        private bool Fake = false;
        private bool CIE_Format = false;

        private Int32 BillerType = 0;

        // Information from the banks table
        private string RPS_ID = string.Empty;

        private string RPS_Name = string.Empty;
        private string RPS_Prefix = string.Empty;

        private string RPS_Suffix = string.Empty;

        // Value for the current day's batches
        private Int32 Batch_ID = 0;

        // Value for the current day's transactions
        private Int32 transaction_id = 0;

        private bool OrigTest = false;
        private string open_batch_id = string.Empty;
        private string open_biller_id = string.Empty;
        private string Open_Biller_Name = string.Empty;
        private string Open_Biller_Class = string.Empty;

        private string Open_Biller_Description = string.Empty;
        private DataSet ds;
        private DataTable tbl;
        private DataView vue;
        private DataRowView drv;

        private Int32 RowNumber;

        // Sequence Counters
        // Number of transactions processed
        private Int32 transaction_number = 0;

        private EncoderReplacementFallback erf;
        private DecoderReplacementFallback drf;

        private Encoding ae;

        public GenerateRPSPayments()
            : base()
        {
            OrigTest       = false;
            Prenote_Count  = 0;

            // Seed values for the sequence numbers to ensure that the values increase for days
            Int32 doy      = CurrentDate.DayOfYear;
            transaction_id = ((doy % 98) + 1) * 100000;
            Batch_ID       = doy * 10000;

            // Create the encoding classes
            erf            = new EncoderReplacementFallback(" ");
            drf            = new DecoderReplacementFallback(" ");
            ae             = Encoding.GetEncoding("us-ascii", erf, drf);
        }

        /// <summary>
        /// Ensure message does not have funny characters
        /// </summary>
        private string clean_string(string message)
        {
            // Find the length of the input stream. It should be the number of bytes for the input.
            Int32 Length = ae.GetByteCount(message);
            if (Length < 1)
            {
                return string.Empty;
            }

            // Convert the string to a list of bytes. This removes the non-ASCII characters.
            byte[] ab = new byte[Length];
            ae.GetBytes(message, 0, message.Length, ab, 0);

            // Convert the bytes back to a string as ASCII characters
            return ae.GetString(ab);
        }

        /// <summary>
        /// Process the RPS payment prenotes
        /// </summary>
        public void Generate_Payment_Prenotes(Int32 eft_file)
        {
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_rpps_generate_prenote";
                        cmd.CommandTimeout = System.Math.Min(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = eft_file;
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        /// <summary>
        /// Write the text record
        /// </summary>

        private void WriteRecord(string strRecord)
        {
            // Count this record in the system
            Int32 _FileRecordNumber = FileRecordNumber();

            if (!Fake)
            {
                OutputTextFile.WriteLine(strRecord);
            }
        }

        /// <summary>
        /// Record number generated in the file
        /// </summary>
        private Int32 static_FileRecordNumber__FileRecordNumber;

        private Int32 FileRecordNumber()
        {
            static_FileRecordNumber__FileRecordNumber += 1;
            return static_FileRecordNumber__FileRecordNumber;
        }

        /// <summary>
        /// Read the configuration from the system table
        /// </summary>
        public bool ReadPaymentConfiguration()
        {
            CIE_Format  = true;          // Use the CIE format for "customer initiated requests"
            RPS_Name    = string.Empty;
            RPS_ID      = string.Empty;
            RPS_Prefix  = string.Empty;
            RPS_Suffix  = string.Empty;

            // Fetch the values from the banks table
            using (var cm = new DebtPlus.UI.Common.CursorManager())
            {
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    cn.Open();
                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandText = "xpr_rpps_config_payment";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank;

                        using (var rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow))
                        {
                            if (rd.Read())
                            {
                                for (Int32 FieldNo = 0; FieldNo <= rd.FieldCount - 1; FieldNo++)
                                {
                                    if (!rd.IsDBNull(FieldNo))
                                    {
                                        switch (rd.GetName(FieldNo).ToLower())
                                        {
                                            case "rps_id":
                                            case "rpps_id":
                                                RPS_ID = Convert.ToString(rd.GetValue(FieldNo));
                                                break;

                                            case "rps_origin_name":
                                            case "rpps_origin_name":
                                                RPS_Name = Convert.ToString(rd.GetValue(FieldNo));
                                                break;

                                            case "rps_prefix":
                                            case "rpps_prefix":
                                                RPS_Prefix = Convert.ToString(rd.GetValue(FieldNo));
                                                break;

                                            case "rps_suffix":
                                            case "rpps_suffix":
                                                RPS_Suffix = Convert.ToString(rd.GetValue(FieldNo));
                                                break;

                                            case "batch_id":
                                                Batch_ID = Convert.ToInt32(rd.GetValue(FieldNo));
                                                break;

                                            case "transaction_id":
                                                transaction_id = Convert.ToInt32(rd.GetValue(FieldNo));
                                                break;

                                            case "cie_format":
                                                CIE_Format = Convert.ToInt32(rd.GetValue(FieldNo)) != 0;
                                                break;

                                            case "bank":
                                                if (bank <= 0)
                                                {
                                                    bank = Convert.ToInt32(rd.GetValue(FieldNo));
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // Update the parameters for the configuration
            if (! string.IsNullOrEmpty(RPS_Name))
            {
                RPS_ID = RPS_ID.PadLeft(8, '0');
                if (RPS_ID != "00000000")
                {
                    return true;
                }
            }

            return DebtPlus.Data.Forms.MessageBox.Show("The RPS Biller ID is not configured properly.\r\nThis is configured in the configuration tool under the RPS tab.\r\n\r\nIf you wish to continue then no output file will be generated but the RPS transactions\r\nwill be allocated to a fake disbursement.\r\n\r\nIf you don't continue now then please set the proper ORIGINATOR (8 digit) ID into the system.\r\n\r\nDo you wish to continue?", "Configuration Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes;
        }

        /// <summary>
        /// Obtain the next trace number for a detail record
        /// </summary>
        private string Next_Detail_Trace()
        {
            Int32 Remainder = default(Int32);

            // Bump the number of transactions generated
            transaction_number += 1;

            // Ensure that the trace number wraps at the proper location
            Remainder = (transaction_id + transaction_number) % 10000000;

            // Return the trace string
            return RPS_ID.PadLeft(8, '0') + Remainder.ToString("0000000");
        }

        /// <summary>
        /// Obtain the next trace number for a detail record
        /// </summary>
        private string Next_Batch_Trace()
        {
            Int32 Remainder = default(Int32);

            // Ensure that the trace number wraps at the proper location
            Remainder = (Batch_ID + TotalFile_BatchCount) % 10000000;

            // Return the trace string
            return RPS_ID.PadLeft(8, '0') + Remainder.ToString("0000000");
        }

        /// <summary>
        /// Generate the account name value for the detail
        /// </summary>
        private string FixedField(object varName, Int32 length)
        {
            // Retrieve the value as a suitable string.
            string answer = DebtPlus.Utils.Nulls.DStr(varName).Trim();

            // Make the field the exact number of spaces needed to generate the fixed format record
            answer = answer.PadRight(length);
            answer = answer.Substring(0, length);
            return answer;
        }

        /// <summary>
        /// Write the file header record
        /// </summary>
        private void Write_File_Header()
        {
            StringBuilder txt = new StringBuilder(100);

            txt.Append("1");
            // Record type
            txt.Append("01");
            // Priority code
            txt.Append(" 999900004");
            // Immediate destination
            txt.AppendFormat(" {0:00000000}0", Convert.ToInt32(RPS_ID));
            // Origin code

            txt.AppendFormat("{0:yyMMddHHmm}", CurrentDate);
            // File creation date/time
            txt.Append("1");
            // File ID modifier
            txt.Append("094");
            // File Record Size
            txt.Append("10");
            // Blocking factor
            txt.Append("1");
            // File format code
            txt.Append("MC REMIT PROC CENT SITE");
            // File destination
            txt.Append(FixedField(RPS_Name, 23));
            // Source name

            // Include the proper "marker" as needed
            if (OrigTest)
            {
                txt.Append("ORIGTEST");
                // Testing string
            }
            else
            {
                txt.Append("        ");
                // Normal string
            }

            // Write the record
            if (txt.Length != 94)
            {
                throw new ApplicationException("File Header record length <> 94 characters");
            }
            WriteRecord(txt.ToString());
        }

        /// <summary>
        /// Write the file trailer record
        /// </summary>
        private void Write_File_Control()
        {
            Int32 _FileRecordNumber = default(Int32);

            // Compute the number of blocks. It is based upon the record size and the blocking factor
            _FileRecordNumber = FileRecordNumber() - 1;
            BlockCount = (_FileRecordNumber + 9) / 10;
            if (BlockCount > 0)
            {
                StringBuilder txt = new StringBuilder();

                txt.Append("9");
                txt.Append(TotalFile_BatchCount.ToString("000000"));
                txt.Append(BlockCount.ToString("000000"));
                txt.Append(TotalFile_DetailCount.ToString("00000000"));
                txt.Append('0', 10);
                txt.Append(TotalFile_Debit.ToString("000000000000"));
                txt.Append(TotalFile_Credit.ToString("000000000000"));
                txt.Append(' ', 39);

                // Write the record to the disk file
                if (txt.Length != 94)
                {
                    throw new ApplicationException("File Trailer record length <> 94 characters");
                }
                WriteRecord(txt.ToString());
            }
        }

        /// <summary>
        /// Write the batch header record
        /// </summary>
        private void Write_Batch_Header()
        {
            StringBuilder txt = new StringBuilder();

            txt.Append("5");
            txt.Append(Open_Biller_Class);
            txt.Append(FixedField(Open_Biller_Name, 16));
            txt.Append(' ', 20);
            txt.Append(open_biller_id);
            txt.Append("CIE");
            txt.Append(Open_Biller_Description);
            txt.Append(' ', 6);
            txt.Append(CurrentDate.ToString("yyMMdd"));
            txt.Append(' ', 3);
            txt.Append("1");
            txt.Append(open_batch_id);

            // Write the record to the disk file
            if (txt.Length != 94)
            {
                throw new ApplicationException("Batch Header record length <> 94 characters");
            }
            WriteRecord(txt.ToString());
        }

        /// <summary>
        /// Write the batch trailer record
        /// </summary>
        private void Write_Batch_Control()
        {
            StringBuilder txt = new StringBuilder();

            // Emit the file control record
            txt.Append("8");
            txt.Append(Open_Biller_Class);
            txt.Append(Batch_DetailCount.ToString("000000"));
            txt.Append('0', 10);
            txt.Append(Batch_Debit.ToString("000000000000"));
            txt.Append(Batch_Credit.ToString("000000000000"));
            txt.Append(open_biller_id);
            txt.Append(' ', 25);
            txt.Append(open_batch_id);

            // Write the record to the disk file
            if (txt.Length != 94)
            {
                throw new ApplicationException("Batch Trailer record length <> 94 characters");
            }
            WriteRecord(txt.ToString());
        }

        /// <summary>
        /// Write the detail record
        /// </summary>
        private void Write_Detail(string Trace_Number, string Transaction_Code)
        {
            Write_Detail(Trace_Number, Transaction_Code, 0, 0, string.Empty, string.Empty);
        }

        /// <summary>
        /// Write the detail record
        /// </summary>
        private void Write_Detail(string Trace_Number, string Transaction_Code, Int32 Debit_Amount, Int32 Credit_Amount)
        {
            Write_Detail(Trace_Number, Transaction_Code, Debit_Amount, Credit_Amount, string.Empty, string.Empty);
        }

        /// <summary>
        /// Write the detail record
        /// </summary>
        private void Write_Detail(string Trace_Number, string Transaction_Code, Int32 Debit_Amount, Int32 Credit_Amount, string Account_Number, string Account_Name)
        {
            StringBuilder txt = new StringBuilder();

            if (Transaction_Code == DebtPlus.UI.Desktop.CS.Disbursement.Common.RPPSConstants.ENTRY_TRANSACTION_CODE_PRENOTE)
            {
                Prenote_Count += 1;

                // Prenotes have zeros for the values
                Debit_Amount = 0;
                Credit_Amount = 0;
            }
            else if (Transaction_Code == DebtPlus.UI.Desktop.CS.Disbursement.Common.RPPSConstants.ENTRY_TRANSACTION_CODE_REVERSAL)
            {
                Batch_Debit += Debit_Amount;
            }
            else if (Transaction_Code == DebtPlus.UI.Desktop.CS.Disbursement.Common.RPPSConstants.ENTRY_TRANSACTION_CODE_PAYMENT)
            {
                Batch_Credit += Debit_Amount;
            }
            else
            {
                DebtPlus.Data.Forms.MessageBox.Show("The transaction type " + Transaction_Code + " is not defined. This will result in the record being skipped and will cause the RPS file to be out of balance.", "RPS Processing Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Build the record
            txt.Append("6");
            txt.Append(Transaction_Code);
            txt.Append(' ', 9);
            txt.Append(' ', 17);
            txt.Append(Debit_Amount.ToString("0000000000"));
            txt.Append(Credit_Amount.ToString("0000000000"));
            txt.Append(Account_Name);
            txt.Append(Account_Number);
            txt.Append(' ', 2);
            txt.Append("0");
            txt.Append(Trace_Number);

            // Write the record
            if (txt.Length != 94)
            {
                throw new ApplicationException("Detail record length <> 94 characters");
            }
            WriteRecord(txt.ToString());

            // Update batch totals
            Batch_DetailCount += 1;
        }

        public Int32 create_eft_file(string file_type)
        {
            Int32 Result = -1;

            // Find the right most 50 characters of the filename
            string local_filename = FileName;
            if (local_filename.Length > 50)
            {
                local_filename = "..." + local_filename.Substring(local_filename.Length - 47);
            }

            // Create the file in the database
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_create_file";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank;
                    cmd.Parameters.Add("@filename", SqlDbType.VarChar, 80).Value = local_filename;
                    cmd.Parameters.Add("@file_type", SqlDbType.VarChar, 80).Value = file_type;

                    cmd.ExecuteNonQuery();
                    Result = Convert.ToInt32(cmd.Parameters[0].Value);
                }
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
                Cursor.Current = current_cursor;
            }

            return Result;
        }

        /// <summary>
        /// Update the count values for subsequent batches today
        /// </summary>
        private bool update_batch_counts()
        {
            bool answer = false;

            // Correct the transaction trace number so that it is never 9000000 or larger.
            // We do wrap at 10000000, but this causes an invalid trace number condition to occur
            // since Matercard never allows the counters to be wrap in a single file. They can
            // start at 0000001, but going from 9999999 to 0000001 is not allowed.
            long NewBatchTrace = (Batch_ID + TotalFile_BatchCount) % 10000000;
            if (NewBatchTrace >= 9000000)
            {
                NewBatchTrace -= 9000000;
            }

            // They never allow a trace number of zero. So, if it is zero then make it 1. (This is an almost impossible condition to occur, but still.)
            if (NewBatchTrace == 0)
            {
                NewBatchTrace = 1;
            }

            // Do the same thing to the transaction trace ID
            long NewTransactionTrace = (transaction_id + transaction_number) % 10000000;
            if (NewTransactionTrace >= 9000000)
            {
                NewTransactionTrace -= 9000000;
            }

            // They never allow a trace number of zero. So, if it is zero then make it 1. (This is an almost impossible condition to occur, but still.)
            if (NewTransactionTrace == 0)
            {
                NewTransactionTrace = 1;
            }

            // Update the banks table with the correct information for the next generated file
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                    cmd.CommandText = "UPDATE [banks] SET [batch_number]=@new_batch_number, [transaction_number]=@new_transaction_number WHERE [bank]=@bank AND [batch_number] = @old_batch_number AND [transaction_number] = @old_transaction_number";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@new_batch_number", SqlDbType.Int).Value = NewBatchTrace;
                    cmd.Parameters.Add("@new_transaction_number", SqlDbType.Int).Value = NewTransactionTrace;
                    cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank;
                    cmd.Parameters.Add("@old_batch_number", SqlDbType.Int).Value = Batch_ID;
                    cmd.Parameters.Add("@old_transaction_number", SqlDbType.Int).Value = transaction_id;

                    answer = (cmd.ExecuteNonQuery() == 1);
                }
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
                Cursor.Current = current_cursor;
            }

            return answer;
        }

        /// <summary>
        /// Generate a RPPS Batch ID
        /// </summary>
        private Int32 Next_Batch(string rpps_biller_id, string biller_name)
        {
            return Next_Batch(rpps_biller_id, biller_name, DebtPlus.UI.Desktop.CS.Disbursement.Common.RPPSConstants.SERVICE_CLASS_CREDIT, DebtPlus.UI.Desktop.CS.Disbursement.Common.RPPSConstants.ENTRY_DESCRIPTION_PAYMENT);
        }

        /// <summary>
        /// Generate a RPPS Batch ID
        /// </summary>
        private Int32 Next_Batch(string rpps_biller_id, string biller_name, string service_class, string description)
        {
            Int32 Result = -1;

            // Clear the batch counters
            Batch_DetailCount = 0;
            Batch_Debit = 0;
            Batch_Credit = 0;

            // Generate the batch trace number
            TotalFile_BatchCount += 1;
            string strBatchID = Next_Batch_Trace();
            if (status_form != null)
                status_form.batch = strBatchID;

            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandText = "xpr_rpps_create_batch";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout;

                    cmd.Parameters.Add("@RETURN_VALUE", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@FileNumber", SqlDbType.Int).Value = eft_file;
                    cmd.Parameters.Add("@BillerID", SqlDbType.VarChar, 80).Value = rpps_biller_id;
                    cmd.Parameters.Add("@TraceNumber", SqlDbType.VarChar, 80).Value = strBatchID;
                    cmd.Parameters.Add("@ServiceClass", SqlDbType.VarChar, 80).Value = service_class;
                    cmd.Parameters.Add("@BillerName", SqlDbType.VarChar, 80).Value = biller_name;
                    cmd.ExecuteNonQuery();

                    Result = Convert.ToInt32(cmd.Parameters[0].Value);
                }
            }
            finally
            {
                if (cn != null)
                    cn.Dispose();
                Cursor.Current = current_cursor;
            }

            // Save the information for the batch control's later processing
            open_batch_id = strBatchID;
            open_biller_id = rpps_biller_id;
            Open_Biller_Name = biller_name;
            Open_Biller_Class = service_class;
            Open_Biller_Description = description;

            // Write the batch header
            if (Result > 0)
            {
                Write_Batch_Header();
            }
            return Result;
        }

        /// <summary>
        /// Format the Decimal value as a long value in cents
        /// </summary>
        private Int32 FormattedMoney(object curValue)
        {
            Int32 answer = 0;

            if (curValue != null && !object.ReferenceEquals(curValue, DBNull.Value))
            {
                if (curValue is decimal)
                {
                    decimal ItemValue = (decimal)curValue * 100m;
                    answer = Convert.ToInt32(decimal.Floor(ItemValue));
                }
                else if (curValue is Int32)
                {
                    answer = (Int32)curValue;
                }
                else
                {
                    throw new ArgumentException("Value must be decimal or Int32");
                }
            }

            return answer;
        }

        /// <summary>
        /// Generate the account name value for the detail
        /// </summary>
        private string AccountName(object varName)
        {
            string InputName = DebtPlus.Utils.Nulls.DStr(varName);
            InputName = clean_string(InputName);

            // The criteria is more strict here. Toss all but printable characters/
            InputName = Regex.Replace(InputName, "[^0-9A-Z]", string.Empty, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            return FixedField(InputName, 5).ToUpper();
        }

        /// <summary>
        /// Generate the account number value for the detail
        /// </summary>
        private string AccountNumber(object varName)
        {
            string InputNumber = DebtPlus.Utils.Nulls.DStr(varName);
            InputNumber = clean_string(InputNumber);

            InputNumber = Regex.Replace(InputNumber, "[^- 0-9A-Z]", string.Empty, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            return FixedField(InputNumber, 22).ToUpper();
        }

        /// <summary>
        /// Write records for a "net" biller
        /// </summary>
        private void Write_Net_Biller()
        {
            string strName = null;
            string trace_number_last = null;
            string trace_number_first = null;
            Int32 rpps_batch = default(Int32);

            // Open a new group for this biller
            rpps_batch = Next_Batch(Convert.ToString(drv["rpps_biller_id"]), Convert.ToString(drv["biller_name"]));

            // Process the records for this biller
            while (RowNumber < vue.Count)
            {
                drv = vue[RowNumber];

                // Stop if the biller IDs differ
                if (Convert.ToString(drv["rpps_biller_id"]) != open_biller_id)
                {
                    break;
                }

                Int32 TransactionBilled = FormattedMoney(drv["billed"]);
                Int32 TransactionGross = FormattedMoney(drv["amount"]);
                Int32 TransactionDeduct = FormattedMoney(drv["deducted"]);
                Int32 TransactionNet = TransactionGross - TransactionDeduct;
                string TransactionCode = Convert.ToString(drv["transaction_code"]);

                // Generate a trace number for this transaction
                trace_number_last = Next_Detail_Trace();
                trace_number_first = trace_number_last;

                // Use the supplied name if one was given. Otherwise, generate one from our copy of the name
                strName = AccountName(drv["account_name"]);

                if (TransactionCode == "23")
                {
                    Write_Detail(trace_number_last, TransactionCode, 0, 0, AccountNumber(drv["account_number"]), strName);
                }
                else
                {
                    // Accumulate the total amount of money billed into the form for display purposes only
                    TotalFile_Billed += TransactionBilled;
                    TotalFile_Net += TransactionNet;
                    TotalFile_Gross += TransactionGross;

                    // DebitAmount = "net" transfer
                    // CreditAmount = "gross" transfer
                    Write_Detail(trace_number_last, TransactionCode, TransactionNet, TransactionGross, AccountNumber(drv["account_number"]), strName);
                }

                // Set the parameters for the transactions update
                drv.BeginEdit();
                drv["trace_number_first"] = trace_number_first;
                drv["trace_number_last"] = trace_number_last;
                drv["rpps_batch"] = rpps_batch;
                drv["rpps_file"] = eft_file;
                drv.EndEdit();

                // Go to the next record in the list
                RowNumber += 1;
            }

            // Update the totals for the file processing
            TotalFile_DetailCount += Batch_DetailCount;
            TotalFile_Debit += Batch_Debit;
            TotalFile_Credit += Batch_Credit;

            // Close the current batch
            Write_Batch_Control();
        }

        /// <summary>
        /// Write records for a "gross" biller
        /// </summary>
        private void Write_Gross_Biller()
        {
            string strName = null;
            string trace_number_last = null;
            string trace_number_first = null;
            Int32 rpps_batch = default(Int32);

            // Open a new group for this biller
            rpps_batch = Next_Batch(Convert.ToString(drv["rpps_biller_id"]), Convert.ToString(drv["biller_name"]));

            // Process the records for this biller
            while (RowNumber < vue.Count)
            {
                drv = vue[RowNumber];

                // Stop if the biller IDs differ
                if (Convert.ToString(drv["rpps_biller_id"]) != open_biller_id)
                {
                    break;
                }

                // Generate a trace number for this transaction
                trace_number_last = Next_Detail_Trace();

                // Use the supplied name if one was given. Otherwise, generate one from our copy of the name
                strName = AccountName(drv["account_name"]);

                // Update the database to reflect the assignment to the batch and trace number
                trace_number_first = trace_number_last;

                Int32 TransactionBilled = FormattedMoney(drv["billed"]);
                Int32 TransactionGross = FormattedMoney(drv["amount"]);
                Int32 TransactionDeduct = FormattedMoney(drv["deducted"]);
                Int32 TransactionNet = TransactionGross - TransactionDeduct;
                string TransactionCode = Convert.ToString(drv["transaction_code"]);

                if (TransactionCode == "23")
                {
                    Write_Detail(trace_number_last, TransactionCode, 0, 0, AccountNumber(drv["account_number"]), strName);
                }
                else
                {
                    // Accumulate the total amount of money billed
                    TotalFile_Billed += TransactionBilled;
                    TotalFile_Net += TransactionNet;
                    TotalFile_Gross += TransactionGross;

                    // DebitAmount = "net" transfer
                    // CreditAmount = "net" transfer
                    Write_Detail(trace_number_last, TransactionCode, TransactionNet, TransactionNet, AccountNumber(drv["account_number"]), strName);
                }

                // Set the parameters for the transactions update
                drv.BeginEdit();
                drv["trace_number_first"] = trace_number_first;
                drv["trace_number_last"] = trace_number_last;
                drv["rpps_batch"] = rpps_batch;
                drv["rpps_file"] = eft_file;
                drv.EndEdit();

                // Go to the next record in the list
                RowNumber += 1;
            }

            // Update the totals for the file processing
            TotalFile_DetailCount += Batch_DetailCount;
            TotalFile_Debit += Batch_Debit;
            TotalFile_Credit += Batch_Credit;

            // Close the current batch
            Write_Batch_Control();
        }

        /// <summary>
        /// Retrieve the account number for the modified gross biller
        /// </summary>
        private string ReversalAccountNumber(string strReversalBillerID)
        {
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            string Result = "1999999999";

            try
            {
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandTimeout = DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout;
                    cmd.CommandText = "SELECT TOP 1 m.mask FROM rpps_masks m WITH (NOLOCK) INNER JOIN rpps_biller_ids ids WITH (NOLOCK) ON ids.rpps_biller_id = m.rpps_biller_id WHERE ids.rpps_biller_id=@rpps_mask";
                    cmd.Parameters.Add("@rpps_mask", SqlDbType.VarChar, 80).Value = strReversalBillerID;

                    object objResult = cmd.ExecuteScalar();
                    if (objResult != null && !object.ReferenceEquals(objResult, DBNull.Value))
                    {
                        Result = Convert.ToString(objResult);
                    }
                }
            }
            finally
            {
                if (cn != null)
                {
                    cn.Dispose();
                }
            }

            return Result;
        }

        /// <summary>
        /// Write records for a "modified gross" biller
        /// </summary>
        private void Write_Modified_Gross_Biller()
        {
            string strName = null;
            string trace_number_last = null;
            string trace_number_first = null;
            Int32 Modified_Gross_Deduct = 0;
            string strCreditor = null;
            Int32 rpps_batch = default(Int32);
            string strReversalBillerID = null;
            string strReversalAccountNumber = null;

            // Open a new group for this biller
            rpps_batch = Next_Batch(Convert.ToString(drv["rpps_biller_id"]), Convert.ToString(drv["biller_name"]));

            // Save the creditor ID for the crediting function later
            strCreditor = string.Empty;
            if (!object.ReferenceEquals(drv["creditor"], DBNull.Value))
            {
                strCreditor = Convert.ToString(drv["creditor"]);
            }

            // Remember the ID for the reversal process later
            strReversalBillerID = Convert.ToString(drv["rpps_biller_id"]);
            if (!object.ReferenceEquals(drv["reversal_biller_id"], DBNull.Value))
            {
                strReversalBillerID = Convert.ToString(drv["reversal_biller_id"]);
            }
            strReversalAccountNumber = ReversalAccountNumber(strReversalBillerID);

            // Process the records for this biller
            while (RowNumber < vue.Count)
            {
                drv = vue[RowNumber];

                // Stop if the biller IDs differ
                if (Convert.ToString(drv["rpps_biller_id"]) != open_biller_id)
                {
                    break;
                }

                // Generate a trace number for this transaction
                trace_number_last = Next_Detail_Trace();
                trace_number_first = trace_number_last;

                // Use the supplied name if one was given. Otherwise, generate one from our copy of the name
                strName                 = AccountName(drv["account_name"]);

                Int32 TransactionBilled = FormattedMoney(drv["billed"]);
                Int32 TransactionGross  = FormattedMoney(drv["amount"]);
                Int32 TransactionDeduct = FormattedMoney(drv["deducted"]);
                Int32 TransactionNet    = TransactionGross - TransactionDeduct;
                string TransactionCode  = Convert.ToString(drv["transaction_code"]);

                if (TransactionCode == "23")
                {
                    Write_Detail(trace_number_last, TransactionCode, 0, 0, AccountNumber(drv["account_number"]), strName);
                }
                else
                {
                    // Accumulate the total amount of money billed
                    TotalFile_Billed      += TransactionBilled;
                    TotalFile_Net         += TransactionNet;
                    TotalFile_Gross       += TransactionGross;
                    Modified_Gross_Deduct += TransactionDeduct;

                    // DebitAmount  = "gross" transfer
                    // CreditAmount = "net" transfer
                    Write_Detail(trace_number_last, TransactionCode, TransactionGross, TransactionNet, AccountNumber(drv["account_number"]), strName);
                }

                // Set the parameters for the transactions update
                drv.BeginEdit();
                drv["trace_number_first"] = trace_number_first;
                drv["trace_number_last"] = trace_number_last;
                drv["rpps_batch"] = rpps_batch;
                drv["rpps_file"] = eft_file;
                drv.EndEdit();

                // Go to the next record in the list
                RowNumber += 1;
            }

            // Update the totals for the file processing
            TotalFile_DetailCount += Batch_DetailCount;
            TotalFile_Debit += Batch_Debit;
            TotalFile_Credit += Batch_Credit;

            // Close the current batch
            Write_Batch_Control();

            // If there is a deducted amount then generate a new batch for the deduction

            if (Modified_Gross_Deduct > 0m)
            {
                // Clear the batch counters
                Batch_DetailCount = 0;
                Batch_Debit = 0;
                Batch_Credit = 0;

                // Generate a transaction record to hold the reversal entry
                var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    cn.Open();

                    rpps_batch = Next_Batch(strReversalBillerID, Open_Biller_Name, "225", "REVERSAL  ");
                    trace_number_last = Next_Detail_Trace();

                    using (var cmd = new SqlCommand())
                    {
                        cmd.Connection = cn;
                        cmd.CommandTimeout = System.Math.Max(300, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                        cmd.CommandText = "xpr_rpps_cie_modified_gross";
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@creditor", SqlDbType.VarChar, 80).Value = strCreditor;
                        cmd.Parameters.Add("@trace_number", SqlDbType.VarChar, 80).Value = trace_number_last;
                        cmd.Parameters.Add("@rpps_batch", SqlDbType.Int).Value = rpps_batch;
                        cmd.Parameters.Add("@biller_id", SqlDbType.VarChar, 80).Value = open_biller_id;
                        cmd.Parameters.Add("@account_number", SqlDbType.VarChar, 80).Value = strReversalAccountNumber;
                        cmd.Parameters.Add("@rpps_file", SqlDbType.Int).Value = eft_file;

                        cmd.ExecuteNonQuery();
                    }

                    // Generate the items for the transaction in the file
                    Write_Detail(trace_number_last, "27", Modified_Gross_Deduct, 0, AccountNumber(strReversalAccountNumber), "     ");

                    // Update the totals for the file processing
                    TotalFile_DetailCount += Batch_DetailCount;
                    TotalFile_Debit += Batch_Debit;
                    TotalFile_Credit += Batch_Credit;

                    // Close the current batch
                    Write_Batch_Control();
                }
                finally
                {
                    if (cn != null)
                        cn.Dispose();

                    Cursor.Current = current_cursor;
                }
            }
        }

        /// <summary>
        /// Generate prenotes for the current file
        /// </summary>
        private bool process_prenote_file(bool useLocalFile)
        {
            bool answer = true;

            // From the transaction, determine the current bank and region information
            bank = Convert.ToInt32(drv["bank"]);
            eft_file = 0;

            // Read the file configuration data for the new file
            Fake = !ReadPaymentConfiguration();

            // Process the records in the batch based upon the type of biller
            while (RowNumber < vue.Count)
            {
                drv = vue[RowNumber];

                // If the bank changed then we need a new file.
                if (Convert.ToInt32(drv["bank"]) != bank)
                {
                    break;
                }

                if (!Fake)
                {
                    // Create an EFT text file
                    if (OutputTextFile == null)
                    {
                        OutputTextFile = status_form.Request_FileName(disbursement_register, bank, "PNOTE", useLocalFile);
                        if (OutputTextFile == null)
                        {
                            Fake = true;
                        }
                    }

                    // Write the file header to the output text file
                    if (!Fake)
                    {
                        if (RPS_Prefix != string.Empty)
                        {
                            OutputTextFile.WriteLine(RPS_Prefix);
                        }
                        Write_File_Header();
                    }
                }

                // Generate the prenote operation
                if (Fake)
                {
                    RowNumber += 1;
                }
                else
                {
                    // Do a payment operation
                    if (eft_file <= 0)
                    {
                        eft_file = create_eft_file("EFT");
                    }
                    BillerType = Convert.ToInt32(drv["biller_type"]);

                    switch (BillerType)
                    {
                        case 2:
                        case 12:
                            // GROSS BILLER
                            Write_Gross_Biller();
                            break;

                        case 3:
                        case 13:
                            // MODIFIED GROSS BILLER
                            Write_Modified_Gross_Biller();
                            break;

                        default:
                            // BILLER_TYPE_NET (and other invalid items)
                            Write_Net_Biller();
                            break;
                    }
                }
            }

            if (!Fake)
            {
                // Write the file trailer to the output
                Write_File_Control();

                // Write the suffix line to the output file
                if (RPS_Suffix != string.Empty)
                {
                    OutputTextFile.WriteLine(RPS_Suffix);
                }

                // Close the output file
                OutputTextFile.Close();
                OutputTextFile = null;

                // Update the system with the batch data
                answer = update_batch_counts();
            }

            return answer;
        }

        /// <summary>
        /// Generate payments for the current file
        /// </summary>
        private bool process_payment_file(bool useLocalFile)
        {
            bool answer = true;

            // From the transaction, determine the current bank and region information
            bank = Convert.ToInt32(drv["bank"]);
            eft_file = 0;

            // Read the file configuration data for the new file
            Fake = !ReadPaymentConfiguration();

            // Retrieve the initial region for processing
            if (!Fake)
            {
                OutputTextFile = status_form.Request_FileName(disbursement_register, bank, "PYMT", useLocalFile);
                if (OutputTextFile == null)
                {
                    Fake = true;
                }
            }

            // Write the file header to the output text file
            if (!Fake)
            {
                if (RPS_Prefix != string.Empty)
                {
                    OutputTextFile.WriteLine(RPS_Prefix);
                }
                Write_File_Header();
            }

            // Process the records in the batch based upon the type of biller
            while (RowNumber < vue.Count)
            {
                drv = vue[RowNumber];

                // If the bank changed then we need a new file.
                if (Convert.ToInt32(drv["bank"]) != bank)
                    break;

                // Create an EFT file
                if (eft_file <= 0)
                    eft_file = create_eft_file("EFT");

                // Generate the payment operation
                if (Fake)
                {
                    RowNumber += 1;
                }
                else
                {
                    // Do a payment operation
                    BillerType = Convert.ToInt32(drv["biller_type"]);

                    switch (BillerType)
                    {
                        case 2:
                        case 12:
                            // GROSS BILLER
                            Write_Gross_Biller();

                            break;

                        case 3:
                        case 13:
                            // MODIFIED GROSS BILLER
                            Write_Modified_Gross_Biller();

                            break;

                        default:
                            // BILLER_TYPE_NET (and other invalid items)
                            Write_Net_Biller();
                            break;
                    }
                }
            }

            if (!Fake)
            {
                // Write the file trailer to the output
                Write_File_Control();

                // Write the suffix line to the output file
                if (RPS_Suffix != string.Empty)
                {
                    OutputTextFile.WriteLine(RPS_Suffix);
                }

                // Close the output file and flush to the disk
                OutputTextFile.Close();
                OutputTextFile = null;

                // Update the system with the batch data
                answer = update_batch_counts();
            }

            return answer;
        }

        /// <summary>
        /// Process the prenote requests for the regions
        /// </summary>
        public bool Process_Prenotes(Iform_RPS status_form, bool useLocalFile = false)
        {
            bool answer = true;

            // Set the global pointer to the form
            this.status_form = status_form;

            // Generate the prenotes for all regions.
            Generate_Payment_Prenotes(-1);

            // Read the list of prenotes
            ds = new DataSet("ds");
            using (SqlCommand select_cmd = new SqlCommand())
            {
                select_cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                select_cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                select_cmd.CommandText = "SELECT [rpps_transaction],[bank],[rpps_file],[creditor],[transaction_code],[client],[deducted],[billed],[amount],[account_number],[account_name],[rpps_biller_id],[reversal_biller_id],[biller_name],[biller_type],[trace_number_first],[trace_number_last],[rpps_batch] FROM view_rpps_cie_info WHERE [transaction_code] = @transaction_code";
                select_cmd.Parameters.Add("@transaction_code", SqlDbType.VarChar, 80).Value = DebtPlus.UI.Desktop.CS.Disbursement.Common.RPPSConstants.ENTRY_TRANSACTION_CODE_PRENOTE.ToString();

                if (bank > 0)
                {
                    select_cmd.CommandText += " AND [bank] = @bank";
                    select_cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank;
                }

                using (SqlDataAdapter da = new SqlDataAdapter(select_cmd))
                {
                    da.Fill(ds, "prenotes");
                    tbl = ds.Tables[0];

                    // Ensure that the columns for update are present in the view
                    if (!tbl.Columns.Contains("trace_number_first"))
                    {
                        var col = tbl.Columns.Add("trace_number_first", typeof(string));
                        col.ReadOnly = false;
                    }

                    if (!tbl.Columns.Contains("trace_number_last"))
                    {
                        var col = tbl.Columns.Add("trace_number_last", typeof(string));
                        col.ReadOnly = false;
                    }

                    if (!tbl.Columns.Contains("rpps_batch"))
                    {
                        var col = tbl.Columns.Add("rpps_batch", typeof(Int32));
                        col.ReadOnly = false;
                    }

                    if (!tbl.Columns.Contains("rpps_file"))
                    {
                        var col = tbl.Columns.Add("rpps_file", typeof(Int32));
                        col.ReadOnly = false;
                    }

                    vue = new DataView(tbl, string.Empty, "bank,rpps_biller_id", DataViewRowState.CurrentRows);

                    if (vue.Count > 0)
                    {
                        // Process the prenote file with the prenotes for all banks.
                        RowNumber = 0;
                        drv = vue[0];
                        answer = process_prenote_file(useLocalFile);

                        // Update the database once we have processed the rows
                        if (answer)
                        {
                            answer = UpdateTables(tbl);
                        }
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// Process the payment requests for the regions
        /// </summary>
        public bool Process_Payments(Iform_RPS status_form, bool useLocalFile = false)
        {
            bool answer = true;

            // Set the global pointer to the form
            this.status_form = status_form;

            // Read the list of payments
            ds = new DataSet("ds");
            using (SqlCommand select_cmd = new SqlCommand())
            {
                select_cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                select_cmd.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                select_cmd.CommandText = "SELECT [rpps_transaction],[bank],[rpps_file],[creditor],[transaction_code],[client],[deducted],[billed],[amount],[account_number],[account_name],[rpps_biller_id],[reversal_biller_id],[biller_name],[biller_type],[trace_number_first],[trace_number_last],[rpps_batch] FROM view_rpps_cie_info WHERE [transaction_code] <> @transaction_code";
                select_cmd.Parameters.Add("@transaction_code", SqlDbType.VarChar, 80).Value = DebtPlus.UI.Desktop.CS.Disbursement.Common.RPPSConstants.ENTRY_TRANSACTION_CODE_PRENOTE.ToString();

                if (bank > 0)
                {
                    select_cmd.CommandText += " AND [bank]=@bank";
                    select_cmd.Parameters.Add("@bank", SqlDbType.Int).Value = bank;
                }

                using (SqlDataAdapter da = new SqlDataAdapter(select_cmd))
                {
                    da.Fill(ds, "payments");
                }
            }

            tbl = ds.Tables[0];

            // Ensure that the columns for update are present in the view
            if (!tbl.Columns.Contains("trace_number_first"))
            {
                var col = tbl.Columns.Add("trace_number_first", typeof(string));
                col.ReadOnly = false;
            }

            if (!tbl.Columns.Contains("trace_number_last"))
            {
                var col = tbl.Columns.Add("trace_number_last", typeof(string));
                col.ReadOnly = false;
            }

            if (!tbl.Columns.Contains("rpps_batch"))
            {
                var col = tbl.Columns.Add("rpps_batch", typeof(Int32));
                col.ReadOnly = false;
            }

            if (!tbl.Columns.Contains("rpps_file"))
            {
                var col = tbl.Columns.Add("rpps_file", typeof(Int32));
                col.ReadOnly = false;
            }

            vue = new DataView(tbl, string.Empty, "bank,rpps_biller_id", DataViewRowState.CurrentRows);

            if (vue.Count > 0)
            {
                RowNumber = 0;
                drv = vue[0];
                answer = process_payment_file(useLocalFile);

                // Update the rows
                if (answer)
                {
                    answer = UpdateTables(tbl);
                }
            }

            return answer;
        }

        /// <summary>
        /// Update the trace numbers on the transactions when required
        /// </summary>
        private bool UpdateTables(DataTable tbl)
        {
            bool answer = false;

            Cursor current_cursor = Cursor.Current;
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlTransaction txn = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                txn = cn.BeginTransaction();

                // Generate the update statement to the transactions
                using (SqlCommand update_cmd = new SqlCommand())
                {
                    update_cmd.Connection = cn;
                    update_cmd.Transaction = txn;
                    update_cmd.CommandText = "UPDATE rpps_transactions SET [trace_number_first]=@trace_number_first, [trace_number_last]=@trace_number_last, [rpps_batch]=@rpps_batch, [rpps_file]=@rpps_file, [death_date]=dateadd(d, 180, getdate()) WHERE [rpps_transaction]=@rpps_transaction AND [trace_number_first] IS NULL;";

                    update_cmd.Parameters.Add("@trace_number_first", SqlDbType.VarChar, Convert.ToByte(80), "trace_number_first");
                    update_cmd.Parameters.Add("@trace_number_last", SqlDbType.VarChar, Convert.ToByte(80), "trace_number_last");
                    update_cmd.Parameters.Add("@rpps_batch", SqlDbType.Int, Convert.ToByte(0), "rpps_batch");
                    update_cmd.Parameters.Add("@rpps_file", SqlDbType.Int, Convert.ToByte(0), "rpps_file");
                    update_cmd.Parameters.Add("@rpps_transaction", SqlDbType.Int, Convert.ToByte(0), "rpps_transaction");

                    // Build the update command to change the rows
                    Int32 rows_updated = default(Int32);
                    using (SqlDataAdapter update_da = new SqlDataAdapter())
                    {
                        update_da.UpdateCommand = update_cmd;
                        rows_updated = update_da.Update(tbl);
                    }
                }

                txn.Commit();
                txn.Dispose();
                txn = null;
                answer = true;
            }
            finally
            {
                if (txn != null)
                {
                    try { txn.Rollback(); }
                    catch { }
                    txn.Dispose();
                    txn = null;
                }

                if (cn != null)
                    cn.Dispose();

                Cursor.Current = current_cursor;
            }

            return answer;
        }
    }
}