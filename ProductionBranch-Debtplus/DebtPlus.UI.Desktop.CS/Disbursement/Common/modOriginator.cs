#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal static class RPPSConstants
    {
        // Length of each record in the database file
        public static readonly System.Int32 FILE_RECORD_LENGTH = 94;

        // File type codes for the various record formats
        public static readonly System.String RECORD_TYPE_FILE_HEADER = "1";
        public static readonly System.String RECORD_TYPE_BATCH_HEADER = "5";
        public static readonly System.String RECORD_TYPE_ENTRY_DETAIL = "6";
        public static readonly System.String RECORD_TYPE_ENTRY_DETAIL_ADDENDUM = "7";
        public static readonly System.String RECORD_TYPE_BATCH_CONTROL = "8";
        public static readonly System.String RECORD_TYPE_FILE_CONTROL = "9";

        /// <summary>
        /// File header record
        /// </summary>
        public static readonly System.String FILE_PRIORITY_CODE = "01";
        public static readonly System.String FILE_IMMEDIATE_DESTINATION = " 999900004";
        public static readonly System.String FILE_ID_MODIFIER_VALUE = "1";
        public static readonly System.String FILE_RECORD_SIZE = "094";
        public static readonly System.String FILE_BLOCKING_FACTOR = "10";
        public static readonly System.String FILE_FORMAT_CODE = "1";
        public static readonly System.String FILE_DESTINATION = "MC REMIT PROC CENT SITE";
        public static readonly System.String FILE_REFERENCE_CODE_TESTING = "ORIGTEST";
        public static readonly System.String FILE_REFERENCE_CODE_NORMAL = "        ";

        /// <summary>
        /// Batch header record
        /// </summary>
        public static readonly System.String BATCH_ORIGINATOR_STATUS_CODE = "1";
        public static readonly System.String SERVICE_CLASS_CREDIT = "220";
        public static readonly System.String SERVICE_CLASS_DEBIT = "225";
        public static readonly System.String SERVICE_CLASS_CREDIT_DEBIT = "200";

        // Payments
        public static readonly System.String ENTRY_CLASS_CIE = "CIE";

        // Proposals
        public static readonly System.String ENTRY_CLASS_CDP = "CDP";

        // Balance verifications
        public static readonly System.String ENTRY_CLASS_CDV = "CDV";

        // Drops
        public static readonly System.String ENTRY_CLASS_CDD = "CDD";

        // Returns (for concentrators only)
        public static readonly System.String ENTRY_CLASS_COR = "COR";

        public static readonly System.String ENTRY_CLASS_BLANK = "   ";
        public static readonly System.String ENTRY_DESCRIPTION_PAYMENT = "RPS PAYMNT";
        public static readonly System.String ENTRY_DESCRIPTION_REVERSAL = "REVERSAL  ";
        public static readonly System.String ENTRY_DESCRIPTION_RETURN = "RETURN    ";
        public static readonly System.String ENTRY_DESCRIPTION_MESSAGE = "MESSAGE   ";

        /// <summary>
        /// Entry detail record
        /// </summary>
        public static readonly System.String ENTRY_TRANSACTION_CODE_CREDIT = "21";
        public static readonly System.String ENTRY_TRANSACTION_CODE_PAYMENT = "22";
        public static readonly System.String ENTRY_TRANSACTION_CODE_PRENOTE = "23";
        public static readonly System.String ENTRY_TRANSACTION_CODE_RETURN = "26";
        public static readonly System.String ENTRY_TRANSACTION_CODE_REVERSAL = "27";
        public static readonly System.String ENTRY_ADDENDUM_UNUSED = "0";
        public static readonly System.String ENTRY_ADDENDUM_USED = "1";
        public static readonly System.String ADDENDUM_TYPE_CODE_RETURN = "99";
        public static readonly System.String ADDENDUM_TYPE_CODE_NONFINANCIAL = "98";
        public static readonly System.String ADDENDUM_TYPE_CODE_REVERSAL = "97";
    }
}