#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal partial class AdditionalTransactions : Disbursement.Common.ISupportContext
    {
        public Disbursement.Common.IDisbursementContext context { get; set; }

        Disbursement.Common.IDisbursementContext Disbursement.Common.ISupportContext.Context
        {
            get { return context; }
            set { context = value; }
        }

        private readonly Int32 m_ClientId;

        public AdditionalTransactions(Disbursement.Common.IDisbursementContext Context)
            : this()
        {
            this.context = Context;
        }

        public AdditionalTransactions(IDisbursementContext Context, Int32 ClientId)
            : this()
        {
            m_ClientId = ClientId;
            this.context = Context;
        }

        private AdditionalTransactions()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private DataSet ds = new DataSet("ds");
        /// <summary>
        /// Initialize the form with the client number
        /// </summary>

        private void RegisterHandlers()
        {
            Load += AdditionalTransactions_Load;
            ComboBoxEdit1.SelectedIndexChanged += ComboBoxEdit1_SelectedIndexChanged;
            GridView1.Layout += LayoutChanged;
        }

        private void UnRegisterHandlers()
        {
            Load -= AdditionalTransactions_Load;
            ComboBoxEdit1.SelectedIndexChanged -= ComboBoxEdit1_SelectedIndexChanged;
            GridView1.Layout -= LayoutChanged;
        }

        /// <summary>
        /// Process the FORM LOAD event
        /// </summary>
        private void AdditionalTransactions_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            try
            {
                // Reload the placement and size
                base.LoadPlacement("Disbursement.Auto.Process.AdditionalTransactions");

                RefreshList();
                GridView1.BestFitColumns();

                // Load the layout for the grid control
                string pathName = XMLBasePath();
                string fileName = System.IO.Path.Combine(pathName, "AdditionalTransactions.Grid.xml");
                if (System.IO.File.Exists(fileName))
                {
                    GridView1.RestoreLayoutFromXml(fileName);
                }
            }

#pragma warning disable 168
            catch (System.IO.DirectoryNotFoundException ex) { }
            catch (System.IO.FileNotFoundException ex) { }
#pragma warning restore 168
            finally
            {
                RegisterHandlers();
            }
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string basePath = string.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), System.IO.Path.DirectorySeparatorChar);
            return System.IO.Path.Combine(basePath, "Disbursement.Auto.Process");
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            string pathName = XMLBasePath();
            if (!System.IO.Directory.Exists(pathName))
            {
                System.IO.Directory.CreateDirectory(pathName);
            }

            string fileName = System.IO.Path.Combine(pathName, "AdditionalTransactions.Grid.xml");
            GridView1.SaveLayoutToXml(fileName);
        }

        /// <summary>
        /// Handle the change in the period control
        /// </summary>
        private void ComboBoxEdit1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshList();
        }

        /// <summary>
        /// Reload the list of transactions for the period
        /// </summary>

        private void RefreshList()
        {
            // The period is simply the index. We don't have values since it is too simple of a list.
            Int32 datePeriod = ComboBoxEdit1.SelectedIndex + 1;

            using (var cmd = new SqlCommand())
            {
                cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                cmd.CommandText = "SELECT date_created, isnull(tran_type,'') + isnull('/' + tran_subtype,'') as tran_type_subtype, isnull(credit_amt,0) as credit_amt, isnull(debit_amt,0) as debit_amt, message FROM registers_client WITH (NOLOCK) WHERE client = @client AND datediff(m, date_created, getdate()) between 0 and @period";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@client", SqlDbType.Int).Value = m_ClientId;
                cmd.Parameters.Add("@period", SqlDbType.Int).Value = datePeriod;

                Cursor current_cursor = Cursor.Current;
                var da = new System.Data.SqlClient.SqlDataAdapter(cmd);
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    ds.Clear();
                    da.Fill(ds, "transactions");

                    GridControl1.DataSource = ds.Tables[0].DefaultView;
                    GridControl1.RefreshDataSource();
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client transactions");
                }
                finally
                {
                    da.Dispose();
                    Cursor.Current = current_cursor;
                }
            }
        }
    }
}