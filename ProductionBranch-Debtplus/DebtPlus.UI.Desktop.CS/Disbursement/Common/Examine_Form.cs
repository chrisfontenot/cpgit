#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DebtPlus.UI.Client.Service;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Base;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal partial class Examine_Form : Disbursement.Common.ISupportContext
    {
        // Fonts used for the difference amount field
        private Font RegularFont;

        private Font BoldFont;

        private Thread NotesThread;

        // Pointer to the client information for the disbursement.
        private readonly ClientClass m_clientInfo;

        public Disbursement.Common.IDisbursementContext context { get; set; }

        Disbursement.Common.IDisbursementContext Disbursement.Common.ISupportContext.Context
        {
            get { return context; }
            set { context = value; }
        }

        public Examine_Form(Disbursement.Common.IDisbursementContext context, ClientClass clientInfo)
            : this()
        {
            this.context = context;
            m_clientInfo = clientInfo;
        }

        private Examine_Form()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            FormClosing                              += Examine_Form_FormClosing;
            Load                                     += Examine_Form_Load;
            ButtonDontPay.Click                      += ButtonDontPay_Click;
            ButtonPay.Click                          += ButtonPay_Click;
            ButtonProrate.Click                      += ButtonProrate_Click;
            ButtonQuit.Click                         += ButtonQuit_Click;
            ButtonPrevious.Click                     += ButtonPrevious_Click;
            ButtonShowNotes.Click                    += ButtonShowNotes_Click;
            ButtonShowClient.Click                   += ButtonShowClient_Click;
            ButtonClear.Click                        += ButtonClear_Click;
            CalcEdit_col_debit_amt.EditValueChanging += RepositoryItemCalcEdit1_EditValueChanging;
            CalcEdit_col_debit_amt.EditValueChanged  += Amount_Validated;
            GridView1.FocusedRowChanged              += GridView1_FocusedRowChanged;
            GridView1.Layout                         += LayoutChanged;
            GridView1.CellValueChanged               += GridView1_CellValueChanged;
        }

        private void UnRegisterHandlers()
        {
            FormClosing                              -= Examine_Form_FormClosing;
            Load                                     -= Examine_Form_Load;
            ButtonDontPay.Click                      -= ButtonDontPay_Click;
            ButtonPay.Click                          -= ButtonPay_Click;
            ButtonProrate.Click                      -= ButtonProrate_Click;
            ButtonQuit.Click                         -= ButtonQuit_Click;
            ButtonPrevious.Click                     -= ButtonPrevious_Click;
            ButtonShowNotes.Click                    -= ButtonShowNotes_Click;
            ButtonShowClient.Click                   -= ButtonShowClient_Click;
            ButtonClear.Click                        -= ButtonClear_Click;
            CalcEdit_col_debit_amt.EditValueChanging -= RepositoryItemCalcEdit1_EditValueChanging;
            CalcEdit_col_debit_amt.EditValueChanged  -= Amount_Validated;
            GridView1.FocusedRowChanged              -= GridView1_FocusedRowChanged;
            GridView1.Layout                         -= LayoutChanged;
            GridView1.CellValueChanged               -= GridView1_CellValueChanged;
        }

        /// <summary>
        /// Cancel the current form
        /// </summary>
        private void Examine_Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Cancel the previous thread execution for the notes.

            if (NotesThread != null)
            {
                // Send an abort request to the thread
                if (NotesThread.IsAlive)
                {
                    NotesThread.Abort();
                }

                // Join the thread to pick up the completion event.
                NotesThread.Join(0);
                NotesThread = null;
            }
        }

        /// <summary>
        /// Process the form load event
        /// </summary>
        private void Examine_Form_Load(object sender, EventArgs e)
        {
            UnRegisterHandlers();
            m_clientInfo.BeginUpdate();

            try
            {
                // Find the fonts to use for the difference figures
                RegularFont = new Font(Difference.Font, FontStyle.Regular);
                BoldFont = new Font(RegularFont, FontStyle.Bold);

                // Restore the form placement
                base.LoadPlacement("Disbursement.Auto.Process.ExamineForm");

                // Load the client information
                const string TableName = "xpr_disbursement_client_info";

                // Enable the buttons as needed
                ButtonPrevious.Enabled = m_clientInfo.CanExaminePrevious;
                ButtonShowNotes.Enabled = m_clientInfo.CanShowNotes;

                DataSet ds = m_clientInfo.ClientDS;
                DataTable tbl = ds.Tables[TableName];
                if (tbl == null)
                {
                    Cursor current_cursor = Cursor.Current;
                    try
                    {
                        Cursor.Current = Cursors.WaitCursor;
                        using (var cmd = new SqlCommand())
                        {
                            cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            cmd.CommandText = "xpr_disbursement_client_info";
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@disbursement_register", SqlDbType.Int).Value = m_clientInfo.disbursement_register;
                            cmd.Parameters.Add("@client", SqlDbType.Int).Value = m_clientInfo.ClientId;

                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                da.FillLoadOption = LoadOption.OverwriteChanges;
                                da.Fill(ds, TableName);
                                tbl = ds.Tables[TableName];
                            }
                        }
                    }
                    catch (SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading client information");
                    }
                    finally
                    {
                        Cursor.Current = current_cursor;
                    }
                }

                lbl_client.Text = string.Format("{0:0000000}", m_clientInfo.ClientId);

                if (tbl != null && tbl.Rows.Count > 0)
                {
                    DataRow row = tbl.Rows[0];

                    // Retrieve the address information
                    StringBuilder sb = new StringBuilder();
                    if (!row.IsNull("address1"))
                    {
                        sb.Append(Environment.NewLine);
                        sb.Append(Convert.ToString(row["address1"]));
                    }

                    if (!row.IsNull("address2"))
                    {
                        sb.Append(Environment.NewLine);
                        sb.Append(Convert.ToString(row["address2"]));
                    }

                    if (!row.IsNull("address3"))
                    {
                        sb.Append(Environment.NewLine);
                        sb.Append(Convert.ToString(row["address3"]));
                    }
                    if (sb.Length > 0)
                    {
                        sb.Remove(0, 2);
                    }
                    lbl_address.Text = sb.ToString();

                    // Retrieve the client name
                    if (!row.IsNull("name"))
                    {
                        lbl_name.Text = Convert.ToString(row["name"]);
                    }

                    // Retrieve the home phone
                    if (!row.IsNull("home_phone"))
                    {
                        lbl_home_ph.Text = Convert.ToString(row["home_phone"]);
                    }

                    // Retrieve the work phone
                    string phone = string.Empty;
                    if (!row.IsNull("work_phone"))
                    {
                        phone = Convert.ToString(row["work_phone"]);
                    }
                    lbl_work_ph.Text = phone;

                    // Retrieve the language
                    if (!row.IsNull("language"))
                    {
                        lbl_language.Text = Convert.ToString(row["language"]);
                    }
                }

                GridControl1.BeginUpdate();
                GridControl1.DataSource = m_clientInfo.DebtList;
                GridControl1.RefreshDataSource();
                GridControl1.EndUpdate();

                // Update the deposit information
                DepositsControl1.DisplayInformation(m_clientInfo.ClientId);

                // Update the display information for the client status
                TrustFundsOnHold.Text = string.Format("{0:c}", m_clientInfo.trust_funds_on_hold);
                TrustFundsAvailable.Text = string.Format("{0:c}", m_clientInfo.held_in_trust);

                UpdateDisplayedAmounts();

                // Load the layout for the grid control
                string PathName = XMLBasePath();
                string FileName = Path.Combine(PathName, "ExamineForm.Grid.xml");
                try
                {
                    if (File.Exists(FileName))
                    {
                        GridView1.RestoreLayoutFromXml(FileName);
                    }
                }
#pragma warning disable 168
                catch (DirectoryNotFoundException ex) { }
                catch (FileNotFoundException ex) { }
#pragma warning restore 168

                // Enable the edit operation on the first row in the list
                col_debit_amt.OptionsColumn.AllowEdit = ShouldAllowEdit();

                // Enable/Disable the prorate button
                ButtonProrate.Enabled = m_clientInfo.CanProrate;
            }
            finally
            {
                m_clientInfo.EndUpdate();
                RegisterHandlers();
            }

            // Show any pending notes if there are any recorded
            if (m_clientInfo.CanShowNotes)
            {
                ShowClientNotes(false);
            }
        }

        /// <summary>
        /// Return the directory to the saved layout information
        /// </summary>
        protected virtual string XMLBasePath()
        {
            string BasePath = string.Format("{0}{1}DebtPlus", Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), Path.DirectorySeparatorChar);
            return Path.Combine(BasePath, "Disbursement.Auto.Process");
        }

        /// <summary>
        /// If the layout is changed then update the file
        /// </summary>
        private void LayoutChanged(object Sender, EventArgs e)
        {
            string PathName = XMLBasePath();
            if (!Directory.Exists(PathName))
            {
                Directory.CreateDirectory(PathName);
            }

            string FileName = Path.Combine(PathName, "ExamineForm.Grid.xml");
            GridView1.SaveLayoutToXml(FileName);
        }

        /// <summary>
        /// Update the amounts on the form when the values change
        /// </summary>
        private void UpdateDisplayedAmounts()
        {
            // Find the total of the disbursement amounts
            decimal DisbursedTotal = DebtPlus.Utils.Nulls.DDec(col_debit_amt.SummaryItem.SummaryValue);
            SumOfDisbursements.Text = string.Format("{0:c}", DisbursedTotal);

            // Compute the difference between the two items
            decimal DifferenceAmount = m_clientInfo.held_in_trust - DisbursedTotal;
            Difference.Text = string.Format("{0:c}", DifferenceAmount);

            // Zero is a normal display
            if (DifferenceAmount == 0m)
            {
                Difference.BackColor = Color.Transparent;
                Difference.ForeColor = Color.Black;
                Difference.Font = RegularFont;

                // Negative is a bold red display
            }
            else if (DifferenceAmount < 0m)
            {
                Difference.BackColor = Color.Red;
                Difference.ForeColor = Color.White;
                Difference.Font = BoldFont;
            }
            else
            {
                // Positive is a normal green display
                Difference.BackColor = Color.Transparent;
                Difference.ForeColor = Color.Green;
                Difference.Font = RegularFont;
            }

            // Enable the payment option if there is a pay amount for a debt and the difference is not negative
            ButtonPay.Enabled = DisbursedTotal >= 0m && DifferenceAmount >= 0m;
        }

#if false
		/// <summary>
		/// Total dollar figure disbursed
		/// </summary>
		private decimal TotalDisbursed()
		{
			decimal answer = 0m;
			foreach (DebtPlus.Debt.IProratable Debt in ClientInfo.DebtList) {
				answer += Debt.debit_amt;
			}
			return answer;
		}
#endif

        /// <summary>
        /// Process a change in the cell value
        /// </summary>
        private void GridView1_CellValueChanged(object sender, CellValueChangedEventArgs e)
        {
            GridView1.UpdateTotalSummary();
            UpdateDisplayedAmounts();

            // Enable the PRORATE button as appropriate
            ButtonProrate.Enabled = m_clientInfo.CanProrate;
        }

        /// <summary>
        /// Do not pay the debts
        /// </summary>
        private void ButtonDontPay_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            m_clientInfo.Do_NoPay();
        }

        /// <summary>
        /// Pay the debts based upon the entered figures
        /// </summary>
        private void ButtonPay_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            m_clientInfo.Do_Pay();
        }

        /// <summary>
        /// Prorate the amount available to the debts
        /// </summary>
        private void ButtonProrate_Click(object sender, EventArgs e)
        {
            m_clientInfo.Do_Prorate();
            GridView1.RefreshData();
            UpdateDisplayedAmounts();
        }

        /// <summary>
        /// Quit the disbursement process
        /// </summary>
        private void ButtonQuit_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            m_clientInfo.Do_Quit();
        }

        /// <summary>
        /// Go to the previous client
        /// </summary>
        private void ButtonPrevious_Click(object sender, EventArgs e)
        {
            m_clientInfo.Do_ExaminePrevious();
        }

        /// <summary>
        /// Display the disbursement notes for the client
        /// </summary>
        private void ButtonShowNotes_Click(object sender, EventArgs e)
        {
            ShowClientNotes(true);
        }

        /// <summary>
        /// Display the alert and disbursement notes
        /// </summary>
        private void ShowClientNotes(bool ShowNoNotesWarningDialog)
        {
            // Cancel the previous thread execution for the notes.

            if (NotesThread != null)
            {
                // If the thread is still alive then we don't do anything.
                if (NotesThread.IsAlive)
                {
                    return;
                }

                // Try to clean up the thread when it is completed
                NotesThread.Join(0);
            }

            // Create a new thread to process the note display
            NotesThread = new Thread(new ParameterizedThreadStart(ShowNotes))
            {
                IsBackground = true,
                Name = "Disbursement Notes"
            };

            NotesThread.SetApartmentState(ApartmentState.STA);
            NotesThread.Start(ShowNoNotesWarningDialog);
        }

        /// <summary>
        /// Thread to display the notes
        /// </summary>
        private void ShowNotes(object ShowWarningMessage)
        {
            using (var disbAlert = new DebtPlus.Notes.AlertNotes.Disbursement())
            {
                // Display the notes
                Int32 NotesShown = disbAlert.ShowAlerts(m_clientInfo.ClientId, m_clientInfo.disbursement_register);

                // Tell the user that there are no notes to give them some sense of a dialog.
                if (NotesShown <= 0 && Convert.ToBoolean(ShowWarningMessage))
                {
                    DebtPlus.Data.Forms.MessageBox.Show("There are no notes to be displayed for this client", "No Notes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        /// <summary>
        /// Display the client information
        /// </summary>
        private void ButtonShowClient_Click(object sender, EventArgs e)
        {
            using (ClientUpdateClass showClient = new ClientUpdateClass())
            {
                showClient.ShowEditDialog(m_clientInfo.ClientId, false);
            }
        }

        /// <summary>
        /// Clear the payment amounts
        /// </summary>
        private void ButtonClear_Click(object sender, EventArgs e)
        {
            // Clear the payments
            foreach (DisbursementDebtRecord row in m_clientInfo.DebtList)
            {
                row.debit_amt = 0m;
            }

            // Redraw the grid and calculate the new amounts
            GridView1.RefreshData();
            UpdateDisplayedAmounts();

            // Enable the PRORATE button as appropriate
            ButtonProrate.Enabled = m_clientInfo.CanProrate;
        }

        /// <summary>
        /// Do not allow negative numbers in the payment field
        /// </summary>
        protected void RepositoryItemCalcEdit1_EditValueChanging(object sender, ChangingEventArgs e)
        {
            // If the debt can not be changed then reject the request
            if (!ShouldAllowEdit())
            {
                e.Cancel = true;
                return;
            }

            // Ensure that the amount is valid
            decimal NewValue = Convert.ToDecimal(e.NewValue);
            if (NewValue < 0m)
            {
                e.Cancel = true;
                return;
            }

            // Ensure that the row is being updated. We need it done immediately rather than buffered for later.
            DisbursementDebtRecord Debt = GridView1.GetFocusedRow() as DisbursementDebtRecord;
            if (Debt != null)
            {
                Debt.debit_amt = Convert.ToDecimal(e.NewValue);
            }
        }

        private bool RecursiveFlag;
        /// <summary>
        /// Handle the completion of an edit cell operation
        /// </summary>

        private void Amount_Validated(object sender, EventArgs e)
        {
            // Do not do this again if we are updating the row for the monthly fee
            if (!RecursiveFlag && !m_clientInfo.InUpdate)
            {
                RecursiveFlag = true;
                try
                {
                    // Determine if this is the fee debt
                    DisbursementDebtRecord CurrentDebt = GridView1.GetFocusedRow() as DisbursementDebtRecord;
                    if (CurrentDebt != null && CurrentDebt.DebtType != DebtPlus.Interfaces.Debt.DebtTypeEnum.MonthlyFee)
                    {
                        RecalculateFee();
                    }
                }
                finally
                {
                    RecursiveFlag = false;
                    GridView1.UpdateTotalSummary();
                    UpdateDisplayedAmounts();
                }
            }
        }

        /// <summary>
        /// Recalculate the monthly fee as debts are changed
        /// </summary>

        protected virtual void RecalculateFee()
        {
            // Find the monthly debt figure and recalculate the disbursement factor for it.
            DisbursementDebtRecord FeeDebt = m_clientInfo.DebtList.MonthlyFeeDebt as DisbursementDebtRecord;
            if (FeeDebt != null)
            {
                try
                {
                    m_clientInfo.FeeInfo.QueryValue += m_clientInfo.FeeInfo_QueryValue;
                    decimal FeeAmount = m_clientInfo.FeeInfo.FeeAmount();
                    FeeDebt.debit_amt = FeeAmount;
                }
                finally
                {
                    m_clientInfo.FeeInfo.QueryValue -= m_clientInfo.FeeInfo_QueryValue;
                }

                // Refresh the grid for this row
                Int32 Ordinal = m_clientInfo.DebtList.IndexOf(FeeDebt);
                if (Ordinal >= 0)
                {
                    Int32 RowHandle = GridView1.GetRowHandle(Ordinal);
                    if (RowHandle >= 0)
                    {
                        GridView1.RefreshRow(RowHandle);
                        // Refresh the row
                        GridView1.InvalidateRow(RowHandle);
                        // Redraw the row on the display
                    }
                }
            }
        }

        /// <summary>
        /// Do not permit HELD debts to be paid
        /// </summary>
        protected void GridView1_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            col_debit_amt.OptionsColumn.AllowEdit = ShouldAllowEdit();
        }

        /// <summary>
        /// Should the debt on the current focus be allowed to be paid?
        /// </summary>
        protected bool ShouldAllowEdit()
        {
            DisbursementDebtRecord Debt = GridView1.GetFocusedRow() as DisbursementDebtRecord;
            return (Debt != null) && (!Debt.hold_disbursements);
        }
    }
}