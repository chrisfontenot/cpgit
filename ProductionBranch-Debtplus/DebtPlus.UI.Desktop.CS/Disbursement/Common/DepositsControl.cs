#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}
#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
    internal partial class DepositsControl
    {
        private Int32 ClientId = -1;

        private DataSet ds = new DataSet("ds");

        protected Disbursement.Common.IDisbursementContext context
        {
            get { return ((Disbursement.Common.ISupportContext)ParentForm).Context; }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DepositsControl()
            : base()
        {
            InitializeComponent();
            RegisterHandlers();
        }

        private void RegisterHandlers()
        {
            SimpleButton1.Click += SimpleButton1_Click;
        }

        private void UnRegisterHandlers()
        {
            SimpleButton1.Click -= SimpleButton1_Click;
        }

        internal void DisplayInformation(Int32 ClientId)
        {
            // Save the client should the user press the deposits button
            this.ClientId = ClientId;
            ds.Clear();

            // Load the list control with the amount and date of the deposits
            Cursor current_cursor = Cursor.Current;
            var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
            SqlDataReader rd = null;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                cn.Open();
                using (var cmd = new SqlCommand())
                {
                    var _with1 = cmd;
                    _with1.Connection = cn;
                    _with1.CommandText = "SELECT deposit_amount, deposit_date FROM client_deposits WITH (NOLOCK) WHERE client = @client";
                    _with1.CommandType = CommandType.Text;
                    _with1.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "client_deposits");
                    }
                }

                var _with2 = GridControl1;
                _with2.DataSource = ds.Tables["client_deposits"].DefaultView;
                _with2.RefreshDataSource();

                // Read the last deposit date and amount from the client
                decimal LastDepositAmount = 0m;
                System.DateTime LastDepositDate = System.DateTime.MinValue;
                using (var cmd = new SqlCommand())
                {
                    var _with3 = cmd;
                    _with3.Connection = cn;
                    _with3.CommandType = CommandType.Text;
                    _with3.CommandText = "SELECT last_deposit_date, last_deposit_amount FROM clients WITH (NOLOCK) WHERE client = @client";
                    _with3.Parameters.Add("@client", SqlDbType.Int).Value = ClientId;
                    rd = _with3.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleRow);
                }

                if (rd.Read())
                {
                    if (!rd.IsDBNull(0))
                        LastDepositDate = rd.GetDateTime(0);
                    if (!rd.IsDBNull(1))
                        LastDepositAmount = Convert.ToDecimal(rd.GetValue(1));
                }

                // Format the date and the amount fields
                if (LastDepositDate != System.DateTime.MinValue)
                    LastDeposit.Text = string.Format("{0:c2} {1:d}", LastDepositAmount, LastDepositDate);
            }
            catch (SqlException ex)
            {
                using (Repository.GetDataResult gdr = new Repository.GetDataResult())
                {
                    gdr.HandleException(ex);
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(gdr, "Error reading client deposits");
                }
            }
            finally
            {
                if (rd != null)
                    rd.Dispose();
                if (cn != null)
                    cn.Dispose();

                Cursor.Current = current_cursor;
            }
        }

        private void SimpleButton1_Click(object sender, EventArgs e)
        {
            using (AdditionalTransactions frm = new AdditionalTransactions(context, ClientId))
            {
                frm.ShowDialog();
            }
        }
    }
}