using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Disbursement.Common
{
	partial class Examine_Form : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
					UnRegisterHandlers();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough()]
		private void InitializeComponent()
		{
			DevExpress.XtraGrid.StyleFormatCondition StyleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
			this.col_held = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_held.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.ButtonQuit = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonClear = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonProrate = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonPay = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonDontPay = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonShowClient = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonShowNotes = new DevExpress.XtraEditors.SimpleButton();
			this.ButtonPrevious = new DevExpress.XtraEditors.SimpleButton();
			this.GroupControl1 = new DevExpress.XtraEditors.GroupControl();
			this.LabelControl6 = new DevExpress.XtraEditors.LabelControl();
			this.lbl_language = new DevExpress.XtraEditors.LabelControl();
			this.lbl_work_ph = new DevExpress.XtraEditors.LabelControl();
			this.lbl_home_ph = new DevExpress.XtraEditors.LabelControl();
			this.lbl_name = new DevExpress.XtraEditors.LabelControl();
			this.lbl_client = new DevExpress.XtraEditors.LabelControl();
			this.lbl_address = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl5 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl4 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl3 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl2 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl1 = new DevExpress.XtraEditors.LabelControl();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.col_creditor = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_name = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_debit_amt = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_debit_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.CalcEdit_col_debit_amt = new DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit();
			this.col_disbursement_factor = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_priority = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_balance = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_acct_number = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_acct_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_interest = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_IncludeInFees = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_IncludeInFees.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_IncludeInProrate = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_IncludeInProrate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_Prorate = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_Prorate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_ZeroBalance = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_ZeroBalance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_orig_balance = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_orig_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_total_interest = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_total_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_debt_id = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_debt_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_sched_payment = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_sched_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_payments_month_0 = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_payments_month_0.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_line_number = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_line_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_creditor_type = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_orig_dmp_payment = new DevExpress.XtraGrid.Columns.GridColumn();
			this.col_orig_dmp_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.DepositsControl1 = new DebtPlus.UI.Desktop.CS.Disbursement.Common.DepositsControl();
			this.GroupControl2 = new DevExpress.XtraEditors.GroupControl();
			this.Difference = new DevExpress.XtraEditors.LabelControl();
			this.SumOfDisbursements = new DevExpress.XtraEditors.LabelControl();
			this.TrustFundsAvailable = new DevExpress.XtraEditors.LabelControl();
			this.TrustFundsOnHold = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl10 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl13 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl14 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl15 = new DevExpress.XtraEditors.LabelControl();
			this.LabelControl16 = new DevExpress.XtraEditors.LabelControl();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).BeginInit();
			this.GroupControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_col_debit_amt).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).BeginInit();
			this.GroupControl2.SuspendLayout();
			this.SuspendLayout();
			//
			//col_held
			//
			this.col_held.Caption = "Hold";
			this.col_held.CustomizationCaption = "Debt Held";
			this.col_held.FieldName = "hold_disbursements";
			this.col_held.Name = "col_held";
			this.col_held.OptionsColumn.AllowEdit = false;
			this.col_held.OptionsColumn.ShowInCustomizationForm = false;
			this.col_held.ToolTip = "Debt on HOLD status?";
			this.col_held.UnboundType = DevExpress.Data.UnboundColumnType.Boolean;
			this.col_held.Width = 33;
			//
			//ButtonQuit
			//
			this.ButtonQuit.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonQuit.Location = new System.Drawing.Point(578, 8);
			this.ButtonQuit.Name = "ButtonQuit";
			this.ButtonQuit.Size = new System.Drawing.Size(75, 23);
			this.ButtonQuit.TabIndex = 0;
			this.ButtonQuit.Text = "Quit";
			//
			//ButtonClear
			//
			this.ButtonClear.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonClear.Location = new System.Drawing.Point(578, 40);
			this.ButtonClear.Name = "ButtonClear";
			this.ButtonClear.Size = new System.Drawing.Size(75, 23);
			this.ButtonClear.TabIndex = 1;
			this.ButtonClear.Text = "Clear";
			//
			//ButtonProrate
			//
			this.ButtonProrate.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonProrate.Location = new System.Drawing.Point(578, 72);
			this.ButtonProrate.Name = "ButtonProrate";
			this.ButtonProrate.Size = new System.Drawing.Size(75, 23);
			this.ButtonProrate.TabIndex = 2;
			this.ButtonProrate.Text = "Prorate";
			//
			//ButtonPay
			//
			this.ButtonPay.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonPay.Location = new System.Drawing.Point(578, 104);
			this.ButtonPay.Name = "ButtonPay";
			this.ButtonPay.Size = new System.Drawing.Size(75, 23);
			this.ButtonPay.TabIndex = 3;
			this.ButtonPay.Text = "Pay";
			//
			//ButtonDontPay
			//
			this.ButtonDontPay.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonDontPay.Location = new System.Drawing.Point(578, 136);
			this.ButtonDontPay.Name = "ButtonDontPay";
			this.ButtonDontPay.Size = new System.Drawing.Size(75, 23);
			this.ButtonDontPay.TabIndex = 4;
			this.ButtonDontPay.Text = "Don't Pay";
			//
			//ButtonShowClient
			//
			this.ButtonShowClient.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonShowClient.Location = new System.Drawing.Point(578, 168);
			this.ButtonShowClient.Name = "ButtonShowClient";
			this.ButtonShowClient.Size = new System.Drawing.Size(75, 23);
			this.ButtonShowClient.TabIndex = 5;
			this.ButtonShowClient.Text = "Show Client";
			//
			//ButtonShowNotes
			//
			this.ButtonShowNotes.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonShowNotes.Location = new System.Drawing.Point(578, 200);
			this.ButtonShowNotes.Name = "ButtonShowNotes";
			this.ButtonShowNotes.Size = new System.Drawing.Size(75, 23);
			this.ButtonShowNotes.TabIndex = 6;
			this.ButtonShowNotes.Text = "Show Notes";
			//
			//ButtonPrevious
			//
			this.ButtonPrevious.Anchor = (System.Windows.Forms.AnchorStyles)(System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.ButtonPrevious.Location = new System.Drawing.Point(578, 232);
			this.ButtonPrevious.Name = "ButtonPrevious";
			this.ButtonPrevious.Size = new System.Drawing.Size(75, 23);
			this.ButtonPrevious.TabIndex = 7;
			this.ButtonPrevious.Text = "Previous";
			//
			//GroupControl1
			//
			this.GroupControl1.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GroupControl1.Controls.Add(this.LabelControl6);
			this.GroupControl1.Controls.Add(this.lbl_language);
			this.GroupControl1.Controls.Add(this.lbl_work_ph);
			this.GroupControl1.Controls.Add(this.lbl_home_ph);
			this.GroupControl1.Controls.Add(this.lbl_name);
			this.GroupControl1.Controls.Add(this.lbl_client);
			this.GroupControl1.Controls.Add(this.lbl_address);
			this.GroupControl1.Controls.Add(this.LabelControl5);
			this.GroupControl1.Controls.Add(this.LabelControl4);
			this.GroupControl1.Controls.Add(this.LabelControl3);
			this.GroupControl1.Controls.Add(this.LabelControl2);
			this.GroupControl1.Controls.Add(this.LabelControl1);
			this.GroupControl1.Location = new System.Drawing.Point(8, 8);
			this.GroupControl1.Name = "GroupControl1";
			this.GroupControl1.Size = new System.Drawing.Size(562, 144);
			this.GroupControl1.TabIndex = 8;
			this.GroupControl1.Text = "Client Information";
			//
			//LabelControl6
			//
			this.LabelControl6.Appearance.Options.UseTextOptions = true;
			this.LabelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl6.Location = new System.Drawing.Point(217, 24);
			this.LabelControl6.Name = "LabelControl6";
			this.LabelControl6.Size = new System.Drawing.Size(47, 13);
			this.LabelControl6.TabIndex = 11;
			this.LabelControl6.Text = "Language";
			//
			//lbl_language
			//
			this.lbl_language.Location = new System.Drawing.Point(270, 24);
			this.lbl_language.Name = "lbl_language";
			this.lbl_language.Size = new System.Drawing.Size(28, 13);
			this.lbl_language.TabIndex = 10;
			this.lbl_language.Text = "NONE";
			//
			//lbl_work_ph
			//
			this.lbl_work_ph.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_work_ph.Location = new System.Drawing.Point(128, 72);
			this.lbl_work_ph.Name = "lbl_work_ph";
			this.lbl_work_ph.Size = new System.Drawing.Size(28, 13);
			this.lbl_work_ph.TabIndex = 9;
			this.lbl_work_ph.Text = "NONE";
			//
			//lbl_home_ph
			//
			this.lbl_home_ph.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_home_ph.Location = new System.Drawing.Point(128, 56);
			this.lbl_home_ph.Name = "lbl_home_ph";
			this.lbl_home_ph.Size = new System.Drawing.Size(28, 13);
			this.lbl_home_ph.TabIndex = 8;
			this.lbl_home_ph.Text = "NONE";
			//
			//lbl_name
			//
			this.lbl_name.Anchor = (System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_name.Location = new System.Drawing.Point(128, 40);
			this.lbl_name.Name = "lbl_name";
			this.lbl_name.Size = new System.Drawing.Size(28, 13);
			this.lbl_name.TabIndex = 7;
			this.lbl_name.Text = "NONE";
			//
			//lbl_client
			//
			this.lbl_client.Location = new System.Drawing.Point(128, 24);
			this.lbl_client.Name = "lbl_client";
			this.lbl_client.Size = new System.Drawing.Size(28, 13);
			this.lbl_client.TabIndex = 6;
			this.lbl_client.Text = "NONE";
			//
			//lbl_address
			//
			this.lbl_address.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.lbl_address.Location = new System.Drawing.Point(128, 88);
			this.lbl_address.Name = "lbl_address";
			this.lbl_address.Size = new System.Drawing.Size(28, 13);
			this.lbl_address.TabIndex = 5;
			this.lbl_address.Text = "NONE";
			//
			//LabelControl5
			//
			this.LabelControl5.Appearance.Options.UseTextOptions = true;
			this.LabelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl5.Location = new System.Drawing.Point(16, 88);
			this.LabelControl5.Name = "LabelControl5";
			this.LabelControl5.Size = new System.Drawing.Size(39, 13);
			this.LabelControl5.TabIndex = 4;
			this.LabelControl5.Text = "Address";
			//
			//LabelControl4
			//
			this.LabelControl4.Appearance.Options.UseTextOptions = true;
			this.LabelControl4.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl4.Location = new System.Drawing.Point(16, 72);
			this.LabelControl4.Name = "LabelControl4";
			this.LabelControl4.Size = new System.Drawing.Size(78, 13);
			this.LabelControl4.TabIndex = 3;
			this.LabelControl4.Text = "Work Telephone";
			//
			//LabelControl3
			//
			this.LabelControl3.Appearance.Options.UseTextOptions = true;
			this.LabelControl3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl3.Location = new System.Drawing.Point(16, 56);
			this.LabelControl3.Name = "LabelControl3";
			this.LabelControl3.Size = new System.Drawing.Size(80, 13);
			this.LabelControl3.TabIndex = 2;
			this.LabelControl3.Text = "Home Telephone";
			//
			//LabelControl2
			//
			this.LabelControl2.Appearance.Options.UseTextOptions = true;
			this.LabelControl2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl2.Location = new System.Drawing.Point(16, 40);
			this.LabelControl2.Name = "LabelControl2";
			this.LabelControl2.Size = new System.Drawing.Size(27, 13);
			this.LabelControl2.TabIndex = 1;
			this.LabelControl2.Text = "Name";
			//
			//LabelControl1
			//
			this.LabelControl1.Appearance.Options.UseTextOptions = true;
			this.LabelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl1.Location = new System.Drawing.Point(16, 24);
			this.LabelControl1.Name = "LabelControl1";
			this.LabelControl1.Size = new System.Drawing.Size(41, 13);
			this.LabelControl1.TabIndex = 0;
			this.LabelControl1.Text = "Client ID";
			//
			//GridControl1
			//
			this.GridControl1.Anchor = (System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) | System.Windows.Forms.AnchorStyles.Left) | System.Windows.Forms.AnchorStyles.Right);
			this.GridControl1.Location = new System.Drawing.Point(8, 265);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { this.CalcEdit_col_debit_amt });
			this.GridControl1.Size = new System.Drawing.Size(645, 172);
			this.GridControl1.TabIndex = 12;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButton.Options.UseForeColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
			this.GridView1.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
			this.GridView1.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.Empty.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.EvenRow.Options.UseBackColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.EvenRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.FilterCloseButton.Options.UseForeColor = true;
			this.GridView1.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FilterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FilterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(133), (Int32)Convert.ToByte(195));
			this.GridView1.Appearance.FixedLine.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FocusedCell.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedCell.Options.UseForeColor = true;
			this.GridView1.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(38), (Int32)Convert.ToByte(109), (Int32)Convert.ToByte(189));
			this.GridView1.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(59), (Int32)Convert.ToByte(139), (Int32)Convert.ToByte(206));
			this.GridView1.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.FocusedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.FocusedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.FooterPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.FooterPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.GroupButton.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupButton.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupFooter.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupFooter.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(170), (Int32)Convert.ToByte(216), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.GroupRow.Options.UseBackColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.GroupRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(139), (Int32)Convert.ToByte(201), (Int32)Convert.ToByte(254));
			this.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = true;
			this.GridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
			this.GridView1.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(105), (Int32)Convert.ToByte(170), (Int32)Convert.ToByte(225));
			this.GridView1.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
			this.GridView1.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.HorzLine.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.OddRow.Options.UseBackColor = true;
			this.GridView1.Appearance.OddRow.Options.UseBorderColor = true;
			this.GridView1.Appearance.OddRow.Options.UseForeColor = true;
			this.GridView1.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5f);
			this.GridView1.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.Preview.Options.UseFont = true;
			this.GridView1.Appearance.Preview.Options.UseForeColor = true;
			this.GridView1.Appearance.Row.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(247), (Int32)Convert.ToByte(251), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.Row.ForeColor = System.Drawing.Color.Black;
			this.GridView1.Appearance.Row.Options.UseBackColor = true;
			this.GridView1.Appearance.Row.Options.UseForeColor = true;
			this.GridView1.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(236), (Int32)Convert.ToByte(246), (Int32)Convert.ToByte(255));
			this.GridView1.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
			this.GridView1.Appearance.RowSeparator.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(83), (Int32)Convert.ToByte(155), (Int32)Convert.ToByte(215));
			this.GridView1.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
			this.GridView1.Appearance.SelectedRow.Options.UseBackColor = true;
			this.GridView1.Appearance.SelectedRow.Options.UseForeColor = true;
			this.GridView1.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
			this.GridView1.Appearance.TopNewRow.Options.UseBackColor = true;
			this.GridView1.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb((Int32)Convert.ToByte(104), (Int32)Convert.ToByte(184), (Int32)Convert.ToByte(251));
			this.GridView1.Appearance.VertLine.Options.UseBackColor = true;
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.col_creditor,
				this.col_name,
				this.col_debit_amt,
				this.col_disbursement_factor,
				this.col_priority,
				this.col_balance,
				this.col_acct_number,
				this.col_interest,
				this.col_held,
				this.col_IncludeInFees,
				this.col_IncludeInProrate,
				this.col_Prorate,
				this.col_ZeroBalance,
				this.col_orig_balance,
				this.col_total_interest,
				this.col_debt_id,
				this.col_sched_payment,
				this.col_payments_month_0,
				this.col_line_number,
				this.col_creditor_type,
				this.col_orig_dmp_payment
			});
			this.GridView1.CustomizationFormBounds = new System.Drawing.Rectangle(816, 570, 208, 170);
			StyleFormatCondition1.Appearance.BackColor = System.Drawing.Color.IndianRed;
			StyleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.Tomato;
			StyleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.Coral;
			StyleFormatCondition1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25f, System.Drawing.FontStyle.Bold);
			StyleFormatCondition1.Appearance.ForeColor = System.Drawing.Color.White;
			StyleFormatCondition1.Appearance.Options.UseBackColor = true;
			StyleFormatCondition1.Appearance.Options.UseBorderColor = true;
			StyleFormatCondition1.Appearance.Options.UseFont = true;
			StyleFormatCondition1.Appearance.Options.UseForeColor = true;
			StyleFormatCondition1.ApplyToRow = true;
			StyleFormatCondition1.Column = this.col_held;
			StyleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.NotEqual;
			StyleFormatCondition1.Value1 = false;
			this.GridView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] { StyleFormatCondition1 });
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsView.ColumnAutoWidth = false;
			this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
			this.GridView1.OptionsView.ShowFooter = true;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.col_creditor, DevExpress.Data.ColumnSortOrder.Ascending) });
			//
			//col_creditor
			//
			this.col_creditor.Caption = "Creditor";
			this.col_creditor.CustomizationCaption = "Creditor ID";
			this.col_creditor.FieldName = "Creditor";
			this.col_creditor.Name = "col_creditor";
			this.col_creditor.OptionsColumn.AllowEdit = false;
			this.col_creditor.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
			this.col_creditor.ToolTip = "ID for the creditor";
			this.col_creditor.UnboundType = DevExpress.Data.UnboundColumnType.String;
			this.col_creditor.Visible = true;
			this.col_creditor.VisibleIndex = 0;
			this.col_creditor.Width = 58;
			//
			//col_name
			//
			this.col_name.AppearanceCell.Options.UseTextOptions = true;
			this.col_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.col_name.AppearanceHeader.Options.UseTextOptions = true;
			this.col_name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.col_name.Caption = "Name";
			this.col_name.CustomizationCaption = "Creditor Name";
			this.col_name.FieldName = "creditor_name";
			this.col_name.Name = "col_name";
			this.col_name.OptionsColumn.AllowEdit = false;
			this.col_name.SummaryItem.DisplayFormat = "{0:n0} debt(s)";
			this.col_name.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
			this.col_name.ToolTip = "Creditor Name";
			this.col_name.UnboundType = DevExpress.Data.UnboundColumnType.String;
			this.col_name.Visible = true;
			this.col_name.VisibleIndex = 1;
			this.col_name.Width = 108;
			//
			//col_debit_amt
			//
			this.col_debit_amt.AppearanceCell.Options.UseTextOptions = true;
			this.col_debit_amt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_debit_amt.AppearanceHeader.Options.UseTextOptions = true;
			this.col_debit_amt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_debit_amt.Caption = "Pay";
			this.col_debit_amt.ColumnEdit = this.CalcEdit_col_debit_amt;
			this.col_debit_amt.CustomizationCaption = "Amount to be disbursed";
			this.col_debit_amt.DisplayFormat.FormatString = "c2";
			this.col_debit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_debit_amt.FieldName = "debit_amt";
			this.col_debit_amt.GroupFormat.FormatString = "c2";
			this.col_debit_amt.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_debit_amt.Name = "col_debit_amt";
			this.col_debit_amt.SummaryItem.DisplayFormat = "{0:c}";
			this.col_debit_amt.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_debit_amt.ToolTip = "Current amount to be paid";
			this.col_debit_amt.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
			this.col_debit_amt.Visible = true;
			this.col_debit_amt.VisibleIndex = 4;
			this.col_debit_amt.Width = 68;
			//
			//CalcEdit_col_debit_amt
			//
			this.CalcEdit_col_debit_amt.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.CalcEdit_col_debit_amt.Appearance.Options.UseTextOptions = true;
			this.CalcEdit_col_debit_amt.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.CalcEdit_col_debit_amt.AutoHeight = false;
			this.CalcEdit_col_debit_amt.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.CalcEdit_col_debit_amt.DisplayFormat.FormatString = "c2";
			this.CalcEdit_col_debit_amt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_col_debit_amt.EditFormat.FormatString = "f2";
			this.CalcEdit_col_debit_amt.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.CalcEdit_col_debit_amt.Name = "CalcEdit_col_debit_amt";
			this.CalcEdit_col_debit_amt.Precision = 2;
			this.CalcEdit_col_debit_amt.ValidateOnEnterKey = true;
			//
			//col_disbursement_factor
			//
			this.col_disbursement_factor.AppearanceCell.Options.UseTextOptions = true;
			this.col_disbursement_factor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_disbursement_factor.AppearanceHeader.Options.UseTextOptions = true;
			this.col_disbursement_factor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_disbursement_factor.Caption = "Disb Factor";
			this.col_disbursement_factor.CustomizationCaption = "Normal Monthly Payment";
			this.col_disbursement_factor.DisplayFormat.FormatString = "c2";
			this.col_disbursement_factor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_disbursement_factor.FieldName = "disbursement_factor";
			this.col_disbursement_factor.GroupFormat.FormatString = "c2";
			this.col_disbursement_factor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_disbursement_factor.Name = "col_disbursement_factor";
			this.col_disbursement_factor.OptionsColumn.AllowEdit = false;
			this.col_disbursement_factor.SummaryItem.DisplayFormat = "{0:c}";
			this.col_disbursement_factor.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_disbursement_factor.ToolTip = "Normal monthly payment amount";
			this.col_disbursement_factor.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
			this.col_disbursement_factor.Visible = true;
			this.col_disbursement_factor.VisibleIndex = 3;
			this.col_disbursement_factor.Width = 67;
			//
			//col_priority
			//
			this.col_priority.AppearanceCell.Options.UseTextOptions = true;
			this.col_priority.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.col_priority.AppearanceHeader.Options.UseTextOptions = true;
			this.col_priority.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
			this.col_priority.Caption = "P";
			this.col_priority.CustomizationCaption = "Priority for the debt payment";
			this.col_priority.DisplayFormat.FormatString = "{0:f0}";
			this.col_priority.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_priority.FieldName = "priority";
			this.col_priority.Name = "col_priority";
			this.col_priority.OptionsColumn.AllowEdit = false;
			this.col_priority.ToolTip = "Priority";
			this.col_priority.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
			this.col_priority.Visible = true;
			this.col_priority.VisibleIndex = 6;
			this.col_priority.Width = 20;
			//
			//col_balance
			//
			this.col_balance.AppearanceCell.Options.UseTextOptions = true;
			this.col_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_balance.AppearanceHeader.Options.UseTextOptions = true;
			this.col_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_balance.Caption = "Balance";
			this.col_balance.CustomizationCaption = "Debt Balance";
			this.col_balance.DisplayFormat.FormatString = "c2";
			this.col_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_balance.FieldName = "display_current_balance";
			this.col_balance.GroupFormat.FormatString = "c2";
			this.col_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_balance.Name = "col_balance";
			this.col_balance.OptionsColumn.AllowEdit = false;
			this.col_balance.SummaryItem.DisplayFormat = "{0:c}";
			this.col_balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_balance.ToolTip = "Debt Balance";
			this.col_balance.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
			this.col_balance.Visible = true;
			this.col_balance.VisibleIndex = 5;
			this.col_balance.Width = 72;
			//
			//col_acct_number
			//
			this.col_acct_number.Caption = "Acct#";
			this.col_acct_number.CustomizationCaption = "Account Number";
			this.col_acct_number.FieldName = "account_number";
			this.col_acct_number.Name = "col_acct_number";
			this.col_acct_number.OptionsColumn.AllowEdit = false;
			this.col_acct_number.ToolTip = "Account Number";
			this.col_acct_number.UnboundType = DevExpress.Data.UnboundColumnType.String;
			this.col_acct_number.Visible = true;
			this.col_acct_number.VisibleIndex = 2;
			this.col_acct_number.Width = 125;
			//
			//col_interest
			//
			this.col_interest.AppearanceCell.Options.UseTextOptions = true;
			this.col_interest.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_interest.AppearanceHeader.Options.UseTextOptions = true;
			this.col_interest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_interest.Caption = "Interest";
			this.col_interest.CustomizationCaption = "Debt Interest Rate";
			this.col_interest.DisplayFormat.FormatString = "{0:p}";
			this.col_interest.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_interest.FieldName = "display_intererst_rate";
			this.col_interest.GroupFormat.FormatString = "{0:p}";
			this.col_interest.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_interest.Name = "col_interest";
			this.col_interest.OptionsColumn.AllowEdit = false;
			this.col_interest.ToolTip = "Interest rate for debt";
			this.col_interest.Visible = true;
			this.col_interest.VisibleIndex = 7;
			this.col_interest.Width = 51;
			//
			//col_IncludeInFees
			//
			this.col_IncludeInFees.Caption = "IncludeInFees";
			this.col_IncludeInFees.CustomizationCaption = "IncludeInFees";
			this.col_IncludeInFees.FieldName = "IncludeInFees";
			this.col_IncludeInFees.Name = "col_IncludeInFees";
			this.col_IncludeInFees.OptionsColumn.AllowEdit = false;
			this.col_IncludeInFees.Width = 80;
			//
			//col_IncludeInProrate
			//
			this.col_IncludeInProrate.Caption = "IncludeInProrate";
			this.col_IncludeInProrate.CustomizationCaption = "IncludeInProrate";
			this.col_IncludeInProrate.FieldName = "IncludeInProrate";
			this.col_IncludeInProrate.Name = "col_IncludeInProrate";
			this.col_IncludeInProrate.OptionsColumn.AllowEdit = false;
			this.col_IncludeInProrate.Width = 93;
			//
			//col_Prorate
			//
			this.col_Prorate.Caption = "Prorate";
			this.col_Prorate.CustomizationCaption = "Prorate";
			this.col_Prorate.FieldName = "ccl_prorate";
			this.col_Prorate.Name = "col_Prorate";
			this.col_Prorate.OptionsColumn.AllowEdit = false;
			this.col_Prorate.Width = 48;
			//
			//col_ZeroBalance
			//
			this.col_ZeroBalance.Caption = "ZeroBalance";
			this.col_ZeroBalance.CustomizationCaption = "ZeroBalance";
			this.col_ZeroBalance.FieldName = "ccl_zero_balance";
			this.col_ZeroBalance.Name = "col_ZeroBalance";
			this.col_ZeroBalance.OptionsColumn.AllowEdit = false;
			this.col_ZeroBalance.Width = 71;
			//
			//col_orig_balance
			//
			this.col_orig_balance.AppearanceCell.Options.UseTextOptions = true;
			this.col_orig_balance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_orig_balance.AppearanceHeader.Options.UseTextOptions = true;
			this.col_orig_balance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_orig_balance.Caption = "Orig Bal";
			this.col_orig_balance.CustomizationCaption = "Original Balance";
			this.col_orig_balance.DisplayFormat.FormatString = "c2";
			this.col_orig_balance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_orig_balance.FieldName = "display_orig_balance";
			this.col_orig_balance.GroupFormat.FormatString = "c2";
			this.col_orig_balance.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_orig_balance.Name = "col_orig_balance";
			this.col_orig_balance.OptionsColumn.AllowEdit = false;
			this.col_orig_balance.SummaryItem.DisplayFormat = "{0:c}";
			this.col_orig_balance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_orig_balance.Width = 49;
			//
			//col_total_interest
			//
			this.col_total_interest.AppearanceCell.Options.UseTextOptions = true;
			this.col_total_interest.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_total_interest.AppearanceHeader.Options.UseTextOptions = true;
			this.col_total_interest.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_total_interest.Caption = "total_interest";
			this.col_total_interest.CustomizationCaption = "total_interest";
			this.col_total_interest.DisplayFormat.FormatString = "c2";
			this.col_total_interest.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_total_interest.FieldName = "display_total_interest";
			this.col_total_interest.GroupFormat.FormatString = "c2";
			this.col_total_interest.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_total_interest.Name = "col_total_interest";
			this.col_total_interest.OptionsColumn.AllowEdit = false;
			this.col_total_interest.SummaryItem.DisplayFormat = "{0:c}";
			this.col_total_interest.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_total_interest.Width = 77;
			//
			//col_debt_id
			//
			this.col_debt_id.Caption = "ID";
			this.col_debt_id.CustomizationCaption = "Debt ID";
			this.col_debt_id.FieldName = "display_debt_id";
			this.col_debt_id.Name = "col_debt_id";
			this.col_debt_id.OptionsColumn.AllowEdit = false;
			this.col_debt_id.Width = 23;
			//
			//col_sched_payment
			//
			this.col_sched_payment.AppearanceCell.Options.UseTextOptions = true;
			this.col_sched_payment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_sched_payment.AppearanceHeader.Options.UseTextOptions = true;
			this.col_sched_payment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_sched_payment.Caption = "Scheduled";
			this.col_sched_payment.CustomizationCaption = "Scheduled Payment";
			this.col_sched_payment.DisplayFormat.FormatString = "c2";
			this.col_sched_payment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_sched_payment.FieldName = "sched_payment";
			this.col_sched_payment.GroupFormat.FormatString = "c";
			this.col_sched_payment.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_sched_payment.Name = "col_sched_payment";
			this.col_sched_payment.OptionsColumn.AllowEdit = false;
			this.col_sched_payment.SummaryItem.DisplayFormat = "{0:c}";
			this.col_sched_payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_sched_payment.Width = 68;
			//
			//col_payments_month_0
			//
			this.col_payments_month_0.AppearanceCell.Options.UseTextOptions = true;
			this.col_payments_month_0.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_payments_month_0.AppearanceHeader.Options.UseTextOptions = true;
			this.col_payments_month_0.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.col_payments_month_0.Caption = "Curr Mo Disb";
			this.col_payments_month_0.CustomizationCaption = "Payments made this month";
			this.col_payments_month_0.DisplayFormat.FormatString = "c2";
			this.col_payments_month_0.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_payments_month_0.FieldName = "payments_month_0";
			this.col_payments_month_0.GroupFormat.FormatString = "c2";
			this.col_payments_month_0.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.col_payments_month_0.Name = "col_payments_month_0";
			this.col_payments_month_0.OptionsColumn.AllowEdit = false;
			this.col_payments_month_0.SummaryItem.DisplayFormat = "{0:c}";
			this.col_payments_month_0.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_payments_month_0.Width = 73;
			//
			//col_line_number
			//
			this.col_line_number.Caption = "Line";
			this.col_line_number.CustomizationCaption = "Stacking Order";
			this.col_line_number.FieldName = "line_number";
			this.col_line_number.Name = "col_line_number";
			this.col_line_number.OptionsColumn.AllowEdit = false;
			this.col_line_number.Width = 41;
			//
			//col_creditor_type
			//
			this.col_creditor_type.Caption = "Type";
			this.col_creditor_type.CustomizationCaption = "Bill/Deduct/None Contrib status";
			this.col_creditor_type.FieldName = "creditor_type";
			this.col_creditor_type.Name = "col_creditor_type";
			this.col_creditor_type.OptionsColumn.AllowEdit = false;
			this.col_creditor_type.Width = 50;
			//
			//col_orig_dmp_payment
			//
			this.col_orig_dmp_payment.Caption = "Orig Pmt";
			this.col_orig_dmp_payment.CustomizationCaption = "Payment proposed to creditor";
			this.col_orig_dmp_payment.FieldName = "orig_dmp_payment";
			this.col_orig_dmp_payment.Name = "col_orig_dmp_payment";
			this.col_orig_dmp_payment.OptionsColumn.AllowEdit = false;
			this.col_orig_dmp_payment.SummaryItem.DisplayFormat = "{0:c}";
			this.col_orig_dmp_payment.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
			this.col_orig_dmp_payment.Width = 53;
			//
			//DepositsControl1
			//
			this.DepositsControl1.Location = new System.Drawing.Point(238, 158);
			this.DepositsControl1.MaximumSize = new System.Drawing.Size(148, 101);
			this.DepositsControl1.Name = "DepositsControl1";
			this.DepositsControl1.Size = new System.Drawing.Size(148, 101);
			this.DepositsControl1.TabIndex = 10;
			//
			//GroupControl2
			//
			this.GroupControl2.Controls.Add(this.Difference);
			this.GroupControl2.Controls.Add(this.SumOfDisbursements);
			this.GroupControl2.Controls.Add(this.TrustFundsAvailable);
			this.GroupControl2.Controls.Add(this.TrustFundsOnHold);
			this.GroupControl2.Controls.Add(this.LabelControl10);
			this.GroupControl2.Controls.Add(this.LabelControl13);
			this.GroupControl2.Controls.Add(this.LabelControl14);
			this.GroupControl2.Controls.Add(this.LabelControl15);
			this.GroupControl2.Controls.Add(this.LabelControl16);
			this.GroupControl2.Location = new System.Drawing.Point(8, 158);
			this.GroupControl2.Name = "GroupControl2";
			this.GroupControl2.Size = new System.Drawing.Size(223, 101);
			this.GroupControl2.TabIndex = 9;
			this.GroupControl2.Text = "Payment Information";
			//
			//Difference
			//
			this.Difference.Appearance.Options.UseTextOptions = true;
			this.Difference.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.Difference.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.Difference.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.Difference.Location = new System.Drawing.Point(134, 72);
			this.Difference.Name = "Difference";
			this.Difference.Size = new System.Drawing.Size(84, 13);
			this.Difference.TabIndex = 14;
			this.Difference.Text = "$0.00";
			this.Difference.ToolTip = "Excess (both greater and less). This should be $0.00 if the disbursements match t" + "he trust. No disbursement allowed for funds greater than what is in the trust.";
			this.Difference.UseMnemonic = false;
			//
			//SumOfDisbursements
			//
			this.SumOfDisbursements.Appearance.Options.UseTextOptions = true;
			this.SumOfDisbursements.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.SumOfDisbursements.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.SumOfDisbursements.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.SumOfDisbursements.Location = new System.Drawing.Point(134, 56);
			this.SumOfDisbursements.Name = "SumOfDisbursements";
			this.SumOfDisbursements.Size = new System.Drawing.Size(84, 13);
			this.SumOfDisbursements.TabIndex = 13;
			this.SumOfDisbursements.Text = "$0.00";
			this.SumOfDisbursements.ToolTip = "Total of all of the monies to be paid to creditors";
			this.SumOfDisbursements.UseMnemonic = false;
			//
			//TrustFundsAvailable
			//
			this.TrustFundsAvailable.Appearance.Options.UseTextOptions = true;
			this.TrustFundsAvailable.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TrustFundsAvailable.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.TrustFundsAvailable.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.TrustFundsAvailable.Location = new System.Drawing.Point(134, 40);
			this.TrustFundsAvailable.Name = "TrustFundsAvailable";
			this.TrustFundsAvailable.Size = new System.Drawing.Size(84, 13);
			this.TrustFundsAvailable.TabIndex = 12;
			this.TrustFundsAvailable.Text = "$0.00";
			this.TrustFundsAvailable.ToolTip = "Dollar amount available in the trust to be disbursed";
			this.TrustFundsAvailable.UseMnemonic = false;
			//
			//TrustFundsOnHold
			//
			this.TrustFundsOnHold.Appearance.Options.UseTextOptions = true;
			this.TrustFundsOnHold.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.TrustFundsOnHold.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.TrustFundsOnHold.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
			this.TrustFundsOnHold.Location = new System.Drawing.Point(134, 24);
			this.TrustFundsOnHold.Name = "TrustFundsOnHold";
			this.TrustFundsOnHold.Size = new System.Drawing.Size(84, 13);
			this.TrustFundsOnHold.TabIndex = 11;
			this.TrustFundsOnHold.Text = "$0.00";
			this.TrustFundsOnHold.ToolTip = "Funds that are deposited but not available for disbursement";
			this.TrustFundsOnHold.UseMnemonic = false;
			//
			//LabelControl10
			//
			this.LabelControl10.Location = new System.Drawing.Point(128, 24);
			this.LabelControl10.Name = "LabelControl10";
			this.LabelControl10.Size = new System.Drawing.Size(0, 13);
			this.LabelControl10.TabIndex = 6;
			//
			//LabelControl13
			//
			this.LabelControl13.Appearance.Options.UseTextOptions = true;
			this.LabelControl13.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl13.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl13.Location = new System.Drawing.Point(16, 72);
			this.LabelControl13.Name = "LabelControl13";
			this.LabelControl13.Size = new System.Drawing.Size(50, 13);
			this.LabelControl13.TabIndex = 3;
			this.LabelControl13.Text = "Difference";
			this.LabelControl13.UseMnemonic = false;
			//
			//LabelControl14
			//
			this.LabelControl14.Appearance.Options.UseTextOptions = true;
			this.LabelControl14.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl14.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl14.Location = new System.Drawing.Point(16, 56);
			this.LabelControl14.Name = "LabelControl14";
			this.LabelControl14.Size = new System.Drawing.Size(108, 13);
			this.LabelControl14.TabIndex = 2;
			this.LabelControl14.Text = "Sum Of Disbursements";
			this.LabelControl14.UseMnemonic = false;
			//
			//LabelControl15
			//
			this.LabelControl15.Appearance.Options.UseTextOptions = true;
			this.LabelControl15.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl15.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl15.Location = new System.Drawing.Point(16, 40);
			this.LabelControl15.Name = "LabelControl15";
			this.LabelControl15.Size = new System.Drawing.Size(103, 13);
			this.LabelControl15.TabIndex = 1;
			this.LabelControl15.Text = "Trust Funds Available";
			this.LabelControl15.UseMnemonic = false;
			//
			//LabelControl16
			//
			this.LabelControl16.Appearance.Options.UseTextOptions = true;
			this.LabelControl16.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.LabelControl16.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
			this.LabelControl16.Location = new System.Drawing.Point(16, 24);
			this.LabelControl16.Name = "LabelControl16";
			this.LabelControl16.Size = new System.Drawing.Size(98, 13);
			this.LabelControl16.TabIndex = 0;
			this.LabelControl16.Text = "Turst Funds On Hold";
			this.LabelControl16.UseMnemonic = false;
			//
			//Fix bug in sorting option
			//
			this.col_acct_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_creditor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_creditor_type.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_debit_amt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_debt_id.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_disbursement_factor.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_held.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_IncludeInFees.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_IncludeInProrate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_line_number.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_name.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_orig_balance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_orig_dmp_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_payments_month_0.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_priority.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_Prorate.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_sched_payment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_total_interest.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			this.col_ZeroBalance.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
			//
			//Examine_Form
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.ClientSize = new System.Drawing.Size(658, 449);
			this.Controls.Add(this.GroupControl2);
			this.Controls.Add(this.DepositsControl1);
			this.Controls.Add(this.GridControl1);
			this.Controls.Add(this.GroupControl1);
			this.Controls.Add(this.ButtonPrevious);
			this.Controls.Add(this.ButtonShowNotes);
			this.Controls.Add(this.ButtonShowClient);
			this.Controls.Add(this.ButtonDontPay);
			this.Controls.Add(this.ButtonPay);
			this.Controls.Add(this.ButtonProrate);
			this.Controls.Add(this.ButtonClear);
			this.Controls.Add(this.ButtonQuit);
			this.Name = "Examine_Form";
			this.Text = "Disbursement Client Information";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl1).EndInit();
			this.GroupControl1.ResumeLayout(false);
			this.GroupControl1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CalcEdit_col_debit_amt).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GroupControl2).EndInit();
			this.GroupControl2.ResumeLayout(false);
			this.GroupControl2.PerformLayout();
			this.ResumeLayout(false);
		}

		internal DevExpress.XtraEditors.LabelControl LabelControl6;
		internal DevExpress.XtraEditors.SimpleButton ButtonQuit;
		internal DevExpress.XtraEditors.SimpleButton ButtonClear;
		internal DevExpress.XtraEditors.SimpleButton ButtonProrate;
		internal DevExpress.XtraEditors.SimpleButton ButtonPay;
		internal DevExpress.XtraEditors.SimpleButton ButtonDontPay;
		internal DevExpress.XtraEditors.SimpleButton ButtonShowClient;
		internal DevExpress.XtraEditors.SimpleButton ButtonShowNotes;
		internal DevExpress.XtraEditors.SimpleButton ButtonPrevious;
		internal DevExpress.XtraEditors.GroupControl GroupControl1;
		internal DevExpress.XtraEditors.LabelControl LabelControl1;
		internal DevExpress.XtraEditors.LabelControl LabelControl2;
		internal DevExpress.XtraEditors.LabelControl LabelControl3;
		internal DevExpress.XtraEditors.LabelControl LabelControl4;
		internal DevExpress.XtraEditors.LabelControl LabelControl5;
		internal DevExpress.XtraEditors.LabelControl lbl_address;
		internal DevExpress.XtraEditors.LabelControl lbl_client;
		internal DevExpress.XtraEditors.LabelControl lbl_name;
		internal DevExpress.XtraEditors.LabelControl lbl_home_ph;
		internal DevExpress.XtraEditors.LabelControl lbl_work_ph;
		internal DevExpress.XtraEditors.LabelControl lbl_language;
		internal DevExpress.XtraGrid.GridControl GridControl1;
		internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		internal DevExpress.XtraEditors.Repository.RepositoryItemCalcEdit CalcEdit_col_debit_amt;
		internal DevExpress.XtraGrid.Columns.GridColumn col_creditor;
		internal DevExpress.XtraGrid.Columns.GridColumn col_debt_id;
		internal DevExpress.XtraGrid.Columns.GridColumn col_sched_payment;
		internal DevExpress.XtraGrid.Columns.GridColumn col_disbursement_factor;
		internal DevExpress.XtraGrid.Columns.GridColumn col_payments_month_0;
		internal DevExpress.XtraGrid.Columns.GridColumn col_orig_balance;
		internal DevExpress.XtraGrid.Columns.GridColumn col_orig_dmp_payment;
		internal DevExpress.XtraGrid.Columns.GridColumn col_interest;
		internal DevExpress.XtraGrid.Columns.GridColumn col_balance;
		internal DevExpress.XtraGrid.Columns.GridColumn col_debit_amt;
		internal DevExpress.XtraGrid.Columns.GridColumn col_priority;
		internal DevExpress.XtraGrid.Columns.GridColumn col_name;
		internal DevExpress.XtraGrid.Columns.GridColumn col_acct_number;
		internal DevExpress.XtraGrid.Columns.GridColumn col_line_number;
		internal DevExpress.XtraGrid.Columns.GridColumn col_ZeroBalance;
		internal DevExpress.XtraGrid.Columns.GridColumn col_Prorate;
		internal DevExpress.XtraGrid.Columns.GridColumn col_creditor_type;
		internal DevExpress.XtraGrid.Columns.GridColumn col_held;
		internal DevExpress.XtraGrid.Columns.GridColumn col_IncludeInProrate;
		internal DevExpress.XtraGrid.Columns.GridColumn col_IncludeInFees;
		internal DevExpress.XtraGrid.Columns.GridColumn col_total_interest;
		internal DebtPlus.UI.Desktop.CS.Disbursement.Common.DepositsControl DepositsControl1;
		internal DevExpress.XtraEditors.GroupControl GroupControl2;
		internal DevExpress.XtraEditors.LabelControl Difference;
		internal DevExpress.XtraEditors.LabelControl SumOfDisbursements;
		internal DevExpress.XtraEditors.LabelControl TrustFundsAvailable;
		internal DevExpress.XtraEditors.LabelControl TrustFundsOnHold;
		internal DevExpress.XtraEditors.LabelControl LabelControl10;
		internal DevExpress.XtraEditors.LabelControl LabelControl13;
		internal DevExpress.XtraEditors.LabelControl LabelControl14;
		internal DevExpress.XtraEditors.LabelControl LabelControl15;
		internal DevExpress.XtraEditors.LabelControl LabelControl16;
	}
}
