using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Counselor.Reassign
{
	partial class Form1 : DebtPlus.Data.Forms.DebtPlusForm
	{
		//Form overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			try {
				if (disposing && components != null) {
					components.Dispose();
				}
			} finally {
				base.Dispose(disposing);
			}
		}

		//Required by the Windows Form Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Windows Form Designer
		//It can be modified using the Windows Form Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
			this.Bar2 = new DevExpress.XtraBars.Bar();
			this.BarSubItem1 = new DevExpress.XtraBars.BarSubItem();
			this.BarButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
			this.BarButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
			this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
			this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
			this.SimpleButton1 = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControl1 = new DevExpress.XtraLayout.LayoutControl();
			this.CheckEdit_ReassignActiveOnly = new DevExpress.XtraEditors.CheckEdit();
			this.LookUpEdit_OldCounselor = new DevExpress.XtraEditors.LookUpEdit();
			this.GridControl1 = new DevExpress.XtraGrid.GridControl();
			this.GridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
			this.GridColumn_Counselor = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_Name = new DevExpress.XtraGrid.Columns.GridColumn();
			this.GridColumn_Percent = new DevExpress.XtraGrid.Columns.GridColumn();
			this.RepositoryItemTextEdit_Percentage = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
			this.SimpleButton2 = new DevExpress.XtraEditors.SimpleButton();
			this.LayoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
			this.LayoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
			this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.LayoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
			this.EmptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
			this.CheckEdit_GenerateLetters = new DevExpress.XtraEditors.CheckEdit();
			this.LayoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).BeginInit();
			this.LayoutControl1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ReassignActiveOnly.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_OldCounselor.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemTextEdit_Percentage).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_GenerateLetters.Properties).BeginInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).BeginInit();
			this.SuspendLayout();
			//
			//barManager1
			//
			this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { this.Bar2 });
			this.barManager1.DockControls.Add(this.barDockControlTop);
			this.barManager1.DockControls.Add(this.barDockControlBottom);
			this.barManager1.DockControls.Add(this.barDockControlLeft);
			this.barManager1.DockControls.Add(this.barDockControlRight);
			this.barManager1.Form = this;
			this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
				this.BarSubItem1,
				this.BarButtonItem1,
				this.BarButtonItem2
			});
			this.barManager1.MainMenu = this.Bar2;
			this.barManager1.MaxItemId = 3;
			//
			//Bar2
			//
			this.Bar2.BarName = "Main menu";
			this.Bar2.DockCol = 0;
			this.Bar2.DockRow = 0;
			this.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
			this.Bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(this.BarSubItem1) });
			this.Bar2.OptionsBar.MultiLine = true;
			this.Bar2.OptionsBar.UseWholeRow = true;
			this.Bar2.Text = "Main menu";
			//
			//BarSubItem1
			//
			this.BarSubItem1.Caption = "&File";
			this.BarSubItem1.Id = 0;
			this.BarSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem1),
				new DevExpress.XtraBars.LinkPersistInfo(this.BarButtonItem2, true)
			});
			this.BarSubItem1.Name = "BarSubItem1";
			//
			//BarButtonItem1
			//
			this.BarButtonItem1.Caption = "&Preview...";
			this.BarButtonItem1.Enabled = false;
			this.BarButtonItem1.Id = 1;
			this.BarButtonItem1.Name = "BarButtonItem1";
			//
			//BarButtonItem2
			//
			this.BarButtonItem2.Caption = "&Exit";
			this.BarButtonItem2.Id = 2;
			this.BarButtonItem2.Name = "BarButtonItem2";
			//
			//barDockControlTop
			//
			this.barDockControlTop.CausesValidation = false;
			this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
			this.barDockControlTop.Size = new System.Drawing.Size(445, 24);
			//
			//barDockControlBottom
			//
			this.barDockControlBottom.CausesValidation = false;
			this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.barDockControlBottom.Location = new System.Drawing.Point(0, 339);
			this.barDockControlBottom.Size = new System.Drawing.Size(445, 0);
			//
			//barDockControlLeft
			//
			this.barDockControlLeft.CausesValidation = false;
			this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
			this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
			this.barDockControlLeft.Size = new System.Drawing.Size(0, 315);
			//
			//barDockControlRight
			//
			this.barDockControlRight.CausesValidation = false;
			this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
			this.barDockControlRight.Location = new System.Drawing.Point(445, 24);
			this.barDockControlRight.Size = new System.Drawing.Size(0, 315);
			//
			//SimpleButton1
			//
			this.SimpleButton1.Enabled = false;
			this.SimpleButton1.Location = new System.Drawing.Point(358, 51);
			this.SimpleButton1.MaximumSize = new System.Drawing.Size(75, 0);
			this.SimpleButton1.Name = "SimpleButton1";
			this.SimpleButton1.Size = new System.Drawing.Size(75, 22);
			this.SimpleButton1.StyleController = this.LayoutControl1;
			this.SimpleButton1.TabIndex = 4;
			this.SimpleButton1.Text = "&OK";
			//
			//LayoutControl1
			//
			this.LayoutControl1.Controls.Add(this.CheckEdit_GenerateLetters);
			this.LayoutControl1.Controls.Add(this.CheckEdit_ReassignActiveOnly);
			this.LayoutControl1.Controls.Add(this.LookUpEdit_OldCounselor);
			this.LayoutControl1.Controls.Add(this.GridControl1);
			this.LayoutControl1.Controls.Add(this.SimpleButton1);
			this.LayoutControl1.Controls.Add(this.SimpleButton2);
			this.LayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.LayoutControl1.Location = new System.Drawing.Point(0, 24);
			this.LayoutControl1.Name = "LayoutControl1";
			this.LayoutControl1.Root = this.LayoutControlGroup1;
			this.LayoutControl1.Size = new System.Drawing.Size(445, 315);
			this.LayoutControl1.TabIndex = 6;
			this.LayoutControl1.Text = "LayoutControl1";
			//
			//CheckEdit_ReassignActiveOnly
			//
			this.CheckEdit_ReassignActiveOnly.Location = new System.Drawing.Point(12, 283);
			this.CheckEdit_ReassignActiveOnly.MenuManager = this.barManager1;
			this.CheckEdit_ReassignActiveOnly.Name = "CheckEdit_ReassignActiveOnly";
			this.CheckEdit_ReassignActiveOnly.Properties.Caption = "Reassign Only Active Clients";
			this.CheckEdit_ReassignActiveOnly.Size = new System.Drawing.Size(208, 19);
			this.CheckEdit_ReassignActiveOnly.StyleController = this.LayoutControl1;
			this.CheckEdit_ReassignActiveOnly.TabIndex = 8;
			//
			//LookUpEdit_OldCounselor
			//
			this.LookUpEdit_OldCounselor.Location = new System.Drawing.Point(101, 12);
			this.LookUpEdit_OldCounselor.MenuManager = this.barManager1;
			this.LookUpEdit_OldCounselor.Name = "LookUpEdit_OldCounselor";
			this.LookUpEdit_OldCounselor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
			this.LookUpEdit_OldCounselor.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Counselor", "ID", 5, DevExpress.Utils.FormatType.Numeric, "f0", true, DevExpress.Utils.HorzAlignment.Far, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.True),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True),
				new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Note", "Note", 10, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending, DevExpress.Utils.DefaultBoolean.True)
			});
			this.LookUpEdit_OldCounselor.Properties.DisplayMember = "Name";
			this.LookUpEdit_OldCounselor.Properties.NullText = "";
			this.LookUpEdit_OldCounselor.Properties.ShowFooter = false;
			this.LookUpEdit_OldCounselor.Properties.SortColumnIndex = 1;
			this.LookUpEdit_OldCounselor.Properties.ValueMember = "Counselor";
			this.LookUpEdit_OldCounselor.Size = new System.Drawing.Size(332, 20);
			this.LookUpEdit_OldCounselor.StyleController = this.LayoutControl1;
			this.LookUpEdit_OldCounselor.TabIndex = 7;
			this.LookUpEdit_OldCounselor.ToolTip = "Counselor that you wish to reassign";
			//
			//GridControl1
			//
			this.GridControl1.Location = new System.Drawing.Point(12, 67);
			this.GridControl1.MainView = this.GridView1;
			this.GridControl1.MenuManager = this.barManager1;
			this.GridControl1.MinimumSize = new System.Drawing.Size(0, 200);
			this.GridControl1.Name = "GridControl1";
			this.GridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { this.RepositoryItemTextEdit_Percentage });
			this.GridControl1.Size = new System.Drawing.Size(342, 200);
			this.GridControl1.TabIndex = 6;
			this.GridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { this.GridView1 });
			//
			//GridView1
			//
			this.GridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
				this.GridColumn_Counselor,
				this.GridColumn_Name,
				this.GridColumn_Percent
			});
			this.GridView1.GridControl = this.GridControl1;
			this.GridView1.Name = "GridView1";
			this.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
			this.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
			this.GridView1.OptionsBehavior.AutoPopulateColumns = false;
			this.GridView1.OptionsSelection.UseIndicatorForSelection = false;
			this.GridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
			this.GridView1.OptionsView.ShowFooter = true;
			this.GridView1.OptionsView.ShowGroupPanel = false;
			this.GridView1.OptionsView.ShowIndicator = false;
			this.GridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] { new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.GridColumn_Name, DevExpress.Data.ColumnSortOrder.Ascending) });
			//
			//GridColumn_Counselor
			//
			this.GridColumn_Counselor.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Counselor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Counselor.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Counselor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Counselor.Caption = "ID";
			this.GridColumn_Counselor.CustomizationCaption = "Counselor ID";
			this.GridColumn_Counselor.DisplayFormat.FormatString = "f0";
			this.GridColumn_Counselor.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Counselor.FieldName = "Counselor";
			this.GridColumn_Counselor.GroupFormat.FormatString = "f0";
			this.GridColumn_Counselor.GroupFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Counselor.Name = "GridColumn_Counselor";
			this.GridColumn_Counselor.OptionsColumn.AllowEdit = false;
			this.GridColumn_Counselor.ToolTip = "Counselor Record ID";
			this.GridColumn_Counselor.Visible = true;
			this.GridColumn_Counselor.VisibleIndex = 0;
			this.GridColumn_Counselor.Width = 87;
			//
			//GridColumn_Name
			//
			this.GridColumn_Name.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_Name.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Name.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
			this.GridColumn_Name.Caption = "Name";
			this.GridColumn_Name.CustomizationCaption = "Counselor Name";
			this.GridColumn_Name.FieldName = "Name";
			this.GridColumn_Name.Name = "GridColumn_Name";
			this.GridColumn_Name.OptionsColumn.AllowEdit = false;
			this.GridColumn_Name.Visible = true;
			this.GridColumn_Name.VisibleIndex = 1;
			this.GridColumn_Name.Width = 401;
			//
			//GridColumn_Percent
			//
			this.GridColumn_Percent.AppearanceCell.Options.UseTextOptions = true;
			this.GridColumn_Percent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Percent.AppearanceHeader.Options.UseTextOptions = true;
			this.GridColumn_Percent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
			this.GridColumn_Percent.Caption = "Percent";
			this.GridColumn_Percent.ColumnEdit = this.RepositoryItemTextEdit_Percentage;
			this.GridColumn_Percent.CustomizationCaption = "Percentage of clients";
			this.GridColumn_Percent.DisplayFormat.FormatString = "{0:#0;-#0; }";
			this.GridColumn_Percent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.GridColumn_Percent.FieldName = "Percent";
			this.GridColumn_Percent.Name = "GridColumn_Percent";
			this.GridColumn_Percent.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value;
			this.GridColumn_Percent.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] { new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Percent", "{0:f0}") });
			this.GridColumn_Percent.ToolTip = "Percentage of the clients";
			this.GridColumn_Percent.Visible = true;
			this.GridColumn_Percent.VisibleIndex = 2;
			this.GridColumn_Percent.Width = 112;
			//
			//RepositoryItemTextEdit_Percentage
			//
			this.RepositoryItemTextEdit_Percentage.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
			this.RepositoryItemTextEdit_Percentage.AutoHeight = false;
			this.RepositoryItemTextEdit_Percentage.DisplayFormat.FormatString = "f0";
			this.RepositoryItemTextEdit_Percentage.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemTextEdit_Percentage.EditFormat.FormatString = "f0";
			this.RepositoryItemTextEdit_Percentage.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
			this.RepositoryItemTextEdit_Percentage.Mask.BeepOnError = true;
			this.RepositoryItemTextEdit_Percentage.Mask.EditMask = "f0";
			this.RepositoryItemTextEdit_Percentage.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
			this.RepositoryItemTextEdit_Percentage.Name = "RepositoryItemTextEdit_Percentage";
			//
			//SimpleButton2
			//
			this.SimpleButton2.Location = new System.Drawing.Point(358, 77);
			this.SimpleButton2.MaximumSize = new System.Drawing.Size(75, 0);
			this.SimpleButton2.Name = "SimpleButton2";
			this.SimpleButton2.Size = new System.Drawing.Size(75, 22);
			this.SimpleButton2.StyleController = this.LayoutControl1;
			this.SimpleButton2.TabIndex = 5;
			this.SimpleButton2.Text = "&Cancel";
			//
			//LayoutControlGroup1
			//
			this.LayoutControlGroup1.CustomizationFormText = "LayoutControlGroup1";
			this.LayoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
			this.LayoutControlGroup1.GroupBordersVisible = false;
			this.LayoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
				this.LayoutControlItem1,
				this.LayoutControlItem2,
				this.LayoutControlItem3,
				this.LayoutControlItem4,
				this.EmptySpaceItem1,
				this.LayoutControlItem5,
				this.EmptySpaceItem2,
				this.LayoutControlItem6
			});
			this.LayoutControlGroup1.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlGroup1.Name = "LayoutControlGroup1";
			this.LayoutControlGroup1.Size = new System.Drawing.Size(445, 315);
			this.LayoutControlGroup1.Text = "LayoutControlGroup1";
			this.LayoutControlGroup1.TextVisible = false;
			//
			//LayoutControlItem1
			//
			this.LayoutControlItem1.Control = this.SimpleButton1;
			this.LayoutControlItem1.CustomizationFormText = "OK Button";
			this.LayoutControlItem1.Location = new System.Drawing.Point(346, 39);
			this.LayoutControlItem1.Name = "LayoutControlItem1";
			this.LayoutControlItem1.Size = new System.Drawing.Size(79, 26);
			this.LayoutControlItem1.Text = "OK Button";
			this.LayoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem1.TextToControlDistance = 0;
			this.LayoutControlItem1.TextVisible = false;
			//
			//LayoutControlItem2
			//
			this.LayoutControlItem2.Control = this.SimpleButton2;
			this.LayoutControlItem2.CustomizationFormText = "Cancel Button";
			this.LayoutControlItem2.Location = new System.Drawing.Point(346, 65);
			this.LayoutControlItem2.Name = "LayoutControlItem2";
			this.LayoutControlItem2.Size = new System.Drawing.Size(79, 194);
			this.LayoutControlItem2.Text = "Cancel Button";
			this.LayoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
			this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem2.TextToControlDistance = 0;
			this.LayoutControlItem2.TextVisible = false;
			//
			//LayoutControlItem3
			//
			this.LayoutControlItem3.Control = this.GridControl1;
			this.LayoutControlItem3.CustomizationFormText = "New Counselor(s)";
			this.LayoutControlItem3.Location = new System.Drawing.Point(0, 39);
			this.LayoutControlItem3.Name = "LayoutControlItem3";
			this.LayoutControlItem3.Size = new System.Drawing.Size(346, 220);
			this.LayoutControlItem3.Text = "New Counselor(s)";
			this.LayoutControlItem3.TextLocation = DevExpress.Utils.Locations.Top;
			this.LayoutControlItem3.TextSize = new System.Drawing.Size(85, 13);
			//
			//LayoutControlItem4
			//
			this.LayoutControlItem4.Control = this.LookUpEdit_OldCounselor;
			this.LayoutControlItem4.CustomizationFormText = "Old Counselor";
			this.LayoutControlItem4.Location = new System.Drawing.Point(0, 0);
			this.LayoutControlItem4.Name = "LayoutControlItem4";
			this.LayoutControlItem4.Size = new System.Drawing.Size(425, 24);
			this.LayoutControlItem4.Text = "Old Counselor";
			this.LayoutControlItem4.TextSize = new System.Drawing.Size(85, 13);
			//
			//EmptySpaceItem1
			//
			this.EmptySpaceItem1.AllowHotTrack = false;
			this.EmptySpaceItem1.CustomizationFormText = "EmptySpaceItem1";
			this.EmptySpaceItem1.Location = new System.Drawing.Point(0, 24);
			this.EmptySpaceItem1.MaxSize = new System.Drawing.Size(0, 15);
			this.EmptySpaceItem1.MinSize = new System.Drawing.Size(10, 15);
			this.EmptySpaceItem1.Name = "EmptySpaceItem1";
			this.EmptySpaceItem1.Size = new System.Drawing.Size(425, 15);
			this.EmptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.EmptySpaceItem1.Text = "EmptySpaceItem1";
			this.EmptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
			//
			//LayoutControlItem5
			//
			this.LayoutControlItem5.Control = this.CheckEdit_ReassignActiveOnly;
			this.LayoutControlItem5.CustomizationFormText = "LayoutControlItem5";
			this.LayoutControlItem5.Location = new System.Drawing.Point(0, 271);
			this.LayoutControlItem5.Name = "LayoutControlItem5";
			this.LayoutControlItem5.Size = new System.Drawing.Size(212, 24);
			this.LayoutControlItem5.Text = "LayoutControlItem5";
			this.LayoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem5.TextToControlDistance = 0;
			this.LayoutControlItem5.TextVisible = false;
			//
			//EmptySpaceItem2
			//
			this.EmptySpaceItem2.AllowHotTrack = false;
			this.EmptySpaceItem2.CustomizationFormText = "EmptySpaceItem2";
			this.EmptySpaceItem2.Location = new System.Drawing.Point(0, 259);
			this.EmptySpaceItem2.MaxSize = new System.Drawing.Size(0, 12);
			this.EmptySpaceItem2.MinSize = new System.Drawing.Size(10, 12);
			this.EmptySpaceItem2.Name = "EmptySpaceItem2";
			this.EmptySpaceItem2.Size = new System.Drawing.Size(425, 12);
			this.EmptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
			this.EmptySpaceItem2.Text = "EmptySpaceItem2";
			this.EmptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
			//
			//CheckEdit_GenerateLetters
			//
			this.CheckEdit_GenerateLetters.Location = new System.Drawing.Point(224, 283);
			this.CheckEdit_GenerateLetters.MenuManager = this.barManager1;
			this.CheckEdit_GenerateLetters.Name = "CheckEdit_GenerateLetters";
			this.CheckEdit_GenerateLetters.Properties.Caption = "Generate Letters";
			this.CheckEdit_GenerateLetters.Size = new System.Drawing.Size(209, 19);
			this.CheckEdit_GenerateLetters.StyleController = this.LayoutControl1;
			this.CheckEdit_GenerateLetters.TabIndex = 9;
			this.CheckEdit_GenerateLetters.ToolTip = "Check if you wish to send a client letter";
			//
			//LayoutControlItem6
			//
			this.LayoutControlItem6.Control = this.CheckEdit_GenerateLetters;
			this.LayoutControlItem6.CustomizationFormText = "LayoutControlItem6";
			this.LayoutControlItem6.Location = new System.Drawing.Point(212, 271);
			this.LayoutControlItem6.Name = "LayoutControlItem6";
			this.LayoutControlItem6.Size = new System.Drawing.Size(213, 24);
			this.LayoutControlItem6.Text = "LayoutControlItem6";
			this.LayoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
			this.LayoutControlItem6.TextToControlDistance = 0;
			this.LayoutControlItem6.TextVisible = false;
			//
			//Form1
			//
			this.AutoScaleDimensions = new System.Drawing.SizeF(6f, 13f);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(445, 339);
			this.Controls.Add(this.LayoutControl1);
			this.Controls.Add(this.barDockControlLeft);
			this.Controls.Add(this.barDockControlRight);
			this.Controls.Add(this.barDockControlBottom);
			this.Controls.Add(this.barDockControlTop);
			this.Name = "Form1";
			this.Text = "Reassign Counselors";
			((System.ComponentModel.ISupportInitialize)this.DxErrorProvider1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.barManager1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControl1).EndInit();
			this.LayoutControl1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_ReassignActiveOnly.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LookUpEdit_OldCounselor.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridControl1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.GridView1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.RepositoryItemTextEdit_Percentage).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlGroup1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem3).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem4).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem1).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem5).EndInit();
			((System.ComponentModel.ISupportInitialize)this.EmptySpaceItem2).EndInit();
			((System.ComponentModel.ISupportInitialize)this.CheckEdit_GenerateLetters.Properties).EndInit();
			((System.ComponentModel.ISupportInitialize)this.LayoutControlItem6).EndInit();
			this.ResumeLayout(false);

		}
		protected internal DevExpress.XtraBars.BarManager barManager1;
		protected internal DevExpress.XtraBars.Bar Bar2;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlTop;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlBottom;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlLeft;
		protected internal DevExpress.XtraBars.BarDockControl barDockControlRight;
		protected internal DevExpress.XtraLayout.LayoutControl LayoutControl1;
		protected internal DevExpress.XtraGrid.GridControl GridControl1;
		protected internal DevExpress.XtraGrid.Views.Grid.GridView GridView1;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Counselor;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Name;
		protected internal DevExpress.XtraGrid.Columns.GridColumn GridColumn_Percent;
		protected internal DevExpress.XtraEditors.Repository.RepositoryItemTextEdit RepositoryItemTextEdit_Percentage;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton1;
		protected internal DevExpress.XtraEditors.SimpleButton SimpleButton2;
		protected internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem1;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
		protected internal DevExpress.XtraEditors.LookUpEdit LookUpEdit_OldCounselor;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem1;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_ReassignActiveOnly;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem5;
		protected internal DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem2;
		protected internal DevExpress.XtraBars.BarSubItem BarSubItem1;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem1;
		protected internal DevExpress.XtraBars.BarButtonItem BarButtonItem2;
		protected internal DevExpress.XtraEditors.CheckEdit CheckEdit_GenerateLetters;
		protected internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem6;
	}
}
