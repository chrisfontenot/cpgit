using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DebtPlus.Letters.Compose;
using DebtPlus.UI.Desktop.CS.Counselor.Reassign.Reports;

namespace DebtPlus.UI.Desktop.CS.Counselor.Reassign
{
    internal partial class Form1
    {
        private DataRow CurrentRow;
        private const string ClientsTableName = "clients";
        private const string TicklersTableName = "ticklers";
        public ArgParser ap;

        private string OldCounselor;

        public Form1() : base()
        {
            InitializeComponent();
        }

        public Form1(ArgParser ap) : this()
        {
            this.ap = ap;

            SimpleButton2.Click += SimpleButton2_Click;
            this.Load += Form1_Load;
            LookUpEdit_OldCounselor.EditValueChanged += LookUpEdit_OldCounselor_EditValueChanged;
            GridView1.CellValueChanged += GridView1_CellValueChanged;
            SimpleButton1.Click += SimpleButton1_Click;
            BarButtonItem1.ItemClick += BarButtonItem1_ItemClick;
            SimpleButton1.EnabledChanged += SimpleButton1_EnabledChanged;
            BarButtonItem2.ItemClick += BarButtonItem2_ItemClick;
        }

        private void SimpleButton2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Set the current list of counselors into the "old" counselor and clear the list of new counselors
            List<ArgParser.NewCounselorListClass> lst = ap.CounselorsList();
            LookUpEdit_OldCounselor.Properties.DataSource = lst;
            GridControl1.DataSource = null;

            // Set or clear the check-box for active items only
            CheckEdit_ReassignActiveOnly.Checked = ap.DefaultActive;

            // Enable or disable the letter generation
            if (ap.LetterCode != string.Empty)
            {
                CheckEdit_GenerateLetters.Checked = true;
                CheckEdit_GenerateLetters.Enabled = true;
            }
            else
            {
                CheckEdit_GenerateLetters.Checked = false;
                CheckEdit_GenerateLetters.Enabled = false;
            }
        }

        private void LookUpEdit_OldCounselor_EditValueChanged(object sender, EventArgs e)
        {
            if (LookUpEdit_OldCounselor.EditValue != null && !object.ReferenceEquals(LookUpEdit_OldCounselor.EditValue, DBNull.Value))
            {
                // Generate the list of new names. But, we need to filter out the current item from the list.
                List<ArgParser.NewCounselorListClass> lst = ap.CounselorsList();
                GridControl1.DataSource = lst;

                GridView1.ActiveFilter.Clear();
                GridView1.ActiveFilterString = string.Format("[Counselor]<>{0:f0}", LookUpEdit_OldCounselor.EditValue);

                // Set the name of the previous counselor
                OldCounselor = LookUpEdit_OldCounselor.Text;

                // Reset the percentages for the various counselors
                foreach (ArgParser.NewCounselorListClass item in lst)
                {
                    item.Percent = 0;
                    item.ClientsProcessed = 0;
                    item.ClientsToProcess = 0;
                }
            }
            else
            {
                GridControl1.DataSource = null;
                OldCounselor = string.Empty;
            }

            // Always disable the OK button.
            SimpleButton1.Enabled = false;
        }

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            Int32 TotalPercent = 0;

            // Calculate the total for the percentages
            List<ArgParser.NewCounselorListClass> lst = GridControl1.DataSource as List<ArgParser.NewCounselorListClass>;
            if (lst != null)
            {
                foreach (ArgParser.NewCounselorListClass item in lst)
                {
                    TotalPercent += item.Percent;
                }
            }

            // The percentage must equal 100 to be valid
            SimpleButton1.Enabled = (TotalPercent == 100);
        }

        private void SimpleButton1_Click(object sender, EventArgs e)
        {
            bool ActiveOnly = CheckEdit_ReassignActiveOnly.Checked;
            bool WantLetters = CheckEdit_GenerateLetters.Checked;

            List<ArgParser.NewCounselorListClass> lst = GridControl1.DataSource as List<ArgParser.NewCounselorListClass>;

            if (lst != null)
            {
                // Retrieve a list of the clients corresponding to the old counselor and active status
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                System.Data.SqlClient.SqlTransaction txn = null;
                try
                {
                    cn.Open();
                    txn = cn.BeginTransaction(IsolationLevel.RepeatableRead, "Reassign Counselors");

                    // Read the clients that we wish to process
                    ReadClients(ref lst, cn, txn, ActiveOnly);

                    // Update the clients with the new counselors
                    UpdateClients(cn, txn);

                    // Commit the data to the database
                    txn.Commit();
                    txn = null;

                    // Close the database connection object as well since we don't need it.
                    cn.Close();
                    cn.Dispose();
                    cn = null;

                    // Start to send letters to the clients.
                    System.Threading.Thread LettersThread = null;
                    if (WantLetters)
                    {
                        LettersThread = GenerateLetters();
                    }
                    else
                    {
                        LettersThread = null;
                    }

                    // Generate the report showing what we did
                    System.Threading.Thread ReportThread = PrintReport();
                    if (ReportThread != null && ReportThread.IsAlive)
                    {
                        ReportThread.Join();
                    }

                    // If the letters are still being processed, wait for them.
                    if (LettersThread != null && LettersThread.IsAlive)
                    {
                        LettersThread.Join();
                    }

                    // Reset the form for the next reassignment
                    LookUpEdit_OldCounselor.EditValue = null;
                    GridControl1.DataSource = null;
                }
                catch (Exception ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error processing reassignment");
                }
                finally
                {
                    if (txn != null)
                    {
                        txn.Rollback();
                        txn = null;
                    }

                    if (cn != null)
                        cn.Dispose();
                }
            }
        }

        private void ReadClients(ref List<ArgParser.NewCounselorListClass> lst, System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn, bool ActiveOnly)
        {
            // Toss the current list of clients from the data space
            DataTable tbl = ap.ds.Tables[ClientsTableName];
            if (tbl != null)
            {
                ap.ds.Tables.Remove(tbl);
            }

            tbl = ap.ds.Tables[TicklersTableName];
            if (tbl != null)
            {
                ap.ds.Tables.Remove(tbl);
            }

            // Retrieve the clients
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with1 = cmd;
                _with1.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                _with1.Connection = cn;
                _with1.Transaction = txn;
                _with1.CommandText = "SELECT c.[client], c.[counselor] as old_counselor, c.[start_date], convert(int,null) as new_counselor, convert(varchar(80),null) as new_counselor_name, dbo.format_normal_name(pn.[prefix],pn.[first],pn.[middle],pn.[last],pn.[suffix]) as client_name, c.[active_status] FROM [clients] c LEFT OUTER JOIN [people] p ON c.[client] = p.[client] AND 1 = p.[relation] LEFT OUTER JOIN [names] pn ON p.[NameID] = pn.[Name] WHERE c.[counselor] = @counselor";
                if (ActiveOnly)
                {
                    _with1.CommandText += " AND c.[active_status] IN ('A','AR')";
                }
                _with1.Parameters.Add("@counselor", SqlDbType.Int).Value = LookUpEdit_OldCounselor.EditValue;

                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                {
                    da.Fill(ap.ds, ClientsTableName);
                }
            }

            // Retrieve the ticklers
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with2 = cmd;
                _with2.CommandTimeout = System.Math.Max(600, DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout);
                _with2.Connection = cn;
                _with2.Transaction = txn;
                _with2.CommandText = "SELECT t.[tickler], c.[client] as client, c.[active_status], t.[counselor] as old_counselor, convert(int,null) as new_counselor FROM [ticklers] t INNER JOIN [clients] c ON t.[client] = c.[client] WHERE t.[counselor] = @counselor";
                if (ActiveOnly)
                {
                    _with2.CommandText += " AND c.[active_status] IN ('A','AR')";
                }
                _with2.Parameters.Add("@counselor", SqlDbType.Int).Value = LookUpEdit_OldCounselor.EditValue;

                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                {
                    da.Fill(ap.ds, TicklersTableName);
                }
            }

            // Determine the number of clients to be assigned to each counselor
            Int32 TotalClientsToAssign = ap.ds.Tables[ClientsTableName].Rows.Count;

            foreach (ArgParser.NewCounselorListClass item in lst)
            {
                item.ClientsProcessed = 0;
                item.ClientsToProcess = (TotalClientsToAssign * item.Percent) / 100;

                // Do not allow conditions where the numbers are just too small to handle.
                // If there are only 5 clients then anything less than 20% is simply 0 so make it 1.
                if (item.Percent > 0 && item.ClientsToProcess < 1)
                {
                    item.ClientsToProcess = 1;
                }
            }

            // Now, scan the counselors and assign the new items to the clients
            Int32 CurrentCounselor = 0;
            Int32 OverflowCounselor = 0;

            foreach (DataRow row in ap.ds.Tables[ClientsTableName].Rows)
            {
                Int32 Client = Convert.ToInt32(row["client"]);

                // Find the counselor in a round-robbin affair so that the clients are distributed
                // according to the percentages desired.
                ArgParser.NewCounselorListClass FoundCounselor = FindNormalCounselor(ref lst, ref CurrentCounselor);
                if (FoundCounselor == null)
                {
                    CurrentCounselor = 0;
                    FoundCounselor = FindNormalCounselor(ref lst, ref CurrentCounselor);
                }

                // If we have not found the counselor normally, take the first one that has a spot.
                if (FoundCounselor == null)
                {
                    FoundCounselor = FindOverflowCounselor(ref lst, ref OverflowCounselor);
                    if (FoundCounselor == null)
                    {
                        OverflowCounselor = 0;
                        FoundCounselor = FindOverflowCounselor(ref lst, ref OverflowCounselor);
                    }
                }

                // Update the client with the new information
                if (FoundCounselor != null)
                {
                    FoundCounselor.ClientsProcessed += 1;
                    row.BeginEdit();
                    row["new_counselor"] = FoundCounselor.Counselor;
                    row["new_counselor_name"] = FoundCounselor.Name;
                    row.EndEdit();

                    // Update the ticklers for that client as well
                    using (System.Data.DataView vue = new System.Data.DataView(ap.ds.Tables[TicklersTableName], string.Format("[client]={0:f0}", Client), string.Empty, DataViewRowState.CurrentRows))
                    {
                        foreach (DataRowView drv in vue)
                        {
                            drv.BeginEdit();
                            drv["new_counselor"] = FoundCounselor.Counselor;
                            drv.EndEdit();
                        }
                    }
                }
            }
        }

        private void UpdateClients(System.Data.SqlClient.SqlConnection cn, System.Data.SqlClient.SqlTransaction txn)
        {
            // Update the counselor information accordingly
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with3 = cmd;
                _with3.Connection = cn;
                _with3.Transaction = txn;
                _with3.CommandText = "UPDATE clients SET [counselor]=@new_counselor";

                if (ap.ReassignOffice)
                {
                    _with3.CommandText += ",[office]=co.office";
                }

                _with3.CommandText += " FROM clients c WITH (NOLOCK)";

                if (ap.ReassignOffice)
                {
                    _with3.CommandText += " LEFT OUTER JOIN counselors co WITH (NOLOCK) ON co.counselor=@new_counselor";
                }

                _with3.CommandText += " WHERE c.[client]=@client";

                _with3.CommandType = CommandType.Text;
                _with3.Parameters.Add("@new_counselor", SqlDbType.Int, 0, "new_counselor");
                _with3.Parameters.Add("@client", SqlDbType.Int, 0, "client");

                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                {
                    da.UpdateCommand = cmd;
                    da.Update(ap.ds.Tables[ClientsTableName]);
                }
            }

            // Update the counselor information accordingly
            using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
            {
                var _with4 = cmd;
                _with4.Connection = cn;
                _with4.Transaction = txn;
                _with4.CommandText = "UPDATE [ticklers] SET [counselor]=@new_counselor WHERE [tickler]=@tickler";
                _with4.CommandType = CommandType.Text;
                _with4.Parameters.Add("@new_counselor", SqlDbType.Int, 0, "new_counselor");
                _with4.Parameters.Add("@tickler", SqlDbType.Int, 0, "tickler");

                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                {
                    da.UpdateCommand = cmd;
                    da.Update(ap.ds.Tables[TicklersTableName]);
                }
            }
        }

        private static ArgParser.NewCounselorListClass FindNormalCounselor(ref List<ArgParser.NewCounselorListClass> lst, ref Int32 CounselorNumber)
        {
            while (CounselorNumber < lst.Count)
            {
                ArgParser.NewCounselorListClass item = lst[CounselorNumber];
                CounselorNumber += 1;
                if (item.ClientsToProcess > item.ClientsProcessed)
                {
                    return item;
                }
            }

            return null;
        }

        private static ArgParser.NewCounselorListClass FindOverflowCounselor(ref List<ArgParser.NewCounselorListClass> lst, ref Int32 CounselorNumber)
        {
            while (CounselorNumber < lst.Count)
            {
                ArgParser.NewCounselorListClass item = lst[CounselorNumber];
                CounselorNumber += 1;
                if (item.Percent > 0)
                {
                    return item;
                }
            }

            return null;
        }

        private void BarButtonItem1_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            bool ActiveOnly = CheckEdit_ReassignActiveOnly.Checked;

            List<ArgParser.NewCounselorListClass> lst = GridControl1.DataSource as List<ArgParser.NewCounselorListClass>;

            if (lst != null)
            {
                // Retrieve a list of the clients corresponding to the old counselor and active status
                using (var cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString))
                {
                    try
                    {
                        cn.Open();
                        ReadClients(ref lst, cn, null, ActiveOnly);
                        PrintReport();
                    }
                    catch { }
                }
            }
        }

        private System.Threading.Thread PrintReport()
        {
            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ThreadStart(PrintReportThread));
            var _with5 = thrd;
            _with5.SetApartmentState(System.Threading.ApartmentState.STA);
            _with5.Name = "ReportThread";
            _with5.Start();

            return thrd;
        }

        private void PrintReportThread()
        {
            // Generate the report and the dataset for the report.
            using (ReassignmentReport rpt = new ReassignmentReport())
            {
                rpt.DataSource = new DataView(ap.ds.Tables[ClientsTableName], string.Empty, "new_counselor_name, client", DataViewRowState.CurrentRows);

                // Print the report only if there is data to printReport

                if (ap.ds.Tables[ClientsTableName].Rows.Count > 0)
                {
                    // Set the report subtitle and if there are any calculated fields, update them.
                    rpt.Subtitle = string.Format("Replacements for counselor '{0}'", OldCounselor);
                    foreach (DevExpress.XtraReports.UI.CalculatedField calc in rpt.CalculatedFields)
                    {
                        calc.Assign(rpt.DataSource, rpt.DataMember);
                    }

                    // Generate the report preview window with the current report and show it.
                    using (DebtPlus.Reports.Template.PrintPreviewForm frm = new DebtPlus.Reports.Template.PrintPreviewForm(rpt))
                    {
                        frm.ShowDialog();
                    }
                }
            }
        }

        private System.Threading.Thread GenerateLetters()
        {
            System.Threading.Thread thrd = new System.Threading.Thread(new System.Threading.ThreadStart(GenerateLettersThread));
            var _with6 = thrd;
            _with6.SetApartmentState(System.Threading.ApartmentState.STA);
            _with6.Name = "LettersThread";
            _with6.Start();

            return thrd;
        }

        private void GenerateLettersThread()
        {
            if (ap.LetterCode != string.Empty)
            {
                foreach (System.Data.DataRow CurrentRowLoop_loopVariable in ap.ds.Tables[ClientsTableName].Rows)
                {
                    CurrentRow = CurrentRowLoop_loopVariable;

                    using (Composition ltr = new Composition(ap.LetterCode))
                    {
                        ltr.GetValue += LetterCallback;
                        ltr.PrintLetter(false, true);
                        ltr.GetValue -= LetterCallback;
                    }
                }
            }
        }

        private void LetterCallback(object Sender, DebtPlus.Events.ParameterValueEventArgs e)
        {
            switch (e.Name.ToLower())
            {
                case "clients.previous_counselor":
                    e.Value = OldCounselor;
                    break;

                case "counselor_name":
                case "counselor":
                case "clients.counselor":
                    e.Value = CurrentRow["new_counselor_name"];
                    break;

                case "client":
                    e.Value = CurrentRow["client"];
                    break;

                case "client_name":
                case "name":
                    e.Value = CurrentRow["client_name"];
                    break;

                case "start_date":
                    e.Value = CurrentRow["start_date"];
                    break;

                case "active_status":
                    e.Value = CurrentRow["active_status"];
                    break;

                default:
                    break;
            }
        }

        private void BarButtonItem2_ItemClick(System.Object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void SimpleButton1_EnabledChanged(object sender, System.EventArgs e)
        {
            BarButtonItem1.Enabled = SimpleButton1.Enabled;
        }
    }
}