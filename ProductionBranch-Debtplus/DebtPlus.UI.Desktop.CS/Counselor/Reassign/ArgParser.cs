using System;

namespace DebtPlus.UI.Desktop.CS.Counselor.Reassign
{
    internal partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        private bool privateDefaultActive;

        public bool DefaultActive
        {
            get { return privateDefaultActive; }
            set { privateDefaultActive = value; }
        }

        private bool privateReassignOffice;

        public bool ReassignOffice
        {
            get { return privateReassignOffice; }
            private set { privateReassignOffice = value; }
        }

        private string privateLetterCode;

        public string LetterCode
        {
            get { return privateLetterCode; }
            private set { privateLetterCode = value; }
        }

        protected override SwitchStatus OnSwitch(string switchSymbol, string switchValue)
        {
            switch (switchSymbol.ToLower())
            {
                case "l":
                    LetterCode = switchValue;
                    break;

                case "a":
                    DefaultActive = !DefaultActive;
                    break;

                default:
                    return base.OnSwitch(switchSymbol, switchValue);
            }

            return global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
        }

        /// <summary>
        /// Create an instance of our class
        /// </summary>
        public ArgParser() : base(new string[] {
            "l",
            "a"
        })
        {
            // Find the defaults for the items from the app.config file
            LetterCode = global::DebtPlus.Utils.Nulls.DStr(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CounselorReassign), "LetterType"));
            Boolean tempDefaultActive = false;
            string settingsDefaultActive = global::DebtPlus.Utils.Nulls.DStr(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CounselorReassign), "DefaultActive"));
            if (!bool.TryParse(settingsDefaultActive, out tempDefaultActive))
            {
                tempDefaultActive = false;
            }
            DefaultActive = tempDefaultActive;

            string settingsReassignOffice = global::DebtPlus.Utils.Nulls.DStr(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.CounselorReassign), "ReassignOffice"));
            bool tempReasssignOfffice = false;
            if (!bool.TryParse(settingsReassignOffice, out tempReasssignOfffice))
            {
                tempReasssignOfffice = false;
            }
            ReassignOffice = tempReasssignOfffice;
        }

        /// <summary>
        /// Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}