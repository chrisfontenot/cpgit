using System;
using System.Collections.Generic;
using System.Data;

#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System.ComponentModel;
using DebtPlus.Utils;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Drawing;

namespace DebtPlus.UI.Desktop.CS.Counselor.Reassign
{
    partial class ArgParser
    {
        /// <summary>
        /// Table names in the dataset
        /// </summary>
        public partial class Names
        {
            public const string action_items_goals = "action_items_goals";
            public const string action_plans = "action_plans";
            public const string active_statuses = "active_statuses";
            public const string addresses = "Addresses";
            public const string asset_ids = "asset_ids";
            public const string assets = "assets";
            public const string appointment_attributes = "appointment_attributes";
            public const string attributetypes = "AttributeTypes";
            public const string balance_verification_status = "BalanceVerificationStatusTypes";
            public const string budgets = "budgets";
            public const string calendar = "Calendar";
            public const string CounselorAttributes = "CounselorAttributes";
            public const string client_addkeys = "client_addkeys";
            public const string client_deposits = "client_deposits";
            public const string client_other_debts = "client_other_debts";
            public const string client_retention_events = "client_retention_events";
            public const string client_retention_actions = "client_retention_actions";
            public const string client_status = "client_status";
            public const string client_ach = "client_ach";
            public const string client_www = "client_www";
            public const string client_www_notes = "client_www_notes";
            public const string clients = "clients";
            public const string config = "config";
            public const string contribution_cycles = "contribution_cycles";
            public const string Counselors = "Counselors";
            public const string counselor_attributes = "counselor_attributes";
            public const string counties = "counties";
            public const string countries = "countries";
            public const string creditagency = "CreditAgency";
            public const string creditor_classes = "creditor_classes";
            public const string disbursement_creditor_note_types = "disbursement_creditor_note_types";
            public const string creditors = "creditors";
            public const string drop_reasons = "Drop_Reasons";
            public const string education = "education";
            public const string employers = "employers";
            public const string Ethnicity = "Ethnicity";
            public const string financial_problems = "financial_problems";
            public const string gender = "gender";
            public const string client_housing = "client_housing";
            public const string HousingDelinquency_types = "HousingDelinquency_types";
            public const string Housing_FICONotIncluded = "Housing_FICONotIncluded";
            public const string Housing_FinancingTypes = "Housing_FinancingTypes";
            public const string housing_properties = "housing_properties";
            public const string housing_borrowers = "housing_borrowers";
            public const string housing_loans = "housing_loans";
            public const string housing_LoanTypes = "housing_LoanTypes";
            public const string housing_loan_details = "housing_loan_details";
            public const string housing_lenders = "housing_lenders";
            public const string housing_lender_servicers = "housing_lender_servicers";
            public const string housing_MortgageTypes = "housing_MortgageTypes";
            public const string housing_interviewAddress = "Housing_InterviewAddress";
            public const string housing_grants = "housing_grants";
            public const string housing_types = "HousingTypes";
            public const string hud_interview_types = "hud_interview_types";
            public const string hud_results = "hud_results";
            public const string hud_termination_reasons = "hud_termination_reasons";
            public const string hud_interviews = "hud_interviews";
            public const string hud_transactions = "hud_transactions";
            public const string indicators = "indicators";
            public const string industries = "industries";
            public const string job_descriptions = "job_descriptions";
            public const string marital_types = "MaritalTypes";
            public const string messages = "messages";
            public const string militaryservice = "MilitaryService";
            public const string names = "Names";
            public const string offices = "offices";
            public const string people = "people";
            public const string proposal_batch_ids = "proposal_batch_ids";
            public const string proposal_messages = "proposal_messages";
            public const string proposal_status = "proposal_status";
            public const string proposal_reject_dispositions = "proposal_reject_dispositions";
            public const string proposal_reject_reasons = "proposal_reject_reasons";
            public const string referred_By = "Referred_By";
            public const string referred_to = "lst_referred_to";
            public const string regions = "Regions";
            public const string registers_client_creditor = "registers_client_creditor";
            public const string retention_actions = "retention_actions";
            public const string retention_events = "retention_events";
            public const string sales_files = "sales_files";
            public const string secured_properties = "secured_properties";
            public const string secured_types = "secured_types";
            public const string states = "states";
            public const string telephonenumbers = "TelephoneNumbers";
            public const string WorkshopLocations = "workshop_locations";
        }

        /// <summary>
        /// Known attributes in the counselor_attributes table
        /// </summary>
        public System.Data.DataTable AttributeTypesTable()
        {
            System.Data.DataTable tbl = ds.Tables[Names.attributetypes];

            // If there is a table then look for a match. If found, return it.

            if (tbl == null)
            {
                // Read the information from the system for the indicated name
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                try
                {
                    cn.Open();
                    lock (ds)
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with1 = cmd;
                            _with1.Connection = cn;
                            _with1.CommandText = "SELECT [oID], [Grouping], [Attribute], [Default], [ActiveFlag] FROM AttributeTypes WITH (NOLOCK)";
                            _with1.CommandType = CommandType.Text;

                            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.FillLoadOption = LoadOption.OverwriteChanges;
                                da.Fill(ds, Names.attributetypes);
                            }
                        }

                        tbl = ds.Tables[Names.attributetypes];
                        if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                            tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["oID"] };
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    if (cn != null)
                        cn.Dispose();
                }
            }

            return tbl;
        }

        /// <summary>
        /// Known attributes in the counselor_attributes table
        /// </summary>
        public string GetAttributeTypeDescription(object value)
        {
            string answer = string.Empty;

            if (value != null && !object.ReferenceEquals(value, System.DBNull.Value))
            {
                System.Data.DataTable tbl = AttributeTypesTable();

                if (tbl != null)
                {
                    System.Data.DataRow row = tbl.Rows.Find(value);

                    if (row != null)
                    {
                        if (row["Attribute"] != null && !object.ReferenceEquals(row["Attribute"], System.DBNull.Value))
                        {
                            answer = Convert.ToString(row["Attribute"]);
                        }
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// Find the specific row for the NAMEID
        /// </summary>
        public DataRow GetNameRowByID(Int32 NameID)
        {
            DataTable tbl = NamesTable(NameID);

            DataRow answer = null;
            if (tbl != null)
            {
                answer = tbl.Rows.Find(NameID);
            }
            else
            {
                answer = null;
            }

            return answer;
        }

        /// <summary>
        /// Name table information
        /// </summary>
        public DataTable NamesTable(Int32 NameID)
        {
            // If the name row is already loaded then don't read it again.
            DataTable tbl = ds.Tables[Names.names];
            if (tbl != null)
            {
                if (tbl.Rows.Find(NameID) != null)
                {
                    return tbl;
                }
            }

            GetNameRowsRow(string.Format(" WHERE [Name]={0:f0}", NameID));
            return ds.Tables[Names.names];
        }

        private void GetNameRowsRow(string SelectClause)
        {
            DataTable tbl = ds.Tables[Names.names];

            // Read the name from the system
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with2 = cmd;
                    _with2.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with2.CommandText = string.Format("SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM Names WITH(NOLOCK){0}", SelectClause);
                    _with2.CommandType = CommandType.Text;

                    // Ask the database for the information
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.FillLoadOption = LoadOption.PreserveChanges;
                        da.Fill(ds, Names.names);
                    }
                }

                tbl = ds.Tables[Names.names];
                if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                {
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["Name"] };
                    var _with3 = tbl.Columns["Name"];
                    _with3.AutoIncrement = true;
                    _with3.AutoIncrementSeed = -1;
                    _with3.AutoIncrementStep = -1;
                }

                // When we do an update, the update results in zero rows being updated. IGNORE IT.
            }
            catch (DBConcurrencyException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                // Something happend in trying to talk to the database.
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                Cursor.Current = current_cursor;
            }
        }

        public DataTable NamesSelectSchema()
        {
            DataTable tbl = ds.Tables[Names.names];

            // Read the schema name from the system
            Cursor current_cursor = Cursor.Current;
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                {
                    var _with4 = cmd;
                    _with4.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    _with4.CommandText = "SELECT [Name],[prefix],[first],[middle],[last],[suffix] FROM Names WITH(NOLOCK)";
                    _with4.CommandType = CommandType.Text;

                    // Ask the database for the information
                    using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                    {
                        da.FillSchema(ds, SchemaType.Source, Names.names);
                    }
                }

                tbl = ds.Tables[Names.names];
                if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                {
                    tbl.PrimaryKey = new DataColumn[] { tbl.Columns["Name"] };
                    var _with5 = tbl.Columns["Name"];
                    _with5.AutoIncrement = true;
                    _with5.AutoIncrementSeed = -1;
                    _with5.AutoIncrementStep = -1;
                }

                // When we do an update, the update results in zero rows being updated. IGNORE IT.
            }
            catch (DBConcurrencyException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);

                // Something happend in trying to talk to the database.
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
            }
            finally
            {
                Cursor.Current = current_cursor;
            }

            return tbl;
        }

        public void UpdateNamesTable()
        {
            DataTable tbl = ds.Tables[Names.names];

            System.Data.SqlClient.SqlCommand UpdateCmd = new SqlCommand();
            System.Data.SqlClient.SqlCommand InsertCmd = new SqlCommand();
            System.Data.SqlClient.SqlCommand DeleteCmd = new SqlCommand();
            System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

            try
            {
                var _with6 = UpdateCmd;
                _with6.Connection = cn;
                _with6.CommandText = "UPDATE Names SET [prefix]=@prefix, [first]=@first, [middle]=@middle, [last]=@last, [suffix]=@suffix WHERE [Name]=@Name";
                _with6.CommandType = CommandType.Text;
                _with6.Parameters.Add("@prefix", SqlDbType.VarChar, 80, "prefix");
                _with6.Parameters.Add("@first", SqlDbType.VarChar, 80, "first");
                _with6.Parameters.Add("@middle", SqlDbType.VarChar, 80, "middle");
                _with6.Parameters.Add("@last", SqlDbType.VarChar, 80, "last");
                _with6.Parameters.Add("@suffix", SqlDbType.VarChar, 80, "suffix");
                _with6.Parameters.Add("@Name", SqlDbType.Int, 0, "Name");

                var _with7 = InsertCmd;
                _with7.Connection = cn;
                _with7.CommandText = "xpr_insert_names";
                _with7.CommandType = CommandType.StoredProcedure;
                _with7.Parameters.Add("@prefix", SqlDbType.VarChar, 80, "prefix");
                _with7.Parameters.Add("@first", SqlDbType.VarChar, 80, "first");
                _with7.Parameters.Add("@middle", SqlDbType.VarChar, 80, "middle");
                _with7.Parameters.Add("@last", SqlDbType.VarChar, 80, "last");
                _with7.Parameters.Add("@suffix", SqlDbType.VarChar, 80, "suffix");
                _with7.Parameters.Add("@RETURN_VALUE", SqlDbType.Int, 0, "Name").Direction = ParameterDirection.ReturnValue;

                var _with8 = DeleteCmd;
                _with8.Connection = cn;
                _with8.CommandText = "DELETE FROM Names WHERE [Name]=@Name";
                _with8.CommandType = CommandType.Text;
                _with8.Parameters.Add("@Name", SqlDbType.Int, 0, "Name");

                Int32 RowsUpdated = default(Int32);
                using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter())
                {
                    var _with9 = da;
                    _with9.UpdateCommand = UpdateCmd;
                    _with9.InsertCommand = InsertCmd;
                    _with9.DeleteCommand = DeleteCmd;
                    _with9.AcceptChangesDuringUpdate = true;
                    RowsUpdated = _with9.Update(tbl);
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error updating Names table");
            }
            finally
            {
                UpdateCmd.Dispose();
                InsertCmd.Dispose();
                DeleteCmd.Dispose();
                cn.Dispose();
            }
        }

        public string FormattedNameByID(System.Int32 NameID)
        {
            System.Data.DataRow row = GetNameRowByID(NameID);
            // GetNameRowByID(NameID)
            string answer = string.Empty;
            if (row != null)
            {
                answer = DebtPlus.LINQ.Name.FormatNormalName(row["prefix"], row["first"], row["middle"], row["last"], row["suffix"]);
            }
            return answer;
        }

        public string FormatReverseNameByID(System.Int32 NameID)
        {
            System.Data.DataRow row = GetNameRowByID(NameID);
            // GetNameRowByID(NameID)
            if (row != null)
            {
                return DebtPlus.LINQ.Name.FormatReverseName(row["prefix"], row["first"], row["middle"], row["last"], row["suffix"]);
            }
            return string.Empty;
        }

        public partial class CounselorListClass : INotifyPropertyChanged, IComparable, IDisposable
        {
            public CounselorListClass() : base()
            {
            }

            private Int32 privateNameID;
            private Int32 privateOffice;
            private Int32 privateTelephoneID;
            private Int32 privateEmailID;
            private Image privateImage;
            private Int32 privateCounselor;
            private string privateName;
            private string privatePerson;
            private string privateSSN;
            private object privateEmpStartDate;
            private object privateEmpEndDate;
            private object privateNote;
            private Int32 privateHUD_ID;
            private bool privateDefault;

            private bool privateActiveFlag;

            public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, e);
                }
            }

            protected void RaisePropertyChanged(PropertyChangedEventArgs e)
            {
                OnPropertyChanged(e);
            }

            protected void RaisePropertyChanged(string PropertyName)
            {
                RaisePropertyChanged(new PropertyChangedEventArgs(PropertyName));
            }

            private Int32 privateColor;

            public Int32 Color
            {
                get
                {
                    if (privateColor == 0)
                    {
                        return System.Drawing.Color.FromKnownColor(KnownColor.Window).ToArgb();
                    }

                    return privateColor;
                }
                set
                {
                    privateColor = value;
                    RaisePropertyChanged("Color");
                }
            }

            public Int32 NameID
            {
                get { return privateNameID; }
                set
                {
                    privateNameID = value;
                    RaisePropertyChanged("NameID");
                }
            }

            public Int32 Office
            {
                get { return privateOffice; }
                set
                {
                    privateOffice = value;
                    RaisePropertyChanged("Office");
                }
            }

            public Int32 TelephoneID
            {
                get { return privateTelephoneID; }
                set
                {
                    privateTelephoneID = value;
                    RaisePropertyChanged("TelephoneID");
                }
            }

            public Int32 EmailID
            {
                get { return privateEmailID; }
                set
                {
                    privateEmailID = value;
                    RaisePropertyChanged("EmailID");
                }
            }

            public Image Image
            {
                get { return privateImage; }
                set
                {
                    if (privateImage != null)
                    {
                        privateImage.Dispose();
                    }

                    privateImage = value;
                    RaisePropertyChanged("Image");
                }
            }

            public Int32 Counselor
            {
                get { return privateCounselor; }
                set
                {
                    privateCounselor = value;
                    RaisePropertyChanged("Counselor");
                }
            }

            public string Name
            {
                get { return privateName; }
                set
                {
                    privateName = value;
                    RaisePropertyChanged("Name");
                }
            }

            public string Person
            {
                get { return privatePerson; }
                set
                {
                    privatePerson = value;
                    RaisePropertyChanged("Person");
                }
            }

            public string SSN
            {
                get { return privateSSN; }
                set
                {
                    privateSSN = value;
                    RaisePropertyChanged("SSN");
                }
            }

            public object EmpStartDate
            {
                get { return privateEmpStartDate; }
                set
                {
                    privateEmpStartDate = value;
                    RaisePropertyChanged("EmpStartDate");
                }
            }

            public object EmpEndDate
            {
                get { return privateEmpEndDate; }
                set
                {
                    privateEmpEndDate = value;
                    RaisePropertyChanged("EmpEndDate");
                }
            }

            public object Note
            {
                get { return privateNote; }
                set
                {
                    privateNote = value;
                    RaisePropertyChanged("Note");
                }
            }

            public Int32 HUD_ID
            {
                get { return privateHUD_ID; }
                set
                {
                    privateHUD_ID = value;
                    RaisePropertyChanged("HUD_ID");
                }
            }

            public bool Default
            {
                get { return privateDefault; }
                set
                {
                    privateDefault = value;
                    RaisePropertyChanged("Default");
                }
            }

            public bool ActiveFlag
            {
                get { return privateActiveFlag; }
                set
                {
                    privateActiveFlag = value;
                    RaisePropertyChanged("ActiveFlag");
                }
            }

            public override string ToString()
            {
                return Name;
            }

            public virtual Int32 CompareTo(object obj)
            {
                return Name.CompareTo(((CounselorListClass)obj).Name);
            }

            private static Color[] DefaultColor_DefaultColors = new Color[] {
                                System.Drawing.Color.LightGray,
                                System.Drawing.Color.Snow,
                                System.Drawing.Color.MistyRose,
                                System.Drawing.Color.LightSalmon,
                                System.Drawing.Color.SeaShell,
                                System.Drawing.Color.Bisque,
                                System.Drawing.Color.PeachPuff,
                                System.Drawing.Color.Tan,
                                System.Drawing.Color.NavajoWhite,
                                System.Drawing.Color.BlanchedAlmond,
                                System.Drawing.Color.LightGreen,
                                System.Drawing.Color.Aquamarine,
                                System.Drawing.Color.SkyBlue,
                                System.Drawing.Color.Plum,
                                System.Drawing.Color.Orchid
                            };

            public static Int32 DefaultColor(Int32 ItemIndex)
            {
                return DefaultColor_DefaultColors[ItemIndex % (DefaultColor_DefaultColors.GetUpperBound(0) + 1)].ToArgb();
            }

            #region " IDisposable Support "

            // To detect redundant calls
            private bool disposedValue;

            // IDisposable
            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    disposedValue = true;
                    if (disposing)
                    {
                        if (privateImage != null)
                        {
                            privateImage.Dispose();
                        }
                    }

                    // Release any large objects
                    privateImage = null;
                }
            }

            // This code added by Visual Basic to correctly implement the disposable pattern.
            public void Dispose()
            {
                // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            #endregion " IDisposable Support "
        }

        public partial class NewCounselorListClass : CounselorListClass
        {
            public NewCounselorListClass() : base()
            {
            }

            private Int32 privatePercent;

            /// <summary>
            /// The percentage of the old counselor's clients to be assigned to this counselor
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public Int32 Percent
            {
                get { return privatePercent; }
                set
                {
                    privatePercent = value;
                    RaisePropertyChanged("Percent");
                }
            }

            private Int32 privateClientsToProcess;

            /// <summary>
            /// The number of clients that we are to process for this counselor based upon the percentage
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public Int32 ClientsToProcess
            {
                get { return privateClientsToProcess; }
                set
                {
                    privateClientsToProcess = value;
                    RaisePropertyChanged("ClientsToProcess");
                }
            }

            private Int32 privateClientsProcessed;

            /// <summary>
            /// The number of items that we have processed already for this counselor
            /// </summary>
            /// <value></value>
            /// <returns></returns>
            /// <remarks></remarks>
            public Int32 ClientsProcessed
            {
                get { return privateClientsProcessed; }
                set
                {
                    privateClientsProcessed = value;
                    RaisePropertyChanged("ClientsProcessed");
                }
            }
        }

        private bool ReadCounselorNames_FristTimeOnly = true;

        private void ReadCounselorNames()
        {
            DataTable tbl = CounselorsTable();

            // Build a list of the NameID values from the counslors so that the names may be read all at once.
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (DataRow row in tbl.Rows)
            {
                Int32 NameID = global::DebtPlus.Utils.Nulls.DInt(row["NameID"]);
                if (NameID > 0)
                {
                    sb.AppendFormat(",{0:f0}", NameID);
                }
            }

            // Retrieve the names into the database for the counselors
            if (sb.Length > 0)
                sb.Remove(0, 1);
            if (sb.Length > 0)
            {
                sb.Insert(0, " WHERE [Name] IN (");
                sb.Append(")");
                GetNameRowsRow(sb.ToString());
            }
        }

        public List<NewCounselorListClass> CounselorsList()
        {
            return CounselorsList(DBNull.Value);
        }

        public List<NewCounselorListClass> CounselorsList(object CurrentCounselor)
        {
            if (ReadCounselorNames_FristTimeOnly)
            {
                ReadCounselorNames_FristTimeOnly = false;
                ReadCounselorNames();
            }

            return BuildCounselorList("COUNSELOR", CurrentCounselor);
        }

        public List<NewCounselorListClass> CSRList()
        {
            return CSRList(DBNull.Value);
        }

        public List<NewCounselorListClass> CSRList(object CurrentCSR)
        {
            if (ReadCounselorNames_FristTimeOnly)
            {
                ReadCounselorNames_FristTimeOnly = false;
                ReadCounselorNames();
            }

            return BuildCounselorList("CSR", CurrentCSR);
        }

        public List<NewCounselorListClass> EducatorList()
        {
            return EducatorList(DBNull.Value);
        }

        public List<NewCounselorListClass> EducatorList(object CurrentEducator)
        {
            if (ReadCounselorNames_FristTimeOnly)
            {
                ReadCounselorNames_FristTimeOnly = false;
                ReadCounselorNames();
            }

            return BuildCounselorList("EDUCATOR", CurrentEducator);
        }

        private List<NewCounselorListClass> BuildCounselorList(string RoleName, object CurrentItem)
        {
            List<NewCounselorListClass> lst = new List<NewCounselorListClass>();

            // Find the ROLE in the list of attributes for the system.
            DataTable RoleTable = AttributeTypesTable();
            Int32 RoleType = default(Int32);
            DataRow[] rows = RoleTable.Select(string.Format("[Grouping]='ROLE' AND [Attribute]='{0}'", RoleName));
            if (rows.GetUpperBound(0) >= 0)
            {
                RoleType = global::DebtPlus.Utils.Nulls.DInt(rows[0]["oID"]);
            }
            else
            {
                RoleType = -1;
            }

            // Read the list of counselor_attributes
            DataTable AttributesTable = CounselorAttributesTable();

            if ((AttributesTable != null) && RoleType > 0)
            {
                // Buld the list of counselors
                foreach (DataRowView drv in CounselorsTable().DefaultView)
                {
                    Int32 Counselor = global::DebtPlus.Utils.Nulls.DInt(drv["counselor"]);

                    if (Counselor > 0)
                    {
                        // If the counselor row is a member of the counselor role then include it here.
                        rows = AttributesTable.Select(string.Format("[counselor]={0:f0} AND [Attribute]={1:f0}", Counselor, RoleType));
                        if (rows.GetUpperBound(0) >= 0)
                        {
                            NewCounselorListClass item = new NewCounselorListClass();
                            var _with10 = item;
                            _with10.Counselor = Counselor;
                            _with10.EmailID = global::DebtPlus.Utils.Nulls.DInt(drv["EmailID"]);
                            _with10.NameID = global::DebtPlus.Utils.Nulls.DInt(drv["NameID"]);
                            _with10.Office = global::DebtPlus.Utils.Nulls.DInt(drv["Office"]);
                            _with10.TelephoneID = global::DebtPlus.Utils.Nulls.DInt(drv["TelephoneID"]);
                            _with10.Default = Nulls.DBool(drv["Default"]);
                            _with10.ActiveFlag = Nulls.DBool(drv["Activeflag"], true);
                            _with10.Person = global::DebtPlus.Utils.Nulls.DStr(drv["Person"]);
                            _with10.SSN = global::DebtPlus.Utils.Nulls.DStr(drv["ssn"]);
                            _with10.EmpStartDate = drv["emp_start_date"];
                            _with10.EmpEndDate = drv["emp_end_date"];
                            _with10.HUD_ID = global::DebtPlus.Utils.Nulls.DInt(drv["HUD_ID"]);
                            _with10.Note = drv["Note"];

                            if (!object.ReferenceEquals(drv["Image"], DBNull.Value))
                                _with10.Image = drv["Image"] as Bitmap;

                            // Ensure that the color is defined. The default is 0 which is black and that is black on black so it can't be seen.
                            Int32 CurrentColor = global::DebtPlus.Utils.Nulls.DInt(drv["Color"]);
                            if (CurrentColor == 0)
                            {
                                CurrentColor = CounselorListClass.DefaultColor(lst.Count);
                            }
                            _with10.Color = CurrentColor;

                            // Read the name from the system
                            DataRow NameRow = GetNameRowByID(_with10.NameID);
                            if ((NameRow != null))
                            {
                                _with10.Name = DebtPlus.LINQ.Name.FormatNormalName(null, NameRow["first"], null, NameRow["last"], null);
                                //.Name = Format.Names.FormatReverseName(Nothing, NameRow("first"), Nothing, NameRow("last"), Nothing)
                            }

                            // Do not include items that are inactive unless they are a match to the current value.
                            bool ValidCounselor = item.ActiveFlag;
                            if (!ValidCounselor)
                            {
                                if (CurrentItem != null && !object.ReferenceEquals(CurrentItem, DBNull.Value))
                                {
                                    if (Convert.ToInt32(CurrentItem) == Counselor)
                                        ValidCounselor = true;
                                }
                            }

                            if (ValidCounselor)
                            {
                                lst.Add(item);
                            }
                        }
                    }
                }

                // Put the items in order by name
                lst.Sort();
            }

            return lst;
        }

        /// <summary>
        /// Determine if the counselor has a specified attribute value
        /// </summary>
        public bool CounselorHasAttribute(Int32 CounselorID, Int32 AttributeID)
        {
            DataTable tbl = CounselorAttributesTable();
            DataRow[] rows = tbl.Select(string.Format("[Counselor]={0:f0} AND [Attribute]={1:f0}", CounselorID, AttributeID));
            return rows.GetUpperBound(0) >= 0;
        }

        /// <summary>
        /// Counselors (Employees) table
        /// </summary>
        public DataTable CounselorsTable()
        {
            DataTable tbl = ds.Tables[Names.Counselors];

            // If the table is not found then read the information from the database
            if (tbl == null)
            {
                Cursor current_cursor = Cursor.Current;
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    lock (ds)
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with11 = cmd;
                            _with11.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                            _with11.CommandText = "SELECT [Counselor],[Person],[Office],[Default],[ActiveFlag],[NameID],[Menu_level],[TelephoneID],[EmailID],[Color],[Image],[SSN],[emp_start_date],[emp_end_date],[HUD_ID],[Note] FROM counselors WITH (NOLOCK)";
                            _with11.CommandType = CommandType.Text;

                            // Ask the database for the information
                            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.FillLoadOption = LoadOption.OverwriteChanges;
                                da.Fill(ds, Names.Counselors);
                            }
                        }

                        tbl = ds.Tables[Names.Counselors];
                        var _with12 = tbl;
                        if (_with12.PrimaryKey.GetUpperBound(0) < 0)
                            _with12.PrimaryKey = new DataColumn[] { _with12.Columns["Counselor"] };
                        var _with13 = _with12.Columns["Counselor"];
                        _with13.AutoIncrement = true;
                        _with13.AutoIncrementSeed = -1;
                        _with13.AutoIncrementStep = -1;
                    }

                    // When we do an update, the update results in zero rows being updated. IGNORE IT.
                }
                catch (DBConcurrencyException ex)
                {
                    DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

                    // Something happend in trying to talk to the database.
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    Cursor.Current = current_cursor;
                }
            }

            return tbl;
        }

        /// <summary>
        /// Obtain a description for the item
        /// </summary>
        public string GetCounselorDescription(object key)
        {
            string answer = string.Empty;

            if (key != null && !object.ReferenceEquals(key, DBNull.Value))
            {
                DataTable tbl = CounselorsTable();
                if (tbl != null)
                {
                    DataRow row = tbl.Rows.Find(key);
                    if (row != null)
                    {
                        if (!object.ReferenceEquals(row["NameID"], DBNull.Value) && row["NameID"] != null)
                        {
                            Int32 NameID = Convert.ToInt32(row["NameID"]);
                            DataRow NameRow = GetNameRowByID(NameID);
                            if (NameRow != null)
                            {
                                answer = DebtPlus.LINQ.Name.FormatNormalName(null, NameRow["first"], null, NameRow["last"], null);
                            }
                        }
                    }
                }
            }

            return answer;
        }

        /// <summary>
        /// List of attribute_types for each counselor
        /// </summary>
        public System.Data.DataTable CounselorAttributesTable()
        {
            System.Data.DataTable tbl = ds.Tables[Names.CounselorAttributes];

            // If there is a table then look for a match. If found, return it.

            if (tbl == null)
            {
                // Read the information from the system for the indicated name
                System.Data.SqlClient.SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                try
                {
                    cn.Open();
                    lock (ds)
                    {
                        using (System.Data.SqlClient.SqlCommand cmd = new SqlCommand())
                        {
                            var _with14 = cmd;
                            _with14.Connection = cn;
                            _with14.CommandText = "SELECT [oID], [Counselor], [Attribute] FROM counselor_attributes WITH (NOLOCK)";
                            _with14.CommandType = CommandType.Text;

                            using (System.Data.SqlClient.SqlDataAdapter da = new System.Data.SqlClient.SqlDataAdapter(cmd))
                            {
                                da.FillLoadOption = LoadOption.OverwriteChanges;
                                da.Fill(ds, Names.CounselorAttributes);
                            }
                        }
                        tbl = ds.Tables[Names.CounselorAttributes];
                        if (tbl.PrimaryKey.GetUpperBound(0) < 0)
                            tbl.PrimaryKey = new System.Data.DataColumn[] { tbl.Columns["oID"] };
                    }
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    global::DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    if (cn != null)
                        cn.Dispose();
                }
            }

            return tbl;
        }
    }
}