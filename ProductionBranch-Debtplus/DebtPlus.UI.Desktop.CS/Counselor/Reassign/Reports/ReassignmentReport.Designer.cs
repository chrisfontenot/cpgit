using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace DebtPlus.UI.Desktop.CS.Counselor.Reassign.Reports
{
	partial class ReassignmentReport
	{
		//XtraReport overrides dispose to clean up the component list.
		[System.Diagnostics.DebuggerNonUserCode]
		protected override void Dispose(bool disposing)
		{
			if (disposing && components != null) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		//Required by the Designer

		private System.ComponentModel.IContainer components = null;
		//NOTE: The following procedure is required by the Designer
		//It can be modified using the Designer.  
		//Do not modify it using the code editor.
		[System.Diagnostics.DebuggerStepThrough]
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReassignmentReport));
			this.XrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.XrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_start_date = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_client_name = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_client = new DevExpress.XtraReports.UI.XRLabel();
			this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.XrLabel_new_counselor_name = new DevExpress.XtraReports.UI.XRLabel();
			this.XrLabel_active_status = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)this).BeginInit();
			//
			//PageHeader
			//
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] { this.XrPanel1 });
			this.PageHeader.HeightF = 145.8333f;
			this.PageHeader.Controls.SetChildIndex(this.XrPanel_AgencyAddress, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrLabel_Title, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrLine_Title, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrLabel_Subtitle, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrPageInfo1, 0);
			this.PageHeader.Controls.SetChildIndex(this.XrPanel1, 0);
			//
			//XrLabel_Title
			//
			this.XrLabel_Title.StylePriority.UseFont = false;
			//
			//XrPageInfo_PageNumber
			//
			this.XrPageInfo_PageNumber.StylePriority.UseTextAlignment = false;
			//
			//XRLabel_Agency_Name
			//
			this.XRLabel_Agency_Name.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Address3
			//
			this.XrLabel_Agency_Address3.StylePriority.UseFont = false;
			this.XrLabel_Agency_Address3.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Address1
			//
			this.XrLabel_Agency_Address1.StylePriority.UseFont = false;
			this.XrLabel_Agency_Address1.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Phone
			//
			this.XrLabel_Agency_Phone.StylePriority.UseFont = false;
			this.XrLabel_Agency_Phone.StylePriority.UseTextAlignment = false;
			//
			//XrLabel_Agency_Address2
			//
			this.XrLabel_Agency_Address2.StylePriority.UseFont = false;
			this.XrLabel_Agency_Address2.StylePriority.UseTextAlignment = false;
			//
			//Detail
			//
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrLabel_active_status,
				this.XrLabel_client,
				this.XrLabel_client_name,
				this.XrLabel_start_date
			});
			this.Detail.HeightF = 16.04166f;
			//
			//XrPanel1
			//
			this.XrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
				this.XrLabel4,
				this.XrLabel3,
				this.XrLabel2,
				this.XrLabel1
			});
			this.XrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 119.7917f);
			this.XrPanel1.Name = "XrPanel1";
			this.XrPanel1.SizeF = new System.Drawing.SizeF(750f, 17f);
			this.XrPanel1.StyleName = "XrControlStyle_HeaderPannel";
			//
			//XrLabel4
			//
			this.XrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(100f, 0.9999847f);
			this.XrLabel4.Name = "XrLabel4";
			this.XrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel4.SizeF = new System.Drawing.SizeF(73.25f, 15f);
			this.XrLabel4.StylePriority.UsePadding = false;
			this.XrLabel4.StylePriority.UseTextAlignment = false;
			this.XrLabel4.Text = "STATUS";
			this.XrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			//
			//XrLabel3
			//
			this.XrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(608f, 0.9999924f);
			this.XrLabel3.Name = "XrLabel3";
			this.XrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel3.SizeF = new System.Drawing.SizeF(142f, 15f);
			this.XrLabel3.StylePriority.UsePadding = false;
			this.XrLabel3.StylePriority.UseTextAlignment = false;
			this.XrLabel3.Text = "START DATE";
			this.XrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			//
			//XrLabel2
			//
			this.XrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(173.25f, 0.9999847f);
			this.XrLabel2.Name = "XrLabel2";
			this.XrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel2.SizeF = new System.Drawing.SizeF(434.7501f, 15f);
			this.XrLabel2.StylePriority.UsePadding = false;
			this.XrLabel2.StylePriority.UseTextAlignment = false;
			this.XrLabel2.Text = "NAME";
			this.XrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			//
			//XrLabel1
			//
			this.XrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0f, 1f);
			this.XrLabel1.Name = "XrLabel1";
			this.XrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel1.SizeF = new System.Drawing.SizeF(100f, 15f);
			this.XrLabel1.StylePriority.UsePadding = false;
			this.XrLabel1.Text = "Client";
			//
			//XrLabel_start_date
			//
			this.XrLabel_start_date.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "start_date", "{0:d}") });
			this.XrLabel_start_date.LocationFloat = new DevExpress.Utils.PointFloat(608f, 0f);
			this.XrLabel_start_date.Name = "XrLabel_start_date";
			this.XrLabel_start_date.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel_start_date.SizeF = new System.Drawing.SizeF(142f, 15f);
			this.XrLabel_start_date.StylePriority.UsePadding = false;
			this.XrLabel_start_date.StylePriority.UseTextAlignment = false;
			this.XrLabel_start_date.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			//
			//XrLabel_client_name
			//
			this.XrLabel_client_name.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "client_name") });
			this.XrLabel_client_name.LocationFloat = new DevExpress.Utils.PointFloat(173.25f, 0f);
			this.XrLabel_client_name.Name = "XrLabel_client_name";
			this.XrLabel_client_name.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel_client_name.SizeF = new System.Drawing.SizeF(434.75f, 15f);
			this.XrLabel_client_name.StylePriority.UsePadding = false;
			this.XrLabel_client_name.StylePriority.UseTextAlignment = false;
			this.XrLabel_client_name.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			//
			//XrLabel_client
			//
			this.XrLabel_client.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "client", "{0:0000000}") });
			this.XrLabel_client.LocationFloat = new DevExpress.Utils.PointFloat(0f, 0f);
			this.XrLabel_client.Name = "XrLabel_client";
			this.XrLabel_client.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel_client.SizeF = new System.Drawing.SizeF(100f, 15f);
			this.XrLabel_client.StylePriority.UsePadding = false;
			this.XrLabel_client.StylePriority.UseTextAlignment = false;
			this.XrLabel_client.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			//
			//GroupHeader1
			//
			this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] { this.XrLabel_new_counselor_name });
			this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] { new DevExpress.XtraReports.UI.GroupField("new_counselor_name", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending) });
			this.GroupHeader1.HeightF = 44.79167f;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.PageBreak = DevExpress.XtraReports.UI.PageBreak.BeforeBand;
			this.GroupHeader1.RepeatEveryPage = true;
			//
			//XrLabel_new_counselor_name
			//
			this.XrLabel_new_counselor_name.BorderColor = System.Drawing.Color.Maroon;
			this.XrLabel_new_counselor_name.Borders = (DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) | DevExpress.XtraPrinting.BorderSide.Right) | DevExpress.XtraPrinting.BorderSide.Bottom);
			this.XrLabel_new_counselor_name.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "new_counselor_name", "New Counselor: {0}") });
			this.XrLabel_new_counselor_name.LocationFloat = new DevExpress.Utils.PointFloat(0f, 10.00001f);
			this.XrLabel_new_counselor_name.Name = "XrLabel_new_counselor_name";
			this.XrLabel_new_counselor_name.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 3, 3, 100f);
			this.XrLabel_new_counselor_name.SizeF = new System.Drawing.SizeF(750.0001f, 23f);
			this.XrLabel_new_counselor_name.StyleName = "XrControlStyle_GroupHeader";
			this.XrLabel_new_counselor_name.StylePriority.UseBorderColor = false;
			this.XrLabel_new_counselor_name.StylePriority.UseBorders = false;
			this.XrLabel_new_counselor_name.StylePriority.UsePadding = false;
			//
			//XrLabel_active_status
			//
			this.XrLabel_active_status.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] { new DevExpress.XtraReports.UI.XRBinding("Text", null, "active_status") });
			this.XrLabel_active_status.LocationFloat = new DevExpress.Utils.PointFloat(100f, 0f);
			this.XrLabel_active_status.Name = "XrLabel_active_status";
			this.XrLabel_active_status.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100f);
			this.XrLabel_active_status.SizeF = new System.Drawing.SizeF(73.25f, 15f);
			this.XrLabel_active_status.StylePriority.UsePadding = false;
			this.XrLabel_active_status.StylePriority.UseTextAlignment = false;
			this.XrLabel_active_status.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			//
			//ReassignmentReport
			//
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
				this.TopMarginBand1,
				this.Detail,
				this.BottomMarginBand1,
				this.PageHeader,
				this.PageFooter,
				this.GroupHeader1
			});
			this.ReportPrintOptions.DetailCountOnEmptyDataSource = 0;
			this.ReportPrintOptions.PrintOnEmptyDataSource = false;
			this.RequestParameters = false;
			this.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic;
			this.ScriptReferencesString = resources.GetString("$this.ScriptReferencesString");
			this.ShowPrintMarginsWarning = false;
			this.ShowPrintStatusDialog = false;
			this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
				this.XrControlStyle_Header,
				this.XrControlStyle_Totals,
				this.XrControlStyle_HeaderPannel,
				this.XrControlStyle_GroupHeader
			});
			this.Version = "11.2";
			this.Controls.SetChildIndex(this.GroupHeader1, 0);
			this.Controls.SetChildIndex(this.PageFooter, 0);
			this.Controls.SetChildIndex(this.PageHeader, 0);
			this.Controls.SetChildIndex(this.BottomMarginBand1, 0);
			this.Controls.SetChildIndex(this.Detail, 0);
			this.Controls.SetChildIndex(this.TopMarginBand1, 0);
			((System.ComponentModel.ISupportInitialize)this).EndInit();

		}
		protected internal DevExpress.XtraReports.UI.XRPanel XrPanel1;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel3;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel2;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel1;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_client;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_client_name;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_start_date;
		protected internal DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_new_counselor_name;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel4;
		protected internal DevExpress.XtraReports.UI.XRLabel XrLabel_active_status;
	}
}
