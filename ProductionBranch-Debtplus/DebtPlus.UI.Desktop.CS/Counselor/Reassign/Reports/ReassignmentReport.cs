using DebtPlus.Reports.Template;

namespace DebtPlus.UI.Desktop.CS.Counselor.Reassign.Reports
{
    public partial class ReassignmentReport : TemplateXtraReportClass
    {
        public ReassignmentReport() : base()
        {
            InitializeComponent();
        }

        public override string ReportTitle
        {
            get { return "Counselor Reassignments"; }
        }

        private string privateSubtitle;

        public string Subtitle
        {
            set { privateSubtitle = value; }
        }

        public override string ReportSubTitle
        {
            get { return privateSubtitle; }
        }
    }
}