#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Windows.Forms;
using DebtPlus.Interfaces.Desktop;

namespace DebtPlus.UI.Desktop.CS.Extract.CSV
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() : base()
        {
        }

        /// <summary>
        /// Interface to the main application
        /// </summary>
        public void OldAppMain(string[] args)
        {
            MyThreadClass cls = new MyThreadClass(args);
            System.Threading.Thread thrd = new System.Threading.Thread(cls.ProcessThread);
            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Name = "ExtractCSV";
            thrd.Start();
        }

        /// <summary>
        /// Class to perform the extract process.
        /// </summary>
        /// <remarks>This class inherits the processing class and overrides some of the functions therein</remarks>
        private class MyThreadClass : DebtPlus.Svc.Extract.CSV.ProcessingClass
        {
            private string[] args;

            private DevExpress.Utils.WaitDialogForm WaitDlg = null;

            public MyThreadClass(string[] args)
            {
                this.args = args;
            }

            /// <summary>
            /// Handle the feedback request to display the progress dialog as needed
            /// </summary>
            /// <param name="e"></param>
            /// <remarks></remarks>
            protected override void OnFeedback(Svc.Extract.CSV.ProcessingClass.FeedbackArgs e)
            {
                base.OnFeedback(e);

                // If there is no waiting dialog, create one.
                if (WaitDlg == null)
                {
                    WaitDlg = new DevExpress.Utils.WaitDialogForm("Performing extract operation");
                    WaitDlg.Show();
                }

                // Update the caption accordingly
                WaitDlg.SetCaption(e.FileName);
            }

            /// <summary>
            /// Handle the request to retrieve the dates from the user. This will call up the dialog for the dates.
            /// </summary>
            /// <param name="e"></param>
            /// <remarks></remarks>
            protected override void OnGetDates(DebtPlus.Svc.Extract.CSV.ProcessingClass.GetDatesArgs e)
            {
                base.OnGetDates(e);
                if (e.IsValid)
                {
                    using (var frm = new DebtPlus.Reports.Template.Forms.DateReportParametersForm())
                    {
                        if (frm.ShowDialog() != DialogResult.OK)
                        {
                            e.IsValid = false;
                        }
                        else
                        {
                            e.FromDate = frm.Parameter_FromDate;
                            e.ToDate = frm.Parameter_ToDate;
                        }
                    }
                }
            }

            /// <summary>
            /// Handle the request to resolve the filename as needed
            /// </summary>
            /// <param name="e"></param>
            /// <remarks></remarks>
            protected override void OnGetFilename(DebtPlus.Svc.Extract.CSV.ProcessingClass.GetFilenameArgs e)
            {
                base.OnGetFilename(e);

                // Look for a wild-card in the name. If there is one then ask for the filenaem.
                if (e.IsValid && (e.FileName.Contains("?") || e.FileName.Contains("*")))
                {
                    using (var dlg = new OpenFileDialog())
                    {
                        dlg.AddExtension = true;
                        dlg.CheckFileExists = false;
                        dlg.CheckPathExists = true;
                        dlg.DefaultExt = "txt";
                        dlg.DereferenceLinks = true;
                        dlg.FileName = e.FileName;
                        dlg.Filter = "Text Files (*.txt)|*.txt|Comma Separated Files (*.csv)|*.csv|All Files (*.*)|*.*";
                        dlg.FilterIndex = 0;
                        dlg.InitialDirectory = System.IO.Path.GetDirectoryName(e.FileName);
                        dlg.Multiselect = false;
                        dlg.RestoreDirectory = true;
                        dlg.ShowReadOnly = false;
                        dlg.Title = e.Title;
                        dlg.ValidateNames = true;

                        if (dlg.ShowDialog() != DialogResult.OK)
                        {
                            e.IsValid = false;
                        }
                        else
                        {
                            e.FileName = dlg.FileName;
                        }
                    }
                }
            }

            /// <summary>
            /// Handle the thread to perform the extract operation
            /// </summary>
            /// <remarks></remarks>
            public void ProcessThread()
            {
                try
                {
                    string ControlFile = args != null && args.GetUpperBound(0) >= 0 ? args[0] : string.Empty;

                    // If there is a control file then go ahead and do the extract
                    if (ControlFile != string.Empty)
                    {
                        PerformExtract(args[0]);
                    }
                    else
                    {
                        // The control file is missing. This is bad news.
                        DebtPlus.Data.Forms.MessageBox.Show("Missing the name of the control file", "Parameter Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
                finally
                {
                    // When the extract completes then remove the waiting dialog.
                    if (WaitDlg != null)
                    {
                        WaitDlg.Close();
                        WaitDlg.Dispose();
                        WaitDlg = null;
                    }
                }
            }
        }
    }
}