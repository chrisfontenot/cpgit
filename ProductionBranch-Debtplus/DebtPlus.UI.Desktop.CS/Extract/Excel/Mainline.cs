#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.Security;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Serialization;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.Reports.Template.Forms;
using DevExpress.Utils;

namespace DebtPlus.UI.Desktop.CS.Extract.Excel
{
    public partial class Mainline : IDesktopMainline
    {
        // global storage for the program
        private string Last_Filename   = string.Empty;
        internal ExcelArgParser ap     = new ExcelArgParser(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ExtractExcel), "configFilePath"));
        private WaitDialogForm Waitdlg = null;
        private string Waitdlg_caption = string.Empty;
        private DataSet ds             = null;
        private DataTable tbl          = null;

        // For all other versions of Office without PIAs
        private object ThisApplication = null;
        private object ThisWorkbook    = null;
        private object ThisSheet       = null;
        private object ThisRange       = null;

        public Mainline() { }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    if (!ap.Parse((string[])param))
                    {
                        return;
                    }

                    XmlSerializer serializer = new XmlSerializer(typeof(List<Sheet>));
                    StreamReader inputfile   = new StreamReader(ap.CommandFile);
                    List<Sheet> sheets       = (List<Sheet>) serializer.Deserialize(inputfile);
                    inputfile.Close();

                    // Set the pointer to the new application
                    if (OpenApplication())
                    {
                        foreach (Sheet sht in sheets)
                        {
                            ProcessSheet(sht);
                        }
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }

                finally
                {
                    if (ThisApplication != null)
                    {
                        CloseApplication();
                    }
                }
            }))
            {
                IsBackground = false,
                Name = "ExtractExcel"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }

        [SecuritySafeCritical]
        private bool OpenApplication()
        {
            // From the registry, allocate a type for the excel object
            Type objClassType = Type.GetTypeFromProgID("Excel.Application");

            // If we can not create the type from the registry then excel is not installed.
            if (objClassType == null)
            {
                DebtPlus.Data.Forms.MessageBox.Show("The type EXCEL.APPLICATION could not be found in the registry." + Environment.NewLine + Environment.NewLine + "Did you forget to install Microsoft Excel? It is required for this application.", "Excel Could not be found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }

            // Given the type, create the application instance
            ThisApplication = Activator.CreateInstance(objClassType);

            // Set the properties
            SetProperty(ThisApplication, "EnableAnimations", false);
            SetProperty(ThisApplication, "EnableAutoComplete", false);
            SetProperty(ThisApplication, "EnableEvents", false);
            SetProperty(ThisApplication, "EnableSound", false);
            SetProperty(ThisApplication, "DisplayAlerts", false);

            return true;
        }

        private void CloseApplication()
        {
            if (ThisApplication != null)
            {
                // Restore the settings back to the default values
                SetProperty(ThisApplication, "EnableAnimations", true);
                SetProperty(ThisApplication, "EnableAutoComplete", true);
                SetProperty(ThisApplication, "EnableEvents", true);
                SetProperty(ThisApplication, "EnableSound", true);
                SetProperty(ThisApplication, "DisplayAlerts", true);

                // Restore control for the keyboard back to the excel sheet
                SetProperty(ThisApplication, "UserControl", true);
                ThisApplication = null;
            }
        }

        private bool QuitApplication()
        {
            try
            {
                InvokeMethod(ThisApplication, "Quit");
                ThisApplication = null;
                return true;
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error quitting application", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private Int32 SheetCount()
        {
            Int32 answer = 0;
            try
            {
                object Sheets = GetProperty(ThisWorkbook, "Sheets");
                answer = Convert.ToInt32(GetProperty(Sheets, "Count"));
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error obtaining sheet count", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return answer;
        }

        private bool CreateSheet()
        {
            try
            {
                object Sheets = GetProperty(ThisWorkbook, "Sheets");
                InvokeMethod(Sheets, "Add");
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error adding new sheet", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool ActivateSheet()
        {
            try
            {
                InvokeMethod(ThisSheet, "Activate");
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error activating sheet", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private bool GetSheet(object Name)
        {
            try
            {
                object Sheets = GetProperty(ThisWorkbook, "Sheets");
                ThisSheet = GetProperty(Sheets, "Item", Name);
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error finding sheet", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void SetVisible(bool Value)
        {
            SetProperty(ThisApplication, "Visible", Value);
        }

        private object GetRange(string CellName)
        {
            return GetProperty(ThisSheet, "Range", CellName);
        }

        private bool SetValue(object Rng, object Value)
        {
            // Can not set cells to null
            if (Value == null || System.Object.Equals(Value, System.DBNull.Value))
            {
                return false;
            }

            // try to use a numeric format if possible
            try
            {
                double doubleValue;
                if (double.TryParse(Convert.ToString(Value), out doubleValue))
                {
                    SetProperty(Rng, "Value", doubleValue);
                    return true;
                }
            }
            catch { }

            // Otherwise use the string format.
            SetProperty(Rng, "Value", Value);
            return true;
        }

        private bool OpenWorkbook(string Fname)
        {
            try
            {
                object Workbooks = GetProperty(ThisApplication, "Workbooks");
                ThisWorkbook = InvokeMethod(Workbooks, "Open", Fname);
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error opening input file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private bool CreateWorkbook(object Fname = null)
        {
            object Workbooks = GetProperty(ThisApplication, "Workbooks");

            if (Fname != null)
            {
                ThisWorkbook = InvokeMethod(Workbooks, "Add", Fname);
            }
            else
            {
                ThisWorkbook = InvokeMethod(Workbooks, "Add");
            }

            return true;
        }

        private bool SaveWorkbook(object WkBk, string Fname)
        {
            try
            {
                InvokeMethod(ThisWorkbook, "SaveAs", Fname);
            }
            catch (Exception ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error saving workbook", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        #region " Wrappers "

        [SecuritySafeCritical]
        private void SetProperty(object obj, string sProperty, object oValue)
        {
            object[] oParam = new object[1];
            oParam[0] = oValue;
            obj.GetType().InvokeMember(sProperty, BindingFlags.SetProperty, null, obj, oParam);
        }

        [SecuritySafeCritical]
        private object GetProperty(object obj, string sProperty, object oValue)
        {
            object[] oParam = new object[1];
            oParam[0] = oValue;
            return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, null, obj, oParam);
        }

        [SecuritySafeCritical]
        private object GetProperty(object obj, string sProperty, object oValue1, object oValue2)
        {
            object[] oParam = new object[2];
            oParam[0] = oValue1;
            oParam[1] = oValue2;
            return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, null, obj, oParam);
        }

        [SecuritySafeCritical]
        private object GetProperty(object obj, string sProperty)
        {
            return obj.GetType().InvokeMember(sProperty, BindingFlags.GetProperty, null, obj, null);
        }

        [SecuritySafeCritical]
        private object InvokeMethod(object obj, string sProperty, object[] oParam)
        {
            return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, null, obj, oParam);
        }

        [SecuritySafeCritical]
        private object InvokeMethod(object obj, string sProperty, object oValue)
        {
            object[] oParam = new object[1];
            oParam[0] = oValue;
            return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, null, obj, oParam);
        }

        [SecuritySafeCritical]
        private object InvokeMethod(object obj, string sProperty)
        {
            return obj.GetType().InvokeMember(sProperty, BindingFlags.InvokeMethod, null, obj, null);
        }

        #endregion " Wrappers "

        private void ProcessSheet(Sheet sht)
        {
            // Open or create the workbook file.
            if (sht.hasOption("exist"))
            {
                if (!OpenWorkbook(sht.File, sht.Title))
                    return;
            }
            else
            {
                if (!CreateWorkbook())
                    return;
            }

            // Display the sheets if desired
            if (sht.hasOption("show"))
                SetVisible(true);

            // Allocate a sheet for the data. Use either the name or the first sheet in the list
            if (sht.Name != string.Empty)
            {
                Int32 SheetNumber = -1;
                if (Int32.TryParse(sht.Name, out SheetNumber) && SheetNumber >= 1)
                {
                    if (!GetSheet(SheetNumber))
                        return;
                }
                else
                {
                    if (!GetSheet(sht.Name))
                        return;
                }
            }
            else
            {
                if (SheetCount() < 1)
                    CreateSheet();
                if (!GetSheet(1))
                    return;
            }

            foreach (Query qry in sht.Queries)
            {
                ProcessQuery(sht, qry);
            }

            // Save and Quit the application if so desired
            if (sht.hasOption("quit"))
            {
                SaveWorkbook(sht.File, sht.Title);
                QuitApplication();
            }
            else
            {
                // Save the sheet if so desired
                if (sht.hasOption("save"))
                    SaveWorkbook(sht.File, sht.Title);
            }
        }

        private DateTime FromDate;
        private DateTime ToDate;

        private bool RequestedDates = false;

        private void ProcessQuery(Sheet Sht, Query qry)
        {
            // Request the date range if so desired
            if (qry.NeedDateDialog())
            {
                if (!RequestedDates)
                {
                    RequestedDates = true;

                    using (DateReportParametersForm dlg = new DateReportParametersForm())
                    {
                        if (dlg.ShowDialog() != DialogResult.OK)
                        {
                            Application.ExitThread();
                            return;
                        }

                        FromDate = dlg.Parameter_FromDate;
                        ToDate = dlg.Parameter_ToDate;
                    }
                }

                // For the date values, find the corresponding items in the parameters
                foreach (Parameter parm in qry.Parameters)
                {
                    switch (parm.DialogTypeValue)
                    {
                        case Parameter.DialogType.FromDate:
                            parm.Value = FromDate.ToShortDateString();
                            break;

                        case Parameter.DialogType.ToDate:
                            parm.Value = ToDate.ToShortDateString() + " 23:59:59";
                            break;
                    }
                }
            }

            try
            {
                // Issue the query
                using (global::DebtPlus.UI.Common.CursorManager cm = new global::DebtPlus.UI.Common.CursorManager())
                {
                    using (SqlCommand cmd = qry.BuildCommand())
                    {
                        cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

                        // Display the wait dialog
                        string Waitdlg_caption = cmd.CommandText;
                        if (Waitdlg_caption.Length > 25)
                            Waitdlg_caption = Waitdlg_caption.Substring(0, 25);
                        Waitdlg = new WaitDialogForm(Waitdlg_caption, "Processing Command");
                        Waitdlg.Show();

                        ds = new DataSet("ds");
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(ds, "table1");

                            tbl = null;
                            if (ds.Tables.Count > 0)
                                tbl = ds.Tables[0];
                        }
                    }
                }

                if (tbl != null)
                {
                    // Determine the type of the processing based upon the sheet directive
                    if (Sht.Type == "extract")
                    {
                        Process_Extract(Sht, qry);
                    }
                    else if (Sht.Type == "values")
                    {
                        Process_Values(qry);
                    }
                }
            }
            catch (SqlException ex)
            {
                DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
                DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading values from database", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                // Dispose of the dialog when we are completed
                if (Waitdlg != null)
                {
                    Waitdlg.Close();
                    Waitdlg.Dispose();
                }
            }
        }

        private void Process_Extract(Sheet sht, Query qry)
        {
            Int32 col = default(Int32);
            Int32 FirstLine = sht.FirstLine - 1;

            // Write the first line with the column names
            if (sht.hasOption("headers"))
            {
                col = sht.FirstColumn;
                FirstLine += 1;
                foreach (DataColumn DataCol in ds.Tables[0].Columns)
                {
                    // ignore time-stamp columns
                    if (!object.ReferenceEquals(DataCol.DataType, typeof(byte[])))
                    {
                        SetValue(GetRange(Cell(FirstLine, col)), DataCol.ColumnName);
                        col += 1;
                    }
                }
            }

            // Load the data for the item
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                col = sht.FirstColumn - 1;
                FirstLine += 1;
                foreach (DataColumn DataCol in ds.Tables[0].Columns)
                {
                    string colName = DataCol.ColumnName;

                    // Just ignore and remove any byte() objects from the list
                    if (object.ReferenceEquals(DataCol.DataType, typeof(byte[])))
                    {
                        continue;
                    }

                    // Advance to the next column
                    col += 1;

                    // Find the value for the item. If null, ignore it now.
                    object value = row[DataCol.Ordinal];
                    if (value == null || object.ReferenceEquals(value, System.DBNull.Value))
                    {
                        continue;
                    }

                    // Find the column in the list of columns for this query sheet
                    if (qry.Columns != null && qry.Columns.Count > 0)
                    {
                        Extract.Excel.Column colData = qry.Columns.Find(selCol => System.String.Compare(selCol.Name, colName, true) == 0);
                        if (colData != null)
                        {
                            value = colData.Translate(value);
                        }

                        if (object.ReferenceEquals(value.GetType(), typeof(System.Guid)))
                        {
                            value = value.ToString();
                        }

                        SetValue(GetRange(Cell(FirstLine, col)), value);
                    }
                }
            }
        }

        private void Process_Values(Query qry)
        {
            // Process the non-extract value to simply update the cell contents
            string last_sheet_name = string.Empty;

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string sheet_name = string.Empty;
                string cell_name = string.Empty;
                object cell_value = null;

                foreach (DataColumn DataCol in ds.Tables[0].Columns)
                {
                    if (!object.ReferenceEquals(DataCol.DataType, typeof(byte[])))
                    {
                        switch (DataCol.ColumnName.ToLower())
                        {
                            case "sheet":
                            case "page":
                                sheet_name = Convert.ToString(row[DataCol.Ordinal]);
                                break;

                            case "location":
                            case "cell":
                                cell_name = Convert.ToString(row[DataCol.Ordinal]);
                                break;

                            case "value":
                                cell_value = row[DataCol.Ordinal];
                                break;
                        }
                    }
                }

                // Ignore null values
                if (cell_value == null || object.ReferenceEquals(cell_value, System.DBNull.Value))
                {
                    continue;
                }

                // Find the sheet for the new value
                if (sheet_name == string.Empty || cell_name == string.Empty)
                {
                    continue;
                }

                // Find any mapping that is needed
                Extract.Excel.Column colItem = qry.Columns.Find(colSearch => string.Compare(colSearch.Name, cell_name, true) == 0);
                if (colItem != null)
                {
                    cell_value = colItem.Translate(cell_value);
                }

                if (string.Compare(sheet_name, last_sheet_name, true) != 0 || ThisSheet == null)
                {
                    Int32 IntegerSheetNumber = default(Int32);
                    if (Int32.TryParse(sheet_name, out IntegerSheetNumber))
                    {
                        if (!GetSheet(IntegerSheetNumber))
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (!GetSheet(sheet_name))
                        {
                            return;
                        }
                    }
                    last_sheet_name = sheet_name;
                }

                // Update the value on the sheet
                ThisRange = GetRange(cell_name);
                SetValue(ThisRange, cell_value);

                // Update the dialog with the cell range
                Waitdlg.SetCaption(Waitdlg_caption + " (" + cell_name + ")");
            }
        }

        private bool OpenWorkbook(string Filename, string Title)
        {
            string Current_Filename = Filename;

            // If the filename matches the previous setting then just return the last application
            if (ThisWorkbook != null)
            {
                if (Last_Filename != string.Empty)
                {
                    if (Last_Filename == Filename)
                    {
                        return true;
                    }
                }
            }

            // Try to find a match to the command line parameters. We will take any filename referenced in the list.
            Regex rx = new Regex(@"\[CommandLine\:\s*(?<argument>\d+)\]", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match FoundMatch = rx.Match(Current_Filename);
            Int32 Argument = default(Int32);
            if (FoundMatch != null && Int32.TryParse(FoundMatch.Groups[1].Value, out Argument))
            {
                if (Argument >= 1 && Argument <= ap.Files.Count)
                {
                    Current_Filename = ap.Files[Argument - 1];
                }
            }

            // Replace the string [My Documents] with the current documents directory
            if (Current_Filename.LastIndexOf("[My Documents]") >= 0)
            {
                string DocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                Current_Filename = Current_Filename.Replace("[My Documents]", DocumentsDirectory);
            }

            // If the file is a wild-card item then ask the user for the filename
            if (Current_Filename.IndexOfAny(new char[] {
                '*',
                '?'
            }) >= 0)
            {
                DialogResult answer = default(DialogResult);

                using (OpenFileDialog dlg = new OpenFileDialog())
                {
                    dlg.AddExtension = true;
                    dlg.CheckFileExists = true;
                    dlg.CheckPathExists = true;
                    dlg.DefaultExt = "xls";
                    dlg.DereferenceLinks = true;
                    dlg.FileName = Path.GetFileName(Current_Filename);
                    dlg.Filter = "2010 Excel Documents (*.xlsx)|*.xlsx|2003/XP Excel Documents (*.xls)|*.xls|All Files (*.*)|*.*";
                    dlg.FilterIndex = 1;
                    dlg.InitialDirectory = Path.GetDirectoryName(Current_Filename);
                    dlg.Multiselect = false;
                    dlg.RestoreDirectory = true;
                    dlg.ShowReadOnly = false;
                    dlg.Title = Title;
                    dlg.ValidateNames = true;
                    answer = dlg.ShowDialog();
                    Current_Filename = dlg.FileName;
                }

                // If the user canceled the dialog then die gracefully
                if (answer == DialogResult.Cancel)
                    return false;
            }

            // Load the excel sheet
            if (!OpenWorkbook(Current_Filename))
            {
                return false;
            }
            Last_Filename = Filename;
            return true;
        }

        private bool SaveWorkbook(string Filename, string Title = "Save Excel Document")
        {
            string Current_Filename = Filename;

            // Try to find a match to the command line parameters. We will take any filename referenced in the list.
            Regex rx = new Regex("\\[CommandLine\\:\\s*(?<argument>\\d+)\\]", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            Match FoundMatch = rx.Match(Current_Filename);
            Int32 Argument = default(Int32);
            if (FoundMatch != null && Int32.TryParse(FoundMatch.Groups[1].Value, out Argument))
            {
                if (Argument >= 1 && Argument <= ap.Files.Count)
                {
                    Current_Filename = ap.Files[Argument - 1];
                }
            }

            // Replace the string [My Documents] with the current documents directory
            if (Current_Filename.LastIndexOf("[My Documents]") >= 0)
            {
                string DocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                Current_Filename = Current_Filename.Replace("[My Documents]", DocumentsDirectory);
            }

            // If the file is a wild-card item then ask the user for the filename
            if (Current_Filename.IndexOfAny(new char[] {
                '*',
                '?'
            }) >= 0)
            {
                DialogResult answer = default(DialogResult);
                using (SaveFileDialog dlg = new SaveFileDialog())
                {
                    dlg.AddExtension = true;
                    dlg.CheckFileExists = false;
                    dlg.CheckPathExists = true;
                    dlg.DefaultExt = "xlsx";
                    dlg.DereferenceLinks = true;
                    dlg.FileName = string.Empty;
                    dlg.Filter = "Excel 2010 Documents (*.xlsx)|*.xlsx|Excel 2007 Documents (*.xls)|*.xls|All Files (*.*)|*.*";
                    dlg.FilterIndex = 0;
                    dlg.InitialDirectory = Path.GetDirectoryName(Current_Filename);
                    dlg.RestoreDirectory = true;
                    dlg.Title = Title;
                    dlg.ValidateNames = true;

                    answer = dlg.ShowDialog();
                    Current_Filename = dlg.FileName;
                }

                // If the user canceled the dialog then die gracefully
                if (answer == DialogResult.Cancel)
                    return false;
            }

            return SaveWorkbook(ThisWorkbook, Current_Filename);
        }

        private string Cell(Int32 Row, Int32 Col)
        {
            return ColString(Col - 1) + RowString(Row - 1);
        }

        private string RowString(Int32 row)
        {
            if (row < 0)
            {
                throw new ArgumentOutOfRangeException("row can not be negative");
            }

            return (row + 1).ToString();
        }

        private string ColString(Int32 col)
        {
            if (col < 0)
            {
                throw new ArgumentOutOfRangeException("col can not be negative");
            }

            if (col < 26)
            {
                return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".Substring(col, 1);
            }

            return ColString((col / 26) - 1) + ColString(col % 26);
        }
    }
}