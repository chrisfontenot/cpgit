namespace DebtPlus.UI.Desktop.CS.Extract.Excel
{
    public partial class Column
    {
        public string Name { get; set; }
        public string Type { get; set; }

        public string Translate(object Input)
        {
            // Get a pointer to the translation class
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();

            // From the type, create an instance of the class
            System.Type typ = asm.GetType(Type);
            if (typ != null)
            {
                using (Extract.IConversion instance = (Extract.IConversion)System.Activator.CreateInstance(typ))
                {
                    if (instance != null)
                    {
                        return instance.ToText(Input);
                    }
                }
            }

            return Input.ToString();
        }
    }
}