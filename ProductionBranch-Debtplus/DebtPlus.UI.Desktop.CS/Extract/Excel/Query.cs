using System;
using System.Collections.Generic;
using System.Data;

using System.Data.SqlClient;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.Extract.Excel
{
    public partial class Query
    {
        public System.Collections.Generic.List<Column> Columns { get; set; }

        private string privateCommand;

        public string Command
        {
            get { return privateCommand; }
            set
            {
                StringBuilder sb = new StringBuilder(value);
                sb.Replace("\r", "");
                sb.Replace("\n", "");
                sb.Replace("\t", "");
                sb.Replace("\v", "");
                privateCommand = sb.ToString().Trim();
            }
        }

        private List<Parameter> privateParameters = new List<Parameter>();

        public List<Parameter> Parameters
        {
            get { return privateParameters; }
            set { privateParameters = value; }
        }

        private Int32 privateCommandTimeout = 0;

        public Int32 CommandTimeout
        {
            get { return privateCommandTimeout; }
            set { privateCommandTimeout = value; }
        }

        private string privateType = "text";

        public string Type
        {
            get { return privateType; }
            set { privateType = value; }
        }

        public CommandType CommandType()
        {
            CommandType answer = System.Data.CommandType.Text;
            switch (Type.ToLower())
            {
                case "proc":
                case "procedure":
                case "storedprocedure":
                    answer = System.Data.CommandType.StoredProcedure;
                    break;

                case "table":
                    answer = System.Data.CommandType.TableDirect;
                    break;
            }
            return answer;
        }

        public SqlCommand BuildCommand()
        {
            SqlCommand cmd = new SqlCommand();
            var _with1 = cmd;
            _with1.CommandText = Command;
            _with1.CommandType = CommandType();
            _with1.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, CommandTimeout);
            foreach (Parameter parm in Parameters)
            {
                _with1.Parameters.Add(parm.BuldParameter());
            }
            return cmd;
        }

        public bool NeedDateDialog()
        {
            bool answer = false;
            foreach (Parameter parm in Parameters)
            {
                if (parm.DialogTypeValue != Parameter.DialogType.Standard)
                {
                    answer = true;
                    break;
                }
            }
            return answer;
        }
    }
}