using System;
using System.Collections.Generic;

namespace DebtPlus.UI.Desktop.CS.Extract.Excel
{
    public partial class Sheet
    {
        private string privateName = string.Empty;

        public string Name
        {
            get { return privateName; }
            set { privateName = value.Trim(); }
        }

        private string privateType = "extract";

        public string Type
        {
            get { return privateType; }
            set { privateType = value.ToLower().Trim(); }
        }

        private string privateTitle = string.Empty;

        public string Title
        {
            get { return privateTitle; }
            set { privateTitle = value.Trim(); }
        }

        private string privateFile = "*.xls";

        public string File
        {
            get { return privateFile; }
            set { privateFile = value; }
        }

        private string privateOptions = string.Empty;

        public string Options
        {
            get { return privateOptions; }
            set { privateOptions = value.ToLower().Replace(" ", ""); }
        }

        public bool hasOption(string OptionString)
        {
            return ("," + Options + ",").IndexOf("," + OptionString.ToLower() + ",") >= 0;
        }

        private Int32 privateFirstLine = 1;

        public Int32 FirstLine
        {
            get { return privateFirstLine; }
            set { privateFirstLine = value; }
        }

        private Int32 privateFirstColumn = 1;

        public Int32 FirstColumn
        {
            get { return privateFirstColumn; }
            set { privateFirstColumn = value; }
        }

        private List<Query> privateQueries = new List<Query>();

        public List<Query> Queries
        {
            get { return privateQueries; }
            set { privateQueries = value; }
        }
    }
}