using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion


 // ERROR: Not supported in C#: OptionDeclaration
 // ERROR: Not supported in C#: OptionDeclaration
 // ERROR: Not supported in C#: OptionDeclaration
using System.IO;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Extract.OpenOffice.Calc
{
	public partial class Sheet
	{

		private string privateName = string.Empty;
		public string Name {
			get { return privateName; }
			set { privateName = value.Trim(); }
		}


		private string privateType = "extract";
		public string Type {
			get { return privateType; }
			set { privateType = value.ToLower().Trim(); }
		}


		private string privateTitle = string.Empty;
		public string Title {
			get { return privateTitle; }
			set { privateTitle = value.Trim(); }
		}


		private string privateFile = "*.xls";
		public string File {
			get { return privateFile; }
			set { privateFile = value; }
		}

		public bool SaveName()
		{
			string Name = File;
			DialogResult answer = DialogResult.OK;

			// Replace the string [My Documents] with the current documents directory
			if (Name.LastIndexOf("[My Documents]") >= 0) {
				string DocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				File = Name.Replace("[My Documents]", DocumentsDirectory);
			}

			// If the file is a wildcard item then ask the user for the filename
			if (Name.IndexOfAny(new char[] {
				'*',
				'?'
			}) >= 0) {
				using (SaveFileDialog dlg = new SaveFileDialog()) {
					var _with1 = dlg;
					_with1.AddExtension = true;
					_with1.CheckFileExists = false;
					_with1.DereferenceLinks = true;
					_with1.CreatePrompt = false;
					_with1.CheckPathExists = true;
					_with1.DefaultExt = "xls";
					_with1.FileName = Path.GetFileName(File);
					_with1.InitialDirectory = Path.GetDirectoryName(File);
					_with1.Filter = "Excel 2009 Documents (*.xls)|*.xls|Excel 2010 Documents (*.xlsx)|*.xlsx|Open Office Document (*.ods)|*.ods|All Files (*.*)|*.*";
					_with1.FilterIndex = 1;
					_with1.RestoreDirectory = true;
					_with1.Title = Title;
					_with1.ValidateNames = true;

					answer = _with1.ShowDialog();
					File = _with1.FileName;
				}
			}

			return answer == DialogResult.OK;
		}

		public bool ResolveName()
		{
			string Name = File;
			DialogResult answer = DialogResult.OK;

			// Replace the string [My Documents] with the current documents directory
			if (Name.LastIndexOf("[My Documents]") >= 0) {
				string DocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				File = Name.Replace("[My Documents]", DocumentsDirectory);
			}

			// If the file is a wildcard item then ask the user for the filename
			if (Name.IndexOfAny(new char[] {
				'*',
				'?'
			}) >= 0) {
				using (OpenFileDialog dlg = new OpenFileDialog()) {
					var _with2 = dlg;
					_with2.AddExtension = true;
					_with2.CheckFileExists = true;
					_with2.CheckPathExists = true;
					_with2.DefaultExt = "xls";
					_with2.DereferenceLinks = true;
					_with2.FileName = Path.GetFileName(File);
					_with2.Filter = "Excel 2009 Documents (*.xls)|*.xls|Excel 2010 Documents (*.xlsx)|*.xlsx|Open Office Document (*.ods)|*.ods|All Files (*.*)|*.*";
					_with2.FilterIndex = 1;
					_with2.InitialDirectory = Path.GetDirectoryName(File);
					_with2.Multiselect = false;
					_with2.RestoreDirectory = true;
					_with2.ShowReadOnly = false;
					_with2.Title = Title;
					_with2.ValidateNames = true;

					answer = _with2.ShowDialog();
					File = _with2.FileName;
				}
			}

			return answer == DialogResult.OK;
		}


		private string privateOptions = string.Empty;
		public string Options {
			get { return privateOptions; }
			set { privateOptions = value.ToLower().Replace(" ", ""); }
		}

		public bool hasOption(string OptionString)
		{
			return ("," + Options + ",").IndexOf("," + OptionString.ToLower() + ",") >= 0;
		}


		private Int32 privateFirstLine = 1;
		public Int32 FirstLine {
			get { return privateFirstLine; }
			set { privateFirstLine = value; }
		}


		private Int32 privateFirstColumn = 1;
		public Int32 FirstColumn {
			get { return privateFirstColumn; }
			set { privateFirstColumn = value; }
		}


		private List<Query> privateQueries = new List<Query>();
		public List<Query> Queries {
			get { return privateQueries; }
			set { privateQueries = value; }
		}
	}
}
