using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion


 // ERROR: Not supported in C#: OptionDeclaration
 // ERROR: Not supported in C#: OptionDeclaration
 // ERROR: Not supported in C#: OptionDeclaration
using DevExpress.Utils;
using System.Xml.Serialization;
using System.IO;
using DebtPlus.Reports.Template.Forms;
using System.Data.SqlClient;
using DebtPlus.Interfaces.Desktop;
using System.Windows.Forms;

namespace DebtPlus.UI.Desktop.CS.Extract.OpenOffice.Calc
{
	public partial class Mainline : IDesktopMainline
	{
	    public Mainline() : base() { }

		// global storage for the program
		internal ExcelArgParser ap = new ExcelArgParser(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ExtractOpenOfficeCalc), "configFilePath"));
		private WaitDialogForm Waitdlg;
		private string Waitdlg_caption = string.Empty;
		private DataSet ds;

		private DataTable tbl;
		public void OldAppMain(string[] args)
		{
			if (ap.Parse(args)) {
				try {
					XmlSerializer serializer = new XmlSerializer(typeof(List<Sheet>));
					using (StreamReader inputfile = new StreamReader(ap.CommandFile)) {
						List<Sheet> sheets = (List<Sheet>)serializer.Deserialize(inputfile);
						inputfile.Close();

						// Set the pointer to the new application
						if (sheets.Count > 0) {
							ProcessAllSheets(sheets);
						}
					}

				} catch (Exception ex) {
					DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);

					// Trap all other error conditions here
					DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error processing sheet", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}


		private void ProcessAllSheets(List<Sheet> Sheets)
		{
			// Create the connection to the Calc service
			dynamic oSM = Interaction.CreateObject("com.sun.star.ServiceManager");
			if (oSM == null) {
				throw new ApplicationException("Unable to create ServiceManager object");
			}

			// Create the desktop
			dynamic oDesk = oSM.createInstance("com.sun.star.frame.Desktop");
			if (oDesk == null) {
				throw new ApplicationException("Unable to create a Desktop instance");
			}

			foreach (Sheet sht in Sheets) {
				if (sht.hasOption("exist")) {
					if (sht.ResolveName()) {
						ProcessExistingSheet(oSM, oDesk, sht);
					}
				} else {
					ProcessNewSheet(oSM, oDesk, sht);
				}
			}
		}


		private void ProcessNewSheet(object oSM, object oDesk, Sheet sht)
		{
			dynamic oDoc = oDesk.LoadComponentFromURL("private:factory/scalc", "_blank", 0, dummyArray());
			if (oDoc == null) {
				throw new ApplicationException("Unable to create the file");
			}

			string TemporaryName = string.Empty;
			bool Successful = false;
			try {
				ProcessDocument(oSM, oDesk, oDoc, sht);

				if (sht.hasOption("save")) {
					if (sht.SaveName()) {
						TemporaryName = sht.File;
					}
				} else {
					// Allocate a temporary file name
					TemporaryName = Path.GetTempFileName();

					// Rename the file to have the extension of our current file
					string NewName = TemporaryName + ".ods";
					File.Move(TemporaryName, NewName);
					TemporaryName = NewName;
				}

				// Save the document back to the temporary location
				if (TemporaryName != string.Empty) {
					object[] filterprops = new object[1];
					filterprops[0] = MakePropertyValue(oSM, "FilterName", GetFilterFromName(TemporaryName));
					oDoc.storeAsUrl(GetURL(TemporaryName), filterprops);
					Successful = true;
				}

			} finally {
				oDoc.close(true);
			}

			// Start the editing of the document
			if (TemporaryName != string.Empty && Successful) {
				if (sht.hasOption("show")) {
					Process.Start(TemporaryName);
				}
			}
		}


		private void ProcessExistingSheet(object oSM, object oDesk, Sheet sht)
		{
			// Allocate a temporary file name
			string TemporaryName = null;

			if (sht.hasOption("save")) {
				TemporaryName = sht.Name;
			} else {
				TemporaryName = Path.GetTempFileName();

				// Rename the file to have the extension of our current file
				string Ext = Path.GetExtension(sht.File);
				if (string.IsNullOrEmpty(Ext))
					Ext = ".xls";

				string NewName = TemporaryName + Ext;
				File.Move(TemporaryName, NewName);
				TemporaryName = NewName;

				// Copy the source file to the temporary location and evaluate the extract
				File.Delete(TemporaryName);
				File.Copy(sht.File, TemporaryName);
			}

			// Attempt to open the file
			dynamic oDoc = oDesk.LoadComponentFromURL(GetURL(TemporaryName), "_blank", 0, dummyArray());
			if (oDoc == null) {
				throw new ApplicationException("Unable to open the file " + TemporaryName);
			}

			bool Successful = false;
			try {
				ProcessDocument(oSM, oDesk, oDoc, sht);

				// Save the document back to the temporary location
				if (sht.hasOption("show") || sht.hasOption("save")) {
					object[] filterprops = new object[1];
					filterprops[0] = MakePropertyValue(oSM, "FilterName", GetFilterFromName(TemporaryName));
					oDoc.storeAsUrl(GetURL(TemporaryName), filterprops);
				}

				Successful = true;

			} finally {
				oDoc.close(true);
			}

			// Start the editing of the document
			if (TemporaryName != string.Empty && Successful) {
				if (sht.hasOption("show")) {
					Process.Start(TemporaryName);
				}
			}
		}


		private void ProcessDocument(object oSM, object oDesk, object oDoc, Sheet sht)
		{
			// Evaluate the result set for the various queries on the sheet
			foreach (Query qry in sht.Queries) {
				ProcessQuery(oSM, oDesk, oDoc, sht, qry);
			}
		}

		DateTime ProcessQuery_FromDate;
		DateTime ProcessQuery_ToDate;
		bool ProcessQuery_RequestedDates = false;

		private void ProcessQuery(object oSM, object oDesk, object oDoc, Sheet sht, Query qry)
		{
			// Request the date range if so desired
			if (qry.NeedDateDialog()) {
				if (!ProcessQuery_RequestedDates) {
					ProcessQuery_RequestedDates = true;

					DateReportParametersForm dlg = new DateReportParametersForm();
					DialogResult answer = dlg.ShowDialog();
					ProcessQuery_FromDate = dlg.Parameter_FromDate;
					ProcessQuery_ToDate = dlg.Parameter_ToDate;
					dlg.Dispose();

					// If the answer is not "OK" then terminate the application now.
					if (answer != DialogResult.OK) {
						Application.ExitThread();
						return;
					}
				}

				// For the date values, find the corresponding items in the parameters
				foreach (Parameter parm in qry.Parameters) {
					switch (parm.DialogTypeValue) {
						case Parameter.DialogType.FromDate:
							parm.Value = ProcessQuery_FromDate.ToShortDateString();
							break;
						case Parameter.DialogType.ToDate:
							parm.Value = ProcessQuery_ToDate.ToShortDateString() + " 23:59:59";
							break;
					}
				}
			}

			// Decode the query information
			SqlCommand cmd = qry.BuildCommand();

			Waitdlg_caption = cmd.CommandText;
			if (Waitdlg_caption.Length > 25)
				Waitdlg_caption = Waitdlg_caption.Substring(0, 25);
			Waitdlg = new WaitDialogForm(Waitdlg_caption, "Processing Command");
			Waitdlg.Show();

			// Issue the query
			Cursor current_cursor = Cursor.Current;
			try {
				current_cursor = Cursors.WaitCursor;
				cmd.Connection = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);

				ds = new DataSet("ds");
				using (SqlDataAdapter da = new SqlDataAdapter(cmd)) {
					da.Fill(ds, "table1");
				}

				if (ds.Tables.Count > 0) {
					tbl = ds.Tables[0];
					if (sht.Type == "extract") {
						Process_Extract(oSM, oDesk, oDoc, sht, qry, tbl);
					} else if (sht.Type == "values") {
						Process_Values(oSM, oDesk, oDoc, sht, qry, tbl);
					}
				}

			} catch (SqlException ex) {
				DebtPlus.Svc.Common.ErrorHandling.HandleErrors(ex);
				DebtPlus.Data.Forms.MessageBox.Show(ex.ToString(), "Error reading values from database", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

			} finally {
				cmd.Dispose();
				Cursor.Current = current_cursor;

				// Dispose of the dialog when we are completed
				if (Waitdlg != null) {
					Waitdlg.Close();
					Waitdlg.Dispose();
				}
			}
		}

		private void Process_Extract(object oSM, object oDesk, object oDoc, Sheet sht, Query qry, DataTable tbl)
		{
			dynamic oSpreadSheet = null;

			// If there is no sheet specified, use the first sheet in the system
			// If, by some chance, there are no sheets then create one with a standard name.
			if (sht.Name == string.Empty) {
				oSpreadSheet = oDoc.getSheets().GetByIndex(0);
				if (oSpreadSheet == null) {
					throw new ApplicationException("Spreadsheet is not defined and can not be created");
				}

				Process_Extract(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl);

			} else {
				// If the value is a number, use the ordinal to find the sheet. We start with 1. Calc starts with 0.
				Int32 SheetNumber = -1;
				if (Int32.TryParse(sht.Name, out SheetNumber) && SheetNumber >= 1) {
					oSpreadSheet = oDoc.getSheets().GetByIndex(SheetNumber - 1);
					if (oSpreadSheet != null) {
						Process_Extract(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl);
					}


				} else {
					// Name was specified. Either use the named object or create it.
					dynamic SheetList = oDoc.Sheets();
					if (!SheetList.HasByName(sht.Name)) {
						oDoc.getSheets().insertNewByName(sht.Name, 0);
					}
					oSpreadSheet = oDoc.getSheets().GetByName(sht.Name);
					Process_Extract(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl);
				}
			}
		}

		private void Process_Extract(object oSM, object oDesk, object oDoc, object oSpreadSheet, Sheet sht, Query qry, DataTable tbl)
		{
			Int32 col = default(Int32);
			Int32 FirstLine = sht.FirstLine;

			// Write the first line with the column names
			if (sht.hasOption("headers")) {
				col = sht.FirstColumn - 1;
				foreach (DataColumn DataCol in tbl.Columns) {
					// ignore timestamp columns
					if (!object.ReferenceEquals(DataCol.DataType, typeof(byte[]))) {
						col += 1;
						SetValue(oSpreadSheet, Cell(FirstLine, col), DataCol.ColumnName);
					}
				}
				FirstLine += 1;
			}

			// Load the data for the item
			foreach (DataRow row in tbl.Rows)
            {
				col = sht.FirstColumn - 1;
				foreach (DataColumn DataCol in tbl.Columns)
                {
					string colName = DataCol.ColumnName;

					// Just ignore and remove any byte() objects from the list
					if (object.ReferenceEquals(DataCol.DataType, typeof(byte[])))
                    {
						continue;
					}

					// Advance to the next column
					col += 1;

					// Find the value for the item. If null, ignore it now.
					dynamic value = row[DataCol.Ordinal];
					if (value == null || object.ReferenceEquals(value, System.DBNull.Value))
                    {
						continue;
					}

					// Find the column in the list of columns for this query sheet
					if (qry.Columns != null && qry.Columns.Count > 0)
                    {
                        Extract.OpenOffice.Calc.Column colData = qry.Columns.Find(s => string.Compare(s.Name, colName, true) == 0);
						if (colData != null)
                        {
							value = colData.Translate(value);
						}

						SetValue(oSpreadSheet, Cell(FirstLine, col), value);
					}
				}
				FirstLine += 1;
			}
		}

		private void Retrieve_Values(DataRow row, string Page, string Location, object Value)
		{
			foreach (DataColumn DataCol in row.Table.Columns)
            {
				if (!object.ReferenceEquals(DataCol.DataType, typeof(byte[])))
                {
					switch (DataCol.ColumnName.ToLower())
                    {
						case "sheet":
						case "page":
							Page = Convert.ToString(row[DataCol.Ordinal]);
							break;
						
                        case "location":
						case "cell":
							Location = Convert.ToString(row[DataCol.Ordinal]);
							break;
						
                        case "value":
							Value = row[DataCol.Ordinal];
							break;
						
                        default:
							break;
					}
				}
			}
		}

		private void Process_Values(object oSM, object oDesk, object oDoc, Sheet sht, Query qry, DataTable tbl)
		{
			Int32 RowNumber = 0;

			while (RowNumber < tbl.Rows.Count)
            {
				string CurrentPageName = string.Empty;
				string CurrentLocation = string.Empty;
				object CurrentValue = null;
				Retrieve_Values(tbl.Rows[RowNumber], CurrentPageName, CurrentLocation, CurrentValue);

				// Translate the page name to a spreadsheet location
				Int32 SheetNumber = -1;
				if (Int32.TryParse(CurrentPageName, out SheetNumber) && SheetNumber >= 1) {
					dynamic oSpreadSheet = oDoc.getSheets().GetByIndex(SheetNumber - 1);
					if (oSpreadSheet != null)
                    {
						Process_Values(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl, RowNumber, CurrentPageName);
					}
				}
                else
                {
					// Avoid the missing exception by making sure that the sheet first exists.
					dynamic SheetList = oDoc.Sheets;
					if (!SheetList.HasByName(CurrentPageName))
                    {
						oDoc.getSheets.insertNewByName(CurrentPageName, 0);
					}

					dynamic oSpreadSheet = oDoc.getSheets().GetByName(CurrentPageName);
					if (oSpreadSheet != null)
                    {
						Process_Values(oSM, oDesk, oDoc, oSpreadSheet, sht, qry, tbl, RowNumber, CurrentPageName);
					}
				}
			}
		}


		private void Process_Values(object oSM, object oDesk, object oDoc, object oSpreadSheet, Sheet sht, Query qry, DataTable tbl, Int32 RowNumber, string CurrentPageName)
		{
			// Find the first set of values for the current row
			while (RowNumber < tbl.Rows.Count)
            {
				string Page = string.Empty;
				string Location = string.Empty;
				object Value = null;

				Retrieve_Values(tbl.Rows[RowNumber], Page, Location, Value);
				if (Page != CurrentPageName)
					break;
				RowNumber += 1;

				// Ignore null values
				if (Value == null || object.ReferenceEquals(Value, System.DBNull.Value)) {
					continue;
				}

				// Find any mapping that is needed
				Extract.OpenOffice.Calc.Column colItem = qry.Columns.Find(s => string.Compare(s.Name, Location, true) == 0);
				if (colItem != null) {
					Value = colItem.Translate(Value);
				}

				SetValue(oSpreadSheet, Location, Value);
			}
		}

		private string Cell(Int32 Row, Int32 Col)
		{
			return ColString(Col - 1) + RowString(Row - 1);
		}

		private string RowString(Int32 row)
		{
			if (row < 0) {
				throw new ArgumentOutOfRangeException("row can not be negative");
			}

			return (row + 1).ToString();
		}

		private string ColString(Int32 col)
		{
			if (col < 0) {
				throw new ArgumentOutOfRangeException("col can not be negative");
			}

			if (col < 26) {
				return "ABCDEFGHIJKLMNOPQRSTUVWXYZ".Substring(col, 1);
			}

			return ColString((col / 26) - 1) + ColString(col % 26);
		}

		public object MakePropertyValue(object oSM, string PropName, object PropValue)
		{
			object Result = oSM.Bridge_GetStruct("com.sun.star.beans.PropertyValue");
			var _with1 = Result;
			_with1.Name = PropName;
			_with1.Value = PropValue;

			return Result;
		}

		private void SetValue(object oSpreadSheet, string Location, object Value)
		{
			object oCell = oSpreadSheet.getCellRangeByName(Location);
			if (oCell != null) {
				if (Value is string) {
					oCell.String = Value;
				} else if (Value is DateTime) {
					oCell.String = Convert.ToDateTime(Value).ToShortDateString();
				} else {
					oCell.Value = Value;
				}
			}
		}

		public object dummyArray()
		{
			// creates an empty array for an empty list
			object[] Result = new object[-1 + 1];
			return Result;
		}

		private string GetURL(string FileName)
		{
			string answer = Path.GetFullPath(FileName).Replace('\\', '/');
			answer = answer.Replace(':', '|');
			answer = answer.Replace(" ", "%20");
			return "file:///" + answer;
		}

		private string GetFilterFromName(string FileName)
		{
			string answer = "calc8";

			string ext = Path.GetExtension(FileName).Trim().ToLower();
			switch (ext) {
				case ".xls":
					answer = "MS Excel 97";
					break;
				case ".dif":
					answer = "DIF";
					break;
				case ".htm":
				case ".html":
					answer = "HTML (StarCalc)";
					break;
				case ".ods":
					answer = "calc8";
					break;
				default:
					answer = "calc8";
					break;
			}

			return answer;
		}
	}
}
