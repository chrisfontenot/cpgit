using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
#region "Copyright 2000-2012 DebtPlus, L.L.C."
//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion


 // ERROR: Not supported in C#: OptionDeclaration
 // ERROR: Not supported in C#: OptionDeclaration
 // ERROR: Not supported in C#: OptionDeclaration
using System.Data.SqlClient;
using System.Text;

namespace DebtPlus.UI.Desktop.CS.Extract.OpenOffice.Calc
{
	public partial class Query
	{

		private string privateCommand;
		public string Command {
			get { return privateCommand; }
			set {
				StringBuilder sb = new StringBuilder(value);
				sb.Replace(ControlChars.Cr, "");
				sb.Replace(ControlChars.Lf, "");
				sb.Replace(ControlChars.Tab, "");
				sb.Replace(ControlChars.VerticalTab, "");
				privateCommand = sb.ToString().Trim();
			}
		}


		private List<Parameter> privateParameters = new List<Parameter>();
		public List<Parameter> Parameters {
			get { return privateParameters; }
			set { privateParameters = value; }
		}


		private Int32 privateCommandTimeout = 0;
		public Int32 CommandTimeout {
			get { return privateCommandTimeout; }
			set { privateCommandTimeout = value; }
		}


		private string privateType = "text";
		public string Type {
			get { return privateType; }
			set { privateType = value; }
		}

		public System.Collections.Generic.List<Column> Columns { get; set; }

		public CommandType CommandType()
		{
			CommandType answer = System.Data.CommandType.Text;
			switch (Type.ToLower()) {
				case "proc":
				case "procedure":
				case "storedprocedure":
					answer = System.Data.CommandType.StoredProcedure;
					break;
				case "table":
					answer = System.Data.CommandType.TableDirect;
					break;
			}
			return answer;
		}

		public SqlCommand BuildCommand()
		{
			SqlCommand cmd = new SqlCommand();
			var _with1 = cmd;
			_with1.CommandText = Command;
			_with1.CommandType = CommandType();
			_with1.CommandTimeout = System.Math.Max(DebtPlus.LINQ.SQLInfoClass.getDefault().CommandTimeout, CommandTimeout);
			foreach (Parameter parm in Parameters) {
				_with1.Parameters.Add(parm.BuldParameter);
			}
			return cmd;
		}

		public bool NeedDateDialog()
		{
			bool answer = false;
			foreach (Parameter parm in Parameters) {
				if (parm.DialogTypeValue != Parameter.DialogType.Standard) {
					answer = true;
					break;
				}
			}
			return answer;
		}
	}
}
