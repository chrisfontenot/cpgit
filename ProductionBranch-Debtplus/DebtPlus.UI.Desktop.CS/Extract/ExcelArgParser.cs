using System;
using System.Collections.Generic;

namespace DebtPlus.UI.Desktop.CS.Extract
{
    public partial class ExcelArgParser : DebtPlus.Utils.ArgParserBase
    {
        private readonly string _configFilePath;
        private readonly List<string> _privateFiles = new List<string>();

        public List<string> Files
        {
            get { return _privateFiles; }
        }

        private string _commandFile = string.Empty;

        public string CommandFile
        {
            get { return _commandFile; }
        }

        public ExcelArgParser() : base(new string[] { })
        {
            _configFilePath = "";
        }

        public ExcelArgParser(string configFilePath) : base(new string[] { })
        {
            _configFilePath = configFilePath;
        }

        protected override void OnUsage(string errorInfo)
        {
            if (errorInfo != null)
            {
                AppendErrorLine(string.Format("Invalid parameter: {0}" + Environment.NewLine, errorInfo));
            }

            AppendErrorLine("Usage: DebtPlus.Extract.CSV.exe ControlFile.xml");
        }

        protected override SwitchStatus OnNonSwitch(string switchValue)
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            if (_commandFile == string.Empty)
            {
                _commandFile = ParseAndQualifyFileName(switchValue);
            }
            else
            {
                _privateFiles.Add(switchValue);
            }
            return ss;
        }

        protected override SwitchStatus OnDoneParse()
        {
            SwitchStatus ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.NoError;
            if (_commandFile == string.Empty)
            {
                AppendErrorLine("This program requires a control file.");
                AppendErrorLine("");
                ss = global::DebtPlus.Utils.ArgParserBase.SwitchStatus.ShowUsage;
            }
            return ss;
        }

        public string[] ArgumentList;

        protected new bool Parse(string[] arguments)
        {
            ArgumentList = arguments;
            return base.Parse(arguments);
        }

        private string ParseAndQualifyFileName(string fileArg)
        {
            string fileName = fileArg.Trim();

            // Remove the quotes if there are any
            Int32 fnStart = fileArg.IndexOf("\"");
            if (fnStart >= 0)
            {
                Int32 fnEnd = fileArg.LastIndexOf("\"");
                fileName = fileArg.Substring(fnStart + 1, fnEnd - fnStart - 1);
            }

            // If the file name is not rooted then join it with the current directory
            if (!System.IO.Path.IsPathRooted(fileName))
            {
                fileName = System.IO.Path.Combine(System.Environment.CurrentDirectory, fileName);
            }

            // Ensure that the file exists
            if (!System.IO.File.Exists(fileName))
            {
                DebtPlus.Data.Forms.MessageBox.Show(string.Format("The filename {0} can not be found.", fileName), "Error opening config file", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return null;
            }

            return fileName;
        }
    }
}