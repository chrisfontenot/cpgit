using System;
using System.Data;

using System.Data.SqlClient;

namespace DebtPlus.UI.Desktop.CS.Extract.Fixed
{
    public partial class Parameter
    {
        public enum DialogType
        {
            Standard = 0,
            FromDate = 1,
            ToDate = 2
        }

        private DialogType privateDialogTypeValue = DialogType.Standard;

        public DialogType DialogTypeValue
        {
            get { return privateDialogTypeValue; }
            set { privateDialogTypeValue = value; }
        }

        private string privateName = string.Empty;

        public string Name
        {
            get { return privateName; }
            set { privateName = value; }
        }

        private string privateType = "int";

        public string Type
        {
            get { return privateType; }
            set { privateType = value; }
        }

        private Int32 privateSize = 0;

        public Int32 Size
        {
            get { return privateSize; }
            set { privateSize = value; }
        }

        private string privateValue = null;

        public string Value
        {
            get { return privateValue; }
            set { this.privateValue = value; }
        }

        private byte privateScale = Convert.ToByte(0);

        public byte Scale
        {
            get { return privateScale; }
            set { privateScale = value; }
        }

        private byte privatePrecision = Convert.ToByte(0);

        public byte Precision
        {
            get { return privatePrecision; }
            set { privatePrecision = value; }
        }

        private string privateDirection = "input";

        public string Direction
        {
            get { return privateDirection; }
            set { privateDirection = value; }
        }

        public ParameterDirection GetDirection()
        {
            ParameterDirection answer = ParameterDirection.Input;
            switch (Direction.ToLower())
            {
                case "input":
                    answer = ParameterDirection.Input;
                    break;

                case "inputoutput":
                    answer = ParameterDirection.InputOutput;
                    break;

                case "output":
                    answer = ParameterDirection.Output;
                    break;

                case "returnvalue":
                    answer = ParameterDirection.ReturnValue;
                    break;
            }
            return answer;
        }

        public SqlDbType GetDbType()
        {
            SqlDbType answer = SqlDbType.VarChar;
            switch (Type.ToLower())
            {
                case "bigInt":
                    answer = SqlDbType.BigInt;
                    break;

                case "binary":
                    answer = SqlDbType.Binary;
                    break;

                case "bit":
                    answer = SqlDbType.Bit;
                    break;

                case "char":
                    answer = SqlDbType.Char;
                    break;

                case "date":
                    answer = SqlDbType.Date;
                    break;

                case "datetime":
                    answer = SqlDbType.DateTime;
                    break;

                case "datetime2":
                    answer = SqlDbType.DateTime2;
                    break;

                case "datetimeoffset":
                    answer = SqlDbType.DateTimeOffset;
                    break;

                case "decimal":
                    answer = SqlDbType.Decimal;
                    break;

                case "float":
                    answer = SqlDbType.Float;
                    break;

                case "image":
                    answer = SqlDbType.Image;
                    break;

                case "int":
                    answer = SqlDbType.Int;
                    break;

                case "money":
                    answer = SqlDbType.Money;
                    break;

                case "nchar":
                    answer = SqlDbType.NChar;
                    break;

                case "ntext":
                    answer = SqlDbType.NText;
                    break;

                case "nvarchar":
                    answer = SqlDbType.NVarChar;
                    break;

                case "real":
                    answer = SqlDbType.Real;
                    break;

                case "smalldatetime":
                    answer = SqlDbType.SmallDateTime;
                    break;

                case "smallint":
                    answer = SqlDbType.SmallInt;
                    break;

                case "smallmoney":
                    answer = SqlDbType.SmallMoney;
                    break;

                case "structured":
                    answer = SqlDbType.Structured;
                    break;

                case "text":
                    answer = SqlDbType.Text;
                    break;

                case "time":
                    answer = SqlDbType.Time;
                    break;

                case "timestamp":
                    answer = SqlDbType.Timestamp;
                    break;

                case "tinyint":
                    answer = SqlDbType.TinyInt;
                    break;

                case "udt":
                    answer = SqlDbType.Udt;
                    break;

                case "uniqueidentifier":
                    answer = SqlDbType.UniqueIdentifier;
                    break;

                case "varbinary":
                    answer = SqlDbType.VarBinary;
                    break;

                case "varchar":
                    answer = SqlDbType.VarChar;
                    break;

                case "variant":
                    answer = SqlDbType.Variant;
                    break;

                case "xml":
                    answer = SqlDbType.Xml;
                    break;
            }

            return answer;
        }

        public object GetValue()
        {
            object answer = DBNull.Value;
            if (Value != null)
            {
                switch (Type.ToLower())
                {
                    case "bit":
                        bool BooleanAnswer = false;
                        if (bool.TryParse(Value, out BooleanAnswer))
                            answer = BooleanAnswer;
                        break;

                    case "date":
                    case "datetime":
                    case "smalldatetime":
                        System.DateTime DateAnswer = default(System.DateTime);
                        if (System.DateTime.TryParse(Value, out DateAnswer))
                            answer = DateAnswer;
                        break;

                    case "decimal":
                    case "money":
                        decimal DecimalAnswer = default(decimal);
                        if (decimal.TryParse(Value, out DecimalAnswer))
                            answer = DecimalAnswer;
                        break;

                    case "float":
                    case "real":
                        double DoubleAnswer = 0;
                        if (double.TryParse(Value, out DoubleAnswer))
                            answer = DoubleAnswer;
                        break;

                    case "int":
                    case "smallint":
                    case "tinyint":
                        Int32 IntAnswer = default(Int32);
                        if (Int32.TryParse(Value, out IntAnswer))
                            answer = IntAnswer;
                        break;

                    default:
                        answer = Value;
                        break;
                }
            }
            return answer;
        }

        public SqlParameter BuldParameter()
        {
            SqlParameter answer = new SqlParameter();
            var _with1 = answer;
            _with1.Direction = GetDirection();
            _with1.SqlDbType = GetDbType();
            _with1.ParameterName = Name;
            _with1.Value = GetValue();
            _with1.SourceVersion = DataRowVersion.Current;
            _with1.Size = Size;
            _with1.Scale = Scale;
            _with1.Precision = Precision;
            return answer;
        }
    }
}