#region "Copyright 2000-2012 DebtPlus, L.L.C."

//{*******************************************************************}
//{                                                                   }
//{       DebtPlus Debt Management System                             }
//{                                                                   }
//{       Copyright 2000-2012 DebtPlus, L.L.C.                        }
//{       ALL RIGHTS RESERVED                                         }
//{                                                                   }
//{   The entire contents of this file is protected by U.S. and       }
//{   International Copyright Laws. Unauthorized reproduction,        }
//{   reverse-engineering, and distribution of all or any portion of  }
//{   the code contained in this file is strictly prohibited and may  }
//{   result in severe civil and criminal penalties and will be       }
//{   prosecuted to the maximum extent possible under the law.        }
//{                                                                   }
//{   RESTRICTIONS                                                    }
//{                                                                   }
//{   THE SOURCE CODE CONTAINED WITHIN THIS FILE AND ALL RELATED      }
//{   FILES OR ANY PORTION OF ITS CONTENTS SHALL AT NO TIME BE        }
//{   COPIED, TRANSFERRED, SOLD, DISTRIBUTED, OR OTHERWISE MADE       }
//{   AVAILABLE TO OTHER INDIVIDUALS WITHOUT EXPRESS WRITTEN CONSENT  }
//{   AND PERMISSION FROM DEBTPLUS, L.L.C.                            }
//{                                                                   }
//{   CONSULT THE END USER LICENSE AGREEMENT FOR INFORMATION ON       }
//{   ADDITIONAL RESTRICTIONS.                                        }
//{                                                                   }
//{*******************************************************************}

#endregion "Copyright 2000-2012 DebtPlus, L.L.C."

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Serialization;
using DebtPlus.Interfaces.Desktop;
using DebtPlus.Reports.Template.Forms;
using DevExpress.Utils;

namespace DebtPlus.UI.Desktop.CS.Extract.Fixed
{
    public partial class Mainline : IDesktopMainline
    {
        public Mainline() : base()
        {
        }

        public void OldAppMain(string[] args)
        {
            var thrd = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(delegate(object param)
            {
                try
                {
                    var ap = new ExcelArgParser(DebtPlus.Configuration.Config.GetConfigValueForKey(DebtPlus.Configuration.Config.GetConfigSettings(DebtPlus.Configuration.Config.ConfigSet.ExtractFixed), "configFilePath"));
                    if (ap.Parse((string[])param))
                    {
                        // Parse and load the parameter file contents
                        XmlSerializer serializer = new XmlSerializer(typeof(List<Sheet>));
                        using (var inputfile = new StreamReader(ap.CommandFile))
                        {
                            List<Sheet> sheets = (List<Sheet>)serializer.Deserialize(inputfile);
                            inputfile.Close();

                            // Process each sheet entry to generate the output file
                            foreach (Sheet sht in sheets)
                            {
                                if (!ProcessSheet(ap, sht))
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                catch (System.Exception ex)
                {
                    // Handle the error conditions
                    DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex);
                }
            }))
            {
                IsBackground = false,
                Name = "DisbursementEdit"
            };

            thrd.SetApartmentState(System.Threading.ApartmentState.STA);
            thrd.Start((object)args);
        }

        private string GetFileName(ExcelArgParser ap, Sheet sht)
        {
            string Fname = sht.File;

            // Try to find a match to the command line parameters. We will take any filename referenced in the list.
            Regex rx = new Regex("\\[CommandLine\\:\\s*(?<argument>\\d+)\\]");
            Match FoundMatch = rx.Match(Fname);
            Int32 Argument = default(Int32);
            if (FoundMatch != null && Int32.TryParse(FoundMatch.Groups[1].Value, out Argument))
            {
                if (Argument <= ap.Files.Count)
                {
                    Fname = ap.Files[Argument - 1];
                }
            }

            // Replace the string [My Documents] with the current documents directory
            if (Fname.LastIndexOf("[My Documents]") >= 0)
            {
                string DocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                Fname = Fname.Replace("[My Documents]", DocumentsDirectory);
            }

            // If the file is a wildcard item then ask the user for the filename
            if (Fname.IndexOfAny(new char[] { '*', '?' }) >= 0)
            {
                using (var dlg = new OpenFileDialog()
                {
                    AddExtension = true,
                    CheckFileExists = false,
                    CheckPathExists = true,
                    DefaultExt = "txt",
                    DereferenceLinks = true,
                    FileName = Fname,
                    Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*",
                    FilterIndex = 0,
                    InitialDirectory = Path.GetDirectoryName(Fname),
                    Multiselect = false,
                    RestoreDirectory = true,
                    ShowReadOnly = false,
                    Title = sht.Title,
                    ValidateNames = true
                })
                {
                    if (dlg.ShowDialog() != DialogResult.OK)
                    {
                        return string.Empty;
                    }
                    Fname = dlg.FileName;
                }
            }

            return Fname;
        }

        // Issue the query to retrieve the results set
        private DateTime static_ProcessSheet_FromDate;
        private DateTime static_ProcessSheet_ToDate;
        private bool ProcessSheet_RequestedDates = false;

        // Decode the query information
        public bool ProcessSheet(ExcelArgParser ap, Sheet sht)
        {
            SqlDataReader rd = null;

            // Create the output file
            string OutputName = GetFileName(ap, sht);
            if (OutputName == string.Empty)
            {
                return false;
            }

            // Open the file. It will create it if needed else simply overwrite it.
            StreamWriter outputfile = new StreamWriter(OutputName, false, Encoding.ASCII, 4096);
            try
            {
                // Change the line ending sequence to match what is requested
                switch (sht.LineEnd.ToLower())
                {
                    case "cr":
                        outputfile.NewLine = "\r";
                        break;

                    case "lf":
                        outputfile.NewLine = "\n";
                        break;

                    case "lf,cr":
                        outputfile.NewLine = "\r\n";
                        break;

                    case "":
                        outputfile.NewLine = string.Empty;
                        break;

                    default:
                        outputfile.NewLine = Environment.NewLine;
                        break;
                }

                // If there is a prefix line, write it
                if (sht.Prefix != string.Empty)
                {
                    outputfile.WriteLine(sht.Prefix);
                }

                WaitDialogForm dlg = null;
                foreach (Query qry in sht.Queries)
                {
                    // Request the date range if so desired
                    if (qry.NeedDateDialog())
                    {
                        if (!ProcessSheet_RequestedDates)
                        {
                            ProcessSheet_RequestedDates = true;
                            using (var dateDlg = new DateReportParametersForm())
                            {
                                if (dateDlg.ShowDialog() != DialogResult.OK)
                                {
                                    return false;
                                }

                                static_ProcessSheet_FromDate = dateDlg.Parameter_FromDate;
                                static_ProcessSheet_ToDate   = dateDlg.Parameter_ToDate;
                            }
                        }

                        // For the date values, find the corresponding items in the parameters
                        foreach (Parameter parm in qry.Parameters)
                        {
                            switch (parm.DialogTypeValue)
                            {
                                case Parameter.DialogType.FromDate:
                                    parm.Value = static_ProcessSheet_FromDate.ToShortDateString();
                                    break;

                                case Parameter.DialogType.ToDate:
                                    parm.Value = static_ProcessSheet_ToDate.ToShortDateString() + " 23:59:59";
                                    break;
                            }
                        }
                    }

                    if (dlg == null)
                    {
                        dlg = new WaitDialogForm(qry.Command, "Processing command:");
                        dlg.Show();
                    }

                    SqlConnection cn = new SqlConnection(DebtPlus.LINQ.SQLInfoClass.getDefault().ConnectionString);
                    try
                    {
                        cn.Open();
                        SqlCommand cmd = qry.BuildCommand();
                        cmd.Connection = cn;
                        rd = cmd.ExecuteReader(CommandBehavior.CloseConnection | CommandBehavior.SingleResult);

                        if (rd != null && rd.Read() && rd.FieldCount > 0)
                        {
                            // If we are to auto-fill the column names then do so now
                            if (sht.hasOption("autofill"))
                            {
                                qry.Columns.Clear();

                                // Populate the list of columns from the input set
                                for (Int32 FieldNo = 0; FieldNo <= rd.FieldCount - 1; FieldNo++)
                                {
                                    string FieldName = rd.GetName(FieldNo);
                                    if (string.IsNullOrEmpty(FieldName))
                                    {
                                        FieldName = string.Format("Col{0:000}", FieldNo + 1);
                                    }
                                    Column col = new Column();
                                    col.Name = FieldName;
                                    col.Ordinal = FieldNo;
                                    qry.Columns.Add(col);
                                }
                            }
                            else
                            {
                                // Find the ordinal positions for the fields by field name
                                for (Int32 FieldNo = 0; FieldNo <= qry.Columns.Count - 1; FieldNo++)
                                {
                                    var col = qry.Columns[FieldNo];
                                    col.Ordinal = FindOrdinal(ref rd, col.Name);
                                }
                            }

                            // If we need to write the field names then do so here before the first record
                            if (sht.hasOption("fieldnames"))
                            {
                                StringBuilder sb = new StringBuilder();
                                for (Int32 FieldNo = 0; FieldNo <= qry.Columns.Count - 1; FieldNo++)
                                {
                                    var col = qry.Columns[FieldNo];
                                    if (col.Spacer)
                                    {
                                        sb.Append(col.AlignString(string.Empty));
                                    }
                                    else
                                    {
                                        if (col.Ordinal >= 0)
                                        {
                                            sb.Append(col.Encode(col.Name));
                                        }
                                    }
                                }
                                outputfile.WriteLine(sb.ToString());
                            }

                            // Process the records until there are no more
                            do
                            {
                                StringBuilder sb = new StringBuilder();
                                for (Int32 FieldNo = 0; FieldNo <= qry.Columns.Count - 1; FieldNo++)
                                {
                                    var col = qry.Columns[FieldNo];
                                    if (col.Spacer)
                                    {
                                        sb.Append(col.AlignString(string.Empty));
                                    }
                                    else
                                    {
                                        if (col.Ordinal >= 0)
                                        {
                                            sb.Append(col.Encode(rd[col.Ordinal]));
                                        }
                                    }
                                }
                                outputfile.WriteLine(sb.ToString());
                            } while (!(!rd.Read()));
                        }
                    }
                    catch (SqlException ex)
                    {
                        DebtPlus.UI.Common.ErrorHandling.HandleErrors(ex, "Error reading information");
                    }
                    finally
                    {
                        if (cn != null)
                        {
                            cn.Dispose();
                        }

                        if (dlg != null)
                        {
                            dlg.Close();
                            dlg = null;
                        }
                    }
                }

                // If there is a suffix line, write it
                if (sht.Suffix != string.Empty)
                {
                    outputfile.WriteLine(sht.Suffix);
                }
            }
            finally
            {
                // Close the file
                outputfile.Close();
                outputfile.Dispose();
                outputfile = null;
            }

            return true;
        }

        private Int32 FindOrdinal(ref SqlDataReader rd, string Name)
        {
            Int32 answer = -1;
            for (Int32 FieldNo = 0; FieldNo <= rd.FieldCount - 1; FieldNo++)
            {
                if (string.Compare(Name, rd.GetName(FieldNo), true) == 0)
                {
                    answer = FieldNo;
                    break;
                }
            }
            return answer;
        }
    }
}