using System.Collections.Generic;

namespace DebtPlus.UI.Desktop.CS.Extract.Fixed
{
    public partial class Sheet
    {
        private string privateTitle = string.Empty;

        public string Title
        {
            get { return privateTitle; }
            set { privateTitle = value.Trim(); }
        }

        private string privateFile = "*.xls";

        public string File
        {
            get { return privateFile; }
            set { privateFile = value; }
        }

        private string privateOptions = string.Empty;

        public string Options
        {
            get { return privateOptions; }
            set { privateOptions = value.ToLower().Replace(" ", ""); }
        }

        public bool hasOption(string OptionString)
        {
            return ("," + Options + ",").IndexOf("," + OptionString.ToLower() + ",") >= 0;
        }

        private string privateLineEnd = "CR,LF";

        public string LineEnd
        {
            get { return privateLineEnd; }
            set { privateLineEnd = value.ToUpper().Replace(" ", ""); }
        }

        private string privatePrefix = string.Empty;

        public string Prefix
        {
            get { return privatePrefix; }
            set { privatePrefix = value; }
        }

        private string privateSuffix = string.Empty;

        public string Suffix
        {
            get { return privateSuffix; }
            set { privateSuffix = value; }
        }

        private List<Query> privateQueries = new List<Query>();

        public List<Query> Queries
        {
            get { return privateQueries; }
            set { privateQueries = value; }
        }
    }
}