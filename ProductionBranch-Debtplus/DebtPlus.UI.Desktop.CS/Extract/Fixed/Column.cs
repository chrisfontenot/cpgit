using System;

using System.Xml.Serialization;

namespace DebtPlus.UI.Desktop.CS.Extract.Fixed
{
    public partial class Column
    {
        public enum AlignEnum
        {
            Left = 0,
            Right = 1
        }

        private AlignEnum privateAlign = AlignEnum.Left;

        [XmlIgnore]
        public AlignEnum Align
        {
            get { return privateAlign; }
            set { privateAlign = value; }
        }

        private Int32 privateOrdinal = -1;

        public Int32 Ordinal
        {
            get { return privateOrdinal; }
            set { privateOrdinal = value; }
        }

        public string privateName = string.Empty;

        public string Name
        {
            get { return privateName; }
            set { privateName = value.Trim(); }
        }

        private bool privateSpacer = false;

        public bool Spacer
        {
            get { return privateSpacer; }
            set { privateSpacer = value; }
        }

        public string privateFormat = string.Empty;

        public string Format
        {
            get { return privateFormat; }
            set { privateFormat = value; }
        }

        public char privateFill = ' ';

        public char Fill
        {
            get { return privateFill; }
            set { privateFill = value; }
        }

        private Int32 privateLength = 0;

        [XmlIgnore]
        public Int32 PaddLeft
        {
            get { return privateLength; }
            set
            {
                privateLength = value;
                Align = AlignEnum.Right;
            }
        }

        [XmlIgnore]
        public Int32 PaddRight
        {
            get
            {
                return privateLength;
            }
            set
            {
                privateLength = value;
                Align = AlignEnum.Left;
            }
        }

        private string Right(string InputString, Int32 Size)
        {
            if (InputString.Length > Size)
            {
                InputString = InputString.Substring(InputString.Length - Size);
            }
            return InputString;
        }

        private string Left(string InputString, Int32 Size)
        {
            if (InputString.Length > Size)
            {
                InputString = InputString.Substring(0, Size);
            }
            return InputString;
        }

        public string Encode(object obj)
        {
            string answer = string.Empty;

            if (!object.ReferenceEquals(obj, DBNull.Value))
            {
                if (Format != string.Empty)
                {
                    answer = string.Format(Format, obj);
                }
                else
                {
                    answer = obj.ToString();
                }
            }

            return AlignString(answer);
        }

        public string AlignString(string InputString)
        {
            // If the field has a size then adjust the string to the desired length
            if (privateLength <= 0)
            {
                return string.Empty;
            }

            if (Align == AlignEnum.Right)
            {
                return Right(InputString.PadLeft(privateLength, Fill), privateLength);
            }

            return Left(InputString.PadRight(privateLength, Fill), privateLength);
        }
    }
}