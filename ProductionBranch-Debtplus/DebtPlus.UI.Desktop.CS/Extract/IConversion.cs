using System;

namespace DebtPlus.UI.Desktop.CS.Extract
{
    public interface IConversion : IDisposable
    {
        string ToText(object InputString);
    }
}