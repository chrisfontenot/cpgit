using System;

namespace DebtPlus.UI.Desktop.CS.Extract
{
    public partial class RTFText : IConversion, IDisposable
    {
        public RTFText()
        {
        }

        public override string ToString()
        {
            return string.Empty;
        }

        public string ToText(object inputText)
        {
            // NULLS are always an empty string and are stopped here
            if (inputText == null || object.ReferenceEquals(inputText, System.DBNull.Value))
            {
                return string.Empty;
            }

            // If the source is not a string then just use the normal conversion
            if (!(inputText is string))
            {
                return Convert.ToString(inputText);
            }

            // Convert the input to a string
            string strText = inputText as string;
            if (strText == null)
            {
                return string.Empty;
            }

            // If the item is not RTF then just use the text as given
            if (!global::DebtPlus.Utils.Format.Strings.IsRTF(strText))
            {
                return strText;
            }

            // Convert the text string from RTF text to suitable text
            using (DevExpress.XtraRichEdit.RichEditControl ctl = new DevExpress.XtraRichEdit.RichEditControl())
            {
                try
                {
                    ctl.RtfText = strText;
                    return ctl.Text;
                }
#pragma warning disable 168
                catch (ArgumentException ex)
                {
                }
#pragma warning restore 168
            }

            // Return the un-changed text if this is not valid RTF text
            return strText;
        }

        #region "IDisposable Support"

        // To detect redundant calls
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                }
            }
            this.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion "IDisposable Support"
    }
}