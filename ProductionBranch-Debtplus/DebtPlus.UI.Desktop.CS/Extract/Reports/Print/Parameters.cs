namespace DebtPlus.UI.Desktop.CS.Reports.Print
{
    internal partial class Parameters
    {
        public string name;
        public string value;

        public Parameters(string name, string value)
        {
            this.name = name;
            this.value = value;
        }
    }
}