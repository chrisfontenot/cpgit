namespace DebtPlus.UI.Desktop.CS.Extract.ARM.v40
{
    public partial class ArgParser : DebtPlus.Utils.ArgParserBase
    {
        public ArgParser() : base(new string[] { })
        {
        }

        public ArgParser(string[] switchSymbols) : base(switchSymbols)
        {
        }

        public ArgParser(string[] switchSymbols, bool caseSensitiveSwitches) : base(switchSymbols, caseSensitiveSwitches)
        {
        }

        public ArgParser(string[] switchSymbols, bool caseSensitiveSwitches, string[] switchChars) : base(switchSymbols, caseSensitiveSwitches, switchChars)
        {
        }

        /// <summary>
        ///     Generate the command usage information
        /// </summary>
        protected override void OnUsage(string errorInfo)
        {
            // deprecated
        }
    }
}