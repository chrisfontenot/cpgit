USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_original_debt]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_original_debt] AS
-- ===========================================================================
-- ==       Show the original debt for active clients                       ==
-- ===========================================================================

SELECT		top 100 percent
		cc.client			as 'client',
		sum(bal.orig_balance)		as 'orig_balance',
		c.start_date			as 'start_date',
		dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as 'name'

from		client_creditor cc with (nolock)
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
left outer join	people p with (nolock) on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
inner join	clients c with (nolock) on cc.client = c.client

where	cc.reassigned_debt = 0
and	c.active_status in ('A', 'AR')

group by cc.client, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix, c.start_date
order by 1
GO
