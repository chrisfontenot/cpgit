USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_offices]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_client_offices] as

-- ==============================================================================================
-- ==       Used to generate client letters with the corresponding office data                 ==
-- ==============================================================================================

select	c.client, o.office, o.name,
		dbo.format_Address_Line_1(oa.house, oa.direction, oa.street, oa.suffix, oa.modifier, oa.modifier_value) as address1,
		oa.address_line_2 as address2,
		oa.city,
		st.mailingcode as state,
		oa.postalcode,
		o.directions,
		dbo.format_TelephoneNumber(o.TelephoneID) as phone,
		dbo.format_TelephoneNumber(o.faxID) as fax,
		dbo.format_TimeZoneLabel(st.TimeZoneID, default) as TimeZone,
		d.ManagerName
from clients c with (nolock)
left outer join offices o with (nolock) on c.office = o.office
left outer join addresses oa with (nolock) on o.addressid = oa.address
left outer join states st with (nolock) on oa.state = st.state
left outer join districts d with (nolock) on o.district = d.district
GO
