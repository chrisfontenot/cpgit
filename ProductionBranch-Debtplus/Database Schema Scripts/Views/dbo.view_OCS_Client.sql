USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_OCS_Client]    Script Date: 06/18/2015 12:37:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[view_OCS_Client] as


SELECT ocs.[Id]
      ,[UploadRecord]
      ,ocs.[ClientId]
      , case when contact.[StatusCode] is null then 0 else contact.[StatusCode] end as [StatusCode]
      , case when contact.[ContactAttempts] is null then 0 else contact.[ContactAttempts] end as [ContactAttempts]
      , CONVERT(bit,case when ocs.[ActiveFlag] is null then 1 else ocs.[ActiveFlag] end) as [ActiveFlag]
      ,[Archive]
      ,[IsDuplicate]
      ,[UploadAttempt]
      ,[InvestorNumber]
      ,[InvestorDueDate]
      ,[InvestorLastChanceList]
      ,[InvestorActualSaleDate]
      ,[Program]
      ,[ClaimedDate]
      ,[ClaimedBy]
      ,[SearchTimezone]
      ,[SearchServicerId]
      ,contact.[Timestamp]
  FROM OCS_Client [ocs]
  LEFT JOIN OCS_ContactAttempt [contact] on 
      ocs.ClientId = contact.ClientId AND 
      contact.Id = (select top 1 Id from OCS_ContactAttempt where ClientId = ocs.ClientId order by [End] desc)



