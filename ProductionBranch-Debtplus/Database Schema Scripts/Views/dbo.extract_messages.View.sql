USE [DebtPlus]
GO
/****** Object:  View [dbo].[extract_messages]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[extract_messages] AS
select top 100 percent item_type, item_value, description, additional, nfcc, rpps, epay, note, [default], activeflag, hud_9902_section
from messages
order by 1, 2
GO
