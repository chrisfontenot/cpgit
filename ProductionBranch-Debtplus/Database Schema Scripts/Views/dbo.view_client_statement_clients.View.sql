USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_statement_clients]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_statement_clients] as

-- ========================================================================================
-- ==         Return information for the client statement header area                    ==
-- ========================================================================================

select	cs.client_statement_batch,
	cs.client,
	cs.last_deposit_date,
	cs.last_deposit_amt,
	cs.deposit_amt,
	cs.refund_amt,
	cs.disbursement_amt,
	cs.held_in_trust,
	cs.reserved_in_trust,
	cs.starting_trust_balance,
	cs.counselor,
	cs.office,
	cs.expected_deposit_date,
	cs.expected_deposit_amt,
	cs.delivery_method,
	cs.delivery_date,
	cs.active_status,
	cs.active_debts,

	dbo.format_counselor_name ( co.person ) as counselor_name,
	o.name as office_name,

	upper(v.name)  as address_1,
	upper(v.addr1) as address_2,
	upper(v.addr2) as address_3,
	upper(v.addr3) as address_4,
	upper(replace(replace(isnull(v.zipcode,''),'-',''),' ','')) as postalcode,

	-- Used for OCR encoding on the statement
	convert(int,0) as company_id,
	
	-- Used to select clients for printing
	c.mail_error_date,
	c.ElectronicStatements,
	c.ElectronicCorrespondence

from	client_statement_clients cs with (nolock)
left outer join counselors co with (nolock) on cs.counselor = co.counselor
left outer join offices o with (nolock) on cs.office = o.office
left outer join clients c with (nolock) on cs.client = c.client
left outer join people  p with (nolock) on cs.client = p.client and 1 = p.relation
left outer join view_client_address v WITH (NOLOCK) ON cs.client = v.client
GO
