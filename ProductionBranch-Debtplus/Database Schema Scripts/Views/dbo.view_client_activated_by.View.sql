USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_activated_by]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_activated_by] AS
SELECT     c.client, cn.client AS note_client, cn.date_created, cn.created_by, vu.name
FROM         dbo.clients AS c INNER JOIN
                      dbo.client_notes AS cn ON cn.client = c.client INNER JOIN
                      dbo.view_users AS vu ON vu.Person = cn.created_by
WHERE     (cn.note LIKE '%changed active_status from %') AND (cn.client_note > 50000000)
GO
