USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_office_addresses]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_office_addresses] as
-- ====================================================================================
-- ==        Find a mailing address for each office. Used by the HUD reports         ==
-- ====================================================================================
select	o.office,
		case
			when o.AddressID is null then dbo.format_address_line_1(cnfa.house, cnfa.direction, cnfa.street, cnfa.suffix, cnfa.modifier, cnfa.modifier_value)
			else dbo.format_address_line_1(oa.house, oa.direction, oa.street, oa.suffix, oa.modifier, oa.modifier_value)
		end as address_1,

		case
			when o.AddressID is null then cnfa.address_line_2
			else oa.address_line_2
		end as address_2,

		case
			when o.AddressID is null then cnfa.address_line_3
			else oa.address_line_3
		end as address_3,

		case
			when o.AddressID is null then dbo.format_city_state_zip(cnfa.city, cnfa.state, cnfa.postalcode)
			else dbo.format_city_state_zip(oa.city, oa.state, oa.postalcode)
		end as address_4,

		case
			when o.AddressID is null then cnfa.city
			else oa.city
		end as city,
		
		case
			when o.addressid is null then cnfst.MailingCode
			else oast.MailingCode
		end as MailingCode,
		
		case
			when o.addressid is null then cnfa.PostalCode
			else oa.PostalCode
		end as PostalCode
		
from	offices o with (nolock)
left outer join config cnf      with (nolock) on 1             = cnf.company_id
left outer join addresses oa    with (nolock) on o.addressid   = oa.address
left outer join addresses cnfa  with (nolock) on cnf.AddressID = cnfa.address
left outer join states    oast  with (nolock) on oa.state      = oast.state
left outer join states    cnfst with (nolock) on cnfa.state    = cnfst.state
GO
