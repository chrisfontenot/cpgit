USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_unposted_deposits]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_unposted_deposits] as
-- =============================================================================================
-- ==            List of the un-posted transactions                                           ==
-- =============================================================================================

select	d.deposit_batch_detail as client_register,
	case b.batch_type when 'CL' then 'DP' when 'CR' then 'RF' when 'AC' then 'DP' else null end as tran_type,
	d.tran_subtype as tran_subtype,
	d.client as client,
	null as trust_register,
	null as disbursement_register,
	d.item_date as item_date,
	d.amount as credit_amt,
	null as debit_amt,
	d.reference as message,
	d.date_created as date_created,
	d.created_by as created_by

from	deposit_batch_details d
inner join deposit_batch_ids b on d.deposit_batch_id = b.deposit_batch_id
where	b.date_posted is null
and	(b.batch_type = 'AC' or b.date_closed is not null)
and	((d.tran_subtype = 'AC' and d.ach_authentication_code is null and d.ach_transaction_code in (27, 37))
or	(d.tran_subtype = 'LB' and d.ok_to_post = 1))
or	d.tran_subtype not in ('AC', 'LB')
GO
