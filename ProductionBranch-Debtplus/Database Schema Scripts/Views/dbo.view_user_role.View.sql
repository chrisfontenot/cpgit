USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_user_role]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_user_role]
AS
SELECT     c.Counselor, t.Attribute AS role, MIN(ca.Attribute) AS attr
FROM         dbo.counselors AS c INNER JOIN
                      dbo.counselor_attributes AS ca ON c.Counselor = ca.Counselor INNER JOIN
                      dbo.AttributeTypes AS t ON ca.Attribute = t.oID
WHERE     (t.Grouping = 'role')
GROUP BY c.Counselor, t.Attribute
GO
