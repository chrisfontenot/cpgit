USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_debt_creditor_list]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[view_debt_creditor_list] as

-- ===================================================================================================
-- ==     Used by the debt creation to find the creditor in the creditors table                     ==
-- ===================================================================================================
SELECT c.creditor_id as creditor_id, c.creditor as creditor, c.sic,
       pct.fairshare_pct_eft, pct.fairshare_pct_check,
	   coalesce(c.creditor_name+' '+c.division,c.creditor_name,c.division,'') as name,
	   c.comment, dbo.format_contribution_type(pct.creditor_type_eft,pct.creditor_type_check) as fairshare,
	   dbo.address_block_6(v.attn,v.addr1,v.addr2,v.addr3,v.addr4,v.addr5) as address,
	   c.prohibit_use
FROM creditors c WITH (NOLOCK)
LEFT OUTER JOIN view_creditor_addresses v WITH (NOLOCK) ON c.creditor=v.creditor AND 'P' = v.type
LEFT OUTER JOIN creditor_contribution_pcts pct WITH (NOLOCK) ON c.creditor_contribution_pct = pct.creditor_contribution_pct
GO
