USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_drop_reasons]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_drop_reasons] as
-- =================================================================================================
-- ==       Return the drop reasons for the specific client. It is used by the letters.           ==
-- =================================================================================================

select	c.client, c.active_status, c.drop_date, case
		when active_status != 'I' then null
		when c.drop_reason is null then c.drop_reason_other
		else coalesce(d.description, c.drop_reason_other, 'Unspecified Termination Reason')
	end as drop_reason
from	clients c with (nolock)
left outer join drop_reasons d with (nolock) on c.drop_reason = d.drop_reason
GO
