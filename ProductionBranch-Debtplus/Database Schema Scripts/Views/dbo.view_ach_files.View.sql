USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_ach_files]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_ach_files] AS

-- ============================================================================================================
-- ==       Return a list of the ACH deposit batches in the system.                                          ==
-- ============================================================================================================

SELECT TOP 100 PERCENT
	deposit_batch_id	AS 'ach_file',
	trust_register,
	ach_pull_date		AS 'pull_date',
	ach_settlement_date	AS 'settlement_date', 
        ach_effective_date	AS 'effective_date',
	ach_file		AS 'filename',
	date_closed,
	date_posted,
	posted_by,
	date_created,
	created_by,
	note			AS 'message'
FROM    deposit_batch_ids WITH (NOLOCK)
WHERE   (batch_type = 'AC')

ORDER BY deposit_batch_id
GO
