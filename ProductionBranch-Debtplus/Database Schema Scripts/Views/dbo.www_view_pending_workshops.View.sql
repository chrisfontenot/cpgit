USE [DebtPlus]
GO
/****** Object:  View [dbo].[www_view_pending_workshops]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[www_view_pending_workshops] as

-- ==============================================================================================
-- ==       Return a list of the next 40 workshops in the future.
-- ==============================================================================================

select top 99
	w.workshop					as IDWorkshop,
	dbo.www_workshop_mailto ( w.workshop )		as HTMLText,
	convert(varchar(8), w.start_time, 1)		as StartDate,
	ltrim(right(convert(varchar, w.start_time),8))	as StartTime,
	wl.name						as WorkshopLocation,
	dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value)	as WorkshopAddress1,
	a.address_line_2								as WorkshopAddress2,

	dbo.format_city_state_zip(a.city, a.state, a.postalcode) + 
	dbo.www_map_address ( dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value), a.address_line_2, a.city, a.state, a.postalcode ) as WorkshopAddress3,

	dbo.Format_TelephoneNumber ( wl.TelephoneID )		as WorkshopPhone,
	wt.description					as WorkshopName,
	isnull(w.seats_available,0)-isnull((select sum(case when workshop_people < 1 then 1 else workshop_people end) from client_appointments ca where ca.workshop = w.workshop and ca.status in ('P','K','W')),0) as SeatsAvailable,
	w.workshop_type					as WorkshopType

from	workshops w with (nolock)
inner join workshop_locations wl with (nolock) on w.workshop_location = wl.workshop_location
left outer join addresses a with (nolock) on wl.addressid = a.address
left outer join states st with (nolock) on a.state = st.state
inner join workshop_types wt with (nolock) on w.workshop_type = wt.workshop_type
where start_time > getdate()
and	isnull(w.seats_available,0)-isnull((select sum(case when workshop_people < 1 then 1 else workshop_people end) from client_appointments ca where ca.workshop = w.workshop and ca.status in ('P','K','W')),0) > 0
order by start_time
GO
