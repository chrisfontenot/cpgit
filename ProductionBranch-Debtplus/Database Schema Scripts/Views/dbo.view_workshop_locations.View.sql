USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_workshop_locations]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_workshop_locations] as
select	w.workshop,
		wl.workshop_location,
		wl.name,
		dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as address_1,
		a.address_line_2 as address_2,
		a.address_line_3 as address_3,
		dbo.format_city_state_zip(a.city, a.state, a.postalcode) as address_4,
		wl.directions
from	workshops w with (nolock)
left outer join workshop_locations wl on w.workshop_location = wl.workshop_location
left outer join addresses a on wl.addressid = a.address
GO
