USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_assets]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_assets] as
	select	client, sum(asset_amount) as asset_amount
	from	assets with (nolock)
	group by client
GO
