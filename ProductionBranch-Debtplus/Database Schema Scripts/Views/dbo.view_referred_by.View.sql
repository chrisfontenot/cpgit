USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_referred_by]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_referred_by] as
SELECT top 100 percent
		r.referred_by as 'referred_by',
		r.description as 'description',
		r.note as 'notes',
		c.description as 'class'
FROM		referred_by r
LEFT OUTER JOIN	referred_by_types c ON c.referred_by_type = r.referred_by_type
ORDER BY	c.description,r.description
GO
