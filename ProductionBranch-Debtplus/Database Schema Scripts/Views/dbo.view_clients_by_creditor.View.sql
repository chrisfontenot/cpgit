USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_clients_by_creditor]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_clients_by_creditor] AS
-- ============================================================================================
-- ==                    Select the creditor disbursments                                    ==
-- ============================================================================================

-- ChangeLog
--   1/19/2001
--   - Added support for new definition of the last_payment_date and last_payment_amount field.

select		cr.creditor									as 'creditor',
			isnull(cr.creditor_name, 'UN-CODED')		as 'creditor_name',
			rcc.date_created							as 'last_payment_date',
			rcc.debit_amt								as 'last_payment_amount',
			c.client									as 'client',
			isnull(cc.reassigned_debt,0)				as 'reassigned_debt',
			dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			c.active_status								as 'status',
	        c.start_date								as 'start_date',
			isnull(cc.message,cc.account_number)		as 'account_number',

			bal.orig_balance + bal.orig_balance_adjustment	as 'orig_balance',
			bal.total_payments								as 'principle_payments',
			bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as 'current_balance',

			isnull(ccl.zero_balance,0)						as 'ccl_zero_balance',
			isnull(ccl.always_disburse,0)					as 'always_disburse',
			isnull(ccl.agency_account,0)					as 'agency_account'

FROM		clients c with (nolock)
INNER JOIN	client_creditor cc with (nolock) ON c.client = cc.client
INNER JOIN	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	people p with (nolock) ON c.client = p.client AND 1 = p.relation
LEFT OUTER JOIN Names pn with (nolock) on p.NameID = pn.Name
LEFT OUTER JOIN	creditors cr with (nolock) ON cc.creditor = cr.creditor
LEFT OUTER JOIN registers_client_creditor rcc with (nolock) on cc.last_payment = rcc.client_creditor_register
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
GO
