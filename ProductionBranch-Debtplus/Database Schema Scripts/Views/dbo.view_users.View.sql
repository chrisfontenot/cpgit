USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_users]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_users] as

select	co.Counselor, co.NameID, co.EmailID, co.TelephoneID, co.Person, co.[default], co.ActiveFlag, co.Office, co.Menu_Level, co.Color, co.date_created, co.created_by,
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as name,
		emx.address as email,
		dbo.format_TelephoneNumber(co.telephoneID) as telephone
from	Counselors co
left outer join Names cox on co.NameID = cox.Name
left outer join EmailAddresses emx on co.EmailID = emx.Email
GO
