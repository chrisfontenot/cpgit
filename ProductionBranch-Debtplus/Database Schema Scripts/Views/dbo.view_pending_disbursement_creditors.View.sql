USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_pending_disbursement_creditors]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_pending_disbursement_creditors] AS

-- ===================================================================================================================
-- ==             Obtain the information for the debts that are to be disbursed                                     ==
-- ===================================================================================================================

-- ChangeLog
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

SELECT		c.disbursement_register				as 'disbursement_register',
		c.client					as 'client',
		dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) AS name,
		c.creditor					as 'creditor',
		c.client_creditor				as 'client_creditor', 
       	c.tran_type					as 'tran_type',
		c.creditor_type					as 'creditor_type',
		c.sched_payment					as 'sched_payment', 
       	c.current_month_disbursement			as 'current_month_disbursement',
		c.orig_balance					as 'orig_balance',
		c.current_balance				as 'current_balance', 
		c.apr						as 'apr',
		c.debit_amt					as 'disbursed',

		case c.creditor_type
			when 'BW' then isnull(pct.fairshare_pct_eft,0)
			else isnull(pct.fairshare_pct_check,0)
		end						as 'fairshare_pct',

		c.fairshare_amt					as 'fairshare_amt',
		cr.creditor_name				as 'creditor_name'

FROM		disbursement_creditors c WITH (NOLOCK)
INNER JOIN	creditors cr WITH (NOLOCK) ON c.creditor = cr.creditor
LEFT OUTER JOIN	creditor_contribution_pcts pct WITH (NOLOCK) ON cr.creditor_contribution_pct = pct.creditor_contribution_pct
LEFT OUTER JOIN	people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
GO
