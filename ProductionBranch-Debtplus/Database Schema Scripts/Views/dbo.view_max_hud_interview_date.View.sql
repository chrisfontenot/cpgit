USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_max_hud_interview_date]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_max_hud_interview_date]
AS
SELECT     client, MAX(interview_date) AS max_hud_int_date
FROM         dbo.hud_interviews
GROUP BY client
GO
