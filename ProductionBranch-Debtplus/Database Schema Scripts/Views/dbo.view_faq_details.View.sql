USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_faq_details]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_faq_details] as
select		d.*,
		convert(varchar(20), case d.grouping when 2 then 'Existing Client' when 3 then 'Other' else 'New Client' end) as formatted_grouping,
		reason.grouping	as 'formatted_reason_group',
		reason.description	as formatted_reason_for_call,
		referral.description	as formatted_referral,
		disposition.description	as formatted_disposition,
		isnull(d.name, dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)) as formatted_client_name

from		faq_details d with (nolock)

left outer join	faq_items reason with (nolock) on d.reason_for_call = reason.faq_item
left outer join	faq_items disposition with (nolock) on d.disposition = disposition.faq_item
left outer join referred_by referral with (nolock) on d.referral = referral.referred_by
left outer join people p with (nolock) on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.name
GO
