USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_est_payout]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_est_payout] as
	select	c.client, dateadd(m,sf.plan_months,c.start_date) as est_payout_date
	from	clients c with (nolock)
	left outer join sales_files sf on c.client = sf.client
GO
