USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_resource_detail]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_resource_detail] as
-- ==============================================================================================
-- ==       Retrieve the resource information                                                  ==
-- ==============================================================================================
SELECT TOP 100 PERCENT
		t.[description]								as 'description',
		r.[name]									as 'name',
		isnull(dbo.format_Address_Line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),'') as 'address1',
		isnull(a.address_line_2,'')					as 'address2',
		isnull(dbo.format_city_state_zip (a.city, a.state, a.postalcode),'') as 'address3',
		isnull(dbo.format_TelephoneNumber ( r.TelephoneID ),'')    as 'telephone',
		isnull(dbo.format_TelephoneNumber ( r.FaxID ),'')          as 'fax',
		isnull(em.[Address],'')						as 'email',
		isnull(r.www,'')							as 'www',
		g.office									as 'office',
		r.resource_type								as 'resource_type',
		r.resource									as 'resource'

FROM		resources r WITH (NOLOCK)
LEFT OUTER JOIN	resource_regions g WITH (NOLOCK)	ON r.resource		= g.resource
LEFT OUTER JOIN	resource_types t WITH (NOLOCK)	ON r.resource_type	= t.resource_type
LEFT OUTER JOIN EmailAddresses em WITH (NOLOCK) ON r.EmailID = em.Email
LEFT OUTER JOIN addresses a WITH (NOLOCK) ON r.AddressID = a.address

ORDER BY	1,  2
GO
