USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_hud_languages]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_hud_languages] as

-- ===============================================================================
-- ==      languages spoken by the agency                                       ==
-- ===============================================================================

select distinct t.HUDLanguage as 'language'
from counselor_attributes col
inner join AttributeTypes t on col.Attribute = t.oID
inner join Counselors co on col.Counselor = co.Counselor
where	co.ActiveFlag	= 1
and		t.Grouping = 'LANGUAGE'
GO
