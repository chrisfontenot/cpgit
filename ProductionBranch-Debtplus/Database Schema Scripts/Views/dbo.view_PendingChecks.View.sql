SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [view_PendingChecks] AS

-- =====================================================================================
-- ==       List of the pending checks                                                ==
-- =====================================================================================
select	checknum			as 'checknum',
	date_created			as 'date',
	credit_amt			as 'credit',
	debit_amt			as 'debit',
	payee				as 'payee'
FROM	view_trust_register
WHERE	cleared = ' '
GO
