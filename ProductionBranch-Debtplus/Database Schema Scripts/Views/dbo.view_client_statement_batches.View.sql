USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_statement_batches]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_statement_batches] as

select	top 50
	client_statement_batch, disbursement_date, note, period_start, period_end, statement_date, created_by, date_created
from	client_statement_batches with (nolock)
order by statement_date desc
GO
