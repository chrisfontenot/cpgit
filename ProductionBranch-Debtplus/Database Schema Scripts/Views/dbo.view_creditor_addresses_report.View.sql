USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_addresses_report]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_creditor_addresses_report] as

-- =================================================================================================
-- ==        View used for the report "creditor names and addresses"                              ==
-- =================================================================================================

SELECT cr.[creditor],
	   a.creditor_prefix_1 as addr_1,
	   a.creditor_prefix_2 as addr_2,
	   dbo.format_address_line_1 (a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as addr_3,
	   a.address_line_2 as addr_4,
	   a.address_line_3 as addr_5,
	   dbo.format_city_state_zip (a.city, a.state, a.postalcode) as addr_6,
	   
	   p.[creditor_type_check],
	   p.[creditor_type_eft],
	   p.[fairshare_pct_check],
	   p.[fairshare_pct_eft],
	   
	   cr.lowest_apr_pct as apr_1,
	   cr.[medium_apr_pct] as apr_2,
	   cr.[highest_apr_pct] as apr_3,
	   cr.[sic],

	   cr.[prohibit_use]
FROM creditors cr WITH (NOLOCK)
LEFT OUTER JOIN creditor_addresses ca WITH (NOLOCK) ON cr.creditor = ca.creditor AND 'P' = ca.type
LEFT OUTER JOIN addresses a WITH (NOLOCK) ON ca.addressid = a.address
LEFT OUTER JOIN creditor_contribution_pcts p WITH (NOLOCK) ON cr.creditor_contribution_pct = p.creditor_contribution_pct
GO
