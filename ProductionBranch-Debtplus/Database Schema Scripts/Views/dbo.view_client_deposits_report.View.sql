USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_deposits_report]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_deposits_report] as
select	'DP'					as 'tran_type',
		isnull(d.tran_subtype,'OT')	as 'tran_subtype',
		isnull(dt.description,'Other')	as 'tran_subtype_description',

		d.ok_to_post			as 'ok_to_post',
		d.deposit_batch_id		as 'deposit_batch_id',
		d.client				as 'client',
		d.amount				as 'credit_amt',
		d.item_date				as 'item_date',
		d.reference				as 'reference',
		d.created_by			as 'created_by',
		d.date_created			as 'date_created',

		dbo.format_reverse_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'client_name',
		d.deposit_batch_detail	as 'deposit_batch_detail',
		bnk.bank_name			as 'bank_name',
		bnk.account_number		as 'bank_account_number'

from		deposit_batch_details d
inner join      deposit_batch_ids db on d.deposit_batch_id = db.deposit_batch_id
left outer join	tran_types dt on d.tran_subtype = dt.tran_type
left outer join people p on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.name
left outer join banks bnk with (nolock) on db.bank = bnk.bank
GO
