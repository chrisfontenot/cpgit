USE [DebtPlus]
GO
/****** Object:  View [dbo].[report_custom_Dropped_DMP_by_District]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[report_custom_Dropped_DMP_by_District] as
-- =========================================================================================
-- ==     Dataset for the report custom_Dropped_DMP_by_District                           ==
-- =========================================================================================

SELECT	c.client,
		c.active_status,
		c.active_status_date,
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as name,
		c.drop_date,
		d.description,
		d.district
FROM   clients c with (nolock)
LEFT OUTER JOIN counselors co with (nolock) ON c.counselor = co.counselor
LEFT OUTER JOIN names cox with (nolock) on co.nameid = cox.name
LEFT OUTER JOIN offices o with (nolock) ON c.office = o.office
LEFT OUTER JOIN districts d with (nolock) ON o.district = d.district
GO
