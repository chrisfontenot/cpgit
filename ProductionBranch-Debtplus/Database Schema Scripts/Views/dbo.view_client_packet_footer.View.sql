USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_packet_footer]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_packet_footer] as
	SELECT c.client, dbo.format_client_id(c.client) as formatted_client, cnf.name as agency_name, dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as client_name FROM clients c LEFT OUTER JOIN people p on c.client = p.client AND 1 = p.relation LEFT OUTER JOIN names pn on p.nameid = pn.name LEFT OUTER JOIN config cnf ON cnf.company_id = 1
GO
