USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_tickler_list]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_tickler_list] AS

select	x.tickler			as 'tickler',			-- Primary key
		x.counselor			as 'counselor',			-- Counselor ID
		x.client			as 'client',			-- Client
		t.description		as 'description',		-- Description of the event
		x.note				as 'note',				-- Message
		x.date_effective	as 'date_effective',	-- Effective date
		x.date_created		as 'date_created',		-- Date item was created
		x.created_by		as 'created_by',		-- Person who created the item
		x.priority			as 'priority'			-- Priority of the item

from	ticklers x with (nolock)
left outer join TicklerTypes t with (nolock) on x.tickler_type = t.oID
inner join counselors co with (nolock) on x.counselor = co.counselor
where	x.date_deleted is null
GO
