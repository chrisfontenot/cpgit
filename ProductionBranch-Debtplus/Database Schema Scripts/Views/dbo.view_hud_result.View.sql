USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_hud_result]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_hud_result] AS
SELECT		iv.hud_interview,
			iv.client,
			m.description AS result
FROM		dbo.hud_interviews iv
INNER JOIN	Housing_ResultTypes m ON iv.hud_result = m.oID
GO
