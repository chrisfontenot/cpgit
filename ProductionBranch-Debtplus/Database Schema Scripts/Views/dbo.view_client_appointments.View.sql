IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'view_client_appointments' AND TYPE='V')
	EXEC ('CREATE VIEW  [dbo].[view_client_appointments] AS SELECT 1 AS X')
GO
ALTER VIEW  [dbo].[view_client_appointments] AS
-- ==================================================================================================
-- ==            List of the client appointments                                                   ==
-- ==================================================================================================

select ca.client_appointment,
  ca.client																										as 'client',
  ca.start_time																									as 'appt_time',
  isnull(t.appt_name,'ANY')																						as 'appt_type',
  convert(varchar(50),left(isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),''),50))	as 'counselor',

  case ca.status
   when 'K' then 'CO'
   when 'M' then 'NS'
   when 'R' then 'RS'
   when 'C' then 'XC'
   when 'P' then 'PE'
   when 'W' then 'WI'
  end																											as 'status',

  isnull(ca.result,'')																							as 'client_status',
  convert(varchar(80),isnull(o.name, 'None'))																	as 'appt_office',
  ca.date_confirmed																								as 'appt_confirmed',
  convert(varchar(50),m.description)																			as 'appt_confirmed_status',
  ca.office,
  ca.workshop,
  ref.description																								as 'referral',
  ca.partner																									as 'partner_code'
from client_appointments ca WITH (NOLOCK)
LEFT OUTER JOIN appt_types t WITH (NOLOCK) ON ca.appt_type = t.appt_type
INNER JOIN offices o WITH (NOLOCK) ON ca.office = o.office
LEFT OUTER JOIN counselors co WITH (NOLOCK) ON ca.counselor = co.counselor
LEFT OUTER JOIN names cox       WITH (NOLOCK) ON co.NameID      = cox.name
LEFT OUTER JOIN clients c WITH (NOLOCK) ON ca.client = c.client
LEFT OUTER JOIN messages m WITH (NOLOCK) ON ca.confirmation_status = m.item_value and 'APPT CONFIRMATION' = m.item_type
LEFT OUTER JOIN referred_by ref WITH (NOLOCK) ON ca.referred_by = ref.referred_by
WHERE ca.workshop is null
AND  ca.office is not null

union all

select ca.client_appointment,
  ca.client																										as 'client',
  ca.start_time																									as 'appt_time',
  isnull(wkt.description,'Workshop')																			as 'appt_type',
  isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'')									as 'counselor',
  case ca.status
   when 'K' then 'CO'
   when 'M' then 'NS'
   when 'R' then 'RS'
   when 'C' then 'XC'
   when 'P' then 'PE'
   when 'W' then 'WI'
  end																											as 'status',
  coalesce(ca.result,'')																						as 'client_status',
  wko.name																										as 'appt_office',
  ca.date_confirmed																								as 'appt_confirmed',
  m.description																									as 'appt_confirmed_status',
  ca.office,
  ca.workshop,
  ref.description																								as 'referral',
  ca.partner																									as 'partner_code'

from client_appointments ca WITH (NOLOCK)
LEFT OUTER JOIN clients c WITH (NOLOCK) ON ca.client = c.client
INNER JOIN workshops wk    WITH (NOLOCK) ON ca.workshop    = wk.workshop
LEFT OUTER JOIN counselors wkco WITH (NOLOCK) ON wk.counselor   = wkco.person
LEFT OUTER JOIN names cox       WITH (NOLOCK) ON wkco.NameID    = cox.name
LEFT OUTER JOIN workshop_locations wko WITH (NOLOCK) ON wk.workshop_location = wko.workshop_location
LEFT OUTER JOIN workshop_types     wkt WITH (NOLOCK) ON wk.workshop_type = wkt.workshop_type
LEFT OUTER JOIN messages m WITH (NOLOCK) ON ca.confirmation_status = m.item_value and 'APPT CONFIRMATION' = m.item_type
LEFT OUTER JOIN referred_by ref WITH (NOLOCK) ON ca.referred_by = ref.referred_by

WHERE ca.workshop is not null
AND  ca.office is null
GO
GRANT SELECT ON view_client_appointments TO public AS dbo;
GO
DENY UPDATE,INSERT,DELETE ON view_client_appointments TO public;
GO
DENY SELECT ON view_client_appointments TO www_role;
GO
