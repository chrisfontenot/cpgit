USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_primary_lender]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_primary_lender] as
	select	h.client											as 'client',
			loan.oID											as 'secured_loan',
			coalesce(svc.description, lender.servicerName,'')	as 'lender',
			loan.CurrentLoanBalanceAmt							as 'balance',
			convert(int,500)									as 'secured_type'
	from	client_housing h with (nolock)
	inner join housing_properties prop with (nolock) on h.client = prop.HousingID and 1 = prop.UseInReports
	inner join housing_loans loan with (nolock) on prop.oID = loan.PropertyID and 1 = loan.UseInReports
	left outer join housing_lenders lender with (nolock) on loan.CurrentLenderID = lender.oID
	left outer join housing_lender_servicers svc with (nolock) on lender.ServicerID = svc.oID
GO
