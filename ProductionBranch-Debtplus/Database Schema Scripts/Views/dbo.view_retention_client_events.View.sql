USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_retention_client_events]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_retention_client_events] AS
-- ============================================================================================================
-- ==          List the retention event information                                                        ==*/
-- ============================================================================================================
SELECT	ce.client_retention_event,
		ce.client,
		c.counselor AS client_counselor,
		c.office AS client_office,
		COALESCE (ce.priority, ced.priority, 9) AS priority, 
		ce.retention_event,
		ce.amount AS event_amount,
		ce.message AS event_message,
		ce.date_created,
		dbo.format_counselor_name ( ce.created_by ) as created_by,
		dbo.format_normal_name(p1n.prefix, p1n.first, p1n.middle, p1n.last, p1n.suffix) AS name,
		dbo.format_normal_name(p2n.prefix, p2n.first, p2n.middle, p2n.last, p2n.suffix) AS co_name,
		c.active_status, 
		
        dbo.format_TelephoneNumber(c.HomeTelephoneID) AS home_ph,
        dbo.format_TelephoneNumber(p1.WorkTelephoneID) AS work_ph,
        dbo.format_TelephoneNumber(c.MsgTelephoneID) AS message_ph,
        
        ced.description AS event_description,
        (
			SELECT  MAX(date_created) AS Expr1
			FROM	dbo.client_retention_actions AS x
            WHERE   (client_retention_event = ce.client_retention_event)
        ) AS action_date,
        ce.date_expired,
        ce.expire_type,
        ce.expire_date, 
        ce.effective_date
        
FROM			dbo.client_retention_events AS ce WITH (NOLOCK)
INNER JOIN		dbo.clients AS c WITH (NOLOCK) ON ce.client = c.client
LEFT OUTER JOIN	dbo.retention_events AS ced WITH (NOLOCK) ON ce.retention_event = ced.retention_event

LEFT OUTER JOIN	dbo.people AS p1 WITH (NOLOCK) ON ce.client = p1.client AND 1 = p1.relation
LEFT OUTER JOIN	Names p1n with (nolock) ON p1.NameID = p1n.Name

LEFT OUTER JOIN	dbo.people AS p2 WITH (NOLOCK) ON ce.client = p2.client AND 1 <> p2.relation
LEFT OUTER JOIN	Names p2n with (nolock) ON p2.NameID = p2n.Name
WHERE	ce.effective_date is null
or		ce.effective_date < convert(varchar(10), dateadd(d, 1, getdate()), 101)
GO
