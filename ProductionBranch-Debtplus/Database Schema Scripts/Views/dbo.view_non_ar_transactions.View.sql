USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_non_ar_transactions]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_non_ar_transactions] AS
SELECT		a.non_ar_register	as 'non_ar_register',
		a.non_ar_source		as 'source',
		a.dst_ledger_account	as 'ledger',
		a.credit_amt		as 'amount',
		a.date_created		as 'date_created',
		a.created_by		as 'created_by',
		a.message		as 'message',
		s.description		as 'description'

FROM		registers_non_ar a WITH (NOLOCK)
LEFT OUTER JOIN	non_ar_sources s WITH (NOLOCK) ON a.non_ar_source = s.non_ar_source
WHERE		tran_type		= 'NA'
GO
