USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_appt_time_templates]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_appt_time_templates]
AS
SELECT		t.appt_time_template,
		o.name AS 'office', 
		convert (varchar, t.dow) + ' ' + datename ( dw, dateadd ( d, t.dow, '4/21/2001' )) as 'dow',
		RIGHT('00' + CONVERT(varchar(2), t.start_time / 60), 2) + ':' + RIGHT('00' + CONVERT(varchar(2), t.start_time % 60), 2) AS start_hour,
		UPPER(ISNULL(a.appt_name, 'ANY TYPE')) AS 'type',
		1 as available_slots,
		1 as full_schedule
FROM		appt_time_templates t WITH (NOLOCK)
INNER JOIN	offices o WITH (NOLOCK) ON t.office = o.office
LEFT OUTER JOIN	appt_types a WITH (NOLOCK) ON t.appt_type = a.appt_type
GO
