USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_disbursement_register]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_disbursement_register] AS
SELECT
	disbursement_register		as 'batch_id',
	date_created			as 'date',
	created_by			as 'creator',

	case
		when tran_type = 'AD' and date_posted is null then 'C AD ' + convert(varchar, disbursement_date)
		when tran_type = 'AD' and date_posted is not null then 'O AD ' + convert(varchar, disbursement_date)
		else '  ' + tran_type
	end				as type,

	prior_balance			as 'prior trust balance',
	post_balance			as 'post trust balance',
	deduct_balance			as 'deduct adjustment'

FROM	registers_disbursement WITH (NOLOCK)
GO
