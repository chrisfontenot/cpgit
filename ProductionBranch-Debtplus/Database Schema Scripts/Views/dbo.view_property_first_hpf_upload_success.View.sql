SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [view_property_first_hpf_upload_success] AS
SELECT     top 100 percent hp.HousingID AS client, hpfs1.oID, hpfs1.propertyID, hpfs1.date_created
FROM       HPFSubmissions hpfs1
INNER JOIN housing_properties hp on hpfs1.propertyID = hp.oID
WHERE	   hpfs1.requestType = 'SUBMIT' AND hpfs1.responseStatus = 'SUCCESS'
AND        hpfs1.date_created = (select MIN(date_created) FROM HPFSubmissions hpfs2 WHERE hpfs1.propertyID = hpfs2.propertyID and hpfs2.requestType = 'SUBMIT' AND hpfs2.responseStatus = 'SUCESS')
ORDER BY   3, 1, 4
GO
