USE [DebtPlus]
GO
/****** Object:  View [dbo].[update_budgets]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[update_budgets] as

-- ================================================================================================
-- ==       List of the budget information                                                       ==
-- ================================================================================================

select	budget					as budget,
	client					as client,

	convert(varchar(50), case isnull(budget_type,'F')
		when 'F' then 'Followup'
		else 'Initial'
	end)					as note,

	date_created				as date_created,
	dbo.format_counselor_name(created_by)	as created_by,

	convert(datetime, null)			as date_updated,
	convert(varchar(80),null)		as updated_by

from	budgets with (nolock)
GO
