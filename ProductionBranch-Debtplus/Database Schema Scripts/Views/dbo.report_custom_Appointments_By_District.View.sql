USE [DebtPlus]
GO
/****** Object:  View [dbo].[report_custom_Appointments_By_District]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[report_custom_Appointments_By_District] AS
-- =========================================================================================
-- ==     Dataset for the reports custom_Appointments_By_District                         ==
-- =========================================================================================

SELECT	c.client,
		c.active_status,
		ca.status,
		ca.result,
		ca.start_time,
		o.name,
		d.description,
		apt.appt_name,
		ca.client_appointment,
		d.district,
		m.nfcc as contact_type,
		ca.counselor,
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as counselor_name,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as client_name
FROM	clients c
LEFT OUTER JOIN client_appointments ca ON c.client = ca.client
INNER JOIN offices o ON ca.office = o.office
INNER JOIN appt_types apt ON ca.appt_type = apt.appt_type
INNER JOIN districts d ON o.district = d.district
inner join FirstContactTypes m on apt.contact_type = m.oID
left outer join counselors co on ca.counselor = co.counselor
left outer join people p on ca.client = p.client and 1 = p.relation
left outer join names pn on p.nameid = pn.name
left outer join names cox on co.nameid = cox.name
WHERE	c.office is not null
and		ca.status <> 'R'
GO
