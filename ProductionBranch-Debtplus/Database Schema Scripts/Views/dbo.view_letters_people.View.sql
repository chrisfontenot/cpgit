USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_letters_people]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_letters_people] as
-- ================================================================================================
-- ==            View used by the letter formatting routine to obtain information about people   ==
-- ================================================================================================
select	convert(int,p.person) as person, p.client, p.relation,	-- needed to find the appopriate record
		pn.prefix, pn.first, pn.middle, pn.last, pn.suffix,
		p.birthdate,
		p.gross_income,
		p.net_income,
		dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'normal_name',
		dbo.format_reverse_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'reverse_name',
		dbo.format_ssn(p.ssn) as 'ssn',
		dbo.format_TelephoneNumber(p.WorkTelephoneID) as 'work_ph',
		dbo.format_TelephoneNumber_BaseOnly(p.WorkTelephoneID) as 'base_work_ph',
		dbo.format_TelephoneNumber(p.CellTelephoneID) as 'cell_ph',
		dbo.format_TelephoneNumber_BaseOnly(p.CellTelephoneID) as 'base_cell_ph'
from people p with (nolock)
left outer join names pn with (nolock) on p.nameid = pn.name
GO
