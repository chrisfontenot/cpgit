USE [DebtPlus]
GO
/****** Object:  View [dbo].[custom_view_inactive_clients]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[custom_view_inactive_clients]
AS
SELECT     client, client_status, office, counselor, active_status
FROM         dbo.clients

WHERE active_status IN ('I')
GO
