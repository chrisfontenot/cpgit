USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_debt_notes]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_debt_notes] as
select	note_amount,
	note_text,
	date_created,
	created_by,
	debt_note,
	client,
	creditor,
	client_creditor,
	convert(int,case when type IN ('PR','DR') then 1 when output_batch is null then 0 else 1 end) as locked
from	debt_notes with (nolock)
where	type in ('AN', 'DR', 'CN', 'SN', 'MS')		-- AN = account note, DR = drop, PR = proposal, CN = client note, SN = agency (system) note
GO
