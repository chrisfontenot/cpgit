USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_rpps_cie_info]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_rpps_cie_info] AS

-- =====================================================================================================================
-- ==           Retrieve the information for the RPS CIE file format                                                  ==
-- =====================================================================================================================

-- Changelog
--   10/18/06
--     Force Deduct creditors to be NET billers so that gross/net is always sent.
--   2/18/2008
--     .Net Support
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- In the rare case where you must send an ABA number, see the comments below

SELECT	t.rpps_transaction									as 'rpps_transaction',
	t.bank											as 'bank',
	t.rpps_file										as 'rpps_file',
	t.creditor										as 'creditor',
	t.transaction_code									as 'transaction_code',
	t.client										as 'client',

	case when rcc.creditor_type = 'D' then rcc.fairshare_amt else 0 end			as 'deducted',
	case when isnull(rcc.creditor_type,'N') in ('N', 'D') then 0 else rcc.fairshare_amt end	as 'billed',

	rcc.debit_amt as 'amount',

	ISNULL(v.account_number, 'MISSING')							AS 'account_number',
	v.proposal_client_name										AS 'account_name',

	-- Choose one of the following
	-- r.rpps_biller_id										as 'rpps_biller_id',	-- for the proper biller ID
	r.biller_aba										as 'rpps_biller_id',		-- for the ABA number
	
	
	r.reversal_biller_id									as 'reversal_biller_id',
	r.biller_name										as 'biller_name',

	-- If sending deduct amounts, send it as a NET biller. RPPS will reject if it is truely not a NET biller.
	case
		when rcc.creditor_type = 'D' then 1
		else r.biller_type
	end											as 'biller_type',

	-- Fields needed for .NET support
	t.trace_number_first,
	t.trace_number_last,
	t.rpps_batch

FROM	rpps_transactions t 

LEFT OUTER JOIN	registers_client_creditor rcc	WITH (NOLOCK) ON t.client_creditor_register = rcc.client_creditor_register
INNER JOIN	rpps_biller_ids r		WITH (NOLOCK) ON t.biller_id = r.rpps_biller_id
LEFT OUTER JOIN	view_last_payment v		WITH (NOLOCK) ON rcc.client_creditor = v.client_creditor

WHERE	t.service_class_or_purpose = 'CIE'
AND	t.transaction_code in ('22','23')
AND	t.trace_number_first is null
GO
