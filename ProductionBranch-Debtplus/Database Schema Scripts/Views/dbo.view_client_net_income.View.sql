USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_net_income]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_net_income] as
	select	client, sum(net_income) as net_income
	from	people with (nolock)
	group by client
GO
