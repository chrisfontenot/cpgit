USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_statement_details]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_client_statement_details] as

-- ========================================================================================
-- ==         Return information for the client statement detail area                    ==
-- ========================================================================================

select	cd.client_statement_detail,
	cd.client_statement_batch,
	cd.client,
	cd.client_creditor,
	cd.current_balance,
	cd.debits,
	cd.credits,
	cd.debits - cd.credits as net_debits,
	cd.interest,
	cd.interest_rate,
	cd.last_payment,
	cd.sched_payment,
	cd.payments_to_date,
	cd.original_balance,
	cc.creditor,
	dbo.statement_account_number(cc.account_number) as account_number,
	isnull(cr.creditor_name,cc.creditor_name) as creditor_name,
	cc.message,
	dbo.statement_account_number(cc.account_number) as formatted_account_number,

	rcc.debit_amt		as last_payment_amt,
	rcc.date_created	as last_payment_date,
	rcc.tran_type		as last_payment_type

from	client_statement_details cd with (nolock)
left outer join client_creditor cc with (nolock) on cd.client_creditor = cc.client_creditor
left outer join registers_client_creditor rcc with (nolock) on cc.last_payment = rcc.client_creditor_register
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
GO
