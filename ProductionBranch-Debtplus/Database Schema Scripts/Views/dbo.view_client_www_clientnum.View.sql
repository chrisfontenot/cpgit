USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_www_clientnum]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_www_clientnum]
AS
SELECT     CAST(UserName AS int) AS client, PKID
FROM         dbo.client_www
GO
