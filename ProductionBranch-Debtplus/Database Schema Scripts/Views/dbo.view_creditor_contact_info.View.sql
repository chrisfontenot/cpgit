USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_contact_info]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_creditor_contact_info] AS

SELECT	creditor_contact				as 'creditor_contact',
	creditor					as 'creditor',
	creditor_contact_type				as 'creditor_contact_type',
	
	isnull(n.prefix,'')				as 'prefix',
	isnull(n.first,'')				as 'first',
	isnull(n.middle,'')				as 'middle',
	isnull(n.last,'')					as 'last',
	isnull(n.suffix,'')				as 'suffix',
	isnull(title,'')				as 'title',
	isnull(dbo.format_Address_Line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),'')				as 'address1',
	isnull(a.address_line_2,'')				as 'address2',
	isnull(ltrim(rtrim(a.city)),'')			as 'city',
	isnull(ltrim(rtrim(st.MailingCode)),'')		as 'state',
	isnull(a.PostalCode, '')				as 'zipcode',
	isnull(dbo.format_TelephoneNumber ( cc.TelephoneID ),'')	as 'phone',
	''								as 'extension',
	isnull(dbo.format_TelephoneNumber ( cc.FAXID ),'')	as 'fax',
	isnull(e.[Address],'')				as 'email',
	isnull(notes,'')				as 'notes'

FROM	creditor_contacts cc with (NOLOCK)
LEFT OUTER JOIN Names n WITH (NOLOCK) ON cc.NameID = n.Name
LEFT OUTER JOIN addresses a WITH (NOLOCK) ON cc.AddressID = a.address
LEFT OUTER JOIN EmailAddresses e with (nolock) on cc.EmailID = e.Email
LEFT OUTER JOIN states st with (nolock) ON a.state = st.state
GO
