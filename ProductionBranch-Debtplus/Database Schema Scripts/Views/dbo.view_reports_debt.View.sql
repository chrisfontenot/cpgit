USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_reports_debt]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_reports_debt] as
	SELECT	report		as 'report',
			menu_name	as 'menu_name',
			assembly	as 'assembly'
	FROM	reports with (nolock)
	WHERE	type = 'DB'
	and		isnull(menu_name,'') <> ''
	and		isnull(assembly,'') <> ''
GO
