USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_appt_counselor_templates]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_appt_counselor_templates]
AS
SELECT		t.appt_time_template							as 'appt_time_template',
		t.office								as 'office',
		t.DOW									as 'DOW',
		t.start_hour								as 'start_hour',
		t.type									as 'type',
		1									as 'available_slots',
		1									as 'full_schedule',
		dbo.format_normal_name(default,cox.first,default,cox.last,default)	as 'counselor'
FROM		counselors c WITH (NOLOCK)
left outer join names cox with (nolock) on c.NameID = cox.name
INNER JOIN	appt_counselor_templates ct WITH (NOLOCK) ON c.counselor = ct.counselor
LEFT OUTER JOIN	view_appt_time_templates t WITH (NOLOCK) ON ct.appt_time_template = t.appt_time_template
GO
