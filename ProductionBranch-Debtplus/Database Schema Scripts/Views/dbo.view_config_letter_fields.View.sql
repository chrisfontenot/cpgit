USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_config_letter_fields]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_config_letter_fields] as
select	cnf.company_id,
		cnf.name,
		dbo.format_address_line_1 ( a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value ) as address1,
		a.address_line_2 as address2,
		dbo.format_city_state_zip (a.city, a.state, a.postalcode) as address3,
		a.city,
		st.MailingCode as state,
		a.PostalCode as postalcode,

		dbo.format_address_line_1 ( a2.house, a2.direction, a2.street, a2.suffix, a2.modifier, a2.modifier_value ) as alt_address1,
		a2.address_line_2 as alt_address2,
		dbo.format_city_state_zip (a2.city, a2.state, a2.postalcode) as alt_address3,
		a2.city as alt_city,
		st2.MailingCode as alt_state,
		a2.PostalCode as alt_postalcode,
	
		cnf.areacode,
		dbo.format_TelephoneNumber (cnf.TelephoneID) as telephone,
		dbo.format_TelephoneNumber (cnf.AltTelephoneID) as alt_telephone,
		dbo.format_TelephoneNumber(cnf.FAXID) as fax,
		cnf.web_site,
		
		cnf.nfcc_unit_no,
		cnf.payment_minimum,
		cnf.acceptance_days,
		cnf.fed_tax_id,
		cnf.dun_nbr,
		cnf.payments_pad_less,
		cnf.payments_pad_greater,
		cnf.threshold,
		cnf.paf_creditor,
		cnf.setup_creditor,
		cnf.deduct_creditor,
		cnf.counseling_creditor,
		cnf.myriad_client_statement_id,
		cnf.client_statement_agency
	
from	config cnf
left outer join addresses a on cnf.addressid = a.address
left outer join addresses a2 on cnf.altaddressid = a.address
left outer join states st on a.state = st.state
left outer join states st2 on a2.state = st.state
GO
