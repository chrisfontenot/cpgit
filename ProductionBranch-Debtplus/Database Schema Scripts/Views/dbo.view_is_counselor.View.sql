USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_is_counselor]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_is_counselor]
AS
SELECT     Counselor, SUM(CAST(CASE WHEN attribute = 101 THEN 1 ELSE 0 END AS int)) AS is_counselor
FROM         dbo.counselor_attributes
GROUP BY Counselor
GO
