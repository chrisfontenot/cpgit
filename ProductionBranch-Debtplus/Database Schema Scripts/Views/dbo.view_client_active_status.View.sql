USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_active_status]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_active_status] AS
SELECT		isnull(c.active_status, 'MISSING')						as 'active_status',
		COUNT(*)									as 'count',
		COALESCE(a.description, 'Unspecified ' + c.active_status, 'Unspecified')	as 'description'
FROM		clients c WITH (NOLOCK)
LEFT OUTER JOIN	active_status a WITH (NOLOCK) ON c.active_status = a.active_status
GROUP BY	c.active_status, a.description
GO
