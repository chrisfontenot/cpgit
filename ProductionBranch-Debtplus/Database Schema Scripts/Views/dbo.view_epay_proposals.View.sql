USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_epay_proposals]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_epay_proposals] as
SELECT	epay_file,
	epay_transaction,
	request,
	client,
	creditor_number,
	epay_biller_id
FROM	epay_transactions
GO
