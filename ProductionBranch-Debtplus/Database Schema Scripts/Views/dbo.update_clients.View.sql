USE [DebtPlus]
GO
/****** Object:  View [dbo].[update_clients]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[update_clients] AS
	SELECT * FROM [clients] WITH (NOLOCK)
GO
