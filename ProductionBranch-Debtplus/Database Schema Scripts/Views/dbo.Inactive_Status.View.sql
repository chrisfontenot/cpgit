USE [DebtPlus]
GO
/****** Object:  View [dbo].[Inactive_Status]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Inactive_Status] as

select p.client, sum(p.net_income) as net_income
from people p, clients c
where p.client = c.client
and c.active_status <> 'I' 
group by p.client
GO
