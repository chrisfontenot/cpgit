USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_mha_screening_completed]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_mha_screening_completed]
AS
SELECT     client, subject
FROM         dbo.client_notes
WHERE     (subject LIKE 'MHA Screening Completed%')
GO
