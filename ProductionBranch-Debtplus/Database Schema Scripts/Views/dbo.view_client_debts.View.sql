USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_debts]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_debts] as

-- =============================================================================================
-- ==       Return the list of client debts                                                   ==
-- =============================================================================================

select	cc.client						as 'client',
	cc.creditor						as 'creditor',
	cc.client_creditor						as 'client_creditor',

	case
		when cc.message is null then ltrim(rtrim(cc.account_number))
		when ltrim(rtrim(cc.message)) = '' then ltrim(rtrim(cc.account_number))
		else ltrim(rtrim(cc.message))
	end							as 'account_number',

	isnull(bal.orig_balance,0)				as 'orig_balance',
	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'current_balance',

	case
		when cc.balance_verify_date is not null then 'V'
		else ' '
	end							as 'verified_balance',

	isnull(cc.disbursement_factor,0)			as 'disbursement_factor',
	coalesce(cc.sched_payment,cc.disbursement_factor,0)	as 'sched_payment',
	isnull(bal.total_payments,0)				as 'total_payments',

	case
		when cc.creditor is null then cc.creditor_name
		else isnull(cr.creditor_name,'')
	end							as 'creditor_name'

from	client_creditor cc with (nolock)
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
where	cc.reassigned_debt = 0
GO
