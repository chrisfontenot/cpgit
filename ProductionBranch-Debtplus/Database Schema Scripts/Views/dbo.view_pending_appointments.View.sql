USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_pending_appointments]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_pending_appointments] AS
-- ===================================================================================
-- ==    Find information about pending client appointments                         ==
-- ==    Used by DebtPlus.Appointments.Update                                       ==
-- ===================================================================================
SELECT
		-- Client appointment items
		ca.[client_appointment],
		ca.[client],
		ca.[appt_time],
		ca.[counselor],
		ca.date_confirmed,
		ca.date_updated,
		ca.date_created,
		ca.created_by,
		convert(int,ca.[confirmation_status]) as 'confirmation_status',
		ca.callback_ph,
		convert(int,ca.[credit]) as 'credit',
		convert(int,ca.[post_purchase]) as 'post_purchase',
		convert(int,ca.[priority]) as 'priority',
		convert(int,ca.[housing]) as 'housing',
		convert(int,datediff(minute,ca.[start_time],ca.[end_time])) as 'duration',
		ca.[status],
		ca.[start_time],
		
		-- Information about the appointment		
		convert(varchar(50),case ca.status when 'P' then 'Pending' when 'C' then 'Cancelled' when 'M' then 'Missed' when 'R' then 'Rescheduled' else '' end) as 'formatted_status',
		m.description as 'formatted_confirmation_status',
		apt.appt_name as 'formatted_appt_type',
		
		-- Fields from the client
		c.active_status,
		o.name as 'formatted_office',
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'formatted_counselor',
		dbo.format_TelephoneNumber(c.HomeTelephoneID) as 'formatted_home_ph',
		dbo.format_TelephoneNumber(c.MsgTelephoneID) as 'formatted_message_ph',
		dbo.format_TelephoneNumber(p.WorkTelephoneID) as 'formatted_work_ph',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'formatted_name',

		-- Selection criteria		
		t.[office] as appt_office,
		t.[start_time] as appt_start_time
		
FROM	client_appointments ca
inner join appt_times t with (nolock) on ca.appt_time = t.appt_time
left outer join clients c with (nolock) ON ca.client = c.client
left outer join people p with (nolock) on ca.client = p.client and 1 = p.Relation
left outer join Names pn with (nolock) ON p.NameID = pn.Name
left outer join offices o with (nolock) on c.office = o.office
left outer join counselors co with (nolock) ON ca.counselor = co.counselor
left outer join Names cox with (nolock) on co.NameID = cox.Name
left outer join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
left outer join AppointmentConfirmationTypes m with (nolock) on ca.confirmation_status = m.oID
WHERE	(ca.[office] IS NOT NULL)
AND		(ca.[status] = 'P')
GO
