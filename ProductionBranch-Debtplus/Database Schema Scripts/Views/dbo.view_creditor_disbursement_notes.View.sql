USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_disbursement_notes]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_creditor_disbursement_notes]
AS

SELECT		n.disbursement_creditor_note				as 'disbursement_creditor_note',
		n.client						as 'client',
		nt.description						as 'description',
		n.disbursement_register					as 'disbursement_register', 
		coalesce(d.date_created, n.note_date, n.date_created)	as 'note_date',
		n.note_amount						as 'note_amount',
                n.created_by						as 'created_by'
                
FROM		disbursement_creditor_notes n WITH (NOLOCK)
INNER JOIN	disbursement_creditor_note_types nt WITH (NOLOCK) ON n.disbursement_creditor_note_type = nt.disbursement_creditor_note_type
LEFT OUTER JOIN	registers_disbursement d WITH (NOLOCK) ON n.disbursement_register = d.disbursement_register
GO
