USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_pending_ticklers]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_pending_ticklers] AS

-- ===========================================================================================
-- ==            View for the tickler report                                                ==
-- ===========================================================================================

select	t.client				as 'client',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)	as 'client_name',
	dbo.format_counselor_name(co.person)	as 'counselor_name',
	convert(datetime, convert(varchar(10), t.date_effective, 101) + ' 00:00:00')	as 'effective_date',

	t.priority					as 'priority',
	isnull(t.note,'')			as 'note',
	t.date_created				as 'date_created',
	dbo.format_counselor_name ( t.created_by ) as 'created_by',
	m.description				as 'tickler_description',
	t.counselor					as 'counselor',
	t.tickler_type				as 'tickler_type',
	c.office					as 'office'

from	ticklers t		with (nolock)
left outer join	TicklerTypes m	with (nolock) on t.tickler_type = m.oID
left outer join people p	with (nolock) on t.client = p.client and 1 = p.relation
LEFT OUTER JOIN names pn with (nolock) on p.nameid = pn.name
left outer join counselors co	with (nolock) on t.counselor = co.counselor
left outer join clients c	with (nolock) on t.client = c.client

where	t.date_deleted is null
GO
