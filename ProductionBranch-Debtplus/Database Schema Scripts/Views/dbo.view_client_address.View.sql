SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ========================================================================================================================
 ==           Return the information about the client database                                                         ==*/
ALTER VIEW [view_client_address]
AS
SELECT     c.client, ISNULL(c.active_status, 'CRE') AS active_status, c.client_status, dbo.date_only(COALESCE (c.start_date, c.date_created, GETDATE())) AS start_date, 
                      a.PostalCode AS zipcode, dbo.format_normal_name(p1x.Prefix, p1x.First, p1x.Middle, p1x.Last, p1x.Suffix) AS name, dbo.format_normal_name(p2x.Prefix, p2x.First, 
                      p2x.Middle, p2x.Last, p2x.Suffix) AS coapplicant, dbo.format_reverse_name(p1x.Prefix, p1x.First, p1x.Middle, p1x.Last, p1x.Suffix) AS last_name_first, 
                      UPPER(dbo.format_Address_Line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value)) AS addr1, UPPER(a.address_line_2) AS addr2, 
                      UPPER(dbo.format_city_state_zip(a.city, a.state, a.PostalCode)) AS addr3, dbo.format_salutation(c.client) AS salutation, 
                      dbo.format_TelephoneNumber(c.HomeTelephoneID) AS phone, dbo.format_TelephoneNumber(p1.WorkTelephoneID) AS app_work_phone, 
                      dbo.format_TelephoneNumber(p1.CellTelephoneID) AS app_cell_phone, dbo.format_TelephoneNumber(p2.WorkTelephoneID) AS coapp_work_phone, 
                      dbo.format_TelephoneNumber(p2.CellTelephoneID) AS coapp_cell_phone, ISNULL(l.LanguageID, 'EN') AS language, LTRIM(RTRIM(a.city)) AS city, 
                      st.MailingCode AS state, c.date_created, dbo.format_ssn(p1.SSN) AS ssn_1, dbo.format_ssn(p2.SSN) AS ssn_2, c.office, dbo.format_counselor_name(co.Person) 
                      AS counselor, dbo.format_normal_name(cox.Prefix, cox.First, cox.Middle, cox.Last, cox.Suffix) AS counselor_name, 
                      dbo.format_TelephoneNumber(p1.WorkTelephoneID) AS work_ph, NULL AS work_ext, dbo.format_TelephoneNumber(c.MsgTelephoneID) AS message_ph, 
                      c.mail_error_date, p1x.Last AS LastName, p1x.First AS FirstName, em1.Address AS EmailAddress
FROM         dbo.clients AS c WITH (NOLOCK) LEFT OUTER JOIN
                      dbo.addresses AS a WITH (NOLOCK) ON c.AddressID = a.address LEFT OUTER JOIN
                      dbo.states AS st WITH (NOLOCK) ON a.state = st.state LEFT OUTER JOIN
                      dbo.people AS p1 WITH (NOLOCK) ON c.client = p1.Client AND 1 = p1.Relation LEFT OUTER JOIN
                      dbo.Names AS p1x WITH (NOLOCK) ON p1.NameID = p1x.Name LEFT OUTER JOIN
                      dbo.people AS p2 WITH (NOLOCK) ON p2.Person =
                          (SELECT     MIN(Person) AS Expr1
                            FROM          dbo.people AS p2y WITH (nolock)
                            WHERE      (Client = c.client) AND (1 <> Relation)) LEFT OUTER JOIN
                      dbo.Names AS p2x WITH (NOLOCK) ON p2.NameID = p2x.Name LEFT OUTER JOIN
                      dbo.AttributeTypes AS l WITH (NOLOCK) ON c.language = l.oID LEFT OUTER JOIN
                      dbo.counselors AS co WITH (NOLOCK) ON c.counselor = co.Counselor LEFT OUTER JOIN
                      dbo.Names AS cox WITH (NOLOCK) ON co.NameID = cox.Name LEFT OUTER JOIN
                      dbo.EmailAddresses AS em1 WITH (NOLOCK) ON p1.EmailID = em1.Email
GO
