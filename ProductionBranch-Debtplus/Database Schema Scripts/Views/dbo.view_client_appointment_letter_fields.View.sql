USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_appointment_letter_fields]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_client_appointment_letter_fields] as

-- ====================================================================================================================
-- ==           Client appointment letter information                                                                ==
-- ====================================================================================================================

-- ChangeLog
--    2/6/2003
--		Added client appointment callback telephone number
select	c.client,							-- Client ID
		ca.client_appointment as client_appointment,		-- Appointment row numerical ID
		ca.start_time,						-- Appointment start time
		ca.date_updated,					-- Appointment date counseled
		ca.counselor as counselor_id,		-- Appointment counselor numerical ID
		ca.office as office_id,				-- Appointment office numerical ID
		ca.status,							-- Appointment status (for selection criteria)
		isnull(ca.callback_ph, dbo.format_TelephoneNumber(c.HomeTelePhoneID)) as callback_ph,	-- Callback telephone number

		dbo.format_normal_name(default,cox.first,default,cox.last,default) as counselor,					-- Name of the counselor
		o.name as office_name,				-- Name of the office

		dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as office_addr1,				-- Address line 1 to the office
		a.address_line_2 as office_addr2,	-- Address line 2 to the office
		a.city as office_city,				-- City of the office
		st.MailingCode as office_state,		-- State the office
		a.Postalcode as office_zipcode,		-- Zipcode to the office
		dbo.format_address_block(o.AddressID) as office_address,

		dbo.format_TelephoneNumber(o.TelephoneID) as office_phone,	-- Local phone # of the office

		o.directions as office_directions,   -- Generic driving directions to the office
		
		dbo.format_TimeZoneLabel(st.TimeZoneID, ca.start_time) as TimeZone

from		clients c with (nolock)

-- This statement pulls the single appointment that is the most recently created item from the appointments table.
left outer join	client_appointments ca with (nolock) on c.client = ca.client -- and (ca.client_appointment = (select top 1 client_appointment from client_appointments cap with (nolock) where cap.client = c.client and cap.status = 'P' order by cap.date_created desc))

-- Include the office and counselor names
left outer join offices o with (nolock) on ca.office = o.office
left outer join addresses a with (nolock) on o.AddressID = a.Address
left outer join states st with (nolock) on a.state = st.state
left outer join counselors co with (nolock) on ca.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name
GO
