SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW  [view_check_details_CR] as

-- ==================================================================================================
-- ==       Examine the details of the check and report the contents of the item                   ==
-- ==       This view only works on creditor checks.                                               ==
-- ==================================================================================================

select	tr.tran_type,
	tr.trust_register,
	tr.checknum,
	tr.cleared,
	tr.reconciled_date,
	tr.bank_xmit_date,
	rcc.client,
	cc.client_creditor,
	rcc.debit_amt,
	rcc.fairshare_amt,
	rcc.creditor_type,
	rcc.date_created,
	rcc.void,
	rcc.disbursement_register,
	cr.creditor,
	cr.creditor_name,
	coalesce(rcc.account_number, cc.account_number, cc.message, 'MISSING') as account_number,
	isnull(cc.client_name, dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)) as client_name
from	registers_trust tr with (nolock)
left outer join registers_client_creditor rcc on tr.trust_register = rcc.trust_register
left outer join creditors cr with (nolock) on rcc.creditor = cr.creditor
left outer join people p with (nolock) on p.client = rcc.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join client_creditor cc on rcc.client_creditor = cc.client_creditor
where	rcc.tran_type in ('AD','BW','MD','CM')
GO
