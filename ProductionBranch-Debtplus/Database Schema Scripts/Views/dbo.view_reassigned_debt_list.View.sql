USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_reassigned_debt_list]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_reassigned_debt_list] as

-- ============================================================================================
-- ==       Return the list of debts that may be reassigned                                  ==
-- ============================================================================================

SELECT	cc.client_creditor as client_creditor,
	cc.client as client,
	cc.creditor as creditor,
	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as balance
FROM	client_creditor cc
INNER JOIN client_creditor_balances bal ON cc.client_creditor_balance = bal.client_creditor_balance
WHERE	cc.reassigned_debt = 0
GO
