USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_hud_cms_counselors]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_hud_cms_counselors] as

-- ===============================================================================
-- ==      Return the information for the counselor names                       ==
-- ===============================================================================

select TOP 100 percent
        co.counselor                     as cms_counselor_id,
        convert(varchar(80), n.first)    as cnslor_fname,
        CONVERT(varchar(80), n.middle)   as cnslor_mname,
        convert(varchar(80), n.last)     as cnslor_lname
from counselors co
inner join counselor_attributes cox on co.counselor = cox.counselor and cox.attribute = 'ROLE:COUNSELOR'
left outer join names n on co.nameid = n.name
order by 1;
GO
