USE [DEBTPLUS]
GO

/****** Object:  View [dbo].[view_aarp_call_data]    Script Date: 7/7/2015 4:16:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[view_aarp_call_data]
as
select c.client as DebtPlusClientID
	     , cl.DateCreated as CreatedOn
		 --, cl.CreatedBy as CreatedBy
		 , case when charindex('\', createdby) > 1 then upper(substring(createdby, charindex('\', createdby) + 1, len(createdby))) else createdby end as CreatedBy
		 , case when ca.client_appointment  is null then null  else c.client_guid end as ClientNumber
		 , n.First as FirstName
		 , n.Last as LastName
		 , isnull(a.house, '') + ' ' + isnull(a.direction, '') + ' ' + isnull(a.street, '') + ' ' + isnull(a.suffix, '') + ' ' + isnull(a.address_line_2, '') as StreetAddress
		 , UPPER(a.city) as City
		 , s.MailingCode as State
		 , case when a.PostalCode is null then null else '''' + convert(varchar, a.PostalCode) end as ZipCode
		 , e.Address as ClientEmail
		 , isnull(ht.Acode, '') + isnull(ht.Number, '') as HomePhone
		 , isnull(ct.Acode, '') + isnull(ct.Number, '') as CellPhone
		 , isnull(wt.Acode, '') + isnull(wt.Number, '') as WorkPhone
		 , ca.referred_by as ReferredBy
		 , case when ca.referred_by is null then (select top(1) ref.description from referred_by as ref where ref.referred_by = cl.referralcode)  else rb.description end as ReferralCode
		 --, rb.description as ReferralCode
		 , convert(varchar, p.Birthdate, 101) as DataOfBirth
		 --, case cl.InitialProgramRequested when 1 then 'Yes' else 'No' end as InitialProgramRequested
		 , ip.description as InitialProgramRequested
		 , case 
			when cl.Btw50ProgramRequested = 1 and cl.SnapProgramRequested = 1 then 'BTW 50 and SNAP'
			when cl.Btw50ProgramRequested = 1  then 'BTW 50'
			when cl.SnapProgramRequested = 1 then 'SNAP'
			else '' 
			end as AllProgramsRequested
		 , case when hst.description is null then '' when hst.description = 'Other' then '' else hst.description end as CurrentHousingType
		 , jd.aarp as EmploymentStatus
		 , (select cr.description from call_reason as cr where oID = cl.CallReason) as CallReason
		 , (select cr.description from call_resolution as cr where oID = cl.CallResolution) as CallResolution
		 , 'Foreclosure Prevention' as LineOfService
		 , convert(varchar, ca.start_time, 101) as StartDate
		 --, convert(varchar, ca.start_time, 108) as StartTime
		 , case when ca.start_time is not null then right(convert(varchar, ca.start_time, 100), 8) else '' end as StartTime
		 , case when ca.status = 'K' then 'Yes' when ca.status = 'W' then 'Yes' else 'No' end as AttendedSession
		 , case when cl.EmailOptIn = 1 then 'Yes' else 'No' end as CanWeEmail
		 , case when cl.TextOptIn = 1 then 'Yes' else 'No' end as CanWeText
		 , (select description from contact_preferred_method where oID = cl.PreferredMethod) as PreferredCommunication
		 , 'Yes' as MailingList
		 , case when cl.MediaFollowup = 1 then 'Yes' else 'No' end as MediaFollowUp
		 , case when cl.SnapProgramRequested = 1 then 'Yes' else 'No' end as SNAPFollowUp
		 , case when cl.Btw50ProgramRequested = 1 then 'Yes' else 'No' end as BTWFollowUp
		 , case when cl.IsAarpClient = 1 then 'Yes' else 'No' end as CurrentAARPClient
		 , case when p.MilitaryStatusID = 2 then 'Yes' else 'No' end as IsVeteran
		 ,cl.DateCreated
	  from clients as c
	  inner join people as p on c.client = p.Client
	  left outer join Names as n on p.NameID = n.Name
	  left outer join addresses as a on c.AddressID = a.address
	  left outer join states as s on a.state = s.state
	  inner join aarp_call_log as cl on c.client = cl.clientID
	  left outer join client_appointments as ca on cl.AppointmentID = ca.client_appointment
	  left outer join EmailAddresses as e on p.EmailID = e.Email
	  left outer join TelephoneNumbers as ht on c.HomeTelephoneID = ht.TelephoneNumber
	  left outer join TelephoneNumbers as ct on p.CellTelephoneID = ct.TelephoneNumber
	  left outer join TelephoneNumbers as wt on p.WorkTelephoneID = wt.TelephoneNumber
	  left outer join referred_by as rb on ca.referred_by = rb.referred_by
	  left outer join client_housing as ch on c.client = ch.client
	  left outer join Housing_StatusTypes as hst on ch.housing_status = hst.oID
	  left outer join job_descriptions as jd on p.job = jd.job_description
	  left outer join initial_program as ip on cl.InitialProgramRequested = ip.oID
	 where p.Relation = 1
	   and (ca.referred_by is null or ca.referred_by IN (708, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 1250))
	   --and cl.DateCreated between @from_date and @to_date


GO


