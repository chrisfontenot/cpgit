USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_most_recent_hud_interview]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_most_recent_hud_interview] AS
SELECT TOP 100 percent	client, dbo.map_client_to_housing_interview(client) AS max_hud_interview
FROM	clients WITH (NOLOCK)
WHERE	client in (
			select client from hud_interviews WITH (NOLOCK)
)
order by 1
GO
