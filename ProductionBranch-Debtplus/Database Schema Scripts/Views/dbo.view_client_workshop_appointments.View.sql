USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_workshop_appointments]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_workshop_appointments] as
-- =============================================================================================
-- ==           View for the nearest workshop appointment for the client.                     ==
-- ==           This is used by the letter routine to generate the details of a workshop      ==
-- =============================================================================================

select	c.client			as 'client',
		w.start_time		as 'workshop_time',

		wt.description		as 'workshop_name',
		a.name				as 'workshop_location',
		a.address_1			as 'workshop_address1',
		
		case
			when a.address_2 <> '' then a.address_2
			else a.address_4
		end					as 'workshop_address2',

		case
			when a.address_2 = '' then a.address_4
			else ''
		end					as 'workshop_address3',

	a.directions			as 'directions',
	w.seats_available		as 'seats_available',

	ca.client_appointment	as 'appointment_id',
	ca.workshop				as 'workshop_id',
	w.workshop_location		as 'workshop_location_id',
	w.workshop_type			as 'workshop_type_id'

from	clients c with (nolock)

-- Include the client appointment that relates to the first workshop for the client
left outer join client_appointments ca on ca.client_appointment = (
	select top 1 client_appointment
	from	client_appointments xca with (nolock)
	where	c.client = xca.client
	and	xca.workshop is not null
	and	xca.start_time >= getdate()
	order by xca.start_time
)

left outer join workshops w		with (nolock) on ca.workshop = w.workshop
left outer join workshop_types wt	with (nolock) on w.workshop_type = wt.workshop_type
left outer join view_workshop_locations a	with (nolock) on w.workshop = a.workshop
GO
