USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_disbursement_review]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_disbursement_review] AS

-- ====================================================================================================
-- ==            Return the information for the review of an automatic disbursement                  ==
-- ====================================================================================================

select	d.disbursement_register,
		d.client,
		isnull(c.held_in_trust,0) + isnull(r.debit_amt,0)	as 'held_in_trust',
		d.reserved_in_trust,
		d.disbursement_factor,
		d.note_count,
		d.client_register,
		d.active_status,
		d.start_date,
		d.region,
		dbo.format_reverse_name(default, pn.first, pn.middle, pn.last, pn.suffix)	as 'client_name',
		isnull(r.debit_amt,0)					as 'disbursed',
		r.created_by							as 'created_by',
		r.date_created							as 'date_created'

from		disbursement_clients d WITH (NOLOCK)
left outer join	clients c WITH (NOLOCK) on d.client = c.client
left outer join people p WITH (NOLOCK) on d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join registers_client r WITH (NOLOCK) on d.client_register = r.client_register
GO
