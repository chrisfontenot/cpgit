SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [view_creditor_general_info] AS

--  ChangeLog
--   12/07/2002
--     Dropped "suppress_bal_verify" flag.
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

select	cr.creditor							as 'creditor',
	isnull(cr.sic,'')						as 'sic',
	isnull(cr.creditor_name,'')					as 'creditor_name',
	isnull(cr.comment,'')						as 'comment',
	isnull(cr.division,'')						as 'division',
	isnull(cr.mail_priority,9)					as 'mail_priority',
        isnull(cr.payment_balance,'B')					as 'payment_balance',

        isnull(cr.contrib_cycle,'M')					as 'contrib_cycle',
	isnull(cr.contrib_bill_month,1)					as 'contrib_bill_month',
	isnull(cr.pledge_amt,0)						as 'pledge_amt',
	isnull(cr.pledge_cycle,'N')					as 'pledge_cycle',
	isnull(cr.pledge_bill_month,1)					as 'pledge_bill_month',
	isnull(cr.min_accept_amt,0)					as 'min_accept_amt',
	isnull(cr.min_accept_pct,0)					as 'min_accept_pct',
    isnull(cr.min_accept_per_bill,0)				as 'min_accept_per_bill',
	cr.date_created							as 'setup_date',

	coalesce(cr.lowest_apr_pct,cr.medium_apr_pct,cr.highest_apr_pct,0)	as 'lowest_apr_pct',
	coalesce(cr.medium_apr_pct,cr.highest_apr_pct,cr.lowest_apr_pct,0)	as 'medium_apr_pct',
	coalesce(cr.highest_apr_pct,cr.lowest_apr_pct,cr.medium_apr_pct,0)	as 'highest_apr_pct',

	coalesce(cr.medium_apr_amt,cr.highest_apr_amt,0)		as 'medium_apr_amt',
	coalesce(cr.highest_apr_amt,cr.medium_apr_amt,0)		as 'highest_apr_amt',

    isnull(cr.max_clients_per_check,-1)				as 'max_clients_per_check',
	isnull(cr.max_amt_per_check,-1)					as 'max_amt_per_check',
	isnull(cr.max_fairshare_per_debt,0)				as 'max_fairshare_per_debt',

	cr.returned_mail						as 'returned_mail',
	isnull(cr.po_number,'')						as 'po_number',
	isnull(cr.usual_priority,9)					as 'usual_priority',

	case
		when isnull(cr.percent_balance,0) <= 0  then 100
		when isnull(cr.percent_balance,0) > 100 then 100
		else isnull(cr.percent_balance,0)
	end								as 'percent_balance',

	isnull(cr.distrib_mtd,0)						as 'distrib_mtd',
        isnull(cr.contrib_mtd_billed,0)						as 'contrib_mtd_billed',
	isnull(cr.contrib_mtd_received,0)					as 'contrib_mtd_received',
	isnull(cr.distrib_ytd,0)+isnull(cr.distrib_mtd,0)			as 'distrib_ytd',
	isnull(cr.contrib_ytd_billed,0)+isnull(cr.contrib_mtd_billed,0)		as 'contrib_ytd_billed',
	isnull(cr.contrib_ytd_received,0)+isnull(cr.contrib_mtd_received,0)	as 'contrib_ytd_received',

	rcc.date_created						as 'first_payment_date',
	coalesce(cr.acceptance_days,-1)					as 'acceptance_days',

	-- Include all of the flags that are referenced by the dialog
	isnull(cr.prohibit_use,0)					as 'prohibit_use',
	isnull(cr.voucher_spacing,0)					as 'voucher_spacing',
	isnull(cr.full_disclosure,0)					as 'full_disclosure',
    isnull(cr.suppress_invoice,0)					as 'suppress_invoice',
	isnull(cr.proposal_budget_info,0)				as 'proposal_budget_info',
	isnull(cr.proposal_income_info,0)				as 'proposal_income_info',
	isnull(cr.chks_per_invoice, -1)					as 'chks_per_invoice',

	isnull(cr.creditor_class,0)					as 'creditor_class',
	isnull(cr.check_bank,1)						as 'check_bank',
	isnull(cr.check_payments,0)					as 'check_payments',

	case
		when cr.creditor_contribution_pct is null then 'None'
		when pct.creditor_type_eft = 'N' then 'None'
		when pct.fairshare_pct_eft = 0 then 'None'
		when pct.creditor_type_eft = 'D' then 'Deduct (' + convert(varchar, pct.fairshare_pct_eft * 100.0) + '%)'
		else 'Bill (' + convert(varchar, pct.fairshare_pct_eft * 100.0) + '%)'
	end								as 'info_eft',

	case
		when cr.creditor_contribution_pct is null then 'None'
		when pct.creditor_type_check = 'N' then 'None'
		when pct.fairshare_pct_check = 0 then 'None'
		when pct.creditor_type_check = 'D' then 'Deduct (' + convert(varchar, pct.fairshare_pct_check * 100.0) + '%)'
		else 'Bill (' + convert(varchar, pct.fairshare_pct_check * 100.0) + '%)'
	end								as 'info_check',

	-- old columns that were removed from the table and should be out of the client application
	convert(bit,1)							as 'contribution_breakdown',
	convert(bit,0)							as 'suppress_invoice_detail'

FROM		creditors cr with (nolock)

left outer join registers_creditor rcc with (nolock) on cr.first_payment = rcc.creditor_register
left outer join creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
GO
