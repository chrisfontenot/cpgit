USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_counselor_or_csrs]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_counselor_or_csrs] as
-- ==============================================================================
-- ==          Return the information from the counselors list                 ==
-- ==============================================================================

select	co.Counselor, co.NameID, co.EmailID, co.TelephoneID, co.Person, co.[default], co.ActiveFlag, co.Office, co.Menu_Level, co.Color, co.ssn, co.billing_mode, co.rate, co.emp_start_date, co.emp_end_date, co.HUD_id, co.date_created, co.created_by,
		dbo.format_normal_name(default,cox.first,default,cox.last,default) + isnull('('+co.Note+')','') as name,
		emx.address as email,
		dbo.format_TelephoneNumber(co.telephoneID) as telephone
from	Counselors co
left outer join Names cox on co.NameID = cox.Name
left outer join EmailAddresses emx on co.EmailID = emx.Email
where	co.Counselor in (
	select	counselor
	from	counselor_attributes a
	inner join AttributeTypes t on a.Attribute = t.oid
	where	t.Grouping = 'ROLE'
	and		t.Attribute in ('CSR', 'COUNSELOR')
)
GO
