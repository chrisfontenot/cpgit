USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_deposit_register]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_deposit_register] AS
select	trust_register				as 'batch_id',
	date_created				as 'date',
	created_by				as 'creator',
	case cleared
		when ' ' then 'PENDING'
		when 'R' then 'RECONCILED' + isnull(' '+convert(varchar(10), reconciled_date, 101),'')
		when 'V' then 'VOID' + isnull(' ' + convert(varchar(10), reconciled_date, 101),'')
		else '? ' + cleared
	end					as 'type',

	credit_amt				as 'amount'
FROM	view_trust_register
WHERE	tran_type = 'DP'
GO
