USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_deposits]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_deposits] as

-- ======================================================================================
-- ==       Used by the deposit batch reports that look at the trust rather than the   ==
-- ==       working deposit table. Make the results look like the un-posted deposits.  ==
-- ======================================================================================

select	tr.tran_type					as 'tran_type',
		isnull(rc.tran_subtype,'OT')	as 'tran_subtype',
		isnull(dt.description,'Other')	as 'tran_subtype_description',
		CONVERT(bit,1)					as 'ok_to_post',
		rc.client_register				as 'deposit_batch_detail',
		tr.trust_register				as 'deposit_batch_id',
		rc.client						as 'client',
		rc.credit_amt					as 'credit_amt',
		rc.item_date					as 'item_date',
		rc.message						as 'reference',
		tr.created_by					as 'created_by',
		tr.date_created					as 'date_created',
		dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default) as 'client_name'
from			registers_client rc with (nolock)
inner join		registers_trust tr with (nolock) on rc.trust_register = tr.trust_register
left outer join	tran_types dt on rc.tran_subtype = dt.tran_type
left outer join people p on rc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.name
where	tr.tran_type = 'DP'
GO
