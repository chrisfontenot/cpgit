USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_OCS_PostMod_Records_Created]    Script Date: 06/18/2015 12:38:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER view [dbo].[view_OCS_PostMod_Records_Created] as

select
ocs.ClientId [Id],
report.[User] [User],
record.FirstName1 + ' ' + record.LastName1 [ClientName],
record.Servicer [ServicerName],
record.LoanNumber [LoanNumber],
record.Uploaddate [ReceivedDate],

case when ocs.Program = 4--FMAC PostMod
then 
	(select top 1 [Begin] 
	from OCS_ContactAttempt 
	where ClientId = ocs.ClientId
	and ResultCode in(4,9,10,12,14,15,16,17,18))--these numbers are "right party contacts"
else
	(select top 1 [Begin] --FMAC EI Family
	from OCS_ContactAttempt 
	where ClientId = ocs.ClientId
	and ResultCode in(2,4,5,9,10,12,13)) --these numbers are "right party contacts"

end [FRPDate],

viewO.ContactAttempts [ContactAttempts],
CONVERT(bit,null) [AppointmentScheduled],
ocs.QueueCode [CurrentQueue],
ocs.Program [Program]

from OCS_Client [ocs]
left join OCS_UploadRecord [record] on record.Id = ocs.UploadRecord
left join view_OCS_Client [viewO] on viewO.ClientId = ocs.ClientId
left join OCS_UploadReport [report] on record.UploadAttempt = report.AttemptId


