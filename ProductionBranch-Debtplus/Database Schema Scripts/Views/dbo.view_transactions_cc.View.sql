SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [view_transactions_cc] AS

-- ========================================================================================
-- ==             Debt Transactions in the system                                        ==
-- ========================================================================================

SELECT		case
			when d.tran_type = 'IN' then d.tran_type + '/' + rtrim(ltrim(convert(varchar, convert(float, d.fairshare_pct * 100.0))))
			when d.tran_type in ('AD','MD','CM','BW') and (isnull(d.creditor_type,'N') = 'N' or d.fairshare_pct = 0.0) then d.tran_type + '/' + isnull(d.creditor_type,'N')
			when d.tran_type in ('AD','MD','CM','BW') then d.tran_type + '/' + isnull(d.creditor_type,'N') + ' ' + convert(varchar, d.fairshare_pct * 100.0)
			else d.tran_type + isnull('/' + d.creditor_type,'')
		end										as 'tran_type',

		d.client									as 'client',
		d.creditor									as 'creditor',
		d.client_creditor									as 'client_creditor',
		cr.creditor_name								as 'creditor_name',

		convert (datetime, convert(varchar(12), d.date_created, 101) + ' 00:00:00')	as 'item_date',
		tr.checknum									as 'checknum',
		convert (datetime, convert(varchar(12), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

		d.credit_amt									as 'credit_amt',
		d.debit_amt									as 'debit_amt',
		d.fairshare_amt									as 'fairshare_amt',
		isnull(d.account_number,cc.account_number)					as 'account_number'

FROM		registers_client_creditor d	WITH (NOLOCK)
LEFT OUTER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
INNER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
LEFT OUTER JOIN client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
GO
