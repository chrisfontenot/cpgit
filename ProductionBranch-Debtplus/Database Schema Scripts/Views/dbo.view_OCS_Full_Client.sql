USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_OCS_Full_Client]    Script Date: 06/18/2015 12:38:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[view_OCS_Full_Client] as


SELECT

--OCS CLIENT INFO
ocs.[Id] as [OCSId]
,ocs.[Program]
,ocs.[UploadRecord]
,ocs.[ClientId]
,ocs.[StatusCode]
,ocs.[ContactAttempts]
,ocs.[ActiveFlag]
,ocs.[Archive]
,ocs.[IsDuplicate]
,ocs.[UploadAttempt]
,ocs.[InvestorNumber]
,ocs.[InvestorDueDate]
,ocs.[InvestorLastChanceList]
,ocs.[InvestorActualSaleDate]
,ocs.[ClaimedDate]
,ocs.[ClaimedBy]
,ocs.[SearchTimezone]
,ocs.[SearchServicerId]
,ocs.[PostModLastChanceFlag]
,ocs.[PostModLastChanceDate]
,ocs.[QueueCode]
,contact.[Timestamp]
,upload.Servicer
,upload.LoanNumber [ServicerNumber]
,cl.language [PreferredLanguage]

--APPLICANT INFO
,CONVERT(bit,case when Applicant.Person is not null then 1 else 0 end) [HasApplicant]
,upload.FirstName1 [AppFName]
,'' [AppMName]
,upload.LastName1 [AppLName]
,AppName.Prefix [AppPrefix]
,AppName.Suffix [AppSuffix]
,upload.SSN1 [AppSSN]
,AppEmail.Address [AppEmail]

--COAPPLICANT INFO
,CONVERT(bit,case when CoApplicant.Person is not null then 1 else 0 end) [HasCoApplicant]
,upload.FirstName2 [CoAppFName]
,'' [CoAppMName]
,upload.LastName2 [CoAppLName]
,CoAppName.Prefix [CoAppPrefix]
,CoAppName.Suffix [CoAppSuffix]
,upload.SSN2 [CoAppSSN]
,CoAppEmail.Address [CoAppEmail]

--CLIENT ADDRESS
,upload.ClientStreet [ClientStreet]
,'' [ClientStreet2]
,upload.ClientCity [ClientCity]
,(case when upload.ClientState is null then '0' when upload.ClientState = '' then '0' else upload.ClientState end) [ClientState]
,upload.ClientZipcode [ClientZip]

--TELEPHONE NUMBERS
,HomePhone.SearchValue [HomePhone]
,HomePhone.user_selected_type [HomePhoneUserSelectedType]
,MsgPhone.SearchValue [MsgPhone]
,MsgPhone.user_selected_type [MsgPhoneUserSelectedType]
,AppCell.SearchValue [AppCellPhone]
,AppCell.user_selected_type [AppCellPhoneUserSelectedType]
,AppWork.SearchValue [AppWorkPhone]
,AppWork.user_selected_type [AppWorkPhoneUserSelectedType]
,CoAppCell.SearchValue [CoAppCellPhone]
,CoAppCell.user_selected_type [CoAppCellPhoneUserSelectedType]
,CoAppWork.SearchValue [CoAppWorkPhone]
,CoAppWork.user_selected_type [CoAppWorkPhoneUserSelectedType]

--PROPERTY-BASED ADDRESS
,Property.UseHomeAddress
,case when Property.UseHomeAddress = 0 then PropertyAddress.street else upload.ClientStreet end [PropertyStreet]
,case when Property.UseHomeAddress = 0 then PropertyAddress.address_line_2 else '' end [PropertyStreet2]
,case when Property.UseHomeAddress = 0 then PropertyAddress.city else upload.ClientCity end [PropertyCity]
,case when Property.UseHomeAddress = 0 
      then 
            case when PropertyState.Name is null or PropertyState.Name = '' then 'None' else PropertyState.MailingCode end

      else (case when upload.ClientState is null then '0' when upload.ClientState = '' then '0' else upload.ClientState end) end [PropertyState]

,case when Property.UseHomeAddress = 0 then PropertyAddress.PostalCode else upload.ClientZipcode end [PropertyZip]
-- Counsel Dates
,ocs.FirstCounselDate
,ocs.SecondCounselDate

FROM 
/*OCS Client Info*/
OCS_Client [ocs]
LEFT JOIN OCS_UploadRecord [upload] on upload.Id = ocs.UploadRecord
LEFT JOIN clients [cl] on cl.client = ocs.ClientId
LEFT JOIN OCS_ContactAttempt [contact] on 
      ocs.ClientId = contact.ClientId AND 
      contact.Id = (select top 1 Id from OCS_ContactAttempt where ClientId = ocs.ClientId order by [End] desc)

/*Applicant*/
LEFT JOIN people [Applicant] on Applicant.Person = (select top 1 Person from people where client = cl.client and Relation = 1)
LEFT JOIN Names [AppName] on AppName.Name = Applicant.NameID
LEFT JOIN EmailAddresses [AppEmail] on AppEmail.Email = Applicant.EmailID
/*CoApplicant*/
LEFT JOIN people [CoApplicant] on CoApplicant.Person = (select top 1 Person from people where client = cl.client and Relation <> 1)
LEFT JOIN Names [CoAppName] on CoAppName.Name = CoApplicant.NameID
LEFT JOIN EmailAddresses [CoAppEmail] on CoAppEmail.Email = CoApplicant.EmailID

/*Telephone Numbers*/
LEFT JOIN TelephoneNumbers [HomePhone] on HomePhone.TelephoneNumber = cl.HomeTelephoneID
LEFT JOIN TelephoneNumbers [MsgPhone] on MsgPhone.TelephoneNumber = cl.MsgTelephoneID
LEFT JOIN TelephoneNumbers [AppCell] on AppCell.TelephoneNumber = Applicant.CellTelephoneID
LEFT JOIN TelephoneNumbers [AppWork] on AppWork.TelephoneNumber = Applicant.WorkTelephoneID
LEFT JOIN TelephoneNumbers [CoAppCell] on CoAppCell.TelephoneNumber = CoApplicant.CellTelephoneID
LEFT JOIN TelephoneNumbers [CoAppWork] on CoAppWork.TelephoneNumber = CoApplicant.WorkTelephoneID

/*Property Chain*/
LEFT JOIN Housing_properties [Property] on Property.OwnerID = Applicant.Person
LEFT JOIN Housing_loans [Loan] on Loan.PropertyID = Property.oID
LEFT JOIN Housing_lenders [Lender] on Loan.OrigLenderID = Lender.oID
LEFT JOIN addresses [PropertyAddress] on PropertyAddress.address = Property.PropertyAddress
LEFT JOIN states [PropertyState] on PropertyAddress.state = PropertyState.state


GO