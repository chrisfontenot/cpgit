USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_rpps_manual_batches]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_rpps_manual_batches] AS
SELECT	rpps_file, date_created, CONVERT(varchar(50), NULL) AS note
FROM	rpps_files WITH (NOLOCK)
WHERE	file_type = 'EFT'
GO
