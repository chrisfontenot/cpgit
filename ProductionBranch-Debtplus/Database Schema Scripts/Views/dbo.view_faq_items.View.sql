USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_faq_items]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_faq_items] as

-- Obtain the standard items
select	grouping,
	faq_item,
	description,
	isnull(attributes,'') as attributes
from	faq_items with (nolock)
where	grouping <> 'REFERRAL'

union all

-- Merge with the current referral list
select	'REFERRAL' as grouping,
	referred_by as faq_item,
	description,
	'' as attributes
from	referred_by with (nolock)
GO
