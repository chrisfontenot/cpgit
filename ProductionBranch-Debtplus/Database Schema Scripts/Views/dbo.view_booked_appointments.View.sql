USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_booked_appointments]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_booked_appointments] as

-- ==================================================================================================
-- ==            Fetch the appointment list for the client.                                        ==
-- ==================================================================================================

-- ChangeLog
--   12/24/2002
--     Removed "housing" from the client_appointments table
--    6/19/2008
--     Added "class" to determine if the appointment is a workshop or not

SELECT

	ca.*,
	ca.start_time															as 'formatted_start_time',
	
	ISNULL(o.name, 'Unknown')												as 'formatted_office',

	case
		when ca.appt_type IS NULL then 'Any Type'
		else ISNULL(t.appt_name,'Unknown')
	end																		as 'formatted_type',
	
	dbo.format_normal_name(default,con.first, default, con.last, default)	as 'formatted_counselor',

	CONVERT(bit,isnull(t.housing,0))										as 'formatted_housing',
	
	case
		when ca.date_confirmed is null then 0
		else isnull(ca.confirmation_status,0)
	end																		as 'formatted_confirmation_status',
	
	'C'																		as 'class'

FROM		client_appointments ca	WITH (NOLOCK)
LEFT OUTER JOIN appt_times         a	WITH (NOLOCK) ON ca.appt_time = a.appt_time
LEFT OUTER JOIN	counselors         c	WITH (NOLOCK) ON ca.counselor = c.counselor
LEFT OUTER JOIN names              con  WITH (NOLOCK) ON c.NameID     = con.name
LEFT OUTER JOIN	offices            o	WITH (NOLOCK) ON a.office = o.office
LEFT OUTER JOIN	appt_types         t	WITH (NOLOCK) ON ca.appt_type = t.appt_type
WHERE	status		= 'P'
AND     ca.office is not null

UNION ALL

SELECT
	ca.*,
	wk.start_time															as 'formatted_start_time',
	
	ISNULL(wko.name, 'Unknown')												as 'formatted_office',

	wkt.description															as 'formatted_type',
	
	dbo.format_normal_name(default,wkcon.first, default, wkcon.last, default)	as 'formatted_counselor',

	CONVERT(bit,0)															as 'formatted_housing',
	
	case
		when ca.date_confirmed is null then 0
		else isnull(ca.confirmation_status,0)
	end																		as 'formatted_confirmation_status',
	
	'W'																		as 'class'

FROM		client_appointments ca	WITH (NOLOCK)
LEFT OUTER JOIN workshops          wk	WITH (NOLOCK) ON ca.workshop = wk.workshop
LEFT OUTER JOIN counselors         wkco WITH (NOLOCK) ON wk.counselor = wkco.person
LEFT OUTER JOIN names              wkcon WITH (NOLOCK) ON wkco.NameID = wkcon.name
LEFT OUTER JOIN workshop_locations wko	WITH (NOLOCK) ON wk.workshop_location = wko.workshop_location
LEFT OUTER JOIN workshop_types     wkt	WITH (NOLOCK) ON wk.workshop_type = wkt.workshop_type

WHERE	status		= 'P'
AND     ca.workshop is not null
GO
