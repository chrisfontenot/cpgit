USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_last_disb_date]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_last_disb_date]
AS
SELECT     client, MAX(date_created) AS last_disb_date
FROM         dbo.registers_client
WHERE     (tran_type = 'ad')
GROUP BY client
GO
