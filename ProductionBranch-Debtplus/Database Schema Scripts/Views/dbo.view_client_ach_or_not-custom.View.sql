USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_ach_or_not-custom]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_ach_or_not-custom]
AS
SELECT     dbo.date_only(d.deposit_date) AS deposit_date, ach.isActive, ach.ABA AS ach_routing_number, v.active_status, v.name, d.deposit_amount, 
                      ach.AccountNumber AS ach_bank_number, c.client, ach.CheckingSavings AS ach_account, ach.PrenoteDate AS ach_prenote_date, ach.StartDate AS ach_start_date, 
                      ach.ErrorDate AS ach_error_date
FROM         dbo.clients AS c WITH (NOLOCK) INNER JOIN
                      dbo.view_client_address AS v WITH (NOLOCK) ON c.client = v.client LEFT OUTER JOIN
                      dbo.client_ach AS ach WITH (NOLOCK) ON c.client = ach.client INNER JOIN
                      dbo.client_deposits AS d WITH (NOLOCK) ON c.client = d.client
GO
