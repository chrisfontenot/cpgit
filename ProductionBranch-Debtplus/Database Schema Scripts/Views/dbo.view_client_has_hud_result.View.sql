USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_has_hud_result]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_has_hud_result] as

select client
from hud_interviews
where hud_result is not null
GO
