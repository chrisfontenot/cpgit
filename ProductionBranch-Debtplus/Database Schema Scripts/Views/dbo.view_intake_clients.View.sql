USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_intake_clients]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_intake_clients] AS

select	c.intake_client,
		c.intake_id,
		case
			when c.date_imported is not null	then	'IMPORTED as client # ' + right('0000000' + convert(varchar, client),7) + isnull(' on ' + convert(varchar(10), date_imported, 101),'') + isnull(' by ' + imported_by,'')
			when c.identity_date is not null	then	'COMPLETED' + isnull(' on ' + convert(varchar(10), c.identity_date, 101),'')
			when c.completed_date is not null	then	'COMP w/o NAME' + isnull(' on ' + convert(varchar(10), c.completed_date, 101),'')
			when datediff(m, c.date_created, getdate()) > 60 then 'IN PROGRESS' + isnull(' (started ' + convert(varchar(10), c.date_created, 101) + ')','')
			else					'ABANDONED'+ isnull(' (started ' + convert(varchar(10), c.date_created, 101) + ')','')
		end	as 'status',

		c.address1,
		c.address2,
		dbo.format_city_state_zip ( c.city, c.state, c.postalcode ) as address3,
		dbo.format_phone_number ( c.home_ph ) as home_ph,
		dbo.format_phone_number ( c.message_ph ) as message_ph,

		housing_status.description	as housing_status,
		housing_type.description	as housing_type,
		referred_by.description		as referred_by,
		cause_fin_problem.description	as cause_fin_problem1,

		isnull(c.fed_tax_owed,0)	as fed_tax_owed,
		isnull(c.state_tax_owed,0)	as state_tax_owed,
		isnull(c.local_tax_owed,0)	as local_tax_owed,

		isnull(c.fed_tax_months,0)	as fed_tax_months,
		isnull(c.state_tax_months,0)	as state_tax_months,
		isnull(c.local_tax_months,0)	as local_tax_months,

		marital_status.description	as marital_status,
		isnull(c.people,0)		as people,

		convert(varchar(1024),null)	as message,

		client				as client,
		date_imported		as date_imported,
		imported_by			as imported_by,	

		c.created_by			as created_by,
		c.date_created			as date_created,
		c.intake_agreement		as intake_agreement,

		convert(varchar(10),case c.bankruptcy when 0 then '' else 'Yes' end)	as bankruptcy

from	intake_clients c with (nolock)
left outer join referred_by referred_by with (nolock) on c.referred_by    = referred_by.referred_by
left outer join HousingTypes housing_type   with (nolock) on c.housing_type   = housing_type.oID
left outer join Housing_StatusTypes housing_status with (nolock) on c.housing_status = housing_status.oID
left outer join messages marital_status with (nolock) on c.marital_status = marital_status.item_value and 'MARITAL'        = marital_status.item_type
left outer join financial_problems cause_fin_problem with (nolock) on c.cause_fin_problem1 = cause_fin_problem.financial_problem
GO
