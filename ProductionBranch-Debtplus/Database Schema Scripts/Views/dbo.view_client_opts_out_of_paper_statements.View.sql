USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_opts_out_of_paper_statements]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_opts_out_of_paper_statements]
AS
SELECT     client, indicator
FROM         dbo.client_indicators
WHERE     (indicator = 134)
GO
