SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [view_last_payment] as

-- =========================================================================================================
-- ==           Return information about the last payment of a debt                                       ==
-- =========================================================================================================

select 
	cc.client_creditor,
	cc.client,
	cc.creditor,
	isnull(cc.reassigned_debt,0) as reassigned_debt,

	isnull(rcc.debit_amt,0) as 'last_payment_amount',
	rcc.date_created as 'last_payment_date',
	rcc.tran_type as 'last_tran_type',

	case
		when rcc.tran_type = 'BW'	then null
						else tr.checknum
	end as last_checknum,

	case
		when rcc.void = 1		then 'V'
		when rcc.tran_type = 'BW'	then 'R'
		when tr.trust_register is null	then null
						else isnull(tr.cleared,' ')
	end as last_cleared,

	case
		when rcc.tran_type = 'BW'	then tr.date_created
						else tr.reconciled_date
	end as 'last_reconciled',

	rcc.created_by as 'last_paid_by',

	coalesce(cc.account_number, cc.message, 'MISSING')	as account_number,
	cc.client_creditor_balance				as nccrc_debt_number,
	cc.person						as account_person,

	case
		when ltrim(rtrim(isnull(cc.client_name,''))) <> '' then cc.client_name
		when cc.person is not null and ltrim(rtrim(isnull(pccn.last,''))) = '' then 'UNKNOWN NAME'
		when cc.person is not null then dbo.format_normal_name ( pccn.prefix, pccn.first, pccn.middle, pccn.last, pccn.suffix )
		else dbo.format_normal_name ( p1n.prefix, p1n.first, p1n.middle, p1n.last, p1n.suffix )
	end							as client_name,

	case
		when ltrim(rtrim(isnull(cc.client_name,''))) <> '' then cc.client_name
		when cc.person is not null and ltrim(rtrim(isnull(pccn.last,''))) = '' then 'UNKNOWN NAME'
		when cc.person is not null then dbo.format_reverse_name ( pccn.prefix, pccn.first, pccn.middle, pccn.last, pccn.suffix )
		else dbo.format_reverse_name ( p1n.prefix, p1n.first, p1n.middle, p1n.last, p1n.suffix )
	end							as client_name_last,

	case
		when cc.person is not null then isnull(dbo.format_ssn (pcc.ssn),'000-00-0000')
		else isnull(dbo.format_ssn (p1.ssn),'000-00-0000')
	end							as ssn,

	isnull(pcc.birthdate, p1.birthdate) as birthdate,

	coalesce(cc.start_date, c.start_date, getdate()) as start_date,
	isnull(bal.orig_balance,0) as orig_balance,
	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as current_balance,
	isnull(bal.total_payments,0) as total_payments,

	case
		when cc.balance_verify_date is not null and cc.balance_verify_by is not null then cc.balance_verify_date
		else null
	end as balance_verify_date,

	case
		when cc.balance_verify_date is not null and cc.balance_verify_by is not null then cc.balance_verify_by
		else null
	end as balance_verify_by,
	
	upper(case
		when ltrim(rtrim(isnull(cc.client_name,''))) <> '' then cc.client_name
		when cc.person is not null and ltrim(rtrim(isnull(pccn.last,''))) = '' then 'UNKNOWN NAME'
		when cc.person is not null then dbo.format_normal_name ( default, pccn.first, default, pccn.last, default )
		else dbo.format_normal_name ( default, p1n.first, default, p1n.last, default )
	end)						as proposal_client_name

from	client_creditor cc with (nolock)
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
left outer join clients c with (nolock) on cc.client = c.client
left outer join people pcc with (nolock) on cc.client = pcc.client and cc.person = pcc.person
left outer join names pccn with (nolock) on pcc.NameID = pccn.name
left outer join people p1 with (nolock) on cc.client = p1.client and 1 = p1.relation
left outer join names p1n with (nolock) on p1.NameID = p1n.name
left outer join registers_client_creditor rcc with (nolock) on cc.last_payment = rcc.client_creditor_register
left outer join registers_trust tr with (nolock) on rcc.trust_register = tr.trust_register
GO
