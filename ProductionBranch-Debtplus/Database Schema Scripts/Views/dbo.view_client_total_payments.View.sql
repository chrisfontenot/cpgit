USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_total_payments]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* Find the total payments for the clients while on the program. It is used
 for a letter field.
 Change log 
 2/3/2003 added the cut off date of 1/1/2004.*/
CREATE VIEW [dbo].[view_client_total_payments]
AS
SELECT     client, SUM(ISNULL(debit_amt, 0) - ISNULL(credit_amt, 0)) AS total_disbursements
FROM         dbo.registers_client WITH (nolock)
WHERE     (tran_type IN ('AD', 'MD', 'CM', 'RF', 'VD', 'RR')) AND (date_created < '1/1/2010') AND (client > 0)
GROUP BY client
GO
