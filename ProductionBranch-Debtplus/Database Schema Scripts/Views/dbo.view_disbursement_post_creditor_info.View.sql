SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [view_disbursement_post_creditor_info] as

-- =========================================================================================================
-- ==       Return the information about the current creditor for the post processing                     ==
-- =========================================================================================================

-- ChangeLog
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

select		cr.creditor							as 'creditor',
		convert(int,isnull(cr.voucher_spacing,0))			as 'voucher_spacing',
		convert(money,isnull(cr.max_amt_per_check,0))		as 'max_amt_per_check',
		convert(money,isnull(cr.max_fairshare_per_debt,0))		as 'max_fairshare_per_debt',
		convert(int,isnull(cr.max_clients_per_check,0))			as 'max_clients_per_check'
FROM		creditors cr with (nolock)
GO
