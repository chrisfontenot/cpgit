USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_ChexSystems_1]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_ChexSystems_1] AS

-- =====================================================================================================
-- ==       Retreive the list of clients for the workshop                                             ==
-- =====================================================================================================

SELECT TOP 100 PERCENT		c.client as 'CLIENT',
				isnull(c.active_status,'CRE') as 'STATUS',
				isnull(pn.first,'') as 'FIRST',
				isnull(pn.last,'') + isnull(', '+pn.suffix,'') as 'LAST',
				ltrim(rtrim(isnull(dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),'') + ' ' + isnull(a.address_line_2,''))) as 'ADDRESS',
				isnull(a.city,'') as 'CITY',

				case left(isnull(a.postalcode,'00000'),5)
					when '00000' then ''
					else left(isnull(a.postalcode,'00000'),5)
				end as 'ZIPCODE',

				dbo.format_TelephoneNumber ( c.HomeTelephoneID ) as 'PHONE',

				w.workshop as 'EVENT ID'

FROM				client_appointments w
inner join clients c		on w.client = c.client
left outer join people p	on w.client = p.client and 1 = p.relation
left outer join Names pn with (nolock) on p.NameID = pn.Name
left outer join addresses a with (nolock) on c.AddressID = a.address
where				w.workshop is not null

ORDER BY 4, 3, 1
GO
