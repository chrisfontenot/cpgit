USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_pending_disbursement_clients]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_pending_disbursement_clients]
AS

-- =================================================================================================================
-- ==            Return the list of pending client information for the disbursement batch                         ==
-- =================================================================================================================

SELECT
		case
			when held_in_trust < disbursement_factor and note_count  = 0 then 0
			when held_in_trust < disbursement_factor and note_count != 0 then 1
			when held_in_trust = disbursement_factor and note_count  = 0 then 2
			when held_in_trust = disbursement_factor and note_count != 0 then 3
			when held_in_trust > disbursement_factor and note_count  = 0 then 4
			when held_in_trust > disbursement_factor and note_count != 0 then 5
		end as [group],

		c.disbursement_register,
		c.client,
		dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) AS name,
		c.last_name AS [name_group],

		c.held_in_trust,
		c.disbursement_factor,
		c.note_count,
		isnull(rc.debit_amt,0) as 'disbursed'

FROM		view_disbursement_clients c	WITH (NOLOCK)
left outer join	registers_client rc	WITH (NOLOCK) ON c.client_register = rc.client_register
LEFT OUTER JOIN people p		WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
GO
