USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_creditor_proposals]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_creditor_proposals] as
	select	pr.*, b.date_closed as batch_date_closed, m.note_text as proposal_note
	from	client_creditor_proposals pr
	left outer join proposal_batch_ids b on pr.proposal_batch_id = b.proposal_batch_id
	left outer join debt_notes m on pr.client_creditor_proposal = m.client_creditor_proposal and 'PR' = m.type
GO
