USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_proposal_letter_fields]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_proposal_letter_fields] as

-- ================================================================================================
-- ==       Include the information about the latest proposal for a debt                         ==
-- ================================================================================================

select	cc.client, cc.creditor,
	cc.client_creditor,

	case isnull(pr.proposal_status,0)
		when 0 then 'Pending'
		when 1 then 'Pending'
		when 2 then 'Accepted'
		when 3 then 'Accepted by Default'
		when 4 then 'Denied'
	end			as proposal_status,

	proposal_print_date,
	dn.note_text as proposal_message,
	proposal_accepted_by,
	proposal_status_date,

	rej.description		as reject_reason,
	disp.description	as reject_disposition,
	pr.counter_amount,
	pr.missing_item,
	pr.proposed_amount,
	pr.proposed_start_date,
	pr.proposed_balance,
	ids.date_created	as batch_date_created,
	ids.created_by		as batch_created_by

from	client_creditor cc
left outer join client_creditor_proposals pr with (nolock)	on cc.client_creditor_proposal = pr.client_creditor_proposal
left outer join proposal_result_reasons rej with (nolock)	on pr.proposal_reject_reason = rej.proposal_result_reason
left outer join proposal_result_dispositions disp with (nolock)	on pr.proposal_reject_reason = disp.proposal_result_disposition
left outer join proposal_batch_ids ids with (nolock)		on pr.proposal_batch_id = ids.proposal_batch_id
left outer join debt_notes dn with (nolock)			on pr.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type
GO
