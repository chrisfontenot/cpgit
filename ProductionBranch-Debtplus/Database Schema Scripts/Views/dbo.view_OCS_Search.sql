USE [DEBTPLUS]
GO

/****** Object:  View [dbo].[view_OCS_Search]    Script Date: 12/1/2015 10:20:51 AM ******/
DROP VIEW [dbo].[view_OCS_Search]
GO

/****** Object:  View [dbo].[view_OCS_Search]    Script Date: 12/1/2015 10:20:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[view_OCS_Search] as

WITH Report AS (SELECT id, AttemptId, BatchName FROM OCS_UploadReport WHERE ID IN (SELECT ID FROM OCS_Client)),
	  OCS AS (SELECT OCS.ID, ClientID, ActiveFlag, Archive, InvestorNumber, SearchServicerId, UploadRecord, BatchName, [Description]
			FROM OCS_Client OCS
			LEFT JOIN OCS_PartnerProgram Program ON ocs.Program = program.Id
			LEFT JOIN Report rpt ON ocs.UploadAttempt = rpt.AttemptId AND ocs.id = rpt.ID),
	OCSUpload AS (SELECT DISTINCT Id, ClientStreet, ClientCity, ClientState, ClientZipcode, SSN1, 
						FirstName1, LastName1, Phone1, Phone2, Phone3, Phone4, Phone5, Servicer, LoanNumber
			FROM OCS_UploadRecord)
SELECT
    ocs.Id [Id],
      ocs.ClientId [ClientId],
      ocs.ActiveFlag,
      ocs.Archive,
      ocs.BatchName,
      LTRIM(RTRIM(ocs.InvestorNumber)) [InvestorNo], 
      LTRIM(RTRIM(upload.ClientStreet)) [StreetAddress],
      LTRIM(RTRIM(upload.ClientCity)) [City],
      LTRIM(RTRIM(upload.ClientState)) [StateAbbreviation],
      LTRIM(RTRIM(upload.ClientZipcode)) [ZipCode],
      LTRIM(RTRIM(upload.SSN1)) [AppSSN],
      LTRIM(RTRIM(ocs.Description)) [Program],
      LTRIM(RTRIM(upload.FirstName1)) [AppFirstName],
      LTRIM(RTRIM(upload.LastName1)) [AppLastName],
      LTRIM(RTRIM(upload.Phone1)) [Phone1],
      LTRIM(RTRIM(upload.Phone2)) [Phone2],
      LTRIM(RTRIM(upload.Phone3)) [Phone3],
      LTRIM(RTRIM(upload.Phone4)) [Phone4],
      LTRIM(RTRIM(upload.Phone5)) [Phone5],
      LTRIM(RTRIM(upload.Servicer)) [Servicer],
      LTRIM(RTRIM(upload.LoanNumber)) [ServicerLoanNum],
      ocs.SearchServicerId [ServicerId]

FROM OCS [ocs]
LEFT JOIN OCSUpload [upload] ON upload.Id = ocs.UploadRecord
WHERE ClientID IN (SELECT Client FROM clients)



GO