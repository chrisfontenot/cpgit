USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_regions]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_regions] as
-- =================================================================================================
-- ==       Return the region for the specific client. It is used by the letters.                 ==
-- =================================================================================================
select	c.client, r.description as region, r.ManagerName
from	clients c with (nolock)
left outer join regions r with (nolock) on c.region = r.region
GO
