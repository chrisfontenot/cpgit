USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_intake_hud_loans]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_intake_hud_loans] AS

select	p.intake_client			as intake_client,
		p.secured_property		as secured_property,
		p.secured_type			as secured_type,
		p.description			as description,
		p.sub_description		as sub_description,
		p.original_price		as original_price,
		p.current_value			as current_value,
		p.year_acquired			as year_acquired,
		p.year_mfg				as year_mfg,
		p.housing_type			as housing_type,
		p.primary_residence		as primary_residence,

		isnull(h.secured_loan,0)	as secured_loan,
		isnull(h.priority,0)		as priority,
		isnull(h.lender,'')			as lender,
		isnull(h.account_number,'')	as account_number,
		isnull(h.case_number,'')	as case_number,
		isnull(h.interest_rate,0)	as interest_rate,
		isnull(h.payment,0)			as payment,
		isnull(h.original_amount,0)	as original_amount,
		isnull(h.balance,0)			as balance,
		isnull(h.periods,0)			as periods,
		isnull(h.past_due_amount,0)	as past_due_amount,
		isnull(h.past_due_periods,0)	as past_due_periods,
		isnull(h.loan_type,0)		as loan_type,

		pos.description				as priority_description,
		typ.description				as loan_type_description,
		sec.description				as secured_type_description,

		p.date_created				as date_created,
		p.created_by				as created_by

from	intake_secured_properties p with (nolock)
left outer join intake_secured_loans h with (nolock) on p.secured_property = h.secured_property
left outer join Housing_LoanPositionTypes pos on h.priority = pos.oID
left outer join Housing_LoanTypes typ on h.loan_type = typ.oID
left outer join secured_types sec on p.secured_type = sec.secured_type
GO
