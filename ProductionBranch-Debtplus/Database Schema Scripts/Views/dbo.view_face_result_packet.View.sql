USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_face_result_packet]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_face_result_packet] as
-- ====================================================================================
-- ==       Primary dataset in the face-result packet                                ==
-- ====================================================================================
SELECT top 100 percent c.client, p.person, dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as client_name, isnull(c.salutation,dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)) as salutation, dbo.address_block_5 ( dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix), dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value), a.address_line_2, default, dbo.format_city_state_zip(a.city, a.state, a.postalcode)) as address, a.PostalCode, cc.creditor, cc.disbursement_factor, cr.creditor_name, ca.start_time, dbo.format_normal_name(default,cox.first,default,cox.last,default) as name
from clients c with (nolock)
left outer join addresses a with (nolock) on c.addressid = a.address
left outer join states st with (nolock) on a.state = st.state
inner join client_creditor cc with (nolock) on c.client = cc.client
inner join client_appointments ca with (nolock) on c.client = ca.client
inner join people p with (nolock) on c.client = p.client and p.relation = 1
left outer join names pn with (nolock) on p.nameid = pn.name
inner join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join counselors co with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
where cc.creditor like 'x%'
order by 1
GO
