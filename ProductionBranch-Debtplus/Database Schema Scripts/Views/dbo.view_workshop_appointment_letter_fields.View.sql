USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_workshop_appointment_letter_fields]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_workshop_appointment_letter_fields] as
-- ====================================================================================================================
-- ==           Workshop letter information                                                                          ==
-- ====================================================================================================================

select	c.client,						-- Client ID
		ca.start_time,					-- Workshop start time
		co.counselor as counselor_id,			-- Workshop counselor numerical ID
		ca.workshop as workshop_id,			-- Workshop numerical ID
		ca.client_appointment as appointment_id,	-- Appointment row numerical ID
		ca.status,					-- Appointment status (for selection criteria)
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as counselor,				-- Name of the counselor
		wt.description as workshop_name,		-- Name of the workshop
		wl.name as workshop_location,			-- Workshop location name
		dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as workshop_addr1,			-- Address line 1 to the workshop
		a.address_line_2 as workshop_addr2,			-- Address line 2 to the workshop
		dbo.format_city_state_zip (a.city, a.state, a.postalcode) as workshop_addr3,
		a.city as workshop_city,			-- City of the workshop
		st.MailingCode as workshop_state,		-- State the workshop
		a.postalcode as workshop_zipcode,		-- Zipcode to the workshop

		dbo.format_TelephoneNumber(wl.TelephoneID) as workshop_phone,			-- Local phone # of the workshop

		convert(varchar(8000), wl.directions) as workshop_directions	-- Generic driving directions to the workshop

from		clients c with (nolock)

-- This statement pulls the single appointment that is the most recently created item from the appointments table.
left outer join	client_appointments ca with (nolock) on c.client = ca.client and (ca.client_appointment = (select top 1 client_appointment from client_appointments cap with (nolock) where cap.client = c.client and cap.status = 'P' and cap.workshop is not null order by date_created desc))

-- Include the office and counselor names
left outer join workshops w with (nolock) on ca.workshop = w.workshop
left outer join workshop_types wt with (nolock) on w.workshop_type = wt.workshop_type
left outer join workshop_locations wl with (nolock) on w.workshop_location = wl.workshop_location
left outer join addresses a with (nolock) on wl.addressid = a.address
left outer join states st with (nolock) on a.state = st.state
left outer join counselors co with (nolock) on w.counselor = co.person
left outer join names cox with (nolock) on co.NameID = cox.name

where		ca.status != 'R'
GO
