USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_deposit_batch_ids]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_deposit_batch_ids] as
-- ==================================================================================
-- ==     View used to select a deposit batch from the deposit batch IDs           ==
-- ==================================================================================

-- ChangeLog
--   6/1/2011
--      Initial creation

SELECT db.deposit_batch_id, db.bank, db.date_created, db.date_closed, db.date_posted, db.batch_type, dbo.format_counselor_name(db.created_by) as created_by, db.note, b.description, isnull(SUM(dd.amount),0) AS amount
FROM deposit_batch_ids db WITH (NOLOCK)
LEFT OUTER JOIN banks b WITH (NOLOCK) ON db.bank = b.bank
LEFT OUTER JOIN deposit_batch_details dd WITH (NOLOCK) ON db.deposit_batch_id = dd.deposit_batch_id AND 1 = dd.ok_to_post
group by db.deposit_batch_id, db.bank, db.date_created, db.date_closed, db.date_posted, db.batch_type, dbo.format_counselor_name(db.created_by), db.note, b.description
GO
