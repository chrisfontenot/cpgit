USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_rpps_transactions]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_rpps_transactions] as

select	t.rpps_file						as 'rpps_file',
	coalesce(d.trace_number, t.trace_number_first, '')	as 'trace_number',
	isnull(t.biller_id,'')					as 'biller_id',
	isnull(t.client,-1)					as 'client',
	ISNULL(v.account_number, 'MISSING')			AS 'account_number',
	d.return_code						as 'return_code',
	isnull(td.description,'')				as 'description',
	isnull(t.rpps_transaction,-1)				as 'rpps_transaction'

from		rpps_transactions t		with (nolock)
left outer join rpps_response_details d		with (nolock) on t.rpps_transaction = d.rpps_transaction and d.processing_error is null
left outer join rpps_reject_codes td		with (nolock) on t.return_code = td.rpps_reject_code
left outer join registers_client_creditor rcc	with (nolock) on t.client_creditor_register = rcc.client_creditor_register
LEFT OUTER JOIN view_last_payment v		WITH (NOLOCK) ON rcc.client_creditor = v.client_creditor

where	t.service_class_or_purpose = 'CIE'
and	t.transaction_code = '22'
GO
