USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_debt_transactions]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_creditor_debt_transactions] as
-- =========================================================================================
-- ==       Information for the creditor.debt.transactions report                         == 
-- =========================================================================================
select		rcc.creditor,
			cr.creditor_name,
			c.office,
			o.name			as office_name,
			c.client,
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as client_name,
			isnull(rcc.account_number, cc.account_number) as account_number,
			c.counselor,			
			dbo.format_normal_name(cox.prefix,cox.first,cox.middle,cox.last,cox.suffix) as counselor_name,
			c.active_status,
			st.mailingcode as state,
			cc.disbursement_factor,
			b.orig_balance,
			b.total_payments,
			b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as current_balance,
			cc.start_date,
			rcc.debit_amt,
			rcc.date_created
from		clients c with (nolock)
left outer join addresses a with (nolock) on c.addressid = a.address
left outer join states st with (nolock) on a.state = st.state
left outer join offices o with (nolock) on c.office = o.office
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join counselors co with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
inner join client_creditor cc with (nolock) on c.client = cc.client_creditor
inner join creditors cr with (nolock) on cc.creditor = cr.creditor
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance
inner join registers_client_creditor rcc with (nolock) on cc.client_creditor = rcc.client_creditor
where		rcc.tran_type in ('AD','MD','CM','BW')
GO
