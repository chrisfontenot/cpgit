USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_ABT]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[view_ABT] as

-- ===========================================================================================================
-- ==               Information needed for the ABT excel sheet extract routine                              ==
-- ===========================================================================================================

select ca.partner as [PARTICIPANT_ID],
	   case when ca.workshop is not null then 1 else 2 end as [TYPE_OF_SESSION],
	   isnull(upper(pn.first),'') as [BORR_FNAME],
	   isnull(upper(pn.middle),'') as [BORR_MID],
	   isnull(upper(pn.last),'') as [BORR_LNAME],
	   '' as [NUMBER_OF_EDU_SESS_REQUIRED],
	   '' as [DATE_OF_SESSION],
	   '' as [IS_FIRSTEDU_SESSION],
	   '' as [SESSION_DURATION],
	   '' as [TOTAL_HOUSEHOLD_MEMBERS],
	   '' as [TOTAL_GROUP_PARTICIPANTS],
	   '' as [FORMAT_OF_COUNSELING_SESSION],
	   upper(dbo.format_normal_name(default,con.first,con.middle,con.last,default)) as [COUNSELOR_NAME],
	   aptxt.[text] as [ACTION STEPS],
	   upper(vot.description) as [OUTCOMES],
	   '' as [SERVICE_INITIATED],
	   convert(varchar(10),ca.start_time,101) as [DATE_SERVICE_INITIATED],
	   '' as [EDU_WORKSHOP_COMPLETED],
	   '' as [DATE_EDU_WORKSHOP_COMPLETED],
	   '' as [ONE_ON_ONE_COMPLETED],
	   case when ca.status in ('K','W') then convert(varchar(10), ca.start_time, 101) else '' end as [DATE_ONE_ON_ONE_COMPLETED],
	   dbo.format_client_id (ca.client) as [CMAX_ID],
	   ca.client_appointment as [CMAX_SESSION_ID],
	   ca.start_time as [START_TIME]
from client_appointments ca
left outer join appt_types apt on ca.appt_type = apt.appt_type
left outer join people p on ca.client = p.client and 1 = p.relation
left outer join names pn on p.nameid = pn.name
left outer join counselors co on ca.counselor = co.counselor
left outer join names con on co.nameid = con.name
left outer join action_items_goals aptxt on dbo.map_client_to_action_plan(ca.client) = aptxt.action_plan and 0 < aptxt.action_plan
left outer join hud_transactions ht on ca.client_appointment = ht.client_appointment
left outer join hud_interviews hi on ht.hud_interview = hi.hud_interview
left outer join Housing_VisitOutcomeTypes vot on hi.hud_result = vot.oid
where		ca.partner like '[0-9]%'
GO
