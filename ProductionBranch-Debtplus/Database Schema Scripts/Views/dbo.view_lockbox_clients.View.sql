USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_lockbox_clients]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_lockbox_clients] as

-- ============================================================================================================
-- ==           Extract information for Sacramento's lockbox provider                                        ==
-- ============================================================================================================

select		dbo.format_client_id(c.client) + char(9) + left(ltrim(isnull(rtrim(upper(pn.first)), ' ')),7)  + char(9) + left(ltrim(isnull(rtrim(upper(pn.last)), ' ')),40) as item
from		clients c with (nolock)
left outer join client_ach ach with (nolock) on c.client = ach.client
left outer join	people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.name
where		c.active_status IN ('A','AR')
AND			isnull(ach.isActive,0) = 0
AND			c.client in (
	select	client as x
	from	client_deposits
	where	deposit_amount > 0
	and	one_time = 0
)
GO
