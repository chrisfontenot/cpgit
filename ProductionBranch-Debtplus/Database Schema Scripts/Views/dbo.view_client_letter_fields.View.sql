USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_letter_fields]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_letter_fields] AS
-- =====================================================================================================
-- ==         Used in writing letters where the textual descriptions are desired rather than the      ==
-- ==         code values stored in the tables. The letter writer does not know how to map the        ==
-- ==         key fields to a corresponding text description.                                         ==
-- =====================================================================================================
select	c.client, 	mmarital.description as 'marital_status'
from	clients c WITH (NOLOCK)
left outer join MaritalTypes mmarital WITH (NOLOCK) on c.marital_status = mmarital.oID
GO
