USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_held_in_trust]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_held_in_trust] AS

SELECT		convert(int, case
			when c.start_date > getdate()				then 1
			when c.hold_disbursements != 0				then 1
			when isnull(c.active_status,' ') NOT IN ('A', 'AR')	then 5
			when c.held_in_trust < 0				then 4
			when c.held_in_trust < 500				then 2
										else 3
		end) as 'group',

		c.held_in_trust	as 'held_in_trust',
		c.client	as 'client',
		convert(varchar(80), dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)) as 'name',

		convert(money,case
			when c.reserved_in_trust_cutoff is null then 0
			when c.reserved_in_trust_cutoff <= convert(datetime, convert(varchar(10), getdate(), 101)) then 0
			when c.reserved_in_trust < 0 then 0
			else c.reserved_in_trust
		end) as reserved_in_trust,

		convert(datetime, case
			when c.reserved_in_trust_cutoff is null then null
			when c.reserved_in_trust_cutoff <= convert(datetime, convert(varchar(10), getdate(), 101)) then null
			else convert(varchar(10), c.reserved_in_trust_cutoff, 101)
		end) as reserved_in_trust_cutoff,

		c.counselor							as 'counselor',
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor_name',
		pn.last								as 'last_name',
		isnull(st.mailingcode,'')					as 'state',
		isnull(c.disbursement_date,1)					as 'disbursement_cycle'

FROM		clients c WITH (NOLOCK)
LEFT OUTER JOIN	people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
LEFT OUTER JOIN Names pn WITH (NOLOCK) ON p.Nameid = pn.name
LEFT OUTER JOIN counselors co WITH (NOLOCK) ON c.counselor = co.counselor
LEFT OUTER JOIN names cox with (nolock) ON co.NameID = cox.name
left outer join addresses a with (nolock) on c.addressid = a.address
LEFT OUTER JOIN states st WITH (NOLOCK) ON a.state = st.state
GO
