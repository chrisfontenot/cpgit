USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_clients_on_ach]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_clients_on_ach] as

-- ===============================================================================================
-- ==   List of clients who are on ACH                                                          ==
-- ===============================================================================================
SELECT	dbo.date_only(d.deposit_date) as deposit_date,
		c.ach_active,
		ach.aba as ach_routing_number,
		v.active_status,
		v.name,
		d.deposit_amount,
		ach.accountnumber as ach_bank_number,
		c.client,
		ach.checkingsavings as ach_account,
		ach.prenotedate as ach_prenote_date,
		ach.startdate as ach_start_date,
		ach.errordate as ach_error_date
FROM	clients c WITH (NOLOCK)
INNER JOIN view_client_address v WITH (NOLOCK) ON c.client=v.client
INNER JOIN client_ach ach WITH (NOLOCK) ON c.client = ach.client
INNER JOIN client_deposits d WITH (NOLOCK) ON c.client=d.client
WHERE ach.IsActive = 1
AND   ach.aba LIKE '_%'
GO
