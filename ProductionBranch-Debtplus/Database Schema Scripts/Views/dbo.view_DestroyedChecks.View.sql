SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [view_DestroyedChecks] AS
-- =====================================================================================
-- ==       List of the destroyed checks                                              ==
-- =====================================================================================
SELECT
	t.checknum		as 'checknum',
	dbo.date_only (t.date_created) as 'date',
	0 as 'credit',
	t.amount as 'debit',
	case
	 	when t.creditor is not null then '[' + t.creditor + '] ' + ltrim(rtrim(cr.creditor_name))
		when t.client is not null then '[' + dbo.format_client_id (t.client) + '] ' + dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)
	end as payee,
	t.trust_register
FROM	registers_trust t WITH (NOLOCK)
LEFT OUTER JOIN people p WITH (NOLOCK) ON t.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN creditors cr WITH (NOLOCK) ON t.creditor = cr.creditor
WHERE	t.cleared		= 'D'
AND	((t.creditor is not null) or (t.client is not null))
GO
