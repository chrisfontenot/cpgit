USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_ZipCodeSearch]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[view_ZipCodeSearch] as
-- ==================================================================================
-- ==           View to abstract the zipcode lookup routine from the build         ==
-- ==================================================================================
	SELECT	oID				AS 'RecordNumber',
			[ZIPCode]		AS 'ZipCode',
			[ZIPType]		AS 'ZipCodeType',
			[CityName]		AS 'City',
			[State]			AS 'State',
			[CityType]		AS 'LocationType',
			[ActiveFlag]	AS 'ActiveFlag'
	FROM	ZipCodeSearch WITH (NOLOCK)
GO
