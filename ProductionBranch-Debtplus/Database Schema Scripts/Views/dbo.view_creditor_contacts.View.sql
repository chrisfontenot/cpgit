USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_contacts]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_creditor_contacts] AS

SELECT	cc.creditor_contact							as 'creditor_contact',
	cc.creditor										as 'creditor',
	cc.creditor_contact_type						as 'creditor_contact_type',
	isnull(ccn.prefix,'')							as 'prefix',
	isnull(ccn.first,'')							as 'first',
	isnull(ccn.middle,'')							as 'middle',
	isnull(ccn.last,'')								as 'last',
	isnull(ccn.suffix,'')							as 'suffix',
	isnull(cc.title,'')								as 'title',
	isnull(dbo.format_address_line_1(cca.house,cca.direction,cca.street,cca.suffix,cca.modifier,cca.modifier_value),'')							as 'address1',
	isnull(cca.address_line_2,'')					as 'address2',
	isnull(cca.city,'')								as 'city',
	isnull(cca.state,0)								as 'state',
	isnull(cca.postalcode,'')						as 'postalcode',
	isnull(dbo.format_TelephoneNumber(cc.TelephoneID),'')	as 'phone',
	''												as 'extension',
	isnull(dbo.format_TelephoneNumber(cc.FAXID),'')		as 'fax',
	isnull(cce.Address,'')							as 'email',
	isnull(cc.notes,'')								as 'notes',

	-- Formatted (non-record) information
	isnull(dbo.format_TelephoneNumber(cc.TelephoneID),'')					as 'formatted_phone',
	isnull(dbo.format_TelephoneNumber(cc.FAXID),'')					as 'formatted_fax',
	dbo.format_normal_name(ccn.prefix, ccn.first, ccn.middle, ccn.last, ccn.suffix)	as 'name',
	dbo.format_city_state_zip (cca.city, cca.state, cca.PostalCode)			as 'address3',
	isnull(t.description,'')							as 'contact_type_description'

FROM	creditor_contacts cc with (nolock)
left outer join addresses cca with (nolock) on cc.AddressID = cca.address
left outer join Names ccn with (nolock) on cc.NameID = ccn.Name
left outer join EmailAddresses cce with (nolock) on cc.EmailID = cce.Email
left outer join creditor_contact_types t with (nolock) on cc.creditor_contact_type = t.creditor_contact_type
GO
