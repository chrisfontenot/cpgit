USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_people_income]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_people_income] as
	select	client, sum(gross_income) as gross_income, sum(net_income) as net_income
	from	people with (nolock)
	group by client
GO
