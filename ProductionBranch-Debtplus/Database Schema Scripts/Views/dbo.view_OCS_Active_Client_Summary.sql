USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_OCS_Active_Client_Summary]    Script Date: 06/18/2015 12:37:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER view [dbo].[view_OCS_Active_Client_Summary] as

select
ocs.ClientId [Id],
record.FirstName1 + ' ' + record.LastName1 [ClientName],
record.Servicer [ServicerName],
record.LoanNumber [LoanNumber],
record.Uploaddate [ReceivedDate],

case when ocs.Program = 4--FMAC PostMod
then 
	(select top 1 [Begin] 
	from OCS_ContactAttempt 
	where ClientId = ocs.ClientId
	and ResultCode in(4,9,10,12,14,15,16,17,18))--these numbers are "right party contacts"
else
	(select top 1 [Begin] --FMAC EI Family
	from OCS_ContactAttempt 
	where ClientId = ocs.ClientId
	and ResultCode in(2,4,5,9,10,12,13)) --these numbers are "right party contacts"

end [FRPDate],

viewO.ContactAttempts [ContactAttempts],
CONVERT(bit,null) [AppointmentScheduled],
ocs.Program [Program]

from OCS_Client [ocs]
left join OCS_UploadRecord [record] on record.Id = ocs.UploadRecord
left join view_OCS_Client [viewO] on viewO.ClientId = ocs.ClientId
where ocs.ActiveFlag = 1



