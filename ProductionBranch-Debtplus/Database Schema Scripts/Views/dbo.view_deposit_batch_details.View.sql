USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_deposit_batch_details]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_deposit_batch_details] as

-- ==========================================================================================
-- ==       Used by the UnPosted Deposit Detail report                                     ==
-- ==========================================================================================

select
	d.deposit_batch_id	as 'deposit_batch_id',
	d.tran_subtype		as 'type',
	d.ok_to_post		as 'ok',
	d.scanned_client	as 'scanned',
	d.client		as 'client',
	d.date_created		as 'date',
	d.amount		as 'amount',
	dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
	d.reference		as 'reference',
	d.message		as 'status'
from	deposit_batch_details d
left outer join people p on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.name
GO
