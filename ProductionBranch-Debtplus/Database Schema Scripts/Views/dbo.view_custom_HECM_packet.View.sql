USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_custom_HECM_packet]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_custom_HECM_packet] as
	select a.*, isnull(ca.start_time,GETDATE()) as AppointmentDate
	from view_client_address a
	left outer join client_appointments ca on ca.client_appointment = dbo.map_client_to_last_completed_office_appt(a.client)
GO
