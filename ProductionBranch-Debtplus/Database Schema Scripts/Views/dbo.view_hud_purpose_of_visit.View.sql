USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_hud_purpose_of_visit]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_hud_purpose_of_visit] AS
SELECT		iv.hud_interview,
			iv.client,
			m.description AS purpose_of_visit
FROM		hud_interviews iv
INNER JOIN	Housing_PurposeOfVisitTypes m ON iv.interview_type = oID
GO
