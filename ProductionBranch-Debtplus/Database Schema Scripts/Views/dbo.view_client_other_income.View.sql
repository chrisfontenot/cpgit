USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_other_income]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_other_income] as
	select	client, sum(asset_amount) as other_income
	from	assets with (nolock)
	group by client
GO
