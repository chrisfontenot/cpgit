USE [DebtPlus]
GO
/****** Object:  View [dbo].[report_proposals_sent_no_response]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[report_proposals_sent_no_response] as
-- =================================================================================================================
-- ==     proposals that have been sent but have not received a response                                          ==
-- =================================================================================================================
 SELECT top 100 percent
			c.client,
			cc.creditor,
			cc.account_number as account_number,
			ccp.proposal_print_date,
			ccp.proposal_status_date,
			ccp.date_created,
			cc.reassigned_debt,
			dbo.format_normal_name(default,cox.first,default,cox.last,default) as name,
			c.active_status,
			cr.creditor_name,
			b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as curr_bal
 from		clients c with (nolock)
 inner join client_creditor cc with (nolock) on c.client = cc.client
 inner join creditors cr with (nolock) on cc.creditor = cr.creditor
 inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
 inner join counselors co with (nolock) on c.counselor = co.counselor
 left outer join names cox with (nolock) on co.nameid = cox.name
 inner join client_creditor_proposals ccp with (nolock) on cc.client_creditor_proposal = ccp.client_creditor_proposal
 WHERE  ccp.proposal_status = 1
 AND	ccp.proposal_print_date IS NOT NULL
 AND	cr.type not in ('g', 'w', 'x')
 AND	c.active_status in ('A','AR')
 AND    b.orig_balance + b.orig_balance_adjustment + b.total_interest > b.total_payments
 ORDER BY 4, 8, 2 -- v.proposal_print_date, co.name, cc.creditor
GO
