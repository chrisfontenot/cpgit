USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_search]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_search] as
-- ================================================================================================
-- ==         Used in the client search. Return the items in the result list                     ==
-- ================================================================================================
	SELECT c.client as Client, p.ssn as SSN, c.active_status as Status,
		   dbo.format_TelephoneNumber(c.HomeTelephoneID) as HomePhone,
		   dbo.format_reverse_name(default, pn.first, pn.middle, pn.last, pn.suffix) as Name,
		   p.former, pn.Last, p.Birthdate,
		   c.HomeTelephoneID, c.MsgTelephoneID, p.CellTelephoneID as CellTelephoneID_1, p2.CellTelephoneID as CellTelephoneID_2,
		   dbo.address_block_5(dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),
							   a.address_line_2,
							   dbo.format_city_state_zip(a.city, a.state, a.postalcode),
							   default, default) as Address,
		   dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as addr_1,
		   a.address_line_2 as addr_2,
		   dbo.format_city_state_zip(a.city, a.state, a.postalcode) as addr_3,
		   a.city as city
	FROM clients c WITH (NOLOCK)
	LEFT OUTER JOIN people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
	LEFT OUTER JOIN people p2 WITH (NOLOCK) ON c.client = p2.client AND 1 <> p2.relation
	LEFT OUTER JOIN Addresses a WITH (NOLOCK) ON c.AddressID = a.Address
	LEFT OUTER JOIN Names pn WITH (NOLOCK) ON p.NameID = pn.Name
GO
