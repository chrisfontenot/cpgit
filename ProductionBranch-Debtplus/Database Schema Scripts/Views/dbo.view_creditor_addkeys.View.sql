USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_addkeys]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_creditor_addkeys]
AS
SELECT     creditor_addkey AS creditor_prefix, creditor, additional AS prefix, date_created, created_by
FROM         dbo.creditor_addkeys WITH (nolock)
WHERE     (type = 'A')
GO
