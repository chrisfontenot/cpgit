USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_totals]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_creditor_totals] AS
SELECT	creditor, creditor_id,
		creditor_name,
		isnull(distrib_mtd,0)						as 'distrib_mtd',
		isnull(distrib_mtd,0) + isnull(distrib_ytd,0)			as 'distrib_ytd',
		isnull(contrib_mtd_billed,0)					as 'contrib_mtd_billed',
		isnull(contrib_ytd_billed,0) + isnull(contrib_mtd_billed,0)	as 'contrib_ytd_billed',
		isnull(contrib_mtd_received,0)					as 'contrib_mtd_received',
		isnull(contrib_ytd_received,0) + isnull(contrib_mtd_received,0)	as 'contrib_ytd_received',
		[type]					as 'type'
FROM	creditors
GO
