USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_proposal_proof]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_proposal_proof] AS
-- ====================================================================================
-- ==       Generate the information for the proposal proof                          ==
-- ====================================================================================

SELECT     p.proposal_batch_id,
		   cc.client,
		   cc.creditor,
		   cc.client_creditor,
		   p.client_creditor_proposal,
		   convert(varchar(2048),dn.note_text) as proposal_message, p.date_created, 
           ISNULL(dbo.creditors.creditor_name, '') + ISNULL(' ' + dbo.creditors.division, '') AS creditor_name,
		   cc.account_number
FROM		dbo.client_creditor_proposals p with (nolock)
INNER JOIN	dbo.client_creditor cc with (nolock) ON p.client_creditor = cc.client_creditor
INNER JOIN	dbo.creditors with (nolock) ON cc.creditor = dbo.creditors.creditor
LEFT OUTER JOIN	dbo.debt_notes dn with (nolock) on p.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type
GO
