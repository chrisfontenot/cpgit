USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_has_outside_30_rule_indic]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_has_outside_30_rule_indic]
AS
SELECT     dbo.client_indicators.client, dbo.indicators.description, dbo.client_indicators.date_created
FROM         dbo.client_indicators INNER JOIN
                      dbo.indicators ON dbo.client_indicators.indicator = dbo.indicators.indicator
WHERE     (dbo.indicators.description = ' Outside of 30-day Rule')
GO
