USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_housing_counselor_trainings]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_housing_counselor_trainings] AS
-- ==================================================================================================
-- ==       List of the counselor training information for H.U.D.                                  ==
-- ==================================================================================================

SELECT	t.[oID]								as 'oID',
		t.[oID]								as 'TrainingCourse',
		t.[Counselor]						as 'Counselor',
		t.[TrainingDate]					as 'cnslor_training_date',
		c.[Certificate]						as 'TrainingCertificate',
		c.[TrainingTitle]					as 'cnslor_training_title',
		c.[TrainingDuration]				as 'cnslor_training_duration',
		dbo.map_hud_9902_training_organization(c.[TrainingOrganization]) as 'cnslor_training_org',
		c.[TrainingOrganizationOther]		as 'cnslor_training_org_other',
		dbo.map_hud_9902_training_sponsor(c.[TrainingSponsor]) as 'cnslor_training_sponsor',
		c.[TrainingSponsorOther]			as 'cnslor_training_sponsor_other'
FROM	housing_counselor_training_courses t
inner join housing_training_courses c on t.TrainingCourse = c.oID
GO
