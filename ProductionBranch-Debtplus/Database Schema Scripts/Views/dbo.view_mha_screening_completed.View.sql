USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_mha_screening_completed]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  View [dbo].[view_mha_screening_completed]    Script Date: 01/26/2011 14:13:45 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
CREATE VIEW [dbo].[view_mha_screening_completed] as

select	distinct c.client--, cn.client_note, cn.subject
from clients c left outer join client_notes cn
	on c.client = cn.client
where cn.client_note > 36319492 and
cn.subject like 'MHA Screening Completed%'
GO
