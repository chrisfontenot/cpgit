USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_addresses]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_creditor_addresses] as
-- ==========================================================================================
-- ==      Formatted creditor addresses spelled out as a line item                         ==
-- ==========================================================================================
select	ca.creditor,
		ca.type,
		convert(varchar(60), case when ltrim(rtrim(isnull(ca.attn,''))) = '' then null else 'ATTN: ' + ca.attn end) as attn,
		a.creditor_prefix_1 as addr1,
		a.creditor_prefix_2 as addr2,
		dbo.format_address_line_1 (a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as addr3,
		a.address_line_2 as addr4,
		a.address_line_3 as addr5,
		dbo.format_city_state_zip(a.city, a.state, a.postalcode) as addr6,
		a.city,
		st.mailingcode as state,
		a.postalcode
from	creditor_addresses ca
inner join addresses a on ca.addressID = a.address
left outer join states st on a.state = st.state
GO
