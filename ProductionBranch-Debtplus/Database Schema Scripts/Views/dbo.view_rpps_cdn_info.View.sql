USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_rpps_cdn_info]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_rpps_cdn_info] AS

-- ChangeLog
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- In the rare case where you must send an ABA number, see the comments below

		-- Information for the transaction
SELECT	t.rpps_transaction				as rpps_transaction,
		t.rpps_file						as rpps_file,
		t.trace_number_first			as trace_number_first,
		t.trace_number_last				as trace_number_last,
		t.rpps_batch					as rpps_batch,
		t.death_date					as death_date,
		t.service_class_or_purpose		as service_class_or_purpose,

		case
			when isnull(t.client,0) <> 0 then v.proposal_client_name
			when ltrim(rtrim(isnull(cnf.name,''))) = '' then 'AGENCY NAME'
			else ltrim(rtrim(isnull(cnf.name,'')))
		end								as account_name,

		-- Choose one of the following
		r.rpps_biller_id										as 'biller_id',	-- for the proper biller ID
		-- r.biller_aba										as 'biller_id',		-- for the ABA number
	
		UPPER(LEFT(ISNULL(RTRIM(LTRIM(r.Biller_Name)), ''), 22)) AS biller_name,

		-- Information for the outbound messages
		case
			when isnull(t.client,0) = 0 then 'MESSAGE'
			else isnull(v.account_number,'UNSPECIFIED')
		end								as account_number,

		isnull(m.note_text,'')			as creditor_message,
		isnull(t.client,0)				as client,
		isnull(m.note_amount,0)			as amount

FROM		rpps_transactions t
INNER JOIN	rpps_biller_ids r WITH (NOLOCK)	ON t.biller_id = r.rpps_biller_id AND r.biller_type in (4, 14)
LEFT OUTER JOIN	view_last_payment v WITH (NOLOCK) on t.client_creditor = v.client_creditor
left outer join debt_notes m on t.debt_note = m.debt_note
full outer join config cnf on cnf.company_id = 1
WHERE		t.service_class_or_purpose	= 'CDN'
AND			t.transaction_code = 23
GO
