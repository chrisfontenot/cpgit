USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_most_recent_budget]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_most_recent_budget] AS
SELECT TOP 100 percent	client, dbo.map_client_to_budget(client) AS max_budget
FROM	clients WITH (NOLOCK)
WHERE	client in (
			select client from budgets
)
order by 1
GO
