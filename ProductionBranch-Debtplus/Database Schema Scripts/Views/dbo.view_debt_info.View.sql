USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_debt_info]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[view_debt_info] as
-- =======================================================================================
-- ==          Return information about the debts for .NET                              ==
-- =======================================================================================
select

	-- Information from the client_creditor record
	cc.account_number					as 'account_number',
	cc.balance_verify_by				as 'balance_verify_by',
	cc.balance_verify_date				as 'balance_verify_date',
	cc.check_payments					as 'check_payments',
	cc.client							as 'client',
	cc.client_creditor					as 'client_creditor',
	cc.client_name						as 'client_name',
	cc.creditor_name					as 'creditor_name',
	cc.contact_name						as 'contact_name',
	cc.created_by						as 'created_by',
	cc.date_created						as 'date_created',
	cc.date_disp_changed				as 'date_disp_changed',
	cc.disbursement_factor				as 'disbursement_factor',
	cc.dmp_interest						as 'dmp_interest',
	cc.dmp_payout_interest				as 'dmp_payout_interest',
	cc.drop_date						as 'drop_date',
	cc.drop_reason						as 'drop_reason',
	cc.drop_reason_sent					as 'drop_reason_sent',
	cc.expected_payout_date				as 'expected_payout_date',
	cc.fairshare_pct_check				as 'fairshare_pct_check',
	cc.fairshare_pct_eft				as 'fairshare_pct_eft',
	cc.hold_disbursements				as 'hold_disbursements',
	cc.interest_this_creditor			as 'interest_this_creditor',
	cc.irs_form_on_file					as 'irs_form_on_file',
	cc.last_communication				as 'last_communication',
	cc.last_payment_date_b4_dmp			as 'last_payment_date_b4_dmp',
	cc.last_stmt_date					as 'last_stmt_date',
	cc.line_number						as 'line_number',
	cc.[message]						as 'message',
	cc.months_delinquent				as 'months_delinquent',
	cc.non_dmp_interest					as 'non_dmp_interest',
	cc.non_dmp_payment					as 'non_dmp_payment',
	cc.orig_dmp_payment					as 'orig_dmp_payment',
	cc.payments_this_creditor			as 'payments_this_creditor',
	cc.percent_balance					as 'percent_balance',
	cc.person							as 'person',
	cc.prenote_date						as 'prenote_date',
	cc.priority							as 'priority',
	
	case
		when cc.reassigned_debt <> 0 then 0
		when cc.client_creditor_balance IS null then 0
		when cc.client_creditor = ccb.client_creditor then 1
		else 0
	end									as 'IsActive',
	
	cc.returns_this_creditor			as 'returns_this_creditor',
	cc.sched_payment					as 'sched_payment',
	cc.send_bal_verify					as 'send_bal_verify',
	cc.send_drop_notice					as 'send_drop_notice',
	cc.[start_date]						as 'start_date',
	cc.student_loan_release				as 'student_loan_release',
	cc.rpps_client_type_indicator		as 'rpps_client_type_indicator',
	cc.terms							as 'terms',
	cc.verify_request_date				as 'verify_request_date',
	cc.balance_verification_release		as 'balance_verification_release',

	-- Information from the client_creditor_proposal record
	ccp.client_creditor_proposal		as 'client_creditor_proposal',
	ccp.proposed_balance				as 'proposal_balance',
	ccp.proposal_status					as 'proposal_status',
	
	-- Information from the client_creditor_balance record
	ccb.client_creditor_balance			as 'client_creditor_balance',		
	ccb.client_creditor					as 'balance_client_creditor',
	ccb.current_sched_payment			as 'current_sched_payment',
	ccb.orig_balance					as 'orig_balance',
	ccb.orig_balance_adjustment			as 'orig_balance_adjustment',
	ccb.payments_month_0				as 'payments_month_0',
	ccb.payments_month_1				as 'payments_month_1',
	ccb.total_interest					as 'total_interest',
	ccb.total_payments					as 'total_payments',	
	ccb.total_sched_payment				as 'total_sched_payment',
	
	-- Information from the creditor record
	cr.lowest_apr_pct					as 'creditor_interest',
	cr.creditor							as 'creditor',
	cr.creditor_name					as 'cr_creditor_name',
	cr.division							as 'cr_division_name',
	cr.payment_balance					as 'cr_payment_balance',
	cr.min_accept_amt					as 'cr_min_accept_amt',
	cr.min_accept_pct					as 'cr_min_accept_pct',
	
	-- Information from the creditor class record
	ccl.zero_balance					as 'ccl_zero_balance',
	ccl.prorate							as 'ccl_prorate',
	ccl.always_disburse					as 'ccl_always_disburse',
	ccl.agency_account					as 'ccl_agency_account',

	-- Information about the first payment
	rccf.client_creditor_register		as 'first_payment',
	rccf.date_created					as 'first_payment_date',
	rccf.debit_amt						as 'first_payment_amt',
	rccf.tran_type						as 'first_payment_type',

	-- Information about the last payment
	rccl.client_creditor_register		as 'last_payment',
	rccl.date_created					as 'last_payment_date',
	rccl.debit_amt						as 'last_payment_amt',
	rccl.tran_type						as 'last_payment_type',
	
	-- Indicators that the account number and payment information are valid.
	cc.payment_rpps_mask				as 'payment_rpps_mask',
	cc.rpps_mask						as 'rpps_mask'
	
from			client_creditor cc
left outer join	client_creditor_proposals ccp on cc.client_creditor_proposal = ccp.client_creditor_proposal
left outer join	client_creditor_balances ccb on cc.client_creditor_balance = ccb.client_creditor_balance
left outer join	creditors cr on cc.creditor = cr.creditor
left outer join	creditor_classes ccl on cr.creditor_class = ccl.creditor_class
left outer join registers_client_creditor rccf on cc.first_payment = rccf.client_creditor_register
left outer join registers_client_creditor rccl on cc.last_payment = rccl.client_creditor_register
GO
