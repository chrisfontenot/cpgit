USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_statement_clients_extract]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_statement_clients_extract] as

-- Information used for the extract and printing routines. This is only a slice
-- of the data that is available and other views will extract more for various
-- other things. However, this inforamtion is what is needed to print the client
-- statements from crystal reports and do the extract for the statement out-sourcing companies.

select	cs.client_statement_batch,
	cs.client,
	cs.deposit_amt,
	cs.refund_amt,
	cs.disbursement_amt,
	cs.held_in_trust,
	cs.reserved_in_trust,
	cs.starting_trust_balance,
	cs.expected_deposit_date,
	cs.expected_deposit_amt,
	dbo.format_counselor_name ( co.person ) as counselor_name,

	-- Used for OCR encoding on the statement
	convert(int,0) as company_id,

	-- Needed for selection of outsourced clients
	c.mail_error_date

from	client_statement_clients cs	with (nolock)
left outer join counselors co		with (nolock) on cs.counselor = co.counselor
inner join clients c			with (nolock) on cs.client    = c.client
where	active_debts	= 1
--and	delivery_method <> 'E'
--and	active_status in ('A','AR')
GO
