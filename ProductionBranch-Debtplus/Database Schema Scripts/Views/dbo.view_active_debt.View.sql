USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_active_debt]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_active_debt]
AS

-- ChangeLog
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

SELECT		c.client,
		isnull(dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix), '') AS name, 
		SUM(ISNULL(bal.orig_balance, 0) + ISNULL(bal.orig_balance_adjustment, 0) + ISNULL(bal.total_interest, 0) - ISNULL(bal.total_payments, 0)) AS current_debt
FROM		clients c		WITH (NOLOCK)
LEFT OUTER JOIN	people p		WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
LEFT OUTER JOIN names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	client_creditor cc	WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class

WHERE	c.active_status IN ('A', 'AR')
AND		cc.reassigned_debt = 0
AND		isnull(ccl.zero_balance,0) = 0
GROUP BY	c.client, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix
GO
