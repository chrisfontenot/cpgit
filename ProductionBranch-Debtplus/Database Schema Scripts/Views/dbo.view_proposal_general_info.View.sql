USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_proposal_general_info]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_proposal_general_info] AS

-- ChangeLog
--   11/12/2002
--      Add "created_by" to the list for the creator of the proposal.
--   6/24/2008
--      Added proposal record fields for .NET interface

SELECT pr.client_creditor_proposal,
	pr.client_creditor,
	pr.proposal_status,
	pr.proposal_print_date		as record_print_date,
	pr.proposal_accepted_by,
	pr.proposal_status_date		as record_status_date,
	pr.proposal_reject_reason,
	pr.proposal_reject_disp,
	pr.counter_amount,
	pr.missing_item,
	pr.percent_balance			as record_percent_balance,
	pr.terms					as record_terms,
	pr.proposed_amount,
	pr.proposed_start_date,
	pr.proposal_batch_id,
	pr.proposed_balance			as record_proposed_balance,
	pr.proposed_date,
	pr.full_disclosure,
	pr.bank,
	pr.rpps_biller_id,
	pr.epay_biller_id,
	pr.date_created,
	pr.created_by,

	case
 	  when pr.proposal_batch_id = 0 then isnull(pr.proposal_print_date, pr.date_created)
	  else b.date_closed
 	end					as 'proposal_print_date',
	dn.note_text				as 'proposal_message',

	case
	   when pr.proposal_status in (0, 1) then null
	   when pr.proposal_status_date is null then pr.date_created
	   else pr.proposal_status_date
	end					as 'proposal_status_date',

	isnull(pr.percent_balance,100)		as 'percent_balance',
	coalesce(pr.terms,0)			as 'terms'

FROM	client_creditor_proposals pr	with (nolock)
left outer join proposal_batch_ids b	with (nolock) on pr.proposal_batch_id = b.proposal_batch_id
left outer join debt_notes dn		with (nolock) on pr.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type
GO
