USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_OCS_Sort]    Script Date: 06/18/2015 12:38:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER view [dbo].[view_OCS_Sort] as

select
      ocs.ClientId [Id],
      ocs.Id [OCSClientId],
      ocs.ClaimedDate [ClaimedDate],
      upload.UploadDate [UploadDate],
    case when contact.[StatusCode] is null then 0 else contact.[StatusCode] end as [StatusCode],
    case when contact.[ContactAttempts] is null then 0 else contact.[ContactAttempts] end as [AttemptCount],
      client.language [PreferredLanguage],
      ocs.Archive [Archive],
      ocs.ActiveFlag [ActiveFlag],
      ocs.QueueCode [QueueCode],
      upload.ClientState [StateAbbreviation],
      ocs.SearchTimezone [TzDescriptor],
      ocs.Program [Program]
from OCS_Client [ocs]
inner join clients [client] on client.client = ocs.ClientId
left join view_OCS_Client [viewO] on viewO.ClientId = ocs.ClientId
left join OCS_UploadRecord [upload] on upload.Id = ocs.UploadRecord

LEFT JOIN OCS_ContactAttempt [contact] on 
      ocs.ClientId = contact.ClientId AND 
      contact.Id = (select top 1 Id from OCS_ContactAttempt where ClientId = ocs.ClientId order by [End] desc)


where ocs.Archive = 0




