USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_rpps_cdp_info]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_rpps_cdp_info] AS

-- ChangeLog
--   07/22/2008
--      Used format_normal_name to exclude prefix and suffix components of the name. The rpps EDI raw file only displays 14 characters.
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- In the rare case where you must send an ABA number, see the comments below

SELECT	t.rpps_transaction			as rpps_transaction, 
		t.rpps_file				as rpps_file, 
		t.trace_number_first			as trace_number_first, 
		t.trace_number_last			as trace_number_last, 
		t.rpps_batch				as rpps_batch, 
		t.service_class_or_purpose		as service_class_or_purpose,

		t.death_date				as death_date, 
		ISNULL(v.account_number, 'MISSING')	AS account_number, 
		v.proposal_client_name		as account_name,
	
		-- Choose one of the following
		r.rpps_biller_id										as 'biller_id',	-- for the proper biller ID
		-- r.biller_aba										as 'biller_id',		-- for the ABA number
	
		UPPER(LEFT(ISNULL(RTRIM(LTRIM(r.Biller_Name)), ''), 22)) AS biller_name 

FROM	rpps_transactions t 
INNER JOIN rpps_biller_ids r WITH (NOLOCK) ON t.biller_id = r.rpps_biller_id AND r.biller_type IN (4, 10, 11, 12, 13, 14) 
LEFT OUTER JOIN view_last_payment v WITH (NOLOCK) on t.client_creditor = v.client_creditor 
WHERE	t.service_class_or_purpose IN ('CDP', 'CDD', 'CDV', 'FBC', 'FBD', 'CDF') 
AND		t.transaction_code = 23
GO
