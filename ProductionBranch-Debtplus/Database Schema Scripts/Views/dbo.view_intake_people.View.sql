USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_intake_people]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_intake_people] AS

-- ============================================================================================
-- ==       This is an "expanded" view (with the code values changed to text strings)        ==
-- ==       of the intake_people table. It is used for the intake reports.                   ==
-- ============================================================================================

select
	p.intake_client		as 'intake_client',
	p.person			as 'person',

	case p.person
		when 1	then 'Applicant'
		when 2	then 'Co-Applicant'
		else 'Applicant # ' + convert(varchar, p.person)
	end			as 'person_description',

	dbo.format_normal_name (p.prefix, p.first, p.middle, p.last, p.suffix) as 'name',
	p.former				as 'former',
	p.email					as 'email',
	dbo.format_ssn (p.ssn)	as 'ssn',

	p.gross_income			as 'gross_income',
	p.net_income			as 'net_income',

	isnull(j.description,p.other_job)	as 'job',
			
	p.birthdate				as 'birthdate',
	race.description		as 'race',
	education.description	as 'education',
	p.employer				as 'employer',
	isnull(e.name,p.employer_name)	as 'employer_name',

	case
	   when e.employer is not null then dbo.format_address_line_1(ea.house, ea.direction, ea.street, ea.suffix, ea.modifier, ea.modifier_value)
	   else p.employer_address1
	end						as 'employer_address1',

	case
	   when e.employer is not null then ea.address_line_2
	   else p.employer_address2
	end						as 'employer_address2',

	case
	   when e.employer is not null then dbo.format_city_state_zip ( ea.city, ea.state, ea.postalcode )
	   else dbo.format_city_state_zip ( p.employer_city, p.employer_state, p.employer_postalcode )
	end						as 'employer_address3',

	dbo.format_phone_number ( p.work_ph ) as work_ph,
	dbo.format_phone_number ( p.work_fax ) as work_fax,
	p.date_created			as 'date_created',
	p.created_by			as 'created_by'

from	intake_people p with (nolock)
left outer join job_descriptions j with (nolock)	on p.job = j.job_description
left outer join GenderTypes gender with (nolock)		on p.gender = gender.oID
left outer join RaceTypes race with (nolock)		on p.race   = race.oID
left outer join EducationTypes education with (nolock) on p.education = education.oID
left outer join employers e with (nolock)		on p.employer = e.employer
left outer join addresses ea with (nolock) on e.addressid = ea.address
GO
