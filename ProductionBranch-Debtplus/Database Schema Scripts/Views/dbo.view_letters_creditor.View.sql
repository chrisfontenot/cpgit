USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_letters_creditor]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[view_letters_creditor] as
	SELECT	letter_code			as 'letter_code',
		letter_type			as 'letter_type',
		convert(varchar(50),null)	as 'label',
		description			as 'description',
		menu_name			as 'menu_name',
		left_margin			as 'left_margin',
		right_margin			as 'right_margin',
		top_margin			as 'top_margin',
		bottom_margin			as 'bottom_margin',
		language			as 'language',

		convert(bit,case
			when queue_name is null then 0
			else 1
		end)				as 'queued',

		[default]			as 'default',
		filename			as 'filename'
	FROM	letter_types with (nolock)
	WHERE	letter_group	= 'CR'
	AND	ISNULL(language,1)=1
	AND	ISNULL(region,1) IN (0,1)
	AND	menu_name is not null
GO
