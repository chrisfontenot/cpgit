USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_financial_problems]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_financial_problems] as
select	client, convert(varchar(50),isnull(fin.description,'Not Specified'))		as 'financial_problem'
from	clients c with (nolock)
left outer join financial_problems fin with (nolock) on c.cause_fin_problem1 = fin.financial_problem
GO
