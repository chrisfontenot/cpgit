USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_disbursement_clients]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_disbursement_clients] as

-- ===================================================================================
-- ==       Retrieve the disbursement selection criteria                            ==
-- ===================================================================================

select	d.disbursement_register			as 'disbursement_register',
	d.client_register			as 'client_register',
	d.client				as 'client',

	case
		when d.reserved_in_trust > d.held_in_trust then 0
		else d.held_in_trust - d.reserved_in_trust
	end					as 'held_in_trust',

	d.disbursement_factor			as 'disbursement_factor',
	d.note_count				as 'note_count',
	isnull(rc.debit_amt,0)			as 'disbursed',
	rc.created_by				as 'disbursed_by',
	rc.date_created				as 'date_disbursed',
	upper(left(isnull(pn.last,'Z'),1))	as 'last_name',
	isnull(c.counselor,0)			as 'counselor',
	isnull(c.office,0)			as 'office'

from		disbursement_clients d	with (nolock)
inner join	clients c		with (nolock) on d.client = c.client
left outer join people p		with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join	registers_client rc	with (nolock) on d.client_register = rc.client_register
GO
