USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_www_creditor_client_list]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_www_creditor_client_list] as

select	c.client,
	cc.creditor,
	cc.client_creditor,
	dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default) as 'client_name',
	cc.account_number as 'account_number',
	convert(varchar(10), coalesce(cc.start_date, c.start_date, cc.date_created, c.date_created), 1) as 'start_date',
	isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) as 'current_balance',
	isnull(cc.disbursement_factor,0) as 'disbursement_factor',
	case c.active_status when 'A' then 'Y' when 'AR' then 'Y' when 'EX' then 'Y' else 'N' end as 'active_status'

from	clients c with (nolock)
left outer join client_creditor cc with (nolock) on c.client = cc.client
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.name

where	c.active_status in ('A', 'AR', 'PND', 'PRO', 'RDY', 'EX')
and	isnull(cc.reassigned_debt,0) = 0
and	cc.drop_date is null
and	cc.creditor is not null
GO
