USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_counselor_office]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_client_counselor_office] as
-- ====================================================================================================
-- ==            Information, primarily for Richmond, to use on proposals for counselor/office       ==
-- ====================================================================================================

-- ChangeLog
--   6/30/2005
--      Added support for counselor telephone, extension, and email address.

select	client							as 'client',
		c.counselor						as 'counselor',
		c.office						as 'office',
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor_name',
		o.name							as 'office_name',
		dbo.format_address_line_1 (a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as 'office_addr1',
		a.address_line_2						as 'office_addr2',
		dbo.format_city_state_zip ( a.city, a.state, a.postalcode)	as 'office_addr3',
		a.postalcode						as 'office_zipcode',
		dbo.format_TelephoneNumber ( co.telephoneID )           as 'counselor_phone',
		dbo.format_TelephoneNumber ( co.TelephoneID )           as 'counselor_phone_ext',
		dbo.format_TelephoneNumber ( o.FAXID )			as 'counselor_fax',
		t.ext                                                   as 'counselor_extension',
		emx.address						as 'counselor_email'

from	clients c with (nolock)
left outer join counselors co with (nolock)	on c.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name
left outer join TelephoneNumbers t with (nolock) on co.TelephoneID = t.TelephoneNumber
left outer join EmailAddresses emx with (nolock) on co.EmailID = emx.email
left outer join offices o with (nolock)		on isnull(c.office,co.office) = o.office
left outer join addresses a with (nolock) on o.AddressID = a.address
GO
