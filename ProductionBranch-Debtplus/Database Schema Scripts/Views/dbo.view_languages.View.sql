USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_languages]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_languages] as
-- ===========================================================================
-- ==   Return the list of possible spoken languages for the agency         ==
-- ===========================================================================

select	oID				as 'oID',
		Attribute		as 'description',
		[default]		as 'default',
		[ActiveFlag]	as 'ActiveFlag',
		[LanguageID]	as 'LanguageID',
		[HUDLanguage]	as 'HUDLanguage'
from	AttributeTypes
where	[Grouping]		= 'LANGUAGE'
GO
