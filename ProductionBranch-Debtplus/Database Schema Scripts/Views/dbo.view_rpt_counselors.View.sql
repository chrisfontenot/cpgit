USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_rpt_counselors]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[view_rpt_counselors] as

-- ====================================================================================
-- ==       Information for the list of counselors report (utilities menu)           ==
-- ====================================================================================

select co.counselor,
	dbo.format_counselor_name ( co.person ) as person,
	dbo.format_normal_name(default,cox.first,default,cox.last,default) as name,
	
	convert (varchar(10), case when exists (
		select	ca.oID
		from	counselor_attributes ca
		inner join attributeTypes t on ca.attribute = t.oID
		where	ca.Counselor = co.counselor
		and		t.Grouping = 'ROLE'
		and		t.Attribute = 'EDUCATOR'
	) then 'Y' else 'N' end) as is_education,

	convert (varchar(10), case when exists (
		select	ca.oID
		from	counselor_attributes ca
		inner join attributeTypes t on ca.attribute = t.oID
		where	ca.Counselor = co.counselor
		and		t.Grouping = 'ROLE'
		and		t.Attribute = 'COUNSELOR'
	) then 'Y' else 'N' end) as is_counselor,

	convert (varchar(10), case when exists (
		select	ca.oID
		from	counselor_attributes ca
		inner join attributeTypes t on ca.attribute = t.oID
		where	ca.Counselor = co.counselor
		and		t.Grouping = 'ROLE'
		and		t.Attribute = 'CSR'
	) then 'Y' else 'N' end) as is_csr,
	
	convert (varchar(10), case when exists (
		select	ca.oID
		from	counselor_attributes ca
		inner join attributeTypes t on ca.attribute = t.oID
		where	ca.Counselor = co.counselor
		and		t.Grouping is null
		and		t.Attribute = 'HOUSING'
	) then 'Y' else 'N' end) as counsel_housing,
	
	convert (varchar(10), case co.[default] when 0 then 'N' else 'Y' end) as [default],
	coalesce (o.name, convert(varchar(10), co.office), '')  as office,
	co.menu_level,
	convert(varchar(20), dbo.Format_TelephoneNumber(co.TelephoneID)) as telephone,
	eox.address as email

from counselors co with (nolock)
left outer join offices o with (nolock) on co.office = o.office
left outer join names cox with (nolock) on co.NameID = cox.name
left outer join EmailAddresses eox with (nolock) on co.EmailID = eox.email

WHERE	co.ActiveFlag = 1
GO
