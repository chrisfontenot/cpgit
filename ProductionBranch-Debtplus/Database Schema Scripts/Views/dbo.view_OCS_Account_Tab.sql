USE [DebtPlus]
GO

/****** Object:  View [dbo].[view_OCS_Account_Tab]    Script Date: 1/6/2016 11:22:24 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER view [dbo].[view_OCS_Account_Tab] as

WITH UploadInfo AS (SELECT DISTINCT AttemptId, BatchName
			FROM dbo.OCS_UploadReport report with (nolock)
			JOIN dbo.OCS_Client OC with (nolock) ON oc.UploadAttempt = report.AttemptId),
	ClientInfo AS (SELECT OC.ID, OC.UploadRecord, OC.ClientId, OC.StatusCode, OC.ContactAttempts, OC.ActiveFlag, OC.Archive, OC.UploadAttempt, 
						OC.InvestorNumber, OC.InvestorDueDate, OC.InvestorLastChanceList, OC.InvestorActualSaleDate, OC.Program, OC.ClaimedDate, 
						OC.ClaimedBy, OC.SearchTimezone, OC.SearchServicerId, OC.ArchiveAttempt, OC.QueueCode, OC.PostModLastChanceFlag, 
						OC.IsDuplicate, OC.PostModLastChanceDate, OC.LoanNumberPlaceHolder, OC.TrialModification, OC.closedate, C.cause_fin_problem1,
            OC.FirstCounselDate, OC.SecondCounselDate
			FROM OCS_Client OC with (nolock)
			JOIN Clients C with (nolock) ON C.Client = OC.ClientID)
      --where OC.ID = (select max(oc2.id) from ocs_client oc2 where oc2.clientid=oc.clientid group by clientid))
SELECT client.ClientId
	,client.Id AS OcsId
	,client.InvestorNumber AS InvestorNo
	,lender.ServicerName AS Servicer
	,COALESCE(lender.AcctNum, client.loannumberplaceholder, '') AS [ServicerLoanNum]
	,lender.ServicerID [ServicerID]
	,property.FcSaleDT AS ForeclosureDate
	,client.InvestorDueDate AS PastDueDate
	,CASE WHEN client.InvestorDueDate IS NULL THEN 0 ELSE DATEDIFF(DAY, client.InvestorDueDate, GETDATE()) END AS DaysPastDue
	,CASE WHEN client.InvestorDueDate IS NULL THEN 0 ELSE DATEDIFF(MONTH, client.InvestorDueDate, GETDATE()) END AS MonthsPastDue
	,client.cause_fin_problem1 AS DefaultReason
	,CASE WHEN ISNULL(record.Description,'') <> '' THEN record.Description ELSE CASE WHEN client.trialmodification IS NOT NULL THEN tmt.description ELSE '' END END [TrialMod]
	,record.TrialMod_Type AS TrialMod_Type
	,record.TrialPeriodStartDate AS LoanModDate
	,property.HPF_foreclosureCaseID AS FCID
	,record.Last_Payment_Applied_Date AS LoanPmtApplied
	,record.Next_Payment_Due_Date AS NextPmtDue
	,client.InvestorActualSaleDate AS ActualSaleDate
	,record.UploadDate AS DateFileReceived
	,CONVERT(DATETIME, contact.[End], 121) AS DateFileModified
	,CONVERT(DATETIME, client.closedate, 121) AS DateClosed
	,client.IsDuplicate AS DuplicateFile
	,record.Referral_Date AS Referral_Date
    ,'' [SponsorCampaign]
	,record.CaseType AS AgencyCase
	,report.BatchName
	,RPC.[Begin] AS FirstRPContactDate
  ,CONVERT(DATETIME, client.FirstCounselDate, 121) AS FirstCounselDate
	,CONVERT(DATETIME, client.SecondCounselDate, 121) AS SecondCounselDate
  ,CONVERT(DATETIME, client.PostModLastChanceDate, 121) AS LastChanceDate
  ,client.PostModLastChanceFlag AS LastChanceFlag
FROM ClientInfo client 
LEFT JOIN dbo.Housing_Properties AS property ON property.HousingID = client.ClientId
LEFT JOIN dbo.Housing_loans AS loan ON loan.PropertyID = property.oID AND loan.Loan1st2nd <> 2
LEFT JOIN dbo.Housing_lenders AS lender ON lender.oID = loan.CurrentLenderID
LEFT JOIN UploadInfo Report ON client.UploadAttempt = report.AttemptId
LEFT JOIN dbo.OCS_UploadRecord AS record ON client.UploadRecord = record.Id
LEFT JOIN dbo.OCS_ContactAttempt AS contact ON client.ClientId = contact.ClientId AND contact.Id = (
		SELECT TOP (1) Id
		FROM dbo.OCS_ContactAttempt
		WHERE (ClientId = client.ClientId)
		ORDER BY [End] DESC
		)
LEFT JOIN dbo.view_client_first_RPC_EI AS rpc ON client.ClientId = RPC.ClientId
LEFT JOIN TrialModificationTypes tmt ON tmt.oID = client.trialmodification

GO