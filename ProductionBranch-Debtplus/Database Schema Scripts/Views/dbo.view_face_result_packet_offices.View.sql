USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_face_result_packet_offices]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_face_result_packet_offices] as
-- ====================================================================================
-- ==       Office address dataset in the face-result packet                         ==
-- ====================================================================================

select top 100 percent ca.client, ca.client_appointment, ca.start_time, ca.status, ca.office, o.name, dbo.address_block_5 ( dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value), a.address_line_2, default, default, dbo.format_city_state_zip(a.city, a.state, a.postalcode)) as address
from client_appointments ca with (nolock)
inner join offices o with (nolock) on ca.office = o.office
inner join addresses a with (nolock) on o.addressid = a.address
where ca.office is not null
and   ca.workshop is null
order by 1, 3 desc
GO
