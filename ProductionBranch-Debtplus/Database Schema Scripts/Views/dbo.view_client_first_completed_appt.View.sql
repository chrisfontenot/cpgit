ALTER VIEW [dbo].[view_client_first_completed_appt] AS
SELECT ca.client_appointment, ca.client, ca.appt_time, ca.counselor, ca.start_time, ca.end_time, ca.office, ca.workshop, ca.workshop_people, ca.appt_type, ca.status, ca.result, ca.previous_appointment, ca.referred_to, ca.referred_by, ca.bankruptcy_class, ca.priority, ca.housing, ca.post_purchase, ca.credit, ca.callback_ph, ca.HousingFeeAmount, ca.confirmation_status, ca.date_confirmed, ca.date_updated, ca.date_created, ca.created_by, min.client_appointment AS Expr1, min.client AS Expr2
FROM dbo.client_appointments AS ca WITH (nolock)
INNER JOIN (SELECT MIN(client_appointment) AS client_appointment, client
            FROM   dbo.client_appointments
            WHERE  (status IN ('k', 'w')) AND (result <> 'is')
            GROUP BY client) AS min ON min.client_appointment = ca.client_appointment
GO
