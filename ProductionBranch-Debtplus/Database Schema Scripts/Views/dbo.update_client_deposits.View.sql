USE [DebtPlus]
GO
/****** Object:  View [dbo].[update_client_deposits]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[update_client_deposits] AS

-- ==========================================================================================
-- ==       This view is to the client_deposits table. It is provided for having           ==
-- ==       a hook to table changes for system note purposes.                              ==
-- ==========================================================================================

SELECT	*
FROM	client_deposits
GO
