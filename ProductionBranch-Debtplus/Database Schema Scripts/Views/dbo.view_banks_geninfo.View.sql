SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER VIEW [view_banks_geninfo]
AS
SELECT	bank,
	description,
	type,
	ContactNameID,
	bank_name,
	BankAddressID,
	aba,
	account_number,
	immediate_origin,
	immediate_origin_name,
	ach_priority,
	ach_company_id,
	ach_origin_dfi,
	ach_enable_offset,
	immediate_destination,
	immediate_destination_name,
	checknum,
	batch_number,
	transaction_number,
	prefix_line,
	suffix_line,
	[default],
	[ActiveFlag],
	convert(int,isnull(max_clients_per_check,0)) as max_clients_per_check,
	convert(money,isnull(max_amt_per_check,0)) as max_amt_per_check,
	created_by,
	date_created
FROM	dbo.banks with (nolock)
GO
