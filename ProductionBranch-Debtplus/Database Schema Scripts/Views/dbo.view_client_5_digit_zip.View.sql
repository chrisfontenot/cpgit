USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_5_digit_zip]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_5_digit_zip] AS

-- ========================================================================================================================
-- ==           Return the information about the client database                                                         ==
-- ========================================================================================================================

SELECT		c.client										as 'client',
		left(a.postalcode,5)										as 'zipcode'

FROM		clients c	WITH (NOLOCK)
left outer join addresses a with (nolock) on c.addressid = a.address
GO
