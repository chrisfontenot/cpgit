USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_retention_client_actions]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_retention_client_actions] AS

-- ============================================================================================
-- ==           Client retention actions                                                     ==
-- ============================================================================================

SELECT
		-- Client retention action
		ca.client_retention_action		as 'client_retention_action',
		ca.client_retention_event		as 'client_retention_event',
		ca.retention_action			as 'retention_action',
		ca.date_created				as 'date_created',
		ca.created_by				as 'created_by',
		ca.message				as 'retention_message',

		-- Action table
		act.description				as 'retention_description',
		act.letter_code				as 'retention_letter_code',

		-- Letter table
		l.description				as 'letter_description'

FROM		client_retention_actions ca	WITH (NOLOCK)
LEFT OUTER JOIN	retention_actions act		WITH (NOLOCK) ON ca.retention_action = act.retention_action
LEFT OUTER JOIN letter_types l			WITH (NOLOCK) ON act.letter_code = l.letter_code and l.letter_type in (select MIN(letter_type) from letter_types where letter_code is not null group by letter_code)
GO
