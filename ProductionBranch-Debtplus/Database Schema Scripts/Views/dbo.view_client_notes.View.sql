USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_notes]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_notes]
AS

-- ================================================================================================================================
-- ==           Select the client notes to be printed in the list of client notes                                                ==
-- ================================================================================================================================

-- ChangeLog
--    4/8/2002
--       Removed test for ownership being a ground for printing the note.
--   1/28/2004
--       Ordered the result set by date

SELECT top 100 percent		n.client_note as 'note_id',
		n.client,
		n.client_creditor as 'client_creditor',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) AS client_name,
		isnull(cc.creditor, '') AS creditor,
		COALESCE (cr.creditor_name, cc.creditor_name, '') AS creditor_name,
		convert(varchar(64),left(n.subject,64)) as subject,
		CASE n.type	
			WHEN 1 THEN 'Permanent'
			WHEN 2 THEN 'Temporary'
			WHEN 3 THEN 'System'
			WHEN 4 THEN 'Alert'
			WHEN 5 THEN 'Disbursement'
			WHEN 6 THEN 'Research'
		END									as type,
		n.created_by,
		n.date_created,
		CONVERT(int, CASE n.is_text
			WHEN 0 THEN CASE WHEN lower(CONVERT(varchar(7), n.note)) LIKE '{\rtf[0-9]\' THEN 1 ELSE 0 END
			ELSE 0 END)  							AS text_type,
		convert(varchar(8000),n.note) as note, 
		CASE n.dont_print  WHEN 0 THEN '' ELSE 'DON''T PRINT' END		AS dont_print, 
		CASE n.dont_edit   WHEN 0 THEN '' ELSE 'DON''T EDIT' END			AS dont_edit,
		CASE n.dont_delete WHEN 0 THEN '' ELSE 'DON''T DELETE' END		AS dont_delete

FROM		client_notes n
LEFT OUTER JOIN	client_creditor cc ON n.client_creditor = cc.client_creditor
LEFT OUTER JOIN	creditors cr ON cc.creditor = cr.creditor
LEFT OUTER JOIN	people p ON n.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		n.dont_print = 0 OR suser_sname() = 'sa'
order by n.date_created
GO
