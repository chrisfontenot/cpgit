USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_dmp_info]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_dmp_info]
AS
-- ====================================================================================================
-- ==            Information about the DMP status for a client                                       ==
-- ====================================================================================================

SELECT	client				as 'client',
	cause_fin_problem1		as 'cause_fin_problem1',
	cause_fin_problem2		as 'cause_fin_problem2',
	cause_fin_problem3		as 'cause_fin_problem3',
	cause_fin_problem4		as 'cause_fin_problem4',
	start_date			as 'start_date',
	disbursement_date		as 'disbursement_date',
	drop_date			as 'drop_date',
	mail_error_date			as 'mail_error_date',

	isnull(drop_reason_other, d.description) as 'drop_reason_other',

	convert(datetime, case isnull(hold_disbursements,0)
		when 1 then '12/31/2099'
		else null
	end)				as 'hold_date',
	hold_disbursements		as 'hold_disbursements',

	stack_proration			as 'stack_proration',
	mortgage_problems		as 'mortgage_problems',
	active_status			as 'active_status',
	active_status_date		as 'active_status_date',
	client_status			as 'client_status',
	client_status_date		as 'client_status_date',
	program_months			as 'program_months',
	bankruptcy_class		as 'bankruptcy_class'

FROM	clients c WITH (NOLOCK)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
GO
