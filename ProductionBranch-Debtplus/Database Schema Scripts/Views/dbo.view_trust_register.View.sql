ALTER VIEW [dbo].[view_trust_register] AS

-- ==============================================================================================================
-- ==           Return information from the trust register as exportable items                                 ==
-- ==============================================================================================================

-- ChangeLog
--   1/10/2002
--     Corrected for reconcilation routine

SELECT	tr.trust_register					as 'trust_register',
		tr.tran_type						as 'tran_type',
		tr.created_by						as 'created_by',

		convert(datetime, convert(varchar(10), tr.date_created, 101) + ' 00:00:00')						as 'date_created',

		case
			when tr.client IS NOT NULL then '[' + dbo.format_client_id ( tr.client ) + ']' + isnull(' ' + dbo.format_normal_name ( p1n.prefix, p1n.first, p1n.middle, p1n.last, p1n.suffix ),'')
			when tr.creditor IS NOT NULL then '[' + tr.creditor + ']' + isnull(' '+cr.creditor_name,'')
			else tt.description
		end							as 'payee',

		isnull(tr.checknum,'')				as 'checknum',
		isnull(tr.cleared,' ')					as 'cleared',

		convert(datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'reconciled_date',

		case
			when tr.tran_type in ('DP', 'BI', 'RF', 'RR', 'VD') then NULL
			else tr.amount
		end							as 'debit_amt',

		case
			when tr.tran_type in ('DP', 'BI', 'RF', 'RR', 'VD') then tr.amount
			else NULL
		end							as 'credit_amt',

		tr.bank							as 'bank'

FROM		registers_trust tr WITH (NOLOCK)
LEFT OUTER JOIN	creditors cr WITH (NOLOCK) ON tr.creditor = cr.creditor
LEFT OUTER JOIN	people p1 WITH (NOLOCK) ON tr.client = p1.client AND 1 = p1.relation
LEFT OUTER JOIN	Names p1n with (nolock) ON p1.NameID = p1n.Name
LEFT OUTER JOIN tran_types tt WITH (NOLOCK) ON tr.tran_type = tt.tran_type
GO
