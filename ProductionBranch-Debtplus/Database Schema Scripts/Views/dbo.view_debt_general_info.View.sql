USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_debt_general_info]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_debt_general_info] AS
-- ====================================================================================================
-- ==       Display the information associated with the debt                                         ==
-- ====================================================================================================

-- ChangeLog
--   1/19/2001
--   - Added support for new definition of the last_payment_date and last_payment_amount field.
--   9/27/2002
--     Added send_cdd, send_cdv, and send_cdp to indicate how debts are managed with the creditor.
--   10/15/2002
--     Added information about "*_this_creditor"
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

SELECT
	cc.client_creditor				as 'client_creditor',
	cc.client					as 'client',
	cc.creditor					as 'creditor',
	isnull(cc.line_number,0)			as 'line_number',

	-- Use the name from the creditor information if there is a 'coded creditor' or not
	case
		when cc.creditor IS NULL then cc.creditor_name
		else isnull(cr.creditor_name,'') + isnull(' ' + cr.division,'')
	end						as 'creditor_name',

	-- Account number information
	isnull(cc.account_number,'')			as 'account_number',
	right(dbo.statement_account_number(isnull(cc.account_number,'')),4) as 'display_account_number',
	isnull(cc.message,'')				as 'message',
	cc.prenote_date					as 'prenote_date',
	isnull(cc.client_name,'')			as 'client_name',
	isnull(cc.contact_name,'')			as 'contact_name',
	coalesce(cc.priority,cr.usual_priority,9)	as 'priority',

	-- Balance information
	isnull(bal.orig_balance,0)			as 'orig_balance',
	isnull(bal.orig_balance_adjustment,0)		as 'orig_balance_adjustment',
	isnull(bal.total_interest,0)			as 'total_interest',
	isnull(bal.total_payments,0)			as 'total_payments',

	isnull(cc.payments_this_creditor,0)		as 'payments_this_creditor',
	isnull(cc.returns_this_creditor,0)		as 'returns_this_creditor',
	isnull(cc.interest_this_creditor,0)		as 'interest_this_creditor',
	coalesce(cc.percent_balance,cr.percent_balance,100)	as 'percent_balance',

	case
		when cc.reassigned_debt > 0 then 0
		when isnull(ccl.zero_balance,0) = 0 then isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
		else 0
	end						as 'balance',

	-- Disbursement information
	isnull(cc.disbursement_factor,0)		as 'disbursement_factor',
	coalesce(cc.sched_payment,cc.disbursement_factor,0) as 'sched_payment',

	isnull(bal.payments_month_0,0)			as 'denorm_current_month_disbursement',
	isnull(bal.payments_month_1,0)			as 'denorm_last_month_disbursement',
	cc.dmp_interest					as 'dmp_interest',
	cc.dmp_payout_interest				as 'dmp_payout_interest',
        cc.date_disp_changed				as 'date_disb_changed',

	-- Last payment information
	rcc.date_created				as 'last_payment_date',
	isnull(rcc.debit_amt,0)				as 'last_payment_amount',

	-- Balance verification information
	cc.balance_verify_date				as 'balance_verify_date',
	cc.balance_verify_by				as 'balance_verify_by',

	isnull(cc.orig_dmp_payment,0)			as 'orig_dmp_payment',
	isnull(cc.non_dmp_interest,0)			as 'non_dmp_interest',
	isnull(cc.non_dmp_payment,0)			as 'non_dmp_payment',
	isnull(cc.months_delinquent,0)			as 'months_delinquent',
	isnull(cc.hold_disbursements,0)			as 'hold_disbursements',
	isnull(cc.irs_form_on_file,0)			as 'irs_form_on_file',
	isnull(cc.student_loan_release,0)		as 'student_loan_release',
	isnull(cc.reassigned_debt,0)			as 'reassigned_debt',
	isnull(prop.proposal_status,0)			as 'proposal_status',
	coalesce(prop.percent_balance,cc.percent_balance,cr.percent_balance,100)	as 'proposal_percent_balance',
	isnull(cc.terms,0)				as 'terms',
	isnull(cc.rpps_client_type_indicator,' ') as 'rpps_client_type_indicator',

	-- Other dates that should be passed as null
	cc.last_payment_date_b4_dmp			as 'last_payment_date_b4_dmp',
	cc.start_date					as 'start_date',
	cc.last_stmt_date				as 'last_stmt_date',
	cc.expected_payout_date				as 'expected_payout_date',

	-- Creditor information
	1						as 'proposals',
	isnull(cr.min_accept_amt,0)			as 'min_accept_amt',
	isnull(cr.min_accept_pct,0)			as 'min_accept_pct',
	isnull(cr.payment_balance,'B')			as 'payment_balance',

	-- Fairshare for this debt
	coalesce(cc.fairshare_pct_check,pct.fairshare_pct_check,0)	as 'fairshare_pct_check',
	coalesce(cc.fairshare_pct_eft,pct.fairshare_pct_eft,0)		as 'fairshare_pct_eft',
	isnull(pct.creditor_type_eft,'N')		as 'creditor_type_eft',
	isnull(pct.creditor_type_check,'N')		as 'creditor_type_check',

	cr.lowest_apr_pct				as 'lowest_apr_pct',
	cr.medium_apr_pct				as 'medium_apr_pct',
	cr.highest_apr_pct				as 'highest_apr_pct',
	cr.medium_apr_amt				as 'medium_apr_amt',
	cr.highest_apr_amt				as 'highest_apr_amt',
/*
	case
		when bbal.type IN ('R','E','V')	then 1
		else 0
	end						as 'send_cdv',

	case
		when bdr.type IN ('R','E','V')	then 1
		else 0
	end						as 'send_cdd',

	case
		when bprop.type IN ('R','E','V') then 1
		else 0
	end						as 'send_cdp',
*/
	-- Creditor class information
	isnull(ccl.creditor_class,0)			as 'creditor_class',
	isnull(ccl.zero_balance,0)			as 'creditor_class_balance',
	isnull(ccl.prorate,0)				as 'creditor_class_prorate',
	isnull(ccl.always_disburse,0)			as 'creditor_class_disburse'

FROM	client_creditor cc WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN creditors cr WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN registers_client_creditor rcc WITH (NOLOCK) ON cc.last_payment = rcc.client_creditor_register
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
LEFT OUTER JOIN creditor_contribution_pcts pct WITH (NOLOCK) ON cr.creditor_contribution_pct = pct.creditor_contribution_pct
LEFT OUTER JOIN client_creditor_proposals prop WITH (NOLOCK) ON cc.client_creditor_proposal = prop.client_creditor_proposal
/*
LEFT OUTER JOIN creditor_methods mprop WITH (NOLOCK) ON cr.creditor_id = mprop.creditor AND 'EDI' = mprop.type
LEFT OUTER JOIN banks bprop WITH (NOLOCK) ON mprop.bank = bprop.bank
LEFT OUTER JOIN creditor_methods mbal WITH (NOLOCK) ON cr.creditor_id = mbal.creditor AND 'BAL' = mbal.type
LEFT OUTER JOIN banks bbal WITH (NOLOCK) ON mbal.bank = bbal.bank
LEFT OUTER JOIN creditor_methods mdr WITH (NOLOCK) ON cr.creditor_id = mdr.creditor AND 'DROP' = mdr.type
LEFT OUTER JOIN banks bdr WITH (NOLOCK) ON mdr.bank = bdr.bank
*/
GO
