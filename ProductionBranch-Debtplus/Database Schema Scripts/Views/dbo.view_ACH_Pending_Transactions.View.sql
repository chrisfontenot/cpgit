USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_ACH_Pending_Transactions]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_ACH_Pending_Transactions] AS
-- =============================================================================
-- ==        Fetch the list of clients to be generated in the file            ==
-- =============================================================================

-- ChangeLog
--   1/01/2002
--     Added support to return transaction code 22 (credit) if the client is active or not. Client 0 is not always active.
--   6/16/2002
--     Moved the table to the deposit_batch_details table in order to make it common with other deposit operations.

SELECT		t.deposit_batch_detail								as ach_transaction,
		t.deposit_batch_id								as ach_file,
		t.ach_transaction_code								as transaction_code,
		isnull(t.ach_routing_number,'MISSING')						as routing_number,
		isnull(t.ach_account_number,'MISSING')						as account_number,
		case when t.ach_transaction_code in ('23', '33', '28', '38') then 0 else t.amount end	as amount,
		c.client									as client,
		null										as discretionary_data,
		t.reference									as trace_number,
		left(upper(dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default)), 22) as 'client_name'

FROM		deposit_batch_details t WITH (NOLOCK)
INNER JOIN	clients c WITH (NOLOCK) ON t.client=c.client
LEFT OUTER JOIN	people p WITH (NOLOCK) ON c.client = p.client AND 1=p.relation
LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.Name
WHERE		t.reference IS NULL
AND		(t.ach_transaction_code = '22' or c.active_status IN ('A', 'AR'))
AND		t.tran_subtype = 'AC'
GO
