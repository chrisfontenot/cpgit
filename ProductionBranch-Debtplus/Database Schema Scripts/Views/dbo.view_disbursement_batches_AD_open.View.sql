USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_disbursement_batches_AD_open]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_disbursement_batches_AD_open] AS

SELECT
	disbursement_register				as 'disbursement_register',
	disbursement_date				as 'disbursement_date',
	region						as 'region',
	created_by					as 'created_by',
	date_created					as 'date_created',
	note						as 'note'

FROM	registers_disbursement WITH (NOLOCK)

WHERE	date_posted IS NULL
AND	tran_type = 'AD'
GO
