USE [DebtPlus]
GO
if not exists (select * from sysobjects where type = 'V' and name = 'view_client_housing')
begin
	exec ('create view view_client_housing as select 0')
	exec ('deny insert,update,delete on view_client_housing to public')
	exec ('grant select on view_client_housing to public')
	exec ('deny select on view_client_housing to www_role')
end
GO
ALTER VIEW [dbo].[view_client_housing] AS
SELECT TOP (100) PERCENT
				c.client,
				ISNULL(l.Loan1st2nd, 1) AS hud_loan_position,
				c.client AS hud_number,
				ISNULL(hs.description, '') AS housing_status,
                ISNULL(ht.description, 'Unspecified') AS housing_type,
				ISNULL(lt.description, 'Unspecified') AS hud_loan_type,
				COALESCE (svr.description, lnd.ServicerName, '') AS lender_name,
				ISNULL(lnd.AcctNum, '') AS lender_loan_number,
				ISNULL(l.CurrentLoanBalanceAmt, 0) AS amount_owed,
				convert(int, 
							case when ISNULL(l.LoanDelinquencyMonths, 0) < 0
									then 0
									else ISNULL(l.LoanDelinquencyMonths, 0)
							end) AS mos_past_due,
				c.date_created,
				c.created_by,
				p.oID AS propertyid
FROM			dbo.clients AS c WITH (nolock)
LEFT OUTER JOIN	dbo.client_housing AS hi WITH (nolock) ON c.client = hi.client
INNER JOIN		dbo.Housing_properties AS p WITH (nolock) ON c.client = p.HousingID
INNER JOIN		dbo.Housing_loans AS l WITH (nolock) ON p.oID = l.PropertyID
INNER JOIN		dbo.Housing_loan_details AS det ON l.IntakeDetailID = det.oID
INNER JOIN		dbo.Housing_lenders AS lnd ON l.CurrentLenderID = lnd.oID
LEFT OUTER JOIN	dbo.Housing_lender_servicers AS svr ON lnd.ServicerID = svr.oID
LEFT OUTER JOIN	dbo.Housing_LoanTypes AS lt WITH (nolock) ON det.LoanTypeCD = lt.oID
LEFT OUTER JOIN	dbo.Housing_StatusTypes AS hs WITH (nolock) ON hi.housing_status = hs.oID
LEFT OUTER JOIN	dbo.HousingTypes AS ht WITH (nolock) ON p.PropertyType = ht.oID
ORDER BY c.client, hud_loan_position
GO
