USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_people_letter_fields]    Script Date: 09/15/2014 13:13:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_people_letter_fields] AS
-- =====================================================================================================
-- ==         Used in writing letters where the textual descriptions are desired rather than the      ==
-- ==         code values stored in the tables. The letter writer does not know how to map the        ==
-- ==         key fields to a corresponding text description.                                         ==
-- =====================================================================================================
select	p.client, p.person, p.relation,
		DATEDIFF(year, p.birthdate, getdate())	as 'age',
		CONVERT(varchar(50), dbo.format_ethnicity (p.ethnicity )) as 'ethnicity',
		CONVERT(varchar(50), dbo.format_race ( p.race )) as 'race',
		mgender.description						as gender,
		meducation.description					as 'education',
		dbo.format_ssn(p.ssn)					as 'ssn',
		p.fico_score							as 'fico_score',
		p.net_income							as 'net_income',
		p.gross_income							as 'gross_income',
		p.birthdate								as 'birthdate',
		dbo.format_TelephoneNumber(p.WorkTelephoneID) as 'work_phone',
		dbo.format_TelephoneNumber(p.CellTelephoneID) as 'cell_phone',
		
		pn.prefix, pn.first, pn.middle, pn.last, pn.suffix,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'normal_name',
		dbo.format_reverse_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'reverse_name'
		
from	people p WITH (NOLOCK)
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join GenderTypes mgender WITH (NOLOCK) on p.gender = mgender.oID
left outer join EducationTypes meducation with (NOLOCK) ON p.education = meducation.oID
GO
