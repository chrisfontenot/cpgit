USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_custom_foreclosure_packet_1]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_custom_foreclosure_packet_1] as
-- ==================================================================================================
-- ==          Retrieve the information for the Forclosure Alternatives Packet                     ==
-- ==================================================================================================

-- ChangeLog
--    5/31/2011
--       Initial creation

select	v1.client					as client,
		v1.ssn						as ssn_1,
		v2.ssn						as ssn_2,
		v1.normal_name				as name_1,
		v2.normal_name				as name_2,
		h.lender_name				as lender_name,
		h.lender_loan_number		as lender_loan_number,
		h.mos_past_due				as mos_past_due,
		h.amount_owed				as amount_owed,
		h.hud_loan_position			as loan_position
from	view_people_letter_fields v1 with (nolock)
left outer join view_people_letter_fields v2 with (nolock) on v1.client = v2.client and 1 <> v2.relation
left outer join view_client_housing h with (nolock) on v1.client = h.client and 1 = hud_loan_position
where	v1.relation = 1
GO
