USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_client_has_missing_info_or_30_day_indic]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_client_has_missing_info_or_30_day_indic]
AS
SELECT     c.client, ci.client AS indic_client, dbo.indicators.description, ci.date_created, ci.created_by, vu.name
FROM         dbo.clients AS c INNER JOIN
                      dbo.client_indicators AS ci ON ci.client = c.client INNER JOIN
                      dbo.indicators ON ci.indicator = dbo.indicators.indicator INNER JOIN
                      dbo.view_users AS vu ON vu.Person = ci.created_by
WHERE     (dbo.indicators.description = ' Outside of 30-day Rule') OR
                      (dbo.indicators.description = ' Missing Information')
GO
