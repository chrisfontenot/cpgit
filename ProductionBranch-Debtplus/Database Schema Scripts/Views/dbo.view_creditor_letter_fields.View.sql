USE [DebtPlus]
GO
/****** Object:  View [dbo].[view_creditor_letter_fields]    Script Date: 09/15/2014 13:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[view_creditor_letter_fields] as

-- =================================================================================================
-- ==       Additional information for creditor letters as needed                                 ==
-- =================================================================================================
select
	cr.creditor				as creditor,
	dbo.format_TelephoneNumber ( ct.TelephoneID )	as payment_phone
from	creditors cr
full outer join creditor_contact_types ctx on ctx.contact_type = 'P'
left outer join creditor_contacts ct on ctx.creditor_contact_type = ct.creditor_contact_type and cr.creditor = ct.creditor
GO
