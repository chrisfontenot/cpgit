USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[valid_checksum_luhn]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[valid_checksum_luhn] (@account_number as varchar(80)) returns int as
begin
	-- ===================================================================================================
	-- ==         Validate the LUHN checksum on an account number                                       ==
	-- ===================================================================================================

	-- Define the scan function
	declare	@factor		int
	declare	@sum		int
	declare	@column		int

	declare	@digit		int

	select	@column		= len(@account_number),
		@factor		= 1,
		@sum		= 0

	-- Process the digits in the columns
	while @column > 0
	begin
		-- Find the digit value
		select	@digit = charindex(substring(@account_number,@column,1), '0123456789') - 1,
			@factor = 1 - @factor

		-- If the digit is invalid the checksum can not match
		if @digit < 0
			return ( 0 )

		-- If this is the even column, scale by a factor of 2.
		if @factor = 1
		begin
			select	@digit = @digit * 2
			if @digit >= 10
				select	@digit = @digit - 9
		end

		-- Accumulate the checksum value
		select	@sum = @sum + @digit,
			@column = @column - 1

		-- It must be modulo 10
		if @sum >= 10
			select	@sum = @sum - 10
	end

	-- The checksum value is correct if the sum is now zero.
	if @sum = 0
		return ( 1 )

	return ( 0 )
end
GO
