USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_HudTerminationReason]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_HudTerminationReason] ( @TerminationReason as int ) returns varchar(15) as
begin
	declare	@Answer		varchar(15)
	select	@Answer = case @TerminationReason
				when 1 then '503'			-- Not responded
				when 2 then '505'			-- Client can handle
				when 3 then '504'			-- did not follow through
				when 4 then '501'			-- no action required
				when 5 then '502'			-- referred to other agency
				when 6 then '503'			-- client will initiate contact
				when 7 then '502'			-- referred to legal
				when 8 then '505'			-- issued hecm certificate
				else '506'					-- failed to attend counseling appointment
			end;
	return isnull ( @Answer, '506' )	
end
GO
