USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_housing_type]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[format_housing_type] ( @housing_type as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	select  @answer = description
	from	HousingTypes with (nolock)
	where	oID = @housing_type
	
	return @answer
end
GO
