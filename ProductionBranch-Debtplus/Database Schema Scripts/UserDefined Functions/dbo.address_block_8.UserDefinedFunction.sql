USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[address_block_8]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[address_block_8] ( @address1 as varchar(80) = null, @address2 as varchar(80) = null, @address3 as varchar(80) = null, @address4 as varchar(80) = null, @address5 as varchar(80) = null, @address6 as varchar(80) = null, @address7 as varchar(80) = null, @address8 as varchar(80) = null) returns varchar(800) as
begin

	-- Remove leading and trailing spaces. NULLs are still NULLs.
	select  @address1 = ltrim(rtrim(@address1)),
			@address2 = ltrim(rtrim(@address2)),
			@address3 = ltrim(rtrim(@address3)),
			@address4 = ltrim(rtrim(@address4)),
			@address5 = ltrim(rtrim(@address5)),
			@address6 = ltrim(rtrim(@address6)),
			@address7 = ltrim(rtrim(@address7)),
			@address8 = ltrim(rtrim(@address8))

	-- If the item is empty then make it null. We want nulls to skip blank lines.
	if @address1 = '' select @address1 = null
	if @address2 = '' select @address2 = null
	if @address3 = '' select @address3 = null
	if @address4 = '' select @address4 = null
	if @address5 = '' select @address5 = null
	if @address6 = '' select @address6 = null
	if @address7 = '' select @address7 = null
	if @address8 = '' select @address8 = null

	-- Merge the lines together into the single text string. Each line is terminated by a CR/LF sequence.
	declare	@answer		varchar(800)
	select	@answer		= isnull(char(13) + char(10) + @address1,'') +
						  isnull(char(13) + char(10) + @address2,'') +
						  isnull(char(13) + char(10) + @address3,'') +
						  isnull(char(13) + char(10) + @address4,'') +
						  isnull(char(13) + char(10) + @address5,'') +
						  isnull(char(13) + char(10) + @address6,'') +
						  isnull(char(13) + char(10) + @address7,'') +
						  isnull(char(13) + char(10) + @address8,'')

	if len(@answer) > 0
		select	@answer = substring(@answer, 3, 800)

	return ( @answer )
end
GO
