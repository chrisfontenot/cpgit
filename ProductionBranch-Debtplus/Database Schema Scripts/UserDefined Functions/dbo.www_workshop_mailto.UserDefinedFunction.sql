USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[www_workshop_mailto]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[www_workshop_mailto]( @IDWorkshop as int ) returns varchar(256) as
begin
	-- ==========================================================================================================
	-- ==          Given a workshop ID, generate the proper Email address to make a reservation request.       ==
	-- ==========================================================================================================
	declare	@result	varchar(256)
/* -- Change bankruptcy workshops to a different email address
	if exists (	select		*
			from		workshops w			with (nolock)
			inner join	workshop_contents c		with (nolock) on w.workshop_type = c.workshop_type
			inner join	workshop_content_types wct	with (nolock) on c.content_type = wct.content_type
			where		wct.name like '%bankruptcy%'
			and			w.workshop = @IDWorkshop )
		select	@result = '<a href="common/bankruptcy_workshop.aspx?workshop=' + convert(varchar, @IDWorkshop) + '">' + convert(varchar, @IDWorkshop) + '</a>'
	else
*/	
		select	@result = '<a href="mailto:user@localhost?subject=Reservation Request for Workshop #' + convert(varchar, @IDWorkshop) + '">' + convert(varchar, @IDWorkshop) + '</a>'

	return	(@result)
end
GO
