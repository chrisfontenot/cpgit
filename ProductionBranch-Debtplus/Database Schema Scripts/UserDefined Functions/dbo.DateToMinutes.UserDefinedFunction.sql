USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[DateToMinutes]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DateToMinutes] ( @item as datetime ) returns int as
begin
	declare	@hour	int
	declare	@minute	int
	select	@hour = datepart(hour, @item),
			@minute = datepart(minute, @item)

	declare @answer int
	select	@answer = dbo.ToMinutes ( @hour, @minute )
	return ( @answer )
end
GO
