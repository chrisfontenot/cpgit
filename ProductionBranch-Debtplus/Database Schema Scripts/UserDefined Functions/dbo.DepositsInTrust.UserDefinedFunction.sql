USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[DepositsInTrust]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DepositsInTrust](@client int) returns money as
begin
	-- Given a client, find the deposits_in_trust amount from the pending transactions
	-- We look at only the "real" transactions and normally, you can do a statement like:
	-- update clients set deposit_in_trust = dbo.DepositsInTrust(client) where client = 1133

	declare	@answer		money
	select	@answer		= sum(d.amount)
	from	deposit_batch_details d with (nolock)
	inner join deposit_batch_ids i with (nolock) on d.deposit_batch_id = i.deposit_batch_id
	where	d.client		= @client
	and		d.ok_to_post	= 1
	and		i.date_posted is null
	and		i.batch_type in ('CL','AC','LB')

	return isnull(@answer,0)
end
GO
