USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_CounselingDuration]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_CounselingDuration] ( @minutes as integer ) returns varchar(15) as
begin
	declare @Answer varchar(15)
	select	@Answer	= 	case
					when @minutes < 30 then '<30'
					when @minutes < 60 then '30-59'
					when @minutes < 90 then '60-89'
					when @minutes < 120 then '90-120'
					else '>121'
				end;
	return ( @Answer )
end
GO
