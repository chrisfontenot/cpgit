USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_referred_by]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_referred_by] ( @ReferralCode as int ) returns int as
begin
	declare	@Answer		int
	declare	@Code		varchar(20)
	
	if @ReferralCode is not null
		select	@Code		= hud_9902_section
		from	referred_by with (NOLOCK)
		where	referred_by	= @ReferralCode
	
	if @Code is null
		select	@Code		= 'NOTA'

	select	@Answer	= id
	from	housing_ARM_ReferenceInfo WITH (NOLOCK)
	where	groupId	= 23
	and		name	= @Code
	
	return isnull(@Answer, 9)
end
GO
