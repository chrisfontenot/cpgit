USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_credit_agency]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_credit_agency] ( @AgencyID as varchar(80) ) returns int as
begin
	declare	@key			int
	
	select	@key = ID
	from	housing_ARM_ReferenceInfo with (nolock)
	where	groupId = 32
	and		name	= @AgencyID
	
	if @key is null
		select	@key = ID
		from	housing_ARM_ReferenceInfo with (nolock)
		where	groupId		= 32
		and		shortDesc	= 'OT'

	return isnull(@key, 5)
end
GO
