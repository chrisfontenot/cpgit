USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[median_income]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[median_income] ( @client as int ) returns money as
begin
	declare	@gross_income	money
	declare	@net_income	money
	select	@gross_income   = sum(gross_income),
		@net_income	= sum(net_income)
	from	people with (nolock)
	where	client	= @client

	declare	@assets		money
	select	@assets		= sum(asset_amount)
	from	assets with (nolock)
	where	client		= @client

	return (isnull(@gross_income,0) + isnull(@assets,0)) * 12.0
end
GO
