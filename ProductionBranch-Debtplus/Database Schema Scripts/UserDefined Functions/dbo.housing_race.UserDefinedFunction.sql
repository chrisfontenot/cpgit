USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_race]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_race] ( @Race as int ) returns varchar(15) as
begin
	DECLARE @Answer varchar(15)
	
	IF @Race IS NOT NULL
	BEGIN
		select @Answer = case @Race
								when  1 then '3'		-- African-American (black)
								when  2 then '2'		-- Asian
								when  3 then '5'		-- Caucasian (white)
								when  4 then '5'		-- Hispanic
								when  5 then '10'		-- Other multi-racial
								when  6 then '11'		-- East Indian
								when  7 then '9'		-- American Indian & Black
								when  8 then '11'		-- Middle Eastern
								when  9 then '1'		-- American Indian
								when 10 then '11'		-- Unspecified
								when 11 then '4'		-- Hawaiian / Pacific Islander
								when 12 then '9'		-- Amerian Indian & White
								when 13 then '7'		-- Asian & White
								when 14 then '8'		-- Black & White
								else '11'				-- Something else
							end
	END

	RETURN ( @Answer )
end
GO
