USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_state]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_state] ( @input_state as varchar(10)) returns int as
begin
	declare	@result		int

	select	@input_state	= ltrim(rtrim(@input_state))
	if isnull(@input_state,'') = ''
		select	@result		= 1
	else
		select	@result		= state
		from	states with (nolock)
		where	mailingcode	= @input_state
		and	ActiveFlag	= 1

	if @result is null
		select	@result		= state
		from	states with (nolock)
		where	mailingcode	= left(@input_state,2)
		and	ActiveFlag	= 1

	if @result is null
		select	@result		= min(state)
		from	states with (nolock)

	if @result is null
		select	@result		= 1

	return ( @result )
END
GO
