USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_unsecured_debt_payment]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[client_unsecured_debt_payment] ( @client as int ) returns money as
begin
	declare	@answer		money
	select	@answer = SUM(case
							when ccl.zero_balance <> 0 then cc.disbursement_factor
							when cc.disbursement_factor < bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments then cc.disbursement_factor
							else bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments
						 end)
	from	client_creditor cc with (nolock)
	left outer join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
	left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
	left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
	where	cc.client = @client
	and		cc.reassigned_debt = 0
	
	return isnull(@answer,0)
end
GO
