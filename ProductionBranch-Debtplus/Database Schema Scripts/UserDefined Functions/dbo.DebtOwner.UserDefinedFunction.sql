USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[DebtOwner]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[DebtOwner](@client_creditor as int) returns int as
begin
	-- ==================================================================================
	-- ==       Find the owner/signor of the indicated debt                            ==
	-- ==================================================================================
	declare	@person		int
	declare	@client		int
	
	select	@person			= cc.person,
			@client			= cc.client
	from	client_creditor cc with (nolock)
	where	client_creditor	= @client_creditor
	
	if @person is null
		select	@person	= person
		from	people
		where	client		= @client
		and		relation	= 1
		
	return @person
end
GO
