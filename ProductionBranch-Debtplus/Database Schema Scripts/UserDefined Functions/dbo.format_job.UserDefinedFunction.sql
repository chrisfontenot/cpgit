USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_job]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_job] ( @job as int, @other_job as varchar(50) = null ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	select	@answer = ltrim(rtrim(isnull(@other_job,'')))
	if @answer = ''
		select	@answer	 = description
		from	job_descriptions
		where	job_description = @job
		
	return @answer
end
GO
