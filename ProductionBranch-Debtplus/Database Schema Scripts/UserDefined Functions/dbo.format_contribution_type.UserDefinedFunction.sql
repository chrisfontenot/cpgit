USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_contribution_type]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_contribution_type] ( @eft_format as varchar(1) = 'N', @check_format as varchar(1) = 'N') returns varchar(10) as
begin
	if @eft_format is null
		select	@eft_format = isnull(@check_format,'N')

	if @check_format is null
		select	@check_format = @eft_format

	if @eft_format <> @check_format
		return @eft_format + '/' + @check_format

	return @eft_format
end
GO
