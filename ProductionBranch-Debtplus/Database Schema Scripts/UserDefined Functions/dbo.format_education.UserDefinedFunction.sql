USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_education]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_education] ( @input as int ) returns varchar(80) as
begin
	declare	@answer		varchar(80)

	select	@answer	= description
	from	EducationTypes
	where	oID		= @input

	return isnull(@answer, 'Unkown')
end
GO
