USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_account_number]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_account_number] ( @input_account varchar(80) ) returns varchar(50) as
begin
	declare	@result	varchar(50)

	-- For the time being, accounts are valid only for the first 22 charaters.
	select	@result = left(replace(replace(@input_account,' ',''),'-',''),22)

	return ( @result )
end
GO
