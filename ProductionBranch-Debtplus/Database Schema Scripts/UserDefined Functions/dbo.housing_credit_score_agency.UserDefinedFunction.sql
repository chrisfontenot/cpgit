USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_credit_score_agency]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_credit_score_agency] ( ) returns varchar(15) as
begin
	RETURN ( 'EQUIFAX' )
end
GO
