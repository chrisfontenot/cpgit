USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_TimeZoneLabel]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_TimeZoneLabel](@TimeZoneID AS INT = NULL, @InputTime AS DATETIME = NULL) RETURNS VARCHAR(80) AS
BEGIN
	-- ==============================================================================================
	-- ==         Given the time and the appropriate timezone, find the proper label               ==
	-- ==============================================================================================
	
	-- If there is no timezone then use the one configured for the system
	IF @TimeZoneID IS NULL
		SELECT	@TimeZoneID	= st.TimeZoneID
		FROM	states st WITH (NOLOCK)
		INNER JOIN addresses a WITH (NOLOCK) ON st.state = a.state
		WHERE	st.[Default] = 1

	DECLARE	@answer			VARCHAR(80)
	DECLARE	@Rule			int
	DECLARE	@StandardTime	varchar(10)
	DECLARE	@DSTTime		varchar(10)

	-- Find the time information from the timezone record.
	select	@Rule			= TimeZoneShiftRule,
			@StandardTime	= StandardTime,
			@DSTTime		= DSTTime
	from	TimeZones WITH (NOLOCK)
	WHERE	oID				= @TimeZoneID

	-- Find the starting period that best covers the time period that we want
	IF @InputTime IS NOT NULL
	BEGIN
		declare	@StartingPeriod	datetime
		select	@StartingPeriod	= max(StartingPeriod)
		from	TimeZoneShifts WITH (NOLOCK)
		WHERE	DSTRule			= @Rule
		AND		StartingPeriod	<= @InputTime;

		-- From that period, find the offset value
		DECLARE	@DSTOffset		float
		IF @StartingPeriod IS NOT NULL
			select	@DSTOffset		= ShiftOffset
			from	TimeZoneShifts WITH (NOLOCK)
			WHERE	DSTRule			= @Rule
			AND		StartingPeriod	= @StartingPeriod;

		-- Given the offset, use either the daylight time or the standard time
		-- for the answer
		IF @DSTOffset IS NOT NULL AND @DSTOffset <> 0.0
			select	@StandardTime	= @DSTTime;

	-- If there is no time specified then just use the normal labels. Include both if they are present.
	END ELSE
		IF @StandardTime <> @DSTTime
			SELECT	@StandardTime = @StandardTime + '/' + @DSTTime

	return @StandardTime
END
GO
