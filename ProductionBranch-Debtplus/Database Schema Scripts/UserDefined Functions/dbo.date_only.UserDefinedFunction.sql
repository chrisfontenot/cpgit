USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[date_only]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[date_only] (@input_date as datetime = null) RETURNS datetime AS  
BEGIN 
	if @input_date is not null
		select	@input_date = convert(datetime, convert(varchar(10), @input_date, 101) + ' 00:00:00')

	return @input_date
END
GO
