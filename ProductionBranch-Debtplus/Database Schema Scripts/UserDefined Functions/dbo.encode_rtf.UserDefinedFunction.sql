USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[encode_rtf]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[encode_rtf] (@text_string as varchar(4000) = null, @font as varchar(80) = 'Times New Roman') RETURNS varchar(6000) AS
BEGIN
	-- =====================================================================================================
	-- ==              Encode the input string to a suitable RTF text string.                             ==
	-- =====================================================================================================
	declare	@header_1	varchar(80)
	declare	@header_2	varchar(80)
	declare	@trailer	varchar(80)
	declare	@result		varchar(4000)

	select	@header_1	= '{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fmodern\fprq1\fcharset0 ',
		@header_2	= ';}}\viewkind4\uc1\pard\f0\fs20 ',
		@trailer	= '}',
		@result		= ltrim(rtrim(isnull(@text_string,'')))

	-- Translate the characters to valid RTF codes.
	select	@result = dbo.encode_rtf_text ( @result )

	-- Apply the line ending sequences as needed
	select	@result = replace(@result, char(13) + char(10), '\par ')	-- End of paragraph
	select	@result = replace(@result, char(10), '\line ')			-- Newline

	-- Return the resulting encoded string
	select	@result = @header_1 + @font + @header_2 + @result + @trailer
	return (@result)
END
GO
