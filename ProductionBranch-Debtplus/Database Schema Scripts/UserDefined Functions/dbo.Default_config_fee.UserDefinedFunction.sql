USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[Default_config_fee]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[Default_config_fee]() returns int as
begin
	-- ===========================================================================================================
	-- ==           Find the default item for the config_fees table                                             ==
	-- ===========================================================================================================
	declare	@answer		int
	declare	@state		int
/* -- Make the answer specific to the state
	declare	@addressID	int

	-- Find the default address
	select	@addressID	= AddressID
	from	[config]

	if @AddressID is not null
		select	@state	= state
		from	addresses with (nolock)
		where	@address	= @AddressID

	if @state is not null
	begin
		select	@answer	= min(config_fee)
		from	config_fees
		where	[state]		= @state
		and		[default]	= 1

		if @answer is null
			select	@answer = min(config_fee)
			from	config_fees
			where	[state]	= @state
	end
*/

	-- Just take any state, but the one marked "DEFAULT"
	if @answer is null
	begin
		select	@answer	= min(config_fee)
		from	config_fees
		where	[default]	= 1

		if @answer is null
			select	@answer = min(config_fee)
			from	config_fees
	end

	-- NULL is NOT acceptable.
	return isnull(@answer,1)
end
GO
