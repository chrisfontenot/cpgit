USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[date_dd_mm_yyyy]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[date_dd_mm_yyyy](@input_date as datetime) returns varchar(10) as
begin
	declare	@month		int
	declare	@day		int
	declare	@year		int

	select	@month	= datepart(month, @input_date),
			@day	= datepart(day, @input_date),
			@year	= datepart(year, @input_date)

	declare	@result		varchar(10)
	select	@result =	right('00' + convert(varchar, @day), 2) + '/' +
						right('00' + convert(varchar, @month), 2) + '/' +
						right('0000' + convert(varchar, @year), 4)
	return  @result
end
GO
