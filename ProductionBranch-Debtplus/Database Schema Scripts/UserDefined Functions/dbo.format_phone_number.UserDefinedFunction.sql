USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_phone_number]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_phone_number] ( @phone as varchar(40) ) returns varchar(40) as
begin
	-- =================================================================================================
	-- ==          Return the formatted telephone number as required                                  ==
	-- =================================================================================================

	-- If the client is null, the answer is null
	if @phone is null
		return null

	select @phone = ltrim(rtrim(@phone))

	-- If the phone number is zeros then return null
	if @phone in ('0000000000', '0000000')
		return null

	-- If the telephone number is only seven digits, use the phone number
	if @phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		return left(@phone,3) + '-' + right(@phone,4)

	-- The phone number must be 10 digits to be formatted at this point
	if @phone not like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		return @phone

	-- Accept 000 as the area code
	if @phone like '000[0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		return substring(@phone, 4, 3) + '-' + right(@phone, 4)

	-- Return the formatted phone
	return '(' + substring(@phone, 1, 3) + ') ' + substring(@phone, 4, 3) + '-' + right(@phone, 4)
end
GO
