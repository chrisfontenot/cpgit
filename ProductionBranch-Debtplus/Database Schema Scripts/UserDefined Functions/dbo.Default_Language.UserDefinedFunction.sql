USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[Default_Language]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[Default_Language]() returns int as
begin
	-- ===========================================================================================================
	-- ==           Find the default item for the Attributes table, languages                                   ==
	-- ===========================================================================================================
	declare	@answer		int

	-- Try for the one marked default
	select	@answer		= min(oID)
	from	AttributeTypes with (nolock)
	where	[default]	= 1
	and		[Grouping]	= 'LANGUAGE'

	-- Failing that, take the first item in the table
	if @answer is null
		select	@answer		= min(oID)
		from	AttributeTypes with (nolock)
		where	[Grouping]	= 'LANGUAGE'

	-- NULL is NOT acceptable.
	return isnull(@answer, 1)
end
GO
