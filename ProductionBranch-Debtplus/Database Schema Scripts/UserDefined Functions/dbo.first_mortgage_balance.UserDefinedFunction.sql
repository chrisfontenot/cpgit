USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[first_mortgage_balance]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[first_mortgage_balance] ( @client as int ) returns money as
begin

	-- ================================================================================================
	-- ==        Given the client, find the mortgage balance for the primary residence               ==
	-- ================================================================================================
	declare	@answer	money

	select	@answer = sum(l.CurrentLoanBalanceAmt)
	from	housing_loans l
	inner join housing_properties p	on l.PropertyID = p.oID
	where	p.Residency			= 1
	and		p.HousingID			= @client

	return isnull(@answer,0)
end
GO
