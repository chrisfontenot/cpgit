USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_rates]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_rates] ( @input_number float ) returns float as
begin
	declare	@result	float

	select	@result = isnull(@input_number,0.0)
	select	@result = case
			when @result < 0.0 then 0.0
			when @result >= 100.0 then 0.99999999
			when @result < 1.0 then @result
			else @result / 100.0
		end

	return ( @result )
end
GO
