USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_ach_checking_savings]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_ach_checking_savings] ( @input as varchar(1) ) returns varchar(50) as
begin
	if @input = 'S'
		return ( 'Savings' )

	return ( 'Checking' )
end
GO
