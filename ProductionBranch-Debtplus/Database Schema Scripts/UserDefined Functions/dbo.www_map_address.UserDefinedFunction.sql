USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[www_map_address]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[www_map_address] ( @address1 as varchar(80) = null, @address2 as varchar(80) = null, @city as varchar(80) = null, @state as int = null, @postalcode as varchar(80) = null ) returns varchar(800) as
begin
	-- ==========================================================================================================
	-- ==          Generate a mapquest link to show a map for the given address. The results are HTML.         ==
	-- ==========================================================================================================

	-- Find the state from the states table
	declare	@result		varchar(800)
	declare	@state_text	varchar(80)
	declare	@country_text	varchar(80)
	declare	@country	int

	-- Translate the state to a valid item and find the country code from the state
	if @state is not null
		select	@state_text	= left(mailingcode,2),
			@country	= country
		from	states with (nolock)
		where	state		= @state

	-- For now, assume that the address is in the U.S.A.
	select	@country_text	= 'US'

	select	@state_text	= ltrim(rtrim(@state_text)),
		@country_text	= ltrim(rtrim(@country_text)),
		@city		= ltrim(rtrim(@city)),
		@postalcode	= ltrim(rtrim(@postalcode)),
		@address1	= ltrim(rtrim(@address1)),
		@address2	= ltrim(rtrim(@address2))

	-- If the country is "USA", then format the zipcodes
	if @country_text = 'US'
	begin
		select	@postalcode = left(replace(@postalcode, '-', ''), 5)
		if isnull(@postalcode,'00000') = '00000'
			select	@postalcode = ''
	end

	-- Generate the map reference
	select	@result = ''
	if @address1 is not null
		select	@result = @result + 'address=' + dbo.encode_html ( @address1 ) + '&'

	if @city is not null
		select	@result = @result + 'city=' + dbo.encode_html ( @city ) + '&'

	if @state_text is not null
		select	@result = @result + 'state=' + dbo.encode_html ( @state_text ) + '&'

	if @postalcode is not null
		select	@result = @result + 'zipcode=' + dbo.encode_html ( @postalcode ) + '&'

	if @country_text is not null
		select	@result = @result + 'country=' + dbo.encode_html ( @country_text ) + '&'

	-- Add the standard formatting if there is an address to be used
	if @result <> ''
		select	@result	= '&nbsp;(<a href="http://www.mapquest.com/maps/map.adp?' + @result + 'cid=lfmaplink" target="_blank">MAP</a>)'

	return ( @result )
end
GO
