USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_client_to_housing_interview]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_client_to_housing_interview] (@client as int) RETURNS int AS
BEGIN
	declare	@last_date		datetime
	declare	@hud_interview	int

	select	@last_date		= max(interview_date)
	from	hud_interviews with (nolock)
	where	client			= @client

	if @last_date is not null
	begin
		select	@hud_interview	= hud_interview
		from	hud_interviews with (nolock)
		where	client			= @client
		and		interview_date	= @last_date		
	end

	return ( @hud_interview )
END
GO
