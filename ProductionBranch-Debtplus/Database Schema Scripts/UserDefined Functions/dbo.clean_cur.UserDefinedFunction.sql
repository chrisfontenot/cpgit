USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[clean_cur]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[clean_cur] ( @value as varchar(80)) returns varchar(80) as
begin
	select	@value = replace(replace(@value, ',', ''), '$', '')
	return ( @value )
end
GO
