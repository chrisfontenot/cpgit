USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_office]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_office] ( @office as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	if @office is not null
		select	@answer = name
		from	offices
		where	office = @office
		
	return @answer
end
GO
