USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_gender]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_gender] ( @gender as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	select	@answer	= description
	from	GenderTypes
	where	oID		= @gender
	
	return @answer
end
GO
