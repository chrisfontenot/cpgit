USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[parse_phone_number]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[parse_phone_number] ( @phone_number varchar(80) ) returns varchar(8) as
begin
	select	@phone_number = replace(replace(replace(replace(@phone_number,' ',''),'-',''),'(',''),')','')
	if len(@phone_number) > 7
		select	@phone_number = right(@phone_number, 7)
	if len(@phone_number) = 7
		select	@phone_number = left(@phone_number, 3) + '-' + right(@phone_number,4)
	return @phone_number
end
GO
