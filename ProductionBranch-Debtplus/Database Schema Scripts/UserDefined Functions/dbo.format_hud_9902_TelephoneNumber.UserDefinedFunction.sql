USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_hud_9902_TelephoneNumber]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_hud_9902_TelephoneNumber] ( @TelephoneNumber as int = null) returns varchar(30) as
begin
	declare	@answer		varchar(30)
	select	@answer	 = dbo.Format_TelephoneNumber_BaseOnly( @TelephoneNumber )
	if len(isnull(@answer,'')) >= 10
		select	@answer = left(@answer,3) + '-' + substring(@answer, 4, 3) + '-' + substring(@answer, 7, 4)
	else
		select	@answer = '000-000-0000'
	return @answer
end
GO
