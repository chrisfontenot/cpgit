if not exists(SELECT * FROM sysobjects WHERE name=N'zero_bal_email_substitutions' AND type='FN')
	EXEC('CREATE function [dbo].[zero_bal_email_substitutions](@client_creditor_balance as int = null, @client_creditor as int = null, @creditor as varchar(10) = null, @creditor_name as varchar(80) = null, @account_number as varchar(80) = null, @disbursement_factor as money = 0, @orig_balance as money = 0, @balance as money = 0, @email_address as varchar(1024) = null, @email_name as varchar(1024) = null) returns varchar(max) as begin return null; end')
GO

ALTER function [dbo].[zero_bal_email_substitutions](@client_creditor_balance as int = null, @client_creditor as int = null, @creditor as varchar(10) = null, @creditor_name as varchar(80) = null, @account_number as varchar(80) = null, @disbursement_factor as money = 0, @orig_balance as money = 0, @balance as money = 0, @email_address as varchar(1024) = null, @email_name as varchar(1024) = null) returns varchar(max) as
begin
	declare	@ans		varchar(max)
	if @creditor is not null
		set @ans = '<substitution><field>creditor_id</field><value>' + @creditor + '</value></substitution>'

	if @creditor_name is not null
		set @ans = @ans + '<substitution><field>creditor_name</field><value>' + @creditor_name + '</value></substitution>'

	if @account_number is not null
		set @ans = @ans + '<substitution><field>account_number</field><value>' + @account_number + '</value></substitution>'

	if @disbursement_factor is not null
		set @ans = @ans + '<substitution><field>disbursement_factor</field><value>$' + convert(varchar,@disbursement_factor) + '</value></substitution>'

	if @orig_balance is not null
		set @ans = @ans + '<substitution><field>orig_balance</field><value>$' + convert(varchar,@orig_balance) + '</value></substitution>'

	if @balance is not null
		set @ans = @ans + '<substitution><field>balance</field><value>$' + convert(varchar,@balance) + '</value></substitution>'

	if @ans is not null
		set @ans = '<substitutions>' + @ans + '</substitutions>'

	return @ans
end
GO
GRANT EXECUTE ON zero_bal_email_substitutions TO public AS dbo;
DENY EXECUTE ON zero_bal_email_substitutions TO www_role;
GO
