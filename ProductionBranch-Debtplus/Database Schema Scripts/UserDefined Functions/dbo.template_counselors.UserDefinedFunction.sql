USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[template_counselors]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[template_counselors] ( @appt_time_template as int ) returns varchar(800) as
-- ============================================================================
-- ==       Used by the .NET interface to return a list of the counselors    ==
-- ==       that are assigned to a given appointment template time slot      ==
-- ============================================================================
begin
	declare	@counselors	varchar(800)
	select	@counselors = isnull(@counselors+',','')+isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'')
	from	appt_counselor_templates a
	inner join counselors co on a.counselor = co.counselor
	left outer join names cox on co.nameid = cox.name
	where	a.appt_time_template = @appt_time_template
	return @counselors
end
GO
