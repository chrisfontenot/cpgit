create function [dbo].[Default_PayFrequency]() returns int as
begin
	-- ===========================================================================================================
	-- ==           Find the default item for the PayFrequency table                                            ==
	-- ===========================================================================================================
	declare	@answer		int

	-- Try for the one marked default
	select	@answer		= min(oID)
	from	PayFrequencyTypes with (nolock)
	where	[default]	= 1

	-- Failing that, take the first item in the table
	if @answer is null
		select	@answer		= min(oID)
		from	PayFrequencyTypes with (nolock)

	-- NULL is NOT acceptable.
	return isnull(@answer,4)
end
GO
