USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_Address_Line_1]    Script Date: 01/20/2016 10:52:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP FUNCTION [dbo].[format_Address_Line_1]
GO

CREATE FUNCTION [dbo].[format_Address_Line_1](@House as varchar(20) = null, @Direction as varchar(20) = null, @street as varchar(80) = null, @Suffix as varchar(10) = null, @Modifier as varchar(10) = null, @Modifier_value as varchar(40) = null) returns varchar(80) as
begin
	declare	@Answer		varchar(80),
			@Additional	varchar(80),
			@require_modifier int

	select	@Answer		= '',
			@additional = ltrim(rtrim(isnull(@modifier,'')))
	
	SELECT @Answer = @Answer 
		+ CASE WHEN isnull(@House,'') <> '' THEN @house + ' ' ELSE '' END
		+ CASE WHEN isnull(@direction,'') <> '' THEN @direction + ' ' ELSE '' END
		+ CASE WHEN isnull(@street,'') <> '' THEN @street + ' ' ELSE '' END
		+ CASE WHEN isnull(@suffix,'') <> '' THEN @suffix + ' ' ELSE '' END

	-- Determine if the modifier is used. If not, and it is not given then clear the modifier
	-- Otherwise, append the modifier value to the field.	
	select  @require_modifier = require_modifier
	from    postal_abbreviations with (nolock)
	where	abbreviation = @additional
	and		type = 3;

	-- If a modifier is required then look for it		
	if ISNULL(@require_modifier, 0) <> 0
	begin
	
		-- If one was not given then ignore the modifier
		if isnull(@modifier_value,'') = ''
			select @additional = ''
		else
			-- Otherwise, add the modifier value
			select	@additional = @additional + ' ' + @modifier_value;
	end

	-- Combine the modifier field			
	select	@answer = ltrim(rtrim(@answer + ' ' + @additional))			
	return ( @Answer )
end