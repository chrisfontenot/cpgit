USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_city_state_zip]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_city_state_zip] ( @City as varchar(80) = null, @State as int = null, @PostalCode as varchar(40) ) returns varchar(256) as
begin
	-- =================================================================================================
	-- ==          Return the formatted address line                                                  ==
	-- =================================================================================================

	-- Find the formatting information from the states table
	declare	@result		varchar(256)
	declare	@AddressFormat	varchar(256)
	declare	@Name		varchar(256)
	declare	@USAFormat	int
	declare	@MailingCode	varchar(10)
	declare	@CountryID	int
	declare	@Country	varchar(256)

	select	@AddressFormat	= st.AddressFormat,
		@USAFormat	= st.USAFormat,
		@MailingCode	= st.MailingCode,
		@CountryID	= st.Country,
		@Country	= co.[Description],
		@Name		= st.[Name]
	from	states st		with (nolock)
	left outer join countries co	with (nolock) ON st.country = co.country
	where	st.state	= @state

	-- If this is an USA address, force the zipcode to be properly defined
	if @USAFormat = 1
	begin
		select	@PostalCode	= dbo.numbers_only ( @PostalCode )
		if len(@PostalCode) = 9
			if right(@PostalCode,4) = '0000'
				select	@PostalCode = left(@PostalCode, 5)
		if len(@PostalCode) = 9
			select	@PostalCode = left(@PostalCode, 5) + '-' + right(@PostalCode, 4)
		else
			select	@PostalCode = left(@PostalCode, 5)
	end

	-- Generate the proper resulting string
	select	@result	= replace(@AddressFormat, '{0}', isnull(@City,''))
	select	@result = replace(@result,        '{1}', isnull(@MailingCode,''))
	select	@result = replace(@result,        '{2}', isnull(@PostalCode,''))
	select	@result = replace(@result,	  '{3}', isnull(@Name, ''))
	select	@result = replace(@result,        '{4}', isnull(@Country,''))
	select	@result = replace(@result,        '\r',  char(13))
	select	@result = replace(@result,        '\n',  char(10))

	return ( @result )
end
GO
