USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_postalcode]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_postalcode] ( @input_zipcode as varchar(50)) returns varchar(50) as
begin
	declare	@result		varchar(50)
	if @input_zipcode is null
		select	@result	= ''
	else begin
		select	@result	= dbo.numbers_only ( @input_zipcode )
		if @result <> ''
		begin
			if len(@result) = 9
				select	@result = left(@result, 5) + '-' + right(@result, 4)
			else
				select	@result = left(@result, 5)
		end
	end

	return ( @result )
end
GO
