USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_marital_status]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_marital_status] ( @MaritalStatus int ) returns varchar(15) as
begin
	declare @Answer	varchar(15)
	
	select	@Answer = case @MaritalStatus
				when 2 then 'M'		-- married
				when 3 then 'D'		-- divorced
				when 4 then 'W'		-- widowed
				when 5 then 'D'		-- separated
				else 'NM'			-- single
			end
			
	return ( @Answer )
end
GO
