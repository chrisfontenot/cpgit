USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[last_charindex]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[last_charindex](@input_char as varchar(1), @input_string as varchar(512)) returns int as
begin
	declare @len int
	declare @pos int
	declare @reverse varchar(512)

	select @len = len(@input_string)
	select @reverse = reverse(@input_string)
	select @pos = charindex(@input_char, @reverse)
	if @pos = 0
		return 0
	return @len - @pos + 1
end
GO
