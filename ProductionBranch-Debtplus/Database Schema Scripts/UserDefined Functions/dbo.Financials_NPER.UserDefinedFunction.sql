USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[Financials_NPER]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Financials_NPER] (@rate As float, @pmt As float, @pv As float, @fv As float = 0.0, @due As float = 0.0) returns float as
begin
	-- ===========================================================================================
	-- ==           Find the number of periods to calculate the loan's payments                 ==
	-- ==                                                                                       ==
	-- ==   7.5% paid monthly, $800.00 payment, current balance $48,500.00 = 77 months          ==
	-- ==   select dbo.Financials_NPER(0.075/12, -800, 48500, 0, 0) = 76.441608095770619	    ==
	-- ==                                                                                       ==
	-- ===========================================================================================
	declare @fh	int 		-- File handle
	declare @hr	int		-- Result from the function
	declare	@ret	float		-- Our returned value

	-- Create the function and call it if possible
	Execute @hr = sp_oaCreate 'DebtPlusFinancials.Financials', @fh output, 1 
	if @hr = 0
		execute @hr = sp_oaMethod @fh, 'NPERIF', @ret out, @rate, @pmt, @pv, @fv, @due

	-- If there is an error then obtain the error code
	if @hr <> 0
	begin
		execute sp_OAGetErrorInfo @fh
		select	@ret = -1.0
	end

	-- Destroy the object when it is created
	if @fh <> 0
		execute @hr = sp_oaDestroy fh 

	-- The answer is the floating point returned value
	return @ret
end
GO
