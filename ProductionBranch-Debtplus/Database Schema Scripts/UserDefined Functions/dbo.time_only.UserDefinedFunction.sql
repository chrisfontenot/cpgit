USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[time_only]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[time_only] ( @start_time as datetime ) returns varchar(10) as
begin
/* -- 12 hour format
	declare	@hr		int
	declare	@min		int
	declare	@am_pm		varchar(10)
	declare	@result		varchar(10)

	-- Find the component pieces for the time
	select	@hr		= datepart ( hour , @start_time ),
		@min		= datepart ( minute, @start_time )

	-- It is PM if it is 12:00 or later
	if @hr >= 12
		select	@am_pm	= 'pm'
	else
		select	@am_pm	= 'am'

	-- Correct the hour to the appropriate value
	if @hr > 12
		select	@hr	= @hr - 12

	-- Combine the results properly
	select	@result = right ( '  ' + convert(varchar, @hr), 2 ) + ':' +
			  right ( '00' + convert(varchar, @min), 2 ) + @am_pm
*/

/* -- 24 hour format
*/

	declare	@hr		int
	declare	@min		int
	declare	@result		varchar(10)
	
	-- Find the component pieces for the time
	select	@hr		= datepart ( hour , @start_time ),
		@min		= datepart ( minute, @start_time )

	-- Combine the results properly
	select	@result = right ( '00' + convert(varchar, @hr), 2  ) + ':' +
			  right ( '00' + convert(varchar, @min), 2 )

	return ( @result )
end
GO
