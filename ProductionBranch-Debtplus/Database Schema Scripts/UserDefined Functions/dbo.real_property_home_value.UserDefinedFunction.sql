USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[real_property_home_value]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[real_property_home_value] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(LandValue + ImprovementsValue)
	from	Housing_Properties p
	where	p.HousingId			= @client
	and		p.UseInReports		= 1
	
	return isnull(@answer,0)
end
GO
