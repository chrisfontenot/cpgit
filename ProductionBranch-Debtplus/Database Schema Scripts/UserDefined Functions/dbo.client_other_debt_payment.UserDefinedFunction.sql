USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_other_debt_payment]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[client_other_debt_payment] ( @client as int ) returns money as
begin
	declare	@answer		money
	select	@answer = SUM(payment)
	from	client_other_debts with (nolock)
	where	client = @client
	
	return isnull(@answer,0)
end
GO
