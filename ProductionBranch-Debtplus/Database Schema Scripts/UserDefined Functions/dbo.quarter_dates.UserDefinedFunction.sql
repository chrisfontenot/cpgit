USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[quarter_dates]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[quarter_dates] ( @item_date as datetime )
returns @retQuarter_Dates TABLE (
	today_start	datetime,
	today_end	datetime,
	month_start	datetime,
	month_end	datetime,
	quarter_start	datetime,
	quarter_end	datetime,
	year_start	datetime,
	year_end	datetime
)
AS
BEGIN
	declare	@today_start	datetime
	declare	@today_end	datetime
	declare	@month_start	datetime
	declare	@month_end	datetime
	declare	@quarter_start	datetime
	declare	@quarter_end	datetime
	declare	@year_start	datetime
	declare	@year_end	datetime

	-- Find the values for today and the current month
	select	@today_start	= convert(datetime, convert(varchar(10), @item_date, 101) + ' 00:00:00'),
		@today_end	= convert(datetime, convert(varchar(10), @item_date, 101) + ' 23:59:59')
	select	@month_start	= convert(datetime, convert(varchar, month(@today_start)) + '/01/' + convert(varchar, year(@today_start)) + ' 00:00:00')
	select	@month_end	= dateadd(s, -1, dateadd(m, 1, @month_start))

	-- The year is just a bit more difficult
	select	@year_start	= convert(datetime, '01/01/' + convert(varchar, year(@today_start)) + ' 00:00:00')
	select	@year_end	= dateadd(s, -1, dateadd(yy, 1, @year_start))

	-- The quarter is hard
	declare	@int_month	int
	select	@int_month	= ((( month ( @today_start ) - 1 ) / 3 ) * 3) + 1
	select	@quarter_start	= convert(datetime, convert(varchar, @int_month) + '/01/' + convert(varchar, year (@today_start)) + ' 00:00:00')
	select	@quarter_end	= dateadd(s, -1, dateadd(month,  3, @quarter_start))

	-- Build the result table
	INSERT INTO @retQuarter_Dates ( today_start, today_end,
				   month_start,	month_end,
				   quarter_start, quarter_end,
				   year_start,    year_end )

	select	@today_start as today_start, @today_end as today_end,
		@month_start as month_start, @month_end as month_end,
		@quarter_start as quarter_start, @quarter_end as quarter_end,
		@year_start as year_start, @year_end as year_end

	RETURN
END
GO
