USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[last_name]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[last_name] ( @name as varchar(800) ) returns varchar(800) as
begin
	declare	@split	int
	select	@name	= ltrim(rtrim(@name))
	select	@split	= patindex('% %', @name)
	if @split > 0
		select	@name = ltrim(substring(@name, @split, 800))

	return ( @name )
end
GO
