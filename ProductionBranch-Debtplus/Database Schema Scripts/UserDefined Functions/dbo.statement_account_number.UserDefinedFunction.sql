USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[statement_account_number]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[statement_account_number] (@account_number varchar(800)) RETURNS varchar(50) AS  
BEGIN 
        -- ========================================================================================
        -- ==     Replace the leading characters of the account number with "X" for statements.  ==
        -- ========================================================================================
	declare	@working_account_number	varchar(44)
	declare	@len			int

	-- If the number is missing then the result is also missing
	if @account_number is null
		return null

	-- Find the account number and its length
	select	@working_account_number = right(ltrim(rtrim(@account_number)), 44)
	select	@len = len(@working_account_number)

	-- Replace the leading characters with "X" for the account number if it is longer than 4 digits
	if @len > 4
		select	@working_account_number = left('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX', @len - 4) + right(@working_account_number, 4)

	-- Return the resulting number
	return @working_account_number
END
GO
