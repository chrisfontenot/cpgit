USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[ToExcelDateTime]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ToExcelDateTime](@input as datetime) returns varchar(80) as
begin
	-- =====================================================================================
	-- ==        Convert an input date/time to the numerical format used by Excel         ==
	-- =====================================================================================

	declare	@answer		int
	declare	@month		int
	declare	@day		int
	declare	@year		int
	declare	@hour		int
	declare	@minute		int
	declare	@second		int
	declare	@enddate	datetime
	declare	@startdate	datetime
	select	@startdate	= dateadd(d, -1, convert(datetime, '1/1/1900'))
	
	select	@month		= datepart(month, @input),
			@day		= datepart(day, @input),
			@year		= datepart(year, @input),
			@hour		= datepart(hour, @input),
			@minute		= datepart(minute, @input),
			@second		= datepart(second, @input)

	select	@enddate	= convert(datetime, convert(varchar, @month) + '/' + convert(varchar, @day) + '/' + convert(varchar, @year))
	select	@answer		= DATEDIFF(Day, @StartDate, @EndDate)
	
	-- Find the number of seconds since midnight for the time
	select	@second		= @second + (@hour * 3600) + (@minute * 60)
	
	-- Normalize it to a portion of the day
	declare	@fraction	float
	select	@fraction	= convert(float,@second) / convert(float,86400)

	declare	@result		varchar(80)
	select	@result	= convert(varchar, @fraction)
	select	@result = substring(@result, 2, 80)
	
	select	@result = convert(varchar, @answer) + @result
	return  @result
end
GO
