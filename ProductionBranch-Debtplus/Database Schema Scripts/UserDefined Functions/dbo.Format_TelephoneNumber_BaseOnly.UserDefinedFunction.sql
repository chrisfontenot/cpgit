USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[Format_TelephoneNumber_BaseOnly]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Format_TelephoneNumber_BaseOnly] ( @TelephoneNumber as int ) returns varchar(80) as
begin
    -- ===================================================================================================
    -- ==           Return the formatted telephone number based upon the country and area code          ==
    -- ===================================================================================================
    declare @answer     varchar(80)
    declare @acode      varchar(20)
    declare @number     varchar(40)
    
    -- Retrieve the values from the record
    if @TelephoneNumber is not null
    begin
        select  @acode      = acode,
                @number     = number
        from    TelephoneNumbers with (nolock)
        where   TelephoneNumber = @TelephoneNumber
    end
    
    -- Include the area code
    if ltrim(rtrim(isnull(@acode,'000'))) = '000'
		select	@acode	= right('000' + convert(varchar,AreaCode),3)
		from	config with (nolock)

	-- Include the area code
    select @answer = right('000' + @acode, 3)
    
    -- Include the telephone number
    if ltrim(rtrim(isnull(@number,''))) <> ''
	    select @answer = isnull(@answer,'') + isnull(replace(replace(@number,' ',''),'-',''),'')
     
    -- Return the resulting value   
    return ( @answer )
end
GO
