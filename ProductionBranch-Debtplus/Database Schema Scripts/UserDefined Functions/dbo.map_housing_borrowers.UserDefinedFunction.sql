USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_housing_borrowers]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_housing_borrowers] ( @HousingPropertyID int, @oID int )
returns @retBorrower TABLE (
	oID					int,
	PersonID			int,
	NameID				int null,
	EmailID				int null,
	Former				varchar(80) null,
	SSN					varchar(10) null,
	Gender				int null,
	Race				int null,
	Ethnicity			int null,
	Language			int null,
	Disabled			int null,
	MilitaryService		int null,
	Marital_Status		int null,
	Education			int null,
	Birthdate			datetime null,
	FICO_Score			int null,
	CreditAgency		varchar(20) null,
	emp_start_date		datetime null,
	emp_end_date		datetime null,
	other_job			varchar(80) null
)
AS
BEGIN

	-- Retrieve the record from the system
	declare	@ClientID			int
	declare @PersonID			int
	declare @NameID				int
	declare @EmailID			int
	declare @Former				varchar(80)
	declare @SSN				varchar(10)
	declare @Gender				int
	declare @Race				int
	declare @Ethnicity			int
	declare @Language			int
	declare @Disabled			int
	declare @MilitaryService	int
	declare @Marital_Status		int
	declare @Education			int
	declare @Birthdate			datetime
	declare @FICO_Score			int
	declare @CreditAgency		varchar(20)
	declare @emp_start_date		datetime
	declare @emp_end_date		datetime
	declare @other_job			varchar(80)

	select	@PersonID			= [PersonID],
			@NameID				= [NameID],
			@EmailID			= [EmailID],
			@Former				= [Former],
			@SSN				= [SSN],
			@Gender				= [Gender],
			@Race				= [Race],
			@Ethnicity			= [Ethnicity],
			@Language			= [Language],
			@Disabled			= [Disabled],
			@MilitaryService	= [MilitaryService],
			@Marital_Status		= [Marital_Status],
			@Education			= [Education],
			@Birthdate			= [Birthdate],
			@FICO_Score			= [FICO_Score],
			@CreditAgency		= [CreditAgency],
			@Emp_Start_Date		= [emp_start_date],
			@Emp_End_Date		= [emp_end_date],
			@other_job			= [other_job]
	from	housing_borrowers with (nolock)
	where	[oID]				= @oID

	if @personID = 1
	begin
		select	@ClientID			= h.[HousingID]
		from	housing_properties h with (nolock)
		where	[oID]				= @HousingPropertyID;

		select	@NameID				= p.[NameID],
				@EmailID			= p.[EmailID],
				@Former				= p.[Former],
				@SSN				= p.[SSN],
				@Gender				= p.[Gender],
				@Race				= p.[Race],
				@Ethnicity			= p.[Ethnicity],
				@Disabled			= p.[Disabled],
				@MilitaryService	= p.[MilitaryService],
				@Education			= p.[Education],
				@Birthdate			= p.[Birthdate],
				@FICO_Score			= p.[FICO_Score],
				@CreditAgency		= p.[CreditAgency],
				@Emp_Start_Date		= p.[emp_start_date],
				@Emp_End_Date		= p.[emp_end_date],
				@other_job			= case when p.[job] is null then j.[description] else p.[other_job] end
		from	[people] p WITH (NOLOCK)
		LEFT OUTER JOIN [job_descriptions] j WITH (NOLOCK) ON p.[job] = j.[job_description]
		WHERE	p.[client]			= @clientID
		and		p.[relation]		= 1

		select	@Language			= c.[language],
				@Marital_Status		= c.[Marital_Status]
		from	[clients] c with (nolock)
		where	[client]			= @clientID;
	end

	if @personID = 2
	begin
		select	@ClientID			= h.[HousingID]
		from	housing_properties h with (nolock)
		where	[oID]				= @HousingPropertyID;

		select	@NameID				= p.[NameID],
				@EmailID			= p.[EmailID],
				@Former				= p.[Former],
				@SSN				= p.[SSN],
				@Gender				= p.[Gender],
				@Race				= p.[Race],
				@Ethnicity			= p.[Ethnicity],
				@Disabled			= p.[Disabled],
				@MilitaryService	= p.[MilitaryService],
				@Education			= p.[Education],
				@Birthdate			= p.[Birthdate],
				@FICO_Score			= p.[FICO_Score],
				@CreditAgency		= p.[CreditAgency],
				@Emp_Start_Date		= p.[emp_start_date],
				@Emp_End_Date		= p.[emp_end_date],
				@other_job			= case when p.[job] is null then j.[description] else p.[other_job] end
		from	[people] p WITH (NOLOCK)
		LEFT OUTER JOIN [job_descriptions] j WITH (NOLOCK) ON p.[job] = j.[job_description]
		WHERE	p.[client]			= @clientID
		and		p.[relation]		<> 1

		select	@Language			= c.[language],
				@Marital_Status		= c.[Marital_Status]
		from	[clients] c with (nolock)
		where	[client]			= @clientID;
	end

	-- Build the result table
	INSERT INTO @retBorrower ([oID],[PersonID],[NameID],[EmailID],[Former],[SSN],[Gender],[Race],[ethnicity],[Language],[Disabled],[MilitaryService],[marital_status],[Education],[Birthdate],[FICO_Score],[CreditAgency],[emp_start_date],[emp_end_date],[other_job])
	values (@oID,@PersonID,@NameID,@EmailID,@Former,@SSN,@Gender,@Race,@ethnicity,@Language,@Disabled,@MilitaryService,@marital_status,@Education,@Birthdate,@FICO_Score,@CreditAgency,@emp_start_date,@emp_end_date,@other_job)

	RETURN
END
GO
