USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_map_budgets]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_map_budgets] ( @expense_type_id int ) returns int as
begin

	declare	@result	int

	select	@result = case isnull(@expense_type_id, -1)
		when  1 then 105           -- Rent Mortgage
		when  2 then 110           -- Second Mortgage
		when  3 then 125           -- Homeowners Fees
		when  4 then 510           -- Home Insurance
		when  5 then 145           -- Home Repairs
		when  6 then 130           -- Electric Gas
		when  7 then 135           -- Water Sewer
		when  8 then 310           -- Phone and Cell Phone
		when  9 then 320           -- Cable and Internet
		when 10 then 710           -- Car Payment
		when 11 then 710           -- Car Payment 2
		when 12 then 530           -- Car Insurance
		when 13 then 730           -- Gasoline
		when 14 then 740           -- Car Maintenance
		when 15 then 740           -- Parking Tolls
		when 16 then 540           -- Health Insurance
		when 17 then 960           -- Prescriptions
		when 18 then 520           -- Life Insurance
		when 19 then 920           -- Groceries
		when 20 then 925           -- School Lunches
		when 21 then 925           -- Meals Out
		when 22 then 905           -- Child Care
		when 23 then 910           -- Child Support
		when 24 then 915           -- Student Loans
		when 25 then 920           -- School Supplies
		when 26 then 940           -- Clothing
		when 27 then 950           -- Barber and Beauty
		when 28 then 965           -- Hobbies and Sports
		when 29 then 955           -- Tobacco and Alcohol
		when 30 then 965           -- Newspaper
		when 31 then 970           -- Pet Care
		when 32 then 980           -- Donations
		when 33 then 975           -- Gifts and Holidays
		when 34 then 430           -- Tax Payments
		when 35 then 10000         -- Other
		when 36 then 410           -- Property Tax
        when 38 then 957	       -- Travel/Recreation
		else 10000		   -- Unknown category
	end;

	return ( @result )
end
GO
