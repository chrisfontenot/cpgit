USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[parse_area_code]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[parse_area_code] ( @phone_number varchar(80) ) returns varchar(3) as
begin
	declare	@answer		varchar(3)
	select	@phone_number = replace(replace(replace(replace(@phone_number,' ',''),'-',''),'(',''),')','')
	if len(@phone_number) > 7
		select	@answer = left(@phone_number, 3)
	return @answer
end
GO
