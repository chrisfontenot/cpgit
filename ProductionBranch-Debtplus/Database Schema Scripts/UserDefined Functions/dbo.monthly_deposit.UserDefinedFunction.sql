USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[monthly_deposit]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[monthly_deposit](@client as int) returns money as
begin
	-- Find the amount for non-ACH deposits
	declare	@normal_deposits	money
	select	@normal_deposits	= sum(deposit_amount)
	from	client_deposits with (nolock)
	where	client			= @client
	and	one_time		= 0

/*
	-- Once the client goes on ACH, this information is used
	declare	@ach_deposits		money
	select	@ach_deposits		= sum(d.pull_amount * s.monthly_scale_factor)
	from	client_ach ach with (nolock)
	inner join client_ach_amounts d with (nolock) on ach.client = d.client
	inner join ach_pull_schedules s with (nolock) on d.ach_pull_schedule = s.ach_pull_schedule
	where	ach.client		= @client
	and	d.pull_amount		> 0
	if @@rowcount > 0
		select	@normal_deposits	= @ach_deposits
*/

	return ( isnull(@normal_deposits,0) )
end
GO
