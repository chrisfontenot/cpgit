USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_client_id]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_client_id] ( @client as int ) returns varchar(10) as
begin

	-- =================================================================================================
	-- ==          Return the formatted client id with the suitable leading zeros as needed           ==
	-- =================================================================================================

	-- If the client is null, the answer is null
	if @client is null
		return null

	-- A negative client needs special processing to put the sign in the proper spot
	if @client < 0
		return '-' + right('0000000' + convert( varchar, 0 - @client ), 7)

	-- If the client is too low, use leading zeros
	if @client < 10000000
		return right ('0000000' + convert ( varchar, @client ), 7)

	-- Otherwise, use the client number as it is for larger client numbers
	return convert(varchar(10), @client)
end
GO
