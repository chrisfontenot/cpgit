USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[custom_counseling_statistics_appt_type]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[custom_counseling_statistics_appt_type] ( @appt_type as int ) returns int as

-- Used in the procedure 'custom_counseling_statistics' to provide the monthly summary extract
-- for Orange County

begin
	declare	@answer		int

	-- Process non-null offices
	if @appt_type is null
	begin
		select	@appt_type = appt_type from appt_types where [default] = 1;
		if @appt_type is null
			select	@appt_type = 1
	end
	
	if @appt_type = 1 -- Initial - In Person
		select	@answer = 1
	else if @appt_type = 2 -- Initial - Telephone
		select	@answer = 1
	else if @appt_type = 3 -- Housing-In Person Default
		select	@answer = 3
	else if @appt_type = 4 -- Follow up - In Person
		select	@answer = 2
	else if @appt_type = 5 -- Client Review - In Person
		select	@answer = 7
	else if @appt_type = 6 -- Initial - Internet
		select	@answer = 1
	else if @appt_type = 7 -- Follow up - Internet
		select	@answer = 2
	else if @appt_type = 8 -- Housing Rev Mtg - In Person
		select	@answer = 6
	else if @appt_type = 9 -- Follow up - Telephone
		select	@answer = 2
	else if @appt_type = 10 -- Credit Report Review
		select	@answer = 300
	else if @appt_type = 11 -- Initial - Mail
		select	@answer = 300
	else if @appt_type = 12 -- ReEval - In Person
		select	@answer = 9
	else if @appt_type = 13 -- ReEval - Telephone
		select	@answer = 9
	else if @appt_type = 14 -- ReEval - Internet
		select	@answer = 9
	else if @appt_type = 15 -- ReInstate - In Person
		select	@answer = 8
	else if @appt_type = 16 -- ReInstate - Telephone
		select	@answer = 8
	else if @appt_type = 17 -- ReInstate - Internet
		select	@answer = 8
	else if @appt_type = 18 -- Client Review - Telephone
		select	@answer = 7
	else if @appt_type = 19 -- Client Review - Internet
		select	@answer = 7
	else if @appt_type = 20 -- Housing-Telephone Default
		select	@answer = 3
	else if @appt_type = 21 -- Housing-In Person PrePurchase
		select	@answer = 3
	else if @appt_type = 22 -- Housing-Telephone PrePurchase
		select	@answer = 3
	else if @appt_type = 23 -- Housing-In Person PostPurchase
		select	@answer = 3
	else if @appt_type = 24 -- Housing-Telephone PostPurchase
		select	@answer = 3
	else if @appt_type = 25 -- Follow up - Housing
		select	@answer = 4
	else if @appt_type = 26 -- Hearing Impaired - Initial
		select	@answer = 1
	else if @appt_type = 27 -- Hearing Impaired - Follow up
		select	@answer = 2
	else if @appt_type = 28 -- Housing Rev Mtg - Telephone
		select	@answer = 6
	else if @appt_type = 29 -- Housing Credit Report Review
		select	@answer = 300
	else if @appt_type = 30 -- Bankruptcy - In Person
		select	@answer = 5
	else if @appt_type = 31 -- Bankruptcy - Telephone
		select	@answer = 5
	else if @appt_type = 32 -- Housing COOP Rev Mtg In Person
		select	@answer = 6
	else if @appt_type = 33 -- Housing COOP Rev Mtg Telephone
		select	@answer = 6
	else if @appt_type = 34 -- Housing Bankruptcy In Person
		select	@answer = 5
	else if @appt_type = 35 -- Housing Bankruptcy Telephone
		select	@answer = 5
	else if @appt_type = 36 -- Bankruptcy - Internet
		select	@answer = 5

	return isnull(@answer,1)
end
GO
