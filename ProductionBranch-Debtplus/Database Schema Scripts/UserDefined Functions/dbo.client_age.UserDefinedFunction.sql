USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_age]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[client_age] ( @client as int, @now as datetime, @person as int = 1 ) returns int as
begin
	declare	@answer	int
	if @person = 1
		select	@answer = datediff(year, birthdate, @now)
		from	people
		where	client	= @client
		and		relation = 1
	else
		select	@answer = datediff(year, birthdate, @now)
		from	people
		where	client	= @client
		and		relation <> 1

	return isnull(@answer,0)
end
GO
