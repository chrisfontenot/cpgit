USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[DisbursementLockCount]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[DisbursementLockCount](@disbursement_register int = 0) returns int as
-- ================================================================================
-- ==        Return the number of locks on the disbursement tables               ==
-- ================================================================================
begin
	declare	@Answer		int

	-- Count the number of locks on the table
	select	@Answer = count(*)
	from	sys.dm_tran_locks l
	inner join sys.objects o on l.resource_associated_entity_id = o.object_id
	where l.resource_database_id = db_id()
	and		o.name = 'DISBURSEMENT_LOCK'
	and		o.type = 'U'

	-- Return the count of locks to the caller
	return isnull(@Answer, 0)
end
GO
