USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_email]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[client_email] ( @client as int ) returns varchar(80) as
begin
	declare	@answer	varchar(80)
	select	@answer = emx.address
	from	people p with (nolock)
	inner join EmailAddresses emx on p.emailid = emx.email
	where	isnull(emx.Address,'') <> ''
	and		p.Client = @client
	order by Relation

	return @answer
end
GO
