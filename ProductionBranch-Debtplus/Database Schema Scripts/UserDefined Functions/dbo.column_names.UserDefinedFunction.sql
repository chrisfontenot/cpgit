USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[column_names]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[column_names] ( @table as varchar(20) ) returns varchar(8000) as
begin
	declare	@columns	varchar(8000)
	select	@columns = isnull(@columns + ',[' + name + ']', '[' + name + ']')
	from	syscolumns
	where	id = (select id from sysobjects where name = @table)

	return ( @columns )
end
GO
