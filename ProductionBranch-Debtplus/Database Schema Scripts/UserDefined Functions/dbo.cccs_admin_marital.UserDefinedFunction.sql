USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_marital]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_marital] ( @input_marital varchar(10) ) returns int as
begin
	declare	@result	int
	select	@result		= case isnull(@input_marital,'S')
					when 'S' then 1
					when 'M' then 2
					when 'D' then 3
					when 'W' then 4
					else 1
				  end
	return ( @result )
end
GO
