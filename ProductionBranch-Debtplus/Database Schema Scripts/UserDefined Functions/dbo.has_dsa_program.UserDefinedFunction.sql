﻿USE [DEBTPLUS]
GO
/****** Object:  UserDefinedFunction [dbo].[has_dsa_program]    Script Date: 12/16/2014 2:22:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[has_dsa_program]
(
	@housingproperty int
)
returns int
as 
begin

	declare @dsadetail	int
	declare @programbenefit	int
	set @programbenefit = 0

	select @dsadetail = dsadetailid 
	  from housing_loans
	 where oID = @housingproperty

	if @dsadetail is not null
	begin

		-- Program Benefit (Yes = 0, No = 1)
		select @programbenefit = case ProgramBenefit when 1 then 0 when 0 then 1 else 0 end
		  from Housing_Loan_DSADetails
		 where oID = @dsadetail;

	end

	return isnull(@programbenefit, 0)

end