USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_default_reason]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_default_reason] ( @financial_problem as int ) returns varchar(15) as
begin
	declare	@answer			varchar(15)
	
	SELECT	@answer			= [code]
	from	hope_default_reason_type_code 
	where	@financial_problem = [debtplusID]
		
	return isnull(@answer, '24')
end
GO
