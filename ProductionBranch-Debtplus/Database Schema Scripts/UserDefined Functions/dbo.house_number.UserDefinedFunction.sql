USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[house_number]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[house_number] (@address varchar(80)) returns varchar(80) as
begin
	declare	@marker		int
	select	@address	= ltrim(rtrim(@address))
	select	@marker		= charindex(' ', @address)

	declare	@house		varchar(80)
	if @marker >= 1
	begin
		select	@house	= rtrim(substring(@address, 1, @marker))
		declare	@pos	int
		select	@pos = patindex('%[^0-9]%', @house)
		if	@pos > 0
			select	@house	= null
	end

	return isnull(@house,'')
end
GO
