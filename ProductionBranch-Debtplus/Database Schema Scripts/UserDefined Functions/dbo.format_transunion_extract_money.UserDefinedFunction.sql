USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_transunion_extract_money]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_transunion_extract_money]( @input_money as money) returns varchar(9) as
begin
	return ( right ('0000000000000000' + isnull(convert(varchar, convert(int, round ( @input_money, 0 ))),''), 9 ))
end
GO
