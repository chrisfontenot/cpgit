USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_hud_counts]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_hud_counts] (@isClient int = 0, @isWorkshop int = 0, @Indicator int = 0) returns int as
begin
	declare	@Answer		int
	
	if @indicator = 0
		select @Answer = 0
	else
		select	@Answer = isnull(@isClient,0) + isnull(@isWorkshop,0)
	
	return @Answer
end
GO
