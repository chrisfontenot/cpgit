USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_address_block]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_address_block](@AddressID as int) returns varchar(800) as
begin
	declare	@result				varchar(800)
	
	-- Find the address information
	declare	@creditor_prefix_1	varchar(80)
	declare	@creditor_prefix_2	varchar(80)
	declare	@house				varchar(80)
	declare	@direction			varchar(80)
	declare	@street				varchar(80)
	declare	@suffix				varchar(80)
	declare	@modifier			varchar(80)
	declare	@modifier_value		varchar(80)
	declare	@address_line_2		varchar(80)
	declare	@city				varchar(80)
	declare	@state				int
	declare	@postalcode			varchar(80)

	select	@creditor_prefix_1	= [creditor_prefix_1],
			@creditor_prefix_2	= [creditor_prefix_2],
			@house				= [house],
			@direction			= [direction],
			@street				= [street],
			@suffix				= [suffix],
			@modifier			= [modifier],
			@modifier_value		= [modifier_value],
			@address_line_2		= [address_line_2],
			@city				= [city],
			@state				= [state],
			@postalcode			= [postalcode]
	from	Addresses WITH (NOLOCK)
	WHERE	[Address]			= @AddressID
	
	-- Format the first line of the address
	declare	@address_line_1		varchar(80)
	select	@address_line_1		= dbo.format_address_line_1(@house, @direction, @street, @suffix, @modifier, @modifier_value)
	
	-- Build the address information
	if isnull(@creditor_prefix_1,'') <> ''
		select	@result			= isnull(@result + char(13) + char(10), '') + @creditor_prefix_1;
		
	if isnull(@creditor_prefix_2,'') <> ''
		select	@result			= isnull(@result + char(13) + char(10), '') + @creditor_prefix_2;

	if isnull(@address_line_1,'') <> ''
		select	@result			= isnull(@result + char(13) + char(10), '') + @address_line_1;

	if isnull(@address_line_2,'') <> ''
		select	@result			= isnull(@result + char(13) + char(10), '') + @address_line_2;

	declare	@address_line_3		varchar(80)
	select	@address_line_3		= dbo.format_city_state_zip(@city, @state, @postalcode)
	
	if isnull(@address_line_3,'') <> ''
		select	@result			= isnull(@result + char(13) + char(10), '') + @address_line_3;

	return @result
end
GO
