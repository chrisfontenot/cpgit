create function [dbo].[Default_Relation]() returns int as
begin
	-- ===========================================================================================================
	-- ==           Find the default item for the RelationTypes table                                           ==
	-- ===========================================================================================================
	declare	@answer		int

	-- Try for the one marked default
	select	@answer		= min(oID)
	from	RelationTypes with (nolock)
	where	[default]	= 1
	AND		[oID]       > 1

	-- Failing that, take the first item in the table
	if @answer is null
		select	@answer		= min(oID)
		from	RelationTypes with (nolock)
		WHERE	[oID] > 1

	-- NULL is NOT acceptable.
	return isnull(@answer,2)
end
GO
