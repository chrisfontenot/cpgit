USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_client_to_last_level2_initiated_audit_note]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================

Create FUNCTION [dbo].[map_client_to_last_level2_initiated_audit_note] (@client as int) RETURNS int AS
BEGIN 
	declare	@last_date		datetime
	declare	@level2_audit_note	int

	select	@last_date		= max(date_created)
	from	client_notes with (nolock)
	where	client_note > 6650028
	and client			= @client
	and	subject like 'Level 2 Initiated Audit%'

	if @last_date is not null
	begin
		select	@level2_audit_note	= client_note
		from	client_notes with (nolock)
		where	client_note > 6650028
	and client			= @client
	and	subject like 'Level 2 Initiated Audit%'
		and	date_created		= @last_date		
	end

	return ( @level2_audit_note )
END
GO
