USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_reverse_name]    Script Date: 01/20/2016 11:39:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[format_reverse_name] (@prefix as varchar(10) = null, @first as varchar(80) = null, @middle as varchar(40) = null, @last as varchar(80) = null, @suffix as varchar(10) = null) RETURNS varchar(256) AS
BEGIN

-- ==============================================================================================================
-- ==           Format the a name as "last, first"                                                             ==
-- ==============================================================================================================

	declare  @result		varchar(256),
			 @result_part1	varchar(256),
			 @result_part2  varchar(256)

	-- Generate the name as "last suffix"
	SELECT @result_part1 = CASE WHEN @last IS NOT NULL THEN ISNULL(LTRIM(RTRIM(@last)), '') ELSE '' END
						+ ' ' 
						+ CASE WHEN @suffix IS NOT NULL THEN ISNULL(LTRIM(RTRIM(@suffix)), '') ELSE '' END

	-- Prepend the prefix value
	SELECT @result_part2 = CASE WHEN @prefix IS NOT NULL THEN ISNULL(LTRIM(RTRIM(@prefix)), '') ELSE '' END

	-- Generate the name as "first middle"
	if not @middle is null
	begin
		select @middle = ltrim(rtrim(@middle))
		if @middle <> ''
		begin
			-- If the middle is one character, it is an initial. Add the period.
			if len(@middle) = 1
				select	@middle = @middle + '.'
			select @result_part2 = @result_part2 + ' ' + @middle
		end
	end

	-- Prepend the first name
	SELECT @result_part2 = CASE WHEN @first IS NOT NULL THEN ISNULL(LTRIM(RTRIM(@first)), '') ELSE '' END + @result_part2

	-- Combine the two pieces as needed
	select	@result_part1	= ltrim(rtrim(@result_part1)),
		@result_part2	= ltrim(rtrim(@result_part2))

	-- Add the two parts together with a comma if both are present.
	if @result_part1 <> '' and @result_part2 <> ''
		select	@result = @result_part1 + ', ' + @result_part2
	else
		select	@result = @result_part1 + @result_part2

	-- If the name is empty then the result is null
	if @result = ''
		select	@result = null

	return ( @result )
END
