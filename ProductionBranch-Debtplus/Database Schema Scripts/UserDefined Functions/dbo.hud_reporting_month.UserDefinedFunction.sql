USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[hud_reporting_month]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[hud_reporting_month] ( @period_start as datetime ) returns int as
begin
	declare	@year	int
	declare	@month	int

	-- Remove the time from the date
	select	@period_start = convert(datetime, convert(varchar(10), @period_start, 101))

	-- Find the fiscal year and month values
	select	@year	= fy,
		@month	= fm
	from	calendar with (nolock)
	where	dt	= @period_start

	if @month is null
		select	@month = datepart(month, @period_start)

	return ( @month )
end
GO
