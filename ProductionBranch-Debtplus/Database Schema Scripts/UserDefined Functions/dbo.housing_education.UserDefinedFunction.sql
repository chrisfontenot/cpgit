USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_education]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_education] ( @Education as int ) returns char(1) as
begin
	DECLARE @Answer char(1)
	
	IF @Education IS NOT NULL
	BEGIN
		SELECT @Answer = 'D'

		IF @Education < 20
			SELECT @Answer = 'M'

		IF @Education < 18
			SELECT @Answer = 'B'

		IF @Education < 16
			SELECT @Answer = 'A'

		IF @Education < 14
			SELECT @Answer = 'H'

		IF @Education < 12
			SELECT @Answer = 'N'
	END

	RETURN ( @Answer )
end
GO
