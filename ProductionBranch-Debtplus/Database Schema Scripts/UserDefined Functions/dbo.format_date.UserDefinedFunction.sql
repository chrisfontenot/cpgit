USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_date]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_date] ( @date as datetime ) returns varchar(10) as
begin
	return convert(varchar(10), @date, 101)
end
GO
