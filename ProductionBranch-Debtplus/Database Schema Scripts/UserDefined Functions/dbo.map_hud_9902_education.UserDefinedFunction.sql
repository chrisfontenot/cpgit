USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_education]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_education] (@input as int) returns int as
begin

	declare	@answer		int
	declare	@label		varchar(80)

	-- Find the id from the map column in the messages table
	if @input is not null
		select	@label	= hud_9902_section
		FROM	EducationTypes WITH (NOLOCK)
		WHERE	oID		= @input

	-- If there is no match, then use "unknown"
	if @label is null
		select	@label		= 'UNKN'

	-- Find the corresponding map in the current values		
	select	@answer		= id
	from	housing_ARM_ReferenceInfo with (nolock)
	where	groupid		= 13
	and		name		= @label
		
	return isnull(@answer,1)
end
GO
