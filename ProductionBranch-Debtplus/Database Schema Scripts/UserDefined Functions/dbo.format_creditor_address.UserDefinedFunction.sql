USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_creditor_address]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_creditor_address] ( @creditor as varchar(10), @type as varchar(1) = 'P' ) returns varchar(256) as
begin
	-- ======================================================================================================
	-- ==   Format the creditor address information from the database for a letter.                        ==
	-- ======================================================================================================

	declare	@attn				varchar(80)
	declare	@addressID			int
	declare	@creditor_prefix_1	varchar(80)
	declare	@creditor_prefix_2	varchar(80)
	declare	@house				varchar(80)
	declare	@direction			varchar(80)
	declare	@street				varchar(80)
	declare	@suffix				varchar(80)
	declare	@modifier			varchar(80)
	declare	@modifier_value		varchar(80)
	declare	@address_line_1		varchar(80)
	declare	@address_line_2		varchar(80)
	declare	@address_line_3		varchar(80)
	declare	@address_line_4		varchar(80)
	declare	@city				varchar(80)
	declare	@state				int
	declare	@postalcode			varchar(80)

	-- Find the creditor information
	SELECT	@attn			= attn,
			@AddressID		= AddressID
	FROM	[creditor_addresses] ca WITH (NOLOCK)
	WHERE	[creditor]		= @creditor
	AND		[type]			= @type

	-- Use the payment address if the record is not available
	if (@@rowcount = 0) and (@type <> 'P')
		SELECT	@attn			= attn,
				@AddressID		= AddressID
		FROM	[creditor_addresses] ca WITH (NOLOCK)
		WHERE	[creditor]		= @creditor
		AND		[type]			= 'P'

	-- Read the address record for the creditor
	if @AddressID is not null
		select	@creditor_prefix_1	= creditor_prefix_1,
				@creditor_prefix_2	= creditor_prefix_2,
				@House				= house,
				@Direction			= direction,
				@Street				= street,
				@Suffix				= suffix,
				@modifier			= modifier,
				@modifier_value		= modifier_value,
				@address_line_2		= address_line_2,
				@address_line_3		= address_line_3,
				@city				= city,
				@state				= state,
				@postalcode			= postalcode
		from	Addresses WITH (NOLOCK)
		where	[Address]			= @AddressID;
		
	declare	@result		varchar(256)

	-- If there is an attention line, start with that
	if isnull(@attn,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text ('ATTN: ' + upper(@attn))

	if isnull(@creditor_prefix_1,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@creditor_prefix_1))

	if isnull(@creditor_prefix_2,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@creditor_prefix_2))

	-- Address lines are next
	select	@address_line_1	= dbo.format_address_line_1 ( @house, @direction, @street, @suffix, @modifier, @modifier_value)
	if isnull(@address_line_1,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@address_line_1))

	if isnull(@address_line_2,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@address_line_2))
		
	if isnull(@address_line_3,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@address_line_3))

	-- The last line is the city, state, zipcode
	select	@address_line_4	= dbo.format_city_state_zip (@city, @state, @postalcode)
	if isnull(@address_line_4,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@address_line_4))

	-- Wrap the result in an RTF header and return it to the caller
	select @result = '{\rtf1 ' + @result + ' }'
	return @result
end
GO
