USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_deposit_dates]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[client_deposit_dates]( @client as int, @cutoff_date as datetime, @current_date_time as datetime ) returns varchar(50) as
begin
	-- Return a character string for the expected deposit dates from a client
	declare	@answer	varchar(50)

	-- Translate the current date/time to a date only function
	select	@current_date_time = dbo.date_only ( @current_date_time ),
		@cutoff_date	   = dbo.date_only ( @cutoff_date );

	-- Find the dates that are in the future
	select	@answer = isnull(@answer+' ', '') + convert(varchar(10), deposit_date, 101)
	from	client_deposits with (nolock)
	where	client		= @client
	and	one_time	= 0
	and	deposit_date	>= @cutoff_date
	and	deposit_date	>= @current_date_time
	order by deposit_date;

	-- Include the dates that are in the past
	select	@answer = isnull(@answer+' ', '') + convert(varchar(10), dateadd(month, 1, deposit_date), 101)
	from	client_deposits with (nolock)
	where	client		= @client
	and	one_time	= 0
	and	deposit_date	>= @cutoff_date
	and	deposit_date	< @current_date_time
	order by deposit_date;

	return ( @answer )
end
GO
