USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_region]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_region] ( @region as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	if @region is not null
		select	@answer = description
		from	regions
		where	region = @region
		
	return @answer
end
GO
