USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_bill_cycle]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_bill_cycle] ( @value varchar(10) ) returns varchar(20) as
begin
	declare	@answer varchar(20)

	select	@answer = case isnull(@value,'M')
				when 'I' then 'immediate'
				when 'M' then 'monthly'
				when 'Q' then 'quarterly'
				when 'S' then 'semi-annual'
				when 'A' then 'annual'
				else isnull(@value,'?')
			end

	return ( @answer )
end
GO
