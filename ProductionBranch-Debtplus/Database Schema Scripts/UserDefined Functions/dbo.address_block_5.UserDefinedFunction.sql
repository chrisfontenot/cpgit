USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[address_block_5]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[address_block_5] ( @address1 as varchar(80) = null, @address2 as varchar(80) = null, @address3 as varchar(80) = null, @address4 as varchar(80) = null, @address5 as varchar(80) = null) returns varchar(800) as
begin
    return dbo.address_block_8 (@address1, @address2, @address3, @address4, @address5, default, default, default)
end
GO
