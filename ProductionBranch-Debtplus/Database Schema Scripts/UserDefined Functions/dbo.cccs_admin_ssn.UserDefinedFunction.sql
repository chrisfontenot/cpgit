USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_ssn]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_ssn] ( @input_ssn varchar(40) ) returns varchar(9) as
begin
	declare	@result	varchar(9)

	-- Remove the formatting from the number sequence
	select	@result	= right('0000000000000000000000' + left(replace(replace(@input_ssn,' ',''),'-',''),9),9)

	return ( @result )
end
GO
