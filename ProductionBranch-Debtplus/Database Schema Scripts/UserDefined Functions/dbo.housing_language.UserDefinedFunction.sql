USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_language]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_language] ( @language as int ) returns varchar(15) as
begin
	declare	@answer		varchar(15)
	declare	@hud_language	varchar(80)
	select	@hud_language	= hud_language
	from	languages
	where	language	= @language;
	
	select	@answer	= case @hud_language
				when 'Russian'	then 'RUS'
				when 'French'	then 'FRE'
				when 'German'	then 'GER'
				when 'Japanese'	then 'JPN'
				when 'Korean'	then 'KOR'
				when 'Spanish'	then 'SPA'
				else 'ENG'
			end;
			
	return @answer
end
GO
