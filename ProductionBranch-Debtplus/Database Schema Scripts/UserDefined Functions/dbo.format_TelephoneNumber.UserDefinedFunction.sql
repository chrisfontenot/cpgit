USE [DebtPlus]
GO

/****** Object:  UserDefinedFunction [dbo].[format_TelephoneNumber]    Script Date: 1/19/2016 3:31:10 PM ******/
DROP FUNCTION [dbo].[format_TelephoneNumber]
GO

/****** Object:  UserDefinedFunction [dbo].[format_TelephoneNumber]    Script Date: 1/19/2016 3:31:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[format_TelephoneNumber] ( @TelephoneNumber as int ) returns varchar(80) as
begin
    -- ===================================================================================================
    -- ==           Return the formatted telephone number based upon the country and area code          ==
    -- ===================================================================================================
    declare @answer     varchar(80),
    		@country    int,
    		@acode      varchar(20),
    		@number     varchar(40),
    		@ext        varchar(20)

    -- Retrieve the values from the record
    if @TelephoneNumber is not null
    begin
        select  @country    = country,
                @acode      = acode,
                @number     = number,
                @ext        = ext
        from    TelephoneNumbers with (nolock)
        where   TelephoneNumber = @TelephoneNumber    
    
		-- There should be a country if nothing else. The 'country' does not allow nulls
    	-- If there is no country then use the default item
		if @country is null
			select @country  = min(country)
			from	countries with (nolock)
			where  [default] = 1;
			
		if @country is null
			select	@country = min(country)
			from	countries with (nolock)
   
        -- Retrieve the dialing code for the country for countries other than '1' which is assumed to be USA.
        declare @country_code   int,
				@default		int

        select  @country_code   = ISNULL(country_code, 1),		-- If there is no country code then assume 'USA'   
				@default		= [default]
        from    countries with (nolock)
        where   country         = @country;
            
        -- Telephone numbers for other countries have a country code and city code
        if isnull(@default,1) <> 1
            select  @answer = '+' + convert(varchar,@country_code)
    end
    
	SELECT @Answer = isnull(@answer+' ','')  
				+ CASE WHEN ltrim(rtrim(isnull(@acode,''))) <> ''	THEN '(' + isnull(@acode, '') + ') ' ELSE '' END 
				+ CASE WHEN ltrim(rtrim(isnull(@number,''))) <> ''	THEN @number		ELSE '' END 
				+ CASE WHEN ltrim(rtrim(isnull(@ext,''))) <> ''		THEN ' x ' + @ext	ELSE '' END
     
    -- Return the resulting value   
    return ( @answer )
end

GO