USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[appt_counselor_list]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[appt_counselor_list] ( @appt_time as int ) returns varchar(800) as
begin
	declare	@counselors	varchar(800)
	select	@counselors = isnull(@counselors+',','')+isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'')
	from	appt_counselors a
	inner join counselors co on a.counselor = co.counselor
	left outer join names cox on co.NameID = cox.name
	where	a.appt_time = @appt_time
	return @counselors
end
GO
