USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[BackupDirectory]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[BackupDirectory]() returns varchar(80) as
begin
-- =================================================================================================================
-- ==            Return the directory with the backups                                                            ==
-- =================================================================================================================
	return 'D:\Disbursement Backups'
end
GO
