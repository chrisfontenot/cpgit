USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[NextBankingDay]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[NextBankingDay] ( @dt as datetime ) returns datetime as
begin
	-- =======================================================================================================
	-- ==     Find the next available banking day (non-holiday) from the given date                         ==
	-- =======================================================================================================
	declare	@answer	datetime

	-- Remove the time value from the date/time entry
	select	@dt = convert(datetime, convert(varchar(10), @dt, 101))

	-- Find the indicated date in the calendar table
	select top 1
		@answer = dt
	from	calendar with (nolock)
	where	dt >= @dt
	and	isWeekday = 1
	and	isHoliday = 0
	and	isBankHoliday = 0

	if @answer is null
		select	@answer = @dt

	return ( @answer )
end
GO
