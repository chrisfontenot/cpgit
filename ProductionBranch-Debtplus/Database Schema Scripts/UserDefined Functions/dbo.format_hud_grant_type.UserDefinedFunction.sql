USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_hud_grant_type]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[format_hud_grant_type] ( @grant_type as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	select	@answer	= description
	from	Housing_GrantTypes with (nolock)
	where	oid = @grant_type
	
	return @answer
end
GO
