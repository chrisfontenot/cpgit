USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[budget_suggested_total]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[budget_suggested_total] ( @budget as int ) returns money as
begin
	declare	@total		money
	if @budget is not null
		select	@total		= sum(suggested_amount)
		from	budget_detail
		where	budget		= @budget
	
	return isnull(@total, 0)
end
GO
