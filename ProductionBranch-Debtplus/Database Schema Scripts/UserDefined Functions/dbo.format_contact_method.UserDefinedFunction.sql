USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_contact_method]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_contact_method] (@input as int = 1) RETURNS varchar(50) AS  
BEGIN 
	declare	@answer varchar(50)
	SELECT	@answer = description
	FROM	FirstContactTypes
	WHERE	oID	= @input;

	if @answer is null
		select	@answer	= 'Unspecified'

	return @answer
END
GO
