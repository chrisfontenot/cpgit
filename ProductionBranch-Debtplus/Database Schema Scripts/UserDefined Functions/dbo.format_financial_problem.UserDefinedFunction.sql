USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_financial_problem]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_financial_problem] ( @financial_problem as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	if @financial_problem is not null
		select	@answer = description
		from	financial_problems
		where	financial_problem = @financial_problem
	
	return @answer
end
GO
