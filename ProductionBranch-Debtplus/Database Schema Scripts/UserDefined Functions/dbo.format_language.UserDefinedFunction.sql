USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_language]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_language] ( @language as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	
	select	@answer	= attribute
	from	attributetypes
	where	[oID] = @language
	
	if @answer is null
		select	@answer = attribute
		from	attributetypes
		where	[default]	= 1
		and		[grouping] = 'LANGUAGE'
		
	if @answer is null
		select TOP 1
				@answer = attribute
		from	attributetypes
		where	[grouping] = 'LANGUAGE'
		order by [oID]
	
	return isnull(@answer,'')
end
GO
