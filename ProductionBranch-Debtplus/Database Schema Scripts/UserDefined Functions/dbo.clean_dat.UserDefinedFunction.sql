USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[clean_dat]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[clean_dat] ( @value as varchar(80)) returns datetime as
begin
	declare	@date	varchar(80)
	declare	@time	varchar(80)
	declare	@answer	datetime
	declare	@mon  varchar(4)
	declare	@day  varchar(2)
	declare	@yr   varchar(4)

	select	@answer = null
	if @value <> '.'
	begin
		select	@date = left(@value, 9)
		select	@time = substring(@value, 11, 100)
		select	@mon = substring(@date, 3, 3),
			@day = substring(@date, 1, 2),
			@yr  = right(@date, 4)

		if @mon  = 'JAN'
			select	@mon = '01'
		else if @mon = 'FEB'
			select	@mon = '02'
		else if @mon = 'MAR'
			select	@mon = '03'
		else if @mon = 'APR'
			select	@mon = '04'
		else if @mon = 'MAY'
			select	@mon = '05'
		else if @mon = 'JUN'
			select	@mon = '06'
		else if @mon = 'JUL'
			select	@mon = '07'
		else if @mon = 'AUG'
			select	@mon = '08'
		else if @mon = 'SEP'
			select	@mon = '09'
		else if @mon = 'OCT'
			select	@mon = '10'
		else if @mon = 'NOV'
			select	@mon = '11'
		else if @mon = 'DEC'
			select	@mon = '12'

		select @value  = @mon + '/' + @day + '/' + @yr
		if isdate(@value) <> 0
			select @answer = convert(datetime, @value + ' ' + @time)
	end
	return ( @answer )
end
GO
