USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_HouseHoldHead]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_HouseHoldHead] ( @input AS INT ) RETURNS INT AS
BEGIN
	DECLARE		@answer		INT
	DECLARE		@section	VARCHAR(256)

	SELECT		@section		= [hud_9902_section]
	FROM		HouseHoldHeadTypes WITH (NOLOCK)
	WHERE		oID				= @input

	IF @section IS NOT NULL
		SELECT	@answer			= [id]
		FROM	[housing_arm_referenceinfo] WITH (NOLOCK)
		WHERE	[groupid]		= 34
		AND		[name]			= @section;

	RETURN @answer
END
GO
