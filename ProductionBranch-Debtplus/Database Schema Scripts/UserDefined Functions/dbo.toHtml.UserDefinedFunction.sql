USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[toHtml]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[toHtml](@input as varchar(8000)) returns varchar(8000) as
begin
	declare	@answer		varchar(8000)
	select	@answer		= replace(replace(replace(replace(@input,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;')
	select	@answer		= replace(@answer, char(13) + char(10), '<br/>')

	return ( @answer )
end
GO
