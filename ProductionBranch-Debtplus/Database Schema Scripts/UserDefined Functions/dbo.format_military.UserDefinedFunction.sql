USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_military]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[format_military] (@military_service as int) returns varchar(80) as
begin
	declare	@answer		varchar(80)
	if @military_service is not null
		select	@answer = case @military_service
								when 0 then 'None'
								when 1 then 'Vetran'
								when 2 then 'Active Duty'
						  end

	return ( @answer )
end
GO
