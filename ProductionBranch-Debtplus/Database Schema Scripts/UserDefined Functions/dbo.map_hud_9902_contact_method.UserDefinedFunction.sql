USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_contact_method]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_contact_method] ( @method as varchar(10) ) returns int as
begin
	declare	@answer		int
	declare	@name		varchar(256)

	if @method is not null
	begin
		select	@name	= case ISNULL(@method, 'F')
							when 'P' then 'Phone Counseling'
							when 'F' then 'Face to Face Counseling'
							when 'I' then 'Internet Counseling'
							when '1' then 'Group Counseling'
							when '2' then 'Video Conference'
							when '4' then 'Phone Counseling Only'
									 else 'Other Counseling'
						end
					
		select	@answer = id
		from	housing_ARM_ReferenceInfo
		where	name	= @name
		and		groupId = 30
	end
		
	return isnull(@answer, 6)
end
GO
