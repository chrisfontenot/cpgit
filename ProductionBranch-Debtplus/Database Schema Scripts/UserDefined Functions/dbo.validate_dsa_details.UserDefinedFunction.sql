SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER function [validate_dsa_details]
(
	@dsaprogram	int = null,
	@servicerid	int = null,
	@investorid	int = null
) returns varchar(250)
as
begin

	-- Make the input values non-null
	if @dsaprogram is null
		set @dsaprogram = -1

	if @servicerid is null
		set @servicerid = -1

	if @investorid is null
		set @investorid = -1

	-- DSA Program Constants
	declare @FreddieMacProgram int
	declare @FannieMaeMhnProgram int	
    declare @FannieMaeHpfProgram int
	declare @WellsFargoProgram int
	declare @HPFfmacProgram int

	set @FreddieMacProgram = 301
	set @FannieMaeMhnProgram = 302
	set @FannieMaeHpfProgram = 303    
	set @WellsFargoProgram = 304    
	set @HPFfmacProgram = 305

	-- Referral Code
	declare @CCRCHopeReferralCode int
	declare @HopeFannieMaeReferralCode int
	declare @HopeMilitaryAssistanceReferralCode int
	declare @FreddieMacAffordableServicesReferralCode int
	declare @180DaysDelinquentFMAC  int
	declare @720DaysDelinquentFMAC  int

	set @CCRCHopeReferralCode = 824
	set @HopeFannieMaeReferralCode = 825
	set @HopeMilitaryAssistanceReferralCode = 838
	  
	set @FreddieMacAffordableServicesReferralCode = 822
	set @180DaysDelinquentFMAC = 1366
	set @720DaysDelinquentFMAC = 1367

	-- Wells Fargo Servicer ID
	declare @WellsFargoHomeMortgageServiceID int
    declare @WellsFargoFinancialServiceID int
    declare @WachoviaMortgageServiceID int
	--declare @WellsFargoHomeEquityServiceID int
	declare @WellsFargoBankNAServiceID int
	declare @AscServiceID int

	set @WellsFargoHomeMortgageServiceID = 35
	set @WellsFargoFinancialServiceID = 614
	set @WachoviaMortgageServiceID = 606
	--set @WellsFargoHomeEquityServiceID = 1368
	set @WellsFargoBankNAServiceID = 1476
	set @AscServiceID = 3

	-- Investor
	declare @InvestorFannieMae int
	set @InvestorFannieMae = 2

	declare @InvestorFreddieMac int
	set @InvestorFreddieMac = 3

	declare @message	varchar(250)

	if isnull(@investorid,-1) = -1 
	begin
		set @message = 'Investor cannot be blank'
		return @message
	end
		
	-- validate HPF-FMAC
	if @dsaprogram = @HPFfmacProgram 
	begin
		if @investorid <> -1 and @investorid <> @InvestorFreddieMac
		begin
			set @message = 'For HPF-FMAC, the investor must be Freddie Mac'
		end
	end

	-- validate Freddie Mac
	if @dsaprogram = @FreddieMacProgram
	begin
		if @investorid <> -1 and @investorid <> @InvestorFreddieMac
		begin
			set @message = 'For Freddie Mac, the investor must be Freddie Mac'
		end
	end

	-- validate Fannie Mae MHN
	if @dsaprogram = @FannieMaeMhnProgram 
	begin
		if @investorid <> -1 and @investorid <> @InvestorFannieMae
		begin
			set @message = 'For Fannie Mae � MHN, the investor must be Fannie Mae'
		end
	end

	-- validate Fannie Mae HPF
	if @dsaprogram = @FannieMaeHpfProgram 
	begin
		if @investorid <> -1 and @investorid <> @InvestorFannieMae
		begin
			set @message = 'For Fannie Mae � HPF, the investor must be Fannie Mae'
		end
	end

	-- validate Wells Fargo
	if @dsaprogram = @WellsFargoProgram 
	begin
		if @servicerid = -1  
		begin
			set @message = 'Lender cannot be blank'
			return @message
		end
	
		if @servicerid <> @WellsFargoHomeMortgageServiceID and @servicerid <> @WellsFargoFinancialServiceID and @servicerid <> @WachoviaMortgageServiceID and
		   @servicerid <> @WellsFargoBankNAServiceID and @servicerid <> @AscServiceID 
		begin
			set @message = 'For Wells Fargo, the lender Name field in Housing main tab > Loans sub tab > Intake Lender tab must be one of the following, Wells Fargo Home Mortgage, Wells Fargo Financial, Wells Fargo Home Equity, ASC, Wachovia.'
		end
	end

	return @message
end
GO
