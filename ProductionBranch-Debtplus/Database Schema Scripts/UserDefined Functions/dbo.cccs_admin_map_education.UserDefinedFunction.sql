USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_map_education]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[cccs_admin_map_education]( @input as int ) returns int as
begin
	declare	@answer		int

	select  @answer =	case isnull(@input,0)
							when 1 then 2	-- elementary
							when 2 then 3	-- junion high
							when 3 then 4	-- high school
							when 4 then 5	-- junior college
							when 5 then 6	-- bachlor
							when 6 then 9	-- master
							when 7 then 7	-- doctorate
							else 0			-- other
						end

	if @answer < 1
	begin
		select	@answer = oID from EducationTypes WHERE [default] = 1
		if @answer is null
			select @answer = min(oID) from EducationTypes
		if @answer is null
			select @answer = 1
	end

	return @answer
end
GO
