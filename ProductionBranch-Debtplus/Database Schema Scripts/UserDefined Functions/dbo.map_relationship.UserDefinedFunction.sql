USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_relationship]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_relationship] (@relationship as int) RETURNS varchar(50) AS
BEGIN 
	-- ====================================================================================================
	-- ==       Convert the relationship value in the database to a suitable string                      ==
	-- ====================================================================================================

	declare	@answer	varchar(50)

	if @relationship is not null
		select	@answer	= description
		from	RelationTypes with (nolock)
		where	oID = @relationship

	return isnull(@answer, 'unknown')
END
GO
