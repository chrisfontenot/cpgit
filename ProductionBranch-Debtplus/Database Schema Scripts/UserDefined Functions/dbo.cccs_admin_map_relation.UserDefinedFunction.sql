USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_map_relation]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_map_relation] ( @relation int ) returns int as
begin

	declare	@result	int

	select	@result = case isnull(@relation, -1)
		when 1 then 2		-- spouse
		when 2 then 2		-- coapplicant/spouse
		when 3 then 8		-- coapplicant/parent
		when 4 then 4		-- coapplicant/sibling
		when 5 then 10		-- coapplicant/other relative
		when 6 then 12		-- coapplicant/friend
		when 7 then 12		-- coapplicant/other
		else 12
	end;

	return ( @result )
end
GO
