USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_property_type]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_property_type] ( @housing_type int ) returns varchar(15) as
begin
	declare	@Answer		varchar(15)
	select	@Answer		= case @housing_type
				when 1 then '5'		-- Appartment
				when 2 then '3'		-- Condo
				when 3 then '9'		-- Co-op
				when 4 then '8'		-- Duplex
				when 5 then '8'		-- Halfplex
				when 6 then '6'		-- Prefab
				when 7 then '4'		-- Multi
				when 9 then '1'		-- Single Family
				when 10 then '2'	-- Townhouse
			end;
			
	return ( isnull(@Answer,'5') )
end
GO
