USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[StudentLoanQuestion]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[StudentLoanQuestion]( @oID as varchar(80) ) returns varchar(800) as
begin
	-- Find the question text
	declare	@answer		varchar(800)
	select	@answer	= question
	from	StudentLoanQuestions with (NOLOCK)
	where	oID = @oID

	-- If the question does not exist then use a generic form for the value
	if @answer is null
		select	@answer = 'Question: ' + @oid

	-- Return the answer to the question text
	return @answer
end
GO
