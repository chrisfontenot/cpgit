USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_client_to_last_completed_office_appt]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_client_to_last_completed_office_appt] (@client as int) RETURNS int AS
BEGIN
	declare	@last_date			datetime
	declare	@client_appointment	int

	select	@last_date		= max(start_time)
	from	client_appointments with (nolock)
	where	client			= @client
	and		workshop is null
	and		status in ('K','M','C')

	if @last_date is not null
	begin
		select	@client_appointment	= min(client_appointment)
		from	client_appointments with (nolock)
		where	client			= @client
		and		workshop is null
		and		status in ('K','M','C')
		and		start_time		= @last_date
	end

	return ( @client_appointment )
END
GO
