USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[notes_same_user]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[notes_same_user] (@created_by typ_counselor) RETURNS bit AS  
BEGIN 
	-- =============================================================================================
	-- ==    Answer the question: Is the current user the same as the one presented to us?        ==
	-- ==    The user presented is the person who created the note. It is the user who has        ==
	-- ==    the rights to change the characteristics of the note.                                ==
	-- =============================================================================================

	-- If the current user is "sa" then it is, by definition, the same user.
	if suser_sname() = 'sa'
		return convert(bit,1)

	-- Compare the user names
	if suser_sname() = @created_by
		return convert(bit,1)

	-- This is not the same user
	return convert(bit,0)
END
GO
