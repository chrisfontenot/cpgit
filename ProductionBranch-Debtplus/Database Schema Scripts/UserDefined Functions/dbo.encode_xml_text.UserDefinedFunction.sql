IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='FN' AND NAME=N'encode_xml_text')
	EXEC('CREATE function [dbo].[encode_xml_text] (@input varchar(4000)) returns varchar(4000) as begin end')
GO
ALTER function [dbo].[encode_xml_text] (@input varchar(4000)) returns varchar(4000) as
begin
	if @input is null
		return ''

	set @input = replace(@input, '&', '&amp;')
	set @input = replace(@input, '<', '&lt;')
	set @input = replace(@input, '>', '&gt;')

	return @input
end
GO
