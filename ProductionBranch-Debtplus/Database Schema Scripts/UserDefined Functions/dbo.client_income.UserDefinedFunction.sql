USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_income]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[client_income] ( @client as int ) returns money    as
begin
	declare	@gross		money
	select	@gross	= sum(case when gross_income <= 0 then net_income else gross_income end)
	from	people with (nolock)
	where	client	= @client;

	declare	@assets		money
	select	@assets	= sum(asset_amount)
	from	assets with (nolock)
	where	client	= @client;

	return (isnull(@gross,0) + isnull(@assets,0)) * 12.0
end
GO
