USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[udf_TitleCase]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udf_TitleCase] (@InputString VARCHAR(256) ) RETURNS VARCHAR(256) AS
BEGIN
	DECLARE @Index INT
	DECLARE @Char CHAR(1)
	DECLARE @OutputString VARCHAR(256)
	
	SELECT @OutputString = LOWER(@InputString)
	SELECT @Index = 2
	SELECT @OutputString = STUFF(@OutputString, 1, 1,UPPER(SUBSTRING(@InputString,1,1)))
	
	WHILE @Index <= LEN(@InputString)
	BEGIN
		SELECT @Char = SUBSTRING(@InputString, @Index, 1)
		IF @Char IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&','''','(')
			IF @Index + 1 <= LEN(@InputString)
			BEGIN
				IF @Char != '''' OR UPPER(SUBSTRING(@InputString, @Index + 1, 1)) != 'S'
					SELECT @OutputString = STUFF(@OutputString, @Index + 1, 1,UPPER(SUBSTRING(@InputString, @Index + 1, 1)))
			END
		SELECT @Index = @Index + 1
	END
	RETURN ISNULL(@OutputString,'')
END
GO
