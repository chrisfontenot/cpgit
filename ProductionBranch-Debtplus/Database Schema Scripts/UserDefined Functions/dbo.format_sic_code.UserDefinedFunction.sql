USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_sic_code]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_sic_code] ( @sic_code as varchar(80) ) returns varchar(80) as
begin
	-- ==========================================================================================
	-- ==   Given the internal format for the SIC code, produce the external format            ==
	-- ==========================================================================================

	if @sic_code is null
		return null

	if @sic_code not like '[A-Z][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		return @sic_code

	return left(@sic_code,1) + '-' + substring(@sic_code, 2, 4) + '-' + substring(@sic_code, 6, 2) + '-' + substring(@sic_code, 8, 4)
end
GO
