USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_insert_telephone]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_insert_telephone] ( @telephone as varchar(80) ) returns int as
begin
	declare	@answer		int
	
	select @telephone = dbo.numbers_only(ltrim(rtrim(isnull(@telephone,''))))
	if @telephone = '0000000000'
		select	@telephone = ''
	if @telephone = ''
		select	@telephone = null
		
	if @telephone is not null
	begin
		declare	@acode		varchar(10)
		declare	@ext		varchar(10)
		if len(@telephone) > 7
			select	@acode	= left(@telephone,3),
					@telephone = substring(@telephone, 4, 80)
		
		declare	@country		int
		select	@country	= min(country)
		from	countries
		where	[default]		= 1
		and		[ActiveFlag]	= 1
		
		if @country is null
			select	@country	= min(country)
			from	countries
			where	[default]		= 1

		if @country is null
			select	@country	= min(country)
			from	countries
			
		if @country is null
			select	@country	= 1
			
		execute @answer = xpr_insert_TelephoneNumbers @country, @acode, @telephone, @ext
	end
		
	return ( @answer )
end
GO
