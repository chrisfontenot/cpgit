USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_map_race]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[cccs_admin_map_race] ( @input as int ) returns int as
begin
	declare	@answer		int
	select	@answer	=	case isnull(@input,-1)
							when 1  then 1  -- African-American
							when 2  then 2  -- Asian
							when 3  then 3  -- Caucasian
							when 4  then 3  -- Hispanic
							when 5  then 6  -- Indian
							when 6  then 7  -- American-Indian & African-American
							when 7  then 9  -- American Indian
							when 8  then 11 -- Hawaiian/Pacific Islander
							when 9  then 12 -- American Indian & Caucasian
							when 10 then 13 -- Asian & Caucasian
							when 11 then 14 -- African-American & Caucasian
							when 12 then 10 -- Middle Eastern
							when 13 then 5  -- Other Multi-Racial
							when 14 then 10 -- I prefer not to respond
							else 0
						end

	if @answer < 1
	begin
		select	@answer = oID from RaceTypes WHERE [default] = 1
		if @answer is null
			select @answer = min(oID) from RaceTypes
		if @answer is null
			select @answer = 1
	end

	return @answer
end
GO
