USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_client_names]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_client_names] ( @client as int ) returns varchar(256) as
begin
	declare	@answer			varchar(256)

	declare	@first_1		varchar(80)
	declare	@middle_1		varchar(80)
	declare	@last_1			varchar(80)
	declare	@prefix_1		varchar(80)
	declare	@suffix_1		varchar(80)
	declare	@person_1		int

	select	@person_1		= p.person,
			@first_1		= pn.first,
			@middle_1		= pn.middle,
			@last_1			= pn.last,
			@prefix_1		= pn.prefix,
			@suffix_1		= pn.suffix
	from	people p with (nolock)
	inner join names pn with (nolock) on p.nameid = pn.name
	where	p.client		= @client
	and		p.relation		= 1

	declare	@first_2		varchar(80)
	declare	@middle_2		varchar(80)
	declare	@last_2			varchar(80)
	declare	@prefix_2		varchar(80)
	declare	@suffix_2		varchar(80)
	declare	@person_2		int

	select	@person_2		= p.person,
			@first_2		= pn.first,
			@middle_2		= pn.middle,
			@last_2			= pn.last,
			@prefix_2		= pn.prefix,
			@suffix_2		= pn.suffix
	from	people p with (nolock)
	inner join names pn with (nolock) on p.nameid = pn.name
	where	p.client		= @client
	and		p.relation		<> 1

	if @person_2 is null
		select	@answer	= ltrim(rtrim(dbo.format_normal_name(@prefix_1,@first_1,@middle_1,@last_1,@suffix_1)))
	else begin
		if isnull(@last_1,'') = isnull(@last_2,'')
			select	@answer = ltrim(rtrim(dbo.format_normal_name(@prefix_1,@first_1,@middle_1,default,default))) + ' and ' + ltrim(rtrim(dbo.format_normal_name(@prefix_2,@first_2,@middle_2,@last_1,@suffix_1)))
		else
			select	@answer = ltrim(rtrim(dbo.format_normal_name(@prefix_1,@first_1,@middle_1,@last_1,@suffix_1) + ' and ' + dbo.format_normal_name(@prefix_2,@first_2,@middle_2,@last_2,@suffix_2)))
	end

	return @answer
end
GO
