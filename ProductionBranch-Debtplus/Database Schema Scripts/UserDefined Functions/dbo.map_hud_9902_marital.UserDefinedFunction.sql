USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_marital]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_marital] ( @input as int ) returns int as
begin
	declare	@answer		int
	if @input is not null
	begin
		declare	@name		varchar(80)
		select	@name		= [hud_9902_section]
		from	[maritalTypes] with (nolock)
		where	[oID]		= @input

		if @name is not null
			select	@answer = ID
			from	housing_ARM_ReferenceInfo
			where	groupId = 6
			and		name	= @name
	end
	return isnull(@answer, 1)
end
GO
