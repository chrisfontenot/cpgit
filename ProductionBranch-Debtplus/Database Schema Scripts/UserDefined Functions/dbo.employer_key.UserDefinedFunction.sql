USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[employer_key]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[employer_key](@name varchar(50)) returns char as

-- ======================================================================
-- ==           Translate the employer name field to the suitable      ==
-- ==           key used in the selection form                         ==
-- ======================================================================

begin
	declare @answer char
	if @name is null
		return '?'

	select @answer = upper(substring(@name,1,1))
	if @answer like '[A-Z]'
		return @answer

	return '?'
end
GO
