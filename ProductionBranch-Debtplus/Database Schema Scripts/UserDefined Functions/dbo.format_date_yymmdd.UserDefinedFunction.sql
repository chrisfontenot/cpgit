USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_date_yymmdd]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_date_yymmdd]( @input_date as datetime) returns varchar(6) as
begin
	declare	@month	int
	declare	@day	int
	declare	@year	int

	select	@month	= datepart (m, @input_date),
		@day	= datepart (d, @input_date),
		@year	= datepart (yy,@input_date)

	return ( right ( '00' + convert(varchar, @year), 2) + 
		 right ( '00' + convert(varchar, @month), 2) + 
		 right ( '00' + convert(varchar, @day), 2)
	)
end
GO
