USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[valid_checksum_aba]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[valid_checksum_aba] (@account_number as varchar(9)) returns int as
begin
	-- ===================================================================================================
	-- ==         Validate the ABA checksum on an account number                                        ==
	-- ===================================================================================================

	-- Define the scan function
	declare	@factor		int
	declare	@sum		int
	declare	@column		int
	declare	@digit		int

	select	@column		= 1,
			@factor		= 3,
			@sum		= 0

	-- Process the digits in the columns
	while @column < 10
	begin
		-- Find the digit value
		select	@digit = charindex(substring(@account_number,@column,1), '0123456789') - 1

		-- If the digit is invalid the checksum can not match
		if @digit < 0
			return ( 0 )

		-- scale by the factor.
		select	@digit = @digit * @factor

		-- Accumulate the checksum value
		select	@sum = @sum + @digit,
				@column = @column + 1,
				@factor = case @factor when 3 then 7 when 7 then 1 when 1 then 3 end;

		-- It must be modulo 10
		while @sum >= 10
			select	@sum = @sum - 10
	end

	-- The checksum value is correct if the sum is now zero.
	if @sum = 0
		return ( 1 )

	return ( 0 )
end
GO
