USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[Default_Gender]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[Default_Gender]() returns int as
begin
	-- ===========================================================================================================
	-- ==           Find the default item for the GenderTypes table                                             ==
	-- ===========================================================================================================
	declare	@answer		int

	-- Try for the one marked default
	select	@answer		= min(oID)
	from	GenderTypes with (nolock)
	where	[default]	= 1

	-- Failing that, take the first item in the table
	if @answer is null
		select	@answer		= min(oID)
		from	GenderTypes with (nolock)

	-- NULL is NOT acceptable.
	return isnull(@answer,1)
end
GO
