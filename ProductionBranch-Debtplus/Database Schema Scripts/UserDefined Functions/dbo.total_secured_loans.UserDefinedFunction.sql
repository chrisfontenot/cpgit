USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[total_secured_loans]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[total_secured_loans] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(l.balance)
	from	secured_properties p
	inner join secured_loans l on p.secured_property = l.secured_property
	where	p.client			= @client
	
	return isnull(@answer,0)
end
GO
