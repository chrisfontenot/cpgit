USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_PurposeOfVisit]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_PurposeOfVisit] ( @InterviewType as int ) returns int as
begin
	declare	@answer			int
	declare	@section		varchar(256)

	select	@section		= hud_9902_section
	from	housing_PurposeOfVisitTypes with (nolock)
	where	[oID]		= @InterviewType

	if @section is not null
		select	@answer		= id
		from	housing_arm_referenceinfo with (nolock)
		where	groupid		= 31
		and		name		= @section
		
	return isnull(@answer, 1)
end
GO
