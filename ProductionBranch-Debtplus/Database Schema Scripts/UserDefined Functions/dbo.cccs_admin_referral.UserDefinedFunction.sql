USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_referral]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_referral] ( @input_referral int ) returns int as
begin
    -- Do any selective mapping in this function. However, it seems that the
	-- values are a 1-to-1 match so just provide the hook should they be different.
	-- Just check to ensure that the value exists.

	-- This is just a special translation.
	if @input_referral = 401
		set @input_referral = 578

	if @input_referral is not null and exists(select * from referred_by where referred_by = @input_referral and ActiveFlag = 1)
		return @input_referral

	-- This is the "other" category.
	select @input_referral = min(referred_by) from referred_by where [default] = 1 and [activeflag] = 1

	if @input_referral is null
		select @input_referral = min(referred_by) from referred_by where [default] = 1

	if @input_referral is null
		select @input_referral = min(referred_by) from referred_by where [ActiveFlag] = 1

	if @input_referral is null
		select @input_referral = min(referred_by) from referred_by

	return @input_referral
end
GO
