USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_HudOutcome]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_HudOutcome]( @InterviewType as int, @Result as int ) returns varchar(15) as
begin
	declare	@Answer		varchar(15)
	select	@Answer = m.code
		from	hud_cars_9902_summary s
		inner join hope_hud_outcome_type_code m on s.section_label = m.DebtPlusID
		where	m.code is not null
		and		s.[hud_interview] = @InterviewType
		and		s.[hud_result] = @Result;
	return isnull(@Answer, '114')
end
GO
