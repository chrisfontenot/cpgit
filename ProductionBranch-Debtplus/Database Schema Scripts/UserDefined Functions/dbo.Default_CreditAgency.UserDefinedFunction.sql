USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[Default_CreditAgency]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[Default_CreditAgency]() returns varchar(20) as
begin
	return 'EQUIFAX'
end
GO
