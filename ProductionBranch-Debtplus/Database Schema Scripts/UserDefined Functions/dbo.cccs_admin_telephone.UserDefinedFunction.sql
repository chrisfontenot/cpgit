USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_telephone]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_telephone] ( @phone_number varchar(80) ) returns varchar(14) as
begin

	-- Remove the extra junk from the telephone number
	select @phone_number = ltrim(rtrim(dbo.numbers_only(isnull(@phone_number,''))))
	
	if @phone_number = '0000000000'
		select @phone_number = ''
		
	if @phone_number = ''
		select @phone_number = null

	return ( @phone_number )
end
GO
