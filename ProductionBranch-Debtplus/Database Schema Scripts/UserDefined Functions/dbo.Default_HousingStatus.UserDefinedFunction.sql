USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[Default_HousingStatus]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[Default_HousingStatus]() returns int as
begin
	-- ===========================================================================================================
	-- ==           Find the default item for the HousingStatusTypes table                              ==
	-- ===========================================================================================================
	declare	@answer		int

	-- Try for the one marked default
	select	@answer		= min(oID)
	from	Housing_StatusTypes with (nolock)
	where	[default]	= 1

	-- Failing that, take the first item in the table
	if @answer is null
		select	@answer		= min(oID)
		from	Housing_StatusTypes with (nolock)

	-- NULL is NOT acceptable.
	return isnull(@answer,0)
end
GO
