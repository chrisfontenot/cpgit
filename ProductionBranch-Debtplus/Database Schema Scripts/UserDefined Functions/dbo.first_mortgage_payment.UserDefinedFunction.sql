USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[first_mortgage_payment]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[first_mortgage_payment] ( @client as int ) returns money as
begin

	-- ================================================================================================
	-- ==        Given the client, find the monthly mortgage payment for the reported residence      ==
	-- ================================================================================================
	declare	@answer	money

	select	@answer = sum(d.payment)
	from	housing_loans l
	inner join housing_properties p	on l.PropertyID = p.oID
	inner join housing_loan_details d on l.IntakeDetailID = d.oID
	where	p.Residency			= 1
	and		p.HousingID			= @client
	
	return isnull(@answer,0)
end
GO
