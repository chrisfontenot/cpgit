USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_drop_reason]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_drop_reason] ( @drop_reason as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	select	@answer	= description
	from	drop_reasons
	where	drop_reason = @drop_reason
	
	return @answer
end
GO
