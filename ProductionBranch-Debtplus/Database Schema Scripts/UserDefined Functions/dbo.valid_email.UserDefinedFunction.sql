USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[valid_email]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[valid_email] (@email as varchar(80) = null) RETURNS int AS  
BEGIN 
-- ------------------------------------------------------------------------------------------
-- --       Additional testing to ensure that the email address is somewhat valid          --
-- --                                                                                      --
-- --  Returns:                                                                            --
-- --    0 = invalid address or bad format                                                 --
-- --    1 = acceptable address                                                            --
-- --    2 = null value                                                                    --
-- --    3 = questionable domain name used. probably invalid entry.                        --
-- ------------------------------------------------------------------------------------------

if @email is not null
begin
	select	@email = ltrim(rtrim(@email))
	if @email = ''
		select @email = null
end

-- The address must be defined
if @email is null
	return ( 2 )

-- There must be a seperator
if @email not like '%@%'
	return ( 0 )

-- There must be at most one seperator and not two or more
if @email like '%@%@%'
	return ( 0 )

-- The address must nost start nor end with the seperator
if @email like '%@' or @email like '@%'
	return ( 0 )

-- Finally don't let the bozo try to spoof us
declare	@domain		varchar(80)
declare	@offset		int
select	@offset = charindex('@', @email) + 1

-- Find the domain name for the email address
select	@domain = ltrim(rtrim(substring( @email, @offset, 80)))
if @domain = ''
	return ( 0 )

-- The address must have some valid domain name following it
if @domain not like '[A-Z0-9]%.[A-Z0-9]%'
	return ( 0 )

-- Do not allow the person to use our loopback address. It is never valid.
if @domain like '127.%'
	return ( 0 )

if @domain like 'localhost%'
	return ( 0 )

-- Try to avoid the frustration words that some people use
if @domain like 'fuck%'
	return ( 3 )

if @domain like 'upyours%'
	return ( 3 )

if @domain like 'up.yours%'
	return ( 3 )

-- Try to catch bad user names
if @email like 'fuck%'
	return ( 3 )

-- The address is valid.
return ( 1 )
END
GO
