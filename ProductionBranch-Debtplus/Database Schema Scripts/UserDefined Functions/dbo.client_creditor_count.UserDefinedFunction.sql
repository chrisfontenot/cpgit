USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_creditor_count]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Al Longyear, DebtPlus, L.L.C.
-- Create date: 12/18/2007
-- Description:	Determine the creditor count for a client
-- =============================================
CREATE FUNCTION [dbo].[client_creditor_count] 
(
	@client int
)
RETURNS int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int

	-- Add the T-SQL statements to compute the return value here
	SELECT	@Result = count(*)
	from	client_creditor cc with (nolock)
	left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
	left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
	where	cc.client = @client
	and	cc.reassigned_debt = 0
	and	isnull(ccl.agency_account,0) = 0

	-- Return the result of the function
	RETURN isnull(@Result,0)
END
GO
