USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[remove_pattern]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[remove_pattern] ( @str as varchar(8000), @pattern as varchar(8000), @DeleteLength as int ) returns varchar(8000) as
begin
	-- =================================================================================================
	-- ==          Remove the pattern from the input string                                           ==
	-- =================================================================================================

	Declare	@pos	as int
	Set	@pos = patindex(@pattern, @str)
	While @pos > 0
	Begin
		Set @str = stuff(@str, @pos, @DeleteLength, '')
		Set @pos = patindex(@pattern, @str)
	End

	Return ( @str )
end
GO
