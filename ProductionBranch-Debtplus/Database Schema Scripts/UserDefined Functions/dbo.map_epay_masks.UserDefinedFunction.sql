USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_epay_masks]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_epay_masks] ( @input_mask as varchar(800) ) returns varchar(800) as
begin
	declare	@result		varchar(800)

	select	@result = replace(@input_mask, 'b', ' ')
	select	@result = ltrim(rtrim(@result))

	-- If there is no result then use the NULL string
	if @result = ''
		return ( 'NULL' )

	-- Replace the alphabetic and numeric codes
	select	@result = replace(@result, '[', '\[')
	select	@result = replace(@result, '_', '\_')
	select	@result = replace(@result, 'i', '_')
	select	@result = replace(@result, 'n', '[0-9]')
	select	@result = replace(@result, 'c', '[0-9]')
	select	@result = replace(@result, 'a', '[A-Z]')
	select	@result = replace(@result, 'x', '_')

	return ( @result )
end
GO
