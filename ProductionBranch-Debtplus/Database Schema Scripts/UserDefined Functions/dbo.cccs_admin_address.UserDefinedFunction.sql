USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_address]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[cccs_admin_address] (@address_line as varchar(80)) RETURNS varchar(80) AS  
BEGIN 
	declare	@result	varchar(80)
	select	@result = ltrim(rtrim(@address_line))
	if @result = ''
		select	@result = null
	return ( @result )
END
GO
