USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_zipcode]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_zipcode] ( @zip as varchar(40) ) returns varchar(40) as
begin
	-- =================================================================================================
	-- ==          Return the formatted zipcode for the address                                       ==
	-- =================================================================================================

	-- If the value is null, the answer is null
	if @zip is null
		return null

	-- If the value is zeros then return null
	if @zip in ('000000000', '00000')
		return null

	-- If the zipcode is not 9 digits then just return the zipcode
	if @zip not like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		return @zip

	-- If the last 4 digits are zero then there are only five digits to the zipcode
	if @zip like '[0-9][0-9][0-9][0-9][0-9]0000'
		return left(@zip,5)

	-- Return the zip+4 value
	return left(@zip, 5) + '-' + right(@zip, 4)
end
GO
