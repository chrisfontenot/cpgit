USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[budget_client_total]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[budget_client_total] ( @budget as int ) returns money as
begin
	declare	@total		money
	if @budget is not null
		select	@total		= sum(client_amount)
		from	budget_detail with (nolock)
		where	budget		= @budget
	
	return isnull(@total, 0)
end
GO
