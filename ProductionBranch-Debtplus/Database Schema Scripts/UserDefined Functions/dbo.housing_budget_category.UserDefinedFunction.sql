USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_budget_category]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_budget_category] ( @budget_category as int ) returns int as
begin
	declare	@answer		int
	
	-- Other budget categories are mapped to one for the translation table below
	if @budget_category > 10000
		select	@budget_category = 10000
		
	select	@answer	= case @budget_category
								when 102 then  8
								when 103 then  9
								when 105 then 10
								
								when 104 then 11
								when 109 then 11
								when 358 then 11
								when 359 then 11
								
								when 351 then 12
								when 101 then 13
								when 202 then 14
								when   0 then 15
								when 203 then 16
								when 204 then 17
								when   0 then 18
								when 218 then 19

								when   0 then 20
								when 217 then 21
								when 106 then 22
								when 356 then 23
								when 354 then 24
								when 207 then 25
								when 225 then 26
								when 355 then 27
								when 201 then 28
								when 353 then 29

								when 223 then 30
								when 206 then 31
								when 209 then 32
								
								when 352 then 33
								when 117 then 33
								when 205 then 33
								when 215 then 33
								when 216 then 33
								when 229 then 33
								
								when 116 then 34
								when 212 then 35
								
								when 210 then 36
								when 360 then 36
								when 361 then 36
								when 114 then 36
								when 115 then 36
								when 116 then 36
								
								when 363 then 37
								when   0 then 38
								when 208 then 39
								
								when 213 then 40
								when 108 then 41
								
								when 224 then 42
								when 228 then 42
								
								when 221 then 43
								when 367 then 44
								when 226 then 45
								when 364 then 46
								when 366 then 47
								when 362 then 48
								
								when 219 then 49
								when 220 then 49
								when 222 then 49
								when 227 then 49
								when 240 then 49
								when 230 then 49
								when 241 then 49
								when 365 then 49

								when 113 then 50
								when   0 then 51
								when   0 then 52
								when   0 then 53
								
								when 10000 then 49
								else 0
							end
	
	return ( @answer )
end
GO
