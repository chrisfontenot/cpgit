USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_net_income]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[client_net_income] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(net_income)
	from	people
	where	client			= @client
	
	return isnull(@answer,0)
end
GO
