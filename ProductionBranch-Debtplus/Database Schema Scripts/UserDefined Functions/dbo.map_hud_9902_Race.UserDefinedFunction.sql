USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_Race]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_Race] ( @race int, @Ethnicity int) returns int as
begin
	declare	@answer		int
	declare	@name		varchar(80)
	
	if @race is not null
		select	@name		= hud_9902_section
		from	RaceTypes
		where	oID			= @race
		
	if @Ethnicity = 1
		select	@name		= 'White'
		
	if @name is null
		select	@name		= 'Unknown'

	select	@answer		= id
	from	housing_ARM_ReferenceInfo
	where	[groupId]		= 3
	and		[Name]			= @name
	
	return isnull(@answer, 12)
end
GO
