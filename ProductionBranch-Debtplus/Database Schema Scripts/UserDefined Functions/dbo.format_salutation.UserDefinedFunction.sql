USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_salutation]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_salutation](@client as int) returns varchar(80) as
begin
	-- ChangeLog
	--   4/3/2008
	--      Changed to use relation rather than person since person is going away

	declare	@answer	varchar(80)

	-- Start with the client's salutation
	select	@answer		= [salutation]
	from	clients with (nolock)
	where	[client]		= @client;

	-- If there is no salutation, use the applicant's name
	if isnull(@answer,'') = ''
		select	@answer			= dbo.format_normal_name (n.[prefix], n.[first], n.[middle], n.[last], n.[suffix])
		from	People p with (nolock)
		inner join Names n with (nolock) on p.NameID = n.Name
		where	p.client		= @client
		and		p.relation		= 1;

	return ( @answer )
end
GO
