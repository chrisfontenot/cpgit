USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_phone_number_ext]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_phone_number_ext] ( @phone as varchar(40) = null, @ext as varchar(10) = null ) returns varchar(60) as
begin
	declare	@formatted_phone	varchar(40)
	declare	@formatted_ext		varchar(10)

	-- Convert the fields to the proper format
	select	@formatted_phone = isnull(dbo.format_phone_number ( @phone ),''),
		@formatted_ext	 = ltrim(rtrim(isnull(@ext,'')))

	-- If there is a phone and extension then merge the two fields together
	if (@formatted_phone <> '') and (@formatted_ext <> '')
		return ( @formatted_phone + ' ext. ' + @formatted_ext )

	-- If there is no extension then return just the phone
	if @formatted_phone <> ''
		return ( @formatted_phone )

	-- If there is no phone then return just the extension
	if @formatted_ext <> ''
		return ( 'ext. ' + @formatted_ext )

	-- Return null for both fields missing
	return null
end
GO
