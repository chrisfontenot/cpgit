USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_grant]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_grant] ( @GrantID as int ) returns int as
begin
	declare	@answer		int
	if @GrantID is not null
	begin
		declare	@label	varchar(20)

		select	@label	= hud_9902_section
		from	Housing_GrantTypes
		where	oID		= @GrantID
		
		if @label IS NOT NULL
		begin
			select	@answer	= [id]
			from	housing_ARM_ReferenceInfo
			where	[groupId]	= 19
			and		[Name]		= @label
		end
	end

	return @answer
end
GO
