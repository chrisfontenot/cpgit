USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_Household]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_Household] ( @Client as int ) returns varchar(15) as
begin
	declare	@Answer		varchar(15)
	declare	@marital_status	int
	declare	@dependents		int
	select	@marital_status	= marital_status,
			@dependents 	= dependents
	from	clients
	where	client			= @client;
	
	if @marital_status = 2 -- married
	begin
		if @dependents = 0
			select	@answer	= 'MNODEP'
		else
			select	@answer = 'MWDEP'
	end else begin
		if @dependents = 0
			select	@answer	= 'SINGLE'
		else
			begin
				declare	@gender		int
				select	@gender		= gender
				from	people
				where	client		= @client
				and		relation	= 1
				select	@answer = case isnull(@gender,1)
							when 2 then 'FHOH'
							else		'MHOH'
						end
			end
	end
	
	return ( isnull (@Answer, 'SINGLE') )
end
GO
