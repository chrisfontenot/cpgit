USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[total_secured_value]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[total_secured_value] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(current_value)
	from	secured_properties
	where	client			= @client
	
	return isnull(@answer,0)
end
GO
