USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_creditor_name]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_creditor_name] ( @str as varchar(8000) ) returns varchar(8000) as
begin
	return	dbo.remove_pattern ( @str, '%[^A-Z0-9]%', 1 )
end
GO
