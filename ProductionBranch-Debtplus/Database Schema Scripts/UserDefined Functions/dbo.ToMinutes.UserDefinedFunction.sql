USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[ToMinutes]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ToMinutes]( @hour as int, @minute as int ) returns int as
begin
	declare	@answer		int
	select	@answer = (@hour * 60) + @minute
	return ( @answer )
end
GO
