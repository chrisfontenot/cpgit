USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[encode_html]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[encode_html] ( @input_text as varchar(800) ) returns varchar(800) as
begin
	return ( replace(replace(replace(replace(dbo.remove_pattern ( @input_text, '%[^A-Za-z0-9. &<>]%', 1 ), ' ','+'), '&', '&amp;'),'<', '&lt;'),'>', '&gt;') )
end
GO
