USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_client_address]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_client_address] ( @client as int ) returns varchar(256) as
begin
	-- ======================================================================================================
	-- ==   Format the client address information from the database for a letter.                        ==
	-- ======================================================================================================

	declare	@address1	varchar(80)
	declare	@address2	varchar(80)
	declare	@address3	varchar(80)

	select	@address1	= dbo.format_Address_Line_1 (a.[house], a.[direction], a.[street], a.[suffix], a.[modifier], a.[modifier_value]),
			@address2	= a.[address_line_2],
			@address3	= dbo.format_city_state_zip (a.[city], a.[state], a.[postalcode])
	from	clients c with (nolock)
	left outer join addresses a on c.AddressID = a.Address
	where	c.[client]		= @client

	declare	@result		varchar(256)

	-- Address line 1 is next
	if isnull(@address1,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@address1))

	-- Address line 2 is next
	if isnull(@address2,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@address2))

	-- Address line 3 is next
	if isnull(@address3,'') <> ''
		select	@result	= isnull(@result+'\line ','') + dbo.encode_rtf_text (upper(@address3))

	-- Wrap the result in an RTF header and return it to the caller
	select @result = '{\rtf1 ' + @result + ' }'
	return @result
end
GO
