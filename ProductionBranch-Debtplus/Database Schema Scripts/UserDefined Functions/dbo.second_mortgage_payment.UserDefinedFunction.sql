USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[second_mortgage_payment]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[second_mortgage_payment] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(l.payment)
	from	secured_loans l
	inner join secured_properties p on l.secured_property = p.secured_property
	inner join secured_types t on p.secured_type = t.secured_type
	where	t.auto_home_other	= 'H'
	and		l.priority			= 2
	and		p.client			= @client
	and		p.primary_residence	= 1
	
	return isnull(@answer,0)
end
GO
