USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_Income_Level]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_Income_Level] ( @ami as money = null, @income as money = null ) returns int as
begin
	declare	@Answer			int
	
	-- Start with less than 50 %
	select	@answer		= 2
		
	-- 50 - 79%
	if isnull(@income,0) > isnull(@ami,0) 
		select	@answer	= 3
		
	-- 80 - 100%
	if isnull(@income,0) > isnull(@ami,0) * 1.60 
		select	@answer	= 4
	
	-- over 100%
	if isnull(@income,0) > isnull(@ami,0) * 2.0 
		select	@answer	= 5

	-- Choose not to respond
	if isnull(@income,0) <= 0 or isnull(@ami,0) <= 0
		select	@answer	= 6
		
	return isnull(@answer, 6)
end
GO
