USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_language]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_language](@language as int) returns int as
begin
	declare	@answer		int
	declare	@name		varchar(50)
	
	if @language is not null
		select	@name		= hudlanguage
		from	attributetypes with (nolock)
		where	[Grouping]	= 'LANGUAGE'
		and		oID			= @language
		
	if @name is null
		select @name = 'OTH'
			
	select	@answer = ID
	from	housing_ARM_ReferenceInfo
	where	[groupid]  = 1
	and		[name]	   = @name

	return isnull(@answer, 26)
end
GO
