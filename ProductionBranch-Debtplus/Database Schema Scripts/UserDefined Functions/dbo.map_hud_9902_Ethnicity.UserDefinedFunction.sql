USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_Ethnicity]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_Ethnicity] ( @Ethnicity int, @race int) returns int as
begin
	declare	@answer		int
	declare	@name		varchar(80)

	if @race = 4
		select	@name = 'Hispanic'
	else

		if @Ethnicity is not null
			select	@name		= [hud_9902_section]
			from	[EthnicityTypes] with (nolock)
			where	[oID]		= @Ethnicity
		else
			select	@name		= 'Choose not to respond'

	if @name is not null
		select	@answer = ID
		from	[housing_ARM_ReferenceInfo] with (nolock)
		where	[groupId] = 4
		and		[name]	= @name

	return isnull(@answer, 1)
end
GO
