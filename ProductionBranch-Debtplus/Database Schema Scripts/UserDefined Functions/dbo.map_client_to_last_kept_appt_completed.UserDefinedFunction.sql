USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_client_to_last_kept_appt_completed]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[map_client_to_last_kept_appt_completed] (@client as int) RETURNS int AS
BEGIN 
	declare	@last_date		datetime
	declare	@client_appointment	int

	select	@last_date		= max(start_time)
	from	client_appointments with (nolock)
	where	client			= @client
	and	workshop is null
	and	status in ('K','W')
	and result <> 'is'

	if @last_date is not null
	begin
		select	@client_appointment	= client_appointment
		from	client_appointments with (nolock)
		where	client			= @client
		and	workshop is null
		and	status in ('K','W')
		and result <> 'is'
		and	start_time		= @last_date		
	end

	return ( @client_appointment )
END
GO
