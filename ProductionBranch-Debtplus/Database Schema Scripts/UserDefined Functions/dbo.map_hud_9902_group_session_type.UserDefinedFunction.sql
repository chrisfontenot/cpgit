USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_group_session_type]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_group_session_type] ( @Type as varchar(80) ) returns int as
begin
	declare	@answer		int
	if @Type is not null
	begin
		select	@answer	= [id]
		from	housing_ARM_ReferenceInfo
		where	[groupId]	= 15
		and		[Name]		= @type
	end

	return @answer
end
GO
