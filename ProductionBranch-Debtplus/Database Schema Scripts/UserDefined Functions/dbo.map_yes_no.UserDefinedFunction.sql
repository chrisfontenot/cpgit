USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_yes_no]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_yes_no] ( @value int ) returns varchar(10) as
begin
	if isnull(@value,0) = 0
		return 'NO'
	return 'YES'
end
GO
