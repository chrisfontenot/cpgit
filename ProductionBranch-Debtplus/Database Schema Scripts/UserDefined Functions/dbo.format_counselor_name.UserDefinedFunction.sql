USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_counselor_name]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_counselor_name] (@counselor_name as varchar(80))  
RETURNS varchar(80) AS  
BEGIN 
	-- If the name is missing, return missing
	if @counselor_name is null
		return null

	-- Take the portion to the right of the authority name
	if @counselor_name like '%\%'
		return substring(@counselor_name, charindex('\', @counselor_name) + 1, 80)

	-- There is no authority name. It is local. Use that.
	return @counselor_name
END
GO
