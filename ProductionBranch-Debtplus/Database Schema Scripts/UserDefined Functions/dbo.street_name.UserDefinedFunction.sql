USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[street_name]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[street_name] ( @address varchar(80)) returns varchar(80) as
begin
	select	@address	= ltrim(rtrim(@address))
	declare	@house		varchar(80)
	select	@house	= dbo.house_number ( @address )
	declare	@house_len	int
	select	@house_len = len(@house)
	return ltrim(rtrim(substring(@address, @house_len + 1, 80)))
end
GO
