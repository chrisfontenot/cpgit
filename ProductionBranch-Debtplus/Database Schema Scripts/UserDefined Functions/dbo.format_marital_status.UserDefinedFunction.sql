USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_marital_status]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_marital_status] (@input as int = 1) RETURNS varchar(50) AS  
BEGIN 
	declare	@answer varchar(50)
	select	@answer = description
	from	MaritalTypes
	where	oID		= @input;

	if @answer is null
		select	@answer	= 'Unspecified'

	return @answer
END
GO
