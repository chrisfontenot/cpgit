USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_map_perferred_contact]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_map_perferred_contact] ( @preferred_contact_method varchar(80) = null ) returns int as
begin

	declare	@result	int

	select	@result = case rtrim(ltrim(lower(isnull(@preferred_contact_method, ''))))
		when  'phone' then   2		-- telephone
		when  'email' then   3		-- email
					  else   1		-- unspecified
	end;

	return ( @result )
end
GO
