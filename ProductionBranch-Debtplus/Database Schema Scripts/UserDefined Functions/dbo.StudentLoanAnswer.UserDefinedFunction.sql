USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[StudentLoanAnswer]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[StudentLoanAnswer]( @QuestionID as varchar(80), @AnswerID as varchar(80) ) returns varchar(800) as
begin
	-- Find the question text
	declare	@answer		varchar(800)
	select	@answer	= isnull(answers,'') + ','
	from	StudentLoanQuestions with (NOLOCK)
	where	oID = @QuestionID

	-- If there is no valid response to the item then just return the invalid code
	if @answer is null
		return 'response = ' + @answerID

	-- Try to parse the answers into the appropriate groups. Each group has a comma.
	-- Each group then has the answer id followed by an equal sign and the text
	declare	@group		varchar(800)
	declare	@ipos		int

	while 1 = 1
	begin
		select	@ipos = charindex(',', @answer)
		if @ipos <= 0 or @ipos > len(@answer)
			break

		declare	@subfield	varchar(800)
		select	@subfield	= substring(@answer, 1, @ipos - 1),
				@answer		= substring(@answer, @ipos + 1, 800)

		if @subfield like @AnswerID + '=%'
		begin
			select	@answer = substring(@subfield, len(@AnswerID) + 2, 800)
			return @answer
		end
	end

	return 'response = ' + @answerID
end
GO
