USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_problem]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_problem] ( @input_problem int ) returns int as
begin
	declare	@answer	int
	select	@answer		= case isnull(@input_problem, 0)
		when  1 then  35	-- Trouble managing money
		when  2 then  37	-- Reduced income
		when  5 then  53	-- Death of a family member
		when  6 then  97	-- Unexpected medical expense/injury/illness
		when  7 then  55	-- Planned medical expense/surgery/pregnancy
		when  8 then  38	-- Divorce
		when  9 then 121	-- Separation
		when 10 then  42	-- Payment Delinquencies
		when 11 then  45	-- Increased home/living/auto expenses
		when 12 then  87	-- Substance abuse
		when 13 then  40	-- Unemployment
		when 14 then 115	-- Fraud/Identity theft victim
		when 15 then 118	-- Bankruptcy
		when 16 then 143	-- Delinquent income taxes
		when 17 then 144	-- Increased home interest rate (ARM)
		when 18 then  63	-- Care for a family member/friend
		when 19 then  67	-- Gambling
		when 20 then  47	-- Other
		else 0
	end

	if @answer < 1
	begin
		select	@answer = financial_problem from financial_problems WHERE [default] = 1
		if @answer is null
			select @answer = min(financial_problem) from financial_problems
		if @answer is null
			select @answer = 1
	end
	return ( @answer )
end
GO
