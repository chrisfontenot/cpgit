USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[total_payments]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[total_payments] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer	= sum(b.total_payments)
	from	client_creditor_balances b
	inner join client_creditor cc on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
	where	cc.client = @client
	and		cc.reassigned_debt = 0

	return isnull(@answer,0)
end
GO
