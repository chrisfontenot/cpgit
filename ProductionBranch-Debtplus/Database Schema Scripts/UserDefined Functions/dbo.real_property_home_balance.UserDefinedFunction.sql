USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[real_property_home_balance]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[real_property_home_balance] ( @client as int ) returns money as
begin
	declare	@answer	money

	select	@answer = sum(l.CurrentLoanBalanceAmt)
	from	housing_properties p
	inner join housing_loans l on p.oID = l.PropertyID
	where	l.Loan1st2nd = 1
	and		p.Residency in (1, 2, 3)
	and		p.HousingID = @client
	
	return isnull(@answer,0)
end
GO
