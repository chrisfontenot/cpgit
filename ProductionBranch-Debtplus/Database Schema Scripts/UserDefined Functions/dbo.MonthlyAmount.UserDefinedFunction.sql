USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[MonthlyAmount]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
create function [dbo].[MonthlyAmount](@DollarAmount as money = 0, @Frequency as int = 4) returns money as
begin
	declare	@answer		Decimal(18,2)
	declare	@periods	int

	select	@periods = [PeriodsPerYear]
	from	PayFrequencyTypes with (nolock)
	where	[oID]	= @Frequency

	select	@answer = ( @DollarAmount * isnull(@Periods,12) ) / 12
	return convert(money, @Answer)
end
GO
