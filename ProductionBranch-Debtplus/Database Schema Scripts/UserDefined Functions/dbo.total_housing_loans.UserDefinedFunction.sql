USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[total_housing_loans]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[total_housing_loans] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(l.CurrentLoanBalanceAmt)
	from	housing_properties p
	inner join housing_loans l on p.oID = l.PropertyID
	inner join client_housing ch on p.HousingID = ch.client
	where	ch.client			= @client

	return isnull(@answer,0)
end
GO
