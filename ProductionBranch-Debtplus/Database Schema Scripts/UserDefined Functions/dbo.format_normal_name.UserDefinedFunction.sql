USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_normal_name]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_normal_name] (@prefix as varchar(10) = null, @first as varchar(80) = null, @middle as varchar(40) = null, @last as varchar(80) = null, @suffix as varchar(10) = null) RETURNS varchar(256) AS
BEGIN

-- ==============================================================================================================
-- ==           Format the normal name notation                                                                ==
-- ==============================================================================================================

	declare  @result	varchar(256)
	select	 @result = ''

	-- Prepend the suffix name with a leading comma
	if not @suffix is null
	begin
		select @suffix = ltrim(rtrim(@suffix))
		if @suffix <> ''
			select @result = ', ' + @suffix
	end

	-- Prepend the last name
	if not @last is null
	begin
		select @last = ltrim(rtrim(@last))
		if @last <> ''
			select @result = @last + @result
	end

	-- Prepend the middle name
	if not @middle is null
	begin
		select @middle = ltrim(rtrim(@middle))
		if @middle <> ''
		begin
			-- If the middle is one character, it is an initial. Add the period.
			if len(@middle) = 1
				select	@middle = @middle + '.'
			select @result = @middle + ' ' + @result
		end
	end

	-- Prepend the first name
	if not @first is null
	begin
		select @first = ltrim(rtrim(@first))
		if @first <> ''
			select @result = @first + ' ' + @result
	end

	-- Prepend the prefix value
	if not @prefix is null
	begin
		select @prefix = ltrim(rtrim(@prefix))
		if @prefix <> ''
			select	@result = @prefix + ' ' + @result
	end

	-- If there is only the suffix then we need to remove the leading comma
	if left(@result,2) = ', '
		select @result = substring(@result, 3, 256)

	return ( @result )
END
GO
