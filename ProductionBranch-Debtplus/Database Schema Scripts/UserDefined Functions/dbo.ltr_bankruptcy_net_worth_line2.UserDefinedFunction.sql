USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[ltr_bankruptcy_net_worth_line2]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ltr_bankruptcy_net_worth_line2] (@row as int, @text1 as varchar(80) = '', @text2 as varchar(80) = '', @value2 as money = null ) returns varchar(8000) as
begin
	declare	@stmt		varchar(8000)

	select	@stmt =         '\irow' + convert(varchar, @row) + '\irowband' +  convert(varchar, @row) + '\trgaph30\trleft0\trftsWidth1\trftsWidthB3\trftsWidthA3\trautofit1\trpaddl30\trpaddr30\trpaddfl3\trpaddft3\trpaddfr3\trpaddfb3\clvertalt\clbrdrl\brdrs\brdrw15\clbrdrt\brdrs'
	select	@stmt = @stmt + '\brdrw15\clbrdrb\brdrs\brdrw15\clftsWidth3\clwWidth3422\cellx3422\clvertalt\clbrdrt\brdrs\brdrw15\clbrdrr\brdrs\brdrw15\clbrdrb\brdrs\brdrw15\clftsWidth3\clwWidth1419\cellx4841\clvertalt\clbrdrl\brdrs\brdrw15\clbrdrr\brdrs\brdrw15\clftsWidth3'
	select	@stmt = @stmt + '\clwWidth141\cellx4982\clvertalt\clbrdrl\brdrs\brdrw15\clbrdrt\brdrs\brdrw15\clbrdrr\brdrs\brdrw15\clbrdrb\brdrs\brdrw15\clftsWidth3\clwWidth4133\cellx9115\clvertalt\clbrdrl\brdrs\brdrw15\clbrdrt\brdrs\brdrw15\clbrdrr\brdrs\brdrw15\clbrdrb\brdrs'
	select	@stmt = @stmt + '\brdrw15\clftsWidth3\clwWidth1419\cellx10534\pard\intbl\qc\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\tx9360\tx10080\plain\f1\fs20\b\cf3 '
	select	@stmt = @stmt + @text1
	select	@stmt = @stmt + '\cell\cell\pard\intbl\qr\tx720\tx1440'
	select	@stmt = @stmt + '\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\tx9360\tx10080\plain\f0\fs20\cf3\cell\pard\intbl\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\tx9360\tx10080\plain\f1\fs20\cf3 '
	select	@stmt = @stmt + @text2
	select	@stmt = @stmt + '\cell'
	select	@stmt = @stmt + '\pard\intbl\qr\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\tx9360\tx10080 '

	if @value2 is not null
		select	@stmt = @stmt + '$' + convert(varchar, @value2, 1)

	select	@stmt = @stmt + '\cell\row\trowd'
	return @stmt
end
GO
