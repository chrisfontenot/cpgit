USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[total_assets]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[total_assets] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(asset_amount)
	from	assets
	where	client			= @client
	
	return isnull(@answer,0)
end
GO
