USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[total_housing_payments]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[total_housing_payments] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = sum(d.Payment)
	from	housing_properties p
	inner join housing_loans l on p.oID = l.PropertyID
	inner join housing_loan_details d on l.IntakeDetailID = d.oID
	inner join client_housing ch on p.HousingID = ch.client
	where	ch.client			= @client

	return isnull(@answer,0)
end
GO
