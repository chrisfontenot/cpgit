USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_hud_9902_postalcode]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_hud_9902_postalcode] ( @PostalCode as varchar(80)) returns varchar(10) as
begin
	declare	@answer		varchar(10)
	if @postalcode is not null
	begin
		select	@answer	= dbo.numbers_only ( @PostalCode )
		if @answer like '[0-9][0-9][0-9][0-9][0-9]%'
			select	@answer = left(@answer, 5)
	end
	return right ('00000' + isnull(@answer,''), 5)
end
GO
