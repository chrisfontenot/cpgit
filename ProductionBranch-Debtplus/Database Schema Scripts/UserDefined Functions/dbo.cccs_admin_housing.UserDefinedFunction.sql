USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_housing]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[cccs_admin_housing] ( @input_housing varchar(40) ) returns int as
begin
	declare	@result	int

	-- Translate the housing status code
	select	@result = case isnull(@input_housing,'?')
				when 'B' then 6 -- Buying
				when 'R' then 8 -- Renting
				when 'H' then 6 -- Homeowner w/Mortgage
				when 'O' then 4 -- Other
			                 else 4 -- Unknown
			end

	return ( @result )
end
GO
