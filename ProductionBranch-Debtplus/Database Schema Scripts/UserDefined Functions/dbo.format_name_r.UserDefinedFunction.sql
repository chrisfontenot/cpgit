USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_name_r]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_name_r](@name int) returns varchar(256) as
begin
    declare @prefix varchar(80)
    declare @first  varchar(80)
    declare @middle varchar(80)
    declare @last   varchar(80)
    declare @suffix varchar(80)
    declare @answer varchar(256)

    select  @answer = ''
    if @name is not null
    begin
        select  @prefix = prefix,
                @first  = first,
                @middle = middle,
                @last   = last,
                @suffix = suffix
        from    names with (nolock)
        where   name    = @name;
        
        select  @answer = dbo.format_reverse_name(@prefix, @first, @middle, @last, @suffix);
    end
    
    return (isnull(ltrim(rtrim(@answer)),''))
end
GO
