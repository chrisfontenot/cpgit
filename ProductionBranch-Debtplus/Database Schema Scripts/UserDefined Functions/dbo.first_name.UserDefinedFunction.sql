USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[first_name]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[first_name] ( @name as varchar(800) ) returns varchar(800) as
begin
	declare	@split	int
	select	@name	= ltrim(rtrim(@name))
	select	@split	= patindex('% %', @name)
	if @split > 0
		select	@name = rtrim(substring(@name, 1, @split))

	return ( @name )
end
GO
