USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[ami]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ami] ( @client as int ) returns money as
begin
	declare	@answer			money

	-- Find the size of the family for this client. The tables are based upon
	-- the size of the family, not dependents.
	declare	@family_size	int
	select	@family_size	= dbo.family_size ( @client )
	
	-- Start with a family of 4 people for the median income from the tables
	select	@answer	= co.median_income
	from	counties co with (nolock)
	inner join clients c with (nolock) on co.county = c.county
	where	c.client		= @client

	if @answer is null
		select	@answer	= median_income
		from	counties with (nolock)
		where	county	= 1;

	if @answer is null
		select	@answer = 0;

	-- Adjust the amount based upon the size of the family
	if @family_size < 2
		select	@answer = @answer * 0.70;

	if @family_size = 2
		select	@answer = @answer * 0.80;

	if @family_size = 3
		select	@answer = @answer * 0.90;

	if @family_size > 4
		select	@answer	= convert(decimal(20,2), convert(float, @answer) * (1.0 + (0.08 * convert(float, @family_size - 4))));

	return @answer
end
GO
