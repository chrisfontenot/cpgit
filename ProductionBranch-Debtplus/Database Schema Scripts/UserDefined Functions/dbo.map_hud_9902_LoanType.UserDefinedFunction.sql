USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_LoanType]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_LoanType] ( @Value as int ) returns int as
begin
	declare		@answer		int
	declare		@key		varchar(80)
	
	if @Value is not null
		select	@key	= hud_9902_section
		from	Housing_LoanTypes
		where	oID		= @Value
		
	if @key is null
		select	@key		= 'Client did not disclose'
		
	select	@answer		= ID
	from	housing_ARM_ReferenceInfo
	where	groupId		= 35
	and		name		= @key
	
	return isnull ( @answer , 9)
end
GO
