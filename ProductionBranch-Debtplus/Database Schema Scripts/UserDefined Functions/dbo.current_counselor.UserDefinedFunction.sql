USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[current_counselor]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[current_counselor]() returns int as
begin
	declare	@counselor		int
	select	@counselor = counselor
	from	Counselors
	where	[Person]	= SUSER_SNAME()

	if @counselor is null AND SUSER_SNAME() = 'sa'
		select	@counselor = 0
	
	return ( @counselor )
end
GO
