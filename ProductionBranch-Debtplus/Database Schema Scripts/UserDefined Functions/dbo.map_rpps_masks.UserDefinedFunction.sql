USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_rpps_masks]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_rpps_masks] ( @input_mask as varchar(800) ) returns varchar(800) as
begin
	declare	@result		varchar(800)
	select	@result = upper(ltrim(rtrim(isnull(@input_mask,''))))

	-- If there is no result then use the NULL string
	if @result = ''
		return ( 'NULL' )

	-- Replace the alphabetic and numeric codes
	select	@result = replace(replace(replace(replace(@result,'[','\['), '#', '[0-9]'), 'A', '[A-Z]'), '@', '_')

	return ( @result )
end
GO
