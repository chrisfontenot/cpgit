USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_minutes]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_minutes] (@hud_interview as int) returns int as
BEGIN
	declare	@answer			int
	if @hud_interview is not null
	begin
		select	@answer	= SUM(minutes)
		from	hud_transactions
		where	hud_interview = @hud_interview
		
		select	@answer = ISNULL(@answer,0)
	end
	
	return ( @answer )
END
GO
