USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_client_to_budget]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_client_to_budget] (@client as int) RETURNS int AS
BEGIN 
	-- ===============================================================================================
	-- ==           Find the latest budget for the indicated client.                                ==
	-- ===============================================================================================

	declare	@date_created		datetime
	declare	@budget_id		int

	-- Find the latest budget created for this client
	select	@date_created = max(date_created)
	from	budgets with (nolock)
	where	client = @client

	-- From the latest date, choose the latest budget
	select	@budget_id	= budget
	from	budgets with (nolock)
	where	client		= @client
	and	date_created	= @date_created

	-- If there is no budget, use 0.
	if @budget_id is null
		select	@budget_id = 0

	return ( @budget_id )
END
GO
