USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_gender]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_gender] ( @Gender as int ) returns varchar(15) as
begin
	DECLARE @Answer varchar(15)
	
	IF @Gender IS NOT NULL
	BEGIN
		SELECT @Answer = 'MALE'
		IF @Gender = 2
			SELECT @Answer = 'FEMALE'
	END

	RETURN ( @Answer )
end
GO
