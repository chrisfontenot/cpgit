USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_CaseSource]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_CaseSource] ( @referred_by as int ) returns varchar(15) as
begin
	declare	@Answer		varchar(15)
	select	@Answer		= case isnull(@referred_by,-1)
								when  1 then 'REFERRAL'		-- friends / relative
								when  2 then 'REFERRAL'		-- retailers
								when  3 then 'REFERRAL'		-- banks
								when  4 then 'REFERRAL'		-- finance companies
								when  5 then 'REFERRAL'		-- phonebook
								when  6 then 'REFERRAL'		-- employer
								when  7 then 'REFERRAL'		-- client referral
								when  8 then 'REFERRAL'		-- collection agency
								when  9 then 'REFERRAL'		-- credit union
								when 10 then 'REFERRAL'		-- navy relief
								when 11 then 'REFERRAL'		-- family service center
								when 12 then 'REFERRAL'		-- publicity and advertising
								when 13 then 'OTHER'		-- other
								when 14 then 'REFERRAL'		-- former client
								when 15 then 'REFERRAL'		-- attorney
								when 16 then 'REFERRAL'		-- better business bureau
								when 17 then 'REFERRAL'		-- mcchord afb
								when 18 then 'REFERRAL'		-- legal aid
								when 19 then 'REFERRAL'		-- chapter 13
								when 20 then 'REFERRAL'		-- aer
								when 21 then 'UTILITYBILL'	-- tacoma public utilities
								when 22 then 'REFERRAL'		-- fort lewis
								when 23 then 'REFERRAL'		-- navy
								when 24 then 'REFERRAL'		-- mortgage company
								when 25 then 'INTERNET'		-- internet
								when 26 then 'COUNSELORREFER'		-- cccs workshop
								when 27 then 'OTHER'		-- testing
								else 'OTHER'
						end;
						
	return ( isnull ( @Answer, 'OTHER' ))
end
GO
