USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_state]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_state] ( @state as int ) returns int as
begin
	declare	@answer			int
	declare	@mailingcode	varchar(10)
	
	if @state is not null
	begin
		select	@mailingcode	= mailingcode
		from	states
		where	state		= @state
		
		if @mailingcode is not null
			select	@answer	= id
			from	housing_ARM_ReferenceInfo
			where	groupid = 0
			and		shortdesc = left(@mailingcode,2)
	end
	return isnull(@answer, 61)
end
GO
