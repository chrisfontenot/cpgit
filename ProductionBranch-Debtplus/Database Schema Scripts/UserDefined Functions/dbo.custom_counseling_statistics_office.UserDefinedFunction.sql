USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[custom_counseling_statistics_office]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[custom_counseling_statistics_office] ( @office as int ) returns int as

-- Used in the procedure 'custom_counseling_statistics' to provide the monthly summary extract
-- for Orange County

begin
	declare	@answer		int

	-- Process non-null offices
	if @office is null
	begin
		select	@office = office from offices where [default] = 1 and [activeflag] = 1;
		if @office is null
			select	@office = min(office) from offices where [ActiveFlag] = 1;
		if @office is null
			select	@office = 1
	end
	
	if @office = 1 -- main office santa ana
		select	@answer = 1
	else if @office = 2 -- brea - oc residents only
		select	@answer = 3
	else if @office = 4 -- anaheim
		select	@answer = 2
	else if @office = 6 -- costa mesa
		select	@answer = 5
	else if @office = 7 -- fullterton
		select	@answer = 3
	else if @office = 8 -- huntington beach
		select	@answer = 5
	else if @office = 9 -- santa ana/octfcu
		select	@answer = 1
	else if @office = 14 -- internet
		select	@answer = 8
	else if @office = 15 -- mission viejo
		select	@answer = 6
	else if @office = 17 -- reverse mortgage phone satellite
		select	@answer = 11
	else if @office = 18 -- bankruptcy phone satellite
		select	@answer = 10
	else if @office = 19 -- disneyland employees
		select	@answer = 1
	else if @office = 20 -- lake forest
		select	@answer = 7
	else if @office = 22 -- fntn vly - no handicap
		select	@answer = 5
	else if @office = 23 -- laguna hills
		select	@answer = 6
	else if @office = 24 -- csulb - students only
		select	@answer = 4

	return isnull(@answer,1)
end
GO
