USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[second_mortgage_balance]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[second_mortgage_balance] ( @client as int ) returns money as
begin
	declare	@answer	money

	select	@answer = sum(CurrentLoanBalanceAmt)
	from	Housing_Loans l
	inner join Housing_Properties p on l.PropertyID = p.oID
	where	p.UseInReports = 1
	and		l.Loan1st2nd   = 2
	and		p.HousingId	   = @client
	
	return isnull(@answer,0)
end
GO
