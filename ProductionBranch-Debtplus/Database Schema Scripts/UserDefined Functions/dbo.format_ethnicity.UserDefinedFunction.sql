USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_ethnicity]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[format_ethnicity] ( @ethnicity as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)

	select	@answer	= description
	from	EthnicityTypes with (nolock)
	where	oID = @ethnicity

	return isnull(@answer, 'Not Specified')
end
GO
