USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[housing_asset_category]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[housing_asset_category] ( @asset_id as int ) returns int as
begin
	declare	@answer		int
	select	@answer =   case @asset_id
							when 1 then 3
							when 3 then 4
							when 5 then 5
							when 6 then 7
							else 6
						end;
	return ( @answer )
end
GO
