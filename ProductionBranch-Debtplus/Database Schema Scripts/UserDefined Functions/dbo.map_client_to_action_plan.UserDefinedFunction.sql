USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_client_to_action_plan]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_client_to_action_plan] ( @client as int ) returns int as
BEGIN
-- ==================================================================================================
-- ==           Return the action plan for the client                                              ==
-- ==================================================================================================

	declare @action_plan	int
	select	@action_plan = action_plan
	from	action_plans
	where	client		 = @client
	order by date_created

	return isnull(@action_plan,0)
END
GO
