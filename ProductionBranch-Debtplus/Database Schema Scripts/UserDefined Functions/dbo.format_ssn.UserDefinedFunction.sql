USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_ssn]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_ssn] ( @ssn as varchar(40) ) returns varchar(40) as
begin
	-- =================================================================================================
	-- ==          Return the formatted social security number                                        ==
	-- =================================================================================================

	-- If the value is null, the answer is null
	if @ssn is null
		return null

	-- If the value is zeros then return null
	if @ssn = '000000000'
		return null

	-- If the zipcode is not 9 digits then just return the value unchanged
	if @ssn not like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		return @ssn

	-- Return the formatted value
	return left(@ssn, 3) + '-' + substring(@ssn, 4, 2) + '-' + right(@ssn, 4)
end
GO
