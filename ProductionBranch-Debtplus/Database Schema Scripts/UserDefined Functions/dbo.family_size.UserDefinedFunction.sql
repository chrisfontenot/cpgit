USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[family_size]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[family_size] ( @client as int ) returns int as
begin
	-- ===================================================================================================
	-- ==      Based upon the household status and dependents, find the number of people in household   ==
	-- ===================================================================================================
	declare	@answer				int
	declare	@household			int
	declare	@dependents			int

	select	@dependents			= dependents,
			@household			= household
	from	clients with (nolock)
	where	client				= @client

	select	@answer				= case @household
									when 1 then 1				-- single
									when 4 then 2				-- married without dependents
									when 5 then @dependents + 2	-- married with dependents
									when 6 then 2				-- two or more unrelated adults
									when 7 then @dependents + 1	-- other
									when 2 then @dependents + 1	-- femaled headed household
									when 3 then @dependents + 1	-- maled headed household
									else @dependents + 1		-- something else
								  end

	return isnull(@answer, 1)
end
GO
