USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_monthly_housing_expenses]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[client_monthly_housing_expenses] (@client as int) returns money as
begin
	declare @answer		money
	declare @budget		int
	select	@budget = dbo.map_client_to_budget(@client)
	if @budget is not null
	begin
		select @answer = sum(bd.suggested_amount)
		from	budget_detail bd
		inner join budget_categories bc on bd.budget_category = bc.budget_category
		where	bc.expensecode = 'H'
		and		bd.budget = @budget
	end

	return isnull(@answer, 0)
end
GO
