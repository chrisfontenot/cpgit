USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_other_income]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[client_other_income] ( @client as int ) returns money as
begin
	declare	@assets		money
	select	@assets	= sum(asset_amount)
	from	assets with (nolock)
	where	client	= @client;

	return isnull(@assets,0) * 12.0
end
GO
