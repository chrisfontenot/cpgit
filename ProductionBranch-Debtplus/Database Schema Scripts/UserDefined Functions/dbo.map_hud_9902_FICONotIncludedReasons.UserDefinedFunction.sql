USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_FICONotIncludedReasons]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_FICONotIncludedReasons] ( @input as int) returns int as
begin
	declare	@answer		int
	declare	@name		varchar(50)
	
	if @input is not null
		select	@name		= hud_9902_section
		from	Housing_FICONotIncludedReasons WITH (NOLOCK)
		where	oID			= @input

	if @name is null
		select @name = 'NFMC Counseling Organization does not analyze credit report for this level of se'
			
	select	@answer = ID
	from	housing_ARM_ReferenceInfo
	where	[groupid]  = 33
	and		left([name],50)	= left(@name,50)

	return isnull(@answer, 6)
end
GO
