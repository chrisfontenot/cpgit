USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[invoice_aging_months]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[invoice_aging_months](@inv_date datetime, @current_date datetime) returns int as
begin
	-- Reduce the dates to the component pieces
	declare	@inv_year		int
	declare	@inv_month		int
	declare	@current_year	int
	declare	@current_month	int

	set		@inv_year		= datepart(yy, @inv_date)
	set		@inv_month		= datepart(m,  @inv_date)
	set		@current_year	= datepart(yy, @current_date)
	set		@current_month	= datepart(m,  @current_date)

	-- Compute the number of months between the two figures
	declare	@months			int
	set		@months = ((@current_year - @inv_year) * 12) + (@current_month - @inv_month)

	-- Negative values are invalid
	if @months < 0
		set	@months = 0

	return @months
end
GO
