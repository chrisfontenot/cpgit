USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[encode_rtf_text]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[encode_rtf_text] (@text_string as varchar(4000) = null) RETURNS varchar(6000) AS
BEGIN
	-- =====================================================================================================
	-- ==              Encode the input string to a suitable RTF text string.                             ==
	-- =====================================================================================================
	select	@text_string = replace(@text_string, '\', '\\')		-- first, escape the escape code
	select	@text_string = replace(@text_string, '{', '\{')		-- next open and close braces
	select	@text_string = replace(@text_string, '}', '\}')
	return (@text_string)
END
GO
