﻿-- Batch submitted through debugger: SQLQuery85.sql|7|0|C:\Users\vnair\AppData\Local\Temp\4\~vs6EA8.sql

alter function [dbo].[get_dsa_disclosure_detail]
(
	@clientid	int
)
returns int
as
begin

	declare @recordcount	int

-- Referral Code
	declare @CCRCHopeReferralCode int
	declare @HopeFannieMaeReferralCode int
	declare @HopeMilitaryAssistanceReferralCode int

	set @CCRCHopeReferralCode = 824
	set @HopeFannieMaeReferralCode = 825
	set @HopeMilitaryAssistanceReferralCode = 838

	declare @referredby  int

	select top(1) @referredby = referred_by 
	  from client_appointments 
     where client = @clientid 
	 --and status in ('P', 'W')
     order by date_created desc

	
--disclosures are mandatory only if the referral code is  one of the above.

	if @referredby is null
	begin
		set @recordcount = 0
	end


	if  @referredby = @CCRCHopeReferralCode or @referredby = @HopeFannieMaeReferralCode or @referredby = @HopeMilitaryAssistanceReferralCode
	begin

		if not exists (select top(1) 1 
						from client_disclosures cd inner join dbo.client_DisclosureLanguageTypes cdl
						on cd.disclosureID = cdl.oID
					   where cdl.clientDisclosureTypeID = 5 
						and cd.client = @clientid
						order by cd.date_created desc)
		begin
			-- Need Authorization to Share Data Disclosure
			set @recordcount = -1
		end
		else
		begin

			-- Authorization to Share Data was not authorized
			select top(1) @recordcount = 1
			 from client_disclosures cd inner join dbo.client_DisclosureLanguageTypes cdl
			on cd.disclosureID = cdl.oID
			where cdl.clientDisclosureTypeID = 5 
			and cd.client = @clientid
			and (cd.applicant < 2 or cd.coapplicant < 2 ) 
	  		order by cd.date_created desc

		end	
	end
	return isnull(@recordcount, 0)
end