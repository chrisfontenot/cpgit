USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[total_taxes]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[total_taxes] ( @client as int ) returns money as
begin
	declare	@answer	money
	select	@answer = isnull(fed_tax_owed,0) + isnull(state_tax_owed,0) + isnull(local_tax_owed,0)
	from	clients
	where	client			= @client
	
	return isnull(@answer,0)
end
GO
