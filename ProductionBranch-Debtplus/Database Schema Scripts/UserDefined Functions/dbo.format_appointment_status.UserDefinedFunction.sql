USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_appointment_status]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_appointment_status] ( @status as varchar(1) ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	if @status = 'P'
		select	@answer = 'Pending'
	else
		if @status = 'K'
			select	@answer = 'Counseled'
		else
			if @status = 'W'
				select	@answer = 'Walk-in'
			else
				if @status = 'M'
					select	@answer = 'Missed'
				else
					if @status = 'C'
						select	@answer = 'Cancelled'
					else
						if @status = 'R'
							select	@answer = 'Rescheduled'
	return ( @answer )
end
GO
