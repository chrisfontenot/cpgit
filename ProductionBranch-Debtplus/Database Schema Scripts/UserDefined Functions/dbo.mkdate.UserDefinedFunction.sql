USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[mkdate]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[mkdate] (@mon integer, @dy integer, @yr integer) returns datetime as
begin
  declare @answer datetime
  select  @answer = convert(datetime, convert(varchar,@mon) + '/' + convert(varchar,@dy) + '/' + convert(varchar,@yr) + ' 00:00:00')
  return ( @answer )
end
GO
