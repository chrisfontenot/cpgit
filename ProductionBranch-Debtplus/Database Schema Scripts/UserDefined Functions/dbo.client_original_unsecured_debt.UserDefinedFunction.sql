USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_original_unsecured_debt]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Al Longyear, DebtPlus, L.L.C.
-- Create date: 12/18/2007
-- Description:	Determine the unsecured original debt balance for a client
-- =============================================
CREATE FUNCTION [dbo].[client_original_unsecured_debt] 
(
	@client int
)
RETURNS money
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result money

	-- Add the T-SQL statements to compute the return value here
	SELECT	@Result = sum(b.orig_balance)
	from	client_creditor cc with (nolock)
	inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
	left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
	left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
	where	cc.client = @client
	and	isnull(ccl.agency_account,0) = 0
	and	cc.reassigned_debt = 0

	-- Return the result of the function
	RETURN isnull(@Result,0)
END
GO
