USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_yn]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_yn] ( @input as int ) returns varchar(1) as
begin
	declare	@Answer varchar(1)
	select	@answer = 'N'
	if isnull(@input,0) <> 0
		select	@answer = 'Y'
	return ( @answer )
end
GO
