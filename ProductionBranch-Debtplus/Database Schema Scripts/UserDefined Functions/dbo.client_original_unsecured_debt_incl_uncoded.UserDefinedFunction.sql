USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_original_unsecured_debt_incl_uncoded]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Paul Brown
-- Create date: 10/10/2012
-- Description:	Determine the unsecured original debt balance for a client, including uncoded creditors,
--              excluding creditors starting with X
-- =============================================
CREATE FUNCTION [dbo].[client_original_unsecured_debt_incl_uncoded] 
(
	@client int
)
RETURNS money
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result money

	-- Add the T-SQL statements to compute the return value here
	SELECT	@Result = sum(b.orig_balance)
	from	client_creditor cc with (nolock)
	inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
	where	cc.client = @client
	and	cc.reassigned_debt = 0
	and (cc.creditor is null or cc.creditor not like 'x%')

	-- Return the result of the function
	RETURN isnull(@Result,0)
END
GO
