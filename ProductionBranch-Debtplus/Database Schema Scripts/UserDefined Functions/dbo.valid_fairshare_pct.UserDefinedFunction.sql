USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[valid_fairshare_pct]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[valid_fairshare_pct] (@fairshare_pct float) RETURNS float AS  
BEGIN 
        -- ========================================================================================
        -- ==     Generate a valid fairshare percentage rate from the input                      ==
        -- ========================================================================================
	if @fairshare_pct is not null
	begin
		if @fairshare_pct < 0.0
			select	@fairshare_pct	= null
		else
			while	@fairshare_pct >= 1.0
				select	@fairshare_pct = @fairshare_pct / 100.0
	end
	return @fairshare_pct
END
GO
