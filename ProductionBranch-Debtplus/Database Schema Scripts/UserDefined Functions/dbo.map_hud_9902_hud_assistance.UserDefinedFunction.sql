USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_hud_assistance]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_hud_assistance] ( @value as int ) returns int as
begin
	declare	@answer		int
	if @value is not null
	begin
		declare	@name	varchar(80)

		select	@name	= hud_9902_section
		from	housing_HUDAssistanceTypes with (nolock)
		where	oID		= @value
		
		if @name is not null
			select	@answer		= id
			from	housing_ARM_ReferenceInfo
			where	groupId		= 18
			and		name		= @name
		
		if @answer is null
			select	@answer		= id
			from	housing_ARM_ReferenceInfo
			where	groupId		= 18
			and		name		= 'NA'
	end
	
	return @answer
end
GO
