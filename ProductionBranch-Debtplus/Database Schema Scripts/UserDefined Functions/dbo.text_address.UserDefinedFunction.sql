USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[text_address]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[text_address] ( @address int ) returns varchar(800) as
begin
	-- ========================================================================
	-- ==   Given a pointer to the address record, return the USPS mail      ==
	-- ==   formatted value of the address. The whole address is returned,   ==
	-- ==   not a single address component. The lines are seperated by a     ==
	-- ==   CR/LF sequence.													 ==
	-- ========================================================================

	declare	@answer			varchar(800)
	declare	@line_1			varchar(256)
	declare @line_2			varchar(80)
	declare @line_3			varchar(80)
	declare @line_4			varchar(80)
	declare @city			varchar(80)
	declare	@state			int
	declare	@postalcode		varchar(80)

	declare	@house			varchar(20)
	declare	@direction		varchar(2)
	declare	@street			varchar(80)
	declare	@suffix			varchar(10)
	declare	@modifier		varchar(10)
	declare	@modifier_value	varchar(30)

	-- Read the address information
	select	@house			= house,
			@direction		= direction,
			@street			= street,
			@suffix			= suffix,
			@modifier		= modifier,
			@modifier_value	= modifier_value,
			@line_2			= address_line_2,
			@line_3			= address_line_3,
			@city			= city,
			@state			= [state],
			@postalcode		= postalcode
	from	addresses
	where	[address]		= @address

	-- Generate the first line from the fields
	select	@line_1 = dbo.format_Address_Line_1 (@house, @direction, @street, @suffix, @modifier, @modifier_value)

	-- Form the last line from the city/state/zip
	select	@line_4 = dbo.format_city_state_zip (@city, @state, @postalcode)

	-- Build the result
	if @line_1 <> ''
		select	@answer = isnull(@answer + char(13) + char(10),'') + @line_1

	if @line_2 <> ''
		select	@answer = isnull(@answer + char(13) + char(10),'') + @line_2

	if @line_3 <> ''
		select	@answer = isnull(@answer + char(13) + char(10),'') + @line_3

	if @line_4 <> ''
		select	@answer = isnull(@answer + char(13) + char(10),'') + @line_4

	return (@answer)
end
GO
