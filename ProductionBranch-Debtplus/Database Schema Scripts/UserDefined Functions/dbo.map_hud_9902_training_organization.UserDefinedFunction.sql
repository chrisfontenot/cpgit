USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_training_organization]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_training_organization] ( @TrainingOrganization as varchar(80) ) returns int as
begin
	declare	@answer		int
	if @TrainingOrganization is not null
	begin
		select	@answer = id
		from	housing_ARM_ReferenceInfo
		where	[groupID] = 26
		and		[name]    = @TrainingOrganization
	end
	return @answer
end
GO
