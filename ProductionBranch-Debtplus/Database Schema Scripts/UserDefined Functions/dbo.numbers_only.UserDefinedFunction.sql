USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[numbers_only]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[numbers_only] ( @str as varchar(8000) ) returns varchar(8000) as
begin
	-- =================================================================================================
	-- ==          Remove all but the numbers from a string                                           ==
	-- =================================================================================================
	return ( dbo.remove_pattern ( @str, '%[^0-9]%', 1 ))
end
GO
