USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_TerminationReason]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_hud_9902_TerminationReason] ( @TerminationrReason as int ) returns int as
begin
	declare	@answer		int
	declare	@name		varchar(80)
	
	if @TerminationrReason is not null
		select	@name	= hud_9902_section
		from	Housing_TerminationReasonTypes
		where	oID		= @TerminationrReason;
		
	if @name is null
		select	@name		= 'ONGN'
		
	select	@answer		= id
	from	housing_ARM_ReferenceInfo
	where	groupId		= 11
	and		name		= @name
	
	return isnull ( @answer, 1)
end
GO
