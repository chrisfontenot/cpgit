﻿USE [DEBTPLUS]
GO
/****** Object:  UserDefinedFunction [dbo].[validate_dsa_referral]    Script Date: 1/7/2015 11:18:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE function [dbo].[validate_dsa_referral]
(
	@clientid  int,
	@dsaprogram int
) returns varchar(250)
as
begin

	-- DSA Program Constants
	declare @FreddieMacProgram int
	declare @FannieMaeMhnProgram int	
    declare @FannieMaeHpfProgram int
	declare @WellsFargoProgram int
	declare @HPFfmacProgram int

	set @FreddieMacProgram = 301
	set @FannieMaeMhnProgram = 302
	set @FannieMaeHpfProgram = 303    
	set @WellsFargoProgram = 304    
	set @HPFfmacProgram = 305

	-- Referral Code
	declare @CCRCHopeReferralCode int
	declare @HopeFannieMaeReferralCode int
	declare @HopeMilitaryAssistanceReferralCode int
	declare @FreddieMacAffordableServicesReferralCode int
	declare @180DaysDelinquentFMAC  int
	declare @720DaysDelinquentFMAC  int

	set @CCRCHopeReferralCode = 824
	set @HopeFannieMaeReferralCode = 825
	set @HopeMilitaryAssistanceReferralCode = 838
	  
	set @FreddieMacAffordableServicesReferralCode = 822
	set @180DaysDelinquentFMAC = 1366
	set @720DaysDelinquentFMAC = 1367
	
	declare @message	varchar(250)
	declare @referredby  int

	select top(1) @referredby = referred_by 
	  from client_appointments 
     where client = @clientid 
	 --and status in ('P', 'W')
     order by date_created desc

	if @referredby is null
	begin

		set @message = 'Referral Source is mandatory for RxOffice DSA upload. Referral Source must be selected when resulting / scheduling an appointment'
		return @message

	end

	-- validate HPF-FMAC
	if @dsaprogram = @HPFfmacProgram and @referredby is not null
	begin
		if @referredby <> @CCRCHopeReferralCode and @referredby <> @HopeFannieMaeReferralCode and @referredby = @HopeMilitaryAssistanceReferralCode
		begin
			set @message = 'For HPF-FMAC, referral source selected must be CCRC/HOPE, HOPE/FANNIE MAE or HOPE MILITARY ASSISTANCE'
		end
	end

	-- validate Freddie Mac
	if @dsaprogram = @FreddieMacProgram and @referredby is not null
	begin
		if @referredby <> @FreddieMacAffordableServicesReferralCode and @referredby <> @180DaysDelinquentFMAC and @referredby <> @720DaysDelinquentFMAC
		begin
			set @message = 'For Freddie Mac, the referral source selected must be either FREDDIE MAC AFFORDABLE SERVICE or 180+ DAYS DELINQUENT FMAC or 720+ DAYS DELINQUENT FMAC, for DSA Partner FMAC.'
		end
	end

	-- validate Fannie Mae MHN
	if @dsaprogram = @FannieMaeMhnProgram and @referredby is not null
	begin
		if @referredby = @CCRCHopeReferralCode or @referredby = @HopeFannieMaeReferralCode or @referredby = @HopeMilitaryAssistanceReferralCode
		begin
			set @message = 'For Fannie Mae – MHN, the referral source selected must be anything other than CCRC/HOPE, HOPE/FANNIE MAE and HOPE MILITARY ASSISTANCE'
		end
	end

	-- validate Fannie Mae HPF
	if @dsaprogram = @FannieMaeHpfProgram and @referredby is not null
	begin
		if @referredby <> @CCRCHopeReferralCode and @referredby <> @HopeFannieMaeReferralCode and @referredby = @HopeMilitaryAssistanceReferralCode
		begin
			set @message = 'For Fannie Mae – HPF, referral source selected must be CCRC/HOPE, HOPE/FANNIE MAE or HOPE MILITARY ASSISTANCE'
		end
	end

	return @message

end