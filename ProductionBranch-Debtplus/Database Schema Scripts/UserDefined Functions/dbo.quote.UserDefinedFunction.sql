USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[quote]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[quote] (@input varchar(8000)) returns varchar(8000) as
begin
	select @input = isnull('''' + replace(@input, '''', '''''') + '''', 'NULL')
	return ( @input )
end
GO
