USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[format_appointment_type]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[format_appointment_type] ( @appt_type as int ) returns varchar(50) as
begin
	declare	@answer	varchar(50)
	
	if @appt_type is null
		select	@answer = 'Any'
	else
		select	@answer	= appt_name
		from	appt_types
		where	appt_type = @appt_type
	
	return @answer
end
GO
