USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_gender]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[cccs_admin_gender](@salutation varchar(80)) returns int as
begin
	declare	@Answer	int
	
	-- Assume male response
	select	@answer = 1
	
	-- Find the female names
	if lower(@salutation) in (	'mrs.','mrs',
								'miss.','miss',
								'ms.','ms')
		select	@answer = 2
		
	return ( @answer )
end
GO
