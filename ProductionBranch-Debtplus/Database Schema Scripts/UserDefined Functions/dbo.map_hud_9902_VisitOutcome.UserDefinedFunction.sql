USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_hud_9902_VisitOutcome]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[map_hud_9902_VisitOutcome] ( @PurposeOfVisit as int, @ResultType as int ) returns int as
begin
	declare	@answer		int
	declare @label		varchar(50)

	select	@label		= hud_9902_section
	from	Housing_AllowedVisitOutcomeTypes
	where	[PurposeOfVisit]	= @PurposeOfVisit
	and		[Outcome]			= @ResultType

	if @label is null
		select	@label		= hud_9902_section
		from	Housing_AllowedVisitOutcomeTypes
		where	[PurposeOfVisit]	= @PurposeOfVisit
		and		[Default]			= 1

	if @label is null
		select	@label				= hud_9902_section
		from	Housing_AllowedVisitOutcomeTypes
		where	[PurposeOfVisit]	= @PurposeOfVisit

	-- Find the purpose of visit to determine which mapping item to use
	declare	@mapped_pov			int
	select	@mapped_pov = dbo.map_hud_9902_PurposeOfVisit ( @PurposeOfVisit );

	declare	@sectionID			int
	select	@sectionID = case @mapped_pov
							when 1 then 22   -- Seeking Pre-Purchase Homebuyer Counseling
							when 2 then 21   -- Locate, Secure, or Maint Residence in Rental Housing
							when 3 then 16   -- Home Maint and Financial Mgmt for Homeowners
							when 4 then 24   -- Resolving or Preventing Mortgage Delinquency
							when 5 then 17   -- Seeking Shelter or Services for the Homeless
							else 24
						end

	-- From the section and the label, find the result code
	select	@answer		= id
	from	Housing_ARM_referenceInfo
	where	[groupID]	= @sectionID
	and		[name]		= @label

	return @answer
end
GO
