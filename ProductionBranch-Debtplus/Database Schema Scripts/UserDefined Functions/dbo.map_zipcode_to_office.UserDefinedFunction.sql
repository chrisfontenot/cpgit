USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[map_zipcode_to_office]    Script Date: 09/15/2014 13:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[map_zipcode_to_office](@zipcode varchar(10)) returns int as
begin
	declare	@answer		int
	select	@zipcode	= ltrim(rtrim(replace(@zipcode,'-','')))
	if len(@zipcode) > 5
		select	@zipcode = left(@zipcode,5)

	select	@answer = office
	from	zipcodes
	where	zip_lower >= @zipcode + '0000'
	and		zip_upper <= @zipcode + '9999'
	
	if @answer is null
		select	@answer = office
		from	zipcodes
		where	zip_lower = '000000000'
		
	if @answer is null
		select	@answer = min(office)
		from	offices
		where	[default]	= 1
		
	if @answer is null
		select	@answer = min(office)
		from	offices
		
	if @answer is null
		select	@answer = 1
		
	return (@answer)
end
GO
