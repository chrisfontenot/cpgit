USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[cccs_admin_map_ethnicity]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[cccs_admin_map_ethnicity]( @input as int ) returns int as
begin
    declare @answer		int
	select	@answer =	case isnull(@input,1)
							when 4  then 1
							when 14 then 2
							else 2
						end

	return @answer
end
GO
