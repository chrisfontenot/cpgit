drop function get_client_phone_numbers
GO
IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'get_client_phone_numbers' AND TYPE='FN')
	EXEC('CREATE function [dbo].[get_client_phone_numbers] (@clientid int) returns varchar(250) as BEGIN RETURN NULL END')
GO

ALTER function [dbo].[get_client_phone_numbers] (@clientid  int) returns varchar(250) as
begin
	declare @homephone		varchar(80)
	declare @messagephone	varchar(80)
	declare @cellphone1		varchar(80)
	declare @cellphone2		varchar(80)

	SELECT	@homephone    = [phone],
			@cellphone1   = [app_cell_phone],
			@cellphone2   = [coapp_cell_phone],
			@messagephone = [message_ph]
	FROM	[view_client_address] WITH (NOLOCK)
	where	[client] = @clientid

	if isnull(@homephone,'') != ''
	begin
		return @homephone
	end

	if isnull(@cellphone1,'') != ''
	begin
		return @cellphone1
	end

	if isnull(@cellphone2,'') != ''
	begin
		return @cellphone2
	end

	if isnull(@messagephone,'') != ''
	begin
		return @messagephone
	end

	return ''
end
GO
GRANT EXECUTE ON get_client_phone_numbers TO PUBLIC AS dbo
GO
DENY EXECUTE ON get_client_phone_numbers TO www_role
GO
