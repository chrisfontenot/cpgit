USE [DebtPlus]
GO
/****** Object:  UserDefinedFunction [dbo].[client_deposit_amount]    Script Date: 09/15/2014 13:13:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Al Longyear, DebtPlus, L.L.C.
-- Create date: 12/18/2007
-- Description:	Determine the expected deposit amount for a client
-- =============================================
CREATE FUNCTION [dbo].[client_deposit_amount] 
(
	@client int
)
RETURNS money
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result money

	-- Add the T-SQL statements to compute the return value here
	SELECT	@Result = sum(deposit_amount)
	FROM	client_deposits WITH (NOLOCK)
	WHERE	client = @client

	-- Return the result of the function
	RETURN isnull(@Result,0)
END
GO
