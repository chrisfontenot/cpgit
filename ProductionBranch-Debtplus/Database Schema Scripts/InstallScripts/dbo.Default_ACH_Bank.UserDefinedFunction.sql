if exists(select * from sysobjects where name = 'Default_ACH_Bank' and type = 'FN')
	EXEC('drop function Default_ACH_Bank')
GO
CREATE function [dbo].[Default_ACH_Bank]() returns int as
begin
	-- ===========================================================================================================
	-- ==           Find the default item for the ACH Bank table                                          ==
	-- ===========================================================================================================
	declare	@answer		int

	-- Try for the one marked default
	select	@answer		= min(bank)
	from	Banks with (nolock)
	where	[default]	= 1
	AND		type		= 'A'

	-- Failing that, take the first item in the table
	if @answer is null
		select	@answer		= min(bank)
		from	Banks with (nolock)
		where	type		= 'A'

	-- NULL is NOT acceptable.
	return isnull(@answer,1)
end
GO
GRANT execute ON Default_ACH_Bank TO public AS dbo;
GO
