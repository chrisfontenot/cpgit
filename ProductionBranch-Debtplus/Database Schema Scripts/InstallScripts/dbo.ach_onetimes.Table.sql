if not exists(select * from sysobjects where name = 'default_ach_batch_type' and type = 'D')
	exec ('create default [dbo].[default_ach_batch_type] as ''TEL''');
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'bank', 'int'
GO
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'batch_type'
GO
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'temp_fhlb_firm_name'
GO
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'temp_fhlb_email'
GO
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'temp_fhlb_firm_phone'
GO
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'client_product', 'int'
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'ach_onetimes'
GO
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_ach_onetimes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_client NOT NULL,
	bank dbo.typ_key NOT NULL,
	batch_type varchar(3) NOT NULL,
	deposit_batch_id dbo.typ_key NULL,
	deposit_batch_date datetime NULL,
	ABA varchar(9) NOT NULL,
	AccountNumber varchar(17) NOT NULL,
	CheckingSavings char(1) NOT NULL,
	Amount money NOT NULL,
	EffectiveDate datetime NOT NULL,
	client_product dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	temp_fhlb_firm_name varchar(80) NULL,
	temp_fhlb_email varchar(256) NULL,
	temp_fhlb_firm_phone varchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_ach_onetimes SET (LOCK_ESCALATION = TABLE)
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the client_products table if this is a non-DMP transaction. Otherwise, for DMP, this is NULL'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_ach_onetimes', N'COLUMN', N'client_product'
GO
ALTER TABLE dbo.Tmp_ach_onetimes ADD CONSTRAINT
	DF_ach_onetimes_bank DEFAULT ([dbo].[default_ach_bank]()) FOR bank
GO
EXECUTE sp_bindefault N'dbo.default_ach_batch_type', N'dbo.Tmp_ach_onetimes.batch_type'
GO
EXECUTE sp_bindefault N'dbo.default_CheckingSavings', N'dbo.Tmp_ach_onetimes.CheckingSavings'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_ach_onetimes.Amount'
GO
SET IDENTITY_INSERT dbo.Tmp_ach_onetimes ON
GO
IF EXISTS(SELECT * FROM dbo.ach_onetimes)
	 EXEC('INSERT INTO dbo.Tmp_ach_onetimes (oID, client, bank, batch_type, deposit_batch_id, deposit_batch_date, ABA, AccountNumber, CheckingSavings, Amount, EffectiveDate, client_product, date_created, created_by, temp_fhlb_firm_name, temp_fhlb_email, temp_fhlb_firm_phone)
		SELECT oID, client, isnull(bank,4) AS bank, isnull(batch_type,''TEL''), deposit_batch_id, deposit_batch_date, ABA, AccountNumber, isnull(CheckingSavings,''C''), Amount, EffectiveDate, client_product, date_created, created_by, temp_fhlb_firm_name, temp_fhlb_email, temp_fhlb_firm_phone FROM dbo.ach_onetimes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ach_onetimes OFF
GO
DROP TABLE dbo.ach_onetimes
GO
EXECUTE sp_rename N'dbo.Tmp_ach_onetimes', N'ach_onetimes', 'OBJECT' 
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	PK_ach_onetime PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ach_onetime_1 ON dbo.ach_onetimes
	(
	deposit_batch_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ach_onetime_2 ON dbo.ach_onetimes
	(
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	FK_ach_onetimes_clients FOREIGN KEY
	(
	client
	) REFERENCES dbo.clients
	(
	client
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
GO
/*
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	FK_ach_onetimes_client_products FOREIGN KEY
	(
	client_product
	) REFERENCES dbo.client_products
	(
	oID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
*/
GO
delete	ach_onetimes
from	ach_onetimes o
left outer join deposit_batch_ids i on o.deposit_batch_id = i.deposit_batch_id
where	o.deposit_batch_id is not null
and		i.deposit_batch_id is null
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	FK_ach_onetimes_deposit_batch_ids FOREIGN KEY
	(
	deposit_batch_id
	) REFERENCES dbo.deposit_batch_ids
	(
	deposit_batch_id
	) ON UPDATE  CASCADE 
	 ON DELETE  SET NULL 
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	FK_ach_onetimes_banks FOREIGN KEY
	(
	bank
	) REFERENCES dbo.banks
	(
	bank
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
COMMIT
GO
GRANT SELECT,UPDATE,INSERT,DELETE ON ach_onetimes TO public AS dbo;
GO
DENY SELECT,UPDATE,INSERT,DELETE to www_role;
GO

