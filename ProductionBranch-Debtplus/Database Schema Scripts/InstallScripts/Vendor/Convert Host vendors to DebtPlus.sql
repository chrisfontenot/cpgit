truncate table product_transactions
truncate table credit_card_payment_details
go
delete from ach_onetimes where client_product is not null
delete from deposit_batch_details where client_product is not null
go
delete from client_products
go

delete from vendors
delete from vendor_addresses
delete from vendor_contacts
delete from vendor_addkeys
delete from vendor_notes
delete from vendor_products
delete from client_products
GO

dbcc checkident (vendors, reseed, 0)
dbcc checkident (vendor_addresses, reseed, 0)
dbcc checkident (vendor_contacts, reseed, 0)
dbcc checkident (vendor_addkeys, reseed, 0)
dbcc checkident (vendor_notes, reseed, 0)
dbcc checkident (vendor_products, reseed, 0)
dbcc checkident (client_products, reseed, 0)
go

-- Correct bogus values
update HOST_ATTYFIRM set [FIRM#] = '0001' where [FIRM#] = '9996'
update HOST_ATTYFIRM set [FIRM#] = '0002' where [FIRM#] = '9999'
update HOST_ATTYFIRM set [FIRM#] = '0003' where [FIRM#] = '7777'
update HOST_ATTYFIRM set [FIRM#] = '0004' where [FIRM#] = '7779'
GO

declare @maxid int
select @maxid = max(telephonenumber) from telephonenumbers
if @maxid is null
   set @maxid = 0
dbcc checkident (telephonenumbers, reseed, @maxid)
GO

declare @maxid int
select @maxid = max(name) from names
if @maxid is null
   set @maxid = 0
dbcc checkident (names, reseed, @maxid)
GO

declare @maxid int
select @maxid = max(email) from emailaddresses
if @maxid is null
   set @maxid = 0
dbcc checkident (emailaddresses, reseed, @maxid)
GO

declare @maxid int
select @maxid = max(address) from addresses
if @maxid is null
   set @maxid = 0
dbcc checkident (addresses, reseed, @maxid)
GO

-- Ensure that the vendor table starts fresh
declare @maxid int
select @maxid = max(oID) from vendors
if @maxid is null
   set @maxid = 0
dbcc checkident (vendors, reseed, @maxid)
GO

execute UPDATE_add_column 'host_attyfirm', 'ptr_contact', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_bill_contact', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_phone', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_fax', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_email', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_billing_email', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_addr1', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_bill_addr1', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_contact_main_addr1', 'int';
execute UPDATE_add_column 'host_attyfirm', 'ptr_contact_bill_addr1', 'int';
GO
update host_attyfirm set ptr_contact = null, ptr_bill_contact = null, ptr_phone = null, ptr_fax = null, ptr_email = null, ptr_billing_email = null, ptr_addr1 = null, ptr_bill_addr1 = null, ptr_contact_main_addr1 = null, ptr_contact_bill_addr1 = null
GO

execute UPDATE_add_column 'names', 'vendor_contact', 'int';
execute UPDATE_add_column 'names', 'vendor_bill_contact', 'int';
GO
update names set vendor_contact = null, vendor_bill_contact = null
GO
execute UPDATE_add_column 'telephonenumbers', 'vendor_phone', 'int';
execute UPDATE_add_column 'telephonenumbers', 'vendor_fax', 'int';
GO
update telephonenumbers set vendor_phone = null, vendor_fax = null
go
execute UPDATE_add_column 'emailaddresses', 'vendor_email', 'int';
execute UPDATE_add_column 'emailaddresses', 'vendor_billing_email', 'int';
GO
update emailaddresses set vendor_email = null, vendor_billing_email = null
GO
execute UPDATE_add_column 'addresses', 'vendor_state', 'int';
execute UPDATE_add_column 'addresses', 'vendor_addr1', 'int';
execute UPDATE_add_column 'addresses', 'vendor_bill_addr1', 'int';
execute UPDATE_add_column 'addresses', 'vendor_contact_main_addr1', 'int';
execute UPDATE_add_column 'addresses', 'vendor_contact_bill_addr1', 'int';
go
update addresses set vendor_state = null, vendor_addr1 = null, vendor_bill_addr1 = null, vendor_contact_main_addr1 = null, vendor_contact_bill_addr1 = null
go

-- Load the base list of vendors
set identity_insert vendors on
insert into vendors (oid, type, label, name, ActiveFlag, SupressPayments, SupressInvoices, SupressPrintingPayments, BillingMode, Ach_checkingSavings, website, MonthlyBillingDay)
select convert(int,[firm#]) as oid, 'A' as type, 'A-' + right('000000' + [firm#],4) as label, substring([firm name],1, 50) as 'name', 1 as ActiveFlag, 0 as SupressPayments, 0 as SupressInvoices, 0 as SupressPrintingPayments, 'None' as BillingMode, 'C' as Ach_checkingSavings, website, 10 as MonthlyBillingDay
from host_attyfirm order by 1
set identity_insert vendors off
GO

-- Find the contact names
select contact, [firm#] as vendor, convert(varchar(80),'') as first, convert(varchar(80),'') as middle, contact as last, convert(varchar(80),'') as suffix
into #names
from host_attyfirm
where isnull(contact,'') <> ''
order by 1

-- Split the first name
update #names set first = ltrim(rtrim(substring(last,1,charindex(' ',last)))), last = ltrim(rtrim(substring(last, charindex(' ',last), 800))) where last like '% %'

-- Split the middle name
update #names set middle = ltrim(rtrim(substring(last,1,charindex(' ',last)))), last = ltrim(rtrim(substring(last, charindex(' ',last), 800))) where last like '% %'

-- Split the suffix
update #names set suffix = ltrim(rtrim(substring(last, charindex(',',last), 800))), last = ltrim(rtrim(substring(last,1,charindex(',',last)))) where last like '%,%'

-- Merge the middle with the last if there really is no middle
update #names set last = middle + ' ' + last, middle = '' where middle in ('de','la','von','van')
GO

-- Set the name pointers
insert into names (vendor_contact, first, middle, last, suffix)
select vendor, first, middle, last, suffix
from #names
GO

drop table #names
GO

update host_attyfirm set ptr_contact = n.name
from host_attyfirm h
inner join names n on h.[firm#] = n.vendor_contact
GO

-- Set the name pointers
insert into names (vendor_bill_contact, first, middle, last, suffix)
select [firm#], [bill fname], [bill mi], [bill lname], '' as suffix
from host_attyfirm
where isnull([bill fname],'') != '' or isnull([bill mi],'') != '' or isnull([bill lname],'') != ''
GO

update host_attyfirm set ptr_bill_contact = n.name
from host_attyfirm h
inner join names n on h.[firm#] = n.vendor_bill_contact
GO

-- Find the telephone numbers
select [firm#] as vendor, phone, dbo.numbers_only(phone) as number, convert(varchar(10),'') as acode, convert(varchar(10),'') as extension
into #phones
from host_attyfirm
where isnull(phone,'') <> ''
order by [firm#]
GO

update #phones set acode = substring(number,1,3), number=substring(number,4, 80) where len(number) >= 10
update #phones set extension = substring(number,7,80), number = substring(number,1,7) where len(number) > 7
update #phones set number = substring(number,1,3) + '-' + substring(number,4,80)

insert into telephonenumbers (vendor_phone,acode,number,ext,country) select vendor,acode,number,extension,1 from #phones order by 1
GO
drop table #phones
GO

update host_attyfirm set ptr_phone = t.telephonenumber
from host_attyfirm h
inner join telephonenumbers t on h.[firm#] = t.vendor_phone
go

select [firm#] as vendor, fax, dbo.numbers_only(fax) as number, convert(varchar(10),'') as acode, convert(varchar(10),'') as extension
into #phones
from host_attyfirm
where isnull(fax,'') <> ''
order by [firm#]
GO

update #phones set acode = substring(number,1,3), number=substring(number,4, 80) where len(number) >= 10
update #phones set extension = substring(number,7,80), number = substring(number,1,7) where len(number) > 7
update #phones set number = substring(number,1,3) + '-' + substring(number,4,80)
GO

insert into telephonenumbers (vendor_fax,acode,number,ext,country) select vendor,acode,number,extension,1 from #phones order by 1
GO
drop table #phones
GO

update host_attyfirm set ptr_fax = t.telephonenumber
from host_attyfirm h
inner join telephonenumbers t on h.[firm#] = t.vendor_fax
go

insert into emailaddresses (vendor_email,address,validation)
select [firm#], email, 1
from host_attyfirm
where email like '%@__%'
go

update host_attyfirm set ptr_email = e.email
from host_attyfirm h
inner join emailaddresses e on h.[firm#] = e.vendor_email
go

insert into emailaddresses (vendor_billing_email,address,validation)
select [firm#], [billing email], 1
from host_attyfirm
where [billing email] like '%@__%'
go

update host_attyfirm set ptr_billing_email = e.email
from host_attyfirm h
inner join emailaddresses e on h.[firm#] = e.vendor_billing_email
go

insert into addresses (vendor_addr1, creditor_prefix_1, street, address_line_2, city, state, postalcode)
select a.[firm#], a.[FIRM NAME], a.addr1, a.addr2, a.city, isnull(st.[state],0) as [state], a.zip
from host_attyfirm a
left outer join states st on a.[state] = st.[mailingcode]
where isnull(a.addr1,'') <> '' or isnull(a.city,'') <> '' or isnull(a.[state],'') <> '' or isnull(a.zip,'') <> ''
go

update host_attyfirm set ptr_addr1 = a.address
from host_attyfirm h
inner join addresses a on h.[firm#] = a.vendor_addr1
GO

insert into addresses (vendor_bill_addr1, creditor_prefix_1, street, address_line_2, city, state, postalcode)
select a.[firm#], a.[FIRM NAME], a.[bill add1], a.[bill add2], a.[bill city], isnull(st.[state],0) as state, a.[bill zip]
from host_attyfirm a
left outer join states st on a.[bill st] = st.[mailingcode]
where isnull(a.[bill add1],'') <> '' or isnull(a.[bill city],'') <> '' or isnull(a.[bill st],'') <> '' or isnull(a.[bill zip],'') <> ''
go

update host_attyfirm set ptr_bill_addr1 = a.address
from host_attyfirm h
inner join addresses a on h.[firm#] = a.vendor_bill_addr1
GO

-- Contact information
insert into addresses (vendor_contact_main_addr1, street, address_line_2, city, state, postalcode)
select a.[firm#], a.addr1, a.addr2, a.city, st.[state], a.zip
from host_attyfirm a
left outer join states st on a.[state] = st.[mailingcode]
where isnull(a.[addr1],'') <> '' or isnull(a.[city],'') <> '' or isnull(a.[state],'') <> '' or isnull(a.[zip],'') <> ''
go

update host_attyfirm set ptr_contact_main_addr1 = a.address
from host_attyfirm h
inner join addresses a on h.[firm#] = a.vendor_contact_main_addr1
GO

insert into addresses (vendor_contact_bill_addr1, street, address_line_2, city, state, postalcode)
select a.[firm#], a.[bill add1], a.[bill add2], a.[bill city], isnull(st.[state],0) as state, a.[bill zip]
from host_attyfirm a
left outer join states st on a.[bill st] = st.[mailingcode]
where isnull(a.[bill add1],'') <> '' or isnull(a.[bill city],'') <> '' or isnull(a.[bill st],'') <> '' or isnull(a.[bill zip],'') <> ''
go

update host_attyfirm set ptr_contact_bill_addr1 = a.address
from host_attyfirm h
inner join addresses a on h.[firm#] = a.vendor_contact_bill_addr1
GO

-- construct the vendor addresses
insert into vendor_addresses (vendor, address_type, line_1, addressid)
select [firm#], 'G', left([firm name], 50), ptr_addr1
from host_attyfirm
where ptr_addr1 is not null
order by 1

insert into vendor_addresses (vendor, address_type, line_1, addressid)
select [firm#], 'I',  left([firm name], 50), ptr_bill_addr1
from host_attyfirm
where ptr_bill_addr1 is not null
order by 1

-- Create the contact information main = 1, invoicing = 2
insert into vendor_contacts (vendor, contact_type, nameID, addressID, telephoneID, faxID, emailID)
select [firm#], 1, ptr_contact, ptr_contact_main_addr1, ptr_phone, ptr_fax, ptr_email
from host_attyfirm
where ptr_contact is not null or ptr_contact_main_addr1 is not null or ptr_phone is not null or ptr_fax is not null or ptr_email is not null
order by 1

insert into vendor_contacts (vendor, contact_type, nameID, addressID, telephoneID, faxID, emailID)
select [firm#], 2, ptr_bill_contact, ptr_contact_bill_addr1, null as ptr_phone, null as ptr_fax, ptr_billing_email
from host_attyfirm
where ptr_bill_contact is not null or ptr_contact_bill_addr1 is not null or ptr_billing_email is not null
order by 1

-- Web access
insert into client_www (PKID, ApplicationName, UserName, Password, Email, PasswordQuestion, PasswordAnswer, Comment, IsLockedOut, isApproved, LastPasswordChangeDate, LastLoginDate, LastActivityDate, LastLockoutDate, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, DatabaseKeyID)
select newid() as PKID, '/AttorneyPortal' as ApplicationName, [LOGIN ID] as UserName, '***' as Password, [EMAIL] as Email, '***NOT USED***' as PasswordQuestion, '***' as PasswordAnswer, [LOG PW] as Comment, 0 as IsLockedOut, 1 as isApproved, getutcdate() as LastPasswordChangeDate, getutcdate() as LastLoginDate, getutcdate() as LastActivityDate,getutcdate() as LastLockoutDate,0 as FailedPasswordAnswerAttemptCount,getutcdate() as FailedPasswordAnswerAttemptWindowStart, 0 as FailedPasswordAttemptCount, getutcdate() as FailedPasswordAttemptWindowStart, convert(int,[firm#]) as DatabaseKeyID
from host_attyfirm
where isnull([LOGIN ID],'') <> ''

-- Vendor products
insert into vendor_products (vendor, product, EscrowProBono, effective_date)
select v.oid as vendor, p.oid as product, 0 as EscrowProBono, getdate() as effective_date
from vendors v
cross join products p

-- Set the reference as escrow if needed
update vendor_products
set EscrowProBono = 1
from vendor_products p
inner join host_attyfirm h on h.[Firm#] = p.vendor
where h.[ESCROW] = '2'
and   p.product  = 1; -- prefile

update vendor_products
set EscrowProBono = 1
from vendor_products p
inner join host_attyfirm h on h.[Firm#] = p.vendor
where h.[ESCROW] = '3'
and   p.product  = 2; -- predischarge

update vendor_products
set EscrowProBono = 1
from vendor_products p
inner join host_attyfirm h on h.[Firm#] = p.vendor
where h.[ESCROW] = '1';

-- Look for ProBono
update vendor_products
set EscrowProBono = 2
from vendor_products p
inner join host_attyfirm h on h.[Firm#] = p.vendor
where h.[ESCROW] = '6';

-- Set the billing day
update vendors
set MonthlyBillingDay = case when isnull(h.[BILL CYCLE],'') = '' then 10 else convert(int,h.[BILL CYCLE]) end
from vendors v
inner join host_attyfirm h on h.[Firm#] = v.oID

-- Update the credit card number
update vendors
set ACH_AccountNumber = h.[BILL CC NO],
    CC_CVV = h.[BILL CCV],
    NameOnCard = replace(h.[CC NAME], '|', ' '),
    BillingMode = 'CreditCard'
from vendors v
inner join host_attyfirm h on h.[Firm#] = v.oID
where isnull(h.[BILL CC NO],'') != ''
and h.[PMT TYPE] = 'C'

update vendors
set CC_ExpirationDate = dateadd(day, -1, dateadd(month, 1, convert(datetime, substring(h.[BILL CC EXP], 1, 2) + '/01/20' + substring(h.[BILL CC EXP], 4, 2))))
from vendors v
inner join host_attyfirm h on h.[Firm#] = v.oID
where h.[bill cc exp] like '[0-9][0-9]_[0-9][0-9]'
and h.[PMT TYPE] = 'C'

-- Set the routing numbers for ACH
update vendors
set ACH_RoutingNumber = h.[ROUTNO],
    BillingMode = 'ACH'
from vendors v
inner join host_attyfirm h on h.[Firm#] = v.oID
where h.[ROUTNO] like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
and   h.[PMT TYPE] = 'B'

-- Set the ACH account number
update vendors
set ACH_AccountNumber = h.[ACCTNO],
    BillingMode = 'ACH'
from vendors v
inner join host_attyfirm h on h.[Firm#] = v.oID
where h.[PMT TYPE] = 'B'

-- Cleanup the addresses table
alter table addresses drop column vendor_addr1
alter table addresses drop column vendor_state
alter table addresses drop column vendor_bill_addr1
alter table addresses drop column vendor_contact_main_addr1
alter table addresses drop column vendor_contact_bill_addr1

-- Cleanup the names table
alter table names drop column vendor_contact
alter table names drop column vendor_bill_contact

-- Cleanup the email addresses table
alter table emailaddresses drop column vendor_email
alter table emailaddresses drop column vendor_billing_email

-- Cleanup the telephone numbers table
alter table telephonenumbers drop column vendor_phone
alter table telephonenumbers drop column vendor_fax
