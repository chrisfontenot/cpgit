
alter table vendor_notes add item_date varchar(80) null
alter table vendor_notes add item_time varchar(80) null
go
update vendor_notes set item_date = created_by
update vendor_notes set item_time = substring(item_date, charindex(' - ',item_date), 800), item_date = substring(item_date, 1, charindex(' - ', item_date)) where item_date like '% - %'
update vendor_notes set item_time = replace(item_time, ' - ','') where item_time like '% - %'
update vendor_notes set item_time = replace(item_time, ')','') where item_time like '%)%'
update vendor_notes set item_time = convert(varchar, convert(int,substring(item_time, 1, 2)) + 12) + substring(item_time, 3, 3) where item_time like '[0-9][0-9]:[0-9][0-9]pm'
update vendor_notes set item_time = substring(item_time, 1, 5) where item_time like '[0-9][0-9]:[0-9][0-9]am'
update vendor_notes set item_time = '00:00' where item_time is null
update vendor_notes set item_time = '12:' + substring(item_time, 4, 2) where item_time like '24:__'
update vendor_notes set item_date = convert(varchar(10), date_created, 101)
update vendor_notes set note = convert(varchar(8000),note) + created_by, created_by = 'CONVERSION', item_time = '00:00' where isdate(item_date + ' ' + item_time) = 0
update vendor_notes set date_created = convert(datetime, item_date + ' ' + item_time)
update vendor_notes set note = substring(note, 1, len(convert(varchar(8000),note))-1) where convert(varchar(8000),note) like '%('
update vendor_notes set created_by = substring(created_by,1,len(created_by)-11) where created_by like '% - [0-9][0-9]:[0-9][0-9][ap]m)'
update vendor_notes set created_by = substring(created_by,1,len(created_by)-1) where created_by like '%)'
update vendor_notes set subject = substring(convert(varchar(8000),note),1,60) where subject = ''
go
alter table vendor_notes drop column item_date
alter table vendor_notes drop column item_time
go

select top 100 * from vendor_notes
 
