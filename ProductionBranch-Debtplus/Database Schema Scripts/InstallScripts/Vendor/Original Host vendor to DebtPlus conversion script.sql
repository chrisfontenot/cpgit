-- Load the vendors from the input table
set identity_insert vendors on
insert into vendors (oid, name, type, label)
select [firm#], substring([firm name], 1, 50), 'A', substring(convert(varchar(80), newid()),10,10)
from host_attyfirm
set identity_insert vendors off

-- Correct the label field
update vendors set label = 'A-' + convert(varchar, oID)

-- Correct long names where needed
update vendors set name = 'KING COUNTY BAR ASSOCIATION VOLUNTEER LEGAL SVC' where name like 'KING COUNTY BAR ASSOCIATION VOLUNTEER LEGAL%'

-- Set the website
update vendors set website = 'http://' + h.website
from vendors v
inner join host_AttyFirm h on h.[firm#] = v.oid
where ltrim(rtrim(isnull(h.website,''))) <> ''
update vendors set website = replace(website, 'http://https://', 'https://') where website like 'http://https://%'
update vendors set website = replace(website, 'http://http://', 'http://') where website like 'http://http://%'
update vendors set website = null where website like '%@%'
update vendors set website = null where website not like '%.%'

-- Update the payment method
update vendors set billingmode = 'NONE'

update vendors set billingmode = 'ACH', ACH_RoutingNumber=h.[ROUTNO], ACH_AccountNumber=h.[ACCTNO]
from vendors v
inner join host_AttyFirm h on h.[firm#] = v.oid
where isnull(h.[ROUTNO],'') <> ''

update vendors set billingmode = 'CreditCard', ACH_AccountNumber=h.[BILL CC NO], CC_CID2=h.[BILL CCV]
from vendors v
inner join host_AttyFirm h on h.[firm#] = v.oid
where isnull([BILL CC NO],'') <> '' AND v.BillingMode = 'NONE'

update vendors set cc_expirationdate = dateadd(d, -1, dateadd(month, 1, convert(datetime, substring(h.[BILL CC EXP],1,2) + '/01/20' + substring(h.[BILL CC EXP],4,2))))
from vendors v
inner join host_AttyFirm h on h.[firm#] = v.oid
where v.BillingMode = 'CreditCard'
and h.[BILL CC EXP] like '[0-9][0-9].[0-9][0-9]'
go

-- Name on the credit card
update vendors set NameOnCard = h.[CC NAME]
from vendors v
inner join host_AttyFirm h on h.[firm#] = v.oid
where v.BillingMode = 'CreditCard'
go

update vendors set NameOnCard = replace(NameOnCard,'|',' ') where NameOnCard like '%|%'
update vendors set NameOnCard = ltrim(rtrim(NameOnCard))
go

-- Empty the contact table
delete from vendor_contacts
dbcc checkident(vendor_contacts, reseed, 0)

-- Add the contacts to the list
insert into vendor_contacts (vendor, contact_type, note)
select v.oid, t.oid, 'Main'
from vendors v, vendor_contact_types t
where t.description = 'Main'

-- Set the title
update vendor_contacts set title = 'Partner'
from vendor_contacts v
inner join host_attyfirm h on v.vendor = h.[firm#]
where h.[partner] = 'Y'

-- Include the invoicing
insert into vendor_contacts (vendor, contact_type, note)
select v.oid, t.oid, 'Invoicing'
from vendors v, vendor_contact_types t
where t.description = 'Invoicing'

-- Add the telephone numbers
alter table telephonenumbers add vendor int null;
go
update telephonenumbers set vendor = null;
go
-- Main contact telephone number
insert into telephonenumbers (number, vendor)
select dbo.numbers_only([PHONE]), [Firm#]
from host_ATTYFIRM
where isnull([PHONE],'') <> ''
go
update	telephonenumbers
set		acode = left(number,3),
		number=substring(number,4, 3) + '-' + substring(number, 7, 4)
from	telephonenumbers
where	vendor is not null
go
update	vendor_contacts
set		telephoneid = t.telephonenumber
from	vendor_contacts v
inner join telephonenumbers t on v.vendor = t.vendor
where	v.note = 'Main'
go
-- Main contact FAX number
update telephonenumbers set vendor = null where vendor is not null
go
insert into telephonenumbers (number, vendor)
select dbo.numbers_only([FAX]), [Firm#]
from host_ATTYFIRM
where isnull([FAX],'') <> ''
go
update	telephonenumbers
set		acode = left(number,3),
		number=substring(number,4, 3) + '-' + substring(number, 7, 4)
from	telephonenumbers
where	vendor is not null
go
update	vendor_contacts
set		Faxid = t.telephonenumber
from	vendor_contacts v
inner join telephonenumbers t on v.vendor = t.vendor
where	v.note = 'Main'
go
alter table telephonenumbers drop column vendor;
go

-- Email addresses
alter table emailaddresses add vendor int null;
go
insert into emailaddresses (address, validation, vendor)
select [email], 1, [firm#]
from host_attyfirm
where [email] like '%_@_%'
go
update	vendor_contacts
set		Emailid = t.email
from	vendor_contacts v
inner join emailAddresses t on v.vendor = t.vendor
where	v.note = 'Main'
go
update emailAddresses set vendor = null where vendor is not null
go
insert into emailaddresses (address, validation, vendor)
select [billing email], 1, [firm#]
from host_attyfirm
where [billing email] like '%_@_%'
go
update	vendor_contacts
set		Emailid = t.email
from	vendor_contacts v
inner join emailAddresses t on v.vendor = t.vendor
where	v.note = 'Invoicing'
go
alter table emailAddresses drop column vendor;
go

-- Invoicing contact address (not really used, but looks good.)
alter table addresses add vendor int null;
go
insert into addresses (vendor, street, address_line_2, city, state, postalcode)
select h.[firm#], h.[bill add1], h.[bill add2], h.[bill city], isnull(st.state,0) as state, h.[bill zip]
from host_attyfirm h
left outer join states st on h.[bill st] = st.mailingcode
where isnull(h.[bill add1],'') <> ''
go
update vendor_contacts set addressid = a.address
from	vendor_contacts v
inner join Addresses a on v.vendor = a.vendor
where	v.note = 'Invoicing'
go
update addresses set vendor = null where vendor is not null;
go

-- Principal contact address
insert into addresses (vendor, street, address_line_2, city, state, postalcode)
select h.[firm#], h.[addr1], h.[addr2], h.[city], isnull(st.state,0) as state, h.[zip]
from host_attyfirm h
left outer join states st on h.[state] = st.mailingcode
where isnull(h.[addr1],'') <> ''
go
update vendor_contacts set addressid = a.address
from	vendor_contacts v
inner join Addresses a on v.vendor = a.vendor
where	v.note = 'Main'
go

alter table addresses drop column vendor;
go

-- Invoicing contact name
alter table names add vendor int null;
go
insert into names (vendor, first, middle, last)
select [firm#], [bill fname], [bill mi], [bill lname]
from host_attyfirm h
where isnull(h.[bill lname],'') <> ''
go
update vendor_contacts set nameid = n.name
from	vendor_contacts v
inner join names n on v.vendor = n.vendor
where	v.note = 'Invoicing'
go
update names set vendor = null where vendor is not null;
go
insert into names (vendor, first, middle, last)
select [firm#], '', '', [contact]
from host_attyfirm h
where isnull(h.[contact],'') <> ''
go
update names set first = rtrim(substring(last, 1, charindex(' ',last))), last = ltrim(substring(last, charindex(' ', last), 80)) where vendor is not null and last like '% %'
update names set middle = rtrim(substring(last, 1, charindex(' ',last))), last = ltrim(substring(last, charindex(' ', last), 80)) where vendor is not null and last like '% %'
update names set last = middle + ' ' + last, middle = '' where middle in ('la', 'de', 'von') and vendor is not null
update names set first = last, last = '' where first = '' and middle = '' and last <> '' and vendor is not null
update names set suffix = last, last = middle, middle = '' where last in ('jr', 'sr', 'i', 'ii', 'iii', 'iv', 'v') and vendor is not null
go
update vendor_contacts set nameid = n.name
from	vendor_contacts v
inner join names n on v.vendor = n.vendor
where	v.note = 'Main'
go
alter table names drop column vendor;
go

-- Clear the note field now that it is no longer needed
update vendor_contacts set note = null

-- Add the products to the vendors
delete from vendor_products
dbcc checkident (vendor_products, reseed, 0)
go
insert into vendor_products (vendor, product, escrow)
select v.oid as vendor, p.oid as product, 0 as escrow
from vendors v
cross join products p
order by 1, 2

-- Set the escrow status
update vendor_products set escrow = 0
update vendor_products set escrow = 1
from vendor_products p
inner join host_attyfirm h on p.vendor = h.[firm#]
where p.product = 1	-- pre
and h.escrow in ('1', '2', '5', '6'); -- both, pre, disabled, legal aid

update vendor_products set escrow = 1
from vendor_products p
inner join host_attyfirm h on p.vendor = h.[firm#]
where p.product = 2	-- post
and h.escrow in ('1', '3', '5', '6'); -- both, post, disabled, legal aid

update vendors set billingmode = 'None'
from vendors v
inner join host_attyfirm h on v.oID = h.[firm#]
where h.escrow in ('5', '6'); -- disabled, legal aid

-- Add the vendor main address entries
alter table addresses add vendor int null
go
insert into addresses (vendor, creditor_prefix_1, street, address_line_2, city, state, postalcode)
select h.[firm#], h.[firm name], h.addr1, h.addr2, h.city, isnull(st.state,0) as state, h.zip
from host_attyfirm h
left outer join states st on h.[state] = st.mailingcode
where isnull(h.[addr1],'') <> ''
go

insert into vendor_addresses (vendor, address_type, addressid, attn)
select h.[firm#], 'G' as address_type, a.[address] as addressid, h.[contact] as attn
from host_attyfirm h
inner join addresses a on h.[firm#] = a.vendor
go

-- Invoicing address
update addresses set vendor = null where vendor is not null;
go
insert into addresses (vendor, street, address_line_2, city, state, postalcode)
select h.[firm#], h.[bill add1], h.[bill add2], h.[bill city], isnull(st.state,0) as state, h.[bill zip]
from host_attyfirm h
left outer join states st on h.[bill st] = st.mailingcode
where isnull(h.[bill add1],'') <> ''
go
insert into vendor_addresses (vendor, address_type, addressid, attn)
select h.[firm#], 'I' as address_type, a.[address] as addressid, ltrim(rtrim(replace(isnull(h.[bill fname],'') + ' ' + isnull(h.[bill mi],'') + ' ' + isnull(h.[bill lname],''), '  ', ''))) as attn
from host_attyfirm h
inner join addresses a on h.[firm#] = a.vendor
go

-- Discard the web access
delete
from	client_www
where	ApplicationName = '/AttorneyPortal';

-- Load the records with un-encrypted passwords.
insert into client_www (ApplicationName, UserName, Comment, Password, Email, PasswordQuestion, PasswordAnswer, IsApproved, LastPasswordChangeDate, LastLoginDate, LastActivityDate, LastLockoutDate, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, DatabaseKeyID)
select '/AttorneyPortal', [LOGIN ID], [LOG PW], '**', [Email], '**NOT USED**', '****', 1, getutcdate(), getutcdate(), getutcdate(), null, 0, getutcdate(), 0, getutcdate(), [Firm#]
from	host_AttyFirm
where	isnull([LOGIN ID],'') <> ''
AND		isnull([LOG PW],'') <> ''

-- State overrides to the fees
delete from product_states
go
dbcc checkident (product_states, reseed, 0)
dbcc checkident (product_states)
go

-- Pre-Filing Bankruptcy
insert into product_states (productID, stateID, amount)
select p.oID as product, st.state as state, h.[cou int amt]
from host_statefees h
inner join states st on h.[state] = st.[mailingcode]
cross join products p
where h.[cou int amt] <> p.amount
and p.description = 'BK Pre-File Internet'
go

-- Pre-Discharge Bankruptcy
insert into product_states (productID, stateID, amount)
select p.oID as product, st.state as state, h.[edu int amt]
from host_statefees h
inner join states st on h.[state] = st.[mailingcode]
cross join products p
where h.[edu int amt] <> p.amount
and p.description = 'BK Pre-Discharge Internet'
go
