create procedure DAILY_Vendor_Statistics AS

-- Calculate the statistics for the vendor based upon the run date

-- Stop noise lines
set nocount on

declare		@today		datetime
declare		@tomorrow	datetime
declare		@yearStart	datetime
declare		@monthStart	datetime

-- Find the start of the day. If it is past 8AM then use tomorrow's date as it is being run early
-- for the daily jobs normally start at midnight.
set			@today      = convert(varchar(10), getdate(), 101)
if datepart(hour, @today) > 8
	set		@today = dateadd(day, 1, @today)

set			@tomorrow   = dateadd(day, 1, @today)
set			@monthStart = convert(datetime, convert(varchar, month(@today)) + '/01/' + convert(varchar, year(@today)))
set			@yearStart  = convert(datetime, '01/01/' + convert(varchar, year(@today)))

-- Find the accounts MTD
select		*
into		#billed
from		client_products with (nolock)
where		invoice_date >= @yearStart and invoice_date < @tomorrow

-- Find the payments YTD
select		*
into		#RC
from		product_transactions with (nolock)
where		date_created > @yearStart and date_created < @tomorrow

-- Summarize Bills by vendor
select		vendor, sum(cost) - sum(discount) as st_BilledYTDAmt
into		#billed_ytd_sum
from		#billed
where		invoice_date < @monthStart
group by	vendor;

select		vendor, sum(cost) - sum(discount) as st_BilledMTDAmt
into		#billed_mtd_sum
from		#billed
where		invoice_date >= @monthStart
group by	vendor;

-- Summarize Receipts by vendor
select		vendor, sum(payment) as st_ReceivedYTDAmt
into		#RC_ytd_sum
from		#RC
where		date_created < @monthStart
group by	vendor;

select		vendor, sum(payment) as st_ReceivedMTDAmt
into		#RC_mtd_sum
from		#RC
where		date_created >= @monthStart
group by	vendor;

-- Apply the figures to the vendors
update		vendors
set			st_BilledYTDAmt = isnull(b.st_BilledYTDAmt,0)
from		vendors v
left outer join	#billed_ytd_sum b on v.oID = b.vendor

update		vendors
set			st_BilledMTDAmt = isnull(b.st_BilledMTDAmt,0)
from		vendors v
left outer join	#billed_mtd_sum b on v.oID = b.vendor

update		vendors
set			st_ReceivedYTDAmt = isnull(b.st_ReceivedYTDAmt,0)
from		vendors v
left outer join	#RC_ytd_sum b on v.oID = b.vendor

update		vendors
set			st_ReceivedMTDAmt = isnull(b.st_ReceivedMTDAmt,0)
from		vendors v
left outer join	#RC_mtd_sum b on v.oID = b.vendor

-- Find the first billing date for a vendor
select		vendor, min(invoice_date) as st_FirstBillingDate
into		#firstBillingDate
from		client_products
group by	vendor

update		vendors
set			st_FirstBillingDate = b.st_FirstBillingDate
from		vendors a
left outer join #firstBillingDate b on a.oID = b.vendor

-- Find the products offered
select		vendor, count(*) as st_ProductsOfferedCount
into		#ProductsOffered
from		vendor_products
group by	vendor

update		vendors
set			st_ProductsOfferedCount = isnull(b.st_ProductsOfferedCount,0)
from		vendors a
left outer join #ProductsOffered b on a.oID = b.vendor

-- Find the client count
select		distinct vendor, client
into		#client_count
from		client_products;

select		vendor, count(*) as st_ClientCountTotalCount
into		#client_count_sum
from		#client_count
group by	vendor;

update		vendors
set			st_ClientCountTotalCount = isnull(b.st_ClientCountTotalCount,0)
from		vendors a
left outer join #client_count_sum b on a.oID = b.vendor;

-- Find the number of produts sold
select		vendor, count(*) as st_ProductsSoldCnt
into		#products_sold
from		client_products
where		ActiveFlag <> 0
group by	vendor

update		vendors
set			st_ProductsSoldCnt = isnull(b.st_ProductsSoldCnt,0)
from		vendors a
left outer join #products_sold b on a.oID = b.vendor;

-- Find the dollar amount of billing adjustments
select		vendor, sum(discount) as st_BillingAdjustmentsAmt
into		#BillingAdjustments
from		client_products
where		ActiveFlag <> 0
group by	vendor

update		vendors
set			st_BillingAdjustmentsAmt = isnull(b.st_BillingAdjustmentsAmt,0)
from		vendors a
left outer join #BillingAdjustments b on a.oID = b.vendor;

-- Find the number of disputed transactions
select		vendor, count(*) as st_DisputeCount
into		#DisputedCount
from		client_products
where		disputed_amount > 0
group by	vendor

update		vendors
set			st_BillingAdjustmentsAmt = isnull(b.st_BillingAdjustmentsAmt,0)
from		vendors a
left outer join #BillingAdjustments b on a.oID = b.vendor;

-- Discard working tables
drop table	#billed
drop table	#billed_ytd_sum
drop table	#billed_mtd_sum
drop table	#RC
drop table	#RC_ytd_sum
drop table	#RC_mtd_sum
drop table #firstBillingDate
drop table #ProductsOffered
drop table #client_count
drop table #client_count_sum
drop table #products_sold
drop table #BillingAdjustments
drop table #DisputedCount
go
