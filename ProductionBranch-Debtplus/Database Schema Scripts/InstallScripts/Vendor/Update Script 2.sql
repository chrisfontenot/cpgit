-- SECTION: RULES

CREATE RULE [dbo].[valid_MonthlyBillingDay] AS @value between 1 and 28
GO
CREATE RULE [dbo].[valid_vendor_type] AS @value like '[A-Z]' or @value like '[A-Z][A-Z]'
GO
CREATE RULE [dbo].[valid_vendor_note_type] AS @value IN (1,3,4)
GO
CREATE RULE [dbo].[valid_vendor_CheckingSavings] AS @value IN ('C','S')
GO
CREATE RULE [dbo].[valid_vendor_BillingMode] AS @value in ('NONE','ACH','CREDITCARD')
GO
CREATE RULE [dbo].[valid_vendor_address_type] AS @value IN ('D','I','G')
GO
CREATE RULE [dbo].[valid_vendor_ACH_RoutingNumber] AS @value LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
GO

-- SECTION: TABLES
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'vendor_notes'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_notes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	type int NOT NULL,
	expires datetime NULL,
	dont_edit bit NOT NULL,
	dont_delete bit NOT NULL,
	dont_print bit NOT NULL,
	subject dbo.typ_subject NULL,
	note text NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_vendor_notes.type'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_notes.dont_edit'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_notes.dont_delete'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_notes.dont_print'
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_notes ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendor_notes')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_notes (oID, vendor, type, expires, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by)
		SELECT oID, vendor, type, expires, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by FROM dbo.vendor_notes WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendor_notes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_notes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_notes', N'vendor_notes', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'vendor_contacts'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_contacts
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	contact_type dbo.typ_key NOT NULL,
	nameID dbo.typ_key NULL,
	addressID dbo.typ_key NULL,
	telephoneID dbo.typ_key NULL,
	faxID dbo.typ_key NULL,
	emailID dbo.typ_key NULL,
	note varchar(256) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	Title varchar(150) NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_contacts ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendor_contacts')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_contacts (oID, vendor, contact_type, nameID, addressID, telephoneID, faxID, emailID, note, date_created, created_by, Title)
		SELECT oID, vendor, contact_type, nameID, addressID, telephoneID, faxID, emailID, note, date_created, created_by, Title FROM dbo.vendor_contacts WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendor_contacts')
END
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_contacts OFF
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_contacts', N'vendor_contacts', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_contact_types
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description varchar(50) NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_vendor_contact_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_contact_types.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_contact_types ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendor_contact_types')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_contact_types (oID, description, ActiveFlag, [Default], date_created, created_by)
		SELECT oID, description, ActiveFlag, [Default], date_created, created_by FROM dbo.vendor_contact_types WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendor_contact_types')
END
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_contact_types OFF
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_contact_types', N'vendor_contact_types', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE UPDATE_drop_constraints 'vendor_addresses'
GO
CREATE TABLE dbo.Tmp_vendor_addresses
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	address_type char(1) NOT NULL,
	attn varchar(50) NULL,
	line_1 varchar(50) NULL,
	line_2 varchar(50) NULL,
	addressID dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_vendor_addresses ADD CONSTRAINT DF_vendor_addresses_address_type DEFAULT ('G') FOR address_type
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_addresses ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendor_addresses')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_addresses (oID, vendor, address_type, attn, line_1, line_2, addressID, date_created, created_by)
		SELECT oID, vendor, address_type, attn, line_1, line_2, addressID, date_created, created_by FROM dbo.vendor_addresses WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendor_addresses')
END
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_addresses OFF
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_addresses', N'vendor_addresses', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'vendor_addkeys'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_addkeys
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	additional dbo.typ_description NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_addkeys ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendor_addkeys')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_addkeys (oID, vendor, additional, date_created, created_by)
		SELECT oID, vendor, additional, date_created, created_by FROM dbo.vendor_addkeys WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendor_addkeys')
END
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_addkeys OFF
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_addkeys', N'vendor_addkeys', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'products'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_products
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description varchar(50) NULL,
	Amount money NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	InvoiceEmailTemplateID dbo.typ_key NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_products.Amount'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_products.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_products.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_products ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'products')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_products (oID, description, Amount, ActiveFlag, [Default], InvoiceEmailTemplateID, date_created, created_by)
		SELECT oID, description, Amount, ActiveFlag, [Default], InvoiceEmailTemplateID, date_created, created_by FROM dbo.products WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.products')
END
GO
SET IDENTITY_INSERT dbo.Tmp_products OFF
GO
EXECUTE sp_rename N'dbo.Tmp_products', N'products', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_product_transactions
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NULL,
	product dbo.typ_key NULL,
	client_product dbo.typ_key NULL,
	transaction_type varchar(2) NOT NULL,
	cost money NOT NULL,
	discount money NOT NULL,
	payment money NOT NULL,
	disputed money NOT NULL,
	note varchar(50) NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.cost'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.discount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.payment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_product_transactions.disputed'
GO
SET IDENTITY_INSERT dbo.Tmp_product_transactions ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'product_transactions')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_product_transactions (oID, vendor, product, client_product, transaction_type, cost, discount, payment, disputed, note, date_created, created_by)
		SELECT oID, vendor, product, client_product, transaction_type, cost, discount, payment, disputed, note, date_created, created_by FROM dbo.product_transactions WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.product_transactions')
END
GO
SET IDENTITY_INSERT dbo.Tmp_product_transactions OFF
GO
EXECUTE sp_rename N'dbo.Tmp_product_transactions', N'product_transactions', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
EXECUTE UPDATE_drop_constraints 'product_states'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_product_states
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	productID dbo.typ_key NOT NULL,
	stateID dbo.typ_key NOT NULL,
	Amount money NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_product_states ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'product_states')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_product_states (oID, productID, stateID, Amount, date_created, created_by)
		SELECT oID, productID, stateID, Amount, date_created, created_by FROM dbo.product_states WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.product_states')
END
GO
SET IDENTITY_INSERT dbo.Tmp_product_states OFF
GO
EXECUTE sp_rename N'dbo.Tmp_product_states', N'product_states', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_product_resolution_types
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_product_resolution_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_product_resolution_types.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_product_resolution_types ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'product_resolution_types')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_product_resolution_types (oID, description, ActiveFlag, [Default], date_created, created_by)
		SELECT oID, description, ActiveFlag, [Default], date_created, created_by FROM dbo.product_resolution_types WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.product_resolution_types')
END
GO
SET IDENTITY_INSERT dbo.Tmp_product_resolution_types OFF
GO
EXECUTE sp_rename N'dbo.Tmp_product_resolution_types', N'product_resolution_types', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_product_dispute_types
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description varchar(50) NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_product_dispute_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_product_dispute_types.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_product_dispute_types ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'product_dispute_types')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_product_dispute_types (oID, description, ActiveFlag, [Default], date_created, created_by)
		SELECT oID, description, ActiveFlag, [Default], date_created, created_by FROM dbo.product_dispute_types WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.product_dispute_types')
END
GO
SET IDENTITY_INSERT dbo.Tmp_product_dispute_types OFF
GO
EXECUTE sp_rename N'dbo.Tmp_product_dispute_types', N'product_dispute_types', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_PaymentMethodTypes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description dbo.typ_description NOT NULL,
	nfcc varchar(4) NULL,
	rpps varchar(4) NULL,
	epay varchar(4) NULL,
	note dbo.typ_message NULL,
	hud_9902_section varchar(256) NULL,
	hpf varchar(50) NULL,
	[default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_PaymentMethodTypes ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'PaymentMethodTypes')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_PaymentMethodTypes (oID, description, nfcc, rpps, epay, note, hud_9902_section, hpf, [default], ActiveFlag, date_created, created_by)
		SELECT oID, description, nfcc, rpps, epay, note, hud_9902_section, hpf, [default], ActiveFlag, date_created, created_by FROM dbo.PaymentMethodTypes WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.PaymentMethodTypes')
END
GO
SET IDENTITY_INSERT dbo.Tmp_PaymentMethodTypes OFF
GO
EXECUTE sp_rename N'dbo.Tmp_PaymentMethodTypes', N'PaymentMethodTypes', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_disputed_status_types
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	description varchar(50) NOT NULL,
	want_note bit NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_disputed_status_types.want_note'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_disputed_status_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_disputed_status_types.[Default]'
GO
SET IDENTITY_INSERT dbo.Tmp_disputed_status_types ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'disputed_status_types')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_disputed_status_types (oID, description, want_note, ActiveFlag, [Default], date_created, created_by)
		SELECT oID, description, want_note, ActiveFlag, [Default], date_created, created_by FROM dbo.disputed_status_types WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.disputed_status_types')
END
GO
SET IDENTITY_INSERT dbo.Tmp_disputed_status_types OFF
GO
EXECUTE sp_rename N'dbo.Tmp_disputed_status_types', N'disputed_status_types', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'credit_card_payment_details'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_credit_card_payment_details
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_product dbo.typ_key NOT NULL,
	client dbo.typ_client NULL,
	payment_medium varchar(20) NOT NULL,
	account_number varchar(50) NULL,
	expiration_date datetime NULL,
	cvv varchar(10) NULL,
	credit_card_type varchar(10) NULL,
	card_status_flag varchar(10) NULL,
	amount money NOT NULL,
	avs_full_name varchar(50) NULL,
	avs_address varchar(50) NULL,
	avs_city varchar(50) NULL,
	avs_state varchar(2) NULL,
	avs_zipcode varchar(10) NULL,
	avs_phone varchar(10) NULL,
	return_code varchar(50) NULL,
	confirmation_number varchar(10) NULL,
	settlement_submission_date datetime NULL,
	result_message varchar(256) NULL,
	request xml NULL,
	response xml NULL,
	submission_date dbo.typ_date NOT NULL,
	submission_batch uniqueidentifier NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_credit_card_payment_details ADD CONSTRAINT DF_Table_1_transaction_type DEFAULT ('CreditCard') FOR payment_medium
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_credit_card_payment_details.amount'
GO
ALTER TABLE dbo.Tmp_credit_card_payment_details ADD CONSTRAINT DF_credit_card_payment_details_submission_batch DEFAULT (newid()) FOR submission_batch
GO
SET IDENTITY_INSERT dbo.Tmp_credit_card_payment_details ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'credit_card_payment_details')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_credit_card_payment_details (oID, client_product, client, payment_medium, account_number, expiration_date, cvv, credit_card_type, card_status_flag, amount, avs_full_name, avs_address, avs_city, avs_state, avs_zipcode, avs_phone, return_code, confirmation_number, settlement_submission_date, result_message, request, response, submission_date, submission_batch, date_created, created_by)
		SELECT oID, client_product, client, payment_medium, account_number, expiration_date, cvv, credit_card_type, card_status_flag, amount, avs_full_name, avs_address, avs_city, avs_state, avs_zipcode, avs_phone, return_code, confirmation_number, settlement_submission_date, result_message, request, response, submission_date, submission_batch, date_created, created_by FROM dbo.credit_card_payment_details WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.credit_card_payment_details')
END
GO
SET IDENTITY_INSERT dbo.Tmp_credit_card_payment_details OFF
GO
EXECUTE sp_rename N'dbo.Tmp_credit_card_payment_details', N'credit_card_payment_details', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'client_products_history'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_products_history
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_product dbo.typ_key NOT NULL,
	vendor dbo.typ_key NOT NULL,
	vendor_label varchar(10) NULL,
	cost money NOT NULL,
	discount money NOT NULL,
	tendered money NOT NULL,
	disputed_amount money NOT NULL,
	disputed_date datetime NULL,
	disputed_status dbo.typ_key NULL,
	disputed_note varchar(2048) NULL,
	resolution_date datetime NULL,
	resolution_status dbo.typ_key NULL,
	resolution_note varchar(2048) NULL,
	invoice_status int NOT NULL,
	invoice_date datetime NULL,
	vendor_invoice dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.cost'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.discount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.tendered'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products_history.disputed_amount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_products_history.invoice_status'
GO
SET IDENTITY_INSERT dbo.Tmp_client_products_history ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'client_products_history')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_products_history (oID, client_product, vendor, vendor_label, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, vendor_invoice, date_created, created_by)
		SELECT oID, client_product, vendor, vendor_label, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, vendor_invoice, date_created, created_by FROM dbo.client_products_history WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.client_products_history')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_products_history OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_products_history', N'client_products_history', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'client_products'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_products
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_key NOT NULL,
	vendor dbo.typ_key NOT NULL,
	product dbo.typ_key NOT NULL,
	ActiveFlag bit NOT NULL,
	message varchar(50) NULL,
	reference varchar(50) NULL,
	cost money NOT NULL,
	discount money NOT NULL,
	tendered money NOT NULL,
	disputed_amount money NOT NULL,
	disputed_date datetime NULL,
	disputed_status dbo.typ_key NULL,
	disputed_note varchar(2048) NULL,
	resolution_date datetime NULL,
	resolution_status dbo.typ_key NULL,
	resolution_note varchar(2048) NULL,
	invoice_status int NOT NULL,
	invoice_date datetime NULL,
	first_payment int NULL,
	last_payment int NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the vendor for this item.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'vendor'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the product item.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'product'
GO
DECLARE @v sql_variant 
SET @v = N'TRUE if the record is currently active.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'ActiveFlag'
GO
DECLARE @v sql_variant 
SET @v = N'Original cost of the item. This is equivalent to the "original balance".'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'cost'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount associated with a discount/adjustment.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'discount'
GO
DECLARE @v sql_variant 
SET @v = N'Total dollar amount of money paid on this product.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'tendered'
GO
DECLARE @v sql_variant 
SET @v = N'Dollar amount of the dispute.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'disputed_amount'
GO
DECLARE @v sql_variant 
SET @v = N'Status for the disputed item. If non-null, the debt is disputed.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'disputed_status'
GO
DECLARE @v sql_variant 
SET @v = N'Note associated with the dispute.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'disputed_note'
GO
DECLARE @v sql_variant 
SET @v = N'Note associated with the resolution.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'resolution_note'
GO
DECLARE @v sql_variant 
SET @v = N'Date when the item was invoiced to the vendor.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'invoice_date'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the record was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'ID of the account where the record was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_products', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_client_products.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.cost'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.discount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.tendered'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_client_products.disputed_amount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_products.invoice_status'
GO
SET IDENTITY_INSERT dbo.Tmp_client_products ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'client_products')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_client_products (oID, client, vendor, product, ActiveFlag, message, reference, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, first_payment, last_payment, date_created, created_by)
		SELECT oID, client, vendor, product, ActiveFlag, message, reference, cost, discount, tendered, disputed_amount, disputed_date, disputed_status, disputed_note, resolution_date, resolution_status, resolution_note, invoice_status, invoice_date, first_payment, last_payment, date_created, created_by FROM dbo.client_products WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.client_products')
END
GO
SET IDENTITY_INSERT dbo.Tmp_client_products OFF
GO
EXECUTE sp_rename N'dbo.Tmp_client_products', N'client_products', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'vendors'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendors
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	Type varchar(2) NOT NULL,
	Label varchar(10) NULL,
	Name varchar(50) NULL,
	ActiveFlag bit NOT NULL,
	SupressPayments bit NOT NULL,
	SupressInvoices bit NOT NULL,
	SupressPrintingPayments bit NOT NULL,
	BillingMode varchar(10) NOT NULL,
	ACH_CheckingSavings char(1) NOT NULL,
	ACH_RoutingNumber char(9) NULL,
	ACH_AccountNumber varchar(22) NULL,
	CC_ExpirationDate datetime NULL,
	CC_CVV varchar(10) NULL,
	NameOnCard varchar(50) NULL,
	MonthlyBillingDay int NOT NULL,
	Website varchar(512) NULL,
	Comment varchar(150) NOT NULL,
	st_BilledMTDAmt money NOT NULL,
	st_BilledYTDAmt money NOT NULL,
	st_ReceivedMTDAmt money NOT NULL,
	st_ReceivedYTDAmt money NOT NULL,
	st_ReturnedMailDate datetime NULL,
	st_FirstBillingDate datetime NULL,
	st_ProductsOfferedCount int NOT NULL,
	st_ClientCountTotalCount int NOT NULL,
	st_ProductsSoldCnt int NOT NULL,
	st_PaymentsReceivedAmt money NOT NULL,
	st_DisputeCnt int NOT NULL,
	st_BillingAdjustmentsAmt money NOT NULL,
	Additional xml NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'If there is a website for this vendor, here is the URL to it.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_vendors', N'COLUMN', N'Website'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_vendors.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendors.SupressPayments'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendors.SupressInvoices'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendors.SupressPrintingPayments'
GO
ALTER TABLE dbo.Tmp_vendors ADD CONSTRAINT DF_vendors_BillingMode DEFAULT ('NONE') FOR BillingMode
GO
ALTER TABLE dbo.Tmp_vendors ADD CONSTRAINT DF_vendors_ACH_CheckingSavings DEFAULT ('C') FOR ACH_CheckingSavings
GO
EXECUTE sp_bindefault N'dbo.default_one', N'dbo.Tmp_vendors.MonthlyBillingDay'
GO
EXECUTE sp_bindefault N'dbo.default_string', N'dbo.Tmp_vendors.Comment'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_vendors.st_BilledMTDAmt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_vendors.st_BilledYTDAmt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_vendors.st_ReceivedMTDAmt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_vendors.st_ReceivedYTDAmt'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_vendors.st_ProductsOfferedCount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_vendors.st_ClientCountTotalCount'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_vendors.st_ProductsSoldCnt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_vendors.st_PaymentsReceivedAmt'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_vendors.st_DisputeCnt'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_vendors.st_BillingAdjustmentsAmt'
GO
SET IDENTITY_INSERT dbo.Tmp_vendors ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendors')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendors (oID, Type, Label, Name, ActiveFlag, SupressPayments, SupressInvoices, SupressPrintingPayments, BillingMode, ACH_CheckingSavings, ACH_RoutingNumber, ACH_AccountNumber, CC_ExpirationDate, CC_CVV, NameOnCard, MonthlyBillingDay, Website, Comment, st_BilledMTDAmt, st_BilledYTDAmt, st_ReceivedMTDAmt, st_ReceivedYTDAmt, st_ReturnedMailDate, st_FirstBillingDate, st_ProductsOfferedCount, st_ClientCountTotalCount, st_ProductsSoldCnt, st_PaymentsReceivedAmt, st_DisputeCnt, st_BillingAdjustmentsAmt, Additional, date_created, created_by)
		SELECT oID, Type, Label, Name, ActiveFlag, SupressPayments, SupressInvoices, SupressPrintingPayments, BillingMode, ACH_CheckingSavings, ACH_RoutingNumber, ACH_AccountNumber, CC_ExpirationDate, CC_CVV, NameOnCard, MonthlyBillingDay, Website, Comment, st_BilledMTDAmt, st_BilledYTDAmt, st_ReceivedMTDAmt, st_ReceivedYTDAmt, st_ReturnedMailDate, st_FirstBillingDate, st_ProductsOfferedCount, st_ClientCountTotalCount, st_ProductsSoldCnt, st_PaymentsReceivedAmt, st_DisputeCnt, st_BillingAdjustmentsAmt, Additional, date_created, created_by FROM dbo.vendors WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendors')
END
GO
SET IDENTITY_INSERT dbo.Tmp_vendors OFF
GO
EXECUTE sp_rename N'dbo.Tmp_vendors', N'vendors', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_types
	(
	oID varchar(2) NOT NULL,
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	[Default] bit NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_vendor_types.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_vendor_types.[Default]'
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendor_types')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_types (oID, description, ActiveFlag, [Default], created_by, date_created)
		SELECT oID, description, ActiveFlag, [Default], created_by, date_created FROM dbo.vendor_types WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendor_types')
END
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_types', N'vendor_types', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'vendor_products'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_vendor_products
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	vendor dbo.typ_key NOT NULL,
	product dbo.typ_key NOT NULL,
	EscrowProBono int NOT NULL,
	effective_date datetime NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_vendor_products.EscrowProBono'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_vendor_products.effective_date'
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_products ON
GO
IF EXISTS(SELECT * FROM sysobjects where type='U' and name=N'vendor_products')
BEGIN
	EXEC('INSERT INTO dbo.Tmp_vendor_products (oID, vendor, product, EscrowProBono, effective_date, date_created, created_by)
		SELECT oID, vendor, product, EscrowProBono, effective_date, date_created, created_by FROM dbo.vendor_products WITH (HOLDLOCK TABLOCKX)')
    EXEC('DROP TABLE dbo.vendor_products')
END
GO
SET IDENTITY_INSERT dbo.Tmp_vendor_products OFF
GO
EXECUTE sp_rename N'dbo.Tmp_vendor_products', N'vendor_products', 'OBJECT' 
GO
COMMIT
GO

-- SECTION: OLD TABLES
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'client_product_notes'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_product_notes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client_product dbo.typ_key NOT NULL,
	type int NOT NULL,
	dont_edit bit NOT NULL,
	dont_delete bit NOT NULL,
	dont_print bit NOT NULL,
	subject dbo.typ_subject NULL,
	note text NULL,
	date_created datetime NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'This table holds all of the client_product notes.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Primary key. Sequential number for the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'oID'
GO
DECLARE @v sql_variant 
SET @v = N'client_product for whom the message applies'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'client_product'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'type'
GO
DECLARE @v sql_variant 
SET @v = N'If set, do not allow others to change the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'dont_edit'
GO
DECLARE @v sql_variant 
SET @v = N'If set, do not allow others to delete this note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'dont_delete'
GO
DECLARE @v sql_variant 
SET @v = N'If set, do not allow others to print the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'dont_print'
GO
DECLARE @v sql_variant 
SET @v = N'Subject for the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'subject'
GO
DECLARE @v sql_variant 
SET @v = N'Text for the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'note'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_product_notes', N'COLUMN', N'created_by'
GO
EXECUTE sp_bindefault N'dbo.default_note_type', N'dbo.Tmp_client_product_notes.type'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_product_notes.dont_edit'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_product_notes.dont_delete'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_product_notes.dont_print'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_client_product_notes.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_client_product_notes ON
GO
IF EXISTS(SELECT * FROM dbo.client_product_notes)
	 EXEC('INSERT INTO dbo.Tmp_client_product_notes (oID, client_product, type, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by)
		SELECT oID, client_product, type, dont_edit, dont_delete, dont_print, subject, note, date_created, created_by FROM dbo.client_product_notes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_client_product_notes OFF
GO
DROP TABLE dbo.client_product_notes
GO
EXECUTE sp_rename N'dbo.Tmp_client_product_notes', N'client_product_notes', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
EXECUTE UPDATE_drop_constraints 'ach_onetimes'
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'bank', 'int'
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'client_product', 'int'
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'batch_type', 'varchar(3)'
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'temp_fhlb_firm_name', 'varchar(80)'
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'temp_fhlb_email', 'varchar(256)'
EXECUTE UPDATE_ADD_COLUMN 'ach_onetimes', 'temp_fhlb_firm_phone', 'varchar(50)'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_ach_onetimes
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	client dbo.typ_client NOT NULL,
	bank dbo.typ_key NOT NULL,
	batch_type varchar(3) NOT NULL,
	deposit_batch_id dbo.typ_key NULL,
	deposit_batch_date datetime NULL,
	ABA varchar(9) NOT NULL,
	AccountNumber varchar(17) NOT NULL,
	CheckingSavings char(1) NOT NULL,
	Amount money NOT NULL,
	EffectiveDate datetime NOT NULL,
	client_product dbo.typ_key NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL,
	temp_fhlb_firm_name varchar(80) NULL,
	temp_fhlb_email varchar(256) NULL,
	temp_fhlb_firm_phone varchar(50) NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_ach_onetimes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_ach_onetimes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_ach_onetimes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_ach_onetimes TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the client_products table if this is a non-DMP transaction. Otherwise, for DMP, this is NULL'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_ach_onetimes', N'COLUMN', N'client_product'
GO
ALTER TABLE dbo.Tmp_ach_onetimes ADD CONSTRAINT DF_ach_onetimes_bank DEFAULT ([dbo].[default_ach_bank]()) FOR bank
GO
EXECUTE sp_bindefault N'dbo.default_ach_batch_type', N'dbo.Tmp_ach_onetimes.batch_type'
GO
EXECUTE sp_bindefault N'dbo.default_CheckingSavings', N'dbo.Tmp_ach_onetimes.CheckingSavings'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_ach_onetimes.Amount'
GO
SET IDENTITY_INSERT dbo.Tmp_ach_onetimes ON
GO
IF EXISTS(SELECT * FROM dbo.ach_onetimes)
	 EXEC('INSERT INTO dbo.Tmp_ach_onetimes (oID, client, bank, batch_type, deposit_batch_id, deposit_batch_date, ABA, AccountNumber, CheckingSavings, Amount, EffectiveDate, client_product, date_created, created_by, temp_fhlb_firm_name, temp_fhlb_email, temp_fhlb_firm_phone)
		SELECT oID, client, isnull(bank,4), isnull(batch_type,''TEL''), deposit_batch_id, deposit_batch_date, ABA, AccountNumber, CheckingSavings, Amount, EffectiveDate, client_product, date_created, created_by, temp_fhlb_firm_name, temp_fhlb_email, temp_fhlb_firm_phone FROM dbo.ach_onetimes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_ach_onetimes OFF
GO
DROP TABLE dbo.ach_onetimes
GO
EXECUTE sp_rename N'dbo.Tmp_ach_onetimes', N'ach_onetimes', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_add_column 'states', 'UseInWeb', 'bit'
EXECUTE UPDATE_add_column 'states', 'NegativeDebtWarning', 'bit'
EXECUTE UPDATE_drop_constraints 'states'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_states
	(
	state dbo.typ_key NOT NULL IDENTITY (1001, 1) NOT FOR REPLICATION,
	MailingCode varchar(4) NULL,
	Name dbo.typ_description NOT NULL,
	Country dbo.typ_key NOT NULL,
	TimeZoneID dbo.typ_key NOT NULL,
	USAFormat int NOT NULL,
	[Default] bit NOT NULL,
	ActiveFlag bit NOT NULL,
	UseInWeb bit NOT NULL,
	DoingBusiness bit NOT NULL,
	NegativeDebtWarning bit NOT NULL,
	AddressFormat varchar(256) NOT NULL,
	created_by varchar(80) NOT NULL,
	date_created datetime NOT NULL,
	ts timestamp NOT NULL,
	FIPS varchar(2) NULL,
	hud_9902 int NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_states TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_states TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_states TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_states TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Mailing code for state names. US format only. Normally two letters.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'MailingCode'
GO
DECLARE @v sql_variant 
SET @v = N'Descriptive name of the state'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'Name'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the country table for this state entry.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'Country'
GO
DECLARE @v sql_variant 
SET @v = N'IF this is a US state, this flag is set. It means that the postal codes are zipcode format.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'USAFormat'
GO
DECLARE @v sql_variant 
SET @v = N'Is this the default item?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'Default'
GO
DECLARE @v sql_variant 
SET @v = N'Can the value be selected and changed to this item?'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'ActiveFlag'
GO
DECLARE @v sql_variant 
SET @v = N'List this state on the web page for a choice by a user'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'UseInWeb'
GO
DECLARE @v sql_variant 
SET @v = N'How to format the last line of the address. {0} is the city, {1} is the Mailing code, {2} is the postal code, and {3} is the Country'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'AddressFormat'
GO
DECLARE @v sql_variant 
SET @v = N'Used in reporting the locality to H.U.D.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'hud_9902'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the record'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the record was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_states', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_states.Country'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_states.TimeZoneID'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.USAFormat'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.[Default]'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_states.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.UseInWeb'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.DoingBusiness'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_states.NegativeDebtWarning'
GO
ALTER TABLE dbo.Tmp_states ADD CONSTRAINT DF_states_hud_9902 DEFAULT ((61)) FOR hud_9902
GO
EXECUTE sp_bindefault N'dbo.default_counselor', N'dbo.Tmp_states.created_by'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_states.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_states ON
GO
IF EXISTS(SELECT * FROM dbo.states)
	 EXEC('INSERT INTO dbo.Tmp_states (state, MailingCode, Name, Country, TimeZoneID, USAFormat, [Default], ActiveFlag, UseInWeb, DoingBusiness, NegativeDebtWarning, AddressFormat, FIPS, hud_9902, created_by, date_created)
		SELECT state, MailingCode, Name, Country, TimeZoneID, USAFormat, [Default], ActiveFlag, ISNULL(UseInWeb,1), DoingBusiness, NegativeDebtWarning, AddressFormat, FIPS, hud_9902, created_by, date_created FROM dbo.states WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_states OFF
GO
DROP TABLE dbo.states
GO
EXECUTE sp_rename N'dbo.Tmp_states', N'states', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'StateMessages'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_StateMessages
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	State dbo.typ_key NOT NULL,
	StateMessageType dbo.typ_key NOT NULL,
	Effective datetime NOT NULL,
	Details text NOT NULL,
	Reason varchar(512) NULL,
	date_updated dbo.typ_date NOT NULL,
	updated_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_StateMessages TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_StateMessages TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_StateMessages TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_StateMessages TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_StateMessages.Effective'
GO
SET IDENTITY_INSERT dbo.Tmp_StateMessages ON
GO
IF EXISTS(SELECT * FROM dbo.StateMessages)
	 EXEC('INSERT INTO dbo.Tmp_StateMessages (oID, State, StateMessageType, Effective, Details, Reason, date_updated, updated_by, date_created, created_by)
		SELECT oID, State, StateMessageType, Effective, Details, Reason, date_updated, updated_by, date_created, created_by FROM dbo.StateMessages WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_StateMessages OFF
GO
DROP TABLE dbo.StateMessages
GO
EXECUTE sp_rename N'dbo.Tmp_StateMessages', N'StateMessages', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
execute UPDATE_drop_constraints 'people'
execute UPDATE_add_column 'people',	'bkCertificateNo', 'varchar(50)'
execute UPDATE_add_column 'people', 'bkCertificateIssued', 'datetime'
execute UPDATE_add_column 'people', 'bkCertificateExpires', 'datetime'
execute UPDATE_add_column 'people', 'bkCaseNumber', 'varchar(50)'
execute UPDATE_add_column 'people', 'bkDistrict', 'int'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_people
	(
	Person dbo.typ_key NOT NULL IDENTITY (1, 1),
	Client dbo.typ_client NOT NULL,
	Relation dbo.typ_key NOT NULL,
	NameID dbo.typ_key NULL,
	WorkTelephoneID dbo.typ_key NULL,
	CellTelephoneID dbo.typ_key NULL,
	EmailID dbo.typ_key NULL,
	Former dbo.typ_lname NULL,
	SSN dbo.typ_ssn NULL,
	Gender dbo.typ_key NOT NULL,
	Race dbo.typ_key NOT NULL,
	Ethnicity dbo.typ_key NOT NULL,
	Disabled bit NOT NULL,
	MilitaryStatusID dbo.typ_key NOT NULL,
	MilitaryServiceID dbo.typ_key NOT NULL,
	MilitaryGradeID dbo.typ_key NOT NULL,
	MilitaryDependentID dbo.typ_key NOT NULL,
	Education dbo.typ_key NOT NULL,
	Birthdate datetime NULL,
	FICO_Score int NULL,
	CreditAgency varchar(10) NULL,
	no_fico_score_reason dbo.typ_key NULL,
	FICO_pull_date datetime NULL,
	gross_income money NOT NULL,
	net_income money NOT NULL,
	Frequency dbo.typ_key NOT NULL,
	emp_start_date datetime NULL,
	emp_end_date datetime NULL,
	employer dbo.typ_key NULL,
	job dbo.typ_key NULL,
	other_job dbo.typ_description NULL,
	bkfileddate datetime NULL,
	bkdischarge datetime NULL,
	bkchapter dbo.typ_key NULL,
	bkPaymentCurrent int NULL,
	bkAuthLetterDate datetime NULL,
	bkCertificateNo varchar(50) NULL,
	bkCertificateIssued datetime NULL,
	bkCertificateExpires datetime NULL,
	bkDistrict dbo.typ_key NULL,
	bkCaseNumber varchar(50) NULL,
	SPOC_ID int NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created dbo.typ_date NOT NULL,
	ts timestamp NOT NULL,
	POA_Name varchar(50) NULL,
	poa datetime NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'Client ID associated with the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Client'
GO
DECLARE @v sql_variant 
SET @v = N'Relationship to the applicant (self, spouse, child, etc.)'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Relation'
GO
DECLARE @v sql_variant 
SET @v = N'Former name of the person if indicated'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Former'
GO
DECLARE @v sql_variant 
SET @v = N'Social security number of the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'SSN'
GO
DECLARE @v sql_variant 
SET @v = N'Gender for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Gender'
GO
DECLARE @v sql_variant 
SET @v = N'Racial background for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Race'
GO
DECLARE @v sql_variant 
SET @v = N'Ethnic background for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Ethnicity'
GO
DECLARE @v sql_variant 
SET @v = N'Number of years of formal education for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Education'
GO
DECLARE @v sql_variant 
SET @v = N'Birthdate of the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'Birthdate'
GO
DECLARE @v sql_variant 
SET @v = N'Fair Issac''s Company score for this person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'FICO_Score'
GO
DECLARE @v sql_variant 
SET @v = N'Date when the FICO score was last updated'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'FICO_pull_date'
GO
DECLARE @v sql_variant 
SET @v = N'Occupation gross income amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'gross_income'
GO
DECLARE @v sql_variant 
SET @v = N'Occupation net income amount'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'net_income'
GO
DECLARE @v sql_variant 
SET @v = N'Starting date of employment'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'emp_start_date'
GO
DECLARE @v sql_variant 
SET @v = N'Ending date of employment'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'emp_end_date'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the employer for the person'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'employer'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the system defined job'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'job'
GO
DECLARE @v sql_variant 
SET @v = N'If the job is not listed then this is the text description'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'other_job'
GO
DECLARE @v sql_variant 
SET @v = N'Bankruptcy Certificate Number'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkCertificateNo'
GO
DECLARE @v sql_variant 
SET @v = N'Date bankruptcy certificate issued'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkCertificateIssued'
GO
DECLARE @v sql_variant 
SET @v = N'Date bankruptcy certificate expires'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkCertificateExpires'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the bankruptcy district where the action was filed'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'bkDistrict'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the row'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time when the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Date of the power-of-attorney document. NULL if none is known.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_people', N'COLUMN', N'poa'
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_relation DEFAULT ([dbo].[Default_Relation]()) FOR Relation
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_Gender DEFAULT ([dbo].[Default_Gender]()) FOR Gender
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_Race DEFAULT ([dbo].[Default_Race]()) FOR Race
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_Ethnicity DEFAULT ([dbo].[default_Ethnicity]()) FOR Ethnicity
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_people.Disabled'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryStatusID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryServiceID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryGradeID'
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.MilitaryDependentID'
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_Education DEFAULT ([dbo].[Default_Education]()) FOR Education
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_CreditAgency DEFAULT ('EQUIFAX') FOR CreditAgency
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_no_fico_score_reason DEFAULT ([dbo].[Default_Housing_FICONotIncludedReason]()) FOR no_fico_score_reason
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_people.gross_income'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_people.net_income'
GO
ALTER TABLE dbo.Tmp_people ADD CONSTRAINT DF_people_Frequency DEFAULT ([dbo].[default_PayFrequency]()) FOR Frequency
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_people.bkPaymentCurrent'
GO
SET IDENTITY_INSERT dbo.Tmp_people ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_people.SSN'
GO
IF EXISTS(SELECT * FROM dbo.people)
	 EXEC('INSERT INTO dbo.Tmp_people (Person, Client, Relation, NameID, WorkTelephoneID, CellTelephoneID, EmailID, Former, SSN, Gender, Race, Ethnicity, Disabled, MilitaryStatusID, MilitaryServiceID, MilitaryGradeID, MilitaryDependentID, Education, Birthdate, FICO_Score, CreditAgency, no_fico_score_reason, FICO_pull_date, gross_income, net_income, Frequency, emp_start_date, emp_end_date, employer, job, other_job, bkfileddate, bkdischarge, bkchapter, bkPaymentCurrent, bkAuthLetterDate, bkCertificateNo, bkCertificateIssued, bkCertificateExpires, bkDistrict, SPOC_ID, created_by, date_created, POA_Name, poa, bkCaseNumber)
		SELECT Person, Client, Relation, NameID, WorkTelephoneID, CellTelephoneID, EmailID, Former, SSN, Gender, Race, Ethnicity, Disabled, MilitaryStatusID, MilitaryServiceID, MilitaryGradeID, MilitaryDependentID, Education, Birthdate, FICO_Score, CreditAgency, no_fico_score_reason, FICO_pull_date, gross_income, net_income, Frequency, emp_start_date, emp_end_date, employer, job, other_job, bkfileddate, bkdischarge, bkchapter, bkPaymentCurrent, bkAuthLetterDate, bkCertificateNo, bkCertificateIssued, bkCertificateExpires, bkDistrict, SPOC_ID, created_by, date_created, POA_Name, poa, bkCaseNumber FROM dbo.people WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_people OFF
GO
DROP TABLE dbo.people
GO
EXECUTE sp_rename N'dbo.Tmp_people', N'people', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
EXECUTE UPDATE_drop_constraints 'Housing_lender_servicers'
execute UPDATE_add_column 'Housing_lender_servicers', 'hlp_servicer', 'bit'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_Housing_lender_servicers
	(
	oID dbo.typ_key NOT NULL IDENTITY (1, 1),
	ServiceID int NULL,
	RxOffice int NULL,
	description dbo.typ_description NOT NULL,
	ActiveFlag bit NOT NULL,
	[default] bit NOT NULL,
	IsOther bit NOT NULL,
	hlp_servicer bit NOT NULL,
	Comment varchar(80) NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_Housing_lender_servicers TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_Housing_lender_servicers.ActiveFlag'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_lender_servicers.[default]'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_lender_servicers.IsOther'
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_Housing_lender_servicers.hlp_servicer'
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_lender_servicers ON
GO
IF EXISTS(SELECT * FROM dbo.Housing_lender_servicers)
	 EXEC('INSERT INTO dbo.Tmp_Housing_lender_servicers (oID, ServiceID, RxOffice, description, ActiveFlag, [default], IsOther, hlp_servicer, Comment)
		SELECT oID, ServiceID, RxOffice, description, ActiveFlag, [default], IsOther, isnull(hlp_servicer,0), Comment FROM dbo.Housing_lender_servicers WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_Housing_lender_servicers OFF
GO
DROP TABLE dbo.Housing_lender_servicers
GO
EXECUTE sp_rename N'dbo.Tmp_Housing_lender_servicers', N'Housing_lender_servicers', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_EmailAddresses
	(
	Email dbo.typ_key NOT NULL IDENTITY (11, 1) NOT FOR REPLICATION,
	Address varchar(800) NULL,
	Validation int NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_EmailAddresses TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_EmailAddresses TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_EmailAddresses TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_EmailAddresses TO public  AS dbo
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_EmailAddresses.Validation'
GO
SET IDENTITY_INSERT dbo.Tmp_EmailAddresses ON
GO
IF EXISTS(SELECT * FROM dbo.EmailAddresses)
	 EXEC('INSERT INTO dbo.Tmp_EmailAddresses (Email, Address, Validation)
		SELECT Email, Address, Validation FROM dbo.EmailAddresses WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_EmailAddresses OFF
GO
DROP TABLE dbo.EmailAddresses
GO
EXECUTE sp_rename N'dbo.Tmp_EmailAddresses', N'EmailAddresses', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
EXECUTE UPDATE_drop_constraints 'deposit_batch_ids'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_deposit_batch_ids
	(
	deposit_batch_id dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	batch_type varchar(2) NOT NULL,
	bank dbo.typ_key NOT NULL,
	trust_register dbo.typ_key NULL,
	ach_pull_date datetime NULL,
	ach_settlement_date varchar(3) NULL,
	ach_effective_date datetime NULL,
	ach_file varchar(4096) NULL,
	note dbo.typ_description NULL,
	date_closed datetime NULL,
	date_posted datetime NULL,
	posted_by varchar(80) NULL,
	date_created datetime NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_deposit_batch_ids TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'For each deposit batch, an id is generated from this table. This table is referenced by the deposit_batch_details table that holds the details of the deposit batch.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Deposit batch ID. This is the primary key.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'deposit_batch_id'
GO
DECLARE @v sql_variant 
SET @v = N'Type of the deposit batch. ''CL'' is a client deposit batch. ''CR'' is a creditor refund batch. ''AC'' is ACH (a flavor of DP).'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'batch_type'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the bank table to receive the deposit.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'bank'
GO
DECLARE @v sql_variant 
SET @v = N'When the batch is posted to the trust, this is the trust register ID.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'trust_register'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the pull date for the clients. It must match the deposit dates in the client_deposits table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_pull_date'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the bank''s settlement date.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_settlement_date'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, this is the effective date of the money transfer. This is a required field for ACH transactions.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_effective_date'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH, we record the filename on the local disk system for the ACH file.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'ach_file'
GO
DECLARE @v sql_variant 
SET @v = N'User defined note text'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'note'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time when the batch was closed. A closed batch posts the item to the trust register.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'date_closed'
GO
DECLARE @v sql_variant 
SET @v = N'Date and time when the deposits are actually posted to the client trust account.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'date_posted'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who posted file batch'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'posted_by'
GO
DECLARE @v sql_variant 
SET @v = N'Date/Time when the row was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_ids', N'COLUMN', N'created_by'
GO
ALTER TABLE dbo.Tmp_deposit_batch_ids ADD CONSTRAINT DF_deposit_batch_ids_batch_type DEFAULT ('CL') FOR batch_type
GO
ALTER TABLE dbo.Tmp_deposit_batch_ids ADD CONSTRAINT DF_deposit_batch_ids_bank DEFAULT (1) FOR bank
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_deposit_batch_ids.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_ids ON
GO
IF EXISTS(SELECT * FROM dbo.deposit_batch_ids)
	 EXEC('INSERT INTO dbo.Tmp_deposit_batch_ids (deposit_batch_id, batch_type, bank, trust_register, ach_pull_date, ach_settlement_date, ach_effective_date, ach_file, note, date_closed, date_posted, posted_by, date_created, created_by)
		SELECT deposit_batch_id, batch_type, bank, trust_register, ach_pull_date, ach_settlement_date, ach_effective_date, ach_file, note, date_closed, date_posted, posted_by, date_created, created_by FROM dbo.deposit_batch_ids WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_ids OFF
GO
DROP TABLE dbo.deposit_batch_ids
GO
EXECUTE sp_rename N'dbo.Tmp_deposit_batch_ids', N'deposit_batch_ids', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
EXECUTE UPDATE_drop_constraints 'deposit_batch_details'
EXECUTE UPDATE_add_column 'deposit_batch_details', 'client_product', 'int'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_deposit_batch_details
	(
	deposit_batch_detail dbo.typ_key NOT NULL IDENTITY (1, 1),
	deposit_batch_id dbo.typ_key NOT NULL,
	tran_subtype dbo.typ_transaction NULL,
	ok_to_post bit NOT NULL,
	scanned_client varchar(50) NULL,
	client dbo.typ_client NULL,
	creditor dbo.typ_creditor NULL,
	client_creditor dbo.typ_key NULL,
	client_product dbo.typ_key NULL,
	amount money NOT NULL,
	fairshare_amt money NULL,
	fairshare_pct dbo.typ_fairshare_rate NULL,
	creditor_type dbo.typ_creditor_type NULL,
	item_date datetime NOT NULL,
	reference dbo.typ_description NULL,
	message dbo.typ_description NULL,
	ach_transaction_code dbo.typ_rps_transaction NULL,
	ach_routing_number dbo.typ_ach_routing NULL,
	ach_account_number dbo.typ_ach_account NULL,
	ach_authentication_code dbo.typ_rps_result NULL,
	ach_response_batch_id dbo.typ_description NULL,
	date_posted datetime NULL,
	created_by dbo.typ_counselor NOT NULL,
	date_created datetime NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_deposit_batch_details TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'This table holds the deposit information until the deposit batch is posted.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', NULL, NULL
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table. This is simply an identity counter.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'deposit_batch_detail'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the deposit batch ID from the deposit_batch_ids table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'deposit_batch_id'
GO
DECLARE @v sql_variant 
SET @v = N'Subtype of the deposit. see tran_types table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'tran_subtype'
GO
DECLARE @v sql_variant 
SET @v = N'If true, the item is acceptable for being posted. There should not be an error.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ok_to_post'
GO
DECLARE @v sql_variant 
SET @v = N'If the source is a lockbox, this is the character string that represents the client ID as it came from the source. It may not be a valid client id.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'scanned_client'
GO
DECLARE @v sql_variant 
SET @v = N'If scanned_client is a number, this is the client number.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'client'
GO
DECLARE @v sql_variant 
SET @v = N'If the item is a creditor refund, this is the creditor associated with the refund.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'creditor'
GO
DECLARE @v sql_variant 
SET @v = N'Pointer to the client_products table if needed, otherwise NULL'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'client_product'
GO
DECLARE @v sql_variant 
SET @v = N'This is the dollar amount of the transaction.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'amount'
GO
DECLARE @v sql_variant 
SET @v = N'For creditor refunds, this is the amount of the deduction taken from the Z9999 creditor.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'fairshare_amt'
GO
DECLARE @v sql_variant 
SET @v = N'For creditor refunds, this is the associated percentage rate of the deduction / billed figure.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'fairshare_pct'
GO
DECLARE @v sql_variant 
SET @v = N'For creditor refunds, this is the deduct/none/billed status of the creditor at the time of the creditor refund.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'creditor_type'
GO
DECLARE @v sql_variant 
SET @v = N'This is the date of the instrument (check, money order, etc). It is for documentation purposes only.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'item_date'
GO
DECLARE @v sql_variant 
SET @v = N'This is the check number, money order number, etc of the deposit item.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'reference'
GO
DECLARE @v sql_variant 
SET @v = N'This is the reason why the item is not acceptable to be posted. It should be null if "ok_to_post" is 1.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'message'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the transaction code associated with the item. "27" is checking, "37" is savings, etc.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_transaction_code'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the ABA routing number with the checkdigit.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_routing_number'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the bank account number'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_account_number'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the response (error) code. If not-null, the ok_to_post should be 0.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_authentication_code'
GO
DECLARE @v sql_variant 
SET @v = N'For ACH deposit batches, this is the batch label associated with the response. It is used to print the report.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'ach_response_batch_id'
GO
DECLARE @v sql_variant 
SET @v = N'This is the date / time when the item is actually posted. It is used to prevent duplicate postings.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'date_posted'
GO
DECLARE @v sql_variant 
SET @v = N'This is the name of the person who created the deposit / refund transaction.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'created_by'
GO
DECLARE @v sql_variant 
SET @v = N'This is the date / time when the transaction was created.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_deposit_batch_details', N'COLUMN', N'date_created'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_deposit_batch_details.ok_to_post'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_deposit_batch_details.amount'
GO
EXECUTE sp_bindefault N'dbo.default_money', N'dbo.Tmp_deposit_batch_details.fairshare_amt'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_deposit_batch_details.item_date'
GO
EXECUTE sp_bindefault N'dbo.default_date', N'dbo.Tmp_deposit_batch_details.date_created'
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_details ON
GO
EXECUTE sp_unbindrule N'dbo.Tmp_deposit_batch_details.creditor'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_deposit_batch_details.fairshare_pct'
GO
EXECUTE sp_unbindrule N'dbo.Tmp_deposit_batch_details.creditor_type'
GO
IF EXISTS(SELECT * FROM dbo.deposit_batch_details)
	 EXEC('INSERT INTO dbo.Tmp_deposit_batch_details (deposit_batch_detail, deposit_batch_id, tran_subtype, ok_to_post, scanned_client, client, creditor, client_creditor, client_product, amount, fairshare_amt, fairshare_pct, creditor_type, item_date, reference, message, ach_transaction_code, ach_routing_number, ach_account_number, ach_authentication_code, ach_response_batch_id, date_posted, created_by, date_created)
		SELECT deposit_batch_detail, deposit_batch_id, tran_subtype, ok_to_post, scanned_client, client, creditor, client_creditor, client_product, amount, fairshare_amt, fairshare_pct, creditor_type, item_date, reference, message, ach_transaction_code, ach_routing_number, ach_account_number, ach_authentication_code, ach_response_batch_id, date_posted, created_by, date_created FROM dbo.deposit_batch_details WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_deposit_batch_details OFF
GO
DROP TABLE dbo.deposit_batch_details
GO
EXECUTE sp_rename N'dbo.Tmp_deposit_batch_details', N'deposit_batch_details', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_drop_constraints 'client_www_notes'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_www_notes
	(
	client_www_note dbo.typ_key NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	PKID uniqueidentifier NOT NULL,
	message dbo.typ_message NOT NULL,
	date_viewed datetime NULL,
	date_shown datetime NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.Tmp_client_www_notes TO public  AS dbo
GO
GRANT INSERT ON dbo.Tmp_client_www_notes TO public  AS dbo
GO
GRANT SELECT ON dbo.Tmp_client_www_notes TO public  AS dbo
GO
GRANT UPDATE ON dbo.Tmp_client_www_notes TO public  AS dbo
GO
DECLARE @v sql_variant 
SET @v = N'Primary key to the table.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www_notes', N'COLUMN', N'client_www_note'
GO
DECLARE @v sql_variant 
SET @v = N'Message text for the note'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www_notes', N'COLUMN', N'message'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time that the message was actually shown to the client. The client clicked on the "view" button.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www_notes', N'COLUMN', N'date_viewed'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the client clicked on the "delete" button.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www_notes', N'COLUMN', N'date_shown'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the message was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www_notes', N'COLUMN', N'date_created'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the note.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www_notes', N'COLUMN', N'created_by'
GO
SET IDENTITY_INSERT dbo.Tmp_client_www_notes ON
GO
IF EXISTS(SELECT * FROM dbo.client_www_notes)
	 EXEC('INSERT INTO dbo.Tmp_client_www_notes (client_www_note, PKID, message, date_viewed, date_shown, date_created, created_by)
		SELECT client_www_note, PKID, message, date_viewed, date_shown, date_created, created_by FROM dbo.client_www_notes WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_client_www_notes OFF
GO
DROP TABLE dbo.client_www_notes
GO
EXECUTE sp_rename N'dbo.Tmp_client_www_notes', N'client_www_notes', 'OBJECT' 
GO
COMMIT
GO
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_add_column 'client_www', 'DatabaseKeyID', 'int'
EXECUTE UPDATE_drop_constraints 'client_www'
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_client_www
	(
	PKID uniqueidentifier NOT NULL,
	ApplicationName varchar(50) NOT NULL,
	UserName varchar(50) NOT NULL,
	Password varchar(50) NULL,
	Email varchar(200) NULL,
	PasswordQuestion varchar(50) NULL,
	PasswordAnswer varchar(80) NULL,
	Comment varchar(255) NULL,
	IsLockedOut bit NOT NULL,
	IsApproved bit NOT NULL,
	LastPasswordChangeDate datetime NOT NULL,
	LastLoginDate datetime NOT NULL,
	LastActivityDate datetime NOT NULL,
	LastLockoutDate datetime NULL,
	FailedPasswordAnswerAttemptCount int NOT NULL,
	FailedPasswordAnswerAttemptWindowStart datetime NOT NULL,
	FailedPasswordAttemptCount int NOT NULL,
	FailedPasswordAttemptWindowStart datetime NOT NULL,
	DatabaseKeyID dbo.typ_key NULL,
	CreationDate dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
DECLARE @v sql_variant 
SET @v = N'ID associated with the WWW access'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'UserName'
GO
DECLARE @v sql_variant 
SET @v = N'Password for the client. This is the MD5 hash of the password.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'Password'
GO
DECLARE @v sql_variant 
SET @v = N'E-mail address of the client associated with the WWW access'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'Email'
GO
DECLARE @v sql_variant 
SET @v = N'Question associated with the client when the account was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'PasswordQuestion'
GO
DECLARE @v sql_variant 
SET @v = N'Client''s answer to the question'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'PasswordAnswer'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the password was last changed by the client'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'LastPasswordChangeDate'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the client last "logged on" to the system'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'LastLoginDate'
GO
DECLARE @v sql_variant 
SET @v = N'Date / Time when the row was created'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'CreationDate'
GO
DECLARE @v sql_variant 
SET @v = N'Name of the person who created the row.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'Tmp_client_www', N'COLUMN', N'created_by'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT DF_client_www_PKID DEFAULT (newid()) FOR PKID
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT DF_client_www_ApplicationName DEFAULT ('/') FOR ApplicationName
GO
EXECUTE sp_bindefault N'dbo.default_false', N'dbo.Tmp_client_www.IsLockedOut'
GO
EXECUTE sp_bindefault N'dbo.default_true', N'dbo.Tmp_client_www.IsApproved'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT DF_client_www_LastPasswordChangeDate DEFAULT (getutcdate()) FOR LastPasswordChangeDate
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT DF_client_www_LastLoginDate DEFAULT (getutcdate()) FOR LastLoginDate
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT DF_client_www_LastActivityDate DEFAULT (getutcdate()) FOR LastActivityDate
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_www.FailedPasswordAnswerAttemptCount'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT DF_client_www_FailedPasswordAnswerAttemptWindowStart DEFAULT (getutcdate()) FOR FailedPasswordAnswerAttemptWindowStart
GO
EXECUTE sp_bindefault N'dbo.default_zero', N'dbo.Tmp_client_www.FailedPasswordAttemptCount'
GO
ALTER TABLE dbo.Tmp_client_www ADD CONSTRAINT DF_client_www_FailedPasswordAttemptWindowStart DEFAULT (getutcdate()) FOR FailedPasswordAttemptWindowStart
GO
IF EXISTS(SELECT * FROM dbo.client_www)
	 EXEC('INSERT INTO dbo.Tmp_client_www (PKID, ApplicationName, UserName, Password, Email, PasswordQuestion, PasswordAnswer, Comment, IsLockedOut, IsApproved, LastPasswordChangeDate, LastLoginDate, LastActivityDate, LastLockoutDate, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, DatabaseKeyID, CreationDate, created_by)
		SELECT PKID, ApplicationName, UserName, Password, Email, PasswordQuestion, PasswordAnswer, Comment, IsLockedOut, IsApproved, LastPasswordChangeDate, LastLoginDate, LastActivityDate, LastLockoutDate, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, DatabaseKeyID, CreationDate, created_by FROM dbo.client_www WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.client_www
GO
EXECUTE sp_rename N'dbo.Tmp_client_www', N'client_www', 'OBJECT' 
GO
COMMIT
GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
GO
EXECUTE UPDATE_DROP_CONSTRAINTS 'product_transactions'
GO
IF EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='U' AND NAME=N'product_transaction_types')
	EXEC ('DROP TABLE product_transaction_types')
GO
BEGIN TRANSACTION
GO
CREATE TABLE dbo.product_transaction_types
	(
	oID varchar(2) NOT NULL,
	Description dbo.typ_description NOT NULL,
	date_created dbo.typ_date NOT NULL,
	created_by dbo.typ_counselor NOT NULL,
	ts timestamp NOT NULL
	)  ON [PRIMARY]
GO
GRANT DELETE ON dbo.product_transaction_types TO public  AS dbo
GRANT INSERT ON dbo.product_transaction_types TO public  AS dbo
GRANT SELECT ON dbo.product_transaction_types TO public  AS dbo
GRANT UPDATE ON dbo.product_transaction_types TO public  AS dbo
GO
DENY DELETE ON dbo.product_transaction_types TO www_role  AS dbo 
DENY INSERT ON dbo.product_transaction_types TO www_role  AS dbo 
DENY SELECT ON dbo.product_transaction_types TO www_role  AS dbo 
DENY UPDATE ON dbo.product_transaction_types TO www_role  AS dbo 
GO
COMMIT
GO
INSERT [dbo].[product_transaction_types] ([oID], [Description], [date_created], [created_by]) VALUES (N'BB', N'Beginning Balance', CAST(N'2016-10-25T19:15:18.010' AS DateTime), N'sa')
INSERT [dbo].[product_transaction_types] ([oID], [Description], [date_created], [created_by]) VALUES (N'CD', N'Cleared Disputed Status', CAST(N'2017-03-30T15:54:44.603' AS DateTime), N'sa')
INSERT [dbo].[product_transaction_types] ([oID], [Description], [date_created], [created_by]) VALUES (N'DC', N'Change in Discount Amount', CAST(N'2017-04-06T16:10:50.137' AS DateTime), N'sa')
INSERT [dbo].[product_transaction_types] ([oID], [Description], [date_created], [created_by]) VALUES (N'DS', N'Disputed Amount', CAST(N'2017-03-30T15:54:44.597' AS DateTime), N'sa')
INSERT [dbo].[product_transaction_types] ([oID], [Description], [date_created], [created_by]) VALUES (N'IV', N'Invoiced Transaction', CAST(N'2016-12-21T11:09:16.410' AS DateTime), N'sa')
INSERT [dbo].[product_transaction_types] ([oID], [Description], [date_created], [created_by]) VALUES (N'PI', N'Pre-Invoice', CAST(N'2016-12-21T07:07:20.067' AS DateTime), N'sa')
INSERT [dbo].[product_transaction_types] ([oID], [Description], [date_created], [created_by]) VALUES (N'RC', N'Funds Received', CAST(N'2016-10-27T11:45:39.567' AS DateTime), N'sa')
GO

-- SECTION: PRIMARY KEYS
ALTER TABLE dbo.product_transaction_types ADD CONSTRAINT
	PK_product_transaction_types PRIMARY KEY CLUSTERED 
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.states ADD CONSTRAINT
	PK_states PRIMARY KEY CLUSTERED
	(
	state
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.StateMessages ADD CONSTRAINT
	PK_StateMessages PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.people ADD CONSTRAINT
	PK_people PRIMARY KEY CLUSTERED
	(
	Person
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.Housing_lender_servicers ADD CONSTRAINT
	PK_housing_lender_servicers PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.EmailAddresses ADD CONSTRAINT
	PK_EmailAddresses PRIMARY KEY CLUSTERED
	(
	Email
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.deposit_batch_ids ADD CONSTRAINT
	PK_deposit_batch_ids PRIMARY KEY CLUSTERED
	(
	deposit_batch_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT
	PK_deposit_batch_details PRIMARY KEY CLUSTERED
	(
	deposit_batch_detail
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_www_notes ADD CONSTRAINT
	PK_client_www_notes PRIMARY KEY CLUSTERED
	(
	client_www_note
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_www ADD CONSTRAINT
	PK_client_www PRIMARY KEY CLUSTERED
	(
	PKID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_product_notes ADD CONSTRAINT
	PK_client_product_notes PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT
	PK_ach_onetime PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.disputed_status_types ADD CONSTRAINT
	PK_disputed_status_types PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.PaymentMethodTypes ADD CONSTRAINT
	PK_PaymentMethodTypes PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.credit_card_payment_details ADD CONSTRAINT
	PK_credit_card_payment_details PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_products_history ADD CONSTRAINT
	PK_client_products_history PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT
	PK_client_products PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendors ADD CONSTRAINT
	PK_vendors PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_types ADD CONSTRAINT
	PK_vendor_types PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT
	PK_vendor_products PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_notes ADD CONSTRAINT
	PK_vendor_notes PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT
	PK_vendor_contacts PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_contact_types ADD CONSTRAINT
	PK_vendor_contact_types PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_addresses ADD CONSTRAINT
	PK_vendor_addresses PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.vendor_addkeys ADD CONSTRAINT
	PK_vendor_addkeys PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.products ADD CONSTRAINT
	PK_products PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT
	PK_product_transactions PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT
	PK_product_states PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.product_resolution_types ADD CONSTRAINT
	PK_product_resolution_types PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.product_dispute_types ADD CONSTRAINT
	PK_product_dispute_types PRIMARY KEY CLUSTERED
	(
	oID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- SECTION: INDEXES
CREATE NONCLUSTERED INDEX IX_states_1 ON dbo.states
	(
	MailingCode
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_1 ON dbo.people
	(
	Client,
	Relation
	) INCLUDE (NameID, Former, SSN)
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_2 ON dbo.people
	(
	CellTelephoneID
	) INCLUDE (Client)
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_3 ON dbo.people
	(
	WorkTelephoneID
	) INCLUDE (Client)
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_4 ON dbo.people
	(
	Former
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_people_5 ON dbo.people
	(
	SSN
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_People_6 ON dbo.people
	(
	EmailID
	) INCLUDE (Client)
 WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_people_7 ON dbo.people
	(
	Relation
	) INCLUDE (Client, NameID, CellTelephoneID, SSN)
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_people_8 ON dbo.people
	(
	Relation,
	NameID
	) INCLUDE (Client, SSN)
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_Email_Address_1 ON dbo.EmailAddresses
	(
	Address
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_ids_1 ON dbo.deposit_batch_ids
	(
	note
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_details_1 ON dbo.deposit_batch_details
	(
	deposit_batch_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_details_2 ON dbo.deposit_batch_details
	(
	ach_response_batch_id
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_deposit_batch_details_3 ON dbo.deposit_batch_details
	(
	deposit_batch_id,
	reference,
	ach_transaction_code,
	ach_routing_number,
	ach_account_number
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_www_notes_1 ON dbo.client_www_notes
	(
	PKID,
	date_viewed
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_www_1 ON dbo.client_www
	(
	UserName,
	ApplicationName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_client_www_2 ON dbo.client_www
	(
	Email,
	ApplicationName
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_client_product_notes_1 ON dbo.client_product_notes
	(
	client_product
	) INCLUDE (oID, type, dont_edit, dont_delete, dont_print, subject, date_created, created_by)
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX ix_client_product_notes_2 ON dbo.client_product_notes
	(
	oID,
	subject
	) INCLUDE (created_by)
 WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ach_onetime_1 ON dbo.ach_onetimes
	(
	deposit_batch_id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_ach_onetime_2 ON dbo.ach_onetimes
	(
	client
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 90, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_vendors_1 ON dbo.vendors
	(
	Label
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
DECLARE @v sql_variant
SET @v = N'Index of vendor labels'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'vendors', N'INDEX', N'IX_vendors_1'
GO
CREATE UNIQUE NONCLUSTERED INDEX IX_vendor_addresses_1 ON dbo.vendor_addresses
	(
	vendor,
	address_type
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_vendor_addkeys_1 ON dbo.vendor_addkeys
	(
	vendor
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_product_transactions_1 ON dbo.product_transactions
	(
	product,
	transaction_type
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


-- SECTION: DATA
TRUNCATE TABLE product_dispute_types
GO
SET IDENTITY_INSERT [dbo].[product_dispute_types] ON 
GO
INSERT [dbo].[product_dispute_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (1, N'Not my Client', 1, 0, CAST(N'2016-10-11 16:33:32.993' AS DateTime), N'sa')
GO
INSERT [dbo].[product_dispute_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (2, N'Duplicate', 1, 0, CAST(N'2016-10-11 16:33:33.020' AS DateTime), N'sa')
GO
INSERT [dbo].[product_dispute_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (3, N'Pro Bono', 1, 0, CAST(N'2016-10-11 16:33:33.027' AS DateTime), N'sa')
GO
INSERT [dbo].[product_dispute_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (5, N'Other', 1, 0, CAST(N'2017-01-30 14:59:29.193' AS DateTime), N'sa')
GO
INSERT [dbo].[product_dispute_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (6, N'Fee Waiver', 1, 0, CAST(N'2017-01-30 15:00:07.440' AS DateTime), N'sa')
GO
SET IDENTITY_INSERT [dbo].[product_dispute_types] OFF
GO
TRUNCATE TABLE product_resolution_types
GO
SET IDENTITY_INSERT [dbo].[product_resolution_types] ON 
GO
INSERT [dbo].[product_resolution_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (1, N'Resolution Reason #1', 1, 0, CAST(N'2016-10-11 16:36:13.850' AS DateTime), N'sa')
GO
INSERT [dbo].[product_resolution_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (2, N'Resolution Reason #2', 1, 0, CAST(N'2016-10-11 16:36:13.873' AS DateTime), N'sa')
GO
INSERT [dbo].[product_resolution_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (3, N'Resolution Reason #3', 1, 0, CAST(N'2016-10-11 16:36:13.880' AS DateTime), N'sa')
GO
SET IDENTITY_INSERT [dbo].[product_resolution_types] OFF
GO
TRUNCATE TABLE vendor_contact_types
GO
SET IDENTITY_INSERT [dbo].[vendor_contact_types] ON 
GO
INSERT [dbo].[vendor_contact_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (1, N'Main', 1, 0, CAST(N'2016-11-02 15:41:28.910' AS DateTime), N'2BDEBTFREE\caroline')
GO
INSERT [dbo].[vendor_contact_types] ([oID], [description], [ActiveFlag], [Default], [date_created], [created_by]) VALUES (2, N'Invoicing', 1, 0, CAST(N'2016-11-02 15:43:37.520' AS DateTime), N'2BDEBTFREE\caroline')
GO
SET IDENTITY_INSERT [dbo].[vendor_contact_types] OFF
GO
TRUNCATE TABLE vendor_types
GO
INSERT [dbo].[vendor_types] ([oID], [description], [ActiveFlag], [Default], [created_by], [date_created]) VALUES (N'A', N'Attorneys', 1, 0, N'sa', CAST(N'2016-07-26 15:42:47.170' AS DateTime))
GO
DELETE FROM email_templates WHERE oID IN (7, 8, 9)
GO
SET IDENTITY_INSERT [dbo].[email_templates] ON 
GO
INSERT [dbo].[email_templates] ([oID], [description], [language], [html_URI], [text_URI], [sender_email], [sender_name], [subject], [attachments], [date_created], [created_by]) VALUES (7, N'CreditCardExpiration', 1, N'file:///D:/only_this_gets_backed_up/shared/EmailServer/Templates/Products/CreditCardExpiration/HTML.html', N'file:///D:/only_this_gets_backed_up/shared/EmailServer/Templates/Products/CreditCardExpiration/Text.txt', N'DoNotReply@clearpointccs.org', N'Do No Reply', N'Credit Card Expiration', NULL, CAST(N'2016-10-28 10:31:47.263' AS DateTime), N'sa')
GO
INSERT [dbo].[email_templates] ([oID], [description], [language], [html_URI], [text_URI], [sender_email], [sender_name], [subject], [attachments], [date_created], [created_by]) VALUES (8, N'Products PreDischargeInvoice', 1, N'file:///D:/only_this_gets_backed_up/shared/EmailServer/Templates/Products/PreDischargeInvoice/HTML.html', N'file:///D:/only_this_gets_backed_up/shared/EmailServer/Templates/Products/PreDischargeInvoice/Text.txt', N'DoNotReply@clearpointccs.org', N'Do No Reply', N'Pre-Discharge Invoice Review', NULL, CAST(N'2016-10-28 10:32:18.390' AS DateTime), N'sa')
GO
SET IDENTITY_INSERT [dbo].[email_templates] OFF
GO
TRUNCATE TABLE products
GO
SET IDENTITY_INSERT [dbo].[products] ON 
GO
INSERT [dbo].[products] ([oID], [description], [Amount], [ActiveFlag], [Default], [InvoiceEmailTemplateID], [date_created], [created_by]) VALUES (1, N'BK Pre-File Internet', 50.0000, 1, 0, 9, CAST(N'2016-10-04 18:26:34.657' AS DateTime), N'sa')
GO
INSERT [dbo].[products] ([oID], [description], [Amount], [ActiveFlag], [Default], [InvoiceEmailTemplateID], [date_created], [created_by]) VALUES (2, N'BK Pre-Discharge Internet', 50.0000, 1, 0, 8, CAST(N'2016-10-04 18:26:53.613' AS DateTime), N'sa')
GO
SET IDENTITY_INSERT [dbo].[products] OFF
GO

-- SECTION: FOREIGN KEYS
ALTER TABLE dbo.DocumentAttributes ADD CONSTRAINT FK_DocumentAttributes_states FOREIGN KEY ( [State] ) REFERENCES dbo.states ( [state] ) ON UPDATE NO ACTION ON DELETE CASCADE
GO
ALTER TABLE dbo.Housing_lenders ADD CONSTRAINT FK_Housing_lenders_Housing_lender_servicers FOREIGN KEY ( [ServicerID] ) REFERENCES dbo.Housing_lender_servicers ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.PeopleFICOScoreReasons ADD CONSTRAINT FK_PeopleFICOScoreReasons_people FOREIGN KEY ( [PersonID] ) REFERENCES dbo.people ( [Person] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.StateMessages ADD CONSTRAINT FK_StateMessages_StateMessageTypes FOREIGN KEY ( [StateMessageType] ) REFERENCES dbo.StateMessageTypes ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.StateMessages ADD CONSTRAINT FK_StateMessages_states FOREIGN KEY ( [State] ) REFERENCES dbo.states ( [state] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.StateSetupFees ADD CONSTRAINT FK_StateSetupFees_states FOREIGN KEY ( [state] ) REFERENCES dbo.states ( [state] ) ON UPDATE NO ACTION ON DELETE CASCADE
GO
ALTER TABLE dbo.ZipCodeSearch ADD CONSTRAINT FK_ZipCodeSearch_states FOREIGN KEY ( [State] ) REFERENCES dbo.states ( [state] ) ON UPDATE CASCADE ON DELETE NO ACTION
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT FK_ach_onetimes_banks FOREIGN KEY ( [bank] ) REFERENCES dbo.banks ( [bank] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT FK_ach_onetimes_client_products FOREIGN KEY ( [client_product] ) REFERENCES dbo.client_products ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT FK_ach_onetimes_clients FOREIGN KEY ( [client] ) REFERENCES dbo.clients ( [client] ) ON UPDATE NO ACTION ON DELETE CASCADE
GO
ALTER TABLE dbo.ach_onetimes ADD CONSTRAINT FK_ach_onetimes_deposit_batch_ids FOREIGN KEY ( [deposit_batch_id] ) REFERENCES dbo.deposit_batch_ids ( [deposit_batch_id] ) ON UPDATE CASCADE ON DELETE SET NULL
GO
ALTER TABLE dbo.addresses ADD CONSTRAINT FK_Addresses_States FOREIGN KEY ( [state] ) REFERENCES dbo.states ( [state] ) ON UPDATE CASCADE ON DELETE NO ACTION
GO
ALTER TABLE dbo.client_product_notes ADD CONSTRAINT FK_client_product_notes_client_products FOREIGN KEY ( [client_product] ) REFERENCES dbo.client_products ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT FK_client_products_clients FOREIGN KEY ( [client] ) REFERENCES dbo.clients ( [client] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT FK_client_products_disputed_status_types FOREIGN KEY ( [disputed_status] ) REFERENCES dbo.disputed_status_types ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT FK_client_products_product_resolution_types FOREIGN KEY ( [resolution_status] ) REFERENCES dbo.product_resolution_types ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT FK_client_products_products FOREIGN KEY ( [product] ) REFERENCES dbo.products ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.client_products ADD CONSTRAINT FK_client_products_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.client_products_history ADD CONSTRAINT FK_client_products_history_client_products FOREIGN KEY ( [client_product] ) REFERENCES dbo.client_products ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.client_products_history ADD CONSTRAINT FK_client_products_history_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.client_www_notes ADD CONSTRAINT FK_client_www_notes_client_www FOREIGN KEY ( [PKID] ) REFERENCES dbo.client_www ( [PKID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.config_fees ADD CONSTRAINT FK_config_fees_states FOREIGN KEY ( [state] ) REFERENCES dbo.states ( [state] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.credit_card_payment_details ADD CONSTRAINT FK_credit_card_payment_details_client_products FOREIGN KEY ( [client_product] ) REFERENCES dbo.client_products ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.credit_card_payment_details ADD CONSTRAINT FK_credit_card_payment_details_clients FOREIGN KEY ( [client] ) REFERENCES dbo.clients ( [client] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT FK_deposit_batch_details_client_products FOREIGN KEY ( [client_product] ) REFERENCES dbo.client_products ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.deposit_batch_details ADD CONSTRAINT FK_deposit_batch_details_deposit_batch_ids FOREIGN KEY ( [deposit_batch_id] ) REFERENCES dbo.deposit_batch_ids ( [deposit_batch_id] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.people ADD CONSTRAINT FK_people_EducationTypes FOREIGN KEY ( [Education] ) REFERENCES dbo.EducationTypes ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.people ADD CONSTRAINT FK_people_GenderTypes FOREIGN KEY ( [Gender] ) REFERENCES dbo.GenderTypes ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.people ADD CONSTRAINT FK_people_Housing_FICONotIncludedReasons FOREIGN KEY ( [no_fico_score_reason] ) REFERENCES dbo.Housing_FICONotIncludedReasons ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.people ADD CONSTRAINT FK_people_RaceTypes FOREIGN KEY ( [Race] ) REFERENCES dbo.RaceTypes ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.people ADD CONSTRAINT FK_people_bankruptcy_districts FOREIGN KEY ( [bkDistrict] ) REFERENCES dbo.bankruptcy_districts ( [bankruptcy_district] ) ON UPDATE NO ACTION ON DELETE SET NULL
GO
ALTER TABLE dbo.people ADD CONSTRAINT FK_people_payfrequencytypes FOREIGN KEY ( [Frequency] ) REFERENCES dbo.PayFrequencyTypes ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT FK_product_states_products FOREIGN KEY ( [productID] ) REFERENCES dbo.products ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.product_states ADD CONSTRAINT FK_product_states_states FOREIGN KEY ( [stateID] ) REFERENCES dbo.states ( [state] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT CK_product_transactions CHECK ((NOT ([vendor] IS NULL AND [product] IS NULL AND [client_product] IS NULL)))
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT FK_product_transactions_client_products FOREIGN KEY ( [client_product] ) REFERENCES dbo.client_products ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT FK_product_transactions_products FOREIGN KEY ( [product] ) REFERENCES dbo.products ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT FK_product_transactions_vendor_transaction_types FOREIGN KEY ( [transaction_type] ) REFERENCES dbo.product_transaction_types ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.product_transactions ADD CONSTRAINT FK_product_transactions_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.products ADD CONSTRAINT FK_products_email_templates FOREIGN KEY ( [InvoiceEmailTemplateID] ) REFERENCES dbo.email_templates ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.states ADD CONSTRAINT FK_states_TimeZones FOREIGN KEY ( [TimeZoneID] ) REFERENCES dbo.TimeZones ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.states ADD CONSTRAINT FK_states_countries FOREIGN KEY ( [Country] ) REFERENCES dbo.countries ( [country] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
ALTER TABLE dbo.vendor_addkeys ADD CONSTRAINT FK_vendor_addkeys_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.vendor_addresses ADD CONSTRAINT FK_vendor_addresses_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT FK_vendor_contacts_vendor_contact_types FOREIGN KEY ( [contact_type] ) REFERENCES dbo.vendor_contact_types ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.vendor_contacts ADD CONSTRAINT FK_vendor_contacts_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.vendor_notes ADD CONSTRAINT FK_vendor_notes_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT FK_vendor_products_products FOREIGN KEY ( [product] ) REFERENCES dbo.products ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.vendor_products ADD CONSTRAINT FK_vendor_products_vendors FOREIGN KEY ( [vendor] ) REFERENCES dbo.vendors ( [oID] ) ON UPDATE CASCADE ON DELETE CASCADE
GO
ALTER TABLE dbo.vendors ADD CONSTRAINT FK_vendors_vendor_types FOREIGN KEY ( [Type] ) REFERENCES dbo.vendor_types ( [oID] ) ON UPDATE NO ACTION ON DELETE NO ACTION
GO
EXECUTE sp_bindrule N'dbo.valid_ssn', N'dbo.people.SSN'
GO
EXECUTE sp_bindrule N'dbo.valid_creditor', N'dbo.deposit_batch_details.creditor'
GO
EXECUTE sp_bindrule N'dbo.valid_fairshare_rate', N'dbo.deposit_batch_details.fairshare_pct'
GO
EXECUTE sp_bindrule N'dbo.valid_creditor_type', N'dbo.deposit_batch_details.creditor_type'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_BillingMode', N'dbo.vendors.BillingMode'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_CheckingSavings', N'dbo.vendors.ACH_CheckingSavings'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_ACH_RoutingNumber', N'dbo.vendors.ACH_RoutingNumber'
GO
EXECUTE sp_bindrule N'dbo.valid_MonthlyBillingDay', N'dbo.vendors.MonthlyBillingDay'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_type', N'dbo.vendor_types.oID'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_note_type', N'dbo.vendor_notes.type'
GO
EXECUTE sp_bindrule N'dbo.valid_vendor_address_type', N'dbo.vendor_addresses.address_type'
GO
DECLARE @v sql_variant
SET @v = N'Ensure that there is at least one primary reference in the transaction log. Empty records are not allowed.'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'product_transactions', N'CONSTRAINT', N'CK_product_transactions'
GO
DECLARE @v sql_variant
SET @v = N'Link the type of the vendor to the vendor_types table entries'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'vendors', N'CONSTRAINT', N'FK_vendors_vendor_types'
GO
DECLARE @v sql_variant
SET @v = N'Link the type of the vendor to the vendor_types table entries'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'vendors', N'CONSTRAINT', N'FK_vendors_vendor_types'
GO
DECLARE @v sql_variant
SET @v = N'Link StateSetupFees.state to States.state'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'StateSetupFees', N'CONSTRAINT', N'FK_StateSetupFees_states'
GO

-- SECTION: TRIGGERS
CREATE TRIGGER   [TRIG_people_D] on dbo.people AFTER DELETE as
-- ================================================================================
-- ==         Remove the table references when a person is removed               ==
-- ================================================================================
-- ChangeLog
--    1/1/2011
--       Initial version
set nocount on
-- Remove the contact name
delete		Names
from		Names n
inner join	deleted d on n.Name = d.NameID
-- Remove the cell telephone reference
delete		TelephoneNumbers
from		TelephoneNumbers t
inner join	deleted d on t.TelephoneNumber = d.CellTelephoneID
-- Remove the work telephone reference
delete		TelephoneNumbers
from		TelephoneNumbers t
inner join	deleted d on t.TelephoneNumber = d.WorkTelephoneID
-- Remove the email reference
delete		EmailAddresses
from		EmailAddresses e
inner join	deleted d on e.Email = d.EmailID
-- Remove references to the housing borrower id information
-- We just want to set the PersonID = 0 for any references to the deleted people.
update		housing_borrowers set PersonID = 0
from		deleted d
inner join	housing_properties prop on d.client = prop.HousingID
inner join	housing_borrowers b on prop.ownerID = b.oID
where		(case d.relation when 1 then 1 else 2 end) = b.PersonID
update		housing_borrowers set PersonID = 0
from		deleted d
inner join	housing_properties prop on d.client = prop.HousingID
inner join	housing_borrowers b on prop.coownerID = b.oID
where		(case d.relation when 1 then 1 else 2 end) = b.PersonID
-- Remove references in the debts table
update		client_creditor
set			Person = null
from		client_creditor cc
inner join deleted d on cc.client = d.client  -- use an indexed field to make things faster.
where		cc.Person is not null
and			cc.Person = d.Person;
-- Remove the references to the people_updates table
delete		people_updates
from		people_updates u
inner join	deleted d on d.Person = u.Person
GO
CREATE TRIGGER [dbo].[TRIG_people_FICO_U] ON dbo.people AFTER UPDATE AS
begin
	-- =================================================================================
	-- ==          Track changes to the FICO score                                    ==
	-- =================================================================================
	-- Supress intermediate results
	set	nocount on
	-- Look for changes to the FICO score
	if update(FICO_Score)
		insert into people_updates ( person, field, old_value, new_value )
		select	d.person, 'FICO_SCORE', convert(varchar, isnull(d.FICO_Score,0)), convert(varchar, isnull(i.FICO_Score,0))
		from	deleted d
		inner join inserted i on d.Person = i.Person
		where	isnull(i.FICO_Score,0) <> 0
		and		isnull(d.FICO_Score,0) <> 0
		and		isnull(d.FICO_Score,0) <> isnull(i.FICO_Score,0)
end
GO
create trigger trig_deposit_batch_details_D on dbo.deposit_batch_details after delete as
begin
	-- =====================================================================
	-- ==        Delete the reference to the deposit_batch_details        ==
	-- =====================================================================
	set nocount on
	-- Find the clients that are involved with the batches
	-- We want the client list independant of the transactions since the transactions may now be missing.
	select distinct client into #deposit_batch_details_clients from deleted where client is not null
	-- Compute the amount of money for the clients that are effected
	select	c.client, isnull(sum(d.amount),0) as amount
	into	#deposit_batch_details_amount
	from	#deposit_batch_details_clients c
	inner join deposit_batch_details d on c.client = d.client and d.amount is not null
	inner join deposit_batch_ids ids on d.deposit_batch_id = ids.deposit_batch_id
	where	ids.date_posted is null
	group by c.client;
	-- Correct the amount that the client has in the deposit batches
	update	clients
	set		deposit_in_trust = isnull(x.amount,0)
	from	clients c with (nolock)
	inner join #deposit_batch_details_clients a on c.client = a.client
	left outer join #deposit_batch_details_amount x on a.client = x.client;
	-- remove the temp tables
	drop table #deposit_batch_details_clients
	drop table #deposit_batch_details_amount
end
GO
CREATE trigger [dbo].[trig_vendors_I_label] on [dbo].[vendors] after insert as
begin
	-- TRIGGER to correct the vendor label field on the insert operation

	-- Do not count this as a result set.
	set nocount on

	-- Correct the vendor label
	update	vendors
	set		label = v.[type] + '-' + right('0000' + convert(varchar,v.[oID]), 4)
	from	vendors v
	inner join inserted i on v.oid = i.oid
	where	v.label is null
	and		v.[oID] < 10000

	-- Do the same without the leading zeros for larger numbers
	update	vendors
	set		label = v.[type] + '-' + convert(varchar,v.[oID])
	from	vendors v
	inner join inserted i on v.oid = i.oid
	where	v.label is null
	and		v.[oID] >= 10000
end
GO
CREATE trigger [dbo].[trig_vendors_D] on [dbo].[vendors] after delete as
begin
	-- Suppress the counter
	set nocount on

	-- Delete the www access for the vendor
	delete		client_www
	from		client_www w
	inner join	deleted d on w.DatabaseKeyId = d.oID
	where		w.ApplicationName = '/AttorneyPortal'
end
go
CREATE TRIGGER [dbo].[trig_vendor_contacts_D_1] ON dbo.vendor_contacts AFTER DELETE AS
BEGIN
	set nocount on
	-- Discard the names, addresses, etc. from the corresponding object tables
	delete		names
	from		names n
	inner join	deleted d on d.nameID = n.name
	where		d.nameID is not null;
	delete		addresses
	from		addresses a
	inner join	deleted d on d.addressID = a.address
	where		d.addressID is not null;
	delete		telephonenumbers
	from		telephonenumbers t
	inner join	deleted d on d.TelephoneID = t.telephonenumber
	where		d.TelephoneID is not null;
	delete		telephonenumbers
	from		telephonenumbers t
	inner join	deleted d on d.faxID = t.telephonenumber
	where		d.faxID is not null;
	delete		emailaddresses
	from		emailaddresses e
	inner join	deleted d on d.emailID = e.email
	where		d.emailID is not null;
END
GO
-- =============================================
-- Author:		Al Longyear
-- Description:	Drop reference fields on delete
-- =============================================
CREATE TRIGGER dbo.TRIG_vendor_addresses_D ON dbo.vendor_addresses AFTER DELETE AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SET NOCOUNT ON;
	-- Discard the address blocks since they are not re-used for anything
	-- but the one reference here.
	DELETE		addresses
	FROM		addresses a
	INNER JOIN	deleted d on a.Address = d.AddressID
END
GO
CREATE TRIGGER dbo.TRIG_StateMessages_U ON dbo.StateMessages AFTER UPDATE AS
BEGIN
	SET NOCOUNT ON;
	-- Set the update information where needed
	if (not update(date_updated)) and (not update(updated_by))
	begin
		update	StateMessages
		set		date_updated = getdate(),
				updated_by	 = suser_sname()
		from	StateMessages m
		inner join inserted i on m.oID = i.OID
	end
END
GO

-- SECTION: VIEWS
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(SELECT * FROM sysobjects WHERE type='V' AND name=N'view_clients_by_vendor')
    EXEC ('CREATE view [dbo].[view_clients_by_vendor] as SELECT 1 as ''object''');
GO
ALTER view [dbo].[view_clients_by_vendor] as
-- Retrieve the list of clients for a specified vendor
select		cp.vendor, c.active_status as status, cp.client, dbo.format_normal_name (n.prefix, n.first, n.middle, n.last, n.suffix) as name,
			prod.description as product, cp.cost - cp.discount as orig_balance, cp.tendered as principle_payments,
			cp.cost - cp.discount - cp.tendered - cp.disputed_amount as current_balance,
			t.payment as last_payment_amount, t.date_created as last_payment_date
from		client_products cp
inner join	clients c on cp.client = c.client
inner join	people p on cp.client = p.client and 1 = p.relation
inner join	products prod on cp.product = prod.oID
left outer join names n on p.nameid = n.name
left outer join product_transactions t on cp.last_payment = t.oID
GO

-- SECTION: STORED PROCEDURES
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'xpr_OrganizationAddress')
    EXEC ('CREATE PROCEDURE xpr_OrganizationAddress AS RETURN 0')
GO
ALTER PROCEDURE [dbo].[xpr_OrganizationAddress] AS
-- =================================================================================================
-- ==                             Fetch the Organization Address information                      ==
-- =================================================================================================

SELECT TOP 1
		convert(varchar(256),upper(isnull(c.name,'')))	AS 'name',
		convert(varchar(256),upper(dbo.format_Address_Line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value))) as 'addr1',
		convert(varchar(256),upper(isnull(a.address_line_2,''))) as 'addr2',
		convert(varchar(256),upper(isnull(dbo.format_city_state_zip (a.city, a.state, a.postalcode),''))) as 'addr3',
		convert(varchar(256),dbo.format_TelephoneNumber(c.TelephoneID )) as 'telephone',
		convert(varchar(10),isnull(a.PostalCode,'')) as 'zipcode'
FROM	config c WITH (NOLOCK)
LEFT OUTER JOIN addresses a WITH (NOLOCK) ON c.AddressID = a.Address
RETURN ( @@rowcount )
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'xpr_ACH_Post_Batch')
    EXEC ('CREATE PROCEDURE xpr_ACH_Post_Batch AS RETURN 0')
GO
ALTER PROCEDURE [dbo].[xpr_ACH_Post_Batch] ( @ACHFile AS INT ) AS
-- =================================================================================
-- ==          Post the ACH batch                                                 ==
-- =================================================================================

-- Start a transaction
BEGIN TRANSACTION

SET XACT_ABORT ON
SET NOCOUNT ON

BEGIN TRY
	DECLARE	@trust_register		INT
	DECLARE @effective_date		DateTime
	DECLARE	@total_amount		MONEY
	DECLARE	@note				varchar(50)
	DECLARE	@bank				int
	DECLARE	@current_date		datetime

	-- Magic marker for "there is none" in the case of a product operation.
	SET @trust_register = -1

	-- Fetch the information from the file header
	SELECT	@effective_date = ach_effective_date,
			@current_date	= getdate(),
			@bank			= bank,
			@note			= note
	FROM	deposit_batch_ids
	WHERE	deposit_batch_id = @ACHFile
	AND		date_posted is null

	IF @@rowcount < 1
		RaisError(50032, 16, 1, @ACHFile)

	-- Find the main trust account
	if @bank is null
		select	@bank		= min(bank)
		from	banks with (nolock)
		where	type		= 'C'
		and		[default]	= 1

	if @bank is null
		select	@bank		= min(bank)
		from	banks with (nolock)
		where	type		= 'C'

	-- If there is not one, punt.
	if @bank is null
		select	@bank = 1

	-- Correct the label for the subsequent update of the registers_trust table
	if @note is null
		select	@note = ''

	select	@note = ltrim(rtrim(@note))
	if @note <> ''
	begin
		select	@note = '#' + convert(varchar, @ACHFile) + ' ''' + @note
		if len(@note) > 49
			select	@note = left(@note, 49)
		select	@note = @note + ''''
	end else
		select	@note = '#' + convert(varchar, @ACHFile)

	-- Process a product transaction group first
	SELECT		deposit_batch_detail, client_product, convert(datetime, convert(varchar(10), date_created, 101) + ' 00:00:00') as date_created, amount
	INTO		#t_product_by_date
	FROM		deposit_batch_details d
	WHERE		deposit_batch_id = @ACHFile
	AND			ok_to_post = 1
	AND			client_product IS NOT NULL
	AND			date_posted IS NULL

	-- ------------------------------------------------------------------------------
	-- --       PRODUCTS                                                          ---
	-- ------------------------------------------------------------------------------

	IF EXISTS(SELECT * FROM #t_product_by_date)
	BEGIN
		-- Update the tendered amount for the various items
		update		client_products
		set			tendered = tendered + amount,
					invoice_status = 4
		from		client_products p
		inner join	#t_product_by_date x on p.oID = x.client_product

		-- Generate the detail transaction for the items
		insert into product_transactions (transaction_type, client_product, vendor, product, payment)
		select		'RC', x.client_product, p.vendor, p.product, x.amount
		from		#t_product_by_date x
		inner join	client_products p on x.client_product = p.oID

		-- Mark the items as having been posted
		update		deposit_batch_details
		set			date_posted			= getdate()
		from		deposit_batch_details d
		inner join #t_product_by_date x on d.deposit_batch_detail = x.deposit_batch_detail

		-- Indicate that the batch has been posted
		update		deposit_batch_ids
		SET			date_posted			= getdate(),
					posted_by			= suser_sname()
		WHERE		deposit_batch_id	= @ACHFile
	END
	drop table #t_product_by_date

	-- Find the ACH transactions that are for a DMP client
	SELECT		[deposit_batch_detail],[deposit_batch_id],[tran_subtype],[ok_to_post],[scanned_client],[client],[creditor],[client_creditor],[client_product],[amount],[fairshare_amt],[fairshare_pct],[creditor_type],[item_date],[reference],[message],[ach_transaction_code],[ach_routing_number],[ach_account_number],[ach_authentication_code],[ach_response_batch_id],[date_posted]
	INTO		#t_client_by_date
	FROM		deposit_batch_details d
	WHERE		deposit_batch_id = @ACHFile
	AND			ok_to_post = 1
	AND			amount > 0
	AND			client > 0
	AND			ach_transaction_code IN ('27', '37')
	AND			client_product IS NULL
	AND			date_posted IS NULL

	-- ------------------------------------------------------------------------------
	-- --       DMP                                                               ---
	-- ------------------------------------------------------------------------------

	IF EXISTS(SELECT * FROM #t_client_by_date)
	BEGIN
		-- Insert the item into the trust register
		insert into registers_trust	(tran_type, amount, cleared, bank, sequence_number, date_created)
		values				('DP',	0, ' ', @bank, @note, @current_date)
		select	@trust_register = SCOPE_IDENTITY()

		-- Insert the transactions into the system
		insert into registers_client (tran_type, tran_subtype, client, credit_amt, message, trust_register, item_date, date_created)
		select		'DP', 'AC', d.client, d.amount, d.reference, @trust_register, isnull(@effective_date,@current_date), @current_date
		FROM		#t_client_by_date d
		LEFT OUTER JOIN	ach_reject_codes e ON d.ach_authentication_code = e.ach_reject_code
		WHERE		( d.ach_authentication_code IS NULL OR isnull(e.serious_error,1) = 0 )

		-- Fetch the client and the deposit amount
		select		d.client, sum(d.amount) as amount
		into		#t_ach_deposits
		from		#t_client_by_date d
		LEFT OUTER JOIN	ach_reject_codes e ON d.ach_authentication_code = e.ach_reject_code
		where		( d.ach_authentication_code IS NULL OR isnull(e.serious_error,1) = 0 )
		GROUP BY	d.client

		-- Update the trust balance and last deposit date/amount
		update		clients
		set			held_in_trust		= isnull(c.held_in_trust,0)    + x.amount,
					deposit_in_trust	= isnull(c.deposit_in_trust,0) - x.amount,
					last_deposit_date	= @current_date,
					last_deposit_amount	= x.amount
		from		clients c
		inner join	#t_ach_deposits x on c.client = x.client

		-- Mark the items as having been posted
		update		deposit_batch_details
		set			date_posted			= getdate()
		from		deposit_batch_details d
		inner join #t_client_by_date x on d.deposit_batch_detail = x.deposit_batch_detail

		-- Set the first deposit date
		update		clients
		set			first_deposit_date	= @current_date
		from		clients c
		inner join	#t_ach_deposits x on c.client = x.client
		where		c.first_deposit_date is null

		-- Determine the total deposit amount
		SELECT		@total_amount = sum(amount)
		FROM		#t_ach_deposits

		-- Update the trust register with the deposit amount and cleared status
		UPDATE		registers_trust
		SET			amount = isnull(@total_amount,0),
					cleared = ' '
		WHERE		trust_register = @trust_register

		-- Update the retention events to expire items that are based upon deposits
		update		client_retention_events
		set			date_expired = @current_date
		from		client_retention_events e
		left outer join	#t_ach_deposits x on e.client = x.client and 2 = e.expire_type
		where		e.date_expired is null
		and			e.amount <= x.amount;

		drop table	#t_ach_deposits

		-- Find the list of clients which will have the error status changed
		select		c.client
		into		#ach_clients
		from		clients c with (nolock)
		inner join	#t_client_by_date d with (nolock) on c.client = d.client
		INNER JOIN	ach_reject_codes e WITH (NOLOCK) ON d.ach_authentication_code = e.ach_reject_code
		where		d.ach_authentication_code is not null
		AND			isnull(e.serious_error,1) > 0

		-- Set the error date to today for the clients
		UPDATE		client_ach
		SET			ErrorDate	= getdate()
		from		client_ach c
		inner join	#ach_clients ac on c.client = ac.client

		-- Generate a system note that the ACH failed
		insert into client_notes (client, type, dont_edit, dont_delete, subject, note, date_created)
		select		client, 3, 1, 1,
					'Deposit Information Changed',
					'Error date changed to ' + convert(varchar(10), @current_date, 101),
					@current_date
		from		#ach_clients

		-- Discard the working table
		drop table	#ach_clients

		-- Mark the item as "closed"
		UPDATE		deposit_batch_ids
		SET			date_posted = getdate(),
					trust_register = @trust_register
		WHERE		deposit_batch_id = @ACHFile
	END

	COMMIT TRANSACTION

	RETURN ( @trust_register )
END TRY

BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	if @@trancount > 0
	BEGIN
		BEGIN TRY
			ROLLBACK TRANSACTION
		END TRY
		BEGIN CATCH
		END CATCH
	END

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	return ( 0 )
END CATCH
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'xpr_ACH_Generate_File')
    EXEC ('CREATE PROCEDURE xpr_ACH_Generate_File AS RETURN 0')
GO
ALTER PROCEDURE [dbo].[xpr_ACH_Generate_File] (@bank as int = null) AS
-- =============================================================================
-- ==        Fetch the list of clients to be generated in the file            ==
-- =============================================================================

set nocount on

if @bank is null
	set @bank = dbo.default_ach_bank();

SELECT	t.deposit_batch_detail			as 'ach_transaction',
		t.deposit_batch_id				as 'ach_file',
		t.ach_transaction_code			as 'transaction_code',
		@bank							as 'bank',

		isnull(t.ach_routing_number,'MISSING')	as 'routing_number',
		isnull(t.ach_account_number,'MISSING')	as 'account_number',

		case	t.ach_transaction_code
			when '23' then 0
			when '33' then 0
			when '28' then 0
			when '38' then 0
				  else t.amount
		end					as 'amount',

		c.client				as 'client',
		null					as 'discretionary_data',
		t.reference				as 'trace_number',
		LEFT(upper(ltrim(rtrim(isnull(pn.last,'')+isnull(' '+pn.suffix,'')+','+isnull(' '+pn.first,'')+isnull(' '+pn.middle,'')))),22) as 'client_name'

FROM	deposit_batch_details t
INNER JOIN deposit_batch_ids id on t.deposit_batch_id = id.deposit_batch_id
INNER JOIN clients c ON t.client=c.client
LEFT OUTER JOIN people p ON c.client = p.client AND 1=p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE	c.active_status IN ('A', 'AR')
AND		t.reference IS NULL
AND		t.tran_subtype = 'AC'
AND		id.bank = @bank
ORDER BY t.ach_routing_number, t.ach_account_number

return ( @@rowcount )
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'rpt_LockboxDetails')
    EXEC ('CREATE PROCEDURE rpt_LockboxDetails AS RETURN 0')
GO
ALTER PROCEDURE [dbo].[rpt_LockboxDetails] ( @deposit_batch_id as int ) as

-- =================================================================================================
-- ==            Return the information for the lockbox report                                    ==
-- =================================================================================================

select
	d.deposit_batch_detail	as deposit_batch_detail,
	d.scanned_client		as scanned_client,
	d.client				as deposit_client,
	d.tran_subtype			as subtype,
	d.ok_to_post			as ok_to_post,
	d.amount				as amount,
	d.item_date				as item_date,
	d.reference				as reference,
	d.message				as message,
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name
from	deposit_batch_details d with (nolock)
left outer join people p with (nolock) on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
where	d.deposit_batch_id = @deposit_batch_id
order by deposit_batch_detail

return ( @@rowcount )
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'product_xpr_deposit_batch_post')
    EXEC ('CREATE PROCEDURE product_xpr_deposit_batch_post AS RETURN 0')
GO
ALTER PROCEDURE [dbo].[product_xpr_deposit_batch_post] ( @deposit_batch_id AS INT ) AS

-- ==================================================================================================
-- ==               Post the deposit batch                                                        ==
-- ==================================================================================================

-- Start a transaction
BEGIN TRANSACTION
SET XACT_ABORT ON
SET NOCOUNT ON

BEGIN TRY
	DECLARE	@bank				int
	DECLARE	@Client_Count		int
	DECLARE	@trust_register		int
	DECLARE	@product_count		int
	DECLARE	@date_closed		datetime
	DECLARE	@date_posted		datetime
	DECLARE	@date_created		datetime
	DECLARE	@Deposit_Amount		money
	DECLARE	@note				varchar(50)
	DECLARE	@deposit_batch_type	varchar(80)

	SET		@client_count   = 0
	SET		@Deposit_Amount = 0
	SET		@product_count  = 0
	SET		@trust_register	= -1

	SELECT	@date_created		= date_created,
			@date_closed		= date_closed,
			@date_posted		= date_posted,
			@bank				= bank,
			@note				= note,
			@deposit_batch_type	= batch_type
	FROM	deposit_batch_ids with (nolock)
	WHERE	deposit_batch_id	= @deposit_batch_id
	AND		batch_type		in ('AC', 'CL')

	IF @date_created IS NULL
		RaisError(50006, 16, 1, @deposit_batch_id)

	IF @date_posted IS NOT NULL
	BEGIN
		declare	@p1	varchar(10)
		SELECT	@p1 = convert(varchar(10), @date_posted, 1)
		RaisError(50083, 16, 1, @p1)
	END

	IF @date_closed IS NULL
		RaisError(50082, 16, 1, @deposit_batch_id)

	-- Correct the label for the subsequent update of the registers_trust table
	if @note is null
		select	@note = ''

	select	@note = ltrim(rtrim(@note))
	if @note <> ''
	begin
		select	@note = '#' + convert(varchar, @deposit_batch_id) + ' ''' + @note
		if len(@note) > 49
			select	@note = left(@note, 49)
		select	@note = @note + ''''
	end else
		select	@note = '#' + convert(varchar, @deposit_batch_id)

	-- Reset all deposit valid items if the client does not exist. It is never, never valid to deposit items to non-existent clients.
	UPDATE		deposit_batch_details
	SET			ok_to_post = 0
	FROM		deposit_batch_details d
	LEFT OUTER JOIN clients c WITH (NOLOCK) on d.client = c.client
	WHERE		d.deposit_batch_id = @deposit_batch_id
	AND			(c.client IS NULL OR d.date_posted IS NOT NULL)

	-- Force the transactions as "not ok to post" if there is an error on the ACH transaction
	if @deposit_batch_type = 'AC'
	begin
		update		deposit_batch_details
		set			ok_to_post = 0
		from		deposit_batch_details d
		where		d.deposit_batch_id = @deposit_batch_id
		and			ach_authentication_code is not null
	end

	-- Process a product transaction group first
	SELECT		d.deposit_batch_detail, d.client_product, convert(datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00') as date_created, d.amount
	INTO		#t_product_by_date
	FROM		deposit_batch_details d
	INNER JOIN	client_products p on d.client_product = p.oID
	WHERE		d.deposit_batch_id = @deposit_batch_id
	AND			d.ok_to_post = 1
	AND			p.invoice_status = 3
	AND			d.date_posted IS NULL

	-- Find the statistic information
	select		@product_count = count(*),
				@Deposit_Amount = sum(amount)
	from		#t_product_by_date

	IF @product_count > 0
	BEGIN
		-- Update the tendered amount for the various items
		update		client_products
		set			tendered = tendered + amount,
					invoice_status = 4
		from		client_products p
		inner join	#t_product_by_date x on p.oID = x.client_product

		-- Generate the detail transaction for the items
		insert into product_transactions (transaction_type, client_product, vendor, product, payment)
		select		'RC', x.client_product, p.vendor, p.product, x.amount
		from		#t_product_by_date x
		inner join	client_products p on x.client_product = p.oID

		-- Mark the items as having been posted
		update		deposit_batch_details
		set			date_posted			= getdate()
		from		deposit_batch_details d
		inner join #t_product_by_date x on d.deposit_batch_detail = x.deposit_batch_detail

		-- Indicate that the batch has been posted
		update		deposit_batch_ids
		SET			date_posted			= getdate(),
					posted_by			= suser_sname()
		WHERE		deposit_batch_id	= @deposit_batch_id
	END
	drop table #t_product_by_date

	-- Break the deposit items down by date for each client
	SELECT		client, convert(datetime, convert(varchar(10), date_created, 101) + ' 00:00:00') as date_created, sum(amount) as amount
	INTO		#t_deposit_by_date
	FROM		deposit_batch_details d
	WHERE		deposit_batch_id = @deposit_batch_id
	AND			ok_to_post = 1
	AND			client_product IS NULL
	AND			date_posted IS NULL

	-- Must do a "group by convert(datetime,...)" or we will update client's trust with only one of the deposits per batch.
	GROUP BY	client, convert(datetime, convert(varchar(10), date_created, 101) + ' 00:00:00')

	-- Create an index to make things work faster
	create index ix1_t_deposit_by_date ON #t_deposit_by_date ( client, date_created );

	-- Calculate the total amount of the deposit by client
	SELECT		client, sum(amount) as amount
	INTO		#t_deposit_by_client
	FROM		#t_deposit_by_date
	GROUP BY	client

	-- Determine the amount and the number of clients
	SELECT	@Client_Count	= count(*),
			@Deposit_Amount	= @Deposit_Amount + isnull(sum(amount),0)
	FROM	#t_deposit_by_client

	-- Look only for DMP clients. There may be product transactions so don't just die here.
	IF @Client_Count > 0
	BEGIN

		-- Create working indicies to make things work faster since this is the majority of the time here.
		create index ix1_t_deposit_by_client ON #t_deposit_by_client ( client, amount );
		create index ix2_t_deposit_by_client ON #t_deposit_by_client ( client );

		-- Update the retention events to expire items that are based upon deposits
		update		client_retention_events
		set			date_expired = getdate()
		from		#t_deposit_by_client x
		left outer join client_retention_events e on x.client = e.client and 2 = e.expire_type
		where		e.date_expired is null
		and			e.amount <= x.amount;

		-- Update the trust balances for the clients
		UPDATE		clients
		SET			held_in_trust		= isnull(c.held_in_trust,0)    + x.amount,
					deposit_in_trust	= isnull(c.deposit_in_trust,0) - x.amount
		FROM		clients c
		INNER JOIN	#t_deposit_by_client x on c.client = x.client;

		-- Update the last deposit date and amount for the client.
		UPDATE		clients
		SET			last_deposit_date	= x.date_created,
					last_deposit_amount	= x.amount
		FROM		clients c
		INNER JOIN	#t_deposit_by_date x ON c.client = x.client
		WHERE		c.client > 0

		-- Set the first deposit date
		update		clients
		set			first_deposit_date	= x.date_created
		from		clients c
		inner join	#t_deposit_by_date x on c.client = x.client
		where		c.client > 0
		and			c.first_deposit_date is null

		-- The $100.00 figure is just a shot in the dark.
		and			x.amount >= 100

		-- Insert the item into the deposit register
		insert into registers_trust (tran_type, amount, cleared, bank, sequence_number) values ('DP', @deposit_amount, ' ', isnull(@bank,1), @note)
		SELECT @trust_register = SCOPE_IDENTITY()

		IF @deposit_batch_type = 'AC'
			insert into registers_client	(tran_type,	client,		tran_subtype,	message,	item_date,	trust_register,		credit_amt)
			select			'DP',		client,		'AC',	reference,	item_date,	@trust_register,	amount
			from			deposit_batch_details
			where			deposit_batch_id = @deposit_batch_id
			and				date_posted is null
			and				ok_to_post = 1
			and				client > 0
		else
			insert into registers_client	(tran_type,	client,		tran_subtype,	message,	item_date,	trust_register,		credit_amt)
			select			'DP',		client,		tran_subtype,	reference,	item_date,	@trust_register,	amount
			from			deposit_batch_details
			where			deposit_batch_id = @deposit_batch_id
			and				date_posted is null
			and				ok_to_post = 1
			and				client > 0

		-- Mark the items as being deposited
		update	deposit_batch_details
		set		date_posted			= getdate()
		where	deposit_batch_id	= @deposit_batch_id
		AND		ok_to_post		> 0
		and		client			> 0

		-- Indicate that the batch has been posted
		update	deposit_batch_ids
		SET		date_posted			= getdate(),
				posted_by			= suser_sname(),
				trust_register		= @trust_register
		WHERE	deposit_batch_id	= @deposit_batch_id

		-- Find the list of clients which will have the error status changed
		if @deposit_batch_type = 'AC'
		BEGIN
			select		c.client
			into		#ach_clients
			from		clients c with (nolock)
			inner join	deposit_batch_details d with (nolock) on c.client = d.client
			INNER JOIN	ach_reject_codes e WITH (NOLOCK) ON d.ach_authentication_code = e.ach_reject_code
			where		d.deposit_batch_id = @deposit_batch_id
			AND			d.ach_authentication_code is not null
			AND			d.ach_transaction_code in ('27', '37')
			AND			isnull(e.serious_error,1) > 0
			AND			c.client > 0

			-- Set the error date to today for the clients
			UPDATE		client_ach
			SET			ErrorDate	= getdate()
			from		clients c
			inner join	#ach_clients ac on c.client = ac.client

			-- Generate a system note that the ACH failed
			insert into client_notes (client, type, dont_edit, dont_delete, subject, note, date_created)
			select		client, 3, 1, 1,
						'Deposit Information Changed',
						'Error date changed to ' + convert(varchar(10), GETDATE(), 101),
						GETDATE()
			from		#ach_clients

			-- Discard the working table
			drop table	#ach_clients
		END

		-- Calculate the pending deposits for the clients in this batch
		select distinct client
		into #distinct_clients
		from deposit_batch_details
		where deposit_batch_id = @deposit_batch_id

		update clients
		set    deposit_in_trust = dbo.DepositsInTrust(c.client)
		from   clients c
		inner join #distinct_clients x on c.client = x.client;

		drop table #distinct_clients;
	END

	-- Finally make the operations permanent
	COMMIT TRANSACTION

	-- Tell the user the dollar amount posted
	declare	@p2					varchar(80)

	if @client_count > 0
		set	@p2 = isnull(@p2,'') + ' and ' + convert(varchar, @Client_Count) + ' Client(s)'

	if @product_count > 0
		set	@p2 = isnull(@p2,'') + ' and ' + convert(varchar, @product_count) + ' Product(s)'

	set @p2 = substring(@p2, 5, 80) + ' were posted'

	if @deposit_amount > 0
		set	@p2 = isnull(@p2,'') + ' with a total of $' + convert(varchar, @Deposit_Amount, 1)

	PRINT	@p2

	RETURN ( @trust_register )
END TRY

BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	if @@trancount > 0
	BEGIN
		BEGIN TRY
			ROLLBACK TRANSACTION
		END TRY
		BEGIN CATCH
		END CATCH
	END

	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
	return ( 0 )
END CATCH

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'MONTHLY_Vendors')
    EXEC ('CREATE PROCEDURE MONTHLY_Vendors AS RETURN 0')
GO
ALTER procedure [dbo].[MONTHLY_Vendors](@today as datetime = null) as
BEGIN

	-- Mark the transactions as ready to be invoiced
	-- This is done at the start of the month

	-- Stop noise lines
	set nocount on

	declare		@tomorrow		datetime
	declare		@monthStart     datetime
	declare		@nextMonth      datetime
	declare		@realMonthStart	datetime

	-- Find the start of the day. If it is past 8AM then use tomorrow's date as it is being run early
	-- for the daily jobs normally start at midnight.
	if @today is null
	begin
		set			@today = getdate()
		if datepart(hour, @today) > 8
			set		@today = dateadd(day, 1, @today)
		set			@today = convert(varchar(10), @today, 101)
	end

	set			@tomorrow		= dateadd(day, 1, @today)
	set			@monthStart     = convert(datetime, convert(varchar, month(@today)) + '/01/' + convert(varchar, year(@today)))
	set			@realMonthStart = convert(datetime, convert(varchar, month(getdate())) + '/01/' + convert(varchar, year(getdate())))
	set			@nextMonth      = dateadd(month, 1, @realMonthStart)

	-- We need others to hold for a moment on our data.
	set transaction isolation level repeatable read
	set xact_abort off
	begin transaction
	begin try

		-- Create the working table
		create table #client_products (client_product int, client int, vendor int, product int, amount money, invoice_date datetime, AccountNumber varchar(4), EscrowProBono int);

		-- Find the transactions that are for valid credit cards
		insert into #client_products (client_product, client, vendor, product, amount, invoice_date, AccountNumber, EscrowProBono)
		select		pr.oID as client_product, pr.client, pr.vendor, pr.product, pr.cost - pr.discount - pr.disputed_amount - pr.tendered as amount, convert(datetime, convert(varchar, datepart(month, @today)) + '/01/' + convert(varchar, datepart(year, @today))) as invoice_date, right(v.ach_AccountNumber, 4) as AccountNumber, vp.EscrowProBono
		from		client_products pr
		inner join	vendors v on pr.vendor = v.oid
		inner join  vendor_products vp on pr.vendor = vp.vendor and pr.product = vp.product
		where		pr.invoice_status = 0
		and			pr.ActiveFlag <> 0
		and			v.SupressInvoices = 0
		and			v.BillingMode = 'CreditCard'
		and			ltrim(rtrim(isnull(v.ACH_AccountNumber,''))) <> ''
		and			ltrim(rtrim(isnull(v.CC_CVV,''))) <> ''
		and			v.CC_ExpirationDate >= @NextMonth
		and			pr.cost - pr.discount - pr.disputed_amount - pr.tendered > 0
		and			pr.date_created < @monthStart

		-- Find the transactions that are for valid ACH
		insert into #client_products (client_product, client, vendor, product, amount, invoice_date, AccountNumber, EscrowProBono)
		select		pr.oID as client_product, pr.client, pr.vendor, pr.product, pr.cost - pr.discount - pr.disputed_amount - pr.tendered as amount, convert(datetime, convert(varchar, datepart(month, @today)) + '/01/' + convert(varchar, datepart(year, @today))) as invoice_date, right(v.ach_AccountNumber, 4) as AccountNumber, vp.EscrowProBono
		from		client_products pr
		inner join	vendors v on pr.vendor = v.oid
		inner join  vendor_products vp on pr.vendor = vp.vendor and pr.product = vp.product
		where		pr.invoice_status = 0
		and			pr.ActiveFlag <> 0
		and			v.SupressInvoices = 0
		and			v.BillingMode = 'ACH'
		and			ltrim(rtrim(isnull(v.ACH_AccountNumber,''))) <> ''
		and			len(ltrim(rtrim(isnull(v.ACH_RoutingNumber,'')))) = 9
		and			pr.cost - pr.discount - pr.disputed_amount - pr.tendered > 0
		and			pr.date_created < @monthStart

		-- Find the invoice date for the transaction
		update		#client_products
		set			invoice_date = dateadd(day, v.MonthlyBillingDay - 1, invoice_date)
		from		#client_products p
		inner join	vendors v on p.vendor = v.oID
		where		v.MonthlyBillingDay > 1

		update		#client_products
		set			invoice_date = dateadd(month, 1, invoice_date)
		where		invoice_date < @tomorrow

		-- Generate the transactions saying tha we are marking the items for invoicing
		insert into product_transactions (vendor, product, client_product, transaction_type)
		select		vendor, product, client_product, 'PI'
		from		#client_products

		-- Mark the products as ready to be invoiced
		update		client_products
		set			invoice_status = 1,
					invoice_date = x.invoice_date
		from		client_products cp
		inner join	#client_products x on cp.oID = x.client_product

		-- Find the information for the email message
		select		vendor, product, sum(amount) as amount, convert(varchar(10), invoice_date, 101) as BillingDate, AccountNumber
		into		#product_totals
		from		#client_products
		where		EscrowProBono <> 2
		group by	vendor, product, invoice_date, AccountNumber;

		-- Send the vendors a notice that we want them to examine the invoices
		insert into email_queue (template, email_address, email_name, status, death_date, last_attempt, next_attempt, date_sent, last_error, attempts, substitutions)

		select p.InvoiceEmailTemplateID, ve.Address as email_address, coalesce(dbo.format_normal_name(vn.prefix,vn.first,vn.middle,vn.last,vn.suffix),ve.address,'') as email_name, 'PENDING' as status, null as death_date, null as last_attempt, null as next_attempt, null as date_sent, null as last_error,0 as attempts,
		
			'<substitutions>' +
				'<substitution>' +
					'<field>VendorName</field>' +
					'<value>' + ltrim(rtrim(replace(replace(replace(isnull(v.name,''),'&','&amp;'),'<','&lt;'),'>','&gt;'))) + '</value>' +
				'</substitution>' +
				'<substitution>' +
					'<field>AccountNumber</field>' +
					'<value>' + ltrim(rtrim(replace(replace(replace(isnull(cp.AccountNumber,''),'&','&amp;'),'<','&lt;'),'>','&gt;'))) + '</value>' +
				'</substitution>' +
				'<substitution>' +
					'<field>VendorLabel</field>' +
					'<value>' + isnull(v.label,'') + '</value>' +
				'</substitution>' +
				'<substitution>' +
					'<field>Amount</field>' +
					'<value>$' + convert(varchar, cp.amount, 1) + '</value>' +
				'</substitution>' +
				'<substitution>' +
					'<field>BillingDate</field>' +
					'<value>' + cp.BillingDate + '</value>' +
				'</substitution>' +
			'</substitutions>' as substitutions
			
		from		#product_totals cp
		inner join	products p on cp.product = p.oid
		inner join	vendors v on cp.vendor = v.oid
		left outer join vendor_contacts vc on cp.vendor = vc.vendor and 2 = vc.contact_type
		left outer join names vn on vc.NameID = vn.name
		left outer join emailAddresses ve on vc.EmailID = ve.Email
		where		ve.address is not null
		and			p.InvoiceEmailTemplateID is not null

		-- Discard working table
		drop table	#client_products

		-- Commit the transactions and return the pointer to the client appointment record that was changed.
		commit transaction
	END TRY

	BEGIN CATCH
		declare @ErrorMessage nvarchar(MAX)
		declare @ErrorSeverity int
		declare @ErrorState int

		select	@ErrorMessage  = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState    = ERROR_STATE()

		-- Abort the transaction if we started one
		if xact_state() = -1
			rollback transaction

		-- Generate the appropriate error condition.
		RaisError (@ErrorMessage, @ErrorSeverity, @ErrorState)
	END CATCH
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'MONTHLY_Vendor_CC_Expiration')
    EXEC ('CREATE PROCEDURE MONTHLY_Vendor_CC_Expiration AS RETURN 0')
GO
ALTER procedure [dbo].[MONTHLY_Vendor_CC_Expiration] as
BEGIN
	-- Find the email addresses from the contact information
	declare	@contact_type		int
	declare	@cutoff				datetime
	declare	@emailTemplate		int

	-- Find the cutoff date for two months from now.
	set		@cutoff = convert(varchar(10), dateadd(month, 2, getdate()), 101)
	set		@cutoff = convert(varchar(2), month(@cutoff)) + '/01/' + convert(varchar(4), year(@cutoff))

	-- Find a list of the accounts to be sent a reminder
	select	oID, label, name, ActiveFlag, SupressPayments, SupressInvoices, BillingMode, ACH_AccountNumber, CC_ExpirationDate, CC_CVV, NameOnCard, convert(varchar(512),null) as EmailAddresss, convert(varchar(512),null) as ContactName
	into	#vendors
	from	vendors
	where	BillingMode = 'CreditCard'
	and		CC_ExpirationDate < @cutoff

	-- Discard items that have no escrow products. There shouldn't be any as the forign keys should clean them out.
	delete
	from	#vendors
	where	oID not in (select vendor from vendor_products where EscrowProBono = 1)

	-- Now discard items that don't have transactions.
/*
	delete
	from	#vendors
	where	oID not in (
			select	vendor
			from	client_products
			where	ActiveFlag <> 0
			and		(cost - discount - tendered - disputed_amount) > 0
			and		invoice_status = 2
	)
*/
	-- Use the main contact if there is no billing one
	select	@contact_type	= oID
	from	vendor_contact_types
	where	description	= 'Main'

	if @contact_type is not null
	begin
		update	#vendors
		set		EmailAddresss = e.Address
		from	#vendors v
		inner join vendor_contacts vc on v.oID = vc.vendor and vc.contact_type = @contact_type
		inner join emailAddresses e on vc.EmailID = e.Email

		-- Find the contact names
		update	#vendors
		set		ContactName = dbo.format_normal_name (n.prefix, n.first, n.middle, n.last, n.suffix)
		from	#vendors v
		inner join vendor_contacts vc on v.oID = vc.vendor and vc.contact_type = @contact_type
		inner join Names n on vc.NameID = n.Name
	end	

	select	@contact_type	= oID
	from	vendor_contact_types
	where	description	= 'Invoicing'

	if @contact_type is null
		return

	update	#vendors
	set		EmailAddresss = e.Address
	from	#vendors v
	inner join vendor_contacts vc on v.oID = vc.vendor and vc.contact_type = @contact_type
	inner join emailAddresses e on vc.EmailID = e.Email

	-- Find the contact names
	update	#vendors
	set		ContactName = dbo.format_normal_name (n.prefix, n.first, n.middle, n.last, n.suffix)
	from	#vendors v
	inner join vendor_contacts vc on v.oID = vc.vendor and vc.contact_type = @contact_type
	inner join Names n on vc.NameID = n.Name

	-- Toss all items that can not be processed
	delete
	from	#vendors
	where	EmailAddresss is null;

	-- Find the template ID for the messages
	select	@emailTemplate = oID
	from	email_Templates
	where	description = 'CreditCardExpiration'

	if @emailTemplate is null
	begin
		return
	end

	-- Generate the email message
	insert into email_queue (template, email_address, email_name, substitutions, status)
	select distinct @emailTemplate, EmailAddresss, coalesce(ContactName, EmailAddresss, '') as email_name,
		'<substitutions>' +
			'<substitution>' +
				'<field>' + 'contact_name' + '</field>' +
				'<value>' + coalesce(ContactName, EmailAddresss, '') + '</value>' +
			'<substitution>' +
			'<substitution>' +
				'<field>' + 'firm_name' + '</field>' +
				'<value>' + name + '</value>' +
			'<substitution>' +
			'<substitution>' +
				'<field>' + 'card_number' + '</field>' +
				'<value>' + dbo.statement_account_number(ACH_AccountNumber) + '</value>' +
			'<substitution>' +
			'<substitution>' +
				'<field>' + 'expiration_date' + '</field>' +
				'<value>' + right('00' + convert(varchar(2),month(CC_ExpirationDate)),2) + '/' + convert(varchar(4),year(CC_ExpirationDate)) + '</value>' +
			'<substitution>' +
		'</substitutions>' as substitutions, 'PENDING' as status
	from	#vendors
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'DAILY_Vendor_Statistics')
    EXEC ('CREATE PROCEDURE DAILY_Vendor_Statistics AS RETURN 0')
GO
ALTER procedure [dbo].[DAILY_Vendor_Statistics] AS

-- Calculate the statistics for the vendor based upon the run date

-- Stop noise lines
set nocount on

declare		@today		datetime
declare		@tomorrow	datetime
declare		@yearStart	datetime
declare		@monthStart	datetime

-- Find the start of the day. If it is past 8AM then use tomorrow's date as it is being run early
-- for the daily jobs normally start at midnight.
set			@today      = convert(varchar(10), getdate(), 101)
if datepart(hour, @today) > 8
	set		@today = dateadd(day, 1, @today)

set			@tomorrow   = dateadd(day, 1, @today)
set			@monthStart = convert(datetime, convert(varchar, month(@today)) + '/01/' + convert(varchar, year(@today)))
set			@yearStart  = convert(datetime, '01/01/' + convert(varchar, year(@today)))

-- Find the accounts MTD
select		*
into		#billed
from		client_products with (nolock)
where		invoice_date >= @yearStart and invoice_date < @tomorrow

-- Find the payments YTD
select		*
into		#RC
from		product_transactions with (nolock)
where		date_created > @yearStart and date_created < @tomorrow

-- Summarize Bills by vendor
select		vendor, sum(cost) - sum(discount) as st_BilledYTDAmt
into		#billed_ytd_sum
from		#billed
where		invoice_date < @monthStart
group by	vendor;

select		vendor, sum(cost) - sum(discount) as st_BilledMTDAmt
into		#billed_mtd_sum
from		#billed
where		invoice_date >= @monthStart
group by	vendor;

-- Summarize Receipts by vendor
select		vendor, sum(payment) as st_ReceivedYTDAmt
into		#RC_ytd_sum
from		#RC
where		date_created < @monthStart
group by	vendor;

select		vendor, sum(payment) as st_ReceivedMTDAmt
into		#RC_mtd_sum
from		#RC
where		date_created >= @monthStart
group by	vendor;

-- Apply the figures to the vendors
update		vendors
set			st_BilledYTDAmt = isnull(b.st_BilledYTDAmt,0)
from		vendors v
left outer join	#billed_ytd_sum b on v.oID = b.vendor

update		vendors
set			st_BilledMTDAmt = isnull(b.st_BilledMTDAmt,0)
from		vendors v
left outer join	#billed_mtd_sum b on v.oID = b.vendor

update		vendors
set			st_ReceivedYTDAmt = isnull(b.st_ReceivedYTDAmt,0)
from		vendors v
left outer join	#RC_ytd_sum b on v.oID = b.vendor

update		vendors
set			st_ReceivedMTDAmt = isnull(b.st_ReceivedMTDAmt,0)
from		vendors v
left outer join	#RC_mtd_sum b on v.oID = b.vendor

-- Find the first billing date for a vendor
select		vendor, min(invoice_date) as st_FirstBillingDate
into		#firstBillingDate
from		client_products
group by	vendor

update		vendors
set			st_FirstBillingDate = b.st_FirstBillingDate
from		vendors a
left outer join #firstBillingDate b on a.oID = b.vendor

-- Find the products offered
select		vendor, count(*) as st_ProductsOfferedCount
into		#ProductsOffered
from		vendor_products
group by	vendor

update		vendors
set			st_ProductsOfferedCount = isnull(b.st_ProductsOfferedCount,0)
from		vendors a
left outer join #ProductsOffered b on a.oID = b.vendor

-- Find the client count
select		distinct vendor, client
into		#client_count
from		client_products;

select		vendor, count(*) as st_ClientCountTotalCount
into		#client_count_sum
from		#client_count
group by	vendor;

update		vendors
set			st_ClientCountTotalCount = isnull(b.st_ClientCountTotalCount,0)
from		vendors a
left outer join #client_count_sum b on a.oID = b.vendor;

-- Find the number of produts sold
select		vendor, count(*) as st_ProductsSoldCnt
into		#products_sold
from		client_products
where		ActiveFlag <> 0
group by	vendor

update		vendors
set			st_ProductsSoldCnt = isnull(b.st_ProductsSoldCnt,0)
from		vendors a
left outer join #products_sold b on a.oID = b.vendor;

-- Find the dollar amount of billing adjustments
select		vendor, sum(discount) as st_BillingAdjustmentsAmt
into		#BillingAdjustments
from		client_products
where		ActiveFlag <> 0
group by	vendor

update		vendors
set			st_BillingAdjustmentsAmt = isnull(b.st_BillingAdjustmentsAmt,0)
from		vendors a
left outer join #BillingAdjustments b on a.oID = b.vendor;

-- Find the number of disputed transactions
select		vendor, count(*) as st_DisputeCount
into		#DisputedCount
from		client_products
where		disputed_amount > 0
group by	vendor

update		vendors
set			st_BillingAdjustmentsAmt = isnull(b.st_BillingAdjustmentsAmt,0)
from		vendors a
left outer join #BillingAdjustments b on a.oID = b.vendor;

-- Discard working tables
drop table	#billed
drop table	#billed_ytd_sum
drop table	#billed_mtd_sum
drop table	#RC
drop table	#RC_ytd_sum
drop table	#RC_mtd_sum
drop table #firstBillingDate
drop table #ProductsOffered
drop table #client_count
drop table #client_count_sum
drop table #products_sold
drop table #BillingAdjustments
drop table #DisputedCount

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(select * FROM sysobjects WHERE type='P' AND name=N'DAILY_Vendor_ACH')
    EXEC ('CREATE PROCEDURE DAILY_Vendor_ACH AS RETURN 0')
GO
ALTER procedure [dbo].[DAILY_Vendor_ACH] (@today as datetime = null) as
BEGIN
	-- Mark the transactions as ready to be invoiced
	-- This is done at the start of the month

	-- Stop noise lines
	set nocount on

	declare		@tomorrow		datetime
	declare		@monthStart     datetime
	declare		@nextMonth      datetime

	-- Find the start of the day. If it is past 8AM then use tomorrow's date as it is being run early
	-- for the daily jobs normally start at midnight.
	if @today is null
	begin
		set			@today = getdate()
		if datepart(hour, @today) > 8
			set		@today = dateadd(day, 1, @today)
		set			@today = convert(varchar(10), @today, 101)
	end

	set			@tomorrow		= dateadd(day, 1, @today)
	set			@monthStart     = convert(datetime, convert(varchar, month(@today)) + '/01/' + convert(varchar, year(@today)))
	set			@nextMonth      = dateadd(month, 1, @monthStart)

	-- We need others to hold for a moment on our data.
	set transaction isolation level repeatable read
	set xact_abort off
	begin transaction
	begin try

		-- Find the transactions marked for ACH billing and the billing date is valid
		select	cp.client, cp.vendor, cp.product, cp.oID as client_product, v.ACH_RoutingNumber as ABA, v.ACH_AccountNumber as AccountNumber, v.ACH_CheckingSavings as CheckingSavings, cp.cost - cp.discount - cp.tendered - cp.disputed_amount as Amount, convert(datetime, convert(varchar(10), dateadd(day, 1, getdate()), 101)) as EffectiveDate
		into	#transactions
		from	client_products cp
		inner join vendors v on cp.vendor = v.oid
		inner join products pr on cp.product = pr.oid
		inner join vendor_products vp on cp.vendor = vp.vendor and cp.product = vp.product
		where	cp.invoice_status < 2
		and		cp.invoice_date < @tomorrow
		and		cp.cost - cp.discount - cp.tendered - cp.disputed_amount > 0
		and		v.SupressInvoices = 0
		and		v.BillingMode = 'ACH'
		and		vp.escrowProBono = 1
		and		cp.ActiveFlag <> 0
		and		v.ActiveFlag <> 0

		-- Generate the ACH transactions for the operations
		insert into ach_onetimes (Client, ABA, AccountNumber, CheckingSavings, Amount, EffectiveDate, client_product)
		select	client, aba, AccountNumber, CheckingSavings, Amount, EffectiveDate, client_product
		from	#transactions

		-- Mark the items as having been generated
		update	client_products
		set		invoice_status = 2
		from	client_products cp
		inner join #transactions t on cp.oID = t.client_product

		-- Insert the transaction that the amount has been invoices
		insert into product_transactions (vendor, product, client_product, transaction_type)
		select	vendor, product, client_product, 'IV' as transaction_type
		from	#transactions

		-- Discard the working table
		drop table #transactions

		-- Commit the transactions and return the pointer to the client appointment record that was changed.
		commit transaction
	END TRY

	BEGIN CATCH
		declare @ErrorMessage nvarchar(MAX)
		declare @ErrorSeverity int
		declare @ErrorState int

		select	@ErrorMessage  = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState    = ERROR_STATE()

		-- Abort the transaction if we started one
		if xact_state() = -1
			rollback transaction

		-- Generate the appropriate error condition.
		RaisError (@ErrorMessage, @ErrorSeverity, @ErrorState)
	END CATCH
END
GO

-- SECTION: PERMISSIONS
EXECUTE CMD_SET_PERMISSIONS
