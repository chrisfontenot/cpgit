USE [DebtPlus]
GO
CREATE TYPE [dbo].[typ_NameField] FROM [varchar](50) NULL
GO
EXECUTE sp_bindrule 'valid_NameField', 'typ_NameField'
GO
