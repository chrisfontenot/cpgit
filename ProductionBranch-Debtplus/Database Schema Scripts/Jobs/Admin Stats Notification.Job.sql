USE [msdb]
GO

/****** Object:  Job [Admin Stats Notification]    Script Date: 2/13/2013 3:18:50 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 2/13/2013 3:18:51 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Admin Stats Notification', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=1, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=N'Dave Mainz', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Admin Count Email]    Script Date: 2/13/2013 3:18:52 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Admin Count Email', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'

DECLARE @rc INT, @YesterdayImportCount int, @TodayImportCount int
DECLARE @subjecttext varchar(250), @bodytext varchar(250)

select @YesterdayImportCount = count(*) from applications
where cast(floor(cast(submission_date AS float)) as datetime)
               = cast(floor(cast(getdate()-1 AS float)) as datetime)


select @TodayImportCount = count(*) from applications
where cast(floor(cast(submission_date AS float)) as datetime)
               = cast(floor(cast(getdate() AS float)) as datetime)

set @subjecttext = ''Admin System Imported Application Counts --  Yesterday:  '' + cast(@YesterdayImportCount as varchar) + '' Today So Far: ''  + cast(@TodayImportCount as varchar) 
set @bodytext =  @subjecttext + ''          |Sent from Admin System Notification Job ON RIC-DP-02.''

IF NOT EXISTS (SELECT * FROM msdb.sys.service_queues 
         WHERE name = N''ExternalMailQueue'' AND is_receive_enabled = 1)
      EXEC @rc = msdb.dbo.sysmail_start_sp
ELSE

       EXEC msdb.dbo.sp_send_dbmail
                @profile_name = ''DBAMail'',
    @recipients = ''rebecca.gershowitz@clearpointccs.org;'',
    @copy_recipients = ''davemainz@yahoo.com;david.mainz@clearpointfs.org'',
     @body = @bodytext,
    @subject =  @subjecttext', 
		@database_name=N'cccs_admin', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Afternoon Run', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20120207, 
		@active_end_date=99991231, 
		@active_start_time=132000, 
		@active_end_time=235959, 
		@schedule_uid=N'8b48a977-fb53-48ee-8b8b-08d9c37d9b2e'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Morning Run', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20120207, 
		@active_end_date=99991231, 
		@active_start_time=70500, 
		@active_end_time=235959, 
		@schedule_uid=N'65e611d6-b70e-4fcb-9cc7-01c0cfce7a1c'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

