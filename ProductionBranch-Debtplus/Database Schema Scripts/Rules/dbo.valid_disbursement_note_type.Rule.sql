USE [DebtPlus]
GO
/****** Object:  Rule [dbo].[valid_disbursement_note_type]    Script Date: 09/15/2014 13:13:39 ******/
create rule [dbo].[valid_disbursement_note_type] as @value in ('PR', 'MS', 'DR')
GO
