USE [DebtPlus]
GO
/****** Object:  Rule [dbo].[valid_fairshare_rate]    Script Date: 09/15/2014 13:13:39 ******/
create rule [dbo].[valid_fairshare_rate] as
    @value is null
or (@value >= 0.0 and @value < 1.0)
or (@value = -1.0)
GO
