USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_message_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_message_list] ( @creditor as typ_creditor ) as
-- ===================================================================================
-- ==            Return the list of messages that are pending for the creditor      ==
-- ===================================================================================
select
	creditor_www_note	as 'item_key',
	message			as 'message',
	date_created		as 'date_created',
	created_by		as 'created_by',
	date_shown		as 'date_shown'
from	creditor_www_notes
where	creditor	= @creditor
order by 3

return ( @@rowcount )
GO
