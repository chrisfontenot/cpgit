USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CreditorPayments_Refunds]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_CreditorPayments_Refunds] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ===============================================================================================
-- ==           Create a report which shows the creditor payments in the date range             ==
-- ===============================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

-- Suppress intermediate result sets
set nocount on

-- Correct the date fields
IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the returns to clients
SELECT		d.creditor		as 'creditor',
		cr.creditor_name	as 'creditor_name',

		case
			when d.tran_type = 'VF' then 0 - d.debit_amt
			else d.credit_amt
		end			as 'check_amount',

		d.date_created		as 'item_date',

		d.client		as 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',

		case
			when  d.creditor_type = 'D' and d.tran_type = 'RF' then d.fairshare_amt
			when  d.creditor_type = 'D' and d.tran_type = 'VF' then 0 - d.fairshare_amt
			else 0
		end			as 'fairshare'

FROM		registers_client_creditor d	WITH (NOLOCK)
left outer join	people p			WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor

WHERE		d.tran_type in ('RF','VF')
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created, cr.type, cr.creditor_id, d.creditor, d.client

-- Return the number of rows in the result set
RETURN ( @@rowcount )
GO
