USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_confirmation]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_confirmation] AS

-- ===================================================================================================
-- ==                Return the list of appointment confirmation types                              ==
-- ===================================================================================================

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
From	AppointmentConfirmationTypes
order by 2

return ( @@rowcount )
GO
