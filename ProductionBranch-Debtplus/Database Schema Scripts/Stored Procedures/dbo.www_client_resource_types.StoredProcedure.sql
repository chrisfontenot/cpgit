USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_resource_types]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_resource_types] AS

-- ========================================================================================================
-- ==            Interface to the resource types for the web pages.                                      ==
-- ========================================================================================================

declare	@count		int
Execute @count = lst_resource_types
return ( @count )
GO
