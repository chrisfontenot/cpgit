USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_create_admin]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_create_admin] ( @creditor as typ_creditor, @password as varchar(32) = null) as 
-- =============================================================================
-- ==            Create access to the creditor from the DebtPlus interface    ==
-- =============================================================================

-- Suppress intermediate results
set nocount on
begin transaction
set xact_abort on

declare	@creditor_www	int

-- Ensure that the client is valid
if not exists (
	select	creditor
	from	creditors
	where	creditor = @creditor )
begin
	RaisError ('There is no creditor %s', 16, 1, @creditor)
	rollback transaction
	return ( 0 )
end

-- Ensure that the creditor does not have www access
if exists (
	select	*
	from	creditor_www
	where	creditor = @creditor )
begin
	RaisError ('The creditor %s currently has WWW access.', 16, 1, @creditor)
	rollback transaction
	return ( 0 )
end

-- Insert the item into the system
insert into creditor_www (creditor, password) values (@creditor, @password)
select	@creditor_www = SCOPE_IDENTITY()

commit transaction
return ( @creditor_www )
GO
