USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_TelephoneNumbers]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_TelephoneNumbers](@TelephoneNumber as int, @Country as int = null, @Acode as varchar(80) = null, @Number as varchar(80) = null, @Ext as varchar(80) = null, @Client as int = null, @Person as int = null, @Type as varchar(80) = 'Work') as
-- ===========================================================================================
-- ==            Change a telephone number field for the client if needed                   ==
-- ===========================================================================================

set nocount on

-- Format the telephone number field
declare	@OldNumber		varchar(80)
declare	@NewNumber		varchar(80)
declare	@Subject		varchar(80)
declare	@RowCount		int
declare	@relation		int

if @Client is not null
	select	@OldNumber= dbo.format_TelephoneNumber ( @TelephoneNumber );

-- Change the telephone number
update	TelephoneNumbers
set		[Country]			= @Country,
		[Acode]				= @Acode,
		[Number]			= @Number,
		[Ext]				= @Ext
WHERE	[TelephoneNumber]	= @TelephoneNumber;
select	@RowCount			= @@ROWCOUNT;

-- If there is a client then prepare the client note
if @Client is not null
begin
	select	@NewNumber= dbo.format_TelephoneNumber ( @TelephoneNumber ),
			@Subject  = 'Changed ';

	-- If the numbers are different then generate a system note
	if isnull(@NewNumber,'') <> isnull(@OldNumber,'')
	begin
	
		-- If there is a person then find the work telephone number
		if @Person is not null
		begin
			select	@relation		= relation
			from	people
			where	client			= @Client
			and		person			= @Person;

			if @Relation = 1
				select @Subject = @Subject + 'Applicant '
			else
				select @Subject = @Subject + 'Co-Applicant '
		end

		-- Generate the proper system note
		select @Subject = @Subject + isnull(@Type + ' ','') + 'Telephone number'
		insert into client_notes ( [client], [type], [subject], [note], [dont_delete], [dont_edit])
		select	@Client, 3, @Subject, 'Changed the telephone number' + ISNULL(' from ' + @OldNumber,'') + ' to ' + ISNULL(@NewNumber,'NOTHING'), 1, 1
	end
end

return ( @RowCount )
GO
