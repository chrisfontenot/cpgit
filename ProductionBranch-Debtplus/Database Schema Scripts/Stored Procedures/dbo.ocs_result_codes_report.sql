USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[ocs_result_codes_report]    Script Date: 06/18/2015 12:26:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Brandon Wilhite
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[ocs_result_codes_report] 
	-- Add the parameters for the stored procedure here
	@beginReport datetime = GETDATE,
	@endReport datetime = GETDATE
--	@Success bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select
	attempt.[User],
	result.Id,
	result.ShortCode + ' - ' + result.[Description] [Outcome],
	count(result.ShortCode + ' - ' + result.[Description]) [Count]
	
	from OCS_ContactAttempt [attempt]
	left join OCS_ResultCode [result] on attempt.ResultCode = result.Id

	WHERE attempt.[End] >= @beginReport
	AND attempt.[End] < @endReport

	group by attempt.[User], result.Id, result.ShortCode + ' - ' + result.[Description]

END

