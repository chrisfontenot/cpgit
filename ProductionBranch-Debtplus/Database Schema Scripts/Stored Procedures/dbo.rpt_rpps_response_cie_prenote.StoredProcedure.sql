USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cie_prenote]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cie_prenote] ( @rpps_response_file as int = null ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of error prenotes                    ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--     Added client's counselor name.

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

-- Retrieve the information for the payment records
select		f.date_created							as 'date_created',  -- 1
		rt.client							as 'client', -- 2
		isnull(pn.last,'')	 					as 'client_name', -- 3
		rt.creditor							as 'creditor', -- 4
		rt.client_creditor							as 'client_creditor', -- 5
		coalesce(cc.account_number,d.account_number, '')		as 'account_number', -- 6
		coalesce(cr.creditor_name,d.consumer_name, '')			as 'creditor_name', -- 7
		coalesce(d.processing_error, x.description, d.return_code, '')	as 'error_code', -- 8
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default))					as 'counselor_name', -- 9
		cr.type, -- 10
		cr.creditor_id -- 11

from		rpps_response_details d		with (nolock)
left outer join	rpps_response_files f		with (nolock) on d.rpps_response_file = f.rpps_response_file
left outer join	rpps_transactions rt		with (nolock) on d.rpps_transaction = rt.rpps_transaction
left outer join	people p			with (nolock) ON rt.client=p.client AND 1=p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
left outer join	rpps_reject_codes x		with (nolock) ON d.return_code = x.rpps_reject_code
left outer join client_creditor cc		with (nolock) ON rt.client_creditor = cc.client_creditor
left outer join	creditors cr			with (nolock) on rt.creditor = cr.creditor
left outer join clients c			with (nolock) on cc.client = c.client
left outer join counselors co			with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

where		d.rpps_response_file		= @rpps_response_file
and		d.service_class_or_purpose	= 'CIE'
and		d.transaction_code		= 23

order by	10, 11, 2, 6  -- cr.type, cr.creditor_id, c.client, cc.account_number

return ( @@rowcount )
GO
