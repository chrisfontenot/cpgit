USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_trust_register_create_SC]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_trust_register_create_SC] ( @Amount AS Money = 0, @ItemDate AS DateTime = NULL, @Cleared AS VarChar(1) = NULL, @CheckNumber as varchar(20) = NULL ) AS

-- ====================================================================================================
-- ==            Create a row in the trust register for depositing bank interest                     ==
-- ====================================================================================================

SET NOCOUNT ON

-- Ensure that the amount is valid
IF @Amount < 0
BEGIN
	RaisError (50019, 16, 1)
	Return ( 0 )
END

-- Default the item date
IF @ItemDate IS NULL
	SET @ItemDate = getdate()

DECLARE	@trust_register		INT

-- Insert the item into the trust register
INSERT INTO	registers_trust (tran_type,	date_created,	amount,		cleared)
VALUES				('SC',		@ItemDate,	@Amount,	'R')

SELECT @trust_register = SCOPE_IDENTITY()

-- Return the trust register ID to the caller
RETURN ( @trust_register )
GO
