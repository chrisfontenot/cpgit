USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTLY_Appointments]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[MONTLY_Appointments] as
-- =======================================================================
-- ==               Discard old appointment slots past their date       ==
-- =======================================================================
declare	@cutoff_date	datetime
select	@cutoff_date	= convert(varchar(10), dateadd(m, -1, getdate()), 101)

-- Find the slots that we want to toss
select	appt_time
into	#appt_time
from	appt_times
where	start_time < @cutoff_date

-- Mark the appointments as having been missed if they are to be deleted
update	client_appointments
set		status = 'M'
from	client_appointments ca
inner join #appt_time a on ca.appt_time = a.appt_time
where	ca.office is not null
and		ca.status = 'P';

-- Remove the client_appointment references
update	client_appointments
set		appt_time	= null
from	client_appointments ca
inner join #appt_time a on ca.appt_time = a.appt_time;

-- Discard the counselor references
delete	appt_counselors
from	appt_counselors co
inner join #appt_time a on co.appt_time = a.appt_time;

-- Discard the appointment time slots
delete	appt_times
from	appt_times x
inner join #appt_time a on a.appt_time = x.appt_time;

drop table #appt_time
GO
