USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_keep]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_keep] ( @Appt AS INT, @Counselor AS INT = NULL, @Office AS INT = NULL ) AS

-- PROBABLY OBSOLETE. Was used at one time to mark the appointment "kept" when a recipionist
--                    would welcome the clients before the counselor interviewed them. Not used any longer.

-- ===================================================================================================
-- ==            Mark the appointment as being kept at this time                                    ==
-- ===================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Start a transaction for consistency
BEGIN TRANSACTION

-- If this is a workshop appointment then just update the status accordingly.
declare	@workshop	int
select	@workshop		= workshop
from	client_appointments with (nolock)
where	client_appointment	= @appt

if @workshop is not null
begin
	update	client_appointments
	set	status			= 'K',
		date_updated		= getdate()
	where	client_appointment	= @appt

end else begin

-- If the counselor was not passed then try to find it in the system tables based upon the name of the person "keeping" the appointment
	if @counselor is null
		select	@counselor	= counselor,
			@office		= isnull(@office,office)
		from	counselors
		where	[person]	= suser_sname()		-- Don't forget the \\DP\jones rather than just 'jones' in the table

-- Try to use the counselor's office from the table if an office was not specified.
	else if @office is null
		select	@office		= office
		from	counselors
		where	counselor	= @counselor

-- Update the status for the appointment
	UPDATE	client_appointments
	SET	status			= 'K',
		start_time		= getdate(),
		counselor		= isnull(@Counselor,	counselor),
		office			= isnull(office,	@office)
	WHERE	client_appointment	= @Appt

-- Fetch the client from the appointment
	DECLARE	@Client	INT
	SELECT	@Client			= client,
		@Counselor		= counselor,
		@Office			= office
	FROM	client_appointments
	WHERE	client_appointment	= @Appt

-- Update the counselor and office from the appointment
	UPDATE	clients
	SET	counselor		= @Counselor,
		office			= @Office
	WHERE	client			= @Client

-- Set the first kept appointment
	UPDATE	clients
	SET	first_kept_appt		= @Appt
	WHERE	client			= @Client
	AND	first_kept_appt IS NULL
end

-- Commit the transaction
COMMIT TRANSACTION

-- Return success to the caller
RETURN ( @@rowcount )
GO
