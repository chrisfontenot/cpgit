USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_message_add]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_message_add] ( @client as typ_client, @message as varchar(256) = NULL ) as
-- ===================================================================================
-- ==            Add the message to the client list of messages                     ==
-- ===================================================================================
set nocount on

-- Remove the extra spaces around the message.
select	@message = ltrim(rtrim(isnull(@message,'')))

-- Find the client id in the list of clients for the web
declare	@pkid	uniqueidentifier
select	@pkid	= pkid
from	client_www
where	username	= convert(varchar,@client)
and		applicationname	= '/'

if @pkid is null
begin
	RaisError ('Client must first be granted web access to have messages.', 16, 1)
	return ( 0 )
end

-- If there is a message, insert it
if @message <> ''
begin
	insert into client_www_notes (PKID, message) values (@pkid, @message)
	return ( SCOPE_IDENTITY() )
end

return	( 0 )
GO
