USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_recon_detail_delete]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_recon_detail_delete] (@recon_detail_1 [typ_key]) AS

-- =====================================================================================================================
-- ==            Remove the item from the list                                                                        ==
-- =====================================================================================================================

DELETE	[recon_details] 
WHERE	[recon_detail]	 = @recon_detail_1

RETURN ( @@rowcount )
GO
