USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_custom_events]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_custom_events] ( @client_appointment as int ) as
-- =====================================================================================================
-- ==            Do custom work when the appointment status changes as needed                         ==
-- =====================================================================================================

-- Changelog
--   7/2/2008
--     Removed exclusion for appointment types
--	 9/27/11
--	   Modified to include FCO as result
--	 3/23/12
--	   Included the custom logic from xpr_appt_result for RM housing appointments

set nocount on

declare	@appt_typ		int
declare	@client			int
declare	@start_time		datetime
declare	@status			varchar(10)
declare	@result			varchar(10)
declare	@contact_type	varchar(1)
declare	@housing		bit
declare	@appt_name		varchar(50)
declare @office			int

select	@appt_typ		= ca.appt_type,
		@result			= ca.result,
		@client			= c.client,
		@start_time		= ca.start_time,
		@status			= ca.status,
		@office			= ca.office
from	client_appointments ca with (nolock)
inner join clients c with (nolock) on ca.client = c.client
where	ca.client_appointment	= @client_appointment
and		ca.office is not null
and		ca.workshop is null;

-- Ignore workshop appointments at this point
if @office is null
	return ( 1 )

-- Find the information about the appointment type
if @appt_typ is not null
	select	@contact_type	= contact_type,
			@housing		= housing,
			@appt_name		= appt_name
	from	appt_types
	where	appt_type		= @appt_typ

-- We must have a client and a result completing the appointment.
if (@client is not null) AND (@status in ('K','W'))
begin

	-- These appointment types that are not BK
	if @appt_typ in (1, 6, 7, 55, 56) and (@result not in ('RLA','BK','BK7'))
	begin
		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client					as client,
				e.retention_event		as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type			as expire_type,
				dateadd(d, 90, getdate())	as expire_date,
				e.priority				as priority,
				getdate()				as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 5;
	end

	-- Include the operations for these appointment types only
	if @appt_typ in (96, 98, 100) and (@result <> 'IS')
	begin
		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(d, 90, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(d, 1, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 127;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 8, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 5, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 119;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 12, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 9, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 125;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 14, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 11, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 120;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 15, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 12, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 126;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 20, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 17, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 121;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 26, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 23, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 122;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 32, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 29, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 123;

		insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
		select	@client						as client,
				e.retention_event			as retention_event,
				'For Appointment # ' + convert(varchar, @client_appointment) + ' at ' + convert(varchar(40), @start_time, 100)		as message,
				e.expire_type				as expire_type,
				dateadd(m, 38, getdate())	as expire_date,
				e.priority					as priority,
				dbo.date_only(dateadd(m, 35, getdate()))	as effective_date
		from	retention_events e with (nolock)
		where	retention_event			= 124;
	end
end

return ( 0 )
GO
