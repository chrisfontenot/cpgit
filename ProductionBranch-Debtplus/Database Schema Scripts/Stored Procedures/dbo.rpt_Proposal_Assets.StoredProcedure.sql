USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_Assets]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_Assets] ( @client as int ) AS

-- ==================================================================================================================
-- ==            Return the information needed for the "ASSET INFO" subreport on the full disclosure               ==
-- ==            printed report. The EDI functions are handled elsewhere.                                          ==
-- ==================================================================================================================
select		isnull(t.grouping+' ','') + isnull(t.description,'')	as 'auto_make',
		isnull(p.description,'')				as 'auto_model',
		isnull(p.year_mfg,0)					as 'auto_year',
		sum(l.balance)						as 'balance',
		p.current_value						as 'value',
		convert(int,0)						as 'arrears_months'
from		secured_properties p with (nolock)
inner join	secured_types t on p.secured_type = t.secured_type
inner join	secured_loans l on p.secured_property = l.secured_property
where		p.client		= @client
and		p.current_value		> 0
group by	t.grouping, t.description, p.description, p.year_mfg, p.current_value

return ( @@rowcount )
GO
