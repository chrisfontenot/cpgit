USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_message_item_type]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_message_item_type] AS

-- =============================================================================
-- ==            Return a list of the standard types in the messages table    ==
-- =============================================================================

-- Return the standard items from the list
select distinct item_type as description from messages
order by 1
GO
