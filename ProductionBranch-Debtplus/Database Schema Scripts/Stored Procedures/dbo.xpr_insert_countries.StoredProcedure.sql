USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_countries]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_countries] ( @description as varchar(50), @country_code as int = 0, @default as bit = 0, @ActiveFlag as bit = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the countries table                      ==
-- ========================================================================================
	insert into countries ( [description], [country_code], [default], [ActiveFlag] ) values ( @description, @country_code, @default, @ActiveFlag )
	return ( scope_identity() )
GO
