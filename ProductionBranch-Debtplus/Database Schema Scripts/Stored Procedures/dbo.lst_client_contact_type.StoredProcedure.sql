USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_client_contact_type]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_client_contact_type] AS
-- ========================================================================================================
-- ==            Return the list of contact types                                                        ==
-- ========================================================================================================

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
FROM	ContactMethodTypes
order by 2

return ( @@rowcount )
GO
