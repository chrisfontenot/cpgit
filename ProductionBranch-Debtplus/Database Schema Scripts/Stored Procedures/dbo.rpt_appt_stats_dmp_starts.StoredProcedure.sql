USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_stats_dmp_starts]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_stats_dmp_starts] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS

-- ====================================================================================================
-- ==            Return the number of DMP starts by counselor/office                                 ==
-- ====================================================================================================

IF @ToDate IS NULL
	select @ToDate = getdate()

IF @FromDate IS NULL
	select @FromDate = @ToDate

-- Adjust the dates for the proper settings
select @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
select @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the results of the appointment
select	isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'Not specified')	as 'counselor',
	isnull(o.name,'Not Specified')	as 'office',
	count(*)			as 'count'

from	clients c
left outer join counselors co	with (nolock) on c.counselor = co.counselor
left outer join offices o	with (nolock) on c.office = o.office
left outer join names cox with (nolock) on co.NameID = cox.name

where	c.active_status		IN ('A', 'AR')
AND	c.start_date BETWEEN @FromDate AND @ToDate

group by cox.first, cox.last, o.name
ORDER BY 1, 2

RETURN ( @@rowcount )
GO
