USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_deposit_subtypes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_deposit_subtypes] AS

-- ====================================================================================================================
-- ==            Return a list of the type of deposits                                                               ==
-- ====================================================================================================================

-- Fetch the types of deposits. This is in the tran_types table but are marked as deposit_subtype
SELECT	(ascii(tran_type) * 256) + ascii(right(tran_type,1))	as 'item_key',
	description						as 'description'
FROM	tran_types
WHERE	deposit_subtype	> 0

RETURN ( @@rowcount )
GO
