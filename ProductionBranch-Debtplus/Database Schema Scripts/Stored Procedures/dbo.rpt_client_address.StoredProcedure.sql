USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_address]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_client_address] ( @client as int ) AS

-- ======================================================================================================
-- ==            Return the client address for use by reports as an address block                      ==
-- ======================================================================================================

select	upper(convert(varchar(80),name))	as 'name',
	upper(convert(varchar(80),addr1))	as 'addr_1',
	upper(convert(varchar(80),addr2))	as 'addr_2',
	upper(convert(varchar(80),addr3))	as 'addr_3',
	upper(zipcode)				as 'zipcode'
from	view_client_address with (nolock)
where	client	= @client

return ( @@rowcount )
GO
