USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_TicklerType]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_TicklerType] ( @description as varchar(50), @default as bit = 0, @ActiveFlag as bit = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the TicklerTypes table                   ==
-- ========================================================================================
	insert into TicklerTypes ( [description],[default],[ActiveFlag] ) values ( @description,@default,@ActiveFlag )
	return ( scope_identity() )
GO
