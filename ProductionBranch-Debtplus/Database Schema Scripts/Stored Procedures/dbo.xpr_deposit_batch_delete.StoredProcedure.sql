USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_delete]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_delete] ( @deposit_batch_id AS INT ) AS

-- ==================================================================================================
-- ==               Delete the deposit batch                                                       ==
-- ==================================================================================================

-- ChangeLog
--   09/04/2001
--     Rewrote to test the batch for the proper status before deleting it.
--   3/05/2004
--     Added 'deposit_in_trust' to the client

-- suppress intermediate result sets
SET NOCOUNT ON

-- Start a transaction to keep things stable
BEGIN TRANSACTION
set xact_abort on

-- Determine the information for the batch
declare	@date_created	datetime
declare	@date_closed	datetime
declare	@date_posted	datetime
declare	@batch_type	varchar(2)
declare	@item_count	int

select	@date_created	= date_created,
	@date_closed	= date_closed,
	@date_posted	= date_posted,
	@batch_type	= batch_type
from	deposit_batch_ids
where	deposit_batch_id = @deposit_batch_id;

-- The batch must exist
if @@rowcount < 1
begin
	rollback transaction;
	raiserror (50017, 16, 1, @deposit_batch_id);
	return ( 0 );
end

-- The batch may not be posted. It is unimportant if there are transactions or not. It is posted.
if @date_posted is not null
begin
	rollback transaction;
	raiserror (50018, 16, 1, @deposit_batch_id);
	return ( 0 );
end;

-- Obtain a subset of the transactions for this deposit batch from the system
select	deposit_batch_detail, client, isnull(amount,0) as amount, convert(int,isnull(ok_to_post,0)) as ok_to_post, tran_subtype
into	#deposit_batch_details
from	deposit_batch_details
where	deposit_batch_id = @deposit_batch_id;

-- Build an index for the client
create index ix_temp_deposit_batch_details_1 on #deposit_batch_details ( client, ok_to_post );

-- Determine if there are any transactions that need to be addressed.
select	@item_count = count(*)
from	#deposit_batch_details

if @item_count is null
	select @item_count = 0;

if @item_count > 0
begin

	-- The batch must be closed if this is not a credit refund batch
	if (@batch_type != 'CR') and (@date_closed IS NULL)
	begin
		drop table #deposit_batch_details
		rollback transaction;
		raiserror (50018, 16, 1, @deposit_batch_id);
		return ( 0 );
	end

	-- delete the transactions from the batch detail
	delete	deposit_batch_details
	from	deposit_batch_details d
	inner join #deposit_batch_details x on d.deposit_batch_detail = x.deposit_batch_detail;
end

-- delete the batch
delete
from	deposit_batch_ids
where	deposit_batch_id = @deposit_batch_id

-- Discard the working table
drop table #deposit_batch_details

-- Update the client deposit figures
select	d.client, sum(d.amount) as deposit_in_trust
into	#deposit_in_trust
from	deposit_batch_details d
inner join deposit_batch_ids i on d.deposit_batch_id = i.deposit_batch_id
where	d.ok_to_post		<> 0
and     i.trust_register is null
and     i.batch_type in ('AC','CL')
group by d.client;

update	clients
set		deposit_in_trust	= x.deposit_in_trust
from	clients c
inner join #deposit_in_trust x on c.client = x.client;

drop table #deposit_in_trust

-- Return success
commit transaction
return ( 1 )
GO
