USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_epay_response_fr]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_epay_response_fr]  ( @epay_response_file int = null ) as
-- ============================================================================================
-- ==            List the free_form response messages                                        ==
-- ============================================================================================

-- Surpress intermediate results
set nocount on

-- Find the file if one was not supplied
if @epay_response_file is null
begin
	select	top 1
			@epay_response_file = epay_response_file
	from	epay_response_files with (nolock)
	order by date_created desc
end

-- There should be a file or we are in trouble.
if @epay_response_file is null
begin
	RaisError ('There are no pending response files', 16, 1)
	return ( 0 )
end

-- Return the information from the database tables for the proposal statuses
select	sending_id, receiving_id, local_biller_id, message
from	epay_responses_fr with (nolock)
where	epay_response_file = @epay_response_file
order by receiving_id, sending_id, local_biller_id

return ( @@rowcount )
GO
