USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Missing_Creditor_Statements]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Missing_Creditor_Statements] ( @FromDate as datetime = null ) as

-- =====================================================================================================
-- ==            Return a list of the clients who have not sent in statements                         ==
-- =====================================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

set nocount on

-- If there is no date then assume 3 months past.
if @FromDate is null
begin
	select	@FromDate = dateadd(m, -3, getdate())
	select	@FromDate = convert(datetime, convert(varchar,month(@FromDate)) + '/01/' + convert(varchar,year(@FromDate)) + ' 00:00:00')
end

-- Make the date legimate without the time period
select	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')

-- Return the results to the report
select	isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'') as 'counselor_name', -- 1
	c.client, -- 2
	cc.creditor, -- 3
	cc.account_number, -- 4
	isnull(cr.creditor_name,cc.creditor_name) as 'creditor_name', -- 5
	dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as client_name, -- 6
	cc.last_stmt_date, -- 7

	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance', -- 8
	case
		when cc.balance_verify_date is not null and cc.balance_verify_by is not null then 'V'
		else ' '
	end as 'verified', -- 9
	rcc.date_created as 'last_disursed', -- 10
	c.counselor as 'counselor', -- 11
	dbo.format_TelephoneNumber ( c.HomeTelephoneID ) as 'home_ph', -- 12
	
	cr.type, -- 13
	cr.creditor_id -- 14

from	clients c		with (nolock)
inner join client_creditor cc	with (nolock) on cc.client = c.client
left outer join creditors cr	with (nolock) on cc.creditor = cr.creditor
left outer join counselors co	with (nolock) on c.counselor = co.counselor
left outer join names cox       with (nolock) on co.NameID = cox.name
left outer join people p	with (nolock) on c.client = p.client and 1 = p.relation
left outer join Names pn with (nolock) on p.NameID = pn.Name
left outer join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
left outer join registers_client_creditor rcc with (nolock) on cc.last_payment = rcc.client_creditor_register

where	(cc.last_stmt_date is null or cc.last_stmt_date < @FromDate)
and	c.active_status in ('A','AR')
and	cc.reassigned_debt = 0
and	(cc.creditor not like 'X%' and cc.creditor not like 'Z%')
and	((bal.orig_balance+bal.orig_balance_adjustment+bal.total_interest > bal.total_payments) or (bal.payments_month_0 <> 0 or bal.payments_month_1 <> 0))
and	c.client > 0
order by 1, 2, 13, 14, 3, 4 -- co.name, cc.client, cr.type, cr.creditor_id, cc.creditor, cc.account_number

return ( @@rowcount )
GO
