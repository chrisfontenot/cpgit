USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_secured_list]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_secured_list] ( @client as int ) as

-- =======================================================================================================
-- ==         Return the list of secured assets for a client                                            ==
-- =======================================================================================================

create table #secured_types (secured_property int, secured_type int null, description varchar(122) null, current_value money null)

insert into #secured_types (secured_property, secured_type, description, current_value)
select	p.secured_property		as secured_property,
	p.secured_type			as secured_type,
	isnull(p.description,'')	as description,
	p.current_value			as current_value
from	secured_properties p with (nolock)
where	p.client			= @client

insert into #secured_types (secured_property, secured_type, description, current_value)
select	0 - convert(int,t.secured_type) as secured_property,
	t.secured_type,
	'' as description,
	convert(money,0) as current_value
from	secured_types t with (nolock)
left outer join #secured_types x on t.secured_type = x.secured_type
where	x.secured_property is null;

/*
update	#secured_types
set	description	= t.description
from	#secured_types x
inner join secured_types t with (nolock) on x.secured_type = t.secured_type;
*/

select	p.secured_property,
	sum(l.balance) as balance
into	#loans
from	secured_loans l with (nolock)
inner join #secured_types p on l.secured_property = p.secured_property
group by p.secured_property

select	p.secured_property			as item_key,
	p.secured_type				as secured_type,
	p.description				as description,
	isnull(p.current_value,0)		as current_value,
	isnull(l.balance,0)			as balance,
	isnull(t.grouping+' ','') + isnull(t.description,'')				as secured_type_description
from	#secured_types p
left outer join #loans l on p.secured_property = l.secured_property
left outer join secured_types t on p.secured_type = t.secured_type
order by isnull(p.secured_type,3000)

drop table #secured_types
drop table #loans

return ( @@rowcount )
GO
