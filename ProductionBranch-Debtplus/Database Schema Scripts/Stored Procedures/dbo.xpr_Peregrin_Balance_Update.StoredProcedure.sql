USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Peregrin_Balance_Update]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_Peregrin_Balance_Update] ( @client_text as varchar(80) = '', @account_number as varchar(80), @disbursement_factor_text as varchar(80) = '', @balance_text as varchar(80), @last_disbursement_date_text as varchar(80) = '', @last_disbursement_amount_text as varchar(80) = '', @remaining_payments_1 as varchar(80) = '', @remaining_payments_2 as varchar(80) = '', @result as varchar(80) OUTPUT, @creditor as varchar(10) = 'E4678' ) as
-- =========================================================================================================
-- ==            Update the balances on capitalone debts from their spreadsheet                           ==
-- =========================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Local storage
declare	@client_creditor		int
declare	@last_error			int
declare	@last_rowcount			int

select	@result		= ''

-- Find the corresponding debt in the list of creditors
select	@client_creditor	= cc.client_creditor
from	client_creditor cc with (nolock)
where	account_number		= @account_number
and		creditor			= @creditor

-- If there is no debt than we can not go any further
select	@last_error		= @@error,
		@last_rowcount	= @@rowcount

if @last_error <> 0
begin
	select	@result = 'DBERROR SELECTING DEBT'
	goto	Quit_Procedure
end

if @client_creditor is null
begin
	select	@result = 'CAN''T FIND DEBT'
	goto	Quit_Procedure
end

-- Retrieve the information from the debt payee record
declare	@client				int
declare	@client_creditor_balance	int

select	@client						= client,
		@client_creditor_balance	= client_creditor_balance
from	client_creditor
where	client_creditor				= @client_creditor

-- Calculate the balance
declare	@balance	money
select	@balance	= convert(money, @balance_text)

-- Determine if there are disbursements after the last date
declare	@last_disbursement_date	datetime
select	@last_disbursement_date	= convert(datetime, @last_disbursement_date_text)

-- Do not count the date that it was received. That balance is not valid.
select	@last_disbursement_date = dateadd(d, 1, @last_disbursement_date)

if @last_disbursement_date is not null
begin
	declare	@payments_since_disbursement	money
	select	@payments_since_disbursement	= sum(isnull(debit_amt,0) - isnull(credit_amt,0))
	from	registers_client_creditor rcc
	where	client_creditor		= @client_creditor
	and	tran_type	in ('AD','BW','MD','CM','RR','VD','RF')
	and	date_created			between @last_disbursement_date and getdate()

	if isnull(@payments_since_disbursement,0) <> 0
		select	@balance	= @balance - @payments_since_disbursement,
			@result		= 'CHG BAL BY $' + convert(varchar, @payments_since_disbursement, 1)
end

declare	@set_bal_verified	int
select	@set_bal_verified	= 0

-- Adjust balances less than $5.00 to be zero
if @balance < 5.00
	select	@balance			= 0.0,
		@result				= 'FORCE ZERO BALANCE',
		@set_bal_verified		= 1

-- Adjust the balance information for the debt
declare	@old_balance		money
select	@old_balance	= orig_balance + orig_balance_adjustment + total_interest - total_payments
from	client_creditor_balances
where	client_creditor_balance		= @client_creditor_balance;

if @old_balance <> @balance
begin
	update	client_creditor_balances
	set	orig_balance_adjustment		= @balance - orig_balance - total_interest + total_payments
	where	client_creditor_balance		= @client_creditor_balance

	-- Add a debt system note that we have changed the balance
	insert into client_notes (client, client_creditor, type, subject, dont_edit, dont_delete, note)
	select	@client, @client_creditor, 3, 'Peregrin Balance Adjusted', 1, 1, 'Adjusted the balance from $' + convert(varchar, @old_balance, 1) + ' to $' + convert(varchar, @balance, 1) + ' due to receiving balance information from the creditor.' + char(13) + char(10) + char(13) + char(10) + 'This was an automatic adjustment from their Excel spreadsheet.'

	select	@set_bal_verified		= 1
end

-- Set the balance verification status
if @set_bal_verified = 1
	update	client_creditor
	set	balance_verify_date		= getdate(),
		balance_verify_by		= 'PEREGRIN',
		verify_request_date		= getdate(),
		send_bal_verify			= 0
	where	client_creditor			= @client_creditor
-- Enable the next line if you want this set only on zero balances
--	and	@balance			= 0

Quit_Procedure:
	return ( @last_error )
GO
