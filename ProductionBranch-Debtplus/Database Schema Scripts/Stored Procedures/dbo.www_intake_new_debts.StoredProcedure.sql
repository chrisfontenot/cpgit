USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_new_debts]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_new_debts] ( @intake_client as int, @intake_id as varchar(20) ) as

-- ==============================================================================================================
-- ==            Start the processing on the new debt information                                              ==
-- ==============================================================================================================

-- Set the proper indicator flags
set nocount	on
set xact_abort	on

-- Validate the client / ID pair
if not exists ( select	*
		from	intake_clients
		where	intake_client	= @intake_client
		and	intake_id	= @intake_id )
begin
	RaisError ('The client ID is not valid', 16, 1)
	return ( 0 )
end

-- Remove the debt information
delete
from	intake_debts
where	intake_client	= @intake_client

-- Update the completion date
update	intake_clients
set	completed_date	= getdate()
where	intake_client	= @intake_client

return ( 1 )
GO
