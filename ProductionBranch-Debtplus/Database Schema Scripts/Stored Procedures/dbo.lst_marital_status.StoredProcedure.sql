USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_marital_status]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_marital_status] AS
-- =====================================================================================================================
-- ==            Retrieve the list of marital status types                                                            ==
-- =====================================================================================================================

-- ChangeLog
--   4/2/2012
--     Use MaritalTypes table rather than messages

select	oID				as 'item_key',
		description		as 'description',
		[Default]		as 'default',
		[ActiveFlag]	as 'ActiveFlag'
from	MaritalTypes with (nolock)
ORDER BY 1

return ( @@rowcount )
GO
