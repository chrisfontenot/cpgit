USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_msa]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Paul Brown
-- Create date: 10/17/*2012
-- Description:	Get MSA based on client zip code
-- =============================================
CREATE PROCEDURE [dbo].[rpt_client_msa]

AS

SET NOCOUNT ON;

select c.client, left(a.postalcode, 5) as zip5, zm.msa_name, c.date_created as client_created
from clients c
inner join addresses a on c.addressid = a.address
inner join zip_msa zm on zm.zipcode = left(a.postalcode, 5)
GO
