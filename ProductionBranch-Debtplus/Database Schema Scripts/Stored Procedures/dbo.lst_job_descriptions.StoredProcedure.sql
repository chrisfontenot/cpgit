USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_job_descriptions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_job_descriptions] AS

-- ===================================================================================================
-- ==                Return the list of standard jobs                                               ==
-- ===================================================================================================

SELECT	job_description		as 'item_key',
	[description]		as 'description'
FROM	job_descriptions WITH ( NOLOCK )

return ( @@rowcount )
GO
