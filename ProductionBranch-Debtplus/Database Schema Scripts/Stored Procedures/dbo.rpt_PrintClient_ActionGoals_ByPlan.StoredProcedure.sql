USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_ActionGoals_ByPlan]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_ActionGoals_ByPlan] ( @ActionPlan AS INT ) AS
-- ====================================================================================
-- ==              Find the goal text for the action plan                            ==
-- ====================================================================================

-- Fetch the information for the client
SELECT
	convert(varchar(8000), [text]) as 'note_text'
FROM	action_items_goals
WHERE	action_plan = @ActionPlan

RETURN ( @@rowcount )
GO
