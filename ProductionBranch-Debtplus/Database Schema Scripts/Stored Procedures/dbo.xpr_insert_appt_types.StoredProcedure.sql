USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_appt_types]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_appt_types] ( @appt_name as varchar(50),@appt_duration as smallint = 0,@default as bit = 0,@housing as bit = 0,@credit as bit = 0,@bankruptcy as bit = 0,@initial_appt as bit = 0,@missed_retention_event as int = null,@contact_type as varchar(4) = null,@create_letter as varchar(10) = null,@reschedule_letter as varchar(10) = null,@cancel_letter as varchar(10) = null,@bankruptcy_class as int = 0 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the appt_types table                     ==
-- ========================================================================================
	insert into appt_types ( [appt_name],[appt_duration],[default],[housing],[credit],[bankruptcy],[initial_appt],[missed_retention_event],[contact_type],[create_letter],[reschedule_letter],[cancel_letter],[bankruptcy_class] ) VALUES ( @appt_name,@appt_duration,@default,@housing,@credit,@bankruptcy,@initial_appt,@missed_retention_event,@contact_type,@create_letter,@reschedule_letter,@cancel_letter,@bankruptcy_class )
	return ( scope_identity() )
GO
