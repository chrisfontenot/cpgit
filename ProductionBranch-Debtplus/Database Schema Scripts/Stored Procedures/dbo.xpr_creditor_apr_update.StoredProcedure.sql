USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_apr_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_apr_update] ( @creditor as varchar(10) ) as

-- ================================================================================================================
-- ==            Update the creditor APR amounts and possibly grandfather the rates to clients                   ==
-- ================================================================================================================

set nocount on
begin transaction

-- Obtain the old information
declare	@old_medium_apr_amt	money
declare	@old_highest_apr_amt	money
declare	@old_lowest_apr_pct	float
declare	@old_medium_apr_pct	float
declare	@old_highest_apr_pct	float
declare	@test_creditor		varchar(10)

select	@test_creditor		= creditor,	-- Just to ensure that we get a valid creditor
	@old_medium_apr_amt	= isnull(medium_apr_amt,0),
	@old_highest_apr_amt	= isnull(highest_apr_amt,0),
	@old_lowest_apr_pct	= coalesce(lowest_apr_pct, medium_apr_pct, highest_apr_pct, 0),
	@old_medium_apr_pct	= coalesce(medium_apr_pct, highest_apr_pct, lowest_apr_pct, 0),
	@old_highest_apr_pct	= coalesce(highest_apr_pct, medium_apr_pct, lowest_apr_pct, 0)
from	update_creditors with (nolock)
where	creditor		= @creditor

if @test_creditor is null	-- Should never be null. It is the primary key to the table.
begin
	rollback transaction
	raiserror ('The creditor %s does not exist in the database', 16, 1, @creditor)
	return ( 0 )
end

-- Build a list of the old debts to be changed
select		cc.client, cc.client_creditor, isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment, 0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as balance, @old_lowest_apr_pct as dmp_interest
into		#creditor_rates
from		client_creditor cc
inner join	client_creditor_balances bal	with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join	clients c			with (nolock) on cc.client = c.client
where		c.active_status in ('A', 'AR')
and		cc.creditor		= @creditor
and		cc.reassigned_debt	= 0
and		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment, 0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0
and		cc.dmp_interest is null
and		cc.first_payment is not null

-- Update the figures for the medium interest rate
update		#creditor_rates
set		dmp_interest	= @old_medium_apr_pct
where		balance	>= @old_medium_apr_amt

-- Update the figures for the highest interest rate
update		#creditor_rates
set		dmp_interest	= @old_highest_apr_pct
where		balance	>= @old_highest_apr_amt

-- Apply the changes to the list of debts
update		client_creditor
set		dmp_interest	= x.dmp_interest
from		client_creditor cc
inner join	#creditor_rates x on cc.client_creditor = x.client_creditor

-- Insert a client note that we changed the rate
insert into client_notes (client, client_creditor, is_text, subject, dont_edit, dont_delete, dont_print, type, note)
select		client, client_creditor, 1, 'APR Rate Changed', 1, 1, 0, 3, 'The APR rate for the client debt was changed to ' + convert(varchar, dmp_interest * 100.0) + '.'
from		#creditor_rates

drop table	#creditor_rates

commit transaction
return ( 1 )
GO
