SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'xpr_housing_arm_v4_client_select' AND TYPE='P')
	EXEC ('CREATE PROCEDURE [dbo].[xpr_housing_arm_v4_client_select] AS ')
GO
ALTER PROCEDURE [dbo].[xpr_housing_arm_v4_client_select] ( @period_start as datetime, @period_end as datetime, @hcs_id as int = null ) as

-- Suppress intermediate results (there are many)
set nocount on

-- Remove the time values from the dates. Make the ending date 1 day later
select	@period_start		= CONVERT(varchar(10), @period_start, 101),
		@period_end			= CONVERT(varchar(10), dateadd(day, 1, @period_end), 101)

-- Delete the partial table from the previous execution
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##hud_9902_clients'' and type = ''U'') drop table ##hud_9902_clients' )
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##hud_9902_workshops'' and type = ''U'') drop table ##hud_9902_workshops' )

-- Create the table to hold the client information
create table ##hud_9902_clients (

	-- Information for the client group (HUD or all other housing)
	is_hud_client														int not null,
	
	-- Information needed for the summary extract
	Ethnicity_Clients_Counseling_Hispanic								int null,
	Ethnicity_Clients_Counseling_No_Response							int null,
	Ethnicity_Clients_Counseling_Non_Hispanic							int null,
	
	Household_Lives_In_Rural_Area										int null,
	Rural_Area_No_Response												int null,
	Household_Does_Not_Live_In_Rural_Area								int null,
	Household_Is_Limited_English_Proficient								int null,
	Limited_English_Proficient_No_Response								int null,
	Household_Is_Not_Limited_English_Proficient							int null,
	
	Less30_AMI_Level													int null,
	a30_49_AMI_Level													int null,
	a50_79_AMI_Level													int null,
	a80_100_AMI_Level													int null,
	Greater100_AMI_Level												int null,
	AMI_No_Response														int null,
	
	MultiRace_Clients_Counseling_AMINDWHT													int null,
	MultiRace_Clients_Counseling_AMRCINDBLK													int null,
	MultiRace_Clients_Counseling_ASIANWHT													int null,
	MultiRace_Clients_Counseling_BLKWHT														int null,
	MultiRace_Clients_Counseling_NoResponse													int null,
	MultiRace_Clients_Counseling_OtherMLTRC													int null,
	Race_Clients_Counseling_American_Indian_Alaskan_Native									int null,
	Race_Clients_Counseling_Asian															int null,
	Race_Clients_Counseling_Black_AfricanAmerican											int null,
	Race_Clients_Counseling_Pacific_Islanders												int null,
	Race_Clients_Counseling_White															int null,

    Compl_HomeMaint_FinMngt																	int null,
    Compl_Workshop_Predatory_Lend															int  null,
    Compl_Help_FairHousing_Workshop															int  null,
    Compl_Resolv_Prevent_Mortg_Deliq														int  null,
    Counseling_Rental_Workshop																int  null,
    Compl_HomeBuyer_Educ_Workshop															int  null,
    Compl_NonDelinqency_PostPurchase_Workshop												int  null,
    Compl_Other_Workshop																	int  null,

    One_Homeless_Assistance_Counseling														int  null,
    One_Rental_Topics_Counseling															int  null,
    One_PrePurchase_HomeBuying_Counseling													int  null,
    One_Home_Maintenance_Fin_Management_Counseling											int  null,
    One_Reverse_Mortgage_Counseling															int  null,
    One_Resolv_Prevent_Mortg_Delinq_Counseling												int  null,

	Impact_One_On_One_And_Group																int  null,
    Impact_Received_Info_Fair_Housing														int  null,
    Impact_Developed_Sustainable_Budget														int  null,
    Impact_Improved_Financial_Capacity														int  null,
    Impact_Gained_Access_Resources_Improve_Housing											int  null,
    Impact_Gained_Access_NonHousing_Resources												int  null,
    Impact_Homeless_Obtained_Housing														int  null,
    Impact_Received_Rental_Counseling_Avoided_Eviction										int  null,
    Impact_Received_Rental_Counseling_Improved_Living_Conditions							int  null,
    Impact_Received_PrePurchase_Counseling_Purchased_Housing								int  null,
    Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM								int  null,
    Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability	int  null,
    Impact_Prevented_Resolved_Mortgage_Default												int  null,

	-- Information needed for the client extract
	Client_ID_Num														int,
	Client_Purpose_Of_Visit												int null,
	Client_Outcome_Of_Visit												int null,
	Client_Case_Num														int null,
	Client_Counselor_ID													int null,
	Client_Head_Of_Household_Type										int null,
	Client_Credit_Score													int null,
	Client_Credit_Score_Source											int null,
	Client_No_Credit_Score_Reason										int null,
	Client_Street_Address_1												varchar(256) null,
	Client_Street_Address_2												varchar(256) null,
	Client_City															varchar(256) null,
	Client_State														int null,
	Client_ZipCode														varchar(256) null,
	Client_New_Street_Address_1											varchar(256) null,
	Client_New_Street_Address_2											varchar(256) null,
	Client_New_City														varchar(256) null,
	Client_New_State													int null,
	Client_New_ZipCode													varchar(256) null,
	Client_First_Name													varchar(80) null,
	Client_Last_Name													varchar(80) null,
	Client_Middle_Name													varchar(80) null,
	Client_Spouse_First_Name											varchar(80) null,
	Client_Spouse_Last_Name												varchar(80) null,
	Client_Spouse_Middle_Name											varchar(80) null,
	Client_Farm_Worker													bit null,
	Client_Colonias_Resident											bit null,
	Client_Disabled														bit null,
	Client_HECM_Certificate												bit null,
	Client_Predatory_Lending											bit null,
	Client_FirstTime_Home_Buyer											bit null,
	Client_Discrimination_Victim										bit null,
	Client_Mortgage_Deliquency											bit null,
	Client_Second_Loan_Exists											bit null,
	Client_Intake_Loan_Type_Is_Hybrid_ARM								bit null,
	Client_Intake_Loan_Type_Is_Option_ARM								bit null,
	Client_Intake_Loan_Type_Is_Interest_Only							bit null,
	Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured						bit null,
	Client_Intake_Loan_Type_Is_Privately_Held							bit null,
	Client_Intake_Loan_Type_Has_Interest_Rate_Reset						bit null,
	Client_Counsel_Session_DT_Start										datetime null,
	Client_Counsel_Session_DT_End										datetime null,
	Client_Intake_DT													datetime null,
	Client_Birth_DT														datetime null,
	Client_HECM_Certificate_Issue_Date									datetime null,
	Client_HECM_Certificate_Expiration_Date								datetime null,
	Client_HECM_Certificate_ID											varchar(80) null,
	Client_Sales_Contract_Signed										datetime null,
	Client_Grant_Amount_Used											money null,
	Client_Mortgage_Closing_Cost										money null,
	Client_Mortgage_Interest_Rate										float null,
	Client_Counseling_Termination										int null,
	Client_Income_Level													int null,
	Client_Highest_Educ_Grade											int null,
	Client_HUD_Assistance												int null,
	Client_Dependents_Num												int null,
	Client_Language_Spoken												int null,
	Client_Session_Duration												int null,
	Client_Counseling_Type												int null,
	Client_Counseling_Fee												money null,
	Client_Attribute_HUD_Grant											int null,
	Client_Finance_Type_Before											int null,
	Client_Finance_Type_After											int null,
	Client_Mortgage_Type												int null,
	Client_Mortgage_Type_After											int null,
	Client_Referred_By													int null,
	Client_Job_Duration													int null,
	Client_Household_Debt												int null,
	Client_Loan_Being_Reported											varchar(1) null,
	Client_Intake_Loan_Type												int null,
	Client_Family_Size													int null,
	Client_Marital_Status												int null,
	Client_Race_ID														int null,
	Client_Ethnicity_ID													int null,
	Client_Activity_Type												int null,
	Client_Household_Gross_Monthly_Income								money null,
	Client_Gender														varchar(10) null,
	Client_Spouse_SSN													varchar(20) null,
	Client_SSN1															varchar(20) null,
	Client_SSN2															varchar(20) null,
	Client_Mobile_Phone_Num												varchar(80) null,
	Client_Phone_Num													varchar(80) null,
	Client_Fax															varchar(80) null,
	Client_Email														varchar(256) null,

	-- Other values related to finding data in our database	
	[language]															int null,
	[office]															int null,
	[hcs_id]															int null,
   	[HUD_Grant]                                                          int null,
	[group_session_id]													int null,
	[person_1]															int null,
	[person_2]															int null,
	[hud_interview]														int null,
	[interview_type]													int null,
	[hud_result]														int null,
	[interview_date]													datetime null,
	[result_date]														datetime null,
	[termination_date]													datetime null,
	[interview_result]													varchar(80) null,
	[ami]																money null,
	[client_appointment]												int null,
	[housing_property]													int null,
	[primary_loan]														int null,
	[secondary_loan]													int null,
	[address]															int null,
	[usda_status]														char(1) not null,
	[Rural_Area]														int,
	[Limited_English_Proficiency]										int,
	[workshop_classification]											int null,
	[is_client]															int not null,
	[is_workshop]														int not null
);

create index ix1_temp_hud_9902_clients on ##hud_9902_clients ( client_id_num );
create index ix2_temp_hud_9902_clients on ##hud_9902_clients ( hud_interview );

-- Load the possible list with everyone who has ever had an interview
insert into ##hud_9902_clients ( is_hud_client, Client_ID_Num, is_client, is_workshop, usda_status, Client_Counseling_Termination, Rural_Area, Rural_Area_No_Response, Limited_English_Proficiency, Household_Is_Not_Limited_English_Proficient, Client_Income_Level )
select distinct 0, client, 2, 0, '?', 1, 3, 1, 2, 1, 7
from	hud_interviews

-- We want to include any interview that has time in the period specified.
select	t.hud_interview, i.client, SUM(t.[minutes]) as minutes
into	#times
from	hud_transactions t with (nolock)
inner join hud_interviews i with (nolock) on t.hud_interview = i.hud_interview
where	t.date_created	>= @period_start
and		t.date_created	<  @period_end
group by t.hud_interview, i.client;

update	##hud_9902_clients
set		Client_Session_Duration		= b.minutes,
		hud_interview				= b.hud_interview
from	##hud_9902_clients x
inner join #times b on x.client_id_num = b.client;

drop table #times

-- Include any interview that may not have time but the interview is in the date indicated.
update	##hud_9902_clients
set		hud_interview				= i.hud_interview
from	##hud_9902_clients c
inner join hud_interviews i on c.Client_ID_Num = i.client
where	i.interview_date >= @period_start and i.interview_date <  @period_end
and		c.hud_interview is null;

-- Set the other parameters based upon the interview selected
update	##hud_9902_clients
set		hud_result						= i.hud_result,
        hud_grant                       = i.hud_grant,
		interview_type					= i.interview_type,
		interview_date					= i.interview_date,
		result_date						= i.result_date,
		termination_date				= i.termination_date,
		Client_Counsel_Session_DT_Start	= i.interview_date,
		Client_Intake_DT				= i.interview_date,
		Client_Counsel_Session_DT_End	= i.result_date,
		Client_Counseling_Fee			= i.HousingFeeAmount,
		Client_Attribute_HUD_Grant		= dbo.map_hud_9902_grant ( i.HUD_grant )
from	##hud_9902_clients x
inner join hud_interviews i on x.hud_interview = i.hud_interview;

-- Create the table for the workshops
create table ##hud_9902_workshops (
	group_session_id					int,
	group_session_counselor_id			int,
	group_session_title					varchar(80) null,
	group_session_date					datetime,
	group_session_duration				int null,
	group_session_type					varchar(80) null,
	group_session_attribute_hud_grant	varchar(80) null,
	workshop_type						int,
	hcs_id								int null
	);

insert into ##hud_9902_workshops ([group_session_id], [group_session_date], [group_session_title], [workshop_type], [group_session_duration], [group_session_counselor_id], [group_session_attribute_hud_grant], [hcs_id])
select	w.workshop				as group_session_id,
		w.start_time			as group_session_date,
		left(t.description,50)	as group_session_title,
		t.workshop_type			as workshop_type,
		t.duration				as group_session_duration,
		w.CounselorID			as group_session_counselor_id,
		w.HUD_Grant             as group_session_attribute_hud_grant,
		l.hcs_id				as hcs_id
from	workshops w with (nolock)
inner join workshop_types t with (nolock) on w.workshop_type = t.workshop_type
left outer join workshop_locations l with (nolock) on w.workshop_location = l.workshop_location
where	(w.start_time >= @period_start and w.start_time < @period_end)
and		l.hcs_id is not null

-- Update the content type with the first item that can be used
update	##hud_9902_workshops
set		[group_session_type]	= t.hud_9902_section
from	##hud_9902_workshops x
inner join workshop_contents c on x.workshop_type = c.workshop_type
inner join workshop_content_types t on c.content_type = t.content_type
where	t.hud_9902_section is not null;

-- Discard the items that we can not use
delete
from	##hud_9902_workshops
where	[group_session_counselor_id] is null
or		[group_session_type] is null
or		[group_session_attribute_hud_grant] is null;

insert into ##hud_9902_clients ( is_hud_client, client_id_num, is_client, is_workshop, Client_Counseling_Fee, Client_Referred_By, group_session_id, Client_Counselor_ID, Client_Counseling_Termination, client_appointment, Compl_Other_Workshop, hud_grant, hcs_id, usda_status, Rural_Area, Rural_Area_No_Response, Limited_English_Proficiency, Household_Is_Not_Limited_English_Proficient, Client_Income_Level )
select	0						 as 'is_hud_client',
		ca.client				 as 'Attendee_id',
		0						 as 'is_client',
		1						 as 'is_workshop',
		ca.HousingFeeAmount		 as 'Attendee_Fee_Amount',
		dbo.map_hud_9902_referred_by ( ca.referred_by ) 			 as 'Attendee_Referred_By',
		ca.workshop				 as 'group_session_id',
		ca.counselor			 as 'Client_Counselor_ID',
		1						 as 'Client_Counseling_Termination',
		ca.client_appointment    as 'client_appointment',
		1						 as 'Compl_Other_Workshop',
        w.group_session_attribute_hud_grant as 'hud_grant',
		w.hcs_id				 as 'hcs_id',
		'?'						 as 'usda_status',
		3, 1, 2, 1, 7

FROM	client_appointments ca
inner join ##hud_9902_workshops w on ca.workshop = w.group_session_id
where	ca.status in ('K','W');

-- Toss everything else as being not acceptable
delete
from	##hud_9902_clients
where	isnull(hud_interview,0) = 0
and		is_client > 0;

-- Do all static item calculations that are based upon only the result table
update	##hud_9902_clients
set		ami										= isnull(dbo.ami ( Client_ID_Num ),0),
		Client_Household_Gross_Monthly_Income	= isnull(dbo.client_gross_income ( Client_ID_Num ),0) + isnull(dbo.client_other_income ( Client_ID_Num ) / 12.0,0)

-- Determine the various levels needed. These need to be annual figures so scale by months/year.
update	##hud_9902_clients set Client_Income_Level =  8 where (Client_Household_Gross_Monthly_Income * 12.0) > ami * 0.60;	-- more than 30 percent
update	##hud_9902_clients set Client_Income_Level =  9 where (Client_Household_Gross_Monthly_Income * 12.0) > ami;			-- more than 50 percent
update	##hud_9902_clients set Client_Income_Level = 10 where (Client_Household_Gross_Monthly_Income * 12.0) > ami * 1.60;	-- more than 80 percent
update	##hud_9902_clients set Client_Income_Level = 10 where (Client_Household_Gross_Monthly_Income * 12.0) > ami * 2.00;	-- more than 100 percent
update	##hud_9902_clients set Client_Income_Level = 12 where ami = 0;														-- choose not to respond

-- Set the referrral information since the appointments have there own values
update	##hud_9902_clients
set		[Client_Referred_By]			= dbo.map_hud_9902_referred_by ( c.referred_by ),
		[Client_Counselor_ID]			= c.[counselor],
		[language]						= c.[language],
		[address]						= c.[addressid],
		[Client_Marital_Status]			= dbo.map_hud_9902_marital ( c.marital_status ),
		[Client_Language_Spoken]		= dbo.map_hud_9902_language ( c.language ),
		[Client_Family_Size]			= dbo.family_size ( c.client ),
		[Client_Dependents_Num]			= c.dependents,
		[Client_Head_Of_Household_Type]	= dbo.map_hud_9902_HouseHoldHead ( c.Household ),
		[office]						= c.office
from	##hud_9902_clients x
inner join clients c on c.client = x.Client_ID_Num;

-- Set the ID from the office table
update	##hud_9902_clients
set		hcs_id							= o.hud_hcs_id
from	##hud_9902_clients c
inner join offices o on c.office = o.office
where	c.is_client > 0;

-- Discard the invalid items
delete
from	##hud_9902_clients
where	isnull(hcs_id,0) <= 0

-- Determine the information from the home address data
update	##hud_9902_clients
set		Client_City					= isnull(a.city,''),
		Client_ZipCode				= LEFT(isnull(a.postalcode,'00000'), 5),
		Client_Street_Address_1		= isnull(dbo.format_Address_Line_1 ( a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),''),
		Client_Street_Address_2		= isnull(a.address_line_2,''),
		Client_State				= isnull(dbo.map_hud_9902_state ( a.state ),61),
		usda_status					= isnull(a.usda_status,'?')
from	##hud_9902_clients x
left outer join addresses a on x.address = a.address

-- Tag the HUD clients based upon the grant information
update	##hud_9902_clients
set		is_hud_client	= 1
where   hud_grant		= 88;

-- Make the household limited english if the language is not english
update	##hud_9902_clients
set		Household_Is_Limited_English_Proficient		= 1,
		Household_Is_Not_Limited_English_Proficient	= 0,
		Limited_English_Proficiency = 1
where	language <> 1	-- ENGLISH

-- Determine the applicant and co-applicant record addresses
update	##hud_9902_clients set person_1 = p.person from ##hud_9902_clients x inner join people p on x.client_id_num = p.client and 1 = p.relation;
update	##hud_9902_clients set person_2 = p.person from ##hud_9902_clients x inner join people p on x.client_id_num = p.client and 1 <> p.relation;

-- Obtain the information about the job data, ssn, race, gender, and ethnicity
update	##hud_9902_clients
set		Client_Job_Duration			= DATEDIFF(MONTH, p.emp_start_date, GETDATE()),
		Client_Birth_DT				= p.Birthdate,
		Client_SSN1					= p.ssn,
		Client_Race_ID				= dbo.map_hud_9902_Race(p.race, p.ethnicity),
		Client_Ethnicity_ID			= dbo.map_hud_9902_Ethnicity(p.ethnicity,p.race),
		Client_Gender				= case p.gender when 1 then 'M' else 'F' end,
		Client_Credit_Score			= p.FICO_Score,
		Client_Credit_Score_Source	= dbo.map_hud_9902_credit_agency ( p.CreditAgency ),
		Client_Highest_Educ_Grade	= dbo.map_hud_9902_education ( p.education ),
		Client_Disabled				= p.[Disabled]
from	##hud_9902_clients x
inner join people p on x.person_1 = p.Person;

-- Set the email address
update	##hud_9902_clients
set		Client_Email				= e.Address
from	##hud_9902_clients x
inner join people p on x.person_1 = p.Person
inner join EmailAddresses e on p.EmailID = e.Email;

update	##hud_9902_clients
set		Client_Credit_Score				= null,
		Client_Credit_Score_Source		= null,
		Client_No_Credit_Score_Reason	= 3
where	isnull(Client_Credit_Score,0)	= 0;

-- Use the termination date if one is given
update	##hud_9902_clients
set		Client_Job_Duration = DATEDIFF(month, p.emp_end_date, p.emp_start_date)
from	##hud_9902_clients x
inner join people p on x.person_1 = p.Person
where	p.emp_end_date is not null;

-- Define the client name
update	##hud_9902_clients
set		Client_First_Name		= n.first,
		Client_Middle_Name		= n.middle,
		Client_Last_Name		= n.last
from	##hud_9902_clients x
inner join people p on x.person_1 = p.Person
inner join Names n on p.nameID = n.Name;

-- Find the spouse's name
update	##hud_9902_clients
set		Client_Spouse_First_Name	= n.first,
		Client_Spouse_Middle_Name	= n.middle,
		Client_Spouse_Last_Name		= n.last
from	##hud_9902_clients x
inner join people p on x.person_2 = p.Person
inner join Names n on p.nameID = n.Name;

-- Find the spouse's SSN
update	##hud_9902_clients
set		Client_Spouse_SSN			= p.ssn
from	##hud_9902_clients x
inner join people p on x.person_2 = p.Person

-- Find the last 4 digits of the client's ssn
update	##hud_9902_clients
set		Client_SSN2		= RIGHT(client_ssn1,4)

-- Find the cell telephone number
update	##hud_9902_clients
set		Client_Mobile_Phone_Num	= dbo.format_hud_9902_TelephoneNumber ( p.CellTelephoneID )
from	##hud_9902_clients x
inner join people p on x.person_1	= p.Person;

-- Find the home telephone number
update	##hud_9902_clients
set		Client_Phone_Num			= dbo.format_hud_9902_TelephoneNumber ( c.HomeTelephoneID )
from	##hud_9902_clients x
inner join clients c on x.client_id_num	= c.client;

-- USDA housing status
update	##hud_9902_clients
set		Household_Lives_In_Rural_Area			= 1,
		Rural_Area_No_Response					= 0,
		Rural_Area								= 1
where	usda_status								= 'Y';

update	##hud_9902_clients
set		Household_Does_Not_Live_In_Rural_Area	= 1,
		Rural_Area_No_Response					= 0,
		Rural_Area								= 2
where	usda_status								= 'N';

-- Update information from the housing record
update	##hud_9902_clients
set		Client_Colonias_Resident				= isnull(c.HUD_colonias,0),
		Client_Farm_Worker						= isnull(c.HUD_migrant_farm_worker,0),
		Client_Predatory_Lending				= isnull(c.HUD_predatory_lending,0),
		Client_FirstTime_Home_Buyer				= isnull(c.HUD_FirstTimeHomeBuyer,0),
		Client_Discrimination_Victim			= isnull(c.HUD_DiscriminationVictim,0),
		Client_HUD_Assistance					= isnull(dbo.map_hud_9902_hud_assistance(c.HUD_Assistance), 8),
		Client_HECM_Certificate					= case when isnull(c.HECM_certificate_id,'') = '' then 0 else 1 end,
		Client_HECM_Certificate_Expiration_Date	= c.HECM_Certificate_expires,
		Client_HECM_Certificate_Issue_Date		= c.HECM_Certificate_Date
from	##hud_9902_clients x
left outer join client_housing c on x.Client_ID_Num = c.client

-- Locate the property for the client
update	##hud_9902_clients
set		housing_property				= p.oID,
		Client_Sales_Contract_Signed	= p.Sales_contract_date
from	##hud_9902_clients x
inner join housing_properties p on x.Client_ID_Num = p.HousingID
where	p.UseInReports		= 1;

-- Set the loan position
update	##hud_9902_clients
set		primary_loan		= l.oID
from	##hud_9902_clients x
inner join housing_properties p on x.housing_property = p.oID
inner join housing_loans l on p.oID = l.PropertyID
where	l.UseInReports		= 1;

-- Find the secondary position
update	##hud_9902_clients
set		secondary_loan		= l.oID
from	##hud_9902_clients x
inner join housing_properties p on x.housing_property = p.oID
inner join housing_loans l on p.oID = l.PropertyID
where	l.UseInReports		= 0;

-- Look for the status of the loan being reports
update	##hud_9902_clients
set		Client_Loan_Being_Reported	= 'F'

update	##hud_9902_clients
set		Client_Loan_Being_Reported	= 'S'
from	##hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
where	l.Loan1st2nd		= 2;

-- Determine if there is a secondary loan
update	##hud_9902_clients
set		Client_Second_Loan_Exists	= 0;

update	##hud_9902_clients
set		Client_Second_Loan_Exists	= 1
where	secondary_loan				is not null;

update	##hud_9902_clients
set		Client_Second_Loan_Exists	= 1
where	Client_Loan_Being_Reported	= 'S';

-- Find the type of the loan from the primary load information
update	##hud_9902_clients
set		Client_Intake_Loan_Type_Is_Hybrid_ARM		= d.Hybrid_ARM_Loan,
		Client_Intake_Loan_Type_Is_Interest_Only	= d.Interest_Only_Loan,
		Client_Mortgage_Closing_Cost				= l.MortgageClosingCosts,
		Client_Intake_Loan_Type_Is_Option_ARM		= d.Option_ARM_Loan,
		Client_Intake_Loan_Type_Is_Privately_Held	= d.Privately_Held_Loan,
		Client_Intake_Loan_Type						= dbo.map_hud_9902_LoanType( d.MortgageTypeCD ),
		Client_Mortgage_Interest_Rate				= d.InterestRate
from	##hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
left outer join housing_loan_details d on l.IntakeDetailID = d.oID

-- Set the FHA/VA insured loan status
update	##hud_9902_clients
set		Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured	= 1
from	##hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
inner join housing_loan_details d on l.IntakeDetailID = d.oID
where	d.FHA_VA_Insured_Loan <> 0

-- Set the ARM reset status
update	##hud_9902_clients
set		Client_Intake_Loan_Type_Has_Interest_Rate_Reset	= 1
from	##hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
inner join housing_loan_details d on l.IntakeDetailID = d.oID
where	d.Arm_Reset <> 0;

-- Seed the intake date from the date that the client was created
update	##hud_9902_clients
set		Client_Intake_DT				= c.date_created
from	##hud_9902_clients x
inner join clients c on x.Client_ID_Num = c.client

-- Use the date that the first appointment was created if possible
update	##hud_9902_clients
set		Client_Intake_DT				= ca.date_created
from	##hud_9902_clients x
inner join clients c on x.Client_ID_Num = c.client
inner join client_appointments ca on c.first_appt = ca.client_appointment

-- Finally, set the intake date from the appointment based upon the HUD transaction
update	##hud_9902_clients
set		Client_Intake_DT				= ca.date_created
from	##hud_9902_clients x
inner join hud_transactions t on x.hud_interview = t.hud_interview
inner join client_appointments ca on t.client_appointment	= ca.client_appointment

-- Set the household debt information
update	##hud_9902_clients
set		client_household_debt					= isnull(dbo.client_unsecured_debt_payment ( client_id_num ),0)
												+ isnull(dbo.client_other_debt_payment ( client_id_num ), 0)
												+ isnull(dbo.budget_client_total ( dbo.map_client_to_budget ( client_id_num )), 0)

-- Ensure that there is a valid result for each interview type
update	##hud_9902_clients
set		hud_result					= b.Outcome
from	##hud_9902_clients x
inner join housing_AllowedVisitOutcomeTypes b on x.interview_type = b.PurposeOfVisit
where	b.[Default]					= 1
and		x.hud_result is null;

-- Set the purpose of visit and the outcome
update	##hud_9902_clients
set		Client_Purpose_Of_Visit		= dbo.map_hud_9902_PurposeOfVisit ( interview_type ),
		Client_Outcome_Of_Visit		= dbo.map_hud_9902_VisitOutcome ( interview_type, hud_result ),
		Client_Case_Num				= Client_ID_Num,
		Client_Activity_Type		= 4,			-- known value for "all non-HUD counseling"
		Client_Counseling_Type		= 2;			-- assume "face to face" unless otherwise

-- Correct the otherwise values
update	##hud_9902_clients
set		Client_Counseling_Type	= dbo.map_hud_9902_contact_method ( apt.contact_type )
from	##hud_9902_clients x
inner join hud_transactions t on x.hud_interview = t.hud_interview
inner join client_appointments ca on t.client_appointment = ca.client_appointment
inner join appt_types apt on ca.appt_type = apt.appt_type

-- Group sessions are for workshops
update	##hud_9902_clients
set		Client_Counseling_Type		= 5 -- group
where	is_workshop					= 1;

-- ==============================================================================================
-- ==              SUMMARY INFORMATION                                                         ==
-- ==============================================================================================

-- Ethnicity
update	##hud_9902_clients
set		Ethnicity_Clients_Counseling_Hispanic		= 1
where	Client_Ethnicity_ID							= 2

update	##hud_9902_clients
set		Ethnicity_Clients_Counseling_Non_Hispanic	= 1
where	Client_Ethnicity_ID							= 3

update	##hud_9902_clients
set		Ethnicity_Clients_Counseling_No_Response	= 1
where	isnull(Client_Ethnicity_ID,0)				not in (2, 3)

-- Race
update	##hud_9902_clients
set		MultiRace_Clients_Counseling_AMINDWHT		= 1
where	Client_Race_ID								= 7

update	##hud_9902_clients
set		MultiRace_Clients_Counseling_AMRCINDBLK		= 1
where	Client_Race_ID								= 10

update	##hud_9902_clients
set		MultiRace_Clients_Counseling_ASIANWHT		= 1
where	Client_Race_ID								= 8

update	##hud_9902_clients
set		MultiRace_Clients_Counseling_BLKWHT			= 1
where	Client_Race_ID								= 9

update	##hud_9902_clients
set		MultiRace_Clients_Counseling_OtherMLTRC		= 1
where	Client_Race_ID								= 11

update	##hud_9902_clients
set		Race_Clients_Counseling_American_Indian_Alaskan_Native	= 1
where	Client_Race_ID								= 2

update	##hud_9902_clients
set		Race_Clients_Counseling_Asian				= 1
where	Client_Race_ID								= 3

update	##hud_9902_clients
set		Race_Clients_Counseling_Black_AfricanAmerican	= 1
where	Client_Race_ID								= 4

update	##hud_9902_clients
set		Race_Clients_Counseling_Pacific_Islanders	= 1
where	Client_Race_ID								= 5

update	##hud_9902_clients
set		Race_Clients_Counseling_White				= 1
where	Client_Race_ID								= 6

update	##hud_9902_clients
set		MultiRace_Clients_Counseling_NoResponse		= 1
where	isnull(Client_Race_ID,0)					not in (2, 3, 4, 5, 6, 7, 8, 9, 10, 11)

-- AMI levels
update	##hud_9902_clients set Less30_AMI_Level     = 1 where Client_Income_Level = 7;
update	##hud_9902_clients set a30_49_AMI_Level		= 1 where Client_Income_Level = 8;
update	##hud_9902_clients set a50_79_AMI_Level		= 1 where Client_Income_Level = 9;
update	##hud_9902_clients set a80_100_AMI_Level	= 1 where Client_Income_Level = 10;
update	##hud_9902_clients set Greater100_AMI_Level = 1 where Client_Income_Level = 11;
update	##hud_9902_clients set AMI_No_Response		= 1 where Client_Income_Level = 12;

-- ==============================================================================================
-- ==              WORKSHOPS                                                                   ==
-- ==============================================================================================

-- Set the "completed some other workshop" status for the clients
update	##hud_9902_clients
set		Compl_Other_Workshop		= 1
from	##hud_9902_clients
where	is_workshop = 1

update	##hud_9902_clients
set		workshop_classification = dbo.map_hud_9902_group_session_type ( wct.hud_9902_section )
from	##hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	x.is_workshop = 1
and		wct.hud_9902_section is not null;

-- Now, look for a workshop that says "FairHousing"
update	##hud_9902_clients
set		Compl_Help_FairHousing_Workshop	= 1,
		Compl_Other_Workshop			= 0
where	is_workshop = 1
and     workshop_classification = 12

-- Now, look for a workshop that says "HomeBuyer_Educ"
update	##hud_9902_clients
set		Compl_HomeBuyer_Educ_Workshop	= 1,
		Compl_Other_Workshop			= 0
from	##hud_9902_clients x
where	is_workshop = 1
and     workshop_classification = 15

-- Now, look for a workshop that says "HomeMaint_FinMngt"
update	##hud_9902_clients
set		Compl_HomeMaint_FinMngt	= 1,
		Compl_Other_Workshop	= 0
from	##hud_9902_clients x
where	is_workshop = 1
and     workshop_classification = 10

-- Now, look for a workshop that says "Resolv_Prevent_Mortg_Deliq"
update	##hud_9902_clients
set		Compl_Resolv_Prevent_Mortg_Deliq	= 1,
		Compl_Other_Workshop				= 0
from	##hud_9902_clients x
where	is_workshop = 1
and     workshop_classification = 17

-- Now, look for a workshop that says "HomeFin_Credit_Repair"
update	##hud_9902_clients
set		Compl_NonDelinqency_PostPurchase_Workshop	= 1,
		Compl_Other_Workshop						= 0
from	##hud_9902_clients x
where	is_workshop = 1
and     workshop_classification = 10

-- Now, look for a workshop that says "Predatory_Lend"
update	##hud_9902_clients
set		Compl_Workshop_Predatory_Lend	= 1,
		Compl_Other_Workshop			= 0
from	##hud_9902_clients x
where	is_workshop = 1
and     workshop_classification = 11

-- Now, look for a workshop that says "Rental"
update	##hud_9902_clients
set		Counseling_Rental_Workshop	= 1,
		Compl_Other_Workshop		= 0
from	##hud_9902_clients x
where	is_workshop = 1
and     workshop_classification = 14

-- Set the One-On-One counseling session information
update ##hud_9902_clients set is_client = 1, [One_Homeless_Assistance_Counseling]             = 1 where [Client_Purpose_Of_Visit]=6
update ##hud_9902_clients set is_client = 1, [One_Rental_Topics_Counseling]                   = 1 where [Client_Purpose_Of_Visit]=7
update ##hud_9902_clients set is_client = 1, [One_PrePurchase_HomeBuying_Counseling]          = 1 where [Client_Purpose_Of_Visit]=8
update ##hud_9902_clients set is_client = 1, [One_Home_Maintenance_Fin_Management_Counseling] = 1 where [Client_Purpose_Of_Visit]=9
update ##hud_9902_clients set is_client = 1, [One_Reverse_Mortgage_Counseling]                = 1 where [Client_Purpose_Of_Visit]=10
update ##hud_9902_clients set is_client = 1, [One_Resolv_Prevent_Mortg_Delinq_Counseling]     = 1 where [Client_Purpose_Of_Visit]=11

/* -- This should not be needed as it is reflected in the standard table. But, just in case, here it the logic.
-- ===========================================================================================================
-- ==                     Map the standard impact types                                                     ==
-- ===========================================================================================================
update		##hud_9902_clients
set			[Impact_One_On_One_And_Group]				= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 1

update		##hud_9902_clients
set			[Impact_Received_Info_Fair_Housing]			= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 2

update		##hud_9902_clients
set			[Impact_Developed_Sustainable_Budget]		= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 3

update		##hud_9902_clients
set			[Impact_Improved_Financial_Capacity]		= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 4

update		##hud_9902_clients
set			[Impact_Gained_Access_Resources_Improve_Housing]	= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 5

update		##hud_9902_clients
set			[Impact_Gained_Access_NonHousing_Resources]	= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 6

update		##hud_9902_clients
set			[Impact_Homeless_Obtained_Housing] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 7

update		##hud_9902_clients
set			[Impact_Received_Rental_Counseling_Avoided_Eviction] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 8

update		##hud_9902_clients
set			[Impact_Received_Rental_Counseling_Improved_Living_Conditions] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 9

update		##hud_9902_clients
set			[Impact_Received_PrePurchase_Counseling_Purchased_Housing] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 10

update		##hud_9902_clients
set			[Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 11

update		##hud_9902_clients
set			[Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 12

update		##hud_9902_clients
set			[Impact_Prevented_Resolved_Mortgage_Default] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_ImpactResultTypes t ON x.[interview_type] = t.POVType and x.[hud_result] = t.ResultType
where		t.ImpactType = 13
*/

-- ===========================================================================================================
-- ==                     Map the user impact types                                                         ==
-- ===========================================================================================================
update		##hud_9902_clients
set			[Impact_One_On_One_And_Group]				= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 1

update		##hud_9902_clients
set			[Impact_Received_Info_Fair_Housing]			= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 2

update		##hud_9902_clients
set			[Impact_Developed_Sustainable_Budget]		= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 3

update		##hud_9902_clients
set			[Impact_Improved_Financial_Capacity]		= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 4

update		##hud_9902_clients
set			[Impact_Gained_Access_Resources_Improve_Housing]	= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 5

update		##hud_9902_clients
set			[Impact_Gained_Access_NonHousing_Resources]	= 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 6

update		##hud_9902_clients
set			[Impact_Homeless_Obtained_Housing] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 7

update		##hud_9902_clients
set			[Impact_Received_Rental_Counseling_Avoided_Eviction] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 8

update		##hud_9902_clients
set			[Impact_Received_Rental_Counseling_Improved_Living_Conditions] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 9

update		##hud_9902_clients
set			[Impact_Received_PrePurchase_Counseling_Purchased_Housing] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 10

update		##hud_9902_clients
set			[Impact_Received_Reverse_Mortgage_Counseling_Obtained_HECM] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 11

update		##hud_9902_clients
set			[Impact_Received_NonDelinquency_PostPurchase_Counseling_Improve_Conditions_Affordability] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 12

update		##hud_9902_clients
set			[Impact_Prevented_Resolved_Mortgage_Default] = 1, is_client = 1
from		##hud_9902_clients x
inner join	Housing_Client_Impacts t ON x.[hud_interview] = t.[POVId]
where		t.ImpactId = 13

-- Toss the client POVs that do not match.
-- delete from ##hud_9902_clients where is_client = 2

-- Set the proper activity type based upon the level of the client.
/*
update	##hud_9902_clients
set		Client_Activity_Type = 4
where	is_hud_client		 = 1
*/

-- Since the agency does not want to send SSN values, clear them
update	##hud_9902_clients
set		Client_Spouse_SSN			= '0000000',
		Client_SSN1					= '0000000',
		Client_SSN2					= '0000'

-- =========================================================================================================
-- ==         Supply bogus values for missing items that should not need to be reported                   ==
-- =========================================================================================================

-- These fields have minimum values and can not take the default of "0" meaning "missing".
update  ##hud_9902_clients set Client_Intake_Loan_type = 1      where isnull(Client_Intake_Loan_type,0) <= 0
update  ##hud_9902_clients set Client_Attribute_HUD_Grant = 1   where isnull(Client_Attribute_HUD_Grant,0) <= 0

-- Use NULLS for missing items rather than blanks since the program tests for NULL values and not blanks.
update	##hud_9902_clients set client_first_name = null         where client_first_name = '';
update	##hud_9902_clients set client_middle_name = null        where client_middle_name = '';
update	##hud_9902_clients set client_last_name = null          where client_last_name = '';
update	##hud_9902_clients set client_spouse_first_name = null  where client_spouse_first_name = '';
update	##hud_9902_clients set client_spouse_middle_name = null where client_spouse_middle_name = '';
update	##hud_9902_clients set client_spouse_last_name = null   where client_spouse_last_name = '';

-- Toss items that don't have a counselor. A counselor is required for HUD.
delete	##hud_9902_clients where Client_Counselor_ID IS NULL
GO
GRANT EXECUTE ON xpr_housing_arm_v4_client_select TO public AS dbo;
GO
