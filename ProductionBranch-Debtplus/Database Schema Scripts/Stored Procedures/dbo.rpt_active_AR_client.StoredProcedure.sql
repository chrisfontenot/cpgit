USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_active_AR_client]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_active_AR_client] (@FromDate as DateTime = Null, @ToDate as DateTime = Null) as
-- ==========================================================================================================
-- ==            Report the active clients and fairshare contributions                                     ==
-- ==========================================================================================================

-- ChangeLog
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Suppress the intermediate results
set nocount on

-- Correct the date values
if @ToDate is null
	select @ToDate = getdate()

if @FromDate is null
	select @FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Retrieve the information
SELECT  rdd.client									as 'client',
       	dbo.format_normal_name(default, pn.first, pn.middle, pn.last, default)		as 'name',
	a.city										as 'city',
       	st.MailingCode									as 'state',
        left(isnull(a.postalcode,''),5)							as 'zipcode',
       	sum(rdd.fairshare_amt + case ccl.zero_balance when 1 then debit_amt else 0 end)	as 'fair_share'

FROM	clients c with (nolock)
left outer join addresses a with (nolock) on c.AddressID = a.Address
LEFT OUTER JOIN states st with (nolock) on a.state = st.state
LEFT OUTER JOIN registers_client_creditor rdd with (nolock)	ON c.client = rdd.client
LEFT OUTER JOIN people p with (nolock)				ON rdd.client = p.client AND 1 = p.relation 
left outer join names pn with (nolock) on p.NameID = pn.Name
INNER JOIN creditors cr with (nolock)				ON cr.creditor = rdd.creditor
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK)		ON cr.creditor_class = ccl.creditor_class

WHERE	c.active_status IN ('A', 'AR')
and	rdd.tran_type in ('AD', 'BW', 'CM', 'MD')
and	rdd.date_created between @FromDate and @ToDate

group by rdd.client, pn.first, pn.middle, pn.last, a.city, a.state, a.postalcode, st.MailingCode
order by 5 -- postalcode

return ( @@rowcount )
GO
