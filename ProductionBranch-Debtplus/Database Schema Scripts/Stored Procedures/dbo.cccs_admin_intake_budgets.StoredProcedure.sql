USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_budgets]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_budgets] ( @application_id as int, @client as int = null ) as

-- Find the client ID from the application if one is not passed to us
if @client is null
	select	@client	= ForeignDatabaseID
	from	cccs_admin..applications
	where	application_id = @application_id
	
if @client is null
begin
	RaisError ('Client is NULL and may not be determined', 16, 1)
	return ( -1 )
end

-- Do not allow the client to be loaded if the client is active
declare	@active_status			varchar(10)

select	@active_status  = active_status
from	clients
where	client			= @client

if isnull(@active_status,'A') in ('A','AR')
	return ( 0 )

-- Load the budget expenses. These may come from the budget_expenses table as well.
execute cccs_admin_intake_budget_expenses	@application_id, @client

-- Load the debts. These are taken only from the budget_creditors table.
-- The previous debts will have been determined to be deleted from the admin system and therefore deleted
-- from DebtPlus until there is a linkage between the two tables (application_creditors and budget_creditors)!!!
execute	cccs_admin_intake_budget_debts		@application_id, @client

-- Load the monthly fee debt. This comes from the budget_expenses table.
execute cccs_admin_intake_X0001				@application_id, @client

-- Load the deposit information. This may come from the budget_expenses table. (Must be done after the debts are loaded!!)
execute cccs_admin_intake_budget_deposits	@application_id, @client

-- Update the other income figure. This comes from the budgets table.
execute cccs_admin_intake_assets			@application_id, @client

-- Override the income fields for the applicant and co-applicant from the budget_expenses table.
execute cccs_admin_intake_income			@application_id, @client

-- Complete the processing here
return ( @client )
GO
