ALTER PROCEDURE [dbo].[xpr_hecm_search_client] (@TelephoneNumberKey AS VARCHAR(10))
AS
BEGIN
	-- Suppress intermediate results
	SET NOCOUNT ON

	DECLARE @TempTable TABLE (
		ClientID INT
		,NAME VARCHAR(50)
		,Active_Status VARCHAR(10)
		,STATUS VARCHAR(10)
		,HomeTelephone VARCHAR(20)
		);
	DECLARE @Numbers TABLE (
		[TelephoneNumber] INT
		,[Country] INT
		,[Acode] [varchar](50)
		,[Number] [varchar](50)
		,[Ext] [varchar](50)
		,[SearchValue] [varchar](50)
		,[old_telephonenumber] [int]
		,[user_selected_type] [varchar](50)
		);

	-- First, create a temp table with the telephonenumber entries that match the desired id
	--SELECT TelephoneNumber
	--INTO   #Numbers
	--FROM   TelephoneNumbers t WITH (NOLOCK)
	--WHERE t.[SearchValue]=@TelephoneNumberKey
	INSERT INTO @Numbers (
		[TelephoneNumber]
		,[Country]
		,[Acode]
		,[Number]
		,[Ext]
		,[SearchValue]
		)
	SELECT [TelephoneNumber]
		,[Country]
		,[Acode]
		,[Number]
		,[Ext]
		,[SearchValue]
	FROM TelephoneNumbers t WITH (NOLOCK)
	WHERE t.[SearchValue] = @TelephoneNumberKey

	-- Create a clustered index to help the following two selects
	--create unique clustered index temp_ix_telephoneNumbers_numbers on @Numbers ( TelephoneNumber )
	-- Build a list of the clients who match the record
	--create table #clients (client int)
	DECLARE @clients TABLE (client INT)

	INSERT INTO @clients (client)
	SELECT client
	FROM clients c WITH (NOLOCK)
	INNER JOIN OCS_Client AS OC ON c.client = OC.ClientId
	INNER JOIN @Numbers n1 ON c.HomeTelephoneID = n1.TelephoneNumber

	INSERT INTO @clients (client)
	SELECT client
	FROM clients c WITH (NOLOCK)
	INNER JOIN OCS_Client AS OC ON c.client = OC.ClientId
	INNER JOIN @Numbers n1 ON c.MsgTelephoneID = n1.TelephoneNumber

	-- Build a list of the people who have the telephone number
	INSERT INTO @clients (client)
	SELECT client
	FROM people p WITH (NOLOCK)
	INNER JOIN OCS_Client AS OC ON p.client = OC.ClientId
	INNER JOIN @Numbers n1 ON p.WorkTelephoneID = n1.TelephoneNumber

	INSERT INTO @clients (client)
	SELECT client
	FROM people p WITH (NOLOCK)
	INNER JOIN OCS_Client AS OC ON p.client = OC.ClientId
	INNER JOIN @Numbers n1 ON p.CellTelephoneID = n1.TelephoneNumber

	-- Retrieve the results
	SELECT c.client AS clientID
		,dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) AS NAME
		,c.active_status
		,p.ssn
		,dbo.Format_TelephoneNumber(c.HomeTelephoneID) AS homeTelephone
	FROM clients c WITH (NOLOCK)
	INNER JOIN people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
	LEFT OUTER JOIN names pn WITH (NOLOCK) ON p.NameID = pn.NAME
	WHERE c.client IN (
			SELECT client
			FROM @clients
			)
END
