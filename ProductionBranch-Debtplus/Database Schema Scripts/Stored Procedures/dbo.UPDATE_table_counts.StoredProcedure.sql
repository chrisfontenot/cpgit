USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_table_counts]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_table_counts] AS
-- =========================================================================================
-- ==            Build a list of the table row counts. Used to check conversions.         ==
-- =========================================================================================

set nocount on

declare	@name	varchar(80)
declare	@stmt	varchar(800)
create table ##result ( table_name varchar(80), cnt int );
declare table_cursor cursor for select name from sysobjects where type = 'U' and name not like 'dt%' order by 1
open table_cursor
fetch table_cursor into @name

while @@fetch_status = 0
begin
	select @stmt = 'insert into ##result (table_name,cnt) select ''' + @name + ''',count(*) from [' + @name + ']'
	exec ( @stmt )
	fetch table_cursor into @name
end

close table_cursor
deallocate table_cursor

select * from ##result order by 1
drop table ##result

set nocount off
GO
