USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ExpectedDisbursement]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_ExpectedDisbursement] ( @Disbursement_Batch AS INT = NULL ) AS

-- =========================================================================================
-- ==            Report which shows disbursed money which is different from expected      ==
-- =========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

IF @disbursement_batch IS NULL
BEGIN
	SELECT	TOP 1	@disbursement_batch	= disbursement_register
	FROM		registers_disbursement
	WHERE		tran_type = 'AD'
	ORDER BY	date_created desc

	IF @disbursement_batch IS NULL
	BEGIN
		RaisError	(50033, 16, 1)
		Return		( 0 )
	END
END

-- Disable the intermediate result sets
SET NOCOUNT ON

-- Generate a list of the disbursements for this batch
SELECT		client as 'client',
		sum(debit_amt) as 'paid'
INTO		#t_disbursement_paid
FROM		registers_client_creditor d WITH (NOLOCK)
WHERE		disbursement_register = @Disbursement_batch
AND		tran_type in ('AD', 'BW', 'MD', 'CM')
GROUP BY	client

-- This is just 11:59:59pm today so that we include all clients with today's starting date.
declare	@cutoff_date	datetime
select	@cutoff_date	= convert(datetime, convert(varchar(10), getdate(), 101) + ' 23:59:59')

-- Generate a list of the disbursements which should have been made
create table #t_disbursement_factor (client int, cr_disbursement money, agency_disbursement money, active_status varchar(10));

INSERT INTO	#t_disbursement_factor (client, cr_disbursement, agency_disbursement, active_status)
SELECT		cc.client			as 'client',

		sum(case
			when isnull(ccl.always_disburse,0) <> 0 then 0
			when c.start_date > @cutoff_date then 0
			when isnull(ccl.zero_balance,0) > 0 then disbursement_factor
			when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0 then disbursement_factor
			else 0
		    end)	as 'cr_disbursement',

		sum(case
			when isnull(ccl.always_disburse,0) = 0 then 0
			when isnull(ccl.zero_balance,0) > 0 then disbursement_factor
			when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0 then disbursement_factor
			else 0
		    end)	as 'agency_disbursement',

		c.active_status			as 'active_status'

FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c		WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		cc.reassigned_debt = 0
GROUP BY cc.client, c.active_status

-- Delete the rows which are not valid
delete from #t_disbursement_factor
where	(active_status not in ('A', 'AR') and agency_disbursement = 0)
or	(agency_disbursement = 0 and cr_disbursement = 0)

-- Include the agency disbursement into the creditor values
update	#t_disbursement_factor
set	cr_disbursement = cr_disbursement + agency_disbursement;

DECLARE	@Rows	int

-- Return the resulting recordset
SELECT		c.client			as 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		isnull(a.paid,0)		as 'paid',
		isnull(b.cr_disbursement,0)	as 'disbursement',
		isnull(c.held_in_trust,0)	as 'trust_balance'
FROM		#t_disbursement_paid a
FULL OUTER JOIN	#t_disbursement_factor b ON a.client = b.client
INNER JOIN	clients c	WITH (NOLOCK) on isnull(a.client,b.client) = c.client
LEFT OUTER JOIN	people p	WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		(isnull(a.paid,0) > 0
OR		(isnull(c.held_in_trust,0) > 0 and isnull(b.cr_disbursement,0) > 0))
AND		isnull(a.paid,0) != isnull(b.cr_disbursement,0)
ORDER BY	c.client

SET @Rows = @@rowcount

-- Discard the working tables
DROP TABLE	#t_disbursement_paid
DROP TABLE	#t_disbursement_factor

-- Return the record count
RETURN ( @rows )
GO
