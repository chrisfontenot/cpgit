USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_lastpayment]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_client_lastpayment] (@client AS typ_client)  AS

-- Obtain the information for the last deposit from the client table.
SELECT	last_deposit_amount	as 'amount',
	last_deposit_date	as 'date',
	'OT'			as 'type'
FROM	clients WITH (NOLOCK)
WHERE	client = @client

-- The result is the number of rows in the result set
return ( @@rowcount )
GO
