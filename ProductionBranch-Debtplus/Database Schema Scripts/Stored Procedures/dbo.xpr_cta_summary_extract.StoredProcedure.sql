USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_cta_summary_extract]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_cta_summary_extract] as
-- Extract the information for the CTA Agency Tracking Form

-- Disable intermediate results
set nocount on

-- Find the date range using today's date as a basis
declare @from_date datetime
declare @to_date   datetime
select  @to_date   = getdate()
select  @to_date   = convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))
select  @from_date = dateadd(m, -12, @to_date)

-- Find the number of sessions for the client
select	convert(varchar, month(ca.start_time)) + '/01/' + convert(varchar, year(ca.start_time)) as session_date, ca.appt_type, ca.client_appointment, ca.client
into	#cta_sessions
from	client_appointments ca
where	ca.status in ('K','W')
and		ca.office is not null
and		ca.start_time >= @from_date
and		ca.start_time  < @to_date;

-- Exclude appointment types that we don't want to examine
delete	#cta_sessions
from	#cta_sessions x
inner join appt_types a on x.appt_type = a.appt_type
where	a.housing		= 1
or		a.bankruptcy	= 1
or		a.credit		= 1;

-- Summarize this by session date
select	session_date, count(*) as sessions
into	#cta_session_count
from	#cta_sessions
group by session_date;

-- Select the clients to be used in the calculations
select	cc.client, convert(money,0) as total_deposits, cc.client_creditor, pr.client_creditor_proposal,
		convert(datetime, convert(varchar, month(pr.date_created)) + '/01/' + convert(varchar, year(pr.date_created))) as proposal_date, pr.rpps_client_type_indicator,
		convert(int,0) as dmp_20, convert(int,0) as dmp_175, convert(int,1) as dmp_reg,
		convert(int,0) as cta_20, convert(int,0) as cta_175, convert(int,1) as cta_reg
into	#cta_tracking_input
from	client_creditor_proposals pr
inner join client_creditor cc on pr.client_creditor = cc.client_creditor
where	pr.date_created >= @from_date
and		pr.date_created < @to_date

-- Find the total deposit figure
select	i.client, sum(credit_amt) as credit_amt
into	#cta_tracking_deposits
from	#cta_tracking_input i
inner join registers_client rc on i.client = rc.client and 'DP' = rc.tran_type
group by i.client;

update	#cta_tracking_input
set		total_deposits	= b.credit_amt
from	#cta_tracking_input i
inner join #cta_tracking_deposits b on i.client = b.client;

-- Set the figures for the clients based upon the characteristics
update	#cta_tracking_input
set		dmp_175		= 1,
		cta_175		= 1
where	rpps_client_type_indicator = 'E'

update	#cta_tracking_input
set		dmp_20		= 1,
		cta_20		= 1
where	rpps_client_type_indicator = 'D'

-- Discard client calculations from DMP information if the deposit amount is too small
update	#cta_tracking_input
set		cta_20		= 0,
		cta_175		= 0,
		cta_reg		= 0
where	total_deposits < 100;

-- Summarize the data by client
select	client, proposal_date, sum(dmp_20) as dmp_20, sum(dmp_175) as dmp_175, sum(dmp_reg) as dmp_reg, sum(cta_20) as cta_20, sum(cta_175) as cta_175, sum(cta_reg) as cta_reg
into	#cta_tracking
from	#cta_tracking_input
group by client, proposal_date;

-- Reduce the counts to the highest form desired
update	#cta_tracking
set		dmp_175			= 1,
		dmp_20			= 0,
		dmp_reg			= 0
where	dmp_175 > 0;

update	#cta_tracking
set		dmp_20			= 1,
		dmp_reg			= 0
where	dmp_20 > 0;

update	#cta_tracking
set		dmp_reg			= 1
where	dmp_reg > 0;

update	#cta_tracking
set		cta_175			= 1,
		cta_20			= 0,
		cta_reg			= 0
where	cta_175 > 0;

update	#cta_tracking
set		cta_20			= 1,
		cta_reg			= 0
where	cta_20 > 0;

update	#cta_tracking
set		cta_reg			= 1
where	cta_reg > 0;

-- Finally summarize the information by proposal date
select	proposal_date, sum(dmp_20) as dmp_20, sum(dmp_175) as dmp_175, sum(dmp_reg) as dmp_reg, sum(cta_20) as cta_20, sum(cta_175) as cta_175, sum(cta_reg) as cta_reg
into	#cta_results
from	#cta_tracking
group by proposal_date;

-- Return the information for the spreadsheet
create table #results (location varchar(20), value varchar(20))

-- Extract the information for the spreadsheet
declare	@proposal_date		datetime
declare @sessions			int
declare	@dmp_20				int
declare	@dmp_175			int
declare	@dmp_reg			int
declare	@cta_20				int
declare	@cta_175			int
declare	@cta_reg			int
declare	@line_no			int
select  @line_no = 9

declare item_cursor cursor for
	select	proposal_date, s.sessions, dmp_20, dmp_175, dmp_reg, cta_20, cta_175, cta_reg
	from	#cta_results r
	left outer join #cta_session_count s on r.proposal_date = s.session_date
	order by proposal_date;

open item_cursor
fetch item_cursor into @proposal_date, @sessions, @dmp_20, @dmp_175, @dmp_reg, @cta_20, @cta_175, @cta_reg
while @@fetch_status = 0
begin
	insert into #results (location, value)
	select	'A' + convert(varchar, @line_no), convert(varchar(10), @proposal_date, 101)

	insert into #results (location, value)
	select	'C' + convert(varchar, @line_no), convert(varchar, @sessions)

	insert into #results (location, value)
	select	'D' + convert(varchar, @line_no), convert(varchar, @dmp_20)

	insert into #results (location, value)
	select	'E' + convert(varchar, @line_no), convert(varchar, @dmp_175)

	insert into #results (location, value)
	select	'F' + convert(varchar, @line_no), convert(varchar, @dmp_reg)

	insert into #results (location, value)
	select	'G' + convert(varchar, @line_no), convert(varchar, @cta_20)

	insert into #results (location, value)
	select	'H' + convert(varchar, @line_no), convert(varchar, @cta_175)
	
	insert into #results (location, value)
	select	'I' + convert(varchar, @line_no), convert(varchar, @cta_reg)
	
	select @line_no = @line_no + 1
	fetch item_cursor into @proposal_date, @sessions, @dmp_20, @dmp_175, @dmp_reg, @cta_20, @cta_175, @cta_reg
end
close item_cursor
deallocate item_cursor

select 'Tracking Sheet' as sheet, location, value
from	#results;

-- Discard the tables
drop table #results
drop table #cta_results
drop table #cta_session_count
drop table #cta_sessions
drop table #cta_tracking
drop table #cta_tracking_input
drop table #cta_tracking_deposits
return ( 1 )
GO
