USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_response_close]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_response_close] (@epay_response_file as int) as

-- ChangeLog
--   3/3/2005
--     Corrected rejection processing to use epay_code properly

-- Find the matching item by the trace number
update	epay_responses_dr
set	epay_transaction = et.epay_transaction
from	epay_responses_dr dr
inner join epay_transactions et on dr.transaction_id = et.trace_number
where	dr.epay_response_file = @epay_response_file
and	dr.epay_transaction is null

-- If we can't find it by the trace number then look for the item by the account number
update	epay_responses_dr
set	epay_transaction = et.epay_transaction
from	epay_responses_dr dr
inner join epay_transactions et on dr.creditor_id = et.epay_biller_id and dr.client_number = et.client and dr.customer_biller_account_number_original = et.account_number
where	dr.epay_response_file = @epay_response_file
and	dr.epay_transaction is null
and	et.request = 1

-- Update the error status for the invalid items
update	epay_responses_dr
set	error = 'UNKNOWN TRACE'
where	epay_response_file = @epay_response_file
and	epay_transaction is null

-- If the proposal is not in a pending status then it is duplicated.
update	epay_responses_dr
set	error = 'DUPLICATE RESPONSE'
from	epay_responses_dr dr
inner join epay_transactions tr on dr.epay_transaction = tr.epay_transaction
inner join client_creditor_proposals pr on tr.client_creditor_proposal = pr.client_creditor_proposal
where	epay_response_file = @epay_response_file
and	error is null
and	pr.proposal_status not in (0, 1)

-- Mark the proposals as being accepted for the epay system
update	client_creditor_proposals
set	proposal_status	= 3,
	proposal_accepted_by = 'EPAY SYSTEM',
	proposal_status_date = dr.response_date,
	counter_amount = dr.accepted_payment_amount
from	client_creditor_proposals p
inner join epay_transactions tr on p.client_creditor_proposal = tr.client_creditor_proposal
inner join epay_responses_dr dr on tr.epay_transaction = dr.epay_transaction
where	dr.epay_response_file = @epay_response_file
and	dr.response_code = 'A'
and	dr.error is null

-- Update the contact information
update	client_creditor
set	contact_name = left(ltrim(rtrim(dr.creditor_contact_name)),50)
from	client_creditor cc
inner join client_creditor_proposals p on cc.client_creditor = p.client_creditor
inner join epay_transactions tr on p.client_creditor_proposal = tr.client_creditor_proposal
inner join epay_responses_dr dr on tr.epay_transaction = dr.epay_transaction
where	dr.epay_response_file = @epay_response_file
and	dr.response_code = 'A'
and	dr.error is null
and	ltrim(rtrim(dr.creditor_contact_name)) <> ''

-- Mark the proposals as being rejected for the epay system
update	client_creditor_proposals
set	proposal_status	= 4,
	proposal_status_date = dr.response_date,
	counter_amount = dr.accepted_payment_amount,
	proposal_reject_reason = isnull(rej.proposal_result_reason,0)
from	client_creditor_proposals p
inner join epay_transactions tr on p.client_creditor_proposal = tr.client_creditor_proposal
inner join epay_responses_dr dr on tr.epay_transaction = dr.epay_transaction
left outer join proposal_result_reasons rej on dr.decline_reason_code = rej.epay_code
where	dr.epay_response_file = @epay_response_file
and	dr.response_code = 'D'
and	dr.error is null

return (1)
GO
