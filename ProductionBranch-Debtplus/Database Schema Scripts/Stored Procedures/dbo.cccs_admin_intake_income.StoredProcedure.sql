USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_income]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_income] ( @application_id as int, @client as int = null ) as

-- Find the client ID from the application if one is not passed to us
if @client is null
	select	@client	= ForeignDatabaseID
	from	cccs_admin..applications
	where	application_id = @application_id
	
if @client is null
begin
	RaisError ('Client is NULL and may not be determined', 16, 1)
	return ( -1 )
end

-- Find the budget information
declare	@monthly_income_net				money
declare	@coapplicant_monthly_income_net	money
declare @applicant_credit_score			int
declare	@coapplicant_credit_score		int
declare	@email_address					varchar(256)
declare @item_found						int

select	@monthly_income_net				= isnull(monthly_income_net,0),
		@coapplicant_monthly_income_net	= isnull(coapplicant_monthly_income_net,0),
		@item_found						= application_id,
		@applicant_credit_score			= credit_score,
		@coapplicant_credit_score		= coapp_credit_score,
		@email_address					= ltrim(rtrim(isnull(email_address,'')))
from	cccs_admin..budgets
where	application_id					= @application_id

-- Since we read the application_id from the database and this is a non-null field, it should be
-- defined. If it was not defined then the only reason is that there is no corresponding row for the data.
-- (The @@rowcount item does not work properly in all cases so I don't use it. It returns 0 when there really is a row.)
if @item_found is not null
begin
	declare	@person					int

	-- Update the co-applicant monthly income figure
	select	@person					= person
	from	people
	where	client					= @client
	and		relation				<> 1;

	if @person is not null
		update	people
		set		net_income			= isnull(@coapplicant_monthly_income_net,0),
				fico_score			= isnull(@coapplicant_credit_score,0)
		where	person				= @person;

	-- Update the applicant monthly income figure
	declare	@emailID				int
	select	@person					= person,
			@emailID				= emailID
	from	people
	where	client					= @client
	and		relation				= 1;

	if @person is not null
	begin
		update	people
		set		net_income			= isnull(@monthly_income_net,0),
				fico_score			= isnull(@applicant_credit_score,0)
		where	person				= @person;

		if @emailID is not null or @email_address <> ''
		begin
			if @emailID is null
			begin
				execute @EmailID = xpr_insert_EmailAddress @email_address
				update				people
				set					EmailID = @EmailID
				where				person = @person;
			end

			update	EmailAddresses
			set		Address			= @email_address
			where	Email			= @EmailID;
		end
	end
end

-- Complete the processing here
return ( @client )
GO
