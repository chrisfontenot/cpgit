SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [www_creditor_transactions] ( @creditor as typ_creditor, @date_range as varchar(20) = NULL, @payments as int = 1 ) as
-- ===============================================================================
-- ==            List the trust activity for the creditor actions               ==
-- ===============================================================================

-- Date ranges
declare	@from_date	datetime
declare	@to_date	datetime
select	@date_range	= lower(@date_range)

-- Determine the current month range
if @date_range = 'current_month'
begin
	select	@to_date = getdate()
	select	@from_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
end

-- Find the last 60 days information
else if @date_range = 'last_60'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -1, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 90 days information
else if @date_range = 'last_90'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -2, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 120 days information
else if @date_range = 'last_120'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -3, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last year information
else if @date_range = 'last_year'
begin
	select	@from_date = dateadd (yy, -1, getdate())
	select	@to_date = convert(datetime, '12/01/' + convert(varchar, year(@from_date))) + ' 23:59:59'
	select	@from_date = convert(datetime, '01/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Assume that the default is "last_month"
else
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = @to_date
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Remove the times from the date fields
select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00')
select	@to_date   = convert(datetime, convert(varchar(10), @to_date, 101)   + ' 23:59:59')

-- Retrieve the creditor name and address information
declare	@attn		varchar(80)
DECLARE @Addr1		varchar(256)
DECLARE @Addr2		varchar(256)
DECLARE @Addr3		varchar(256)
DECLARE @Addr4		varchar(256)
DECLARE @Addr5		varchar(256)
DECLARE @Addr6		varchar(256)

-- Use the creditor address if this is a creditor check
create table #www_creditor_address (line int, address varchar(80))

select	@attn		= attn,
		@addr1		= addr1,
		@addr2		= addr2,
		@addr3		= addr3,
		@addr4		= addr4,
		@addr5		= addr5,
		@addr6		= addr6
from	view_creditor_addresses
where	creditor	= @creditor
and		type		= 'P'

insert into #www_creditor_address (line, address) values (1, 'ATTN: ' + @attn)
insert into #www_creditor_address (line, address) values (2, @addr1)
insert into #www_creditor_address (line, address) values (3, @addr2)
insert into #www_creditor_address (line, address) values (4, @addr3)
insert into #www_creditor_address (line, address) values (5, @addr4)
insert into #www_creditor_address (line, address) values (6, @addr5)
insert into #www_creditor_address (line, address) values (7, @addr6)

select		upper(address) as 'creditor_name'
from		#www_creditor_address
where		address is not null
order by	line

drop table #www_creditor_address

-- Retrieve the transactions for disbursements to the creditor
select		rc.creditor_register				as 'item_key',
		convert(varchar(10), rc.date_created, 101)	as 'date_created',
		rc.tran_type					as 'tran_type',
		isnull(rc.credit_amt,0)				as 'credit_amt',
		isnull(rc.debit_amt,0)				as 'debit_amt',
		rc.invoice_register				as 'invoice_register',
		convert(varchar,tr.checknum)					as 'check_number',
		isnull(tr.cleared,' ')				as 'cleared',
		convert(varchar(10), tr.reconciled_date,101)	as 'date_reconciled'
from		registers_creditor rc with (nolock)
left outer join	registers_trust tr with (nolock) on rc.trust_register = tr.trust_register
where		rc.creditor = @creditor
and		rc.tran_type in ('AD', 'MD', 'MC', 'BW', 'RF', 'VF', 'RR', 'ER', 'VD')
and		rc.date_created between @from_date and @to_date
order by	rc.date_created

return ( @@rowcount )
GO
