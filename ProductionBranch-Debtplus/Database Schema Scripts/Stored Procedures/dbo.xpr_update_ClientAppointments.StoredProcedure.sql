USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_ClientAppointments]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_ClientAppointments](@oID as uniqueidentifier,@Client as int = null,@StartDate as datetime = null,@EndDate as datetime = null,@Office as int = null,@Counselor as int = null,@ReminderInfo as text = null,@RecurrenceInfo as text = null,@Description as text = null,@AppointmentType as int = 0,@CallbackTelephone as varchar(80) = null,@isAllDay as bit = 0,@isLocked as bit = 0,@isPriority as bit = 0,@RecurringType as int = 0,@ConfirmStatus as int = 0,@ConfirmedBy as varchar(80) = null,@ConfirmedDate as datetime = null,@ReferredBy as int = null,@BusyStatus as int = 0,@Subject as varchar(80) = null,@Location as varchar(80) = null) as
-- ========================================================================================================
-- ==           Update an entry in the ClientAppointments table                                          ==
-- ========================================================================================================
update	ClientAppointments
set		[Client]=@Client,
		[StartDate]=@StartDate,
		[EndDate]=@EndDate,
		[Office]=@Office,
		[Counselor]=@Counselor,
		[ReminderInfo]=@ReminderInfo,
		[RecurrenceInfo]=@RecurrenceInfo,
		[Description]=isnull(@Description,''),
		[AppointmentType]=@AppointmentType,
		[CallbackTelephone]=isnull(@CallbackTelephone,''),
		[isAllDay]=isnull(@isAllDay,0),
		[isLocked]=isnull(@isLocked,0),
		[isPriority]=isnull(@isPriority,0),
		[RecurringType]=@RecurringType,
		[ConfirmStatus]=@ConfirmStatus,
		[ConfirmedBy]=@ConfirmedBy,
		[ConfirmedDate]=@ConfirmedDate,
		[ReferredBy]=@ReferredBy,
		[BusyStatus]=@BusyStatus,
		[Subject]=@Subject,
		[Location]=@Location
where	[oID] = @oID;

return ( @@rowcount )
GO
