USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CheckVoucher_Detail]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_CheckVoucher_Detail] ( @TrustRegister AS INT, @creditor as varchar(10) = null ) AS
-- =============================================================================================
-- ==            Obtain the transaction detail for the indicated check                        ==
-- =============================================================================================

-- ChangeLog
--   9/8/2002
--      Added the "@creditor" parameter to permit bank wire transactions

if @creditor is not null
begin
	select	@creditor = ltrim(rtrim(@creditor))
	if @creditor = ''
		select	@creditor = null
end

DECLARE	@rows		int
Execute @rows = lst_check_contents @TrustRegister, @creditor
RETURN ( @rows )
GO
