USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_create]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_create] ( @client as int, @ssn as varchar(10), @dob as datetime, @email as varchar(50) = null, @password as varchar(80) = null, @question as varchar(80) = null, @answer as varchar(80) = null) as 
-- =============================================================================
-- ==            Create access to the client from the WWW interface           ==
-- =============================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that the client is valid
declare	@active_status	varchar(4)
select	@active_status	= active_status
from	clients
where	client = @client

if @@rowcount < 0
begin
	RaisError ('The client ID is not valid at this time.', 16, 1)
	return ( 0 )
end

-- Validate the active status
if @active_status is null
	select @active_status = 'CRE'

if @active_status not in ('A', 'AR', 'EX')
begin
	RaisError ('The client ID is not valid at this time.', 16, 1)
	return ( 0 )
end

-- Ensure that the client is valid
if @client < 1
begin
	RaisError ('The client ID is not valid at this time.', 16, 1)
	return ( 0 )
end

-- Check to determine if the client currently has access
if exists (select * from client_www where username = convert(varchar,@client))
begin
	RaisError ('You currently have WWW access. Please call our offices if you are unable to access your password.', 16, 1)
	return ( 0 )
end

-- Validate the SSN number for the client
declare	@people_ssn	varchar(20)
declare	@people_dob	datetime
select	@people_ssn	= ssn,
		@people_dob	= birthdate
from	people
where	client		= @client
and	ssn		= @ssn

if @@rowcount < 1
begin
	RaisError ('The social security number does not match our records.', 16, 1)
	return ( 0 )
end

-- Validate the client's birthdate
if convert(datetime, convert(varchar(10), @people_dob, 101) + ' 00:00:00') != convert(datetime, convert(varchar(10), @dob, 101) + ' 00:00:00')
begin
	RaisError ('The birthdate does not match our records.', 16, 1)
	return ( 0 )
end

-- Generate the client account
insert into client_www	(PKID, ApplicationName,  username,  [password], PasswordQuestion, PasswordAnswer, Email,  isLockedOut,            isApproved,            Comment)
select					newid(), '/', convert(varchar,@client), @password,  @question, @answer, @Email, 0, 1, null

-- Return success if the client was created
return ( @@rowcount )
GO
