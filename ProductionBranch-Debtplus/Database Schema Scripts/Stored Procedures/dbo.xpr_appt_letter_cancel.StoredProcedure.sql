USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_letter_cancel]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_letter_cancel] ( @client_appointment as typ_key ) AS

SELECT		a.cancel_letter as 'letter_type'
FROM		appt_types a
INNER JOIN	client_appointments c ON c.appt_type = a.appt_type
WHERE		c.client_appointment = @client_appointment
AND		c.workshop IS NULL

return ( @@rowcount )
GO
