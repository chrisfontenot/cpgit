USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_CDC]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_CDC] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(11), @return_code as varchar(3) = null, @name as varchar(5) = null, @old_account_number as varchar(50) = null, @net as money = 0, @new_account_number as varchar(50) = null, @monthly_payment_amount as money = null, @balance as money = null) as

-- ================================================================================================
-- ==            Process an account number change                                                ==
-- ================================================================================================

-- Suppress intermediate results
set nocount on

-- Load the basic transaction
declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'CDC', @name, @old_account_number, @net

if @rpps_response_detail > 0
begin
	-- Insert the modifier information into the tables
	insert into rpps_response_details_cdc ( rpps_response_detail, monthly_payment_amount, balance, new_account_number )
	values (@rpps_response_detail, @monthly_payment_amount, @balance, @new_account_number )
end

return ( @rpps_response_detail )
GO
