USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Split_Unposted_Disbursement_Transactions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Split_Unposted_Disbursement_Transactions] ( @old_disbursement_register as int = null ) as

-- Correct the condition where someone edited transactions after the disbursement batch was posted.
-- This will leave un-paid debts and not balance the trust.

-- The only solution is to create a new disbursement batch with the pending transactions and let the
-- agency post the new batch.

-- ChangeLog
--    3/12/2012
--       Allow for clients to have never been paid to be moved to the new batch.

-- If there is not disbursement then list the ones posted recently
if @old_disbursement_register is null
begin
	select	top 10
			disbursement_register, date_created, created_by, note
	from	registers_disbursement
	where	date_posted is not null
	and		disbursement_register in (select disbursement_register from disbursement_creditors where debit_amt > 0 and trust_register is null)
	order by date_created desc

	RaisError ('You need a disbursement register. Please choose from the list.', 16, 1)
	return ( 0 )
end

-- Ensure that the batch is present
if not exists ( select * from registers_disbursement where disbursement_register = @old_disbursement_register)
begin
	RaisError ('The disbursement register is not valid.', 16, 1)
	return ( 0 )
end

-- Read the parameters from the existing item
declare	@date_posted		datetime
declare	@disbursement_note	varchar(256)
declare	@tran_type		varchar(2)
select	@date_posted		= date_posted,
		@disbursement_note	= note,
		@tran_type		= tran_type
from	registers_disbursement
where	disbursement_register	= @old_disbursement_register

-- This must be a valid type
if @tran_type <> 'AD'
begin
	RaisError ('This procedure only works with Auto-Disbursements. It is not an auto-disbursement.', 16, 1)
	return ( 0 )
end

-- It must be posted or we have the wrong batch
if @date_posted is null
begin
	RaisError ('The disbursement has not been posted. There is no need for this procedure.', 16, 1)
	return ( 0 )
end

-- DO THE WORK HERE!
begin transaction

-- Find the list of clients that have been paid but not closed.
-- These are the ones that we need to look carefully at again.
select	distinct client
into	#clients1
from	disbursement_creditors
where	disbursement_register	= @old_disbursement_register
and		trust_register is null
and		debit_amt > 0

-- Ensure that the clients are fully paid or not paid at all
if exists (	select	*
			from	disbursement_creditors d
			inner join #clients1 c on d.client = c.client
			where	d.disbursement_register = @old_disbursement_register
			and		d.debit_amt > 0
			and		d.trust_register is not null )
begin
	rollback transaction
	RaisError ('There are one or more clients who have been partially paid. This procedure can not help.', 16, 1)
	return ( 0 )
end

drop table #clients1

-- Create the full table for items to be processed
-- Here, we don't care about the paid figure. We want all of the clients who were not worked as well.
select	distinct client
into	#clients
from	disbursement_creditors
where	disbursement_register	= @old_disbursement_register
and		trust_register is null

-- Move the transactions to the new disbursement
declare	@new_disbursement_register	int

-- Create a new disbursmement to hold the un-posted transactions
insert into registers_disbursement (tran_type, note, date_posted)
select	'AD', left('Correction' + isnull(' of ' + @disbursement_note,''), 50) as note, getdate() as date_posted
select	@new_disbursement_register	= scope_identity()

print 'New disbursement batch is #' + convert(varchar, @new_disbursement_register)

-- Adjust the client register to show the new disbursement register
update	registers_client
set		disbursement_register	= @new_disbursement_register
from	registers_client rc
inner join disbursement_clients c on rc.client_register = c.client_register
inner join #clients x on c.client = x.client
where	c.disbursement_register	= @old_disbursement_register

-- Update the creditor list to hold the new disbursement value
update	disbursement_creditors
set		disbursement_register	= @new_disbursement_register
from	disbursement_creditors c
inner join #clients x on c.client = x.client
where	c.disbursement_register	= @old_disbursement_register

-- Update the client list to hold the new disbursement value
update	disbursement_clients
set		disbursement_register	= @new_disbursement_register
from	disbursement_clients c
inner join #clients x on c.client = x.client
where	c.disbursement_register	= @old_disbursement_register

-- Clear the posted date so that it may be posted again
update	registers_disbursement
set		date_posted	= null
where	disbursement_register	= @new_disbursement_register

-- Make things stable at this point
commit transaction

return ( 1 )
GO
