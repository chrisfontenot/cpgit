ALTER PROCEDURE [dbo].[xpr_hecm_create_client] (
	@program INT
	,@referralCode INT
	,@servicerID INT
	,@loanNumber VARCHAR(50)
	,@language INT
	,@applicantName INT
	,@coApplicantName INT
	,@homePhone INT
	,@applicantCell INT
	,@coapplicantCell INT
	,@messagePhone INT
	,@applicantWorkPhone INT
	,@coapplicantWorkPhone INT
	,@address INT
	,@methodOfFirstContact INT
	,@username VARCHAR(80)
	,@applicantEmail INT
	,@coapplicantEmail INT
	,@InvestorAccountNumber VARCHAR(50)
	,@Corporate_Advance MONEY = NULL
	,@delinq_Type INT
	,@SPOC_ID INT = NULL
	)
AS
BEGIN
	DECLARE @clientid INT
	DECLARE @zipcode VARCHAR(10)

	DECLARE @ownerId INT
	SET @ownerId = NULL

	DECLARE @coownerId INT
	SET @coownerId = NULL

	-- set mortgage problem to default value
	DECLARE @mortgageProblems INT
	SET @mortgageProblems = 1

	DECLARE @isDuplicate BIT
	SET @isDuplicate = 0
	
	DECLARE @uploadedRecordId INT
	SET @uploadedRecordId = 0

	BEGIN TRANSACTION

	BEGIN TRY
		DECLARE @return_value INT
		DECLARE @servicerName VARCHAR(80)

		SELECT @servicerName = servicers.[description]
		FROM Housing_lender_servicers [servicers]
		WHERE servicers.oID = @servicerID

		if @servicerName is NULL
			SELECT @servicerName = servicers.[description]
			FROM Housing_lender_servicers [servicers]
			WHERE IsOther = 1

		if @servicerName is NULL
			SET @servicerName = 'Other or Unknown'

		EXEC @isDuplicate = OCS_Check_Existing_LoanNumber @loanNumber, @program, @clientID = @clientid OUTPUT

		IF (@isDuplicate = 1 AND @clientid > 0)
		BEGIN
			UPDATE OCS_Client
			SET Archive = 1
				,ActiveFlag = 0
				,IsDuplicate = 1
			WHERE Id = (
					SELECT TOP 1 Id
					FROM OCS_Client
					WHERE ClientId = @clientId AND Program = @program
					ORDER BY Id DESC
					)

			-- insert the data into ocs_client and ocs_uploadrecord table
			EXEC @uploadedRecordId = xpr_hecm_create_ocsclient_uploadrecord @clientid
				,@program
				,@servicerName
				,@loanNumber
				,@language
				,@applicantName
				,@coApplicantName
				,@address
				,@homePhone
				,@messagePhone
				,@applicantCell
				,@applicantWorkPhone
				,@coapplicantCell
				,@coapplicantWorkPhone
				,@InvestorAccountNumber

			EXEC [xpr_hecm_update_client] @clientid
				,@referralCode
				,@servicerID
				,@loanNumber
				,@language
				,@applicantName
				,@coApplicantName
				,@homePhone
				,@applicantCell
				,@coapplicantCell
				,@messagePhone
				,@applicantWorkPhone
				,@coapplicantWorkPhone
				,@address
				,@methodOfFirstContact
				,@username
				,@applicantEmail
				,@coapplicantEmail
				,@InvestorAccountNumber
				,@Corporate_Advance 
				,@delinq_Type
				,@SPOC_ID
		END
		ELSE
		BEGIN
			EXEC @clientid = [dbo].[xpr_create_client] @homePhone
				,@mortgageProblems
				,@referralCode
				,@methodOfFirstContact --no contact has been made at this point
				,@bankruptcy = 0

			UPDATE clients
			SET disbursement_date = 1
				,config_fee = dbo.default_config_fee()
				,AddressID = @address
				,MsgTelephoneID = @messagePhone
				,LANGUAGE = @language
			WHERE client = @clientid

			IF @applicantName IS NOT NULL
			BEGIN
				-- since applicant data is already inserted by the xpr_create_client procedure
				-- update the record
				UPDATE people
				SET NameID = @applicantName
					,WorkTelephoneID = @applicantWorkPhone
					,CellTelephoneID = @applicantCell
					,EmailID = @applicantEmail
					,SPOC_ID = @SPOC_ID
				WHERE client = @clientid AND Relation = 1

				--0 = some different 3rd party, 1 = use applicant, 2 = use coapplicant
				INSERT INTO Housing_borrowers ([PersonID]) values (1)
				SELECT @ownerId = SCOPE_IDENTITY()
			END

			IF @coApplicantName IS NOT NULL
			BEGIN
				INSERT INTO people (
					Client
					,NameID
					,Relation
					,WorkTelephoneID
					,CellTelephoneID
					,EmailID
					)

				SELECT
					@clientid
					,@coApplicantName
					,2
					,@coapplicantWorkPhone
					,@coapplicantCell
					,@coapplicantEmail

				INSERT INTO Housing_borrowers ([PersonID]) values (2)
				SET @coownerId = SCOPE_IDENTITY()
			END

			-- insert the data into ocs_client and ocs_uploadrecord table
			EXEC xpr_hecm_create_ocsclient_uploadrecord @clientid
				,@program
				,@servicerName
				,@loanNumber
				,@language
				,@applicantName
				,@coApplicantName
				,@address
				,@homePhone
				,@messagePhone
				,@applicantCell
				,@applicantWorkPhone
				,@coapplicantCell
				,@coapplicantWorkPhone
				,@InvestorAccountNumber

			DECLARE @placeholderloannumber VARCHAR(20)

			-- if the counselor did not enter a loan number, create a placeholder loan number
			IF @loanNumber IS NULL OR @loanNumber = ''
			BEGIN
				SELECT TOP (1) @placeholderloannumber = LoanNumberPlaceHolder
				FROM ocs_client
				WHERE clientid = @clientid;
			END

			DECLARE @newLoanNumber VARCHAR(20)

			SELECT @newLoanNumber = CASE WHEN (@loanNumber IS NULL OR @loanNumber = '') THEN @placeholderloannumber ELSE @loanNumber END

			DECLARE @property INT

			SET @property = NULL

			DECLARE @ins_delinq_state INT
			DECLARE @tax_delinq_state INT

			SET @ins_delinq_state = NULL
			SET @tax_delinq_state = NULL

			IF (@delinq_Type = 3)
			BEGIN
				SET @ins_delinq_state = 2
				SET @tax_delinq_state = 2
			END

			IF (@delinq_Type = 2)
				SET @ins_delinq_state = 2

			IF (@delinq_Type = 1)
				SET @tax_delinq_state = 2

			INSERT INTO Housing_properties (
				OwnerID
				,CoOwnerID
				,UseHomeAddress
				,HousingID
				,Residency
				,PropertyType
				,tax_delinq_state
				,ins_delinq_state
				)
			VALUES (
				@ownerId
				,@coownerId
				,1
				,@clientid
				,0
				,8
				,@tax_delinq_state
				,@ins_delinq_state
				)

			SET @property = SCOPE_IDENTITY()

			DECLARE @lender INT

			SET @lender = NULL

			INSERT INTO Housing_lenders (
				ServicerName
				,ServicerID
				,AcctNum
				,InvestorID
				,InvestorAccountNumber
				,Corporate_Advance
				)
			VALUES (
				@servicerName
				,@servicerID
				,@newLoanNumber
				,NULL
				,@InvestorAccountNumber
				,@Corporate_Advance
				)

			SET @lender = SCOPE_IDENTITY()

			DECLARE @loan INT

			SET @loan = NULL

			INSERT INTO Housing_loans (
				PropertyID
				,CurrentLenderID
				,UseOrigLenderID
				,IntakeSameAsCurrent
				,Loan1st2nd
				,UseInReports
				,LoanDelinquencyMonths
				)
			VALUES (
				@property
				,@lender
				,1
				,1
				,1
				,1
				,0
				)

			SET @loan = SCOPE_IDENTITY()
		END

		DECLARE @uploadAttempt UNIQUEIDENTIFIER

		-- get the upload attempt of the last record inserted in the 
		-- ocs client table for this client
		SELECT TOP (1) @uploadAttempt = UploadAttempt
		FROM ocs_client
		WHERE clientid = @clientid
		ORDER BY id DESC

		INSERT INTO [dbo].[OCS_UploadReport] (
			[User]
			,[BatchName]
			,[AttemptId]
			,[Servicer]
			,[UploadDate]
			,[StartTime]
			,[EndTime]
			,[RunTimeSeconds]
			,[RecordsGiven]
			,[RecordsDuplicates]
			,[RecordsAttemptInsert]
			,[RecordsInserted]
			,[DupeTableEntriesCreated]
			,[PurgeEntriesCreated]
			,[RecordsMissingPhoneNumbers]
			,[PhoneNumbersAdded]
			,[RecordsSentToMTI]
			,[InsertDuplicates]
			,[Success]
			,[ReportSaved]
			,[Program]
			)
		VALUES (
			@username
			,'Manual Entry'
			,@uploadAttempt
			,@servicerName
			,getdate()
			,getdate()
			,getdate()
			,0
			,1
			,0
			,1
			,1
			,0
			,0
			,0
			,0
			,0
			,1
			,1
			,1
			,@program
			)

		COMMIT TRANSACTION

		SELECT @clientid AS ClientID
	END TRY

	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE();

		ROLLBACK TRANSACTION

		RAISERROR (
				@ErrorMessage -- Message text.
				,@ErrorSeverity -- Severity.
				,@ErrorState -- State.
				);

		SELECT convert(int,0) AS ClientID
	END CATCH
END
