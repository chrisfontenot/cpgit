USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_invoice_status]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_invoice_status] ( @creditor as typ_creditor ) as
-- ===========================================================================================
-- ==            Return the information for the invoice aging                               ==
-- ===========================================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that the creditor is the same one that it should be
if not exists ( select * from creditors where creditor = @creditor )
begin
	RaisError ('The creditor ID is not valid for the invoice table', 16, 1)
	return ( 0 )
end

-- Generate the aging table as the first result set
Execute	rpt_creditor_invoice_aging @creditor

-- Retrieve the invoice address for the creditor
Execute xpr_creditor_addresses @creditor, 'I'

-- Retrieve the invoice list
Execute rpt_creditor_invoice_detail @creditor
return ( 1 )
GO
