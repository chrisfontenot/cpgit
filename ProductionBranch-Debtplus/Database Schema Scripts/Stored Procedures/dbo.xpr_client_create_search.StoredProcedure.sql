USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_create_search]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_client_create_search] (@TelephoneNumberKey as varchar(10)) as
begin

	-- Suppress intermediate results
	set nocount on

	-- First, create a temp table with the telephonenumber entries that match the desired id
	SELECT TelephoneNumber
	INTO   #Numbers
	FROM   TelephoneNumbers t WITH (NOLOCK)
	WHERE t.[SearchValue]=@TelephoneNumberKey

	-- Create a clustered index to help the following two selects
	create unique clustered index temp_ix_telephoneNumbers_numbers on #Numbers ( TelephoneNumber )

	-- Build a list of the clients who match the record
	create table #clients (client int)
	insert into #clients (client)
	select	client
	from	clients c with (NOLOCK)
	inner join #numbers n1 WITH (NOLOCK) ON (c.HomeTelephoneID = n1.TelephoneNumber) or (c.MsgTelephoneID = n1.TelephoneNumber)

	-- Build a list of the people who have the telephone number
	insert into #clients (client)
	select	client
	from	people p with (NOLOCK)
	inner join #numbers n1 WITH (NOLOCK) ON (p.WorkTelephoneID = n1.TelephoneNumber) or (p.CellTelephoneID = n1.TelephoneNumber)

	-- Retrieve the results
	SELECT c.client as clientID, dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name, c.active_status, p.ssn, dbo.Format_TelephoneNumber(c.HomeTelephoneID) AS homeTelephone
	FROM clients c WITH (NOLOCK)
	INNER JOIN people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
	LEFT OUTER JOIN names pn WITH (NOLOCK) on p.NameID = pn.Name
	where c.client in (
		select client from #clients
	)

	drop table #clients
	drop table #numbers
end
GO
