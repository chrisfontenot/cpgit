USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ACH_deposit_batch]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ACH_deposit_batch] ( @trust_register as typ_key ) AS

-- ========================================================================================================================
-- ==           Display the client deposits for the indicated trust register                                             ==
-- ========================================================================================================================

select		'DP'				as 'tran_type',
		isnull(d.tran_subtype,'OT')	as 'tran_subtype',
		isnull(dt.description,'Other')	as 'tran_subtype_description',

		d.trust_register		as 'deposit_batch_id',
		d.client			as 'client',
		d.credit_amt			as 'credit_amt',
		d.item_date			as 'item_date',
		d.message			as 'reference',
		d.created_by			as 'created_by',
		d.date_created			as 'date_created',

		dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default) as 'client_name'

from		registers_client d
left outer join	tran_types dt on d.tran_subtype = dt.tran_type
left outer join people p on d.client = p.client and 1 = p.relation
left outer join names pn on p.nameid = pn.name
where		d.tran_type = 'DP'
and		d.trust_register = @trust_register

return ( @@rowcount )
GO
