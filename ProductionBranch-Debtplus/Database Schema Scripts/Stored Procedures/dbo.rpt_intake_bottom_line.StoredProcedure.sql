USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_intake_bottom_line]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_intake_bottom_line] ( @intake_client as int ) as

-- ===================================================================================
-- ==          "Bottom Line" information for intake clients                         ==
-- ===================================================================================

declare	@applicant_net		money
declare	@coapplicant_net	money
declare	@applicant_gross	money
declare	@coapplicant_gross	money
declare	@other_income		money
declare	@expenses		money

select	@applicant_net	= coalesce(net_income, gross_income, 0),
	@applicant_gross = coalesce(gross_income,0)
from	intake_people
where	person = 1
and	intake_client = @intake_client

select	@coapplicant_net = coalesce(net_income, gross_income, 0),
	@coapplicant_gross = coalesce(gross_income,0)
from	intake_people
where	person = 2
and	intake_client = @intake_client

select	@other_income	= sum(isnull(asset_amount,0))
from	intake_assets
where	intake_client	= @intake_client

select	@expenses	= sum(isnull(client_amount,0))
from	intake_budgets
where	intake_client	= @intake_client

select	isnull(@applicant_net,0)		as applicant_net,
	isnull(@applicant_gross,0)		as applicant_gross,
	isnull(@coapplicant_net,0)		as coapplicant_net,
	isnull(@coapplicant_gross,0)		as coapplicant_gross,
	isnull(@other_income,0)			as other_income,
	isnull(@expenses,0)			as expenses

return ( 1 )
GO
