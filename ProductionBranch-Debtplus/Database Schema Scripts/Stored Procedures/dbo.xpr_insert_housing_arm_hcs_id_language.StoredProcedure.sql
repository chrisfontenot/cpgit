USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_arm_hcs_id_language]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_housing_arm_hcs_id_language] ( @hcs_id int, @attribute int ) as
insert into housing_arm_hcs_id_languages ( [hcs_id], [attribute] ) values ( @hcs_id, @attribute )
return ( scope_identity() )
GO
