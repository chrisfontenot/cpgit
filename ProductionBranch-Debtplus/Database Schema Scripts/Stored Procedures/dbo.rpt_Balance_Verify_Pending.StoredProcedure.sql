USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Balance_Verify_Pending]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rpt_Balance_Verify_Pending] ( @from_date as datetime = null, @to_date as datetime = null ) as
-- =================================================================================================
-- ==            Get a list of the debts that are still pending a response on pre-balance-verify  ==
-- =================================================================================================
set nocount on

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date = convert(varchar(10), @from_date, 101),
		@to_date   = dateadd(d, 1, convert(varchar(10), @to_date, 101))

-- Retrieve the list of debts in the pending status.
select	cc.creditor,
		cr.creditor_name,
		v.client,
		v.name,
		cc.account_number,
		f.date_created as date_sent,
		t.trace_number_first as trace_number,
		t.old_balance as balance
from	rpps_transactions t with (nolock)
inner join rpps_batches b with (nolock) on t.rpps_batch = b.rpps_batch
inner join rpps_files f with (nolock) on b.rpps_file = f.rpps_file
inner join client_creditor cc with (nolock) on t.client_creditor = cc.client_creditor
inner join creditors cr with (nolock) on cc.creditor = cr.creditor
inner join view_client_address v with (nolock) on cc.client = v.client
inner join clients c with (nolock) on cc.client = c.client
left outer join rpps_response_details r with (nolock) on t.rpps_transaction = r.rpps_transaction
where	t.service_class_or_purpose = 'CDV'
and		t.prenote_status = 2
and		cc.send_bal_verify not in (0, 1)
and		r.rpps_transaction is null
and		c.active_status_date >= @from_date
and		c.active_status_date < @to_date
order by 1, 3, 5

return ( @@rowcount )
GO
