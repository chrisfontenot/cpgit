USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_client_name]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_creditor_client_name] ( @creditor as varchar(10), @client_creditor as int ) as
-- =================================================================================================
-- ==            Retrieve the client information for the headers                                  ==
-- =================================================================================================

-- Suppress intermediate results
set nocount on

declare	@client			int
declare	@client_name	varchar(80)
declare	@account_number	varchar(22)
declare	@address1		varchar(80)
declare	@address2		varchar(80)
declare	@address3		varchar(80)

-- Fetch the client information
select		@client			= v.client,
			@client_name	= v.client_name,
			@account_number	= v.account_number,
			@address1		= n.addr1,
			@address2		= n.addr2,
			@address3		= n.addr3
from		view_last_payment v with (nolock)
inner join	view_client_address n with (nolock) on v.client = n.client
where		v.client_creditor	= @client_creditor
and			v.creditor		= @creditor

-- The client should not be null. It is null if the creditor does not own the debt record.
if @client is null
begin
	RaisError ('The client information is not valid for this record.', 16, 1)
	return ( 0 )
end

-- Return the client information
select	@client			as 'client',
		@client_name	as 'client_name',
		@account_number	as 'account_number',
		@address1		as 'address1',
		@address2		as 'address2',
		@address3		as 'address3'
return ( 1 )
GO
