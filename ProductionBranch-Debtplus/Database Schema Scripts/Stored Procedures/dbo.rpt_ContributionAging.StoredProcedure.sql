USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ContributionAging]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_ContributionAging] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ============================================================================================
-- ==              Creditor contribution aging report information                            ==
-- ============================================================================================

-- ChangeLog
--   12/30/2001
--     Removed duplicate items should more than one "invoice" creditor contact record exist in the creditor contacts.
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

-- Disable the warning about dropping null values
SET NOCOUNT ON
SET ANSI_WARNINGS OFF

-- Ensure that a cutoff date is specified
IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
select	@FromDate	= convert(varchar(10), @FromDate, 101),
		@ToDate		= convert(varchar(10), dateadd(d, 1, @ToDate), 101)

-- ============================================================================================
-- ==          Part 1 : Create the table with creditor and invoice information               ==
-- ============================================================================================

SELECT		r.creditor,
       		aging_months AS 'months',
			isnull(r.inv_amount,0) - isnull(r.pmt_amount,0) - isnull(r.adj_amount,0) AS 'amount'
INTO		#t_CreditorAging_1
FROM		registers_invoices r with (nolock)
WHERE		r.inv_date >= @FromDate
AND			r.inv_date <  @ToDate
AND			isnull(r.inv_amount,0) - isnull(r.pmt_amount,0) - isnull(r.adj_amount,0) > 0

-- ============================================================================================
-- ==          Part 3 : Split the information into the columns                               ==
-- ============================================================================================

SELECT		creditor,

		case
			when months < 1 then amount
				        else 0
		end as 'net_current',

		case
			when months = 1 then amount
				        else 0
		end as 'net_30',

		case
			when months = 2 then amount
				        else 0
		end as 'net_60',

		case
			when months = 3 then amount
				        else 0
		end as 'net_90',

		case
			when months > 3 then amount
				        else 0
		end as 'net_120'
INTO		#t_CreditorAging_2
FROM		#t_CreditorAging_1
WHERE		amount > 0

-- ============================================================================================
-- ==          Part 3 : Merge the values into totals by creditor                             ==
-- ============================================================================================

SELECT		creditor,
			SUM(net_current) as 'net_current',
			SUM(net_30) AS 'net_30',
			SUM(net_60) AS 'net_60',
			SUM(net_90) AS 'net_90',
			SUM(net_120) AS 'net_120'
INTO		#t_CreditorAging_3
FROM		#t_CreditorAging_2
GROUP BY	creditor;

-- Create a temporary index for the table
create index ix_temp_CreditorAging_3 on #t_CreditorAging_3 ( creditor );

-- ============================================================================================
-- ==          Part 4 : Fetch the contact type information                                   ==
-- ============================================================================================

DECLARE @Creditor_Contact_Type     typ_key
SELECT  @Creditor_Contact_Type = creditor_contact_type
FROM	creditor_contact_types
WHERE	contact_type like '%I%'

IF @creditor_contact_type is null
begin
	SELECT top 1	@creditor_contact_type = creditor_contact_type
	from		creditor_contact_types

	if @creditor_contact_type is null
		SET	@creditor_contact_type = 1
end

select	distinct creditor, convert(int,null) as creditor_contact
into	#t_CreditorAging_4
from	#t_CreditorAging_3;

update	#t_CreditorAging_4
set	creditor_contact = cc.creditor_contact
from	#t_CreditorAging_4 x
inner join creditor_contacts cc on x.creditor = cc.creditor and cc.creditor_contact_type = @creditor_contact_type;

-- Create a temporary index for the table
create index ix_temp_CreditorAging_4 on #t_CreditorAging_4 ( creditor );

-- ============================================================================================
-- ==          Part 5 : Merge the aging table with the contact information to give results   ==
-- ============================================================================================

SELECT		t.creditor,
		cr.creditor_name,
		cr.contrib_cycle,

		dbo.format_normal_name(DEFAULT,ccn.first,ccn.middle,ccn.last,ccn.suffix) as 'contact_name',

		dbo.format_TelephoneNumber ( cc.TelephoneID ) AS 'contact_phone',

		net_current,
		net_30,
		net_60,
		net_90,
		net_120
FROM		#t_CreditorAging_3 t
INNER JOIN	creditors cr with (nolock) ON t.creditor = cr.creditor
LEFT OUTER JOIN	#t_CreditorAging_4 x on x.creditor = t.creditor
LEFT OUTER JOIN	creditor_contacts cc with (nolock) on x.creditor_contact = cc.creditor_contact
LEFT OUTER JOIN names ccn with (nolock) on cc.NameID = ccn.Name
ORDER BY	cr.type, cr.creditor_id, t.creditor

-- Discard the working tables
DROP TABLE #t_CreditorAging_1
DROP TABLE #t_CreditorAging_2
DROP TABLE #t_CreditorAging_3
DROP TABLE #t_CreditorAging_4

RETURN ( @@rowcount )
GO
