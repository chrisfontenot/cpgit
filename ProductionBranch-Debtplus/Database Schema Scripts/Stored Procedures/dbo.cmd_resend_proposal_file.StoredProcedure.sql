USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_resend_proposal_file]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[cmd_resend_proposal_file] ( @rpps_file as int ) as
begin
	-- ======================================================================================
	-- ==     Procedure to remove the transmitted status from items normally in a proposal ==
	-- ==     file sent to Mastercard. It will cause the proposal items to be resent when  ==
	-- ==     the next proposal file is generated.                                         ==
	-- ======================================================================================

	-- ChangeLog
	--    11/5/2012
	--       Initial creation

	-- Suppress intermediate results
	set nocount on

	-- Replace the flag to send balance verifications
	update client_creditor
	set    send_bal_verify = isnull(t.prenote_status,1)
	from   rpps_transactions t
	inner join client_creditor cc on t.client_creditor = cc.client_creditor
	where  t.rpps_File = @rpps_file
	and    t.service_class_or_purpose = 'CDV'

	-- Clear the drop notice sent flag
	update	debt_notes
	set		rpps_transaction		= null,
			output_batch			= null
	from	rpps_transactions t
	inner join debt_notes d on t.rpps_transaction = d.rpps_transaction
	where	t.rpps_File = @rpps_file
	and		t.service_class_or_purpose = 'CDD'

	-- Put the proposal back into a pending status
	update	proposal_batch_ids
	set		date_transmitted		= null
	from	rpps_transactions t
	inner join client_creditor_proposals p on t.client_creditor_proposal = p.client_creditor_proposal
	inner join proposal_batch_ids i on p.proposal_batch_id = i.proposal_batch_id
	where	t.rpps_file = @rpps_file
	and		t.service_class_or_purpose = 'CDP'
	and		i.date_transmitted is not null;

	return 1
end
GO
