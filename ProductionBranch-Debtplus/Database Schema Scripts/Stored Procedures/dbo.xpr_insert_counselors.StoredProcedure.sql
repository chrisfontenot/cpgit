USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_counselors]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_counselors] (
    @Person as varchar(80) = null,
    @NameID as int = null,
    @Office as int = null,
    @TelephoneID as int = null,
    @EmailID as int = null,
    @ActiveFlag as bit = 1,
    @Default as bit = 0,
    @Menu_level as int = 9,
    @Color as int = -24454,
    @Image as image = null,
    
    -- Added for HUD
    @ssn as varchar(10) = null,
    @billing_mode as varchar(10) = null,
    @rate as money = 0,
    @emp_start_date as datetime = null,
    @emp_end_date as datetime = null,
    @hud_id as int = 0,
    @Note as varchar(256) = null
    ) as
    
    insert into counselors ( [NameID], [person], [office], [TelephoneID], [emailid], [ActiveFlag], [default], menu_level, [color], [image], [Note]) values (@nameID, @Person, @office, @telephoneID, @emailID, @ActiveFlag, @Default, @Menu_level, @Color, @Image, @Note)
    return (scope_identity())
GO
