USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_check_payments]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_check_payments] as

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

select client_creditor, count(*) as check_payments
into #check_payments
from registers_client_creditor
where tran_type in ('AD','MD','CM')
group by client_creditor;

update client_creditor
set	check_payments = x.check_payments
from	client_creditor cc
inner join #check_payments x on cc.client_creditor = x.client_creditor

drop table #check_payments
GO
