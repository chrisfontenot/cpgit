USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_distribute_clients_new_counselor]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_distribute_clients_new_counselor] ( @old_counselor int, @new_counselor_list varchar(800), @limit int = 0 ) as 

-- ChangeLog
--    6/23/2008
--       Changed to temporary table rather than connection temporary so that a report may be given
--       for the reassigned clients.
--    7/9/2008
--       Added 'limit' to restrict the count of clients transferred

-- ============================================================================================================
-- ==      Given an old counselor, move the clients to a list of new counselors                              ==
-- ============================================================================================================

set nocount on

-- The "old counselor" must not be null
if @old_counselor is null
begin
	RaisError ('The old counselor must not be missing', 16, 1)
	return ( 0 )
end

-- Ensure that the string is terminated with the proper null
select	@new_counselor_list = replace(@new_counselor_list,' ','') + ','
if @new_counselor_list = ','
begin
	RaisError ('The list of new counselors is missing', 16, 1)
	return ( 0 )
end

begin transaction
set xact_abort on

-- Remove the old table if there is one
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##client_list'' and type = ''U'') drop table ##client_list' )

-- Create the table to hold the transformations
create table ##client_list(ID int identity(1,1), client int, new_counselor int null);
create unique index ix1_temp_client_list on ##client_list ( client );

insert into ##client_list (client)
select	client
from	clients with (nolock)
where	counselor	= @old_counselor
and	active_status in ('A','AR')
order by client;

-- If there is a limit then toss all items above the limit
if isnull(@limit,0) > 0
	delete
	from	##client_list
	where	id > @limit;

-- Distribute the clients to the counselors
declare	item_cursor cursor for
	select	client, new_counselor
	from	##client_list
	for update;

declare	@client		int
declare	@new_counselor	int

open	item_cursor
fetch	item_cursor into @client, @new_counselor

while @@fetch_status = 0
begin
	declare	@pat	int

	select	@pat			= charindex(',', @new_counselor_list)
	select	@new_counselor		= convert(int, left(@new_counselor_list, @pat - 1))
	select	@new_counselor_list	= substring(@new_counselor_list, @pat + 1, 800) + left(@new_counselor_list, @pat)

	-- Update the client with the new counselor
	update	##client_list
	set	new_counselor		= @new_counselor
	where current of item_cursor;

	-- Process the next record
	fetch	item_cursor into @client, @new_counselor
end

close item_cursor
deallocate item_cursor

-- Update the clients with the new counselors
update	clients
set	counselor	= x.new_counselor
from	clients c
inner join ##client_list x on c.client = x.client
where	x.new_counselor is not null;

-- drop table ##client_list

commit transaction
return ( 1 )
GO
