USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_workshop_info]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_workshop_info] ( @workshop as typ_key, @client as typ_client ) AS

-- ================================================================================================================
-- ==            Fetch the information about the client and workshop booking                                     ==
-- ================================================================================================================

-- ChangeLog
--   09/18/01
--     Corrected order of parameters to match caller
--     Removed extra item 'workshop_name' that is not used

-- Suppress intermediate result sets
set nocount on

-- Local storage
declare	@description		varchar(80)
declare	@duration		int
declare	@followup		typ_key
declare	@seats_booked		int
declare	@seats_booked_by_client	int
declare	@counselor		typ_counselor
declare	@counselor_name		varchar(80)
declare	@start_time		datetime
declare	@seats_available	int
declare	@workshop_type		int
declare	@location		int
declare	@location_name		varchar(80)
declare	@HUD_Grant		int
declare	@FeeAmount		money

select	@counselor		= counselor,
	@start_time		= start_time,
	@seats_available	= seats_available,
	@workshop_type		= workshop_type,
	@location		= workshop_location,
	@HUD_Grant		= HUD_Grant,
	@FeeAmount		= HousingFeeAmount
from	workshops with (nolock)
where	workshop		= @workshop

-- Translate the counselor to a person name
if @counselor is not null
	select	@counselor_name	= dbo.format_normal_name(default,n.first,default,n.last,default)
	from	counselors co with (nolock)
	inner join names n with (nolock) on co.NameID = n.name
	where	co.[person]	= @counselor

-- Fetch the number of people attending the workshop
select	@seats_booked		= sum(workshop_people)
from	client_appointments with (nolock)
where	workshop		= @workshop
and	status			= 'P'

if @seats_booked is null
	SELECT @seats_booked = 0

-- Fetch the number of people attending for the client
select	@seats_booked_by_client	= workshop_people
from	client_appointments with (nolock)
where	workshop		= @workshop
and	client			= @client
and	status			= 'P'

if @seats_booked_by_client is null
	SELECT @seats_booked_by_client = 0

-- Ensure that there are seats available for use
if @seats_available <= @seats_booked - @seats_booked_by_client
begin
	raiserror (50048, 16, 1)
	return ( 0 )
end

-- Obtain the workshop location
if isnull(@location,0) > 0
	select	@location_name		= [name]
	from	workshop_locations with (nolock)
	where	workshop_location	= @location

-- Obtain the workshop description
select	@description	= description,
		@duration	= duration
from	workshop_types with (nolock)
where	workshop_type	= @workshop_type

-- Return the information to the user
select	isnull(@description,'')					as 'description',
		isnull(@duration,0)						as 'duration',
--		isnull(@location_name,'')				as 'workshop_name',
		isnull(@seats_booked,0)					as 'seats_booked',
		isnull(@seats_booked_by_client,0)		as 'seats_booked_by_client',
		isnull(@counselor_name,'')				as 'counselor_name',
		isnull(@start_time, convert(datetime, '1/1/1900'))	as 'start_time',
		isnull(@seats_available,0)				as 'seats_available',
		isnull(@workshop_type,0)				as 'workshop_type',
		isnull(@location,0)						as 'location',
		isnull(@location_name,'')				as 'location_name',
		@HUD_Grant								as 'HUD_Grant',
		isnull(@FeeAmount,0)					as 'FeeAmount',
		0										as 'followup'		-- OBSOLETE

return ( 1 )
GO
