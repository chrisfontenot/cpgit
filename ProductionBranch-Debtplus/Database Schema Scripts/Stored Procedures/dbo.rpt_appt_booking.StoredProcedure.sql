USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_booking]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_booking] ( @from_date as datetime = null, @to_date as datetime = null ) as

if @To_Date is null
	select	@To_Date	= getdate()

if @From_Date is null
	select	@From_date	= @To_Date

select	@From_Date = convert(varchar(10), @From_Date, 101) + ' 00:00:00',
	@To_Date   = convert(varchar(10), @To_Date, 101)   + ' 23:59:59'

select	ca.client								as 'client',
	dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)	as 'client_name',
	ca.date_created								as 'date_created',
	isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default), dbo.format_counselor_name (ca.created_by))		as 'counselor',
	ca.start_time								as 'start_time',
	apt.appt_name								as 'appt_type',
	ca.status								as 'status'
from	client_appointments ca with (nolock)
left outer join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
left outer join people p with (nolock) on ca.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join counselors co with (nolock) on ca.created_by = co.person
left outer join names cox with (nolock) on co.NameID = cox.name

where	ca.date_created between @From_Date and @To_Date
and	ca.status in ('P','K','W')
order by 4, 3

return ( @@rowcount )
GO
