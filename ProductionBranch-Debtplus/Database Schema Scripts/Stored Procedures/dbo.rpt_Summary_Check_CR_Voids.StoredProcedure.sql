SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Summary_Check_CR_Voids] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for voided creditor checks                               ==
-- ======================================================================================================

set nocount on

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate, 101  ) + ' 23:59:59')

-- Find the transactions before we start to group by date/time	
select      d.creditor                                                                          as creditor,
            d.credit_amt                                                                        as amount,
            d.trust_register                                                                    as trust_register,
            convert(money, case when d.creditor_type = 'D' then d.fairshare_amt else 0 end)   as fairshare_amt,
            convert(datetime, convert(varchar(10), d.date_created, 101))                        as date_created,
            d.created_by                                                                        as created_by
into        #transactions
FROM	    registers_client_creditor d	WITH (NOLOCK)
WHERE	    tran_type = 'VD'
AND	    client >= 0
AND	    date_created BETWEEN @FromDate AND @ToDate

-- Return the resulting recordset to the caller
select  x.creditor                                                                  as 'creditor',
        sum(x.amount)                                                               as 'amount',
        sum(x.fairshare_amt)                                                        as 'fairshare_amt',
		cr.creditor_name							as 'creditor_name',
        x.date_created                                                              as 'date_created',
		tr.checknum								as 'checknum',
        x.created_by                                                                as 'created_by',
		tr.trust_register							as 'trust_register',
		tr.date_created								as 'date_written'
from        #transactions x
inner join  registers_trust tr with (nolock) on tr.trust_register = x.trust_register
inner join  creditors cr with (nolock) on x.creditor = cr.creditor
group by    x.creditor, cr.creditor_name, x.date_created, tr.checknum, x.created_by, tr.trust_register, tr.date_created
order by    date_created, trust_register;

drop table  #transactions;

RETURN ( @@rowcount )
GO
