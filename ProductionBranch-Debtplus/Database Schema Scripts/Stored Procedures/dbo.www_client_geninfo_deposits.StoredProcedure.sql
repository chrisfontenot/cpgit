USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_geninfo_deposits]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_geninfo_deposits] ( @client as int ) AS
-- ======================================================================================
-- ==            List of the expected deposit dates                                    ==
-- ======================================================================================
-- ChangeForm
-- 7/13/07 Return deposit information for ACH change form
SELECT		dbo.date_only ( deposit_date )	as deposit_date,
		sum(deposit_amount)		as deposit_amount
FROM		client_deposits
WHERE		client = @client
GROUP BY	dbo.date_only ( deposit_date )
ORDER BY 1

return ( @@rowcount )
GO
