USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_states]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_states] ( @MailingCode as varchar(4) = null, @Name as varchar(50), @Country as int = 1, @USAFormat as bit = 1, @default as bit = 0, @ActiveFlag as bit = 1, @AddressFormat as varchar(256) = '{0} {1}  {2}', @DoingBusiness as bit = 0, @TimeZoneID as INT = 12 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the states table                         ==
-- ========================================================================================
	insert into states ( [MailingCode], [Name], [Country], [USAFormat], [default], [ActiveFlag], [AddressFormat], [DoingBusiness], [TimeZoneID] ) values ( @MailingCode, @Name, @Country, @USAFormat, @default, @ActiveFlag, @AddressFormat, @DoingBusiness, @TimeZoneID )
	return ( scope_identity() )
GO
