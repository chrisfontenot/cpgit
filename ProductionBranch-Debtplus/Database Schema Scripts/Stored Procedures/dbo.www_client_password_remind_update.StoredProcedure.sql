USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_password_remind_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_password_remind_update] ( @client as int, @answer as varchar(80) = NULL, @ssn as varchar(11) = null, @dob as datetime = null, @new_password as varchar(80) = NULL ) AS
-- =========================================================================================================
-- ==            Update the password for the client when the answer matches                               ==
-- =========================================================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that the client exists
if not exists ( select	client
		from	clients
		where	client = @client )
begin
	return ( 0 )
end

-- Ensure that the client exists in the www page
if not exists ( select	pkid
		from	client_www
		where	username = convert(varchar,@client)
		and	PasswordAnswer = ltrim(rtrim(@answer)) )
begin
	RaisError ('The answer does not match the value that we have stored for your account', 16, 1)
	return ( 0 )
end

-- Update the database value
update	client_www
set	[password]	= @new_password,
    LastPasswordChangeDate = getutcdate()
where	username		= convert(varchar,@client)

return ( @@rowcount )
GO
