SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[xpr_get_dsa_loan_details]
(
	@clientid		int,
	@propertyid		int,
	@housingloanid	int,
	@currentlender	int,
	@intakedetail	int
)
as
begin

	declare @accountNumber				varchar(50)
	declare @loantype					varchar(100)
	declare @servicerid					int
	declare @mortgagecompanysourceid	int
	declare @budget						int

	declare @borfirstname				varchar(50)
	declare @bormiddlename				varchar(50)
	declare @borlastname				varchar(50)
	declare @borssn						varchar(50)
	declare @borbirthdate				datetime
	declare @borethnicity				varchar(100)
	declare @borrace					varchar(100)
	declare @borlanguage				varchar(50)
	declare @borgender					varchar(50)
	declare @bormaritalstatus			varchar(50)
	declare @borhomephone				varchar(50)
	declare @borworkphone				varchar(50)
	declare @borcellphone				varchar(50)
	declare @boremail					varchar(50)
	declare @boraddress1				varchar(100)
	declare @boraddress2				varchar(100)
	declare @boraddress3				varchar(100)
	declare @borcity					varchar(50)
	declare @borcounty					varchar(50)
	declare @borstatecode				varchar(10)
	declare @borzip						varchar(10)
	declare @borpreferredcontact		varchar(50)
	declare @borcurrentlyemployed		varchar(10)
	declare @boremployer				varchar(50)

	declare @bortotalgrossmonthlyincome	money
	declare @bortotalmonthlytakehome	money
	

	declare @cobortotalgrossmonthlyincome	money
	declare @cobortotalmonthlytakehome	money

	declare @coborfirstname				varchar(50)
	declare @cobormiddlename			varchar(50)
	declare @coborlastname				varchar(50)
	declare @coborssn					varchar(50)
	declare @coborbirthdate				datetime
	declare @coborgender				varchar(50)
	declare @coborethnicity				varchar(100)
	declare @coborrace					varchar(100)
	declare @coborworkphone				varchar(50)
	declare @coborcellphone				varchar(50)
	declare @coboremail					varchar(50)
	declare @coborcurrentlyemployed		varchar(10)
	declare @coboremployer				varchar(50)
	declare @coborlanguage				varchar(50)
	declare @cobormaritalstatus			varchar(50)

	declare @usehomeaddress				bit
	declare @propertyaddress			int
	declare @referralcode				int

	declare @propertyaddressline1		varchar(100)
	declare @propertyaddressline2		varchar(100)
	declare @propertyaddressline3		varchar(100)
	declare @propertycity		varchar(100)
	declare @propertycounty		varchar(100)
	declare @propertystate		varchar(100)
	declare @propertyzip		varchar(100)

	declare @numberofpersonliving		int
	declare @dependents					int
	declare @dsaprogram					int

	declare @balance					money
	declare @delinquencyamount			money
	declare @amountclientowe			varchar(50)
	declare @havingtroublepayingmortgage		varchar(50)

	declare @propertytaxpaidby		varchar(50)
	declare @taxescurrent			varchar(50)
	declare @policycurrent			varchar(50)
	declare @clientsituation		varchar(50)
	declare @bankruptcyfiled		varchar(50)
	declare @plantokeephome			varchar(50)
	declare @loanoriginationdate	varchar(50)
	declare @foreclosurenoticereceived varchar(50)
	declare @propertyforsale		varchar(50)
	declare @foreclosuresalescheduled  varchar(50)
	declare @foreclosuresaledate    datetime
	declare @bankruptcychapter		varchar(50)
	declare @bankruptcyfiledate     datetime
	declare @bankruptcydischarged	varchar(50)
	declare @propertytype			varchar(50)
	declare @whopayshazardinsurance	varchar(50)
	declare @modifiedinlastsixmonths varchar(10)

	declare @estimatedcurrentvalue money
	declare @interestrate			float
	declare @monthlypayment			money
	declare @monthsbehind			int
	declare @ifforeclosure			varchar(10)
	declare @saledate				datetime
	declare @investorsourceid		int
	declare @investorcode		varchar(10)
	declare @investorname			varchar(50)
	declare @investorloannumber		varchar(50)	
	declare @ownerid				int
	declare @coownerid				int
	declare @personid				int
	declare @coownerpersonid		int

	declare @financialproblemcause1	varchar(50)
	declare @financialproblemcause2	varchar(50)
	declare @financialproblemcause3	varchar(50)

	declare @counseloragency			varchar(50)
	declare @counselordesignateduser	varchar(50)
	declare @counselorname				varchar(50)

	declare @payhoafee					varchar(10)
	declare @howdidyouhearaboutus		varchar(50)
	declare @dsadetailid			int

	declare @defaultethnicitytype varchar(50)
	set @defaultethnicitytype = 'nothispanic'

	declare @defaultracetype varchar(50)
	set @defaultracetype = 'oth'

	declare @defaultfinancialproblem varchar(50)
	set @defaultfinancialproblem = '24'

	declare @defaultreferredby varchar(50)
	set @defaultreferredby = 'HEAR_ABOUT_US3'



	--Housing_loans(current_lenderid)->Housing lender(oid)
	--Housing lender(servicerid)->housing_lender_servicers(oid)
	--for dsa @mortgagecompanysourceid = RXOfficeID

	-- get dsa detail id from housing loans table
	select @dsadetailid = DsaDetailID
	  from Housing_loans
	 where oID = @housingloanid

	print 'get account number'
	--Details > LoanInformation > LoanNumber
	SELECT @accountNumber = hl.[acctnum] 
		  ,@servicerid = hl.[ServicerID]
		  ,@investorsourceid = hl.InvestorID
		  ,@investorname = i.InvestorName
		  ,@investorloannumber = hl.InvestorAccountNumber
	  FROM [Housing_lenders] as hl
	left outer join Investor as i on hl.InvestorID = i.ID
	 where oID = @currentlender

	-- get the rxofficeid for servicer from the housing_loan_servicers table
	if @servicerid is null 
	begin
		set @mortgagecompanysourceid = 958
	end
	else
	begin
		select @mortgagecompanysourceid = RXOffice
		  from Housing_lender_servicers
		 where oID = @servicerid
	end

	-- get investor code from investor table
	select @investorcode = RxOffice
	  from Investor
	 where ID = @investorsourceid;

	--SELECT [IntakeDetailID] FROM [Debtplus].[dbo].[Housing_loans] where PropertyID = @propertyid
	
	print 'get loan details'
	-- Use the IntakeDetailID as oID to get the Loan Type CD
	SELECT @loantype = (select description from Housing_LoanTypes where oID = [loantypecd]),
	       @interestrate = InterestRate,
		   @monthlypayment = Payment
	  FROM [Housing_loan_details] 
	 where oID = @intakedetail

	-- get property address
	
	print 'get housing properties details'
	select @usehomeaddress = isnull(UseHomeAddress, 0),
		   @propertyaddress = isnull(PropertyAddress, 0),
		   @numberofpersonliving       = occupancy,
	       @propertytaxpaidby          = case [property_tax]      when 0 then 'Lender does' when 1 then 'I do' else 'NA'end,
		   @taxescurrent               = case tax_delinq_state    when 0 then 'No' when 1 then 'Yes' else 'NA'end,
		   @policycurrent              = case ins_delinq_state    when 0 then 'No' when 1 then 'Yes' else 'NA'end,
		   @plantokeephome             = case PlanToKeepHomeCD    when 1 then 'Sell' when 2 then 'Keep' when 3 then 'Vacate' else 'Undecided'end,
		   @foreclosurenoticereceived  = case FcNoticeReceivedIND when 0 then 'No' else 'Yes' end,
		   @propertyforsale            = case ForSaleIND          when 0 then 'No' else 'Yes' end,
		   @foreclosuresalescheduled   = case when FcSaleDT is null then 'No' else 'Yes' end,
		   @foreclosuresaledate        = FcSaleDT,
		   @propertytype = case Residency when 1 then 'Primary Residence' when 2 then 'Second Home' when 3 then 'Investment' end,
		   @whopayshazardinsurance = case pmi_insurance when 0 then 'Lender Does' when 1 then 'I do' end,
		   @ifforeclosure = case when FcNoticeReceivedIND = 1 or FcSaleDT is not null then 'Y' else 'N' end,
		   @saledate = sales_contract_date,
		   @ownerid = OwnerID,
		   @coownerid = CoOwnerID,
		   @estimatedcurrentvalue = ImprovementsValue
	  FROM Housing_properties
	 where oID = @propertyid and [HousingID] = @clientid
	
	-- if user home address is specified, get the client address 
	if @usehomeaddress = 1
	begin
		print 'get property address using home address'
		select @propertyaddressline1 = isnull(a.house, '') + ' ' + isnull(a.direction, '') + ' ' + isnull(a.street, '') + ' ' + isnull(a.suffix, ''),
		       @propertyaddressline2 = a.address_line_2,
			   @propertyaddressline3 = a.address_line_3,
			   @propertycity = a.city,
			   @propertystate = s.MailingCode,
			   @propertyzip = a.PostalCode
		  from clients c
		  join addresses a on c.AddressID = a.address
		  join states s on a.state = s.state
		where c.client = @clientid
	end
	else
	begin
		print 'get property address based on property address'
		select @propertyaddressline1 = isnull(a.house, '') + ' ' + isnull(a.direction, '') + ' ' + isnull(a.street, '') + ' ' + isnull(a.suffix, ''),
		       @propertyaddressline2 = a.address_line_2,
			   @propertyaddressline3 = a.address_line_3,
			   @propertycity = a.city,
			   @propertystate = s.MailingCode,
			   @propertyzip = a.PostalCode
		  from addresses a 
		  join states s on a.state = s.state
		where a.address = @propertyaddress
	end

	select @borlanguage = (select isnull(RxOffice, '') from AttributeTypes where Grouping='LANGUAGE' and oID=language)
	      ,@bormaritalstatus = (select isnull(RxOffice, '') from MaritalTypes where oID = marital_status)
		  ,@borhomephone = (select isnull(acode, '') + '-' + isnull(number, '') from TelephoneNumbers where TelephoneNumber = HomeTelephoneID)
		  ,@boraddress1 = isnull(a.house, '') + ' ' + isnull(a.direction, '') + ' ' + isnull(a.street, '') + ' ' + isnull(a.suffix, '')
		  ,@boraddress2 = a.address_line_2
		  ,@boraddress3 = a.address_line_3
		  ,@borcity = a.city
		  ,@borstatecode = s.MailingCode
		  ,@borzip = a.PostalCode
		  ,@borcounty = co.name
		  ,@borpreferredcontact = case when cmt.description like '%phone%' then 'phone' when cmt.description like '%email%' then 'email' when cmt.description like '%postal%' then 'letter' else 'phone' end
		  ,@financialproblemcause1 = fp1.RxOffice
		  ,@financialproblemcause2 = fp2.RxOffice
		  ,@financialproblemcause3 = fp3.RxOffice
	  from clients c
	  left outer join addresses a on c.AddressID = a.address
	  left outer join states s on a.state = s.state
	  left outer join counties co on c.county = co.county
	  left outer join ContactMethodTypes cmt on c.preferred_contact = cmt.oID
	  left outer join financial_problems fp1 on c.cause_fin_problem1 = fp1.financial_problem
	  left outer join financial_problems fp2 on c.cause_fin_problem2 = fp2.financial_problem
	  left outer join financial_problems fp3 on c.cause_fin_problem3 = fp3.financial_problem
	 where client = @clientid

	print 'get borrower details'

	select @personid = PersonID
	  from Housing_borrowers
	 where oID = @ownerid

	-- Borrower details
	if @personid = 1
		-- Get borrower first and last name
		select @borfirstname = n.first
		     , @bormiddlename = n.Middle
			 , @borlastname = n.Last
			 , @borssn = p.SSN
			 , @borbirthdate = p.Birthdate
			 , @borethnicity = et.RxOffice
			 , @borrace      = rt.RxOffice
			 , @bankruptcyfiled =  case when bkfileddate is NULL then 'No' else  'Yes'end
			 , @bankruptcychapter = bc.description
			 , @bankruptcyfiledate = p.bkfileddate
			 , @bankruptcydischarged = case when p.bkfileddate is not null and p.bkdischarge is null then 'No' when p.bkfileddate is not null and p.bkdischarge is not null then 'Yes' end
			 , @borgender = gt.description
			 , @borworkphone = isnull(wp.Acode, '') + '-' + isnull(wp.Number, '')
			 , @borcellphone = isnull(cell.Acode, '') + '-' + isnull(cell.Number, '')
			 , @boremail = case 
						    email.Address when 'NOT SPECIFIED' then ''
							when 'NONE' then ''
							when 'DECLINED' then ''
							else
							email.Address
						   end 
			 , @borcurrentlyemployed = case 
										when p.emp_start_date is not null and p.emp_end_date is null then 'Y'
										--when p.emp_start_date is null and p.emp_end_date is null then 'N'
										when p.emp_end_date is not null then 'N'
									   end
			 , @boremployer = emp.name
			 , @bortotalgrossmonthlyincome = p.gross_income
			 , @bortotalmonthlytakehome = p.net_income
		  from Names n
		  join people p on n.Name = p.NameID
		  left outer join EthnicityTypes et on p.Ethnicity = et.oID
		  left outer join RaceTypes rt on p.Race = rt.oID
		  left outer join BankruptcyClassTypes bc on p.bkchapter = bc.oID
		  left outer join GenderTypes gt on p.Gender = gt.oID
		  left outer join TelephoneNumbers wp on p.WorkTelephoneID = wp.TelephoneNumber
		  left outer join TelephoneNumbers cell on p.CellTelephoneID = cell.TelephoneNumber
		  left outer join EmailAddresses email on p.EmailID = email.Email
		  left outer join employers emp on p.employer = emp.employer
		 where p.Relation = 1 and p.Client = @clientid

	else if @personid = 2
		-- Get borrower first and last name
		select @borfirstname = n.first
		     , @bormiddlename = n.Middle
			 , @borlastname = n.Last
			 , @borssn = p.SSN
			 , @borbirthdate = p.Birthdate
			 , @borethnicity = et.RxOffice
			 , @borrace      = rt.RxOffice
			 , @bankruptcyfiled =  case when bkfileddate is NULL then 'No' else  'Yes'end
			 , @bankruptcychapter = bc.description
			 , @bankruptcyfiledate = p.bkfileddate
			 , @bankruptcydischarged = case when p.bkfileddate is not null and p.bkdischarge is null then 'No' when p.bkfileddate is not null and p.bkdischarge is not null then 'Yes' end
			 , @borgender = gt.description
			 , @borworkphone = isnull(wp.Acode, '') + '-' + isnull(wp.Number, '')
			 , @borcellphone = isnull(cell.Acode, '') + '-' + isnull(cell.Number, '')
			 , @boremail = case 
						    email.Address when 'NOT SPECIFIED' then ''
							when 'NONE' then ''
							when 'DECLINED' then ''
							else
							email.Address
						   end 
			 , @borcurrentlyemployed = case 
										when p.emp_start_date is not null and p.emp_end_date is null then 'Y'
										--when p.emp_start_date is null and p.emp_end_date is null then 'N'
										when p.emp_end_date is not null then 'N'
									   end
			 , @boremployer = emp.name
			 , @bortotalgrossmonthlyincome = p.gross_income
			 , @bortotalmonthlytakehome = p.net_income
		  from Names n
		  join people p on n.Name = p.NameID
		  left outer join EthnicityTypes et on p.Ethnicity = et.oID
		  left outer join RaceTypes rt on p.Race = rt.oID
		  left outer join BankruptcyClassTypes bc on p.bkchapter = bc.oID
		  left outer join GenderTypes gt on p.Gender = gt.oID
		  left outer join TelephoneNumbers wp on p.WorkTelephoneID = wp.TelephoneNumber
		  left outer join TelephoneNumbers cell on p.CellTelephoneID = cell.TelephoneNumber
		  left outer join EmailAddresses email on p.EmailID = email.Email
		  left outer join employers emp on p.employer = emp.employer
		 where p.Relation <> 1 and p.Client = @clientid
	else 
		select @borfirstname = n.first
				 , @bormiddlename = n.Middle
				 , @borlastname = n.Last
				 , @borssn = hb.SSN
				 , @borbirthdate = hb.Birthdate
				 , @borethnicity = et.RxOffice
				 , @borrace      = rt.RxOffice
				 , @borgender = gt.description
				 , @boremail = case 
								email.Address when 'NOT SPECIFIED' then ''
								when 'NONE' then ''
								when 'DECLINED' then ''
								else
								email.Address
							   end 
				 , @borlanguage = at.RxOffice
				 , @bormaritalstatus = mt.RxOffice
			  from Housing_borrowers hb
			  left outer join Names n on hb.NameID = n.Name
			  left outer join EthnicityTypes et on hb.Ethnicity = et.oID
			  left outer join RaceTypes rt on hb.Race = rt.oID
			  left outer join GenderTypes gt on hb.Gender = gt.oID
			  left outer join EmailAddresses email on hb.EmailID = email.Email
			  left outer join AttributeTypes at on hb.Language = at.oID and 'LANGUAGE' = at.grouping
			  left outer join MaritalTypes mt on hb.marital_status = mt.oID
			 where hb.oID = @ownerid

	print 'get co borrower details'

	select @coownerpersonid = PersonID
	  from Housing_borrowers
	 where oID = @coownerid

	if @coownerpersonid = 1
		select @coborfirstname = n.first
		     , @cobormiddlename = n.Middle
			 , @coborlastname = n.Last
			 , @coborssn = p.SSN
			 , @coborbirthdate = p.Birthdate
			 , @coborgender = gt.description
			 , @coborethnicity = et.RxOffice
			 , @coborrace      = rt.RxOffice
			 , @coborworkphone = isnull(wp.Acode, '') + '-' + isnull(wp.Number, '')
			 , @coborcellphone = isnull(cell.Acode, '') + '-' + isnull(cell.Number, '')
			 , @coboremail = case 
								email.Address when 'NOT SPECIFIED' then ''
								when 'NONE' then ''
								when 'DECLINED' then ''
								else
								email.Address
							  end 
			 , @coborcurrentlyemployed = case 
										when p.emp_start_date is not null and p.emp_end_date is null then 'Y'
										--when p.emp_start_date is null and p.emp_end_date is null then 'N'
										when p.emp_end_date is not null then 'N'
									   end
			 , @coboremployer = emp.name
			 , @cobortotalgrossmonthlyincome = p.gross_income
			 , @cobortotalmonthlytakehome = p.net_income
		  from Names n
		  join people p on n.Name = p.NameID
		  left outer join EthnicityTypes et on p.Ethnicity = et.oID
		  left outer join RaceTypes rt on p.Race = rt.oID
		  left outer join BankruptcyClassTypes bc on p.bkchapter = bc.oID
		  left outer join GenderTypes gt on p.Gender = gt.oID
		  left outer join TelephoneNumbers wp on p.WorkTelephoneID = wp.TelephoneNumber
		  left outer join TelephoneNumbers cell on p.CellTelephoneID = cell.TelephoneNumber
		  left outer join EmailAddresses email on p.EmailID = email.Email
		  left outer join employers emp on p.employer = emp.employer
		 where p.Relation = 1 and p.Client = @clientid

	else if @coownerpersonid = 2
		select @coborfirstname = n.first
		     , @cobormiddlename = n.Middle
			 , @coborlastname = n.Last
			 , @coborssn = p.SSN
			 , @coborbirthdate = p.Birthdate
			 , @coborgender = gt.description
			 , @coborethnicity = et.RxOffice
			 , @coborrace      = rt.RxOffice
			 , @coborworkphone = isnull(wp.Acode, '') + '-' + isnull(wp.Number, '')
			 , @coborcellphone = isnull(cell.Acode, '') + '-' + isnull(cell.Number, '')
			 , @coboremail = case 
								email.Address when 'NOT SPECIFIED' then ''
								when 'NONE' then ''
								when 'DECLINED' then ''
								else
								email.Address
							  end 
			 , @coborcurrentlyemployed = case 
										when p.emp_start_date is not null and p.emp_end_date is null then 'Y'
										--when p.emp_start_date is null and p.emp_end_date is null then 'N'
										when p.emp_end_date is not null then 'N'
									   end
			 , @coboremployer = emp.name
			 , @cobortotalgrossmonthlyincome = p.gross_income
			 , @cobortotalmonthlytakehome = p.net_income
		  from Names n
		  join people p on n.Name = p.NameID
		  left outer join EthnicityTypes et on p.Ethnicity = et.oID
		  left outer join RaceTypes rt on p.Race = rt.oID
		  left outer join BankruptcyClassTypes bc on p.bkchapter = bc.oID
		  left outer join GenderTypes gt on p.Gender = gt.oID
		  left outer join TelephoneNumbers wp on p.WorkTelephoneID = wp.TelephoneNumber
		  left outer join TelephoneNumbers cell on p.CellTelephoneID = cell.TelephoneNumber
		  left outer join EmailAddresses email on p.EmailID = email.Email
		  left outer join employers emp on p.employer = emp.employer
		 where p.Relation <> 1 and p.Client = @clientid
	else 
		select @coborfirstname = n.first
				 , @cobormiddlename = n.Middle
				 , @coborlastname = n.Last
				 , @coborssn = hb.SSN
				 , @coborbirthdate = hb.Birthdate
				 , @coborethnicity = et.RxOffice
				 , @coborrace      = rt.RxOffice
				 , @coborgender = gt.description
				 , @coboremail = case 
									email.Address when 'NOT SPECIFIED' then ''
									when 'NONE' then ''
									when 'DECLINED' then ''
									else
									email.Address
								 end 
				 , @coborlanguage = at.RxOffice
				 , @cobormaritalstatus = mt.RxOffice
			  from Housing_borrowers hb
			  left outer join Names n on hb.NameID = n.Name
			  left outer join EthnicityTypes et on hb.Ethnicity = et.oID
			  left outer join RaceTypes rt on hb.Race = rt.oID
			  left outer join GenderTypes gt on hb.Gender = gt.oID
			  left outer join EmailAddresses email on hb.EmailID = email.Email
			  left outer join AttributeTypes at on hb.Language = at.oID AND 'LANGUAGE' = at.grouping
			  left outer join MaritalTypes mt on hb.marital_status = mt.oID
			 where hb.oID = @coownerid

	print 'get housing loan details'
	SELECT @balance             = [CurrentLoanBalanceAmt],
	       @delinquencyamount   = LoanDelinquencyAmount,
		   @loanoriginationdate = case when LoanOriginationDate < Convert(datetime,'01/01/2009') then 'Yes' else 'No' end,
		   @havingtroublepayingmortgage = case LoanDelinquencyAmount when 0 then 'No' else  'Yes' end,
		   @modifiedinlastsixmonths = case when LoanOriginationDate is not null then case when datediff(day, LoanOriginationDate, dateadd(month, datediff(month, 0, dateadd(month, -5, getdate())), 0)) < 0 then 'Y' else 'N' end end,
		   @monthsbehind = LoanDelinquencyMonths
		   
      FROM [Housing_loans] 
     where IntakeDetailID = @intakedetail

	 print 'get current loan balance amount'
	SELECT @amountclientowe = case when CurrentLoanBalanceAmt < 729750 then 'Yes' else  'No' end 
	  from Housing_loans
	 where Loan1st2nd = 1 and IntakeDetailID = @intakedetail
	 
	print 'get client details'
	select @dependents = dependents,
		   @referralcode = referred_by
	  from clients 
	 where client = @clientid


	 print 'get housing loan dsa details'
	 -- get dsa details
	select @dsaprogram = program,
		   @clientsituation = case [ClientSituation] when 1 then 'short term' when 2 then 'long term' when 3 then 'permanent'  else  'NA'end 
	  from Housing_Loan_DSADetails
	 where oid = @dsadetailid;

	-- Counselor Information
	select top(1) @counseloragency = name 
	  from config

--	select @counselordesignateduser = c.RxOfficeCounselorID
--		 , @counselorname = isnull(n.first, '') + ' ' + isnull(n.last, '')
--	  from counselors c
--	left outer join names n on c.NameID = n.Name
--	 where person = (select top(1) interview_counselor 
--	                   from hud_interviews 
--					  where client = @clientid 
--				   order by result_date desc)


	select  @counselorname = isnull(n.first, '') + ' ' + isnull(n.last, '')
	  from counselors c
	left outer join names n on c.NameID = n.Name
	 where person = (select top(1) interview_counselor 
	                   from hud_interviews 
					  where client = @clientid 
				   order by result_date desc)

	select @counselordesignateduser = c.RxOfficeCounselorID
	 from counselors c
	where c.counselor = (select Specialist from dbo.Housing_Loan_DSADetails where oId = @dsadetailid)


	select @budget = budget
	  from budgets
	 where client = @clientid
	select @payhoafee = case when client_amount is not null and client_amount > 0 then 'Yes' else 'No' end
	  from budget_detail
	 where budget = @budget 
	   and budget_category = 1000

	select @howdidyouhearaboutus = RxOffice 
	  from referred_by 
	 where referred_by = (select top(1) referred_by 
	                        from client_appointments 
                           where client = @clientid and status in ('P', 'W')
                        order by start_time desc)

	select @accountNumber as AccountNumber
	     , isnull(@loantype, 0) as LoanType
		 , @mortgagecompanysourceid as MortgageCompanySourceID
		 
		 , @borfirstname as BorrowerFirstName
		 , @bormiddlename as BorrowerMiddleName
		 , @borlastname as BorrowerLastName
		 , @borssn as BorrowerSSN
		 , @borbirthdate as BorrowerBirthdate
		 , ISNULL(@borethnicity, @defaultethnicitytype) as BorrowerEthnicity
		 , ISNULL(@borrace, @defaultracetype) as BorrowerRace
		 , @borlanguage as BorrowerLanguage
		 , @borgender as BorrowerGender
		 , @bormaritalstatus as BorrowerMaritalStatus
		 , @borhomephone as BorrowerHomePhone
		 , @borworkphone as BorrowerWorkPhone
		 , @borcellphone as BorrowerCellPhone
		 , @boremail as BorrowerEmail
		 , @boraddress1 as BorrowerAddress1
		 , @boraddress2 as BorrowerAddress2
		 , @boraddress3 as BorrowerAddress3
		 , @borcity as BorrowerCity
		 , @borcounty as BorrowerCounty
		 , @borstatecode as BorrowerStateCode
		 , @borzip as BorrowerZip
		 , @numberofpersonliving as NoOfPersonLiving
		 , @dependents as Dependents
		 , @borpreferredcontact as BorrowerPreferredContact
		 , @borcurrentlyemployed as BorrowerCurrentlyEmployed
		 , @boremployer as BorrowerEmployer

		 , @coborfirstname as CoborrowerFirstName
		 , @cobormiddlename as CoborrowerMiddleName
		 , @coborlastname as CoborrowerLastName
		 , @coborssn as CoborrowerSSN
		 , @coborbirthdate as CoborrowerBirthdate
		 , @coborgender as CoborrowerGender
		 , ISNULL(@coborethnicity, @defaultethnicitytype) as CoborrowerEthnicity
		 , ISNULL(@coborrace, @defaultracetype) as CoborrowerRace
		 , @coborworkphone as CoborrowerWorkphone
		 , @coborcellphone as CoborrowerCellphone
		 , @coboremail as CoborrowerEmail
		 , @coborcurrentlyemployed as CoborrowerCurrentlyEmployed
		 , @coboremployer as CoborrowerEmployer
		 , @coborlanguage as CoborrowerLanguage
		 , @cobormaritalstatus as CoborrowerMaritalStatus


		 , @propertyaddressline1 as AddressLine1
		 , @propertyaddressline2 as AddressLine2
		 , @propertyaddressline3 as AddressLine3
		 , @propertycity as City
		 , @propertystate as StateCode
		 , @propertyzip as ZipCode

		 , @balance as Balance
		 , @delinquencyamount as DelinquencyAmount

		 , @bortotalgrossmonthlyincome as BorrowerTotalGrossMonthlyIncome
		 , @bortotalmonthlytakehome as BorrowerTotalMonthlyTakeHome
		 

		 , @cobortotalgrossmonthlyincome as CoborrowerTotalGrossMonthlyIncome
		 , @cobortotalmonthlytakehome as CoborrowerTotalMonthlyTakeHome

		 , @estimatedcurrentvalue as EstimatedCurrentValue

		 , @dsaprogram as DsaProgram
		 , @referralcode as ReferralCode
		 , @propertytaxpaidby as PropertyTaxPaidBy
		 , @taxescurrent as TaxesCurrent
		 , @policycurrent as PolicyCurrent
		 , @clientsituation as ClientSituation
		 , @bankruptcyfiled as BankruptcyFiled
		 , @plantokeephome as PlanToKeepHome
		 , @amountclientowe as AmountClientOwe
		 , @loanoriginationdate as LoanOriginationDate
		 , @havingtroublepayingmortgage as HavingTroublePayingMortgage
		 , @foreclosurenoticereceived as ForecloseNoticeReceived
		 , @propertyforsale as PropertyForSale
		 , @foreclosuresalescheduled as ForeclosureSaleScheduled
		 , @foreclosuresaledate as ForeclosureSaleDate
		 , @bankruptcychapter as BankruptcyChapter
		 , @bankruptcyfiledate as BankruptcyFileDate
		 , @bankruptcydischarged as BankruptcyDischarged
		 , @propertytype as PropertyType
		 , @whopayshazardinsurance as WhoPaysHazardInsurance
		 , @modifiedinlastsixmonths as ModifiedInLastSixMonths
		 , @interestrate as InterestRate
		 , @monthlypayment as MonthlyPayment
		 , @monthsbehind as MonthsBehind
		 , @ifforeclosure as IfForeclosure
		 , @saledate as SaleDate
		 , @payhoafee as HoaFee
		 , @investorsourceid as InvestorSourceID
		 , @investorcode as InvestorCode
		 , @investorname as InvestorName
		 , @investorloannumber as InvestorLoanNumber
		 , @counseloragency as CounselorAgency
		 , @counselordesignateduser as CounselorDesignatedUser
		 , @counselorname as CounselorName
		 , ISNULL(@howdidyouhearaboutus, @defaultreferredby) as HowDidYouHearAboutUs
		 , isnull(@financialproblemcause1, @defaultfinancialproblem) as CauseOfFinancialProblem1
		 , isnull(@financialproblemcause2, @defaultfinancialproblem) as CauseOfFinancialProblem2
		 , isnull(@financialproblemcause3, @defaultfinancialproblem) as CauseOfFinancialProblem3
end
GO