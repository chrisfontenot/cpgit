USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[help_index]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[help_index] (@table_name varchar(256), @index_name varchar(256))  AS CREATE TABLE #TmpStats (TABLE_QUALIFIER varchar(256) NULL, TABLE_OWNER  varchar(256) NULL, TABLE_NAME varchar(256) NOT NULL, NON_UNIQUE smallint null, INDEX_QUALIFIER varchar(256) null, IND_NAME varchar(256) null, TYPE smallint NOT NULL,  SEQ_IN_INDEX smallint null, COLUMN_NAME  varchar(256) null, COLLATION char(1) null, CARDINALITY  int null, PAGES int null, FILTER_CONDITION varchar(256)) INSERT INTO #TmpStats EXECUTE sp_statistics @table_name SELECT COLUMN_NAME FROM #TmpStats WHERE IND_NAME = @index_name ORDER BY SEQ_IN_INDEX  DROP TABLE #TmpStats
GO
