USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_Interest]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MONTHLY_Interest] AS
-- ============================================================================================
-- ==              Compute the monthly interest burden for the client debts                  ==
-- ============================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress warnings and errors
SET ANSI_WARNINGS OFF
SET ARITHABORT OFF
SET NUMERIC_ROUNDABORT OFF
SET XACT_ABORT ON
SET NOCOUNT ON

-- Create a transaction for the whole thing or nothing
BEGIN TRANSACTION

-- Create the working table for the interest calculations
create table #interest (client int, creditor varchar(10), client_creditor int, client_creditor_balance int, balance money, rate float null, interest money null)

-- Create the table with the interest figures from the debt information
insert into #interest (client, creditor, client_creditor, client_creditor_balance, balance, rate)
select		cc.client, cc.creditor, cc.client_creditor, cc.client_creditor_balance, isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0), cc.dmp_interest
from		client_creditor cc
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c ON cc.client = c.client
INNER JOIN	creditors cr ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl ON cr.creditor_class = ccl.creditor_class
WHERE		cc.reassigned_debt = 0
AND		isnull(ccl.zero_balance,0) = 0
AND		c.active_status IN ('A', 'AR')
AND		((c.start_date IS NOT NULL) AND (c.start_date <= getdate()))

-- Remove items which have no balance
delete
from		#interest
where		balance <= 0

-- Fill in the creditor interest from the creditor table as a default value
update		#interest
set		rate	= cr.medium_apr_pct
from		#interest i
inner join	creditors cr with (nolock) on i.creditor = cr.creditor
where		i.rate is null
and		i.balance < cr.medium_apr_amt

update		#interest
set		rate	= cr.medium_apr_pct
from		#interest i
inner join	creditors cr with (nolock) on i.creditor = cr.creditor
where		i.rate is null
and		i.balance between cr.medium_apr_amt and cr.highest_apr_amt

update		#interest
set		rate	= cr.highest_apr_pct
from		#interest i
inner join	creditors cr with (nolock) on i.creditor = cr.creditor
where		i.rate is null
and		i.balance >= cr.highest_apr_amt

-- The rate must be valid to be processed
update		#interest
set		rate	= 0
where		rate is null

-- Reduce the rate to a value less than 1.
update		#interest
set		rate	= rate / 100.0
where		rate >= 1.0

update		#interest
set		rate	= rate / 100.0
where		rate >= 1.0

update		#interest
set		rate	= rate / 100.0
where		rate >= 1.0

-- Ignore rates which are zero
delete
from		#interest
where		rate	<= 0

-- Calculate the interest amount
update		#interest
set		interest = convert(decimal(19,2), (convert(float,balance) * (rate / 12.0)) )

-- Delete items which have no interest amount. The balance may be too small to properly calculate an interest amount.
delete
from		#interest
where		interest <= 0

/*
-- Delete transactions from ones which have already been processed.
-- Do not enable this. It is only if the process was partially performed. If you do enable it, check the date on the statement.
delete	#interest
from	#interest i
inner join registers_client_creditor rcc on i.client_creditor = rcc.client_creditor and 'IN' = rcc.tran_type
where   rcc.date_created >= '12/31/2014'
*/

-- Apply the interest to the balance information
update		client_creditor_balances
set		total_interest = isnull(bal.total_interest,0) + i.interest
from		client_creditor_balances bal
inner join	#interest i on bal.client_creditor_balance = i.client_creditor_balance

-- Update the statistics for the current instance of the debt
update		client_creditor
set		interest_this_creditor = isnull(cc.interest_this_creditor,0) + i.interest
from		client_creditor cc
inner join	#interest i on cc.client_creditor = i.client_creditor

-- Generate the interest transaction
INSERT INTO registers_client_creditor (tran_type, client, creditor, client_creditor, credit_amt, fairshare_pct)
select		'IN', client, creditor, client_creditor, interest, rate
from		#interest

drop table #interest

-- Commit the changes to the database
COMMIT TRANSACTION

-- Reset the flags
SET ANSI_WARNINGS ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT ON
SET NOCOUNT OFF

-- Return success
RETURN ( 1 )
GO
