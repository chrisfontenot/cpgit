USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_secured_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_secured_types] as

select	secured_type	as item_key,
	isnull(grouping+' ','')  + isnull(description,'') as description
from	secured_types with (nolock)
order by 2
GO
