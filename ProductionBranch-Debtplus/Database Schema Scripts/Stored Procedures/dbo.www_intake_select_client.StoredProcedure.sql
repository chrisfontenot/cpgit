USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_select_client]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_select_client] (@intake_client as int, @intake_id as varchar(20)) as
	select	*
	from	intake_clients with (nolock)
	where	intake_client	= @intake_client
	and		intake_id		= @intake_id
	return ( @@rowcount )
GO
