USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AppointmentActivity_ByResult]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_AppointmentActivity_ByResult] ( @FromDate AS DateTime = NULL, @office as int = null ) AS
-- ===========================================================================================
-- ==           Information for the appointment activity report                             ==
-- ===========================================================================================

-- ChangeLog
--   6/16/2002
--      Added walkin as a completed appointment
--   7/28/2003
--      Added office

-- Disable intermediate result sets
SET NOCOUNT ON

-- Suppress ansi warnings about nulls
SET ANSI_WARNINGS OFF

-- This is the cutoff date
DECLARE @CutoffDate DATETIME

-- Default the date to today if it is not supplied
IF @FromDate IS NULL
	SELECT @FromDate = getdate()

-- Remove the time from the date value
SELECT	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')

-- Split the input date into the appropriate items
DECLARE @Month as INT
DECLARE @Year  as INT
DECLARE @Day   as INT
SELECT	@Month = datepart ( month, @FromDate ),
	@Day = datepart ( day, @FromDate ),
	@Year = datepart ( year, @FromDate )

-- Set the limits for the month operation
DECLARE @Lower_Month    DATETIME
SELECT  @Lower_Month  = CONVERT ( DATETIME, convert(varchar,@Month) + '/01/' + convert(varchar,@Year) )

-- Set the limits for the year operation
DECLARE @Lower_Year     DATETIME
SELECT  @Lower_Year   = CONVERT ( DATETIME, '01/01/' + convert(varchar,@Year) )

-- Set the limits for the quarter
DECLARE @Lower_Quarter  DATETIME
SELECT	@Month = (((@Month - 1) / 3) * 3) + 1
SELECT  @Lower_Quarter = CONVERT ( DATETIME, convert(varchar, @Month ) + '/01/' + convert(varchar,@Year) )

-- Define the cutoff at the end of the desired month
SELECT	@CutoffDate	= dateadd(s, -1, dateadd(m, 1, @Lower_Month))
IF @CutoffDate > getdate()
	SELECT	@CutoffDate	= getdate()

-- Create a temporary table to hold the results so that it may be summarized
CREATE TABLE #t_Appointment_Results(
                Description VarChar(50),
                MTDCount Int,
                QTDCount Int,
                YTDCount Int)

-- Populate the table with the labels but no counts
INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
VALUES ('DMP', 0, 0, 0)

INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
VALUES ('FCO', 0, 0, 0)

INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
VALUES ('ROA', 0, 0, 0)

INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
VALUES ('WKS', 0, 0, 0)

INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
VALUES ('PND', 0, 0, 0)

-- Calculate the appointments by the month to date
INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
SELECT		ca.result,
		COUNT(ca.result) as 'MTDCount',
		0 as 'QTDCount',
		0 as 'YTDCount'
FROM		client_appointments ca
WHERE		ca.status in ('K', 'W')
AND		ca.start_time BETWEEN @Lower_Month AND @CutoffDate
AND		ca.workshop is null
AND		((@office = -1) or (ca.office = @office))
GROUP BY	ca.result

-- Calculate the appointments by the quarter to date
INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
SELECT		ca.result,
		0 as 'MTDCount',
		COUNT(ca.result) as 'QTDCount',
		0 as 'YTDCount'
FROM		client_appointments ca
WHERE		ca.status IN ('K', 'W')
AND		ca.start_time BETWEEN @Lower_Quarter AND @CutoffDate
AND		ca.workshop IS NULL
AND		((@office = -1) or (ca.office = @office))
GROUP BY	ca.result

-- Calculate the appointments by the year to date
INSERT INTO #t_Appointment_Results (Description, MTDCount, QTDCount, YTDCount)
SELECT		ca.result,
		0 as 'MTDCount',
		0 as 'QTDCount',
		COUNT(ca.result) as 'YTDCount'
FROM		client_appointments ca
WHERE		ca.status IN ('K', 'W')
AND		ca.start_time BETWEEN @Lower_Year AND @CutoffDate
AND		ca.workshop IS NULL
AND		((@office = -1) or (ca.office = @office))
GROUP BY	ca.result

-- Return the resulting recordset which is the sum of the temporary table
SELECT		case Description
			when 'DMP' then 'Debt Management Program'
			when 'FCO' then 'Financial Counseling Only'
			when 'PND' then 'Pending information'
			when 'ROA' then 'Referred to Other Agency'
			when 'WKS' then 'Referred to Workshop'
			when NULL  then 'None was specified'
			else 'Not Specified ' + Description
		end as 'Description',

		SUM(MTDCount) as 'MTDCount',
		SUM(QTDCount) as 'QTDCount',
		SUM(YTDCount) as 'YTDCount'
FROM		#t_Appointment_Results
GROUP BY	Description
ORDER BY	1

-- Drop the working table
DROP TABLE #t_Appointment_Results

RETURN ( @@rowcount )
GO
