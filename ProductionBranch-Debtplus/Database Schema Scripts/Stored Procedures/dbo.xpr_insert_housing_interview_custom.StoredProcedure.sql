USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_interview_custom]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_housing_interview_custom] ( @oID AS INT ) AS
-- ===============================================================================================
-- ==          Do any custom events based upon insert a new housing Purpose of Visit            ==
-- ===============================================================================================

-- Suppress duplicate datasets
set nocount on

-- Load the washington state retention events
insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
	select iv.client as client, ev.retention_event as retention_event, 'For housing interview created on ' + convert(varchar(10), iv.interview_date, 101) as message, ev.expire_type as expire_type, dbo.date_only(dateadd(d, 45+30, getdate())) as expire_date, ev.priority as priority, dbo.date_only(dateadd(d, 45, getdate())) as effective_date
	from   hud_interviews iv
	inner join clients c on iv.client = c.client
	inner join addresses a on c.addressid = a.address
	inner join states st on a.state = st.state
	full outer join retention_events ev on 128 = ev.retention_event
	where st.mailingcode = 'WA'
	and   ev.retention_event is not null
	and	  iv.hud_interview = @oID

-- Load the washington state retention event #2
insert into client_retention_events (client, retention_event, message, expire_type, expire_date, priority, effective_date)
	select iv.client as client, ev.retention_event as retention_event, 'For housing interview created on ' + convert(varchar(10), iv.interview_date, 101) as message, ev.expire_type as expire_type, dbo.date_only(dateadd(d, 90+30, getdate())) as expire_date, ev.priority as priority, dbo.date_only(dateadd(d, 90, getdate())) as effective_date
	from   hud_interviews iv
	inner join clients c on iv.client = c.client
	inner join addresses a on c.addressid = a.address
	inner join states st on a.state = st.state
	full outer join retention_events ev on 129 = ev.retention_event
	where st.mailingcode = 'WA'
	and   ev.retention_event is not null
	and	  iv.hud_interview = @oID

return ( 1 )
GO
