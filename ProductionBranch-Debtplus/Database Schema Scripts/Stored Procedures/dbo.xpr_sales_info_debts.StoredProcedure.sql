USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_sales_info_debts]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_sales_info_debts] ( @sales_file as int, @client as int = null ) as

-- ======================================================================================================
-- ==            Find the list of debts for the sales tool function                                    ==
-- ======================================================================================================

select	creditor_type, creditor_name, client_creditor, balance, interest_rate, minimum_payment, late_fees, overlimit_fees, cosigner_fees, finance_charge, total_interest_fees, principal, plan_min_prorate, plan_min_payment, plan_payment, plan_interest_rate, plan_total_fees, plan_finance_charge, plan_total_interest_fees, plan_principal, account_number
from	sales_debts with (nolock)
where	sales_file = @sales_file

return ( @@rowcount )
GO
