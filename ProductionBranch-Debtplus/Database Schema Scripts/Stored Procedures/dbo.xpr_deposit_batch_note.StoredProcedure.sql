USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_note]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_note] (@client as INT, @deposit_batch_id AS INT) AS
-- ========================================================================
-- ==          Fetch the client disbursement note for this batch         ==
-- ========================================================================

SET NOCOUNT ON

DECLARE	@disbursement_note	int

-- Fetch the existing note ID from the system
SELECT	@disbursement_note	= disbursement_note
FROM	disbursement_notes
WHERE	client			= @client
AND	deposit_batch_id	= @deposit_batch_id

-- If there is no note then create one
IF @disbursement_note IS NULL
BEGIN
	-- Insert a blank disbursement note for this disbursement batch
	INSERT INTO disbursement_notes	(client,	deposit_batch_id,	subject)
	VALUES				(@client,	@deposit_batch_id,	'Disbursement note for deposit batch ' + right('00000000' + convert(varchar,@deposit_batch_id), 8))

	-- The note ID is the newly inserted record
	SELECT @disbursement_note = SCOPE_IDENTITY()
END

-- Return the disbursement note ID
RETURN ( @disbursement_note )
GO
