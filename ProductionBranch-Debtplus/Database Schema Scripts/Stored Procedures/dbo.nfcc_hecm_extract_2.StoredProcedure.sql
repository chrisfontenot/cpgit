USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_hecm_extract_2]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nfcc_hecm_extract_2] ( @from_date as datetime, @to_date as datetime ) as

execute nfcc_hecm_extract @from_date, @to_date

exec ('drop table temp_nfcc_hecm_extract_2');
-- truncate table temp_nfcc_hecm_extract_2
-- insert into temp_nfcc_hecm_extract_2 ( outcome, client, [Date], Counselor, CounselingHours)
select
	case
		when seeking_help like '%Obtained Home Equity Conversion%' then '7c-1'
		else '7c-2'
	end as outcome,
	dbo.format_client_id ( client ) as client,
	DateCertIssued as 'Date',
	dbo.format_reverse_name(default, left(cox.first,1), default, cox.last, default) as Counselor,
	round( convert(float, SessionTime) / 60.0, 4) as CounselingHours

into	temp_nfcc_hecm_extract_2
from	temp_nfcc_hecm_extract x
left outer join counselors co on x.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name
GO
