USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ACH_Transaction_Header]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ACH_Transaction_Header] ( @FileID AS INT = NULL ) AS
-- ========================================================================================
-- ==             Fetch the header information for the ACH report                        ==
-- ========================================================================================

-- Disable the count of rows
SET NOCOUNT ON

-- Fetch the most recient file if there is none supplied
IF @FileID IS NULL
BEGIN
	SELECT TOP 1	@FileID = deposit_batch_id
	FROM		deposit_batch_ids
	WHERE		batch_type = 'AC'
	ORDER BY	date_created desc
END

-- There should be a file at this point
IF @FileID IS NULL
BEGIN
	RaisError (50032, 11, 1)
	RETURN ( 0 )
END

-- Fetch the information for the report
SELECT	case
		when date_posted IS NULL then 'OPEN'
					 else 'CLOSED'
	end				as 'status',

	ach_pull_date			as 'pull_date',
	null				as 'file_id_modifier',
	ach_effective_date		as 'effective_entry_date',
	ach_settlement_date		as 'settlement_date',
	[ach_file]			as 'filename',
	date_created			as 'date_created',
	date_posted			as 'date_posted'
	
FROM	deposit_batch_ids f WITH (NOLOCK)
WHERE	f.deposit_batch_id = @FileID

-- Return the number of records
RETURN ( @@rowcount )
GO
