USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Compare_Disbursement_To_Deposits]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Compare_Disbursement_To_Deposits]( @counselor as int = null ) AS
-- ===========================================================================================
-- ==                   Find the deposit amount(s) total for the month by client            ==
-- ===========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   2/26/2003
--     Added support for counselor selection
--   3/10/2008
--     Added "total_debt" column to the result
--   3/23/2009
--     Added "disbursement_date" column to the result

-- Suppress intermediate result sets
set nocount on
CREATE table #t_total_deposits ( client int, deposit money );
create index ix1_total_deposits on #t_total_deposits ( client );
create table #t_total_disbursements ( client int, disbursement money, debt money, disbursement_date int );
create index ix1_total_disbursements on #t_total_deposits ( client );

if @counselor is not null
begin
	if not exists (select * from counselors where counselor = @counselor)
		select	@counselor = null
end

if @counselor is null
begin
	-- Fetch the deposited amounts
	insert into	#t_total_deposits (client, deposit )
	select		d.client,
			sum(d.deposit_amount) as 'deposit'
	from		client_deposits d WITH (NOLOCK)
	inner join clients c WITH (NOLOCK) on d.client = c.client
	where		c.active_status in ('A','AR')
	group by	d.client

	-- fetch the disbursement amounts
	insert into	#t_total_disbursements ( client, disbursement, debt, disbursement_date )
	select		cc.client,
			sum(isnull(cc.disbursement_factor,0)) as 'disbursement',
			sum(bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments) as 'debt',
			c.disbursement_date
	from		client_creditor cc WITH (NOLOCK)
	inner join	client_creditor_balances bal WITH (NOLOCK) on cc.client_creditor_balance = bal.client_creditor_balance
	inner join	clients c WITH (NOLOCK) on cc.client = c.client
	inner join	creditors cr WITH (NOLOCK) on cc.creditor = cr.creditor
	LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) on cr.creditor_class = ccl.creditor_class
	where		c.active_status in ('A','AR')
	AND		cc.reassigned_debt = 0
	AND		(bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments > 0) or (isnull(ccl.zero_balance,0) > 0)
	group by	cc.client, c.disbursement_date

end else begin

	-- Fetch the deposited amounts
	insert into	#t_total_deposits (client, deposit )
	select		d.client,
			sum(d.deposit_amount) as 'deposit'
	from		client_deposits d WITH (NOLOCK)
	inner join clients c WITH (NOLOCK) on d.client = c.client
	where		c.active_status in ('A','AR')
	and		c.counselor = @counselor
	group by	d.client

	-- fetch the disbursement amounts
	insert into	#t_total_disbursements ( client, disbursement, debt, disbursement_date )
	select		cc.client,
			sum(isnull(cc.disbursement_factor,0)) as 'disbursement',
			sum(bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments) as 'debt',
			c.disbursement_date
	from		client_creditor cc WITH (NOLOCK)
	inner join	client_creditor_balances bal WITH (NOLOCK) on cc.client_creditor_balance = bal.client_creditor_balance
	inner join	clients c WITH (NOLOCK) on cc.client = c.client
	inner join	creditors cr WITH (NOLOCK) on cc.creditor = cr.creditor
	LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) on cr.creditor_class = ccl.creditor_class
	where		c.active_status in ('A','AR')
	AND		c.counselor = @counselor
	AND		cc.reassigned_debt = 0
	AND		(bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments > 0) or (isnull(ccl.zero_balance,0) > 0)
	group by	cc.client, c.disbursement_date

end

-- ===========================================================================================
-- ==                   Find the deposit amount(s) total for the month by client            ==
-- ===========================================================================================

select		dep.client					as 'client',
		isnull(dep.deposit,0)			as 'deposit',
		isnull(disb.disbursement,0)		as 'disbursement',
		isnull(disb.debt,0)			as 'debt',
		dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'name',
		disb.disbursement_date		as 'disbursement_date'
FROM		#t_total_disbursements disb
FULL OUTER JOIN	#t_total_deposits dep ON disb.client = dep.client
LEFT OUTER JOIN	people p WITH (NOLOCK) on p.client = dep.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		dep.deposit <> disb.disbursement
ORDER BY	disb.disbursement_date, dep.client

drop table #t_total_deposits
drop table #t_total_disbursements

return ( @@rowcount )
GO
