USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_transfer_op_op]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_transfer_op_op] ( @SrcAccount AS VarChar(80), @DestAccount AS VarChar(80), @Amount AS Money, @Description AS VarChar(80) = NULL ) AS

-- ===============================================================================================
-- ==            Generate a transfer from one operating account to another                      ==
-- ===============================================================================================

DECLARE	@ErrorMsg	VarChar(128)
DECLARE	@OldBalance	Money

-- Do not generate intermediate result sets
SET NOCOUNT ON

-- Do nothing if the amount is zero
IF @Amount = 0
	Return ( 0 )

-- Ensure that the amount is valid
IF @Amount < 0
BEGIN
	RaisError ( 50019, 16, 1 )
	Return ( 0 )
END

-- Generate a transaction for the operation
BEGIN TRANSACTION

-- Determine the source for client transfers
INSERT INTO registers_non_ar	(tran_type,	credit_amt,	debit_amt,	src_ledger_account,	dst_ledger_account,	message)
VALUES				('DX',		@Amount,	@Amount,	@SrcAccount,		@DestAccount,		@Description)

-- Terminate normally
COMMIT TRANSACTION
RETURN ( 1 )
GO
