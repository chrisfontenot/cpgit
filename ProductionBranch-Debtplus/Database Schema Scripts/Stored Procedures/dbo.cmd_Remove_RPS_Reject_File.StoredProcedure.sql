USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Remove_RPS_Reject_File]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Remove_RPS_Reject_File] ( @response_file as int = null ) as
-- ==============================================================================================
-- ==      Rollback changes to the payments from a RPPS response file that should not have     ==
-- ==      been read into the system. Only payments are changed. Proposals may, at some point  ==
-- ==      be included, but they are harder to put back into a pending state.                  ==
-- ==============================================================================================

-- Ensure the response file is present
if not exists (select * from rpps_response_files where rpps_response_file = @response_file)
begin
	RaisError ('The response file is not in the system.', 16, 1)
	select top 30 * from rpps_response_files order by date_created desc
	return ( 0 )
end

-- Find the payment transactions.
select *
into	#items
from	rpps_response_details d
where	service_class_or_purpose = 'CIE'
and	transaction_code	= 22
and	processing_error is null
and	rpps_response_file	= @response_file

if not exists (select * from #items)
begin
	drop table #items
	RaisError ('The response file does not contain any payment transactions to reverse.', 16, 1)
	return ( 0 )
end

-- Print a bit of information for the history books
declare	@amount		money
declare	@items		int
select	@amount = sum(gross), @items = count(*)
from	#items

print	'Number of payments = ' + convert(varchar, @items)
print	'Gross amount = ' + convert(varchar, @amount, 1)

select	@amount = sum(net) from #items
print	'Net amount = ' + convert(varchar, @amount, 1)

-- Start a transaction group so that things can be rolled back
begin transaction MARK_cmd_Remove_RPS_Reject_File WITH MARK 'Rolling back rpps response file rejects'

-- Process the payment transactions, one at a time, from the selected group
declare	item_cursor cursor for
	select	rpps_transaction
	from	#items

declare	@rpps_transaction	int
open	item_cursor
fetch	item_cursor into @rpps_transaction
while @@fetch_status = 0
begin
	execute	cmd_Remove_RPS_Reject @rpps_transaction = @rpps_transaction
	fetch	item_cursor into @rpps_transaction
end

close		item_cursor
deallocate	item_cursor

-- Delete the items from the response file as if they were never present
delete		rpps_response_details
from		rpps_response_details d
inner join	#items i on i.rpps_response_detail = d.rpps_response_detail

-- If there are no detail items then remove the file
if exists (select * from rpps_response_details where rpps_response_file = @response_file)
	delete
	from	rpps_response_files
	where	rpps_response_file = @response_file

-- Commit the changes to the database
commit transaction MARK_cmd_Remove_RPS_Reject_File

drop table #items
return ( 1 )
GO
