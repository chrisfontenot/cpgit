USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_appointment]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_appointment](@client as int, @office as int = null, @workshop as int = null, @counselor as int = null, @appt_type as int = null, @start_time as datetime = null, @status as varchar(1) = 'P') as
	insert into client_appointments([client],[office],[workshop],[counselor],[appt_type],[start_time],[status]) values (@client,@office,@workshop,@counselor,@appt_type,@start_time,@status)
	return ( scope_identity() )
GO
