SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_insert_banks] ( @description as varchar(50),@default as bit = 0,@ActiveFlag as bit = 1,@type as varchar(1) = 'C',@ContactNameID as int = null,
									@bank_name as varchar(80) = null,@BankAddressID as int = null,
									@aba as varchar(9) = null,@account_number as varchar(40) = null,
									@immediate_origin as varchar(12) = null,@immediate_origin_name as varchar(30) = null,
									@ach_priority as varchar(2) = null,@ach_company_id as varchar(10) = null,@ach_origin_dfi as varchar(12) = null,@ach_enable_offset as bit = 0,@ach_company_identification as varchar(10) = null,@ach_message_authentication as varchar(19) = null,@ach_batch_company_id as varchar(10) = null,
									@immediate_destination as varchar(19) = null,@immediate_destination_name as varchar(30) = null,@checknum as BigInt = 101,
									@max_clients_per_check as int = 20,@max_amt_per_check as money = 0,@batch_number as int = 0,@transaction_number as int = 0,
									@prefix_line as varchar(256) = null,@suffix_line as varchar(256) = null,@output_directory as varchar(256) = null, @check_number as varchar(50) = null, @max_amount_per_check as int = 0 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the banks table                          ==
-- ========================================================================================
	insert into banks ( [description],[default],[ActiveFlag],[type],[ContactNameID],[bank_name],[BankAddressID],[aba],[account_number],[immediate_origin],[immediate_origin_name],[ach_priority],[ach_company_id],[ach_origin_dfi],[ach_enable_offset],[ach_company_identification],[ach_message_authentication],[ach_batch_company_id],[immediate_destination],[immediate_destination_name],[checknum],[max_clients_per_check],[max_amt_per_check],[batch_number],[transaction_number],[prefix_line],[suffix_line],[output_directory] ) VALUES ( @description,@default,@ActiveFlag,@type,@ContactNameID,@bank_name,@BankAddressID,@aba,@account_number,@immediate_origin,@immediate_origin_name,@ach_priority,@ach_company_id,@ach_origin_dfi,@ach_enable_offset,@ach_company_identification,@ach_message_authentication,@ach_batch_company_id,@immediate_destination,@immediate_destination_name,@checknum,@max_clients_per_check,@max_amt_per_check,@batch_number,@transaction_number,@prefix_line,@suffix_line,@output_directory )
	return ( scope_identity() )
GO
