USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_referred_by_type]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_referred_by_type] ( @description as varchar(50), @nfcc as smallint = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the referred_by_types table              ==
-- ========================================================================================
	insert into referred_by_types ( [description], [nfcc] ) values ( @description, @nfcc )
	return ( scope_identity() )
GO
