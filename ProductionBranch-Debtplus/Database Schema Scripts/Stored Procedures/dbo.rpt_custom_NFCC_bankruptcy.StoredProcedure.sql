USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_NFCC_bankruptcy]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_NFCC_bankruptcy] ( @From_Date as datetime = null, @To_date as datetime = null ) as

declare	@avg_income_0		money
declare	@avg_income_1		money

declare	@unsecured_debt_0	money
declare	@unsecured_debt_1	money

declare	@debt_count_0		int
declare	@debt_count_1		int

declare	@delinquent_0		int
declare	@delinquent_1		int

declare	@fico_score_0		int
declare	@fico_score_1		int

declare	@male_0			int
declare	@female_0		int
declare	@male_1			int
declare	@female_1		int

declare	@male_pct_0		float
declare	@female_pct_0		float
declare	@male_pct_1		float
declare	@female_pct_1		float

declare	@fin_problem1_0		int
declare	@fin_problem2_0		int
declare	@fin_problem3_0		int
declare	@fin_problem1_1		int
declare	@fin_problem2_1		int
declare	@fin_problem3_1		int

if @To_Date is null
	select	@To_Date = getdate()

if @From_Date is null
	select	@From_Date = @To_date

select	@From_date	= convert(varchar(10), @From_Date, 101),
	@To_Date	= convert(varchar(10), @To_date, 101) + ' 23:59:59';

-- clients
select	ca.client, apt.bankruptcy
into	#appts
from	client_appointments ca with (nolock)
inner join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
where	ca.start_time between @From_Date and @To_Date
and	ca.status in ('K','W')
and	ca.office is not null
group by ca.client, apt.bankruptcy;

-- gross income
select	a.client, a.bankruptcy, p.gross_income as gross_income
into	#gross_income_1
from	people p with (nolock)
inner join #appts a on a.client = p.client

union all

select	a.client, a.bankruptcy, p.asset_amount as gross_income
from	assets p with (nolock)
inner join #appts a on a.client = p.client;

select	client, bankruptcy, sum(gross_income) as gross_income
into	#gross_income
from	#gross_income_1
group by client, bankruptcy
having sum(gross_income) > 0;

-- unsecured debt
select	a.client, a.bankruptcy, b.orig_balance as unsecured_debt, 1 as unsecured_count
into	#unsecured_debt_1
from	client_creditor cc with (nolock)
inner join #appts a on cc.client = a.client
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where	isnull(ccl.zero_balance,0) = 0
and	b.orig_balance > 0

union all

select	a.client, a.bankruptcy, b.balance as unsecured_debt, 0 as unsecured_count
from	client_other_debts b with (nolock)
inner join #appts a on a.client = b.client
where	isnull(b.balance,0) > 0;

select	client, bankruptcy, sum(unsecured_debt) as unsecured_debt
into	#unsecured_debt
from	#unsecured_debt_1
group by client, bankruptcy;

-- debt count
select	client, bankruptcy, sum(unsecured_count) as unsecured_count
into	#unsecured_count
from	#unsecured_debt_1
group by client, bankruptcy;

-- delinquent mortgages
select	a.bankruptcy, count(l.secured_loan) as delinquent_mortgages
into	#delinquent_mortgage
from	secured_loans l with (nolock)
inner join secured_properties p with (nolock) on p.secured_property = l.secured_property
inner join secured_types t with (nolock) on p.secured_type = t.secured_type
inner join #appts a on p.client = a.client
where	l.past_due_periods > 0
and	t.auto_home_other = 'H'
group by a.bankruptcy;

-- FICO score
select	a.bankruptcy, convert(int, avg(convert(float, p.fico_score))) as fico_score
into	#fico_score
from	people p with (nolock)
inner join #appts a on p.client = a.client
where	isnull(p.fico_score,0) > 0
group by a.bankruptcy;

-- Gender
select	a.bankruptcy, sum(case when p.gender = 1 then 0 else 1 end) as male, sum(case when p.gender = 1 then 1 else 0 end) as female
into	#gender
from	people p with (nolock)
inner join #appts a on p.client = a.client
where	p.gender in (1,2)
group by a.bankruptcy;

-- fin_problem
select a.client, a.bankruptcy, c.cause_fin_problem1 as fin_problem
into	#fin_problem_1
from	clients c with (nolock)
inner join #appts a on c.client = a.client
where	isnull(c.cause_fin_problem1,0) > 0

union all

select a.client, a.bankruptcy, c.cause_fin_problem2 as fin_problem
from	clients c with (nolock)
inner join #appts a on c.client = a.client
where	isnull(c.cause_fin_problem2,0) > 0

union all

select a.client, a.bankruptcy, c.cause_fin_problem3 as fin_problem
from	clients c with (nolock)
inner join #appts a on c.client = a.client
where	isnull(c.cause_fin_problem3,0) > 0

union all

select a.client, a.bankruptcy, c.cause_fin_problem4 as fin_problem
from	clients c with (nolock)
inner join #appts a on c.client = a.client
where	isnull(c.cause_fin_problem4,0) > 0;

select	bankruptcy, fin_problem, count(*) as fin_problem_count
into	#fin_problem
from	#fin_problem_1
group by bankruptcy, fin_problem;

declare	cursor_1 cursor for
	select	fin_problem
	from	#fin_problem
	where	bankruptcy = 0
	and	fin_problem is not null
	order by fin_problem_count desc

open cursor_1
fetch cursor_1 into @fin_problem1_0
if @@fetch_status = 0
	fetch cursor_1 into @fin_problem2_0

if @@fetch_status = 0
	fetch cursor_1 into @fin_problem3_0

close		cursor_1
deallocate	cursor_1

declare	cursor_2 cursor for
	select	fin_problem
	from	#fin_problem
	where	bankruptcy = 1
	and	fin_problem is not null
	order by fin_problem_count desc

open cursor_2
fetch cursor_2 into @fin_problem1_1
if @@fetch_status = 0
	fetch cursor_2 into @fin_problem2_1

if @@fetch_status = 0
	fetch cursor_2 into @fin_problem3_1

close		cursor_2
deallocate	cursor_2

-- Calculate counts
select	@male_0	= male, @female_0 = female
from	#gender
where	bankruptcy = 0;

select	@male_1	= male, @female_1 = female
from	#gender
where	bankruptcy = 1;

if @male_0   is null select @male_0   = 0
if @male_1   is null select @male_1   = 0
if @female_0 is null select @female_0 = 0
if @female_1 is null select @female_1 = 0

select	@male_pct_0	= 0, @male_pct_1	= 0, @female_pct_0	= 0, @female_pct_1	= 0
if @male_0 + @female_0 > 0
	select	@male_pct_0	= round(convert(float, @male_0)   / convert(float, @male_0 + @female_0) * 100.0, 2),
		@female_pct_0	= round(convert(float, @female_0) / convert(float, @male_0 + @female_0) * 100.0, 2)

if @male_1 + @female_1 > 0
	select	@male_pct_1	= round(convert(float, @male_1)   / convert(float, @male_1 + @female_1) * 100.0, 2),
		@female_pct_1	= round(convert(float, @female_1) / convert(float, @male_1 + @female_1) * 100.0, 2)

-- Collect the results
select	@avg_income_0		= avg(gross_income)   from #gross_income   where bankruptcy = 0
select	@avg_income_1		= avg(gross_income)   from #gross_income   where bankruptcy = 1
select	@unsecured_debt_0	= avg(unsecured_debt) from #unsecured_debt where bankruptcy = 0
select	@unsecured_debt_1	= avg(unsecured_debt) from #unsecured_debt where bankruptcy = 1
select	@debt_count_0		= convert(int,avg(convert(float,unsecured_count))) from #unsecured_count where bankruptcy = 0
select	@debt_count_1		= convert(int,avg(convert(float,unsecured_count))) from #unsecured_count where bankruptcy = 1

select	@delinquent_0		= 0, @delinquent_1 = 0
if exists(select * from #delinquent_mortgage where bankruptcy = 0 and delinquent_mortgages > 0) select @delinquent_0 = 1
if exists(select * from #delinquent_mortgage where bankruptcy = 1 and delinquent_mortgages > 0) select @delinquent_1 = 1

select	@fico_score_0		= convert(int,avg(convert(float,fico_score))) from #fico_score where bankruptcy = 0
select	@fico_score_1		= convert(int,avg(convert(float,fico_score))) from #fico_score where bankruptcy = 1

-- Return the results
select	round(@avg_income_0,2)		as 'avg_income_0',
	round(@unsecured_debt_0,2)	as 'unsecured_debt_0',
	isnull(@debt_count_0,0)		as 'debt_count_0',
	@delinquent_0			as 'delinquent_0',
	isnull(@fico_score_0,0)		as 'fico_score_0',
	@male_pct_0			as 'male_0',	@female_pct_0			as 'female_0',
	(select description from financial_problems with (nolock) where financial_problem = @fin_problem1_0) as 'fin_problem1_0',
	(select description from financial_problems with (nolock) where financial_problem = @fin_problem2_0) as 'fin_problem2_0',
	(select description from financial_problems with (nolock) where financial_problem = @fin_problem3_0) as 'fin_problem3_0',

	round(@avg_income_1,2)		as 'avg_income_1',
	round(@unsecured_debt_1,2)	as 'unsecured_debt_1',
	isnull(@debt_count_1,0)		as 'debt_count_1',
	@delinquent_1			as 'delinquent_1',
	isnull(@fico_score_1,0)		as 'fico_score_1',
	@male_pct_1			as 'male_1',
	@female_pct_1			as 'female_1',
	(select description from financial_problems with (nolock) where financial_problem = @fin_problem1_1) as 'fin_problem1_1',
	(select description from financial_problems with (nolock) where financial_problem = @fin_problem2_1) as 'fin_problem2_1',
	(select description from financial_problems with (nolock) where financial_problem = @fin_problem3_1) as 'fin_problem3_1'

-- Discard working tables
drop table	#gross_income
drop table	#unsecured_debt
drop table	#unsecured_debt_1
drop table	#unsecured_count
drop table	#delinquent_mortgage
drop table	#fico_score
drop table	#gender
drop table	#fin_problem
drop table	#fin_problem_1
drop table	#gross_income_1
drop table	#appts

-- Exit
return ( 0 )
GO
