USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ProductionReport_By_Status]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ProductionReport_By_Status] ( @FromDate AS datetime, @ToDate as datetime ) AS
-- =================================================================================
-- ==          Return the information for the appointments by status              ==
-- =================================================================================

-- ChangeLog
--   4/30/2009
--      Updated intermediate table for improved performance

-- Suppress intermediate results
set nocount on

-- Default the dates to the proper values
if @ToDate is null
	select @ToDate = getdate()

if @FromDate is null
	select @FromDate = @ToDate

-- Remove the time values
select	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
select	@ToDate		= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Extract the list of appointments and the relative status for them
select	appt_type, [status] as 'status', convert(int,0) as pending, convert(int,0) as missed, convert(int,0) as rescheduled, convert(int,0) as cancelled, convert(int,0) as kept, convert(int,0) as walkin
into	#t_appt_counts
from	client_appointments ca
where	ca.start_time between @FromDate and @ToDate;

create index ix1_appointment_counts on #t_appt_counts ( [status] );

update  #t_appt_counts set pending = 1     where [status] = 'P';
update  #t_appt_counts set missed = 1      where [status] = 'M';
update  #t_appt_counts set rescheduled = 1 where [status] = 'R';
update  #t_appt_counts set cancelled = 1   where [status] = 'C';
update  #t_appt_counts set kept = 1        where [status] = 'K';
update  #t_appt_counts set walkin = 1      where [status] = 'W';

-- Combine the appointments with the descriptions
select
	isnull(td.appt_name, 'Any Type')		as 'description',
	sum(pending)							as 'pending',
	sum(missed)								as 'missed',
	sum(rescheduled)						as 'rescheduled',
	sum(cancelled)							as 'cancelled',
	sum(kept)								as 'kept',
	sum(walkin)								as 'walkin'
from	#t_appt_counts a
left outer join appt_types td on x.appt_type = td.appt_type
group by	td.appt_name
order by	1;

-- Discard the working table
drop table #t_appt_by_status;

return ( @@rowcount )
GO
