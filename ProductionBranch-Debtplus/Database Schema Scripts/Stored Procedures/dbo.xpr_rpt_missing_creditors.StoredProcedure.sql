USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpt_missing_creditors]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_rpt_missing_creditors] AS
-- ============================================================================================
-- ==              Find the list of missing creditors and account numbers                    ==
-- ============================================================================================

-- ChangeLog
--   12/10/2001
--      Removed the sorting order by client to allow the report to do a local sort

SELECT		c.client											as 'client',
		dbo.format_normal_name(default,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		cc.account_number										as 'account_number',
		cc.client_creditor										as 'client_creditor',
		cc.creditor_name										as 'creditor_name',
		cc.created_by											as 'created_by',
		cc.date_created											as 'date_created'
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_creditor cc WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN	people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		cc.creditor_name IS NOT NULL
AND		cc.creditor IS NULL
AND		c.active_status IN ('PND', 'RDY', 'PRO', 'A', 'AR', 'EX')

return ( @@rowcount )
GO
