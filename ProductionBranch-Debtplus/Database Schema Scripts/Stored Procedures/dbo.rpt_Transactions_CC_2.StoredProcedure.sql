SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Transactions_CC_2] ( @client_creditor as int, @FromDate as datetime = null, @ToDate as datetime = null ) AS

-- =======================================================================================================================
-- ==            List the client/creditor transactions in the date range indicated                                      ==
-- =======================================================================================================================

-- ChangeLog
--   1/5/2002
--     Support for check/eft creditor types
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Remove the time values from the date ranges
if @ToDate is null
	SELECT @ToDate = getdate()

if @FromDate IS NULL
	SELECT @FromDate = '1/1/1900'

SELECT	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Retrieve the information from the view
-- ========================================================================================
-- ==             Debt Transactions in the system                                        ==
-- ========================================================================================

SELECT
		convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',

		case
			when d.tran_type = 'IN'			  then d.tran_type + '/' + rtrim(ltrim(convert(varchar, convert(float, d.fairshare_pct * 100.0))))
			when d.tran_type IN ('AD','BW','MD','CM') and d.creditor_type = 'N' then d.tran_type + '/N'
			when d.tran_type IN ('AD','BW','MD','CM') and d.fairshare_amt = 0 then d.tran_type + '/N'
			when d.tran_type IN ('AD','BW','MD','CM') then d.tran_type + '/' + d.creditor_type + isnull('(' + rtrim(ltrim(convert(varchar, convert(float, d.fairshare_pct * 100.0)))) + ')','')
			else d.tran_type + isnull('/' + d.creditor_type,'')
		end										as 'tran_type',

		cr.creditor_name								as 'creditor_name',
		d.client_creditor									as 'client_creditor',

		case when d.tran_type = 'IN' then null else d.credit_amt end			as 'credit_amt',
		case when d.tran_type = 'IN' then null else d.debit_amt	 end			as 'debit_amt',
		case when d.tran_type = 'IN' then d.credit_amt else null end			as 'interest_amt',

		tr.checknum									as 'checknum',
		convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',
		isnull(d.account_number,cc.account_number)					as 'account_number'

FROM		registers_client_creditor d	WITH (NOLOCK)
LEFT OUTER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
INNER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
LEFT OUTER JOIN client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor

where	d.client_creditor	= @client_creditor
and	d.date_created BETWEEN @FromDate AND @ToDate

order by 1

return ( @@rowcount )
GO
