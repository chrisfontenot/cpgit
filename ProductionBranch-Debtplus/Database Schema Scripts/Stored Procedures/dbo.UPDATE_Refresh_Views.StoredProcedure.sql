USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_Refresh_Views]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[UPDATE_Refresh_Views] ( @TableName as varchar(80) = null ) as
-- =============================================================================================
-- ==        Rebuild the views should the underlying tables be modified                       ==
-- =============================================================================================
set nocount on

declare	@view		varchar(80)
declare	@stmt		varchar(800)

if @TableName is null
begin
	-- Declare cursor
	declare	views cursor FOR
		SELECT	TABLE_NAME
		FROM	information_schema.VIEWS
		ORDER BY TABLE_NAME

	open views
	fetch views into @view
	while @@fetch_status = 0
	begin
		select @stmt = 'execute sp_refreshview ''' + @view + ''';'
		print @stmt
		exec ( @stmt )
		fetch views into @view
	end

	close views
	deallocate views

end else begin

	-- Declare cursor
	declare	table_views cursor FOR
		SELECT DISTINCT name 
		FROM	sys.objects AS so 
		INNER JOIN sys.sql_expression_dependencies AS sed ON so.object_id = sed.referencing_id
		WHERE	so.type = 'V'
		AND		sed.referenced_id = OBJECT_ID(@TableName)

	open table_views
	fetch table_views into @view
	while @@fetch_status = 0
	begin
		select @stmt = 'execute sp_refreshview ''' + @view + ''';'
		print @stmt
		exec ( @stmt )
		fetch table_views into @view
	end

	close table_views
	deallocate table_views
end

return ( 0 )
GO
