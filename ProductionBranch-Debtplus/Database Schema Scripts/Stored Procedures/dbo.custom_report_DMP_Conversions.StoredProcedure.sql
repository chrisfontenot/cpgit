USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[custom_report_DMP_Conversions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[custom_report_DMP_Conversions] ( @From_Date as datetime = null, @To_Date as datetime = null) as
-- ======================================================================================================
-- ==            Client appointment conversion information for St. Louis                               ==
-- ======================================================================================================

-- Default the dates
if @To_Date is null
	select	@To_Date = getdate()

if @From_Date is null
	select	@From_Date = @To_Date

select	@From_Date = convert(datetime, convert(varchar(10), @From_Date, 101) + ' 00:00:00'),
	@To_Date = convert(datetime, convert(varchar(10), @To_Date, 101) + ' 23:59:59')

-- Find the list of all clients who have an appointment in the time period.
select distinct	client,
	convert(int,null)	as counselor,
	convert(int,null)	as office,
	convert(int,0)		as appt_type,
	convert(int,0)		as initial,
	convert(int,0)		as review,
	convert(varchar(10),null) as status,
	convert(int,0)		as active_restart,
	convert(datetime, null) as start_time,
	convert(datetime, null) as dmp_status_date,
	convert(varchar(10),null) as active_status
into	#appointments
from	client_appointments
where	start_time between @From_Date and @To_Date
and	status in ('K','W')

-- Set the dmp status date
update	#appointments
set	dmp_status_date	= c.dmp_status_date,
	active_status	= c.active_status
from	#appointments a
inner join clients c on a.client = c.client

-- Find the appointment information
update	#appointments
set	counselor	= apt.counselor,
	office		= apt.office,
	appt_type	= apt.appt_type,
	status		= apt.result,
	start_time	= apt.start_time
from	#appointments x
inner join client_appointments apt on x.client = apt.client
and	apt.start_time between @From_Date and @To_Date
and	apt.status in ('K','W')

-- Find the appointment information for initial appointments. This overrides the one earlier.
update	#appointments
set	counselor	= apt.counselor,
	office		= apt.office,
	appt_type	= apt.appt_type,
	initial		= t.initial_appt,
	status		= apt.result,
	start_time	= apt.start_time
from	#appointments x
inner join client_appointments apt on x.client = apt.client
inner join appt_types t on apt.appt_type = t.appt_type
and	apt.start_time between @From_Date and @To_Date
and	apt.status in ('K','W')
-- and	t.initial_appt = 1

/*
-- Translate the appointment to initial v.s. not-initial
update	#appointments
set	initial = apt.initial_appt
from	#appointments a
inner join appt_types apt on a.appt_type = apt.appt_type
*/

-- This is a hack. Look for the word "review" in the name for St. Louis.
update	#appointments
set	review = 1
from	#appointments
where	appt_type in (
	select	appt_type
	from	appt_types
	where	appt_name like '%review%'
)

-- Find the active restarts
update	#appointments
set	active_restart = 1
where	client in (
	select	client
	from	clients
	where	restart_date between @From_Date and @To_Date
)

-- Create a table to hold the merged information
create table #appointment_temp (counselor int null, office int null, appt_type int null, clients_counseled int null, appt_clients int null, dmp_clients int null);

-- Find the number of counseled appointments
insert into #appointment_temp (counselor, office, appt_type, clients_counseled, appt_clients, dmp_clients)
select	counselor, office, appt_type, count(*), 0, 0
from	#appointments
where	((initial = 1) or (active_status = 'I' and review = 1))
group by counselor, office, appt_type

-- Find the number of dmp starts
insert into #appointment_temp (counselor, office, appt_type, clients_counseled, appt_clients, dmp_clients)
select	counselor, office, appt_type, 0, count(*), 0
from	#appointments
where	dmp_status_date is not null
and	((initial = 1) or (active_status = 'I' and review = 1))
and	dmp_status_date between @From_Date and @To_Date
and	status = 'DMP'
group by counselor, office, appt_type

-- Ensure that the appointment types will not conflict
update #appointment_temp
set appt_type = 0
where appt_type = -1;

-- Find the dmp conversions strictly by the counselor and office of the client
insert into #appointment_temp (counselor, office, appt_type, clients_counseled, appt_clients, dmp_clients)
select	counselor, office, -1, 0, 0, count(*)
from	clients c with (nolock)
where	dmp_status_date between @from_date and @to_date
group by c.counselor, c.office;

-- Return the information for the report
select counselor, office, appt_type, sum(clients_counseled) as clients_counseled, sum(appt_clients) as appt_clients, sum(dmp_clients) as dmp_clients
into	#appointment_results
from    #appointment_temp
group by counselor, office, appt_type;

-- Return the results once the fields are updated
select	x.counselor				as counselor,
	x.office				as office,
	x.appt_type				as appt_type,
	x.clients_counseled			as clients_counseled,
	x.appt_clients				as appt_clients,
	x.dmp_clients				as dmp_clients,
	coalesce (dbo.format_normal_name(default,cox.first,default,cox.last,default), 'Counselor ' + convert(varchar, x.counselor), '(not specified)') as counselor_name,
	coalesce (o.name, 'Office ' + convert(varchar, x.office), '(not specified)') as office_name,
	case
		when apt.appt_name is not null then apt.appt_name
		when x.appt_type is null then '(not specified)'
		when x.appt_type = -1 then 'All DMPs'
		else 'Type ' + convert(varchar, x.appt_type)
	end					as appt_name,
	apt.contact_type			as contact_type

from	#appointment_results x
left outer join counselors co	with (nolock) on x.counselor = co.counselor
left outer join Names cox with (nolock) on co.NameID = cox.Name
left outer join offices o	with (nolock) on x.office = o.office
left outer join appt_types apt	with (nolock) on x.appt_type = apt.appt_type

order by x.office, x.counselor, x.appt_type

-- Discard working tables
drop table #appointments
drop table #appointment_results
drop table #appointment_temp

return ( @@rowcount )
GO
