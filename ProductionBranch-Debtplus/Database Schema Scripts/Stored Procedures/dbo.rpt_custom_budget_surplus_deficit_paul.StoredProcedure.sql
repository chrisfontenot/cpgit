USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_budget_surplus_deficit_paul]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_budget_surplus_deficit_paul]  ( @From_Date as datetime = null, @To_Date as datetime = NULL, @appt_type as int = NULL ) as

-- Surpress intermediate results
set nocount on

-- Default the dates to the current one if something is missing
if @to_date is null
	select	@to_date	= getdate()

if @from_date is null
	select	@from_date	= @to_date

-- Remove the times from the dates
select	@from_date	= convert(varchar(10), @From_date, 101),
	@to_date	= convert(varchar(10), @To_date, 101) + ' 23:59:59';

-- Retrieve the client information
select	ca.client									as client,
	dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix )	as client_name,
	m.description									as gender,
	ca.start_time									as appt_date_time,
	apt.appt_name									as appt_type_description,
	ca.appt_type									as appt_type,
	c.active_status									as active_status,
	c.active_status_date							as active_status_date,
	convert(int,0)									as budget,
	convert(money,0)								as budget_amount,
	convert(money,0)								as net_income,
	convert(money,0)								as disbursement_factors
into	#budget_surplus_deficit

from	client_appointments ca with (nolock)
inner join clients c WITH (NOLOCK) on ca.client = c.client
inner join people p WITH (NOLOCK) on ca.client = p.client and 1 = p.relation
LEFT OUTER JOIN Names pn WITH (NOLOCK) ON p.NameID = pn.Name
left outer join GenderTypes m WITH (NOLOCK) on p.gender = m.oID
left outer join appt_types apt WITH (NOLOCK) on ca.appt_type = apt.appt_type

where	ca.office is not null
and	ca.workshop is null
and	ca.status in ('K','W')
and	ca.start_time between @From_date and @To_date;

-- IF there is an appointment type then reject all but that one type
if @appt_type is not null
	delete
	from	#budget_surplus_deficit
	where	appt_type <> @appt_type

-- Create an index for the client information
create index temp_ix1_clients on #budget_surplus_deficit ( client );

-- Find the total income for the clients
create table #net_incomes_1 (client int null, net_income money null);
insert into #net_incomes_1 (client, net_income)
select	p.client, p.net_income as net_income
from	people p
where	p.client in (select client from #budget_surplus_deficit)

insert into #net_incomes_1 (client, net_income)
select	a.client, a.asset_amount as net_income
from	assets a
where	a.client in (select client from #budget_surplus_deficit)

select	client, sum(net_income) as net_income
into	#net_incomes
from	#net_incomes_1
group by client;

update	#budget_surplus_deficit
set	net_income	= x.net_income
from	#budget_surplus_deficit i
inner join #net_incomes x on i.client = x.client;

-- Find the budget dates for the clients
select	client, max(date_created) as budget_date
into	#budget_dates
from	budgets b
where	client in (select client from #budget_surplus_deficit)
group by client;

update	#budget_surplus_deficit
set	budget		= b.budget
from	#budget_surplus_deficit i
inner join #budget_dates d on i.client = d.client
inner join budgets b on i.client = b.client and d.budget_date = b.date_created

-- Find the budget expenses once the budgets have been determined
select	budget, sum(suggested_amount) as budget_amount
into	#budget_amounts
from	budget_detail with (nolock)
where	budget in (select budget from #budget_surplus_deficit)
group by budget;

update	#budget_surplus_deficit
set	budget_amount	= x.budget_amount
from	#budget_surplus_deficit i
inner join #budget_amounts x on i.budget = x.budget;

-- Find the disbursement amounts
select	cc.client,
	sum(case when isnull(ccl.zero_balance,1) > 0 then disbursement_factor
					   when b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments > cc.disbursement_factor then cc.disbursement_factor
					   else b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments
	end) as disbursement_factor
into	#disbursements
from	client_creditor cc with (nolock)
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where	cc.reassigned_debt	= 0
and	cc.client in (select client from #budget_surplus_deficit)
group by cc.client;

update	#budget_surplus_deficit
set	disbursement_factors	= x.disbursement_factor
from	#budget_surplus_deficit i
inner join #disbursements x on i.client = x.client;

-- Return the results
select	i.client									as client,
	i.client_name									as client_name,
	i.gender									as gender,
	i.appt_date_time								as appt_date_time,
	i.appt_type_description								as appt_type,
	i.active_status									as active_status,
	i.active_status_date								as active_status_date,
	i.budget_amount,
	i.net_income,
	i.disbursement_factors,
	i.net_income - i.budget_amount - i.disbursement_factors				as deficit_or_surplus
from	#budget_surplus_deficit i
order by 1;

drop table #budget_surplus_deficit
drop table #net_incomes_1
drop table #net_incomes
drop table #budget_dates
drop table #budget_amounts
drop table #disbursements

return ( 1 )
GO
