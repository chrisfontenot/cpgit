USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_client_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_client_info] (@client as INT) AS
-- ========================================================================
-- ==          Fetch the client information for the deposits             ==
-- ========================================================================

SELECT		v.addr1								as 'address1',
			v.addr2								as 'address2',
			v.addr3								as 'address3',
			isnull(c.active_status,'CRE')		as 'active_status',
			isnull(c.personal_checks,0)			as 'personal_checks',
			v.name								as 'name',
			v.coapplicant						as 'name2',
			isnull(c.held_in_trust,0)			as 'held_in_trust',

			-- This is the normal type of deposit for this client
			'MO'								as 'deposit_type'
			
from		view_client_address v with (nolock)
inner join	clients c on v.client = c.client
WHERE	v.client = @client

RETURN ( @@rowcount )
GO
