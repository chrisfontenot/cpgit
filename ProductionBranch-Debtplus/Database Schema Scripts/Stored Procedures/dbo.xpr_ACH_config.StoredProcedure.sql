USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_config]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_ACH_config] ( @bank as int = 0 ) AS

-- ==================================================================================================
-- ==                Retrieve the configuration information used for ACH                           ==
-- ==================================================================================================

-- ChangeLog
--    12/3/2001
--      Added immediate origin entry
--    11/24/2002
--      Added bank id number in preparation for moving data from "ach_config" to "banks".
--    1/31/2003
--      Moved information to the banks table. "ach_config" is now obsolete.
--    12/10/2003
--      Added ach_company_identification and ach_message_authentication to the result set.

-- Suppress intermediate result sets
set nocount on

-- Find the last bank which matches the indicated type

if isnull(@bank,0) = 0
	select	@bank			= bank
	from	banks
	where	type			= 'A'

-- Return the information to the caller.
SELECT	ach_priority			as 'priority_code',
	ach_company_id			as 'company_identification',
	immediate_destination		as 'immediate_destination',
	immediate_destination_name	as 'immediate_destination_name',
	immediate_origin_name		as 'immediate_origin_name',
	immediate_origin		as 'immediate_origin',
	null				as 'reference_code',
	ach_origin_dfi			as 'originating_dfi_identification',
	'PPD'				as 'standard_entry_class',
	'CCCS PYMNT'			as 'company_entry_description',
	prefix_line			as 'prefix',
	suffix_line			as 'suffix',
	batch_number			as 'batch_number',
	transaction_number		as 'transaction_number',

	-- This may be a calculated field. We don't know yet. For now, allow a constant to be generated.
	ach_message_authentication	as 'ach_message_authentication',

	-- We use the standard company id for the batch trailer. If this is different then we can override it here.
	case
		when ach_company_id is null then null
		else right('0000000000000000' + ltrim(rtrim(ach_company_id)), 10)
	end				as 'ach_company_identification'

FROM	banks with (nolock)
where	bank				= @bank
and	type				= 'A'

RETURN ( @@rowcount )
GO
