USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_tickler_delete]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_tickler_delete] ( @tickler as int ) AS

-- =========================================================================================================
-- ==            Delete the tickler event for the counselor                                               ==
-- =========================================================================================================

-- Set the date that the item is deleted. This will still keep the tickler event around for a short time
-- period just should something happen and the agency wants them "back".

set nocount on

update	ticklers
set	date_deleted	= getdate()
where	tickler		= @tickler
and	date_deleted is null

return ( @@rowcount )
GO
