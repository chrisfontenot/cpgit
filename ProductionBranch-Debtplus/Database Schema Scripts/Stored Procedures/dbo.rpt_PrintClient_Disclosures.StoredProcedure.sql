USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_Disclosures]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[rpt_PrintClient_Disclosures](@client as int) as
-- ======================================================================================
-- ==    Return the information needed in the one-click packet disclousre subreport    ==
-- ======================================================================================
	set nocount on
	select @client as client
GO
