USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_statements_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_statements_list] ( @client as int ) as

-- ========================================================================================
-- ==       Return the list of valid client statment batches that are defined            ==
-- ========================================================================================

select	b.client_statement_batch			as 'item_key',
	convert(varchar(50), datename(month, b.period_start) +
	', ' +
	convert(varchar, datepart(yy, b.period_start))) as 'description'

from		client_statement_clients cs with (nolock)
inner join	client_statement_batches b on cs.client_statement_batch = b.client_statement_batch
where		cs.client	= @client
order by	b.period_start desc
GO
