USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_address_merged]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_address_merged] ( @creditor AS typ_creditor, @type AS varchar(10)) AS

-- ===============================================================
-- ==        Retrieve the creditor name and address information ==
-- ==        This is similar to xpr_creditor_addresses except   ==
-- ==        that the result set is only one row of all four    ==
-- ==        address lines                                      ==
-- ===============================================================

SET NOCOUNT ON

-- Testing and "verify database" functions. Handle the NULL condition.
declare	@zipcode		varchar(80)
declare	@addr1			varchar(80)
declare	@addr2			varchar(80)
declare	@addr3			varchar(80)
declare	@addr4			varchar(80)
declare	@addr5			varchar(80)
declare	@addr6			varchar(80)
declare	@addr7			varchar(80)

-- If the creditor is null, use the agency information
if @creditor is null
	select	@addr1		= name,
			@addr2		= address1,
			@addr3		= address2,
			@addr4		= address3,
			@zipcode	= postalcode
	from	view_config_lettter_fields with (nolock)

else begin

	-- Find the address from the creditor address view.
	select	@addr1 = isnull(attn,''),
			@addr2 = upper(addr1),
			@addr3 = upper(addr2),
			@addr4 = upper(addr3),
			@addr5 = upper(addr4),
			@addr6 = upper(addr6),
			@zipcode = postalcode
	from	view_creditor_addresses with (nolock)
	where	creditor		= @creditor
	and		type			= @type

	-- Use the payment address if it is missing and this is not the payment address	
	if @@rowcount = 0 and @type <> 'P'
		select	@addr1 = isnull(attn,''),
				@addr2 = upper(addr1),
				@addr3 = upper(addr2),
				@addr4 = upper(addr3),
				@addr5 = upper(addr4),
				@addr6 = upper(addr6),
				@zipcode = postalcode
		from	view_creditor_addresses with (nolock)
		where	creditor		= @creditor
		and		type			= 'P';
end

-- Return the results
select	@ZipCode					as 'zipcode',
		@addr1						as 'addr1',
		@addr2						as 'addr2',
		@addr3						as 'addr3',
		@addr4						as 'addr4',
		@addr5						as 'addr5',
		@addr6						as 'addr6',
		convert(varchar(80),null)	as 'addr7',
		@creditor					as 'creditor'

RETURN ( 1 )
GO
