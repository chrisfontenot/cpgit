USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_create_special_creditor]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[cmd_create_special_creditor] ( @creditor varchar(10) ) as

-- Ensure that the creditor is a valid name for now.
if @creditor not like '[A-Z][0-9][0-9][0-9][0-9]%' and @creditor not like '[A-Z][A-Z][0-9][0-9][0-9][0-9]%'
begin
	RaisError('Creditor is not a valid ID', 16, 1)
    return
end

-- Find the type and the ID
declare @type	varchar(10)
declare @id		int

if @creditor like '[A-Z][A-Z]%'
begin
	select	@type = substring(@creditor, 1, 2),
			@id   = convert(int, substring(@creditor, 3, 80))
end else begin
	select	@type = substring(@creditor, 1, 1),
			@id   = convert(int, substring(@creditor, 2, 80))
end

-- The type must be valid
if not exists( select * From creditor_types where [type] = @type )
begin
	RaisError('Creditor type is invalid. It must be one of the valid types.', 16, 1)
	return
end

-- Start a transaction
begin transaction

-- The creditor must not have been defined
if exists( select * from creditors where creditor = @creditor )
begin
	Rollback
	RaisError('Creditor name is currently defined and can not be duplicated', 16, 1)
	return
end

-- The ID must not have been used
if exists( select * from creditors where creditor_id = @id )
begin
	Rollback
	RaisError('Creditor ID is currently defined and can not be duplicated', 16, 1)
	return
end

-- Add the creditor to the table
declare @stmt		varchar(800)
select	@stmt = 'set identity_insert creditors on;
				 insert into creditors(creditor, creditor_id, type, creditor_name, creditor_class, voucher_spacing, mail_priority, payment_balance, prohibit_use, full_disclosure, suppress_invoice, proposal_budget_info, proposal_income_info, contrib_cycle, contrib_bill_month, pledge_cycle, pledge_bill_month, lowest_apr_pct, medium_apr_pct, highest_apr_pct, usual_priority, percent_balance, check_payments, check_bank)
				 values (''' + @creditor + ''', ' + convert(varchar,@id) + ', ''' + @type + ''', ''CREDITOR JUST CREATED'', 1, 0, 9, ''B'', 0, 0, 0, 0, 0, ''M'', 1, ''M'', 1, 0, 0, 0, 9, 1, 0, 1);
				 set identity_insert creditors off;'
exec ( @stmt );

-- Reevaluate the identity for the creditors table
exec ( 'dbcc checkident (creditors, reseed, 1)' )
exec ( 'dbcc checkident (creditors)' )

-- Each creditor has at least one of these records.
exec xpr_insert_creditor_contribution_pcts @creditor

declare @creditor_contribution_pct	int
select	@creditor_contribution_pct = [creditor_contribution_pct] from [creditor_contribution_pcts] where [creditor] = @creditor

update	[creditors] set [creditor_contribution_pct] = @creditor_contribution_pct where [creditor] = @creditor
update	[creditor_contribution_pcts] set [date_updated] = getdate() where [creditor_contribution_pct] = @creditor_contribution_pct

commit
return ( 0 )
GO
