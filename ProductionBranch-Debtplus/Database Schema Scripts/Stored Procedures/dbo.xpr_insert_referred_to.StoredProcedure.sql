USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_referred_to]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_referred_to] ( @description as varchar(50), @notes as varchar(50) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the referred_to table                    ==
-- ========================================================================================
	insert into referred_to ( [description], [notes] ) values ( @description, @notes )
	return ( scope_identity() )
GO
