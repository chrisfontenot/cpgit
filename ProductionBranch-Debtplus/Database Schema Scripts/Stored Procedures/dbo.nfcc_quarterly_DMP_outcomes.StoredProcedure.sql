USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_quarterly_DMP_outcomes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nfcc_quarterly_DMP_outcomes] ( @from_date as datetime = null, @to_date as datetime = null )  AS 

-- Supress intermediate results
set nocount on

-- This is the name of the resulting sheet in the spreadsheet output
declare	@SheetName	varchar(80)
select	@SheetName	= 'DMPs and Outcomes'

-- Find last quarter if required
if (@to_date is null) and (@from_date is null)
begin
	select	@from_date	= min(dt) from calendar where y = datepart(year, getdate()) and q = (select q from calendar where dt = convert(varchar(10), getdate(), 101))
	select	@from_date	= dateadd(m, -3, @from_date)
	select	@to_date	= max(dt) from calendar where y = datepart(year, @from_date) and q = (select q from calendar where dt = convert(varchar(10), @from_date, 101))
end

select	@from_date	= convert(varchar(10), @from_date, 101) + ' 00:00:00',
	@to_date	= convert(varchar(10), @to_date, 101)   + ' 23:59:59';

-- Table to hold the results
create table #results ( sheet varchar(80), location varchar(80), [value] varchar(80) null);
declare	@dmp_count	int

-- Number recommended for DMP
select	@dmp_count = count(*)
from	client_appointments ca with (nolock)
inner join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
where	isnull(ca.result,'') = 'DMP'
and	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing = 0
and	apt.bankruptcy = 0;

insert into #results (sheet, location, value) values (@SheetName, 'B8', @dmp_count)

-- Referred to Legal Assistance
select	@dmp_count = count(*)
from	client_appointments ca with (nolock)
inner join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
where	isnull(ca.result,'') = 'RLA'
and	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing = 0
and	apt.bankruptcy = 0;
insert into #results (sheet, location, value) values (@SheetName, 'B9', @dmp_count)

-- Referred to Social Assistance
select	@dmp_count = count(*)
from	client_appointments ca with (nolock)
inner join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
where	isnull(ca.result,'') = 'ROA'
and	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing = 0
and	apt.bankruptcy = 0;
insert into #results (sheet, location, value) values (@SheetName, 'B10', @dmp_count)

-- Financial Counseling Only
select	@dmp_count = count(*)
from	client_appointments ca with (nolock)
inner join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
where	isnull(ca.result,'') = 'FCO'
and	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing = 0
and	apt.bankruptcy = 0;
insert into #results (sheet, location, value) values (@SheetName, 'B11', @dmp_count)

-- Others
select	@dmp_count = count(*)
from	client_appointments ca with (nolock)
inner join appt_types apt with (nolock) on ca.appt_type = apt.appt_type
where	isnull(result,'') not in ('FCO','DMP','RLA','ROA')
and	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing = 0
and	apt.bankruptcy = 0;
insert into #results (sheet, location, value) values (@SheetName, 'B12', @dmp_count)

-- Housing results
select	i.*, 33 as line, ltrim(rtrim(isnull(v.description,''))) + '*' + ltrim(rtrim(isnull(m.hud_9902_section,''))) as section_label
into	#hud_sections
from	hud_interviews i
left outer join Housing_PurposeOfVisitTypes      v on i.interview_type = v.oID
left outer join Housing_AllowedVisitOutcomeTypes m on i.interview_type = m.PurposeOfVisit and i.hud_result = m.Outcome
where	interview_date between @from_date and @to_date;

-- Correct the line numbers to the proper spots
update	#hud_sections set line = 17 where section_label in ('Pre Purchase Homebuyer Counseling*PURC');
update	#hud_sections set line = 18 where section_label in ('Pre Purchase Homebuyer Counseling*MRWN', 'Pre Purchase Homebuyer Counseling*MRAN');
update	#hud_sections set line = 19 where section_label in ('Pre Purchase Homebuyer Counseling*LONG');
update	#hud_sections set line = 20 where section_label in ('Resolving or Preventing Mortgage Deliquency*MBCT');
update	#hud_sections set line = 21 where section_label in ('Resolving or Preventing Mortgage Deliquency*REFI');
update	#hud_sections set line = 22 where section_label in ('Resolving or Preventing Mortgage Deliquency*FORE');
update	#hud_sections set line = 23 where section_label in ('Resolving or Preventing Mortgage Deliquency*SOLD', 'Resolving or Preventing Mortgage Deliquency*PREF');
update	#hud_sections set line = 24 where section_label in ('Resolving or Preventing Mortgage Deliquency*BANK');
update	#hud_sections set line = 25 where section_label in ('Resolving or Preventing Mortgage Deliquency*MTMO','Resolving or Preventing Mortgage Deliquency*SEMT','Resolving or Preventing Mortgage Deliquency*REPA','Resolving or Preventing Mortgage Deliquency*EDIL','Resolving or Preventing Mortgage Deliquency*PCFH','Resolving or Preventing Mortgage Deliquency*DEBT','Resolving or Preventing Mortgage Deliquency*RTLS');
update	#hud_sections set line = 26 where section_label in ('Homeowners help w/Home Maint. and Financial Mgmt.*ONRM');
update	#hud_sections set line = 27 where section_label in ('Homeowners help w/Home Maint. and Financial Mgmt.*HELO','Homeowners help w/Home Maint. and Financial Mgmt.*CNLO','Homeowners help w/Home Maint. and Financial Mgmt.*REFI','Homeowners help w/Home Maint. and Financial Mgmt.*SOLD','Homeowners help w/Home Maint. and Financial Mgmt.*BUDC','Homeowners help w/Home Maint. and Financial Mgmt.*HMCL','Homeowners help w/Home Maint. and Financial Mgmt.*UTBC','Homeowners help w/Home Maint. and Financial Mgmt.*RLAS');
update	#hud_sections set line = 28 where section_label in ('Locating/Securing/Maint. Residence Rental Housing*HSAS','Locating/Securing/Maint. Residence Rental Housing*TEMP','Locating/Securing/Maint. Residence Rental Housing*RRAP','Locating/Securing/Maint. Residence Rental Housing*RCHP','Locating/Securing/Maint. Residence Rental Housing*RLAF','Locating/Securing/Maint. Residence Rental Housing*RLAE','Locating/Securing/Maint. Residence Rental Housing*ALTE','Locating/Securing/Maint. Residence Rental Housing*RCHS','Locating/Securing/Maint. Residence Rental Housing*RESO','Locating/Securing/Maint. Residence Rental Housing*DEBT','Locating/Securing/Maint. Residence Rental Housing*UBCU','Locating/Securing/Maint. Residence Rental Housing*SCDD','Seeking Selter or Services for the Homeless*EMSH','Seeking Selter or Services for the Homeless*TNLH','Seeking Selter or Services for the Homeless*PHWA','Seeking Selter or Services for the Homeless*PHOA','Seeking Selter or Services for the Homeless*RHLE');
update	#hud_sections set line = 29 where section_label in ('Resolving or Preventing Mortgage Deliquency*PREV','Homeowners help w/Home Maint. and Financial Mgmt.*RECL','Locating/Securing/Maint. Residence Rental Housing*CURR','Seeking Selter or Services for the Homeless*RECL');
update	#hud_sections set line = 30 where section_label in ('Resolving or Preventing Mortgage Deliquency*RESS','Homeowners help w/Home Maint. and Financial Mgmt.*ROSA','Locating/Securing/Maint. Residence Rental Housing*ROSS','Seeking Selter or Services for the Homeless*ROSS');
update	#hud_sections set line = 31 where section_label in ('Pre Purchase Homebuyer Counseling*WITH','Resolving or Preventing Mortgage Deliquency*WITH','Homeowners help w/Home Maint. and Financial Mgmt.*WITH','Locating/Securing/Maint. Residence Rental Housing*WITH','Seeking Selter or Services for the Homeless*WITH');
update	#hud_sections set line = 32 where section_label in ('Pre Purchase Homebuyer Counseling*LEAS','Pre Purchase Homebuyer Counseling*NOPR','Pre Purchase Homebuyer Counseling*OTHE','Resolving or Preventing Mortgage Deliquency*OTHE','Homeowners help w/Home Maint. and Financial Mgmt.*OTHE','Locating/Securing/Maint. Residence Rental Housing*OTHE','Seeking Selter or Services for the Homeless*OTHE');
update	#hud_sections set line = 37 where section_label in ('Homeowners help w/Home Maint. and Financial Mgmt.*HOBT');
update	#hud_sections set line = 38 where section_label in ('Homeowners help w/Home Maint. and Financial Mgmt.*HCNM');

insert into #results (sheet, location, value)
select	@SheetName, 'B' + convert(varchar, line), count(*)
from	#hud_sections
group by line;

drop table #hud_sections

-- Starting number of clients
select	@dmp_count = isnull(clients_a,0)
from	[statistics]
where	period_stop = convert(varchar(10), @from_date, 101);

insert into #results (sheet, location, value) values (@SheetName, 'B51', @dmp_count)

-- Number of clients added to the system
select	@dmp_count = count(*)
from	clients
where	client in (
	select	client
	from	client_appointments
	where	office is not null
	and	status in ('K','W')
	and	result = 'DMP'
	and	start_time between @from_date and @to_date
)
insert into #results (sheet, location, value) values (@SheetName, 'B52', @dmp_count)

-- Number of successful dmps
select	@dmp_count	= count(*)
from	clients c
inner join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	d.nfcc in ('SC1', 'SC2')
and	c.drop_date between @from_date and @to_date;

insert into #results (sheet, location, value) values (@SheetName, 'B53', @dmp_count)

-- Number of un-successful dmps
select	@dmp_count	= count(*)
from	clients c
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @from_date and @to_date
and	isnull(d.nfcc,'') not in ('SC1', 'SC2');

insert into #results (sheet, location, value) values (@SheetName, 'B54', @dmp_count)

-- Return the results and exit
select * from #results;
drop table #results;

return ( 0 );
GO
