USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_creation_byAccount]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_debt_creation_byAccount] ( @accountNumber as varchar(80) ) as
begin
	set nocount on
	declare	@acctLen	int
	select	@acctLen	= len(@accountNumber)

	-- ==================================================================================================
	-- ==         Return the possible matches to an account number                                     ==
	-- ==================================================================================================
	select * from view_debt_creditor_list
	where	prohibit_use = 0
	and		creditor_id in
	(
        SELECT creditor FROM creditor_methods m WITH (NOLOCK)
        INNER JOIN rpps_biller_ids ids WITH (NOLOCK) on m.rpps_biller_id = ids.rpps_biller_id
        INNER JOIN banks b WITH (NOLOCK) on m.bank = b.bank
        INNER JOIN rpps_masks mk WITH (NOLOCK) on ids.rpps_biller_id = mk.rpps_biller_id
        WHERE b.type = 'R'
        AND @accountNumber LIKE dbo.map_rpps_masks ( mk.mask )
        AND mk.length = @acctLen
	)
end
GO
