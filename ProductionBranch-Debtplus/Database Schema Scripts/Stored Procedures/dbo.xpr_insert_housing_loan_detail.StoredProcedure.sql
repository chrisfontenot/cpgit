USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_loan_detail]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_housing_loan_detail] ( @LoanTypeCD as int = 0,@FinanceTypeCD as int = 0,@MortgageTypeCD as int = 0,@InterestRate as float = 0,@Hybrid_ARM_Loan as bit = 0,@Option_ARM_Loan as bit = 0,@Interest_Only_Loan as bit = 0,@FHA_VA_Insured_Loan as bit = 0,@Privately_Held_Loan as bit = 0,@Arm_Reset as bit = 0,@Payment as money = 0 ) as
-- ====================================================================================
-- ==            Insert a row into the housing_doan_details table for .NET           ==
-- ====================================================================================
insert into housing_loan_details ( [LoanTypeCD],[FinanceTypeCD],[MortgageTypeCD],[InterestRate],[Hybrid_ARM_Loan],[Option_ARM_Loan],[Interest_Only_Loan],[FHA_VA_Insured_Loan],[Privately_Held_Loan],[Arm_Reset],[Payment] ) VALUES ( @LoanTypeCD,@FinanceTypeCD,@MortgageTypeCD,@InterestRate,@Hybrid_ARM_Loan,@Option_ARM_Loan,@Interest_Only_Loan,@FHA_VA_Insured_Loan,@Privately_Held_Loan,@Arm_Reset,@Payment )
return ( scope_identity() )
GO
