USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_client_retention]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_client_retention] AS
-- =================================================================================
-- ==          Process the daily expiration events                                ==
-- =================================================================================

-- Allocate a transaction for the operation
BEGIN TRANSACTION
SET NOCOUNT ON

-- Determine the cutoff date for this transaction
declare	@today	datetime
select	@today = convert(datetime, convert(varchar(10), getdate(), 101) + ' 23:59:59')

-- Update the retention events to expire items that are based upon a date
update		client_retention_events
set		date_expired = getdate()
from		client_retention_events e
where		expire_type = 3
and		date_expired is null
and		expire_date <= @today;

COMMIT TRANSACTION
RETURN ( 1 )
GO
