USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_creation_byName]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_debt_creation_byName] ( @searchName as varchar(80) ) as
begin
	set nocount on
	
	-- ==================================================================================================
	-- ==         Return the possible matches to a creditor name                                       ==
	-- ==================================================================================================
	select * from view_debt_creditor_list
	where	prohibit_use = 0
	and		creditor in
	(
		select	creditor from creditors where creditor_name like '%' + @searchName + '%' or division like '%' + @searchName + '%'
				union all
				select creditor from creditor_addkeys where additional like '%' + @searchName + '%' and type = 'A'
	)
end
GO
