USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_dfs]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_dfs] ( @rpps_response_file as int = null ) AS
-- =======================================================================================================
-- ==           Obtain the information for the response conditions of error payments                    ==
-- =======================================================================================================
-- ChangeLog

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

select		r.rpps_biller_id			as 'rpps_biller_id',
		r.trace_number				as 'trace_number',
		b.biller_name				as 'creditor_name',
		r.account_number			as 'account_number',
		isnull(r.processing_error, '')		as 'error_code',
		isnull(r.gross,0)			as 'gross_amt'

from		rpps_response_details r		with (nolock)
left outer join rpps_biller_ids b		with (nolock) on r.rpps_biller_id = b.rpps_biller_id

where		r.rpps_response_file		= @rpps_response_file
and		r.service_class_or_purpose	= 'DFS'

order by	1, 2		-- biller id, trace number

return ( @@rowcount )
GO
