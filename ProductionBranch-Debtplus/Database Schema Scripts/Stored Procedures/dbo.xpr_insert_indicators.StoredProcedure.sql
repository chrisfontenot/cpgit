USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_indicators]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_indicators] ( @description as varchar(50), @hud_9902_section as varchar(256) = null, @Hidden as bit = 0 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the indicators table                     ==
-- ========================================================================================
	insert into indicators ( [description], [hud_9902_section], [Hidden] ) values ( @description, @hud_9902_section, @Hidden )
	return ( scope_identity() )
GO
