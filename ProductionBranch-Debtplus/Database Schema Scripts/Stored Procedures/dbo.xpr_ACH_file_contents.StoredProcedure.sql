USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_file_contents]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_ACH_file_contents] ( @FileID AS INT ) AS

-- ====================================================================================================
-- ==            Retrieve the contents of the ACH file                                               ==
-- ====================================================================================================

SELECT	deposit_batch_detail	as 'item_key',
		deposit_batch_id		as 'deposit_batch_id',
		ach_transaction_code	as 'transaction_code',
		client					as 'client',
		amount					as 'amount',
		reference				as 'trace_number',
		ach_authentication_code	as 'authentication_code'
FROM	deposit_batch_details with (nolock)
WHERE	deposit_batch_id = @FileID
AND	ach_transaction_code <> '22'		-- Ignore credit accounts for the time being
ORDER BY 6 -- trace_number

RETURN ( @@rowcount )
GO
