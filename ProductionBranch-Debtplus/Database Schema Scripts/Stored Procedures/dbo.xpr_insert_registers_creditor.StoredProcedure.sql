USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_registers_creditor]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_registers_creditor] ( @tran_type as varchar(10), @creditor as varchar(10), @item_date as datetime = null, @fairshare_pct as float = null, @disbursement_register as int = null, @trust_register as int = null, @invoice_register as int = null, @debit_amt as money = null, @credit_amt as money = null, @message as varchar(50) = null ) as
-- ====================================================================================================
-- ==            Create a transaction in the registers_creditor table for .NET                       ==
-- ====================================================================================================
	insert into registers_creditor (tran_type, creditor, item_date, fairshare_pct, disbursement_register, trust_register, invoice_register, debit_amt, credit_amt, message )
	values ( @tran_type, @creditor, @item_date, @fairshare_pct, @disbursement_register, @trust_register, @invoice_register, @debit_amt, @credit_amt, @message )
	return ( scope_identity() )
GO
