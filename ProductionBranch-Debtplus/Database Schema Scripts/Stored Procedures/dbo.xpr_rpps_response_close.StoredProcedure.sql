USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_close]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_close] ( @rpps_response_file as int ) as

-- ==============================================================================================
-- ==            Do the post processing on the file                                            ==
-- ==============================================================================================

-- ChangeLog
--   4/14/2003
--     Do not erase the transaction pointer if we find an error. It is needed for the reports.
--   5/12/2003
--     Ensure that the trace number is a valid numerical string and not a date reference for old items

/*
-- Attempt to match the trace numbers
update	rpps_response_details
set	rpps_transaction	= t.rpps_transaction
from	rpps_response_details d
inner join rpps_transactions t on d.trace_number between t.trace_number_first and t.trace_number_last
and	t.trace_number_first like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
where	rpps_response_file = @rpps_response_file

-- Mark the transactions as no match where there is no match
update	rpps_response_details
set	processing_error = 'UNKNOWN TRACE NUMBER'
where	rpps_response_file = @rpps_response_file
and	rpps_transaction is null

-- Identify the transactions which have been previoudly processed
update	rpps_response_details
set	processing_error = 'DUPLICATE RESPONSE'
from	rpps_response_details d
left outer join rpps_transactions r on d.rpps_transaction = r.rpps_transaction
where	rpps_response_file = @rpps_response_file
and	d.processing_error is null
and	r.return_code is not null;
*/

-- Do the post processing for the file to keep things "simple".
execute xpr_rpps_response_process_post @rpps_response_file
GO
