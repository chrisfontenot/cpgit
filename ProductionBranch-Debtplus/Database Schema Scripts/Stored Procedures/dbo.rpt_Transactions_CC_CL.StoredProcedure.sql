SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Transactions_CC_CL] ( @client_register as int ) AS

-- =====================================================================================================================
-- ==            Retrieve the transactions for a debt given the client and a date.                                    ==
-- =====================================================================================================================

-- ChangeLog
--   1/5/2002
--     Support for check/eft creditor types
--   6/22/2005
--     Allow for items such as "VD" which have a trust register but no disbursement register to occur on the
--     same day.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Obtain the information about the transaction desired
declare	@trust_register		typ_key
declare	@disbursement_register	typ_key
declare	@client			typ_client
declare	@tran_type		typ_transaction
declare	@date_created		datetime
declare	@from_date		datetime
declare	@to_date		datetime

select	@client			= client,
	@tran_type		= tran_type,
	@trust_register		= trust_register,
	@disbursement_register	= disbursement_register,
	@date_created		= date_created
from	registers_client
where	client_register		= @client_register

if @@rowcount < 1
begin
	raiserror (50089, 16, 1, @client_register)
	return ( 0 )
end

-- Generate the date range for the items to be found
select	@From_Date = convert(datetime, convert(varchar(10), @date_created, 101) + ' 00:00:00')
select	@To_Date   = convert(datetime, convert(varchar(10), @date_created, 101) + ' 23:59:59')

-- Look for specific items that should be in the detail table but have no other linkage but the date/client/tran_type.
-- These would be items such as RPS/EPAY rejects, etc. They don't go through the trust register since the money is not put back into the trust via a deposit. (Maybe it should be done???)
if @tran_type in ('RR', 'ER')
begin

	SELECT		case
				when d.creditor_type = 'N' then d.tran_type + '/N'
				when d.fairshare_amt = 0 then d.tran_type + '/N'
				else d.tran_type + '/' + rtrim(coalesce(d.creditor_type,pct.creditor_type_eft,pct.creditor_type_check,'N')) + isnull('(' + rtrim(ltrim(convert(varchar, convert(float, d.fairshare_pct * 100.0)))) + ')','')
			end										as 'tran_type',

			d.client									as 'client',
			d.creditor									as 'creditor',
			d.client_creditor								as 'client_creditor',
			cr.creditor_name								as 'creditor_name',

			convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
			tr.checknum									as 'checknum',
			convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

			d.credit_amt									as 'credit_amt',
			d.debit_amt									as 'debit_amt',
			d.fairshare_amt									as 'fairshare_amt',
			isnull(d.account_number,cc.account_number)					as 'account_number'

	FROM		registers_client_creditor d	WITH (NOLOCK)
	LEFT OUTER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
	INNER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
	LEFT OUTER JOIN client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
	LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
	WHERE	d.tran_type		= @tran_type
	AND		d.client		= @client
	AND		d.date_created between @From_Date AND @To_Date

	return ( @@rowcount )
END

-- Look for transactions that would be a disbursement
if @disbursement_register is not null
BEGIN
	SELECT		case
				when d.creditor_type = 'N' then d.tran_type + '/N'
				when d.fairshare_amt = 0 then d.tran_type + '/N'
				else d.tran_type + '/' + rtrim(coalesce(d.creditor_type,pct.creditor_type_eft,pct.creditor_type_check,'N')) + isnull('(' + rtrim(ltrim(convert(varchar, convert(float, d.fairshare_pct * 100.0)))) + ')','')
			end										as 'tran_type',

			d.client									as 'client',
			d.creditor									as 'creditor',
			d.client_creditor								as 'client_creditor',
			cr.creditor_name								as 'creditor_name',

			convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
			tr.checknum									as 'checknum',
			convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

			d.credit_amt									as 'credit_amt',
			d.debit_amt									as 'debit_amt',
			d.fairshare_amt									as 'fairshare_amt',
			isnull(d.account_number,cc.account_number)					as 'account_number'

	FROM		registers_client_creditor d	WITH (NOLOCK)
	LEFT OUTER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
	INNER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
	LEFT OUTER JOIN client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
	LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
	WHERE		d.tran_type IN ('AD', 'BW', 'MD', 'CM')
	AND		d.client		= @client
	AND		d.disbursement_register = @disbursement_register

	return ( @@rowcount )
END

-- Look for a deposit or similar item which has a trust_register but is not in the registers_client_creditor
if @tran_type in ('DP', 'CR', 'VR', 'AR') and @trust_register is not null
begin
	SELECT	d.tran_type								as 'tran_type',
		d.client								as 'client',
		dbo.format_client_id ( d.client )					as 'creditor',
		convert(int,null)							as 'client_creditor',
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)	as 'creditor_name',

		convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
		tr.checknum								as 'checknum',
		convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

		d.credit_amt								as 'credit_amt',
		d.debit_amt								as 'debit_amt',
		convert(money,0.0)							as 'fairshare_amt',
		d.message								as 'account_number'

	FROM		registers_client d	WITH (NOLOCK)
	LEFT OUTER JOIN	registers_trust tr	WITH (NOLOCK) ON d.trust_register = tr.trust_register
	LEFT OUTER JOIN	people p		WITH (NOLOCK) on d.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name

	where	d.trust_register = @trust_register
	and	d.client	 = @client
	and	d.tran_type	 = @tran_type

	return ( @@rowcount )
end

-- If there is a trust reigister then look in the registers_detail for a match on the trust register. These would be creditor refunds, etc.
if (@trust_register is not null) and (@tran_type <> 'AD')
begin

	SELECT		case
				when d.creditor_type = 'N' then d.tran_type + '/N'
				when d.fairshare_amt = 0 then d.tran_type + '/N'
				else d.tran_type + '/' + rtrim(coalesce(d.creditor_type,pct.creditor_type_eft,pct.creditor_type_check,'N')) + isnull('(' + rtrim(ltrim(convert(varchar, convert(float, d.fairshare_pct * 100.0)))) + ')','')
			end										as 'tran_type',

			d.client									as 'client',
			d.creditor									as 'creditor',
			d.client_creditor								as 'client_creditor',
			cr.creditor_name								as 'creditor_name',

			convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
			tr.checknum									as 'checknum',
			convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

			d.credit_amt									as 'credit_amt',
			d.debit_amt									as 'debit_amt',
			d.fairshare_amt									as 'fairshare_amt',
			isnull(d.account_number,cc.account_number)					as 'account_number'

	FROM		registers_client_creditor d	WITH (NOLOCK)
	LEFT OUTER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
	INNER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
	LEFT OUTER JOIN client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
	LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
	WHERE		d.client		= @client
	AND		d.trust_register	= @trust_register
	and		d.tran_type		= @tran_type

	return ( @@rowcount )
END

-- If there is a trust reigister then look in the registers_detail for a match on the trust register. These are disbursements without a disbursement register??
if @trust_register is not null and @tran_type = 'AD'
begin

	SELECT		case
				when d.creditor_type = 'N' then d.tran_type + '/N'
				when d.fairshare_amt = 0 then d.tran_type + '/N'
				else d.tran_type + '/' + rtrim(coalesce(d.creditor_type,pct.creditor_type_eft,pct.creditor_type_check,'N')) + isnull('(' + rtrim(ltrim(convert(varchar, convert(float, d.fairshare_pct * 100.0)))) + ')','')
			end										as 'tran_type',

			d.client									as 'client',
			d.creditor									as 'creditor',
			d.client_creditor							as 'client_creditor',
			cr.creditor_name								as 'creditor_name',

			convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
			tr.checknum									as 'checknum',
			convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

			d.credit_amt									as 'credit_amt',
			d.debit_amt									as 'debit_amt',
			d.fairshare_amt									as 'fairshare_amt',
			isnull(d.account_number,cc.account_number)					as 'account_number'

	FROM		registers_client_creditor d	WITH (NOLOCK)
	LEFT OUTER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
	INNER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
	LEFT OUTER JOIN client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
	LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
	WHERE		d.client		= @client
	AND		d.trust_register	= @trust_register
	AND		d.tran_type		in ('AD','BW')

	return ( @@rowcount )
END

-- Finally, give up and look only at the client register. This has no other pointers. It would be items such as client-client or client-operating transfers, etc.
SELECT	d.tran_type								as 'tran_type',
	d.client								as 'client',
	dbo.format_client_id ( d.client )					as 'creditor',
	convert(int,null)							as 'client_creditor',
	dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)	as 'creditor_name',

	convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
	tr.checknum								as 'checknum',
	convert (datetime, convert(varchar(10), tr.reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

	d.credit_amt								as 'credit_amt',
	d.debit_amt								as 'debit_amt',
	convert(money,0.0)							as 'fairshare_amt',
	d.message								as 'account_number'

FROM		registers_client d	WITH (NOLOCK)
LEFT OUTER JOIN	registers_trust tr	WITH (NOLOCK) ON d.trust_register = tr.trust_register
LEFT OUTER JOIN	people p		WITH (NOLOCK) on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
where	d.client = @client
and	d.date_created between @from_date and @to_date

return ( @@rowcount )
GO
