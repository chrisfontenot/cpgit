USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_StudentLoanAnswer]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_StudentLoanAnswer]( @StudentLoan int, @QuestionID varchar(40), @AnswerID varchar(40) ) as
insert into StudentLoanAnswers ( StudentLoan, QuestionID, AnswerID ) values ( @studentLoan, @QuestionID, @AnswerID )
return ( scope_identity() )
GO
