USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PolicyMatrix_Top30]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PolicyMatrix_Top30] ( @n as int = 30 ) as

-- ==============================================================================================================
-- ==            Select the top 30 disbursed creditors which have SIC codes in the policy matrix database      ==
-- ==============================================================================================================

set nocount on

if @n is null
	select	@n = 30

declare	@stmt	varchar(800)

if @n > 0
	select @stmt = 'select top ' + convert(varchar, @n) + ' creditor from creditors with (nolock) where sic in (select sic from policy_matrix_policies with (nolock)) order by distrib_ytd desc'
else
	select @stmt = 'select creditor from creditors with (nolock) where sic in (select sic from policy_matrix_policies with (nolock)) order by distrib_ytd desc'

exec ( @stmt )
return ( @@rowcount )
GO
