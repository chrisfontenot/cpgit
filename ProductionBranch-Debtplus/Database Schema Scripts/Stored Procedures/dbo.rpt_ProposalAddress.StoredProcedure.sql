USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ProposalAddress]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ProposalAddress] (@client_creditor_proposal as int = null) AS
-- =========================================================================================
-- ==                             Fetch the Client Address information                    ==
-- =========================================================================================
SELECT		upper (
				case when ltrim(rtrim(isnull(n.coapplicant,''))) = '' then 'NONE'
				else n.coapplicant
				end
			) as 'spouse',

			upper ( v.client_name ) as 'name',
			upper ( n.addr1 ) as 'address1',
			upper ( n.addr2 ) as 'address2',
			upper ( n.addr3 ) as 'address3',
			upper ( n.zipcode ) as 'zipcode',
			convert(varchar(10), dbo.format_client_id ( n.client )) as 'client'
FROM		client_creditor_proposals prop
INNER JOIN	view_last_payment v ON prop.client_creditor = v.client_creditor
inner join	view_client_address n on v.client = n.client
WHERE		prop.client_creditor_proposal = @Client_Creditor_Proposal

RETURN ( @@rowcount )
GO
