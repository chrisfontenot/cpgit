IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_proposal_create_batch')
	EXEC ('CREATE PROCEDURE [dbo].[xpr_proposal_create_batch] ( @from_date as datetime = null, @to_date as datetime = null, @batch_label as typ_message = null, @include_pr_std as int = 1, @include_pr_full as int = 1, @include_pr_rpps as int = 1 ) as')
GO
ALTER PROCEDURE [dbo].[xpr_proposal_create_batch] ( @from_date as datetime = null, @to_date as datetime = null, @batch_label as typ_message = null, @include_pr_std as int = 1, @include_pr_full as int = 1, @include_pr_rpps as int = 1 ) as

-- =============================================================================================
-- ==               Generate the batch for proposals                                          ==
-- =============================================================================================

-- Suppress intermediate result sets
set nocount on
begin transaction
set xact_abort on

if @from_date is null
	select	@from_date	= convert(datetime, '1/1/1900')

if @to_date is null
	select	@to_date	= getdate()

select	@to_date	= dateadd(d, 1, convert(datetime, convert(varchar(10), @to_date, 101) + ' 00:00:00')),
		@from_date	= convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00')

-- Build a list of the proposals that we need to process
select	p.client_creditor,
		p.client_creditor_proposal,

		cc.creditor,
		cr.creditor_id,
		cc.client,
		cc.send_bal_verify,

		p.proposal_batch_id,

		convert(int,1) as bank,
		convert(bit,isnull(cr.full_disclosure,0)) as full_disclosure,
		convert(varchar(80),null) as rpps_biller_id,

		cc.account_number,
		convert(int,null) as creditor_method,
		convert(varchar(10), 'X') as [type],
		1 as proposal_type,
		2 as proposal_status

into	#pending_proposals
from	client_creditor_proposals p with (nolock)
inner join client_creditor cc on p.client_creditor = cc.client_creditor
inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
inner join clients c on cc.client = c.client
inner join creditors cr on cc.creditor = cr.creditor

where	isnull(p.proposal_batch_id,-1) = -1
and		p.proposal_status < 2
and		cc.creditor is not null
and		c.active_status in ('A','AR','PRO')
and		cc.account_number is not null
and		cc.reassigned_debt = 0
and		isnull(b.orig_balance,0) + isnull(b.orig_balance_adjustment,0) + isnull(b.total_interest,0) > isnull(b.total_payments,0)
and		p.proposed_date >= @from_date
and		p.proposed_date < @to_date;

-- Creditors with no methods are always accepted by default
update	#pending_proposals
set		proposal_status = 1
from	#pending_proposals x
inner join clients c on x.client = c.client
inner join creditor_methods mt on x.creditor_id = mt.creditor and mt.region in (0, c.region) and mt.type = 'EDI'

-- Update the proposal information for the proposals without a method
update	client_creditor_proposals
set		proposed_balance	= isnull(b.orig_balance,0) + isnull(b.orig_balance_adjustment,0) + isnull(b.total_interest,0) - isnull(b.total_payments,0),
		proposed_amount		= isnull(cc.disbursement_factor,0),
		proposed_start_date	= isnull(cc.start_date, c.start_date),
		bank				= x.bank,
		proposal_status		= 2,
		proposal_status_date	= getdate(),
		proposal_print_date	= getdate(),
		proposal_batch_id	= 0
from	client_creditor_proposals p
inner join client_creditor cc with (nolock) on p.client_creditor = cc.client_creditor
inner join clients c with (nolock) on cc.client = c.client
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance
inner join #pending_proposals x on p.client_creditor_proposal = x.client_creditor_proposal
where	x.proposal_status	= 2

delete
from	#pending_proposals
where	proposal_status		= 2;

-- Default the proposals to "paper" where appropriate
update	#pending_proposals
set		creditor_method	= mt.creditor_method,
		bank		= mt.bank,
		type		= b.type,
		proposal_type	= case full_disclosure when 0 then 1 else 2 end
from	#pending_proposals x
inner join clients c on x.client = c.client
inner join creditor_methods mt on x.creditor_id = mt.creditor and mt.region in (0, c.region) and mt.type = 'EDI'
inner join banks b on mt.bank = b.bank
where	b.type	= 'C'
and		x.type  = 'X'

-- Look for RPPS banks
update	#pending_proposals
set		creditor_method	= mt.creditor_method,
		bank		= mt.bank,
		type		= b.type,
		rpps_biller_id	= mt.rpps_biller_id,
		proposal_type	= 3
from	#pending_proposals x
inner join clients c on x.client = c.client
inner join creditor_methods mt on x.creditor_id = mt.creditor and mt.region in (0, c.region) and mt.type = 'EDI'
inner join banks b on mt.bank = b.bank
inner join rpps_biller_ids ids on mt.rpps_biller_id = ids.rpps_biller_id
inner join rpps_masks m on ids.rpps_biller_id = m.rpps_biller_id
where	b.type	= 'R'
and		ids.biller_type in (4, 10, 11, 12, 13, 14)
and		x.account_number like dbo.map_rpps_masks ( m.mask )
and		x.type  = 'X'

-- Move the proposals that are just invalid to the batch id of -1. This is a magic number and indicates that the
-- proposal is to be listed on the invalid report. Later, when the procedure is run again, the -1 items are assumed
-- to be not in a batch and will be re-scanned for validity.
update	client_creditor_proposals
set		proposal_batch_id = -1
from	client_creditor_proposals p
inner join #pending_proposals x on p.client_creditor_proposal = x.client_creditor_proposal
where	x.type	= 'X'

-- The ones that have no types are items that can not be matched
delete
from	#pending_proposals
where	type	= 'X'

-- Remove the proposals from the batches that are not desired
if @include_pr_rpps = 0
	update	#pending_proposals
	set	type		= 'X'
	where type		= 'R';

if @include_pr_std = 0
	update	#pending_proposals
	set	type		= 'X'
	where type		= 'C'
	and	proposal_type	= 1;

if @include_pr_full = 0
	update	#pending_proposals
	set	type		= 'X'
	where type		= 'C'
	and	proposal_type	= 2;

-- Ensure that if the batch is RPPS that the debt is not marked as needing balance verification first
update	#pending_proposals
set		type		= 'X'
where	type		= 'R'
and		send_bal_verify in (2, 3, 4);

-- Make sure that none of these are still left in the "invalid proposals" batch just because they have not been
-- selected for a batch.
update	client_creditor_proposals
set		proposal_batch_id	= null
from	client_creditor_proposals p
inner join #pending_proposals x on p.client_creditor_proposal = x.client_creditor_proposal
where	p.proposal_batch_id	= -1
and		x.type			= 'X';

-- Discard the ones that we don't want to generate as a batch for this time. It has served it purpose to take them
-- from the list of "invalid proposals" and we do not need to process them further.
delete
from	#pending_proposals
where	type	= 'X';

-- Make the debt start date 5 days from now if it is too short.
declare	@min_start_date		datetime
select	@min_start_date		= dbo.date_only ( dateadd( d, 5, getdate() ));

update	client_creditor_proposals
set		proposed_start_date	= @min_start_date
from	client_creditor_proposals pr
inner join #pending_proposals x on pr.client_creditor_proposal = x.client_creditor_proposal
where	pr.proposed_start_date is null
or		pr.proposed_start_date	< @min_start_date;

-- Generate the various proposal batches
declare	item_cursor cursor for
	select distinct bank, proposal_type
	from	#pending_proposals
	order by 1, 2

declare	@batch_id		int
declare	@new_bank		int
declare	@new_proposal_type	int

open	item_cursor
fetch	item_cursor into @new_bank, @new_proposal_type

while @@fetch_status = 0
begin
	insert into proposal_batch_ids (proposal_type,note,bank) values (@new_proposal_type,@batch_label,@new_bank)
	select @batch_id = SCOPE_IDENTITY()

	update	#pending_proposals
	set		proposal_batch_id = @batch_id
	where	proposal_type	  = @new_proposal_type
	and		bank			  = @new_bank;
	
	fetch	item_cursor into @new_bank, @new_proposal_type
end

close		item_cursor
deallocate	item_cursor

-- Update the proposal records to set the batch id
update	client_creditor_proposals
set		proposal_batch_id	= x.proposal_batch_id,
		bank				= x.bank,
		full_disclosure		= x.full_disclosure,
		rpps_biller_id		= x.rpps_biller_id,
		proposal_status		= 1
from	client_creditor_proposals pr
inner join #pending_proposals x on pr.client_creditor_proposal = x.client_creditor_proposal

-- Discard the working table
drop table #pending_proposals

commit transaction
return ( 1 )
GO
GRANT EXECUTE ON xpr_proposal_create_batch TO PUBLIC AS dbo;
GO
DENY EXECUTE ON xpr_proposal_create_batch TO www_role;
GO
