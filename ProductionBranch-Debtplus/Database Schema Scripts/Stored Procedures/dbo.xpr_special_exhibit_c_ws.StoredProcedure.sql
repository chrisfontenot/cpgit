USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_special_exhibit_c_ws]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_special_exhibit_c_ws] ( @from_date datetime = null, @to_date datetime = null, @special as int = 0 ) as

set nocount on

if @to_date is null
	select @to_date = getdate()

if @from_date is null
	select @from_date = @to_date

select @from_date = convert(varchar(10), @from_date, 101),
	   @to_date = convert(varchar(10), @to_date, 101) + ' 23:59:59'

-- Find the workshop list
create table #workshops (workshop int, hud_9902_section varchar(20), client varchar(20), start_time varchar(10), presenter varchar(50) null, educator_hours varchar(20) null, admin_hours varchar(20) null, lnno int identity(1,1))

insert into #workshops (workshop, hud_9902_section, client, start_time, presenter, educator_hours, admin_hours)
select	w.workshop,
	isnull(ct.hud_9902_section,'6.a') as hud_9902_section,
	dbo.format_client_id(ca.client) as client,
	convert(varchar(10), ca.start_time, 101) as start_time,
	dbo.format_normal_name(default,con.first,default,con.last,default) as presenter,
	convert(varchar,round(convert(float,wt.duration)/60.0,3)) as educator_hours,
	convert(int,0) as admin_hours
from client_appointments ca
inner join workshops w on ca.workshop = w.workshop
inner join counselors co on w.counselor = co.person
left outer join names con on co.NameID = con.name
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types ct on wc.content_type = ct.content_type
where ca.status in ('K','W')
AND   ct.hud_9902_section is not null
order by ca.workshop, ca.client

-- Create a results table with the item results for the workshop
create table #results ( page varchar(80), location varchar(20), value varchar(20));

declare @current_page varchar(80)
declare @current_lnno int
declare @last_workshop int
select  @current_lnno = 13, @last_workshop = -1, @current_page = '3' -- '3. Group WS Attendance Log'

declare @current_workshop int
declare @current_hud_9902_section varchar(20)
declare @current_client varchar(20)
declare @current_start_time varchar(10)
declare @current_presenter varchar(80)
declare @current_educator_hours varchar(20)
declare @current_admin_hours varchar(20)

declare item_cursor cursor for
	select workshop, hud_9902_section, client, start_time, presenter, educator_hours, admin_hours
	from	#workshops
	order by workshop, start_time desc, client

open item_cursor
fetch item_cursor into @current_workshop, @current_hud_9902_section, @current_client, @current_start_time, @current_presenter, @current_educator_hours, @current_admin_hours

while @@fetch_status = 0
begin

	-- Include information only when the workshop changes
	if @last_workshop <> @current_workshop
	begin

		-- skip a blank line
		select @current_lnno = @current_lnno + 1

		insert into #results ( page, location, value )
		select @current_page, 'C' + convert(varchar, @current_lnno), @current_start_time
		union all
		select @current_page, 'D' + convert(varchar, @current_lnno), @current_presenter
		union all
		select @current_page, 'E' + convert(varchar, @current_lnno), @current_educator_hours
		union all
		select @current_page, 'F' + convert(varchar, @current_lnno), @current_admin_hours
	end

	-- Include information common to all items
	insert into #results (page, location, value)
	select @current_page, 'A' + convert(varchar, @current_lnno), @current_hud_9902_section
	union all
	select @current_page, 'B' + convert(varchar, @current_lnno), @current_client

	select	@last_workshop = @current_workshop,
			@current_lnno = @current_lnno + 1

	fetch item_cursor into @current_workshop, @current_hud_9902_section, @current_client, @current_start_time, @current_presenter, @current_educator_hours, @current_admin_hours
end

close item_cursor
deallocate item_cursor

-- Return the results from the selections
select * from #results order by page, location
drop table #results
drop table #workshops

return ( 0 )
GO
