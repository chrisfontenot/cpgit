USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_masks]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_masks](@creditor AS typ_creditor) AS
-- ======================================================================================
-- ==             Retrieve the information needed to validate the creditor account     ==
-- ======================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

SELECT		m.length	AS 'account_length',
		m.mask		AS 'account_mask',
		m.description	as 'description'

FROM		creditors cr		with (nolock)
INNER JOIN	creditor_methods mt	with (nolock) ON cr.creditor_id   = mt.creditor AND mt.type = 'EFT'
INNER JOIN	banks b			with (nolock) ON mt.bank	  = b.bank and 'R' = b.type
INNER JOIN	rpps_biller_ids ids	with (nolock) ON mt.rpps_biller_id = ids.rpps_biller_id
INNER JOIN	rpps_masks m		with (nolock) ON m.rpps_biller_id = ids.rpps_biller_id
WHERE		cr.creditor = @creditor

return ( @@rowcount )
GO
