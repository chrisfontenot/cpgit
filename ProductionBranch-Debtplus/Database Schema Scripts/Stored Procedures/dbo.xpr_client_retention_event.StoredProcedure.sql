USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_retention_event]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_client_retention_event] ( @client_retention_event as int ) AS

-- =================================================================================================================
-- ==            Retrieve the information for a specific event                                                    ==
-- =================================================================================================================

SELECT	client_retention_event as 'item_key',
	event_description,
	event_amount,
	created_by,
	date_created,
	event_message,
	action_date,
	null as last_action_description
FROM	view_retention_client_events
WHERE	client_retention_event=@client_retention_event

return ( @@rowcount )
GO
