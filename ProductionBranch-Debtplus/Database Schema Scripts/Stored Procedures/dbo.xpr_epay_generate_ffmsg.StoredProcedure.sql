USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_generate_ffmsg]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_generate_ffmsg] ( @epay_file as int ) as

-- ==================================================================================================
-- ==            Generate the Free Format message                                                  ==
-- ==================================================================================================

-- ChangeLog
--    4/15/2003
--     Initial version
--   10/1/2003
--     Expanded the proposal_message field to "text" format.

-- Suppress intermediate results
set nocount on

-- Include the FMSG types
insert into epay_transactions (request, client, account_number, client_creditor_proposal, epay_biller_id, epay_file)
select		'4', cc.client, cc.account_number, pr.client_creditor_proposal, t.epay_biller_id, @epay_file

from		epay_transactions t with (nolock)
inner join	client_creditor_proposals pr with (nolock)	on t.client_creditor_proposal = pr.client_creditor_proposal
inner join	client_creditor cc with (nolock)		on pr.client_creditor = cc.client_creditor
inner join	debt_notes dn with (nolock)			on pr.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type

where		t.epay_file			= @epay_file
and		ltrim(rtrim(isnull(convert(varchar(256),dn.note_text),''))) <> ''
and		t.request			= '1';

return ( @@rowcount )
GO
