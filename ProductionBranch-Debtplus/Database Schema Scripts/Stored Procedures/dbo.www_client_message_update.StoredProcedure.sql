USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_message_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_message_update] ( @msgid as int, @msgtext as varchar(256) ) as
-- ===================================================================================
-- ==            Update the message with the text from the user                     ==
-- ===================================================================================
update	client_www_notes
set	message		= @msgtext
where	client_www_note = @msgid
return ( @@rowcount )
GO
