USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_geninfo_client]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_geninfo_client]  ( @client as int ) as
-- =================================================================================
-- ==            Return the people information                                    ==
-- =================================================================================

-- Suppress intermediate results
set nocount on

select distinct
	isnull(dbo.format_address_line_1(ca.house,ca.direction,ca.street,ca.suffix,ca.modifier,ca.modifier_value),'')				as 'address1',
	isnull(ca.address_line_2,'')				as 'address2',
	dbo.format_city_state_zip (ca.city, ca.state, ca.postalcode ) as 'address3',
	dbo.format_TelephoneNumber ( c.HomeTelephoneID )		as 'phone',
	dbo.format_TelephoneNumber ( c.MsgTelephoneID )	as 'message_ph',
	isnull(l.Attribute, 'English')				as 'language',
	isnull(o.name,'')							as 'office',
	isnull(dbo.format_normal_name(default,con.first,default,con.last,default),'') as 'counselor',
	convert(smallint,isnull(c.dependents,0) + (case c.marital_status when 2 then 2 else 1 end))		as 'people',
	isnull(housing_status.description,'')		as 'housing_status',
	isnull(housing_type.description,'')			as 'housing_type',
	isnull(marital_status.description,'')		as 'marital_status',
	isnull(fin.description,'')					as 'financial_problem',

	-- ACH information
	convert(bit,isnull(ach.isActive,0))						as 'ach_active',
	dbo.format_ach_checking_savings ( ach.CheckingSavings ) as 'ach_account',
	isnull(ach.ABA,'')										as 'ach_routing_number',
	isnull(ach.AccountNumber,'')							as 'ach_bank_number',
	isnull(convert(varchar(10), ach.StartDate, 1),'')		as 'ach_start_date',
	isnull(convert(varchar(10), c.first_deposit_date,1),'')	as 'first_deposit_date',
	isnull(convert(varchar(10), c.last_deposit_date,1),'')	as 'last_deposit_date',
	c.last_deposit_amount									as 'last_deposit_amount'

from		clients c		with (nolock)
inner join client_housing h with (nolock) ON c.client = h.client
left outer join client_ach ach with (nolock) on c.client = ach.client
left outer join	offices o		with (nolock) on c.office = o.office
left outer join	counselors co		with (nolock) on c.counselor = co.counselor
left outer join names con               with (nolock) on co.NameID = con.name
left outer join	Housing_StatusTypes housing_status	with (nolock) on h.housing_status = housing_status.oID
left outer join housing_properties p with (nolock) on h.client = p.HousingID and 1 = p.Residency
left outer join HousingTypes housing_type with (nolock) on p.PropertyType = housing_type.oID
left outer join	MaritalTypes marital_status	with (nolock) on c.marital_status = marital_status.oID
left outer join	financial_problems fin	with (nolock) on c.cause_fin_problem1 = fin.financial_problem
left outer join	AttributeTypes l		with (nolock) on c.language = l.oID
left outer join addresses ca with (nolock) on c.addressid = ca.address
where	c.client = @client
return ( @@rowcount )
GO
