USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_tickler_items]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_tickler_items] ( @counselor as int = NULL, @date_effective as datetime = null ) AS
-- ==========================================================================================================
-- ==            List the tickler items for the counselor                                                  ==
-- ==========================================================================================================

set nocount on
if @counselor is not null
	if @counselor <= 0
		select @counselor = null

if @counselor is null
begin
	select	@counselor	= counselor
	from	counselors
	where	person		= suser_sname()

	if @counselor is null
		select @counselor = 0
end

-- Find the effective date
if @date_effective is null
	select	@date_effective = getdate()

--if @date_effective < getdate()
--	select	@date_effective = getdate()

select	@date_effective = convert(datetime, convert(varchar(10), dateadd(d, 1, @date_effective), 101) + ' 00:00:00')

-- Find the items that are pending for this counselor which are effective before today and have not been deleted
select		tickler, date_effective, client, priority, description
from		view_tickler_list with (nolock)
where		counselor = @counselor
and		date_effective < @date_effective
order by	priority, date_effective, client

return ( @@rowcount )
GO
