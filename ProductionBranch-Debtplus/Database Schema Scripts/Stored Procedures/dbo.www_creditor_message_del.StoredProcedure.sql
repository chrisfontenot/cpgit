USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_message_del]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_message_del] ( @msgid as int ) as
-- ===================================================================================
-- ==            Delete the message from the creditor                               ==
-- ===================================================================================

delete
from	creditor_www_notes
where	creditor_www_note = @msgid

return ( @@rowcount )
GO
