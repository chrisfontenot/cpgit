USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_FirstNetStopSuspend]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_FirstNetStopSuspend] ( @BatchID as UniqueIdentifier, @CompanyN as varchar(80) = null, @LastName as varchar(80) = null, @FirstName as varchar(80) = null, @Client as int = null, @MaintDate as datetime = null, @PullDates as varchar(80) = null, @Status as varchar(80) = null, @Amount as money = null, @RestartDate as datetime = null, @RoutingNum as varchar(80) = null, @AccountNum as varchar(80) = null ) as
-- =========================================================================================================
-- ==         Record information for First Net Stop Suspend condition                                     ==
-- =========================================================================================================

-- ChangeLog
--   10/12/2012
--      Added "Stopped By Company" to the list of events/notes.

	set nocount on

	-- Build the note message
	declare @Note  varchar(800)
	select  @Note =  isnull(char(13) + char(10) + 'Last Name: '    + @LastName, '')
				   + isnull(char(13) + char(10) + 'First Name: '   + @FirstName, '')
				   + isnull(char(13) + char(10) + 'Maint Date: '   + convert(varchar(10), @MaintDate, 101), '')
				   + isnull(char(13) + char(10) + 'Pull Dates: '   + @PullDates, '')
				   + isnull(char(13) + char(10) + 'Status: '       + @Status, '')
				   + isnull(char(13) + char(10) + 'Amount: '       + '$' + convert(varchar,@Amount,1), '')
				   + isnull(char(13) + char(10) + 'Restart Date: ' + convert(varchar(10), @RestartDate, 101), '')

	if Len(@Note) > 0
		select @Note = substring(@Note, 3, 800)

	-- Insert the appropriate note
	if @Status = 'Stopped by Customer'
		insert into client_notes (client, type, dont_edit, dont_delete, dont_print, subject, note)
		select client, 1, 1, 1, 0, 'ACH STOPPED PER FIRSTNET', @Note from clients with (nolock) where client = @client

	if @Status = 'Stopped by Company'
		insert into client_notes (client, type, dont_edit, dont_delete, dont_print, subject, note)
		select client, 1, 1, 1, 0, 'ACH STOPPED PER FIRSTNET', @Note from clients with (nolock) where client = @client

	if @Status = 'Suspend'
		insert into client_notes (client, type, dont_edit, dont_delete, dont_print, subject, note)
		select client, 1, 1, 1, 0, 'ACH SUSPENDED PER FIRSTNET' + isnull(' UNTIL ' + convert(varchar(10), @RestartDate, 101),''), @Note from clients with (nolock) where client = @client

	return ( 0 )
GO
