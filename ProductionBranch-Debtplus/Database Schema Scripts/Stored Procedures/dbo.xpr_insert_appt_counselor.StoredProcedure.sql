USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_appt_counselor]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_appt_counselor](@appt_time as int, @counselor as int, @inactive as bit = 0, @inactive_reason as varchar(50) = null) as
	insert into appt_counselors(appt_time,counselor,inactive,inactive_reason) values (@appt_time,@counselor,@inactive,@inactive_reason)
	return (scope_identity())
GO
