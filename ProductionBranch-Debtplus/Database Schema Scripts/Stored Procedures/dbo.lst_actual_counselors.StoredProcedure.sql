USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_actual_counselors]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_actual_counselors] as

-- =========================================================================================================
-- ==             Return a list of the counselors who have the is_counselor flag set                      ==
-- =========================================================================================================

-- ChangeLog
--   03/03/2006 Created

set nocount on
declare	@attribute	int
select	@attribute = oID
from	AttributeTypes
where	[grouping] = 'ROLE'
and		[attribute] = 'COUNSELOR'

if @attribute is not null
	select	co.counselor	as 'coun_num',
			dbo.format_normal_name(cox.prefix,cox.first,cox.middle,cox.last,cox.suffix) as 'coun_name'
	from	counselors co with (nolock)
	inner join names cox on co.nameid = cox.name
	inner join counselor_attributes a on co.counselor = a.counselor
	where	a.attribute = @attribute
	order by 2

return ( @@rowcount )
GO
