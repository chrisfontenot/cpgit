USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_reports]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_insert_reports] ( @label as varchar(50) = null, @description as varchar(50), @menu_name as varchar(255) = null, @filename as varchar(255) = null, @assembly as varchar(50) = null, @type as varchar(10) = 'OT' ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the reports table                        ==
-- ========================================================================================
	insert into reports ( [label], [description], [menu_name], [filename], [assembly], [type] ) values ( @label, @description, @menu_name, @filename, @assembly, @type )
	return ( scope_identity() )
GO
