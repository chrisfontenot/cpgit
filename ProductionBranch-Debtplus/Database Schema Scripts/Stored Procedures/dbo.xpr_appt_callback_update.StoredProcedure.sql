USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_callback_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_callback_update] ( @client_appointment as int, @callback_ph as varchar(30) = null) as

-- =======================================================================================
-- ==            Update the callback telephone number for an appointment                ==
-- =======================================================================================

set nocount on

-- If there is an appointment then process it
if @client_appointment > 0
begin
	-- Remove the leading and trailing blanks (which I add to make other than an empty string)
	if @callback_ph is null
		select	@callback_ph = ''
	select @callback_ph = left(rtrim(ltrim(@callback_ph)), 20)

	-- If there is no number then use null
	if @callback_ph = ''
		select	@callback_ph = null

	-- Update the appointment table with the results
	update	client_appointments
	set	callback_ph		= @callback_ph
	where	client_appointment	= @client_appointment
end

return ( @@rowcount )
GO
