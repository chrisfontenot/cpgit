USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_proposal_proof_cdp]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_proposal_proof_cdp] ( @client_creditor_proposal as int ) AS

-- ================================================================================================================
-- ==            Return the proposal information for the CDP detail                                              ==
-- ================================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   12/4/2006
--     Added min_start_date to ensure that the debts start date is at least 5 days in the future.

-- Fetch the information from the current proposal information
DECLARE	@Client			int
DECLARE	@client_creditor	int
DECLARE	@FirstCounsel		datetime
DECLARE	@Proposed_Start		DateTime

SELECT				@Client		 = cc.client,
				@client_creditor = cc.client_creditor,
				@Proposed_Start	 = isnull(cc.start_date, c.start_date)
FROM				client_creditor_proposals prop WITH (NOLOCK)
INNER JOIN client_creditor cc	WITH (NOLOCK) ON prop.client_creditor = cc.client_creditor
INNER JOIN clients c		WITH (NOLOCK) ON cc.client = c.client
WHERE				prop.client_creditor_proposal = @client_creditor_proposal

declare	@min_start_date		datetime
select	@min_start_date		= dbo.date_only ( dateadd ( d, 5, getdate() ));

if @proposed_start is null
	select	@proposed_start	= @min_start_date

if @proposed_start < @min_start_date
	select	@proposed_start	= @min_start_date

-- Fetch the social security number
DECLARE		@SSN		VarChar(9)
SELECT TOP 1	@SSN = ssn
FROM		people
WHERE		client = @client
AND		ssn IS NOT NULL
ORDER BY	relation

IF @SSN IS NULL
	SELECT @SSN = '000000000'

-- Fetch the information about the current debt
DECLARE	@Proposed_Payment	Money
DECLARE	@Account_Balance	Money
DECLARE	@Account_Number		VarChar(50)

SELECT	@Proposed_Payment	= cc.disbursement_factor,
	@Account_Balance	= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0),
	@Account_Number		= left(cc.account_number,50)
FROM	client_creditor cc
inner join client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
WHERE	cc.client_creditor = @client_creditor
AND	cc.reassigned_debt = 0

-- Default the date of the first counsel (client create date)
SELECT		@FirstCounsel		= date_created
FROM		clients
WHERE		client = @client

-- Compute the total number of creditors for this client
DECLARE	@Creditor_Count		int

SELECT		@Creditor_Count			= COUNT(*)
FROM		client_creditor cc
INNER JOIN	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr ON cc.creditor	= cr.creditor
LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) on cr.creditor_class = ccl.creditor_class
WHERE		cc.client			= @Client
AND		isnull(cc.reassigned_debt,0)	= 0
AND		isnull(ccl.proposal_balance,1) <> 0
AND		((bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest > bal.total_payments) OR (ccl.zero_balance > 0) OR (cc.client_creditor = @Client_Creditor))

IF @Creditor_Count IS NULL
	SELECT @Creditor_Count = 0

-- Find the total consumer debt
DECLARE	@Total_Orig_Balance	Money
DECLARE	@Total_Orig_Balance_Adj	Money
DECLARE	@Total_Interest		Money
DECLARE	@Total_Payments		Money

SELECT		@Total_Orig_Balance		= SUM(isnull(bal.orig_balance,0)),
		@Total_Orig_Balance_Adj		= SUM(isnull(bal.orig_balance_adjustment,0)),
		@Total_Interest			= SUM(isnull(bal.total_interest,0)),
		@Total_Payments			= SUM(isnull(bal.total_payments,0))
FROM		client_creditor cc
INNER JOIN	creditors cr ON cc.creditor = cr.creditor
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) on cr.creditor_class = ccl.creditor_class
WHERE		cc.client			= @client
AND		isnull(ccl.proposal_balance,1) <> 0
AND		isnull(cc.reassigned_debt,0)	= 0
AND		isnull(ccl.zero_balance,0)	= 0

DECLARE	@Total_Debt		Money
SELECT	@Total_Debt = isnull(@Total_Orig_balance + @Total_Orig_balance_Adj + @Total_Interest - @Total_Payments,0)

-- Find the total monthly income
DECLARE	@Total_Income		Money
SELECT	@Total_Income	= sum(coalesce(net_income,gross_income,0))
FROM	people
WHERE	client		= @client

DECLARE	@Total_Assets		money
SELECT	@Total_Assets	= sum(asset_amount)
FROM	assets
WHERE	client		= @client

SELECT	@Total_Income = isnull(@Total_Income,0) + isnull(@Total_Assets,0)

-- Find the total consumer debt payments
DECLARE	@Total_Debt_Payments	Money
SELECT	@Total_Debt_Payments = sum(
	case
		when isnull(ccl.zero_balance,0) = 1 then cc.disbursement_factor
		when isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) > isnull(cc.disbursement_factor,0) then isnull(cc.disbursement_factor,0)
		else isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0)
	end)

FROM	client_creditor cc
inner join client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN creditors cr on cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) on cr.creditor_class = ccl.creditor_class
WHERE	cc.reassigned_debt = 0
AND	isnull(ccl.proposal_balance,1) <> 0
AND	cr.creditor is not null
AND	cc.client = @client

-- Find the consumer living expenses
DECLARE	@Living_Expenses	Money
DECLARE	@Budget_ID		INT

select  @budget_id = dbo.map_client_to_budget ( @client )

IF isnull(@Budget_ID,0) > 0
BEGIN
	SELECT		@Living_Expenses = sum(suggested_amount)
	FROM		budget_detail d
	WHERE		d.budget = @Budget_ID
END

-- Fetch the consumer home phone
DECLARE		@HomePhone	VarChar(10)
select		@HomePhone = dbo.format_TelephoneNumber_BaseOnly(c.HomeTelephoneID)
FROM		clients c
WHERE		c.client = @client

-- Fetch the consumer work phone
DECLARE		@WorkPhone	VarChar(10)
select		@WorkPhone = dbo.format_TelephoneNumber_BaseOnly(p.WorkTelephoneID)
from		people p
WHERE		client = @client
ORDER BY	relation desc

IF @WorkPhone IS NULL
	SELECT @WorkPhone = @HomePhone

IF @HomePhone IS NULL
	SELECT @HomePhone = @WorkPhone

-- Return the information from the request
SELECT	isnull(@client,0)		as client,
	isnull(@ssn,'000000000')	as ssn,
	isnull(@FirstCounsel,0)		as 'first_counsel',
	isnull(@proposed_payment,0)	as 'proposed_payment',
	isnull(@proposed_start,0)	as 'proposed_start',
	isnull(@account_balance,0)	as 'account_balance',
	isnull(@account_number,0)	as 'account_number',
	isnull(@creditor_count,0)	as 'creditor_count',
	isnull(@Total_Debt,0)		as 'total_debt',
	isnull(@total_income,0)		as 'total_income',
	isnull(@total_debt_payments,0)	as 'total_debt_payments',
	isnull(@living_expenses,0)	as 'living_expenses',
	isnull(@HomePhone,'0000000000')	as 'home_phone',
	isnull(@WorkPhone,'0000000000')	as 'work_phone',
	@client_creditor_proposal			as 'client_creditor_proposal'

return ( 1 )
GO
