SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_rpt_disbursement_detail]( @FromDate AS datetime = NULL, @ToDate AS datetime = NULL ) AS
-- =============================================================================================
-- ==               Fetch the information on payments to creditors                            ==
-- =============================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Fetch the information from the database
select		d.creditor		as 'creditor',
		cr.creditor_name	as 'creditor_name',
		cr.division		as 'division',
		d.date_created		as 'check_date',
		t.checknum		as 'checknum',

		d.client		AS 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) AS 'name',
		coalesce(d.account_number,cc.account_number,'MISSING') AS 'account_number',

		isnull(d.debit_amt,0)	AS 'gross',

		case d.creditor_type
			when 'D' then isnull(d.fairshare_amt,0)
			else 0
		end			as 'deducted',

		case d.creditor_type
			when 'B' then isnull(d.fairshare_amt,0)
			else 0
		end			as 'billed'

FROM		registers_client_creditor d	WITH (NOLOCK)
LEFT OUTER JOIN	people p			WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
LEFT OUTER JOIN	registers_trust t		WITH (NOLOCK) ON d.trust_register = t.trust_register
LEFT OUTER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
WHERE		d.date_created BETWEEN @FromDate AND @ToDate
AND		d.tran_type = 'AD'

--ORDER BY	d.creditor, d.date_created, d.client
ORDER BY	cr.type, cr.creditor_id, 1, 4, 6

RETURN ( @@rowcount )
GO
