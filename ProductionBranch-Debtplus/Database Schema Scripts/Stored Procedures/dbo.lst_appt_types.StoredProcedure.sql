USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_appt_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_appt_types] AS

-- ===================================================================================================
-- ==            Retrieve the information about the appointment types                               ==
-- ===================================================================================================

set nocount on

declare	@IsCreditType		int
select	@IsCreditType	= oID
from	AttributeTypes
where	[grouping] is null
and		attribute = 'CREDIT'

if @isCreditType is null
	select @isCreditType = -100

declare	@IsHousingType		int
select	@IsHousingType	= oID
from	AttributeTypes
where	[grouping] is null
and		attribute = 'HOUSING'

if @isHousingType is null
	select @isHousingType = -100

select	t.oID				as 'item_key',
		Description			as 'description',
		duration			as 'duration',
		case when IsHousing.oID is null then 0 else 1 end as 'housing',
		case when IsCredit.oID is null then 0 else 1 end as 'credit',
		[Default]			as 'default',
		ActiveFlag,
		BankruptcyClass		as 'bankruptcy_class'
from	AppointmentTypes t
left outer join AppointmentTypeAttributes IsHousing WITH (NOLOCK) ON t.oID = IsHousing.AppointmentType AND @isHousingType = IsHousing.Attribute
left outer join AppointmentTypeAttributes IsCredit WITH (NOLOCK) ON t.oID = IsCredit.AppointmentType AND @isCreditType = IsCredit.Attribute
ORDER BY 2
GO
