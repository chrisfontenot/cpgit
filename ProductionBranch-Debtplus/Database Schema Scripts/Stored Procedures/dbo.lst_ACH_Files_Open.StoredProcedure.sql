USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_ACH_Files_Open]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_ACH_Files_Open] AS

-- ====================================================================================================
-- ==            List of the files created for ACH                                                   ==
-- ====================================================================================================

-- ChangeLog
--   10/29/2002
--      Corrected to include information needed by the "post batch" function.

SELECT	deposit_batch_id		as 'item_key',
	date_created			as 'date_created',
	ach_pull_date			as 'pull_date',
	ach_effective_date		as 'effective_entry_date'
FROM	deposit_batch_ids with (nolock)
WHERE	date_posted is null
AND	batch_type = 'AC'

ORDER BY date_created DESC

return ( @@rowcount )
GO
