USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_invoice_register_correction]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DAILY_invoice_register_correction] as

-- Temporarily correct a "bug" in the invoice generation routine. It needs to update
-- the invoice_register field in the registers_creditor table. This was corrected in
-- a later build. Until the new build is installed, we can use this daily to catch
-- the invoices generated "today".

select distinct creditor, trust_register, invoice_register
into	#a
from	registers_client_creditor
where	tran_type in ('AD','BW','MD','CM')
and	invoice_register is not null

update	registers_creditor
set	invoice_register = x.invoice_register
from	registers_creditor rc
inner join #a x on x.creditor = rc.creditor and x.trust_register = rc.trust_register
where	rc.invoice_register is null
and	rc.tran_type in ('AD','BW','MD','CM')

drop table #a
GO
