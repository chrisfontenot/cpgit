USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AppointmentsNotConfirmed]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_AppointmentsNotConfirmed] ( @FromDate as datetime, @ToDate as datetime ) AS
-- ===========================================================================================
-- ==                    Fetch the list of unconfirmed appointments                         ==
-- ===========================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT  @FromDate   = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
        @ToDate     = convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- ===========================================================================================
-- ==                    Select the appointments in the range which are not confirmed       ==
-- ==                    but are still marked as "pending"                                  ==
-- ===========================================================================================

SELECT		ap.client_appointment	                	as 'client_appointment',
		ap.date_updated		                	as 'created_date',
		ap.client	                		as 'client_id',

		dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'client_name',

		isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),ap.counselor)            	as 'counselor',
		ap.start_time		                      	as 'time',
		o.name			                	as 'office',
		isnull(t.appt_name,'')	                	as 'appointment_type',
		dbo.format_counselor_name(ap.created_by)	as 'created_by'

FROM		client_appointments ap WITH (NOLOCK)
LEFT OUTER JOIN	people p WITH (NOLOCK) ON ap.client = p.client AND p.relation = 1
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	appt_times tm WITH (NOLOCK) ON ap.appt_time = tm.appt_time
LEFT OUTER JOIN	appt_types t WITH (NOLOCK) ON ap.appt_type = t.appt_type
LEFT OUTER JOIN	offices o WITH (NOLOCK) ON ap.office = o.office
LEFT OUTER JOIN	counselors co WITH (NOLOCK) ON ap.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

WHERE		ap.status = 'P'
AND		confirmation_status = 0
AND		ap.start_time BETWEEN @FromDate AND @ToDate
AND		ap.workshop is null
ORDER BY	7, 6; -- office, start_time

RETURN ( @@rowcount )
GO
