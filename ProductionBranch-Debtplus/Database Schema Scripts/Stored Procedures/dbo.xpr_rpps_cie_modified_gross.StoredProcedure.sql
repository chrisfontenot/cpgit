USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cie_modified_gross]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cie_modified_gross] ( @Creditor AS VarChar(10), @TraceNumber AS VarChar(15), @rpps_batch AS INT, @BillerID AS VarChar(10) = NULL, @AccountNumber AS VarChar(22) = NULL, @rpps_file as int = null) AS

-- ========================================================================================================================
-- ==            Create an entry in the RPPS transaction table to represent the reversal biller for modified gross       ==
-- ========================================================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Disable result sets
SET NOCOUNT ON

declare	@bank		int

if @rpps_file is not null
	select	@bank		= bank
	from	rpps_files with (nolock)
	where	rpps_file	= @rpps_file

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'R'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'R';

if @bank is null
	select	@bank		= 2;

-- If the biller ID is not supplied then complain
IF @BillerID IS NULL
BEGIN
	RaisError (50067, 16, 1, @Creditor)
	Rollback Transaction
	Return ( 0 )
END

-- Insert the operation into the tables
INSERT INTO rpps_transactions	(creditor,	biller_id,	trace_number_first,	trace_number_last,	rpps_batch,	service_class_or_purpose,	transaction_code,	death_date,			bank,	rpps_file)
VALUES				(@Creditor,	@BillerID,	@TraceNumber,		@TraceNumber,		@rpps_batch,	'CIE',				27,			dateadd(d, 180, getdate()),	@bank,	@rpps_file)

-- Return success
RETURN ( 1 )
GO
