USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_ledger_codes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_ledger_codes] AS

-- ===================================================================================================
-- ==                Return the list of deposit reasons for the creditor contributions              ==
-- ===================================================================================================

SELECT	ledger_id		as 'item_key',
	[description]		as 'description'
FROM	ledger_codes WITH ( NOLOCK )

return ( @@rowcount )
GO
