USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Creditor_Statistics]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_Creditor_Statistics] ( @Creditor AS VarChar(10), @force_calculation as int = 0 ) AS

-- =========================================================================================
-- ==              Retrieve the creditor statistics information                           ==
-- =========================================================================================

-- ChangeLog
--   2/6/2002
--     Changed the result set to return 0 rather than null values where needed.
--   2/15/2002
--     Allow for non-active clients to have a balance. Do not count these clients.
--   9/8/2002
--     Added "force_calculation" and the creditor_statistics table to cache the values.
--     The values should be updated daily (or weekly) on a schedule basis. If weekly, correct the
--     period that they "expire" from 3 days to 9 days.
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Disable intermediate result sets
SET NOCOUNT ON

DECLARE	@Client			int
DECLARE	@Last_Client		int
DECLARE	@active_status		varchar(10)
DECLARE	@balance		money
DECLARE	@payment		money

DECLARE	@last_date		datetime
DECLARE	@clients		int
DECLARE	@active_clients		int
DECLARE	@clients_balance	int
DECLARE	@zero_bal_clients	int
DECLARE	@total_disbursement	money
DECLARE	@total_balance		money
DECLARE	@avg_disbursement	money

-- If the information is current in the creditor statistics table then use it.
select	@last_date		= date_created,
		@clients		= clients,
		@active_clients		= active_clients,
		@clients_balance	= clients_balance,
		@zero_bal_clients	= zero_bal_clients,
		@total_disbursement	= total_disbursement,
		@total_balance		= total_balance,
		@avg_disbursement	= avg_disbursement
from	creditor_statistics
where	creditor		= @creditor

if (@last_date is not null) and (@force_calculation = 0)
begin
	if datediff(d, @last_date, getdate()) < 3
	begin
		SELECT	@clients		as 'clients',
			@active_clients		as 'active_clients',
			@clients_balance	as 'clients_balance',
			@zero_bal_clients	as 'zero_bal_clients',
			@total_disbursement	as 'total_disbursement',
			@total_balance		as 'total_balance',
			@avg_disbursement	as 'avg_disbursement'
		return ( 1 )
	end
end

-- Discard the creditor statistics
delete	creditor_statistics
from	creditor_statistics
where	creditor = @creditor

-- Insert the basic creditor statistics for all creditors
insert into creditor_statistics (creditor, avg_disbursement, total_disbursement, total_balance, clients_balance, zero_bal_clients, active_clients, clients)
select	creditor, 0, 0, 0, 0, 0, 0, 0
from	creditors
where	creditor = @creditor;

-- Select the debt information for the current disbursements
create table #creditor_statistics (creditor varchar(10), client int, active_status varchar(10) null, balance money, disbursement_factor money, zero_balance int, client_creditor int, client_creditor_balance int)

-- Retrieve the debt list for every active client on the system
insert into #creditor_statistics (creditor, client, active_status, balance, disbursement_factor, zero_balance, client_creditor, client_creditor_balance)
SELECT		cc.creditor as creditor,
			cc.client as client,
			c.active_status as active_status,
			isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as balance,
			isnull(cc.disbursement_factor,0) as disbursement_factor,
			isnull(ccl.zero_balance,0) as zero_balance,
			cc.client_creditor,
			cc.client_creditor_balance
FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c		WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		c.active_status in ('A', 'AR')
AND			cc.reassigned_debt = 0
and			cc.creditor = @creditor;

-- Correct the balances
update	#creditor_statistics set balance = 0 where zero_balance <> 0;

-- Find the statistics by the creditor/client pair. (Merge duplicate debts together.)
select creditor, client, sum(balance) as balance, 0 as has_balance, count(*) as debt_count, sum(disbursement_factor) as disbursement_factor
into	#creditor_statistics_client_count
from	#creditor_statistics
group by creditor, client;

-- Determine the number of clients who have a balance
update	#creditor_statistics_client_count set has_balance = 1 where balance <> 0;

-- Find the number of clients for each creditor
select	creditor, count(*) as cnt, sum(disbursement_factor) as disbursement_factor, sum(balance) as balance, sum(has_balance) as has_balance, count(*) as debt_count -- , sum(debt_count) as debt_count
into	#creditor_statistics_count
from	#creditor_statistics_client_count
group by creditor;

update	creditor_statistics
set	clients				= x.cnt,
	active_clients		= x.cnt,
	total_disbursement	= x.disbursement_factor,
	total_balance		= x.balance,
	clients_balance		= x.has_balance,
	zero_bal_clients	= x.debt_count - x.has_balance
from	creditor_statistics s
inner join #creditor_statistics_count x on s.creditor = x.creditor;

-- Find the average disbursement information
select	creditor, avg(convert(float,disbursement_factor)) as avg_disbursement
into	#creditor_statistics_avg
from	#creditor_statistics
group by creditor;

update	creditor_statistics
set	avg_disbursement	= convert(money,convert(decimal(10,2),x.avg_disbursement))
from	creditor_statistics s
inner join #creditor_statistics_avg x on s.creditor = x.creditor;

drop table #creditor_statistics_client_count;
drop table #creditor_statistics_count;
drop table #creditor_statistics_avg;
drop table #creditor_statistics;

-- Return the new results
select	clients,
		active_clients,
		clients_balance,
		zero_bal_clients,
		total_disbursement,
		total_balance,
		avg_disbursement
from	creditor_statistics
where	creditor		= @creditor

RETURN ( 1 )
GO
