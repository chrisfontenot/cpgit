SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_housing_arm_v4_group_sessions] as

-- Retrieve the workshop data from the previously created HUD extract table
SELECT hcs_id, [group_session_id],[group_session_counselor_id],[group_session_title],[group_session_date],[group_session_duration],dbo.map_hud_9902_group_session_type([group_session_type]) as group_session_type,dbo.map_hud_9902_grant([group_session_attribute_HUD_Grant]) AS group_session_attribute_hud_grant FROM ##hud_9902_workshops

GO
