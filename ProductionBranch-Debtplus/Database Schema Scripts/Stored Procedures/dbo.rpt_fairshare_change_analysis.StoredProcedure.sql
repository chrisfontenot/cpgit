USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_fairshare_change_analysis]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_fairshare_change_analysis] ( @creditor as typ_creditor, @new_fairshare_check as float, @new_fairshare_eft as float ) as

-- ==========================================================================================
-- ==            Determine the likely change in fairshare with a new percentage            ==
-- ==========================================================================================

-- ChangeLog
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

set nocount on

-- Correct the percentages
select	@new_fairshare_check	= dbo.valid_fairshare_pct ( @new_fairshare_check ),
	@new_fairshare_eft	= dbo.valid_fairshare_pct ( @new_fairshare_eft )

-- ==========================================================================================
-- ==               Make any changes within the last 2 months of disbursements             ==
-- ==========================================================================================

declare	@date_start	datetime
declare	@date_stop	datetime
select	@date_stop  = '8/30/2008'
select	@date_start = '1/1/2008'

-- ==========================================================================================
-- ==               Build the information for the previous month                           ==
-- ==========================================================================================

select	rcc.creditor								as 'creditor',
	rcc.creditor_type							as 'creditor_type',
	convert(varchar(10), rcc.date_created, 121)				as 'date_created',
	sum(rcc.debit_amt)							as 'debit_amt',
	rcc.tran_type								as 'tran_type',
	case rcc.tran_type when 'BW' then 0 else rcc.fairshare_pct end		as 'old_fairshare_check',
	sum(case rcc.tran_type when 'BW' then 0 else rcc.fairshare_amt end)	as 'old_fairshare_amt_check',
	case rcc.tran_type when 'BW' then rcc.fairshare_pct else 0 end		as 'old_fairshare_eft',
	sum(case rcc.tran_type when 'BW' then rcc.fairshare_amt else 0 end)	as 'old_fairshare_amt_eft',

	case rcc.tran_type when 'BW' then 0 else rcc.fairshare_pct end		as 'new_fairshare_check',
	sum(case rcc.tran_type when 'BW' then 0 else rcc.fairshare_amt end)	as 'new_fairshare_amt_check',
	case rcc.tran_type when 'BW' then rcc.fairshare_pct else 0 end		as 'new_fairshare_eft',
	sum(case rcc.tran_type when 'BW' then rcc.fairshare_amt else 0 end)	as 'new_fairshare_amt_eft'

into	#fairshare_update

from	registers_client_creditor rcc
left outer join creditors cr on rcc.creditor = cr.creditor
where	rcc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rcc.void = 0
and	rcc.creditor = @creditor
and	rcc.date_created between @date_start and @date_stop

group by cr.type, cr.creditor_id, rcc.creditor, rcc.creditor_type, rcc.tran_type, rcc.fairshare_pct, convert(varchar(10), rcc.date_created, 121)
order by cr.type, cr.creditor_id, rcc.creditor, rcc.creditor_type, convert(varchar(10), rcc.date_created, 121)

-- Determine the percentage for the existing creditor
declare	@old_fairshare_eft		float
declare	@old_fairshare_check		float
select	@old_fairshare_eft	= isnull(pct.fairshare_pct_eft,0),
	@old_fairshare_check	= isnull(pct.fairshare_pct_check,0)
from	creditors cr with (nolock)
left outer join creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
where	cr.creditor = @creditor

if isnull(@old_fairshare_check,0) < @new_fairshare_check
begin
	-- Update the fairshare percentages with the new values
	update	#fairshare_update
	set	new_fairshare_check		= @new_fairshare_check
	where	convert(datetime, date_created) between @date_start and @date_stop
	and	isnull(new_fairshare_check,0.0) > 0.0
	and	isnull(new_fairshare_check,0.0) < @new_fairshare_check
	and	tran_type != 'BW';


end else begin

	-- Update the fairshare percentages with the new values
	update	#fairshare_update
	set	new_fairshare_check		= @new_fairshare_check
	where	convert(datetime, date_created) between @date_start and @date_stop
	and	isnull(new_fairshare_check,0.0) > 0.0
	and	isnull(new_fairshare_check,0.0) > @new_fairshare_check
	and	tran_type != 'BW';
end

-- Update the EFT amount.
if isnull(@old_fairshare_check,0) < @new_fairshare_check
begin
	update	#fairshare_update
	set	new_fairshare_eft		= @new_fairshare_eft
	where	convert(datetime, date_created) between @date_start and @date_stop
	and	isnull(new_fairshare_eft,0.0) > 0.0
	and	isnull(new_fairshare_eft,0.0) > @new_fairshare_eft
	and	tran_type = 'BW';

end else begin

	update	#fairshare_update
	set	new_fairshare_eft		= @new_fairshare_eft
	where	convert(datetime, date_created) between @date_start and @date_stop
	and	isnull(new_fairshare_eft,0.0) > 0.0
	and	isnull(new_fairshare_eft,0.0) > @new_fairshare_eft
	and	tran_type = 'BW';
end

-- Update the dollar amounts for the fairshare with the new rates
update	#fairshare_update
set	new_fairshare_amt_check		= convert(money, convert(decimal(10,2),isnull(debit_amt,0) * new_fairshare_check)),
	new_fairshare_amt_eft		= convert(money, convert(decimal(10,2),isnull(debit_amt,0) * new_fairshare_eft))
where	convert(datetime, date_created) between @date_start and @date_stop;

-- Retrieve the dollar amounts for the various items
select	creditor, creditor_type, date_created,
	sum(debit_amt) as debit_amt,
	tran_type,
	old_fairshare_check as 'old_fairshare_pct_check',
	sum(old_fairshare_amt_check) as 'old_fairshare_amt_check',
	new_fairshare_check as 'new_fairshare_pct_check',
	sum(new_fairshare_amt_check) as 'new_fairshare_amt_check',
	old_fairshare_eft as 'old_fairshare_pct_eft',
	sum(old_fairshare_amt_eft) as 'old_fairshare_amt_eft',
	new_fairshare_eft as 'new_fairshare_pct_eft',
	sum(new_fairshare_amt_eft) as 'new_fairshare_amt_eft'

into	#fairshare
from	#fairshare_update
group by creditor, creditor_type, date_created, tran_type,
	old_fairshare_check,	new_fairshare_check,
	old_fairshare_eft,	new_fairshare_eft

select x.creditor,cr.creditor_name,x.creditor_type,
convert(datetime, x.date_created + ' 00:00:00') as date_created,x.debit_amt,
case when tran_type = 'BW' then x.old_fairshare_pct_eft else x.old_fairshare_pct_check end as 'old_fairshare_pct',
case when tran_type = 'BW' then x.old_fairshare_amt_eft else x.old_fairshare_amt_check end as 'old_fairshare_amt',
case when tran_type = 'BW' then x.new_fairshare_pct_eft else x.new_fairshare_pct_check end as 'new_fairshare_pct',
case when tran_type = 'BW' then x.new_fairshare_amt_eft else x.new_fairshare_amt_check end as 'new_fairshare_amt'
from #fairshare x
left outer join creditors cr on x.creditor = cr.creditor order by date_created desc;

-- Discard working tables
drop table #fairshare
drop table #fairshare_update

return ( @@rowcount )
GO
