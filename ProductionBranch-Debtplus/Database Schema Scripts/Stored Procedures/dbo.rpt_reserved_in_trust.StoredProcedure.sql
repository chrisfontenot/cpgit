USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_reserved_in_trust]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_reserved_in_trust] ( @counselor as int = 0, @region as int = 0 ) as
-- ======================================================================================================
-- ==            Show a list of the clients who have funds that are marked "reserved"                  ==
-- ======================================================================================================

-- Suppress intermediate results
set nocount on

-- Generate the select statement. It is variable depending upon the input parameters.
declare	@stmt		varchar(8000)

select	@stmt = '
select	c.client,
	dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as name,
	c.held_in_trust,
	c.reserved_in_trust,
	c.reserved_in_trust_cutoff,
	c.deposit_in_trust,
	dbo.format_normal_name(default,cox.first,default,cox.last,default) as counselor,
	convert(varchar(50),null) as region
from	clients c
left outer join people p on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join counselors co on c.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
where	isnull(c.reserved_in_trust,0) > 0
and	c.reserved_in_trust_cutoff is not null
and	c.reserved_in_trust_cutoff > getdate()
'

-- Select based upon a counselor
if isnull(@counselor,0) > 0
	select	@stmt = @stmt + ' and c.counselor = ' + convert(varchar, @counselor)

-- Select based upon a region--if isnull(@region,0) > 0--	select	@stmt = @stmt + ' and c.region = ' + convert(varchar, @region)

-- Include the proper sequence information
select	@stmt = @stmt + ' order by 8,7,1'  -- region, counselor, client

exec ( @stmt )
return ( @@rowcount )
GO
