SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_PrintChecks_Info] ( @TrustRegister AS INT, @bank as int = -1 ) AS

-- ==============================================================================================
-- ==                   Return the information for printing a single check                     ==
-- ==============================================================================================

SET NOCOUNT ON

-- Fetch the check number from the trust account
DECLARE	@checknum	BigInt
if isnull(@bank,-1) <= 0
begin
	select	@bank	= min(bank)
	from	banks with (nolock)
	where	type	= 'C'
end

select	@checknum = checknum
from	banks with (nolock)
where	bank		= @bank

-- Fetch the information for the header of the check
SELECT	isnull(@checknum,101)	as 'checknum',
	t.date_created			as 'date_created',

	case
		when t.creditor IS NOT NULL then t.creditor + '-'
		when t.client IS NOT NULL then dbo.format_client_id ( t.client ) + '-'
		else ''
	end + right('00000000' + convert(varchar, @TrustRegister), 8) as [sequence],

	isnull(t.amount,0)		as 'amount'

FROM	registers_trust t WITH (NOLOCK)
WHERE	t.trust_register = @TrustRegister
AND	((t.creditor IS NOT NULL) OR (t.client IS NOT NULL))
AND	t.amount > 0
AND	isnull(t.cleared,' ') = 'P'

-- Include the address information for the check
EXECUTE	rpt_PrintChecks_Address	@TrustRegister

-- Return success
RETURN ( 1 )
GO
