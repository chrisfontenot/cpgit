USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_sales_policies]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_sales_policies] ( @sales_creditor as int ) as

-- =====================================================================================================
-- ==            Return the policy information for the creditor                                       ==
-- =====================================================================================================
declare	@creditor	varchar(10)
select	@creditor	= creditor
from	sales_creditors
where	sales_creditor	= @sales_creditor

select	pt.policy_matrix_policy_type		as 'policy_type',
	pt.description				as 'policy_description',
	pv.message				as 'policy_message'
from	policy_matrix_policy_types pt with (nolock)
inner join policy_matrix_policies pv with (nolock) on pt.policy_matrix_policy_type = pv.policy_matrix_policy_type
where	pv.creditor		= @creditor
and	ltrim(rtrim(isnull(pv.message,''))) <> ''

order by 1

return ( @@rowcount )
GO
