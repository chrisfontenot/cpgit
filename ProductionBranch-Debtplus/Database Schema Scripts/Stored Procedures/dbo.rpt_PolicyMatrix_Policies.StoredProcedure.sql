USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PolicyMatrix_Policies]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PolicyMatrix_Policies]( @creditor as varchar(10) ) as

-- ==============================================================================================================
-- ==            Retrieve the policy information for the indicated policy matrix item of the creditor          ==
-- ==============================================================================================================

set nocount on
-- Fetch the SIC code from the creditor
declare	@sic_code	varchar(80)
select	@sic_code = sic
from	creditors with (nolock)
where	creditor = @creditor

-- Retrieve the indicated policies
select 		@sic_code as sic,
		p.policy_matrix_policy_type,
		p.changed_date,
		p.item_value,
		p.message,

		t.description

from		policy_matrix_policy_types t with (nolock)
inner join	policy_matrix_policies p with (nolock) on t.policy_matrix_policy_type = p.policy_matrix_policy_type
where		p.creditor = @creditor

union all

-- Merge it with the policies that do not have a value in the creditor records
select 		null as sic,
		t.policy_matrix_policy_type,
		null as changed_date,
		null as item_value,
		null as message,

		t.description

from		policy_matrix_policy_types t with (nolock)
where		policy_matrix_policy_type not in (
			select	policy_matrix_policy_type
			from	policy_matrix_policies a with (nolock)
			where	a.creditor = @creditor
		)

-- Order this by the policy_matrix_policy_type column
order by	2 -- policy_matrix_policy_type

return ( @@rowcount )
GO
