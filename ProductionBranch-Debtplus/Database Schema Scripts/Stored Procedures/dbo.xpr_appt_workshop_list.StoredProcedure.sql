USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_workshop_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_workshop_list] ( @slack_days as int = 0, @client as int = -1 ) as
-- =========================================================================================================
-- ==           Return the information needed to produce the list of workshops for booking                ==
-- =========================================================================================================

-- ChangeLog
--   1/14/2001
--     Initial version
--   1/20/2001
--     Changed to use client_appointments table now that it is in place.
--   06/16/2002
--     Changed the limiting period to today's starting time at midnight. This allows workshops to be booked
--     on a walk-in basis for today.
--   06/02/2011
--     Changed default for slack days to 60.
--   03/08/2012
--     Modified to use the proper field for the counselor ID

set nocount on

-- Find the value for "today". This is the limiting factor for the workshops to allow "today's" workshops to
-- appear in the list of possible workshops to book.
declare	@date_start	datetime
select	@date_start = convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00')

-- Default the value to 60 days if it is not specified. It may still be overriden, but unspecified (or specified as 0) means "60 days".
if @slack_days <= 0
	select	@slack_days = 10

-- Back up the criteria by the specified number of days to allow for post-booking.
if @slack_days > 0
	select	@date_start = dateadd(d, 0 - @slack_days, @date_start)
	
-- Remove the following comment if you want to force the date value
-- select	@date_start = '12/31/2011'
	
create table #answer (	workshop int null,
						start_time datetime null,
						duration int null,
						workshop_location varchar(80) null,
						seats_available int null,
						counselor varchar(80) null,
						[description] varchar(80) null,
						HUD_Grant varchar(80) null,
						HousingFeeAmount money null, 
						booked int null,
						location int null );
						
-- Return the information needed to book workshops
if isnull(@client,-1) >= 0
	insert into #answer ( workshop, start_time, duration, workshop_location, seats_available, counselor, [description], hud_grant, housingfeeamount, booked, location )
	SELECT	w.workshop			as 'workshop',
			w.start_time		as 'start_time',
			t.duration			as 'duration',
			l.name				AS 'workshop_location',
			w.seats_available	as 'seats_available',
			dbo.format_normal_name(default,cox.first,default,cox.last,default) AS 'counselor',
			t.description		as 'description',
			w.HUD_Grant			as 'HUD_Grant',
			w.HousingFeeAmount	as 'HousingFeeAmount',
			isnull((SELECT sum(workshop_people) FROM client_appointments apt WITH (NOLOCK) WHERE apt.client <> @client and apt.workshop=w.workshop and apt.[status] in ('P', 'M', 'K')),0) as 'booked',
			w.workshop_location as 'location'
	FROM		workshops w WITH (NOLOCK)
	LEFT OUTER JOIN counselors co WITH (NOLOCK) ON w.counselorID = co.Counselor
	LEFT OUTER JOIN names cox WITH (NOLOCK) ON co.NameID = cox.name
	LEFT OUTER JOIN workshop_locations l WITH (NOLOCK) ON w.workshop_location = l.workshop_location
	INNER JOIN workshop_types t WITH (NOLOCK) ON w.workshop_type = t.workshop_type
	WHERE		w.start_time >= @date_start
	
else

	insert into #answer ( workshop, start_time, duration, workshop_location, seats_available, counselor, [description], hud_grant, housingfeeamount, booked, location )
	SELECT	w.workshop			as 'workshop',
			w.start_time		as 'start_time',
			t.duration			as 'duration',
			l.name				AS 'workshop_location',
			w.seats_available	as 'seats_available',
			dbo.format_normal_name(default,cox.first,default,cox.last,default) AS 'counselor',
			t.description		as 'description',
			w.HUD_Grant			as 'HUD_Grant',
			w.HousingFeeAmount	as 'HousingFeeAmount',
			isnull((SELECT sum(workshop_people) FROM client_appointments apt WITH (NOLOCK) WHERE apt.workshop=w.workshop and apt.[status] in ('P', 'M', 'K')),0) as 'booked',
			w.workshop_location as 'location'
	FROM		workshops w WITH (NOLOCK)
	LEFT OUTER JOIN counselors co WITH (NOLOCK) ON w.counselorID = co.Counselor
	LEFT OUTER JOIN names cox WITH (NOLOCK) ON co.NameID = cox.name
	LEFT OUTER JOIN workshop_locations l WITH (NOLOCK) ON w.workshop_location=l.workshop_location
	INNER JOIN workshop_types t WITH (NOLOCK) ON w.workshop_type=t.workshop_type
	WHERE		w.start_time >= @date_start

SELECT	workshop,
		start_time,
		duration,		
		workshop_location,
		seats_available,
		counselor,
		description,
		booked,
		convert(int, case
			when booked > seats_available then 0
			else seats_available - booked
		end) as seats_left,
		HUD_Grant,
		HousingFeeAmount,
		location
from		#answer
order by start_time

drop table #answer

return ( @@rowcount )
GO
