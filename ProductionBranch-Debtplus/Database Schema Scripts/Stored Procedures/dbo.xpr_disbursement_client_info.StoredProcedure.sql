USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_client_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_client_info] ( @disbursement_register as int, @client AS INT ) AS

-- ===================================================================================================
-- ==            Return the various informational fields about the client                           ==
-- ===================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Fetch the various other items
SELECT		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)	as 'name',
			dbo.format_Address_Line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value) as 'address1',
			a.address_line_2											as 'address2',
			dbo.format_city_state_zip (a.city, a.state, a.PostalCode)		as 'address3',

			'United States of America'							as 'country',

			dbo.format_TelephoneNumber ( c.HomeTelephoneID )	as 'home_phone',
			dbo.format_TelephoneNumber ( c.MsgTelephoneID )		as 'message_phone',
			dbo.format_TelephoneNumber ( p.WorkTelephoneID )	as 'work_phone',
			''													as 'work_ext',

			isnull(l.Attribute,'English')						as 'language',
			isnull(dbo.format_normal_name(default,con.first,default,con.last,con.suffix),'')	as 'counselor',
			isnull(o.name,'')									as 'office',

			(case
				when c.held_in_trust < dc.reserved_in_trust then 0
				else c.held_in_trust - dc.reserved_in_trust
			end) + isnull(rc.debit_amt,0)						as 'held_in_trust',

			c.last_deposit_date								as 'last_deposit_date',
			c.last_deposit_amount							as 'last_deposit_amount'

FROM		clients c	WITH ( NOLOCK )
left outer join addresses a with (nolock) on c.AddressID = a.Address
inner join	disbursement_clients dc WITH ( NOLOCK ) on c.client = dc.client and dc.disbursement_register = @disbursement_register
left outer join	people p	WITH ( NOLOCK ) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.Nameid = pn.Name
left outer join AttributeTypes l WITH (NOLOCK) ON c.language = l.oID
left outer join	counselors co	WITH ( NOLOCK ) ON c.counselor = co.counselor
left outer join names con       with ( nolock ) on co.NameID = con.name
left outer join offices o	WITH ( NOLOCK ) ON c.office = o.office
left outer join registers_client rc with ( nolock ) on dc.client_register = rc.client_register
WHERE		c.client = @client

-- Append a second result set with the client deposit information
select		datepart(d, deposit_date)	as 'deposit_date',
			deposit_amount			as 'deposit_amount'
from		client_deposits with (nolock)
where		client = @client
and			isnull(one_time,0) = 0

RETURN ( @@rowcount )
GO
