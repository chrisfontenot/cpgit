USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_UpostedDepositDetail]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_UpostedDepositDetail] ( @deposit_batch_id as int ) as

-- =================================================================================================
-- ==            Return the information for the lockbox report                                    ==
-- =================================================================================================

select	d.scanned_client	as scanned_client,
		d.client		as deposit_client,
		d.tran_subtype		as subtype,
		d.ok_to_post		as ok_to_post,
		d.amount		as amount,
		d.item_date		as item_date,
		d.reference		as reference,
		d.message		as message,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name
from	deposit_batch_details d with (nolock)
left outer join people p with (nolock) on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
where	d.deposit_batch_id = @deposit_batch_id
order by deposit_batch_detail

return ( @@rowcount )
GO
