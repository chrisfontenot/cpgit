USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_retention_events]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_retention_events] ( @description as varchar(50), @priority as smallint = 9, @initial_retention_action as int = null, @expire_type as smallint = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the retention_events table               ==
-- ========================================================================================
	insert into retention_events ( [description], [priority], [initial_retention_action], [expire_type] ) values ( @description, @priority, @initial_retention_action, @expire_type )
	return ( scope_identity() )
GO
