USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_start_file]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_ACH_start_file] ( @PullDate AS datetime, @FileName as VarChar(80), @EffectiveDate AS DateTime, @bank as int = null, @Note as varchar(50) = null ) AS

-- ===================================================================================================================
-- ==            Generate the new ACH file                                                                          ==
-- ===================================================================================================================

-- Suppress intermediate results
SET NOCOUNT ON

-- If the bank account is not specified, find the last one with a type of "ACH"
if isnull(@bank,0) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'A'
	and	[default]	= 1

if isnull(@bank,0) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'A'

if @bank is null
	select	@bank = 1

-- This must be an ACH bank account or we can not create it.
if not exists ( select * from banks where type = 'A' and bank = @bank )
begin
	RaisError ('The bank account # %d is not for an ACH transaction', 16, 1, @bank )
	return ( 0 )
end

-- Determine the note to be used for the deposit batch
IF ltrim(rtrim(isnull(@Note,''))) = ''
	select	@note		= 'ACH ' + right(isnull(@FileName,''),46)

-- Create the deposit for the ACH file
declare	@deposit_batch_id	int
execute @deposit_batch_id = xpr_deposit_batch_create @note, 'AC', @bank

-- If there is a file then update the ACH information
if @deposit_batch_id > 0
	update	deposit_batch_ids
	set	ach_pull_date		= @PullDate,
		ach_file		= @FileName,
		ach_effective_date	= @EffectiveDate,
		date_posted		= null
	where	deposit_batch_id	= @deposit_batch_id

return ( @deposit_batch_id )
GO
