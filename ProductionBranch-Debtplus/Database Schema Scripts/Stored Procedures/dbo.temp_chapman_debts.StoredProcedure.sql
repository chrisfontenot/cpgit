USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[temp_chapman_debts]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[temp_chapman_debts] as

-- Extract the debt information
if exists (select * from sysobjects where name = 'chapman_debts' and type = 'U')
    drop table chapman_debts
    
select	cc.client,
	isnull(cc.creditor_name,cr.creditor_name) as creditor_name,
	b.orig_balance as beginning_debt_bal,
	b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments as current_balance,
	b.total_payments as payments,
	b.client_creditor_balance as debt_id
into	chapman_debts
from	client_creditor cc
inner join client_creditor_balances b on cc.client_creditor = b.client_creditor and cc.client_creditor_balance = b.client_creditor_balance
left outer join creditors cr on cc.creditor = cr.creditor

-- Discard the clients not in the last spreadsheet
delete
from	chapman_debts
where	client not in (
	select	client
	from	chapman_clients
)

-- Remove all other "secondary" copies.
update chapman_clients set other_income = 0, marital_status = null, housing_status = null, date_created = null, last_deposit_date = null, drop_date = null, drop_reason = null, program_months = 0, expected_deposit_amount = 0, deposits_received = 0 where relation <> 'Applicant'
GO
