USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CDN]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CDN] ( @rpps_response_file as int ) as

-- ========================================================================================================
-- ==            Process the CDN responses to the proposals                                              ==
-- ========================================================================================================

-- ChangeLog
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress intermediate results
set nocount on

-- Correct the transaction pointers for the source values
update	rpps_response_details_cdm
set	rpps_transaction	= tr.rpps_transaction
from	rpps_response_details_cdm d
inner join rpps_transactions tr on d.trace_number between tr.trace_number_first and tr.trace_number_last and tr.service_class_or_purpose = 'CDN'
where	d.rpps_response_file = @rpps_response_file

-- Build a list of the possible creditors for the transactions.
select		d.rpps_response_detail_cdm, cr.creditor, d.biller_id, d.client_creditor, d.account_number
into		#cdm

from		rpps_response_details_cdm d	with (nolock)
full outer join	rpps_biller_ids ids		with (nolock) on ids.rpps_biller_id = d.biller_id
inner join	creditor_methods cm		with (nolock) on ids.rpps_biller_id = cm.rpps_biller_id
inner join	banks b				with (nolock) on cm.bank = b.bank and 'R' = b.type
inner join	creditors cr			with (nolock) on cm.creditor = cr.creditor_id

where		d.rpps_response_file = @rpps_response_file

-- Set the message text to the creditor based soley upon the biller id.
update	rpps_response_details_cdm
set	creditor	= d.creditor
from	rpps_response_details_cdm m
inner join #cdm d on m.rpps_response_detail_cdm = d.rpps_response_detail_cdm
where	d.account_number = 'MESSAGE'

-- Discard the message codes
delete
from	#cdm
where	account_number	= 'MESSAGE'

-- Try to find the client/creditor records for the account numbers that do not say "MESSAGE"
update	#cdm
set	client_creditor = cc.client_creditor
from	#cdm m
inner join client_creditor cc on m.creditor = cc.creditor and m.account_number = cc.account_number

-- Set the appropriate pointers to the detail transactions
update	rpps_response_details_cdm
set	creditor	= m.creditor,
	client_creditor	= m.client_creditor
from	rpps_response_details_cdm d
inner join #cdm m on d.rpps_response_detail_cdm = m.rpps_response_detail_cdm
where	m.client_creditor is not null;

-- Remove the processed items
delete
from	#cdm
where	client_creditor is not null;

-- Discard the working table
drop table #cdm

return ( 1 )
GO
