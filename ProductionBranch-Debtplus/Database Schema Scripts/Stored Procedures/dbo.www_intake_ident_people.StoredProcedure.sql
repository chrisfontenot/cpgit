USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_ident_people]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_ident_people] ( @intake_client as int, @intake_id as varchar(20), @person as int,
					   @email as varchar(81) = null,
					   @prefix as varchar(11) = null, @first as varchar(41) = null, @middle as varchar(41) = null, @last as varchar(41) = null, @suffix as varchar(11) = null,
					   @former as varchar(41) = null,
					   @ssn as varchar(20) = null,
					   @employer_name as varchar(51) = null, @employer_address1 as varchar(81) = null, @employer_city as varchar(61) = null, @employer_state as int = null, @employer_postalcode as varchar(60) = null,
					   @work_ph as varchar(20) = null, @work_ext as int = null,
					   @other_job as varchar(51) = null) as

-- ==============================================================================================================
-- ==            Start the processing on the new debt information                                              ==
-- ==============================================================================================================

-- Set the proper indicator flags
set nocount	on
set xact_abort	on

if not exists (select	*
		from	intake_clients
		where	intake_client	= @intake_client
		and	intake_id	= @intake_id)
begin
	RaisError ('The client is not in the system', 16, 1, @intake_client)
	return ( 0 )
end

-- Remove leading/trailing blanks from the description.
select	@email			= ltrim(rtrim(@email)),
	@prefix			= ltrim(rtrim(@prefix)),
	@first			= ltrim(rtrim(@first)),
	@middle			= ltrim(rtrim(@middle)),
	@last			= ltrim(rtrim(@last)),
	@suffix			= ltrim(rtrim(@suffix)),
	@former			= ltrim(rtrim(@former)),
	@ssn			= ltrim(rtrim(@ssn)),
	@employer_name		= ltrim(rtrim(@employer_name)),
	@employer_address1	= ltrim(rtrim(@employer_address1)),
	@employer_city		= ltrim(rtrim(@employer_city)),
	@employer_postalcode	= replace(ltrim(rtrim(@employer_postalcode)),'-',''),
	@work_ph		= ltrim(rtrim(@work_ph)),
	@other_job		= ltrim(rtrim(@other_job))

-- Convert the postalcode to a legal value
if @employer_postalcode = '000000000'
	select	@employer_postalcode = ''
else if @employer_postalcode is null
	select	@employer_postalcode = ''
else if @employer_postalcode like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
begin
	if right(@employer_postalcode,4) = '0000'
		select	@employer_postalcode = left(@employer_postalcode,5)
	else
		select	@employer_postalcode = left(@employer_postalcode,5) + '-' + right(@employer_postalcode, 4)
end else if len(@employer_postalcode) > 5
	select	@employer_postalcode = left(@employer_postalcode,5)

-- Translate the other empty items to NULL
if @email = ''
	select @email = null

if @prefix = ''
	select @prefix = null

if @prefix = ''
	select @prefix = null

if @first = ''
	select @first = null

if @middle = ''
	select @middle = null
if len(@middle) = 1
	select @middle = @middle + '.'

if @last = ''
	select @last = null

if @suffix = ''
	select @suffix = null

if @former = ''
	select @former = null

if @ssn = ''
	select @ssn = null

if @employer_name = ''
	select @employer_name = null

if @work_ph = ''
	select @work_ph = null

if @other_job = ''
	select @other_job = null

if @ssn = '000000000'
	select @ssn = null

if @work_ph = '0000000000'
	select @work_ph = null

if @work_ext <= 0
	select @work_ext = null

-- Validate the email address
if @person = 1
begin
	declare @answer int
	select @answer = dbo.valid_email ( @email )
	if @answer in (0, 2)
	begin
		raiserror ('Please use a valid e-mail address. We use it to contact you regarding your information. If you don''t want to give us a value, please call our offices and arrange for an in-person appointment.', 16, 1 )
		return ( 0 )
	end
end

-- Update the client with the information
update				intake_people
set	email			= left(@email,80),
	prefix			= left(@prefix,10),
	[first]			= left(@first,40),
	middle			= left(@middle,40),
	[last]			= left(@last,40),
	suffix			= left(@suffix,10),
	former			= left(@former,40),
	ssn			= left(@ssn,9),
	employer_name		= left(@employer_name,50),
	employer_address1	= left(@employer_address1,80),
	employer_city		= left(@employer_city,60),
	employer_state		= @employer_state,
	employer_postalcode	= @employer_postalcode,
	work_ph			= left(@work_ph,14),
	work_ext		= @work_ext,
	other_job		= left(@other_job,50)

where	intake_client		= @intake_client
and	person			= @person

if @@rowcount < 1
begin
	insert into intake_people (email, prefix, [first], middle, [last], suffix, former, ssn, employer_name,
				   employer_address1, employer_city, employer_state, employer_postalcode,
				   work_ph, work_ext, other_job,
				   intake_client, person)

	values (left(@email,80), left(@prefix,10), left(@first,40), left(@middle,40), left(@last,40), left(@suffix,10),
		left(@former,40), left(@ssn,9), left(@employer_name,50), left(@employer_address1,80), left(@employer_city,60),
		@employer_state, @employer_postalcode, left(@work_ph,14), @work_ext, left(@other_job,50),
		@intake_client, @person)
end

return ( @@rowcount )
GO
