USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CreditorPayments_Checks]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_CreditorPayments_Checks] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ===============================================================================================
-- ==           Create a report which shows the creditor payments in the date range             ==
-- ===============================================================================================

set nocount on

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the list of received checks
SELECT	d.creditor		as 'creditor',
	cr.creditor_name	as 'creditor_name',
	d.credit_amt		as 'check_amount',
	d.message		as 'reference',
	d.date_created		as 'item_date'
FROM	registers_creditor d
LEFT OUTER JOIN creditors cr on d.creditor = cr.creditor
WHERE	d.tran_type = 'RC'
AND	d.date_created BETWEEN @FromDate AND @ToDate

-- Return the number of rows in the result set
RETURN ( @@rowcount )
GO
