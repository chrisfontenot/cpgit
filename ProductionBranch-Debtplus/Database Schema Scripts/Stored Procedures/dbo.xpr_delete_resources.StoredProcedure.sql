USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_resources]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_resources] ( @resource int ) as
-- ========================================================================================
-- ==         Remove a resource reference once it has been created                       ==
-- ========================================================================================

set nocount on

delete
from		resources
where		[resource] = @resource;

return ( @@rowcount )
GO
