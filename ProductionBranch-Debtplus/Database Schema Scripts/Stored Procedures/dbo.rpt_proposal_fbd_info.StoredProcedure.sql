USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_proposal_fbd_info]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_proposal_fbd_info] ( @proposal_record as int ) AS

-- =================================================================================================================
-- ==            Generate the information for the FBD proposal record                                             ==
-- =================================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Suppress intermediate results
set nocount on

-- Start a transaction so that things have a chance of completing in a reasonable time period
BEGIN TRANSACTION

declare @client		int
select	@client = cc.client
from	client_creditor_proposals p
inner join client_creditor cc on p.client_creditor = cc.client_creditor
where	p.client_creditor_proposal = @proposal_record

-- Create the temporary table to hold the information
CREATE TABLE #t_Results (
	category	int,
	item		int,
	value		money null
);

-- Include the budget information
DECLARE		@Budget		INT
select		@budget = dbo.map_client_to_budget ( @client )

INSERT INTO	#t_Results ( category, item, value )
SELECT		1, isnull(cat.rpps_code,11), d.suggested_amount
FROM		budget_detail d WITH (NOLOCK)
LEFT OUTER JOIN	budget_categories cat WITH (NOLOCK) ON d.budget_category = cat.budget_category
WHERE		budget = @Budget

-- Compute the current balances for the cards
DECLARE		@OrigBalance		Money
DECLARE		@OrigBalanceAdj		Money
DECLARE		@TotalInterest		Money
DECLARE		@TotalPayments		Money
DECLARE		@NonDmpPayment		Money
DECLARE		@DmpPayment		Money

SELECT		@OrigBalance		= sum(bal.orig_balance),
		@OrigBalanceAdj		= sum(bal.orig_balance_adjustment),
		@TotalInterest		= sum(bal.total_interest),
		@TotalPayments		= sum(bal.total_payments),
		@NonDmpPayment		= sum(isnull(non_dmp_payment,0)),
		@DmpPayment		= sum(isnull(disbursement_factor,0))
FROM		client_creditor cc
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		cc.creditor IS NOT NULL
AND		cc.reassigned_debt = 0
AND		isnull(ccl.proposal_balance,1) <> 0
AND		cc.client = @client

INSERT INTO	#t_Results ( category, item, value )
VALUES		( 2, 1, isnull(@origBalance,0) + isnull(@OrigBalanceAdj,0) + isnull(@TotalInterest,0) - isnull(@TotalPayments,0) )

INSERT INTO	#t_Results ( category, item, value )
VALUES		( 2, 2, isnull(@DmpPayment, 0) )

-- Include the secured loans
INSERT INTO	#t_Results ( category, item, value )
SELECT		2, 3, sum(l.balance)
from		secured_loans l WITH (NOLOCK)
inner join secured_properties p WITH (NOLOCK) ON l.secured_property = p.secured_property
inner join secured_types t	WITH (NOLOCK) ON p.secured_type = t.secured_type
where		p.client = @client

-- To make the numbers work for L.A. which puts the home loans into the auto-loans table....
-- For everyone else, do not include "housing" by removing this comment below!
-- and		t.auto_home_other <> 'H'

-- Fetch the personal income values
INSERT INTO	#t_Results ( category, item, value )
SELECT		3,
		case person when 1 then 1 else 2 end,
		net_income
FROM		people
WHERE		client = @client

-- Fetch the asset information
INSERT INTO	#t_Results ( category, item, value )
SELECT		3,
		isnull(i.rpps_code,6),
		asset_amount
from		assets a
inner join	asset_ids i WITH (NOLOCK) ON a.asset_id = i.asset_id
WHERE		a.client = @client

-- Calculate the total assets
INSERT INTO	#t_Results ( category, item, value )
SELECT		3, 7, sum(p.current_value)
FROM		secured_properties p with (NOLOCK)
inner join	secured_types t	WITH (NOLOCK) ON p.secured_type = t.secured_type
where		p.client = @client

-- To make the numbers work for L.A. which puts the home loans into the auto-loans table....
-- For everyone else, do not include "housing" by removing this comment below!
-- and		t.auto_home_other <> 'H'

-- Calculate the total liabalities
INSERT INTO	#t_Results ( category, item, value )
SELECT		3, 8, sum(l.balance)
from		secured_loans l WITH (NOLOCK)
inner join secured_properties p WITH (NOLOCK) ON l.secured_property = p.secured_property
inner join secured_types t	WITH (NOLOCK) ON p.secured_type = t.secured_type
where		p.client = @client

-- To make the numbers work for L.A. which puts the home loans into the auto-loans table....
-- For everyone else, do not include "housing" by removing this comment below!
-- and		t.auto_home_other <> 'H'

-- Calculate the monthly income
DECLARE		@MonthlyIncome	money
SELECT		@MonthlyIncome = sum(value)
FROM		#t_Results
WHERE		category = 3
AND		item BETWEEN 1 AND 6

INSERT INTO	#t_Results ( category, item, value )
VALUES		( 3, 9, @MonthlyIncome )

-- Include the number of dependants
INSERT INTO	#t_Results ( category, item, value )
SELECT		4, 4, c.dependents + (case c.marital_status when 2 then 2 else 1 end)
FROM		clients c
WHERE		client = @client

-- Include the reason for hardship
INSERT INTO	#t_Results ( category, item, value )
SELECT		5, isnull (f.rpps_code, 7), 0.0
FROM		clients c
LEFT OUTER JOIN	financial_problems f WITH (NOLOCK) ON coalesce(c.cause_fin_problem1,c.cause_fin_problem2,c.cause_fin_problem3,c.cause_fin_problem4) = f.financial_problem
WHERE		c.client = @client

-- Insert the default values for the "required" items
insert into #t_results ( category, item, value ) VALUES ( 2, 1, 0 )
insert into #t_results ( category, item, value ) VALUES ( 2, 2, 0 )
insert into #t_results ( category, item, value ) VALUES ( 2, 3, 0 )
insert into #t_results ( category, item, value ) VALUES ( 3, 1, 0 )
insert into #t_results ( category, item, value ) VALUES ( 3, 2, 0 )
insert into #t_results ( category, item, value ) VALUES ( 3, 7, 0 )
insert into #t_results ( category, item, value ) VALUES ( 3, 8, 0 )
insert into #t_results ( category, item, value ) VALUES ( 3, 9, 0 )
insert into #t_results ( category, item, value ) VALUES ( 4, 1, 0 )
insert into #t_results ( category, item, value ) VALUES ( 4, 2, 0 )
insert into #t_results ( category, item, value ) VALUES ( 4, 3, 0 )
insert into #t_results ( category, item, value ) VALUES ( 4, 4, 0 )

-- Calculate the total income
DECLARE		@HouseholdExpense	money
SELECT		@HouseholdExpense = sum(suggested_amount)
FROM		budget_detail d
WHERE		budget = @Budget

IF @HouseholdExpense IS NULL
	SELECT @HouseholdExpense = 0

-- Household Expense to Income Variance
INSERT INTO #t_results (category, item, value)
VALUES		( 4, 1, @MonthlyIncome - @HouseholdExpense )

-- Total Expenses to Income Variance
INSERT INTO #t_results (category, item, value)
VALUES		( 4, 2, @MonthlyIncome - @HouseholdExpense - @DmpPayment )

-- Calculate total debt to total income percentage
DECLARE		@Total_Debt	money
DECLARE		@Total_Income	money

SELECT		@Total_Debt = sum(value)
FROM		#t_results
WHERE		category = 2
AND		item IN (1, 3)

SELECT		@Total_Income = sum(value)
FROM		#t_results
WHERE		category = 3
AND		item BETWEEN 1 and 6

IF @Total_Debt IS NULL
	SELECT @Total_Debt = 0

IF @Total_Income IS NULL
	SELECT @Total_Income = 0

IF @Total_Income > 0.0100
	INSERT INTO #t_results (category, item, value)
	VALUES			(4, 3, convert(money, convert(float,@Total_Debt) / convert(float,@Total_Income)) )

-- Merge the results together
DECLARE		@Rows		INT

SELECT		@proposal_record as client_creditor_proposal, category, item, sum(value) as 'value'
FROM		#t_Results
WHERE		value IS NOT NULL
GROUP BY	category, item
ORDER BY	category, item

SELECT @Rows = @@rowcount

-- Discard the intermediate table
DROP TABLE #t_Results
COMMIT TRANSACTION

RETURN ( @Rows )
GO
