USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_delete_rpps_response_file]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_delete_rpps_response_file] ( @rpps_response_file as int ) as

-- ==============================================================================================================
-- ==            Delete the contents of an RPPS response file                                                  ==
-- ==============================================================================================================

delete
from	rpps_response_details_cda
where	rpps_response_detail in (
	select	rpps_response_detail
	from	rpps_response_details
	where	rpps_response_file	= @rpps_response_file
)

delete
from	rpps_response_details_cdd
where	rpps_response_detail in (
	select	rpps_response_detail
	from	rpps_response_details
	where	rpps_response_file	= @rpps_response_file
)

delete
from	rpps_response_details_cdp
where	rpps_response_detail in (
	select	rpps_response_detail
	from	rpps_response_details
	where	rpps_response_file	= @rpps_response_file
)

delete
from	rpps_response_details_cdr
where	rpps_response_detail in (
	select	rpps_response_detail
	from	rpps_response_details
	where	rpps_response_file	= @rpps_response_file
)

delete
from	rpps_response_details_cdt
where	rpps_response_detail in (
	select	rpps_response_detail
	from	rpps_response_details
	where	rpps_response_file	= @rpps_response_file
)

delete
from	rpps_response_details_cdv
where	rpps_response_detail in (
	select	rpps_response_detail
	from	rpps_response_details
	where	rpps_response_file	= @rpps_response_file
)

delete
from	rpps_response_details_other
where	rpps_response_detail in (
	select	rpps_response_detail
	from	rpps_response_details
	where	rpps_response_file	= @rpps_response_file
)

delete
from	rpps_response_details
where	rpps_response_file	= @rpps_response_file

delete
from	rpps_response_files
where	rpps_response_file	= @rpps_response_file
GO
