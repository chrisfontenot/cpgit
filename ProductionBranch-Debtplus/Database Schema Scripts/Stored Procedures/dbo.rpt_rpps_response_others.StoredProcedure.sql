USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_others]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_others] ( @rpps_response_file as int = null ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of error payments                    ==
-- =======================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

-- Retrieve the information for the payment records
select		d.date_created							as 'date_created',
		isnull(d.debit_amt,r.gross)					as 'gross',

		case
			when d.creditor_type = 'D' then d.fairshare_amt
			when d.creditor_type is not null then 0
			else r.gross - r.net
		end								as 'deducted',

		case
			when d.creditor_type = 'D' then 0
			when d.creditor_type = 'N' then 0
			when d.creditor_type is not null then d.fairshare_amt
			else 0
		end								as 'billed',

		d.client							as 'client',
		coalesce(pn.last,r.consumer_name,'') 				as 'client_name',
		d.creditor							as 'creditor',
		d.client_creditor							as 'client_creditor',
		isnull(d.account_number,r.account_number)			as 'account_number',
		coalesce(cr.creditor_name,'')					as 'creditor_name',
		coalesce(r.processing_error, x.description, rt.return_code, '')	as 'error_code'

from		rpps_response_details r with (nolock)
left outer join	rpps_transactions rt with (nolock) on r.rpps_transaction = rt.rpps_transaction
left outer join	registers_client_creditor d with (nolock) ON rt.client_creditor_register = d.client_creditor_register
left outer join	creditors cr with (nolock) on d.creditor = cr.creditor
left outer join	people p ON d.client=p.client AND 1=p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
left outer join	rpps_reject_codes x with (nolock) ON rt.return_code = x.rpps_reject_code

where		r.rpps_response_file		= @rpps_response_file
and		r.service_class_or_purpose	= 'CIE'
and		r.transaction_code		= 22

order by	cr.type, cr.creditor_id, d.client, 9		-- creditor, client, account number

return ( @@rowcount )
GO
