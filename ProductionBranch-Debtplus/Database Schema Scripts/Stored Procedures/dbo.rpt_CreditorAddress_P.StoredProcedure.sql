USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CreditorAddress_P]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_CreditorAddress_P] ( @Creditor AS typ_creditor ) AS
-- ================================================================================================
-- ==                Return the creditor proposal address                                        ==
-- ================================================================================================
execute xpr_creditor_address_merged @Creditor, 'P'
return ( 1 )
GO
