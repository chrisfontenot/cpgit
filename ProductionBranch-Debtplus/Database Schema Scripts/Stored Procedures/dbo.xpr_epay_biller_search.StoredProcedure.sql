USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_biller_search]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_biller_search] ( @biller_name as varchar(80) ) AS

-- ===============================================================================================
-- ==               Return a list of the matching biller IDs for this name                      ==
-- ===============================================================================================

-- ChageLog

SET NOCOUNT ON

SELECT	epay_biller_id		as 'biller_id',
	biller_name		as 'name'
FROM	epay_biller_ids
where	biller_name like '%' + ltrim(rtrim(@biller_name)) + '%'
and	type			= 'D'

ORDER BY 1, 2

RETURN ( @@rowcount )
GO
