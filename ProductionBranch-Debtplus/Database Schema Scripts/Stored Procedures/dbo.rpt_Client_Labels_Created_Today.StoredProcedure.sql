USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Labels_Created_Today]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Client_Labels_Created_Today] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL, @language as int = 1 ) AS

-- =================================================================================================
-- ==            Generate the the list information needed to produce labels for clients who       ==
-- ==            have appointments that are for the date range indicated in a "pending" status.   ==
-- =================================================================================================

-- This routine is specifc to St. Louis.

-- This is the indicator in the list of indicators that we use to track the "i printed this label"
declare	@label_printed_indicator	int
select	@label_printed_indicator	= 5

-- Remove the timestamp from the date ranges
if @ToDate is null
	select @ToDate = getdate()

if @FromDate is null
	select @FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@Todate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Build a list of the clients in the date range indicated
select	distinct c.client
into	#clients
from	clients c with (nolock)
left outer join client_indicators ci with (nolock) on c.client = ci.client and @label_printed_indicator = ci.indicator
where	c.active_status = 'PND'
and	c.active_status_date between @FromDate and @ToDate
and	ci.client_indicator is null;	-- "not found" condition

-- Make an index to help speed processing
create unique index temp_ix_labels_client on #clients ( client );

-- Discard clients from the list if they have the indicator set
delete		#clients
from		#clients t
inner join	client_indicators ci with (nolock) on t.client = ci.client
where		ci.indicator	= @label_printed_indicator

-- Add the client indicators to the list of client indicators
insert into client_indicators (client, indicator)
select	client, @label_printed_indicator
from	#clients;

-- Return the list of the clients that need to be printed
select	v.client		as 'client',
	v.name			as 'name',
	v.addr1 		as 'addr_1',
	v.addr2			as 'addr_2',
	v.addr3			as 'addr_3',
	v.zipcode		as 'zipcode'

from	view_client_address v with (nolock)

where	client in (
	select	client
	from	#clients
)

order by v.client;

-- Drop the working table
drop table #clients

return ( @@rowcount )
GO
