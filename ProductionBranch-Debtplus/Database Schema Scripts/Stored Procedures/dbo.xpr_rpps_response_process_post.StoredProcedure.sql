USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_post]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_post] ( @response_file as int, @disbursement_note as int = 1 ) AS

-- =====================================================================================================
-- ==            Post the response file and update the database with the responses                    ==
-- =====================================================================================================
set nocount on
begin transaction
set xact_abort on

if not exists (select * from rpps_response_files WHERE rpps_response_file = @response_file)
begin
	rollback transaction
	RaisError ('The response file %d is not valid', 16, 1, @response_file )
	return ( 0 )
end

-- Ensure that it has not been previously posted
if not exists (select * from rpps_response_files WHERE rpps_response_file = @response_file AND date_posted is null)
begin
	rollback transaction
	RaisError ('The response file %d was posted previously', 16, 1, @response_file )
	return ( 0 )
end

-- Execute the processing for the various types of files
execute xpr_rpps_response_process_DFS		@response_file
execute xpr_rpps_response_process_CIE		@response_file
execute xpr_rpps_response_process_CIE_prenote	@response_file
execute xpr_rpps_response_process_CDD		@response_file
execute xpr_rpps_response_process_CDP		@response_file
execute xpr_rpps_response_process_CDA		@response_file
execute xpr_rpps_response_process_CDR		@response_file
execute xpr_rpps_response_process_CDT		@response_file
execute xpr_rpps_response_process_CDV		@response_file
execute xpr_rpps_response_process_COA		@response_file

-- Indicate that the file is now posted
update	rpps_response_files
set	date_posted		= getdate()
where	rpps_response_file	= @response_file

-- Commit the changes and leave
commit transaction
return ( 1 )
GO
