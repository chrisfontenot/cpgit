USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_contributions_zipcode]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_client_contributions_zipcode] ( @From_Date as datetime = null, @To_Date as datetime = null ) as

-- =================================================================================================
-- ==            Client contributions by zipcode                                                  ==
-- =================================================================================================

-- ChangeLog
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   7/12/2003
--     Split the fairshare into billed and deducted

-- Initialize
set nocount on

if @To_Date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
		@to_date = convert(datetime, convert(varchar(10), @to_date, 101) + ' 23:59:59')

-- Find the default office
declare	@default_office	int
select	@default_office	 = min(office)
from	offices
where	[ActiveFlag]	= 1
and		[Default]		= 1;

if @default_office is null
	select	@default_office	 = min(office)
	from	offices
	where	[ActiveFlag]	= 1

if @default_office is null
	select	@default_office	 = min(office)
	from	offices

if @default_office is null
	select	@default_office	 = 1

-- First, make a slice of the contributions by the client/creditor
select	client, convert(varchar(10),null) as zipcode, @Default_Office as office, creditor, creditor_type, sum(debit_amt) as amount, sum(fairshare_amt) as fairshare_amt, convert(money,0) as fee_amount, convert(money,0) as paf_amount, convert(money,0) as fairshare_deduct, convert(money,0) as fairshare_bill
into #contrib
from registers_client_creditor with (nolock)
where	tran_type in ('AD','MD','BW')
and		date_created between @from_date and @to_date
and		void = 0
group by client, creditor, creditor_type

-- Create an index for the client information
create index ix1_contrib on #contrib ( client );
create index ix2_contrib on #contrib ( zipcode );

-- Next update the zipcodes for the offices
update	#contrib
set		zipcode		= left(a.postalcode,5)
from	#contrib x
inner join clients c on x.client = c.client
inner join addresses a on c.addressid = a.[address];

-- Find the office based upon the zipcode
update	#contrib
set		office = z.office
from	#contrib c
inner join zipcodes z on c.zipcode = left(z.zip_lower,5);

-- Move the fields to the proper spots
update	#contrib
set		fairshare_deduct	= fairshare_amt
where	creditor_type	= 'D'

update	#contrib
set		fairshare_bill		= fairshare_amt
where	creditor_type not in ('D','N');

declare	@paf_creditor		varchar(10)
declare	@setup_creditor		varchar(10)
declare	@deduct_creditor	varchar(10)

select	@paf_creditor		= paf_creditor,
		@deduct_creditor	= deduct_creditor,
		@setup_creditor		= setup_creditor
from	config

-- Correct the setup amount
update	#contrib
set		fee_amount			= amount,
		fairshare_deduct	= 0,
		fairshare_bill		= 0
from	#contrib x
where	creditor			= @setup_creditor;

-- Correct the monthly fee creditor
update	#contrib
set		paf_amount			= amount,
		fairshare_deduct	= 0,
		fairshare_bill		= 0
where	creditor		= @paf_creditor;

-- Produce the result
select	x.client,
		c.active_status,
		x.zipcode,
		x.office,
		o.name		as office_name,
		dbo.format_reverse_name(p1n.prefix,p1n.first,p1n.middle,p1n.last,p1n.suffix) as client_name,
		sum(fee_amount)			as 'fee_amount',
		sum(paf_amount)			as 'paf_amount',
		sum(fairshare_deduct)	as 'fairshare_deduct',
		sum(fairshare_bill)		as 'fairshare_bill'
from	#contrib x
inner join clients c on x.client = c.client
inner join offices o on x.office = o.office
left outer join people p1 on c.client = p1.client and 1 = p1.relation
left outer join names p1n on p1.nameid = p1n.name
where	fee_amount			> 0
or		paf_amount			> 0
or		fairshare_deduct	> 0
or		fairshare_bill		> 0
group by x.client, c.active_status, x.zipcode, x.office, o.name, p1n.prefix, p1n.first, p1n.middle, p1n.last, p1n.suffix

drop table #contrib;

return ( @@rowcount )
GO
