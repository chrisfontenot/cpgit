USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_ActionGoals_ByClient]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_ActionGoals_ByClient] ( @ClientID AS INT ) AS
-- ====================================================================================
-- ==           Fetch the action plan information for the indicated client           ==
-- ====================================================================================

-- Do not generate intermediate result sets
SET NOCOUNT		ON

-- Fetch the action plan from the list of action plans for this client
DECLARE @ActionPlan	INT
SET ROWCOUNT 1

SELECT	@ActionPlan	= action_plan
FROM	action_plans
WHERE	client = @ClientID
ORDER BY date_created DESC

SET ROWCOUNT 0

-- Ensure that there is an action plan
IF @ActionPlan IS NULL
	SELECT @ActionPlan = 0

-- Restore the count information
Execute rpt_PrintClient_ActionGoals_ByPlan @ActionPlan
GO
