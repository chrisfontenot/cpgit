USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_event_read]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_debt_event_read] ( @client_creditor_event as int, @client_creditor as int ) as

-- ==================================================================================================
-- ==            Retrieve the information for the indicated event                                  ==
-- ==================================================================================================

set nocount on

-- Complain if the record has been "used"
if exists (select * from client_creditor_events where date_updated is not null and client_creditor_event = @client_creditor_event)
begin
	RaisError ('The record has been updated in the database and may not be edited.', 16, 1)
	rollback transaction
	return ( 0 )
end

-- Find the P.A.F. creditor from the config table
declare	@paf_creditor	varchar(10)
select	@paf_creditor	= paf_creditor
from	config

-- If there is no passed key then use the event record ID for the information
if @client_creditor is null
	select	@client_creditor = client_creditor
	from	client_creditor_events
	where	client_creditor_event = @client_creditor_event

-- Retrieve the client/creditor creditor ID from the system
declare	@item_creditor	varchar(10)

select	@item_creditor = creditor
from	client_creditor
where	client_creditor = @client_creditor

-- Retrieve the information about the current item
select	client_creditor_event				as 'item_key',
	effective_date					as 'item_date',
	[value]						as 'item_value',
	config_fee					as 'config_fee',
	case when @item_creditor = @paf_creditor then 1 else 0 end	as 'paf'
from	client_creditor_events e with (nolock)
where	client_creditor_event = @client_creditor_event

union all

select	@client_creditor_event				as 'item_key',
	null						as 'item_date',
	null						as 'item_value',
	null						as 'config_fee',
	case when @item_creditor = @paf_creditor then 1 else 0 end as 'paf'

return ( @@rowcount )
GO
