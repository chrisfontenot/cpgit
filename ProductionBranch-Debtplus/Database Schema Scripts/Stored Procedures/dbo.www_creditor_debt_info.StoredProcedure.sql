USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_debt_info]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_creditor_debt_info] ( @creditor as varchar(10), @client_creditor as int ) AS
-- =======================================================================================
-- ==            Determine the client/creditor debt information                         ==
-- =======================================================================================

-- ChangeLog
--   1/13/2001
--     Changed because the "first_payment_date", "first_payment_amount", and "last_payment_date" was moved
--     to the registers_client_creditor based upon first_payment and last_payment pointers.
--   9/31/2002
--     Added support for client_creditor_balances table

-- Disable intermediate results
set nocount on

-- Return the date for the transaction. Format it for the proper style
select
	isnull(cc.client,0)								as 'client',
	isnull(cc.creditor+' ','') + coalesce(cr.creditor_name, cc.creditor_name,'')	as 'creditor_name',
	cr.division									as 'creditor_division_name',
	v.client_name									as 'account_name',
	v.account_number								as 'account_number',
	bal.orig_balance									as 'original_balance',
	isnull(bal.orig_balance_adjustment,0)						as 'original_balance_adjustment',
	isnull(bal.total_interest,0)							as 'total_interest',
	bal.total_payments								as 'total_payments',
	isnull(cc.disbursement_factor,0)						as 'disbursement_factor',
	isnull(bal.payments_month_1,0)					as 'last_month_disbursement',
	isnull(bal.payments_month_0,0)					as 'current_month_disbursement',
	convert(varchar(10), rcc_first.date_created, 1)					as 'first_payment_date',
	convert(varchar(10), v.last_payment_date, 1)					as 'last_payment_date',
	v.last_payment_amount								as 'last_payment_amount',
	convert(varchar(10), cc.last_stmt_date, 1)					as 'last_statement_date',
	cc.expected_payout_date								as 'payout_date',
	coalesce(cc.dmp_interest, cr.lowest_apr_pct, cr.medium_apr_pct, cr.highest_apr_pct) as 'interest_rate',
	cc.contact_name									as 'contact_name',
	cc.non_dmp_payment								as 'non_dmp_payment',
	cc.non_dmp_interest								as 'non_dmp_interest',
	bal.total_sched_payment								as 'total_sched_payment'
from	client_creditor cc with (nolock)
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join view_last_payment v with (nolock) on cc.client_creditor = v.client_creditor
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join registers_client_creditor rcc_first with (nolock) on cc.first_payment = rcc_first.client_creditor_register
where	cc.creditor = @creditor
and	cc.client_creditor = @client_creditor

return ( @@rowcount )
GO
