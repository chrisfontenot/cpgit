USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PendingAppointments_ByOffice_Counselors]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PendingAppointments_ByOffice_Counselors] ( @from_date as datetime, @office as int ) as

-- =================================================================================================================
-- ==            Return the information for the subreport on the office pending appointment report                ==
-- =================================================================================================================

-- ChangeLog
--   8/2/2005
--      Corrected logic merging the times to the list of 5 time slots. It was skipping every 6th time.

-- Initialize
set nocount on

declare	@day_start		datetime
declare	@day_stop		datetime

-- Widen the time to encompass the entire day
select	@day_start = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@day_stop  = convert(datetime, convert(varchar(10), @from_date, 101) + ' 23:59:59')

select
	dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor',
 	convert(varchar(20), a.start_time, 101) as 'date',
	a.start_time as 'time'
into	#PendingAppointments_ByOffice_Counselors_1
from	appt_times a with (nolock)
left outer join appt_counselors c with (nolock) on a.appt_time = c.appt_time
left outer join counselors co with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name
where	a.start_time between @day_start and @day_stop
and	a.office = @office
and	c.inactive = 0

-- Create a table for the resulting cube
create table #PendingAppointments_ByOffice_Counselors_2 (
	counselor	varchar(80) null,
	[date]		varchar(10) null,
	[time_1]	varchar(10) null,
	[time_2]	varchar(10) null,
	[time_3]	varchar(10) null,
	[time_4]	varchar(10) null,
	[time_5]	varchar(10) null
);

-- Create a cursor for the temporary file
declare	PendingAppointments_Cursor cursor for
	select	counselor, [date], [time]
	from	#PendingAppointments_ByOffice_Counselors_1
	order by 1, 2, 3

declare	@counselor	varchar(80)
declare	@date		varchar(10)
declare	@time		datetime

open	PendingAppointments_Cursor
fetch	PendingAppointments_Cursor into @counselor, @date, @time

declare	@current_counselor	varchar(80)
declare	@current_date		varchar(10)
declare	@time_1			varchar(10)
declare	@time_2			varchar(10)
declare	@time_3			varchar(10)
declare	@time_4			varchar(10)
declare	@time_5			varchar(10)

-- Initialize the first record information
select	@current_counselor	= @counselor,
	@current_date		= @date,
	@time_1			= null,
	@time_2			= null,
	@time_3			= null,
	@time_4			= null,
	@time_5			= null

while @@fetch_status = 0
begin

	-- Record the current information if the counselor has changed
	if (@counselor <> @current_counselor) or (@date <> @current_date) or (@time_5 is not null)
	begin
		if (@time_1 is not null)
			insert into #PendingAppointments_ByOffice_Counselors_2 (counselor, [date], [time_1], [time_2], [time_3], [time_4], [time_5])
			values	(@current_counselor, @current_date, @time_1, @time_2, @time_3, @time_4, @time_5)

		select	@current_counselor	= @counselor,
			@current_date		= @date,
			@time_1			= null,
			@time_2			= null,
			@time_3			= null,
			@time_4			= null,
			@time_5			= null

	end

	if @time_1 is null
		select	@time_1		= right(convert(varchar(20), @time), 7)
	else if @time_2 is null
		select	@time_2		= right(convert(varchar(20), @time), 7)
	else if @time_3 is null
		select	@time_3		= right(convert(varchar(20), @time), 7)
	else if @time_4 is null
		select	@time_4		= right(convert(varchar(20), @time), 7)
	else
		select	@time_5		= right(convert(varchar(20), @time), 7)

	fetch	PendingAppointments_Cursor into @counselor, @date, @time
end

-- Record the last component record item
if (@current_counselor is not null) and (@time_1 is not null)
	insert into #PendingAppointments_ByOffice_Counselors_2 (counselor, [date], [time_1], [time_2], [time_3], [time_4], [time_5])
	values	(@current_counselor, @current_date, @time_1, @time_2, @time_3, @time_4, @time_5)

-- Release the cursor values
close		PendingAppointments_Cursor
deallocate	PendingAppointments_Cursor

-- Return the resulting recordset
select counselor, [date], [time_1], [time_2], [time_3], [time_4], [time_5]
from  #PendingAppointments_ByOffice_Counselors_2
order by counselor, [date]

-- Discard working table
drop table #PendingAppointments_ByOffice_Counselors_1
drop table #PendingAppointments_ByOffice_Counselors_2

-- Return success
return ( 1 )
GO
