USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cda]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cda] ( @rpps_response_file as int = null ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of rejected proposals                ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--      Added client's counselor name
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

select	cr.type													as 'type',
		cr.creditor_id											as 'creditor_id',
		isnull('['+cr.creditor+'] ','')							as 'creditor',
		isnull(tr.client,cda.client)							as 'client',
		coalesce(cr.creditor_name,ix1.biller_name, 'UNKNOWN CREDITOR') as 'creditor_name',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)		as 'client_name',
		d.rpps_biller_id										as 'biller_id',
		d.account_number										as 'account_number',
		isnull(pr.proposal_print_date, b.date_transmitted)		as 'proposal_print_date',

		-- These two fields may be overriden in the response.
		coalesce(cda.payment_amount, pr.proposed_amount, 0)										as 'proposed_amount',
		convert(datetime, convert(varchar, isnull(cda.start_date, pr.proposed_start_date),101))	as 'proposed_start_date',

		coalesce(d.processing_error,'')							as 'response_reason',
		d.trace_number											as 'trace_number',
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as 'counselor_name'

-- Manditory linkage                
from		rpps_response_details d				with (nolock)
inner join	rpps_response_details_cda cda		with (nolock) on d.rpps_response_detail = cda.rpps_response_detail

-- Optional Linkage
left outer join	rpps_biller_ids ix1				with (nolock) on d.rpps_biller_id = ix1.rpps_biller_id
left outer join rpps_transactions tr			with (nolock) on d.rpps_transaction = tr.rpps_transaction
left outer join client_creditor_proposals pr	with (nolock) on tr.client_creditor_proposal = pr.client_creditor_proposal
left outer join proposal_batch_ids b			with (nolock) on pr.proposal_batch_id = b.proposal_batch_id
left outer join client_creditor cc				with (nolock) on pr.client_creditor = cc.client_creditor
left outer join creditors cr					with (nolock) on cc.creditor = cr.creditor
left outer join people p						with (nolock) on isnull(tr.client,cda.client) = p.client and 1 = p.relation
left outer join names pn						with (nolock) on p.nameid = pn.name
left outer join clients c						with (nolock) on isnull(tr.client,cda.client) = c.client
left outer join counselors co					with (nolock) on c.counselor = co.counselor
left outer join names cox						with (nolock) on co.NameID = cox.name

where		d.rpps_response_file = @rpps_response_file
and			d.service_class_or_purpose = 'CDA'

order by	1, 2, 3 -- cr.type, cr.creditor_id, cr.creditor

return ( @@rowcount )
GO
