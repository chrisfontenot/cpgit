USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AppointmentActivity_Misc]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_AppointmentActivity_Misc] ( @FromDate AS DateTime = NULL, @office as int = null ) AS
-- ===========================================================================================
-- ==           Information for the appointment activity report                             ==
-- ===========================================================================================

-- Global storage
DECLARE @FirstLabel     VARCHAR(50)
DECLARE @FirstID        INT
DECLARE @CutoffDate	DATETIME

-- Default the date to today if it is not supplied
IF @FromDate IS NULL
	SELECT @FromDate = getdate()

-- Remove the time from the date value
SELECT	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')

-- Split the input date into the appropriate items
DECLARE @Month as INT
DECLARE @Year  as INT
DECLARE @Day   as INT
SELECT	@Month = datepart ( month, @FromDate ),
	@Day = datepart ( day, @FromDate ),
	@Year = datepart ( year, @FromDate )

-- Set the limits for the month operation
DECLARE @Lower_Month    DATETIME
SELECT  @Lower_Month  = CONVERT ( DATETIME, convert(varchar,@Month) + '/01/' + convert(varchar,@Year) )

-- Set the limits for the year operation
DECLARE @Lower_Year     DATETIME
SELECT  @Lower_Year   = CONVERT ( DATETIME, '01/01/' + convert(varchar,@Year) )

-- Set the limits for the quarter
DECLARE @Lower_Quarter  DATETIME
SELECT	@Month = (((@Month - 1) / 3) * 3) + 1
SELECT  @Lower_Quarter = CONVERT ( DATETIME, convert(varchar, @Month ) + '/01/' + convert(varchar,@Year) )

-- Define the cutoff at the end of the desired month
SELECT	@CutoffDate	= dateadd(s, -1, dateadd(m, 1, @Lower_Month))
IF @CutoffDate > getdate()
	SELECT	@CutoffDate	= getdate()

-- Disable the counts as a resulting recordset
SET NOCOUNT ON

declare	@Month_Appointments		float
declare	@Month_DMP_Appointments		float
declare	@Month_Deposit_Appointments	float

declare	@Quarter_Appointments		float
declare	@Quarter_DMP_Appointments	float
declare	@Quarter_Deposit_Appointments	float

declare	@Year_Appointments		float
declare	@Year_DMP_Appointments		float
declare	@Year_Deposit_Appointments	float

-- ==========================================================================================
-- ==                MONTH TO DATE : Find number of first appointments                     ==
-- ==========================================================================================
SELECT		@Month_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
AND		((@office = -1) or (ca.office = @office))
AND		ca.start_time BETWEEN @Lower_Month AND @CutoffDate

if @Month_Appointments IS NULL
	SET @Month_Appointments = 0

-- ==========================================================================================
-- ==                MONTH TO DATE : Find number of first DMP appointments                 ==
-- ==========================================================================================
SELECT		@Month_DMP_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
WHERE		ca.result = 'DMP'
AND		((@office = -1) or (ca.office = @office))
AND		ca.start_time BETWEEN @Lower_Month AND @CutoffDate

if @Month_DMP_Appointments IS NULL
	SET @Month_DMP_Appointments = 0

-- ==========================================================================================
-- ==                MONTH TO DATE : Find number of first deposits                         ==
-- ==========================================================================================
SELECT		@Month_Deposit_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
AND		ca.result = 'DMP'
AND		c.first_deposit_date IS NOT NULL
AND		((@office = -1) or (ca.office = @office))
AND		ca.start_time BETWEEN @Lower_Month AND @CutoffDate

if @Month_DMP_Appointments IS NULL
	SET @Month_DMP_Appointments = 0

-- ==========================================================================================
-- ==                QUARTER TO DATE : Find number of first appointments                   ==
-- ==========================================================================================
SELECT		@Quarter_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
WHERE		ca.start_time BETWEEN @Lower_Quarter AND @Lower_Month
AND		((@office = -1) or (ca.office = @office))

if @Quarter_Appointments IS NULL
	SET @Quarter_Appointments = 0

-- ==========================================================================================
-- ==                QUARTER TO DATE : Find number of first DMP appointments               ==
-- ==========================================================================================
SELECT		@Quarter_DMP_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
WHERE		ca.result = 'DMP'
AND		((@office = -1) or (ca.office = @office))
AND		ca.start_time BETWEEN @Lower_Quarter AND @Lower_Month

if @Quarter_DMP_Appointments IS NULL
	SET @Quarter_DMP_Appointments = 0

-- ==========================================================================================
-- ==                QUARTER TO DATE : Find number of first deposits                       ==
-- ==========================================================================================
SELECT		@Quarter_Deposit_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
WHERE		ca.result = 'DMP'
AND		c.first_deposit_date IS NOT NULL
AND		((@office = -1) or (ca.office = @office))
AND		ca.start_time BETWEEN @Lower_Quarter AND @Lower_Month

if @Quarter_DMP_Appointments IS NULL
	SET @Quarter_DMP_Appointments = 0

-- ==========================================================================================
-- ==                YEAR TO DATE : Find number of first appointments                      ==
-- ==========================================================================================
SELECT		@Year_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
WHERE		ca.start_time BETWEEN @Lower_Year AND @Lower_Quarter
AND		((@office = -1) or (ca.office = @office))

if @Year_Appointments IS NULL
	SET @Year_Appointments = 0

-- ==========================================================================================
-- ==                YEAR TO DATE : Find number of first DMP appointments                  ==
-- ==========================================================================================
SELECT		@Year_DMP_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
WHERE		ca.result = 'DMP'
AND		((@office = -1) or (ca.office = @office))
AND		ca.start_time BETWEEN @Lower_Year AND @Lower_Quarter

if @Year_DMP_Appointments IS NULL
	SET @Year_DMP_Appointments = 0

-- ==========================================================================================
-- ==                YEAR TO DATE : Find number of first deposits                          ==
-- ==========================================================================================
SELECT		@Year_Deposit_Appointments = COUNT(*)
FROM		clients c WITH (NOLOCK)
INNER JOIN	client_appointments ca WITH (NOLOCK) ON c.first_kept_appt = ca.client_appointment
WHERE		ca.result = 'DMP'
AND		c.first_deposit_date IS NOT NULL
AND		((@office = -1) or (ca.office = @office))
AND		ca.start_time BETWEEN @Lower_Year AND @Lower_Quarter

if @Year_DMP_Appointments IS NULL
	SET @Year_DMP_Appointments = 0

-- Months flow into quarter
SET @Quarter_Appointments		= @Quarter_Appointments + @Month_Appointments
SET @Quarter_DMP_Appointments		= @Quarter_DMP_Appointments + @Month_DMP_Appointments
SET @Quarter_Deposit_Appointments	= @Quarter_Deposit_Appointments + @Month_Deposit_Appointments

-- Quarters flow into year
SET @Year_Appointments			= @Year_Appointments + @Quarter_Appointments
SET @Year_DMP_Appointments		= @Year_DMP_Appointments + @Quarter_DMP_Appointments
SET @Year_Deposit_Appointments		= @Year_Deposit_Appointments + @Quarter_Deposit_Appointments

-- ============================================================================================
-- ==             Compute the percentages                                                    ==
-- ============================================================================================

-- Month percentages
IF @Month_Appointments > 0
	SET @Month_Appointments = ( @Month_DMP_Appointments * 100 ) / @Month_Appointments

IF @Month_DMP_Appointments > 0
	SET @Month_DMP_Appointments = ( @Month_Deposit_Appointments * 100 ) / @Month_DMP_Appointments

-- Quarter Percentages
IF @Quarter_Appointments > 0
	SET @Quarter_Appointments = ( @Quarter_DMP_Appointments * 100 ) / @Quarter_Appointments

IF @Quarter_DMP_Appointments > 0
	SET @Quarter_DMP_Appointments = ( @Quarter_Deposit_Appointments * 100 ) / @Quarter_DMP_Appointments

-- Year Percentages
IF @Year_Appointments > 0
	SET @Year_Appointments = ( @Year_DMP_Appointments * 100 ) / @Year_Appointments

IF @Year_DMP_Appointments > 0
	SET @Year_DMP_Appointments = ( @Year_Deposit_Appointments * 100 ) / @Year_DMP_Appointments

-- ============================================================================================
-- ==             Return the results                                                         ==
-- ============================================================================================

SELECT TOP 1 @FirstLabel = appt_name from appt_types order by appt_type

SELECT
	1 as 'RecType',
	'% ' + @FirstLabel + ' Appointments to DMP' as 'Description',
	@Month_Appointments AS 'MTDCount',
	@Quarter_Appointments AS 'QTDCount',
	@Year_Appointments AS 'YTDCount'

UNION ALL

SELECT
	2 as 'RecType',
	'% ' + @FirstLabel + ' Appointments to first deposit' as 'Description',
	@Month_DMP_Appointments AS 'MTDCount',
	@Quarter_DMP_Appointments AS 'QTDCount',
	@Year_DMP_Appointments AS 'YTDCount'

RETURN ( 1 )
GO
