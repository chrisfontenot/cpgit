USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_deposit_detail_ByBatch]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_deposit_detail_ByBatch] ( @deposit_batch_id as INT = NULL ) AS

-- ===================================================================================================================
-- ==            Display the batch contents                                                                         ==
-- ===================================================================================================================

-- Suppress intermediate results
set nocount on

if @deposit_batch_id IS NULL
begin
	set	rowcount 1
	select	@deposit_batch_id = deposit_batch_id
	from	deposit_batch_ids
	where	date_posted is null
	order by date_created desc
	set	rowcount 0

	if @deposit_batch_id is null
	begin
		raiserror (50006, 16, 1)
		return ( 0 )
	end
end

-- Fetch the batch contents
select		case
			when d.tran_subtype = 'LB' then d.scanned_client
			when d.scanned_client is null then dbo.format_client_id ( d.client )
			else d.scanned_client
		end						as 'scanned_client',

		d.client					as 'deposit_client',
		d.tran_subtype					as 'subtype',
		d.ok_to_post					as 'ok_to_post',
		d.amount					as 'amount',
		d.item_date					as 'item_date',
		d.date_created					as 'date_created',
		d.reference					as 'reference',
		d.message					as 'message',
		dbo.format_reverse_name(default,pn.first,pn.middle,pn.last,default) as 'client_name',
		d.deposit_batch_detail				as 'deposit_batch_detail'

from		deposit_batch_details d with (nolock)
left outer join	people p with (nolock) on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
where		deposit_batch_id = @deposit_batch_id
order by	date_created, subtype

return ( @@rowcount )
GO
