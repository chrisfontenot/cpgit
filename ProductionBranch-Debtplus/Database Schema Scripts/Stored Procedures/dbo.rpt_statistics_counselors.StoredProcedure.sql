USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_statistics_counselors]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_statistics_counselors] as

-- ======================================================================================================
-- ==            Retrieve the last 12 dates for the office statistics                                  ==
-- ======================================================================================================

set nocount on

-- Retrieve the distinctive dates from the items
select distinct period_start
into		#raw_dates
from		statistics_counselors with (nolock);

-- Take the most recient 12 dates
select top 12	period_start
into		#date_selections
from		#raw_dates
order by	1 desc;

-- Return the information from the statistics
select	s.period_start,
	coalesce(dbo.format_normal_name(default,cox.first,cox.middle,cox.last,cox.suffix), 'counselor #' + convert(varchar, s.counselor), 'UNSPECIFIED') as 'counselor_name',
	coalesce(o.name,  'office #'    + convert(varchar, s.office),    'UNSPECIFIED') as 'office_name',
	s.active_clients as clients_active,
	s.clients_disbursed as clients_disbursed,
	s.dropped_clients as clients_dropped,
	s.new_dmp_clients as clients_new_dmp,
	s.new_dmp_debt as debt_new_dmp,
	s.expected_disbursement as disbursement_expected,
	s.actual_disbursement as disbursement_actual,

	case
		when s.expected_disbursement <= 0 then 0
		else convert(float,s.actual_disbursement) / convert(float, s.expected_disbursement) * 100.0
	end as disbursement_percentage,

	paf_fees as fees_paf
	
from	statistics_counselors s with (nolock)
left outer join offices o with (nolock) on s.office = o.office
left outer join counselors co with (nolock) on s.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name

where period_start in (select period_start from #date_selections)
and	((s.active_clients <> 0) or
	 (s.clients_disbursed <> 0) or
	 (s.dropped_clients <> 0) or
	 (s.new_dmp_clients <> 0) or
	 (s.new_dmp_debt <> 0) or
	 (s.expected_disbursement <> 0) or
	 (s.actual_disbursement <> 0) or
	 (s.paf_fees <> 0))

order by 1, 2;

-- Discard the working tables
drop table #date_selections
drop table #raw_dates

return ( @@rowcount );
GO
