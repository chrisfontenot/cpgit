USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_letter_client_appointment]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_letter_client_appointment] ( @client as int ) as
-- =========================================================================================
-- ==      We have to print a client appointment letter but were not given an appointment ==
-- ==      to be printed. So, try to find the "best" solution to the problem.             ==
-- =========================================================================================

declare	@start_time	datetime

-- Find the closest time in the future for the appointment. Since it is probably a booking
-- letter or a reminder one, this is as good of a choice as any.
select	@start_time	= max(start_time)
from	client_appointments with (nolock)
where	client		= @client
and	client_appointment is not null
and	status		<> 'R' -- ignore rescheduled (old appointment before reschedule) appointments

declare	@client_appointment	int

-- If we have a starting time then try to use that appointment.
if @start_time is not null
	select	@client_appointment	= max(client_appointment)
	from	client_appointments with (nolock)
	where	client			= @client
	and	start_time		= @start_time
	and	client_appointment	is not null
	and	status			<> 'R'

-- If there is no appointment then say so
if @client_appointment is null
	select	@client_appointment	= 0

return ( @client_appointment )
GO
