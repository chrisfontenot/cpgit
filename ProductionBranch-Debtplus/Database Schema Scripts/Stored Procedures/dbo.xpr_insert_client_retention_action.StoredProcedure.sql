USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_client_retention_action]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_client_retention_action] (@client_retention_event int, @retention_action int, @amount money = 0, @message varchar(1024) = null ) as
	INSERT INTO client_retention_actions (client_retention_event, retention_action, amount, [message])
	VALUES (@client_retention_event, @retention_action, @amount, @message)
	return ( scope_identity() )
GO
