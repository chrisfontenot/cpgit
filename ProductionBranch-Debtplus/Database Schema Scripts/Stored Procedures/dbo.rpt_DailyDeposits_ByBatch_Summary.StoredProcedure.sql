USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_DailyDeposits_ByBatch_Summary]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_DailyDeposits_ByBatch_Summary] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL, @counselor as int = null ) AS
-- =============================================================================================
-- ==                Fetch the information for the daily deposits report                      ==
-- ==                This is the summary information. It is used as a precursor to the        ==
-- ==                details report.                                                          ==
-- =============================================================================================

-- ChangeLog
--    3/25/2003
--       Created from rpt_DailyDeposits

-- Suppress intermediate results
set nocount on

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert (datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

if @counselor is not null
begin
	if not exists (select * from counselors where counselor = @counselor)
		select	@counselor = 0
end

-- If there is no counselor then do not select based upon the counselor
if isnull(@counselor,0) <= 0
begin
	SELECT
		tr.bank										as 'bank',
		convert(datetime, convert(varchar(10), d.date_created,101) + ' 00:00:00')	as 'date',
		b.description									as 'bank_name',
		d.trust_register								as 'trust_register',
		convert(money,sum(isnull(d.credit_amt,0)))		as 'amount',
		tr.sequence_number								as 'deposit_label',
		convert(int, 0)									as 'counselor'
	
	FROM		registers_client d	WITH (NOLOCK)
	LEFT OUTER JOIN registers_trust tr	WITH (NOLOCK) ON d.trust_register = tr.trust_register
	LEFT OUTER JOIN banks b			WITH (NOLOCK) ON tr.bank = b.bank
	
	WHERE		d.date_created BETWEEN @FromDate AND @ToDate
	AND		d.tran_type = 'DP'
	GROUP BY	d.trust_register, tr.bank, b.description, convert(datetime, convert(varchar(10), d.date_created,101) + ' 00:00:00'), tr.sequence_number
	ORDER BY	1, 2, 3, 4

-- Otherwise, select only the clients which match the counselor.
end else begin

	SELECT
		tr.bank										as 'bank',
		convert(datetime, convert(varchar(10), d.date_created,101) + ' 00:00:00')	as 'date',
		b.description									as 'bank_name',
		d.trust_register								as 'trust_register',
		convert(money,sum(isnull(d.credit_amt,0)))		as 'amount',
		tr.sequence_number								as 'deposit_label',
		convert(int, @counselor)						as 'counselor'

	FROM		registers_client d	WITH (NOLOCK)
	INNER JOIN	clients c		WITH (NOLOCK) ON d.client = c.client
	LEFT OUTER JOIN registers_trust tr	WITH (NOLOCK) ON d.trust_register = tr.trust_register
	LEFT OUTER JOIN banks b			WITH (NOLOCK) ON tr.bank = b.bank

	WHERE		d.date_created BETWEEN @FromDate AND @ToDate
	AND		d.tran_type = 'DP'
	AND		c.counselor = @counselor
	GROUP BY	d.trust_register, tr.bank, b.description, convert(datetime, convert(varchar(10), d.date_created,101) + ' 00:00:00'), tr.sequence_number
	ORDER BY	1, 2, 3, 4
end

RETURN ( @@rowcount )
GO
