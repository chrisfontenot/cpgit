USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdv_generate]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdv_generate] ( @rpps_file as int ) AS

-- ===================================================================================================
-- ==            Generate transactions for the RPPS balance verifications                           ==
-- ===================================================================================================

-- ChangeLog
--   10/2/2002
--      Use the "suppress_bal_verify" flag in the creditor to suppress balance verifications
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   12/07/2002
--     Added support for balance_verify. Dropped "suppress_bal_verify" flag.
--   6/2/2003
--     Change to support "dual" (both proposals and payments) billers
--   11/24/2003
--     Added "@bank" as a parameter for the time being. It is optional.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id
--   6/1/2012
--      Set the death_date when creating the transaction

-- Find the bank number for the creditors
declare	@bank		int

select	@bank		= bank
from	rpps_files with (nolock)
where	rpps_file	= @rpps_file

if isnull(@bank,0) <= 0
begin
	RaisError ('The file is not associated with a bank number. Please use the new procedures.', 16, 1)
	return ( 0 )
end

-- Start a transaction for the operation
BEGIN TRANSACTION

-- Disable intermediate result sets
SET NOCOUNT ON

-- Create a working table for the drop notices
create table #client_creditor (client int, creditor varchar(10), client_creditor int, creditor_id int, region int, rpps_biller_id varchar(10) null, account_number varchar(80) null, drop_reason int null, creditor_method int null, send_bal_verify int, old_balance money null)

-- Build a list of the balance verifications to go out via RPPS
INSERT INTO	#client_creditor (client, creditor, client_creditor, creditor_id, region, drop_reason, account_number, send_bal_verify, old_balance)
SELECT		cc.client, cc.creditor, cc.client_creditor, cr.creditor_id, c.region, c.drop_reason, account_number, send_bal_verify, bal.orig_balance+bal.orig_balance_adjustment+bal.total_interest-bal.total_payments as old_balance
FROM		client_creditor cc
INNER JOIN	clients c		with (nolock) on cc.client = c.client
INNER JOIN	creditors cr	with (nolock) on cc.creditor = cr.creditor
INNER JOIN  client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
WHERE		cc.reassigned_debt = 0
AND			cc.send_bal_verify in (1, 2)
AND			cc.drop_date is null
AND			cc.drop_reason is null
AND			cc.account_number is not null
AND			c.active_status not in ('CRE', 'I')

-- Create a couple of indexes to help speed things along
create index temp_ix_client_creditor_3 on #client_creditor ( creditor_id );
create index temp_ix_client_creditor_4 on #client_creditor ( region );

-- Locate the appropriate mask for the billers to be sent
update		#client_creditor
set			creditor_method	= mt.creditor_method,
			rpps_biller_id	= ids.rpps_biller_id
from		#client_creditor x
inner join creditor_methods mt	with (nolock) on x.creditor_id = mt.creditor and mt.region in (0, x.region) and mt.type = 'BAL'
inner join rpps_biller_ids ids	with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
inner join rpps_masks m		with (nolock) on ids.rpps_biller_id = m.rpps_biller_id
where		ids.biller_type in (4, 10, 11, 12, 13, 14)
and			mt.bank		= @bank
and			x.account_number like dbo.map_rpps_masks ( m.mask )

-- Items that have no methods associated with them are to be turned into "normal" mode
-- there is no reason to suppress the proposals as this time since there is no prenote
-- balance verification that is allowed
update		client_creditor
set			send_bal_verify = 0
from		client_creditor cc with (nolock)
inner join	#client_creditor x on cc.client_creditor = x.client_creditor
where		x.creditor_method is null
and			x.send_bal_verify = 2;

-- Discard the items that have no creditor method associated with them
delete		#client_creditor
where		creditor_method is null;

-- Clear the flags that will be sent
update		client_creditor
set			send_bal_verify			= 0,
			verify_request_date		= getdate()
from		client_creditor cc
inner join	#client_creditor x on cc.client_creditor = x.client_creditor
and			x.send_bal_verify		= 1;

-- Indicate that we are sending the prenote balance verify operation
update		client_creditor
set			send_bal_verify			= 3,
			proposal_prenote_date	= getdate()
from		client_creditor cc
inner join	#client_creditor x on cc.client_creditor = x.client_creditor
and			x.send_bal_verify		= 2;

-- Date when the transactions are to expire
declare		@death_date		datetime
select		@death_date	= dateadd(d, 180, getdate())

-- Generate the CDV transaction for the balance verification
insert into	rpps_transactions (trace_number_first, trace_number_last, client, creditor, client_creditor, biller_id, transaction_code, service_class_or_purpose, death_date, prenote_status, bank, rpps_file, old_balance)
select	null						as trace_number_first,
		null						as trace_number_last,
		client						as client,
		creditor					as creditor,
		client_creditor				as client_creditor,
		rpps_biller_id				as rpps_biller_id,
		23							as transaction_code,
		'CDV'						as service_class_or_purpose,
		@death_date					as death_date,
		send_bal_verify				as prenote_status,
		@bank						as bank,
		@rpps_file					as rpps_file,
		old_balance					as old_balance
from	#client_creditor

-- Discard the working table
drop table	#client_creditor

-- Make the changes permanent
COMMIT TRANSACTION

RETURN ( 1 )
GO
