USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_statistics_trust]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_statistics_trust] ( @FromDate AS smalldatetime, @ToDate as smalldatetime ) AS

-- ==================================================================================================
-- ==                 Update the daily statistics with the trust information                       ==
-- ==================================================================================================

-- Do general configuration options
SET NOCOUNT ON
SET ANSI_WARNINGS OFF
BEGIN TRANSACTION

-- ==================================================================================================
-- ==                  Generate the statistic information for the month                            ==
-- ==================================================================================================

DECLARE @active_trust_balance	MONEY
DECLARE @inactive_trust_balance	MONEY
DECLARE @client_0		MONEY

SELECT	@inactive_trust_balance	= sum(held_in_trust)
FROM	clients

SELECT	@active_trust_balance = sum(held_in_trust)
FROM	clients
WHERE	active_status IN ('A', 'AR')

SELECT	@inactive_trust_balance = @inactive_trust_balance - @active_trust_balance

SELECT	@client_0 = held_in_trust
from	clients
where	client = 0

-- ==================================================================================================
-- ==         Update the statistics for the day                                                    ==
-- ==================================================================================================

UPDATE	[statistics]

SET	trust_active	= isnull(@active_trust_balance,0),
	trust_inactive	= isnull(@inactive_trust_balance,0),
	client_0	= isnull(@client_0, 0)

WHERE	period_start	= @FromDate
AND	period_stop	= @ToDate

-- Return success to the caller
COMMIT TRANSACTION
RETURN ( 1 )
GO
