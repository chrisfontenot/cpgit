USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_pif]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_pif] ( @creditor as typ_creditor, @amount as money = 0.0, @message as typ_description = null ) AS

-- =====================================================================================================================
-- ==            Pay the invoices in full from the oldest invoice to the newest                                       ==
-- =====================================================================================================================

-- ChageLog
--   2/20/2002
--     Changed to xpr_creditor_contribution_pif to make similar to the other items in the database.
--   10/9/2002
--     Maintained creditor payment statistic
--   3/1/2011
--     Removed cursor update logic

-- Initialize
set nocount on

declare item_cursor cursor for
	select	invoice_register, pmt_amount, pmt_date, isnull(inv_amount,0)-isnull(pmt_amount,0)-isnull(adj_amount,0) as 'balance'
	from	registers_invoices with (nolock)
	where	isnull(inv_amount,0) > (isnull(pmt_amount,0) + isnull(adj_amount,0))
	and		creditor = @creditor
	order by inv_date

open item_cursor
declare	@invoice_register	typ_key
declare	@pmt_amount		money
declare	@pmt_date		datetime
declare	@balance		money

fetch item_cursor into @invoice_register, @pmt_amount, @pmt_date, @balance
while @@fetch_status = 0
begin
	-- Calculate the amount that may be paid
	if @balance > @amount
		select @balance = @amount

	if @balance > 0
	begin
		-- Remove the money from the available funds
		select	@amount = @amount - @balance

		-- Pay the invoice
		update	registers_invoices
		set		pmt_amount	= pmt_amount + @balance,
				pmt_date	= getdate()
		where	invoice_register = @invoice_register

		-- Record the payment into the creditor list
		insert into registers_creditor	(tran_type,	creditor,	credit_amt,	invoice_register,	message)
		values				('RC',		@creditor,	@balance,	@invoice_register,	@message)

		-- Update the statistics for the creditor
		update	creditors
		set		contrib_mtd_received	= isnull(contrib_mtd_received,0) + @balance
		where	creditor				= @creditor
	end

	-- Fetch the next row from the table
	fetch item_cursor into @invoice_register, @pmt_amount, @pmt_date, @balance
end

close item_cursor
deallocate item_cursor

-- Return the amount of money left
select	@amount		as 'left_over'
return ( 1 )
GO
