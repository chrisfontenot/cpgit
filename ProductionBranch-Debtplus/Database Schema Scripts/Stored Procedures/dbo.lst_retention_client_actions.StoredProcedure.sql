USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_retention_client_actions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_retention_client_actions] ( @client_retention_event as typ_key ) AS

-- ==========================================================================================================================
-- ==            Return the information for the actions of an event                                                        ==
-- ==========================================================================================================================

SELECT
	a.client_retention_action		as 'item_key',
	d.description				as 'description',
	a.created_by				as 'created_by',
	a.date_created				as 'date_created',
	a.message				as 'message'

FROM	client_retention_actions a with (nolock)
INNER JOIN retention_actions d with (nolock) on a.retention_action = d.retention_action

WHERE	client_retention_event = @client_retention_event
order by 2

return ( @@rowcount )
GO
