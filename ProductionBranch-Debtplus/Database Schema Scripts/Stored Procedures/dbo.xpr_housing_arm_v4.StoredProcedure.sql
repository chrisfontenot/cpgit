SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_housing_arm_v4] ( @period_start as datetime, @period_end as datetime ) as

-- Suppress intermediate results (there are many)
set nocount on

-- Remove the time values from the dates. Make the ending date 1 day later
select	@period_start		= CONVERT(varchar(10), @period_start, 101),
		@period_end			= CONVERT(varchar(10), dateadd(day, 1, @period_end), 101)

-- Create the table to hold the client information
create table #hud_9902_clients (
	
	-- Information needed for the summary extract
	Compl_Help_FairHousing_Workshop										int null,
	Compl_HomeBuyer_Educ_Workshop										int null,
	Compl_HomeMaint_FinMngt												int null,
	Compl_Other_Workshop												int null,
	Compl_Resolv_Prevent_Mortg_Deliq									int null,
	Compl_Workshop_HomeFin_Credit_Repair								int null,
	Compl_Workshop_Predatory_Lend										int null,
	Counseling_Rental_Workshop											int null,
	
	Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling			int null,
	Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling		int null,
	Count_HomeMaintenance_Fin_Management_Consumer_Loan						int null,
	Count_HomeMaintenance_Fin_Management_HECM_Counseled						int null,
	Count_HomeMaintenance_Fin_Management_HECM_Obtained						int null,
	Count_HomeMaintenance_Fin_Management_Homeequity_Loan						int null,
	Count_HomeMaintenance_Fin_Management_Legal_Assistance						int null,
	Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced					int null,
	Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage					int null,
	Count_HomeMaintenance_Fin_Management_Other								int null,
	Count_HomeMaintenance_Fin_Management_Other_Social_Agency					int null,
	Count_HomeMaintenance_Fin_Management_Receiving_Counseling					int null,
	Count_HomeMaintenance_Fin_Management_Sold_House							int null,
	Count_HomeMaintenance_Fin_Management_Utilities_Current					int null,
	Count_HomeMaintenance_Fin_Management_Withdrew_Counseling					int null,
	
	Count_Occupied_Emergency_Shelter											int null,
	Count_Occupied_PermntHouse_wo_RentAssist									int null,
	Count_Occupied_Permnthouse_RentAssist										int null,
	Count_Occupied_Transitional_Housing										int null,
	Count_Remained_Homeless													int null,
	Count_Seek_Shelter_Other													int null,
	Count_Withdrew_Counseling													int null,
	Count_Counseled_Ref_Other_SocAgency										int null,
	Count_Currently_Receiving_Counsel											int null,
	
	Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog					int null,
	Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling					int null,
	Count_Prepurchase_Homebuyer_Counsel_Other									int null,
	Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase				int null,
	Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew					int null,
	Count_Prepurchase_Homebuyer_Counsel_PurchHousing							int null,
	Count_Prepurchase_Homebuyer_Counsel_Readyafter_90							int null,
	Count_Prepurchase_Homebuyer_Counsel_Within_90								int null,
	
	Count_Prevent_Mortgage_Deliquency_Bankcruptcy								int null,
	Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention			int null,
	Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan						int null,
	Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu						int null,
	Count_Prevent_Mortgage_Deliquency_Mortgage_Current						int null,
	Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed						int null,
	Count_Prevent_Mortgage_Deliquency_Mortgage_Modified						int null,
	Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced						int null,
	Count_Prevent_Mortgage_Deliquency_Other									int null,
	Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender				int null,
	Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale					int null,
	Count_Prevent_Mortgage_Deliquency_Referred_to_Legal						int null,
	Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv					int null,
	Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated				int null,
	Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received				int null,
	Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative				int null,
	Count_Prevent_Mortgage_Deliquency_Withdrew								int null,
	
	Count_Seeking_Help_Housing_Current_Counseled								int null,
	Count_Seeking_Help_Housing_Debt_Mngmt_Entered								int null,
	Count_Seeking_Help_Housing_Found_Alternative_Housing						int null,
	Count_Seeking_Help_Housing_Other											int null,
	Count_Seeking_Help_Housing_Recertification_Subsidy_Program				int null,
	Count_Seeking_Help_Housing_Referred_Legal_Agency_Eviction					int null,
	Count_Seeking_Help_Housing_Referred_Legal_Aid_Agency						int null,
	Count_Seeking_Help_Housing_Referred_Other_Social_Agency					int null,
	Count_Seeking_Help_Housing_Referred_to_Rental_Assistance					int null,
	Count_Seeking_Help_Housing_Remain_CurrentHousing							int null,
	Count_Seeking_Help_Housing_Resolved_Issue									int null,
	Count_Seeking_Help_Housing_Search_Assistance								int null,
	Count_Seeking_Help_Housing_Security_Dep_Dispute							int null,
	Count_Seeking_Help_Housing_Temp_Rental_Relief								int null,
	Count_Seeking_Help_Housing_Utilities_Current								int null,
	Count_Seeking_Help_Housing_Withdrew_Counseling							int null,
	
	Ethnicity_Clients_Counseling_Hispanic								int null,
	Ethnicity_Clients_Counseling_No_Response							int null,
	Ethnicity_Clients_Counseling_Non_Hispanic							int null,
	
	AMI_No_Response														int null,
	Greater100_AMI_Level												int null,
	Lesser50_AMI_Level													int null,
	a50_79_AMI_Level													int null,
	a80_100_AMI_Level													int null,
	
	MultiRace_Clients_Counseling_AMINDWHT								int null,
	MultiRace_Clients_Counseling_AMRCINDBLK								int null,
	MultiRace_Clients_Counseling_ASIANWHT								int null,
	MultiRace_Clients_Counseling_BLKWHT									int null,
	MultiRace_Clients_Counseling_NoResponse								int null,
	MultiRace_Clients_Counseling_OtherMLTRC								int null,
	Race_Clients_Counseling_American_Indian_Alaskan_Native				int null,
	Race_Clients_Counseling_Asian										int null,
	Race_Clients_Counseling_Black_AfricanAmerican						int null,
	Race_Clients_Counseling_Pacific_Islanders							int null,
	Race_Clients_Counseling_White										int null,

	-- Information needed for the client extract
	Client_ID_Num														int,
	Client_Purpose_Of_Visit												int null,
	Client_Outcome_Of_Visit												int null,
	Client_Case_Num														int null,
	Client_Counselor_ID													int null,
	Client_Head_Of_Household_Type										int null,
	Client_Credit_Score													int null,
	Client_Credit_Score_Source											int null,
	Client_No_Credit_Score_Reason										int null,
	Client_First_Name													varchar(80) null,
	Client_Last_Name													varchar(80) null,
	Client_Middle_Name													varchar(80) null,
	Client_Street_Address_1												varchar(256) null,
	Client_Street_Address_2												varchar(256) null,
	Client_City															varchar(256) null,
	Client_State														int null,
	Client_ZipCode														varchar(256) null,
	Client_New_Street_Address_1											varchar(256) null,
	Client_New_Street_Address_2											varchar(256) null,
	Client_New_City														varchar(256) null,
	Client_New_State													int null,
	Client_New_ZipCode													varchar(256) null,
	Client_Spouse_First_Name											varchar(80) null,
	Client_Spouse_Last_Name												varchar(80) null,
	Client_Spouse_Middle_Name											varchar(80) null,
	Client_Farm_Worker													bit null,
	Client_Colonias_Resident											bit null,
	Client_Disabled														bit null,
	Client_HECM_Certificate												bit null,
	Client_Predatory_Lending											bit null,
	Client_FirstTime_Home_Buyer											bit null,
	Client_Discrimination_Victim										bit null,
	Client_Mortgage_Deliquency											bit null,
	Client_Second_Loan_Exists											bit null,
	Client_Intake_Loan_Type_Is_Hybrid_ARM								bit null,
	Client_Intake_Loan_Type_Is_Option_ARM								bit null,
	Client_Intake_Loan_Type_Is_Interest_Only							bit null,
	Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured						bit null,
	Client_Intake_Loan_Type_Is_Privately_Held							bit null,
	Client_Intake_Loan_Type_Has_Interest_Rate_Reset						bit null,
	Client_Counsel_Session_DT_Start										datetime null,
	Client_Counsel_Session_DT_End										datetime null,
	Client_Intake_DT													datetime null,
	Client_Birth_DT														datetime null,
	Client_HECM_Certificate_Issue_Date									datetime null,
	Client_HECM_Certificate_Expiration_Date								datetime null,
	Client_HECM_Certificate_ID											varchar(80) null,
	Client_Sales_Contract_Signed										datetime null,
	Client_Grant_Amount_Used											money null,
	Client_Mortgage_Closing_Cost										money null,
	Client_Mortgage_Interest_Rate										float null,
	Client_Counseling_Termination										int null,
	Client_Income_Level													int null,
	Client_Highest_Educ_Grade											int null,
	Client_HUD_Assistance												int null,
	Client_Dependents_Num												int null,
	Client_Language_Spoken												int null,
	Client_Session_Duration												int null,
	Client_Counseling_Type												int null,
	Client_Counseling_Fee												money null,
	Client_Attribute_HUD_Grant											int null,
	Client_Finance_Type_Before											int null,
	Client_Finance_Type_After											int null,
	Client_Mortgage_Type												int null,
	Client_Mortgage_Type_After											int null,
	Client_Referred_By													int null,
	Client_Job_Duration													int null,
	Client_Household_Debt												int null,
	Client_Loan_Being_Reported											varchar(1) null,
	Client_Intake_Loan_Type												int null,
	Client_Family_Size													int null,
	Client_Marital_Status												int null,
	Client_Race_ID														int null,
	Client_Ethnicity_ID													int null,
	Client_Household_Gross_Monthly_Income								money null,
	Client_Gender														varchar(10) null,
	Client_Spouse_SSN													varchar(20) null,
	Client_SSN1															varchar(20) null,
	Client_SSN2															varchar(20) null,
	Client_Mobile_Phone_Num												varchar(80) null,
	Client_Phone_Num													varchar(80) null,
	Client_Fax															varchar(80) null,
	Client_Email														varchar(256) null,

	-- Other values related to finding data in our database	
	office																int null,
	hcs_id																int null,
	group_session_id													int null,
	person_1															int null,
	person_2															int null,
	hud_interview														int null,
	interview_type														int null,
	hud_result															int null,
	interview_date														datetime null,
	result_date															datetime null,
	termination_date													datetime null,
	interview_result													varchar(80) null,
	ami																	money null,
	client_appointment													int null,
	housing_property													int null,
	primary_loan														int null,
	secondary_loan														int null,
	is_client															int null,
	is_workshop															int null
);

-- Load the possible list with everyone who has ever had an interview
insert into #hud_9902_clients ( Client_ID_Num, is_client, is_workshop )
select distinct client, 2, 0
from	hud_interviews

-- We want to include any interview that has time in the period specified.
select	t.hud_interview, i.client, SUM(t.[minutes]) as minutes
into	#times
from	hud_transactions t with (nolock)
inner join hud_interviews i with (nolock) on t.hud_interview = i.hud_interview
where	t.date_created	>= @period_start
and		t.date_created	<  @period_end
group by t.hud_interview, i.client;

update	#hud_9902_clients
set		Client_Session_Duration		= b.minutes,
		hud_interview				= b.hud_interview
from	#hud_9902_clients x
inner join #times b on x.client_id_num = b.client;

drop table #times

-- Include any interview that may not have time but is in the date indicated.
update	#hud_9902_clients
set		hud_interview				= i.hud_interview
from	#hud_9902_clients c
inner join hud_interviews i on c.Client_ID_Num = i.client
where	i.interview_date >= @period_start
and		i.interview_date <  @period_end
and		c.hud_interview is null;

-- Toss all of the other clients from the list
delete
from	#hud_9902_clients
where	isnull(hud_interview,0) = 0;

-- Set the other parameters based upon the interview selected
update	#hud_9902_clients
set		hud_result						= i.hud_result,
		interview_type					= i.interview_type,
		interview_date					= i.interview_date,
		result_date						= i.result_date,
		termination_date				= i.termination_date,
		Client_Counsel_Session_DT_Start	= i.interview_date,
		Client_Intake_DT				= i.interview_date,
		Client_Counsel_Session_DT_End	= i.result_date,
		Client_Counseling_Fee			= i.HousingFeeAmount
from	#hud_9902_clients x
inner join hud_interviews i on x.hud_interview = i.hud_interview;

-- Set the referrral information since the appointments have there own values
update	#hud_9902_clients
set		Client_Referred_By		= dbo.map_hud_9902_referred_by ( c.referred_by ),
		Client_Counselor_ID		= c.counselor
from	#hud_9902_clients x
inner join clients c on c.client = x.Client_ID_Num;

-- Create the table for the workshops
create table #hud_9902_workshops (
	group_session_id					int,
	group_session_counselor_id			int,
	group_session_title					varchar(80) null,
	group_session_date					datetime,
	group_session_duration				int null,
	group_session_type					varchar(80) null,
	group_session_attribute_hud_grant	varchar(80) null,
	workshop_type						int
	);

insert into #hud_9902_workshops ([group_session_id], [group_session_date], [group_session_title], [workshop_type], [group_session_duration], [group_session_counselor_id], [group_session_attribute_hud_grant])
select	w.workshop				as group_session_id,
		w.start_time			as group_session_date,
		left(t.description,50)	as group_session_title,
		t.workshop_type			as workshop_type,
		t.duration				as group_session_duration,
		w.CounselorID			as group_session_counselor_id,
		w.HUD_Grant             as group_session_attribute_hud_grant
from	workshops w with (nolock)
inner join workshop_types t with (nolock) on w.workshop_type = t.workshop_type
where	w.start_time >= @period_start
and		w.start_time <  @period_end

-- Update the content type with the first item that can be used
update	#hud_9902_workshops
set		[group_session_type]	= t.hud_9902_section
from	#hud_9902_workshops x
inner join workshop_contents c on x.workshop_type = c.workshop_type
inner join workshop_content_types t on c.content_type = t.content_type
where	t.hud_9902_section is not null;

-- Discard the items that we can not use
delete
from	#hud_9902_workshops
where	[group_session_counselor_id] is null
or		[group_session_type] is null
or		[group_session_attribute_hud_grant] is null;

insert into #hud_9902_clients ( client_id_num, is_client, is_workshop, Client_Counseling_Fee, Client_Referred_By, group_session_id, Client_Counselor_ID, Client_Counseling_Termination, client_appointment, Compl_Other_Workshop )
select	ca.client				 as 'Attendee_id',
		0						 as 'is_client',
		1						 as 'is_workshop',
		ca.HousingFeeAmount		 as 'Attendee_Fee_Amount',
		dbo.map_hud_9902_referred_by ( ca.referred_by ) 			 as 'Attendee_Referred_By',
		ca.workshop				 as 'group_session_id',
		ca.counselor			 as 'Client_Counselor_ID',
		1						 as 'Client_Counseling_Termination',
		ca.client_appointment    as 'client_appointment',
		1						 as 'Compl_Other_Workshop'
FROM	client_appointments ca
inner join #hud_9902_workshops w on ca.workshop = w.group_session_id
where	ca.status in ('K','W');

-- calculate the AMI value for the clients
update	#hud_9902_clients
set		ami										= isnull(dbo.ami ( Client_ID_Num ),0),
		Client_Household_Gross_Monthly_Income	= isnull(dbo.client_gross_income ( Client_ID_Num ),0) + isnull(dbo.client_other_income ( Client_ID_Num ) / 12.0,0),
		Client_Income_Level						= 2;																-- Assume < 50% for everyone

-- Determine the various levels needed. These need to be annual figures so scale by months/year.
update	#hud_9902_clients set Client_Income_Level = 3 where (Client_Household_Gross_Monthly_Income * 12.0) > ami;			-- more than 50 percent
update	#hud_9902_clients set Client_Income_Level = 4 where (Client_Household_Gross_Monthly_Income * 12.0) > ami * 1.60;	-- more than 80 percent
update	#hud_9902_clients set Client_Income_Level = 5 where (Client_Household_Gross_Monthly_Income * 12.0) > ami * 2.00;	-- more than 100 percent
update	#hud_9902_clients set Client_Income_Level = 6 where ami = 0;												-- choose not to respond

-- Determine the information from the home address data
update	#hud_9902_clients
set		Client_City					= a.city,
		Client_ZipCode				= LEFT(a.postalcode, 5),
		Client_Street_Address_1		= dbo.format_Address_Line_1 ( a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),
		Client_Street_Address_2		= a.address_line_2,
		Client_State				= dbo.map_hud_9902_state ( a.state )
from	#hud_9902_clients x
inner join clients c on x.Client_ID_Num = c.client
inner join addresses a on c.AddressID = a.address

-- Determine the marital status, etc. from the client
update	#hud_9902_clients
set		Client_Marital_Status			= dbo.map_hud_9902_marital ( c.marital_status ),
		Client_Language_Spoken			= dbo.map_hud_9902_language ( c.language ),
		Client_Family_Size				= dbo.family_size ( c.client ),
		Client_Dependents_Num			= c.dependents,
		Client_Head_Of_Household_Type	= dbo.map_hud_9902_HouseHoldHead ( c.Household ),
		office							= c.office
from	#hud_9902_clients x
inner join clients c on x.Client_ID_Num = c.client;

-- Determine the applicant and co-applicant record addresses
update	#hud_9902_clients set person_1 = p.person from #hud_9902_clients x inner join people p on x.client_id_num = p.client and 1 = p.relation;
update	#hud_9902_clients set person_2 = p.person from #hud_9902_clients x inner join people p on x.client_id_num = p.client and 1 <> p.relation;

-- Obtain the information about the job data, ssn, race, gender, and ethnicity
update	#hud_9902_clients
set		Client_Job_Duration			= DATEDIFF(MONTH, p.emp_start_date, GETDATE()),
		Client_Birth_DT				= p.Birthdate,
		Client_SSN1					= p.ssn,
		Client_Race_ID				= dbo.map_hud_9902_Race(p.race, p.ethnicity),
		Client_Ethnicity_ID			= dbo.map_hud_9902_Ethnicity(p.ethnicity,p.race),
		Client_Gender				= case p.gender when 1 then 'M' else 'F' end,
		Client_Credit_Score			= p.FICO_Score,
		Client_Credit_Score_Source	= dbo.map_hud_9902_credit_agency ( p.CreditAgency ),
		Client_Highest_Educ_Grade	= dbo.map_hud_9902_education ( p.education ),
		Client_Disabled				= p.[Disabled]
from	#hud_9902_clients x
inner join people p on x.person_1 = p.Person;

-- Find the last 4 digits of the client's ssn
update	#hud_9902_clients
set		Client_SSN2		= RIGHT(client_ssn1,4)

-- Set the email address
update	#hud_9902_clients
set		Client_Email				= e.Address
from	#hud_9902_clients x
inner join people p on x.person_1 = p.Person
inner join EmailAddresses e on p.EmailID = e.Email;

update	#hud_9902_clients
set		Client_Credit_Score				= null,
		Client_Credit_Score_Source		= null,
		Client_No_Credit_Score_Reason	= 3
where	isnull(Client_Credit_Score,0)	= 0;

-- Use the termination date if one is given
update	#hud_9902_clients
set		Client_Job_Duration = DATEDIFF(month, p.emp_end_date, p.emp_start_date)
from	#hud_9902_clients x
inner join people p on x.person_1 = p.Person
where	p.emp_end_date is not null;

-- Define the client name
update	#hud_9902_clients
set		Client_First_Name		= n.first,
		Client_Middle_Name		= n.middle,
		Client_Last_Name		= n.last
from	#hud_9902_clients x
inner join people p on x.person_1 = p.Person
inner join Names n on p.nameID = n.Name;

-- Find the spouse's name
update	#hud_9902_clients
set		Client_Spouse_First_Name	= n.first,
		Client_Spouse_Middle_Name	= n.middle,
		Client_Spouse_Last_Name		= n.last
from	#hud_9902_clients x
inner join people p on x.person_2 = p.Person
inner join Names n on p.nameID = n.Name;

-- Find the spouse's SSN
update	#hud_9902_clients
set		Client_Spouse_SSN			= p.ssn
from	#hud_9902_clients x
inner join people p on x.person_2 = p.Person

-- Find the cell telephone number
update	#hud_9902_clients
set		Client_Mobile_Phone_Num	= dbo.format_hud_9902_TelephoneNumber ( p.CellTelephoneID )
from	#hud_9902_clients x
inner join people p on x.person_1	= p.Person;

-- Find the home telephone number
update	#hud_9902_clients
set		Client_Phone_Num			= dbo.format_hud_9902_TelephoneNumber ( c.HomeTelephoneID )
from	#hud_9902_clients x
inner join clients c on x.client_id_num	= c.client;

-- Update information from the housing record
update	#hud_9902_clients
set		Client_Colonias_Resident				= c.HUD_colonias,
		Client_Farm_Worker						= c.HUD_migrant_farm_worker,
		Client_Predatory_Lending				= c.HUD_predatory_lending,
		Client_FirstTime_Home_Buyer				= c.HUD_FirstTimeHomeBuyer,
		Client_Discrimination_Victim			= c.HUD_DiscriminationVictim,
		Client_HUD_Assistance					= c.HUD_Assistance,
		Client_HECM_Certificate					= c.HECM_certificate_id,
		Client_HECM_Certificate_Expiration_Date	= c.HECM_Certificate_expires,
		Client_HECM_Certificate_Issue_Date		= c.HECM_Certificate_Date,
		Client_Attribute_HUD_Grant				= dbo.map_hud_9902_grant ( c.HUD_grant )
from	#hud_9902_clients x
inner join client_housing c on x.Client_ID_Num = c.client

-- Locate the property for the client
update	#hud_9902_clients
set		housing_property				= p.oID,
		Client_Sales_Contract_Signed	= p.Sales_contract_date
from	#hud_9902_clients x
inner join housing_properties p on x.Client_ID_Num = p.HousingID
where	p.UseInReports		= 1;

-- Set the loan position
update	#hud_9902_clients
set		primary_loan		= l.oID
from	#hud_9902_clients x
inner join housing_properties p on x.housing_property = p.oID
inner join housing_loans l on p.oID = l.PropertyID
where	l.UseInReports		= 1;

-- Find the secondary position
update	#hud_9902_clients
set		secondary_loan		= l.oID
from	#hud_9902_clients x
inner join housing_properties p on x.housing_property = p.oID
inner join housing_loans l on p.oID = l.PropertyID
where	l.UseInReports		= 0;

-- Look for the status of the loan being reports
update	#hud_9902_clients
set		Client_Loan_Being_Reported	= 'F'

update	#hud_9902_clients
set		Client_Loan_Being_Reported	= 'S'
from	#hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
where	l.Loan1st2nd		= 2;

-- Determine if there is a secondary loan
update	#hud_9902_clients
set		Client_Second_Loan_Exists	= 0;

update	#hud_9902_clients
set		Client_Second_Loan_Exists	= 1
where	secondary_loan				is not null;

update	#hud_9902_clients
set		Client_Second_Loan_Exists	= 1
where	Client_Loan_Being_Reported	= 'S';

-- Find the type of the loan from the primary load information
update	#hud_9902_clients
set		Client_Intake_Loan_Type_Is_Hybrid_ARM		= d.Hybrid_ARM_Loan,
		Client_Intake_Loan_Type_Is_Interest_Only	= d.Interest_Only_Loan,
		Client_Mortgage_Closing_Cost				= l.MortgageClosingCosts,
		Client_Intake_Loan_Type_Is_Option_ARM		= d.Option_ARM_Loan,
		Client_Intake_Loan_Type_Is_Privately_Held	= d.Privately_Held_Loan,
		Client_Intake_Loan_Type						= dbo.map_hud_9902_LoanType( d.MortgageTypeCD ),
		Client_Mortgage_Interest_Rate				= d.InterestRate
from	#hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
left outer join housing_loan_details d on l.IntakeDetailID = d.oID

-- Set the FHA/VA insured loan status
update	#hud_9902_clients
set		Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured	= 1
from	#hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
inner join housing_loan_details d on l.IntakeDetailID = d.oID
where	d.FHA_VA_Insured_Loan <> 0

-- Set the ARM reset status
update	#hud_9902_clients
set		Client_Intake_Loan_Type_Has_Interest_Rate_Reset	= 1
from	#hud_9902_clients x
inner join housing_loans l on x.primary_loan = l.oID
inner join housing_loan_details d on l.IntakeDetailID = d.oID
where	d.Arm_Reset <> 0;

-- Seed the intake date from the date that the client was created
update	#hud_9902_clients
set		Client_Intake_DT				= c.date_created
from	#hud_9902_clients x
inner join clients c on x.Client_ID_Num = c.client

-- Use the date that the first appointment was created if possible
update	#hud_9902_clients
set		Client_Intake_DT				= ca.date_created
from	#hud_9902_clients x
inner join clients c on x.Client_ID_Num = c.client
inner join client_appointments ca on c.first_appt = ca.client_appointment

-- Finally, set the intake date from the appointment based upon the HUD transaction
update	#hud_9902_clients
set		Client_Intake_DT				= ca.date_created
from	#hud_9902_clients x
inner join hud_transactions t on x.hud_interview = t.hud_interview
inner join client_appointments ca on t.client_appointment	= ca.client_appointment

-- Set the household debt information
update	#hud_9902_clients
set		client_household_debt					= isnull(dbo.client_unsecured_debt_payment ( client_id_num ),0)
												+ isnull(dbo.client_other_debt_payment ( client_id_num ), 0)
												+ isnull(dbo.budget_client_total ( dbo.map_client_to_budget ( client_id_num )), 0)

-- Ensure that there is a valid result for each interview type
update	#hud_9902_clients
set		hud_result					= b.Outcome
from	#hud_9902_clients x
inner join housing_AllowedVisitOutcomeTypes b on x.interview_type = b.PurposeOfVisit
where	b.[Default]					= 1
and		x.hud_result is null;

-- Set the purpose of visit and the outcome
update	#hud_9902_clients
set		Client_Purpose_Of_Visit		= dbo.map_hud_9902_PurposeOfVisit ( interview_type ),
		Client_Outcome_Of_Visit		= dbo.map_hud_9902_VisitOutcome ( interview_type, hud_result ),
		Client_Case_Num				= Client_ID_Num

-- Client counseling type
update	#hud_9902_clients
set		Client_Counseling_Type	= 2;		-- assume "face to face" unless otherwise

-- Correct the otherwise values
update	#hud_9902_clients
set		Client_Counseling_Type	= dbo.map_hud_9902_contact_method ( apt.contact_type )
from	#hud_9902_clients x
inner join hud_transactions t on x.hud_interview = t.hud_interview
inner join client_appointments ca on t.client_appointment = ca.client_appointment
inner join appt_types apt on ca.appt_type = apt.appt_type

-- Group sessions are for workshops
update	#hud_9902_clients
set		Client_Counseling_Type		= 5 -- group
where	is_workshop					= 1;

-- ==============================================================================================
-- ==              SUMMARY INFORMATION                                                         ==
-- ==============================================================================================

-- Ethnicity
update	#hud_9902_clients
set		Ethnicity_Clients_Counseling_Hispanic		= 1
where	Client_Ethnicity_ID							= 2

update	#hud_9902_clients
set		Ethnicity_Clients_Counseling_Non_Hispanic	= 1
where	Client_Ethnicity_ID							= 3

update	#hud_9902_clients
set		Ethnicity_Clients_Counseling_No_Response	= 1
where	isnull(Client_Ethnicity_ID,0)				not in (2, 3)

-- Race
update	#hud_9902_clients
set		MultiRace_Clients_Counseling_AMINDWHT		= 1
where	Client_Race_ID								= 7

update	#hud_9902_clients
set		MultiRace_Clients_Counseling_AMRCINDBLK		= 1
where	Client_Race_ID								= 10

update	#hud_9902_clients
set		MultiRace_Clients_Counseling_ASIANWHT		= 1
where	Client_Race_ID								= 8

update	#hud_9902_clients
set		MultiRace_Clients_Counseling_BLKWHT			= 1
where	Client_Race_ID								= 9

update	#hud_9902_clients
set		MultiRace_Clients_Counseling_OtherMLTRC		= 1
where	Client_Race_ID								= 11

update	#hud_9902_clients
set		Race_Clients_Counseling_American_Indian_Alaskan_Native	= 1
where	Client_Race_ID								= 2

update	#hud_9902_clients
set		Race_Clients_Counseling_Asian				= 1
where	Client_Race_ID								= 3

update	#hud_9902_clients
set		Race_Clients_Counseling_Black_AfricanAmerican	= 1
where	Client_Race_ID								= 4

update	#hud_9902_clients
set		Race_Clients_Counseling_Pacific_Islanders	= 1
where	Client_Race_ID								= 5

update	#hud_9902_clients
set		Race_Clients_Counseling_White				= 1
where	Client_Race_ID								= 6

update	#hud_9902_clients
set		MultiRace_Clients_Counseling_NoResponse		= 1
where	isnull(Client_Race_ID,0)					not in (2, 3, 4, 5, 6, 7, 8, 9, 10, 11)

-- AMI levels
update	#hud_9902_clients set Lesser50_AMI_Level   = 1 where Client_Income_Level = 2;
update	#hud_9902_clients set a50_79_AMI_Level		= 1 where Client_Income_Level = 3;
update	#hud_9902_clients set a80_100_AMI_Level	= 1 where Client_Income_Level = 4;
update	#hud_9902_clients set Greater100_AMI_Level = 1 where Client_Income_Level = 5;
update	#hud_9902_clients set AMI_No_Response		= 1 where Client_Income_Level = 6;

-- ==============================================================================================
-- ==              WORKSHOPS                                                                   ==
-- ==============================================================================================

-- Set the "completed some other workshop" status for the clients
update	#hud_9902_clients
set		Compl_Other_Workshop		= 1
from	#hud_9902_clients
where	is_workshop = 1

-- Now, look for a workshop that says "FairHousing"
update	#hud_9902_clients
set		Compl_Help_FairHousing_Workshop	= 1,
		Compl_Other_Workshop			= 0
from	#hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	wct.hud_9902_section			= 'Help_FairHousing'
and		x.is_workshop = 1

-- Now, look for a workshop that says "HomeBuyer_Educ"
update	#hud_9902_clients
set		Compl_HomeBuyer_Educ_Workshop	= 1,
		Compl_Other_Workshop			= 0
from	#hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	wct.hud_9902_section			= 'HomeBuyer_Educ'
and		x.is_workshop = 1

-- Now, look for a workshop that says "HomeMaint_FinMngt"
update	#hud_9902_clients
set		Compl_HomeMaint_FinMngt	= 1,
		Compl_Other_Workshop	= 0
from	#hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	wct.hud_9902_section			= 'HomeMaint_FinMngt'
and		x.is_workshop = 1

-- Now, look for a workshop that says "Resolv_Prevent_Mortg_Deliq"
update	#hud_9902_clients
set		Compl_Resolv_Prevent_Mortg_Deliq	= 1,
		Compl_Other_Workshop				= 0
from	#hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	wct.hud_9902_section			= 'Resolv_Prevent_Mortg_Deliq'
and		x.is_workshop = 1

-- Now, look for a workshop that says "HomeFin_Credit_Repair"
update	#hud_9902_clients
set		Compl_Workshop_HomeFin_Credit_Repair	= 1,
		Compl_Other_Workshop					= 0
from	#hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	wct.hud_9902_section			= 'HomeFin_Credit_Repair'
and		x.is_workshop = 1

-- Now, look for a workshop that says "Predatory_Lend"
update	#hud_9902_clients
set		Compl_Workshop_Predatory_Lend	= 1,
		Compl_Other_Workshop			= 0
from	#hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	wct.hud_9902_section			= 'Predatory_Lend'
and		x.is_workshop = 1

-- Now, look for a workshop that says "Rental"
update	#hud_9902_clients
set		Counseling_Rental_Workshop	= 1,
		Compl_Other_Workshop		= 0
from	#hud_9902_clients x
inner join client_appointments ca on x.client_appointment = ca.client_appointment
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types wt on w.workshop_type = wt.workshop_type
inner join workshop_contents wc on wt.workshop_type = wc.workshop_type
inner join workshop_content_types wct on wc.content_type = wct.content_type
where	wct.hud_9902_section			= 'Rental'
and		x.is_workshop = 1

-- Finally, set the flags for counting summaries based upon the outcome types
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog] = 1				WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=6 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling] = 1				WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=5 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_Other] = 1							WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=9 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase] = 1			WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=7 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew] = 1			WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=8 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_PurchHousing] = 1					WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=2 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_Readyafter_90] = 1					WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=4 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prepurchase_Homebuyer_Counsel_within_90] = 1						WHERE [Client_Purpose_Of_Visit]=1 AND [Client_Outcome_Of_Visit]=3 and is_client = 2;

update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Bankcruptcy] = 1						WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=13 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention] = 1	WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=16 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan] = 1					WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=14 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu] = 1				WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=7 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Mortgage_Current] = 1					WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=2 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed] = 1				WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=10 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Mortgage_Modified] = 1					WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=4 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced] = 1				WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=3 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Other] = 1								WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=18 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender] = 1			WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=12 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale] = 1				WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=9 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Referred_to_Legal] = 1					WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=15 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv] = 1			WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=11 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated] = 1			WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=6 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received] = 1			WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=5 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative] = 1			WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=8 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Prevent_Mortgage_Deliquency_Withdrew] = 1							WHERE [Client_Purpose_Of_Visit]=2 AND [Client_Outcome_Of_Visit]=17 and is_client = 2;

update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling] = 1		WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=10 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling] = 1	WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=11 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Consumer_Loan] = 1					WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=6 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_HECM_Counseled] = 1					WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=3 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_HECM_Obtained] = 1					WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=2 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Homeequity_Loan] = 1				WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=5 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Legal_Assistance] = 1				WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=13 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced] = 1			WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=7 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage] = 1			WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=4 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Other] = 1							WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=16 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Other_Social_Agency] = 1			WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=8 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Receiving_Counseling] = 1			WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=14 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Sold_House] = 1						WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=9 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Utilities_Current] = 1				WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=12 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_HomeMaintenance_Fin_Management_Withdrew_Counseling] = 1			WHERE [Client_Purpose_Of_Visit]=3 AND [Client_Outcome_Of_Visit]=15 and is_client = 2;

update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Current_Counseled] = 1						WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=15 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Debt_Mngmt_Entered] = 1						WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=12 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Found_Alternative_Housing] = 1				WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=9 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Other] = 1									WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=17 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Recertification_Subsidy_Program] = 1			WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=5 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Referred_Legal_Agency_Eviction] = 1			WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=8 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Referred_Legal_Aid_Agency] = 1				WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=7 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Referred_Other_Social_Agency] = 1				WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=6 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Referred_to_Rental_Assistance] = 1			WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=4 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Remain_Currenthousing] = 1					WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=10 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Resolved_Issue] = 1							WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=11 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Search_Assistance] = 1						WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=2 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Security_Dep_Dispute] = 1						WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=14 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Temp_Rental_Relief] = 1						WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=3 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Utilities_Current] = 1						WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=13 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seeking_Help_Housing_Withdrew_Counseling] = 1						WHERE [Client_Purpose_Of_Visit]=4 AND [Client_Outcome_Of_Visit]=16 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Counseled_Ref_Other_SocAgency] = 1									WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=6 and is_client = 2;

update #hud_9902_clients set is_client = 1, [Count_Currently_Receiving_Counsel] = 1									WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=8 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Occupied_Emergency_Shelter] = 1									WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=2 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Occupied_PermntHouse_RentAssist] = 1								WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=4 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Occupied_PermntHouse_wo_RentAssist] = 1							WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=5 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Occupied_Transitional_Housing] = 1									WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=3 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Remained_Homeless] = 1												WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=7 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Seek_Shelter_Other] = 1											WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=10 and is_client = 2;
update #hud_9902_clients set is_client = 1, [Count_Withdrew_Counseling] = 1											WHERE [Client_Purpose_Of_Visit]=5 AND [Client_Outcome_Of_Visit]=9 and is_client = 2;

-- Toss the client POVs that do not match.
delete from #hud_9902_clients where is_client = 2

-- Map the office to the ID.
update	#hud_9902_clients
set		hcs_id					= o.hud_hcs_id
from	#hud_9902_clients c
inner join offices o on c.office = o.office;

-- Toss all items that do not have an HCS ID as we can not report them
delete
from	#hud_9902_clients
where	isnull(hcs_id,0) <= 0;

-- Since the agency does not want to send SSN values, clear them
update	#hud_9902_clients
set		Client_Spouse_SSN			= null,
		Client_SSN1					= null,
		Client_SSN2					= null

-- Return the current set of results
select	[Compl_Help_FairHousing_Workshop],[Compl_HomeBuyer_Educ_Workshop],[Compl_HomeMaint_FinMngt],[Compl_Other_Workshop],[Compl_Resolv_Prevent_Mortg_Deliq],[Compl_Workshop_HomeFin_Credit_Repair],[Compl_Workshop_Predatory_Lend],[Counseling_Rental_Workshop],
		[Count_HomeMaintenance_Fin_Management_Complete_Budget_Counseling],[Count_HomeMaintenance_Fin_Management_Complete_Maintence_Counseling],[Count_HomeMaintenance_Fin_Management_Consumer_Loan],[Count_HomeMaintenance_Fin_Management_HECM_Counseled],
		[Count_HomeMaintenance_Fin_Management_HECM_Obtained],[Count_HomeMaintenance_Fin_Management_Homeequity_Loan],[Count_HomeMaintenance_Fin_Management_Legal_Assistance],[Count_HomeMaintenance_Fin_Management_Mortgage_Refinanced],
		[Count_HomeMaintenance_Fin_Management_Non_Reverse_Mortgage],[Count_HomeMaintenance_Fin_Management_Other],[Count_HomeMaintenance_Fin_Management_Other_Social_Agency],[Count_HomeMaintenance_Fin_Management_Receiving_Counseling],[Count_HomeMaintenance_Fin_Management_Sold_House],
		[Count_HomeMaintenance_Fin_Management_Utilities_Current],[Count_HomeMaintenance_Fin_Management_Withdrew_Counseling],[Count_Occupied_Emergency_Shelter],[Count_Occupied_PermntHouse_wo_RentAssist],[Count_Occupied_Permnthouse_RentAssist],[Count_Occupied_Transitional_Housing],
		[Count_Remained_Homeless],[Count_Seek_Shelter_Other],[Count_Withdrew_Counseling],[Count_Counseled_Ref_Other_SocAgency],[Count_Currently_Receiving_Counsel],[Count_Prepurchase_Homebuyer_Counsel_Lease_Purchase_Prog],[Count_Prepurchase_Homebuyer_Counsel_Longterm_Counseling],
		[Count_Prepurchase_Homebuyer_Counsel_Other],[Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Nopurchase],[Count_Prepurchase_Homebuyer_Counsel_Prepurchase_Withdrew],[Count_Prepurchase_Homebuyer_Counsel_PurchHousing],[Count_Prepurchase_Homebuyer_Counsel_Readyafter_90],
		[Count_Prepurchase_Homebuyer_Counsel_Within_90],[Count_Prevent_Mortgage_Deliquency_Bankcruptcy],[Count_Prevent_Mortgage_Deliquency_Current_Foreclosure_Prevention],[Count_Prevent_Mortgage_Deliquency_Debt_Manage_Plan],[Count_Prevent_Mortgage_Deliquency_Executed_Deed_Lieu],
		[Count_Prevent_Mortgage_Deliquency_Mortgage_Current],[Count_Prevent_Mortgage_Deliquency_Mortgage_Foreclosed],[Count_Prevent_Mortgage_Deliquency_Mortgage_Modified],[Count_Prevent_Mortgage_Deliquency_Mortgage_Refinanced],[Count_Prevent_Mortgage_Deliquency_Other],
		[Count_Prevent_Mortgage_Deliquency_Partial_Claim_FHA_Lender],[Count_Prevent_Mortgage_Deliquency_Pre_Foreclosure_Sale],[Count_Prevent_Mortgage_Deliquency_Referred_to_Legal],[Count_Prevent_Mortgage_Deliquency_Referred_to_Social_Serv],
		[Count_Prevent_Mortgage_Deliquency_Repayment_Plan_Initiated],[Count_Prevent_Mortgage_Deliquency_Second_Mortgage_Received],[Count_Prevent_Mortgage_Deliquency_Sold_Property_Alternative],[Count_Prevent_Mortgage_Deliquency_Withdrew],
		[Count_Seeking_Help_Housing_Current_Counseled],[Count_Seeking_Help_Housing_Debt_Mngmt_Entered],[Count_Seeking_Help_Housing_Found_Alternative_Housing],[Count_Seeking_Help_Housing_Other],[Count_Seeking_Help_Housing_Recertification_Subsidy_Program],
		[Count_Seeking_Help_Housing_Referred_Legal_Agency_Eviction],[Count_Seeking_Help_Housing_Referred_Legal_Aid_Agency],[Count_Seeking_Help_Housing_Referred_Other_Social_Agency],[Count_Seeking_Help_Housing_Referred_to_Rental_Assistance],
		[Count_Seeking_Help_Housing_Remain_CurrentHousing],[Count_Seeking_Help_Housing_Resolved_Issue],[Count_Seeking_Help_Housing_Search_Assistance],[Count_Seeking_Help_Housing_Security_Dep_Dispute],[Count_Seeking_Help_Housing_Temp_Rental_Relief],
		[Count_Seeking_Help_Housing_Utilities_Current],[Count_Seeking_Help_Housing_Withdrew_Counseling],[Ethnicity_Clients_Counseling_Hispanic],[Ethnicity_Clients_Counseling_No_Response],[Ethnicity_Clients_Counseling_Non_Hispanic],[AMI_No_Response],[Greater100_AMI_Level],
		[Lesser50_AMI_Level],[a50_79_AMI_Level],[a80_100_AMI_Level],[MultiRace_Clients_Counseling_AMINDWHT],[MultiRace_Clients_Counseling_AMRCINDBLK],[MultiRace_Clients_Counseling_ASIANWHT],[MultiRace_Clients_Counseling_BLKWHT],[MultiRace_Clients_Counseling_NoResponse],
		[MultiRace_Clients_Counseling_OtherMLTRC],[Race_Clients_Counseling_American_Indian_Alaskan_Native],[Race_Clients_Counseling_Asian],[Race_Clients_Counseling_Black_AfricanAmerican],[Race_Clients_Counseling_Pacific_Islanders],[Race_Clients_Counseling_White],
		[Client_ID_Num],[Client_Purpose_Of_Visit],[Client_Outcome_Of_Visit],[Client_Case_Num],[Client_Counselor_ID],[Client_Head_Of_Household_Type],[Client_Credit_Score],[Client_Credit_Score_Source],[Client_No_Credit_Score_Reason],[Client_First_Name],[Client_Last_Name],
		[Client_Middle_Name],[Client_Street_Address_1],[Client_Street_Address_2],[Client_City],[Client_State],[Client_ZipCode],[Client_New_Street_Address_1],[Client_New_Street_Address_2],[Client_New_City],[Client_New_State],[Client_New_ZipCode],[Client_Spouse_First_Name],
		[Client_Spouse_Last_Name],[Client_Spouse_Middle_Name],[Client_Farm_Worker],[Client_Colonias_Resident],[Client_Disabled],[Client_HECM_Certificate],[Client_Predatory_Lending],[Client_FirstTime_Home_Buyer],[Client_Discrimination_Victim],[Client_Mortgage_Deliquency],
		[Client_Second_Loan_Exists],[Client_Intake_Loan_Type_Is_Hybrid_ARM],[Client_Intake_Loan_Type_Is_Option_ARM],[Client_Intake_Loan_Type_Is_Interest_Only],[Client_Intake_Loan_Type_Is_FHA_Or_VA_Insured],[Client_Intake_Loan_Type_Is_Privately_Held],
		[Client_Intake_Loan_Type_Has_Interest_Rate_Reset],[Client_Counsel_Session_DT_Start],[Client_Counsel_Session_DT_End],[Client_Intake_DT],[Client_Birth_DT],[Client_HECM_Certificate_Issue_Date],[Client_HECM_Certificate_Expiration_Date],[Client_HECM_Certificate_ID],
		[Client_Sales_Contract_Signed],[Client_Grant_Amount_Used],[Client_Mortgage_Closing_Cost],[Client_Mortgage_Interest_Rate],[Client_Counseling_Termination],[Client_Income_Level],[Client_Highest_Educ_Grade],[Client_HUD_Assistance],[Client_Dependents_Num],
		[Client_Language_Spoken],[Client_Session_Duration],[Client_Counseling_Type],[Client_Counseling_Fee],[Client_Attribute_HUD_Grant],[Client_Finance_Type_Before],[Client_Finance_Type_After],[Client_Mortgage_Type],[Client_Mortgage_Type_After],[Client_Referred_By],
		[Client_Job_Duration],[Client_Household_Debt],[Client_Loan_Being_Reported],[Client_Intake_Loan_Type],[Client_Family_Size],[Client_Marital_Status],[Client_Race_ID],[Client_Ethnicity_ID],[Client_Household_Gross_Monthly_Income],[Client_Gender],[Client_Spouse_SSN],
		[Client_SSN1],[Client_SSN2],[Client_Mobile_Phone_Num],[Client_Phone_Num],[Client_Fax],[Client_Email],[office],[hcs_id],[group_session_id],[person_1],[person_2],[hud_interview],[interview_type],[hud_result],[interview_date],[result_date],[termination_date],
		[interview_result],[ami],[client_appointment],[housing_property],[primary_loan],[secondary_loan],[is_client],[is_workshop]

from	#hud_9902_clients

-- Toss the result table
drop table #hud_9902_clients
return ( @@rowcount )

GO
