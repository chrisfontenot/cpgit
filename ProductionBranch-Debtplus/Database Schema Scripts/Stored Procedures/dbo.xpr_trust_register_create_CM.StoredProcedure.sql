SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_trust_register_create_CM] ( @creditor as typ_creditor, @amount AS Money = 0, @item_date AS DateTime = NULL, @cleared AS VarChar(1) = NULL, @checknum as BigInt = NULL, @bank as int = 1 ) AS

-- ====================================================================================================
-- ==   Create a check in the trust register for paying this creditor on an automatic disbursement   ==
-- ====================================================================================================

-- ChangeLog
--   11/20/2003
--     Added bank number to parameter list

SET NOCOUNT ON

-- Ensure that the amount is valid
IF @Amount < 0
BEGIN
	RaisError (50019, 16, 1)
	Return ( 0 )
END

-- Default the item date
IF @Item_Date IS NULL
	SET @Item_Date = getdate()

-- The check can not be blank and still not be pending to be printed
if @Cleared IS NULL
	SET @Cleared = 'P'

-- Validate the cleared status
IF @Cleared NOT IN (' ','R','C','E','P')
BEGIN
	RaisError (50024, 16, 1, @Cleared)
	Return ( 0 )
END

IF @checknum IS NULL
	SET @Cleared = 'P'

-- Insert the item into the trust register
INSERT INTO	registers_trust (tran_type,	creditor,	date_created,	amount,		cleared,	checknum,	bank)
VALUES				('CM',		@creditor,	@Item_Date,	@Amount,	@Cleared,	@checknum,	@bank)

-- Return the trust register ID to the caller
RETURN ( SCOPE_IDENTITY() )
GO
