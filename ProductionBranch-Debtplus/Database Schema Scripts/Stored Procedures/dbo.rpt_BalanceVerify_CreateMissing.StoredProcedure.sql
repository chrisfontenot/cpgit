USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_BalanceVerify_CreateMissing]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_BalanceVerify_CreateMissing] ( @rpps_file as int = null ) AS

-- ========================================================================================
-- ==            Create the balance verification request for pending debts               ==
-- ========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   2/18/2003
--     Removed "same day" test to avoid problems with RPPS transmissions that do not have reprint issues.

-- Suppress intermediate result sets
SET NOCOUNT ON

-- Fetch the time period before payout to send a balance verification
DECLARE	@WarningMonths	FLOAT
SELECT	@WarningMonths = bal_verify_months
FROM	config

-- Default the period to 3.0 months
IF @WarningMonths IS NULL
	SET @WarningMonths = 3.0

-- Mark the debts as needing a balance verification operation

UPDATE		client_creditor
SET		send_bal_verify = 1
FROM		client_creditor cc
INNER JOIN	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c	WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr	WITH (NOLOCK) ON cc.creditor = cr.creditor
inner join	creditor_methods cm with (nolock) ON cr.creditor_id = cm.creditor and 'BAL' = cm.type
WHERE		isnull(cc.reassigned_debt,0) = 0
AND		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) > isnull(bal.total_payments,0)
AND		c.active_status in ('A','AR')
AND		cc.creditor IS NOT NULL
AND
		(
			(cc.verify_request_date IS NULL)						-- No verification was sent
--			OR (datediff(d,cc.verify_request_date,getdate()) < 1)				-- This is the same date as printed. Therefore, it is a reprint function (do not do this here.)
			OR (datediff(m,cc.verify_request_date,getdate()) > (@WarningMonths + 1.0))	-- Do a subsequent verification if 3 months period takes more than 3
		)
AND
		(
			((isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) < (@WarningMonths * isnull(cc.disbursement_factor,0)))
		)

-- Return success to the caller
RETURN ( @@rowcount )
GO
