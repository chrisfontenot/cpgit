USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_faq_items]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_faq_items] ( @grouping as varchar(50), @description as varchar(128), @attributes as varchar(50) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the faq_items table                      ==
-- ========================================================================================
	insert into faq_items ( [grouping], [description], [attributes] ) values ( @grouping, @description, @attributes )
	return ( scope_identity() )
GO
