IF EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'xpr_create_client' AND TYPE='P')
	EXEC ('DROP PROCEDURE xpr_create_client')
GO
CREATE PROCEDURE [dbo].[xpr_create_client]  (@HomeTelephoneID as int = NULL, @mortgage_problems AS BIT = 0, @referral_code as INT = NULL, @method_first_contact as int = NULL, @bankruptcy as BIT = 0, @Initial_Program AS INT = 1, @InboundTelephoneNumberID AS INT = NULL) AS
-- ================================================================
-- ==    Create a new client with the home_phone and mortgage    ==
-- ==    problems defaulted from the input values.               ==
-- ================================================================

-- ChangeLog
--   2/20/2002
--     Added default for ach_start_date to ensure that the item is never NULL.
--   4/28/2002
--     Added "F" as a legal "first contact" type.
--   5/15/2003
--     called xpr_create_person to create the applicant when the client is created. We don't 'exist' properly without an applicant.
--   11/03/2005
--     defaulted the marital status to 1 (single)
--   8/11/2006
--     changed method_first_contact to a number
--   3/11/2011
--     used default hud_grant from messages table when creating the client

declare	@client		int

-- Start a transaction for the operation
SET NOCOUNT ON
BEGIN TRANSACTION
SET XACT_ABORT ON

-- Default the referral information to the first item in the referred_by list
IF @referral_code IS NULL
begin
	SELECT		@referral_code = min(referred_by)
	from		referred_by
	where		[default] = 1
	and			[ActiveFlag] = 1

IF @referral_code IS NULL
	SELECT		@referral_code = min(referred_by)
	from		referred_by
	where		[default] = 1

IF @referral_code IS NULL
	SELECT		@referral_code = min(referred_by)
	from		referred_by
	where		[ActiveFlag] = 1

IF @referral_code IS NULL
	SELECT		@referral_code = min(referred_by)
	from		referred_by;
end

-- Default the method of first contact to "not specified"
if @method_first_contact is NULL
begin
	select  @method_first_contact = min(oID)
	from	ContactMethodTypes
	where	[default] = 1
	and		ActiveFlag = 1

	if @method_first_contact IS NULL
		SELECT	@method_first_contact = min(oID)
		from	ContactMethodTypes
		where	[default] = 1

	if @method_first_contact IS NULL
		SELECT	@method_first_contact = min(oID)
		from	ContactMethodTypes
		where	ActiveFlag = 1

	if @method_first_contact IS NULL
		SELECT	@method_first_contact = min(oID)
		from	ContactMethodTypes

	if @method_first_contact IS NULL
		select	@method_first_contact = 1
end

-- Default the program if needed
if @Initial_Program is null
begin
	select	@Initial_Program = min(oID)
	from	ProgramParticipationTypes with (nolock)
	where	[default] <> 0

	if @Initial_Program is null
		select	@Initial_Program = min(oID)
		from	ProgramParticipationTypes with (nolock)

	-- This should fail, but it is a good indication tha something is missing.
	if @Initial_Program is null
		set		@Initial_Program = 1
end

-- If the counselor was not passed then try to find it in the system tables based upon the name of the person "keeping" the appointment
declare	@counselor	typ_key
declare	@office		typ_key

select	@counselor	= counselor,
		@office		= office
from	counselors
where	[person]	= suser_sname()		-- Don't forget the \\DP\jones rather than just 'jones' in the table

-- Find the region from the office
declare	@region		int
select	@region		= region
from	offices with (nolock)
where	office		= @office

if @region is NULL
	select	@region	= min(region)
	from	regions with (nolock)

if @region is NULL
	select	@region	= 1

-- Find the configuration fee
declare	@config_fee		int
declare	@language		int
declare	@marital_status	int
select	@config_fee		= dbo.default_config_fee(),
		@language		= dbo.default_language(),
		@marital_status	= dbo.default_marital_status()

-- Find the InboundTelephoneGroup code from the InboundTelephoneNumberID field
-- If there isn't one specified then leave the field as null. It is allowed to be null.
declare	@InboundTelephoneGroupID		int

if @InboundTelephoneNumberID is not null
	select	@InboundTelephoneGroupID = InboundTelephoneNumberGroup
	from	InboundTelephoneNumbers WITH (NOLOCK)
	WHERE	oID = @InboundTelephoneNumberID

-- Create the client by inserting the value into the clients table
INSERT INTO clients (HomeTelephoneID,mortgage_problems,config_fee,referred_by,marital_status,language,counselor,office,active_status,method_first_contact,county,region,bankruptcy_class, [ElectronicCorrespondence],[ElectronicStatements], [initial_program], [current_program], [InboundTelephoneGroupID], [InboundTelephoneNumberID])
SELECT	@HomeTelephoneID,					-- Home telephone number
		isnull(@mortgage_problems,0),		-- Started with mortgage problems
		isnull(@config_fee,1),				-- Configuration fee record
		isnull(@referral_code,1),			-- referred_by
		isnull(@marital_status,1),			-- marital status
		isnull(@language,1),				-- language
		@counselor,							-- The current counselor "owns" the client
		@office,							-- at the current office
		'CRE',								-- All clients are created with this status
		isnull(@method_first_contact,1),	-- Method of first contact
		1,									-- county
		isnull(@region,1),					-- region
		isnull(@bankruptcy,0),				-- client filing for bankrupcy?
		2,2,								-- Temporarilty set the Electronic flags to false
		@Initial_Program, @Initial_Program,	-- Starting and current programs are the same on a new client
		@InboundTelephoneGroupID,			-- Telephone number group identifier for this phone number
		@InboundTelephoneNumberID			-- Telephone number group identifier for this phone number

-- Fetch the client ID
SELECT	@client = SCOPE_IDENTITY()

-- Create the housing record
declare	@housing_status	int
declare	@hud_assistance	int
declare @nfmcp_level    varchar(10)
select	@housing_status		= dbo.default_HousingStatus(),
		@hud_assistance		= dbo.default_HudAssistance(),
		@nfmcp_level        = ''  -- none, '1' = 1, '2' = 2, '3' = 3, '4a' = 4a, '4b' = 4b, etc.

insert into client_housing (client,housing_status,hud_assistance,nfmcp_level) values (@client,@housing_status,@hud_assistance,@nfmcp_level)

-- Insert the starting balance for the first month
insert into registers_client (tran_type, client, credit_amt) values ('BB', @client, 0)

-- Find the default values when we create the record. Since the values won't be defaulted by the build, do it here.
declare @no_fico_score_reason int
declare @gender				  int
declare	@race				  int
declare	@education			  int
declare	@MilitaryService	  int
declare @Ethnicity			  int
declare	@CreditAgency		  varchar(20)

select  @no_fico_score_reason = dbo.default_Housing_FICONotIncludedReason(),
		@gender				  = dbo.default_gender(),
		@race				  = dbo.default_race(),
		@ethnicity			  = dbo.default_ethnicity(),
		@education			  = dbo.default_education(),
		@MilitaryService	  = dbo.default_MilitaryService(),
		@CreditAgency		  = dbo.default_CreditAgency()

-- Create the person with the aid of the stored procedure to insert a new row into that table correctly.
execute xpr_insert_people @client=@client, @relation=1, @no_fico_score_reason=@no_fico_score_reason, @race=@race, @ethnicity=@ethnicity, @gender=@gender, @education=@education, @MilitaryStatusID=@MilitaryService, @CreditAgency=@CreditAgency

-- Commit the transaction at this time
COMMIT TRANSACTION
RETURN ( @client )
GO
GRANT EXECUTE ON xpr_create_client TO public AS dbo;
GO
