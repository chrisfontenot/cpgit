USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_hud_loan_type]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_hud_loan_type] as
-- =====================================================================================================================
-- ==            Retrieve the list of hud loan types                                                                  ==
-- =====================================================================================================================

-- ChangeLog
--   4/2/2012
--     Use Housing_LoanTypes table rather than messages

select	oID				as 'item_key',
		description		as 'description',
		[Default]		as 'default',
		[ActiveFlag]	as 'ActiveFlag'
from	Housing_LoanTypes with (nolock)
ORDER BY 1

return ( @@rowcount )
GO
