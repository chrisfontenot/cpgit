USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_NSF_Report]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_NSF_Report] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ============================================================================================
-- ==             Report information to list collected/disbursed/should be disbursed         ==
-- ============================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert (datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Construct the table which lists the amount collected by each client
SELECT		d.client				as 'client',
		sum(d.credit_amt)			as 'collected'
INTO		#t_nsf_collected
FROM		registers_client d WITH (NOLOCK)
WHERE		d.tran_type = 'DP'
AND		d.date_created BETWEEN @FromDate AND @ToDate
GROUP BY	d.client

-- Collect the amount that should be disbursed to each creditor
SELECT		cc.client				as 'client',
		sum(cc.disbursement_factor)		as 'disbursment_factor'
INTO		#t_nsf_factor
FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c		WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		cc.reassigned_debt = 0
AND		cc.disbursement_factor > 0
AND		c.active_status IN ('A','AR')
AND		((isnull(ccl.zero_balance,0) <> 0) OR (bal.payments_month_0 > 0 or bal.payments_month_1 > 0))
GROUP BY	cc.client

-- Collect the amount that was disbursed this month
SELECT		d.client				as 'client',
		sum(d.debit_amt)			as 'amount'
INTO		#t_nsf_actual
FROM		registers_client_creditor d WITH (NOLOCK)
INNER JOIN	registers_disbursement s ON d.disbursement_register = s.disbursement_register
INNER JOIN	clients c ON d.client = c.client
WHERE		s.date_created BETWEEN @FromDate and @ToDate
AND		d.tran_type IN ('AD', 'BW')
AND		c.active_status IN ('A','AR')
GROUP BY	d.client

-- Merge the items together to form the resulting table showing what was collected,
-- what should have been collected, and what was disbursed for each client

SELECT		coalesce(a.client, c.client, d.client)	as 'client',
		isnull(c.collected,0)			as 'collected',
		isnull(a.amount,0)			as 'disbursed',
		isnull(d.disbursment_factor,0)		as 'expected',

		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name'

FROM		#t_nsf_actual a
LEFT OUTER JOIN	#t_nsf_collected c	ON a.client = c.client
LEFT OUTER JOIN	#t_nsf_factor d		ON a.client = d.client
LEFT OUTER JOIN	people p WITH (NOLOCK)	ON coalesce(a.client, c.client, d.client) = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
ORDER BY	1

-- Drop the working tables
drop table #t_nsf_collected
drop table #t_nsf_factor
drop table #t_nsf_actual

RETURN ( @@rowcount )
GO
