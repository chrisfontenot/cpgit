USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_COA]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_COA] (@response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @name as varchar(80) = null, @OldAccountNumber as varchar(22), @ChangeAccountNumber as bit = 0, @NewAccountNumber as varchar(22) = '', @ChangeBillerID as bit = 0, @NewBillerID as varchar(10) = '') as

-- =======================================================================================================================
-- ==            Generate the response for a COA request                                                                ==
-- =======================================================================================================================

-- ChangeLog

-- Generate the base response row
declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'COA'

-- Insert the addendum information for account number changes notifications
if @rpps_response_detail > 0
	insert into rpps_response_details_coa (rpps_response_detail, ChangeAccountNumber, NewAccountNumber, ChangeBillerID, NewBillerID)
	values (@rpps_response_detail, @ChangeAccountNumber, @NewAccountNumber, @ChangeBillerID, @NewBillerID)

return ( @rpps_response_detail )
GO
