USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_delete]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_delete] (@creditor_contribution_pct typ_key) AS

-- ====================================================================================================
-- ==            Remove the indicated contribution percentage record from the system                 ==
-- ====================================================================================================

-- ChangeLog
--   1/5/2002
--     Supported eft/check creditor types

-- Disable intermediate results
set nocount on
BEGIN TRANSACTION

DECLARE	@creditor			typ_creditor
declare	@new_creditor_contribution_pct	int
declare	@effective_date			datetime

SELECT	@creditor			= creditor
FROM	creditor_contribution_pcts WITH (NOLOCK)
WHERE	creditor_contribution_pct	= @creditor_contribution_pct

-- Remove the row from the system
DELETE FROM	creditor_contribution_pcts
WHERE		creditor_contribution_pct	= @creditor_contribution_pct

-- If there is no contribution record then there is no contribution
select	@new_creditor_contribution_pct	= null

-- Process the most current update on the contribution fees for this creditor

select	@effective_date			= max(effective_date)
from	creditor_contribution_pcts
where	creditor			= @creditor
and	effective_date			<= getdate()

select	@creditor_contribution_pct	= max(creditor_contribution_pct)
from	creditor_contribution_pcts with (nolock)
where	creditor			= @creditor
and	effective_date			= @effective_date

update	creditors
set	creditor_contribution_pct	= @creditor_contribution_pct
where	creditor			= @creditor

COMMIT TRANSACTION
return ( @@rowcount )
GO
