USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_misc_indicator_by_client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_misc_indicator_by_client] (@indicator as int = null) AS
-- ScZ / 01-19-2004

-- Ensure that the indicator is valid
if not exists (select * from indicators where indicator = @indicator)
	select @indicator = null

-- If there is no indicator then return all of the indicators for the clients
if @indicator is null
	select		c.client,
				v.name,
				c.start_date,
				isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'') as counselor,
				c.active_status,
				isnull(i.description, 'Indicator #' + convert(varchar, i.indicator)) as indicator
	from		clients c with (nolock)
	inner join	view_client_address v with (nolock) on c.client = v.client
	inner join	client_indicators ci with (nolock) on c.client = ci.client
	left outer join	counselors co with (nolock) on c.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name
	left outer	join indicators i with (nolock) on ci.indicator = i.indicator
	where		c.active_status in ('A','AR')
	order by	4, 3, 1  -- counselor, start_date, client

-- Otherwise, return just the indicator that is desired
else

	select		c.client,
				v.name,
				c.start_date,
				isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),'') as counselor,
				c.active_status,
				isnull(i.description, 'Indicator #' + convert(varchar, i.indicator)) as indicator
	from		clients c with (nolock)
	inner join	view_client_address v with (nolock) on c.client = v.client
	inner join	client_indicators ci with (nolock) on c.client = ci.client
	left outer	join indicators i with (nolock) on ci.indicator = i.indicator
	left outer join	counselors co with (nolock) on c.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name
	where		c.active_status in ('A','AR')
	and			ci.indicator = @indicator
	order by	4, 3, 1  -- counselor, start_date, client

return ( @@rowcount )
GO
