USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[creditor_refund_amounts]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[creditor_refund_amounts] ( @FromDate AS DateTime, @ToDate AS DateTime ) AS

-- Return the list of creditor refund amounts deducted from the refund check
SET @FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate = convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59')

SELECT	CONVERT(varchar(10), date_created, 101)	as 'date',
	sum(fairshare_amt)			as 'amount'
from	registers_client_creditor
where	date_created between @FromDate AND @ToDate
and	tran_type in ('RF', 'VD', 'RR')
GROUP BY date_created

UNION ALL

SELECT	CONVERT(varchar(10), date_created, 101)	as 'date',
	0 - sum(fairshare_amt)			as 'amount'
from	registers_client_creditor
where	date_created between @FromDate AND @ToDate
and	tran_type = 'VF'
and	fairshare_amt > 0
GROUP BY date_created

ORDER BY 1

return ( @@rowcount )
GO
