USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Deposit_Disbursement_Amount]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Client_Deposit_Disbursement_Amount] AS

-- =========================================================================================
-- ==            Report listing the client, Scheduled payment (deposit amount),           ==
-- ==            Scheduled payment (disbursement factor), and current month disbursement  ==
-- ==            For Salem Oregon.                                                        ==
-- =========================================================================================

select client,sum(deposit_amount) as deposit_amount
into #t_deposits
from client_deposits
group by client;

select client,sum(disbursement_factor) as disbursement_factor
into #t_disbursements
from client_creditor
group by client;

select client,sum(bal.payments_month_0) as current_month_disbursement
into #t_current_month
from client_creditor cc
inner join client_creditor_balanaces bal on cc.client_creditor_balance = bal.client_creditor_balance
where cc.reassigned_debt = 0
group by client;

select	left(pn.last,20) as 'Last',
	left(isnull(pn.first,'') + isnull(' '+pn.middle,''),20) as 'First',
	p.client as 'Number',
	dep.deposit_amount as 'TSCHPAY',
	disb.disbursement_factor as 'SchedPay Sum',
	cm.current_month_disbursement as 'Disbursed'

from	clients c
left outer join people p on c.client = p.client and 1 = p.relation
left outer join names pn on p.nameid = pn.name
left outer join #t_deposits dep on c.client = dep.client
left outer join #t_disbursements disb on c.client = disb.client
left outer join #t_current_month cm on c.client = cm.client
where c.active_status in ('A', 'AR')
order by 1, 2, 3 -- last, first, client

drop table #t_deposits;
drop table #t_disbursements;
drop table #t_current_month;

return ( @@rowcount )
GO
