USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_transaction]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_housing_transaction] (@hud_interview int, @minutes int = 0, @GrantAmountUsed as money = 0, @client_appointment as int = null) as
-- ========================================================================================
-- ==             Insert a new housing transaction record                                ==
-- ========================================================================================
insert into hud_transactions ([hud_interview], [minutes], [GrantAmountUsed], [client_appointment]) values ( @hud_interview, isnull(@minutes,0), isnull(@GrantAmountUsed,0), @client_appointment )
return ( scope_identity() )
GO
