USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_regions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_regions] AS
select	[region] as item_key,
	[description] as description,
	[default],
	[ActiveFlag]
from	regions with (nolock)
order by 2
GO
