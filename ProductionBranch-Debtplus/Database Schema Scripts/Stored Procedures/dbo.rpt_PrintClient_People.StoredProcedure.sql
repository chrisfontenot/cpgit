USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_People]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_People] ( @Client AS INT ) AS

-- ===============================================================================================
-- ==             Fetch the people for this client                                              ==
-- ===============================================================================================

SELECT	convert(int,case relation when 1 then 1 else 2 end) as person,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		former,
		em.address as email,
		dbo.format_ssn(ssn) as 'ssn',
		gross_income,
		net_income,
		e.name as 'employer',

		dbo.format_TelephoneNumber ( p.WorkTelephoneID ) AS 'work_ph',

		convert(varchar(10),null) as work_ext,
		isnull(j.description, other_job) as 'job',
		birthdate,
		case isnull(gender, 1)
			when 1 then 'M'
			when 2 then 'F'
		end as 'gender',

		case isnull(race, 10)
			when 1 then 'A.AM.'
			when 3 then 'CAUC'
			when 4 then 'HISP'
			when 5 then 'OTHER'
			when 6 then 'E.ID.'
			when 7 then 'JWSH'
			when 8 then 'M.EST'
			when 9 then 'A.ID.'
			when 10 then 'UNSP'
		end as 'race'
FROM	people p
left outer join names pn on p.nameid = pn.name
left outer join emailaddresses em on p.emailid = em.email
LEFT OUTER JOIN job_descriptions j ON p.job = j.job_description
LEFT OUTER JOIN employers e ON p.employer = e.employer
WHERE	client = @Client
ORDER BY person

RETURN ( @@rowcount )
GO
