USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_creditor_info]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_creditor_info] ( @disbursement_register AS INT, @client AS typ_client ) AS

-- ======================================================================================================
-- ==            Fetch the information for the current client's debts                                  ==
-- ======================================================================================================

-- ChangeLog
--   3/21/2002
--     Add original DMP payment information
--   3/29/2002
--     Add "held" flag for held debts
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   2/10/2010
--      Added oID to the list of fields returned

select	dc.creditor					as 'creditor',
		dc.client_creditor			as 'client_creditor',
		dc.sched_payment			as 'sched_payment',
		dc.disbursement_factor		as 'disbursement_factor',
		dc.current_month_disbursement		as 'current_month_disbursement',
		dc.orig_balance				as 'orig_balance',
		cc.orig_dmp_payment			as 'orig_dmp_payment',
		dc.current_balance			as 'current_balance',
		dc.apr						as 'apr',
		dc.debit_amt				as 'debit_amt',

		isnull(cc.priority,9)			as 'priority',
		isnull(cr.creditor_name,'')		as 'creditor_name',
		isnull(cc.account_number,'')	as 'account_number',
		isnull(cc.line_number,0)		as 'line_number',

		isnull(ccl.zero_balance,0)		as 'zero_balance',
		isnull(ccl.prorate,0)			as 'prorate',

		case
			when dc.creditor_type = 'N' then 'N'
			when dc.tran_type = 'AD' and dc.fairshare_pct_check <= 0.0 then 'N'
			when dc.tran_type = 'BW' and dc.fairshare_pct_eft <= 0.0 then 'N'
			else isnull(dc.creditor_type,'N')
		end					as 'creditor_type',

		isnull(dc.held,0)			as 'held',
		dc.oID						as 'oid'

FROM		disbursement_creditors dc		with (nolock)
inner join	creditors cr					with (nolock) on dc.creditor = cr.creditor
inner join	client_creditor cc				with (nolock) on dc.client_creditor = cc.client_creditor
left outer join	creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class

where	disbursement_register	= @disbursement_register
and		dc.client		= @client

RETURN ( @@rowcount )
GO
