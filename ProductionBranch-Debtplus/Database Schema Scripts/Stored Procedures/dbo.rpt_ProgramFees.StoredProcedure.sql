USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ProgramFees]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ProgramFees] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) AS
-- =============================================================================================
-- ==                    Fetch the list of "fee" account transfers in the period              ==
-- =============================================================================================

-- ChangeLog
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00' ),
	@ToDate		= convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59' )

SELECT		d.date_created			as 'date_created',
		d.debit_amt			as 'amount',
		d.client			as 'client',
		d.creditor			as 'creditor',

		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',

		cr.creditor_name		as 'creditor_name'

FROM		registers_client_creditor d	WITH (NOLOCK)
INNER JOIN	creditors cr			WITH (NOLOCK) ON d.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl		WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
INNER JOIN	clients c			WITH (NOLOCK) ON d.client = c.client
LEFT OUTER JOIN	people p			WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

WHERE		isnull(ccl.agency_account,0) > 0
AND		d.tran_type IN ('AD', 'BW', 'CM', 'MD')
AND		d.client > 0
ORDER BY	cr.type, cr.creditor_id, d.creditor, d.date_created

RETURN ( @@rowcount )
GO
