USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_response_dr]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_response_dr] (
	@epay_response_file as int,
	@transaction_id as varchar(27),
	@transaction_type as varchar(2),
	@response_date as datetime,
        @agency_name as varchar(35),
        @agency_id as varchar(12),
        @creditor_id as varchar(12),
        @creditor_name as varchar(35),
        @client_number as int,
        @client_name as varchar(35),
        @response_code as varchar(1),
        @proposed_payment_amount as money,
        @accepted_payment_amount as money,
        @decline_reason_code as varchar(2),
        @customer_biller_account_number_original as varchar(32),
        @customer_biller_account_number_corrected as varchar(32),
        @last_payment_date as datetime,
        @current_client_balance as money,
        @date_of_balance as datetime,
        @number_of_days_delinquent as int,
        @creditor_contact_name as varchar(35),
        @date_of_authorization as datetime,
        @corrected_balance_included as varchar(1)
) as

-- Remove the extra spaces around the fields. ADODB does not like empty strings so we don't trim them there.
select @customer_biller_account_number_original = ltrim(rtrim(@customer_biller_account_number_original))
select @customer_biller_account_number_corrected = ltrim(rtrim(@customer_biller_account_number_corrected))

select @transaction_id = ltrim(rtrim(@transaction_id))
if @transaction_id = ''
	select @transaction_id = null

select @agency_id   = ltrim(rtrim(@agency_id))
select @creditor_id = ltrim(rtrim(@creditor_id))

select @agency_name = ltrim(rtrim(@agency_name))
if @agency_name = ''
	select @agency_name = null

select @creditor_name = ltrim(rtrim(@creditor_name))
if @creditor_name = ''
	select @creditor_name = null

select @client_name = ltrim(rtrim(@client_name))
if @client_name = ''
	select @client_name = null

select @creditor_contact_name = ltrim(rtrim(@creditor_contact_name))
if @creditor_contact_name = ''
	select @creditor_contact_name = null;

declare	@null_date	datetime
select	@null_date = convert(datetime, '1/1/1901 23:59:59')

if @last_payment_date <= @null_date
	select @last_payment_date = null;

if @date_of_balance <= @null_date
	select @date_of_balance = null;

if @date_of_authorization <= @null_date
	select @date_of_authorization = null;

-- Insert the item into the response tables for later processing
insert into epay_responses_dr (
	epay_response_file,
	transaction_id,
	response_date,
        agency_name,
        agency_id,
        creditor_id,
        creditor_name,
        client_number,
        client_name,
        response_code,
        proposed_payment_amount,
        accepted_payment_amount,
        decline_reason_code,
        customer_biller_account_number_original,
        customer_biller_account_number_corrected,
        last_payment_date,
        current_client_balance,
        date_of_balance,
        number_of_days_delinquent,
        creditor_contact_name,
        date_of_authorization,
        corrected_balance_included

) values (

	@epay_response_file,
	@transaction_id,
	@response_date,
        @agency_name,
        @agency_id,
        @creditor_id,
        @creditor_name,
        @client_number,
        @client_name,
        @response_code,
        @proposed_payment_amount,
        @accepted_payment_amount,
        @decline_reason_code,
        @customer_biller_account_number_original,
        @customer_biller_account_number_corrected,
        @last_payment_date,
        @current_client_balance,
        @date_of_balance,
        @number_of_days_delinquent,
        @creditor_contact_name,
        @date_of_authorization,
        @corrected_balance_included
)

-- The result is the record number
return ( @@identity )
GO
