USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_create_appt_time]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_create_appt_time] ( @office as int, @start_time as datetime, @appt_type as int = null ) AS

-- ======================================================================================================
-- ==            Create an appointment time for the indicated slot                                     ==
-- ======================================================================================================

set nocount on
declare	@appt_time	int

if @appt_type is not null
begin
	select	@appt_time	= appt_time
	from	appt_times
	where	office		= @office
	and	start_time	= @start_time
	and	appt_type	= @appt_type

end else begin

	select	@appt_time	= appt_time
	from	appt_times
	where	office		= @office
	and	start_time	= @start_time
	and	appt_type is null
end

-- If there is no entry then create one
if @appt_time is null
begin

	if @appt_type is null
	begin
		insert into appt_times (office, start_time, appt_type)
		values	(@office, @start_time, null)

		select	@appt_time = SCOPE_IDENTITY()

	end else begin

		insert into appt_times (office, start_time, appt_type)
		values	(@office, @start_time, @appt_type)

		select	@appt_time = SCOPE_IDENTITY()
	end
end

-- Return the pointer to the appointment time slot
return ( @appt_time )
GO
