USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Delete_Disbursement]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Delete_Disbursement] ( @disbursement_register as int = null ) AS

-- ==================================================================================================
-- ==            Remove the disbursement values from the system                                    ==
-- ==================================================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that there is a disbursement register. If not, give the user the list of outstanding items
if @disbursement_register is null
begin
	print 'The disbursement batch is required. The possible disbursement batches are:'
	select top 10 disbursement_register, date_created as 'date', created_by as 'creator', note as 'note'
	from	registers_disbursement
	where date_posted is null
	order by date_created desc;

	return ( 0 )
end

-- Retreive the posting date from the disbursement table
declare	@date_posted	datetime
select	@date_posted = date_posted
from	registers_disbursement
where	disbursement_register = @disbursement_register

-- If there is no batch by this label the complain.
if @@rowcount < 1
begin
	raiserror ('The disbursement batch %d is not defined in the system and may not be deleted', 16, 1, @disbursement_register);
	return ( 0 )
end

-- If the batch is not marked then check for a transaction with disbursement date and stop when the item is found
if @date_posted is null
begin
	select	top 1 @date_posted = date_created
	from	registers_client_creditor
	where	tran_type in ('AD', 'BW', 'MD', 'CM')
	and	disbursement_register = @disbursement_register;
end

-- If the batch is posted then do not accept the operation. There is no hope about removing it.
if @date_posted is not null
begin
	declare	@posting_date	varchar(10)
	select	@posting_date = convert(varchar(10), @date_posted, 101);

	raiserror ('The disbursement batch %d was posted on %s and may not be deleted', 16, 1, @disbursement_register, @posting_date);
	return ( 0 )
end

-- Find the list of clients which have been paid on this disbursement
select	client, sum(isnull(debit_amt,0)) as 'disbursed'
into	#t_payments
from	disbursement_creditors
where	disbursement_register = @disbursement_register
and	isnull(debit_amt,0) > 0
group by client;

-- Discard the zero payment amounts
delete from #t_payments where disbursed = 0

-- Update the disbursement data with the amount disbursed
update	registers_client
set	debit_amt = x.disbursed
from	disbursement_clients dc
inner join registers_client rc on dc.client_register = rc.client_register
inner join #t_payments x on dc.client = x.client and dc.disbursement_register = @disbursement_register

-- Find the total amount to be refunded to the clients
declare	@refund_amount		money
declare	@client_count		int

-- Count the number of clients and the refund amount due to the total
select	@client_count = count(*),
	@refund_amount = sum(isnull(disbursed, 0))
from	view_disbursement_clients
where	disbursement_register = @disbursement_register
and	disbursed > 0

-- The default count is zero
if @client_count is null
	set @client_count = 0

-- The default amount is zero
if @refund_amount is null
	set @refund_amount = 0

-- Tell the user if there is an amount to be refunded
if @refund_amount > 0
begin
	print 'We had to refund $' + convert(varchar, @refund_amount, 1) + ' to ' + convert(varchar, @client_count) + ' client(s) because they were disbursed in this batch.'
end

begin transaction

-- Refund the money to the clients which were disbursed in the batch
update	clients
set	held_in_trust = isnull(c.held_in_trust,0) + dc.disbursed
from	clients c
inner join view_disbursement_clients dc on c.client = dc.client
where	dc.disbursement_register = @disbursement_register
and	dc.disbursed > 0

-- Discard the client disbursement records
delete
from	registers_client
where	disbursement_register = @disbursement_register
and	tran_type = 'AD'

-- Discard the creditor information
delete
from	disbursement_creditors
where	disbursement_register = @disbursement_register

-- Discard the client information
delete
from	disbursement_clients
where	disbursement_register = @disbursement_register

-- Remove the item from the disbursement register
delete
from	registers_disbursement
where	disbursement_register = @disbursement_register

-- At this point, the databasee is intact.
commit transaction
return ( 1 )
GO
