USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_appt_callbacks]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_appt_callbacks] ( @prev_appt as int = null, @client as int = null ) AS

-- =========================================================================================================
-- ==            Obtain a list of the callback telephone numbers for the client                           ==
-- =========================================================================================================

if @prev_appt is null
	select	@prev_appt = 0

if @prev_appt > 0
	select	@client	= client
	from	client_appointments with (nolock)
	where	client_appointment = @prev_appt
	and	client is not null

if @client is null
	select	@client = 0

if @client < 0
	select	@client = 0

if (@prev_appt > 0) and (@client > 0)
begin
	select	1							as item_key,
		callback_ph						as description
	from	client_appointments with (nolock)
	where	client_appointment	= @prev_appt
	and	callback_ph is not null

	union

	select	0							as item_key,
		dbo.format_TelephoneNumber ( HomeTelephoneID )			as description
	from	clients with (nolock)
	where	client			= @client
	and	HomeTelephoneID is not null

	union

	select
		0							as item_key,
		dbo.format_TelephoneNumber ( WorkTelephoneID )	as description

	from	people with (nolock)
	where	client			= @client
	and		WorkTelephoneID is not null

end ELSE if (@prev_appt > 0)
begin
	select	1							as item_key,
		callback_ph						as description
	from	client_appointments with (nolock)
	where	client_appointment	= @prev_appt
	and	callback_ph is not null

end ELSE if (@client > 0)
begin

	select
		0							as item_key,
		dbo.format_TelephoneNumber ( HomeTelephoneID )			as description
	from	clients with (nolock)
	where	client			= @client
	and		HomeTelephoneID is not null

	union

	select
		0							as item_key,
		dbo.format_TelephoneNumber ( WorkTelephoneID )			as description

	from	people with (nolock)
	where	client			= @client
	and		WorkTelephoneID is not null

END ELSE

	select	0							as item_key,
		convert(varchar(14), null)				as description

return ( @@rowcount )
GO
