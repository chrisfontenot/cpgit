USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_emailaddress]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_emailaddress] ( @Address as varchar(80) = NULL, @Client as int = null, @Person as int = null, @Validation as int = 1 ) as
-- ===========================================================================================================================
-- ==             Create an entry in the EmailAddresses table for .NET                                                      ==
-- ===========================================================================================================================

declare	@Email		int
declare @NoteText	varchar(800)
declare	@Subject	varchar(80)

set nocount on

insert into EmailAddresses(Address, Validation) values ( @Address, @Validation )
select	@Email = SCOPE_IDENTITY()

-- Create a system note if one is given
if @Client is not null
begin
	select	@Subject = 'Changed '
	if @Person is not null
	begin
		declare	@relation	int
		select	@relation = [relation]
		from	people with (nolock)
		where	[Client]	= @Client
		and		[Person]	= @Person;
		
		if @relation <> 1
			select	@Subject = @Subject + 'Co-Applicant '
		else
			select	@Subject = @Subject + 'Applicant '
	end
	select @Subject = @Subject + 'Email address'
	insert into client_notes (client, type, subject, dont_delete, dont_edit, note)
	select	@Client, 3, @Subject, 1, 1, 'Changed Email address to ' + ISNULL('''' + replace(@Address, '''', '''''') + '''', 'NOTHING')	
end

return ( @Email )
GO
