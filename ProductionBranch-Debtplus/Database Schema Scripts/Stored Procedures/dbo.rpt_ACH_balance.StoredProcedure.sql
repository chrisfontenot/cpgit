USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ACH_balance]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_ACH_balance] ( @ACH_Pull_Date AS datetime ) as

-- ==============================================================================
-- ==        Report showing the clients and dollar amounts for a specific      ==
-- ==        ACH pull date. This will help in checking the ACH pull.           ==
-- ==============================================================================

-- ChangeLog
--   4/12/2004
--      Do not consider negative balances on debts into the total of debts.
--   7/6/2004
--      Do not discard one-time pull amounts from the initial balance information.
--   12/19/2008
--      Revisions for SQL2008

-- Suppress extra results
set nocount on

-- Find the PAF creditor
declare	@paf_creditor	varchar(10)
select	@paf_creditor	= paf_creditor
from	config with (nolock);

-- Ensure that the pull date is proper
declare	@ach_start_date	datetime
select	@ach_start_date = convert(datetime, convert(varchar(10), @ACH_Pull_Date, 101) + ' 23:59:59'),
	@ach_pull_date  = convert(datetime, convert(varchar(10), @ACH_Pull_Date, 101) + ' 00:00:00')

-- Build a list of the clients and their normal pull values
select	convert(int,1)			as status_group,
	c.client			as client,
	convert(varchar(80),null)	as reason,
	sum(d.deposit_amount) as deposit_amount,
	sum(d.deposit_amount) as original_amount,
	0 as non_ach_amount,
	convert(money,0.0)		as extra_amount,
	ach.ErrorDate		as ach_error_date,
	ach.ABA		as ach_routing_number,
	ach.AccountNumber		as ach_bank_number,
	ach.StartDate		as ach_start_date,
	ach.PrenoteDate		as ach_prenote_date
into	#ach_deposits

FROM	clients c
inner join client_ach ach with (nolock) on c.client = ach.client
INNER JOIN client_deposits d on d.client = c.client AND convert(datetime, convert(varchar(10), d.deposit_date, 101) + ' 00:00:00') = @ACH_Pull_Date

-- Ignore client 0 even if it is active
WHERE	c.client > 0

-- Accept only active clients
and	c.active_status in ('A', 'AR')

-- On ACH
and	ach.isActive = 1

-- With a deposit for ACH
-- and	isnull(d.ach_pull,1) <> 0
and	d.deposit_date = @ach_pull_date

group by c.client, ach.ErrorDate, ach.ABA, ach.AccountNumber, ach.StartDate, ach.PrenoteDate
HAVING sum(d.deposit_amount) > 0

-- Create an index for the client to speed things up in the matching operation
create index #tmp_ach_deposits on #ach_deposits ( client );

-- Find the disbursement information for the clients that will be pulled now
select	cc.client			as 'client',
	sum(cc.disbursement_factor)	as 'disbursement_factor',
	sum(
		case
			when isnull(ccl.zero_balance,0) <> 0 then 0
			when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) < 0 then 0
			when cr.creditor is null then isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
			when cr.creditor = @paf_creditor then 0
			else isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
		end
	)				as 'std_balance',

	sum(
		case
			when cr.creditor is null then 0
			when cr.creditor = @paf_creditor then 0
			when ccl.zero_balance <> 0 then cc.disbursement_factor
			else 0
		end
	)				as 'fee_balance',

	sum(
		case
			when cr.creditor = @paf_creditor then cc.disbursement_factor
			else 0
		end
	)				as 'paf_balance'

into	#ach_disbursement
from	client_creditor cc with (nolock)
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join #ach_deposits c on cc.client = c.client
left outer join creditors cr on cc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where	cc.reassigned_debt	= 0
and	cc.hold_disbursements	= 0
group by cc.client;

-- Add the PAF amount when there is a disbursement
update	#ach_disbursement
set	std_balance = std_balance + paf_balance
where	std_balance > 0;

-- Apply the fees into the balance
update	#ach_disbursement
set	std_balance = std_balance + fee_balance;

CREATE unique index #tmp_ach_disbursement on #ach_disbursement ( client );

-- Reduce the deposit amount by the amount needed for the disbursements
update	#ach_deposits
set	deposit_amount	= b.std_balance
from	#ach_deposits d
inner join #ach_disbursement b on d.client = b.client
where	d.deposit_amount > b.std_balance

-- If the start date is wrong then set the group to 5
update	#ach_deposits
set	reason		= 'START DATE'
where	ach_start_date > @ach_start_date;

-- If there is no prenote, set the group to 3
update	#ach_deposits
set	reason		= 'NOT PRENOTED'
where	ach_prenote_date is null;

-- If there is an error date, set the group to 2
update	#ach_deposits
set	reason		= 'PENDING ERROR'
where	ach_error_date is not null;

-- If there is no account, set the group to 4
update	#ach_deposits
set	reason		= 'MISCONFIGURED CLIENT'
where	ach_bank_number is null
or	len(isnull(ach_routing_number,'')) <> 9
or	non_ach_amount <> 0;

-- Include the one-time ach pulls into the mixture. These are performed after the limits to prevent them from taking part in them.
update	#ach_deposits
set	extra_amount	= a.extra_amount + d.deposit_amount
from	#ach_deposits a
inner join client_deposits d on a.client = d.client
where	d.deposit_date	= @ach_pull_date
-- and	isnull(d.ach_pull,1) <> 0
and	isnull(d.one_time,0) <> 0

select  x.extra_amount,
	x.client				as 'client',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
	x.status_group				as 'status_group',
	x.original_amount			as 'deposit_amount',

	case
		when x.reason is null then x.deposit_amount + x.extra_amount
		else convert(money,0)
	end					as 'normal_amount',

	case
		when x.reason is not null then x.reason
		when x.extra_amount > 0 then 'ONE TIME EXTRA AMOUNT'
		when x.deposit_amount <> x.original_amount then 'PAYOFF DEBT BALANCE ONLY'
		else null
	end					as 'reason',

	case
		when x.reason is not null then convert(money,0)
		else x.original_amount - x.deposit_amount - x.extra_amount
	end					as 'difference'

from	#ach_deposits x
left outer join people p on x.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
order by 2; -- client

-- Discard the working tables at this point
drop table #ach_deposits
drop table #ach_disbursement

return ( @@rowcount )
GO
