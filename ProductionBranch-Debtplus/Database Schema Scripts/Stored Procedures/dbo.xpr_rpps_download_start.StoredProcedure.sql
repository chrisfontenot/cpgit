USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_download_start]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_rpps_download_start] ( @biller_id as varchar(10), @acdind as varchar(1) = 'A' ) AS

-- ========================================================================================================
-- ==            Remove the information in preparation for loading the biller id                         ==
-- ========================================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   6/2/2003
--     Change to support "dual" (both proposals and payments) billers
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

set nocount on

-- Validate the A/C/D indicator
select @acdind = left(lower(ltrim(rtrim(@acdind)))+' ',1)

-- If this is a delete operation then remove the references to the "dead" biller ids
if @acdind = 'D'
begin
	-- Remove the references from the creditor
	create table #creditors (creditor varchar(10), creditor_id int null, type varchar(10));

	insert into #creditors (creditor, creditor_id, type)
	select cr.creditor, cm.creditor, cm.type
	from		creditor_methods cm
	inner join	rpps_biller_ids ids on cm.rpps_biller_id = ids.rpps_biller_id
	inner join	creditors cr on cm.creditor = cr.creditor_id
	inner join	banks b on cm.bank = b.bank
	where		b.type = 'R'
	and		ids.rpps_biller_id = @biller_id

	insert into creditor_notes (creditor, is_text, type, subject, note)
	select		creditor, 1, 3, 'RPPS biller ID removed', 'The RPPS biller id was removed because it was deleted by mastercard.'
	from		#creditors
	where		type <> 'EFT';

	insert into creditor_notes (creditor, is_text, type, subject, note)
	select		creditor, 1, 3, 'RPPS biller ID removed', 'The RPPS biller id was removed because it was deleted by mastercard. The creditor was returned to check status.'
	from		#creditors
	where		type = 'EFT';

	delete		creditor_methods
	from		creditor_methods cm
	inner join	#creditors x on cm.creditor_method = x.creditor_method

	drop table #creditors;

	-- Remove the AKA referneces
	delete
	from	rpps_akas
	where	rpps_biller_id	= @biller_id;

	-- Remove the masks from the system
	delete
	from	rpps_masks
	where	rpps_biller_id = @biller_id;

	-- Remove the references from the LOB items
	delete
	from	rpps_lobs
	where	rpps_biller_id = @biller_id;

	-- Remove the references from the addresses
	delete
	from	rpps_addresses
	where	rpps_biller_id = @biller_id;

	-- Remove the biller id from the system
	delete
	from	rpps_biller_ids
	where	rpps_biller_id = @biller_id;

end else begin

	-- Remove the AKA references
	delete
	from	rpps_akas
	where	rpps_biller_id	= @biller_id;

	-- Remove the references from the LOB items
	delete
	from	rpps_lobs
	where	rpps_biller_id = @biller_id;

	-- Remove the references from the addresses
	delete
	from	rpps_addresses
	where	rpps_biller_id = @biller_id;

	-- Remove the masks
	delete
	from	rpps_masks
	where	rpps_biller_id = @biller_id;

	-- Ensure that the item is present in the biller id table.
	if not exists (select * from rpps_biller_ids where rpps_biller_id = @biller_id)
		insert into rpps_biller_ids (rpps_biller_id, biller_type, biller_name)
		values (@biller_id, 1, 'undefined biller')
end

-- Return succes
return ( 1 )
GO
