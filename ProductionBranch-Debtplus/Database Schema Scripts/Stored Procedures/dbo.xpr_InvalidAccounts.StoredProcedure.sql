USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_InvalidAccounts]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_InvalidAccounts] AS

-- ==========================================================================================
-- ==            Process the invalid account list                                          ==
-- ==========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Create the table to hold the selection information
CREATE TABLE ##t_valid_accounts (
	client		INT NULL,
	creditor	VARCHAR(10) NULL,
	client_creditor	INT,
	account_number	VARCHAR(50),
	client_name	VARCHAR(255) NULL,
	creditor_name	VARCHAR(255) NULL,
	account_mask	VARCHAR(255) NULL,
	account_length	INT NULL,
	checksum	INT NULL,
	error_condition	INT NULL,
	rpps_biller_id	VARCHAR(10) NULL,
	type            varchar(1) NULL,
	creditor_id     INT NULL
);

-- Insert the items into the temporary table
INSERT INTO ##t_valid_accounts ( client,creditor,client_creditor,account_number,client_name,creditor_name,rpps_biller_id, type, creditor_id )
SELECT		cc.client,
			cc.creditor,
			cc.client_creditor,
			isnull (cc.account_number,'MISSING'),
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			cr.creditor_name,
			rpps.rpps_biller_id,
			cr.type,
			cr.creditor_id
FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c		WITH (NOLOCK) ON cc.client = c.client
INNER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
INNER JOIN	people p		WITH (NOLOCK) ON cc.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
INNER JOIN      creditor_methods cm     WITH (NOLOCK) ON cr.creditor_id = cm.creditor AND 'EFT' = cm.type
INNER JOIN	rpps_biller_ids rpps	WITH (NOLOCK) ON cm.rpps_biller_id=rpps.rpps_biller_id
LEFT OUTER JOIN	creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class

WHERE		c.active_status IN ('A','AR')
AND			cc.reassigned_debt=0
AND			isnull(ccl.agency_account,0) = 0
AND			isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)>isnull(bal.total_payments,0)
AND			cc.disbursement_factor > 0

ORDER BY	8, 9, 2 -- type, creditor_id, creditor

RETURN ( @@rowcount )
GO
