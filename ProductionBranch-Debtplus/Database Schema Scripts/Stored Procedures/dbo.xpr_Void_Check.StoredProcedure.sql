SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[xpr_Void_Check] ( @trust_register AS INT, @Message AS typ_message = NULL ) AS
-- ================================================================================================================
-- ==                   Void a check                                                                             ==
-- ================================================================================================================

-- ChangeLog
--   2/17/2002
--     Added client register transaction for deduct refund amount on client 0
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   9/17/2003
--     Added back the support for the bank_xmit_date on voids
--   11/10/2003
--     Made voiding client 0 refund check a client refund void.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

declare @check_date		datetime
declare	@checknum		BigInt
declare @creditor		typ_creditor
declare	@client			typ_client
declare @date_created		datetime
declare @amount			money
declare	@tran_type		typ_transaction
declare	@invoice_register	int
DECLARE	@disbursed_amount	money
DECLARE	@deducted		money
DECLARE	@billed			money
declare	@cursor_client		typ_client
declare @cursor_client_creditor int
declare	@cursor_creditor	typ_creditor
declare	@cursor_creditor_type	char(1)
declare	@cursor_gross		money
declare	@cursor_fairshare	money
declare	@cursor_register	int

-- Suppress intermediate results
SET NOCOUNT ON

-- Obtain the information for the transaction
SELECT	@client			= client,
	@tran_type		= tran_type,
	@creditor		= creditor,
	@check_date		= date_created,
	@amount			= amount,
	@checknum		= checknum,
	@invoice_register	= invoice_register

FROM	registers_trust WITH (NOLOCK)

WHERE	trust_register	= @trust_register
AND	tran_type IN ('AR', 'MR', 'CR', 'AD', 'MD', 'CM')
AND	isnull(cleared,' ') = ' '

-- If this is a client check then void the client amount
if (@client IS NOT NULL) AND (@tran_type IN ('AR', 'MR', 'CR'))
BEGIN
	BEGIN TRANSACTION
	update	clients
	set		held_in_trust	= held_in_trust + @amount
	where	client			= @client

	insert into registers_client (tran_type, client, credit_amt, message, trust_register)
	values	('VR', @client, @amount, @message, @trust_register)

	update	registers_trust
	set		cleared			= 'V',
			reconciled_date	= getdate(),
			bank_xmit_date	= null
	where	trust_register	= @trust_register

	COMMIT TRANSACTION
	return ( 1 )
END

-- Handle the voiding of the deduct check. This is really a client refund so we can do a client refund void.
-- The problem is that there is no guarantee that the debt exists so we can't create a debt transaction that would
-- be required for a creditor check void operation.

declare	@deduct_creditor	typ_creditor
select	@deduct_creditor	= deduct_creditor
from	config

IF (@creditor is not null) AND (@creditor = @deduct_creditor) AND (@tran_type = 'CM')
BEGIN
	BEGIN TRANSACTION
	update	clients
	set		held_in_trust	= held_in_trust + @amount
	where	client			= 0

	insert into registers_client (tran_type, client, credit_amt, message, trust_register)
	values	('VR', 0, @amount, @message, @trust_register)

	insert into registers_creditor (tran_type, creditor, credit_amt, message, trust_register)
	values	('VD', 0, @amount, @message, @trust_register)

	update	registers_trust
	set	cleared			= 'V',
		reconciled_date		= getdate(),
		bank_xmit_date		= null
	where	trust_register		= @trust_register

	COMMIT TRANSACTION
	return ( 1 )
END	

-- This must be a creditor check. The details are in the client_creditor register table.
if (@creditor IS NOT NULL) AND (@tran_type IN ('AD', 'CM', 'MD'))
begin

	BEGIN TRANSACTION

	-- generate the client transactions showing the refunds amounts
	select	*
	into	#t_void_list
	from	registers_client_creditor
	where	trust_register		= @trust_register
	AND		tran_type			= @tran_type

	-- Determine the gross, deducted, and billed amounts
	SELECT	@disbursed_amount	= sum(debit_amt),
			@deducted			= sum(case creditor_type when 'D' then fairshare_amt else 0 end),
			@billed				= sum(case creditor_type when 'B' then fairshare_amt else 0 end)
	FROM	#t_void_list

	-- Adjust the "gross" amount to be a "net" amount
	SET @disbursed_amount = @disbursed_amount - @deducted

	-- The two "net" amounts should match or we are in trouble right here in river city.
	IF @disbursed_amount != @amount
	BEGIN
		declare @p1	varchar(50)
		declare @p2	varchar(50)
		SET		@p1 = convert(varchar, @disbursed_amount, 1)
		set		@p2 = convert(varchar, @amount, 1)

		ROLLBACK TRANSACTION
		RaisError(50078, 16, 1, @p1, @p2)
		drop table #t_void_list
		RETURN ( 0 )
	END

	-- Insert the creditor record that the item is voided
	insert into registers_creditor (tran_type, creditor, credit_amt, message, trust_register)
	values	('VD', @creditor, @amount, @message, @trust_register)

	-- void the check
	update	registers_trust
	set		cleared					= 'V',
			reconciled_date			= getdate(),
			bank_xmit_date			= null
	where	trust_register			= @trust_register

	-- Return the money for the creditor
	update	creditors
	set		distrib_mtd				= distrib_mtd - @disbursed_amount,
			contrib_mtd_billed		= contrib_mtd_billed	- @billed,
			contrib_mtd_received	= contrib_mtd_received	- @deducted
	where	creditor				= @creditor

	-- If there is a deducted amount then adjust the deducted balance figure
	if @deducted > 0
	begin
		if @deduct_creditor IS NOT NULL
			update	creditors
			set		distrib_mtd	= distrib_mtd - @deducted
			where	creditor	= @deduct_creditor

		update	clients
		set		held_in_trust	= held_in_trust - @deducted
		where	client			= 0

		-- For client 0, the deduction amount is a debit (removed from trust)
		insert into registers_client	(tran_type,	debit_amt,	client,	trust_register,		message)
		values				('VD',		@deducted,	0,	@trust_register,	'Deduct Contribution for VD' + isnull(' of check #' + convert(varchar,@checknum),''))
	end

	-- Insert the messages indicating the refund amounts for the client
	insert registers_client (tran_type, client, credit_amt, trust_register, message)
	select	'VD', client, sum(debit_amt), @trust_register, @message
	from	#t_void_list
	group by client

	-- Void the items on the disbursement check
	declare	void_cursor CURSOR FORWARD_ONLY READ_ONLY FOR
		select	client_creditor_register, client, creditor, client_creditor, debit_amt, fairshare_amt, creditor_type
		from	#t_void_list

	open void_cursor

	fetch void_cursor into @cursor_register, @cursor_client, @cursor_creditor, @cursor_client_creditor, @cursor_gross, @cursor_fairshare, @cursor_creditor_type
	while @@fetch_status = 0
	begin
		-- Adjust the client trust balance
		update	clients
		set		held_in_trust			= held_in_trust + @cursor_gross
		where	client					= @cursor_client

		-- Adjust the debt record
		update	client_creditor_balances
		set		payments_month_0		= payments_month_0 - @cursor_gross,
				total_payments			= total_payments - @cursor_gross
		from	client_creditor_balances bal
		inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
		where	cc.client_creditor		= @cursor_client_creditor

		-- Count the amount as a refund for the instance of this debt
		update	client_creditor
		set		returns_this_creditor	= isnull(returns_this_creditor,0) + @cursor_gross
		from	client_creditor cc
		where	cc.client_creditor		= @cursor_client_creditor

		-- Mark the original transaction as void
		update	registers_client_creditor
		set		void = 1
		where	client_creditor_register = @cursor_register

		-- Record the clent/creditor transaction that the item is voided
		insert into registers_client_creditor (tran_type, client, creditor, client_creditor, credit_amt, fairshare_amt, creditor_type, trust_register)
		values ('VD', @cursor_client, @cursor_creditor, @cursor_client_creditor, @cursor_gross, @cursor_fairshare, @cursor_creditor_type, @trust_register)

		fetch void_cursor into @cursor_register, @cursor_client, @cursor_creditor, @cursor_client_creditor, @cursor_gross, @cursor_fairshare, @cursor_creditor_type
	end
	close void_cursor
	deallocate void_cursor

	-- Discard the item table. It is no longer needed.
	drop table #t_void_list

	-- Adjust the invoice information
	if (@invoice_register is not null) AND (@billed > 0)
	begin
		insert into registers_creditor (tran_type, creditor, invoice_register, credit_amt, message)
		values ('RA', @creditor, @invoice_register, @billed, @message)

		update	registers_invoices
		set		adj_amount			= isnull(adj_amount,0) + @billed,
				adj_date			= getdate()
		where	invoice_register	= @invoice_register
	end
	COMMIT TRANSACTION
	return ( 1 )
end

-- The transaction is invalid at this point
RaisError(50079, 16, 1)
return ( 0 )
GO
