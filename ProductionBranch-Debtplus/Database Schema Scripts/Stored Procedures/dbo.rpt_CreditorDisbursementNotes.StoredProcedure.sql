USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CreditorDisbursementNotes]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_CreditorDisbursementNotes] AS
-- ===========================================================================================
-- ==            Fetch the last disbursement if one was not supplied                        ==
-- ===========================================================================================

-- ChangeLog
--   12/8/2002
--     Processed the creditor flag "drop_notice" correctly
--   4/8/2003
--     Removed test for note type 1 in the reprint logic.
--   4/23/2003
--     Changed logic to deal with EDI creditors
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   8/11/2004
--     Do not generate notes for reassigned debts

SET NOCOUNT ON

-- Find the date ranges for the current date
declare	@todays_batch_label	varchar(80)
select	@todays_batch_label = 'printed ' + convert(varchar(10), getdate(), 101)

-- ===========================================================================================
-- ==            Explode the client debts into the creditor list                            ==
-- ===========================================================================================

SET ANSI_PADDING OFF

CREATE table #results (
		debt_note			int,
		current_balance		money null,
		client_creditor		int null,
		creditor			varchar(10),
		postalcode			varchar(50)	 null,
		cr_type				varchar(4)	 null,
		cr_creditor_id		int			 null,
		client_id			int			 null,
		client_name			varchar(160) null,
		account_number		varchar(40)	 null,
		note_text			varchar(80)	 null,
		amount				money		 null,
		note_date			datetime	 null,
		rpps_biller_id		varchar(80)  null,
		epay_biller_id		varchar(80)	 null,
		has_clo             int          null,
		reassigned_debt		int			 null
);

-- ===========================================================================================
-- ==            Load the items which do not specify a debt. (old format)                   ==
-- ===========================================================================================

-- Generate a temporary table with the result sets
insert into #results (debt_note, cr_type, cr_creditor_id, client_creditor, note_text, amount, note_date, has_clo, reassigned_debt)
SELECT		n.debt_note							as 'debt_note',
			cr.type								as 'cr_type',
			cr.creditor_id						as 'cr_creditor_id',
			cc.client_creditor					as 'client_creditor',
			convert(varchar(80), note_text)		as 'note_text',
			n.note_amount						as 'amount',
			n.note_date							as 'note_date',
			0                                   as 'has_clo',
			cc.reassigned_debt					as 'reassigned_debt'

FROM		debt_notes n				WITH (NOLOCK)
inner join	client_creditor cc			WITH (NOLOCK) ON n.client_creditor = cc.client_creditor
INNER JOIN	creditors cr				WITH (NOLOCK) ON cc.creditor = cr.creditor
WHERE		n.type			in ('DR', 'AN', 'SN')
AND			(output_batch is null or output_batch = @todays_batch_label)

-- Mark the ones that have a CLO creditor method entry
update		#results
set			has_clo				= 1
from		#results r
inner join	creditor_methods cm on r.cr_creditor_id = cm.creditor and 'CLO' = cm.type;

-- Toss the creditor messages where the closout entry is missing from the system
delete		debt_notes
from		debt_notes n
inner join	#results r on n.debt_note = r.debt_note
where		has_clo				= 0
or			reassigned_debt		<> 0;

-- Discard the items that are missing the drop notice entry. We don't send those
delete
from		#results
where		has_clo				= 0
or			reassigned_debt		<> 0;

-- Set the RPPS biller id if possible
update		#results
set			rpps_biller_id		= ids.rpps_biller_id
from		#results d
inner join	creditor_methods cm on d.cr_creditor_id = cm.creditor and 'CLO' = cm.type
inner join	banks b on cm.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids ids on cm.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks m on ids.rpps_biller_id = m.rpps_biller_id
where		d.account_number like dbo.map_rpps_masks ( m.mask )

-- Set the ePay biller id if possible
update		#results
set			epay_biller_id		= ids.epay_biller_id
from		#results d
inner join	creditor_methods cm on d.cr_creditor_id = cm.creditor and 'CLO' = cm.type
inner join	banks b on cm.bank = b.bank and 'E' = b.type
inner join	epay_biller_ids ids on cm.epay_biller_id = ids.epay_biller_id
inner join	epay_masks m on ids.epay_biller_id = m.biller_id
where		d.account_number like dbo.map_epay_masks ( m.mask )

-- Toss all of the EFT transactions. They will be sent separately.
delete
from		#results
where		rpps_biller_id is not null
or			epay_biller_id is not null;

-- Supply some account information as needed
update		#results
set			creditor				= v.creditor,
			client_id				= v.client,
			client_name				= v.client_name,
			account_number			= v.account_number,
			current_balance			= v.current_balance
from		#results r
INNER JOIN	view_last_payment v WITH (NOLOCK) ON r.client_creditor = v.client_creditor

-- ===========================================================================================
-- ==            Set the postalcode for the report ordering                                 ==
-- ===========================================================================================
update		#results
set			postalcode		= a.postalcode
from		#results r
inner join	creditor_addresses ca with (Nolock) ON r.creditor = ca.creditor AND ca.type = 'L'
inner join  addresses a on ca.addressid = a.[address];

update		#results
set			postalcode		= a.postalcode
from		#results r
inner join	creditor_addresses ca with (Nolock) ON r.creditor = ca.creditor AND ca.type = 'P'
inner join addresses a on ca.AddressID = a.[address]
where		r.postalcode is null;

-- ===========================================================================================
-- ==            Mark the notes as having been printed                                      ==
-- ===========================================================================================
update		debt_notes
set			output_batch			= @todays_batch_label
from		debt_notes n
inner join	#results r on n.debt_note = r.debt_note;

-- Mark these as having been sent the notice
update		client_creditor
set			drop_reason_sent = GETDATE()
from		client_creditor cc with (nolock)
inner join	#results x on cc.client_creditor = x.client_creditor

-- ===========================================================================================
-- ==            Return the combined list                                                   ==
-- ===========================================================================================
select 		v.creditor,
			v.client_id,
			v.client_name,
			v.account_number,
			v.note_text,
			v.amount,
			v.note_date,
			getdate() as disbursement_date,

			convert(varchar(160), isnull(dbo.format_address_line_1(ca.house,ca.direction,ca.street,ca.suffix,ca.modifier,ca.modifier_value),''))			as 'client_addr1',
			convert(varchar(160), isnull(ca.address_line_2,''))			as 'client_addr2',
			convert(varchar(160), isnull(dbo.format_city_state_zip (ca.city, ca.state, ca.postalcode),'')) as 'client_addr3',
			convert(varchar(80),  isnull(dbo.format_TelephoneNumber (c.HomeTelephoneID),'')) as 'client_phone'

from		#results v
INNER JOIN	client_creditor cc WITH (NOLOCK) on v.client_creditor = cc.client_creditor
LEFT OUTER JOIN	clients c on cc.client = c.client
LEFT OUTER JOIN addresses ca on c.addressid = ca.address

ORDER BY	v.postalcode, v.cr_type, v.cr_creditor_id, v.creditor, v.client_id;

drop table #results
return ( @@rowcount )
GO
