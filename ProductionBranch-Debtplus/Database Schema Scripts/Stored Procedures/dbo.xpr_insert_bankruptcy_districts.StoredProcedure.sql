USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_bankruptcy_districts]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_bankruptcy_districts] ( @description as varchar(50), @default as bit = 0, @ActiveFlag as bit = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the bankruptcy_districts table           ==
-- ========================================================================================
	insert into bankruptcy_districts ( [description],[default],[ActiveFlag] ) values ( @description, @default, @ActiveFlag )
	return ( scope_identity() )
GO
