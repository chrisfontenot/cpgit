USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintChecks_Mark_Single]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintChecks_Mark_Single] ( @bank as int = -1, @TrustRegister AS INT = -1 ) AS

-- ==========================================================================================
-- ==			Print the indicated check 					   ==
-- ==========================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

if isnull(@bank,-1) <= 0
	select	@bank	= min(bank)
	from	banks with (nolock)
	where	type	= 'C'

-- Fetch the next marker string
DECLARE	@Marker		VarChar(50)
SELECT	@Marker = newid()

-- Do the standard routine to mark a selected check with the indicated sequence
DECLARE	@Items		INT
EXECUTE @Items = rpt_PrintChecks_Mark_Selected @bank, @Marker, @TrustRegister

-- Return the marker to the calller along with the number of items selected
SELECT	@Marker as id,
	@Items as items

-- Return the number of rows in the result set
RETURN ( 1 )
GO
