USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_epay_response]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_epay_response] ( @epay_response_file int = null ) as
-- ============================================================================================
-- ==            Response information for ePay                                               ==
-- ============================================================================================

-- Surpress intermediate results
set nocount on

-- Find the file if one was not supplied
if @epay_response_file is null
begin
	select	top 1
		@epay_response_file = epay_response_file
	from	epay_response_files with (nolock)
	order by date_created desc
end

-- There should be a file or we are in trouble.
if @epay_response_file is null
begin
	RaisError ('There are no pending response files', 16, 1)
	return ( 0 )
end

-- Return the information from the database tables for the response file
select	epay_response_file				as epay_response_file,
	label						as label,
	date_created					as date_created
from	epay_response_files with (nolock)
where	epay_response_file = @epay_response_file

return ( @@rowcount )
GO
