USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_districts]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_districts] as
select	district as item_key,
	description as description
from	districts with (nolock)
order by 2
GO
