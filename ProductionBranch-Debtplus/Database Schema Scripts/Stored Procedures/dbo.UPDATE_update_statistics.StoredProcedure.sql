USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_update_statistics]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_update_statistics] as

declare @table varchar(80)
declare table_cursor cursor for
	select	name
	from	sysobjects
	where	type = 'U'
	and	name not like 'dt_%'
	order by 1

declare	@stmt	varchar(800)
open table_cursor
fetch table_cursor into @table

while @@fetch_status = 0
begin
	select @stmt = 'UPDATE STATISTICS [' + @table + '] WITH FULLSCAN'
	exec ( @stmt )
	fetch table_cursor into @table
end

close table_cursor
deallocate table_cursor
GO
