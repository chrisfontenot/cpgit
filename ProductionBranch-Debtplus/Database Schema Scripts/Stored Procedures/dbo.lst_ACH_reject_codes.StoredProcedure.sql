USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_ACH_reject_codes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_ACH_reject_codes] AS

-- ====================================================================================================
-- ==            Retrieve the list of error conditions for ACH operations                            ==
-- ====================================================================================================

SELECT	ach_reject_code		as 'item_key',
	[description]		as 'description'
FROM	ach_reject_codes
ORDER BY ach_reject_code

RETURN ( @@rowcount )
GO
