USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_counselor_attribute]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_counselor_attribute] ( @counselor as int, @attribute as int ) as
	insert into counselor_attributes ( counselor, attribute ) values (@counselor, @attribute)
	return (scope_identity())
GO
