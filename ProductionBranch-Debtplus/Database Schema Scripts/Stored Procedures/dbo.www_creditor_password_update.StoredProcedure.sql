USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_password_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_password_update] ( @creditor as typ_creditor, @old_password as varchar(32) = NULL, @new_password as varchar(32) = NULL ) AS
-- =========================================================================================================
-- ==            Update the password for the creditor                                                     ==
-- =========================================================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that the client exists
if not exists ( select creditor from creditors where creditor = @creditor )
begin
	return ( 0 )
end

-- If the old password is empty then ensure that here is no client. If not, add one.
if @old_password is null
begin
	if exists ( select * from creditor_www where creditor = @creditor )
		return ( 0 )
	insert into creditor_www (creditor,password) values (@creditor, @new_password)
	return ( @@rowcount )
end

-- Ensure that the password matches the old value
if not exists ( select * from creditor_www where creditor = @creditor and [password] = @old_password )
begin
	return ( 0 )
end

-- Update the database value
update	creditor_www
set	[password]	= @new_password
where	[password]	= @old_password
and	creditor	= @creditor

return ( @@rowcount )
GO
