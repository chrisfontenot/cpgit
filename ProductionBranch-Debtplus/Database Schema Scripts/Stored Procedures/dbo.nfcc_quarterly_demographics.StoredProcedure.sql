USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_quarterly_demographics]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nfcc_quarterly_demographics] ( @from_date as datetime = null, @to_date as datetime = null )  AS 

-- This is the name of the resulting sheet in the spreadsheet output
declare	@SheetName	varchar(80)
select	@SheetName	= 'Demographics'

-- Find last quarter if required
if (@to_date is null) and (@from_date is null)
begin
	select	@from_date	= min(dt) from calendar where y = datepart(year, getdate()) and q = (select q from calendar where dt = convert(varchar(10), getdate(), 101))
	select	@from_date	= dateadd(m, -3, @from_date)
	select	@to_date	= max(dt) from calendar where y = datepart(year, @from_date) and q = (select q from calendar where dt = convert(varchar(10), @from_date, 101))
end

select	@from_date	= convert(varchar(10), @from_date, 101) + ' 00:00:00',
	@to_date	= convert(varchar(10), @to_date, 101)   + ' 23:59:59';

-- Table to hold the results
create table #results ( sheet varchar(80), location varchar(80), [value] varchar(80) null);

-- Build a list of the clients who are counseled and their corresponding states
create table #sessions (client int, age int null, gender int null, marital_status int null, people int null, income money null, ami money null, ethnic int null, race int null, employer int null, unsecured_debt money null, mortgage_costs money null, creditor_count int null, line int null, [column] varchar(1), appt_type int null, housing int null, bankruptcy int null);

-- One on One financial counseling sessions are easy
insert into #sessions (client, [column], age, gender, marital_status, people, ethnic, race, employer, appt_type, housing, bankruptcy)
select	c.client, 'B', isnull(datediff(year, p.birthdate, getdate()),0), p.gender, c.marital_status, c.dependents + (case c.marital_status when 2 then 2 else 1 end) as people, p.ethnicity, p.race, p.employer, ca.appt_type, apt.housing, apt.bankruptcy
from	clients c
left outer join people p on c.client = p.client and 1 = p.relation
left outer join client_appointments ca on c.client = ca.client
inner join appt_types apt on ca.appt_type = apt.appt_type
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null;

-- Prefiling Bankruptcy
update #sessions set [column] = 'E'
where	housing = 0 and bankruptcy = 1;

-- Housing Counseling
update #sessions set [column] = 'C'
where	housing <> 0 and bankruptcy = 0;

-- Reverse Mortgage
update #sessions set [column] = 'D'
where	[column] = 'C' and appt_type in (1000);

-- Find the age breakdown
update	#sessions set line = 0;
update	#sessions set line = 8  where age between  1 and 17;
update	#sessions set line = 9  where age between 18 and 24;
update	#sessions set line = 10 where age between 25 and 34;
update	#sessions set line = 11 where age between 35 and 44;
update	#sessions set line = 12 where age between 45 and 54;
update	#sessions set line = 13 where age between 55 and 64;
update	#sessions set line = 14 where age >= 65;
update	#sessions set line = 15 where age <= 0;

insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line), count(*)
from	#sessions
where	line > 0
group by line, [column];

-- Find the gender breakdown
update	#sessions set line = 19;
update	#sessions set line = 18 where gender = 1;

insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line), count(*)
from	#sessions
where	line > 0
group by line, [column];

-- Find the marital status breakdown
update	#sessions set line = 29;
update	#sessions set line = 23 where marital_status = 1;
update	#sessions set line = 24 where marital_status = 2;
update	#sessions set line = 27 where marital_status = 3;
update	#sessions set line = 28 where marital_status = 4;
update	#sessions set line = 25 where marital_status = 5;

insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line), count(*)
from	#sessions
where	line > 0
group by line, [column];

-- Find the average # in household
insert into #results (sheet, location, value)
select	@SheetName, [column] + '32', convert(varchar, convert(int,avg(convert(float,people)) + 0.50))
from	#sessions
group by [column];

-- Find the client income and ami values
update	#sessions set
	income = dbo.client_income(client),
	ami = dbo.ami(client),
	unsecured_debt = dbo.client_original_unsecured_debt(client),	
	creditor_count = dbo.client_creditor_count(client),
	line = 35;

-- Find the income breakdown
update      #sessions set line = 36 where income > ami;
update      #sessions set line = 37 where income > (1.60 * ami);
update      #sessions set line = 38 where income > (2.0 * ami);
update      #sessions set line = 39 where ami <= 0;

insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line), count(*)
from	#sessions
where	line > 0
group by line, [column];

-- Find the ethnic breakdown
update	#sessions set line = 44;
update #sessions set line = 43, race = 3 where ethnic = 1;
 
insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line), count(*)
from	#sessions
where	line > 0
group by line, [column];

-- Find the race breakdown
update	#sessions set line = 59;
update	#sessions set line = 49 where race = 3;
update	#sessions set line = 50 where race = 1;
update	#sessions set line = 51 where race = 2;
update	#sessions set line = 52 where race = 9;
update	#sessions set line = 53 where race = 11;
update	#sessions set line = 54 where race = 12;
update	#sessions set line = 55 where race = 13;
update	#sessions set line = 56 where race = 14;
update	#sessions set line = 57 where race = 7;
update	#sessions set line = 58 where race = 5;

insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line), count(*)
from	#sessions
where	line > 0
group by line, [column];

-- Employment status
update	#sessions set line = 63;

update	#sessions
set	line	= 64
from	#sessions s
inner join people p on s.client = p.client and 1 = p.relation
where	p.job is not null
or	isnull(p.other_job,'') <> '';

update	#sessions
set	line	= 65
from	#sessions s
inner join people p on s.client = p.client and 1 = p.relation
inner join job_descriptions j on p.job = j.job_description
where	j.description = 'retired';

update	#sessions
set	line	= 63
from	#sessions s
inner join people p on s.client = p.client and 1 = p.relation
inner join job_descriptions j on p.job = j.job_description
where	j.description in ('unemployed', 'un-employed');

insert into #results (sheet, location, value)
select	@SheetName, [column] + convert(varchar, line), count(*)
from	#sessions
where	line > 0
group by line, [column];

-- Mortgage delinquencies
insert into #results (sheet, location, value)
select	@SheetName, [column] + '68', convert(varchar, count(*))
from	#sessions s
inner join clients c on s.client = c.client
where   c.mortgage_problems = 1
group by [column];

-- Find the average annual income
insert into #results (sheet, location, value)
select	@SheetName, [column] + '70', convert(varchar, avg(convert(float,income)))
from	#sessions
group by [column];

-- Find the unsecured debt
insert into #results (sheet, location, value)
select	@SheetName, [column] + '71', convert(varchar, avg(convert(float,unsecured_debt)))
from	#sessions
group by [column];

-- Find the creditor count
insert into #results (sheet, location, value)
select	@SheetName, [column] + '72', convert(varchar, avg(convert(float,creditor_count)))
from	#sessions
group by [column];

-- Return the results and exit
select * from #results;
drop table #results;
drop table #sessions

return ( 0 );
GO
