USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_client_action_items]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_client_action_items] ( @client_action_item int ) as
	delete from client_action_items
    where client_action_item = @client_action_item
    return ( @@rowcount )
GO
