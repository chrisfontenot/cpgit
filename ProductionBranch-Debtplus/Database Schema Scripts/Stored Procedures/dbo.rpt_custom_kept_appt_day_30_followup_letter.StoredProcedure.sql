USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_kept_appt_day_30_followup_letter]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_kept_appt_day_30_followup_letter] as
-- =================================================================================================================
-- ==            Print all of the internet followup letters that have not been printed                            ==
-- =================================================================================================================

-- Suppress intermediate results
set nocount on

-- Remove the printed status from letters printed today
declare	@now	datetime
select	@now	= convert(varchar(10), getdate(), 101)

-- Discard old printed items from the system
delete
from	customfollowupevents
where	date_printed	< dateadd(d, -30, @now)

-- Mark the items as "not printed" if they were printed today. This is simply a re-print operation.
update	customfollowupevents
set	date_printed	= null
where	date_printed	between @now and dateadd(s, -1, dateadd(d, 1, @now))

-- Find the items that should be printed in this batch
select * into #results from customfollowupevents where date_printed is null

-- Mark the appointments as having been printed
update	customfollowupevents
set	date_printed = getdate()
from	customfollowupevents e
inner join #results r on e.client_appointment = r.client_appointment

-- Return the information needed to print the report
select	r.client,
	coalesce(c.salutation, dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ),'') as salutation,
	r.client_appointment,
	ca.start_time,
	r.letter_date,
	d.managername
from	#results r
inner join client_appointments ca with (nolock) on r.client_appointment = ca.client_appointment
inner join clients c with (nolock) on r.client = c.client
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join addresses a with (nolock) on c.addressid = a.address
left outer join offices o on o.office = c.office
left outer join districts d on d.district = o.district
order by a.postalcode

drop table #results
return ( @@rowcount )
GO
