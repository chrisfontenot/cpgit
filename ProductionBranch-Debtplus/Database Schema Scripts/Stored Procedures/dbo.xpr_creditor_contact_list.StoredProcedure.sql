USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contact_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contact_list] ( @Creditor as typ_creditor ) AS

-- ==================================================================================================================
-- ==            Return the specific creditor contact information                                                  ==
-- ==================================================================================================================

-- ChangeLog
--   2/18/2002
--     Added space after middle and before last name

SELECT
	c.creditor_contact																	as 'creditor_contact',
	coalesce(t.[name],'Unknown type ' + convert(varchar, c.creditor_contact_type), '')	as 'creditor_contact_type',
	dbo.format_normal_name(cn.prefix,cn.first,cn.middle,cn.last,cn.suffix)					as 'name',
	
	dbo.format_TelephoneNumber(c.TelephoneID)											as 'phone',
	dbo.format_TelephoneNumber(c.FAXID)													as 'fax',
	
	c.creditor_contact_type										as 'input_creditor_contact_type',
	cn.prefix													as 'input_prefix',
	cn.first														as 'input_first',
	cn.middle													as 'input_middle',
	cn.last														as 'input_last',
	cn.suffix													as 'input_suffix',
	c.title														as 'input_title',
	dbo.format_address_line_1(ca.house,ca.direction,ca.street,ca.suffix,ca.modifier,ca.modifier_value)		as 'input_address1',
	ca.address_line_2											as 'input_address2',
	ca.city														as 'input_city',
	ca.state														as 'input_state',
	ca.PostalCode												as 'input_postalcode',
	tn.number														as 'input_phone',
	tn.ext													as 'input_extension',
	f.number														as 'input_fax',
	ce.address														as 'input_email',
	c.notes														as 'input_notes',
	
	dbo.format_city_state_zip( ca.city, ca.state, ca.PostalCode )	as 'address3'

FROM	creditor_contacts c with (nolock)
LEFT OUTER JOIN	creditor_contact_types t WITH (NOLOCK) ON c.creditor_contact_type = t.creditor_contact_type
left outer join addresses ca with (nolock) on c.AddressID = ca.Address
left outer join EmailAddresses ce with (nolock) on c.EmailID = ce.Email
left outer join TelephoneNumbers tn WITH (nolock) ON c.TelephoneID = tn.TelephoneNumber
left outer join TelephoneNumbers f with (nolock) on c.FAXID = f.TelephoneNumber
left outer join Names cn WITH (NOLOCK) ON c.NameID = cn.Name
WHERE	c.creditor = @creditor

RETURN ( @@rowcount )
GO
