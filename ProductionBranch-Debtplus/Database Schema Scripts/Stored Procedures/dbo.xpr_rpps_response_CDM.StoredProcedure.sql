USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_CDM]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_CDM] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(11), @error_code as varchar(3) = null, @name as varchar(5) = null, @account_number as varchar(50) = null, @net as money = 0, @line_number as int, @message as varchar(80) = null) as

-- ================================================================================================
-- ==            Process a creditor message response                                             ==
-- ================================================================================================

-- Suppress intermediate results
set nocount on

-- Add the record to the table
if @message is not null
begin
	select @message = ltrim(rtrim(@message))
	if @message = ''
		select	@message = null
end

declare @result	int
select  @result = 0
if @message is not null
begin
	insert into rpps_response_details_cdm ( rpps_response_file, trace_number, line_number, biller_id, account_number, net, message)
	values ( @response_file, @trace_number, @line_number, @biller_id, @account_number, @net, @message )
	select @result = scope_identity()
end

return ( @result )
GO
