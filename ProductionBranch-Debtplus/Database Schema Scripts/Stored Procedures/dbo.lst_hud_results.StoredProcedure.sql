USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_hud_results]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_hud_results]  (@interview_type AS int = null) AS

-- =====================================================================================================
-- ==            Return the list of results for a housing appointment.                                ==
-- =====================================================================================================

-- ChangeLog
--   12/24/2002
--     Limited the result set to just the valid items for the interview type
--   5/9/2006
--     Dropped using 'additional' and used the hud_cars_9902 table to map results to interviews
--   1/11/2011
--     Changed messages to housing_VisitOutcomeTypes and hud_cars_9902 to housing_AllowedVisitOutcomeTypes

set nocount on

IF @interview_type IS NULL
	SELECT @interview_type = 0

IF @interview_type <= 0
	SELECT	oID				as 'item_key',
			description,
			ActiveFlag,
			convert(bit,0) as 'Default'
	FROM	housing_VisitOutcomeTypes WITH (NOLOCK)
	ORDER BY 2;
	
else

	select	o.oID			as 'item_key',
			o.description,
			o.ActiveFlag,
			a.[Default]
	FROM	housing_VisitOutcomeTypes o WITH (NOLOCK)
	INNER JOIN housing_AllowedVisitOutcomeTypes a WITH (NOLOCK) ON o.oID = a.Outcome
	WHERE	a.PurposeOfVisit	= @interview_type
	ORDER BY 2;

return ( @@rowcount )
GO
