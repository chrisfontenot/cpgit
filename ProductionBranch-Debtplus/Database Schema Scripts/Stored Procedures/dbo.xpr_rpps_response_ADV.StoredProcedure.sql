USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_ADV]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_ADV] ( @response_file int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @name as varchar(10) = null, @account_number as varchar(40) = null, @net as money = 0, @gross as money = 0, @addendum_code as varchar(2) = '98', @addendum_date as varchar(8) = null, @addendum_routing as varchar(8) = null, @addendum_information as varchar(50) = null ) as

-- =======================================================================================================================
-- ==            Generate the response for a ADV request                                                                ==
-- =======================================================================================================================

-- Determine the transaction type for the request
-- If this is a "RETURN" then make it a payment reject (CIE).
declare @tran_type              varchar(3)
if @addendum_code = '99'
    select  @tran_type = 'CIE'
else
    select  @tran_type = 'ADV'
    
declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, @tran_type, @name, @account_number, @net

-- Update the record with the additional information that is not part of the basic item
if @rpps_response_detail > 0
begin
        -- This is a payment or not based upon the addendum_code. Assume payments if so indicated.
	update	rpps_response_details
	set	gross			= @gross,
	        addendum_code           = @addendum_code,
	        addendum_date           = @addendum_date,
	        addendum_routing        = @addendum_routing,
	        addendum_information    = @addendum_information
	where	rpps_response_detail	= @rpps_response_detail
end

return ( @rpps_response_detail )
GO
