USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_counselor_conversion]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_client_counselor_conversion] as

-- ============================================================================================================
-- ==            Generate the statistics report for the conversion rate of counseling appointments           ==
-- ============================================================================================================

-- Suppress intermediate result sets
set nocount on

declare	@month_start	datetime
declare	@year_start	datetime

select	@month_start	= convert(datetime, convert(varchar,month(getdate())) + '/01/' + convert(varchar,year(getdate())) + ' 00:00:00' ),
	@year_start	= convert(datetime, '01/01/' + convert(varchar,year(getdate())) + ' 00:00:00' )

-- Find the total number of appointments for the month
select	counselor, count(*) as appointments, sum( case status when 'W' then 1 when 'K' then 1 else 0 end ) as kept_appointments
into	#appointments
from	client_appointments with (nolock)
where	start_time >= @month_start
group by counselor;

-- Find the number of converted clients
select	ca.counselor, count(*) as converted_clients
into	#converted
from	client_appointments ca with (nolock)
inner join clients c with (nolock) on ca.client = c.client
where	c.dmp_status_date >= @month_start
and	c.active_status not in ('CRE', 'WKS', 'I')
and	ca.status in ('K', 'W')
and	ca.start_time >= @month_start
group by ca.counselor;

-- Find the total number of appointments for the year
select	ca.counselor, count(*) as appointments, sum( case ca.status when 'W' then 1 when 'K' then 1 else 0 end ) as kept_appointments
into	#ytd_appointments
from	client_appointments ca with (nolock)
where	ca.start_time between @year_start and @month_start
group by ca.counselor;

-- Find the number of converted clients
select	ca.counselor, count(*) as converted_clients
into	#ytd_converted
from	client_appointments ca with (nolock)
inner join clients c with (nolock) on ca.client = c.client
where	ca.start_time between @year_start and @month_start
and	c.active_status not in ('CRE', 'WKS', 'I')
and	ca.status in ('K', 'W')
group by ca.counselor;

-- Merge the information together
select		co.counselor as counselor, dbo.format_normal_name(default,cox.first,default,cox.last,default) as counselor_name,

		isnull(a.appointments,0) as month_appointments, isnull(a.kept_appointments,0) as month_kept, isnull(b.converted_clients,0) as month_converted,

		case
			when isnull(a.kept_appointments,0) <= 0 then convert(float,0)
			else convert(float, isnull(b.converted_clients,0)) / convert(float, a.kept_appointments) * 100.0
		end	as month_percent,

		isnull(a.appointments,0)+isnull(c.appointments,0) as year_appointments, isnull(a.kept_appointments,0)+isnull(c.kept_appointments,0) as year_kept, isnull(b.converted_clients,0)+isnull(d.converted_clients,0) as year_converted,

		case
			when isnull(a.kept_appointments,0)+isnull(c.kept_appointments,0) <= 0 then convert(float,0)
			else convert(float, isnull(b.converted_clients,0)+isnull(d.converted_clients,0)) / convert(float, isnull(a.kept_appointments,0)+isnull(c.kept_appointments,0)) * 100.0
		end	as year_percent

from		counselors co
left outer join names cox with (nolock) on co.NameID = cox.Name
left outer join	#appointments a on co.counselor = a.counselor
left outer join	#converted b on co.counselor = b.counselor
left outer join	#ytd_appointments c on co.counselor = c.counselor
left outer join	#ytd_converted d on co.counselor = d.counselor
where		(co.is_counselor > 0) or (isnull(a.kept_appointments,0) > 0)
order by	6 desc

drop table #appointments
drop table #ytd_appointments
drop table #converted
drop table #ytd_converted

return ( @@rowcount )
GO
