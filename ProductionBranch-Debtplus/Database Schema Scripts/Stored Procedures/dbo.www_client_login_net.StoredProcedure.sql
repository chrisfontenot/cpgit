USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_login_net]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_login_net] ( @client as int, @password as varchar(80)) as
-- =============================================================================================
-- ==            Perform the client logon function                                            ==
-- =============================================================================================

-- ChangeLog
--    1/22/07
--       Added email address to the result set.

-- Disable intermediate results
set nocount on

select	@password = lower(ltrim(rtrim(@password)))
if @password = ''
	select @password = null
	
-- Parameters for the lockout items
declare	@maxFailedPasswordAttemptCount	int
declare	@PasswordAttemptWindow			int

-- lockout = 4 attempts in a 10 minute window.
select	@maxFailedPasswordAttemptCount	= 4,
		@PasswordAttemptWindow			= 10

declare	@pkid								uniqueidentifier
declare	@isLockedOut						int
declare	@isApproved							int
declare	@FailedPasswordAttemptCount			int
declare	@FailedPasswordAttemptWindowStart	datetime
declare	@LastLoginDate						datetime
declare	@LastLockoutDate					datetime
declare	@stored_password					varchar(256)

select	@pkid								= [pkid],
		@stored_password					= [password],
		@isLockedOut						= [isLockedOut],
		@isApproved							= [isApproved],
		@LastLoginDate						= [LastLoginDate],
		@LastLockoutDate					= [LastLockoutDate],
		@FailedPasswordAttemptCount			= [FailedPasswordAttemptCount],
		@FailedPasswordAttemptWindowStart	= [FailedPasswordAttemptWindowStart]
from	client_www
where	[username]							= convert(varchar, @client)
and		[applicationname]					= '/'

-- Check the existance of the record
if @pkid is null
begin
	RaisError ('The client ID is not valid', 16, 1)
	return ( 0 )
end

-- Determine if the password matches the original value
declare	@PasswordMatch		bit
select	@PasswordMatch	 =	1

-- NULL matches a NULL
if @password is null and @stored_password is null
	select	@PasswordMatch	 = 1
	
-- NULL on one never matches a NULL on the other
else if @password is null or @stored_password is null
	select	@PasswordMatch	= 0
	
-- Otherwise, the non-null items must match
else if @password <> @stored_password
	select	@PasswordMatch	= 0
	
-- If the password matches but the user is within the window, then the match status is cleared.
if @passwordMatch = 1
begin
	if @isLockedOut <> 0
	begin
		if dateadd(minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart) > getutcdate()
			select	@PasswordMatch	= 0
	end
end

-- If the password does not match, count the failure attempts and reset the window period.
if @PasswordMatch = 0
begin
	select	@FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
			
	if @FailedPasswordAttemptCount >= @maxFailedPasswordAttemptCount
		select	@isLockedOut				= 1,
				@FailedPasswordAttemptCount = @maxFailedPasswordAttemptCount,
				@LastLockoutDate			= getutcdate()
end

-- If the user password passed, then reset the values
if @PasswordMatch	 = 1
begin
	select	@FailedPasswordAttemptCount			= 0,
			@isLockedOut						= 0,
			@LastLoginDate						= getutcdate()
end

-- Update the database for the attempt
update	client_www
set		[isLockedOut]						= @isLockedOut,
		[FailedPasswordAttemptCount]		= @FailedPasswordAttemptCount,
		[LastLoginDate]						= @LastLoginDate,
		[LastLockoutDate]					= @LastLockoutDate,
		[FailedPasswordAttemptWindowStart]	= getutcdate(),
		[LastActivityDate]					= getutcdate()
where	[PKID]								= @pkid;

-- Reject the login if not sucessful
if @PasswordMatch	 = 0
begin
	RaisError ('The client ID and password combination does not match our records', 16, 1)
	return ( 0 )
end

-- Determine the parameters for the logon once the information is correct
select	dbo.format_normal_name(default,pn.first,default,pn.last,pn.suffix)		as name,
		isnull(dbo.format_address_line_1(a.house,a.direction,a.street,a.suffix,a.modifier,a.modifier_value),'')							as address1,
		isnull(a.address_line_2,'')							as address2,
		dbo.format_city_state_zip (a.city, a.state, a.postalcode)		as address3,
		e.address								as email,

		case c.active_status
			when 'CRE' then 'N'
			when 'APT' then 'N'
			when 'WKS' then 'N'
			when 'PND' then 'N'
			when 'RDY' then 'N'
			when 'PRO' then 'N'
			when 'A'   then 'Y'
			when 'AR'  then 'Y'
			when 'EX'  then 'Y'
			when 'I'   then case
							when drop_date is null   then 'N'
							when drop_date < dateadd(m, -6, getdate()) then 'N'
							else 'Y'
						end
			else		'N'
		end								as active_status

from		people p	with (nolock)
inner join	clients c	with (nolock) on p.client = c.client
left outer join names pn on p.nameid = pn.name
left outer join addresses a on c.addressid = a.address
left outer join emailaddresses e on p.emailid = e.email
where		p.client = @client
and			p.relation = 1

if @@rowcount < 1
begin
	RaisError ('The client ID is not valid', 16, 1)
	return ( 0 )
end

return ( 1 )
GO
