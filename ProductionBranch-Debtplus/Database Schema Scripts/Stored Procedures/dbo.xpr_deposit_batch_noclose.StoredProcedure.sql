USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_noclose]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[xpr_deposit_batch_noclose] ( @deposit_batch_id int ) as
-- =======================================================================================================
-- ==      Dummy procedure to simulate the closing of the batch without actually doing it.              ==
-- =======================================================================================================
return ( 0 )
GO
