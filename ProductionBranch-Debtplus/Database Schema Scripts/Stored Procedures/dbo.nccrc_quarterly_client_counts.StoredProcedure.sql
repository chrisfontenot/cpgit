USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nccrc_quarterly_client_counts]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nccrc_quarterly_client_counts] ( @period_start as datetime = null, @period_end as datetime = null ) as

-- Create the result table
create table #results (sheet varchar(80), location varchar(80), value varchar(80));
create unique index ix1_results on #results ( location );

-- Find the average client deposit
declare	@avg_deposit	float
select	d.client, sum(d.deposit_amount) as deposit_amount
into	#deposits
from	client_deposits d	with (nolock)
inner join clients c		WITH (NOLOCK) on d.client = c.client where c.date_created >= @period_start or c.drop_date <= @period_end
group by d.client;

select @avg_deposit = round(avg(convert(float, deposit_amount)),2)
from	#deposits;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B261' as location, @avg_deposit

-- Average number of creditors on the plan
select	c.client, convert(float,count(cc.client_creditor)) as cnt
into	#creditor_count
from	client_creditor cc		with (nolock)
inner join clients c			WITH (NOLOCK) on cc.client = c.client and (c.date_created >= @period_start or c.drop_date <= @period_end)
inner join client_creditor_balances b	with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
left outer join creditors cr		with (nolock) on cc.creditor = cr.creditor
left outer join creditor_classes ccl	with (nolock) on cr.creditor_class = ccl.creditor_class
where	cc.reassigned_debt = 0
and	isnull(ccl.agency_account,0) = 0
group by c.client;

declare	@avg_creditors	float
select	@avg_creditors = round(avg(cnt),0)
from	#creditor_count

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B260' as location, @avg_creditors

-- Average program months on closed clients
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B263' as location, round(avg(convert(float,program_months)),0) as value
from	clients c with (nolock)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @period_start and @period_end
and	isnull(d.nfcc,'') in ('SC1', 'SC2');

-- Number of clients for the drop reasons
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B252' as location, count(*)
from	clients c with (nolock)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @period_start and @period_end
and	d.rpps_code = 'PF';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B253' as location, count(*)
from	clients c with (nolock)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @period_start and @period_end
and	d.rpps_code = 'CP';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B255' as location, count(*)
from	clients c with (nolock)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @period_start and @period_end
and	d.rpps_code = 'CH';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B256' as location, count(*)
from	clients c with (nolock)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @period_start and @period_end
and	d.rpps_code = 'MP';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B257' as location, count(*)
from	clients c with (nolock)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @period_start and @period_end
and	d.rpps_code = 'BK';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B258' as location, count(*)
from	clients c with (nolock)
left outer join drop_reasons d on c.drop_reason = d.drop_reason
where	c.active_status = 'I'
and	c.drop_date between @period_start and @period_end
and	isnull(d.rpps_code,'') not in ('BK','MP','CH','CP','PF');

-- Find the number of DMPS at the end of the last quarter
declare	@dmp_count	int

select	@dmp_count	= clients_a
from	[statistics] with (nolock)
where	period_stop	= convert(datetime, convert(varchar(10), @period_end, 101) + ' 00:00:00')

if @dmp_count is null
	select	@dmp_count	= count(*)
	from	clients with (nolock)
	where	dmp_status_date <= @period_end
	and	(((drop_date > @period_end) and (active_status in ('I','EX')))
	or	active_status in ('A','AR'))

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B249' as location, @dmp_count

-- Find the number of DMPs established within the last quarter
declare	@established_count	int

select	@established_count = count(*)
from	clients with (nolock)
where	dmp_status_date between @period_start and @period_end;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B246' as location, @established_count

-- Return the results
select * from #results;
drop table #results
drop table #deposits
drop table #creditor_count
RETURN ( 1 )
GO
