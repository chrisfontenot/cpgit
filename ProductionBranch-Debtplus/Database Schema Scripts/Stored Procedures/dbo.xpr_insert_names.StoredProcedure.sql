USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_names]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_names] ( @prefix as varchar(50) = null, @first as varchar(50) = null, @middle as varchar(50) = null, @last as varchar(50) = null, @suffix as varchar(50) = null) as

    insert into names ([prefix], [first], [middle], [last], [suffix]) values (@prefix, @first, @middle, @last, @suffix)
    return (scope_identity())
GO
