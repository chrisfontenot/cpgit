USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[pfs_GetClients]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pfs_GetClients]
	@last_name VARCHAR(50) ='',
	@email_address VARCHAR(50) ='',
	@telephone_home VARCHAR(50) ='',
	@client_id int,
	@ssn VARCHAR(50) =''
As

Select c.client as client_id, 
		pn.[last] as last_name,
		pn.[first] as first_name,
		dbo.format_TelephoneNumber(c.HomeTelephoneID) as telephone_home,
		p.ssn,
		p.birthdate as birth_date,
		pe.address as email
From people p with (nolock)
inner join Clients c with (nolock) on p.Client = c.Client
left outer join EmailAddresses pe with (nolock) on p.EmailID = pe.Email
left outer join Names pn with (nolock) on p.NameID = pn.Name
left outer join TelephoneNumbers t with (nolock) on c.HomeTelephoneID = t.TelephoneNumber
Where pn.[last] like @last_name + '%'
and   pe.email like @email_address + '%'
and   isnull(t.Acode,'')+isnull(t.Number,'') = @telephone_home
and   (p.client = @client_id or @client_id = -1)
and   (p.ssn = @ssn or @ssn = '')
GO
