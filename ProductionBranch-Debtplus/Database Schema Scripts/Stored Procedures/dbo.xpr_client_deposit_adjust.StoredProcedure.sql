USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_deposit_adjust]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_client_deposit_adjust] ( @client as int, @new_amount as money ) AS

-- ====================================================================================================
-- ==            Prorate the new amount for a deposit amoung the various deposit dates               ==
-- ====================================================================================================

-- Start processing
set nocount on
begin transaction

-- If there are no deposits then create one
declare	@target_amount			money
declare	@old_deposited_amount	money
declare	@total_deposits			money
declare	@deposits_count			int
declare	@client_deposit			int

select	@old_deposited_amount	= sum(deposit_amount),
		@deposits_count			= count(*)
from	client_deposits
where	client = @client

if isnull(@deposits_count,0) = 0
	select @deposits_count = 0, @old_deposited_amount = 0

if @deposits_count < 1
begin
	declare	@deposit_date	datetime
	select	@deposit_date	= dateadd(month, 1, getdate())
	select	@deposit_date	= convert(datetime, convert(varchar, month(@deposit_date)) + '/01/' + convert(varchar, year(@deposit_date)) + ' 00:00:00')

	insert into client_deposits (client, deposit_date, deposit_amount)
	values	(@client, @deposit_date, @new_amount)

	-- Generate a system note when the deposit amount changes
	if @new_amount > 0
		insert into client_notes (type, client, client_creditor, is_text, dont_edit, dont_delete, dont_print, subject, note)
		values 	(3, @client, null, 1, 1, 1, 0, 'Client Deposit Amount Changed', 'The monthly deposit amount was changed from $0.00 to $' + convert(varchar, @new_amount, 1))

	commit transaction
	return ( 1 )
end

if @deposits_count = 1 or @old_deposited_amount = 0

	-- Set the single deposit item, which may be $0.00, to the proper value
	update	client_deposits
	set		deposit_amount = @new_amount
	where	client_deposit = (select top 1 client_deposit from client_deposits where client = @client)

else

	-- Calculate the associated amount for each of the deposits
	update	client_deposits
	set		deposit_amount = convert(decimal(10,2), convert(float, (deposit_amount / @old_deposited_amount)) * @new_amount)
	where	client = @client

-- Recompute the total amount on deposit
declare	@new_deposit_amount	money
select	@new_deposit_amount = sum(deposit_amount)
from	client_deposits
where	client = @client

-- Calculate the adjustment as needed
declare	@difference	money
select	@difference = @new_amount - @new_deposit_amount
if @difference <> 0
begin
	if @difference < 0
		select	@target_amount = max(deposit_amount)
		from	client_deposits
		where	client = @client
	else
		select	@target_amount = min(deposit_amount)
		from	client_deposits
		where	client = @client

	-- Find the first record that matches the target value
	select	@client_deposit = client_deposit
	from	client_deposits
	where	client = @client
	and	deposit_amount = @target_amount

	-- Adjust it to correct for any small differences
	update	client_deposits
	set		deposit_amount = deposit_amount + @difference
	where	client_deposit = @client_deposit
end

-- Generate a system note when the deposit amount changes
if @new_amount <> @old_deposited_amount
	insert into client_notes (type, client, client_creditor, is_text, dont_edit, dont_delete, dont_print, subject, note)
	values 	(3, @client, null, 1, 1, 1, 0, 'Client Deposit Amount Changed', 'The monthly deposit amount was changed from $' + convert(varchar, @old_deposited_amount, 1) + ' to $' + convert(varchar, @new_amount, 1))

-- Terminate
commit transaction
return ( 0 )
GO
