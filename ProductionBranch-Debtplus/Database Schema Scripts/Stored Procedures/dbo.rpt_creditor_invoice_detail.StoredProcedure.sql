USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_creditor_invoice_detail]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_creditor_invoice_detail] (@creditor as typ_creditor) AS
-- ================================================================
-- ==         Retrieve the Invoice Detail Information            ==
-- ================================================================

SELECT
	@creditor + '-' + right('00000000'+convert(varchar,r.invoice_register),8) as 'invoice',

	r.inv_date    as 'inv_date',
	r.inv_amount  as 'inv_amt',

	r.pmt_date    as 'pmt_date',
	r.pmt_amount  as 'pmt_amt',

	r.adj_date    as 'adj_date',
	r.adj_amount  as 'adj_amt'

FROM	registers_invoices r
WHERE	r.creditor = @Creditor
AND	(isnull(r.inv_amount,0) > (isnull(r.pmt_amount,0) + isnull(r.adj_amount,0)))

ORDER BY	r.inv_date, r.invoice_register

RETURN ( @@rowcount )
GO
