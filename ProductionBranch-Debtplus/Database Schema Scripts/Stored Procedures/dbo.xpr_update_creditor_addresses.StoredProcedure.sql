USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_creditor_addresses]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_creditor_addresses] ( @oID as int, @attn as varchar(50) = null, @AddressID as int ) as
    -- ==========================================================================================================
    -- ==         Update a creditor address item for a creditor                                                ==
    -- ==========================================================================================================
    update	creditor_addresses
    set		[attn]				= @attn,
			[AddressID]			= @AddressID
	where	[oID]				= @oID
	return ( @@rowcount )
GO
