USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_biller_search]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_rpps_biller_search] ( @biller_name as varchar(80) ) AS

-- ===============================================================================================
-- ==               Return a list of the matching biller IDs for this name                      ==
-- ===============================================================================================

-- ChageLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

SET NOCOUNT ON
SELECT	i.rpps_biller_id	AS rpps_biller_id,
	a.[name]	as 'name'
FROM	rpps_akas a
INNER JOIN rpps_biller_ids i on a.rpps_biller_id = i.rpps_biller_id
where	a.[name] like '%' + ltrim(rtrim(@biller_name)) + '%'
and	i.biller_type in (1,2,3,11,12,13)

ORDER BY 1, 2

RETURN ( @@rowcount )
GO
