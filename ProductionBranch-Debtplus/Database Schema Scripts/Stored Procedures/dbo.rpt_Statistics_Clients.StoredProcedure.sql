USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Statistics_Clients]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Statistics_Clients] ( @CutoffDate as DateTime = NULL ) AS

-- ===================================================================================
-- ==             Fetch statistics for the number of clients                        ==
-- ===================================================================================

IF @CutoffDate IS NULL
	SET @CutoffDate = getdate()

SELECT @CutoffDate = convert(datetime, convert(varchar(10), @cutoffdate, 101) + ' 00:00:00')

-- The range is 13 months earlier
DECLARE @FromDate 	smalldatetime
SET @FromDate = dateadd ( m, -13, @CutoffDate )

-- Fetch the client statistics in this range
SELECT		period_start	 	as 'Month',
		clients_cre		as 'Created',
		clients_wks		as 'Workshop Only',
		clients_apt		as 'Appointment',
		clients_pnd		as 'Pending Information',
		clients_rdy		as 'Ready',
		clients_pro		as 'Proposal',
		clients_a		as 'Active',
		clients_ex		as 'Exit',
		clients_i		as 'Inactive',
		clients_other		as 'All Other Types'
FROM		[statistics]
WHERE		period_start BETWEEN @FromDate AND @CutoffDate
ORDER BY	period_start desc

RETURN ( @@rowcount )
GO
