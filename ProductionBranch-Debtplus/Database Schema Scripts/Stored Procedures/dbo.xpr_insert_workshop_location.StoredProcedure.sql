USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_workshop_location]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_workshop_location] (@name varchar(80), @organization varchar(80) = null, @type int = null, @AddressID int = null, @TelephoneID int = null, @Directions varchar(1024) = null, @Milage float = null, @ActiveFlag as bit = 1, @hcs_id as int = null) as
insert into workshop_locations([AddressID], [directions], [milage], [name], [organization], [TelephoneID], [type], [ActiveFlag], [hcs_id]) values (@AddressID, @directions, @milage, @name, @organization, @TelephoneID, @type, isnull(@ActiveFlag,1), @hcs_id)
return ( scope_identity() )
GO
