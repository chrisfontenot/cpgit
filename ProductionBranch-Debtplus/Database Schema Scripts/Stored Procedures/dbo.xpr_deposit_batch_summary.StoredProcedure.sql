USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_summary]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_summary] (@deposit_batch_id as INT) AS
-- ========================================================================
-- ==          Fetch the summary values for the Client Deposits          ==
-- ========================================================================

SELECT	sum(amount)	as 'amount',
	count(*)	as 'count'
FROM	deposit_batch_details WITH (NOLOCK)
WHERE	deposit_batch_id = @deposit_batch_id

RETURN ( @@rowcount )
GO
