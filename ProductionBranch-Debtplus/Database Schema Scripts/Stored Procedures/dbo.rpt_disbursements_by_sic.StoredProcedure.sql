USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_disbursements_by_sic]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_disbursements_by_sic] ( @FromDate as datetime = null, @ToDate as datetime = null ) as
-- =========================================================================================================
-- ==            Generate a report showing the information needed for the annual report section 4         ==
-- =========================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Disable intermediate results
set nocount on

if @ToDate is null
	set @toDate = getdate()

if @FromDate is null
	set @FromDate = @ToDate

select	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59')

-- Build the list of clients and creditor codes
select distinct cc.client, cc.creditor
into	#client_creditor
from	client_creditor cc
where	cc.client > 0;

-- Generate the disbursement information for the creditors in the list
select		rcr.creditor, rcr.client, rcr.client_creditor, sum(isnull(debit_amt,0)) as 'debit_amt', sum(isnull(credit_amt,0)) as 'credit_amt'
into		#nccrc_disbursements_1
from		registers_client_creditor rcr
where		rcr.tran_type in ('AD', 'BW', 'MD', 'CM', 'RF')
and		rcr.date_created between @FromDate and @ToDate
and		isnull(rcr.void,0) = 0
group by	rcr.client, rcr.creditor, rcr.client_creditor;

-- Calculate the disbursements by individual creditor
select		creditor, sum(isnull(debit_amt,0)) as debit_amt, sum(isnull(credit_amt,0)) as credit_amt
into		#nccrc_disbursements_2
from		#nccrc_disbursements_1
group by	creditor;

-- Determine the number of distinct clients by the line number
select		creditor, count(*) as client_count
into		#nccrc_clients
from		#client_creditor
group by	creditor;

-- Find the balance information
select		cc.creditor, sum(isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) as 'debt'
into		#debt_balance
from		client_creditor cc
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
where		cc.client > 0
AND		cc.reassigned_debt = 0
group by	creditor;

-- Produce the report, order by disbursement descending
select		a.sic as 'creditor', isnull(a.creditor+' ','')+isnull(a.creditor_name,'') as creditor_name, isnull(c.debit_amt,0) - isnull(c.credit_amt,0) as disbursement, b.client_count as 'active_count', d.debt as 'debt'
from		creditors a
inner join	#nccrc_clients b on b.creditor = a.creditor
inner join	#nccrc_disbursements_2 c on c.creditor = a.creditor
inner join	#debt_balance d on d.creditor = a.creditor
order by 3 desc;

-- Discard working tables
drop table	#client_creditor
drop table	#nccrc_clients
drop table	#nccrc_disbursements_2
drop table	#nccrc_disbursements_1
drop table	#debt_balance

return ( @@rowcount )
GO
