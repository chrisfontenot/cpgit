USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Transactions_CC_header]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Transactions_CC_header] ( @client_creditor as int ) AS

-- =======================================================================================================================
-- ==            Header information for the client creditor transactions report                                         ==
-- =======================================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Retrieve the information from the view
select	cc.client,
	cc.creditor,
	cc.client_creditor,
	coalesce(cc.message, cc.account_number, '') as account_number,
	coalesce(cr.creditor_name, cc.creditor_name, '') as creditor_name

from	client_creditor cc with (nolock)
left outer join	creditors cr with (nolock) on cc.creditor = cr.creditor

where	cc.client_creditor	= @client_creditor

return ( @@rowcount )
GO
