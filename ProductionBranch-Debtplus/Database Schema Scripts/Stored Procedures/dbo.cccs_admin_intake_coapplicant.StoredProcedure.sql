USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_coapplicant]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_coapplicant] ( @application_id as int, @client as int ) as

-- Find the source row to insert
declare	@associate_id		int
select	@associate_id = associate_id
from	cccs_admin..applicant_associates
where	application_id = @application_id;

-- If there is a row then insert it
if @associate_id is not null
begin
	declare	@relation	int
	select	@relation	= dbo.cccs_admin_map_relation ( associate_type_id )
	from	cccs_admin..applicant_associates
	where	application_id	= @application_id

	-- Create the co-applicant record if needed
	declare	@person		int
	select	@person	= person
	from	people
	where	client		= @client
	and		relation	<> 1;

	if @person is null
		execute @person = xpr_insert_people @client, @relation

	declare	@coapplicant_employer			varchar(80)
	declare	@coapplicant_job_title			varchar(80)
	declare	@coapplicant_monthly_income_gross	money
	declare	@coapplicant_monthly_income_net		money
	declare @coapplicant_ssn		varchar(10)

	select	@coapplicant_employer				= coapplicant_employer,
			@coapplicant_job_title				= dbo.cccs_admin_address ( coapplicant_job_title ),
			@coapplicant_monthly_income_gross	= coapplicant_monthly_income_gross,
			@coapplicant_monthly_income_net		= coapplicant_monthly_income_net
	from	cccs_admin..applications
	where	application_id	= @application_id

	declare	@prefix					varchar(50)
	declare	@first					varchar(50)
	declare	@middle					varchar(50)
	declare	@last					varchar(80)
	declare	@suffix					varchar(50)
	declare	@email					varchar(256)
	declare	@gross_income			money
	declare	@net_income				money
	declare	@work_ph				varchar(80)
	declare	@work_ext				varchar(80)
	declare	@birthdate				datetime
	declare	@other_job				varchar(80)
	declare	@gender					int
	declare	@ssn					varchar(80)
	declare @ethnicity				int
	declare @race					int
	declare @education				int

	-- Obtain the input values (we can't merge a table into an update....)	
	select	@prefix						= UPPER(dbo.cccs_admin_address ( salutation )),
			@first						= UPPER(dbo.cccs_admin_address ( first_name )),
			@middle						= UPPER(case len(ltrim(rtrim(middle_initial)))
												when 0 then null
												when 1 then ltrim(rtrim(middle_initial)) + '.'
												else ltrim(rtrim(middle_initial))
										  end),
										  
			@last						= UPPER(dbo.cccs_admin_address ( last_name )),
			@suffix						= UPPER(dbo.cccs_admin_address ( suffix )),
			@email						= email_address,
			@gross_income				= isnull(@coapplicant_monthly_income_gross,0),
			@net_income					= isnull(@coapplicant_monthly_income_net,0),
			@work_ph					= isnull(dbo.numbers_only (telephone_work),''),
			@work_ext					= isnull(dbo.numbers_only (telephone_work),''),
			@other_job					= @coapplicant_job_title,
			@birthdate					= birth_date,
			@gender						= dbo.cccs_admin_gender (salutation),
			@ssn						= dbo.cccs_admin_ssn ( ssn ),
			@ethnicity					= dbo.cccs_admin_map_ethnicity ( ethnicity_id ),
			@race						= dbo.cccs_admin_map_race ( ethnicity_id ),
			@education					= dbo.cccs_admin_map_education ( education_id )
	from	cccs_admin..applicant_associates
	where	application_id	= @application_id

	-- Update the name
	declare	@NameID			int
	select	@NameID			= NameID
	from	people
	where	person			= @person;

	if @NameID is null
	begin
		execute @NameID = xpr_insert_names @prefix, @first, @middle, @last, @suffix

		update	people
		set		NameID = @NameID
		where	person = @Person;
	end

	update	names
	set		[prefix]	= UPPER(@prefix),
			[first]		= UPPER(@first),
			[middle]	= UPPER(@middle),
			[last]		= UPPER(@last),
			[suffix]	= UPPER(@suffix)
	where	[name]		= @NameID;

	-- Update the email address
	declare	@EmailID			int
	select	@EmailID			= NameID
	from	people
	where	person			= @person;

	if @EmailID is null
	begin
		execute @EmailID = xpr_insert_emailaddress @email

		update	people
		set		EmailID = @EmailID
		where	person = @Person;
	end

	update	EmailAddresses
	set		[address]	= @email
	where	[Email]		= @EmailID;

	-- Split the telephone number
	declare	@country		int
	declare	@acode			varchar(10)
	declare	@number			varchar(50)
	declare	@ext			varchar(10)

	-- Find the area code
	if len(@work_ph) > 7
		select	@acode		= left(@work_ph, 3),
				@number		= substring(@work_ph, 4, 80)

	-- Find the default country
	if @country is null
		select	@country = min(country)
		from	countries
		where	[default]		= 1
		and		[activeflag]	= 1

	if @country is null
		select	@country = min(country)
		from	countries
		where	[default]	= 1

	if @country is null
		select	@country = min(country)
		from	countries

	if @country is null
		select	@country = 1

	-- Update the telephone number
	declare	@WorkTelephoneID	int
	select	@WorkTelephoneID	= WorkTelephoneID
	from	people
	where	person				= @person;

	if @WorkTelephoneID is null
	begin
		execute @WorkTelephoneID = xpr_insert_telephonenumbers @country, @acode, @work_ph, @ext

		update	people
		set		WorkTelephoneID = @WorkTelephoneID
		where	person = @Person;
	end

	update TelephoneNumbers
	set		[country]			= @country,
			[acode]				= @acode,
			[number]			= @work_ph,
			[ext]				= @ext
	where	[TelephoneNumber]	= @WorkTelephoneID;

	-- Update the information from the input file
	update	people
	set		[relation]		= @relation,
			[gross_income]	= @gross_income,
			[net_income]	= @net_income,
			[other_job]		= @other_job,
			[birthdate]		= @birthdate,
			[gender]		= @gender,
			[ssn]			= @ssn,
			[ethnicity]		= @ethnicity,
			[race]			= @race,
			[education]		= @education
	where	client			= @client
	and		relation		= @relation;
end
return ( 1 )
GO
