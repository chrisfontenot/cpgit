USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_housing_budget]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_housing_budget](@ForeclosureCase int, @BudgetSubcategoryID int, @BudgetItemAmt decimal(18, 2), @BudgetNote varchar(100), @oID INT = NULL OUTPUT) as

	-- ==============================================================================================================
	-- ==         Add a row to the housing lender table for outstanding loans                                      == 
	-- ==============================================================================================================
	if @oID is null
	begin
		insert into hpf_budgetItemDTO([ForeclosureCase], [BudgetSubcategoryID], [BudgetItemAmt], [BudgetNote]) VALUES (@ForeclosureCase, @BudgetSubcategoryID, @BudgetItemAmt, @BudgetNote)
		select @oID = SCOPE_IDENTITY()
	end else
		update hpf_budgetItemDTO SET BudgetItemAmt = @BudgetItemAmt, BudgetNote = @BudgetNote WHERE oID = @oID
	
	return ( @oID )
GO
