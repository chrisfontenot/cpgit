USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Notes]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Notes] ( @Criteria AS varchar(8000) = NULL ) AS

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

DECLARE @stmt	varchar(8000)

-- This is the statement which forms the base for the selections
SELECT @stmt = 'SELECT n.note_id,n.client,dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) AS client_name,' +
	'isnull(cc.creditor,'''') AS creditor,cc.account_number,COALESCE(cr.creditor_name,cc.creditor_name,'''') AS creditor_name,n.subject,CASE n.type WHEN 1 ' +
	'THEN ''Permanent'' WHEN 2 THEN ''Temporary'' WHEN 3 THEN ''System'' WHEN 4 THEN ''Alert'' WHEN 5 THEN ''Disbursement'' WHEN 6 THEN ''Research'' END AS type,n.created_by,n.date_created,' +
	'CONVERT(int,CASE n.is_text WHEN 0 THEN CASE WHEN lower(CONVERT(varchar(7),n.note)) LIKE ''{\rtf[0-9]\'' THEN 1 ELSE 0 END ELSE 0 END) AS text_type, n.note, CASE n.dont_print WHEN 0 THEN '''' ELSE ''DON''''T PRINT'' END AS dont_print,' +
    	'CASE n.dont_edit WHEN 0 THEN '''' ELSE ''DON''''T EDIT'' END AS dont_edit,CASE n.dont_delete WHEN 0 THEN '''' ELSE ''DON''''T DELETE'' END AS dont_delete ' +
	'FROM client_notes n ' +
	'LEFT OUTER JOIN client_creditor cc ON n.client_creditor = cc.client_creditor ' +
	'LEFT OUTER JOIN creditors cr ON cc.creditor = cr.creditor ' +
	'LEFT OUTER JOIN people p ON n.client = p.client AND 1 = p.relation ' +
	'LEFT OUTER JOIN names pn ON p.nameid = pn.name ' +
	'WHERE n.dont_print = 0 OR ((n.created_by = suser_sname()) OR suser_sname() = ''sa'')'

exec ( @stmt )
GO
