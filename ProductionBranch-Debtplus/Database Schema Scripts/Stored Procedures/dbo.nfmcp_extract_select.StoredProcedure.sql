IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'nfmcp_extract_select')
	EXEC ('CREATE PROCEDURE [dbo].[nfmcp_extract_select]  (@from_date AS DATETIME = NULL, @to_date AS DATETIME = NULL ) AS')
GO
ALTER PROCEDURE [dbo].[nfmcp_extract_select]  (@from_date AS DATETIME = NULL, @to_date AS DATETIME = NULL ) AS

-- ChangeLog
--  5/14/08
--    Used codes in the NFCC column of the 'Financial Problems' table of LOI (Loss of Income), IEX (Increase in Expense),
--	  BVF (Business Venture Failed), and ILP (Increase in Loan Payment).
--    Added test to ensure that resulted clients are for the proper hud_grant type
--  6/4/08 
--	  modified session length to decimal format
--  6/19/08
--    Corrected loan status
--  7/9/08
--    Added hud_interview ID to the selection table
--  8/4/08
--    Ensured that only priority 1 loans were processed
--  8/5/08
--    Changed date format to MM/DD/YYYY
--	03/06/12
--	  Added field for most recent HUD purpose of visit
--	03/08/12
--	  Added field for MSA
--	9/04/13
--	  Expanded width of text fields

-- Suppress intermediate results
SET nocount ON

-- Default the values
if @to_date IS NULL
	SELECT	@to_date = getdate()

if @from_date IS NULL
	SELECT	@from_date = @to_date

-- Convert the dates to suitable form
SELECT	@from_date = convert(VARCHAR(10), @from_date, 101),
	@to_date = convert(VARCHAR(10), @to_date, 101) + ' 23:59:59'

-- Delete the partial table from the previous execution
EXEC ( 'if exists (SELECT * FROM tempdb..sysobjects WHERE name = ''##tmp_nfcc_nfmc_extract'' AND type = ''U'') DROP TABLE ##tmp_nfcc_nfmc_extract' )

-- Find the grant type
DECLARE	@hud_grant_type		INT
SELECT	@hud_grant_type	= oID
FROM	Housing_GrantTypes
WHERE	description = 'Pending Housing'
-- WHERE description like 'Cal HFA%'
-- WHERE description like 'zNFMCP Round 2-DO NOT USE'
-- WHERE description = 'WA State 2012-2013'
-- WHERE description = 'none'
-- WHERE description = 'Oregon Pre-Mediation 2012'

-- If there is no grant for this type then just ignore the results.
IF @hud_grant_type IS NULL
	RETURN ( 0 );

-- Create the selection table for the clients to be reported
CREATE TABLE ##tmp_nfcc_nfmc_extract (
             sequenceID                                    INT identity(1,1),
             line                                          INT NULL,
             OrgID                                         VARCHAR(256) NULL,
             BranchName                                    VARCHAR(256) NULL,
             ClientID                                      INT NOT NULL,
             CounselingLevel                               VARCHAR(256) NULL,
             CounselingIntakeDate                          DATETIME NULL,
             CounselingMode                                VARCHAR(256) NULL,
             FirstName                                     VARCHAR(256) NULL,
             LastName                                      VARCHAR(256) NULL,
             Age                                           VARCHAR(256) NULL,
             Race                                          VARCHAR(256) NULL,
             Ethnicity                                     VARCHAR(256) NULL,
             Gender                                        VARCHAR(256) NULL,
             HouseholdType                                 VARCHAR(256) NULL,
             HouseholdIncome                               VARCHAR(256) NULL,
             IncomeCategory                                VARCHAR(256) NULL,
             HouseNo                                       VARCHAR(256) NULL,
             Street                                        VARCHAR(256) NULL,
             City                                          VARCHAR(256) NULL,
             [State]                                       VARCHAR(256) NULL,
             Zip                                           VARCHAR(256) NULL,
             Total_Individual_foreclosure_hours_received   VARCHAR(256) NULL,
             Total_group_foreclosure_hours_received        VARCHAR(256) NULL,
             NameofOriginatingLender                       VARCHAR(512) NULL,
             FDICofOriginalLender                          VARCHAR(512) NULL,
             OriginalLoanNumber                            VARCHAR(512) NULL,
             CurrentLoanServicer                           VARCHAR(512) NULL,
             Current_Servicer_FDIC                         VARCHAR(512) NULL,
             CurrentServicerLoanNo                         VARCHAR(512) NULL,
             CreditScore                                   VARCHAR(256) NULL,
             ScoreType                                     VARCHAR(256) NULL,
             PITIatIntake                                  VARCHAR(256) NULL,
             LoanProductType                               VARCHAR(256) NULL,
             InterestOnly                                  VARCHAR(256) NULL,
             Hybrid                                        VARCHAR(256) NULL,
             OptionARM                                     VARCHAR(256) NULL,
             VAorHFAInsured                                VARCHAR(256) NULL,
             PrivatelyHeld                                 VARCHAR(256) NULL,
             ARMReset                                      VARCHAR(256) NULL,
             DefaultReasonCode                             VARCHAR(256) NULL,
             LoanStatusAtContact                           VARCHAR(256) NULL,
             CounselingOutcomeCode                         VARCHAR(256) NULL,
             CounselingOutcomeDate                         DATETIME NULL,
             indicator_date                                DATETIME NULL,
             inhibit_date                                  DATETIME NULL,
             housing_property                              INT NULL,
             housing_loan                                  INT NULL,
             office                                        INT NULL,
             hud_interview                                 INT NULL,
             WhyNoCreditScore                              VARCHAR(256) NULL,
             FirstOrSecondLoan                             VARCHAR(256) NULL,
             HasSecondLoan                                 VARCHAR(256) NULL,

-- Used only in the text extract version. Values are defined within that procedure.
			 discard_reason                                VARCHAR(80) NULL,

-- ====================== ADDED FIELDS FOR CLEARPOINT =======================================
             PurposeOfVisit                                VARCHAR(256) NULL,
             pov_created_by                                VARCHAR(256) NULL,
             MSA_Name                                      VARCHAR(256) NULL,
             MHA_screening_note                            VARCHAR(256) NULL,
             GLB_verbal_note                               VARCHAR(256) NULL,
             NFMCP_verbal_note                             VARCHAR(256) NULL,
             Level_2_note                                  VARCHAR(256) NULL);

-- Load the client list into the table.
INSERT INTO ##tmp_nfcc_nfmc_extract (clientid)
SELECT	distinct client
FROM	hud_interviews
WHERE	interview_date BETWEEN @from_date AND @to_date
 AND		hud_grant = @hud_grant_type


-- Remove the null values from the table. It is just easier to create the insert list and do this after the fact rather than bind default values to the temporary table.
UPDATE	##tmp_nfcc_nfmc_extract
SET OrgID = '0000',
	CounselingLevel = '',
	CounselingMode = '2',
	FirstName = '',
	LastName = '',
	Age = '',
	Race = 9,
	Ethnicity = 0,
	Gender = 1,
	HouseholdType = 1,
	HouseholdIncome = '',
	IncomeCategory = '',
	HouseNo = '',
	Street = '',
	City = '',
	[State] = '',
	Zip = '',
	Total_Individual_foreclosure_hours_received = 0,
	Total_group_foreclosure_hours_received = 0,
	NameofOriginatingLender = '',
	FDICofOriginalLender = '',
	OriginalLoanNumber = '',
	CurrentLoanServicer = '',
	Current_Servicer_FDIC = '',
	CurrentServicerLoanNo = '',
	CreditScore = '',
	ScoreType = '',
	PITIatIntake = '',
	LoanProductType = 11,
	InterestOnly = 0,
	Hybrid = 0,
	OptionARM = 0,
	VAorHFAInsured = 0,
	PrivatelyHeld = 0,
	ARMReset = 0,
	DefaultReasonCode = 0,
	LoanStatusAtContact = 1,
	CounselingOutcomeCode = '',
	WhyNoCreditScore = '3',
	FirstOrSecondLoan = '2',
	HasSecondLoan = '0',

	-- Clearpoint fields
	PurposeOfVisit = '',
	MSA_Name = '',
	MHA_screening_note = 0,
	GLB_verbal_note = 0,
	NFMCP_verbal_note = 0,
	Level_2_note = 0;

-- Set the hud interview pointer
UPDATE	##tmp_nfcc_nfmc_extract
SET		hud_interview = i.hud_interview
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN hud_interviews i ON x.clientid = i.client AND i.interview_date BETWEEN @from_date AND @to_date AND i.hud_grant = @hud_grant_type;

-- Set the privacy policy indicator date
UPDATE	##tmp_nfcc_nfmc_extract
SET		indicator_date = getdate()
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN client_housing i ON x.clientid = i.client
WHERE	i.nfmcp_privacy_policy = 1;

-- Set the decline authorization indicator
UPDATE	##tmp_nfcc_nfmc_extract
SET		inhibit_date = getdate()
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN client_housing i ON x.clientid = i.client
WHERE	i.nfmcp_decline_authorization = 1;

-- Find the property field
UPDATE	##tmp_nfcc_nfmc_extract
SET		housing_property = p.oID
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN client_housing h ON x.clientid = h.client
INNER JOIN housing_properties p ON h.client = p.HousingID
WHERE	p.UseInReports		= 1;

-- Override the current value with the primary residence item
UPDATE	##tmp_nfcc_nfmc_extract
SET		housing_property = p.oID
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN client_housing h ON x.clientid = h.client
INNER JOIN housing_properties p ON h.client = p.HousingID
WHERE	p.Residency			= 1
AND		x.housing_property IS NULL;

-- Find the loan information. Start with loans that are not reported.
UPDATE	##tmp_nfcc_nfmc_extract
SET		housing_loan		= l.oID,
		FirstOrSecondLoan	= '2'
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN Housing_Loans l ON x.housing_property = l.PropertyID
WHERE	l.UseInReports		= 1;

-- Prefer the first loan
UPDATE	##tmp_nfcc_nfmc_extract
SET		housing_loan		= l.oID,
		FirstOrSecondLoan	= '1'
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN Housing_Loans l ON x.housing_property = l.PropertyID
WHERE	l.UseInReports		= 1
AND		l.Loan1st2nd		= 1;

-- Set the 2nd loan indicator if there is a second loan
UPDATE	##tmp_nfcc_nfmc_extract
SET		HasSecondLoan		= 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN Housing_Loans l ON x.housing_property = l.PropertyID
WHERE	l.Loan1st2nd		<> 1;

-- Fill in the current lender name into both original and current fields.
UPDATE	##tmp_nfcc_nfmc_extract
SET		CurrentLoanServicer		= case when svc.IsOther = 1 then lnd.ServicerName when svc.description is null then '' else svc.description end,
		Current_Servicer_FDIC	= isnull(lnd.FdicNcusNum,''),
		CurrentServicerLoanNo	= lnd.AcctNum,
		FirstOrSecondLoan		= 1
FROM	##tmp_nfcc_nfmc_extract x
LEFT OUTER JOIN housing_loans l ON x.housing_loan = l.oID
LEFT OUTER JOIN housing_lenders lnd ON l.CurrentLenderID = lnd.oID
LEFT OUTER JOIN housing_lender_servicers svc ON lnd.ServicerID = svc.oID

-- Include the original field as well if it is supplied
UPDATE	##tmp_nfcc_nfmc_extract
SET		NameOfOriginatingLender	= case when svc.IsOther = 1 then lnd.ServicerName when svc.description is null then '' else svc.description end,
		OriginalLoanNumber		= lnd.AcctNum,
		FDICofOriginalLender	= isnull(lnd.FdicNcusNum,'')
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_lenders lnd ON l.OrigLenderID = lnd.oID
LEFT OUTER JOIN housing_lender_servicers svc ON lnd.ServicerID = svc.oID
WHERE	l.UseOrigLenderID = 1;

-- Set the flag for the ARM interest rate reset
UPDATE	##tmp_nfcc_nfmc_extract
SET		ARMReset = 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
WHERE	det.ARM_Reset = 1;

-- Level 1 counseling
UPDATE	##tmp_nfcc_nfmc_extract
SET		CounselingLevel = h.nfmcp_level
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN client_housing h ON x.clientid = h.client

-- Fill in the client information where possible
UPDATE ##tmp_nfcc_nfmc_extract
SET		Street	= a.street,
		HouseNo	= a.house,
		City	= a.city,
		[State] = st.mailingcode,
		Zip     = a.postalcode
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON c.client = x.clientid
INNER JOIN addresses a ON c.addressid = a.address
INNER JOIN states st ON a.state = st.state;

-- Fill in the people information
UPDATE ##tmp_nfcc_nfmc_extract
SET		FirstName	= pn.first,
		LastName	= pn.last,
		Age			= datediff(year, p.birthdate, getdate()),
		race		= isnull(rt.nfmcp_section,'9'),
		Ethnicity	= isnull(et.nfmcp_section,'0'),
		gender		= isnull(gt.nfmcp_section,'0')
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN people p ON p.client = x.clientid
LEFT OUTER JOIN names pn ON p.nameid = pn.name
LEFT OUTER JOIN RaceTypes rt ON p.Race = rt.oID
LEFT OUTER JOIN EthnicityTypes et ON p.Ethnicity = et.oID
LEFT OUTER JOIN GenderTypes gt ON p.Gender = gt.oID
WHERE	p.relation = 1;

-- Set the fico score and the agency if there is one
UPDATE	##tmp_nfcc_nfmc_extract
SET		CreditScore		= p.fico_score,
		ScoreType		= CreditAgency
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN people p ON p.client = x.clientid
WHERE	p.relation		= 1
AND		p.fico_score	> 0;

-- Set the reason why there is no score
UPDATE	##tmp_nfcc_nfmc_extract
SET		CreditScore		 = '',
		ScoreType		 = '',
		WhyNoCreditScore = r.nfmcp_section
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN people p ON p.client = x.clientid
LEFT OUTER JOIN housing_FICONotIncludedReasons r ON p.no_fico_score_reason = r.oID
WHERE	p.relation = 1
AND		isnull(p.fico_score,0) = 0;

-- Clear the failure reason if there is a score
UPDATE	##tmp_nfcc_nfmc_extract
SET		WhyNoCreditScore	= NULL
WHERE	CreditScore <> '';

-- Set the hispanic marker if the race is hispanic
UPDATE	##tmp_nfcc_nfmc_extract
SET		ethnicity	= 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN people p ON p.client = x.clientid
WHERE	p.relation	= 1
AND		p.race		= 4; -- old Hispanic race

-- Set the race to white if hispanic
UPDATE	##tmp_nfcc_nfmc_extract
SET		race		= 4  -- nfmcp's caucasian race
WHERE	ethnicity	= 1;

-- Set the household head information
UPDATE	##tmp_nfcc_nfmc_extract
SET		HouseholdType	= t.nfmcp_section
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON x.ClientID = c.client
INNER JOIN HouseHoldHeadTypes t ON c.Household = t.oID

-- Look at the first kept appointment for the client
UPDATE	##tmp_nfcc_nfmc_extract
SET		CounselingMode	= isnull(m.nfmcp_section, '5')
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON x.clientid = c.client
INNER JOIN client_appointments ca ON c.first_kept_appt = ca.client_appointment
INNER JOIN appt_types apt ON ca.appt_type = apt.appt_type
INNER JOIN FirstContactTypes m ON apt.contact_type = m.oID

-- Set the office from the client
UPDATE	##tmp_nfcc_nfmc_extract
SET		office = c.office
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON x.clientid = c.client

-- Update the branch ID from the office field
UPDATE	##tmp_nfcc_nfmc_extract
SET		BranchName = o.nfcc
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN offices o ON x.office = o.office;

-- Household income
SELECT	clientid, dbo.client_income (clientid) AS income, dbo.ami(clientid) AS ami, 'A' AS householdlevel
INTO	#ami
FROM	##tmp_nfcc_nfmc_extract

UPDATE	#ami
SET		householdlevel = 'B'
WHERE	income >= ami;

UPDATE	#ami
SET		householdlevel = 'C'
WHERE	income >= ami * 1.60;

UPDATE	#ami
SET		householdlevel = 'D'
WHERE	income >= ami * 2.00;

UPDATE	#ami
SET		householdlevel = ''
WHERE	ami = 0;

UPDATE	##tmp_nfcc_nfmc_extract
SET		HouseholdIncome	= convert(VARCHAR, convert(decimal(10,2), a.income)),
		IncomeCategory = a.householdlevel
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN #ami a ON x.clientid = a.clientid;

DROP TABLE #ami

-- PITI (loan payment) at intake.
UPDATE	##tmp_nfcc_nfmc_extract
SET		PITIatIntake = convert(VARCHAR, convert(decimal(10,2), det.Payment))
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_properties p ON x.ClientID = p.HousingID
INNER JOIN housing_loans l ON p.oID = l.PropertyID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
WHERE	det.Payment < 10000000;

-- Interest only loan
UPDATE	##tmp_nfcc_nfmc_extract
SET		InterestOnly	= 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
WHERE	det.Interest_only_loan = 1;

-- option ARM
UPDATE	##tmp_nfcc_nfmc_extract
SET		OptionARM	= 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
WHERE	det.Option_ARM_Loan = 1;

-- hybrid ARM
UPDATE	##tmp_nfcc_nfmc_extract
SET		Hybrid	= 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
WHERE	det.Hybrid_ARM_Loan = 1;

-- VA or FHA insured
UPDATE	##tmp_nfcc_nfmc_extract
SET		VAorHFAInsured	= 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
WHERE	det.FHA_VA_Insured_Loan = 1;

-- Privately held is all non-va or fha loans
UPDATE	##tmp_nfcc_nfmc_extract
SET		PrivatelyHeld = 1
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
WHERE	det.Privately_Held_Loan = 1;

-- Set the housing loan type code
UPDATE	##tmp_nfcc_nfmc_extract SET LoanProductType = '11';

UPDATE	##tmp_nfcc_nfmc_extract
SET		LoanProductType = isnull(t.nfmcp_section,'11')
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID
INNER JOIN housing_loan_details det ON l.IntakeDetailID = det.oID
INNER JOIN housing_loantypes t ON det.LoanTypeCD = t.oID

-- DefaultReasonCode
UPDATE	##tmp_nfcc_nfmc_extract SET DefaultReasonCode = '10'

UPDATE	##tmp_nfcc_nfmc_extract
SET		DefaultReasonCode	= isnull(fin.nfmcp_section,'10')
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON x.clientid = c.client
INNER JOIN financial_problems fin ON c.cause_fin_problem1 = fin.financial_problem;

-- LoanStatusAtContact
UPDATE	##tmp_nfcc_nfmc_extract SET LoanStatusAtContact = '1'

UPDATE	##tmp_nfcc_nfmc_extract
set		LoanStatusAtContact = case isnull(l.LoanDelinquencyMonths,0)
								when -1 then 1	-- current but struggling
								when  0 then 1  -- current
								when  1 then 2	-- 30 days
								when  2 then 3	-- 60 days
								when  3 then 4	-- 90 days
										else 5	-- 120+ days
							  end
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN housing_loans l ON x.housing_loan = l.oID

-- Find the total amount of time spent so far on the client case
SELECT	x.clientid,
		SUM(t.minutes) AS minutes
INTO	#hours
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN hud_transactions t ON x.hud_interview = t.hud_interview
group by x.clientid;

UPDATE	##tmp_nfcc_nfmc_extract
SET		Total_Individual_foreclosure_hours_received = round(convert(FLOAT, t.minutes) / 60.0, 3)
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN #hours t ON x.clientid = t.clientid;

DROP TABLE #hours;

-- Find the total amount of time spent so far on the client case
SELECT	x.clientid, SUM(t.duration) AS minutes
INTO	#workshop
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN client_appointments ca ON x.ClientID = ca.client AND ca.start_time BETWEEN @from_date AND @to_date AND ca.workshop IS NOT NULL AND ca.office IS NULL AND ca.status IN ('K','W')
INNER JOIN workshops w ON ca.workshop = w.workshop
INNER JOIN workshop_types t ON w.workshop_type = t.workshop_type
WHERE	t.workshop_type IN ( -- Make sure that we count workshops only once.
	SELECT	wc.workshop_type
	FROM	workshop_contents wc
	INNER JOIN workshop_content_types wct ON wc.content_type = wct.content_type
	WHERE	wct.hud_9902_section = '6.c'
)
group by x.clientid;

UPDATE	##tmp_nfcc_nfmc_extract
SET		Total_group_foreclosure_hours_received = round(convert(FLOAT, t.minutes) / 60.0, 3)
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN #workshop t ON x.clientid = t.clientid;

DROP TABLE #workshop;

-- CounselingIntakeDate
UPDATE	##tmp_nfcc_nfmc_extract
SET		CounselingIntakeDate = i.interview_date
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN hud_interviews i ON x.hud_interview = i.hud_interview

-- Add clients who have results but are not in the list
INSERT INTO ##tmp_nfcc_nfmc_extract (clientid, hud_interview)
SELECT	iv.client, iv.hud_interview
FROM	hud_interviews iv
WHERE	iv.hud_grant = @hud_grant_type
AND		iv.result_date BETWEEN @from_date AND @to_date
AND		iv.client NOT IN (
	SELECT	ClientID
	FROM	##tmp_nfcc_nfmc_extract);

-- CounselingOutcomeDate
UPDATE	##tmp_nfcc_nfmc_extract
SET		CounselingOutcomeDate	= i.result_date
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN hud_interviews i ON x.hud_interview = i.hud_interview;

-- CounselingOutcomeCode
UPDATE	##tmp_nfcc_nfmc_extract
SET		CounselingOutcomeCode	= isnull(h.NFMCP_Section,'20')
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN hud_interviews i ON x.hud_interview = i.hud_interview
INNER JOIN [housing_AllowedVisitOutcomeTypes] h ON h.PurposeOfVisit = i.interview_type AND h.Outcome = i.hud_result;

-- ========================= ADDITIONAL FIELDS FOR CLEARPOINT ==============================

-- Purpose of Visit
UPDATE	##tmp_nfcc_nfmc_extract
SET		PurposeOfVisit = vhpov.description
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN hud_interviews i ON x.hud_interview = i.hud_interview
INNER JOIN housing_PurposeOfVisitTypes vhpov ON i.interview_type = vhpov.oID

-- MSA Name (Take any acceptable name at the start)
/*
UPDATE	##tmp_nfcc_nfmc_extract
SET		MSA_Name = zm.Location
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON x.clientid = c.client
INNER JOIN addresses a ON c.addressid = a.address
LEFT OUTER JOIN ZipcodeSearch zm ON left(isnull(a.postalcode,'')+'00000',5) = zm.ZipCode 
WHERE	ZipType IN ('M','P','S')

-- MSA Name (Use the preferred name if possible)
UPDATE	##tmp_nfcc_nfmc_extract
SET		MSA_Name = zm.Location
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON x.clientid = c.client
INNER JOIN addresses a ON c.addressid = a.address
INNER JOIN ZipcodeSearch zm ON left(isnull(a.postalcode,'')+'00000',5) = zm.ZipCode 
WHERE	ZipType IN ('M','P')
*/

UPDATE	##tmp_nfcc_nfmc_extract
SET		MSA_Name = zm.msa_name
FROM	##tmp_nfcc_nfmc_extract x
INNER JOIN clients c ON x.clientid = c.client
INNER JOIN addresses a ON c.addressid = a.address
INNER JOIN zip_msa zm ON left(isnull(a.postalcode,'')+'00000',5) = zm.zipcode

-- POV created by
update	##tmp_nfcc_nfmc_extract
set		pov_created_by = dbo.format_normal_name(default,n.first,default,n.last,default)
from	##tmp_nfcc_nfmc_extract x
inner join hud_interviews i on x.hud_interview = i.hud_interview
inner join counselors co on i.interview_counselor = co.Person
inner join names n on co.NameID = n.Name

-- MHA Note
update	##tmp_nfcc_nfmc_extract
set		MHA_screening_note = 'Y'
from	##tmp_nfcc_nfmc_extract x
left outer join view_client_has_mha_note_subject mha on x.clientid = mha.client
where	mha.client is not null;

-- GLB Note
update	##tmp_nfcc_nfmc_extract
set		GLB_verbal_note = 'Y'
from	##tmp_nfcc_nfmc_extract x
left outer join view_client_has_glb_note_subject glb on x.clientid = glb.client
where	glb.client is not null;

-- NFMCP Note
update	##tmp_nfcc_nfmc_extract
set		NFMCP_verbal_note = 'Y'
from	##tmp_nfcc_nfmc_extract x
left outer join view_client_has_nfmcp_note_subject nfmcp on x.clientid = nfmcp.client
where	nfmcp.client is not null;

-- Level 2 Note
update	##tmp_nfcc_nfmc_extract
set		Level_2_note = 'Y'
from	##tmp_nfcc_nfmc_extract x
left outer join view_client_has_level_2_initiated_note_subject lvltwo on x.clientid = lvltwo.client
where	lvltwo.client is not null;

-- Return success to the caller
return ( 1 )
GO
GRANT EXECUTE ON nfmcp_extract_select TO public AS dbo;
GO
DENY EXECUTE ON nfmcp_extract_select TO www_role;
GO
