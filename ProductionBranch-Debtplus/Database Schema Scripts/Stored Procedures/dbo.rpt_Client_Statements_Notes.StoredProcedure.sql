USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statements_Notes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statements_Notes] ( @client_statement_batch as int = null, @client as int = null) as
-- ===========================================================================================
-- ==        Client statement notes information                                             ==
-- ===========================================================================================

-- Get the statement note
declare	@batch_note	varchar(8000)
select	@batch_note	= note
from	client_statement_batches with (nolock)
where	client_statement_batch	= @client_statement_batch

-- Find the client notes
select	@batch_note	= isnull(@batch_note + char(13) + char(10) + char(13) + char(10), '') + ltrim(rtrim(isnull(note,'')))
from	client_statement_notes with (nolock)
where	client_statement_batch	= @client_statement_batch
and	client			= @client

-- Make the note empty if there are no notes
while left(@batch_note,1) = char(13)
	select	@batch_note = substring(@batch_note, 3, 800)

if ltrim(rtrim(isnull(@batch_note,''))) = ''
	select	@batch_note	= null

if @batch_note is null
begin
	-- We need to create an empty result set. Do it with a temp table.
	create table #results ( note varchar(800) );
	select	* from #results;
	drop table #results
end else
	select	@batch_note	as note

return ( @@rowcount )
GO
