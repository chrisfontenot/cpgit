USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_impossible_debts]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_impossible_debts] AS
-- =================================================================================================
-- ==       Return a list of the active clients who have an impossible debt situation             ==
-- =================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SET NOCOUNT ON

-- First, build a table showing the debt and the correct interest rate for the debt balance
SELECT		cc.client_creditor,
		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance',

		case
			when dmp_interest IS NOT NULL then dmp_interest / 12.0
			when (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) >= cr.highest_apr_amt then cr.highest_apr_pct / 12.0
			when (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)) >= cr.medium_apr_amt then cr.medium_apr_pct / 12.0
			else cr.lowest_apr_pct / 12.0
		end as dmp_interest_rate

INTO		#t_problem_debts

FROM		client_creditor cc		WITH (NOLOCK)
inner join	client_creditor_balances bal	WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
INNER JOIN	clients c			WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN	creditor_classes ccl		WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class

WHERE		isnull(ccl.agency_account,0) = 0
AND		c.active_status IN ('A', 'AR')
AND		cc.reassigned_debt = 0
AND		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0

-- Return the information from the temporary table
SELECT	cc.client,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',
		cc.creditor,
		cr.creditor_name,
		cc.client_creditor,
		t.balance,

		case
			when t.dmp_interest_rate < 1.0 then t.dmp_interest_rate * 1200.0
			else t.dmp_interest_rate * 12.0
		end as 'dmp_interest_rate',

		cc.disbursement_factor,
		(t.balance * t.dmp_interest_rate) as 'interest_burden',
		cc.account_number

FROM		#t_problem_debts t
INNER JOIN	client_creditor cc ON t.client_creditor = cc.client_creditor
INNER JOIN	creditors cr ON cc.creditor = cr.creditor
LEFT OUTER JOIN	people p ON cc.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

WHERE		t.dmp_interest_rate > 0
AND			(t.balance * t.dmp_interest_rate) >= cc.disbursement_factor
ORDER BY	cc.client, cr.type, cr.creditor_id, cc.creditor, cc.client_creditor

RETURN ( @@rowcount )
GO
