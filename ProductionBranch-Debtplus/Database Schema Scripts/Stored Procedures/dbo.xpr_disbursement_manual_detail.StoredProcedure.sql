USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_manual_detail]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_manual_detail] ( @trust_register as int, @disbursement_register AS typ_key, @client_creditor as int, @amount AS Money, @fairshare_amt AS Money, @creditor_type as char(1) = 'N' ) AS

-- ===========================================================================================================
-- ==                   Create an item for the manual disbursement of this debt                             ==
-- ===========================================================================================================

-- ChangeLog
--   1/13/2001
--     Changed because the "first_payment_date", "first_payment_amount", and "last_payment_date" was moved
--     to the registers_client_creditor based upon first_payment and last_payment pointers.
--   1/19/2001
--     Added support for max_fairshare_per_check in the creditor to limit the fairshare dollar amount.
--   2/18/2002
--     Added client 0 transaction to record the change in the deduct balance.
--   3/14/2002
--     Ensure that the disbursement register is listed in the registers_client_creditor transaction
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   8/12/2004
--     Changed column from "max_fairshare_per_check" to "max_fairshare_per_debt" in the creditors table

DECLARE	@client						int
DECLARE	@creditor					varchar(10)
DECLARE	@error_string			VarChar(80)
DECLARE	@account_number			VarChar(22)
DECLARE	@debt_string			VarChar(16)
DECLARE	@fairshare_pct_check		FLOAT
DECLARE	@local_return			INT
DECLARE	@client_creditor_register	INT
DECLARE	@active_status			VarChar(10)
DECLARE	@current_balance		Money
DECLARE	@deducted			Money
DECLARE	@billed				Money
DECLARE	@max_fairshare_per_debt		Money

-- Suppress intermediate result sets
SET NOCOUNT ON

-- Generate the debt string
SELECT	@debt_string = dbo.format_client_id ( @client ) + '*' + upper( @creditor )

-- Fetch the client information
SELECT	@client					= cc.client,
		@creditor				= cc.creditor,
		@account_number			= left( isnull(cc.account_number,''), 22),
	@active_status		= isnull(c.active_status,'PND'),
	@fairshare_pct_check	= coalesce(cc.fairshare_pct_check, pct.fairshare_pct_check, 0),
	@max_fairshare_per_debt = isnull(cr.max_fairshare_per_debt,0)

FROM	client_creditor cc	WITH (NOLOCK)
LEFT OUTER JOIN clients c	WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN creditors cr	WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_contribution_pcts pct WITH (NOLOCK) on cr.creditor_contribution_pct = pct.creditor_contribution_pct

WHERE	cc.client_creditor	= @client_creditor

IF isnull(@active_status,'PND') not in ('A', 'AR')
BEGIN
	Rollback Transaction
	RaisError ( 50060, 16, 1, @client )
	Return ( 0 )
END

IF @account_number IS NULL
BEGIN
	Rollback Transaction
	RaisError ( 50061, 11, 1, @client, @creditor, @client_creditor )
	Return ( 0 )
END

IF @account_number = ''
BEGIN
	Rollback Transaction
	RaisError ( 50061, 11, 1, @client, @creditor, @client_creditor )
	Return ( 0 )
END

-- Reject a fairshare contribution if the client/creditor does not authorize a fairshare. I don't check for a match, only that the creditor has some fairshare.
IF @fairshare_pct_check <= 0 AND @fairshare_amt > 0
BEGIN
	Rollback Transaction
	RaisError ( 50062, 16, 1, @client, @creditor, @client_creditor )
	Return ( 0 )
END

-- Attempt to remove the money from the client's trust account
IF NOT EXISTS (SELECT TOP 1 client FROM clients WHERE client = @client)
BEGIN
	Rollback Transaction
	RaisError (50014, 11, 1, @client)
	Return ( 0 )
END

-- Verify the current balance from the trust account.
SELECT	@current_balance	= isnull(held_in_trust,0)
FROM	clients WITH (NOLOCK)
WHERE	client			= @client

IF isnull(@current_balance,-1) < 0
BEGIN
	Rollback Transaction
	RaisError (50063, 16, 1, @client)
	Return ( 0 )
END

-- Take the money from the client
UPDATE	clients
SET	held_in_trust	= isnull(held_in_trust,0) - @amount
WHERE	client		= @client

-- Verify the client no longer has a negative balance
SELECT	@current_balance	= held_in_trust
FROM	clients
WHERE	client			= @client

IF @current_balance < 0
BEGIN
	Rollback Transaction
	RaisError (50064, 16, 1, @client)
	Return ( 0 )
END

-- Determine the appropirate values for the deducted and billed values
if @creditor_type IS NULL
begin
	SELECT	@creditor_type = isnull(pct.creditor_type_check,'N')
	FROM	creditors cr WITH (NOLOCK)
	LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
	WHERE	cr.creditor = @creditor
end

-- Limit the fairshare to the maximum dollar amount on the check
if @max_fairshare_per_debt > 0
begin
	if @max_fairshare_per_debt < @fairshare_amt
		select @fairshare_amt = @max_fairshare_per_debt
end

SELECT	@billed		= 0,
	@deducted	= 0

IF @creditor_type = 'B'
	SELECT	@billed		= @fairshare_amt
ELSE IF @creditor_type = 'D'
	SELECT	@deducted	= @fairshare_amt

-- If there is a deduction amount, transfer the amount to the deduction account
IF @deducted > 0
BEGIN
	update	clients
	set	held_in_trust	= held_in_trust + @deducted
	where	client		= 0

	insert into registers_client	(tran_type,	client,		credit_amt,	disbursement_register,	trust_register)
	values				('MD',		0,		@deducted,	@disbursement_register,	@trust_register)
END

-- Record the payment of the debt
insert into registers_client_creditor	(tran_type,	client,		creditor,	client_creditor,	debit_amt,	fairshare_amt,	fairshare_pct,		creditor_type,	trust_register,		disbursement_register)
values					('MD',		@client,	@creditor,	@client_creditor,	@amount,	@fairshare_amt,	@fairshare_pct_check,	@creditor_type,	@trust_register,	@disbursement_register)

SELECT	@client_creditor_register = SCOPE_IDENTITY()

-- Record the debt information
update	client_creditor
set	first_payment		= isnull(first_payment, @client_creditor_register),
	last_payment		= @client_creditor_register,
	payments_this_creditor	= isnull(payments_this_creditor,0) + @amount
where	client_creditor		= @client_creditor

update	client_creditor_balances
set	total_payments		= total_payments + @amount,
	payments_month_0	= payments_month_0 + @amount
from	client_creditor_balances bal
inner join client_creditor cc on bal.client_creditor_balance = cc.client_creditor_balance
where	cc.client_creditor		= @client_creditor

-- Record the creditor information
update	creditors
set	distrib_mtd				= isnull(distrib_mtd,0) + @amount,
	contrib_mtd_billed			= isnull(contrib_mtd_billed,0) + @billed,
	contrib_mtd_received			= isnull(contrib_mtd_received,0) + @deducted
where	creditor				= @creditor

-- Update the check amount
update	registers_trust
set	amount					= isnull(amount,0) + @amount - @deducted,
	check_order				= 0
where	trust_register				= @trust_register

-- Return success to the caller
RETURN ( 1 )
GO
