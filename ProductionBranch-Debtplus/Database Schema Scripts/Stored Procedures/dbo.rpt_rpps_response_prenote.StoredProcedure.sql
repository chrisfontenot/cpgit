USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_prenote]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_prenote] ( @response_batch_id as varchar(50) ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of error prenotes                    ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--      Added client's counselor name.

-- Suppress the count of records
set nocount on

-- Retrieve the information for the payment records
select		null								as 'date_created', -- 1
		rt.client							as 'client', -- 2
		isnull(pn.last,'')	 					as 'client_name', -- 3
		rt.creditor							as 'creditor', -- 4
		rt.client_creditor							as 'client_creditor', -- 5
		cc.account_number						as 'account_number', -- 6
		isnull(cr.creditor_name,'')					as 'creditor_name', -- 7
		coalesce(x.description, rt.return_code, '')			as 'error_code', -- 8
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as 'counselor_name', -- 9
		
		cr.type, -- 10
		cr.creditor_id -- 11

from		rpps_transactions rt		with (nolock)
inner join	people p			with (nolock) ON rt.client=p.client AND 1=p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
left outer join	rpps_reject_codes x		with (nolock) ON rt.return_code = x.rpps_reject_code
left outer join client_creditor cc		with (nolock) ON rt.client_creditor = cc.client_creditor
left outer join	creditors cr			with (nolock) on rt.creditor = cr.creditor

left outer join clients c			with (nolock) on rt.client = c.client
left outer join counselors co			with (nolock) on c.counselor = co.counselor
left outer join names cox                       with (nolock) on co.NameID = cox.name

where		rt.response_batch_id = @response_batch_id
and		rt.service_class_or_purpose = 'CIE'
and		rt.transaction_code = 23

order by	10, 11, 2, 6 -- cr.type, cr.creditor_id, c.client, cc.account_number

return ( @@rowcount )
GO
