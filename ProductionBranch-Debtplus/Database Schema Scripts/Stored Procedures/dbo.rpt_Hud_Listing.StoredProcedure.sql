USE [DebtPlus]
GO
if not exists(select * from sysobjects where type = 'p' and name = 'rpt_Hud_Listing')
begin
	exec ('create procedure rpt_Hud_Listing as return 0;')
	exec ('grant execute on rpt_Hud_Listing to public as dbo')
	exec ('deny execute on rpt_Hud_Listing to www_role')
end
GO
ALTER PROCEDURE [dbo].[rpt_Hud_Listing] AS

-- ==============================================================================================
-- ==           Generate a list of the clients associated with the HUD grants                  ==
-- ==============================================================================================

-- Fetch the detail information with the HUD id and the client
SELECT	h.client as hud_id,
		h.client as client,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name,
		c.active_status,
		hp.description as 'loan_position',
		ht.description as 'loan_type',
		coalesce(svc.description, lnd.ServicerName, '') as lender_name,
		lnd.AcctNum as lender_loan_number,
		isnull(sl.CurrentLoanBalanceAmt,0) as amount_owed,
		case when isnull(sl.LoanDelinquencyMonths,0) < 0 then 0 else isnull(sl.LoanDelinquencyMonths,0) end as mos_past_due
FROM		client_housing h
LEFT OUTER JOIN	people p ON h.client=p.client and 1=p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
INNER JOIN	clients c ON h.client = c.client
inner join  housing_properties sp with (nolock) on c.client = sp.HousingID and 1 = sp.UseInReports
inner join  housing_loans sl with (nolock) on sp.oID = sl.PropertyID AND 1 = sl.UseInReports
inner join  housing_loan_details d with (nolock) on sl.IntakeDetailID = d.oID
left outer join Housing_LoanPositionTypes hp with (nolock) on sl.Loan1st2nd  = hp.oID
left outer join Housing_LoanTypes ht with (nolock) on d.LoanTypeCD = ht.oID
left outer join housing_lenders lnd with (nolock) on sl.CurrentLenderID = lnd.oID
left outer join housing_lender_servicers svc with (nolock) on lnd.ServicerID = svc.oID
ORDER by 1, 2	-- hud id, client

RETURN ( @@rowcount )
GO
