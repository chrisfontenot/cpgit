USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_geninfo_people]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_geninfo_people] ( @client as int ) as

-- =================================================================================
-- ==            Return the people information                                    ==
-- =================================================================================

-- Suppress intermediate results
set nocount on

select case p.relation
			when 1	then 'Applicant'
					else 'Co-Applicant'
			end										as 'person',

		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'name',
		dbo.format_ssn ( p.ssn )					as 'ssn',
	 	dbo.Format_TelephoneNumber ( p.WorkTelephoneID )	as 'work_ph',
		convert(varchar(10),null)					as 'work_ext',
		isnull(e.name,'')							as 'employer',
		dbo.format_gender ( p.gender )				as 'gender',
		dbo.format_race ( p.race )					as 'race',
		convert(varchar(10), p.birthdate, 101)		as 'dob',
		coalesce(j.description, p.other_job, '')	as 'job',
		dbo.format_education ( p.education )		as 'education'

from		people p		with (nolock)
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join	employers e		with (nolock) on p.employer = e.employer
left outer join	job_descriptions j	with (nolock) on p.job = j.job_description
where	p.client = @client
order by p.relation

return ( @@rowcount )
GO
