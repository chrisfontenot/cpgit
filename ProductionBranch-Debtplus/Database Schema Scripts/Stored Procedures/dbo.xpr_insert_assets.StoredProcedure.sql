USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_assets]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_assets] ( @client as int, @asset_id as int, @amount as money, @asset_amount as money, @frequency as int = 4) as
	insert into assets([client], [asset_id], [amount], [frequency]) values (@client, @asset_id, ISNULL(@amount,0), isnull(@frequency,4))
	return ( scope_identity() )
GO
