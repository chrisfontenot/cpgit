USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_AppointmentTypeAttribute]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_AppointmentTypeAttribute] ( @AppointmentType as int, @Attribute as int ) as
	insert into AppointmentTypeAttributes ( AppointmentType, Attribute ) values ( @AppointmentType, @Attribute )
	return ( scope_identity() )
GO
