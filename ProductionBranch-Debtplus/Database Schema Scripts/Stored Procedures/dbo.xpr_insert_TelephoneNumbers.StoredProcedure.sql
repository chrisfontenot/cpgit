USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_TelephoneNumbers]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_TelephoneNumbers] ( @country as int = NULL, @acode as varchar(10) = null, @number as varchar(50) = null, @ext as varchar(20) = null ) as

set nocount on

if ISNULL(@country,0) < 1
    select @country = country
    from    countries
    where   [default] = 1
    
if ISNULL(@country,0) < 1
    select @country = MIN(country)
    from    countries

-- Add the telephone number
insert into TelephoneNumbers ( [Country], [Acode], [Number], [Ext] ) 
SELECT	isnull(@country,1), isnull(@acode,''), isnull(@number,''), isnull(@ext,'')

return ( scope_identity() )
GO
