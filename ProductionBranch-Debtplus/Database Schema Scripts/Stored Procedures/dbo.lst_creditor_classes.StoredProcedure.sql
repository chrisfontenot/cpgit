USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_creditor_classes]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_creditor_classes] AS

-- ==========================================================================================================
-- ==            Return the list of creditor classes for the creditor form                                 ==
-- ==========================================================================================================

select	creditor_class		as item_key,
	description		as description
from	creditor_classes with (nolock)
order by 2

return ( @@rowcount )
GO
