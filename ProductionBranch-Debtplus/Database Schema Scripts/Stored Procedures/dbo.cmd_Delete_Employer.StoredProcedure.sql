USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Delete_Employer]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Delete_Employer] ( @Employer AS INT ) AS
-- ==========================================================================
-- ==               Delete an Employer                                     ==
-- ==========================================================================
set nocount on

-- Remove the employer references from the clients
update people set employer = null where employer = @Employer

-- Remove the employer from the system
delete from employers where employer = @Employer

return ( 1 )
GO
