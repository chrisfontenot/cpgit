USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cdv]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cdv] ( @rpps_response_file as int ) as

-- =================================================================================================================
-- ==            Generate the information for the RPPS subreport for CDV transactions                             ==
-- =================================================================================================================

-- ChangeLog
--   2/18/2004
--      Added client's counselor name.
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

if @rpps_response_file is null
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)

select		isnull(t.client,cdv.client) as client,
			cdv.ssn,
			cdv.account_balance,
			t.old_balance,

			case
				when d.return_code = 'CDV' then isnull(d.processing_error,'')
				when rej.description is not null then rej.description
				else d.processing_error
			end as processing_error,

			d.account_number,
			isnull('[' + t.creditor + ']', d.rpps_biller_id) as creditor,
			coalesce(cr.creditor_name,biller1.biller_name,'UNKNOWN CREDITOR') as creditor_name,
			dbo.format_normal_name ( default, pn.first, pn.middle, pn.last, default ) as client_name,
			convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default)) as 'counselor_name'

from	rpps_response_details d					with (nolock)
left outer join	rpps_response_details_cdv cdv	with (nolock) on d.rpps_response_detail = cdv.rpps_response_detail
left outer join rpps_transactions t				with (nolock) on d.rpps_transaction = t.rpps_transaction
left outer join rpps_biller_ids biller1			with (nolock) on d.rpps_biller_id = biller1.rpps_biller_id
left outer join creditors cr					with (nolock) on t.creditor = cr.creditor
left outer join rpps_reject_codes rej			with (nolock) on d.return_code = rej.rpps_reject_code and d.return_code <> 'CDV'
left outer join people p						with (nolock) on isnull(t.client,cdv.client) = p.client and 1 = p.relation
left outer join names pn						with (nolock) on p.NameID = pn.Name
left outer join clients c						with (nolock) on p.client = c.client
left outer join counselors co					with (nolock) on c.counselor = co.counselor
left outer join names cox						with (nolock) on co.NameID = cox.name

where	d.service_class_or_purpose	= 'CDV'
and		d.rpps_response_file		= @rpps_response_file

order by	4, 3		-- creditor, account number

return ( @@rowcount )
GO
