USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appts_scheduled_analysis]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_appts_scheduled_analysis] ( @From_date as datetime = null, @To_date as datetime = null, @office as int = null ) as

-- Make the dates valid
set nocount on

if @To_Date is null
	select	@To_Date	= getdate()

if @From_Date is null
	select	@From_Date	= @To_Date

select	@From_Date	= convert(datetime, convert(varchar(10), @From_Date, 101) + ' 00:00:00'),
	@To_Date	= convert(datetime, convert(varchar(10), @To_Date, 101) + ' 23:59:59')

create table #avail_appts ( appt_time int, office int, start_time datetime, avail int, booked int )

-- Find the available slots for appointments. Look at the list of counselors and count those as a free slot
-- for the indicated time period.
if @office is not null

	insert into #avail_appts ( appt_time, office, start_time, avail, booked )
	select	t.appt_time as appt_time,
		t.office as office,
		t.start_time as start_time,
		count (ao.appt_counselor) as avail,
		0 as booked

	from	appt_times t with (nolock)
	inner join offices o with (nolock) on t.office = o.office
	inner join appt_counselors ao with (nolock) on t.appt_time = ao.appt_time
	where	t.office = @office
	and	t.start_time between @from_date and @to_date
	and	inactive = 0
	group by t.appt_time, t.office, t.start_time;

else

	insert into #avail_appts ( appt_time, office, start_time, avail, booked )
	select	t.appt_time as appt_time,
		t.office as office,
		t.start_time as start_time,
		count (ao.appt_counselor) as avail,
		0 as booked

	from	appt_times t with (nolock)
	inner join appt_counselors ao with (nolock) on t.appt_time = ao.appt_time
	where	t.start_time between @from_date and @to_date
	and	t.office is not null
	and	inactive = 0
	group by t.appt_time, t.office, t.start_time;

-- Find the booked appointments by appt_time
select	x.appt_time, count(ca.client_appointment) as booked
into	#booked_appts
from	client_appointments ca with (nolock)
inner join #avail_appts x on ca.appt_time = x.appt_time
where	isnull(ca.status,'P') = 'P'
group by x.appt_time;

update	#avail_appts
set	booked	= i.booked
from	#avail_appts x
inner join #booked_appts i on x.appt_time = i.appt_time

drop table #booked_appts

-- Return the results to the user
-- "appt_count" at one time was the number of booked appointments. It was changed later to be the number
-- of AVAILABLE appointments.
select	dbo.date_only ( t.start_time )			as start_date,
	dbo.time_only ( t.start_time )			as start_time,
	t.office					as office,
	o.name						as office_name,
	convert(int,isnull(avail,0) - isnull(booked,0))	as appt_count

from	#avail_appts t
left outer join offices o with (nolock) on t.office = o.office
order by 4, 1, 2

drop table #avail_appts

return ( @@rowcount )
GO
