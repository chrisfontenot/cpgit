USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CreditorPayments_Invoices]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_CreditorPayments_Invoices] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ===============================================================================================
-- ==           Create a report which shows the creditor payments in the date range             ==
-- ===============================================================================================

-- ChangeLog
--   12/26/2002
--     Ordered the result set by creditor ID and then invoice number. This helps the report keep the creditors
--     together.
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

-- Suppress intermediate results
set nocount on

-- Default the dates
IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the list of payments to invoices
SELECT	d.creditor							as 'creditor',
	sum(case when tran_type = 'RC' then d.credit_amt else 0 end)	as 'check_amount',
	sum(case when tran_type = 'RA' then d.credit_amt else 0 end)	as 'adjustment_amount',
	isnull(d.message,'')						as 'reference',
	d.date_created							as 'date_created',
	d.invoice_register						as 'invoice',
	isnull(cr.creditor_name,'')					as 'creditor_name'
FROM	registers_creditor d
LEFT OUTER JOIN creditors cr ON d.creditor = cr.creditor
WHERE	tran_type IN ('RA', 'RC')
AND	d.date_created BETWEEN @FromDate AND @ToDate
GROUP BY d.creditor, cr.type, cr.creditor_id, d.message, d.date_created, d.invoice_register, cr.creditor_name
ORDER BY cr.type, cr.creditor_id, d.creditor, d.date_created, d.invoice_register

-- Return the number of rows in the result set
RETURN ( @@rowcount )
GO
