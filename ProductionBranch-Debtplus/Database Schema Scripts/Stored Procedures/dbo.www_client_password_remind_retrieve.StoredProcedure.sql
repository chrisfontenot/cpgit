USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_password_remind_retrieve]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_password_remind_retrieve] ( @client as int ) AS
-- =========================================================================================================
-- ==            Retrieve the question associated with the client                                         ==
-- =========================================================================================================

-- Suppress intermediate results
set nocount on

-- Ensure that the client exists
if not exists ( select	client
		from	clients
		where	client = @client )
begin
	RaisError ('The client account does not have www access', 16, 1)
	return ( 0 )
end

-- Ensure that the client exists in the www page
declare	@pkid		uniqueidentifier
declare	@question	varchar(256)
select	@pkid		= pkid,
		@question	= passwordquestion
from	client_www
where	username	= convert(varchar, @client)

if @pkid is null
begin
	RaisError ('The client account does not have www access', 16, 1)
	return ( 0 )
end

-- Retrieve the value for the question
select	@question as PasswordQuestion
return ( 1 )
GO
