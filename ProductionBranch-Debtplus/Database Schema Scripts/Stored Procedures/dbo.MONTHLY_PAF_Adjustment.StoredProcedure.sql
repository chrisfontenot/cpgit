USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_PAF_Adjustment]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MONTHLY_PAF_Adjustment] AS

-- =================================================================================================================
-- ==            Generate the next fee amount for the San Anonio system                                           ==
-- =================================================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information

RaisError('Don''t run this unless you know what you are doing!', 16, 1)
return ( 0 )

-- Program adminstration fee creditor
DECLARE	@PAF_Creditor	typ_creditor
select	@PAF_Creditor = paf_creditor
from	config

-- Do nothing if there is no creditor defined for a PAF fee
if @paf_creditor is null
	return ( 0 )

-- Set the needed flags
SET NOCOUNT ON
SET XACT_ABORT ON

BEGIN TRANSACTION

-- Find the information that we will be using for the update
select	cc.client, cc.creditor, cc.client_creditor, cc.disbursement_factor, bal.client_creditor_balance
into	#items
from	client_creditor_balances bal
inner join client_creditor cc on cc.client_creditor = bal.client_creditor
inner join clients c on cc.client = c.client
where	c.active_status in ('A', 'AR')
and	cc.creditor = @PAF_Creditor
and	cc.disbursement_factor > 0.0
and	cc.client_creditor_balance = bal.client_creditor_balance

insert into registers_client_creditor (tran_type, client, creditor, client_creditor, credit_amt, fairshare_pct, fairshare_amt)
select	'IN', client, creditor, client_creditor, 0.0, 0.0, disbursement_factor
from	#items

-- Insert a system note that we are adding amount to the fee
INSERT INTO client_notes (client,client_creditor,type,is_text,subject,note)
select	client, client_creditor, 3, 1, 'PAF Fee Increased', 'The Program administration fee was assesed and additional amount of $' + convert(varchar, disbursement_factor, 1)
from	#items

-- Adjust the statitistics for the total interest on this instance of the debt
update	client_creditor
set		interest_this_creditor	= isnull(interest_this_creditor,0) + x.disbursement_factor,
		sched_payment		= x.disbursement_factor
from	client_creditor cc
inner join #items x on cc.client_creditor = x.client_creditor

-- Adjust the balance on the fee account
UPDATE	client_creditor_balances
SET		total_interest		= bal.total_interest + x.disbursement_factor,
		current_sched_payment	= x.disbursement_factor
from	client_creditor_balances bal
inner join #items x on bal.client_creditor_balance = x.client_creditor_balance

-- Discard the working table
drop table #items

-- Complete the transaction processing
COMMIT TRANSACTION

-- Return success
RETURN ( 1 )
GO
