USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_CDV]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_CDV] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @name as varchar(10) = null, @account_number as varchar(40) = null, @net as money = 0,  @client as int, @ssn as varchar(9) = null, @balance as money = 0, @last_communication as varchar(15) = null ) as

-- =======================================================================================================================
-- ==            Generate the response for a CDV request                                                                ==
-- =======================================================================================================================

-- ChangeLog
--   12/10/2008
--      Changed '@account' to '@account_number' for .NET

-- Generate the base response row
declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'CDV'

-- Insert the addendum information for proposal acceptances
if @rpps_response_detail > 0
begin
	if @ssn is not null
		select @ssn = ltrim(rtrim(@ssn))

	if @ssn not like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		select @ssn = null

	if @last_communication is not null
		select @last_communication = ltrim(rtrim(@last_communication))

	if @last_communication = ''
		select @last_communication = null

	update	rpps_response_details
	set	account_number		= @account_number
	where	rpps_response_detail	= @rpps_response_detail

	-- Update the addendum information for a rejected proposal
	insert into rpps_response_details_cdv (rpps_response_detail, last_communication, account_balance, ssn, client)
	values (@rpps_response_detail, @last_communication, @balance, @ssn, @client)
end

return ( @rpps_response_detail )
GO
