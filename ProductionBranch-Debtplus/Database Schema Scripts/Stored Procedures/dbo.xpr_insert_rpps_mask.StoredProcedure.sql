USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_rpps_mask]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_rpps_mask] (@rpps_biller_id varchar(20), @length int, @mask varchar(80), @description varchar(80) = null, @exceptionmask bit = 0, @checkdigit bit = 0 ) as
INSERT INTO rpps_masks (rpps_biller_id, length, mask, description, ExceptionMask, CheckDigit) VALUES (@rpps_biller_id, @length, @mask, @description, @ExceptionMask, @CheckDigit)
return (scope_identity())
GO
