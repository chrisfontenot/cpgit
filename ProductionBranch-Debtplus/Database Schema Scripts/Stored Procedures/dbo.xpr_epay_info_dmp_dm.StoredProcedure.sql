USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_info_dmp_dm]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_info_dmp_dm] ( @epay_transaction as int ) as

-- ================================================================================================
-- ==         Find the information for the current transaction                                   ==
-- ================================================================================================

-- ChangeLog
--   12/14/2005
--      Allow an agency concentrator to be used rather than the default of the immediate_origin

-- Suppress intermediate results
set nocount on

-- Parameters that are needed for the client header are listed below
declare	@transaction_type	varchar(2)
declare	@request_date		datetime
declare	@client			int
declare	@agency_id		varchar(35)
declare	@resubmission		varchar(10)
declare	@disbursement_factor	money
declare	@start_date		datetime
declare	@client_name		varchar(80)
declare	@home_addr1		varchar(80)
declare	@home_addr2		varchar(80)
declare	@home_city		varchar(60)
declare	@home_state		varchar(10)
declare	@home_zipcode		varchar(60)
declare	@agency_name		varchar(35)
declare	@living_expenses	money
declare	@gross_income		money
declare	@number_of_creditors	int
declare	@total_debt		money
declare	@ssn			varchar(14)

-- Find the client information
declare	@client_creditor		int
declare	@client_creditor_proposal	int
declare	@epay_file			int
declare	@bank				int

select	@client_creditor_proposal	= client_creditor_proposal,
	@epay_file			= epay_file,
	@transaction_type		= 'DM'	-- This is a constant for proposals.
from	epay_transactions with (nolock)
where	epay_transaction		= @epay_transaction;

select	@bank				= bank
from	epay_files with (nolock)
where	epay_file			= @epay_file;

select	@agency_name			= isnull(b.immediate_origin_name, cnf.[name]),
	@agency_id			= isnull(b.ach_origin_dfi, right('000000000000' + ltrim(rtrim(isnull(b.immediate_origin,''))), 12))
from	banks b with (nolock), config cnf with (nolock)
where	b.bank				= @bank

select	@client_creditor		= cc.client_creditor,
	@start_date			= c.start_date,		-- spec says "client program start date", not debt start date.
	@client				= cc.client,
	@resubmission			= case
						when c.active_status = 'AR' then 'Y'
						else 'N'
					  end,

	@client_name			= coalesce(cc.client_name, dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix), ''),
	@ssn				= isnull(p.ssn,''),
	@home_addr1			= dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),
	@home_addr2			= a.address_line_2,
	@home_city			= a.city,
	@home_state			= st.MailingCode,
	@home_zipcode			= left(a.PostalCode,5)

from	client_creditor_proposals pr with (nolock)
inner join client_creditor cc with (nolock)	on pr.client_creditor = cc.client_creditor
inner join clients c with (nolock)		on cc.client = c.client
left outer join people p with (nolock)		on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join addresses a with (nolock) on c.addressid = a.address
left outer join states st with (nolock) on a.state = st.state
where	pr.client_creditor_proposal	= @client_creditor_proposal;

-- Find the total proposed deposit amount
select	@disbursement_factor		= sum(isnull(deposit_amount,0))
from	client_deposits with (nolock)
where	client				= @client
and	isnull(one_time,0)		= 0;

-- Find the gross income
select	@gross_income			= sum(isnull(gross_income,0))
from	people with (nolock)
where	client				= @client;

-- Find the latest budget
declare	@budget		int
select	@budget		= dbo.map_client_to_budget ( @client )

-- Find the living expenses from the budget
if @budget is not null
	select		@living_expenses	= sum(isnull(suggested_amount,0))
	from		budget_detail d
	left outer join	budget_categories cat on d.budget_category = cat.budget_category
	where		d.budget		= @budget
	and		isnull(cat.housing_expense,0) = 0;

if @living_expenses is null
	select	@living_expenses	= 0

-- Find the number of creditors
select	@number_of_creditors	= count(*)
from	client_creditor cc		with (nolock)
inner join client_creditor_proposals pr	with (nolock) on pr.client_creditor = cc.client_creditor
inner join creditors cr			with (nolock) on cc.creditor = cr.creditor
inner join creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class
inner join banks b			with (nolock) on pr.bank = b.bank and 'E' = b.type
where	pr.epay_biller_id is not null
and	cc.client		= @client
and	cc.reassigned_debt	= 0
and	ccl.agency_account	= 0;

-- Find the total debt owed
select		@total_debt		= sum(isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0))
from		client_creditor_balances bal
inner join	client_creditor cc	on cc.client_creditor_balance = bal.client_creditor_balance
inner join	creditors cr		on cc.creditor = cr.creditor
inner join	creditor_classes ccl	on cr.creditor_class = ccl.creditor_class
where		client			= @client
and		cc.reassigned_debt	= 0
and		ccl.agency_account	= 0;

-- Return the results
select	@transaction_type	as transaction_type,
	@request_date		as request_date,
	@client			as client,
	@agency_id		as agency_id,
	@resubmission		as resubmission,
	@disbursement_factor	as disbursement_factor,
	@start_date		as start_date,
	@client_name		as client_name,
	@home_addr1		as home_addr1,
	@home_addr2		as home_addr2,
	@home_city		as home_city,
	@home_state		as home_state,
	@home_zipcode		as home_zipcode,
	@agency_name		as agency_name,
	@living_expenses	as living_expenses,
	@gross_income		as gross_income,
	@number_of_creditors	as number_of_creditors,
	@total_debt		as total_debt,
	@ssn			as ssn

return ( 1 )
GO
