USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response] ( @file_id as int = null ) as

set nocount on

if @file_id is null
begin
	select	@file_id = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

select	rpps_response_file,
	isnull(filename,'')	as filename,
	isnull(label,'')	as label,
	file_date		as file_date
from	rpps_response_files
where	rpps_response_file = @file_id

return ( @@rowcount )
GO
