USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_referred_by]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_referred_by] ( @description as varchar(50), @referred_by_type as int = 1, @note as varchar(256) = null, @hud_9902_section as varchar(50) = null, @default as bit = 0, @ActiveFlag as bit = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the referred_by table                    ==
-- ========================================================================================
	insert into referred_by ( [referred_by_type], [description], [note], [hud_9902_section], [default], [ActiveFlag] )
    select isnull(@referred_by_type,1), isnull(@description,''), @note, @hud_9902_section, isnull(@default,0), isnull(@ActiveFlag,1)
	return ( scope_identity() )
GO
