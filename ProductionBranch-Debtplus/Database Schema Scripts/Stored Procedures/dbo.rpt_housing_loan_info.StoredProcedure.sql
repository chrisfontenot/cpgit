USE [DebtPlus]
GO
IF NOT EXISTS(SELECT * FROM sysobjects WHERE type = 'P' AND name = N'rpt_housing_loan_info')
BEGIN
	EXEC ('CREATE PROCEDURE [dbo].[rpt_housing_loan_info] AS RETURN 0')
	EXEC ('GRANT EXECUTE ON [dbo].[rpt_housing_loan_info] TO public AS dbo')
	EXEC ('DENY EXECUTE ON [dbo].[rpt_housing_loan_info] TO www_role')
END
GO
ALTER PROCEDURE [dbo].[rpt_housing_loan_info] AS
-- ==============================================================================================
-- ==           Generate a list of the clients associated with the Housing grants              ==
-- ==============================================================================================

-- Fetch the detail information with the HUD id and the client
SELECT	c.client as hud_id,
		h.client as client,
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as name,
		c.active_status,
		hp.description as 'loan_position',
		ht.description as 'loan_type',
		coalesce(svc.description, lnd.ServicerName,'') as lender_name,
		lnd.AcctNum as lender_loan_number,
		isnull(sl.CurrentLoanBalanceAmt,0) as amount_owed,
		case when isnull(sl.LoanDelinquencyMonths,0) < 0 then 0 else isnull(sl.LoanDelinquencyMonths,0) end as mos_past_due

FROM		client_housing h					with (nolock)
LEFT OUTER JOIN	people p						with (nolock) ON h.client=p.client and 1=p.relation
left outer join names pn						with (nolock) on p.nameid = pn.name
INNER JOIN	clients c							with (nolock) ON h.client = c.client
INNER JOIN  housing_properties sp				with (nolock) on c.client = sp.HousingID AND 1 = sp.UseInReports
INNER JOIN  housing_loans sl					WITH (NOLOCK) ON sp.oID = sl.PropertyID AND 1 = sl.UseInReports
left outer join housing_lenders lnd				with (nolock) on sl.CurrentLenderID = lnd.oID
left outer join Housing_LoanPositionTypes hp	with (nolock) on sl.Loan1st2nd  = hp.oID
left outer join housing_loan_details det		with (nolock) on sl.IntakeDetailID = det.OID
left outer join Housing_LoanTypes ht			with (nolock) on det.LoanTypeCD = ht.oID
left outer join housing_lender_servicers svc	with (nolock) on lnd.ServicerID = svc.oID

ORDER by 1, 2	-- hud id, client

RETURN ( @@rowcount )
GO
