SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_insert_recon_detail] ( @Batch AS INT, @TrustRegister as int = null, @bank as int = null, @checknum AS BigInt = NULL, @Cleared AS VarChar(1) = 'R', @ReconDate AS DateTime = NULL, @ReconAmount AS Money = NULL, @tran_type AS VarChar(2) = NULL, @Comments as varchar(80) = NULL, @ErrorMessage as varchar(80) = null ) AS

-- =======================================================================================================================
-- ==            Attempt to reconcile the item against the check register                                               ==
-- =======================================================================================================================

-- ChangeLog
--   2/15/2002
--     Added support for other transaction types
--   7/22/2003
--     Add test to determine if the item is reconciled earlier in the same batch.
--   7/28/2004
--     Do not call it "DUPLICATE ITEM" if the bank is confirming that the check is now void.

-- Disable intermediate results
SET NOCOUNT ON
	
-- Trust register pointer
DECLARE	@TrustTranType	VarChar(5)
DECLARE	@TrustAmount	Money
DECLARE	@TrustCleared	VarChar(2)
DECLARE	@Item			INT

DECLARE	@FromDate		datetime
DECLARE	@ToDate			datetime
SELECT	@FromDate	= convert(varchar(10), @ReconDate, 101)
SELECT	@ToDate		= dateadd( d, 1, @FromDate)

-- Attempt to find the item in the trust register
IF @tran_type IS NULL
	SELECT @tran_type = 'AD'
	
ELSE IF @tran_type NOT IN ('AD', 'BW', 'SC', 'BI', 'CR', 'MD', 'CM', 'DP', 'RR', 'RF')
BEGIN
	RaisError (50021, 16, 1, @tran_type)
	Return ( 0 )
END

-- The status must be valid
IF @Cleared NOT IN ('R', 'C', 'V')
BEGIN
	RaisError (50022, 16, 1, @Cleared)
	Return ( 0 )
END

-- Start a transaction for these requests
BEGIN TRANSACTION

-- Find the trust register if we need
if @TrustRegister is null
begin
	-- Try to find the default bank if possible
	if @bank is null
	begin
		-- Look for deposits
		if @tran_type in ('DP','RF')
		begin
			select	@bank	= min(bank)
			from	banks
			where	[default] = 1
			and		[type]	  in ('D','C')
		
			if @bank is null
				select	@bank	= min(bank)
				from	banks
				where	[type] in ('D','C')
		end

		-- Look for bank wires
		if @tran_type = 'BW'
		begin
			select	@bank	= min(bank)
			from	banks
			where	[default] = 1
			and		[type]	  = 'R'
		
			if @bank is null
				select	@bank	= min(bank)
				from	banks
				where	[type] = 'R'
		end

		-- Look for physical checks
		if @tran_type in ('AD','CR')
		begin
			select	@bank	= min(bank)
			from	banks
			where	[default] = 1
			and		[type]	  = 'C'
		
			if @bank is null
				select	@bank	= min(bank)
				from	banks
				where	[type] = 'C'
		end
	end

	-- Finally, give up and take the first bank (or just assume that it is 1).
	if @bank is null
		select	@bank	= min(bank)
		from	banks

	if @bank is null
		select	@bank	= 1

	-- If there is no check number then clear the check number
	if @checknum <= 0
		SELECT	@checknum = null

	-- Transactions other than checks do not have a check number
	IF @tran_type NOT IN ('AD', 'CM', 'CR', 'MD', 'RF')
		SELECT @checknum = NULL

	-- There must be a check number for a check. If there is no check number then this must be a bank error.
	IF @checknum is null and @tran_type = 'AD'
		SELECT	@ErrorMessage = 'NO NUMBER'

	-- If there is a comment and no trust reigster, then try to find the item in the reconciled list
	IF @Comments IS NOT NULL and @tran_type in ('SC', 'BE', 'BI', 'DP', 'RF') and @ErrorMessage is null
		SELECT	@TrustRegister	= trust_register
		FROM	recon_details
		WHERE	comments	= @Comments
		AND		recon_date	= @ReconDate
		AND		amount		= @ReconAmount

	-- Find the check or deposit in the trust register
	IF @TrustRegister IS NULL and @checknum is not null
	BEGIN
		SELECT	TOP 1
				@TrustRegister	= trust_register,
				@tran_type	= tran_type
		FROM	registers_trust WITH (NOLOCK)
		WHERE	tran_type		IN ('AD', 'CM', 'MD', 'CR', 'AR')
		AND		checknum		= @checknum
		AND		bank			= @bank
		AND		isnull(cleared,' ')	<> 'D'
		ORDER BY	date_created desc;
	END

	-- Try to match the transaction against the trust register for the item type and its amount
	if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
		SELECT	@TrustRegister	= trust_register
		FROM	registers_trust with (nolock)
		WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
		AND		amount		= @ReconAmount
		AND		isnull(cleared,' ')	!= 'D'
		AND		date_created >= @FromDate
		AND		date_created < @ToDate

	-- Try one day earlier
	if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
		SELECT	@TrustRegister	= trust_register
		FROM	registers_trust with (nolock)
		WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
		AND		amount		= @ReconAmount
		AND		isnull(cleared,' ')	!= 'D'
		AND		date_created >= dateadd( d, -1, @FromDate )
		AND		date_created < @ToDate

	-- Try two days earlier
	if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
		SELECT	@TrustRegister	= trust_register
		FROM	registers_trust with (nolock)
		WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
		AND		amount		= @ReconAmount
		AND		isnull(cleared,' ')	!= 'D'
		AND		date_created >= dateadd( d, -2, @FromDate )
		AND		date_created < @ToDate

	-- Try three days earlier
	if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
		SELECT	@TrustRegister	= trust_register
		FROM	registers_trust with (nolock)
		WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
		AND		amount		= @ReconAmount
		AND		isnull(cleared,' ')	!= 'D'
		AND		date_created >= dateadd( d, -3, @FromDate )
		AND		date_created < @ToDate
end
		
-- There should be a trust register if this is valid
IF @TrustRegister IS NULL AND @ErrorMessage IS NULL
	SELECT @ErrorMessage = 'NOT FOUND'

-- Fetch the information from the trust register
IF @ErrorMessage IS NULL
BEGIN
	SELECT	@TrustTranType	= tran_type,
			@TrustAmount	= amount,
			@TrustCleared	= cleared
	FROM	registers_trust
	WHERE	trust_register	= @TrustRegister

	-- Check to ensure that the transaction types match
	IF @TrustTranType != @tran_type
	begin
		-- Deposits may be of several different types
		IF @tran_type = 'DP' AND @TrustTranType in ('RR', 'RF', 'VD')
			SELECT @tran_type = @TrustTranType
			
		-- Do the same for checks
		if @tran_type in ('AD','CM','MD','CR') and @TrustTranType in ('AD','CM','MD','CR')
			select @tran_type = @TrustTranType
	end
	
	IF @TrustTranType != @tran_type
		SELECT @ErrorMessage = 'WRONG TYPE'
END

IF @ErrorMessage IS NULL
BEGIN
	IF @ReconAmount IS NULL
		SELECT @ReconAmount = @TrustAmount
	ELSE
		IF @ErrorMessage IS NULL AND @TrustAmount != @ReconAmount
			SELECT @ErrorMessage = 'WRONG AMOUNT'
END

-- If the trust register is not pending then look for a void status being confirmed as void. Accept that as being OK.
-- All others are duplicated reconcilation information.
IF @ErrorMessage IS NULL AND isnull(@TrustCleared,' ') != ' '
BEGIN
	IF (@TrustCleared <> 'V') OR (@Cleared <> 'V')
		SELECT @ErrorMessage = 'DUPLICATE ITEM'
END

-- Look for duplicate items in the same batch.
-- This would mean that the bank cashed the same check for the same amount in the current list of checks. It is
-- definately "not good".
IF @ErrorMessage IS NULL
begin
	IF exists (SELECT * from recon_details WHERE trust_register = @TrustRegister AND recon_batch = @Batch)
		SELECT @ErrorMessage = 'DUPLICATE ITEM'
end

-- Default the reconcilation date
IF @ReconDate IS NULL
	SELECT @ReconDate = convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00')

if @ErrorMessage IS NOT NULL
	SELECT	@Cleared = ' '

-- Insert the item into the table
INSERT INTO	recon_details	(recon_batch,	tran_type,	checknum,	amount,			cleared,	recon_date,	trust_register,	message,		comments)
VALUES						(@Batch,		@tran_type,	@checknum,	@ReconAmount,	@Cleared,	@ReconDate,	@TrustRegister,	@ErrorMessage,	@Comments)

SELECT @Item = SCOPE_IDENTITY()

-- Return the status to the caller
COMMIT TRANSACTION
RETURN ( @Item )
GO
