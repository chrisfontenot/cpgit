USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_delete]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_delete] ( @Appt AS INT ) AS

-- ==================================================================================================
-- ==            Delete the indicated appointment                                                  ==
-- ==================================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

DECLARE	@Client		INT
DECLARE	@Rows		INT
DECLARE	@New_Appt	INT
DECLARE	@workshop	INT

-- Fetch the client ID
SELECT	@Client			= client,
	@workshop		= workshop
FROM	client_appointments
WHERE	client_appointment	= @Appt

IF (@Client IS NOT NULL) AND (@workshop is null)
BEGIN
	-- Remove the linkage on the first appointment for the client
	UPDATE	clients
	SET	first_appt		= NULL
	WHERE	client			= @client
	AND	first_appt		= @Appt

	-- Remove the rescheduled appointment
	UPDATE	clients
	SET	first_resched_appt	= NULL
	WHERE	client			= @client
	AND	first_resched_appt	= @Appt
END

-- Remove the appointment from the system tables
DELETE	client_appointments
WHERE	client_appointment	= @Appt

-- Save the row count for the ending process
SET @rows = @@rowcount

IF (@rows > 0) and (@workshop is null)
BEGIN
	-- Find the first appointment for the client now that the current one has been deleted
	SELECT TOP 1	@New_Appt	= client_appointment
	FROM		client_appointments
	WHERE		client		= @client
	ORDER BY	start_time

	-- Update the first client appointment from the resulting appointment list
	UPDATE	clients
	SET	first_appt		= @New_Appt
	WHERE	client			= @client

	-- If the appointment matches the rescheduled value then remove the rescheduled item
	UPDATE	clients
	SET	first_resched_appt	= NULL
	WHERE	client			= @client
	AND	first_resched_appt	= @New_Appt
END

-- Return success status to the caller
RETURN ( @Rows )
GO
