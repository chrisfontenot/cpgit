USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_level2_followup]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Paul Brown
-- Create date: 08/01/2012
-- Description:	Followup report for level 2 housing sessions based on notes
-- =============================================
CREATE PROCEDURE [dbo].[rpt_custom_level2_followup] 

AS
BEGIN

	SET NOCOUNT ON;

create table #results (client int, initiated datetime, initiated_by varchar(50), thirty_day_fu_done datetime, thirty_day_fu_done_by varchar(50), sixty_day_fu_done datetime, sixty_day_fu_done_by varchar(50), ninety_day_fu_done datetime, ninety_day_fu_done_by varchar(50), onetwenty_day_fu_done datetime, onetwenty_day_fu_done_by varchar(50), terminated datetime, terminated_by varchar(50));

-- select notes for level 2 initiation
SELECT     cn.client, cn.date_created as initiated, vu.name AS initiated_by
into #initiated
FROM         dbo.client_notes AS cn INNER JOIN
                      dbo.view_users AS vu ON vu.Person = cn.created_by
WHERE     (cn.client_note >= 40000000) AND (cn.subject like 'Level 2 Housing Initiated%');



-- select notes for 30-day fu complete
select cn.client, cn.date_created as thirty_day_fu_done, vu.name as thirty_day_fu_done_by
into #30day_fu
FROM         client_notes AS cn 
INNER JOIN view_users AS vu ON vu.Person = cn.created_by
where (cn.client_note > 52000000) AND (cn.subject LIKE 'Level 2 30-day Followup Complete%');

-- select notes for 60-day fu complete
select cn.client, cn.date_created as sixty_day_fu_done, vu.name as sixty_day_fu_done_by
into #60day_fu
FROM         dbo.client_notes AS cn INNER JOIN
                      dbo.view_users AS vu ON vu.Person = cn.created_by
where (cn.client_note > 52000000) AND (cn.subject LIKE 'Level 2 60-day Followup Complete%');

-- select notes for 90-day fu complete
select cn.client, cn.date_created as ninety_day_fu_done, vu.name as ninety_day_fu_done_by
into #90day_fu
FROM         dbo.client_notes AS cn INNER JOIN
                      dbo.view_users AS vu ON vu.Person = cn.created_by
where (cn.client_note > 52000000) AND (cn.subject LIKE 'Level 2 90-day Followup Complete%');

-- select notes for 120-day fu complete
select cn.client, cn.date_created as onetwenty_day_fu_done, vu.name as onetwenty_day_fu_done_by
into #120day_fu
FROM         dbo.client_notes AS cn INNER JOIN
                      dbo.view_users AS vu ON vu.Person = cn.created_by
where (cn.client_note > 52000000) AND (cn.subject LIKE 'Level 2 120-day Followup Complete%');


-- select notes for terminated
select cn.client, cn.date_created as terminated, vu.name as terminated_by
into #terminated
FROM         dbo.client_notes AS cn INNER JOIN
                      dbo.view_users AS vu ON vu.Person = cn.created_by
where (cn.client_note > 40000000) AND (cn.subject LIKE 'Level 2 Counseling Terminated%');

-- insert into results
insert into #results (client, initiated, initiated_by, thirty_day_fu_done, thirty_day_fu_done_by, sixty_day_fu_done, sixty_day_fu_done_by, ninety_day_fu_done, ninety_day_fu_done_by, onetwenty_day_fu_done, onetwenty_day_fu_done_by, terminated, terminated_by)
select init.client as client, init.initiated, init.initiated_by, tdf.thirty_day_fu_done, tdf.thirty_day_fu_done_by, sdf.sixty_day_fu_done, sdf.sixty_day_fu_done_by, ndf.ninety_day_fu_done, ndf.ninety_day_fu_done_by, otdf.onetwenty_day_fu_done, otdf.onetwenty_day_fu_done_by, t.terminated, t.terminated_by
from #initiated as init
left outer join #30day_fu as tdf on tdf.client = init.client
left outer join #60day_fu as sdf on sdf.client = init.client
left outer join #90day_fu as ndf on ndf.client = init.client
left outer join #120day_fu as otdf on otdf.client = init.client
left outer join #terminated as t on t.client = init.client
;

-- Return results to report
select *
from #results

-- Discard the working tables
drop table #initiated;
drop table #thirty_day_fu;
drop table #60day_fu;
drop table #90day_fu;
drop table #120day_fu;
drop table #terminated;
drop table #results

END
GO
