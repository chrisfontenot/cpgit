SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_recon_item_list] ( @recon_batch as int, @tran_type as varchar(2) = 'AD' ) as

-- =====================================================================================================
-- ==            Return the information needed to display the reconcilation batch list                ==
-- =====================================================================================================

-- ChangeLog
--   10/15/2003
--      Added ability to list check number from recon batch if not found in trust register.

-- Suppress intermediate result sets
set nocount on

-- Create a table to hold the intermediate results
create table #results ( recon_detail int null, trust_register int null, checknum BigInt null, item_date datetime null, item_amount money null, payee varchar(80) null, recon_date datetime null, message varchar(80) null, tran_type varchar(2) null)
create index ix1_results_1 on #results ( trust_register );

-- Include the details from the reconcilation batch
insert into #results ( recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type )
select	recon_detail, trust_register, checknum, null as item_date, amount as item_amount, '' as payee, recon_date as recon_date, message as message, d.tran_type
from	recon_details d with (nolock)
where	d.recon_batch = @recon_batch

-- Attempt to match the client payees where possible
update	#results
set		item_amount			= tr.amount,
		item_date			= tr.date_created,
		payee				= dbo.format_normal_name(n.prefix,n.first,n.middle,n.last,n.suffix),
		tran_type			= tr.tran_type
from	#results r
inner join registers_trust tr on r.trust_register = tr.trust_register
left outer join people p with (nolock) on tr.client = p.client and 1 = p.Relation
left outer join Names n with (nolock) on p.NameID = n.Name
where	tr.client is not null;

-- Do the same with the creditor checks
update	#results
set		item_amount			= tr.amount,
		item_date			= tr.date_created,
		payee				= coalesce(cr.creditor_name,'UNKNOWN CREDITOR ' + tr.creditor,'UNKNOWN CREDITOR'),
		tran_type			= tr.tran_type
from	#results r
inner join registers_trust tr on r.trust_register = tr.trust_register
left outer join creditors cr with (nolock) on tr.creditor = cr.creditor
where	tr.creditor is not null;

-- Set the bankwire transactions
update	#results
set		item_amount			= tr.amount,
		item_date			= tr.date_created,
		payee				= 'Bank Wire',
		tran_type			= tr.tran_type
from	#results r
inner join registers_trust tr on r.trust_register = tr.trust_register
where	tr.tran_type		= 'BW'

-- And the deposits
update	#results
set		item_amount			= tr.amount,
		item_date			= tr.date_created,
		payee				= 'DEPOSIT',
		tran_type			= tr.tran_type
from	#results r
inner join registers_trust tr on r.trust_register = tr.trust_register
where	tr.tran_type		in ('DP','RR','RF')

-- Set the other information
update	#results
set		item_amount			= tr.amount,
		item_date			= tr.date_created,
		payee				= 'Service Charge',
		tran_type			= tr.tran_type
from	#results r
inner join registers_trust tr on r.trust_register = tr.trust_register
where	tr.tran_type		= 'SC'

update	#results
set		item_amount			= tr.amount,
		item_date			= tr.date_created,
		payee				= 'Interest',
		tran_type			= tr.tran_type
from	#results r
inner join registers_trust tr on r.trust_register = tr.trust_register
where	tr.tran_type		= 'BI'

-- For normal checks, fold the client refunds into the disbursement transactions
if @tran_type in ('AD','MD','CM','CR','AR')
begin
	insert into #results (recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type)
	select	null as recon_detail,tr.trust_register,tr.checknum,tr.date_created,tr.amount,dbo.format_normal_name(n.prefix,n.first,n.middle,n.last,n.suffix),null as recon_date,null as message,tr.tran_type
	from	registers_trust tr with (nolock)
	left outer join #results r with (nolock) on tr.trust_register = r.trust_register
	left outer join people p with (nolock) on tr.client = p.client and 1 = p.Relation
	left outer join Names n with (nolock) on p.NameID = n.Name
	where	r.recon_detail is null
	and		tr.tran_type in ('CR','AR')
	and		ltrim(rtrim(isnull(tr.cleared,''))) = ''
	and		tr.client is not null

	insert into #results (recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type)
	select	null as recon_detail,tr.trust_register,tr.checknum,tr.date_created,tr.amount,coalesce(cr.creditor_name,'UNKNOWN CREDITOR ' + tr.creditor,'UNKNOWN CREDITOR'),null as recon_date,null as message,tr.tran_type
	from	registers_trust tr with (nolock)
	left outer join #results r with (nolock) on tr.trust_register = r.trust_register
	left outer join creditors cr with (nolock) on tr.creditor = cr.creditor
	where	r.recon_detail is null
	and		tr.tran_type in ('AD','MD','CM')
	and		ltrim(rtrim(isnull(tr.cleared,''))) = ''
	and		tr.creditor is not null
end

-- For bank wires, there is no creditor
if @tran_type = 'BW'
begin
	insert into #results (recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type)
	select	null as recon_detail,tr.trust_register,tr.checknum,tr.date_created,tr.amount,'BANK WIRE',null as recon_date,null as message,tr.tran_type
	from	registers_trust tr with (nolock)
	left outer join #results r with (nolock) on tr.trust_register = r.trust_register
	where	r.recon_detail is null
	and		tr.tran_type = 'BW'
	and		ltrim(rtrim(isnull(tr.cleared,''))) = ''
end

-- Deposits, creditor refunds, rpps rejects, epay rejects
if @tran_type in ('DP','RR','ER','RF')
begin
	insert into #results (recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type)
	select	null as recon_detail,tr.trust_register,tr.checknum,tr.date_created,tr.amount,'DEPOSITS',null as recon_date,null as message,tr.tran_type
	from	registers_trust tr with (nolock)
	left outer join #results r with (nolock) on tr.trust_register = r.trust_register
	where	r.recon_detail is null
	and		tr.tran_type in ('DP','RR','ER','RF')
	and		ltrim(rtrim(isnull(tr.cleared,''))) = ''
end

-- Service charges
if @tran_type = 'SC'
begin
	insert into #results (recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type)
	select	null as recon_detail,tr.trust_register,tr.checknum,tr.date_created,tr.amount,'SERVICE CHARGE',null as recon_date,null as message,tr.tran_type
	from	registers_trust tr with (nolock)
	left outer join #results r with (nolock) on tr.trust_register = r.trust_register
	where	r.recon_detail is null
	and		tr.tran_type = 'SC'
	and		ltrim(rtrim(isnull(tr.cleared,''))) = ''
end

-- Interest
if @tran_type = 'BI'
begin
	insert into #results (recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type)
	select	null as recon_detail,tr.trust_register,tr.checknum,tr.date_created,tr.amount,'INTEREST',null as recon_date,null as message,tr.tran_type
	from	registers_trust tr with (nolock)
	left outer join #results r with (nolock) on tr.trust_register = r.trust_register
	where	r.recon_detail is null
	and		tr.tran_type = 'BI'
	and		ltrim(rtrim(isnull(tr.cleared,''))) = ''
end

-- Return the results. Combine the special creditor checks together
if @tran_type in ('AD','MD','CM')
	select recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, 'AD' as tran_type
	from #results
	where tran_type in ('AD','MD','CM')
else
	select recon_detail, trust_register, checknum, item_date, item_amount, payee, recon_date, message, tran_type
	from #results
	where tran_type = @tran_type

drop table #results

return ( @@rowcount )
GO
