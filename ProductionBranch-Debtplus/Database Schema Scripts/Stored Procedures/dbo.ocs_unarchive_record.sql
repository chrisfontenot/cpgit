USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[ocs_unarchive_record]    Script Date: 06/18/2015 12:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Brandon Wilhite
-- Create date: 
-- Description:	Sets the Archive flag to 0 for a specific record
-- =============================================
ALTER PROCEDURE [dbo].[ocs_unarchive_record] 
	-- Add the parameters for the stored procedure here
	@OCSID int = -1,
	@Success bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin transaction

	begin try

		update OCS_Client
		set Archive = 0,
		ActiveFlag = 1
		where Id = @OCSID

		if @@ROWCOUNT = 1
		begin
			set @Success = 1
		end
				
        commit transaction

	end try

	begin catch
	  rollback transaction
	  set @Success = 0
	end catch

END


