USE [DebtPlus-11242014]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_loan]    Script Date: 11/25/2014 1:14:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[xpr_insert_housing_loan] 
( @PropertyID as Int,
@Loan1st2nd as Int,
@LoanDelinquencyMonths as Int,
@LoanDelinquencyAmount as money =0.00,
@LoanOriginationDate as Datetime,
@UseOrigLenderID as Bit = 1,
@CurrentLenderID as Int = null,
@OrigLenderID as Int = null,
@OrigLoanAmt as Money = 0,
@CurrentLoanBalanceAmt as Money = 0,
@IntakeSameAsCurrent as Bit = 1,
@IntakeDetailID as Int = null,
@CurrentDetailID as Int = null,
@MortgageClosingCosts as Money = 0,
@UseInReports as Bit = 0,
 @DsaDetailID as Int = null ) as
-- ==================================================================================================
-- ==         Insert an housing loan record                                                        ==
-- ==================================================================================================
set nocount on

insert into housing_loans 
([PropertyID],
[Loan1st2nd],[LoanDelinquencyMonths],[LoanDelinquencyAmount], [LoanOriginationDate], [UseOrigLenderID],[CurrentLenderID],[OrigLenderID],[OrigLoanAmt],[CurrentLoanBalanceAmt],[IntakeSameAsCurrent],[IntakeDetailID],[CurrentDetailID],[MortgageClosingCosts],[UseInReports], [DsaDetailID]) 
values (@PropertyID,@Loan1st2nd,@LoanDelinquencyMonths,@LoanDelinquencyAmount,@LoanOriginationDate,@UseOrigLenderID,@CurrentLenderID,@OrigLenderID,@OrigLoanAmt,@CurrentLoanBalanceAmt,@IntakeSameAsCurrent,@IntakeDetailID,@CurrentDetailID,@MortgageClosingCosts,@UseInReports, @DsaDetailID)
return ( scope_identity() )
