USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Erase_All_Tables]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Erase_All_Tables] ( @AreYouSure as varchar (30) = NULL ) as

-- ===================================================================================================
-- ==            Delete the contents of the tables                                                  ==
-- ===================================================================================================

-- ChangeLog
--   2/1/2002
--     Change to query the schema to find the table names in order to allow for additional table name changes.

if @AreYouSure is null
	set @AreYouSure = 'NO'

if @AreYouSure != 'destroy'
begin
	print 'This procedure is designed to totally and irrevocably erase the contents of the database tables.'
	print ''
	print 'This is a very dangerous procedure and should only be run during the initial conversion of the data.'
	print ''
	print 'If you meant to erase the contents, run the procedure with the command:'
	print ''
	print 'cmd_Erase_All_Tables ''destroy'''
	return ( 0 )
end

-- Truncate the tables
declare	@name		varchar(800)
declare	@stmt		varchar(800)

declare	table_names cursor for
	select	name
	from	sysobjects
	where	xtype = 'U'

	and	name not like 'input%'			-- INPUT_ tables are original codes and may not be changed
	and	name not like 'working%'		-- WORKING_ tables are temporary data and should not be changed

	and	name not in (				-- These are specific settings and are not changed from client to client
		'ach_reject_codes',
		'action_items',
		'active_status',
		'asset_ids',
		'budget_categories',
		'config',
		'creditor_types',
		'drop_reasons',
		'faq_questions',
		'faq_types',
		'financial_problems',
		'industries',
		'job_descriptions',
		'languages',
		'ledger_codes',
		'letter_fields',
		'letter_types',
		'menus',
		'messages',
		'policy_matrix_policy_types',
		'referred_by',
		'referred_by_types',
		'referred_to',
		'reports',
		'retention_actions',
		'retention_events',
		'rpps_reject_codes',
		'subjects',
		'tran_types',
		'workshop_content_types',
		'workshop_organization_types'
	)
	order by 1

open		table_names
fetch		table_names into @name

while @@fetch_status = 0
begin
	select		@stmt = 'trucate table ' + @name
	print		@stmt
	exec ( @stmt )
	fetch		table_names into @name
end

close		table_names
deallocate	table_names

-- Remove the information from the faq questions
update	faq_questions
set	count_mtd		= 0,
	count_ytd		= 0,
	count_prior_year	= 0;

-- Return the status to the caller
return ( 1 )
GO
