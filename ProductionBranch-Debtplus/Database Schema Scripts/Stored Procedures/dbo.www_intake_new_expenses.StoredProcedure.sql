USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_new_expenses]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_new_expenses] ( @intake_client as int, @intake_id as varchar(20) ) as

-- ==============================================================================================================
-- ==            Start the processing on the new expense information                                           ==
-- ==============================================================================================================

-- Set the proper indicator flags
set nocount	on
set xact_abort	on

-- Validate the client / ID pair
if not exists ( select	*
		from	intake_clients
		where	intake_client	= @intake_client
		and	intake_id	= @intake_id )
begin
	RaisError ('The client ID is not valid', 16, 1)
	return ( 0 )
end

-- Remove the budget information
delete
from	intake_budgets
where	intake_client	= @intake_client

return ( 1 )
GO
