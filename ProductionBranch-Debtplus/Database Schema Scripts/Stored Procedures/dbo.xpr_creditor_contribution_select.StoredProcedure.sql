USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_select]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_select] ( @record as int ) as

-- =====================================================================================================================
-- ==            Retrieve the contribution record from the database                                                   ==
-- =====================================================================================================================

-- ChangeLog
--   2/12/2002
--     Added "minimum_date" to limit the selection to today's clock setting rather than using the workstation's clock.

select	effective_date						as 'effective_date',
		fairshare_pct_check					as 'fairshare_pct_check',
		fairshare_pct_eft					as 'fairshare_pct_eft',
		creditor_type_eft					as 'creditor_type',
		creditor_type_eft					as 'creditor_type_eft',
		creditor_type_check					as 'creditor_type_check',
		date_updated						as 'date_updated',
		convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00')	as 'minimum_date'

from	creditor_contribution_pcts with (nolock)
where	creditor_contribution_pct = @record

return ( @@rowcount )
GO
