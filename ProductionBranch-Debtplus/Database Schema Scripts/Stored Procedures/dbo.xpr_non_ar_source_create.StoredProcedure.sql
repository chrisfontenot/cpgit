USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_non_ar_source_create]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_non_ar_source_create] ( @Description AS VarChar(80) = NULL ) AS

-- ===============================================================================================
-- ==            Insert the description as a non-AR source field                                ==
-- ===============================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

SET @Description	= ltrim(rtrim(@Description))
IF @Description = ''
	SET @Description = NULL

-- Do not attempt to insert a null description
IF @Description IS NULL
BEGIN
	RollBack Transaction
	RaisError (50020, 16, 1)
	Return ( 0 )
END

-- If the source is already present then use the indicated record
DECLARE	@record		INT
SELECT	@record		= non_ar_source
FROM	non_ar_sources WITH (NOLOCK)
WHERE	description	= @Description

IF @record IS NOT NULL	
	RETURN ( @record )

-- If the record is not defined then insert the new description into the database table
INSERT INTO non_ar_sources ([description]) VALUES (@Description)
RETURN ( SCOPE_IDENTITY() )
GO
