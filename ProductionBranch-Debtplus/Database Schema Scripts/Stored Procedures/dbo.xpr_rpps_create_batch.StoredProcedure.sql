USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_create_batch]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_create_batch] ( @FileNumber AS INT, @BillerID AS VarChar(10), @TraceNumber AS VarChar(15), @ServiceClass AS VarChar(3), @BillerName AS VarChar(22) = NULL ) AS

-- ============================================================================================================
-- ==            Create the batch record for the RPPS file                                                   ==
-- ============================================================================================================

-- Do not generate intermediate result sets
SET NOCOUNT ON

DECLARE @NewBatch	INT

INSERT INTO	rpps_batches	(rpps_file,	rpps_biller_id,	biller_name,		trace_number,	service_class)
VALUES				(@FileNumber,	@BillerID,	left(@BillerName,16),	@TraceNumber,	@ServiceClass)

SELECT @NewBatch = SCOPE_IDENTITY()

RETURN ( @NewBatch )
GO
