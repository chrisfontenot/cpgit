USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_Reject_Letters]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_ACH_Reject_Letters] ( @ACHFile AS INT ) AS
-- =================================================================================
-- ==          Generate a list of the rejection letters for ACH                   ==
-- =================================================================================
SET NOCOUNT ON

SELECT		d.client,
		e.letter_code
FROM		deposit_batch_details d
LEFT OUTER JOIN	ach_reject_codes e ON d.ach_authentication_code = e.ach_reject_code
WHERE		d.deposit_batch_id = @ACHFile
AND		d.ach_transaction_code IN ('27', '37')
AND		e.letter_code IS NOT NULL
AND		d.client > 0

RETURN ( @@rowcount )
GO
