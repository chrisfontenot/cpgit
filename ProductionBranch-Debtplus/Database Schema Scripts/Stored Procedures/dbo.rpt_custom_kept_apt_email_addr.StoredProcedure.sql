USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_kept_apt_email_addr]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_kept_apt_email_addr] ( @from_date datetime = null, @to_date datetime = null ) as

-- ================================================================================================
-- ==     custom report to show email addresses for kept appointments                            ==
-- ================================================================================================

set nocount on

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date	= convert(varchar(10), @from_date, 101),
	@to_date	= convert(varchar(10), @to_date, 101) + ' 23:59:59'

select	ca.client			as 'client',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)			as 'name',
	pe.address				as 'email',
	ca.start_time,
	ca.status,
	pn.first as first_name,
	pn.last as last_name
from	client_appointments ca with (nolock)
left outer join people p with (nolock) on ca.client = p.client and 1 = p.relation
left outer join Names pn with (nolock) on p.NameID = pn.name
left outer join EmailAddresses pe with (nolock) on p.EmailID = pe.Email
where	ca.start_time between @from_date and @to_date
and	ca.status in ('K','W')
order by 1

return ( @@rowcount )
GO
