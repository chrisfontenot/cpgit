USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_StudentLoanAccounts]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_StudentLoanAccounts]( @oID AS INT, @homeAddressID AS INT = null, @homePhoneID AS INT = null, @dependents AS INT = null, @household_people AS INT = null, @marital_status AS INT = null, @EmailID AS INT = null, @NameID AS INT = null, @race AS INT = null, @gender AS INT = null, @ethnicity AS INT = null, @dob AS DATETIME = null, @gross_income AS MONEY = 0, @contact_me AS BIT = 0, @receive_tips AS BIT = 0, @why_taking_modules AS VARCHAR(512) = '', @main_goal AS VARCHAR(512) = '', @company_id AS VARCHAR(512) = '', @financial_challenge AS VARCHAR(512) = '' ) as

-- ======================================================================================
-- ==           Insert a row into the StudentLoanAccounts table                        ==
-- ======================================================================================

insert into StudentLoanAccounts( [oID],[dob],[contact_me],[dependents],[household_people],[company_id],[EmailID],[ethnicity],[financial_challenge],[gender],[gross_income],[homeAddressID],[homePhoneID],[main_goal],[marital_status],[NameID],[race],[why_taking_modules],[receive_tips] ) values ( @oID, @dob,@contact_me,@dependents,@household_people,@company_id,@EmailID,@ethnicity,@financial_challenge,@gender,@gross_income,@homeAddressID,@homePhoneID,@main_goal,@marital_status,@NameID,@race,@why_taking_modules,@receive_tips )
return ( @oID )
GO
