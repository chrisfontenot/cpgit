USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_BudgetSummary]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_BudgetSummary] ( @BudgetID AS INT ) AS
-- ==========================================================================================
-- ==             Fetch the client for the indicated budget ID                             ==
-- ==========================================================================================

-- Prevent the temporary recordsets from being generated with the item count
SET NOCOUNT ON

DECLARE @Client    INT
SELECT	@client = client
FROM	budgets
WHERE	budget = @BudgetID

IF @Client IS NULL
	SELECT @Client = 0

-- ==========================================================================================
-- ==             Create a recordset for the resulting information                         ==
-- ==========================================================================================
CREATE TABLE #t_PrintClient_BudgetSummary(
	description	varchar(50),
	amount		money
)

-- ==========================================================================================
-- ==             Process the budget categories                                            ==
-- ==========================================================================================

-- Create the server cursor for the budget categories
DECLARE budget_cursor CURSOR FOR
	SELECT c.budget_category, c.description, c.heading, isnull(d.suggested_amount, d.client_amount)
	FROM	budget_categories c
	LEFT OUTER JOIN  budget_detail d ON c.budget_category = d.budget_category AND d.budget = @BudgetID
	ORDER BY c.budget_category

OPEN	budget_cursor

DECLARE	@Budget_Category	INT
DECLARE @Description		VARCHAR(80)
DECLARE @Heading		BIT
DECLARE @Amount			MONEY

DECLARE @Category_Label		VARCHAR(50)
DECLARE @Category_Amount	MONEY

SET @Category_Label = Null
SET @Category_Amount = 0

FETCH FROM budget_cursor INTO @Budget_Category, @Description, @Heading, @Amount

WHILE @@fetch_status = 0
BEGIN
	IF ISNULL(@Heading,0) > 0
	BEGIN
		IF @Category_Label IS NOT NULL
		BEGIN
			INSERT INTO #t_PrintClient_BudgetSummary (description,amount) VALUES (@Category_Label, @Category_Amount)
		END
		SET @Category_Label = @Description
		SET @Category_Amount = 0
	END
	ELSE
	BEGIN
		SELECT @Category_Amount = @Category_Amount + ISNULL(@Amount, 0)
	END
	FETCH FROM budget_cursor INTO @Budget_Category, @Description, @Heading, @Amount
END

IF @Category_Label IS NOT NULL
BEGIN
	INSERT INTO #t_PrintClient_BudgetSummary (description,amount) VALUES (@Category_Label, @Category_Amount)
END

CLOSE budget_cursor
DEALLOCATE budget_cursor

-- ==========================================================================================
-- ==             Include any additional categories that the user may supply               ==
-- ==========================================================================================

DECLARE @Other_Misc	MONEY

SELECT @Other_Misc = SUM(b.suggested_amount)
FROM   budget_categories_other o
INNER JOIN budget_detail b ON b.budget_category = o.budget_category AND b.budget = @BudgetID
WHERE  o.client = @Client

IF @Other_Misc IS NOT NULL
BEGIN
	INSERT INTO #t_PrintClient_BudgetSummary (description,amount) VALUES ('OTHER MISCELLANEOUS', @Other_Misc)
END

-- ==========================================================================================
-- ==             Return the results to the user                                           ==
-- ==========================================================================================

-- Restore the counter function now that we have the answer
SET NOCOUNT ON

SELECT * FROM #t_PrintClient_BudgetSummary

-- Discard the budget information working table
DROP TABLE #t_PrintClient_BudgetSummary

-- The result of the procedure is the grand total amount
RETURN ( @@rowcount )
GO
