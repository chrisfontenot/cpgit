USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_referred_to]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_referred_to] AS

-- ===================================================================================================
-- ==                Return the list of referral codes                                              ==
-- ===================================================================================================

SELECT	referred_to		as 'item_key',
	[description]		as 'description'
FROM	referred_to WITH ( NOLOCK )
order by 2

return ( @@rowcount )
GO
