if EXISTS(SELECT * FROM SYSOBJECTS WHERE NAME=N'xpr_debt_coding' AND TYPE='P')
	DROP PROCEDURE xpr_debt_coding
GO
CREATE PROCEDURE [dbo].[xpr_debt_coding] AS

-- ============================================================================================
-- ==              Find the list of missing creditors and account numbers                    ==
-- ============================================================================================
SELECT		c.client																 as 'client',
			isnull(c.held_in_trust,0)												 as 'held_in_trust',
			isnull(c.active_status,'CRE')											 as 'active_status',
			dbo.format_normal_name(default, pn.first, pn.middle, pn.last, pn.suffix) as 'name',
			isnull(cc.account_number,'')											 as 'account_number',
			isnull(cc.message,'')													 as 'message',
			cc.client_creditor														 as 'client_creditor',
			cc.creditor_name														 as 'creditor_name',
			cc.created_by															 as 'created_by',
			cc.date_created															 as 'date_created',
			coalesce(cc.start_date,c.start_date,c.date_created)						 as 'start_date',
			isnull(b.orig_balance,0)+isnull(b.orig_balance_adjustment,0)+isnull(b.total_interest,0)-isnull(b.total_payments,0) as 'balance'

FROM		clients c		WITH (NOLOCK)
INNER JOIN	client_creditor cc	WITH (NOLOCK) ON cc.client = c.client
INNER JOIN  client_creditor_balances b WITH (NOLOCK) ON cc.client_creditor_balance = b.client_creditor_balance
LEFT OUTER JOIN	people p		WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name

WHERE		cc.creditor_name IS NOT NULL
AND			cc.creditor IS NULL
AND			c.active_status IN ('PND', 'RDY', 'PRO', 'A', 'AR', 'EX')
AND			cc.creditor_name not like '%CUSHION%'

return ( @@rowcount )
GO
grant execute on xpr_debt_coding TO public AS dbo;
GO
