USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_CreditorAddress_I]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_CreditorAddress_I] ( @Creditor AS typ_creditor ) AS
-- ================================================================================================
-- ==                Return the creditor invoice address                                         ==
-- ================================================================================================
execute xpr_creditor_address_merged @Creditor, 'I'
GO
