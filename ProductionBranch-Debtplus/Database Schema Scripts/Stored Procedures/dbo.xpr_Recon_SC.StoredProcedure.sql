USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Recon_SC]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_Recon_SC] ( @Batch AS INT, @item_date as DateTime = NULL, @Amount as Money = 0, @bank as int = 0 ) AS

-- ========================================================================================================================
-- ==            Generate a bank service charge                                                                          ==
-- ========================================================================================================================

-- Do not tell the user the number of rows
SET NOCOUNT ON

if @item_date is null
	set @item_date = getdate()

-- Generate the register entry
insert into registers_trust	( tran_type,	date_created,	amount,		cleared,	reconciled_date, bank )
values				( 'SC',		@item_date,	@amount,	'R',		getdate(),	 @bank )

RETURN ( SCOPE_IDENTITY() )
GO
