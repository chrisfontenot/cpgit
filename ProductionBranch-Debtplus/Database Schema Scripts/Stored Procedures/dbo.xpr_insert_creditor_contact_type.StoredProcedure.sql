USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_creditor_contact_type]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_creditor_contact_type] (@description as varchar(50), @name as varchar(50) = null, @contact_type as varchar(10) = null) as
	insert into creditor_contact_types (description,name,contact_type) values (@description,@name,@contact_type)
	return (scope_identity())
GO
