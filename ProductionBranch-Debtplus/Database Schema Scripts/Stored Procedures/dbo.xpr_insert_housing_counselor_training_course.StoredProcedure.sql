USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_counselor_training_course]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_housing_counselor_training_course](@Counselor as int, @TrainingCourse as int, @TrainingDate as datetime) as
insert into housing_counselor_training_courses([Counselor],[TrainingCourse],[TrainingDate]) values (@Counselor,@TrainingCourse,@TrainingDate)
return ( scope_identity() )
GO
