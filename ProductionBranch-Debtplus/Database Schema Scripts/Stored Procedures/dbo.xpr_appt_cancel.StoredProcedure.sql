USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_cancel]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_cancel] ( @Appt AS INT ) AS

-- ====================================================================================================
-- ==            Cancel the indicated appointment                                                    ==
-- ====================================================================================================

-- ChangeLog
--   9/18/2002
--     Allowed the cancel of the workshop appointments to be performed by this procedure.

DECLARE	@Client		INT
DECLARE	@workshop	VarChar(80)
DECLARE	@workshop_id	int
DECLARE	@office		VarChar(80)
DECLARE	@start_time	datetime
DECLARE	@appt_type	VarChar(80)
DECLARE	@counselor	varchar(80)
DECLARE	@CancelMsg	Varchar(400)
DECLARE	@Status		VarChar(4)
DECLARE	@subject	varchar(80)

-- Start a transaction to ensure that no one cancels this appointment twice
BEGIN TRANSACTION

-- Generate the cancel message that is written to the system note
SELECT
	@workshop_id	= ca.workshop,
	@office		= ltrim(rtrim(o.name)),
	@counselor	= dbo.format_normal_name(default,con.first,default,con.last,default),
	@appt_type	= ltrim(rtrim(t.appt_name)),
	@start_time	= ca.start_time,
	@workshop	= ca.workshop,
	@Client		= ca.client,
	@Status		= ca.status

FROM client_appointments ca
LEFT OUTER JOIN appt_times a	WITH (NOLOCK) ON ca.appt_time = a.appt_time
LEFT OUTER JOIN counselors c	WITH (NOLOCK) ON ca.counselor = c.counselor
left outer join names con       WITH (NOLOCK) ON c.NameID     = con.name
LEFT OUTER JOIN offices o	WITH (NOLOCK) ON a.office     = o.office
LEFT OUTER JOIN appt_types t	WITH (NOLOCK) ON ca.appt_type = t.appt_type
WHERE ca.client_appointment = @Appt

-- Generate the cancel message
if @workshop_id is not null
begin
	SELECT	@CancelMsg = 'The workshop appointment (#' + convert(varchar,@Appt) + ') for ' + convert(varchar, @start_time, 100) + ' was cancelled.' + char(13) + char(10)
	select	@CancelMsg = @CancelMsg + 'It is a workshop of the ''' + isnull(@workshop, 'Unknown') + ''' type.' + char(13) + char(10)
	select	@subject   = 'Client workshop appointment was cancelled'
end else begin
	SELECT	@CancelMsg = 'The counselor appointment (#' + convert(varchar,@Appt) + ') for ' + convert(varchar, @start_time, 100) + ' was cancelled.' + char(13) + char(10)
	select	@CancelMsg = @CancelMsg + 'It is an appointment of type ''' + isnull(@appt_type, 'Unknown Type') + '''' + char(13) + char(10)
	select	@CancelMsg = @CancelMsg + 'The appointment was with ' + isnull(@counselor,'an unspecified counselor') + ' at the ' + isnull(@office,'unspecified') + ' office.'
	select	@subject   = 'Client counseling appointment was cancelled'
end

-- There must be a client for this appointment. It is not an error to have a missing client if we are
-- deleting the appointments from the system and just blindly deleting all references to the appointments
-- from the client table.

IF @Client IS NULL
BEGIN
	Rollback Transaction
--	RaisError (50045, 16, 1, @Appt)
	Return ( 0 )
END

-- Ensure that the appointment is pending
IF @Status != 'P'
BEGIN
	Rollback Transaction
	RaisError (50046, 16, 1, @Appt)
	Return ( 0 )
END

IF @CancelMsg IS NULL
	SELECT	@CancelMsg = 'The appointment or workshop (' + convert(varchar, @Appt) + ') was cancelled but its type could not be determined at the time the cancel was processed.'

-- Cancel the pending appointment
UPDATE	client_appointments
SET	status			= 'C',
	date_updated		= getdate(),
	appt_time		= NULL,
	workshop_people		= 0
WHERE	client_appointment 	= @Appt

-- If the appointment was not cancelled then bail
IF @@rowcount < 1
BEGIN
	Rollback Transaction
	RETURN ( 0 )
END

-- Insert the system note that the appointment was cancelled
INSERT INTO client_notes(client,	type,	is_text,	dont_edit,	dont_delete,	subject,	note)
VALUES			(@client,	3,	1,		1,		1,		@subject,	@CancelMsg)

-- Return success to the caller
Commit Transaction
RETURN ( 1 )
GO
