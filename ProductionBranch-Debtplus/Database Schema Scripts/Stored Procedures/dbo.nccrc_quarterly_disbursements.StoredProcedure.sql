USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nccrc_quarterly_disbursements]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[nccrc_quarterly_disbursements] ( @period_start as datetime = null, @period_end as datetime = null ) as

-- Specific creditors
declare	@line_32		varchar(80)	-- Counseling Fee
declare	@line_33		varchar(80)	-- Setup Fees
declare	@line_34		varchar(80)	-- Monthly Fees
declare	@line_35		varchar(80)	-- Seminar Fees
declare	@line_36		varchar(80)	-- Credit Report Fees
declare	@line_37		varchar(80)	-- Other Program Fees
declare	@line_38		varchar(80)	-- Other Service Fees
declare	@deduct_creditor	varchar(80)
declare	@total_clients		int

-- This does not change.
select	@deduct_creditor	= 'Z9999'

-- Change as needed (Leave as '' if there is no creditor)
select	@line_32		= ''					-- Counseling Fee
select	@line_33		= 'X0006'					-- Setup Fees
select	@line_34		= 'X0001'				-- Monthly Fees
select	@line_35		= ''					-- Seminar Fees
select	@line_36		= ''					-- Credit Report Fees
select	@line_37		= 'X0005'				-- Other Program Fees   
select	@line_38		= 'X0002,X0003'				-- Other Service Fees

-- Find last quarter if required
if (@period_end is null) and (@period_start is null)
begin
	select	@period_start	= dateadd(m, -2, getdate())
	select	@period_start	= convert(varchar, month(@period_start)) + '/01/' + convert(varchar, year(@period_start))

	while month(@period_start) not in (1, 4, 7, 10)
		select	@period_start = dateadd(m, -1, @period_start)

	select	@period_end	= dateadd(d, -1, dateadd(m, 3, @period_end))
end

select	@period_start	= convert(varchar(10), @period_start, 101) + ' 00:00:00',
	@period_end	= convert(varchar(10), @period_end, 101)   + ' 23:59:59';

-- Build a map of the creditors to determine where to place the answers
select	creditor, creditor_id, 26 as line, 'B' as fairshare_line, 'D' as disbursement_line, convert(money,0) as deduct, convert(money,0) as billed, convert(money,0) as disbursements, convert(money,0) as max_debit_amt
into	##map_creditors
from	creditors with (nolock);

create index temp_ix_map_creditors_1 on ##map_creditors ( creditor_id );
create index temp_ix_map_creditors_2 on ##map_creditors ( creditor );

-- For the specific creditors, update their locations. Change fairshare_line to C if want to show in grant column.
update	##map_creditors
set	line	= 15,					-- American Express
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('6300019402','6300019401')

update	##map_creditors
set	line	= 16,					-- Bank of Amercia
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('3000000003','9061799597','0004609083','0004609084','0031609086','0031609087','0031609088','0031609089','0031609090','0311001617',
'0311001622','0311001623','0311001624')

update	##map_creditors
set	line		= 17,				-- Capital One
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('2100001001','2020622581','0427060002')

update	##map_creditors
set	line		= 18,				-- Chase/Bank One
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0006609200','0090520050','0097539150','0257609028','0257609032',
'0257609040','0390101002','5200000002','0092236480','0090753463','9133357362')

update	##map_creditors
set	line		= 19,				-- Citigroup
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0043539050','0068539011','0068539016','0072539041','0072539042',
'0072539043','0072539044','0072539045','0072539046','0072539047','0072539051','0072539052',
'0072539053','0072539054','0072539055','0072539056','0072539057','0072539058','0072539059',
'0072539060','0072539061','0072539062','0072539063','0072539064','0072539065','0072539066',
'0072539067','0072539070','0072539071','0072539072','0072539074','0072539075','0072539076',
'0072539077','0072539078','0072539079','0072539080','0072539081','0072539082','0072539083',
'0072539084','0072539085','0072539086','0072539087','0072539088','0072539090','0088539007',
'0090004936','0090036384','0090264344','0090264439','0090264517','0090264758','0090264928',
'0090426822','0090551806','0090563121','0090617986','0090785651','0090948569','0090991466',
'0091003128','0091412279','0092075436','0092585680','0092648392','0092693767','0092693786',
'0134539123','0135609002','0135609003','0135609004','0135609005','0135609007','0135609008',
'0135609009','0135609010','0135609011','0135609065','0390101001','0440101001','0440101002',
'0540101005','0920101001','0920101003','0940101001','0980001001','0980001003','0980001004',
'0980001005','1310001001','1500000009','1580001003','1580001004','1580001005','1580001006',
'1580001007','1899901001','1899901002','1899901003','1899901004','2000060600','2000060601',
'2000060602','2000060603','2000060603','3188500001','7100000001','7200000002','7300000003',
'7400000004','7600000006','7700000008','7751800009','7900000009','9030799296','9500070399',
'9500071109','9500077999','9734697019','9902847009')



update	##map_creditors
set	line		= 20,				-- Discover
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('2189901001','0090074124')

update	##map_creditors
set	line		= 21,				-- GE Capital
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0063539020','0090005013','0090005074','0090005077','0090005078',
'0090005079','0090005086','0090005280','0090006001','0090185019','0090185040','0090018971','0090483228',
'0090520025','0090520030','0090520050','0090520060','0090520090','0090604412','0090628065','0090635646',
'0090888281','0090948570','0134539087','0134539090','0134539092','0134539100','0134539106','0134539109',
'0134539110','0134539112','0134539116','0134539119','0134539120','0134539121','0134539125','0134539128',
'0134539136','0134539139','0134539142','0134539152','0134539160','0134539162','0134539168','0134539170',
'0980001001','1030000350','1030000351','2589901001','8410215170','9000000137')

update	##map_creditors
set	line		= 22,				-- HSBC
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0001510000','0001540000','0001560000','0001650000','0001670000','0001680000',
'0001690000','0001710000','0001860000','0002030000','0002220000','0002230000','0002290000','0002330000',
'0002340000','0002350000','0002420000','0002440000','0002450000','0002456110','0002490000','0002550000',
'0002640000','0002750000','0002950000','0002960000','0003050000','0003100000','0003110000','0003420000',
'0003700000','0003710000','0004000000','0004060000','0004130000','0004220000','0004260000','0004310000',
'0004650000','0004990000','0005200000','0005310000','0005500000','0005890000','0005930000','0006070000',
'0007100000','0007200000','0007610000','0007670000','0007750000','0008010000','0008020000','0009910920',
'0009920020','0009920030','0009920040','0009920060','0009920100','0009920130','0009920140','0009920170',
'0009920180','0009920360','0009920370','0009920380','0009930010','0009930020','0009930030','0009930040',
'0009930050','0009930060','0009930070','0009930080','0009930090','0009930110','0009930150','0009930220',
'0009930260','0009930390','0009930400','0009930420','0009930430','0009930440','0009967000','0078599004',
'0078599012','0078599014','2000040001','3429901001','9051999675','9051999687','9051999776','9051999796',
'9051999960','9051999961','9052099010','9052099013','9052099015','9052099019','9052099024','9052099028',
'9052099128','9052099255','9052099257','9052099001')

update	##map_creditors
set	line		= 23,				-- MBNA
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('')

update	##map_creditors
set	line		= 24,				-- Providian
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('1400001001')

update	##map_creditors
set	line		= 25,				-- Wells Fargo
	fairshare_line	= 'B'
from	##map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0037609250','0037609053','0037609054','0037609055','0037609056','0730002384',
'3100000018')

-- Do not count the creditors that are listed in the fee amounts
update	##map_creditors
set	line		= 0
where	creditor	= isnull(@deduct_creditor, 'Z9999')

declare	@stmt		varchar(800)
if @line_32 <> ''
begin
	select @stmt = 'update ##map_creditors set line = -32 where creditor in (''' + replace(@line_32,',',''',''') + ''')'
	exec ( @stmt )
end

if @line_33 <> ''
begin
	select @stmt = 'update ##map_creditors set line = -33 where creditor in (''' + replace(@line_33,',',''',''') + ''')'
	exec ( @stmt )
end

if @line_34 <> ''
begin
	select @stmt = 'update ##map_creditors set line = -34 where creditor in (''' + replace(@line_34,',',''',''') + ''')'
	exec ( @stmt )
end

if @line_35 <> ''
begin
	select @stmt = 'update ##map_creditors set line = -35 where creditor in (''' + replace(@line_35,',',''',''') + ''')'
	exec ( @stmt )
end

if @line_36 <> ''
begin
	select @stmt = 'update ##map_creditors set line = -36 where creditor in (''' + replace(@line_36,',',''',''') + ''')'
	exec ( @stmt )
end

if @line_37 <> ''
begin
	select @stmt = 'update ##map_creditors set line = -37 where creditor in (''' + replace(@line_37,',',''',''') + ''')'
	exec ( @stmt )
end

if @line_38 <> ''
begin
	select @stmt = 'update ##map_creditors set line = -38 where creditor in (''' + replace(@line_38,',',''',''') + ''')'
	exec ( @stmt )
end

-- Find the disbursements in the period
select	creditor,
	sum(isnull(debit_amt,0) - isnull(credit_amt,0)) as disbursements,
	sum (case when creditor_type <> 'D' then 0
	          when tran_type in ('AD','BW','MD','CM') then fairshare_amt
	          else 0 - fairshare_amt end) as deduct,
	max(debit_amt) as max_debit_amt
into	#disbursements
from	registers_client_creditor rcc with (nolock)
where	rcc.tran_type in ('AD','BW','MD','CM','RR','VD','RF')
and	rcc.date_created between @period_start and @period_end
group by creditor;

update	##map_creditors
set	disbursements	= d.disbursements,
	deduct		= d.deduct,
	max_debit_amt	= d.max_debit_amt
from	##map_creditors m
inner join #disbursements d on m.creditor = d.creditor

drop table #disbursements

-- Find the amount received on invoices
select	creditor, sum(isnull(credit_amt,0)) as billed
into	#billed
from	registers_creditor rc with (nolock)
where	rc.tran_type = 'RC'
and	rc.date_created between @period_start and @period_end
group by creditor;

update	##map_creditors
set	billed		= d.billed
from	##map_creditors m
inner join #billed d on m.creditor = d.creditor

drop table #billed

-- Build a list of the disbursement information
create table #results (sheet varchar(80), location varchar(80), value varchar(80))

-- Find the total number of clients in the period
select	distinct client
into	#client_count
from	registers_client_creditor with (nolock)
where	tran_type in ('AD','BW','MD','CM','RR','VD','RF')
and	date_created between @period_start and @period_end;

select	@total_clients	= count(*)
from	#client_count;

drop table #client_count;

-- Counseling Fees
if isnull(@line_32,'') <> ''
begin

	declare	@line_32_total		money
	declare	@line_32_max		money
	declare	@line_32_count		int

	select	@line_32_total		= sum(disbursements),
		@line_32_max		= max(max_debit_amt)
	from	##map_creditors m
	where	line = -32;

	select	distinct client
	into	#line_32_count
	from	registers_client_creditor cc with (nolock)
	inner join ##map_creditors m on cc.creditor = m.creditor
	where	m.line = -32
	and	tran_type in ('AD','BW','MD','CM','RR','RF','VD')
	and	date_created between @period_start and @period_end;

	select	@line_32_count = count(*)
	from	#line_32_count;

	drop table #line_32_count;

	insert into #results (sheet, location, value) values ('Sheet1', 'B32', convert(varchar,@line_32_total));
	insert into #results (sheet, location, value) values ('Sheet1', 'C32', convert(varchar,@line_32_count));
	insert into #results (sheet, location, value) select  'Sheet1', 'D32', convert(varchar,round(convert(float,@line_32_count)/convert(float,@total_clients) * 100.0, 2));
	insert into #results (sheet, location, value) values ('Sheet1', 'E32', convert(varchar,@line_32_max));
end

-- Setup Fees
if isnull(@line_33,'') <> ''
begin
	declare	@line_33_total		money
	declare	@line_33_max		money
	declare	@line_33_count		int

	select	@line_33_total		= sum(disbursements),
		@line_33_max		= max(max_debit_amt)
	from	##map_creditors m
	where	line = -33;

	select	distinct client
	into	#line_33_count
	from	registers_client_creditor cc with (nolock)
	inner join ##map_creditors m on cc.creditor = m.creditor
	where	m.line = -33
	and	tran_type in ('AD','BW','MD','CM','RR','RF','VD')
	and	date_created between @period_start and @period_end;

	select	@line_33_count = count(*)
	from	#line_33_count;

	drop table #line_33_count;

	insert into #results (sheet, location, value) values ('Sheet1', 'B33', convert(varchar,@line_33_total));
	insert into #results (sheet, location, value) values ('Sheet1', 'C33', convert(varchar,@line_33_count));
	insert into #results (sheet, location, value) select  'Sheet1', 'D33', convert(varchar,round(convert(float,@line_33_count)/convert(float,@total_clients) * 100.0, 2));
	insert into #results (sheet, location, value) values ('Sheet1', 'E33', convert(varchar,@line_33_max));
end

-- Monthly Fees
if isnull(@line_34,'') <> ''
begin
	declare	@line_34_total		money
	declare	@line_34_max		money
	declare	@line_34_count		int

	select	@line_34_total		= sum(disbursements),
		@line_34_max		= max(max_debit_amt)
	from	##map_creditors m
	where	line = -34;

	select	distinct client
	into	#line_34_count
	from	registers_client_creditor cc with (nolock)
	inner join ##map_creditors m on cc.creditor = m.creditor
	where	m.line = -34
	and	tran_type in ('AD','BW','MD','CM','RR','RF','VD')
	and	date_created between @period_start and @period_end;

	select	@line_34_count = count(*)
	from	#line_34_count;

	drop table #line_34_count;

	insert into #results (sheet, location, value) values ('Sheet1', 'B34', convert(varchar,@line_34_total));
	insert into #results (sheet, location, value) values ('Sheet1', 'C34', convert(varchar,@line_34_count));
	insert into #results (sheet, location, value) select  'Sheet1', 'D34', convert(varchar,round(convert(float,@line_34_count)/convert(float,@total_clients) * 100.0, 2));
	insert into #results (sheet, location, value) values ('Sheet1', 'E34', convert(varchar,@line_34_max));
end

-- Seminar Fees
if isnull(@line_35,'') <> ''
begin
	declare	@line_35_total		money
	declare	@line_35_max		money
	declare	@line_35_count		int

	select	@line_35_total		= sum(disbursements),
		@line_35_max		= max(max_debit_amt)
	from	##map_creditors m
	where	line = -35;

	select	distinct client
	into	#line_35_count
	from	registers_client_creditor cc with (nolock)
	inner join ##map_creditors m on cc.creditor = m.creditor
	where	m.line = -35
	and	tran_type in ('AD','BW','MD','CM','RR','RF','VD')
	and	date_created between @period_start and @period_end;

	select	@line_35_count = count(*)
	from	#line_35_count;

	drop table #line_35_count;

	insert into #results (sheet, location, value) values ('Sheet1', 'B35', convert(varchar,@line_35_total));
	insert into #results (sheet, location, value) values ('Sheet1', 'C35', convert(varchar,@line_35_count));
	insert into #results (sheet, location, value) select  'Sheet1', 'D35', convert(varchar,round(convert(float,@line_35_count)/convert(float,@total_clients) * 100.0, 2));
	insert into #results (sheet, location, value) values ('Sheet1', 'E35', convert(varchar,@line_35_max));
end

-- Credit Report Fees
if isnull(@line_36,'') <> ''
begin
	declare	@line_36_total		money
	declare	@line_36_max		money
	declare	@line_36_count		int

	select	@line_36_total		= sum(disbursements),
		@line_36_max		= max(max_debit_amt)
	from	##map_creditors m
	where	line = -36;

	select	distinct client
	into	#line_36_count
	from	registers_client_creditor cc with (nolock)
	inner join ##map_creditors m on cc.creditor = m.creditor
	where	m.line = -36
	and	tran_type in ('AD','BW','MD','CM','RR','RF','VD')
	and	date_created between @period_start and @period_end;

	select	@line_36_count = count(*)
	from	#line_36_count;

	drop table #line_36_count;

	insert into #results (sheet, location, value) values ('Sheet1', 'B36', convert(varchar,@line_36_total));
	insert into #results (sheet, location, value) values ('Sheet1', 'C36', convert(varchar,@line_36_count));
	insert into #results (sheet, location, value) select  'Sheet1', 'D36', convert(varchar,round(convert(float,@line_36_count)/convert(float,@total_clients) * 100.0, 2));
	insert into #results (sheet, location, value) values ('Sheet1', 'E36', convert(varchar,@line_36_max));
end

-- Other Program Fees
if isnull(@line_37,'') <> ''
begin
	declare	@line_37_total		money
	declare	@line_37_max		money
	declare	@line_37_count		int

	select	@line_37_total		= sum(disbursements),
		@line_37_max		= max(max_debit_amt)
	from	##map_creditors m
	where	line = -37;

	select	distinct client
	into	#line_37_count
	from	registers_client_creditor cc with (nolock)
	inner join ##map_creditors m on cc.creditor = m.creditor
	where	m.line = -37
	and	tran_type in ('AD','BW','MD','CM','RR','RF','VD')
	and	date_created between @period_start and @period_end;

	select	@line_37_count = count(*)
	from	#line_37_count;

	drop table #line_37_count;

	insert into #results (sheet, location, value) values ('Sheet1', 'B37', convert(varchar,@line_37_total));
	insert into #results (sheet, location, value) values ('Sheet1', 'C37', convert(varchar,@line_37_count));
	insert into #results (sheet, location, value) select  'Sheet1', 'D37', convert(varchar,round(convert(float,@line_37_count)/convert(float,@total_clients) * 100.0, 2));
	insert into #results (sheet, location, value) values ('Sheet1', 'E37', convert(varchar,@line_37_max));
end

-- Other Service Fees
if isnull(@line_38,'') <> ''
begin
	declare	@line_38_total		money
	declare	@line_38_max		money
	declare	@line_38_count		int

	select	@line_38_total		= sum(disbursements),
		@line_38_max		= max(max_debit_amt)
	from	##map_creditors m
	where	line = -38;

	select	distinct client
	into	#line_38_count
	from	registers_client_creditor cc with (nolock)
	inner join ##map_creditors m on cc.creditor = m.creditor
	where	m.line = -38
	and	tran_type in ('AD','BW','MD','CM','RR','RF','VD')
	and	date_created between @period_start and @period_end;

	select	@line_38_count = count(*)
	from	#line_38_count;

	drop table #line_38_count;

	insert into #results (sheet, location, value) values ('Sheet1', 'B38', convert(varchar,@line_38_total));
	insert into #results (sheet, location, value) values ('Sheet1', 'C38', convert(varchar,@line_38_count));
	insert into #results (sheet, location, value) select  'Sheet1', 'D38', convert(varchar,round(convert(float,@line_38_count)/convert(float,@total_clients) * 100.0, 2));
	insert into #results (sheet, location, value) values ('Sheet1', 'E38', convert(varchar,@line_38_max));
end


-- Return the results to the sheet
insert into #results (sheet, location, value)
select	'Sheet1' as sheet,
	fairshare_line + convert(varchar, line) as location,
	convert(varchar, sum(deduct + billed)) as value
from	##map_creditors
where	line > 0
group by fairshare_line, line;

insert into #results (sheet, location, value)
select	'Sheet1' as sheet,
	disbursement_line + convert(varchar, line) as location,
	convert(varchar, sum(disbursements)) as value
from	##map_creditors
where	line > 0
group by disbursement_line, line;

drop table ##map_creditors

-- Return the results to the user
select sheet, location, value from #results
drop table #results

return ( 1 )
GO
