SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_ClientPackage_SC_disbursements] ( @Client AS INT ) AS
-- ===================================================================================
-- ==             Fetch the creditor disbursement information                       ==
-- ===================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SELECT		d.creditor	as 'creditor',
		cr.creditor_name					as 'creditor_name',
		coalesce (d.account_number, cc.account_number, '')	as 'account_number',
		d.date_created						as 'payment_date',

		tr.checknum						as 'checknum',
		d.debit_amt						as 'amount',
		d.client_creditor				as 'client_creditor'

FROM		registers_client_creditor d	WITH (NOLOCK)
INNER JOIN	client_creditor cc		WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
INNER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
INNER JOIN	registers_trust tr		WITH (NOLOCK) ON d.trust_register = tr.trust_register
WHERE		d.client = @Client
AND			d.tran_type in ('AD', 'BW', 'MD', 'CM')
ORDER BY	cr.type, cr.creditor_id, d.creditor, d.client_creditor, d.date_created	

RETURN ( @@rowcount )
GO
