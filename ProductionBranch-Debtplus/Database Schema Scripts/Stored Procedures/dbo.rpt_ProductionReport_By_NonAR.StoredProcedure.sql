USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ProductionReport_By_NonAR]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_ProductionReport_By_NonAR] ( @FromDate as datetime = null, @ToDate as datetime = null ) as

-- ====================================================================================================
-- ==            Return the information for the production report non-ar details                     ==
-- ====================================================================================================

set nocount on

-- Correct the date range
if @ToDate is null
	select	@ToDate = getdate()

if @FromDate is null
	select	@FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

select	isnull(ar.src_ledger_account, ar.dst_ledger_account)	as 'ledger_code',
	d.description						as 'description',
	sum(isnull(ar.credit_amt,0))				as 'credit_amt',
	sum(isnull(ar.debit_amt,0))				as 'debit_amt'
from	registers_non_ar ar with (nolock)
left outer join ledger_codes d with (nolock) on isnull(ar.src_ledger_account, ar.dst_ledger_account) = d.ledger_code
where	ar.date_created between @FromDate and @ToDate
and	ar.tran_type != 'DX'
group by isnull(ar.src_ledger_account,ar.dst_ledger_account), d.description

order by 2, 1

return ( @@rowcount )
GO
