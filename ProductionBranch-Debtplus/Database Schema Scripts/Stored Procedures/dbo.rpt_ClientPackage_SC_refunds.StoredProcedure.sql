SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_ClientPackage_SC_refunds] ( @client as int ) as

-- =====================================================================================
-- ==            Retrieve information about client refunds for the SC package         ==
-- =====================================================================================

select	tr.checknum,
	d.date_created,
	d.debit_amt

from	registers_client d with (nolock)
left outer join registers_trust tr with (nolock) on d.trust_register = tr.trust_register
where	d.client = @client
and	d.tran_type = 'CR'
order by d.date_created

return ( @@rowcount )
GO
