SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Reconcile_recon_items] ( @trust_register as int ) as

-- ================================================================================================
-- ==            Given an item in the reconcilation batch, return the list of items              ==
-- ==            which have been reconciled in the same batch. It is easier to leverage          ==
-- ==            this logic into a more general case than to simply use the sequence id.         ==
-- ================================================================================================

-- ChangeLog
--   3/22/2002
--     Added RR to the list of deposit transaction types

-- Determine the sequence number from the first matched transaction
declare	@sequence_number	varchar(80)
select	@sequence_number = sequence_number
from	registers_trust
where	trust_register	= @trust_register

-- Return the list of checks and deposits which are marked with the proper sequence identifier
select
		tr.trust_register		as 'trust_register',
		tr.tran_type			as 'tran_type',
		tr.checknum				as 'checknum',
		tr.date_created			as 'date_created',
		tr.cleared				as 'cleared',
		tr.reconciled_date		as 'reconciled_date',

		case
			when tr.tran_type in ('DP', 'RF', 'BI', 'RR') then 0
			when tr.tran_type = 'VF' then tr.amount
			else tr.amount
		end				as 'debit_amt',

		case
			when tr.tran_type in ('DP', 'RF', 'BI', 'RR') then tr.amount
			when tr.tran_type = 'VF' then 0
			else 0
		end				as 'credit_amt',

		case
			when tr.client is not null then dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)
			when tr.creditor is not null then cr.creditor_name
			else isnull(tt.description,'')
		end				as 'payee'

from		registers_trust tr	with (nolock)
left outer join	people p		with (nolock) on tr.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join creditors cr		with (nolock) on tr.creditor = cr.creditor
left outer join	tran_types tt		with (nolock) on tr.tran_type = tt.tran_type

where		tr.sequence_number = @sequence_number

order by	checknum, date_created
GO
