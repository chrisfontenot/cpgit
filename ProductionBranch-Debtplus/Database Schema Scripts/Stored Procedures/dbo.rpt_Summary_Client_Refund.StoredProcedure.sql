SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Summary_Client_Refund] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for client refunds                                       ==
-- ======================================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SELECT @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the transactions
SELECT		d.client					as 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		d.debit_amt					as 'gross',
		t.checknum					as 'checknum',
		d.message					as 'reason',
		d.date_created					as 'item_date',
		dbo.format_counselor_name(d.created_by)					as 'counselor'

FROM		registers_client d
LEFT OUTER JOIN	registers_trust t ON d.trust_register = t.trust_register
LEFT OUTER JOIN	people p on d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		d.tran_type IN ('CR','AR')
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created

RETURN ( @@rowcount )
GO
