USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_proposal_file_header]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_proposal_file_header] ( @bank as int = null ) as

-- ==============================================================================================
-- ==            Retrieve the information for the EPAY proposal file                           ==
-- ==============================================================================================

-- ChangeLog
--   12/14/2005
--      Removed "canned" 59 from the immediate origin field. It now must be specified in the banks table.

-- Suppress intermediate results
set nocount on

-- Find the appropriate bank if one is not specified
if @bank is null
	select	@bank	= min(bank)
	from	banks
	where	type	= 'E'
	and	[default] = 1

if @bank is null
	select	@bank = min(bank)
	from	banks
	where	type	= 'E'

if @bank is null
	select	@bank = 5

-- Return the information for the requested operation
select	prefix_line									as 'prefix',
	suffix_line									as 'suffix',
	'DSPHEADER'									as 'record_type',
	'CCCS'										as 'file_type',
	''										as 'function',
	getdate()									as 'file_creation_date',
	''										as 'file_control_id',
	isnull(transaction_number,0)							as 'transaction_sequence',
	immediate_origin								as 'originator_id',
	ach_origin_dfi									as 'origin_dfi'

from	banks with (nolock)
where	bank		= @bank

return ( @@rowcount )
GO
