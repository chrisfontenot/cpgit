USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_DailyDeposits]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_DailyDeposits] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL, @counselor as int = null ) AS
-- =============================================================================================
-- ==                Fetch the information for the daily deposits report                      ==
-- =============================================================================================

-- ChangeLog
--    9/24/2002
--       Added selection by counselor
--    1/25/2003
--       Added bank name

-- Suppress intermediate results
set nocount on

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert (datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert (datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

if @counselor is null
begin
	SELECT
		tr.bank								as 'bank',
		b.description							as 'bank_name',
		d.trust_register						as 'batch_id',
		d.client							as 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		isnull(t.description,'unknown type ' + d.tran_subtype)		as 'type',
		d.credit_amt							as 'amount',
		d.date_created							as 'date',
		d.message							as 'reference'
	
	FROM		registers_client d
	LEFT OUTER JOIN	tran_types t WITH (NOLOCK) ON d.tran_subtype = t.tran_type
	LEFT OUTER JOIN	people p WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON d.trust_register = tr.trust_register
	LEFT OUTER JOIN banks b WITH (NOLOCK) ON tr.bank = b.bank
	
	WHERE		d.date_created BETWEEN @FromDate AND @ToDate
	and		d.tran_type = 'DP'
	
	ORDER BY	d.date_created, d.tran_subtype, d.client

end else begin

	if not exists (select * from counselors where counselor = @counselor)
		select	@counselor = 0

	if @counselor <= 0
	begin
		SELECT
			tr.bank								as 'bank',
			b.description							as 'bank_name',
			d.trust_register						as 'batch_id',
			d.client							as 'client',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			isnull(t.description,'unknown type ' + d.tran_subtype)		as 'type',
			d.credit_amt							as 'amount',
			d.date_created							as 'date',
			d.message							as 'reference'

		FROM		registers_client d
		LEFT OUTER JOIN	tran_types t WITH (NOLOCK) ON d.tran_subtype = t.tran_type
		LEFT OUTER JOIN	people p WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
		left outer join names pn with (nolock) on p.nameid = pn.name
		INNER JOIN	clients c WITH (NOLOCK) ON d.client = c.client
		LEFT OUTER JOIN counselors co WITH (NOLOCK) ON c.counselor = co.counselor
		LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON d.trust_register = tr.trust_register
		LEFT OUTER JOIN banks b WITH (NOLOCK) ON tr.bank = b.bank

		WHERE		d.date_created BETWEEN @FromDate AND @ToDate
		and		d.tran_type = 'DP'
		AND		co.counselor is null

		ORDER BY	d.date_created, d.tran_subtype, d.client

	end else begin

		SELECT
			tr.bank								as 'bank',
			b.description							as 'bank_name',
			d.trust_register						as 'batch_id',
			d.client							as 'client',
			dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
			isnull(t.description,'unknown type ' + d.tran_subtype)		as 'type',
			d.credit_amt							as 'amount',
			d.date_created							as 'date',
			d.message							as 'reference'

		FROM		registers_client d
		LEFT OUTER JOIN	tran_types t WITH (NOLOCK) ON d.tran_subtype = t.tran_type
		LEFT OUTER JOIN	people p WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
		left outer join names pn with (nolock) on p.nameid = pn.name
		INNER JOIN	clients c WITH (NOLOCK) ON d.client = c.client
		LEFT OUTER JOIN registers_trust tr WITH (NOLOCK) ON d.trust_register = tr.trust_register
		LEFT OUTER JOIN banks b WITH (NOLOCK) ON tr.bank = b.bank

		WHERE		d.date_created BETWEEN @FromDate AND @ToDate
		and		d.tran_type = 'DP'
		AND		c.counselor = @counselor

		ORDER BY	d.date_created, d.tran_subtype, d.client
	end
end

RETURN ( @@rowcount )
GO
