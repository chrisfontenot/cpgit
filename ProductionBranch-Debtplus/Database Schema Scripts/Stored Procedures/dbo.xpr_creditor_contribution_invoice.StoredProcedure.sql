USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_invoice]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_invoice] ( @invoice_register AS INT = NULL, @amount AS Money = 0, @message AS typ_message = NULL ) AS

-- ======================================================================================================
-- ==             Pay this invoice the indicated amount                                                ==
-- ======================================================================================================

-- ChangeLog
--   2/20/2002
--     Changed to xpr_creditor_contribution_invoice to make similar to the other items in the database.
--   10/9/2002
--     Updated contribtuion statistic in creditor table.

SET NOCOUNT ON

-- Fetch the creditor from the invoice
DECLARE	@creditor	typ_creditor

begin transaction

SELECT	@creditor		= creditor
FROM	registers_invoices
WHERE	invoice_register	= @invoice_register

-- There must be a creditor
if @creditor is null
begin
	RaisError ('Unable to determine creditor from the invoice %d', 16, 1, @invoice_register)
	rollback transaction
	return ( 0 )
end

-- Ensure that the payment amount is valid
if @amount < 0
begin
	RaisError ('The payment amount is not valid', 16, 1)
	rollback transaction
	return ( 0 )
end

-- Clean up the reference/message field
if @message is not null
begin
	set @message = ltrim(rtrim(@message))
	if @message = ''
		set @message = null

	if len(@message) > 50
		select	@message = left(@message, 50)
end

-- Process the payment to the invoice once it is located in the system
IF @creditor IS NOT NULL
BEGIN

	-- Add the transaction to the creditor log
	insert into registers_creditor	(tran_type,	creditor,	credit_amt,	invoice_register,	message)
	values				('RC',		@creditor,	@amount,	@invoice_register,	@message)

	-- Account for the contribution in the creditor statistics
	update	creditors
	set	contrib_mtd_received	= isnull(contrib_mtd_received,0) + @amount
	where	creditor		= @creditor

	-- Adjust the invoice to indicate the payment
	UPDATE	registers_invoices
	SET	pmt_amount		= isnull(pmt_amount,0) + @amount,
		pmt_date		= getdate()
	WHERE	invoice_register	= @invoice_register
END

commit transaction

-- Return success to the caller
RETURN ( 1 )
GO
