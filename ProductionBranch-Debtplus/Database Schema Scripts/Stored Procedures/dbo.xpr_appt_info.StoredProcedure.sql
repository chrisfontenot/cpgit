USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_info]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_info] ( @client_appointment as typ_key ) AS

-- Suppress intermediate results
set nocount on

declare	@counselor	typ_key
declare	@office		typ_key
select	@counselor	= counselor,
	@office		= office
from	counselors
where	[person] 	= suser_sname()

select	coalesce(counselor, @counselor, 1)		as 'counselor',
	coalesce(office, @office, 1)			as 'office',
	isnull(referred_to, 1)				as 'referred_to',
	isnull(appt_type, 1)				as 'appt_type',
	isnull(credit, 0)				as 'credit',
	isnull(housing, 0)				as 'housing'
from	client_appointments
where	client_appointment = @client_appointment

return ( @@rowcount )
GO
