USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_create_debt]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_create_debt] ( @client AS typ_client, @Creditor AS typ_creditor = NULL, @Creditor_Name AS typ_description = NULL, @Account_Number AS varchar(50) = NULL ) AS

-- ===================================================================================================
-- ==                     Create the debt record for the system                                     ==
-- ===================================================================================================

-- ChangeLog
--   1/19/2001
--     Corrected the debt creation to remove fields that were dropped
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   11/8/2003
--     Added additional check to ensure that the debt id, when specified, is unique.
--   10/5/2007
--     Changed the proposal status from 0 to 1 on the debt creation.
--   6/24/2008
--     General cleanup. Nothing significant.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SET NOCOUNT ON

-- If the creditor is a fee account then use the client ID as the account number
DECLARE	@agency_account		bit
DECLARE	@priority		int
DECLARE	@proposals		int
DECLARE	@creditor_id		int

-- Program administration fee creditor
DECLARE @paf_creditor		varchar(10)
select	@paf_creditor = paf_creditor
from	config;

if @creditor is not null
begin
	SELECT	@agency_account	= ccl.agency_account,
		@priority	= cr.usual_priority,
		@creditor_id	= cr.creditor_id
	FROM	creditors cr with (nolock)
	LEFT OUTER JOIN creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
	WHERE	creditor	= @creditor

	-- If this is the fee account then ensure that there is not a pending fee account
	if isnull(@paf_creditor,'**NONE**') = @creditor
	BEGIN
		IF exists (	SELECT	*
				FROM	client_creditor cc
				WHERE	cc.client	= @client
				AND	cc.creditor	= @paf_creditor )
		BEGIN
			RaisError (50051, 16, 1, @creditor)
			Return ( 0 )
		END
	END
end

IF @priority IS NULL
	SELECT @priority = 9

IF isnull(@agency_account,0) > 0
	SELECT @account_number = dbo.format_client_id ( @client )

-- Create the debt record (the other fields should be defaulted to the proper values.)
DECLARE	@client_creditor	int

-- Since the debt and balanace records must match, start a transaction now.
BEGIN TRANSACTION
SET XACT_ABORT ON

-- Create the debt record
INSERT INTO client_creditor (	client,		creditor,		client_creditor_balance,	creditor_name,	account_number,		priority,	line_number,	created_by,	date_created,	dmp_interest, send_bal_verify	)
VALUES			    (	@client,	@creditor,		0,				@creditor_name,	@account_number,	@priority,	0,		suser_sname(),	getdate(),	null, 2		)

-- The identity information is the debt record.
SELECT	@client_creditor = SCOPE_IDENTITY()

-- Create the balance record
declare	@client_creditor_balance	int
insert into client_creditor_balances (client_creditor,orig_balance,orig_balance_adjustment,total_interest,total_payments) values (@client_creditor,0,0,0,0)
select	@client_creditor_balance = SCOPE_IDENTITY()

-- Update the balance record pointer
update	client_creditor
set		client_creditor_balance	= @client_creditor_balance
where	client_creditor			= @client_creditor

-- Create a proposal for the new debt record
declare	@client_creditor_proposal	int
execute @client_creditor_proposal = xpr_insert_client_creditor_proposal @client_creditor

if @client_creditor_proposal <= 0
begin
	rollback transaction
	return ( 0 )
end	

-- Commit the transaction block
COMMIT TRANSACTION

-- Return the information for the newly created debt record
RETURN ( @client_creditor )
GO
