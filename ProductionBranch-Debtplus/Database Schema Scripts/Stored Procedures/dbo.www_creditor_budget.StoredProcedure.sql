USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_budget]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_budget] ( @creditor as varchar(10), @client_creditor as int ) AS
declare	@client		int
select	@client		= client
from	client_creditor
where	creditor	= @creditor
and	client_creditor	= @client_creditor
Execute rpt_BudgetSummary @client
return ( 1 )
GO
