USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_client_last_deposit]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[UPDATE_client_last_deposit] as

-- Find the deposits for all of the clients
select client, convert(datetime, convert(varchar(10), date_created, 101)) as deposit_date, sum(credit_amt) as deposit_amount
into #deposits
from registers_client
where tran_type = 'DP'
group by client, convert(datetime, convert(varchar(10), date_created, 101))

-- Find the first deposit item
select	client, min(deposit_date) as deposit_date
into	#first_deposits
from	#deposits
group by client

-- Find the last deposit item
select	client, max(deposit_date) as deposit_date
into	#last_deposits
from	#deposits
group by client

-- Delete the items that are not the last
delete	#deposits
from	#deposits d
left outer join #last_deposits l on d.client = l.client and d.deposit_date = l.deposit_date
where	l.client is null

-- Clear the last deposit information
update	clients
set		first_deposit_date	= null,
		last_deposit_date	= null,
		last_deposit_amount	= 0

-- Correct the first deposit information
update	clients
set		first_deposit_date	= d.deposit_date
from	clients c
inner join #first_deposits d on c.client = d.client

-- Correct the last deposit information
update	clients
set		last_deposit_date	= d.deposit_date,
		last_deposit_amount	= d.deposit_amount
from	clients c
inner join #deposits d on c.client = d.client

drop table #deposits
drop table #last_deposits
drop table #first_deposits

select top 100 * from clients
GO
