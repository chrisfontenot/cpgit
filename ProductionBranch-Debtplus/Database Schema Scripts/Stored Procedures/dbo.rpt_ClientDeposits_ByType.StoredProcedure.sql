USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ClientDeposits_ByType]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ClientDeposits_ByType] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) AS
-- =============================================================================================
-- ==                      Retreive the client deposits by type of deposit                    ==
-- =============================================================================================

-- ChangeLog
--   02/01/2002
--     Corrected ambiguous column 'date_created'.

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

SELECT		d.client				as 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		d.date_created				as 'deposit_date',
		d.message				as 'reference',
		isnull(t.description,'Other')		as 'deposit_type',
		d.credit_amt				as 'amount'
FROM		registers_client d WITH (NOLOCK)
LEFT OUTER JOIN	people p WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	tran_types t WITH (NOLOCK) ON d.tran_subtype = t.tran_type
WHERE		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	5, 3, 1 -- type, date, client

RETURN ( @@rowcount )
GO
