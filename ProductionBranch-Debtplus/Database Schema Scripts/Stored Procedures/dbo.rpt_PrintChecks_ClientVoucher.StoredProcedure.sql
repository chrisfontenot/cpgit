USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintChecks_ClientVoucher]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintChecks_ClientVoucher] ( @trust_register AS int ) AS
-- ====================================================================================
-- ==               Retrieve the information for the client voucher                  ==
-- ====================================================================================

-- ChangeLog
--   11/18/2002
--     Added the message text with the check voucher contents

-- Return a message set as the result for clients
declare	@client		int
select	@client	= [client]
from	registers_trust tr with (nolock)
where	tr.trust_register	= @trust_register;

-- Find the message for the client refund from the registers_client table
declare	@msg		varchar(80)
select	@msg	= message
from	registers_client rc with (nolock)
where	rc.[tran_type]  = 'CR'
and		rc.[client]		= @client
and		trust_register  = @trust_register

if @msg is not null
	SELECT	'CLIENT REFUND CHECK' as msg
	UNION ALL
	SELECT  '' as msg
	UNION ALL
	SELECT  @msg as msg
	UNION ALL
	SELECT  '' as msg
	UNION ALL
	SELECT	'Please detach and discard this voucher before depositing the draft. Thank you.' as msg
else
	SELECT	'CLIENT REFUND CHECK' as msg
	UNION ALL
	SELECT	'Please detach and discard this voucher before depositing the draft. Thank you.' as msg

-- Return success to the caller
RETURN ( 1 )
GO
