SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_trust_register_create_BE] ( @Amount AS Money = 0, @ItemDate AS DateTime = NULL, @Cleared AS VarChar(1) = NULL, @checknum as BigInt = null ) AS

-- ====================================================================================================
-- ==            Create a row in the trust register for a bank error                                 ==
-- ====================================================================================================

SET NOCOUNT ON

-- Default the item date
IF @ItemDate IS NULL
	SET @ItemDate = getdate()

DECLARE	@trust_register		INT

-- Insert the item into the trust register
INSERT INTO	registers_trust (tran_type,	date_created,	amount,		checknum,	cleared)
VALUES				('BE',		@ItemDate,	@Amount,	@checknum,	isnull(@cleared, ' '))

SELECT @trust_register = SCOPE_IDENTITY()

-- Return the trust register ID to the caller
RETURN ( @trust_register )
GO
