USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_retention_client_delete]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_retention_client_delete] ( @client_retention_event as typ_key ) AS

-- ==========================================================================================================================
-- ==            Delete the retention event from the system                                                                ==
-- ==========================================================================================================================

DELETE
FROM		client_retention_events
WHERE		client_retention_event = @client_retention_event

RETURN ( @@rowcount )
GO
