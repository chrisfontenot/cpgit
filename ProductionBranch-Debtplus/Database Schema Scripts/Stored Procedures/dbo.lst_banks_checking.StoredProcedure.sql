USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_banks_checking]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_banks_checking] AS
-- ==========================================================================================================
-- ==            Return the list of bank accounts suitable as a checking account                           ==
-- ==========================================================================================================
select	bank			as item_key,
	description		as description
from	banks with (nolock)
where	type = 'C'
order by bank

return ( @@rowcount )
GO
