USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ACH_Transaction_Errors]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ACH_Transaction_Errors] ( @FileID AS INT = NULL ) AS
-- ========================================================================================
-- ==             Fetch the detail information for the ACH report                        ==
-- ========================================================================================

-- ChangeLog
--   3/20/2002
--     Removed the agency "credit" transaction to client 0 for the total amount of the transfer from the report.

-- Disable the count of rows
SET NOCOUNT ON

-- Fetch the most recient file if there is none supplied
IF @FileID IS NULL
BEGIN
	SELECT TOP 1	@FileID = deposit_batch_id
	FROM		deposit_batch_ids
	WHERE		batch_type = 'AC'
	ORDER BY	date_created desc
END

-- There should be a file at this point
IF @FileID IS NULL
BEGIN
	RaisError (50032, 16, 1)
	RETURN ( 0 )
END

-- Fetch the information for the report
SELECT	
	t.reference,
	case t.ach_transaction_code
		when '22' then 'CHECKING REF'
		when '32' then 'SAVINGS REF'
		when '27' then 'CHECKING'
		when '37' then 'SAVINGS'
		when '23' then 'CHECKING RPRE'
		when '33' then 'SAVINGS RPRE'
		when '28' then 'CHECKING PRE'
		when '38' then 'SAVINGS PRE'
	end as 'transaction_code',
	t.ach_routing_number,
	t.ach_account_number,
	t.amount,
	t.client,
	t.ach_authentication_code + ': ' + isnull(a.description,'No Error') as 'authentication_code',
	isnull(a.serious_error,0) as 'serious_error',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',
	t.ach_authentication_code as authentication_error
FROM	deposit_batch_details t
LEFT OUTER JOIN people p ON t.client = p.client AND 1 = p.relation
LEFT OUTER JOIN Names pn ON p.NameID = pn.Name
LEFT OUTER JOIN ach_reject_codes a ON t.ach_authentication_code = a.ach_reject_code
WHERE	t.deposit_batch_id = @FileID
AND	(t.client > 0 or t.ach_transaction_code not in ('22', '32'))
ORDER BY t.ach_authentication_code, t.reference

-- Return the number of records
RETURN ( @@rowcount )
GO
