USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_letter_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_letter_types] AS

set nocount on

select distinct	letter_code, convert(int, 0) as letter_type, description as 'description'
into		#letter_types
from		letter_types

update		#letter_types
set		letter_type = l.letter_type
from		#letter_types a
inner join	letter_types l on a.letter_code = l.letter_code;

select		letter_type				as 'item_key',
		description				as 'description'
from		#letter_types

drop table	#letter_types;

return ( @@rowcount )
GO
