USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_fbc_info]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_fbc_info] ( @rpps_transaction AS INT ) AS

-- ===========================================================================================
-- ==            Generate an RPPS FBC information for a specific proposal                   ==
-- ===========================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   11/10/2003
--     Used only first 256 characters of note text for standard proposals.

-- Suppress intermediate result sets
SET NOCOUNT ON

-- Determine the client/creditor/debt from the record
DECLARE	@proposal	INT
DECLARE	@client		INT
DECLARE	@creditor_count	INT
DECLARE	@message	VarChar(256)

SELECT	@client		= client,
	@proposal	= client_creditor_proposal
FROM	rpps_transactions WITH (NOLOCK)
WHERE	rpps_transaction = @rpps_transaction

SELECT	@message	= convert(varchar(256), note_text)
FROM	client_creditor_proposals prop WITH (NOLOCK)
inner join debt_notes dn on prop.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type
WHERE	prop.client_creditor_proposal = @proposal

-- Ensure that there is no extra "junk" in the message
select	@message = replace(replace(replace(replace(replace(@message,char(13),' '),char(10),' '),char(12),' '),char(8),' '),char(9),' ')

-- Count the number of creditors
SELECT	@creditor_count = count(*)
FROM	client_creditor cc	WITH (NOLOCK)
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN creditors cr	WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class

WHERE	cc.client = @client
AND	isnull(ccl.zero_balance,0) = 0
AND	cc.reassigned_debt = 0
AND	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0
AND	cc.disbursement_factor > 0

-- Return the information
SELECT	isnull(@creditor_count,0)	as 'creditor_count',
	isnull(@message,'')		as 'creditor_message',
	@client				as 'client'

-- Return success to the caller
RETURN ( 1 )
GO
