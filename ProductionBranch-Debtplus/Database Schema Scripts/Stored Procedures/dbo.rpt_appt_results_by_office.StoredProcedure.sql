USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_results_by_office]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_results_by_office] ( @FromDate as datetime, @ToDate as datetime ) as

-- ===============================================================================================
-- ==            Return the information for the appointments based upon the result              ==
-- ===============================================================================================

set nocount on
if @ToDate is null
	select	@ToDate = getdate()

if @FromDate is null
	select	@FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar, @FromDate, 101) + ' 00:00:00'),
	@ToDate	  = convert(datetime, convert(varchar, @ToDate, 101) + ' 23:59:59')

-- Return the results
select	ca.start_time,
	o.name as 'office',
	ca.client as client,
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',

	ca.result as 'appt_results'

from	client_appointments ca
left outer join people p on ca.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join offices o on ca.office = o.office
where	ca.start_time between @FromDate and @ToDate
and	ca.status in ('K','W')
and	o.office is not null
and	ca.result is not null
order by ca.result, ca.office, ca.start_time, ca.client

return ( @@rowcount )
GO
