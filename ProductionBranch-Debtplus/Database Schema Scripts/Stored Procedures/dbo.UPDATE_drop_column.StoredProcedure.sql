USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_drop_column]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_drop_column] (@Table varchar(80), @Column varchar(80)) as

declare	@stmt		varchar(800)
declare	@default	int
declare	@domain		int
declare	@id			int

-- Find the column information
select	@default	= c.cdefault,
		@domain		= c.domain,
		@id			= c.id
from	syscolumns c
inner join sysobjects o on c.id = o.id
where	c.name	= @column
and		o.name = @table

-- The column must exist or we don't do anything more
if @id is not null
begin
	select	@stmt = 'dbo.' + @table + '.' + @column

	-- Check for a rule (before removing the default)
	if isnull(@domain,0) <> 0
		execute sp_unbindrule @stmt

	-- Check for a default
	if isnull(@default,0) <> 0
		execute sp_unbindefault @stmt

	-- Remove the column	
	select  @stmt = 'alter table [' + @Table + '] drop column [' + @Column + '];'
	exec ( @stmt )
end
GO
