USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_PrintChecks_Pending]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_PrintChecks_Pending] ( @bank as int = null ) AS

-- =====================================================================================
-- ==          Create a list of the pending checks to be printed                      ==
-- =====================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Correct the bank number
if isnull(@bank,0) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'

if @bank is null
	select	@bank		= 1

-- Use the trust register view to obtain the properly formatted information
SELECT	trust_register		as 'item_key',
	date_created		as 'item_date',
	debit_amt		as 'item_amount',
	payee			as 'item_name'

FROM	view_trust_register WITH (NOLOCK)

WHERE	cleared		= 'P'
AND	debit_amt	> 0
AND	tran_type	<> 'BW'
AND	bank		= @bank

-- Return the number of items in the list
RETURN ( @@rowcount )
GO
