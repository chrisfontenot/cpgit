if not exists(select * from sysobjects where type = 'p' and name = 'xpr_intake_import')
begin
	exec ('create procedure xpr_intake_import as return 0;')
end
GO
ALTER PROCEDURE [dbo].[xpr_intake_import]  ( @intake_client as int ) as
-- =================================================================================
-- ==     Import the clients from the intake system to the client system          ==
-- =================================================================================

-- ChangeLog
--   4/5/2002
--     Initial version
--   5/1/2002
--     Change to intake_client for program use
--   8/30/2002
--     Added "intake_agreement" to the client
--   9/31/2002
--     Added support for client_creditor_balances table
--   11/3/2002
--     Created proposals for the debts. This was forgotten.
--   11/13/2002
--     Added import to the "sales_debts" table
--   11/25/2002
--     Do not consider 0 months or $0 to be "past due" for auto loans
--   12/5/2002
--     Added county and relation to the import.
--   5/15/2003
--     Ensured that there is an applicant for the client. We don't operate properly without an applicant.
--   6/9/2003
--     Use the fee_sched_payment to set the disbursement factor for fixed PAF creditors.
--   3/26/2004
--     Mark the debts as exported from the sales tool.
--   4/27/2005
--     Imported "education".
--   5/9/2006
--     Added support for intake_secured_properties / intake_secured_loans
--     Removed intake_hud_loans and intake_auto_loans
--   7/20/2011
--     Created mandatory housing record for each client created. We don't call
--     xpr_create_client, so we need to do all of the work that it did.
--   10/26/2011
--     Corrected support for the assets table

-- All local variables used in this procedure are declared here.
declare	@account_number				typ_client_account
declare	@Acode						varchar(3)
declare	@Address1					varchar(80)
declare	@Address2					varchar(80)
declare	@AddressID					int
declare	@balance					money
declare	@budget_id					int
declare	@case_number				varchar(80)
declare	@city						varchar(80)
declare	@client						int
declare	@client_creditor			int
declare	@client_creditor_balance	int
declare	@comment_text				varchar(1024)
declare	@config_fee					int
declare	@counselor					int
declare	@creditor_name				typ_description
declare @current_value				money
declare @dependents					int
declare	@description				varchar(50)
declare	@HomeTelephoneID			int
declare	@home_ph					varchar(80)
declare	@house						varchar(80)
declare	@housing_status				int
declare	@housing_type				int
declare	@intake_id					varchar(80)
declare	@intake_property			int
declare	@interest_rate				float
declare	@language					int
declare	@last_error					int
declare	@last_rowcount				int
declare @lender						varchar(80)
declare	@loan_type					int
declare @loanID						int
declare @marital_status				int
declare	@message_ph					varchar(80)
declare	@method_first_contact		int
declare	@months_delinquent			int
declare	@MsgTelephoneID				int
declare	@non_dmp_interest			float
declare	@non_dmp_payment			money
declare	@Number						varchar(80)
declare	@office						int
declare	@original_price				money
declare @occupancy					int
declare @original_amount			money
declare @payment					money
declare	@paf_creditor				typ_creditor
declare	@past_due_amount			money
declare	@past_due_periods			int
declare @people						int
declare @periods					int
declare	@PostalCode					varchar(80)
declare	@PropertyID					int
declare @priority					int
declare	@purchaseprice				money
declare @residency					int
declare @referredby					int
declare @sales_file					int
declare	@secured_property			int
declare	@secured_type				int
declare	@state						int
declare	@street						varchar(80)
declare	@sub_description			varchar(50)
declare	@year_acquired				int
declare	@year_mfg					int
declare @UseInReports				bit

begin transaction
set xact_abort on
set nocount on

select	@intake_id		= intake_id,
		@client			= client
from	intake_clients
where	intake_client	= @intake_client

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

if @intake_id is null
begin
	rollback transaction
	RaisError ('The client (%d) is not in the intake client list.', 16, 1, @intake_client)
	return ( 0 )
end

-- Ensure that the client has not been converted
if @client is not null
begin
	commit transaction
	return ( @client )
end

-- =================================================================================
-- ==              Create the client                                              ==
-- =================================================================================

-- Determine the current configuration fee record for the client. It is not an optional column.
select	@state	= state
from	intake_clients
where	intake_client	= @intake_client;

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

-- If there is a state then try to find the fee associated with that state
if isnull(@state,1) > 1
begin
	select	@config_fee	= config_fee
	from	config_fees
	where	state		= @state

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort
end

-- If there is no entry for the state then use the system default value
if @config_fee is null
begin
	select	@config_fee	= min(config_fee)
	from	config_fees
	where	[default]	= 1

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort
end

-- If there is no default, then choose the first one in the system
if @config_fee is null
begin
	select	@config_fee	= min(config_fee)
	from	config_fees

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort
end

-- If there is a new config fee then update the client
if @config_fee is null
	select	@config_fee = 1

-- Try to use the person doing the import operation for the counselor
select	@counselor	= co.counselor
from	counselors co
inner join counselor_attributes coa on co.counselor = coa.counselor
inner join attributetypes a on coa.attribute = a.oID
where	a.[attribute] = 'COUNSELOR'
and		a.[Grouping] = 'ROLE'
and		co.[Default]     = 1
AND     co.ActiveFlag    = 1
and		co.person = suser_sname();

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

-- Define the counselor if there is none
if @counselor is null
begin
	select	@counselor = min(co.counselor)
	from	counselors co
	inner join counselor_attributes coa on co.counselor = coa.counselor
	inner join attributetypes a on coa.attribute = a.oID
	where	a.[attribute] = 'COUNSELOR'
	and		a.[Grouping] = 'ROLE'
	and     co.[ActiveFlag] = 1

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	if @counselor is null
	begin
		select	@counselor = min(counselor)
		from	counselors
		where   ActiveFlag = 1

		select	@last_error	= @@error,
			@last_rowcount	= @@rowcount
		if @last_error <> 0
			goto	ErrorAbort
	end
end

-- Use the office information if possible from the counselor
if @counselor is not null
begin
	select	@office		= office
	from	counselors
	where	counselor	= @counselor

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort
end

if @counselor is null
	select	@counselor = 1

-- Define the office if there is none
if @office is null
begin
	select	@office = office
	from	offices
	where	type = 1

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	if @office is null
	begin
		select	@office = min(office)
		from	offices
	end
end

if @office is null
	select	@office = 1

-- Try to define the language requirement
select	@language = oID
from	AttributeTypes
where	[grouping] = 'LANGUAGE'
and		[default]	= 1

select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

if @language is null
begin
	select	@language = min(oID)
	from	AttributeTypes
	where	[grouping] = 'LANGUAGE'
	
	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort
end

if @language is null
	select	@language = 1

select	@method_first_contact	= item_value
from	messages with (nolock)
where	item_type	= 'METHOD FIRST CONTACT'
and	nfcc		= 'I'

if @method_first_contact is null
	select	@method_first_contact	= min(item_value)
	from	messages with (nolock)
	where	item_type	= 'METHOD FIRST CONTACT'

if @method_first_contact is null
	select	@method_first_contact	= 1

-- Find the home address object
select	@Address1		= UPPER(Address1),
		@Address2		= UPPER(Address2),
		@city			= UPPER(City),
		@state			= state,
		@PostalCode		= UPPER(PostalCode),
		@home_ph		= home_ph,
		@message_ph		= message_ph
from	intake_clients
where	intake_client	= @intake_client;

-- Try to split the house and street if possible.
if @address1 like '[0-9]% %'
	select	@house			= ltrim(rtrim(left(@Address1, charindex(' ', @Address1)))),
			@Street			= ltrim(rtrim(substring(@Address1, charindex(' ', @Address1), 80)))
else
	select	@house			= '',
			@street			= @Address1

execute	@AddressID	= xpr_insert_address @house = @house, @street = @street, @address_line_2 = @Address2, @city = @City, @state = @State, @postalcode = @PostalCode

-- Find the home telephone number object
if @home_ph like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
begin
	select	@Acode = LEFT(@home_ph, 3),
			@Number = SUBSTRING(@home_ph, 4, 3) + '-' + SUBSTRING(@home_ph, 7, 4)
	execute	@HomeTelephoneID	= xpr_insert_TelephoneNumbers 1, @Acode, @Number
end

-- Find the message telephone number object
if @message_ph like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
begin
	select	@Acode = LEFT(@message_ph, 3),
			@Number = SUBSTRING(@message_ph, 4, 3) + '-' + SUBSTRING(@message_ph, 7, 4)
	execute	@MsgTelephoneID	= xpr_insert_TelephoneNumbers 1, @Acode, @Number
end

-- Calculate the number of dependents for this client
select  @people			= people,
		@dependents		= people - 1,
		@marital_status	= marital_status,
		@referredby		= referred_by
from	intake_clients
where	intake_client	= @intake_client

if @marital_status = 2
	select	@dependents	= @dependents - 1

if @dependents < 0
	select	@dependents	= 0

-- Create the client.
execute @client = xpr_create_client @HomeTelephoneID, 0, @ReferredBy, @method_first_contact, 0

-- Correct the client record
update	clients
set		AddressID				= @AddressID,
		MsgTelephoneID			= @MsgTelephoneID,
		cause_fin_problem1		= b.cause_fin_problem1,
		fed_tax_owed			= b.fed_tax_owed,
		fed_tax_months			= b.fed_tax_months,
		state_tax_owed			= b.state_tax_owed,
		state_tax_months		= b.state_tax_months,
		local_tax_owed			= b.local_tax_owed,
		local_tax_months		= b.local_tax_months,
		dependents				= @dependents,
		config_fee				= @config_fee,
		active_status			= 'CRE',
		active_status_date		= getdate(),
		counselor				= @counselor,
		office					= @office,
		language				= @language,
		method_first_contact	= @method_first_contact,
		marital_status			= @marital_status
from	clients c
inner join intake_clients b on b.intake_client = @intake_client
where	c.client				= @client

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount
		
if @last_error <> 0
	goto	ErrorAbort

-- Update the intake table with the imported information
update	intake_clients
set		client			= @client,
		imported_by		= suser_sname(),
		date_imported	= getdate()
where	intake_client	= @intake_client

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort
	
-- =================================================================================
-- ==              Create the people records                                      ==
-- =================================================================================

-- Load the applicant information if there is an applicant
if exists (select * from intake_people where intake_client=@intake_client and person = 1)
	execute xpr_intake_import_people @intake_client, @client, 1
	
-- Load the co-applicant if needed. The co-applicant is optional.
if exists (select * from intake_people where intake_client=@intake_client and person = 2)
begin
	execute xpr_insert_people @client = @client, @relation = 2
	execute xpr_intake_import_people @intake_client, @client, 2
end

-- =================================================================================
-- ==              Create the budget information                                  ==
-- =================================================================================
if exists (	select	*
		from	intake_budgets
		where	intake_client	= @intake_client
		and	client_amount > 0		)
begin

	-- Create the budget for the client
	insert into	budgets (client,starting_date,budget_type) values (@client, getdate(), 'I')
	select		@budget_id = SCOPE_IDENTITY(),
			@last_error	= @@error,
			@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	-- Include the budget category descriptions where needed
	insert into	budget_categories_other (client, description)
	select		@client, ' temp ' + convert(varchar, budget_category)
	from		intake_budgets
	where		intake_client	= @intake_client
	and		other_budget_category is not null
	and		budget_category >= 10000

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	-- Build a map to the new values for the budget categories
	select		budget_category as new_budget_category, convert(int, substring(description, 7, 80)) as old_budget_category
	into		#intake_map_budget_categories
	from		budget_categories_other
	where		client		= @client
	and		description like ' temp %'

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	-- Replace the description fields in the budget categories from the temporary labels used
	update		budget_categories_other
	set		description	= other_budget_category
	from		budget_categories_other o
	inner join	#intake_map_budget_categories m on o.budget_category = m.new_budget_category
	inner join	intake_budgets b on m.old_budget_category = b.budget_category and b.intake_client = @intake_client
	where		o.client	= @client
	and		b.budget_category >= 10000

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	-- Load the detail information for the standard budget categories
	insert into budget_detail (budget, budget_category, client_amount, suggested_amount) 	select		@budget_id, budget_category, client_amount, client_amount
	from		intake_budgets
	where		intake_client	= @intake_client
	and		budget_category < 10000

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	-- Load the detail information for the additional budget categories
	insert into	budget_detail (budget, budget_category, client_amount, suggested_amount)
	select		@budget_id, m.new_budget_category, client_amount, client_amount
	from		intake_budgets b
	inner join	#intake_map_budget_categories m on b.budget_category = m.old_budget_category
	where		b.intake_client	= @intake_client
	and		b.budget_category >= 10000

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort

	drop table	#intake_map_budget_categories
end

-- =================================================================================
-- ==              Create the asset categories                                    ==
-- =================================================================================
insert into assets (client, asset_id, amount, frequency)
select	@client, asset_id, asset_amount, 4
from	intake_assets
where	intake_client	= @intake_client

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

-- =================================================================================
-- ==              Create an entry in the sales table                             ==
-- =================================================================================

execute @sales_file		= xpr_insert_sales_file @client

-- Update the export information so that it is not performed again
update	sales_files
set		date_exported	= getdate(),
		exported_by		= suser_sname()
where	sales_file		= @sales_file

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount

if @last_error <> 0
	goto	ErrorAbort

-- =================================================================================
-- ==              Create the balance information                                 ==
-- =================================================================================

declare	debt_cursor cursor for
	select	isnull(creditor_name,''), isnull(account_number,''), balance, isnull(non_dmp_payment,0), isnull(non_dmp_interest,0), isnull(months_delinquent,0)
	from	intake_debts
	where	intake_client	= @intake_client

open	debt_cursor
fetch	debt_cursor into @creditor_name, @account_number, @balance, @non_dmp_payment, @non_dmp_interest, @months_delinquent

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount

if @last_error <> 0
begin
	close	debt_cursor
	deallocate debt_cursor
	goto	ErrorAbort
end

while @@fetch_status = 0
begin
	-- Create the system debt information. It will create the needed proposal
	execute @client_creditor = xpr_create_debt @client, default, @creditor_name, @account_number

	if @client_creditor is not null
	begin
		-- Update the information
		update	client_creditor
		set		non_dmp_payment		= @non_dmp_payment,
				non_dmp_interest	= @non_dmp_interest,
				months_delinquent	= @months_delinquent
		where	client_creditor		= @client_creditor

		select	@last_error		= @@error,
				@last_rowcount	= @@rowcount

		if @last_error <> 0
		begin
			close	debt_cursor
			deallocate debt_cursor
			goto	ErrorAbort
		end

		-- Update the initial balance information
		update	client_creditor_balances
		set		orig_balance		= @balance
		from	client_creditor_balances b
		inner join client_creditor cc on cc.client_creditor_balance = b.client_creditor_balance
		where	cc.client_creditor	= @client_creditor


		select	@last_error		= @@error,
				@last_rowcount	= @@rowcount

		if @last_error <> 0
		begin
			close	debt_cursor
			deallocate debt_cursor
			goto	ErrorAbort
		end
	end

	-- Insert the debt information
	insert into sales_debts (sales_file, creditor_name, creditor_type, balance, account_number, interest_rate, minimum_payment)
	values (@sales_file, @creditor_name, 'Q', @balance, @account_number, isnull(@non_dmp_interest,0), isnull(@non_dmp_payment,0))

	select	@last_error		= @@error,
			@last_rowcount	= @@rowcount

	if @last_error <> 0
	begin
		close	debt_cursor
		deallocate debt_cursor
		goto	ErrorAbort
	end

	fetch	debt_cursor into @creditor_name, @account_number, @balance, @non_dmp_payment, @non_dmp_interest, @months_delinquent
end

close	debt_cursor
deallocate debt_cursor

-- =================================================================================
-- ==              Create the PAF debt information                                ==
-- =================================================================================
select	@paf_creditor	= paf_creditor
from	config

select	@last_error		= @@error,
		@last_rowcount	= @@rowcount

if @last_error <> 0
begin
	close	debt_cursor
	deallocate debt_cursor
	goto	ErrorAbort
end

if @paf_creditor is not null
begin
	-- Create the system debt information. It will create the needed proposal
	select  @account_number = dbo.format_client_id ( @client );
	execute @client_creditor = xpr_create_debt @client, @paf_creditor, default, @account_number

	-- Update the information for the debt based upon the config fee
	declare	@fee_sched_payment	money
	declare	@fee_type		int

	select	@fee_sched_payment	= fee_sched_payment,
			@fee_type		= fee_type
	from	config_fees
	where	config_fee		= @config_fee

	select	@last_error		= @@error,
			@last_rowcount	= @@rowcount

	if @last_error <> 0
	begin
		close	debt_cursor
		deallocate debt_cursor
		goto	ErrorAbort
	end

	if isnull(@fee_type,0) = 1
	begin
		update	client_creditor
		set		disbursement_factor	= isnull(@fee_sched_payment,0),
				sched_payment		= isnull(@fee_sched_payment,0)
		where	client_creditor		= @client_creditor

		select	@last_error		= @@error,
				@last_rowcount	= @@rowcount

		if @last_error <> 0
			goto	ErrorAbort

		update	client_creditor_balances
		set	current_sched_payment	= isnull(@fee_sched_payment,0),
			orig_balance			= isnull(@fee_sched_payment,0)
		from	client_creditor_balances b
		inner join client_creditor cc on b.client_creditor_balance = cc.client_creditor_balance
		where	cc.client_creditor = @client_creditor

		select	@last_error		= @@error,
				@last_rowcount	= @@rowcount

		if @last_error <> 0
			goto	ErrorAbort
	end
end

-- =================================================================================
-- ==              Load the secured loan information                              ==
-- ==              This does NOT include housing. For housing, see below.         ==
-- =================================================================================
declare	property_cursor_1 cursor for
	select	secured_property,
			p.secured_type,
			case when p.description = '' then t.description else p.description end as description,
			sub_description,
			original_price,
			current_value,
			year_acquired,
			year_mfg
	from	intake_secured_properties p with (nolock)
	left outer join secured_types t with (nolock) on p.secured_type = t.secured_type
	WHERE	t.auto_home_other <> 'H'
	AND		intake_client	= @intake_client
	order by p.secured_property;

open	property_cursor_1

select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
if @last_error <> 0
begin
	deallocate property_cursor_1
	goto	ErrorAbort
end

fetch	property_cursor_1 into @secured_property, @secured_type, @description, @sub_description, @original_price, @current_value, @year_acquired, @year_mfg

select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
if @last_error <> 0
begin
	close	property_cursor_1
	deallocate property_cursor_1
	goto	ErrorAbort
end

while @@fetch_status = 0
begin
	-- Load the property information
	insert into secured_properties (client, class, secured_type, description, sub_description, original_price, current_value, year_acquired, year_mfg)
	values (@client, 1, @secured_type, @description, @sub_description, @original_price, @current_value, @year_acquired, @year_mfg)
	select	@intake_property	= scope_identity(),
			@last_error	= @@error,
			@last_rowcount	= @@rowcount

	if @last_error <> 0
	begin
		close	property_cursor_1
		deallocate property_cursor_1
		goto	ErrorAbort
	end

	-- Add any loans against the property
	insert into secured_loans (secured_property, lender, account_number, interest_rate, payment, balance, past_due_amount, past_due_periods)
	select	@intake_property, lender, account_number, interest_rate, payment, balance, past_due_amount, past_due_periods
	from	intake_secured_loans
	where	secured_property	= @secured_property;

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount

	if @last_error <> 0
	begin
		close	property_cursor_1
		deallocate property_cursor_1
		goto	ErrorAbort
	end

	fetch	property_cursor_1 into @secured_property, @secured_type, @description, @sub_description, @original_price, @current_value, @year_acquired, @year_mfg
end

close	property_cursor_1
deallocate property_cursor_1

select	@housing_type		= housing_type,
		@housing_status		= housing_status
from	intake_clients
where	intake_client		= @intake_client;

select @residency = 1

-- Update the housing status for the client in the client_housing table
update	client_housing
set		housing_status		= @housing_status
from	client_housing
where	client				= @client

-- Start with the first record used in the reporting data
select	@UseInReports		= 1

-- Determine the price and current value for the homes
declare	property_cursor_2 cursor for
	select	secured_property,
			original_price,
			current_value,
			year_acquired,
			p.secured_type
	from	intake_secured_properties p with (nolock)
	left outer join secured_types t with (nolock) on p.secured_type = t.secured_type
	WHERE	t.auto_home_other = 'H'
	AND		intake_client	= @intake_client
	order by p.secured_property;

open		property_cursor_2
fetch		property_cursor_2 into @secured_property, @original_price, @current_value, @year_acquired, @secured_type
while @@fetch_status = 0
begin
	-- Create an owner and co-owner record
	declare	@ownerID		int
	execute @OwnerID = xpr_insert_housing_borrower @PersonID = 1

	declare	@CoOwnerID		int
	execute @CoOwnerID = xpr_insert_housing_borrower @PersonID = 2

	-- Define the residency if needed. This is very custom to this site.
	select @residency = case @secured_type when 500 then 1 when 510 then 3 else 4 end

	-- Define the occupancy
	select	@occupancy		= @dependents + case @marital_status when 2 then 2 else 1 end;
	if @residency <> 1
		select @occupancy = 0

	-- Create a property record for this item
	execute @PropertyID	= xpr_insert_housing_property @HousingID = @client, @UseHomeAddress = 1, @OwnerID = @OwnerID, @CoOwnerID = @CoOwnerID, @Residency = @Residency, @Occupancy = @Occupancy, @PropertyType = @housing_type, @PurchasePrice = @PurchasePrice, @ImprovementsValue = @current_value, @UseInReports = @UseInReports

	declare	loan_cursor_1 cursor for
		select	priority,
				lender,
				account_number,
				case_number,
				interest_rate,
				payment,
				original_amount,
				balance,
				periods,
				past_due_periods + 1,
				loan_type
		from	intake_secured_loans l with (nolock)
		WHERE	secured_property = @secured_property
		order by l.priority, l.secured_loan;

	open		loan_cursor_1
	fetch		loan_cursor_1 into @priority, @lender, @account_number, @case_number, @interest_rate, @payment, @original_amount, @balance, @periods, @past_due_periods, @loan_type

	while @@fetch_status = 0
	begin

		-- Create a detail record for the loan
		declare	@IntakeDetailID	int
		execute @IntakeDetailID = xpr_insert_housing_loan_detail @LoanTypeCD=5, @FinanceTypeCD=1, @MortgageTypeCD=@loan_type, @InterestRate=@interest_rate, @Payment=@payment

		-- Create a lender record
		declare	@CurrentLenderID	int
		execute @CurrentLenderID = xpr_insert_housing_lender @ServicerName=@lender, @AcctNum=@account_number

		-- Check to ensure that the past due periods reflects reality
		if @past_due_periods < 1
				select @past_due_periods = min(oID) from housing_delinquencytypes
		else
			if not exists(select * from housing_delinquencytypes where oID = @past_due_periods)
				select @past_due_periods = max(oID) from housing_delinquencytypes

		-- Create the loans against the property. This is an additional cursor to load the loan data correctly
		execute @LoanID = xpr_insert_housing_loan @PropertyiD=@PropertyID, @Loan1st2nd=@priority, @LoanDelinquencyMonths=@past_due_periods, @UseOrigLenderID=1, @CurrentLenderID=@CurrentLenderID, @OrigLoanAmt=@original_amount, @CurrentLoanBalanceAmt=@balance, @IntakeSameAsCurrent=1, @IntakeDetailID=@IntakeDetailID, @UseInReports=@UseInReports
		select  @UseinReports = 0

		fetch		loan_cursor_1 into @priority, @lender, @account_number, @case_number, @interest_rate, @payment, @original_amount, @balance, @periods, @past_due_amount, @loan_type
	end
	close loan_cursor_1
	deallocate loan_cursor_1

	-- Obtain the next property record
	fetch	property_cursor_2 into @secured_property, @original_price, @current_value, @year_acquired, @secured_type
end
close		property_cursor_2
deallocate	property_cursor_2

/*
-- Ensure that there is a primary residence for the user
select	@primary_residence = min(secured_property)
from	secured_properties p with (nolock)
inner join secured_types t on p.secured_type = t.secured_type
where	t.auto_home_other	= 'H'
and	p.client		= @client

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

-- Correct the housing type from the client for a temporary fix
select	@housing_type		= housing_type
from	intake_clients
where	intake_client		= @intake_client;

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

if @primary_residence is not null
begin
	update	secured_properties
	set	primary_residence	= 1,
		housing_type		= @housing_type
	where	secured_property	= @secured_property

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort
end
*/

-- =================================================================================
-- ==              Include an alias for the intake id                             ==
-- =================================================================================
insert into client_addkeys (client, additional)
values (@client, 'intake ' + @intake_id)

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

insert into client_addkeys (client, additional)
select	@client, 'intake client ' + dbo.format_client_id ( @intake_client )

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

-- =================================================================================
-- ==              Include intake notes                                           ==
-- =================================================================================
insert into client_notes (client, client_creditor, type, is_text, subject, note)
values	(@client, null, 3, 1, 'Intake conversion notes',
	'The client was created by importing intake client ' + @intake_id )

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

-- =================================================================================
-- ==              Include any intake notes that may be present                   ==
-- =================================================================================
insert into client_notes (client, client_creditor, type, is_text, subject, note, date_created, created_by)
select	@client, null, 1, 1, 'Intake conversion notes', note, date_created, created_by
from	intake_notes
where	intake_client	= @intake_client

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

-- =================================================================================
-- ==              Include a comment note if present                              ==
-- =================================================================================
select	@comment_text	= message
from	intake_clients
where	intake_client	= @intake_client

select	@last_error	= @@error,
	@last_rowcount	= @@rowcount
if @last_error <> 0
	goto	ErrorAbort

select	@comment_text	= ltrim(rtrim(@comment_text))
if @comment_text is null
	select @comment_text = ''

if @comment_text <> ''
begin
	insert into client_notes (client, client_creditor, is_text, subject, note)
	values (@client, null, 1, 'Intake prospect comments', @comment_text)

	select	@last_error	= @@error,
		@last_rowcount	= @@rowcount
	if @last_error <> 0
		goto	ErrorAbort
end

-- =================================================================================
-- ==              Complete the transaction                                       ==
-- =================================================================================
commit transaction
return ( @client )

ErrorAbort:
if @@trancount > 0
	rollback transaction

raiserror (@last_error, 16, 1)
return ( 0 )
GO
exec ('grant execute on xpr_intake_import to public as dbo')
exec ('deny execute on xpr_intake_import to www_role')
