USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_client_creditor]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MONTHLY_client_creditor] AS
-- ======================================================================================
-- ==         Do "end of month" processing on the client_creditor table                ==
-- ======================================================================================

-- ChangeLog
--   12/5/2001
--     Added updates to the "statistics_offices" table for the disbursement information.
--   1/2/2002
--     Added 2nd and 3rd month disbursements to hold quarterly statement information.
--   7/1/2002
--     Update scheduled pay on all clients *BUT* "EX" and "I" active status. Previouly, it did only "A" and "AR".
--   8/1/2002
--     Removed "statistics_offices" from the procedure and write a seperate MONTHLY_office_statistics procedure.
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"

-- Keep things honest
BEGIN TRANSACTION

-- Calculate the next monthly scheduled payment.
UPDATE		client_creditor

SET		sched_payment				= case
			when isnull(c.active_status,'') IN ('EX', 'I')		then 0
			when cc.reassigned_debt > 0				then 0
			when isnull(ccl.zero_balance,0) > 0			then disbursement_factor
			when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) <= 0 then 0
			when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > cc.disbursement_factor then cc.disbursement_factor
			else isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
		end

FROM		client_creditor cc
INNER JOIN	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
INNER JOIN	clients c	WITH (NOLOCK) ON cc.client	= c.client
INNER JOIN	creditors cr	WITH (NOLOCK) ON cc.creditor	= cr.creditor
LEFT OUTER JOIN	creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class

-- Calculate the statistics for the next month's cycle
update	client_creditor_balances
set	total_sched_payment	= bal.total_sched_payment + bal.current_sched_payment,
	current_sched_payment	= cc.sched_payment,
	payments_month_1	= bal.payments_month_0,
	payments_month_0	= 0
from	client_creditor_balances bal
inner join client_creditor cc on bal.client_creditor = cc.client_creditor

COMMIT TRANSACTION
return ( @@rowcount )
GO
