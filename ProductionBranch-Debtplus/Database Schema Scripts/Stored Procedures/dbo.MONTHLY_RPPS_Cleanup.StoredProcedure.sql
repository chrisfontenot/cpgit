USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_RPPS_Cleanup]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[MONTHLY_RPPS_Cleanup] AS

-- ==========================================================================================
-- ==            Discard obsolete non-error RPPS transactions                              ==
-- ==========================================================================================

-- ChangeLog
--   6/20/2007
--     Discarded old RPPS response information

-- Disable row counts
set nocount on
BEGIN TRANSACTION

-- Discard the transactions that have no bearing today. They are too old. We use the death_date field to determine when to toss them.
delete from rpps_transactions         where (death_date is null) or (convert(datetime, convert(varchar(10), death_date,101)) < dateadd(d, 1, convert(datetime, convert(varchar(10), getdate(), 101))))
delete from rpps_batches              where rpps_batch not in (select rpps_batch from rpps_transactions)
delete from rpps_files                where rpps_file not in (select rpps_file from rpps_batches)

-- Remove the stranded response details
delete from rpps_response_details     where rpps_transaction is not null and rpps_transaction not in (select rpps_transaction from rpps_transactions)
delete from rpps_response_details_cda where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_details_cdr where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_details_cdc where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_details_cdd where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_details_cdp where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_details_cdt where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_details_cdv where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_details_coa where rpps_response_detail not in (select rpps_response_detail from rpps_response_details)
delete from rpps_response_files       where rpps_response_file not in (select rpps_response_file from rpps_response_details)

COMMIT TRANSACTION
RETURN ( 1 )
GO
