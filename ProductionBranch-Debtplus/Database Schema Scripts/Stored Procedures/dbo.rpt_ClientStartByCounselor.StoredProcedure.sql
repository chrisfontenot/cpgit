USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ClientStartByCounselor]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_ClientStartByCounselor] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) as
-- ==============================================================================================
-- ==               List the client start dates by counselor                                   ==
-- ==============================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table

-- Suppress intermediate result sets
set nocount on

-- Update the dates so that the times are removed
if @ToDate is null
	select @ToDate = getdate()

if @FromDate is null
	select @FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Generate the list of clients which match the start dates
select	c.client								as 'client',
	dbo.format_normal_name(default, pn.first, default, pn.last, pn.suffix)	as 'name',
	c.active_status								as 'status',
	c.start_date								as 'start_date',
	c.last_deposit_date							as 'last_deposit_date',
	c.last_deposit_amount							as 'last_deposit_amount',
	(select sum(isnull(deposit_amount,0)) from client_deposits d where d.client = c.client)						as 'disbursement_factor',
	sum(case when cc.reassigned_debt = 0 then isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0) else 0 end) as 'balance',

	c.counselor								as 'counselor',
	dbo.format_normal_name(default,cox.first,default,cox.last,default)	as 'counselor_name',

	o.name									as 'office_name'

from	clients c with (nolock)
left outer join client_creditor cc with (nolock) on c.client = cc.client
inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
left outer join counselors co with (nolock) on c.counselor = co.counselor
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
left outer join offices o with (nolock) on c.office = o.office
left outer join names cox with (nolock) on co.NameID = cox.name

where	c.start_date between @FromDate and @ToDate

group by c.client, pn.first, pn.last, pn.suffix, c.active_status, c.start_date, c.last_deposit_date, c.last_deposit_amount, c.counselor, cox.first, cox.last, o.name
order by 10, 1; -- co.name, c.client

return ( @@rowcount )
GO
