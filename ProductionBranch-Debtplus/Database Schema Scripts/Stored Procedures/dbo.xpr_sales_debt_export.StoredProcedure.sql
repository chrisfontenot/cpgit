IF NOT EXISTS(SELECT * FROM SYSOBJECTS WHERE TYPE='P' AND NAME=N'xpr_sales_debt_export')
	EXEC ('CREATE PROCEDURE xpr_sales_debt_export AS')
GO
ALTER procedure [dbo].[xpr_sales_debt_export] ( @sales_file as int ) as

-- =============================================================================================
-- ==            Export the debts from the sales tool to the client's debts                   ==
-- =============================================================================================

-- ChangeLog
-- 6/2/07 changed procedure to create a uncoded creditor called 'payment cushion'.

set nocount on

-- Set to 1 if you want a payment cushion debt to be created
declare	@UsePaymentCushion		int
select	@UsePaymentCushion = 1

declare	@client				int			-- Client ID
declare @setup_creditor		varchar(10)	-- Creditor for setup accounts
declare	@paf_creditor		varchar(10)	-- Creditor for PAF accounts
declare	@assigned_debts		int			-- Number of debts in the client's list
declare	@creditor_name		varchar(80)	-- Name of the creditor
declare	@account_number		varchar(80)	-- Account number
declare	@client_creditor	int			-- Pointer to the debt table
declare	@sales_debt			int			-- Pointer to the sales debt table
declare	@plan_payment		money		-- Payment amount for this debt
declare	@minimum_payment	money		-- Non-DMP payment amount
declare	@interest_rate		float		-- Non-DMP interest rate
declare	@plan_interest_rate	float		-- DMP interest rate
declare	@balance			money		-- Balance on the debt
declare	@fee_type			int			-- Type of FEE creditor
declare	@total_balance		money		-- Total of the balances
declare @client_state		int
declare	@client_state_name	varchar(10)

-- Find today (without time)
declare @now			datetime
declare	@today			datetime
set		@now = getdate()
set		@today = convert(datetime,convert(varchar(10), @now, 101))

-- A transaction keeps things streight
begin transaction
begin try

	-- Find the client from the sales file
	select	@client		= client
	from	sales_files with (nolock)
	where	sales_file	= @sales_file

	-- The client must be defined
	if @client is null
		raiserror ('The client can not be determined from the sales tool (#%d)', 16, 1, @sales_file )

	-- The client must be legal
	if @client <= 0
		raiserror ('The client (%d) is not valid for the sales tool (#%d)', 16, 1, @client, @sales_file )
	
	-- Find the client's state from the client row
	select	@client_state	= a.[state]
	from	[clients] c with (nolock)
	inner join [addresses] a with (nolock) on c.[addressid] = a.[address]
	where	c.[client]		= @client

	-- Find the setup and paf creditors from the config file
	select	@setup_creditor = [setup_creditor],
			@paf_creditor   = [paf_creditor]
	from	[config] with (nolock)

	if @setup_creditor is null
		select @setup_creditor = ''

	if @paf_creditor is null
		select @paf_creditor = ''

	-- Ensure that the export has not been performed
	declare	@date_exported	datetime
	select	@date_exported	= [date_exported]
	from	[sales_files]
	where	[sales_file]	= @sales_file

	if @date_exported is not null
		raiserror ('The information for the sales tool (#%d) has been exported and may not be updated.', 16, 1, @client, @sales_file )

	-- Update the date/time for the export operation
	update	sales_files
	set		[date_exported]	= @now,
			[exported_by]	= suser_sname()
	where	[sales_file]	= @sales_file

	-- Start with a zero balance for the debts
	select	@total_balance	= 0

	-- Do the export operation. Do this by creating a debt for all of the listed debts
	-- Ignore any reference to the setup and PAF creditors. These are created below.
	declare	input_cursor cursor for
		select	isnull([client_creditor],0) as client_creditor, [sales_debt], [creditor_name], [account_number], [plan_payment], [minimum_payment], [interest_rate], [plan_interest_rate], [balance]
		from	[sales_debts]
		where	[sales_file] = @sales_file

	open	input_cursor
	fetch	input_cursor into @client_creditor, @sales_debt, @creditor_name, @account_number, @plan_payment, @minimum_payment, @interest_rate, @plan_interest_rate, @balance

	while @@fetch_status = 0
	begin
		if @client_creditor <= 0
		begin

			-- Create the creditor in the system
			execute @client_creditor = xpr_create_debt @client, default, @creditor_name, @account_number
			if @client_creditor <= 0
			begin
				close		input_cursor
				deallocate	input_cursor
				rollback transaction
				return ( 0 )
			end

			-- Update the debt record with the pointer to the exported item
			update	sales_debts
			set		client_creditor		= @client_creditor
			where	sales_debt			= @sales_debt
		end

		-- Update the balance information for the debt
		update	client_creditor_balances
		set		orig_balance			= isnull(@balance,0),
				orig_balance_adjustment = 0,
				total_interest			= 0,
				total_payments			= 0
		from	client_creditor_balances b
		inner join client_creditor cc on b.client_creditor_balance = cc.client_creditor_balance
		where	cc.client_creditor	= @client_creditor

		-- Update the other information for the debt
		update	client_creditor
		set		disbursement_factor	= isnull(@plan_payment,0),
				sched_payment		= isnull(@plan_payment,0),
				non_dmp_payment		= @minimum_payment,	-- NULL is ok
				non_dmp_interest	= @interest_rate,	-- NULL is ok
				dmp_payout_interest	= @plan_interest_rate	-- NULL is ok
		from	client_creditor
		where	client_creditor		= @client_creditor

		-- Accumulate the total balances
		select	@total_balance		= @total_balance + isnull(@balance,0)

		-- Go to the next creditor
		fetch	input_cursor into @client_creditor, @sales_debt, @creditor_name, @account_number, @plan_payment, @minimum_payment, @interest_rate, @plan_interest_rate, @balance
	end

	-- Disconnect from the cursor
	close		input_cursor
	deallocate	input_cursor

	-- Correct the account number for an agency account
	select	@account_number = dbo.format_client_id ( @client )

	-- The creditor may be null. If so, no item is created.
	if @paf_creditor <> ''
	begin

		-- There should be only one creditor of this type.
		if not exists (select * from client_creditor where client = @client and creditor = @paf_creditor)
		begin

			-- Create the PAF account
			execute @client_creditor = xpr_create_debt @client, @paf_creditor, default, @account_number
			if @client_creditor = 0
			begin
				rollback transaction
				return ( 0 )
			end

			-- Update the information for the PAF account
			select	@balance	= isnull(fee_sched_payment,0),
					@fee_type	= fee_type
			from	config_fees f with (nolock)
			inner join clients c on f.config_fee = c.config_fee
			where	c.client = @client

			-- Set the disbursement factor and the scheduled pay
			if @fee_type = 1
				update	client_creditor
				set		disbursement_factor	= @balance,
						sched_payment		= @balance
				where	client_creditor		= @client_creditor
		end
	end

	-- If there is a payment cushion then adjust the cushion account
	if @UsePaymentCushion = 1
	begin

		-- Calculate the amount for the Payment Cushion special creditor
		declare	@disbursement_factor	money
		select	@disbursement_factor	= 10
		if @total_balance >= 10000
		begin
			select	@disbursement_factor	= 15
			if @total_balance >= 15000
			begin
				select	@disbursement_factor	= 20
				if @total_balance >= 20000
				begin
					select	@disbursement_factor	= 25
					if @total_balance >= 30000
					begin
						select	@disbursement_factor = 30
						if @total_balance >= 40000
							select	@disbursement_factor = 35
					end
				end
			end
		end

		-- If there is a state then find the name from the states table
		if @client_state is not null
			select	@client_state_name	= MailingCode
			from	states with (nolock)
			where	[state]				= @client_state

		-- If the state is IN (Indiana) or MN (Minnesota) then use $0.00 for the amount
		if isnull(@client_state_name,'') in ('IN','MN')
			select	@disbursement_factor = 0

		-- Create the uncoded creditor
		execute @client_creditor = xpr_create_debt @client, default, 'PAYMENT CUSHION', @account_number
	
		if @client_creditor = 0
		begin
			rollback transaction
			return ( 0 )
		end

		-- Correct the balance for the debt
		update	client_creditor_balances
		set		orig_balance			= @disbursement_factor,
				orig_balance_adjustment	= 0,
				total_interest			= 0,
				total_payments			= 0,
				current_sched_payment	= @disbursement_factor
		where	client_creditor			= @client_creditor

		-- Correct the disbursement factor for the debt
		update	client_creditor
		set		disbursement_factor	= @disbursement_factor,
				sched_payment		= @disbursement_factor
		where	client_creditor		= @client_creditor;
	end

	-- Add the setup fee from the table if there is one
	if @setup_creditor <> ''
	begin
		-- There should be only one creditor of this type.
		if not exists (select * from client_creditor where client = @client and creditor = @setup_creditor)
		begin
			declare	@setup_fee		money
			select	@setup_fee = [amount]
			from	[StateSetupFees]
			where	[effective_date]	<= @today
			and		([cutoff_date] IS NULL OR [cutoff_date] >= @today)
			and		[state]				 = @client_state

			if @setup_fee is null
				select	@setup_fee = 0

			-- If there is a fee then create the creditor
			if @setup_fee > 0
			begin
				execute @client_creditor = xpr_create_debt @client, @setup_creditor, null, @account_number
				if @client_creditor = 0
				begin
					rollback transaction
					return ( 0 )
				end

				-- Update the balance information for the debt
				update	client_creditor_balances
				set		orig_balance			= @setup_fee,
						current_sched_payment	= @setup_fee,
						orig_balance_adjustment = 0,
						total_interest			= 0,
						total_payments			= 0
				from	client_creditor_balances b
				inner join client_creditor cc on b.client_creditor_balance = cc.client_creditor_balance
				where	cc.client_creditor	= @client_creditor

				-- Update the other information for the debt
				update	client_creditor
				set		disbursement_factor	= @setup_fee,
						sched_payment		= @setup_fee,
						orig_dmp_payment	= @setup_fee,
						non_dmp_payment		= 0,
						non_dmp_interest	= 0,
						dmp_payout_interest	= 0
				from	client_creditor
				where	client_creditor		= @client_creditor
			end
		end					
	end

	-- Complete the processing
	commit transaction
	return ( 1 )
END TRY

BEGIN CATCH
	DECLARE	@ErrorMessage	NVARCHAR(4000);
	DECLARE	@ErrorSeverity	INT;
	DECLARE	@ErrorState		INT;

	SELECT	@ErrorMessage	= ERROR_MESSAGE(),
			@ErrorSeverity	= ERROR_SEVERITY(),
			@ErrorState		= ERROR_STATE();

	ROLLBACK TRANSACTION

	-- Raise the error condition that was trapped
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	return ( 0 )
END CATCH
GO
GRANT EXECUTE ON xpr_sales_debt_export TO public AS dbo;
DENY EXECUTE ON xpr_sales_debt_export TO www_role;
GO
