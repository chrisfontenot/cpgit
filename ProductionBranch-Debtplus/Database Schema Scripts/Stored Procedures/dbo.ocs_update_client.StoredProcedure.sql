SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Brandon Wilhite
-- Create date: 
-- Description:	Update an already existing client
-- 12/4/2015 KWilkie - Updated to speed up overall attempts
-- =============================================

ALTER PROCEDURE [dbo].[ocs_update_client] (
	@clientId int,	
	@queueCode varchar(10) = NULL,		--string QueueCode
	@language int,

	@prefix1 varchar(40) = null,
	@suffix1 varchar(40) = null,
	@email1 varchar(80) = null,
	@SSN1 varchar(50) = null,
	
	@prefix2 varchar(40) = null,
	@suffix2 varchar(40) = null,
	@email2 varchar(80) = null,
	@SSN2 varchar(50) = null,

	@useHomeAddress bit = null,			--property address same as client address
	@street varchar(50),				--string Street,
	@street2 varchar(50),
	@city varchar(50),					--string City,
	@state int,							--int State,
	@zipcode varchar(50),				--string Zipcode,
  @firstcounseldate datetime = null,
  @secondcounseldate datetime = null,
	@Error varchar(max) OUTPUT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set @Error = ''

	begin transaction

	declare @p1 int,	    -- Person 1 ID
			@n1 int,		-- Name 1 ID
			@e1 int,		-- Email 1 ID
			@p2 int,		-- Person 2 ID
			@n2 int,		-- Name 2 ID
			@e2 int,		-- Email 2 ID
			@prop1 int,		-- Property ID
			@propAdd1 int	-- Property Address ID

	SELECT  @P1 = NULL,
			@N1 = NULL,
			@E1 = NULL,
			@P2 = NULL,
			@N2 = NULL,
			@E2 = NULL,
			@Prop1 = NULL,
			@PropAdd1 = NULL

	begin try
	
		update OCS_Client
		set QueueCode = @queueCode,
        FirstCounselDate = @firstcounseldate,
        SecondCounselDate = @secondcounseldate
		where ClientId = @clientId
		and Archive = 0
		
		update clients
		set language = @language
		where client = @clientId

		--insert people, if needed
		--applicant		
		set @p1 = (select top 1 Person from people where Client = @clientId and Relation = 1)
		if @p1 is null and (@prefix1 is not null or @suffix1 is not null or @email1 is not null or @SSN1 is not null)
		begin
			insert into people(Client,Relation)
			values(@clientId,1)

			set @p1 = SCOPE_IDENTITY()
		end

		--co applicant
		set @p2 = (select top 1 Person from people where Client = @clientId and Relation <> 1)
		if @p2 is null and (@prefix2 is not null or @suffix2 is not null or @email2 is not null or @SSN2 is not null)
		begin
			insert into people(Client,Relation,Gender,Race,Ethnicity,Education,CreditAgency,no_fico_score_reason)
			values(@clientId,2,2,10,2,1,'EQUIFAX',4)

			set @p2 = SCOPE_IDENTITY()
		end
		
		--applicant updates

		-- Check that the Names and Emails exist or not
		SELECT @n1 = NameID, @e1 = EmailID 
		FROM people 
		WHERE Person = @p1

		if @prefix1 is not null or @suffix1 is not null
		begin
			if @n1 is null
			begin
				insert into Names(First,Last,Prefix,Suffix)
				values ('','',@prefix1,@suffix1)

				set @n1 = SCOPE_IDENTITY()

				update people
				set NameID = @n1
				where Person = @p1
			end
			else
				update Names
				set Prefix = @prefix1,
					Suffix = @suffix1
				where Name = @n1
		end

		if @email1 is not null
		begin
			if @e1 is null
			begin
				insert into EmailAddresses(Address,Validation)
				values(@email1,1)

				set @e1 = SCOPE_IDENTITY()

				update people 
				set EmailID = @e1
				where Person = @p1
			end
			else
				update EmailAddresses
				set Address = @email1
				where Email = @e1
		end

		if @SSN1 is not null
		begin
			update people
			set SSN = @SSN1
			where Person = @p1
		end

		-- Check that the Names and Emails exist or not
		SELECT @n2 = NameID, @e2 = EmailID 
		FROM people 
		WHERE Person = @p2

		--co applicant updates
		if @prefix2 is not null or @suffix2 is not null
		begin
			if @n2 is null
			begin
				insert into Names(First,Last,Prefix,Suffix)
				values ('','',@prefix2,@suffix2)

				set @n2 = SCOPE_IDENTITY()

				update people
				set NameID = @n2
				where Person = @p2
			end
			else
				update Names
				set Prefix = @prefix2,
					Suffix = @suffix2
				where Name = @n2
		end

		if @email2 is not null
		begin
			if @e2 is null
			begin
				insert into EmailAddresses(Address,Validation)
				values(@email2,1)

				set @e2 = SCOPE_IDENTITY()

				update people 
				set EmailID = @e2
				where Person = @p2
			end
			else
				update EmailAddresses
				set Address = @email2
				where Email = @e2
		end

		if @SSN2 is not null
		begin
			update people
			set SSN = @SSN2
			where Person = @p2
		end

		--udpate property address
		if @useHomeAddress is not null
		begin
			set @prop1 = (select top 1 oID from Housing_properties where OwnerID = @p1)

			update Housing_properties
			set UseHomeAddress = @useHomeAddress
			where oID = @prop1

			if @useHomeAddress = 0
			begin
				SELECT @propAdd1 = PropertyAddress 
				from Housing_properties 
				where oID = @prop1

				if @propAdd1 is null
				begin
					INSERT INTO addresses (PostalCode,street,address_line_2,city,[state])
					VALUES (@zipcode,@street,@street2,@city,@state)

					set @propAdd1 = SCOPE_IDENTITY()

					update Housing_properties
					set PropertyAddress = @propAdd1
					where oID = @prop1
				end
				else
					update addresses
					set PostalCode = @zipcode,
						street = @street,
						address_line_2 = @street2,
						city = @city,
						[state] = @state
					where [address] = @propAdd1
			end
		end

		commit transaction
	end try

	begin catch
		set @Error = ERROR_MESSAGE()
		rollback transaction
	end catch

END

GO