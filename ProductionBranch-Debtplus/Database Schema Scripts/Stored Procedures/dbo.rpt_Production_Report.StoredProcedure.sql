USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Production_Report]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Production_Report] ( @FromDate as DateTime, @ToDate as DateTime ) AS

-- ============================================================================================
-- ==            Production Report information                                               ==
-- ============================================================================================

-- ChangeLog
--   10/30/2001
--     Corrected (removed) extra count of deduction amount in "total disbursements"
--   2/11/2002
--     Corrected to allow "walkin" appointments as "kept"
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   7/7/2003
--     Ignore workshop appointments
--   8/22/2003
--     Change housing appointment conditions to look at the appointment type table rather than the housing flag.
--   12/8/2004
--     Ignore voided items in calculating disbursements
--   4/30/2009
--     Created temporary table for the transaction information to improve performance
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Supply the proper defaults
IF @ToDate IS NULL
	SELECT @ToDate = getdate();

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate;

-- Remove the time information from the dates
select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59');

-- Special creditor IDs
declare	@paf_creditor		typ_creditor
declare	@deduct_creditor	typ_creditor
declare	@setup_creditor		typ_creditor

-- Obtain the creditor IDs from the configuration table
select	@paf_creditor		= paf_creditor,
	@deduct_creditor	= deduct_creditor,
	@setup_creditor		= setup_creditor
from	config with (nolock);

-- Local storage
declare	@total_appts_scheduled		int
declare	@total_first_appts		int
declare	@total_counseled_counseled	int
declare	@total_dmp_written		int
declare	@total_sa_written		int
declare	@total_bk7_written		int
declare	@total_bk13_written		int
declare	@total_fco_written		int
declare	@total_rla_written		int
declare	@total_same_month_returns	int
declare	@total_recounsel		int
declare	@total_hud			int
declare	@total_dmp_starts		int
declare	@total_first_deposits		int
declare	@total_clients			int
declare	@total_active_clients		int
declare	@total_disbursed_clients	int
declare	@total_dropped_clients		int
declare	@total_sc_clients		int
declare	@total_sa_clients		int
declare	@total_disbursed_amount		money
declare	@total_setup_fees		money
declare	@total_paf_amount		money
declare	@other_fees_amount		money
declare	@deduction_amount		money
declare	@deduction_earned		money
declare	@billed_amount			money
declare	@invoices_generated		money
declare	@total_invoiced			money
declare	@invoice_payments		money
declare	@invoice_adjustments		money

-- -------------------------------------------------------------------------------
-- --         Total number of appointments scheduled                            --
-- -------------------------------------------------------------------------------

select	@total_appts_scheduled	= count(*)
from	client_appointments with (nolock)
where	start_time between @FromDate AND @ToDate
and	workshop is null;

-- -------------------------------------------------------------------------------
-- --         Number of first appointments                                      --
-- -------------------------------------------------------------------------------

select		@total_first_appts	= count(*)
from		client_appointments ca with (nolock)
inner join	clients c with (nolock) on ca.client = c.client
where		start_time between @FromDate AND @ToDate
and		ca.client_appointment = c.first_appt
and		workshop is null;

-- -------------------------------------------------------------------------------
-- --         Number of clients counseled                                       --
-- -------------------------------------------------------------------------------

select	distinct client
into	#t_production_rpt_1
from	client_appointments ca with (nolock)
where	start_time between @FromDate AND @ToDate
and	ca.status in ('K','W')
and	workshop is null;

select	@total_counseled_counseled	= count(*)
from	#t_production_rpt_1;

drop table #t_production_rpt_1;

-- -------------------------------------------------------------------------------
-- --         Number of DMPs written                                            --
-- -------------------------------------------------------------------------------

select	@total_dmp_written = count(*)
from	clients c with (nolock)
where	dmp_status_date between @FromDate and @ToDate;

-- -------------------------------------------------------------------------------
-- --         Number of CCH counselings                                         --
-- -------------------------------------------------------------------------------

select	@total_sa_written = count(*)
from	clients c with (nolock)
where	c.client in (
	select	client
	from	client_appointments ca with (nolock)
	where	start_time between @FromDate AND @ToDate
	and	ca.status in ('K','W')
	and	ca.result = 'CCH'
	and	workshop is null
);

-- -------------------------------------------------------------------------------
-- --         Number of BK7 counselings                                         --
-- -------------------------------------------------------------------------------

select	@total_bk7_written = count(*)
from	clients c with (nolock)
where	c.client in (
	select	client
	from	client_appointments ca with (nolock)
	where	start_time between @FromDate AND @ToDate
	and	ca.status in ('K','W')
	and	ca.result = 'BK7'
	and	workshop is null
);

-- -------------------------------------------------------------------------------
-- --         Number of BK13 counselings                                        --
-- -------------------------------------------------------------------------------

select	@total_bk13_written = count(*)
from	clients c with (nolock)
where	c.client in (
	select	client
	from	client_appointments ca with (nolock)
	where	start_time between @FromDate AND @ToDate
	and	ca.status in ('K','W')
	and	ca.result = 'BK13'
	and	workshop is null
);

-- -------------------------------------------------------------------------------
-- --         Number of FCO counselings                                         --
-- -------------------------------------------------------------------------------

select	@total_fco_written = count(*)
from	clients c with (nolock)
where	c.client in (
	select	client
	from	client_appointments ca with (nolock)
	where	start_time between @FromDate AND @ToDate
	and	ca.status in ('K','W')
	and	ca.result = 'FCO'
	and	workshop is null
);

-- -------------------------------------------------------------------------------
-- --         Number of RLA counselings                                         --
-- -------------------------------------------------------------------------------

select	@total_rla_written = count(*)
from	clients c with (nolock)
where	c.client in (
	select	client
	from	client_appointments ca with (nolock)
	where	start_time between @FromDate AND @ToDate
	and	ca.status in ('K','W')
	and	ca.result = 'RLA'
	and	workshop is null
);

-- -------------------------------------------------------------------------------
-- --         Number of same month returns                                      --
-- -------------------------------------------------------------------------------

select		client, count(*) as 'cnt'
into		#t_production_rpt_2
from		client_appointments ca with (nolock)
where		start_time between @FromDate AND @ToDate
and		ca.status in ('K','W')
and		workshop is null
group by	client;

select		@total_same_month_returns = count(*)
from		#t_production_rpt_2
where		cnt > 1;

drop table	#t_production_rpt_2;

-- -------------------------------------------------------------------------------
-- --         Number of recounsels                                              --
-- -------------------------------------------------------------------------------

select		@total_recounsel	= count(*)
from		client_appointments ca with (nolock)
inner join	clients c with (nolock) on c.client = ca.client
where		ca.start_time between @FromDate AND @ToDate
and		c.first_appt != ca.client_appointment
and		ca.status in ('K','W')
and		workshop is null;

-- -------------------------------------------------------------------------------
-- --         Number of HUD counselings                                         --
-- -------------------------------------------------------------------------------

select	@total_hud	= count(*)
from	client_appointments ca with (nolock)
inner join appt_types t with (nolock) on ca.appt_type = t.appt_type
where	ca.start_time between @FromDate AND @ToDate
and	t.housing > 0
and	ca.status in ('K','W')
and	workshop is null;

-- -------------------------------------------------------------------------------
-- --         Number of DMP starts                                              --
-- -------------------------------------------------------------------------------

select	@total_dmp_starts	= count(*)
from	clients c with (nolock)
where	c.start_date between @FromDate AND @ToDate
and	c.active_status in ('A', 'AR');

-- -------------------------------------------------------------------------------
-- --         Number of first deposits                                          --
-- -------------------------------------------------------------------------------

select		@total_first_deposits = count(*)
from		clients with (nolock)
where		first_deposit_date BETWEEN @FromDate and @ToDate;

-- -------------------------------------------------------------------------------
-- --         Number of clients                                                 --
-- -------------------------------------------------------------------------------

select		@total_clients = count(*)
from		clients with (nolock);

-- -------------------------------------------------------------------------------
-- --         Number of active clients                                          --
-- -------------------------------------------------------------------------------

select		@total_active_clients = count(*)
from		clients with (nolock)
where		active_status in ('A', 'AR');

-- -------------------------------------------------------------------------------
-- --         Break down disbursements by creditor                              --
-- -------------------------------------------------------------------------------

-- Retrieve the transactions
select rcc.client_creditor_register, rcc.client, rcc.creditor, rcc.debit_amt, rcc.creditor_type, rcc.fairshare_amt, convert(int,0) as type, convert(int,isnull(ccl.zero_balance,0)) as zero_balance, isnull(ccl.agency_account,0) as agency_account, isnull(rcc.void,0) as void
into #disbursed_creditors
from registers_client_creditor rcc with (nolock)
left outer join creditors cr with (nolock) on rcc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where rcc.date_created between @FromDate and @ToDate
and   rcc.tran_type in ('AD', 'BW', 'MD', 'CM')

-- Create an index on the working table by creditor id
create index ix1_temp_disbursed_creditors on #disbursed_creditors ( creditor );

-- Mark the creditors in the table
if @paf_creditor is not null
	update #disbursed_creditors set [type] = 1 where creditor = @paf_creditor;
	
if @setup_creditor is not null
	update #disbursed_creditors set [type] = 2 where creditor = @setup_creditor;
	 
if @deduct_creditor is not null
	update #disbursed_creditors set [type] = 3 where creditor = @deduct_creditor;

-- -------------------------------------------------------------------------------
-- --         Number of clients disbursed                                       --
-- -------------------------------------------------------------------------------

select distinct	client
into		#t_production_rpt_4
from		#disbursed_creditors
where		[agency_account] = 0;

select		@total_disbursed_clients = count(*)
from		#t_production_rpt_4;

drop table	#t_production_rpt_4;

-- Toss all voided transactions at this point. It is wrong to count them.
delete
from		#disbursed_creditors
where		void <> 0;

-- -------------------------------------------------------------------------------
-- --         Total setup fees                                                  --
-- -------------------------------------------------------------------------------

if @setup_creditor is not null
begin
	select		@total_setup_fees = sum(debit_amt)
	from		#disbursed_creditors
	where		[type]	= 2;
	
	select		@other_fees_amount = sum(debit_amt)
	from		#disbursed_creditors
	where		[type]	= 0
	and			zero_balance <> 0;

end else begin

	select		@other_fees_amount = sum(debit_amt)
	from		#disbursed_creditors
	where		[type] not in (1, 3)
	and			zero_balance <> 0;

end

if @total_setup_fees is null
	select @total_setup_fees = 0;

-- -------------------------------------------------------------------------------
-- --         Total program fees                                                --
-- -------------------------------------------------------------------------------

select		@total_paf_amount = sum(debit_amt)
from		#disbursed_creditors
where		[type] = 1;

-- -------------------------------------------------------------------------------
-- --         Total issued on checks to the deduct creditor                     --
-- -------------------------------------------------------------------------------

select		@deduction_amount = sum(debit_amt)
from		#disbursed_creditors
where		[type] = 3;

-- -------------------------------------------------------------------------------
-- --         Total disbursed to creditors                                      --
-- -------------------------------------------------------------------------------

-- Remove the "fee" amounts to get the total amount disbursed to real "creditors"
select		@total_disbursed_amount = sum(debit_amt)     from #disbursed_creditors
select		@deduction_earned       = sum(fairshare_amt) from #disbursed_creditors where creditor_type = 'D'
select      @billed_amount          = sum(fairshare_amt) from #disbursed_creditors where creditor_type not in ('D','N')

-- Remove the "fee" amounts to get the total amount disbursed to real "creditors"
select		@total_disbursed_amount	= isnull(@total_disbursed_amount,0) -
					  isnull(@other_fees_amount,0) -
					  isnull(@total_paf_amount,0) -
					  isnull(@total_setup_fees,0) -
					  isnull(@deduction_amount,0);

drop table	#disbursed_creditors

-- -------------------------------------------------------------------------------
-- --         Number of clients dropped                                         --
-- -------------------------------------------------------------------------------

select		client, drop_date, c.drop_reason, dr.nfcc
into		#clients
from		clients c with (nolock)
left outer join	drop_reasons dr on c.drop_reason = dr.drop_reason
where		c.drop_date between @FromDate and @ToDate
and			c.active_status = 'I'

select		@total_dropped_clients = count(*)
from		#clients

-- -------------------------------------------------------------------------------
-- --         Number of successful completions                                  --
-- -------------------------------------------------------------------------------

select		@total_sc_clients = count(*)
from		#clients
where		nfcc = 'SC1'

-- -------------------------------------------------------------------------------
-- --         Number of self-administered                                       --
-- -------------------------------------------------------------------------------

select		@total_sa_clients = count(*)
from		#clients
where		nfcc in ('SC2', 'SC3')

drop table	#clients

-- -------------------------------------------------------------------------------
-- --         invoices generated                                                --
-- -------------------------------------------------------------------------------

select		inv_date,
		sum(isnull(inv_amount,0) - isnull(pmt_amount,0) - isnull(adj_amount,0)) as 'inv_amount'
into		#t_production_rpt_5
from		registers_invoices with (nolock)
where		isnull(inv_amount,0) - isnull(pmt_amount,0) - isnull(adj_amount,0) > 0
group by	inv_date;

select	@invoices_generated = sum(inv_amount)
from	#t_production_rpt_5
where	inv_date between @FromDate AND @ToDate;

select	@total_invoiced = sum(inv_amount)
from	#t_production_rpt_5;

drop table #t_production_rpt_5;

-- -------------------------------------------------------------------------------
-- --         invoice payments                                                  --
-- -------------------------------------------------------------------------------

select	@invoice_payments = sum(d.credit_amt)
from	registers_creditor d with (nolock)
where	d.tran_type = 'RC'
and	d.date_created between @FromDate AND @ToDate;

-- -------------------------------------------------------------------------------
-- --         invoice adjustments                                               --
-- -------------------------------------------------------------------------------

select	@invoice_adjustments = sum(d.credit_amt)
from	registers_creditor d with (nolock)
where	d.tran_type = 'RA'
and	d.date_created between @FromDate AND @ToDate;

-- -------------------------------------------------------------------------------
-- --         Return the results                                                --
-- -------------------------------------------------------------------------------

select	@FromDate								as 'from_date',
		@ToDate									as 'to_date',
		isnull(@total_appts_scheduled,0)		as 'total_appts_scheduled',
		isnull(@total_first_appts,0)			as 'total_first_appts',
		isnull(@total_counseled_counseled,0)	as 'total_counseled_counseled',
		isnull(@total_dmp_written,0)			as 'total_dmp_written',
		isnull(@total_sa_written,0)				as 'total_sa_written',
		isnull(@total_fco_written,0)			as 'total_fco_written',
		isnull(@total_rla_written,0)			as 'total_rla_written',
		isnull(@total_same_month_returns,0)		as 'total_same_month_returns',
		isnull(@total_recounsel,0)				as 'total_recounsel',
		isnull(@total_hud,0)					as 'total_hud',
		isnull(@total_dmp_starts,0)				as 'total_dmp_starts',
		isnull(@total_first_deposits,0)			as 'total_first_deposits',
		isnull(@total_clients,0)				as 'total_clients',
		isnull(@total_active_clients,0)			as 'total_active_clients',
		isnull(@total_disbursed_clients,0)		as 'total_disbursed_clients',
		isnull(@total_dropped_clients,0)		as 'total_dropped_clients',
		isnull(@total_sc_clients,0)				as 'total_sc_clients',
		isnull(@total_sa_clients,0)				as 'total_sa_clients',
		isnull(@total_disbursed_amount,0)		as 'total_disbursed_amount',
		isnull(@total_setup_fees,0)				as 'total_setup_fees',
		isnull(@total_paf_amount,0)				as 'total_paf_amount',
		isnull(@other_fees_amount,0)			as 'other_fees_amount',
		isnull(@deduction_amount,0)				as 'deduction_amount',
		isnull(@deduction_earned,0)				as 'deduction_earned',
		isnull(@billed_amount,0)				as 'billed_amount',
		isnull(@invoices_generated,0)			as 'invoices_generated',
		isnull(@total_invoiced,0)				as 'total_invoiced',
		isnull(@invoice_payments,0)				as 'invoice_payments',
		isnull(@invoice_adjustments,0)			as 'invoice_adjustments',
		isnull(@total_bk7_written,0)			as 'total_bk7_clients',
		isnull(@total_bk13_written,0)			as 'total_bk13_clients';

return ( 1 )
GO
