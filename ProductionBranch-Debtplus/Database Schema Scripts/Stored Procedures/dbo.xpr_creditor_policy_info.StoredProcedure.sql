USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_policy_info]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_policy_info] ( @creditor as varchar(10), @policy_matrix_policy_type as varchar(50) ) as

SELECT	message
FROM	policy_matrix_policies
WHERE	creditor = @creditor
AND	policy_matrix_policy_type = @policy_matrix_policy_type

return ( @@rowcount )
GO
