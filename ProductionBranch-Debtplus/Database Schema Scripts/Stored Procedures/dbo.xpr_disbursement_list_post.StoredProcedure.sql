USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_list_post]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_list_post] ( @disbursement_register AS INT ) AS
-- ===========================================================================================
-- ==            Fetch the information for postdisbursement processing                      ==
-- ===========================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   1/1/2003
--     Added check to ensure that the client and creditor disbursments match before paying debts.
--     This occurs on a rare condition where the review of a client is cancelled without paying.
--   11/20/2003
--     Added the bank number to the result set and removed the creditor/config information.
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   4/27/07
--     Refund amounts to the client that have been paid to debts deleted since the disbursement batch
--     was created. Make no effort to correct the Monthly fee amount at this time.
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Suppress intermediate results
SET NOCOUNT ON

-- -------------------------------------------------------------------------------------------
-- --            discard disbursements from clients who were not paid                       --
-- -------------------------------------------------------------------------------------------

begin transaction disbursement_list_post			-- Start a sub transaction

-- Build a list of the clients that we must return the paid amounts because someone has deleted
-- the debts.
select	x.*
into	#missing_debts
from	disbursement_creditors x
left outer join client_creditor cc on x.client_creditor = cc.client_creditor
where	cc.client_creditor is null
and	x.debit_amt > 0
and	x.trust_register is null
and	x.disbursement_register = @disbursement_register;

-- Determine if we found this condition. It needs to be returned to the client.
if exists (select * from #missing_debts)
begin
	-- Suppress the amount paid on the debt
	update	disbursement_creditors
	set	debit_amt = 0
	from	disbursement_creditors cr
	inner join #missing_debts x on cr.disbursement_register = x.disbursement_register and cr.client_creditor = x.client_creditor;

	-- Calculate the amount to be refunded to the client
	select	client, sum(debit_amt) as debit_amt
	into	#missing_amounts
	from	#missing_debts
	group by client;

	-- Refund the amounts to the client
	update	clients
	set	held_in_trust	= held_in_trust + x.debit_amt
	from	clients c
	inner join #missing_amounts x on c.client = x.client;

	-- Adjust the amount disbursed for this client
	update	registers_client
	set	debit_amt = rc.debit_amt - x.debit_amt
	from	registers_client rc
	inner join disbursement_clients dc on rc.client_register = dc.client_register
	inner join #missing_amounts x on dc.client = x.client
	where	dc.disbursement_register = @disbursement_register;
	
	drop table #missing_amounts
end

drop table #missing_debts

-- Build a list of the amounts paid by debt. Compute the total amount for each client.
select	isnull(c.client,cr.client) as client, sum(cr.debit_amt) as creditor_amt, isnull(rc.debit_amt,0) as client_amt, rc.client_register
into	#creditors
from	disbursement_creditors cr
full outer join disbursement_clients c	on cr.disbursement_register = c.disbursement_register and cr.client = c.client
left outer join registers_client rc	on c.client_register = rc.client_register
where	cr.disbursement_register	= @disbursement_register
group by cr.client, c.client, rc.debit_amt, rc.client_register;

-- Discard the client amounts which are zero. This is just making things "nice".
delete	registers_client
from	registers_client rc
inner join #creditors cr on rc.client_register = cr.client_register
where	cr.client_amt = 0;

update	#creditors
set	client_register = null
where	client_amt = 0;

-- Discard the items that do not match penny for penny
delete
from	#creditors
where	isnull(client_amt,0) = isnull(creditor_amt,0);

-- What is left are the ones which have creditor amounts but no matching client amount.
-- These were called up in a review and not paid.
-- Do not pay them.
if exists (select * from #creditors)
begin

	-- Return the money to the client. (This should have been done and the amounts should be $0.00 but just in case . . . .)
	update	clients
	set	held_in_trust	= isnull(c.held_in_trust,0) + isnull(rc.debit_amt,0)
	from	clients c
	inner join registers_client rc on rc.client = c.client
	inner join #creditors cr on rc.client_register = cr.client_register;

	-- Discard the client amounts which are now zero. This is just making things "nice".
	delete	registers_client
	from	registers_client rc
	inner join #creditors cr on rc.client_register = cr.client_register;

	-- Cancel the creditor disbursements
	update	disbursement_creditors
	set	debit_amt		= 0
	from	disbursement_creditors d
	inner join #creditors cr on d.client = cr.client
	where	d.disbursement_register	= @disbursement_register;
end

-- Discard the working table
drop table #creditors;

commit transaction disbursement_list_post

-- -------------------------------------------------------------------------------------------
-- --            creditor information for the disbursement                                  --
-- -------------------------------------------------------------------------------------------
SELECT
		-- Amount paid
		x.debit_amt						as 'debit_amt',

		-- Information for .NET
		cc.client_creditor_balance				as 'debt',
		cr.creditor_id						as 'creditor_id',
		x.creditor_type						as 'creditor_type',

		-- Information for the current ID
		cc.client						as 'client',
		upper(cc.creditor)					as 'creditor',
		cc.client_creditor					as 'client_creditor',

		-- Information for the current debt being paid
		convert(float,isnull(x.fairshare_pct_eft,-1))		as 'fairshare_pct_eft',
		convert(float,isnull(x.fairshare_pct_check,-1))		as 'fairshare_pct_check',

		case ltrim(rtrim(isnull(cc.account_number,'')))
			when '' then 'MISSING'
			else cc.account_number
		end							as 'account_number',

		LEFT(UPPER(ISNULL(cc.client_name, isnull(pn.last,'')+isnull(' '+pn.first,'')))+'            ',12) AS 'client_name',

		-- EFT payment information
		x.bank							as 'bank',
		x.rpps_biller_id					as 'rpps_biller_id',
		convert(varchar(20),null)				as 'epay_biller_id'

FROM		disbursement_creditors x
INNER JOIN	clients c		WITH (NOLOCK) ON x.client = c.client
INNER JOIN	client_creditor cc	WITH (NOLOCK) ON x.client_creditor = cc.client_creditor
LEFT OUTER JOIN	people p		WITH (NOLOCK) ON x.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
INNER JOIN	creditors cr		WITH (NOLOCK) ON x.creditor = cr.creditor

WHERE		x.disbursement_register = @disbursement_register
AND		x.debit_amt > 0
AND		x.trust_register IS NULL
ORDER BY	x.bank, x.rpps_biller_id, cr.type, cr.creditor_id, x.creditor, x.client, x.client_creditor

return ( @@rowcount )
GO
