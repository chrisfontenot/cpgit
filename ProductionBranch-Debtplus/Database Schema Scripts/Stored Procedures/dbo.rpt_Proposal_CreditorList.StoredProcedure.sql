USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_CreditorList]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_CreditorList] ( @client_creditor_proposal AS INT = null ) AS
-- ===========================================================================================
-- ==                    Fetch the client_creditor from the proposal table                  ==
-- ===========================================================================================

-- ChangeLog
--   9/20/2002
--     Change to use the full creditor name
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   9/2/2003
--     Changed for CR 9.0 to call with "NULL" as a parameter value.
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

DECLARE	@client_creditor	INT

if @client_creditor_proposal = 0
	select	@client_creditor_proposal = null

IF @client_creditor_proposal IS NULL
	SELECT	@client_creditor	= 0

ELSE BEGIN

	SELECT	@client_creditor = client_creditor
	FROM	client_creditor_proposals
	WHERE	client_creditor_proposal = @client_creditor_proposal

	IF @client_creditor IS NULL
	BEGIN
		RAISERROR (50038, 16, 1, @client_creditor_proposal)
		RETURN ( 0 )
	END
END

-- ===========================================================================================
-- ==                    Fetch the client from the client_creditor table                    ==
-- ===========================================================================================

DECLARE @client INT

IF @client_creditor_proposal IS NULL
	SELECT		@client = 0

ELSE BEGIN

	SELECT	@client = client
	FROM	client_creditor
	WHERE	client_creditor = @client_creditor

	IF @client IS NULL
	BEGIN
		RAISERROR (50039, 16, 1, @client_creditor)
		RETURN ( 0 )
	END
END

-- ===========================================================================================
-- ==             Retrieve the list of debts for this client                                ==
-- ===========================================================================================
SELECT		CASE isnull(ccl.zero_balance,0)
			WHEN 0 then isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
			ELSE 0
		END AS 'balance',

		cc.disbursement_factor,

		CASE
			WHEN cc.client_creditor = @Client_Creditor THEN '*'
			ELSE ' '
		END AS 'marker',

		case
			when cc.creditor is null then isnull(cc.creditor_name,'')
			else isnull(cr.creditor_name,'Unknown')
		end								as 'creditor_name',
		
		@client_creditor_proposal as client_creditor_proposal

FROM		client_creditor cc	WITH (NOLOCK)
INNER JOIN	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_classes ccl WITH (NOLOCK) on cr.creditor_class = ccl.creditor_class

WHERE		client = @client
AND		cc.reassigned_debt = 0
AND		((bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest > bal.total_payments) OR (isnull(ccl.zero_balance,0) > 0) OR (cc.client_creditor = @Client_Creditor))
AND		isnull(ccl.proposal_balance,1) <> 0
ORDER BY	1 DESC, 2 DESC

-- Return the number of debt records selected
RETURN ( @@rowcount )
GO
