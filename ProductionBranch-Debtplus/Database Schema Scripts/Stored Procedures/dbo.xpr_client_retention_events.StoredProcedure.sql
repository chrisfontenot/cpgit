USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_retention_events]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_client_retention_events] ( @client as int, @show_all as int = 0 ) AS

-- =================================================================================================================
-- ==            List the retention events for a client                                                           ==
-- =================================================================================================================

if @show_all is null
	SELECT @show_all = 0

if @show_all = 0
begin
	SELECT	client_retention_event as 'item_key',
		event_description,
		event_amount,
		created_by,
		date_created,
		event_message,
		action_date,
		(select description from retention_actions where retention_action = (select retention_action from client_retention_actions where client = 6011767 and date_created = action_date)) as last_action_description
	FROM	view_retention_client_events
	WHERE	client=@client
	AND	date_expired IS NULL

end else begin
	SELECT	client_retention_event as 'item_key',
		event_description,
		event_amount,
		created_by,
		date_created,
		event_message,
		action_date,
		(select description from retention_actions where retention_action = (select retention_action from client_retention_actions where client = 6011767 and date_created = action_date)) as last_action_description
	FROM	view_retention_client_events
	WHERE	client=@client
end

return ( @@rowcount )
GO
