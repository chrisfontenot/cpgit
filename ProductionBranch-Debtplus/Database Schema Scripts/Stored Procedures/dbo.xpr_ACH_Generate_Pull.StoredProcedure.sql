USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_ACH_Generate_Pull]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_ACH_Generate_Pull] ( @ACH_Pull_Date AS datetime, @ach_file as int, @bank as int = 0 ) AS
-- ========================================================================================
-- ==           Generate the transactions to sweep the funds into the ACH table          ==
-- ========================================================================================

-- ChangeLog
--    09/25/2001
--      Removed time from the calculation of the ach_start_date. Added a @ach_start_date
--      variable which is set to the end of the indicated date.
--    10/03/2001
--      Added support for Sacramento's ACH to generate a credit transaction
--    10/26/2001
--      Added "one_time" and "ach_enabled" to the client deposits. Deleted one-time deposits.
--    12/8/2001
--      Added "ach_file" in preparation for "electronic check" type file transactions.
--    2/20/2002
--      Ignore client 0, even if it is configured for ACH.
--    6/16/2002
--      Limit the amount of the ACH pull to the total balance that will be disbursed this month.
--      This should work if there are multiple pulls in a month since the first one will not be
--      limited because the balance will be too high, the second one will be. However, there will
--      not be a "proration" between them as that is too complicated to program at this time.
--
--      We can also limit it to the disbursement factors, but that may or may not be correct since
--      the disbursement factors can vary depending upon when the debts are prorated. (I decided to not
--      do this so it is commented out below.)
--
--      I specifically did not include the "one-time" pull operation into the balance limitation as I feel
--      that the one-time is really a "make-up" type of operation and do not wish to limit the value as it
--      is destroyed once the item is used.
--   9/31/2002
--     Added support for client_creditor_balances table
--   11/24/2002
--     Added bank reference in preparation for identifying clients by ACH banking reference information
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   2/20/2003
--     Corrected the offsetting deposit amount to only add the items for the current file.
--   4/12/2004
--     Do not honor negative balances. They would falsely reduce the pull amount.

-- Disable warning conditions about truncation
SET ANSI_WARNINGS OFF

-- Suppress the result set for the count of records
SET NOCOUNT ON

-- Do things with a transaction to make it all work or all fail
BEGIN TRANSACTION
SET XACT_ABORT ON

-- Find the PAF creditor
declare	@paf_creditor	varchar(10)
select	@paf_creditor	= paf_creditor
from	config with (nolock);

-- Ensure that the pull date is proper
declare	@ach_start_date	datetime
select	@ach_start_date = convert(datetime, convert(varchar(10), @ACH_Pull_Date, 101) + ' 23:59:59'),
	@ACH_Pull_Date  = convert(datetime, convert(varchar(10), @ACH_Pull_Date, 101) + ' 00:00:00')

-- Find the bank reference if there is none
if (@bank is null) and (@ach_file is not null)
	select	@bank	= bank
	from	deposit_batch_ids
	where	deposit_batch_id = @ach_file

if @bank is null
	select	@bank	= 0

if not exists (select * from banks where type = 'A' and bank = @bank)
begin
	select	@bank		= min(bank)
	from	banks
	where	type		= 'A'
	and	[default]	= 1

	if @bank is null
		select	@bank	= min(bank)
		from	banks
		where	type	= 'A'

	if @bank is null
		select	@bank = 0

	if @bank < 1
		select	@bank = 1
end

-- Build a list of the clients and their normal pull values
select	c.client			as client,
	sum(d.deposit_amount)		as deposit_amount,
	sum(d.deposit_amount)		as original_amount,
	convert(int,null)		as client_deposit
into	#ach_deposits

FROM	clients c with (nolock)
INNER JOIN client_deposits d with (nolock) on d.client = c.client AND convert(datetime, convert(varchar(10), d.deposit_date, 101) + ' 00:00:00') = @ACH_Pull_Date
INNER JOIN client_ach ach with (nolock) on c.client = ach.client

-- Ignore client 0 even if it is active
WHERE	c.client > 0

-- Accept only active clients
and	c.active_status in ('A', 'AR')

-- Valid ACH
AND	ach.ABA IS NOT NULL
and ach.ABA like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'

-- On ACH
and	ach.isActive = 1
AND	ach.ErrorDate IS NULL
AND	ach.PrenoteDate IS NOT NULL
AND	ltrim(rtrim(isnull(ach.AccountNumber,''))) <> ''
AND	ach.StartDate <= @ach_start_date
AND (ach.EndDate IS NULL OR ach.EndDate > @ach_start_date)

-- With a deposit for ACH
-- and	isnull(d.ach_pull,1) <> 0
and	isnull(d.one_time,0) = 0

group by c.client
having sum(d.deposit_amount) > 0

-- Create an index for the client to speed things up in the matching operation
create index #tmp_ach_deposits on #ach_deposits ( client );

-- Find the disbursement information for the clients that will be pulled now
select	cc.client			as 'client',
	sum(cc.disbursement_factor)	as 'disbursement_factor',
	sum(
		case
			when isnull(ccl.zero_balance,0) <> 0 then 0
			when isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) < 0 then 0

			when cr.creditor is null then isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
			when cr.creditor = @paf_creditor then 0

			else isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
		end
	)				as 'std_balance',

	sum(
		case
			when cr.creditor is null then 0
			when cr.creditor = @paf_creditor then 0
			when ccl.zero_balance <> 0 then cc.disbursement_factor
			else 0
		end
	)				as 'fee_balance',

	sum(
		case
			when cr.creditor = @paf_creditor then cc.disbursement_factor
			else 0
		end
	)				as 'paf_balance'

into	#ach_disbursement
from	client_creditor cc with (nolock)
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join #ach_deposits c on cc.client = c.client
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where	cc.reassigned_debt	= 0
and	cc.hold_disbursements	= 0
group by cc.client;

-- Add the PAF amount when there is a disbursement
update	#ach_disbursement
set	std_balance = std_balance + paf_balance
where	std_balance > 0;

-- Apply the fees into the balance
update	#ach_disbursement
set	std_balance = std_balance + fee_balance;

CREATE unique index #tmp_ach_disbursement on #ach_disbursement ( client );

-- Reduce the deposit amount by the amount needed for the disbursements
update	#ach_deposits
set	deposit_amount	= b.std_balance
from	#ach_deposits d
inner join #ach_disbursement b on d.client = b.client
where	d.deposit_amount > b.std_balance

-- Include the one-time ach pulls into the mixture. These are performed after the limits to prevent them from taking part in them.
insert into #ach_deposits ( client, deposit_amount, client_deposit )
select	d.client, d.deposit_amount, d.client_deposit
FROM	clients c with (nolock)
INNER JOIN client_deposits d with (nolock) on d.client = c.client AND convert(datetime, convert(varchar(10), d.deposit_date, 101) + ' 00:00:00') = @ACH_Pull_Date
INNER JOIN client_ach ach with (nolock) on c.client = ach.client

-- Ignore client 0 even if it is active
WHERE	c.client > 0

-- Accept only active clients
and	c.active_status in ('A', 'AR')

-- On ACH
and	ach.isActive = 1
AND	ach.ErrorDate IS NULL
AND	ach.PrenoteDate IS NOT NULL
AND	ach.ABA IS NOT NULL
AND	ach.AccountNumber IS NOT NULL
AND	ach.StartDate <= @ach_start_date
AND (ach.EndDate is null or ach.EndDate > @ach_start_date)

-- With a deposit for ACH
-- and	isnull(d.ach_pull,1) <> 0
and	isnull(d.one_time,0) > 0

-- Generate the ACH transactions
insert into deposit_batch_details (tran_subtype,ach_transaction_code,ach_routing_number,ach_account_number,amount,client,deposit_batch_id,ok_to_post)
select	'AC' as tran_subtype,
	case ltrim(rtrim(ach.CheckingSavings))
		when 'S' then 37
			 else 27
	end as ach_transaction_code,
	left(ltrim(rtrim(replace(ach.ABA,' ',''))),9) as ach_routing_number,
	left(ltrim(rtrim(replace(ach.AccountNumber,' ',''))), 17) as ach_bank_number,
	sum(d.deposit_amount) as amount,
	c.client as client,
	@ach_file as deposit_batch_id,
	1 as ok_to_post
FROM	clients c with (nolock)
INNER JOIN #ach_deposits d on d.client = c.client
INNER JOIN client_ach ach with (nolock) on c.client = ach.client
GROUP BY ach.CheckingSavings, ach.ABA, ach.AccountNumber, c.client
HAVING sum(d.deposit_amount) > 0

-- Delete the one-time transactions
DELETE
FROM	client_deposits
WHERE	client_deposit in (
	select	client_deposit
	from	#ach_deposits
	where	client_deposit is not null
)

-- Discard the working tables at this point
drop table #ach_deposits
drop table #ach_disbursement

-- Insert the credit transaction if one is required
declare	@credit_routing_number		varchar(9)
declare	@credit_bank_number		varchar(17)
declare	@credit_amount			money
declare	@enable_offset			bit

-- Fetch the credit account codes if they are defined
select	@credit_routing_number	= aba,
	@credit_bank_number	= account_number,
	@enable_offset		= ach_enable_offset
from	banks with (nolock)
where	bank			= @bank

-- Close the deposit batch to put the deposit amounts into the client
-- This must be done before the credit transaction is inserted to ensure that we deal only with true clients.
execute xpr_deposit_batch_close	@ach_file

-- Generate the credit for the ACH transaction to transfer the money to the agency account
if (@credit_routing_number is not null) and (@credit_bank_number is not null) and (isnull(@enable_offset,0) <> 0)
begin
	-- Find the dollar amount of the transfer
	select	@credit_amount	= sum(amount)
	from	deposit_batch_details with (nolock)
	where	deposit_batch_id = @ach_file
	and	ach_transaction_code in ('27', '37')

	if @credit_amount is null
		select	@credit_amount = 0

	-- Insert the transaction for the credit operation
	if @credit_amount > 0
		insert into deposit_batch_details (ach_transaction_code,ach_routing_number,ach_account_number,amount,client,deposit_batch_id,tran_subtype,ok_to_post)
		values ('22', @credit_routing_number, @credit_bank_number, @credit_amount, 0, @ach_file, 'AC', 0)
end

-- Finalize the operations now
COMMIT TRANSACTION

-- Return the number of items inserted
RETURN ( @@rowcount )
GO
