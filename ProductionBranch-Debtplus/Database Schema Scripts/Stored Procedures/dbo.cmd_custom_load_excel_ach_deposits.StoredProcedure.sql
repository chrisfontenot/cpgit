USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_custom_load_excel_ach_deposits]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_custom_load_excel_ach_deposits] as

set nocount on

-- Create a working table to hold the deposit batch IDs
create table #deposit_batches ( deposit_batch_id int null, description varchar(80))
insert into #deposit_batches ( description )
select distinct [Batch #]
from	ach_input
where	[Batch #] not like '% Total'
order by 1

-- Create a list of deposit batches for each seperate batch in the original table
declare batch_cursor cursor for
	select	description
	from	#deposit_batches
	order by 1

declare	@description	varchar(80)
open	batch_cursor
fetch	batch_cursor into @description
while @@fetch_status = 0
begin
	declare	@deposit_batch_id	int
	declare	@updated_description	varchar(50)
	select	@updated_description = 'Import of ACH file - ' + @description
	execute @deposit_batch_id = xpr_deposit_batch_create @updated_description, 'CL', 1

	update	#deposit_batches
	set	deposit_batch_id	= @deposit_batch_id
	where	description		= @description

	fetch	batch_cursor into @description
end
close	batch_cursor
deallocate batch_cursor

-- Load the transactions into the batch from the input table
declare	detail_cursor cursor for
	select	convert(datetime, substring(i.[Date], 5, 2) + '/' + substring(i.[Date], 7, 2) + '/' + substring(i.[Date], 1, 4) + ' 00:00:00') as [Date],
		i.ClientID,
		i.Amount,
		b.deposit_batch_id
	from	ach_input i
	inner join #deposit_batches b on i.[Batch #] = b.description
	order by 4, 1, 2, 3

declare	@input_date	datetime
declare	@input_client	int
declare	@input_amount	money
declare	@input_batch	int
		
open	detail_cursor
fetch	detail_cursor into @input_date, @input_client, @input_amount, @input_batch
while @@fetch_status = 0
begin
	execute xpr_deposit_batch_entry_LB @input_batch, @input_client, @input_date, @input_amount, NULL
	fetch	detail_cursor into @input_date, @input_client, @input_amount, @input_batch
end
close	detail_cursor
deallocate detail_cursor

-- Discard the list of batches
drop table #deposit_batches

-- Empty the input table once the import is complete
truncate table ach_input

print 'Operation completed'

set nocount off
return ( 1 )
GO
