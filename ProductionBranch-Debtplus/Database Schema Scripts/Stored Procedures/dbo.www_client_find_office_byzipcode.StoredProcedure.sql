USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_find_office_byzipcode]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_find_office_byzipcode] ( @zipcode as varchar(50) ) AS

-- ============================================================================================================
-- ==            Interface procedure to xpr_find_office_byZipcode for the web pages                          ==
-- ============================================================================================================

declare	@count		int
Execute	@count = xpr_find_office_ByZipcode @zipcode
return ( @count )
GO
