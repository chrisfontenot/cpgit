USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Create_Creditor_Billing]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Create_Creditor_Billing] ( @billing_a AS int = -1, @billing_s AS int = -1, @billing_q as int = -1, @billing_m as int = -1, @billing_w as int = -1, @ignore_billed as int = 0) AS

-- =====================================================================================================
-- ==            Create invoices from the miscallenous transactions                                   ==
-- =====================================================================================================

-- Suppress intermediate results
SET NOCOUNT ON
SET CURSOR_CLOSE_ON_COMMIT OFF

-- Construct the selection clause for the list of possible creditors choosen to be built
declare	@stmt			varchar(8000)
set @stmt = ''

-- include the annually cycle
if @billing_a >= 0
begin
	set @stmt = @stmt + ' or (contrib_cycle = ''A'''
	if @billing_a > 0
		set @stmt = @stmt + ' and contrib_bill_month=' + convert(varchar,@billing_a)
	set @stmt = @stmt + ')'
end

-- include the semi-annually cycle
if @billing_s >= 0
begin
	set @stmt = @stmt + ' or (contrib_cycle = ''S'''
	if @billing_s > 0
		set @stmt = @stmt + ' and contrib_bill_month=' + convert(varchar,@billing_s)
	set @stmt = @stmt + ')'
end

-- include the quarterly cycle
if @billing_q >= 0
begin
	set @stmt = @stmt + ' or (contrib_cycle = ''Q'''
	if @billing_q > 0
		set @stmt = @stmt + ' and contrib_bill_month=' + convert(varchar,@billing_q)
	set @stmt = @stmt + ')'
end

-- include the monthly cycle
if @billing_m >= 0
begin
	set @stmt = @stmt + ' or (contrib_cycle = ''M'''
	if @billing_m > 0
		set @stmt = @stmt + ' and contrib_bill_month=' + convert(varchar,@billing_m)
	set @stmt = @stmt + ')'
end

-- include the weekly cycle
if @billing_w >= 0
begin
	set @stmt = @stmt + ' or (contrib_cycle = ''W'''
	if @billing_w > 0
		set @stmt = @stmt + ' and contrib_bill_month=' + convert(varchar,@billing_w)
	set @stmt = @stmt + ')'
end

-- There should be some cycle definitions
if @stmt = ''
begin
	RaisError (50026, 10, 1)
	Return ( 0 )
end

-- Create the temporary table with the possible creditors in the list
create table #t_billing_creditors (
	creditor		varchar(10),
	min_accept_per_bill	money,
	chks_per_invoice	int
);

-- Create an index for the table
create clustered index ix1_cmd_Create_Creditor_Billing on #t_billing_creditors ( creditor );

-- Construct the list of possible creditors who wish to be billed in this cycle
set @stmt = 'insert into #t_billing_creditors (creditor, min_accept_per_bill, chks_per_invoice) select cr.creditor, case when coalesce(cr.chks_per_invoice, cfg.chks_per_invoice, 0) > 0 then 0 else isnull(cr.min_accept_per_bill, 0) end, coalesce(cr.chks_per_invoice, cfg.chks_per_invoice, 0) from creditors cr, config cfg WHERE (creditor_type != ''B'') OR (' + substring(@stmt, 5, 4000) + ')'
exec ( @stmt )

-- Complain if there are no creditors in the list
if not exists ( select top 1 * from #t_billing_creditors )
begin
	RaisError (50027, 16, 1 )
	Return ( 0 )
end

-- Construct the list of outstanding bills for the transactions
if @ignore_billed = 0
begin
	select		creditor, sum(fairshare_amt) as 'outstanding_bills'
	into		#t_billing_counts
	from		registers_client_creditor rcc
	where		invoice_register IS NULL
	AND		creditor_type = 'B'
	AND		fairshare_amt > 0
	AND		void = 0
	GROUP BY	creditor
	ORDER BY	creditor

	-- Create an index for the table
	create clustered index ix2_cmd_Create_Creditor_Billing on #t_billing_counts ( creditor );

	-- Generate the bills into summary tables
	DECLARE	invoice_cursor CURSOR FORWARD_ONLY READ_ONLY FOR
		SELECT		d.client_creditor_register,
				d.creditor,
				d.fairshare_amt,
				d.trust_register,
				d.invoice_register,
				t.chks_per_invoice
		FROM		registers_client_creditor d
		INNER JOIN	#t_billing_creditors t WITH (NOLOCK) ON d.creditor = t.creditor
		INNER JOIN	#t_billing_counts c WITH (NOLOCK) ON t.creditor = c.creditor
		WHERE		t.min_accept_per_bill < c.outstanding_bills
		AND		d.invoice_register IS NULL
		AND		d.creditor_type = 'B'
		AND		d.fairshare_amt > 0
		AND		d.void = 0
		ORDER BY	d.creditor, d.trust_register;
end else begin
	-- Generate the bills into summary tables
	DECLARE	invoice_cursor CURSOR FORWARD_ONLY READ_ONLY FOR
		SELECT		d.client_creditor_register,
				d.creditor,
				d.fairshare_amt,
				d.trust_register,
				d.invoice_register,
				t.chks_per_invoice
		FROM		registers_client_creditor d
		INNER JOIN	#t_billing_creditors t WITH (NOLOCK) ON d.creditor = t.creditor
		WHERE		d.invoice_register IS NULL
		AND		d.creditor_type = 'B'
		AND		d.fairshare_amt > 0
		AND		d.void = 0
		ORDER BY	d.creditor, d.trust_register;
end

declare	@creditor			typ_creditor
declare	@last_creditor			typ_creditor
declare	@fairshare_amt			money
declare	@inv_amount			money
declare	@trust_register			int
declare	@chks_per_invoice		int
declare	@update_trust			int
declare	@client_creditor_register	int

declare	@create_new_invoice		bit
declare	@invoice_register		int
declare	@input_invoice_register		int
declare	@chks_processed			int
declare	@last_check			int

select	@create_new_invoice	= 1,
	@chks_processed		= 0,
	@invoice_register	= 0,
	@update_trust		= 0,
	@inv_amount		= 0

-- Generate the invoices and update the records to point to the new invoices
OPEN invoice_cursor
fetch invoice_cursor into @client_creditor_register, @creditor, @fairshare_amt, @trust_register, @input_invoice_register, @chks_per_invoice

-- The "last check" processed is the current check
set @last_check	= @trust_register

-- Start a transaction for the update at this point
begin transaction

-- Abort on an error
set xact_abort on

WHILE @@fetch_status = 0
BEGIN

	-- Count the number of checks processed
	if @last_check != @trust_register
	begin
		SELECT	@chks_processed	= @chks_processed + 1,
			@update_trust	= @trust_register,
			@last_check	= @trust_register

		-- If the limit is reached for the number of checks on a creditor, generate a new invoice
		if @chks_per_invoice > 0
		begin
			if @chks_processed > @chks_per_invoice
				set @create_new_invoice = 1
		end
	end

	-- When the creditor changes, generate a new invoice
	if @last_creditor != @creditor
		set @create_new_invoice = 1

	set @last_creditor = @creditor

	-- Create the new record for the creditor invoice
	if @create_new_invoice > 0
	begin
		if (@invoice_register > 0) and (@inv_amount > 0)
			update	registers_invoices
			set	inv_amount		= @inv_amount
			where	invoice_register	= @invoice_register

		-- Close the current transaction and start a new one.
		commit transaction
		begin transaction

		insert into registers_invoices	(creditor,	inv_amount,	inv_date)
		values				(@creditor,	0,		getdate())

		select	@invoice_register	= SCOPE_IDENTITY(),
			@chks_processed		= 1,
			@inv_amount		= 0,
			@create_new_invoice	= 0
	end

	-- Update the trust register to reflect the invoice pointer
	if @update_trust > 0
	begin
		update	registers_trust
		set	invoice_register	= @invoice_register
		where	trust_register		= @update_trust

		set @update_trust = 0
	end

	-- Accumulate the amount into the invoice total
	set	@inv_amount = @inv_amount + @fairshare_amt

	-- Write the invoice register into the detail table
	update	registers_client_creditor
	set	invoice_register		= @invoice_register
	where	client_creditor_register	= @client_creditor_register

	-- Go on to the next pending item
	fetch invoice_cursor into @client_creditor_register, @creditor, @fairshare_amt, @trust_register, @input_invoice_register, @chks_per_invoice
end

-- Update the final invoice total
if @invoice_register > 0
begin
	update	registers_invoices
	set	inv_amount		= @inv_amount
	where	invoice_register	= @invoice_register
end

-- Close the current transaction and start a new one.
commit transaction

-- Close the cursor
close		invoice_cursor
deallocate	invoice_cursor

-- Discard working tables
if @ignore_billed = 0
	drop table #t_billing_counts

drop table #t_billing_creditors
return ( 1 )
GO
