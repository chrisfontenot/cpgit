USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_manual_check]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_manual_check] ( @client_creditor as int, @amount as money, @creditor_type as char(1) = NULL, @message as typ_message = NULL ) AS

-- =============================================================================================================
-- ==                Generate a manual check for a specific client/creditor/debt and amount                   ==
-- =============================================================================================================

-- ChangeLog
--   1/13/2001
--     Changed because the "first_payment_date", "first_payment_amount", and "last_payment_date" was moved
--     to the registers_client_creditor based upon first_payment and last_payment pointers.
--   1/19/2001
--     Added support for max_fairshare_per_check from the creditor to limit the fairshare to the maximum allowed
--   2/18/2002
--     Added client 0 transaction to record the change in the deduct balance.
--   9/31/2002
--     Added support for client_creditor_balances table
--   10/15/2002
--     Added support for client_creditor's "*_this_creditor" information
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.

-- Disable intermediate results
SET NOCOUNT ON

-- Take the money from the client's account
BEGIN TRANSACTION
SET XACT_ABORT ON

DECLARE	@client						int
DECLARE	@creditor					varchar(10)
DECLARE	@trust_balance			money
DECLARE	@fairshare_amt			money
DECLARE	@billed				money
DECLARE	@deducted			money
DECLARE	@net				money
DECLARE	@account_number			typ_client_account
DECLARE	@fairshare_pct_check		float
DECLARE	@disbursement_register		int
DECLARE	@trust_register			int
DECLARE	@client_creditor_register	int
DECLARE	@creditor_register		int
DECLARE	@max_fairshare_per_check	money
DECLARE @client_creditor_balance	int

-- Fetch the information for the current debt record
SELECT	@account_number			= account_number,
		@client					= client,
		@creditor				= creditor,
		@fairshare_pct_check	= fairshare_pct_check,
		@client_creditor_balance = client_creditor_balance
FROM	client_creditor WITH (NOLOCK)
WHERE	client_creditor		= @client_creditor

-- Ensure that the debt is present.
if @client is null
BEGIN
	Rollback Transaction
	RaisError(50076, 16, 1, @client, @creditor, @client_creditor)
	Return ( 0 )
END

SELECT	@trust_balance = isnull(held_in_trust,0)
FROM	clients
WHERE	client = @client

-- A null value for the trust is indicative of no client
IF @@rowcount < 1
BEGIN
	ROLLBACK TRANSACTION
	RaisError(50014, 16, 1, @client)
	Return ( 0 )
END

-- Ensure that there is sufficient funds for the check
if @trust_balance < @amount
BEGIN
	ROLLBACK TRANSACTION
	RaisError(50064, 16, 1, @client)
	Return ( 0 )
END

-- Remove the money from the client's trust balance
UPDATE	clients
SET	held_in_trust = held_in_trust - @amount
WHERE	client = @client

SELECT	@trust_balance = held_in_trust
FROM	clients
WHERE	client = @client

IF @trust_balance < 0
BEGIN
	ROLLBACK TRANSACTION
	RaisError(50064, 16, 1, @client)
	Return ( 0 )
END

-- Fetch the creditor information to override debt information. NULL in the debt means "use the creditor info".
SELECT	@creditor_type			= pct.creditor_type_check,
	@fairshare_pct_check		= isnull(@fairshare_pct_check, pct.fairshare_pct_check)
FROM	creditors cr WITH (NOLOCK)
left outer join creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
WHERE	cr.creditor			= @creditor

if @max_fairshare_per_check is null
	select	@max_fairshare_per_check	= 0.0

-- Make the percentage between 0.0 and 1.0
if @fairshare_pct_check < 0.0
	select @fairshare_pct_check = 0.0

WHILE @fairshare_pct_check >= 1.0
	select @fairshare_pct_check = @fairshare_pct_check / 100.0

-- Generate the proper information for this transaction
SELECT	@fairshare_amt	= convert(money, convert(decimal(10,2), @amount * @fairshare_pct_check)),
	@billed		= 0,
	@deducted	= 0

-- Limit the fairshare to the maximum limit for this transaction
IF @max_fairshare_per_check > 0
begin
	IF @max_fairshare_per_check < @fairshare_amt
		SELECT @fairshare_amt = @max_fairshare_per_check
end

IF @creditor_type = 'D'
	SELECT	@deducted	= @fairshare_amt
ELSE IF @creditor_type = 'B'
	SELECT	@billed		= @fairshare_amt

SELECT @net = @amount - @deducted

-- Create the disbursement register
INSERT INTO registers_disbursement (tran_type) VALUES ('CM')
SELECT	@disbursement_register = SCOPE_IDENTITY()

-- Create the trust tregister with the creditor check
INSERT INTO registers_trust 	(tran_type,	creditor,	amount,	cleared,	check_order)
VALUES				('CM',		@creditor,	@net,	'P',		0)
SELECT	@trust_register = SCOPE_IDENTITY()

-- Insert the client/creditor transaction with the details
INSERT INTO registers_client_creditor	(tran_type,	client,		creditor,	client_creditor,	debit_amt,	trust_register,		disbursement_register,	fairshare_pct,		creditor_type,	fairshare_amt,	account_number,		invoice_register)
VALUES									('CM',		@client,	@creditor,	@client_creditor,	@amount,	@trust_register,	@disbursement_register,	@fairshare_pct_check,	@creditor_type,	@fairshare_amt,	@account_number,	null)
SELECT	@client_creditor_register = SCOPE_IDENTITY()

-- Insert the client transaction with the details
INSERT INTO registers_client	(tran_type,	client,		debit_amt,	trust_register,		disbursement_register,	message)
VALUES				('CM',		@client,	@amount,	@trust_register,	@disbursement_register,	@message)

-- Insert the creditor transaction with the details
INSERT INTO registers_creditor	(tran_type,	creditor,	debit_amt,	trust_register,		disbursement_register,	invoice_register)
VALUES				('CM',		@creditor,	@net,		@trust_register,	@disbursement_register,	null)
select	@creditor_register = SCOPE_IDENTITY()

-- If there is a deduction amount then credit the deduction account
IF @deducted > 0
BEGIN
	UPDATE	clients
	SET	held_in_trust	= isnull(held_in_trust,0) + @deducted
	WHERE	client		= 0

	insert into registers_client	(tran_type,	client,	credit_amt,	disbursement_register)
	values				('CM',		0,	@deducted,	@disbursement_register)
END

-- Record the debt information
update	client_creditor
set	first_payment		= isnull(first_payment, @client_creditor_register),
	last_payment		= @client_creditor_register,
	payments_this_creditor	= isnull(payments_this_creditor,0) + @amount
where	client_creditor		= @client_creditor

update	client_creditor_balances
set	total_payments		= total_payments + @amount,
	payments_month_0	= payments_month_0 + @amount
where	client_creditor_balance	= @client_creditor_balance

-- Record the creditor information
update	creditors
set	distrib_mtd		= isnull(distrib_mtd,0) + @amount,
	contrib_mtd_billed	= isnull(contrib_mtd_billed,0) + @billed,
	contrib_mtd_received	= isnull(contrib_mtd_received,0) + @deducted,
	first_payment		= isnull(first_payment, @creditor_register),
	last_payment		= @creditor_register
where	creditor		= @creditor

-- Complete the processing and return the trust register for the check
COMMIT TRANSACTION

RETURN ( @trust_register )
GO
