USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_transfer_non_ar]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_transfer_non_ar] ( @source as typ_key, @ledger_code AS typ_glaccount, @Amount AS Money, @EffectiveDate AS DateTime = NULL, @reference as typ_description, @message AS typ_message = NULL ) AS

-- ===============================================================================================
-- ==            Generate a deposit into the non-ar source                                      ==
-- ===============================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

IF @Amount < 0
BEGIN
	RaisError(50019, 16, 1)
	RETURN ( 0 )
END

-- If there is a record then update the record
insert into registers_non_ar	(tran_type,	non_ar_source,	dst_ledger_account,	credit_amt,	message,	reference,	item_date)
values				('NA',		@source,	@ledger_code,		@amount,	@message,	@reference,	@effectivedate)

RETURN ( SCOPE_IDENTITY() )
GO
