USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[CMD_Set_Permissions_cccs_admin]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CMD_Set_Permissions_cccs_admin] as

grant execute on [cccs_admin_account_number] to public;
grant execute on [cccs_admin_address] to public;
grant execute on [cccs_admin_gender] to public;
grant execute on [cccs_admin_housing] to public;
grant execute on [cccs_admin_intake_appointment] to public;
grant execute on [cccs_admin_intake_budget_debts] to public;
grant execute on [cccs_admin_intake_budget_deposits] to public;
grant execute on [cccs_admin_intake_budget_expenses] to public;
grant execute on [cccs_admin_intake_budgets] to public;
grant execute on [cccs_admin_intake_coapplicant] to public;
grant execute on [cccs_admin_intake_import] to public;
grant execute on [cccs_admin_intake_import_single] to public;
grant execute on [cccs_admin_map_budgets] to public;
grant execute on [cccs_admin_map_perferred_contact] to public;
grant execute on [cccs_admin_map_relation] to public;
grant execute on [cccs_admin_marital] to public;
grant execute on [cccs_admin_postalcode] to public;
grant execute on [cccs_admin_problem] to public;
grant execute on [cccs_admin_rates] to public;
grant execute on [cccs_admin_referral] to public;
grant execute on [cccs_admin_ssn] to public;
grant execute on [cccs_admin_state] to public;
grant execute on [cccs_admin_telephone] to public;
GO
