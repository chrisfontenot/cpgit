USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Recon_Item]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_Recon_Item] ( @TrustRegister as INT, @ReconDate as DateTime = NULL, @Batch as VarChar(80) = Null, @ReconAmount as Money = NULL, @Type as char(1) = NULL ) AS

-- ========================================================================================================================
-- ==            Mark the item as reconciled                                                                             ==
-- ========================================================================================================================

-- ChangeLog
--   12/12/2001
--     Supported marking items as "un-reconciled" by setting the type to ' '.

SET NOCOUNT ON

IF @type IS NULL
	SET @Type = 'R'

-- The type must be one character and no more.
select	@type = upper(left(@type + ' ',1))

-- Do not allow people to be "stupid".
if @type = 'V'
begin
	RaisError ('The type ''V'' is not allowed since this would indicate a voided check/deposit.', 16, 1)
	return ( 0 )
end

-- If the item is to be un-reconciled then clear the fields
if @type = ' '
begin
	UPDATE	registers_trust
	SET	cleared			= ' ',
		reconciled_date		= null,
		sequence_number		= null
	WHERE	trust_register		= @TrustRegister
	AND	cleared			!= 'V'

end else begin

	-- If there is no date given then use the current date
	IF @ReconDate IS NULL
		SET @ReconDate = getdate()

	-- Ensure that there is no reference to a time
	SET @ReconDate = convert(datetime, convert(varchar(10), @ReconDate, 101) + ' 00:00:00')

	IF @ReconAmount IS NULL
	BEGIN
		-- If there is no reconciled amount then use the amount of the item for the reconciled value
		UPDATE	registers_trust
		SET	cleared			= @Type,
			reconciled_date		= @ReconDate,
			sequence_number		= @Batch
		WHERE	trust_register		= @TrustRegister
		AND	cleared			!= 'V'

	END ELSE BEGIN

		-- There is an amount. Use it.
		UPDATE	registers_trust
		SET	cleared			= @Type,
			reconciled_date		= @ReconDate,
			sequence_number		= @Batch
		WHERE	trust_register		= @TrustRegister
		AND	cleared			!= 'V'
	END
END

RETURN ( @@rowcount )
GO
