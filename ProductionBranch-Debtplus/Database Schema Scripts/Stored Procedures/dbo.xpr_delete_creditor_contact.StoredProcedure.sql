USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_creditor_contact]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_creditor_contact] ( @creditor_contact as int) as
-- =================================================================================
-- ==      Delete a creditor contact from the system                              ==
-- =================================================================================

delete
from	creditor_contacts
WHERE	[creditor_contact] = @creditor_contact;

return ( @@ROWCOUNT )
GO
