SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_housing_arm_v4_counselors] as

-- Suppress intermediate results
set nocount on

-- Clear the partial table if needed
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##hud_9902_counselors'' and type = ''U'') drop table ##hud_9902_counselors' )

-- Change the counselors to a suitable "good" counselor for the report. The clients can't point
-- to counselors that are not included in the submission.
declare	@default_counselor	int
select	@default_counselor = min(counselor)
from	view_counselors
where	[ActiveFlag] = 1
and		[Default] = 1

-- There should be a counselor. If not, use zero.
if @default_counselor is null
	select	@default_counselor = min(counselor)
	from	view_counselors
	where	[Default] = 1

-- There should be a counselor. If not, use zero.
if @default_counselor is null
	select	@default_counselor = min(counselor)
	from	view_counselors
	where	[ActiveFlag] = 1

-- There should be a counselor. If not, use zero.
if @default_counselor is null
	select	@default_counselor = 1

update	##hud_9902_clients
set		client_counselor_id = @default_counselor
from	##hud_9902_clients x
left outer join counselors co with (nolock) on x.client_counselor_id = co.counselor
where	co.counselor is null

create table ##hud_9902_counselors (
	cms_counselor_id		int null,
	cnslor_fname			varchar(80) null,
	cnslor_lname			varchar(80) null,
	cnslor_mname			varchar(80) null,
	cnslor_emp_start_date	datetime null,
	cnslor_emp_end_date		datetime null,
	cnslor_rate				decimal null,
	cnslor_billing_method	varchar(20) null,
	cnslor_HUD_id			int null,
	cnslor_SSN				varchar(20) null,
	cnslor_phone			varchar(20) null,
	cnslor_email			varchar(80) null,
	is_counselor			int null
);

insert into ##hud_9902_counselors (cms_counselor_id, cnslor_fname, cnslor_lname, cnslor_mname, cnslor_emp_start_date, cnslor_emp_end_date, cnslor_rate, cnslor_billing_method, cnslor_HUD_id, cnslor_SSN, cnslor_phone, cnslor_email, is_counselor)
select	co.counselor						as cms_counselor_id,
		ltrim(rtrim(isnull(n.first,'')))	as cnslor_fname,
		ltrim(rtrim(isnull(n.last,'')))		as cnslor_lname,
		ltrim(rtrim(isnull(n.middle,'')))   as cnslor_mname,
		emp_start_date						as cnslor_emp_start_date,
		emp_end_date						as cnslor_emp_end_date,
		rate								as cnslor_rate,
		isnull(billing_mode,'Fixed')		as cnslor_billing_method,
		hud_id								as cnslor_HUD_id,
		dbo.format_ssn(SSN)					as cnslor_ssn,
		dbo.Format_TelephoneNumber_BaseOnly(TelephoneID) as cnslor_phone,
		e.Address							as cnslor_email,
		0									as is_counselor
from	counselors co
inner join names n on co.nameid = n.name
left outer join EmailAddresses e on co.EmailID = e.Email
where	co.ActiveFlag	<> 0

-- Look for referenced counselors in the client list table
update		##hud_9902_counselors
set			is_counselor	= 1
from		##hud_9902_counselors r
inner join	##hud_9902_clients c on r.cms_counselor_id = c.client_counselor_id
where		c.is_client		= 1

-- Look for the referenced counselors as educators
update		##hud_9902_counselors
set			is_counselor	= 1
from		##hud_9902_counselors r
inner join	##hud_9902_clients c on r.cms_counselor_id = c.client_counselor_id
where		c.is_workshop	= 1;

-- Look for items referenced in the workshop list
update		##hud_9902_counselors
set			is_counselor	= 1
from		##hud_9902_counselors co
inner join ##hud_9902_workshops w on co.cms_counselor_id = w.group_session_counselor_id;

-- Toss everyone else
delete
from		##hud_9902_counselors
where		is_counselor	= 0
	
			-- Counselor information
select		co.cms_counselor_id,
			co.cnslor_fname,
			co.cnslor_lname,
			co.cnslor_mname,
			co.cnslor_emp_start_date,
			co.cnslor_emp_end_date,
			co.cnslor_rate,
			co.cnslor_billing_method,
			co.cnslor_HUD_id,
			co.cnslor_SSN,
			co.cnslor_phone,
			co.cnslor_email,

			-- Training information
			t.[oID]								as 'training_ID',
			t.[TrainingDate]					as 'training_date',
			c.[Certificate]						as 'training_certificate',
			c.[TrainingTitle]					as 'training_title',
			c.[TrainingDuration]				as 'training_duration',
			dbo.map_hud_9902_training_organization(c.[TrainingOrganization]) as 'training_training_org',
			c.[TrainingOrganizationOther]		as 'training_training_org_other',
			dbo.map_hud_9902_training_sponsor(c.[TrainingSponsor]) as 'training_sponsor',
			c.[TrainingSponsorOther]			as 'training_sponsor_other'

from		##hud_9902_counselors co
left outer join housing_counselor_training_courses t on co.cms_counselor_id = t.counselor
left outer join housing_training_courses c on t.TrainingCourse = c.oID
order by co.cms_counselor_id;

return ( @@rowcount )

GO
