USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_update_expense]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_update_expense] ( @intake_client as int, @intake_id as varchar(20),
	@fed_tax_owed as money,		@fed_tax_months		as int,
	@state_tax_owed as money,	@state_tax_months	as int,
	@local_tax_owed as money,	@local_tax_months	as int,
	@car_payment_current as int,
	@auto_arrears_amt as money,	@auto_arrears_months	as int ) as

-- ==============================================================================================================
-- ==            Start the processing on the new expense information                                           ==
-- ==============================================================================================================

-- Set the proper indicator flags
set nocount	on
set xact_abort	on

-- Update the client with the information
update	intake_clients

set	fed_tax_owed		= @fed_tax_owed,
	fed_tax_months		= @fed_tax_months,
	state_tax_owed		= @state_tax_owed,
	state_tax_months	= @state_tax_months,
	local_tax_owed		= @local_tax_owed,
	local_tax_months	= @local_tax_months,
	car_payment_current	= @car_payment_current,
	auto_arrears_amt	= @auto_arrears_amt,
	auto_arrears_months	= @auto_arrears_months

where	intake_client		= @intake_client
and	intake_id		= @intake_id

-- Verify that the client existed with the appropriate ID
if @@rowcount < 1
begin
	RaisError ('The client ID is not valid', 16, 1)
	return ( 0 )
end

return ( 1 )
GO
