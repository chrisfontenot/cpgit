USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_client_creditor_balances]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_client_creditor_balances] AS
/*
-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table

-- Rename the columns in the client_creditor table to break old references
execute sp_rename 'client_creditor.orig_balance', 'old_orig_balance', 'COLUMN'
execute sp_rename 'client_creditor.orig_balance_adjustment', 'old_orig_balance_adjustment', 'COLUMN'
execute sp_rename 'client_creditor.total_payments', 'old_total_payments', 'COLUMN'
execute sp_rename 'client_creditor.total_interest', 'old_total_interest', 'COLUMN'
execute sp_rename 'client_creditor.denorm_current_month_disbursement', 'old_denorm_current_month_disbursement', 'COLUMN'
execute sp_rename 'client_creditor.denorm_last_month_disbursement', 'old_denorm_last_month_disbursement', 'COLUMN'
execute sp_rename 'client_creditor.denorm_2nd_month_disbursement', 'old_denorm_2nd_month_disbursement', 'COLUMN'
execute sp_rename 'client_creditor.denorm_3rd_month_disbursement', 'old_denorm_3rd_month_disbursement', 'COLUMN'
execute sp_rename 'client_creditor.current_sched_payment', 'old_current_sched_payment', 'COLUMN'
execute sp_rename 'client_creditor.total_sched_payment', 'old_total_sched_payment', 'COLUMN'
execute sp_rename 'client_creditor.nccrc_debt_number', 'client_creditor_balance', 'COLUMN'

-- Empty the table
truncate table client_creditor_balances

-- Insert the debt numbers from the existing table
set identity_insert client_creditor_balances on
insert into client_creditor_balances (client_creditor_balance)
select distinct client_creditor_balance
from client_creditor
where client_creditor_balance > 0
set identity_insert client_creditor_balances off;
dbcc checkident (client_creditor_balances)

-- Assign the current item to the debt
update	client_creditor_balances
set	client_creditor	= cc.client_creditor
from	client_creditor_balances b
inner join client_creditor cc on b.client_creditor_balance = cc.client_creditor_balance
where	cc.reassigned_debt = 0

-- Deal with the staglers that have been left due to the conversion from CPR and others
update	client_creditor_balances
set	client_creditor	= cc.client_creditor
from	client_creditor_balances b
inner join client_creditor cc on b.client_creditor_balance = cc.client_creditor_balance
where	b.client_creditor = 0

-- Include the balance information
update	client_creditor_balances
set	orig_balance		= cc.old_orig_balance,
	orig_balance_adjustment	= cc.old_orig_balance_adjustment,
	total_payments		= cc.old_total_payments,
	total_interest		= cc.old_total_interest,
	payments_month_0	= cc.old_denorm_current_month_disbursement,
	payments_month_1	= cc.old_denorm_last_month_disbursement,
	payments_month_2	= cc.old_denorm_2nd_month_disbursement,
	payments_month_3	= cc.old_denorm_3rd_month_disbursement,
	current_sched_payment	= cc.old_current_sched_payment,
	total_sched_payment	= cc.old_total_sched_payment,
	created_by		= cc.created_by,
	date_created		= cc.date_created
from	client_creditor_balances b
inner join client_creditor cc on b.client_creditor = cc.client_creditor
*/
RaisError ('This procedure is run only once. Please do not use it again.', 16, 1)
GO
