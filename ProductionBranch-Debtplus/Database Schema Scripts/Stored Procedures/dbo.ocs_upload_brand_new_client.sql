USE [DebtPlus]
GO

/****** Object:  StoredProcedure [dbo].[ocs_upload_brand_new_client]    Script Date: 6/22/2015 10:47:03 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Brandon Wilhite
-- Create date: 
-- Description:	Take values from fmac ei spreadsheet and create correct database entries for upload
-- =============================================
/*
1. Insert address 
2. Insert phone number rows
3. Insert person names, as appropriate
4. Insert client (FK address)
5. Insert person records, as appropriate (FK phone number, name, client)
6. Insert servicer/lender chain (FK person)
7. Insert OCS Client (FK client)
*/
ALTER PROCEDURE [dbo].[ocs_upload_brand_new_client] (

	@program int,
	@archive bit,						--bool Archive, 
    @activeFlag bit,					--bool ActiveFlag, 
    @isDuplicate bit,					--bool IsDuplicate, 
    @uploadedRecordId int,				--int UploadRecordId, 
	@uploadAttempt varchar(50) = NULL,	--actually a guid
    @investorNo varchar(20),			--string InvestorNo, 

    @servicer varchar(50) = NULL,			--string Servicer, 
    @servicerLoan varchar(50) = NULL,		--string ServicerNo,
	 
    @lastChanceList datetime = NULL,		--DateTime LastChanceList, 
    @actualSaleDate datetime = NULL,		--DateTime ActualSaleDAte, 
    @dueDate datetime = NULL,				--DateTime DueDate, 

	@queueCode varchar(10) = NULL,		--string QueueCode

	@street varchar(50),				--string Street,
	@city varchar(50),					--string City,
	@state int,							--int State,
	@zipcode varchar(50),				--string Zipcode,
            
    @hasPerson1 bit,					--bool HasPerson1, 
    @firstName1 varchar(50) = NULL,		--string FirstName1, 
    @lastName1 varchar(50) = NULL,		--string LastName1, 
    @SSN1 varchar(50) = NULL,				--string SSN1, 
            
    @hasPerson2 bit,					--bool HasPerson2,
    @firstName2 varchar(50) = NULL,		--string FirstName1, 
    @lastName2 varchar(50) = NULL,		--string LastName1, 
    @SSN2 varchar(50) = NULL,				--string SSN1, 
	
	@p1area varchar(50) = NULL, @p1num varchar(50) = NULL,
	@p2area varchar(50) = NULL, @p2num varchar(50) = NULL,
	@p3area varchar(50) = NULL, @p3num varchar(50) = NULL,
	@p4area varchar(50) = NULL, @p4num varchar(50) = NULL,
	@p5area varchar(50) = NULL, @p5num varchar(50) = NULL,
	@p6area varchar(50) = NULL, @p6num varchar(50) = NULL,	

	@Inserted int OUTPUT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin transaction

	begin try

		--INSERT ADDRESS
		INSERT INTO addresses (PostalCode,street,address_line_2,city,state)
		VALUES (@zipcode,@street,'',@city,@state)

		declare @Address int
		set @Address = SCOPE_IDENTITY()

		--INSERT PHONE NUMBERS
		declare @p1 int
		set @p1 = NULL
		if @p1area is not null
		begin
			insert into TelephoneNumbers (Country,Acode,Number)
			values (1,@p1area,@p1num)
			set @p1 = SCOPE_IDENTITY()			
		end
		declare @p2 int
		set @p2 = NULL
		if @p2area is not null
		begin
			insert into TelephoneNumbers (Country,Acode,Number)
			values (1,@p2area,@p2num)
			set @p2 = SCOPE_IDENTITY()			
		end
		declare @p3 int
		set @p3 = NULL
		if @p3area is not null
		begin
			insert into TelephoneNumbers (Country,Acode,Number)
			values (1,@p3area,@p3num)
			set @p3 = SCOPE_IDENTITY()			
		end
		declare @p4 int
		set @p4 = NULL
		if @p4area is not null
		begin
			insert into TelephoneNumbers (Country,Acode,Number)
			values (1,@p4area,@p4num)
			set @p4 = SCOPE_IDENTITY()			
		end
		declare @p5 int
		set @p5 = NULL
		if @p5area is not null
		begin
			insert into TelephoneNumbers (Country,Acode,Number)
			values (1,@p5area,@p5num)
			set @p5 = SCOPE_IDENTITY()			
		end		
		declare @p6 int
		set @p6 = NULL
		if @p6area is not null
		begin
			insert into TelephoneNumbers (Country,Acode,Number)
			values (1,@p6area,@p6num)
			set @p6 = SCOPE_IDENTITY()			
		end

		--INSERT NAMES
		declare @person1 int
		declare @name1 int
		set @person1 = NULL
		set @name1 = NULL
		if @hasPerson1 = 1
		begin
			insert into Names(First,Last)
			values (@firstName1,@lastName1)
			set @name1 = SCOPE_IDENTITY()
		end

		declare @person2 int
		declare @name2 int
		set @person2 = NULL
		set @name2 = NULL
		if @hasPerson2 = 1
		begin
			insert into Names(First,Last)
			values (@firstName2,@lastName2)
			set @name2 = SCOPE_IDENTITY()
		end

		--INSERT CLIENT
		declare @client int
		set @client = NULL

		--previously existing create_client procedure
		DECLARE	@return_value int

		EXEC	@return_value = [dbo].[xpr_create_client]
				@HomeTelephoneID = NULL,
				@mortgage_problems = 1,
				@referral_code = NULL,
				@method_first_contact = NULL, --no contact has been made at this point
				@bankruptcy = 0

		set @client = @return_value

		declare @referred_by int
		set @referred_by = case 
							when @program = 0 then 822 
							when @program = 1 then 1366
							when @program = 2 then 1367
							when @program = 4 then 415
							else null
							end


		update clients 
		set active_status = 'CRE',
			disbursement_date = 1,
			config_fee = 3,
			AddressID = @Address,
			HomeTelephoneID = (select case when @p1 is not null then @p1 else NULL end),
			MsgTelephoneID = (select case when @p2 is not null then @p2 else NULL end),
			language = 1,
			referred_by = @referred_by
		where client = @client		
		
		declare @ownerId int
		set @ownerId = null
		declare @coownerId int
		set @coownerId = null

		--INSERT PEOPLE
		if @hasPerson1 = 1
		begin
			if (select count(*) from people where Client = @client and Relation = 1) = 1
			begin
				set @person1 = (select Person from people where Client = @client and Relation = 1)

				update people 
				set NameID = @name1,
					CellTelephoneID = @p3,
					WorkTelephoneID = @p4,
					Gender = 1,
					Race = 10,
					Ethnicity = 2,
					Education = 1,
					CreditAgency = 'EQUIFAX',
					no_fico_score_reason = 4
				where Person = @person1
			end
			else			
			begin
				insert into people(Client,NameID,Relation,CellTelephoneID,WorkTelephoneID,Gender,Race,Ethnicity,Education,CreditAgency,no_fico_score_reason)
				values(@client,@name1,1,@p3,@p4,1,10,2,1,'EQUIFAX',4)
				set @person1 = SCOPE_IDENTITY()
			end

			INSERT INTO Housing_borrowers (
				[PersonID]
				,[NameID]
				,[Gender]
				,[Race]
				,[ethnicity]
				,[Education]
				,[Language]
				,[CreditAgency])
				 VALUES
			   (1 --1 = applicant, 2 = coapplicant
			   ,@name1
				,1
				,10
				,2
				,1
				,1
				,'EQUIFAX')

			set @ownerId = SCOPE_IDENTITY()

		end

		if @hasPerson2 = 1
		begin
			insert into people(Client,NameID,Relation,CellTelephoneID,WorkTelephoneID,Gender,Race,Ethnicity,Education,CreditAgency,no_fico_score_reason)
			values(@client,@name2,2,@p5,@p6,2,10,2,1,'EQUIFAX',4)
			set @person2 = SCOPE_IDENTITY()

			INSERT INTO Housing_borrowers (
				[PersonID]
				,[NameID]
				,[Gender]
				,[Race]
				,[ethnicity]
				,[Education]
				,[Language]
				,[CreditAgency])
				 VALUES
			   (2 --1 = applicant, 2 = coapplicant
			   ,@name2
				,1
				,10
				,2
				,1
				,1
				,'EQUIFAX')

			set @coownerId = SCOPE_IDENTITY()
		end
		
		set @person1 = (select Person from people where Client = @client and Relation = 1)

		--INSERT SERVICER/LENDER CHAIN
		--NOTE: YES, the HousingID should be the ClientId!!
		declare @property int
		set @property = NULL
		insert into Housing_properties(OwnerID,CoOwnerID,UseHomeAddress,HousingID,Residency,PropertyType)
		values(@ownerId,@coownerId,1,@client,0,8)
		set @property = SCOPE_IDENTITY()

		declare @calcServicerId int 
		set @calcServicerId =	COALESCE((select top 1 oID from Housing_lender_servicers [servicers] 
								where servicers.[description] = @servicer),442)

		declare @calcServicerName varchar(50)
		set @calcServicerName = case when @calcServicerId = 442 then 'Other or Unknown' else @servicer end

		--For Freedie-EI,180,720 POST, investor is Freddie.
		declare @investor int
		if @program in (0,1,2,4)
		BEGIN
		set @investor = 3
		END


		declare @lender int
		set @lender = NULL
		insert into Housing_lenders(ServicerName,ServicerID,AcctNum,InvestorID,InvestorAccountNumber)
		values(@calcServicerName,@calcServicerId,@servicerLoan,@investor,@investorNo)
		set @lender = SCOPE_IDENTITY()

		--case when ocs.InvestorDueDate is null then 0 else DATEDIFF(MONTH,ocs.InvestorDueDate,GETDATE()) end [MonthsPastDue]

		declare @moPastDue int
		set @moPastDue = case when @dueDate is null then 0 else DATEDIFF(MONTH,@dueDate,GETDATE()) end

		declare @loan int
		set @loan = NULL
		insert into Housing_loans(PropertyID,CurrentLenderID,UseOrigLenderID,IntakeSameAsCurrent,Loan1st2nd,UseInReports,LoanDelinquencyMonths)
		values(@property,@lender,1,1,1,1,@moPastDue)
		set @loan = SCOPE_IDENTITY()	

		--INSERT OCS CLIENT
		declare @foundTz varchar(50) 
		set @foundTz = 
                (select top 1 map.time_zone from zip_to_timezone [map] where map.zip like (@zipcode + '%'))

		if @foundTz is null
		begin
			set @foundTz = 
				(select top 1 zipInfo.TimeZone 
				from ZipCodeSearch [zipInfo]
				inner join states [state]
				on state.MailingCode = zipInfo.StateAbbr
				where state.state = @state)
		end

		declare @ocs int
		set @ocs = NULL
		insert into OCS_Client(ClientId,Program,StatusCode,Archive,ActiveFlag,IsDuplicate,UploadRecord,UploadAttempt,InvestorNumber,InvestorLastChanceList,InvestorActualSaleDate,InvestorDueDate,SearchTimezone,SearchServicerId,QueueCode)
		values(@client,@program,0,@archive,@activeFlag,@isDuplicate,@uploadedRecordId,@uploadAttempt,@investorNo,@lastChanceList,@actualSaleDate,@dueDate,COALESCE(@foundTz,'Indeterminate'),@calcServicerId,@queueCode)
		set @ocs = SCOPE_IDENTITY()

		commit transaction
	end try

	begin catch
	  rollback transaction
	end catch
	
	set @Inserted = @client
END




GO


