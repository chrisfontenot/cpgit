USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_list]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_list] ( @office AS INT, @start_date AS DATETIME = NULL, @end_date AS DATETIME = NULL ) AS
-- ==========================================================================================================
-- ==                 Return the information for the appointment booking screen                            ==
-- ==========================================================================================================

-- ChangeLog
--    9/6/2002
--      Validate the status of the appointment to be considered "booked". It must be "Pending".
--      Validate the counselor in the counselors table to be counted as "available".
--   12/18/2002
--     Added support for "inactive" flag in appt_counselors

IF @start_date IS NULL
	SELECT @start_date = getdate()

IF @end_date IS NULL
	SELECT @end_date = @start_date

-- Adjust the times for the boundaries to be the corrected values
SELECT	@start_date = convert ( DateTime, convert(varchar(10), @start_date, 101) + ' 00:00:00'),
	@end_date   = convert ( DateTime, convert(varchar(10), @end_date,   101) + ' 23:59:59')

-- Fetch the appointment information
SELECT		s.appt_time,
		s.start_time,
		UPPER(ISNULL(t.appt_name, 'ANY TYPE'))									AS 'appt_type',
		(SELECT COUNT(*) FROM appt_counselors co WITH ( NOLOCK ) WHERE co.appt_time = s.appt_time AND co.inactive = 0 AND co.counselor in (SELECT counselor FROM counselor_attributes ca with (nolock) LEFT OUTER JOIN AttributeTypes t WITH (NOLOCK) ON ca.Attribute = t.oID where t.[grouping] = 'ROLE' and t.[attribute] = 'COUNSELOR') )		AS 'available',
		(SELECT COUNT(appt_time) FROM client_appointments c WITH ( NOLOCK ) WHERE c.appt_time = s.appt_time AND c.status = 'P')	AS 'booked',
		1 as full_schedule
FROM		appt_times s WITH ( NOLOCK )
LEFT OUTER JOIN	appt_types t WITH ( NOLOCK ) ON t.appt_type = s.appt_type
WHERE		s.office = @office
AND		s.start_time BETWEEN @start_date AND @end_date
ORDER BY	s.start_time

RETURN ( @@rowcount )
GO
