USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_statistics_clients]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_statistics_clients] ( @FromDate AS smalldatetime, @ToDate as smalldatetime ) AS

-- ======================================================================================================
-- ==                   Update the monthly statistics with the trust balance                           ==
-- ======================================================================================================

-- Do general configuration options
SET NOCOUNT ON
SET ANSI_WARNINGS OFF
BEGIN TRANSACTION

-- =================================================================================================
-- ==                 Generate the statistic information for the month                            ==
-- =================================================================================================

DECLARE	@value_clients_cre	int
DECLARE @value_clients_wks	int
DECLARE	@value_clients_apt	int
DECLARE @value_clients_pnd	int
DECLARE @value_clients_rdy	int
DECLARE @value_clients_pro	int
DECLARE @value_clients_a	int
DECLARE @value_clients_ex	int
DECLARE @value_clients_i	int
DECLARE @value_clients_other	int

SELECT	@value_clients_cre	= 0,
	@value_clients_wks	= 0,
	@value_clients_apt	= 0,
	@value_clients_pnd	= 0,
	@value_clients_rdy	= 0,
	@value_clients_pro	= 0,
	@value_clients_a	= 0,
	@value_clients_ex	= 0,
	@value_clients_i	= 0,
	@value_clients_other	= 0

DECLARE item_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT		active_status,
			count(active_status) as cnt
	FROM		clients
	GROUP BY	active_status

DECLARE @active_status		varchar(10)
DECLARE @cnt			int

OPEN  item_cursor
FETCH item_cursor INTO @active_status, @cnt
WHILE @@fetch_status = 0
BEGIN
	SET @active_status = ltrim(rtrim(lower(isnull(@active_status,'cre'))))

	IF @active_status = 'cre'
		SET @value_clients_cre = @value_clients_cre + @cnt
	ELSE IF @active_status = 'apt'
		SET @value_clients_apt = @value_clients_apt + @cnt
	ELSE IF @active_status = 'wks'
		SET @value_clients_wks = @value_clients_wks + @cnt
	ELSE IF @active_status = 'pnd'
		SET @value_clients_pnd = @value_clients_pnd + @cnt
	ELSE IF @active_status = 'rdy'
		SET @value_clients_rdy = @value_clients_rdy + @cnt
	ELSE IF @active_status = 'pro'
		SET @value_clients_pro = @value_clients_pro + @cnt
	ELSE IF @active_status = 'a'
		SET @value_clients_a = @value_clients_a + @cnt
	ELSE IF @active_status = 'ar'
		SET @value_clients_a = @value_clients_a + @cnt
	ELSE IF @active_status = 'ex'
		SET @value_clients_ex = @value_clients_ex + @cnt
	ELSE IF @active_status = 'i'
		SET @value_clients_i = @value_clients_i + @cnt
	ELSE
	 	SET @value_clients_other = @value_clients_other + @cnt

	FETCH item_cursor INTO @active_status, @cnt
END
CLOSE		item_cursor
DEALLOCATE	item_cursor

-- =====================================================================================================
-- ==           Update the statistics for the month                                                   ==
-- =====================================================================================================

UPDATE	[statistics]

SET	clients_cre	= @value_clients_cre,
	clients_wks	= @value_clients_wks,
	clients_apt	= @value_clients_apt,
	clients_pnd	= @value_clients_pnd,
	clients_rdy	= @value_clients_rdy,
	clients_pro	= @value_clients_pro,
	clients_a	= @value_clients_a,
	clients_ex	= @value_clients_ex,
	clients_i	= @value_clients_i,
	clients_other	= @value_clients_other

WHERE	period_start	= @FromDate
AND	period_stop	= @ToDate

-- Complete the transaction
COMMIT TRANSACTION

-- Return success to the caller
RETURN ( 1 )
GO
