USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_update_bankruptcy]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_update_bankruptcy] ( @intake_client as int, @bankruptcy as int = 0) as
-- ============================================================================================================
-- ==            Set the bankruptcy status flag for the client                                               ==
-- ============================================================================================================

-- Initialize to process the request
set nocount on

update intake_clients
set    bankruptcy = isnull(@bankruptcy,0)
where  intake_client = @intake_client;

return ( @@rowcount )
GO
