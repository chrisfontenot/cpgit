USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_DailyDeposits_ByBatch_Detail]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_DailyDeposits_ByBatch_Detail] ( @trust_register as int, @counselor as int = null ) AS
-- =============================================================================================
-- ==                Fetch the information for the daily deposits report                      ==
-- =============================================================================================

-- ChangeLog
--    3/25/2003
--       Created from rpt_DailyDeposits
--    3/27/2003
--       Order the report by bank #, date, client

-- Suppress intermediate results
set nocount on

if isnull(@counselor,0) > 0
begin
	if not exists (select * from counselors where counselor = @counselor)
		select	@counselor = 0
end

-- This is the main select statement to return the results from the dataset
declare	@stmt		varchar(8000)
select	@stmt	= '
	SELECT	tr.bank								as ''bank'',
		b.description							as ''bank_name'',
		d.trust_register						as ''trust_register'',
		d.client							as ''client'',
		dbo.format_normal_name (pn.prefix,pn.first, pn.middle, pn.last, pn.suffix) as ''name'',
		isnull(t.description,''unknown type '' + d.tran_subtype)	as ''type'',
		d.credit_amt							as ''amount'',
		d.date_created							as ''date'',
		d.message							as ''reference'',
		tr.sequence_number						as ''deposit_label''
	
	FROM		registers_client d	WITH (NOLOCK)
	LEFT OUTER JOIN	tran_types t		WITH (NOLOCK) ON d.tran_subtype = t.tran_type
	LEFT OUTER JOIN	people p		WITH (NOLOCK) ON d.client = p.client AND 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name
	LEFT OUTER JOIN registers_trust tr	WITH (NOLOCK) ON d.trust_register = tr.trust_register
	LEFT OUTER JOIN banks b			WITH (NOLOCK) ON tr.bank = b.bank
	
	WHERE		d.tran_type = ''DP''
	AND		d.trust_register = ' + convert(varchar, @trust_register)

-- Removed from the select statement
--		d.item_date							as ''item_date'',
--		dbo.format_counselor_name (d.created_by)			as ''counselor''

if isnull(@counselor,0) > 0
	select	@stmt = @stmt + ' AND d.created_by=' + convert(varchar, @counselor)

select @stmt = @stmt + ' ORDER BY d.date_created, d.trust_register, d.client'
exec ( @stmt )
RETURN ( 1 )
GO
