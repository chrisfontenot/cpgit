USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_client_creditor_proposal]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_client_creditor_proposal] (@client_creditor_proposal as int, @proposal_status as int = 1, @proposal_print_date as datetime = null, @proposal_accepted_by as varchar(50) = null, @proposal_status_date as datetime = null, @proposal_reject_reason as int = null, @proposal_reject_disp as int = null, @counter_amount as money = null, @missing_item as varchar(50) = null, @percent_balance as float = null, @terms as int = null, @proposed_amount as money = null, @proposed_start_date as datetime = null, @proposal_batch_id as int = null, @proposed_balance as money = null, @proposed_date as datetime = null, @full_disclosure as bit = 0, @bank as int = null, @rpps_biller_id as varchar(10) = null, @epay_biller_id as varchar(50) = null, @proposal_note as text = null) as

	-- =======================================================================================
	-- ==          .NET interface to update a proposal in the system                        ==
	-- =======================================================================================

	-- If there is a note then update the debt note for the proposal
	if @client_creditor_proposal > 0
	begin
	
		-- It is all or nothing at this point
		begin transaction
	
		-- Find the debt note for this proposal in the system
		declare	@debt_noteID	int
		select	@debt_noteID	= debt_note
		from	debt_notes
		where	client_creditor_proposal = @client_creditor_proposal
		and		type = 'PR';

		-- If there is no note now and there was in the past then delete the note		
		if @proposal_note is null and @debt_noteID is not null
			delete
			from	debt_notes
			where	debt_note	= @debt_noteID;

		-- If there is a new note and there wasn't one earlier then create a new note.
		else if @proposal_note is not null and @debt_noteID is null
			insert into debt_notes (type, client, creditor, client_creditor, client_creditor_proposal,  note_text, note_amount, note_date)
			select	'PR', cc.client, cc.creditor, cc.client_creditor, @client_creditor_proposal, @proposal_note, 0, GETDATE()
			from	client_creditor cc with (nolock)
			inner join client_creditor_proposals pr with (nolock) on cc.client_creditor = pr.client_creditor
			where	pr.client_creditor_proposal = @client_creditor_proposal

		-- If there was a note and there is a note then update the text
		else if @proposal_note is not null and @debt_noteID is not null
			update	debt_notes
			set		note_text	= @proposal_note
			where	debt_note	= @debt_noteID

		-- Update the proposal with the corresponding new values
		update	update_client_creditor_proposals
		set		proposed_date				= isnull(@proposed_date,getdate()),
				proposal_status				= @proposal_status,
				proposal_accepted_by		= @proposal_accepted_by,
				proposal_status_date		= @proposal_status_date,
				proposal_reject_reason		= @proposal_reject_reason,
				proposal_reject_disp		= @proposal_reject_disp
		where	client_creditor_proposal	= @client_creditor_proposal

		-- These fields are not in the update view. Do them directly.
		update	client_creditor_proposals
		set		proposal_print_date			= @proposal_print_date,
				counter_amount				= @counter_amount,
				missing_item				= @missing_item,
				percent_balance				= @percent_balance,
				terms						= @terms,
				proposed_amount				= @proposed_amount,
				proposed_start_date			= @proposed_start_date,
				proposal_batch_id			= @proposal_batch_id,
				proposed_balance			= @proposed_balance,
				full_disclosure				= @full_disclosure,
				bank						= @bank,
				rpps_biller_id				= @rpps_biller_id,
				epay_biller_id				= @epay_biller_id
		where	client_creditor_proposal	= @client_creditor_proposal
		
		-- Commit the changes to the database when completed
		commit transaction
	end

	return ( @@rowcount )
GO
