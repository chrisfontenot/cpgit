USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_AttributeTypes]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_AttributeTypes] ( @grouping as varchar(50) = null, @attribute as varchar(50), @default as bit = 0, @ActiveFlag as bit = 1, @LanguageID as varchar(50) = null, @HUDLanguage as varchar(10) = null, @HUDServiceType as varchar(10) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the AttributeTypes table                 ==
-- ========================================================================================
insert into AttributeTypes ( [Grouping], [Attribute], [default], [ActiveFlag], [LanguageID], [HUDLanguage], [HUDServiceType] ) select @grouping, isnull(@attribute,''), isnull(@default,0), isnull(@ActiveFlag,1), @LanguageID, @HUDLanguage, @HUDServiceType
return ( scope_identity() )
GO
