USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_letter_queue_list]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_letter_queue_list] as 

-- ===========================================================================
-- ==            Return the information needed to print the letter queue    ==
-- ===========================================================================

select	min(letter_queue)	as 'item_key',
	queue_name		as 'description',
	count(*)		as 'count'
from	letter_queue with (nolock)
where	date_printed is null
group by queue_name

return ( @@rowcount )
GO
