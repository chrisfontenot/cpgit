USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_transfer_cl_cl]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_transfer_cl_cl] ( @SrcClient AS INT, @DestClient AS INT, @Amount AS Money, @Description AS VarChar(80) = NULL ) AS

-- ===============================================================================================
-- ==            Generate a transfer from one client to another                                 ==
-- ===============================================================================================

-- ChangeLog
--   3/20/2002
--     Make the date/time stamp the same on all transactions so that it is easier to match the two clients if
--     I really, really, really need to generate a report that shows client to client transfers with paired clients.
--   10/30/2002
--     Removed check for existing funds in the client account.

DECLARE	@ErrorMsg	VarChar(128)
DECLARE	@OldBalance	Money
DECLARE	@NewBalance	Money
DECLARE	@DateStamp	datetime

-- Do not generate intermediate result sets
SET NOCOUNT ON

-- Do nothing if the amount is zero
IF @Amount = 0
	Return ( 0 )

-- Ensure that the amount is valid
IF @Amount < 0
BEGIN
	RaisError(50019, 16, 1 )
	Return ( 0 )
END

-- Generate a transaction for the operation
BEGIN TRANSACTION

-- Fetch the date/time stamp for the transactions. This is done so that both transactions have the identical
-- timestamp and it is easier to locate the "pairs" if the timestamps are identical (to the microsecond.)
select	@DateStamp	= getdate()

-- Fetch the starting balance for the operation
SELECT	@OldBalance = isnull(held_in_trust,0)
FROM	clients
WHERE	client = @SrcClient

IF @OldBalance IS NULL
BEGIN
	RaisError(50014, 16, 1, @SrcClient)
	RollBack Transaction
	Return ( 0 )
END

SELECT	@NewBalance = isnull(held_in_trust,0)
FROM	clients
WHERE	client = @DestClient

IF @NewBalance IS NULL
BEGIN
	RaisError(50014, 16, 1, @DestClient)
	RollBack Transaction
	Return ( 0 )
END

-- If the source and destination clients are the same then do nothing
IF @SrcClient = @DestClient
BEGIN
	RollBack Transaction
	SELECT	@OldBalance as 'balance'
	RETURN ( 1 )
END

-- Debit the src client
UPDATE	clients
SET	held_in_trust = isnull(held_in_trust,0) - @Amount
WHERE	client = @SrcClient

-- Credit the dest client
UPDATE	clients
SET	held_in_trust = isnull(held_in_trust,0) + @Amount
WHERE	client = @DestClient

-- Fetch the new balance
SELECT	@NewBalance = isnull(held_in_trust,0)
FROM	clients
WHERE	client = @SrcClient

/*
-- Ensure that the trust balance is valid after the transfer
IF (@OldBalance > 0) AND (@NewBalance < 0)
BEGIN
	RaisError(50064, 16, 1, @Src_Client)
	RollBack Transaction
	Return ( 0 )
END
*/

-- Record the items into the client register
insert into registers_client	(date_created,	item_date,	tran_type,	client,		debit_amt,	message)
SELECT				@DateStamp,	@DateStamp,	'CW',		@SrcClient,	@amount,	isnull(@Description, 'Transfer to client ' + dbo.format_client_id ( @destclient ))

insert into registers_client	(date_created,	item_date,	tran_type,	client,		credit_amt,	message)
SELECT				@DateStamp,	@DateStamp,	'CD',		@DestClient,	@amount,	isnull(@Description, 'Transfer from client ' + dbo.format_client_id ( @srcclient ))

-- Commit the transaction
COMMIT TRANSACTION

-- Return the new balance for the client
SELECT	@NewBalance as 'balance'
RETURN ( 1 )
GO
