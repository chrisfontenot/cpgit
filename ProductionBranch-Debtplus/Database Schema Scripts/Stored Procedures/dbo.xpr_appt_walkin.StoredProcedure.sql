USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_walkin]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_walkin] ( @client AS INT, @bankruptcy_class as int = -1 ) AS

-- ================================================================================================
-- ==            Create a walkin apponitment for this client *NOW*                               ==
-- ================================================================================================

-- ChangeLog
--   12/24/2002
--     Added first_appt and first_kept_appt to the client update

-- Suppress intermediate results
set nocount on

declare	@counselor		typ_key
declare	@office			typ_key
declare	@client_appointment	typ_key

-- If the counselor was not passed then try to find it in the system tables based upon the name of the person "keeping" the appointment
select	@counselor	= counselor,
	@office		= isnull(@office,office)
from	counselors
where	[person]	= suser_sname()		-- Don't forget the \\DP\jones rather than just 'jones' in the table

-- If there is no counselor then just use the first one
if @counselor is null
begin
	select	@counselor	= min(coa.counselor)
	from	counselor_attributes coa
    inner join AttributeTypes t on coa.Attribute = t.oID
	inner join counselors co on coa.counselor = co.Counselor and co.ActiveFlag = 1
	where	t.attribute = 'COUNSELOR'
    and     t.[grouping] = 'ROLE'
    and     co.ActiveFlag = 1

	if @counselor is null
		select	@counselor	= min(counselor)
		from	counselors

	-- If there still is no counselor then use "1"
	if @counselor is null
		select	@counselor	= 1
end

-- If there is no office then use the "main" office
if @office is null
begin
	select	@office	= office
	from	offices
	where	type	= 1

	if @office is null
		select	@office		= min(office)
		from	offices

	-- If there still is no counselor then use "1"
	if @office is null
		select	@office		= 1
end

-- Strip the seconds and milliseconds from the time periods
declare	@start_time	datetime
declare	@end_time	datetime

select	@start_time	= GETDATE()
select	@start_time = convert(varchar(10), @start_time, 101) + ' ' + CONVERT(varchar(5), @start_time, 8) + ':00'
select	@end_time	= @start_time	-- the duration is zero until the counselor changes it.

-- Create the walkin appointment
INSERT INTO client_appointments (client,	start_time, end_time,	status,	appt_type,	housing,	confirmation_status,	date_confirmed,	counselor,	office,		bankruptcy_class)
VALUES				(@client,	@start_time, @end_time,	'W',	NULL,		0,		2,			getdate(),	@Counselor,	@office,	case when @bankruptcy_class < 0 then 0 else @bankruptcy_class end)

set @client_appointment	= @@identity

-- Set the active status correctly
update	clients
set	active_status		= 'APT'
where	client			= @client
and	active_status		= 'CRE'

-- If a bankruptcy class was specified on the client then set it correctly.
if @bankruptcy_class >= 0
	update	clients
	set	bankruptcy_class	= @bankruptcy_class
	where	client			= @client

-- Change the counselor if there is no counselor
if @counselor is not null
	update	clients
	set	counselor	= @counselor
	where	client		= @client

-- Change the office if there is no office
if @office is not null
	update	clients
	set	office		= @office
	where	client		= @client

-- This is the first appointment
update	clients
set	first_appt		= @client_appointment
from	clients
where	client			= @client
and	first_appt is null

-- The first kept appointment is this one
update	clients
set	first_kept_appt		= @client_appointment
from	clients
where	client			= @client
and	first_kept_appt is null

-- Return the information about the appointment
select	@counselor		as 'counselor',
	@office			as 'office',
	@client_appointment	as 'client_appointment'

-- Return the appointment pointer
return ( @client_appointment )
GO
