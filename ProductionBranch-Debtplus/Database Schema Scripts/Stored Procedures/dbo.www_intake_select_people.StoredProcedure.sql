USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_select_people]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_select_people] (@intake_client as int, @intake_id as varchar(20), @person as int) as
	select	p.*
	from	intake_people p with (nolock)
	inner join intake_clients c with (nolock) on c.intake_client = p.intake_client
	where	c.intake_client		= @intake_client
	and		c.intake_id			= @intake_id
	and		p.person			= @person
	return ( @@rowcount )
GO
