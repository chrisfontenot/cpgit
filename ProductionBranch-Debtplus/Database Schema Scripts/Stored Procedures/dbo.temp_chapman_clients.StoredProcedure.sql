USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[temp_chapman_clients]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[temp_chapman_clients] AS 

select client, sum(credit_amt) as deposit_amount
into #dep
from registers_client with (nolock)
where tran_type = 'DP'
group by client;

if exists (select * from sysobjects where name = 'chapman_clients' and type = 'U')
    drop table chapman_clients

select	c.client,
	case p.relation
		when 1 then 'Applicant'
		else 'Co-Applicant'
	end as relation,
	datediff(year, birthdate, getdate()) as age,
	case p.gender
		when 1 then 'Male'
		else 'Female'
	end as gender,
	p.gross_income,
	p.net_income,
	(select sum(asset_amount) from assets where client = c.client) as other_income,
	m_marital.description as marital_status,
	m_race.description as race,
	m_housing.description as housing_status,
	dbo.date_only ( c.date_created ) as date_created,
	dbo.date_only ( c.last_deposit_date ) as last_deposit_date,
	convert(datetime, case c.active_status
		when 'I' then dbo.date_only ( c.drop_date )
		else null
	end) as drop_date,
	convert(varchar(50), case
		when c.active_status = 'I' and ltrim(rtrim(isnull(c.drop_reason_other,''))) <> '' then c.drop_reason_other
		when c.active_status = 'I' and c.drop_reason is not null then dr.description
		else null
	end) as drop_reason,
	c.program_months as program_months,
	(select sum(deposit_amount) from client_deposits where client = c.client) as expected_deposit_amount,
	dep.deposit_amount as deposits_received,
	convert(money,0) as total_payments
INTO	chapman_clients
from	clients c
inner join client_housing h on c.client = h.client
inner join people p on c.client = p.client
left outer join MaritalTypes m_marital on c.marital_status = m_marital.oID
left outer join RaceTypes m_race on p.race = m_race.oID
left outer join Housing_StatusTypes m_housing on h.housing_status = m_housing.oID
left outer join drop_reasons dr on c.drop_reason = dr.drop_reason
left outer join #dep dep on c.client = dep.client
where	c.client > 0

-- Discard the clients that are not in the last spreadsheet
exec ('delete from chapman_clients where client not in (select convert(int,client) from [chapman_input_clients$])')

drop table #dep
GO
