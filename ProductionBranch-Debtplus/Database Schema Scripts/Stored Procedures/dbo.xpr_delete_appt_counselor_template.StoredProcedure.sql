USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_appt_counselor_template]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_appt_counselor_template] ( @appt_counselor_template as int ) as
	delete
	from	appt_counselor_templates
	where	appt_counselor_template = @appt_counselor_template;
	
	return ( @@rowcount )
GO
