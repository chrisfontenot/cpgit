USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_MonthEndSummary_Payments]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_MonthEndSummary_Payments] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL ) AS
-- =============================================================================================
-- ==                   Find the deposits by type                                             ==
-- =============================================================================================

-- ChangeLog
--   11/1/2001
--     Corrected amounts for deducted and billed to convert the value to type 'money'. For some reason, the
--     SQL servers do not decide to handle the conversion correctly on some cases unless it is explictly performed.

-- Suppress intermediate result sets
SET NOCOUNT ON

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT          @FromDate   = CONVERT(varchar(10), @FromDate, 101),
                @ToDate     = CONVERT(varchar(10), @ToDate, 101) + ' 23:59:59';

-- Fetch the deduct creditor to exclude it from the reports
DECLARE	@deduct_creditor	varchar(10)
SELECT	@deduct_creditor = deduct_creditor
FROM	config

-- Fetch the client/creditor information
SELECT
	1										as 'group',

	td.description									as 'type',

	convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'created_date',

	sum (isnull(d.debit_amt,0))							as 'gross',
	sum (case isnull(d.creditor_type,'N') when 'D' then d.fairshare_amt else convert(money,0.0) end)		as 'deducted',
	sum (case isnull(d.creditor_type,'N') when 'B' then d.fairshare_amt else convert(money,0.0) end)		as 'billed',

	isnull('[' + d.creditor + ']','') + isnull(' ' + cr.creditor_name,'')		as 'name',
	d.tran_type                                                                     as 'tran_type'

FROM	registers_client_creditor d WITH (NOLOCK)
left outer join tran_types td WITH (NOLOCK) ON d.tran_type = td.tran_type
left outer join creditors cr with (nolock) on d.creditor = cr.creditor

where	d.date_created BETWEEN @FromDate AND @ToDate
and	d.tran_type IN ('AD', 'BW', 'CM', 'MD')
and	d.creditor != @deduct_creditor

GROUP BY d.tran_type, d.creditor, cr.creditor_name, d.date_created, td.description

-- The client refunds are only in the client table. Look there.
union all

select
	2										as 'group',
	td.description									as 'type',
	convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'created_date',
	sum ( isnull(d.debit_amt,0) )							as 'gross',
	convert(money,0.0)								as 'deducted',
	convert(money,0.0)								as 'billed',
	isnull('[' + dbo.format_client_id ( d.client ) + '] ','') + ISNULL(dbo.format_normal_name(default,pn.first,pn.middle,pn.last,default),'')  as 'name',
	d.tran_type                                                                     as 'tran_type'

FROM	registers_client d WITH (NOLOCK)
left outer join tran_types td WITH (NOLOCK) ON d.tran_type = td.tran_type
left outer join people p with (nolock) on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

where	d.date_created BETWEEN @FromDate AND @ToDate
and	d.tran_type = 'CR'
group by d.tran_type, d.client, pn.first, pn.middle, pn.last, d.date_created, td.description

ORDER BY 1, 2, 3

RETURN ( @@rowcount )
GO
