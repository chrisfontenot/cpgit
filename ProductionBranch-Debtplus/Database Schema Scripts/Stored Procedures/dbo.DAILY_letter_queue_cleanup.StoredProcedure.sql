USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_letter_queue_cleanup]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DAILY_letter_queue_cleanup] AS
-- =======================================================================================================
-- ==                 Delete expired letters from the letter queue                                      ==
-- =======================================================================================================

-- ChangeLog
--   7/3/2004
--     Initial creation

-- Start a transaction for the request
begin transaction

-- Delete the expired letters to prevent them from just taking over the entire database
delete
from	letter_queue
where	date_printed		is not null
and	death_date		<= getdate()

-- Complete the transaction
commit transaction

-- Indicate success
return ( 1 )
GO
