USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Documents]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[rpt_Documents] ( @DocumentID as int, @client as int = null ) as
-- ===============================================================================================
-- ==         Retrieve the Document information                                                 ==
-- ===============================================================================================
set nocount on

-- Find the state for the client
declare	@state		int
declare	@language	int
if @client is not null
	select	@state		= a.state,
			@language	= c.language
	from	clients c with (nolock)
	inner join addresses a with (nolock) on c.addressid = a.address
	where	c.client	= @client

-- Determine the current date (without the time)
declare	@today		datetime
select	@today = convert(varchar(10), getdate(), 101)

-- Find the corresponding items for this report
select	distinct c.SequenceID, c.PageEjectBeforeFLG, c.SectionID, c.ComponentType, c.ComponentLocationURI, c.EffectiveExpireDate, c.EffectiveStartDate, @client as client
from	documentcomponents c with (nolock)
inner join documentAttributes a with (nolock) on c.oID = a.DocumentID and (a.AllLanguagesFLG <> 0 or a.LanguageID = @Language) and (a.AllStatesFLG <> 0 OR a.State = @State)
where	c.DocumentID = @DocumentID
and		(c.EffectiveStartDate is null or c.EffectiveStartDate <= @today)
and		(c.EffectiveExpireDate is null or c.EffectiveExpireDate >= @today)
order by c.SequenceID;

return ( @@rowcount )
GO
