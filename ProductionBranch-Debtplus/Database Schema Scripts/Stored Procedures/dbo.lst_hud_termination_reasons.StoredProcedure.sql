USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_hud_termination_reasons]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_hud_termination_reasons] AS
-- ========================================================================================
-- ==            Return the list of H.U.D. Termination reasons                           ==
-- ========================================================================================

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
FROM	Housing_TerminationReasonTypes
order by 2

return ( @@rowcount )
GO
