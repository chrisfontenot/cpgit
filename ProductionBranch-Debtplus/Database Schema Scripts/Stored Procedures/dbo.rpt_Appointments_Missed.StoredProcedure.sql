USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Appointments_Missed]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Appointments_Missed] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- =========================================================================================
-- ==                   Retrieve the missed appointment list                              ==
-- =========================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

set nocount on

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate    = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
       @ToDate      = convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Retrieve the information from the database
SELECT		a.client as 'client',
		a.start_time as 'time',
		dbo.format_normal_name(default, pn.first, pn.middle, pn.last, pn.suffix) as 'name',
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor',
		o.name as 'office',
		at.appt_name as 'type'
FROM		client_appointments a WITH (NOLOCK)
INNER JOIN	people p WITH (NOLOCK) ON a.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	offices o WITH (NOLOCK) ON a.office = o.office
LEFT OUTER JOIN	appt_types at WITH (NOLOCK) ON a.appt_type = at.appt_type
LEFT OUTER JOIN	counselors c WITH (NOLOCK) ON a.counselor = c.counselor
left outer join names cox with (nolock) on c.NameID = cox.name

WHERE		a.status = 'M'
AND		a.appt_time IS NULL
AND		a.start_time BETWEEN @FromDate AND @ToDate
AND		a.workshop IS NULL
ORDER BY	5, 2, 4, 1;     -- office, start_time, counselor, client

RETURN ( @@rowcount )
GO
