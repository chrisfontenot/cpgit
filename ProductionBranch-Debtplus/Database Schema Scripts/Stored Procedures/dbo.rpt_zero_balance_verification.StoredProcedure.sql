USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_zero_balance_verification]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_zero_balance_verification] as

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Find the latest disbursement
declare	@disbursement_register	int
select	top 1 @disbursement_register = disbursement_register
from	registers_disbursement with (nolock)
where	(date_posted is not null or tran_type <> 'AD')
order by date_created desc;

-- override the disbursement batch id here
-- select	@disbursement_register = 5174	-- Put the number here

-- Find the list of debts that were paid in the disbursement
select	client, creditor, client_creditor, date_created, debit_amt, account_number, convert(money,1) as balance, convert(int,0) as client_creditor
into	#paid_debts
from	registers_client_creditor with (nolock)
where	tran_type in ('AD','BW','MD','CM')
and	disbursement_register = @disbursement_register
and	void = 0;

-- Find the balances for the debts
update	#paid_debts
set	balance = b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments,
	client_creditor = cc.client_creditor
from	#paid_debts d
inner join client_creditor cc with (nolock) on d.client_creditor = cc.client_creditor
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance
inner join creditors cr with (nolock) on cc.creditor = cr.creditor
inner join creditor_classes cl with (nolock) on cr.creditor_class = cl.creditor_class
where	cl.agency_account = 0;

-- Generate a system note that we are going to print the letter
insert into client_notes (client, client_creditor, type, is_text, subject, dont_edit, dont_delete, dont_print, note)
select	client, client_creditor, 3, 1, 'Printing zero-balance verification letter', 1, 1, 0, 'Printed the zero balance verification letter from after the disbursement batch # ' + convert(varchar, @disbursement_register)
from	#paid_debts
where	balance = 0;

-- return the list of clients for the fields
select	x.client, x.creditor, cr.creditor_name, x.account_number, x.debit_amt, x.date_created
from	#paid_debts x
inner join creditors cr on x.creditor = cr.creditor
inner join clients c on x.client = c.client
where	balance = 0
order by c.postalcode, c.client;

-- Drop the working table and exit
drop table #paid_debts
return ( @@rowcount )
GO
