SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [lst_pending_checks] ( @bank as int = null ) AS

-- ===================================================================================================================
-- ==            Fetch the information for the list of outstanding checks in the reconcilation process              ==
-- ===================================================================================================================

-- Find the list of pending checks
if isnull(@bank,-1) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'

if @bank is null
	select	@bank		= 1

-- Return the results to the list of pending (unreconciled) checks
SELECT	k.trust_register,
		k.tran_type,

	isnull(k.cleared,' ') as 'cleared',

	k.creditor,
	k.date_created,
	k.checknum,
	k.amount,
	k.reconciled_date,

	CASE
		when	k.client	is not null then dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix )
		when	k.creditor	is not null then c.creditor_name
		else	typ.description
	end as payee

FROM	registers_trust k	WITH (NOLOCK)
LEFT OUTER JOIN	creditors c	WITH (NOLOCK) ON k.creditor=c.creditor
LEFT OUTER JOIN	people p	WITH (NOLOCK) ON k.client=p.client and 1=p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN tran_types typ	WITH (NOLOCK) ON k.tran_type = typ.tran_type

WHERE	isnull(k.cleared,' ') in (' ','C','E','P')
AND		k.amount	> 0
AND		k.bank		= @bank
AND		k.tran_type in ('AD','MD','CM','CR')

RETURN ( @@rowcount )
GO
