USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_reserved_in_trust_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_client_reserved_in_trust_update] ( @client as int, @amount as money, @cutoff_date as datetime ) AS

begin transaction

if (isnull(@amount,0) < 0) or (@cutoff_date is null)
begin
	select	@amount		= 0,
		@cutoff_date	= null
end

declare	@old_amount		money
declare	@old_date		datetime
select	@old_amount		= reserved_in_trust,
	@old_date		= reserved_in_trust_cutoff
from	clients
where	client			= @client

if @old_date is not null
begin
	if @old_date < getdate()
		select	@old_amount	= 0,
			@old_date	= null
end

select	@old_date	= convert(varchar(10), @old_date, 101),
	@cutoff_date	= convert(varchar(10), @cutoff_date, 101),
	@old_amount	= convert(decimal(10,2), @old_amount),
	@amount		= convert(decimal(10,2), @amount)

if @amount <= 0
	select	@amount		= 0,
		@cutoff_date	= null

if @old_amount <= 0
	select	@old_amount	= 0,
		@old_date	= null

if (isnull(@old_date,'1/1/1900') <> isnull(@cutoff_date,'1/1/1900')) or (@amount <> @old_amount)
begin
	-- Record the change in the status as a note
	insert into client_notes (client, type, subject, is_text, dont_edit, dont_delete, note)
	values (@client, 3, 'Reserved Amount in trust changed', 1, 1, 1,
	'The reserved amount in trust was changed from ' + isnull('$' + convert(varchar, @old_amount,1) + ' on ' + convert(varchar(10), @old_date, 101), 'nothing') + ' to ' + 
	isnull('$' + convert(varchar, @amount,1) + ' on ' + convert(varchar(10), @cutoff_date, 101), 'nothing'))

	-- Update the the clients amounts accordingly
	update	clients
	set	reserved_in_trust		= @amount,
		reserved_in_trust_cutoff	= dbo.date_only ( @cutoff_date )
	where	client				= @client
end

commit transaction
return ( 1 )
GO
