USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_address]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_address] (@Address as int, @creditor_prefix_1 varchar(80) = null, @creditor_prefix_2 varchar(80) = null, @house varchar(10) = null, @direction varchar(10) = null, @street varchar(60) = null, @suffix varchar(10) = null, @modifier varchar(10) = null, @modifier_value varchar(10) = null, @address_line_2 varchar(80) = null, @address_line_3 varchar(80) = null, @city varchar(60) = null, @state int = null, @postalcode varchar(30) = null) as

-- The state must not be null.
if @state is null
	select @state = MIN(state)
	from   states
	where  [Default] <> 0;
	
if @state is null
	select	@state = MIN(state)
	from	states;
	
if @state is null
	select	@state = 1;

-- Add the record
update	addresses
set		[creditor_prefix_1] = @creditor_prefix_1, 
		[creditor_prefix_2] = @creditor_prefix_2,
		[house]				= @house,
		[direction]			= @direction,
		[street]			= @street,
		[suffix]			= @suffix,
		[modifier]			= @modifier,
		[modifier_value]	= @modifier_value,
		[address_line_2]	= @address_line_2,
		[address_line_3]	= @address_line_3,
		[city]				= @city,
		[state]				= @state,
		[postalcode]		= @PostalCode
where	[Address]			= @Address

return ( @@RowCount )
GO
