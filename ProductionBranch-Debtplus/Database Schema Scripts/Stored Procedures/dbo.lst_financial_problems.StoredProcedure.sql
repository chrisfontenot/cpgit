USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_financial_problems]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_financial_problems] AS

-- =================================================================================================
-- ==            Reasons for the problems which led the client to come to us                      ==
-- =================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Fetch the list of drop reasons
SELECT	financial_problem	as 'item_key',
	[description]		as 'description'
FROM	financial_problems with (nolock)

RETURN ( @@rowcount )
GO
