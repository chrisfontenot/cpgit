USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_resource_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_resource_types] as

-- ============================================================================
-- ==            Resource types                                              ==
-- ============================================================================

select	resource_type	as 'item_key',
	description	as 'description',
	label		as 'label'
FROM	resource_types
ORDER BY 2

return ( @@rowcount )
GO
