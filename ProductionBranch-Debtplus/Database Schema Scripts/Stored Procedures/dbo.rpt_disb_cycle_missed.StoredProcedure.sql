USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_disb_cycle_missed]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_disb_cycle_missed] ( @disbursement_register int = null ) as

if @disbursement_register is null
	select	@disbursement_register	= disbursement_register
	from	registers_disbursement
	where	tran_type		= 'AD'
	order by date_created

declare	@stmt			varchar(8000)
declare	@disbursement_date	varchar(80)
declare	@region			varchar(80)
declare	@date_created		datetime
select	@disbursement_date	= disbursement_date,
	@date_created		= date_created,
	@region			= region
from	registers_disbursement where disbursement_register = @disbursement_register

-- Discard the temporary table if one is present
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##missing'' and type = ''U'') drop table ##missing' )

select	@stmt	= '
select	c.client
into	##missing
from	clients c
left outer join disbursement_clients x on c.client = x.client and ' + convert(varchar, @disbursement_register) + ' = x.disbursement_register '

select @stmt = @stmt + ' where c.client > 0 and c.active_status in (''A'', ''AR'') and x.client is null '

if ltrim(rtrim(isnull(@disbursement_date,''))) <> ''
	select	@stmt = @stmt + ' AND c.disbursement_date in (' + @disbursement_date + ') '

if ltrim(rtrim(isnull(@region,''))) <> ''
	select	@stmt = @stmt + ' AND c.region in (' + @region + ') '

exec ( @stmt )

select	v.client, v.name, c.disbursement_date, c.start_date, d.deposit_date, d.deposit_amount, c.last_deposit_date, @disbursement_register as disbursement_register, @date_created as date_created, convert(varchar(10), case isnull(ach.isActive,0) when 0 then 'N' else 'Y' end) as ACH_Indicator
from	view_client_address v
inner join ##missing m on v.client = m.client
inner join clients c on v.client = c.client
left outer join client_ach ach with (nolock) on c.client = ach.client
left outer join client_deposits d on c.client = d.client
order by v.client

drop table ##missing
return ( @@rowcount )
GO
