USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_trace_update]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_trace_update] ( @bank as int, @transaction_number as int, @batch_number as int ) as

-- ============================================================================================================
-- ==            Update the trace number for the associate EPAY bank                                         ==
-- ============================================================================================================

-- Suppress intermediate results
set nocount on

-- Find the corresponding bank information
if isnull(@bank,0) <= 0
	select	@bank	= null

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'E'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'E'

if @bank is null
	select	@bank	= 1

-- Update the information
update	banks
set	transaction_number	= @transaction_number,
	batch_number		= @batch_number
where	bank			= @bank

return ( @@rowcount )
GO
