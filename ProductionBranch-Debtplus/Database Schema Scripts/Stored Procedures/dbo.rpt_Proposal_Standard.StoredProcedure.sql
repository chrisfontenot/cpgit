USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_Standard]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_Standard] ( @client_creditor_proposal AS INT = 0, @proposal_mode AS INT = 0, @proposal_batch_label as typ_message = null ) AS
-- ===========================================================================================
-- ==           Print a standard proposal letter                                            ==
-- ===========================================================================================

-- ChangeLog
--   11/2/2001
--     Corrected system note for the "reprint" function to properly generate the note.
--   2/6/2002
--     Use the batch close date as the print date.
--   2/18/2002
--     Use the batch creation date as the print date.
--    7/8/2002
--       Added counselor name, creditor type, and fairshare rate to the result set.
--   7/18/2002
--     Added support for the "person" field in the client_creditor table.
--    8/12/2002
--       Removed secured debt from "total debt" information
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   9/1/2003
--     Changed sequence for reprint of proposals system note to retrieve the information from #t_proposals_1 rather than duplicate the work.
--   10/1/2003
--     Expanded the proposal_message field to "text" format.
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.
--   4/6/2005
--     Added client_creditor to the response result set
--   12/4/2006
--     Added min_start_date to ensure that the debts start date is at least 5 days in the future.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- ===========================================================================================
-- ==           Retrieve supplimental information used in the letter                        ==
-- ===========================================================================================
DECLARE @AgencyName	VARCHAR(255)
DECLARE @FeeRecord	INT
DECLARE	@Current_Date	DateTime

SET ANSI_WARNINGS OFF
SET NOCOUNT ON

-- Earliest start date for proposals
declare	@min_start_date	datetime
select	@min_start_date	= dbo.date_only ( dateadd( d, 5, getdate() ));

-- ===========================================================================================
-- ==                 Fetch the information for the standard proposal letter                ==
-- ===========================================================================================
create table #t_proposals_1 (
	client_creditor_proposal	int,
	client_creditor			int,
	proposal_message		varchar(256),
	proposal_print_date		datetime,
	client				int,
	creditor			varchar(10),
	debt_balance			money,
	proposed_payment		money,
	disbursement_date		int,
	start_date			datetime,
	account_number			varchar(50),
	counselor_name			varchar(50)	null,
	creditor_type			varchar(10)	null,
	fairshare_rate			float		null,
	cr_type				varchar(4)	null,
	cr_creditor_id			int		null
)

-- ===========================================================================================
-- ==                Determine the config fee amount                                        ==
-- ===========================================================================================
-- If there is no record then use the value from the config table
SELECT	@AgencyName	= isnull(name + ' is ', 'We are '),
		@Current_Date	= getdate()
FROM	config

select	@FeeRecord		= min(config_fee)
from	config_fees
where	[default]		= 1;

-- If there still is no record then take the latest one
IF @FeeRecord IS NULL
	SELECT @FeeRecord = min(config_fee)
	FROM		config_fees

IF @FeeRecord IS NULL
	SELECT @FeeRecord = 1

-- ====================================================================================
-- ==                If choosing a batch by name, fetch the numeric batch id         ==
-- ====================================================================================
if @proposal_mode = 1
begin
	select	@client_creditor_proposal	= proposal_batch_id,
			@proposal_mode			= 2
	from	proposal_batch_ids
	where	note				= @proposal_batch_label
	and	proposal_type			= 1
end

-- ====================================================================================
-- ==                Process the batch ID                                            ==
-- ====================================================================================
if @proposal_mode = 2
begin
	declare	@print_date	datetime
	declare	@closed_date	datetime

	select	@print_date			= date_created,
			@closed_date			= date_closed
	from	proposal_batch_ids with (nolock)
	where	proposal_batch_id		= @client_creditor_proposal

	-- If the batch is not closed then update the balance information
	if @closed_date is null
	begin
		update	client_creditor_proposals
		set	proposed_amount		= cc.disbursement_factor,
			proposed_start_date	= isnull(cc.start_date, c.start_date),
			proposed_balance	= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
		from	client_creditor_proposals p
		inner join client_creditor cc on p.client_creditor = cc.client_creditor
		inner join client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
		inner join clients c on cc.client = c.client
		where	proposal_batch_id	= @client_creditor_proposal

		-- Correct the start date if needed
		update	client_creditor_proposals
		set	proposed_start_date	= @min_start_date
		where	proposed_start_date	< @min_start_date
		and	proposal_batch_id	= @client_creditor_proposal;
	end

	insert into #t_proposals_1 (cr_type, cr_creditor_id, client_creditor_proposal,client_creditor,proposal_message,proposal_print_date,client,creditor,debt_balance,proposed_payment,disbursement_date,start_date,account_number,counselor_name,creditor_type,fairshare_rate)
	select	cr.type,
			cr.creditor_id,
			pr.client_creditor_proposal 			as 'client_creditor_proposal',
			pr.client_creditor  				as 'client_creditor',
			convert(varchar(256),dn.note_text)		as 'proposal_message',
			coalesce (@print_date, pr.proposed_start_date, cc.start_date, c.start_date, getdate()) as 'print_date',
			cc.client					as 'client',
			cc.creditor					as 'creditor',
			isnull(pr.proposed_balance,0)			as 'debt_balance',
			isnull(pr.proposed_amount,0)			as 'proposed_payment',
			c.disbursement_date				as 'disbursement_date',
			pr.proposed_start_date				as 'start_date',
			cc.account_number				as 'account_number',
			dbo.format_normal_name(default,cox.first,default,cox.last,default)	as 'counselor_name',
			isnull(pct.creditor_type_eft,'N')		as 'creditor_type',
			isnull(pct.fairshare_pct_eft,0)			as 'fairshare_rate'

	FROM		client_creditor_proposals pr	WITH (NOLOCK)
	LEFT OUTER JOIN	debt_notes dn			WITH (NOLOCK) ON pr.client_creditor_proposal = dn.client_creditor_proposal AND 'PR' = dn.type
	INNER JOIN	client_creditor cc		WITH (NOLOCK) ON cc.client_creditor = pr.client_creditor
	INNER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
	LEFT OUTER JOIN	creditor_contribution_pcts pct	WITH (NOLOCK) ON cr.creditor_contribution_pct = pct.creditor_contribution_pct
	INNER JOIN	clients c			WITH (NOLOCK) ON cc.client = c.client
	INNER JOIN	proposal_batch_ids ids		WITH (NOLOCK) ON ids.proposal_batch_id = pr.proposal_batch_id
	LEFT OUTER JOIN counselors co			WITH (NOLOCK) ON c.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name

	where		pr.proposal_batch_id	= @client_creditor_proposal
	order by	cr.type, cr.creditor_id, cc.client
end

-- ===========================================================================================
-- ==                Print a specific proposal ID                                           ==
-- ===========================================================================================
if @proposal_mode = 0
begin
	insert into #t_proposals_1 (cr_type, cr_creditor_id, client_creditor_proposal,client_creditor,proposal_message,proposal_print_date,client,creditor,debt_balance,proposed_payment,disbursement_date,start_date,account_number,counselor_name,creditor_type,fairshare_rate)
	select		cr.type,
			cr.creditor_id,
			pr.client_creditor_proposal 			as 'client_creditor_proposal',
			pr.client_creditor  				as 'client_creditor',
			convert(varchar(256),dn.note_text)		as 'proposal_message',
			coalesce (ids.date_closed, pr.proposed_start_date, cc.start_date, c.start_date, getdate()) as 'print_date',
			cc.client					as 'client',
			cc.creditor					as 'creditor',
			coalesce(pr.proposed_balance,isnull(bal.orig_balance,0)+isnull(bal.orig_balance_adjustment,0)+isnull(bal.total_interest,0)-isnull(bal.total_payments,0),0)		as 'debt_balance',
			coalesce(pr.proposed_amount,cc.disbursement_factor,0)			as 'proposed_payment',
			c.disbursement_date							as 'disbursement_date',
			coalesce(pr.proposed_start_date,cc.start_date,c.start_date,getdate())	as 'start_date',
			cc.account_number							as 'account_number',
			dbo.format_normal_name(default,cox.first,default,cox.last,default)	as 'counselor_name',
			isnull(pct.creditor_type_eft,'N')		as 'creditor_type',
			isnull(pct.fairshare_pct_eft,0)			as 'fairshare_rate'

	FROM		client_creditor_proposals pr	WITH (NOLOCK)
	LEFT OUTER JOIN	debt_notes dn			WITH (NOLOCK) ON pr.client_creditor_proposal = dn.client_creditor_proposal AND 'PR' = dn.type
	INNER JOIN	client_creditor cc		WITH (NOLOCK) ON cc.client_creditor = pr.client_creditor
	inner join	client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
	INNER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
	LEFT OUTER JOIN	creditor_contribution_pcts pct	WITH (NOLOCK) ON cr.creditor_contribution_pct = pct.creditor_contribution_pct
	INNER JOIN	clients c			WITH (NOLOCK) ON cc.client = c.client
	LEFT OUTER JOIN	proposal_batch_ids ids		WITH (NOLOCK) ON ids.proposal_batch_id = pr.proposal_batch_id
	LEFT OUTER JOIN	counselors co			WITH (NOLOCK) ON c.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name

	where		pr.client_creditor_proposal	= @client_creditor_proposal

	-- Include the system note for the "re-print" function.
	insert into client_notes (client, client_creditor, type, is_text, dont_edit, dont_delete, dont_print, subject, note)
	select		client,
			client_creditor,
			3, 1, 1, 1, 0,
			'Standard Proposal Reprinted upon demand',
			'A Proposal was reprinted upon request with the following information:' + char(13) + char(10) +
			char(13) + char(10) +
			'Balance: $'   + convert(varchar,     debt_balance, 1)		+ char(13) + char(10) +
			'Payment: $'   + convert(varchar,     proposed_payment, 1)	+ char(13) + char(10) +
			'Start Date: ' + convert(varchar(10), start_date, 101)		+ char(13) + char(10)

	from		#t_proposals_1
end

-- ===========================================================================================
-- ==           Fetch the total managed debt for the client        			    ==
-- ===========================================================================================
SELECT			cc.client					as 'client',
			sum(bal.orig_balance)			as 'orig_balance',
			sum(bal.orig_balance_adjustment)	as 'orig_balance_adjustment',
			sum(bal.total_interest)			as 'total_interest',
			sum(bal.total_payments)			as 'total_payments',
			count(*)				as 'creditor_count'

INTO			#t_proposals_2

FROM			client_creditor cc	WITH (NOLOCK)
inner join		client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN		creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN		creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class

WHERE			(cr.creditor IS NULL OR isnull(ccl.proposal_balance,1) = 1)
AND			cc.reassigned_debt = 0
AND			(isnull(bal.total_payments,0) < (isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0)))
AND			cc.client IN (select client from #t_proposals_1)

GROUP BY		cc.client

-- ==========================================================================================
-- ==           Return the information for the current letter                              ==
-- ==========================================================================================
SELECT		a.client,
		a.creditor,
		a.cr_type,
		a.cr_creditor_id,
		
		a.proposal_message,
		isnull(a.proposal_print_date, @Current_Date)		as 'print_date',
		isnull(b.orig_balance,0) + isnull(b.orig_balance_adjustment,0) + isnull(b.total_interest,0) - isnull(b.total_payments,0) as 'total_debt',
		convert(int,isnull(b.creditor_count,0))			as 'total_creditors',
		convert(int,isnull(a.disbursement_date, 1))		as 'disbursement_date',
		a.debt_balance						as 'creditor_debt',
		a.proposed_payment					as 'creditor_payment',
		a.start_date						as 'first_disbursement',
		@AgencyName						as 'agency_name',

		isnull(e.description, 'a reasonable monthly fee')	as 'fee_description',
		a.client_creditor_proposal				as 'client_creditor_proposal',
		a.client_creditor					as 'client_creditor',
		a.account_number					as 'account_number',
		p1.ssn							as 'ssn1',
		p2.ssn							as 'ssn2',
		a.counselor_name					as 'counselor_name',
		a.creditor_type						as 'creditor_type',
		a.fairshare_rate					as 'fairshare_rate'

FROM		#t_proposals_1 a	WITH (NOLOCK)
LEFT OUTER JOIN	#t_proposals_2 b	WITH (NOLOCK) ON a.client = b.client
LEFT OUTER JOIN	clients cl		WITH (NOLOCK) ON a.client = cl.client
LEFT OUTER JOIN	config_fees e		WITH (NOLOCK) ON e.config_fee = isnull(cl.config_fee, @FeeRecord)
LEFT OUTER JOIN	people p1		WITH (NOLOCK) ON a.client = p1.client AND 1 = p1.relation
LEFT OUTER JOIN	people p2		WITH (NOLOCK) ON a.client = p2.client AND 1 <> p2.relation

order by	cr_type, cr_creditor_id, creditor, 1

-- Drop the working tables
drop table #t_proposals_1
drop table #t_proposals_2
--drop table #t_proposals_3

-- Return the number of records
RETURN ( @@rowcount )
GO
