USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_cie]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_cie] ( @rpps_response_file as int = null ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of error payments                    ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--     Added client's counselor name.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress the count of records
set nocount on

-- Try to find the response file if one is not specified
if @rpps_response_file is null
begin
	select	@rpps_response_file = max(rpps_response_file)
	from	rpps_response_files
	where	date_created = (
		select	max(date_created)
		from	rpps_response_files
	)
end

CREATE TABLE #rpps_response_cie(
	[date_created] datetime NULL,
	[gross] money NULL,
	[deducted] money NULL,
	[billed] money NULL,
	[client] int NULL,
	[client_name] varchar(256) NULL,
	[creditor] varchar(10) NULL,
	[client_creditor] int NULL,
	[account_number] varchar(22) NULL,
	[creditor_name] varchar(255) NULL,
	[error_code] varchar(256) NULL,
	[rpps_biller_id] varchar(10) NULL
) ON [PRIMARY];

-- Retrieve the information for the payment records
insert into #rpps_response_cie (date_created,gross,deducted,billed,client,client_name,creditor,client_creditor,account_number,creditor_name,error_code,rpps_biller_id)
select		d.date_created							as 'date_created',
		isnull(d.debit_amt,r.gross)					as 'gross',

		case
			when d.creditor_type = 'D' then d.fairshare_amt
			when d.creditor_type is not null then 0
			else r.gross - r.net
		end								as 'deducted',

		case
			when d.creditor_type = 'D' then 0
			when d.creditor_type = 'N' then 0
			when d.creditor_type is not null then d.fairshare_amt
			else 0
		end								as 'billed',

		d.client							as 'client',
		case
			when p.client is not null then dbo.format_reverse_name (default, pn.first, pn.middle, pn.last, default)
			else isnull(r.consumer_name,'')
		end								as 'client_name',

		isnull(d.creditor, r.rpps_biller_id)				as 'creditor',
		d.client_creditor							as 'client_creditor',
		coalesce(r.account_number,d.account_number,'MISSING')		as 'account_number',
		cr.creditor_name						as 'creditor_name',
		coalesce(r.processing_error, x.description, rt.return_code, '')	as 'error_code',
		r.rpps_biller_id						as 'rpps_biller_id'

from		rpps_response_details r with (nolock)
left outer join	rpps_transactions rt with (nolock) on r.rpps_transaction = rt.rpps_transaction and 'CIE' = rt.service_class_or_purpose
left outer join	registers_client_creditor d with (nolock) ON rt.client_creditor_register = d.client_creditor_register
left outer join	creditors cr with (nolock) on d.creditor = cr.creditor
left outer join	people p ON d.client=p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
left outer join	rpps_reject_codes x with (nolock) ON rt.return_code = x.rpps_reject_code

where		r.rpps_response_file		= @rpps_response_file
and		r.service_class_or_purpose  = 'CIE'
and		r.transaction_code		= 22

update		#rpps_response_cie
set			creditor_name	= b1.biller_name
from		#rpps_response_cie r
left outer join rpps_biller_ids b1 with (nolock) on r.rpps_biller_id = b1.rpps_biller_id
where		r.creditor_name is null;

select 	a.date_created, a.gross, a.deducted, a.billed,
	a.client, a.client_name,
	a.creditor, a.client_creditor, a.account_number, a.creditor_name, a.error_code,
	convert(varchar(80), dbo.format_normal_name(default,cox.first,cox.middle,cox.last,cox.suffix)) as counselor_name
from	#rpps_response_cie a
left outer join clients c with (nolock) on a.client = c.client
left outer join counselors co with (nolock) on c.counselor = co.counselor
left outer join creditors cr with (nolock) on a.creditor = cr.creditor
left outer join names cox with (nolock) on co.nameid = cox.name
order by cr.type, cr.creditor_id, a.client, a.account_number;

drop table    #rpps_response_cie

return ( @@rowcount )
GO
