USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_transaction_end]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_transaction_end] ( @disbursement_register as int, @client as typ_client, @debit_amt as money ) AS

-- ======================================================================================================
-- ==            End a payment cycle for the client. Deduct the amount from the trust.                 ==
-- ======================================================================================================

-- ChangeLog
--   6/16/2002
--     Removed reference to "disbursed", "disbursed_by", and "date_disbursed" from disbursement_clients table. These columns were dropped.

-- Suppress intermediate result sets
set nocount on

-- Fetch the pointer to the withdrawn information
declare	@prior_transaction	int
declare	@trust_balance		money
declare	@p1			varchar(7)
declare	@p2			varchar(80)

-- Fetch the current information from the disbursement table
select	@prior_transaction	= client_register
from	disbursement_clients
where	disbursement_register	= @disbursement_register
and	client			= @client

-- If there is a preivous disbursement then reset it
if @prior_transaction is null
	SELECT @prior_transaction = 0

if @prior_transaction = 0
begin
	insert into registers_client	(tran_type,	client,		debit_amt,	disbursement_register,	message)
	values				('AD',		@client,	@debit_amt,	@disbursement_register,	'Auto Disbursement')
	select	@prior_transaction = SCOPE_IDENTITY()

end else begin

	update	registers_client
	set	debit_amt		= @debit_amt,
		message			= 'Auto Disbursement',
		disbursement_register	= @disbursement_register,
		created_by		= suser_sname(),
		date_created		= getdate()
	where	client_register		= @prior_transaction
end

-- Record the information into the client totals table for the disbursement
update	disbursement_clients
set	client_register		= @prior_transaction
where	disbursement_register	= @disbursement_register
and	client			= @client

-- Withdraw the money from the client's trust
update	clients
set	held_in_trust		= held_in_trust - @debit_amt
where	client			= @client

-- Fetch the current trust balance from the client
select	@trust_balance		= held_in_trust
from	clients
where	client			= @client

-- If the trust balance is now negative then the disbursement is not allowed
if @trust_balance < 0
begin
	RaisError(50064, 16, 1, @client)
	return ( 0 )
end

-- Remove the old disbursement note
delete from	disbursement_notes
where		disbursement_register = @disbursement_register
and		client = @client
and		type = 3

-- Insert the new disbursement note
execute xpr_disbursement_sysnote @Client, @disbursement_register, @trust_balance, null

-- Return success when the operation is complete
return ( 1 )
GO
