USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Summary_RPS_Refunds]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Summary_RPS_Refunds] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for voided creditor checks                               ==
-- ======================================================================================================

IF @ToDate IS NULL
	SET @ToDate = getdate()

IF @FromDate IS NULL
	SET @FromDate = @ToDate

-- Adjust the dates for the proper settings
SET @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SET @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the transactions
SELECT		d.creditor				as 'creditor',
		cr.creditor_name			as 'creditor_name',
		d.client				as 'client',
		d.date_created				as 'item_date',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'client_name',
		d.credit_amt				as 'gross',
		case d.creditor_type when 'D' then d.fairshare_amt else 0 end as 'deducted',
		case d.creditor_type when 'B' then d.fairshare_amt else 0 end as 'billed'

FROM		registers_client_creditor d
LEFT OUTER JOIN	creditors cr on d.creditor = cr.creditor
INNER JOIN	clients c ON d.client = c.client
INNER JOIN	people p on c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		d.tran_type = 'RR'
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created

RETURN ( @@rowcount )
GO
