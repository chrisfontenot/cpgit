USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_transaction_start]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_transaction_start] ( @disbursement_register as int, @client as typ_client ) AS

-- ======================================================================================================
-- ==            Start a payment cycle for the client. Refund the previous payment.                    ==
-- ======================================================================================================

-- Suppress intermediate result sets
set nocount on

-- Start a transaction for the update
begin transaction
set xact_abort on

-- Fetch the pointer to the withdrawn information
declare	@prior_amount		money
declare	@prior_transaction	int
declare	@FeeRecord		typ_key
declare	@PaymentMinimum		money
declare	@paf_creditor		typ_creditor
declare	@stack_proration	bit

select	@prior_transaction	= isnull(client_register,0)
from	disbursement_clients
where	disbursement_register	= @disbursement_register
and	client			= @client

-- Fetch the information from the configuration table
SELECT	@paf_creditor		= paf_creditor,
	@PaymentMinimum		= payment_minimum
FROM	config

select	@FeeRecord		= min(config_fee)
from	config_fees
where	[default]		= 1;

if @FeeRecord is null
	select	@FeeRecord		= min(config_fee)
	from	config_fees

if @prior_transaction > 0
begin
	-- Fetch the previous amount taken from the trust. It is in the trace record.
	select	@prior_amount		= debit_amt
	from	registers_client
	where	client_register		= @prior_transaction

	if @prior_amount > 0
	begin
		-- Give the money back to the client trust
		update	clients
		set	held_in_trust		= held_in_trust + @prior_amount
		where	client			= @client

		-- Update the trace record to show that the item now has a zero disbursement
		update	registers_client
		set	debit_amt		= 0,
			message			= null
		where	client_register		= @prior_transaction

		-- Remove the transactions from the creditor disbursement table
		update	disbursement_creditors
		set	debit_amt		= 0,
			fairshare_amt		= 0
		where	disbursement_register	= @disbursement_register
		and	client			= @client

		-- Remove a previous disbursement note
		delete
		from	disbursement_notes
		where	client			= @client
		and	disbursement_register	= @disbursement_register
		and	type			= 3
	end
end

-- Commit the changes
commit transaction

-- ======================================================================================================
-- ==            Obtain the information to process this client.                                        ==
-- ======================================================================================================
select		case
			when dc.reserved_in_trust > dc.held_in_trust	then 0
			when dc.reserved_in_trust > c.held_in_trust	then 0
			when dc.held_in_trust < c.held_in_trust		then dc.held_in_trust - dc.reserved_in_trust
									else c.held_in_trust - dc.reserved_in_trust
		end						AS 'trust_balance',

		dc.disbursement_factor				as 'disbursement_factor',
		dc.note_count					as 'notes',

		-- Information from the config fee table
		isnull(f.fee_type,0)				as 'fee_type',
		isnull(f.fee_base,0)				as 'fee_base',
		isnull(f.fee_percent,0)				as 'fee_percent',
		isnull(f.fee_per_creditor,0)			as 'fee_per_creditor',
		isnull(f.fee_disb_max,0)			as 'fee_disb_max',
		isnull(f.fee_monthly_max,0)			as 'fee_monthly_max',
		isnull(f.fee_balance_max,0)			as 'fee_balance_max',
		isnull(c.stack_proration,0)			as 'stack_proration',

		-- Information from the config table
		@paf_creditor					as 'paf_creditor',
		@PaymentMinimum					as 'payment_minimum',
		coalesce(c.config_fee,@FeeRecord,1)		as 'config_fee',

		isnull(dc.reserved_in_trust,0)			as 'reserved_in_trust'

from		disbursement_clients dc	WITH (NOLOCK)
inner join	clients c		WITH (NOLOCK) ON c.client = dc.client
left outer join	config_fees f		with (nolock) on f.config_fee = isnull(c.config_fee, @FeeRecord)
where		dc.disbursement_register	= @disbursement_register
and		dc.client			= @client

-- Return success to the caller when complete
return ( 1 )
GO
