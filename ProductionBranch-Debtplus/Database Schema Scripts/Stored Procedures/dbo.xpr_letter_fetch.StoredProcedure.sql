USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_letter_fetch]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_letter_fetch] ( @letter_code AS VarChar(10) = NULL, @client AS INT = NULL, @creditor_id as int = null, @creditor as varchar(10) = null, @debt as int = null ) AS
-- =====================================================================================================
-- ==                Fetch the letter information for the specific client                             ==
-- =====================================================================================================

-- ChangeLog
--   6/23/2004
--     Initial creation
--   12/1/2004
--     Do not define the letter code when fetching by letter code. This allows the spanish letters to work properly.

-- Do not generate intermediate result sets
SET NOCOUNT ON

declare	@letter_type	int
DECLARE	@margin_top	float
DECLARE	@margin_left	float
DECLARE @margin_bottom	float
DECLARE @margin_right	float
DECLARE	@filename	varchar(256)
DECLARE @description	varchar(256)
DECLARE	@queue_name	varchar(50)
DECLARE	@language	int
declare	@sort_order	varchar(16)
declare	@priority	int
declare	@client_creditor_id	int
declare @display_mode   int

-- If there is a debt then find the client and creditor_id from the information
if isnull(@debt,-1) > 0
	select	@client				= client,
			@creditor			= creditor,
			@client_creditor_id	= client_creditor
	from	client_creditor with (nolock)
	where	client_creditor	= @debt

-- Reduce the creditor_id label to null if one was not passed
select	@creditor = ltrim(rtrim(isnull(@creditor,'')))
if @creditor = ''
	select	@creditor = null

-- If there is a creditor_id but no label then find the creditor from the system
if (isnull(@creditor_id,-1) > 0) and (@creditor is null)
	select	@creditor		= creditor
	from	creditors with (nolock)
	where	creditor_id	= @creditor_id

-- If there is no creditor_id but there is a creditor_id label then find the creditor_id from the system
if (isnull(@creditor_id,-1) <= 0) and (@creditor is not null)
	select	@creditor_id	= creditor_id
	from	creditors with (nolock)
	where	creditor	= @creditor

-- Find the language for the client if there is one.
select	@language	= null,
--	@sort_order	= '00000',
--	@priority	= 9,
	@letter_type	= null

-- If the letter type is numeric then see if it is one of the items in the letter types table directly
if @letter_code like '=[0-9]%'
begin
	if isnumeric(substring(@letter_code,2,10)) > 0
	begin
		select	@letter_type	= null, -- letter_type,
				@letter_code	= letter_code
		from	letter_types with (nolock)
		where	letter_type	= convert(int, substring(@letter_code,2,10))
	end
end

-- Find the language for the client
if @letter_type is null
begin
	if isnull(@client,0) > 0
		select	@language	= language
		from	clients with (nolock)
		where	client		= @client

	-- The language in the client must exist
	if @language is not null
	begin
		if not exists (select * from AttributeTypes where oID = @language)
			select	@language	= null
	end

	-- If there is no language then use the default one
	if @language is null
		select	@language	= min(oID)
		from	AttributeTypes with (nolock)
		where	[Grouping] = 'LANGUAGE'
		and		[default]	= 1

	-- If there is no default one, use the first one
	if @language is null
		select	@language	= min(oID)
		from	AttributeTypes with (nolock)
		where	[Grouping] = 'LANGUAGE'

	-- If there is no first one, use "1".
	if @language is null
		select	@language	= 1

	-- Find the default letter of this language/type
	SELECT	@letter_type		= min(letter_type)
	from	letter_types with (nolock)
	where	letter_code		= @letter_code
	and	language		= @language
	and	[default]		= 1

	-- Look for the letter of this language/type
	if @letter_type is null
		select	@letter_type	= min(letter_type)
		from	letter_types with (nolock)
		where	letter_code	= @letter_code
		and	language	= @language

	-- Look for the default letter of this type
	if @letter_type is null
		SELECT	@letter_type		= min(letter_type)
		from	letter_types with (nolock)
		where	letter_code		= @letter_code
		and	[default]		= 1

	-- If there still is no letter then use the first letter of this type for any language
	if @letter_type is null
		SELECT	@letter_type		= min(letter_type)

		from	letter_types with (nolock)
		where	letter_code		= @letter_code
end

-- If there is no letter then complain.
if @letter_type is null
begin
	declare	@error_message	varchar(800)
	select	@error_message = 'The letter type ''' + isnull(@letter_code,'') + ''' is not defined in the system'
	RaisError (@error_message, 16, 1)

end else begin

	-- return the resulting information about the letter from the system.
	select	@filename		= [filename],
			@language		= [language],
			@description	= [description],
			@margin_top		= [top_margin],
			@margin_left	= [left_margin],
			@margin_right	= [right_margin],
			@margin_bottom	= [bottom_margin],
			@queue_name		= ltrim(rtrim(isnull(queue_name,''))),
			@display_mode	= display_mode
	from	letter_types with (nolock)
	where	letter_type		= @letter_type
end

-- Return the result set from the letter fetch (however complete it is)
SELECT	isnull(@margin_top,0.0)		as 'top_margin',
		isnull(@margin_left,0.0)	as 'left_margin',
		isnull(@margin_right,0.0)	as 'right_margin',
		isnull(@margin_bottom,0.0)	as 'bottom_margin',
		isnull(@description,'')		as 'description',
		isnull(@filename,'')		as 'filename',
		isnull(@language,1)			as 'language',
		isnull(@letter_code,'')		as 'letter_code',
		isnull(@letter_type,-1)		as 'letter_type',
		isnull(@display_mode,-1)	as 'display_mode',

		-- Information needed for the letter queue
		isnull(@queue_name,'')		as 'queue_name'

RETURN ( 1 )
GO
