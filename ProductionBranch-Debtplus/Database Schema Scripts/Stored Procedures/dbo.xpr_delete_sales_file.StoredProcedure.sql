USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_sales_file]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_sales_file] ( @sales_file as int ) as
	delete
	from		sales_files
	where		sales_file	= @sales_file
	
	return ( @@rowcount )
GO
