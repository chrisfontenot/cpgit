USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_counselor]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_counselor] ( @Counselor as int ) as
-- ===========================================================================================================
-- ==          Delete a counselor from the system for the .NET interface                                    ==
-- ===========================================================================================================

-- Remove the counselor from the counselors table
delete
from    counselors
where   counselor   = @counselor;

return ( @@rowcount )
GO
