USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_hud_demographics]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_hud_demographics]  ( @FromDate as datetime = null, @ToDate as datetime = null ) as

-- ==============================================================================================
-- ==            Return the demographics information for the hud transactions                  ==
-- ==============================================================================================

-- ChangeLog
--   1/1/2003
--     Change to use new HUD transactions and interviews tables.
--   4/27/2006
--     Change to use secured_properites and secured_loans.

-- Remove the time values from the dates
if @ToDate is null
	select	@ToDate = getdate()

if @FromDate is null
	select	@FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Return the hud interview types
select
	'TYPE OF INTERVIEW'			as 'type',
	ht.description				as 'description',
	sum(hd.minutes)				as 'units',
	sum(case when isnull(hd.client_appointment,0) = 0 then 0 else 1 end) as 'count_ap',
	sum(case when isnull(hd.client_appointment,0) = 0 then 1 else 0 end) as 'count_cl'

from	hud_transactions hd with (nolock)
inner join hud_interviews hi with (nolock) on hd.hud_interview = hi.hud_interview
left outer join Housing_PurposeOfVisitTypes ht with (nolock) on hi.interview_type = ht.oID
where	hi.interview_date between @FromDate and @ToDate
group by ht.description

union all

-- Return the results of the interview
select	'RESULT OF THE INTERVIEW'				as 'type',
		hr.description							as 'description',
		sum(hd.minutes)							as 'units',
		sum(case when isnull(hd.client_appointment,0) = 0 then 0 else 1 end) as 'count_ap',
		sum(case when isnull(hd.client_appointment,0) = 0 then 1 else 0 end) as 'count_cl'

from	hud_transactions hd with (nolock)
inner join hud_interviews hi with (nolock) on hd.hud_interview = hi.hud_interview
left outer join Housing_ResultTypes hr with (nolock) on hi.hud_result = hr.oID
where	hi.result_date between @FromDate and @ToDate
group by hr.description

union all

-- Return the type of housing
select	'TYPE OF HOUSING'						as 'type',
		isnull(m.description,'Unspecified')		as 'description',
		sum(hd.minutes)							as 'units',
		sum(case when isnull(hd.client_appointment,0) = 0 then 0 else 1 end) as 'count_ap',
		sum(case when isnull(hd.client_appointment,0) = 0 then 1 else 0 end) as 'count_cl'

from	hud_transactions hd with (nolock)
inner join hud_interviews hi with (nolock) on hd.hud_interview = hi.hud_interview
left outer join client_housing c with (nolock) on hi.client = c.client
left outer join housing_properties p with (nolock) on hi.client = p.HousingID and 1 = p.UseInReports
left outer join HousingTypes m on p.PropertyType = m.oID
where	(hi.interview_date between @FromDate and @ToDate)
or		(hi.result_date between @FromDate and @ToDate)
group by m.description

union all

-- Return the ethnic background of the clients
select
	'ETHNIC GROUPS'				as 'type',
	isnull(m.description,'Unspecified')	as 'description',
	sum(hd.minutes)				as 'units',
	sum(case when isnull(hd.client_appointment,0) = 0 then 0 else 1 end) as 'count_ap',
	sum(case when isnull(hd.client_appointment,0) = 0 then 1 else 0 end) as 'count_cl'

from	hud_transactions hd with (nolock)
inner join hud_interviews hi with (nolock) on hd.hud_interview = hi.hud_interview
left outer join people p with (nolock) on hi.client = p.client and 1 = p.relation
left outer join RaceTypes m with (nolock) on p.race = m.oID
where	(hi.interview_date between @FromDate and @ToDate)
or	(hi.result_date between @FromDate and @ToDate)
group by m.description

union all

-- Return the gender of the clients
select
	'GENDER'				as 'type',
	isnull(m.description,'Unspecified')	as 'description',
	sum(hd.minutes)				as 'units',
	sum(case when isnull(hd.client_appointment,0) = 0 then 0 else 1 end) as 'count_ap',
	sum(case when isnull(hd.client_appointment,0) = 0 then 1 else 0 end) as 'count_cl'

from	hud_transactions hd with (nolock)
inner join hud_interviews hi with (nolock) on hd.hud_interview = hi.hud_interview
left outer join people p with (nolock) on hi.client = p.client and 1 = p.relation
left outer join GenderTypes m with (nolock) on p.gender = m.oID
where	(hi.interview_date between @FromDate and @ToDate)
or	(hi.result_date between @FromDate and @ToDate)
group by m.description

union all

-- Return the loan type
select
	'LOAN TYPE'				as 'type',
	isnull(t.description,'Unspecified')	as 'description',
	sum(hd.minutes)				as 'units',
	sum(case when isnull(hd.client_appointment,0) = 0 then 0 else 1 end) as 'count_ap',
	sum(case when isnull(hd.client_appointment,0) = 0 then 1 else 0 end) as 'count_cl'

from	hud_transactions hd with (nolock)
inner join hud_interviews hi with (nolock) on hd.hud_interview = hi.hud_interview
left outer join housing_properties p with (nolock) on hi.client = p.HousingID and 1 = p.UseInReports
left outer join housing_loans l with (nolock) on p.oID = l.PropertyID and 1 = l.UseInReports
left outer join Housing_Loan_Details d WITH (nolock) ON l.IntakeDetailID = d.oID
left outer join Housing_LoanTypes t with (nolock) on d.LoanTypeCD = t.oID
where	(hi.interview_date between @FromDate and @ToDate)
or		(hi.result_date between @FromDate and @ToDate)
group by t.description

union all

-- Return the housing status
select
	'HOUSING STATUS'			as 'type',
	isnull(m.description,'Unspecified')	as 'description',
	sum(hd.minutes)				as 'units',
	sum(case when isnull(hd.client_appointment,0) = 0 then 0 else 1 end) as 'count_ap',
	sum(case when isnull(hd.client_appointment,0) = 0 then 1 else 0 end) as 'count_cl'

from	hud_transactions hd with (nolock)
inner join hud_interviews hi with (nolock) on hd.hud_interview = hi.hud_interview
left outer join client_housing c with (nolock) on hi.client = c.client
left outer join Housing_StatusTypes m with (nolock) on c.housing_status = m.oID
where	(hi.interview_date between @FromDate and @ToDate)
or		(hi.result_date between @FromDate and @ToDate)
group by m.description

return ( @@rowcount )
GO
