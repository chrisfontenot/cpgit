USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_prefix_import]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_rpps_prefix_import] ( @billerID as varchar(10), @creditor as varchar(10)) AS

-- =================================================================================================
-- ==               Import the masks into the creditor prefix table for this creditor             ==
-- =================================================================================================

-- ChangeLog
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

insert into creditor_prefixes ([creditor], [lower], [upper], [description])
select	distinct @creditor,

	left(replace(replace(m.mask, '@', 'A'), '#', '0') + '000000000', 9),
	left(replace(replace(m.mask, '@', 'Z'), '#', '9') + '999999999', 9),
	left(coalesce(m.description, m.mask, ''),50)

FROM		rpps_biller_ids rps
inner join	rpps_masks m on rps.rpps_biller_id = m.rpps_biller_id
WHERE		rps.rpps_biller_id = @BillerID

RETURN ( @@rowcount )
GO
