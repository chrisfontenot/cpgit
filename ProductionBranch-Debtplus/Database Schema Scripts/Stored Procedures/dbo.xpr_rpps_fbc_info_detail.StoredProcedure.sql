USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_fbc_info_detail]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_fbc_info_detail] ( @rpps_transaction AS INT ) AS
-- ==================================================================================================
-- ==            Fetch the full budget counselor information                                       ==
-- ==================================================================================================

-- ChangeLog
--   9/20/2002
--     Generated creditor name rather than creditor type for all creditors
--   9/31/2002
--     Added support for client_creditor_balances table
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Disable intermediate result sets
SET NOCOUNT ON

-- Retrieve the information about the current record
DECLARE	@client_creditor_proposal	INT
DECLARE	@client				INT
DECLARE	@creditor			varchar(10)

SELECT	@client_creditor_proposal	= client_creditor_proposal,
	@client				= client,
	@creditor			= creditor
FROM	rpps_transactions WITH (NOLOCK)
WHERE	rpps_transaction = @rpps_transaction

IF @client IS NULL
BEGIN
	RaisError (50039, 16, 1, @client_creditor_proposal)
	Return ( 0 )
END

-- Retrieve the list of creditors
SELECT	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) AS 'balance',
	isnull(cc.disbursement_factor,0) as 'payment',

/*
	case	when cc.creditor = @creditor then upper(left( coalesce (cc.creditor_name, cr.creditor_name, '') + '            ', 12))
		else upper(left(isnull(ct.description, 'UNCLASSIFIED') + '            ', 12))
	end as 'creditor_name'
*/
	upper(left( coalesce (cc.creditor_name, cr.creditor_name, '') + '            ', 12)) as 'creditor_name'

FROM	client_creditor cc		WITH (NOLOCK)
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN creditors cr		WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl	WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
-- LEFT OUTER JOIN rpps_biller_ids b	WITH (NOLOCK) ON isnull(cr.rpps_proposal_id, cr.rpps_biller_id) = b.rpps_biller_id
-- LEFT OUTER JOIN creditor_types ct	WITH (NOLOCK) ON cr.type = ct.type

WHERE	cc.client = @client
AND	isnull(ccl.zero_balance,0) = 0
AND	cc.reassigned_debt = 0
AND	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) > 0
AND	cc.disbursement_factor > 0

ORDER BY isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) DESC

RETURN ( @@rowcount )
GO
