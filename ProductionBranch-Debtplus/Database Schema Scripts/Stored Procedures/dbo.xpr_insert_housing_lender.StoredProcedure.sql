USE [Debtplus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_lender]    Script Date: 09/17/2014 11:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER procedure [dbo].[xpr_insert_housing_lender] ( 
@ServicerID as Int = null
,@ServicerName as Varchar(80) = null
,@AcctNum as Varchar(50) = null
,@FdicNcusNum as Varchar(20) = null
,@CaseNumber as Varchar(20) = null
,@InvestorID as int = null
,@InvestorAccountNumber as Varchar(50) = null
,@ContactLenderDate as datetime
,@AttemptContactDate as datetime
,@ContactLenderSuccess as bit
,@WorkoutPlanDate as datetime) as

-- ========================================================================================================
-- ==           Add a lender to the system for .NET                                                      ==
-- ========================================================================================================
set nocount on

insert into housing_lenders ( [ServicerID],[ServicerName],[AcctNum],[FdicNcusNum],[CaseNumber],[ContactLenderDate],[AttemptContactDate],[ContactLenderSuccess],[WorkoutPlanDate],[InvestorID], [InvestorAccountNumber]) 
values ( @ServicerID,@ServicerName,@AcctNum,@FdicNcusNum,@CaseNumber,@ContactLenderDate,@AttemptContactDate,@ContactLenderSuccess,@WorkoutPlanDate,@InvestorID,@InvestorAccountNumber)
return ( scope_identity() )
