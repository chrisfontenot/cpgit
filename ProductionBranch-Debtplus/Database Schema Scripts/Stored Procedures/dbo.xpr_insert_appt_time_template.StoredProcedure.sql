USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_appt_time_template]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_appt_time_template] ( @office as int, @dow as int, @start_time as int ) as
	insert into appt_time_templates([office], [dow], [start_time]) values (@office, @dow, @start_time)
	return ( scope_identity() )
GO
