USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_verify_balance]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_verify_balance] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL, @counselor as int = null ) AS
-- ==============================================================================================
-- ==               Selecting Creditor's Client by last Disbursement Date given Date Range     ==
-- ==============================================================================================

--     Note: this report requires that the creditor is set for paper balance verifications. It only looks at creditors
--     who are set for paper balance verifications not EDI.	 

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   5/6/2003
--     Removed test for balance verify status. Now it does not matter when it was verfied.
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   11/27/2006
--     Added "counselor" to the parameters 

set nocount on

-- Supply the proper defaults

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Remove the time information from the dates
select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
select	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

SELECT		dbo.format_client_id(c.client) as client,
		isnull(cc.client_name, dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)) as 'name',
		cc.creditor,
        cr.creditor_name,
		cc.account_number,
		rcc.date_created as last_payment_date,
		cc.disbursement_factor as payment_amount,
		bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as balance,
		c.counselor,
		cr.type as creditor_type,
		cr.creditor_id as creditor_id

into		#results
FROM		client_creditor cc		WITH (NOLOCK)
inner join	client_creditor_balances bal	WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	people p			WITH (NOLOCK) ON cc.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
LEFT OUTER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
INNER JOIN	clients c			WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN registers_client_creditor rcc	WITH (NOLOCK) ON cc.last_payment = rcc.client_creditor_register
RIGHT OUTER JOIN	creditor_methods cm	WITH (NOLOCK) ON cm.creditor = cr.creditor_id and cm.type = 'BAL' AND cm.region in (0, c.region)
INNER JOIN	banks b				WITH (NOLOCK) ON cm.bank = b.bank and 'C' = b.type

WHERE		cc.reassigned_debt = 0
and		c.active_status IN ('A', 'AR')
and		rcc.date_created >= @FromDate
and 		rcc.date_created <= @ToDate

-- Balance not verified within the last 4 months
--and		(cc.balance_verify_date is null
--		or datediff(month, balance_verify_date, getdate()) > 4)

-- Zero balance
and		(bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest) <= bal.total_payments;

-- If there is a counselor then include it
if @counselor is not null
	delete
	from	#results
	where	counselor <> @counselor;

-- Return the results
select	client, name, creditor, creditor_name, account_number, last_payment_date, payment_amount, balance
from	#results
ORDER BY creditor_type, creditor_id, creditor;

drop table #results;
return ( @@rowcount )
GO
