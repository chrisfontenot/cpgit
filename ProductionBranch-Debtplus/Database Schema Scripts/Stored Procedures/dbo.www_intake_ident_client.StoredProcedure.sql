USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_ident_client]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_ident_client] ( @intake_client as int, @intake_id as varchar(20),
					   @address1 as varchar(81) = null, @address2 as varchar(81) = null, @city as varchar(61) = null, @state as int = null, @postalcode as varchar(50) = null,
					   @home_ph as varchar(20) = null, @message_ph as varchar(20) = null) as

-- ==============================================================================================================
-- ==            Start the processing on the new debt information                                              ==
-- ==============================================================================================================

-- Set the proper indicator flags
set nocount	on
set xact_abort	on

-- Remove leading/trailing blanks from the description.
select	@address1		= ltrim(rtrim(@address1)),
	@address2		= ltrim(rtrim(@address2)),
	@city			= ltrim(rtrim(@city)),
	@postalcode		= ltrim(rtrim(@postalcode)),
	@home_ph		= ltrim(rtrim(@home_ph)),
	@message_ph		= ltrim(rtrim(@message_ph))

if @address1 = ''
	select @address1 = null

if @address2 = ''
	select @address2 = null

if @city = ''
	select @city = null

if @postalcode = ''
	select @postalcode = null

if @home_ph = ''
	select @home_ph = null

if @message_ph = ''
	select @message_ph = null

if @home_ph = '0000000000'
	select @home_ph = null

if @message_ph = '0000000000'
	select @message_ph = null

-- Force the postal code to be a legimate U.S. value
if @postalcode is not null
	select	@postalcode = ltrim(rtrim(@postalcode))

if len(@message_ph) = 7
	select @message_ph = '000' + @message_ph

if len(@home_ph) = 7
	select @home_ph = '000' + @home_ph

-- Default the area code from the system if it is missing
if left(@message_ph,3) = '000' or left(@home_ph,3) = '000'
begin
	declare	@default_area_code	varchar(3)
	select	@default_area_code = areacode
	from	config

	if @default_area_code is null
		select @default_area_code = '000'

	if left(@message_ph,3) = '000'
		select @message_ph = @default_area_code + substring(@message_ph, 4, 80)

	if left(@home_ph,3) = '000'
		select @home_ph = @default_area_code + substring(@home_ph, 4, 80)
end

-- Update the client with the information
update				intake_clients
set	address1		= left(@address1,80),
	address2		= left(@address2,80),
	city			= left(@city,60),
	state			= @state,
	postalcode		= @postalcode,
	home_ph			= left(@home_ph, 14),
	message_ph		= left(@message_ph, 14),
	identity_date		= getdate()

where	intake_client		= @intake_client
and	intake_id		= @intake_id

if @@rowcount < 1
begin
	RaisError ('The client is not in the system', 16, 1, @intake_client)
	return ( 0 )
end

return ( 1 )
GO
