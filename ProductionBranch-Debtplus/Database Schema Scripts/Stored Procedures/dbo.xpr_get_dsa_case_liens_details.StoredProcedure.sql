﻿USE [DEBTPLUS]
GO
/****** Object:  StoredProcedure [dbo].[xpr_get_dsa_case_liens_details]    Script Date: 1/6/2015 11:52:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[xpr_get_dsa_case_liens_details] (
	@housingloanid	int,
	@propertyid		int
)
as
begin

	declare @currentlenderid	int

	declare @secondarymortgagecompany	varchar(100)
	declare @secondarymortgagesourceid	int
	declare @accountnumber				int
	declare @balance					money
	declare @interestrate				float
	declare @monthsbehind				int
	declare @intakedetailid				int
	declare @lienloantype				int
	declare @loantype					int

	-- lien loan is the loan details of the other loan on the same property than
	-- the current loan

	-- get the loan type
	select @loantype = Loan1st2nd
	  from Housing_loans
	 where oID = @housingloanid

	select case 
			when LN.ServicerID is null then LN.ServicerName 
			else (select top(1) description from Housing_lender_servicers where ServiceID=LN.ServicerID) 
		   end as SecondaryMortgageCompany
		 , LN.ServicerID as SecondaryMortgageCompanySourceID
		 , LN.AcctNum as SecondaryMortgageLoanNumber
		 , LO.CurrentLoanBalanceAmt as Balance
		 , HLD.interestrate as InterestRate
		 , LO.LoanDelinquencyMonths as MonthsBehind
      from Housing_loans as LO
left outer join Housing_lenders as LN on LO.CurrentLenderID = LN.oID
left outer join Housing_loan_details as HLD on LO.IntakeDetailID = HLD.oID
     where LO.PropertyID = @propertyid 
       and LO.Loan1st2nd <> @loantype

end