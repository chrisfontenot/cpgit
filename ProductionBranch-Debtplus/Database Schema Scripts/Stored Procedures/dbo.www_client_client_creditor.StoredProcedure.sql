USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_client_creditor]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_client_creditor] ( @client as int, @client_creditor as int ) AS
-- =======================================================================================
-- ==            Determine the client/creditor debt information                         ==
-- =======================================================================================

-- ChangeLog
--   1/13/2001
--     Changed because the "first_payment_date", "first_payment_amount", and "last_payment_date" was moved
--     to the registers_client_creditor based upon first_payment and last_payment pointers.
--   9/31/2002
--     Added support for client_creditor_balances table
--   1/28/2004
--     Zero disbursement and balance on reassigned debts

-- Disable intermediate results
set nocount on

-- Return the date for the transaction. Format it for the proper style
select
	isnull(cc.creditor+' ','') + coalesce(cr.creditor_name, cc.creditor_name,'')	as 'creditor_name',
	coalesce(cc.message, cc.account_number, '')					as 'account_number',
	isnull(bal.orig_balance,0)							as 'original_balance',

	-- Make reassigned debts show a zero balance even if they don't have one.
	case
		when cc.reassigned_debt = 0 then isnull(bal.orig_balance_adjustment,0)
		else isnull(bal.total_payments,0) - isnull(bal.orig_balance,0) - isnull(bal.total_interest,0)
	end										as 'original_balance_adjustment',

	isnull(bal.total_interest,0)							as 'total_interest',
	isnull(bal.total_payments,0)							as 'total_payments',

	-- Make reassigned debts show zero disbursement even if they don't have one.
	case
		when cc.reassigned_debt = 0 then isnull(cc.disbursement_factor,0)
		else convert(money,0)
	end										as 'disbursement_factor',

	isnull(bal.payments_month_1,0)					as 'last_month_disbursement',
	isnull(bal.payments_month_0,0)					as 'current_month_disbursement',
	convert(varchar(10), rcc_first.date_created, 1)					as 'first_payment_date',
	convert(varchar(10), v.last_payment_date, 1)					as 'last_payment_date',
	isnull(v.last_payment_amount, 0)						as 'last_payment_amount',
	convert(varchar(10), cc.last_stmt_date, 1)					as 'last_statement_date',
	cc.expected_payout_date								as 'payout_date',
	coalesce(cc.dmp_interest, cr.lowest_apr_pct, cr.medium_apr_pct, cr.highest_apr_pct) as 'interest_rate',
	cr.division									as 'creditor_division_name',
	v.client_name									as 'account_name'

from	client_creditor cc with (nolock)
inner join client_creditor_balances bal with (nolock) on cc.client_creditor_balance = bal.client_creditor_balance
inner join view_last_payment v with (nolock) on cc.client_creditor = v.client_creditor
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join people p with (nolock) on cc.client = p.client and 1 = p.relation
left outer join registers_client_creditor rcc_first with (nolock) on cc.first_payment = rcc_first.client_creditor_register
left outer join registers_client_creditor rcc_last  with (nolock) on cc.last_payment  = rcc_last.client_creditor_register
where	cc.client = @client
and	cc.client_creditor = @client_creditor

return ( @@rowcount )
GO
