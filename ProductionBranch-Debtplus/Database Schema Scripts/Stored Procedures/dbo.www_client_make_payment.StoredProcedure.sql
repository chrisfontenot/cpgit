USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_make_payment]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_make_payment] ( @client as int ) as

-- Suppress intermediate results
SET NOCOUNT ON;

-- Find the client name and email address
declare	@name		varchar(256)
declare	@email		varchar(256)
select	@name	= dbo.format_normal_name(default, pn.first, pn.middle, pn.last, pn.suffix),
		@email	= e.address
from	people p
left outer join names pn on p.nameid = pn.name
left outer join emailaddresses e on p.emailid = e.email
where	p.client	= @client
and		p.relation	= 1;

if @name is null
	select	@name = ''

if @email is null
	select	@email = ''

-- Build the resulting string
select  'https://www.vancodemo.com/cgi-bin/specialwebapp.vps?appid=47725f4b1616795c9ed499e17d152a217fcb15e7b4e3836a0a3e77d469b42c162914d70617173108679164cceed1a911&client_name={1}&client_email={2}&client_id={0}' as format, convert(varchar, @client) as '{0}', @name as '{1}', @email as '{2}'
GO
