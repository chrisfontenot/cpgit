USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_quarterly_sessions_details]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[nfcc_quarterly_sessions_details]  ( @from_date as datetime = null, @to_date as datetime = null )  AS 

-- This is the name of the resulting sheet in the spreadsheet output
declare	@SheetName	varchar(80)
select	@SheetName	= 'Session Details'

-- Find last quarter if required
if (@to_date is null) and (@from_date is null)
begin
	select	@from_date	= min(dt) from calendar where y = datepart(year, getdate()) and q = (select q from calendar where dt = convert(varchar(10), getdate(), 101))
	select	@from_date	= dateadd(m, -3, @from_date)
	select	@to_date	= max(dt) from calendar where y = datepart(year, @from_date) and q = (select q from calendar where dt = convert(varchar(10), @from_date, 101))
end

select	@from_date	= convert(varchar(10), @from_date, 101) + ' 00:00:00',
	@to_date	= convert(varchar(10), @to_date, 101)   + ' 23:59:59';

-- Create the table to hold the results
create table #session_details_1 ( client int, relation int, mix varchar(1), [column] varchar(1), line int, duration int, financial_problem int null);

-- financial counseling one-on-one appointments
insert into #session_details_1 (client, relation, mix, [column], line, duration, financial_problem)
select	p.client, p.relation, isnull(m.nfcc,'X') as mix, convert(varchar(1),'B') as [column], convert(int,0) as line, datediff(minute,ca.[start_time],ca.[end_time]), c.cause_fin_problem1
from	people p
inner join clients c on p.client = c.client
inner join client_appointments ca on p.client = ca.client
inner join appt_types apt on ca.appt_type = apt.appt_type
inner join FirstContactTypes m on apt.contact_type = m.oID
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing = 0
and	apt.bankruptcy = 0;

-- Include housing one-on-one counseling sessions
insert into #session_details_1 (client, relation, mix, [column], line, duration, financial_problem)
select	p.client, p.relation, isnull(m.nfcc,'X') as mix, convert(varchar(1),'C') as [column], convert(int,0) as line, datediff(minute,ca.[start_time],ca.[end_time]), c.cause_fin_problem1
from	people p
inner join clients c on p.client = c.client
inner join client_appointments ca on p.client = ca.client
inner join appt_types apt on ca.appt_type = apt.appt_type
inner join FirstContactTypes m on apt.contact_type = m.oID
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing <> 0
and	apt.appt_type not in (1000); -- exclude reverse mtg

-- Include housing reverse mtg counseling sessions
insert into #session_details_1 (client, relation, mix, [column], line, duration, financial_problem)
select	p.client, p.relation, isnull(m.nfcc,'X') as mix, convert(varchar(1),'D') as [column], convert(int,0) as line, datediff(minute,ca.[start_time],ca.[end_time]), c.cause_fin_problem1
from	people p
inner join clients c on p.client = c.client
inner join client_appointments ca on p.client = ca.client
inner join appt_types apt on ca.appt_type = apt.appt_type
inner join FirstContactTypes m on apt.contact_type = m.oID
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.housing <> 0
and	apt.appt_type in (1000);

-- Bankruptcy pre-filing
insert into #session_details_1 (client, relation, mix, [column], line, duration, financial_problem)
select	p.client, p.relation, isnull(m.nfcc,'X') as mix, convert(varchar(1),'E') as [column], convert(int,0) as line, datediff(minute,ca.[start_time],ca.[end_time]), c.cause_fin_problem1
from	people p
inner join clients c on p.client = c.client
inner join client_appointments ca on p.client = ca.client
inner join appt_types apt on ca.appt_type = apt.appt_type
inner join FirstContactTypes m on apt.contact_type = m.oID
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date
and	ca.office is not null
and	apt.bankruptcy <> 0;

-- Workshop Sessions
select	p.client, p.relation, 'F' as mix, convert(varchar(1),'X') as [column], convert(int,0) as line, isnull(t.duration,0) as duration, c.cause_fin_problem1, w.workshop_type
into	#workshops
from	people p
inner join clients c on p.client = c.client
inner join client_appointments ca on p.client = ca.client
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types t on w.workshop_type = t.workshop_type
where	ca.status in ('K','W')
and	ca.start_time between @from_date and @to_date;

-- Change the column to pre-discharge bankruptcy if needed
update	#workshops
set	[column] = 'F'
from	#workshops w
inner join workshop_types wt on w.workshop_type = wt.workshop_type
where	isnull(wt.description,'') like '%bankruptcy%'

-- Load the workshops
insert into #session_details_1 (client, relation, mix, [column], line, duration, financial_problem)
select	client, relation, mix, [column], convert(int,0) as line, duration, cause_fin_problem1
from	#workshops
where	[column] = 'F';

drop table #workshops;

-- Find the appropriate line number
update	#session_details_1 set line = 0 where mix = 'F';
update	#session_details_1 set line = 1 where mix = 'P';
update	#session_details_1 set line = 2 where mix in ('E','I');
update	#session_details_1 set line = 3 where mix = 'M';

-- Find the number of results for the appointments
create table #results ( sheet varchar(80), location varchar(80), [value] varchar(80) null);

-- Load the session counts
insert into #results (sheet, location, [value])
select @SheetName, [column] + convert(varchar, line + 9) as location, convert(varchar, count(*)) as value
from #session_details_1
where relation = 1
group by [column], [line];

-- Find the results for the average length
insert into #results (sheet, location, [value])
select @SheetName, [column] + convert(varchar, line + 16) as location, avg(convert(float, duration)) as value
from #session_details_1
where relation = 1
group by [column], [line];

-- Change the line number to the financial reason
update	#session_details_1
set	line = 32;

-- Reduced income
update	#session_details_1
set	line = 22
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'RIC';

-- Unemployment
update	#session_details_1
set	line = 23
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'UNE';

-- Poor money management (overspending)
update	#session_details_1
set	line = 24
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'PMM';

-- Excessive spending
update	#session_details_1
set	line = 25
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'OVS';

-- Medical
update	#session_details_1
set	line = 26
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'MED';

-- Divorce
update	#session_details_1
set	line = 27
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'DIV';

-- Death
update	#session_details_1
set	line = 28
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'DEA';

-- Gambling
update	#session_details_1
set	line = 29
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'GAM';

-- Substance Abuse
update	#session_details_1
set	line = 30
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc = 'SUB';

-- Other
update	#session_details_1
set	line = 31
from	#session_details_1 x
inner join financial_problems p on x.financial_problem = p.financial_problem
where	nfcc NOT IN ('RIC', 'UNE', 'PMM', 'OVS', 'MED', 'DIV', 'DEA', 'GAM', 'SUB');

-- Find the results for the average length
insert into #results (sheet, location, [value])
select @SheetName, [column] + convert(varchar, line) as location, convert(varchar, count(*)) as value
from #session_details_1
where relation = 1
group by [column], [line];

drop table #session_details_1
select * from #results;
drop table #results;

return ( 0 );
GO
