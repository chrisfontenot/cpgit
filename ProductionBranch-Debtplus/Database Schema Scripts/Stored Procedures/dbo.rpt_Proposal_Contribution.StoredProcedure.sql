USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_Contribution]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_Contribution] ( @client_creditor AS INT ) AS
-- ===========================================================================================
-- ==              Determine the message text for the creditor proposals                    ==
-- ===========================================================================================

-- ChangeLog
--   1/5/2002
--      Added support for check/eft creditor types
--   7/9/2002
--      Changed the message to indicate the type of fairshare and the percentage rate.
--      Added counselor name to the result set.
--   6/11/2003
--      Corrected fairshare message to show the proper payment method (deduct/bill)
--   08/20/2004
--     Removed fairshare_pct_* and creditor_type_* from the creditor. Added creditor_contribution_pct link.
--   06/09/2009
--      Remove references to debt_number and use client_creditor

SET NOCOUNT ON

DECLARE		@client				typ_client
DECLARE		@creditor			typ_creditor
DECLARE		@creditor_type			varchar(1)
DECLARE		@fairshare_rate			float
DECLARE		@counselor_name			varchar(80)
DECLARE		@check_contribution		FLOAT
DECLARE		@eft_contribution		FLOAT
DECLARE		@no_contribution		BIT
DECLARE		@creditor_id			INT
DECLARE		@eft_creditor			INT

SELECT		@client			= cc.client,
		@creditor		= cc.creditor,
		@check_contribution	= COALESCE(cc.fairshare_pct_check,pct.fairshare_pct_check,0),
		@eft_contribution	= COALESCE(cc.fairshare_pct_eft,pct.fairshare_pct_eft,0),
		@creditor_type		= coalesce(pct.creditor_type_eft,'N'),
		@fairshare_rate		= coalesce(cc.fairshare_pct_eft, pct.fairshare_pct_eft, 0),
		@creditor_id		= cr.creditor_id
FROM		client_creditor cc WITH (NOLOCK)
INNER JOIN	creditors cr WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN	creditor_contribution_pcts pct ON cr.creditor_contribution_pct = pct.creditor_contribution_pct
WHERE		cc.client_creditor = @client_creditor

-- Determine if the creditor is paid by EFT
select	@eft_creditor	= 0
if exists (select * from creditor_methods WHERE creditor = @creditor_id AND type = 'EFT')
	select	@eft_creditor	= 1

-- Fetch the counselor full name from the database
SELECT		@counselor_name		= dbo.format_normal_name(default,cox.first,default,cox.last,default)
FROM		counselors co with (nolock)
left outer join names cox with (nolock) on co.NameID = cox.name
INNER JOIN	clients c with (nolock) on c.counselor = co.counselor
WHERE		c.client		= @client

if @counselor_name is null
	select @counselor_name = 'House Account'

-- ===========================================================================================
-- ==                  Return the appropriate message text                                  ==
-- ===========================================================================================

-- Correct the contribution rate as needed
if @creditor_type = 'N'
	select	@fairshare_rate	= 0.0

if @fairshare_rate <= 0.0
	select	@creditor_type = 'N'

-- Return the appropriate message text
SELECT
	@client as 'client',
	@creditor as 'creditor',
	@client_creditor as 'client_creditor',
	@counselor_name as 'counselor_name',

	CASE @creditor_type
		when 'D' then 'OUR RECORDS INDICATE THAT WE DEDUCT FAIRSHARE AT ' + convert(varchar, @fairshare_rate * 100.0,0) + '%. THANK YOU FOR YOUR CONTRIBUTION.'
		when 'N' then 'OUR RECORDS INDICATE THAT YOU DO NOT CONTRIBUTE FAIRSHARE.'
		else 'OUR RECORDS INDICATE THAT WE INVOICE YOU FOR FAIRSHARE AT ' + convert(varchar, @fairshare_rate * 100.0,0) + '%. THANK YOU.'
	END AS 'message'

RETURN ( 1 )
GO
