USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_budget_categories_other]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_budget_categories_other] ( @client int, @description varchar(256) ) as

-- Suppress intermediate results
set nocount on

-- Create the budget other category id
insert into budget_categories_other ([client], [description]) values (@client, @description)
return ( scope_identity() )
GO
