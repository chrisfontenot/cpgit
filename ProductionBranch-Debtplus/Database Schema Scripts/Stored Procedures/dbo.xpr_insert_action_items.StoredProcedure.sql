USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_action_items]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_action_items] ( @description as varchar(50), @item_group as int = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the action_items table                   ==
-- ========================================================================================
	insert into action_items ( [description], [item_group] ) values ( @description, @item_group )
	return ( scope_identity() )
GO
