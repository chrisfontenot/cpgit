USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_StateNotices_Select]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_StateNotices_Select] ( @state int, @Effective datetime = null ) as
-- =============================================================================
-- ==            Return the state notices for the state and effective date    ==
-- =============================================================================

-- ChangeLog
--  10/16/12
--    Initial

-- Suppress intermediate results
set nocount on

-- Retrieve the message type list. The items should be in the list only once.
select	oID as type_oID,
		convert(int,null) as oID
into	#StateMessageTypes
from	StateMessageTypes

-- Use today's date if one is not given
if @effective is null
	select	@effective = getdate();

-- Remove the time from the date
select	@effective = convert(varchar(10), @effective, 101);

-- Find the corresponding notes for the types
update	#StateMessageTypes
set		oID			= (select TOP 1 m.oID from StateMessages m where t.type_oID = m.StateMessageType and m.State = @state and convert(datetime, convert(varchar(10), m.Effective, 101)) <= @effective ORDER BY m.Effective )
from	#StateMessageTypes t

-- Return the corresponding information
select	b.oID,
		b.state,
		b.StateMessageType,
		b.effective,
		b.reason,
		b.date_updated,
		b.updated_by,
		b.date_created,
		b.created_by,
		b.Details
from	#StateMessageTypes a
inner join statemessages b on a.OID = b.OID

drop table #StateMessageTypes

return ( @@rowcount )
GO
