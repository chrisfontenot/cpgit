USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_rpps_lob]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_rpps_lob] (@rpps_biller_id varchar(20), @biller_lob varchar(50)) as
INSERT INTO rpps_lobs (rpps_biller_id, biller_lob) VALUES (@rpps_biller_id, @biller_lob)
return (scope_identity())
GO
