SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_insert_housing_borrower] (@PersonID as Int = 0,@NameID as Int = null,@EmailID as Int = null,@Former as VarChar(80) = null,@SSN as VarChar(10) = null,@Gender as Int = null,@Race as Int = null,@ethnicity as Int = null,@Language as Int = null,@Disabled as Bit = 0,@marital_status as Int = null,@Education as Int = null,@Birthdate as DateTime = null,@FICO_Score as Int = null,@CreditAgency as VarChar(80) = null,@emp_start_date as DateTime = null,@emp_end_date as DateTime = null,@other_job as VarChar(80) = null,@MilitaryServiceID as Int = null,@MilitaryDependentID as Int = null,@MilitaryGradeID as Int = null,@MilitaryStatusID as Int = null,@bkfileddate as DateTime = null,@bkdischarge as DateTime = null,@bkchapter as Int = null,@bkPaymentCurrent as Int = null,@bkAuthLetterDate as DateTime = null
	-- These fields are not present and are only for historical access
	, @MilitaryService as int = null
	) AS 
BEGIN

	-- =========================================================================================================
	-- ==        Insert a record into the housing_borrowers table for .NET                                    ==
	-- =========================================================================================================
	insert into housing_borrowers ([PersonID],[NameID],[EmailID],[Former],[SSN],[Gender],[Race],[ethnicity],[Language],[Disabled],[MilitaryServiceID],[MilitaryStatusID],[MilitaryGradeID],[MilitaryDependentID],[marital_status],[Education],[Birthdate],[FICO_Score],[CreditAgency],[emp_start_date],[emp_end_date],[other_job],[bkfileddate],[bkdischarge],[bkchapter],[bkPaymentCurrent],[bkAuthLetterDate]) 
				values            (@PersonID, @NameID, @EmailID, @Former, @SSN, @Gender, @Race, @ethnicity, @Language, @Disabled, @MilitaryServiceID, @MilitaryStatusID, @MilitaryGradeID, @MilitaryDependentID, @marital_status, @Education, @Birthdate, @FICO_Score, @CreditAgency, @emp_start_date, @emp_end_date, @other_job, @bkfileddate, @bkdischarge, @bkchapter, @bkPaymentCurrent, @bkAuthLetterDate )

	return (scope_identity())
END
GO
