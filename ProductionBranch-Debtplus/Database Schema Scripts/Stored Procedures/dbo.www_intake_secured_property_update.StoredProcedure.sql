USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_secured_property_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_secured_property_update] ( @intake_client as int, @type as int, @value as money = 0, @first_mortgage as money = 0, @first_lender as varchar(80) = '', @first_account as varchar(80) = '', @second_mortgage as money = 0, @second_lender as varchar(80) = '', @second_account as varchar(80) = '', @third_mortgage as money = 0, @third_lender as varchar(80) = '', @third_account as varchar (80) = '') as

-- Insert the new item into the list.
declare @secured_property	int
insert into intake_secured_properties (intake_client, secured_type, current_value, year_mfg, year_acquired, original_price, description, sub_description) values ( @intake_client, @type, @value, 0, 0, 0, '', '' )

select	@secured_property	= scope_identity()

-- Insert the mortgage information if needed
if @first_mortgage > 0
begin
	-- Find the loan type for 'conventional'
	declare	@loan_type	int
	select	@loan_type	= oID
	from	Housing_LoanTypes
	WHERE	[default]	= 1

	if @loan_type is null
		select	@loan_type	= oID
		from	Housing_LoanTypes
		WHERE	[default]	= 1
		and		description like '%conventional%'

	if @loan_type is null
		select	@loan_type	= min(oID)
		from	Housing_LoanTypes

	if @loan_type is null
		select	@loan_type	= 1

	insert into intake_secured_loans ( secured_property, priority, lender, account_number, balance, loan_type, past_due_periods, past_due_amount, periods, original_amount, payment, interest_rate, case_number )
	values ( @secured_property, 1, @first_lender, @first_account, @first_mortgage, @loan_type, 0, 0, 0, 0, 0, 0, '' )
end
GO
