USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_appt_booked]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_appt_booked]  ( @client as int ) AS

-- ==================================================================================================
-- ==            Fetch the appointment list for the client.                                        ==
-- ==================================================================================================

-- ChangeLog
--   12/24/2002
--     Removed "housing" from the client_appointments table
--    6/19/2008
--     Added "class" to determine if the appointment is a workshop or not

SELECT	ca.client_appointment					as 'item_key',
		ca.start_time							as 'start_time',
        isnull(o.name,'Unknown')				as 'office',
		case
			when ca.appt_type is null then 'Any Type'
			else isnull(t.description, 'Unknown')
		end										as 'type',
        dbo.format_normal_name(default,con.first,default,con.last,default) as 'counselor',
		ca.priority								as 'priority',
		0										as 'housing',
		ca.confirmation_status					as 'confirmation_status',
        'C'										as 'class'

FROM		client_appointments ca	WITH (NOLOCK)
LEFT OUTER JOIN appt_times         a	WITH (NOLOCK) ON ca.appt_time = a.appt_time
LEFT OUTER JOIN	counselors         c	WITH (NOLOCK) ON ca.counselor = c.counselor
LEFT OUTER JOIN names              con  WITH (NOLOCK) ON c.NameID     = con.name
LEFT OUTER JOIN	offices            o	WITH (NOLOCK) ON a.office	  = o.office
LEFT OUTER JOIN	AppointmentTypes   t	WITH (NOLOCK) ON ca.appt_type = t.oID
LEFT OUTER JOIN AppointmentConfirmationTypes   cnf  WITH (NOLOCK) ON ca.confirmation_status = cnf.oID
WHERE	status		= 'P'
AND     ca.office is not null
AND		ca.client	= @client

UNION ALL

SELECT	ca.client_appointment					as 'item_key',
        wk.start_time                           as 'start_time',
		isnull(wko.name,'Workshop')             as 'office',
        isnull(wkt.description,'')              as 'type',
        isnull(dbo.format_normal_name(default,wkcon.first,default,wkcon.last,default),'')   as 'counselor',
		ca.priority								as 'priority',
		0               						as 'housing',
		ca.confirmation_status					as 'confirmation_status',
        'W'                                     as 'class'

FROM		client_appointments ca	WITH (NOLOCK)
LEFT OUTER JOIN workshops          wk	WITH (NOLOCK) ON ca.workshop = wk.workshop
LEFT OUTER JOIN counselors         wkco WITH (NOLOCK) ON wk.counselor = wkco.person
LEFT OUTER JOIN names              wkcon WITH (NOLOCK) ON wkco.NameID = wkcon.name
LEFT OUTER JOIN workshop_locations wko	WITH (NOLOCK) ON wk.workshop_location = wko.workshop_location
LEFT OUTER JOIN workshop_types     wkt	WITH (NOLOCK) ON wk.workshop_type = wkt.workshop_type
LEFT OUTER JOIN AppointmentConfirmationTypes   cnf  WITH (NOLOCK) ON ca.confirmation_status = cnf.oID

WHERE	status		= 'P'
AND     ca.workshop is not null
AND		ca.client	= @client

ORDER BY 2

RETURN ( @@rowcount )
GO
