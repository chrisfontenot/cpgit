USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CDT]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CDT] ( @rpps_response_file as int ) AS

-- ====================================================================================================================
-- ==            This is a terminate item. The response is basically ignored since we did not inititate it.          ==
-- ====================================================================================================================

set nocount on
set xact_abort on

-- Cleanup
return ( 1 )
GO
