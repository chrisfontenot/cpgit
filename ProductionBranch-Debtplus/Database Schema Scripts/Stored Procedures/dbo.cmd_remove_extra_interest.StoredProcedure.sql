USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_remove_extra_interest]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_remove_extra_interest] as

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Find the last interest transaction for the system 
declare @latest_date	datetime
select	@latest_date = max(date_created) from registers_client_creditor where tran_type = 'IN'

if @latest_date is not null
begin
	-- Make the time relative to the start of the day. We don't need fractions of a second to mess things up.
	select	@latest_date = convert(datetime, convert(varchar(10), @latest_date, 101) + ' 00:00:00')

	-- Find the latest transaction for interest that occured on or after the cutoff date
	-- This limits the transactions to the last interest recorded and not something that happened
	-- two years ago.
	select	client_creditor, max(date_created) as date_created, convert(int,null) as client_creditor_register, convert(money,0) as amount
	into	#in
	from	registers_client_creditor
	where	tran_type = 'IN'
	and	date_created >= @latest_date
	group by client_creditor;

	-- Find the specific transaction for this interest figure. We don't just want the last interest
	-- for any debt as it may be from two years ago.
	update	#in
	set	client_creditor_register = rcc.client_creditor_register,
		amount	= rcc.credit_amt
	from	#in i
	inner join registers_client_creditor rcc on i.client_creditor = rcc.client_creditor and i.date_created = rcc.date_created
	where	rcc.tran_type = 'IN'

	-- Adjust the debt statistics
	update	client_creditor_balances
	set	total_interest = total_interest - i.amount
	from	client_creditor cc
	inner join client_creditor_balances b on b.client_creditor = cc.client_creditor
	inner join #in i on i.client_creditor = cc.client_creditor

	-- Adjust the payee statistics
	update	client_creditor
	set	interest_this_creditor = interest_this_creditor - i.amount
	from	client_creditor cc
	inner join #in i on i.client_creditor = cc.client_creditor

	-- Remove the interest transactions
	delete	registers_client_creditor
	from	registers_client_creditor rcc
	inner join #in i on rcc.client_creditor_register = i.client_creditor_register

	-- Discard the working table
	drop table #in
end
GO
