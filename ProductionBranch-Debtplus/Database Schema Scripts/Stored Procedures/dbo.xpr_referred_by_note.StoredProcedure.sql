USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_referred_by_note]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_referred_by_note] ( @referred_by as int ) AS

-- ====================================================================================================
-- ==            Return the note text associated with the referral source. Normally, you can         ==
-- ==            just access the referred_by table, but I am trying to break the linkage from        ==
-- ==            the program to knowing the table layout. So, I have this procedure.                 ==
-- ====================================================================================================

-- Return the note information
select	isnull(note,'')		as 'note'
from	referred_by
where	referred_by		= @referred_by

return ( @@rowcount )
GO
