USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Recon_Close]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_Recon_Close] ( @Batch AS VarChar(80), @ClearedBalance AS Money = 0 ) AS

-- ========================================================================================================================
-- ==            Mark the reconciled batch as being closed                                                               ==
-- ========================================================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

/*
-- Update the configuration file with the new cleared balance
UPDATE	config
SET	cleared_trust_account_balance	= @ClearedBalance
*/

-- Return success to the caller
RETURN ( 1 )
GO
