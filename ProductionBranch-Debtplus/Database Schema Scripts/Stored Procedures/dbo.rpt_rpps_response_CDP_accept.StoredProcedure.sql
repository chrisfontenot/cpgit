USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_response_CDP_accept]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_rpps_response_CDP_accept] ( @response_batch_id as varchar(50) ) AS

-- =======================================================================================================
-- ==           Obtain the information for the response conditions of rejected proposals                ==
-- =======================================================================================================

-- ChangeLog
--   2/18/2004
--      Added client's counselor name
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress the count of records
set nocount on

-- Retrieve the information for the records
select		b.date_transmitted						as 'proposal_print_date',
		pr.proposal_status_date						as 'reject_date',
		isnull(pr.proposed_amount,cc.disbursement_factor)		as 'proposed_amount',
		coalesce(pr.proposed_start_date,cc.start_date,c.start_date)	as 'start_date',

		cc.client							as 'client',
		cc.creditor							as 'creditor',
		cc.client_creditor							as 'client_creditor',
		cc.account_number							as 'account_number',
		cc.disbursement_factor						as 'disbursement_factor',

		isnull(cr.creditor_name,'')					as 'creditor_name',
		convert(varchar(80), dbo.format_normal_name(default,cox.first,default,cox.last,default))					as 'counselor_name'

from		rpps_transactions rt
inner join	client_creditor_proposals pr ON rt.client_creditor_proposal = pr.client_creditor_proposal
inner join	client_creditor cc ON pr.client_creditor = cc.client_creditor
inner join	creditors cr on cc.creditor = cr.creditor
inner join	clients c on cc.client = c.client
left outer join counselors co			with (nolock) on c.counselor = co.counselor
left outer join names cox with (nolock) on co.nameid = cox.name
left outer join	proposal_batch_ids b on pr.proposal_batch_id = b.proposal_batch_id
where		response_batch_id = @response_batch_id
and		service_class_or_purpose = 'CDP'
and		pr.proposal_status = 3

order by	1

return ( @@rowcount )
GO
