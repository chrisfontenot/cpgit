USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_kept_appt_day_45]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DAILY_kept_appt_day_45]  ( @from_date as datetime = null, @to_date as datetime = null, @event as int = null ) as
-- =============================================================================================================================
-- ==            Custom stored procedure for internet followup letters and retention events                                   ==
-- =============================================================================================================================

-- Default the parameters when they are not supplied (which is the normal condition)
if @event is null
	select	@event	= 70

if @to_date is null
	select	@to_date = dateadd(d, -30, getdate())

if @from_date is null
	select	@from_date = @to_date

select	@from_date	= convert(varchar(10), @from_date, 101) + ' 00:00:00',
	@to_date	= convert(varchar(10), @to_date, 101) + ' 23:59:59'

-- Find the next non-holiday day from today.
DECLARE	@letter_date	datetime
SELECT	@letter_date	= dbo.nextbankingday ( getdate() )

-- Find the client and the client appointment for the followup information
select	ca.client,
	ca.client_appointment,
	@letter_date as letter_date
into	#selected
from	client_appointments ca
inner join clients c on ca.client = c.client
inner join appt_types apt on ca.appt_type = apt.appt_type
inner join FirstContactTypes m on apt.contact_type = m.oID
where	ca.status in ('K','W')
and	ca.result	= 'DMP'
and	m.nfcc		<> 'I'
and	c.active_status = 'APT'
and	c.client_status = 'DMP'
and	ca.start_time between @from_date and @to_date;

-- Insert into the report table
insert into customfollowupevents (client, client_appointment, letter_date)
select	* from #selected

-- Generate a system note that we created the letter. It won't be printed until later.
insert into client_notes (client, type, subject, note)
select	s.client,
	3,
	'Printing kept appt day 30 follow-up letter',
	'Printed Kept appt day 30 follow up letter on ' + convert(varchar(10), s.letter_date, 101) + ' regarding the appointment on ' + convert(varchar(10), ca.start_time, 101) + '.'
from	#selected s
inner join client_appointments ca on s.client_appointment = ca.client_appointment

/*
-- Generate the followup event 15 days from now for the client
insert into client_retention_events ( client, retention_event, amount, message, expire_type, expire_date, priority, effective_date )
select	client, @event as retention_event, 0 as amount, ev.description, ev.expire_type, dateadd(d, 30, getdate()) as expire_date, ev.priority, dateadd(d, 15, getdate()) as effective_date
from	#selected x
inner join retention_events ev on ev.retention_event = @event
*/

drop table #selected
return ( 0 )
GO
