USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Delete_Creditor]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_Delete_Creditor] ( @Creditor AS typ_creditor ) AS
-- ==========================================================================
-- ==               Delete a Client                                        ==
-- ==========================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

declare @creditor_name varchar(256)
select @creditor_name = isnull(creditor_name,'some un-named deleted creditor') from creditors where creditor = @creditor
if not exists (select * from creditors where creditor = @creditor)
begin
	RaisError (50029, 11, 1, @creditor)
	return ( 0 )
end

set nocount on

-- Remove any client/creditor notes
delete from creditor_notes where creditor = @creditor

-- Go through and invalidate any proposal information
delete from client_creditor_proposals where client_creditor in (select client_creditor from client_creditor where creditor = @creditor)

-- Remove the references in the additional sort keys
delete from creditor_addkeys where creditor = @creditor

-- Remove the contact information
delete from creditor_contacts where creditor = @creditor

-- remove the address information
delete from creditor_addresses where creditor = @creditor

-- remove the contribution percentage table items
delete from creditor_contribution_pcts where creditor = @creditor

-- remove the invoice information
delete from registers_invoices where creditor = @creditor

-- Remove the information from the creditor detail table
delete from	registers_creditor where creditor = @creditor

-- Remove the information from the client/creditor detail table
delete from registers_client_creditor where creditor = @creditor

-- Remove the references in the trust register
update registers_trust set creditor = null where creditor = @creditor

-- Invalidate the creditor references for the client
update client_creditor set creditor_name = @creditor_name, @creditor = null where creditor = @creditor

-- Update the rpps transaction table
delete	rpps_transactions
from	rpps_transactions r
inner join registers_client_creditor rcc on r.client_creditor_register = rcc.client_creditor_register
where	rcc.creditor = @creditor
and	r.service_class_or_purpose = 'CIE'

delete
from	rpps_transactions
WHERE	creditor = @creditor

-- remove the creditor notes
delete from creditor_notes where creditor = @creditor

-- remove the creditor
delete from creditors where creditor = @creditor

return ( 1 )
GO
