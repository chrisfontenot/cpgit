USE [DEBTPLUS]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_BudgetDetail_ByBudget]    Script Date: 12/8/2015 2:11:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[rpt_PrintClient_BudgetDetail_ByBudget] ( @Budget AS INT, @Language AS INT = 1 ) AS
-- ======================================================================================================
-- ==                List the budget categories by the budget category                                 ==
-- ======================================================================================================
SET NOCOUNT ON
DECLARE @Client INT

-- Fetch the client ID from the budget list
SELECT	@Client = client
FROM	budgets
WHERE	budget = @Budget

-- Ignore the fact that the client may be missing. Assume an invalid client for the purposes of additional budget categories.
IF @Client IS NULL
	select @client = -1

-- Build a list of the budget detail items and the heading entries
create table #heading_categories (budget_category int, description varchar(80) null, heading_category int null, heading_description varchar(80) null, client_amount money, suggested_amount money);

if @language = 2
begin
	insert into #heading_categories (budget_category, description, heading_category, heading_description, client_amount, suggested_amount)
	select a.budget_category, a.spanish_description, (select max(budget_category) from budget_categories b where b.budget_category < a.budget_category and b.heading = 1) as heading_category, convert(varchar(80), null) as heading_description, convert(money,0) as client_amount, convert(money,0) as suggested_amount
	from budget_categories a
	where detail = 1
	order by a.budget_category

	update #heading_categories set heading_description = b.spanish_description
	from #heading_categories a inner join budget_categories b on a.heading_category = b.budget_category;

	-- Add the additional categories to the list
	insert into #heading_categories (budget_category, description, heading_category, heading_description, client_amount, suggested_amount)
	select a.budget_category, 'Otra partida de gastos' as description, 10000 as heading_category, 'OTROS GASTOS' as heading_description, convert(money,0) as client_amount, convert(money,0) as suggested_amount
	from budget_categories_other a
	where client = @client
end
else
begin
	insert into #heading_categories (budget_category, description, heading_category, heading_description, client_amount, suggested_amount)
	select a.budget_category, a.description, (select max(budget_category) from budget_categories b where b.budget_category < a.budget_category and b.heading = 1) as heading_category, convert(varchar(80), null) as heading_description, convert(money,0) as client_amount, convert(money,0) as suggested_amount
	from budget_categories a
	where detail = 1
	order by a.budget_category

	update #heading_categories set heading_description = b.description
	from #heading_categories a inner join budget_categories b on a.heading_category = b.budget_category;

	-- Add the additional categories to the list
	insert into #heading_categories (budget_category, description, heading_category, heading_description, client_amount, suggested_amount)
	select a.budget_category, a.description, 10000 as heading_category, 'OTHER EXPENSES' as heading_description, convert(money,0) as client_amount, convert(money,0) as suggested_amount
	from budget_categories_other a
	where client = @client
end

-- Update the amounts from the details
update #heading_categories
set client_amount = isnull(bd.client_amount,0),
    suggested_amount = isnull(bd.suggested_amount,0)
from budget_detail bd
inner join #heading_categories cat on cat.budget_category = bd.budget_category
where bd.budget = @budget;

-- Toss the empty items
delete from #heading_categories where client_amount = 0 and suggested_amount = 0;

-- Split the list back into the proper form at this point
create table #t_printclient_budgetcategories (budget_category int, description varchar(50), heading int, detail int, client_amount money, suggested_amount money, heading_category int null, heading_description varchar(80) null);

-- Start by adding the detail items
insert into #t_printclient_budgetcategories (budget_category, description, heading, detail, client_amount, suggested_amount, heading_category, heading_description)
select budget_category, description, 0, 1, client_amount, suggested_amount, heading_category, heading_description
from #heading_categories;

-- Finish by adding the headings
insert into #t_printclient_budgetcategories (budget_category, description, heading, detail, client_amount, suggested_amount, heading_category, heading_description)
select distinct heading_category, heading_description, 1, 0, 0 as client_amount, 0 as suggested_amount, heading_category, heading_description
from #heading_categories;

-- Return the resulting list
select budget_category, description, heading, detail, client_amount, suggested_amount, @client as client, heading_category, heading_description
from #t_printclient_budgetcategories
order by 1;

drop table #t_printclient_budgetcategories
drop table #heading_categories
	
-- Return success to the caller
RETURN ( @@rowcount )
