USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_action_items_other]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_action_items_other] ( @action_plan as int, @item_group as int, @description as varchar(50) ) as
	insert into action_items_other([action_plan], [item_group], [description]) values (@action_plan, @item_group, @description)
	return ( scope_identity() )
GO
