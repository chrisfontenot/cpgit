USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_hud_grant_types]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_hud_grant_types] AS
-- =====================================================================================================================
-- ==            Retrieve the list of hud grant types from the messages table                                         ==
-- =====================================================================================================================

-- ChangeLog
--   2/01/2002
--     Switch to the messages table

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
FROM	Housing_GrantTypes
ORDER BY 1

return ( @@rowcount )
GO
