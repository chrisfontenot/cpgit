SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [www_creditor_debt_transactions] ( @creditor as typ_creditor, @client_creditor as int, @date_range as varchar(20) = NULL ) as
-- ===============================================================================
-- ==            List the trust activity for the creditor actions               ==
-- ===============================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on
declare	@client		int

-- Retrieve the client and debt number from the transaction
select	@client		= client
from	client_creditor
where	creditor	= @creditor
and	client_creditor	= @client_creditor

if @@rowcount < 1
begin
	RaisError ('You do not have access to the transactions for this request', 16, 1)
	return ( 0 )
end

-- Date ranges
declare	@from_date	datetime
declare	@to_date	datetime
select	@date_range	= lower(@date_range)

-- Determine the current month range
if @date_range = 'current_month'
begin
	select	@to_date = getdate()
	select	@from_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
end

-- Find the last 60 days information
else if @date_range = 'last_60'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -1, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 90 days information
else if @date_range = 'last_90'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -2, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last 120 days information
else if @date_range = 'last_120'
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = dateadd (m, -3, @to_date)
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Find the last year information
else if @date_range = 'last_year'
begin
	select	@from_date = dateadd (yy, -1, getdate())
	select	@to_date = convert(datetime, '12/01/' + convert(varchar, year(@from_date))) + ' 23:59:59'
	select	@from_date = convert(datetime, '01/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Assume that the default is "last_month"
else
begin
	select	@to_date = getdate()
	select	@to_date = convert(datetime, convert(varchar, month(@to_date)) + '/01/' + convert(varchar, year(@to_date))) + ' 00:00:00'
	select	@to_date = dateadd (s, -1, @to_date)
	select	@from_date = @to_date
	select	@from_date = convert(datetime, convert(varchar, month(@from_date)) + '/01/' + convert(varchar, year(@from_date))) + ' 00:00:00'
end

-- Remove the times from the date fields
select	@from_date = convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00')
select	@to_date   = convert(datetime, convert(varchar(10), @to_date, 101)   + ' 23:59:59')

-- Retrieve the transactions for disbursements to the creditor
select		rcc.client_creditor_register			as 'item_key',
		convert(varchar(10), rcc.date_created, 101)	as 'date_created',
		rcc.tran_type					as 'tran_type',
		rcc.credit_amt					as 'credit_amt',
		rcc.debit_amt					as 'debit_amt',
		convert(varchar,tr.checknum)	as 'check_number',
		isnull(tr.cleared,' ')				as 'cleared',
		convert(varchar(10), tr.reconciled_date,101)	as 'date_reconciled'
from		registers_client_creditor rcc with (nolock)
left outer join	registers_trust tr with (nolock) on rcc.trust_register = tr.trust_register
where		rcc.creditor	= @creditor
and		rcc.client	= @client
and		rcc.client_creditor	= @client_creditor
and		rcc.date_created between @from_date and @to_date
order by	rcc.date_created
return ( @@rowcount )
GO
