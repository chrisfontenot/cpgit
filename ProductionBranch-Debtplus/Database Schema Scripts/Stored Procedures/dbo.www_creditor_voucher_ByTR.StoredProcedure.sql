SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
ALTER procedure [www_creditor_voucher_ByTR] ( @creditor as typ_creditor, @trust_register as int ) AS
-- =================================================================================================================
-- ==            Return the check voucher given the trust register                                                ==
-- =================================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate result sets
set nocount on

-- Verify that the creditor has the proper ID for the item
declare	@trust_creditor		varchar(10)
declare	@tran_type		varchar(2)
declare	@checknum		bigint
declare	@check_amount		money
declare	@check_date		datetime
declare	@cleared		varchar(2)
declare	@date_reconciled	datetime

select	@trust_creditor		= creditor,
	@checknum		= checknum,
	@tran_type		= tran_type,
	@check_amount		= amount,
	@check_date		= date_created,
	@cleared		= cleared,
	@date_reconciled	= reconciled_date
from	registers_trust with (nolock)
where	trust_register		= @trust_register
and	tran_type in ('AD', 'BW', 'MD', 'CM')

if @@rowcount < 1
begin
	RaisError ('The check register ID is not valid for this creditor', 16, 1)
	return ( 0 )
end

-- If the transaction is a bank wire then calculate the portion of the wire for this creditor
if @tran_type = 'BW'
begin
	select	@check_amount	= sum( case when creditor_type = 'D' then debit_amt - fairshare_amt else debit_amt end )
	from	registers_client_creditor with (nolock)
	where	creditor = @creditor
	and	tran_type = 'BW'
	and	trust_register = @trust_register

end else begin

	if @creditor != @trust_creditor
	begin
		RaisError ('The check register ID is not valid for this creditor', 16, 1)
		return ( 0 )
	end
end

declare	@attn		varchar(80)
DECLARE @Addr1		varchar(256)
DECLARE @Addr2		varchar(256)
DECLARE @Addr3		varchar(256)
DECLARE @Addr4		varchar(256)
DECLARE @Addr5		varchar(256)
DECLARE @Addr6		varchar(256)

-- Use the creditor address if this is a creditor check
create table #www_creditor_address (line int, address varchar(80) null)

IF @creditor IS NOT NULL
BEGIN
	select	@attn		= attn,
			@addr1		= addr1,
			@addr2		= addr2,
			@addr3		= addr3,
			@addr4		= addr4,
			@addr5		= addr5,
			@addr6		= addr6
	from	view_creditor_addresses
	where	creditor	= @creditor
	and	type		= 'P'

	insert into #www_creditor_address (line, address) values (1, 'ATTN: ' + @attn)
	insert into #www_creditor_address (line, address) values (2, @addr1)
	insert into #www_creditor_address (line, address) values (3, @addr2)
	insert into #www_creditor_address (line, address) values (4, @addr3)
	insert into #www_creditor_address (line, address) values (5, @addr4)
	insert into #www_creditor_address (line, address) values (6, @addr5)
	insert into #www_creditor_address (line, address) values (7, @addr6)
END

select		upper(address) as 'address'
from		#www_creditor_address
where		address is not null
order by	line

drop table #www_creditor_address

-- Return the information about the check
select	convert(varchar(20),@checknum)	as 'check_number',
	@tran_type							as 'tran_type',
	@check_amount							as 'check_amount',
	convert(varchar(10), @check_date, 1)				as 'date_created',
	isnull(@cleared, ' ')						as 'cleared',
	convert(varchar(10), @date_reconciled, 1)			as 'date_reconciled'

-- Return the transactions for the check
select	v.client							as 'client',
	isnull ( rcc.account_number, v.account_number)			as 'account_number',
	v.client_name							as 'name',
	rcc.debit_amt							as 'debit_amt',
	rcc.fairshare_amt						as 'fairshare_amt',
	rcc.creditor_type						as 'creditor_type',
	v.client_creditor						as 'client_creditor'

from	registers_client_creditor rcc with (nolock)
left outer join view_last_payment v with (nolock) on rcc.client_creditor = v.client_creditor
where	rcc.creditor = @creditor
and	rcc.tran_type in ('AD', 'BW', 'MD', 'CM')
and	rcc.trust_register = @trust_register
order by 2, 1

return ( @@rowcount )
GO
