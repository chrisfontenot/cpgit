USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_method_first_contact]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_method_first_contact] AS
select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
from	FirstContactTypes
order by 2
GO
