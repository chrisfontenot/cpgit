USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_proposal_item_remove]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_proposal_item_remove] ( @client_creditor_proposal as int ) AS

-- =========================================================================================================
-- ==            Remove the proposal from the batch                                                       ==
-- =========================================================================================================

-- Take the proposal out of a batch.
update	client_creditor_proposals
set		proposal_batch_id = null
where	client_creditor_proposal = @client_creditor_proposal

return ( @@rowcount )
GO
