USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_create_file]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_create_file] ( @bank as int = null, @filename as varchar(1024) = null ) as

-- suppress intermediate results
set nocount on
declare	@epay_file	int

-- Find the corresponding bank information
if isnull(@bank,0) <= 0
	select	@bank	= null

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'E'
	and	[default]	= 1

if @bank is null
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'E'

if @bank is null
	select	@bank	= 1

-- Create the file reference
insert into epay_files (bank, date_created, created_by) values (@bank, getdate(), suser_sname())
select	@epay_file	= scope_identity()

-- Return the file number
return ( @epay_file )
GO
