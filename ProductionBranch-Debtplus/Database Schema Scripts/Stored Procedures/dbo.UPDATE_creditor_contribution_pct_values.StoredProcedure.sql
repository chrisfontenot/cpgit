USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_creditor_contribution_pct_values]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_creditor_contribution_pct_values] as

-- Find the highest effective date record for each creditor
select	creditor, max(effective_date) as effective_date, convert(datetime,null) as date_created, convert(int,null) as creditor_contribution_pct
into	#pcts
from	creditor_contribution_pcts
where	effective_date < getdate()
group by creditor

-- Now, since there are duplicates, find the highest for the date_created on each contribution record
select	creditor, effective_date, max(date_created) as date_created
into	#pcts_dup
from	creditor_contribution_pcts
where	effective_date < getdate()
group by creditor, effective_date

-- Update the created dates on the contribution records
update	#pcts set date_created = b.date_created
from	#pcts a
inner join #pcts_dup b on a.creditor = b.creditor and a.effective_date = b.effective_date

-- Find the contribution record for each creditor
update	#pcts
set		creditor_contribution_pct = x.creditor_contribution_pct
from	#pcts p
inner join creditor_contribution_pcts x on p.creditor = x.creditor and p.effective_date = x.effective_date and p.date_created = x.date_created

-- Clear the pointers in the creditor references
update	creditors
set		creditor_contribution_pct	= null;

-- Update the creditors with the new record pointers
update	creditors
set		creditor_contribution_pct = x.creditor_contribution_pct
from	creditors cr
inner join	#pcts x on cr.creditor = x.creditor

-- Cleanup tables
drop table #pcts_dup
drop table #pcts
GO
