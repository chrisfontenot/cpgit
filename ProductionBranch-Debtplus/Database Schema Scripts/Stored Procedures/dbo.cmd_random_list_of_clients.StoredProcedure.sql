USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_random_list_of_clients]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_random_list_of_clients] ( @desired_pct float = 0.02 ) as

-- This is the percentage of clients. It must be less than 1.0. "2%" is 0.02.
-- declare	@desired_pct	float
-- select	@desired_pct = 0.02		-- 2 percent

-- Find the list of active clients
create table #total_clients (client int, selected int, pk int identity(0,1));
create unique clustered index ix_total_clients_1 on #total_clients ( pk );
create index ix_total_clients_2 on #total_clients ( selected );

insert into #total_clients (client, selected)
select	client, convert(int,0) as selected
from	clients
where	active_status in ('A','AR')

declare	@total_client_count	int
select	@total_client_count = count(*) from #total_clients

if @total_client_count <= 0
begin
	raiserror ('There are no active clients', 16, 1)

end else begin

	-- Find the number desired
	declare	@desired_count	int
	select	@desired_count = convert(int, convert(float, @total_client_count) * @desired_pct)

	-- Generate the clients until we found a suitable match
	declare	@current_count	int
	select	@current_count = count(*) from #total_clients where selected = 1
	while @current_count < @desired_count
	begin

		-- Find the relative client
		declare	@pk	int
		select	@pk = convert(int, rand() * convert(float, @total_client_count))

		-- Set the relative client to "selected" status
		update	#total_clients
		set	selected = 1
		where	pk = @pk;

		-- Update the count of selected clients
		select	@current_count = count(*) from #total_clients where selected = 1
	end

	-- Select the information from indicated clients
	select	v.client, v.name, v.addr1, v.addr2, v.addr3, v.zipcode, pn.First as first, pn.Last as last, e.Address as email
	from	view_client_address v
	left outer join people p on v.client = p.client and 1 = p.Relation
	left outer join Names pn on p.NameID = pn.name
	left outer join EmailAddresses e on p.EmailID = e.Email
	inner join #total_clients c on v.client = c.client
	where	c.selected = 1
	order by v.client;
end

-- Drop the working table
drop table #total_clients
GO
