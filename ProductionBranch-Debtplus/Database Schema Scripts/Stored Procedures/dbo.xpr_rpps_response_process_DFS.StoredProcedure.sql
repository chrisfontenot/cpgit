USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_DFS]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_DFS] ( @rpps_response_file as int ) AS

-- ====================================================================================================================
-- ==            This is a fairshare payment to the agency. Discover is the only one who uses this!!		     ==
-- ====================================================================================================================

set nocount on
set xact_abort on

declare	@creditor		varchar(10)
declare	@ledger_code		varchar(50)
declare	@amount			money
declare	@rpps_response_detail	int
declare	@biller_id		varchar(12)

-- Start a transaction
begin transaction

-- Find the information from the tables for the transaction
select	@biller_id			= rpps_biller_id,
	@rpps_response_detail		= rpps_response_detail,
	@amount				= gross
from	rpps_response_details with (nolock)
where	service_class_or_purpose	= 'DFS'
and	processing_error		is null
and	rpps_response_file		= @rpps_response_file;

if @rpps_response_detail is not null
begin

	-- Find the ledger code for the transactions
	select	@ledger_code			= ledger_code
	from	ledger_codes with (nolock)
	where	description like		'%fairshare%';

	if @ledger_code is null
		select	@ledger_code		= ledger_code
		from	ledger_codes with (nolock)
		where	description like	'%contribution%';

	if @ledger_code is null
		select	@ledger_code		= '1000';		-- Ledger code for the NON-AR transaction

	-- Find the creditor for the contribution
	select	@creditor		= cr.creditor
	from	creditor_methods m	with (nolock)
	inner join creditors cr		with (nolock) on m.creditor = cr.creditor_id
	inner join banks b		with (nolock) on m.bank = b.bank
	where	b.type			= 'R'
	and	m.rpps_biller_id	= @biller_id

	-- Insert the creditor transaction
	if @creditor is not null
	begin
		declare	@date_created	datetime
		select	@date_created	= getdate()

		insert into registers_creditor (tran_type, creditor, credit_amt, message, date_created, item_date)
		select	'NA', @creditor, @amount, 'RPPS Fairshare Payment', @date_created, @date_created

		insert into registers_non_ar (tran_type, dst_ledger_account, creditor, credit_amt, date_created, message)
		select	'NA', @ledger_code, @creditor, @amount, @date_created, 'RPPS Fairshare Payment'

		-- Count this as contribution (fairshare) received
		update	creditors
		set	contrib_mtd_received = isnull(contrib_mtd_received,0) + @amount
		where	creditor	= @creditor

	end else

		update	rpps_response_details
		set	processing_error	= 'UNKNOWN BILLER ID'
		where	rpps_response_detail	= @rpps_response_detail;
end

-- Commit the transaction
commit transaction

-- Cleanup
return ( 1 )
GO
