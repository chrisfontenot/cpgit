USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_faq]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[MONTHLY_faq] AS

-- ====================================================================================================
-- ==            Update the statistics for the Frequently Asked Questions list                       ==
-- ====================================================================================================

-- Start the transaction
begin transaction
set nocount on

-- Roll the month to date counts into the year to date
update	faq_questions
set	count_ytd = count_ytd + count_mtd,
	count_mtd = 0

-- Roll the year to date items to the new year at the start of the year
if month(getdate()) = 1
begin
	update	faq_questions
	set	count_prior_year = count_ytd,
		count_ytd	 = 0
end

commit transaction
return ( 1 )
GO
