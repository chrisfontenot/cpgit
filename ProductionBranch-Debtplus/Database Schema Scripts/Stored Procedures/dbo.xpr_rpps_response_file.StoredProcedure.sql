USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_file]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_file] ( @label as varchar(50) = null, @filename as varchar(256) = null ) as
-- ============================================================================
-- ==            Create a new response file for RPPS processing              ==
-- ============================================================================

-- Create the response file for processing
insert into rpps_response_files (date_created, created_by, label, filename) values (getdate(), suser_sname(), @label, @filename)
return ( SCOPE_IDENTITY() )
GO
