SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_PrintChecks_Void] ( @bank as int, @Marker AS VarChar(50)) AS

-- ====================================================================================================
-- ==               Correct the ckecks in the indicated range to force a reprint                     ==
-- ====================================================================================================

-- ChangeLog
--   11/18/2002
--     Added bank account ID information

-- Do not produce temporary result sets
set nocount on

-- Create a new marker string to hold the list of checks for the destoyed sequence
declare	@destroyed_marker	VarChar(120)
SELECT	@destroyed_marker = convert(varchar(120), newid())

insert into registers_trust (tran_type,	client,	creditor,	cleared,	checknum,	amount,	date_created,	check_order,	reconciled_date,	created_by,	sequence_number,	bank)
select						 tran_type,	client,	creditor,	'D',		checknum,	amount,	date_created,	check_order,	getdate(),			created_by,	@destroyed_marker,	bank
from		registers_trust
where		sequence_number = @Marker
and			bank			= @bank
AND			ltrim(rtrim(isnull(cleared,''))) = ''

-- Mark the checks in destroyed section as "ready to print" again
update		registers_trust
set			cleared		= 'P',
			checknum	= NULL,
			sequence_number = NULL
where		sequence_number = @Marker
and			bank		= @bank
AND			ltrim(rtrim(isnull(cleared,''))) = ''

return ( 1 )
GO
