USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ClientPackage_SC_deposits]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_ClientPackage_SC_deposits] ( @Client AS INT ) AS
-- ===================================================================================
-- ==             Fetch the client deposit information                              ==
-- ===================================================================================

SELECT		case tran_type
			when 'CD' then 'Deposit'
			when 'CW' then 'Correction'
			when 'DP' then 'Deposit'
			when 'DM' then 'Transfer'
				  else 'Refund'
		end as 'type',

		case tran_type
			when 'VF' then 0 - d.debit_amt
			when 'CW' then 0 - d.debit_amt
			else d.credit_amt
		end as 'amount',

		d.date_created	as 'item_date'
FROM		registers_client d
WHERE		tran_type in ('DP', 'DM', 'RF', 'VF', 'VD', 'RR', 'RE', 'CD', 'CW')
AND		client = @Client
ORDER BY	d.date_created

RETURN ( @@rowcount )
GO
