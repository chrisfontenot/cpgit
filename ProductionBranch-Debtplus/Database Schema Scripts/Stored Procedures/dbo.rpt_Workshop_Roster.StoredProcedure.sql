USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Workshop_Roster]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Workshop_Roster] ( @Workshop as INT ) AS
-- ======================================================================================================
-- ==                   Retrieve a class roster for a workshop                                         ==
-- ======================================================================================================

-- ChangeLog
--   12/1/2002
--     Ignore cancelled appointments from the roster
--   4/28/2003
--     Added client referral information
--   12/21/2005
--     Added email address for primary applicant

SELECT		a.client							as 'client',
			dbo.format_reverse_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'client_name',
			a.workshop_people						as 'attending',
			c.active_status							as 'active_status',
			a.status							as 'attending_status',
			dbo.format_TelephoneNumber ( c.HomeTelephoneID )				AS 'home_ph',
			dbo.format_address_line_1(ad.house,ad.direction,ad.street,ad.suffix,ad.modifier,ad.modifier_value)	as 'addr_1',
			ad.address_line_2							as 'addr_2',
			dbo.format_city_state_zip (ad.city, ad.state, ad.postalcode)	as 'addr_3',
			isnull(r.description,'')					as 'client_referral',
			em.address								as 'email'

FROM		client_appointments a	with (nolock)
INNER JOIN	clients c		with (nolock) ON a.client = c.client
LEFT OUTER JOIN	people p		with (nolock) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join emailaddresses em with (nolock) on p.emailid = em.email
left outer join addresses ad with (nolock) on c.addressid = ad.address
left outer join referred_by r		with (nolock) on c.referred_by = r.referred_by
WHERE		a.workshop IS NOT NULL
AND			a.workshop = @Workshop
AND			a.status IN ('P', 'K')
ORDER BY	2, 1

RETURN ( @@rowcount )
GO
