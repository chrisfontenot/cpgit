USE [DEBTPLUS]
GO
/****** Object:  StoredProcedure [dbo].[xpr_get_dsa_case_liability_details]    Script Date: 12/5/2014 1:12:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[xpr_get_dsa_case_liability_details] (
	@clientid	int
)
as
begin

	declare @autoloanbalance			money
	declare @creditcardinstallmentloan	money
	declare @other1						money
	declare @other2						money
	declare @other3						money
	declare @other4						money

	select @autoloanbalance = current_value 
	  from secured_properties 
	 where client = @clientid 
	   and secured_type = 110

	select @autoloanbalance = sum(balance) 
      from secured_loans 
     where secured_property in (select secured_property 
	                              from secured_properties 
								 where client = @clientid and secured_type = 400)

	-- get the other balance of the client other than creditor debts
	declare @totalotherbalance  money
	select @totalotherbalance = ISNULL(sum(balance), 0)
	  from client_other_debts
     where client = @clientid

	select @creditcardinstallmentloan = sum(isnull(ccb.[orig_balance], 0) + isnull(ccb.[orig_balance_adjustment], 0) + isnull(ccb.[total_interest], 0) - isnull(ccb.[total_payments], 0)) + isnull(@totalotherbalance, 0)
	  from [dbo].[client_creditor] cc
inner join [dbo].[client_creditor_balances] ccb on cc.[client_creditor_balance] = ccb.[client_creditor_balance] 
	 where reassigned_debt = 0
	   and client = @clientid 
	   and ((ccb.[orig_balance] + ccb.[orig_balance_adjustment] + ccb.[total_interest] - ccb.[total_payments]) + @totalotherbalance) > 0 

	select @other1 = sum(balance) 
      from secured_loans 
     where secured_property in (select secured_property 
	                              from secured_properties 
								 where client = @clientid and secured_type in (410, 420))

	select @other2 = sum(balance) 
      from secured_loans 
     where secured_property in (select secured_property 
	                              from secured_properties 
								 where client = @clientid and secured_type = 600)

	select @other3 = sum(balance) 
      from secured_loans 
     where secured_property in (select secured_property 
	                              from secured_properties 
								 where client = @clientid and secured_type = 720)

	select @other4 = sum(balance) 
      from secured_loans 
     where secured_property in (select secured_property 
	                              from secured_properties 
								 where client = @clientid and secured_type in (740, 750))

	select @autoloanbalance as AutoLoanBalance
	     , @creditcardinstallmentloan as CreditCardInstallmentLoan
		 , @other1 as Other1
		 , @other2 as Other2
		 , @other3 as Other3
		 , @other4 as Other4

end