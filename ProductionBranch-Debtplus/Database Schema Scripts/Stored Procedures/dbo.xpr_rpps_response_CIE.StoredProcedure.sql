USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_CIE]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_CIE] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @name as varchar(5) = null, @account_number as varchar(22) = null, @net as money = 0, @gross as money = 0 ) as

-- =======================================================================================================================
-- ==            Generate the response for a CIE request                                                                ==
-- =======================================================================================================================

declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'CIE', @name, @account_number, @net

-- Update the record
if @rpps_response_detail > 0
begin
	update	rpps_response_details
	set	gross			= @gross
	where	rpps_response_detail	= @rpps_response_detail

	-- Change the type to the appropriate item if there is a match
	-- The problem is that it may not be a payment. It may be any type but because the error code does not tell us
	-- the type, we have to find it.

	if exists (select * from rpps_response_details where rpps_response_detail = @rpps_response_detail and service_class_or_purpose = 'CIE')
	begin
		declare	@service_class_or_purpose	varchar(80)
		declare	@rpps_transaction		int

		select	@rpps_transaction	= rpps_transaction
		from	rpps_response_details with (nolock)
		where	rpps_response_detail	= @rpps_response_detail

		if @rpps_transaction is not null
		begin
			select	@service_class_or_purpose	= service_class_or_purpose
			from	rpps_transactions
			where	rpps_transaction		= @rpps_transaction

			if @service_class_or_purpose is not null
			begin
				update	rpps_response_details
				set	service_class_or_purpose	= @service_class_or_purpose
				where	rpps_response_detail		= @rpps_response_detail
			end
		end
	end
end

return ( @rpps_response_detail )
GO
