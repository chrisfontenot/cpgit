USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rps_transmission_report]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rps_transmission_report] ( @FileID AS INT ) AS
-- ===========================================================================================
-- ==                Select the information needed for the RPS transmission report          ==
-- ===========================================================================================

-- ChangeLog
--   5/19/2004
--     Changed to only look at EFT file types (not proposals)
--     Used dbo.format_reverse_name rather than internal logic
--   06/09/2009
--      Remove references to debt_number and use client_creditor

IF @FileID IS NULL
	select	@fileID = max(rpps_file)
	from	rpps_files
	where	date_created = (
		select	max(date_created)
		from	rpps_files
		where	file_type = 'EFT'
	)
	and	file_type = 'EFT'

IF @FileID IS NULL
BEGIN
	RaisError (50041, 16, 1)
	return ( 0 )
END

-- Fetch the date of this file
DECLARE @FileDate  DateTime
SELECT  @FileDate = date_created
FROM	rpps_files with (nolock)
WHERE	rpps_file = @FileID

-- Generate the transaction listing
SELECT		r.rpps_transaction			as 'rpps_transaciton',
		d.creditor				as 'creditor',
		cr.creditor_name			as 'creditor_name',
		r.biller_id				as 'biller_id',
		r.trace_number_first			as 'item_key',
		d.client				as 'client',
		b.trace_number				as 'rpps_batch',

		case r.transaction_code
			when 22 then isnull(dbo.format_reverse_name ( default, pn.first, pn.middle, pn.last, default ),'')
			when 23 then 'PRENOTE: ' + isnull(dbo.format_reverse_name ( default, pn.first, pn.middle, pn.last, default ),'')
			when 27 then 'FAIRSHARE CONTRIBUTION'
		end					as 'client_name',

		case
			when r.transaction_code IN (23, 27) then 0
			else d.debit_amt
		end					as 'gross',

		case
			when r.transaction_code IN (23, 27) then 0
			when d.creditor_type = 'D' then d.debit_amt - d.fairshare_amt
			else d.debit_amt
		end					as 'net',

		isnull(d.account_number,cc.account_number) as 'account_number',
		@FileDate				as 'file_date'

FROM		rpps_transactions r	WITH (NOLOCK)
LEFT OUTER JOIN	people p 		WITH (NOLOCK) ON r.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.Nameid = pn.Name
LEFT OUTER JOIN	creditors cr		WITH (NOLOCK) ON r.creditor = cr.creditor
LEFT OUTER JOIN	rpps_batches b		WITH (NOLOCK) ON r.rpps_batch = b.rpps_batch
LEFT OUTER JOIN	registers_client_creditor d WITH (NOLOCK) ON r.client_creditor_register = d.client_creditor_register and d.tran_type = 'BW'
LEFT OUTER JOIN client_creditor cc	WITH (NOLOCK) ON d.client_creditor = cc.client_creditor
WHERE		b.rpps_file = @FileID
AND		r.service_class_or_purpose = 'CIE'

ORDER BY 5

RETURN ( @@rowcount )
GO
