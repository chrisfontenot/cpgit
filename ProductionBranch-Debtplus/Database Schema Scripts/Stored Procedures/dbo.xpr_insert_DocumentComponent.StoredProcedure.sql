USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_DocumentComponent]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_DocumentComponent]( @DocumentID as int, @SequenceID as int, @EffectiveStartDate as DateTime = null, @EffectiveExpireDate As DateTime = null, @PageEjectBeforeFLG as Bit = 0, @SectionID as Varchar(20) = 'DETAIL', @ComponentType as Varchar(20) = 'RTF', @ComponentLocationURI as Varchar(512) = null, @RevisionDate as DateTime = null ) as
	-- =====================================================================================================================
	-- ==         Insert a row into the DocumentComponents table                                                          ==
	-- =====================================================================================================================

	insert into DocumentComponents ([DocumentID], [SequenceID], [EffectiveStartDate], [EffectiveExpireDate], [PageEjectBeforeFLG], [SectionID], [ComponentType], [ComponentLocationURI], [RevisionDate])
	values (@DocumentID, @SequenceID, @EffectiveStartDate, @EffectiveExpireDate, @PageEjectBeforeFLG, @SectionID, @ComponentType, @ComponentLocationURI, @RevisionDate)

	return scope_identity()
GO
