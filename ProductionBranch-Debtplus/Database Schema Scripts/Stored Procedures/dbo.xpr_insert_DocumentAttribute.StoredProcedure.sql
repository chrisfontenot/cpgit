USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_DocumentAttribute]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[xpr_insert_DocumentAttribute] (@DocumentID AS INT = null, @LanguageID AS INT = null, @State AS INT = null, @AllLanguagesFLG AS BIT = 0, @AllStatesFLG AS BIT = 0) as
insert into DocumentAttributes([AllLanguagesFLG],[AllStatesFLG],[DocumentID],[LanguageID],[State]) values (@AllLanguagesFLG,@AllStatesFLG,@DocumentID,@LanguageID,@State)
return scope_identity()
GO
