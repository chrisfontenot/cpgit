USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_letter_types]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_letter_types] (@letter_code as varchar(10),@queue_name as varchar(256) = null,@description as varchar(50),@menu_name as varchar(256) = null,@letter_group as varchar(10) = null,@display_mode as int = -1,@region as int = null,@top_margin as float = 0.0,@left_margin as float = 0.0,@bottom_margin as float = 0.0,@right_margin as float = 0.0,@filename as varchar(256),@language as int = null,@default as bit = 0) as
	-- =================================================================================
	-- ==     Create an entry in the letter_types table for .NET                      ==
	-- =================================================================================
	
	insert into letter_types ([letter_code],[queue_name],[description],[menu_name],[letter_group],[display_mode],[region],[top_margin],[left_margin],[bottom_margin],[right_margin],[filename],[language],[default]) values (@letter_code,@queue_name,@description,@menu_name,@letter_group,@display_mode,@region,@top_margin,@left_margin,@bottom_margin,@right_margin,@filename,@language,@default)
	return ( scope_identity() )
GO
