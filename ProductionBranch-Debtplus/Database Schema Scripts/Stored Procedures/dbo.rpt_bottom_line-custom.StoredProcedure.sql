USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_bottom_line-custom]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_bottom_line-custom] ( @client as typ_client ) as

-- ==============================================================================================================
-- ==            retrieve the information for the "bottom line"                                                ==
-- ==============================================================================================================

-- Suppress intermediate result sets
set nocount on

declare @person_1_net	money
declare @person_2_net	money
declare @asset			money
declare @budget_date	datetime
declare @suggested		money
declare	@current		money
declare	@dmp			money

-- Obtain the net income from the employer
select	@person_1_net = net_income
from	people
where	client = @client
and		relation = 1

select	@person_2_net = sum(net_income)
from	people
where	client = @client
and		Relation  <> 1

-- Obtain the additional income amounts
select	@asset = sum(asset_amount)
from	assets
where	client = @client

-- Calculate the DMP program information
select	@dmp = sum(cc.disbursement_factor)
from	client_creditor cc
where	cc.client = @client
and		cc.reassigned_debt = 0

-- Find the budget id from the budgets table for this client
declare @budget_id int
select  @budget_id = dbo.map_client_to_budget ( @client )

if @budget_id is not null
begin
	-- Find the date the budget was created
	select	@budget_date = date_created
	from	budgets
	where	budget = @budget_id

	-- Calculate the expenses
	select	@suggested = sum(suggested_amount),
			@current   = sum(client_amount)
	from	budget_detail
	where	budget = @budget_id
end

-- Return the information
SELECT	@client					as 'client',
		isnull(@person_1_net,0)	as 'person_1_net',
		isnull(@person_2_net,0)	as 'person_2_net',
		isnull(@asset,0)		as 'asset',
		isnull(@current,0)		as 'current',
		isnull(@suggested,0)	as 'suggested',
		isnull(@dmp,0)			as 'dmp',

		isnull(@suggested,0)	as 'expense',
		@budget_date			as 'budget_date'

return ( 1 )
GO
