USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_x0001_disb_fact_equal_0]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_x0001_disb_fact_equal_0] as

declare	@Paf_creditor	varchar(10)
select	@Paf_creditor = paf_creditor
from	config with (nolock)

if @Paf_creditor is null
	select	@Paf_creditor = 'X0001'

select	c.client,
	dbo.format_reverse_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as client_name,
	rcc.date_created as disbursed_date,
	rcc.debit_amt	 as disbursed_amount

from	client_creditor cc with (nolock)
inner join clients c with (nolock) on cc.client = c.client
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
left outer join registers_client_creditor rcc with (nolock) on cc.last_payment = rcc.client_creditor_register
left outer join people p with (nolock) on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

where	c.active_status in ('A','AR')
and	cc.creditor = @paf_creditor
and	cc.reassigned_debt = 0
and	cc.disbursement_factor = 0

order by 2, 1

return ( @@rowcount )
GO
