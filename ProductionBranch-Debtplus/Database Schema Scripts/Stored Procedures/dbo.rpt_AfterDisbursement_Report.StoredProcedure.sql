USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AfterDisbursement_Report]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_AfterDisbursement_Report] ( @DisbursementBatch AS INT = NULL ) AS
-- ============================================================================================
-- ==           If a disbursement batch was not specified, use the latest one                ==
-- ============================================================================================

-- Suppress intermediate results
set nocount on

IF @DisbursementBatch IS NULL
BEGIN
	SELECT TOP 1	@DisbursementBatch = disbursement_register
	FROM		registers_disbursement
	ORDER BY	date_created DESC

	IF @DisbursementBatch IS NULL
	BEGIN
		RaisError (50033, 11, 1)
		RETURN ( 0 )
	END
END

-- ============================================================================================
-- ==           Determine the information from the disbursement table                        ==
-- ============================================================================================
declare	@disbursement_label	varchar(80)
select	@disbursement_label	= 'This report covers batch # ' + convert(varchar, disbursement_register) + ' created on ' + convert(varchar(10), date_created, 101)
from	registers_disbursement with (nolock)
where	disbursement_register	= @disbursementbatch

-- ============================================================================================
-- ==           Fetch the check information for this batch                                   ==
-- ============================================================================================

declare @sum_check_gross	money
declare @sum_check_billed	money
declare @sum_check_deducted	money
declare @sum_check_net		money

DECLARE @RPS_Gross		money
DECLARE @RPS_Net		money
DECLARE @RPS_Billed		money
DECLARE @RPS_deducted		money

--Since client 0 is part of the trust, the deduct creditor exclusions are no longer needed. It is disbursed along with the others.
--DECLARE	@Deduct_Creditor	varchar(10)
--SELECT	@Deduct_Creditor = deduct_creditor
--FROM	config

-- Examine the disbursement information. Group it by payment type and creditor type
SELECT		tran_type, creditor_type, sum(debit_amt) as debit_amt, sum(fairshare_amt) as fairshare_amt
INTO		#t_after_disbursement_disb
FROM		registers_client_creditor b WITH (NOLOCK)
WHERE		b.disbursement_register = @DisbursementBatch
AND		b.tran_type in ('AD', 'BW')
GROUP BY	tran_type, creditor_type

-- Check gross amount
select		@sum_check_gross	= sum(debit_amt)
FROM		#t_after_disbursement_disb
WHERE		tran_type = 'AD'

-- Check deduct amount
select		@sum_check_deducted	= fairshare_amt
FROM		#t_after_disbursement_disb
WHERE		tran_type = 'AD'
AND		creditor_type = 'D'

-- Check billed amount
select		@sum_check_billed	= fairshare_amt
FROM		#t_after_disbursement_disb
WHERE		tran_type = 'AD'
AND		creditor_type = 'B'

-- RPS gross amount
select		@RPS_gross	= sum(debit_amt)
FROM		#t_after_disbursement_disb
WHERE		tran_type = 'BW'

-- RPS deduct amount
select		@RPS_deducted	= fairshare_amt
FROM		#t_after_disbursement_disb
WHERE		tran_type = 'BW'
AND		creditor_type = 'D'

-- RPS billed amount
select		@RPS_billed	= fairshare_amt
FROM		#t_after_disbursement_disb
WHERE		tran_type = 'BW'
AND		creditor_type = 'B'

drop table #t_after_disbursement_disb

-- Compute the sum amounts for the desired parameters
SELECT  @sum_check_net	= isnull(@sum_check_gross,0)    - isnull(@sum_check_deducted,0),
	@RPS_Net	= isnull(@RPS_gross,0)		- isnull(@RPS_deducted,0)

DECLARE	@Prior_balance		money
DECLARE	@Post_Balance		money
DECLARE	@Deduct_balance		money

SELECT	@prior_balance	= isnull(prior_balance,0),
	@post_balance	= isnull(post_balance,0),
	@deduct_balance	= isnull(deduct_balance,0)
FROM	registers_disbursement
WHERE	disbursement_register = @DisbursementBatch

-- ===========================================================================================
-- ==          Calculate the amount left in the transaction table                           ==
-- ===========================================================================================
DECLARE @RPS_Unallocated money

SELECT	@RPS_Unallocated = SUM(d.debit_amt)
FROM	rpps_transactions t
INNER JOIN registers_client_creditor d ON t.client_creditor_register = d.client_creditor_register
WHERE	t.trace_number_first IS NULL
AND	t.service_class_or_purpose = 'CIE'
AND	t.transaction_code IN (22, 23)

-- ============================================================================================
-- ==            Fetch the trust balance                                                     ==
-- ============================================================================================

-- Fetch the sum of the clients who have a posative balance in their account
DECLARE @posative_trust_balance	money
DECLARE @negative_trust_balance	money
DECLARE @inactive_trust_balance	money

select	case [group] when 4 then 1 when 5 then 3 when 1 then 3 else 2 end as [group],
	sum(held_in_trust) as held_in_trust
into	#t_after_disbursement_trust
from	view_held_in_trust
group by [group];

select	@negative_trust_balance = sum(held_in_trust)
from	#t_after_disbursement_trust
where	[group] = 1;

select	@posative_trust_balance = sum(held_in_trust)
from	#t_after_disbursement_trust
where	[group] = 2;

select	@inactive_trust_balance = sum(held_in_trust)
from	#t_after_disbursement_trust
where	[group] = 3;

drop table #t_after_disbursement_trust;

DECLARE @total_trust_balance	money
SELECT	@total_trust_balance = isnull(@posative_trust_balance,0) + isnull(@negative_trust_balance,0) + isnull(@inactive_trust_balance,0)

-- ============================================================================================
-- ==            Return the proper recordset                                                 ==
-- ============================================================================================

SELECT	isnull(@sum_check_gross,0)		as 'total_gross_checks',
	isnull(@sum_check_billed,0)		as 'total_billed_checks',
	isnull(@sum_check_deducted,0)		as 'total_deduct_checks',
	isnull(@sum_check_net,0)		as 'total_net_checks',
	isnull(@RPS_Gross,0)			as 'total_gross_rps',
	isnull(@RPS_Billed,0)			as 'total_billed_rps',
	isnull(@RPS_Net,0)			as 'total_net_rps',
	isnull(@posative_trust_balance,0)	as 'posative_trust_balance',
	isnull(@negative_trust_balance,0)	as 'negative_trust_balance',
	isnull(@inactive_trust_balance,0)	as 'inactive_trust_balance',
	isnull(@total_trust_balance,0)		as 'total_trust_balance',
	isnull(@RPS_Unallocated,0)		as 'total_unallocated_rps',
	isnull(@prior_balance,0)		as 'prior_balance',
	isnull(@post_balance,0)			as 'post_balance',
	isnull(@deduct_balance,0)		as 'deduct_balance',
	isnull(@disbursement_label, '')		as 'disbursement_label'

RETURN ( 0 )
GO
