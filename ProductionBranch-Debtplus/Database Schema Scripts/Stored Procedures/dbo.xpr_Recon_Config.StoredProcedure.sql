USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_Recon_Config]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_Recon_Config] AS

-- ======================================================================================================================
-- ==            Obtain the information for the configuration of the check reconfiguration                             ==
-- ======================================================================================================================

/*
SELECT	cleared_trust_account_balance	as 'trust_balance'
FROM	config

RETURN ( @@rowcount )
*/

-- This is no longer used
select	convert(money,0)		as 'trust_balance'
return ( 1 )
GO
