USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nccrc_annual]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nccrc_annual] ( @period_start as datetime, @period_end as datetime ) as

-- Find the types of counseling sessions
select	ca.client_appointment, ca.client, a.housing, a.initial_appt, ca.result, ca.status
into	#appts
from	client_appointments ca with (nolock)
left outer join appt_types a on ca.appt_type = a.appt_type
where	start_time between @period_start and @period_end
and	office is not null
and	status in ('K','W')

-- Build a list of the clients to be processed. We don't care about anyone else
select distinct client
into #client_list
from #appts

-- Create the result table
create table #results (sheet varchar(80), location varchar(80), value varchar(80));

-- Return the results for the appointment types
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B17' as location,
count(*) from #appts where status <> 'R' and initial_appt = 1 and housing = 0

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'C17' as location,
count(*) from #appts where status <> 'R' and initial_appt = 0 and housing = 0

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'D17' as location,
count(*) from #appts where status <> 'R' and initial_appt = 1 and housing = 1

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'E17' as location,
count(*) from #appts where status <> 'R' and initial_appt = 0 and housing = 1

-- Examine the results for the appointments
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B20' as location,
count(*) from #appts where status <> 'R' and result = 'DMP'

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B21' as location,
count(*) from #appts where status <> 'R' and result = 'RLA'

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B22' as location,
count(*) from #appts where status <> 'R' and result = 'ROA'

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B23' as location,
count(*) from #appts where status <> 'R' and result = 'CCH'

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B24' as location,
count(*) from #appts where status <> 'R' and isnull(result,'XXX') not in ('DMP','RLA', 'ROA', 'CCH')

-- Find the types of counseling sessions
select	c.client, convert(money, isnull(sum(p.gross_income),0)) as gross, convert(money,0) as assets, convert(money,0) as other_debt, convert(money,0) as secured_debt, convert(money,0) as unsecured_debt, convert(float,0) as ratio, f.nfcc as cause
into	#clients
from	clients c
inner join #client_list x on c.client = x.client
left outer join people p on c.client = p.client
left outer join financial_problems f on c.cause_fin_problem1 = f.financial_problem
group by c.client, f.nfcc;

select  a.client, sum(a.asset_amount) as asset_amount
into	#assets
from	assets a with (nolock)
inner join #clients c on a.client = c.client
group by a.client;

update	#clients
set	assets = a.asset_amount
from	#clients c
inner join #assets a on c.client = a.client

select  cc.client, sum(b.orig_balance + b.orig_balance_adjustment) as unsecured_debt
into	#unsecured_debt
from	client_creditor cc
inner join client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
inner join creditors cr on cc.creditor = cr.creditor
inner join creditor_classes ccl on cr.creditor_class = ccl.creditor_class
inner join #clients c on cc.client = c.client
where	isnull(ccl.agency_account,0) = 0
group by cc.client;

select  l.client, sum(l.balance) as secured_debt
into	#secured_debt
from	auto_loans l
inner join #clients c on l.client = c.client
group by l.client;

select  o.client, sum(o.balance) as other_debt
into	#other_debt
from	client_other_debts o
inner join #clients c on o.client = c.client
group by o.client;

update	#clients
set	unsecured_debt = a.unsecured_debt
from	#clients c
inner join #unsecured_debt a on c.client = a.client

update	#clients
set	secured_debt = a.secured_debt
from	#clients c
inner join #secured_debt a on c.client = a.client

update	#clients
set	other_debt = a.other_debt
from	#clients c
inner join #other_debt a on c.client = a.client

-- Return the results for the appointment types
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B30' as location,
count(*) from #clients where ((gross + assets) * 12.0) < 25000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B31' as location,
count(*) from #clients where ((gross + assets) * 12.0) between 25000.01 and 50000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B32' as location,
count(*) from #clients where ((gross + assets) * 12.0) between 50000.01 and 75000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B33' as location,
count(*) from #clients where ((gross + assets) * 12.0) between 75000.01 and 100000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B34' as location,
count(*) from #clients where ((gross + assets) * 12.0) > 100000.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B38' as location,
count(*) from #clients where cause = 'OVS';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B39' as location,
count(*) from #clients where cause in ('RIC','UNE');

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B40' as location,
count(*) from #clients where cause = 'MED';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B41' as location,
count(*) from #clients where cause = 'DEA';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B42' as location,
count(*) from #clients where cause = 'DIV';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B43' as location,
count(*) from #clients where cause = 'SUB';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B44' as location,
count(*) from #clients where cause = 'GAM';

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B45' as location,
count(*) from #clients where isnull(cause,'XXX') not in ('GAM','SUB','DIV','DEA','MED','RIC','OVS','UNE');

-- Find the income to debt ratios
update #clients
set ratio = (unsecured_debt + secured_debt + other_debt) / ((gross + assets) * 12.0) where (gross+assets) > 0.0;

-- Determine the relative counts
insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B48' as location,
count(*) from #clients where ratio <= 0.50;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B49' as location,
count(*) from #clients where ratio > 0.50 and ratio <= 1.00;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B50' as location,
count(*) from #clients where ratio > 1.00 and ratio <= 1.50;

insert into #results (sheet, location, value)
select 'Sheet1' as sheet, 'B51' as location,
count(*) from #clients where ratio > 1.50;

-- Find the number of clients at the end of the year
declare	@ending_clients	int
select	@ending_clients = count(*)
from	clients with (nolock)
where	(active_status in ('A','AR') and active_status_date <= @period_end)
or	(active_status = 'I' and drop_date >= @period_start)

-- Find the number of clients at the start of 2004
declare	@starting_clients	int
select	@starting_clients = count(*)
from	clients with (nolock)
where	(active_status in ('A','AR') and active_status_date <= @period_start)

-- Try the statistics table for the information
declare	@clients_a	int
select	@clients_a	= clients_a
from	[statistics] with (nolock)
where	period_stop	= @period_start;

if @clients_a is not null
	select	@starting_clients	= @clients_a

select	@clients_a	= clients_a
from	[statistics] with (nolock)
where	period_stop	= dateadd(s, 1, @period_end);

if @clients_a is not null
	select	@ending_clients	= @clients_a

-- Find the disbursements for last year
declare	@starting_disbursements	money
select	@starting_disbursements = sum(isnull(debit_amt,0) - isnull(credit_amt,0))
from	registers_client_creditor with (nolock)
where	date_created between dateadd(yy, -1, @period_start) and @period_start
and	tran_type in ('AD','BW','MD','CM','RR','VD','RF')

-- Find the disbursements for this year
declare	@ending_disbursements	money
select	@ending_disbursements = sum(isnull(debit_amt,0) - isnull(credit_amt,0))
from	registers_client_creditor with (nolock)
where	date_created between @period_start and @period_end
and	tran_type in ('AD','BW','MD','CM','RR','VD','RF')

declare	@increase_percent	float

if isnull(@ending_disbursements,0) = 0 or isnull(@starting_disbursements,0) = 0
	select	@increase_percent = 0.0
else
	if @ending_disbursements = @starting_disbursements
		select	@increase_percent = 0.0
	else
		if @ending_disbursements > @starting_disbursements
			select	@increase_percent = ( convert(float,@ending_disbursements - @starting_disbursements) / convert(float,@starting_disbursements) ) * 100.0
		else
			select	@increase_percent = 0.0 - ( ( convert(float,@starting_disbursements - @ending_disbursements) / convert(float,@ending_disbursements) ) * 100.0)

-- Find the average number of creditors on the plan
select	c.client, convert(float,count(cc.client_creditor)) as cnt
into	#creditor_count
from	client_creditor cc		with (nolock)
inner join client_creditor_balances b	with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
inner join #client_list c		with (nolock) on cc.client = c.client
left outer join creditors cr		with (nolock) on cc.creditor = cr.creditor
left outer join creditor_classes ccl	with (nolock) on cr.creditor_class = ccl.creditor_class
where	cc.reassigned_debt = 0
and	isnull(ccl.agency_account,0) = 0
group by c.client;

declare	@avg_creditors	float
select	@avg_creditors = avg(cnt)
from	#creditor_count

drop table #creditor_count

-- Return the results for the appointment types
insert into #results (sheet, location, value) values ('Sheet1', 'B56', @starting_clients)
insert into #results (sheet, location, value) values ('Sheet1', 'B57', @ending_clients)
insert into #results (sheet, location, value) values ('Sheet1', 'B59', @avg_creditors)

if @increase_percent = 0
	insert into #results (sheet, location, value) values ('Sheet1', 'B64', @increase_percent)
else
	if @increase_percent > 0
		insert into #results (sheet, location, value) values ('Sheet1', 'B62', @increase_percent)
	else
		insert into #results (sheet, location, value) select 'Sheet1', 'B65', 0.0 - @increase_percent

select * from #results;

-- Discard the temporary tables
drop table #results;
drop table #clients;
drop table #unsecured_debt;
drop table #other_debt;
drop table #secured_debt;
drop table #appts
drop table #client_list

return ( 1 )
GO
