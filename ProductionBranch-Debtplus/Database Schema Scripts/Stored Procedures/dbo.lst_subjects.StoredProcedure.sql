USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_subjects]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_subjects] AS

select	subject					as 'item_key',
	description				as 'description'
from	subjects with (nolock)

return ( @@rowcount )
GO
