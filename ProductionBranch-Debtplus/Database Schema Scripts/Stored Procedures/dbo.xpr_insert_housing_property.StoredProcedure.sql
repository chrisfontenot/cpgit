USE [Debtplus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_property]    Script Date: 09/17/2014 11:12:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[xpr_insert_housing_property] 
(@HousingID AS INT = null
, @UseHomeAddress AS BIT = 0
, @PropertyAddress AS INT = null
, @PreviousAddress AS INT = null
, @TrustName AS VARCHAR(256) = ''
, @OwnerID AS INT = null
, @CoOwnerID AS INT = null
, @Residency AS INT = null
, @Occupancy AS INT = null
, @PropertyType AS INT = null
, @PurchaseYear AS INT = null
, @PurchasePrice AS MONEY = 0
, @LandValue AS MONEY = 0
, @ImprovementsValue AS MONEY = 0
, @PlanToKeepHomeCD AS INT = NULL
, @ForSaleIND AS BIT = 0
, @SalePrice AS MONEY = 0
, @RealityCompany AS VARCHAR(50) = ''
, @sales_contract_date AS DATETIME = null
, @FcNoticeReceivedIND AS BIT = 0
, @FcSaleDT AS DATETIME = null
, @UseInReports AS BIT = 0
, @HPF_interviewID AS INT = null
, @HPF_foreclosureCaseID AS INT = null
, @HPF_actionItemNoteID AS INT = null
, @HPF_loanDefaultReasonNoteID AS INT = null
, @HPF_followupNoteID AS INT = null
, @property_tax AS INT = null
, @annual_property_tax AS MONEY = 0
, @tax_delinq_state AS INT = null
, @tax_due_date AS DATETIME = null
, @pmi_insurance AS MONEY = 0
, @annual_ins_amount AS MONEY = 0
, @ins_delinq_state AS INT = null
, @ins_due_date AS DATETIME = null)

 as
begin
	-- ==========================================================================================================
	-- ==            Create a new row into the housing_properties table                                        ==
	-- ==========================================================================================================
	set nocount on

	insert into housing_properties ([annual_ins_amount],[annual_property_tax],[CoOwnerID],[FcNoticeReceivedIND],[FcSaleDT],[ForSaleIND],[HousingID],[HPF_actionItemNoteID],[HPF_followupNoteID],[HPF_foreclosureCaseID],[HPF_interviewID],[HPF_loanDefaultReasonNoteID],[ImprovementsValue],[ins_delinq_state],[ins_due_date],[LandValue],[Occupancy],[OwnerID],[PlanToKeepHomeCD],[pmi_insurance],[PreviousAddress],[property_tax],[PropertyAddress],[PropertyType],[PurchasePrice],[PurchaseYear],[RealityCompany],[Residency],[SalePrice],[sales_contract_date],[tax_delinq_state],[tax_due_date],[TrustName],[UseHomeAddress],[UseInReports]) 
		values (@annual_ins_amount,@annual_property_tax,@CoOwnerID,@FcNoticeReceivedIND,@FcSaleDT,@ForSaleIND,@HousingID,@HPF_actionItemNoteID,@HPF_followupNoteID,@HPF_foreclosureCaseID,@HPF_interviewID,@HPF_loanDefaultReasonNoteID,@ImprovementsValue,@ins_delinq_state,@ins_due_date,@LandValue,@Occupancy,@OwnerID,@PlanToKeepHomeCD,@pmi_insurance,@PreviousAddress,@property_tax,@PropertyAddress,@PropertyType,@PurchasePrice,@PurchaseYear,@RealityCompany,@Residency,@SalePrice,@sales_contract_date,@tax_delinq_state,@tax_due_date,@TrustName,@UseHomeAddress,@UseInReports)
	return (scope_identity())
end
