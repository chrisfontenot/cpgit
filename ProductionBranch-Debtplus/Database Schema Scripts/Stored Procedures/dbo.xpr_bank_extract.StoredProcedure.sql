SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_bank_extract] ( @bank as int ) AS

-- =======================================================================================================
-- ==            Generate the extract list for the bank reconcilation file to be sent to the bank       ==
-- =======================================================================================================

-- ChangeLog
--   2/27/2004
--     Returned reconciled date for voided checks rather than check date

-- Suppress intermediate results
set nocount on

-- Determine the bank account number
declare	@account_number	varchar(80)
select	@account_number	= replace(replace(account_number,'-',''),' ','')
from	banks with (nolock)
where	bank	= @bank

-- Build the list of the trust register pointers that will be extracted
create table #extract (
	trust_register	int,
	checknum	BigInt,
	cleared		varchar(2)
);

create unique clustered index ix1_temp_extract on #extract ( trust_register );
create index ix2_temp_extract on #extract ( checknum, cleared );

insert into #extract (trust_register, checknum, cleared)
select	trust_register, checknum, cleared
from	registers_trust
where	checknum is not null
and	tran_type in ('AD', 'MD', 'CM', 'CR', 'AR')
and	bank_xmit_date IS NULL
and	isnull(cleared,' ') in (' ','V', 'D')
and	bank = @bank

-- Delete the items which are duplicated. Remove the "D" items where the check number matches one which is
-- not destroyed.
select	a.trust_register
into	#dups
from	#extract a
inner join #extract b on a.checknum = b.checknum and b.cleared <> 'D'
where	a.cleared = 'D';

delete
from	#extract
where	trust_register in (
	select	trust_register
	from	#dups
);

-- Mark the duplicated items as having been sent on 1/1/1980. This is just a garbage value to prevent
-- the item from being selected again.
update	registers_trust
set	bank_xmit_date = convert(datetime, '1/1/1980')
from	registers_trust t
inner join #dups x ON t.trust_register = x.trust_register;

-- Discard the duplicates table (and indicies)
drop table #dups

-- Update the transmit date to now
update	registers_trust
set	bank_xmit_date = getdate()
from	registers_trust t
inner join #extract x ON t.trust_register = x.trust_register;

-- Extract the information for the program
SELECT	isnull(@account_number,'')							as account_number,

	case
		when t.creditor is not null then t.creditor
		when t.client is not null then dbo.format_client_id( t.client )
		else ''
	end										as payee,

	case isnull(t.cleared,' ')
		when ' ' then 'I'
		when 'V' then '2'
		when 'D' then '2'
		else ' '
	end										as cleared,

	t.checknum							as checknum,
	isnull(t.amount,0)								as amount,

	case
		when isnull(t.cleared,' ') in ('D','V') and t.reconciled_date is not null then convert(datetime, convert(varchar(10), t.reconciled_date, 101) + ' 00:00:00')
		else convert(datetime, convert(varchar(10), t.date_created, 101) + ' 00:00:00')
	end										as date_created

FROM	registers_trust t
inner join #extract x on t.trust_register = x.trust_register;

-- Discard the working table
drop table #extract

return ( @@rowcount )
GO
