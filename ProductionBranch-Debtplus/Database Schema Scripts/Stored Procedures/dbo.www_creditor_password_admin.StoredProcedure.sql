USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_password_admin]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_password_admin] ( @creditor as typ_creditor, @new_password as varchar(32) = NULL ) AS
-- =========================================================================================================
-- ==            Update the password for the creditor                                                     ==
-- =========================================================================================================

-- Update the database value
update	creditor_www
set	[password]	= @new_password
where	creditor	= @creditor

return ( @@rowcount )
GO
