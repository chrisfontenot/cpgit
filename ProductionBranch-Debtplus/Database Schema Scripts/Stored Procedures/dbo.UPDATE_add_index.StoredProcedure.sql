USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_add_index]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_add_index] ( @IndexName as varchar(80), @TableName as varchar(80), @Columns as varchar(800), @Includes as varchar(800) = null, @IsUnique AS bit = 0) as
-- ===========================================================================================
-- ==            Create indexes on existing tables                                          ==
-- ===========================================================================================

declare	@stmt	varchar(800)
set nocount on

-- Discard the existing index
if exists (
SELECT t.[name],i.[name] as IndexName FROM SYS.INDEXES AS i WITH (NOLOCK)
INNER JOIN SYS.TABLES AS t WITH (NOLOCK) ON i.[object_id] = t.[object_id]
INNER JOIN SYS.INDEX_COLUMNS AS ic WITH (NOLOCK) ON i.[object_id] = ic.[object_id] AND i.index_id = ic.index_id
WHERE t.[type] = 'U' AND t.Is_MS_Shipped = 0 AND i.Is_Hypothetical = 0
AND i.name = @IndexName
AND t.name = @TableName
) BEGIN
	select	@stmt = 'DROP INDEX [' + @IndexName + '] ON [' + @TableName + ']'
	print  @stmt
	exec ( @stmt )
END

-- Create the index	
select	@stmt	= 'CREATE '
if @IsUnique = 1
	select  @stmt   = @stmt + 'UNIQUE '
select  @stmt   = @stmt + 'NONCLUSTERED INDEX [' + @IndexName + '] ON dbo.[' + @TableName + '] (' + @Columns

if @Includes IS NOT NULL AND convert(varchar, SERVERPROPERTY('productversion')) not like '8.%'
	select @stmt = @stmt + ') INCLUDE (' + @Includes
select @stmt = @stmt + ') ON [PRIMARY]'

print  @stmt
exec ( @stmt )

return ( 0 )
GO
