USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_retention_client_events]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_retention_client_events] ( @selection_string varchar(800) = '' ) as

declare	@stmt	varchar(8000)
select	@stmt = 'SELECT * FROM view_retention_client_events vc ' + isnull(@selection_string,'') + ' ORDER BY vc.client, vc.date_created DESC'

exec ( @stmt )
return ( @@rowcount )
GO
