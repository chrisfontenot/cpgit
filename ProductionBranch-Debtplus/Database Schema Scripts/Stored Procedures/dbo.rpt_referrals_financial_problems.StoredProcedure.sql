USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_referrals_financial_problems]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_referrals_financial_problems] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL, @office as int = null ) AS
-- ==========================================================================================
-- ==           Return the information to generate the referral report                     ==
-- ==========================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
declare	@stmt	varchar(8000)
select	@stmt = '
SELECT		coalesce(prb.description,convert(varchar,c.cause_fin_problem1),''Unspecified'') as ''financial_problem'',
		COUNT(*) AS ''items''
FROM		clients c WITH (NOLOCK)
LEFT OUTER JOIN financial_problems prb WITH (NOLOCK) ON c.cause_fin_problem1 = prb.financial_problem '

select	@stmt = @stmt + ' WHERE c.date_created >= ''' + convert(varchar(10), @FromDate, 101) + ' 00:00:00'''
select	@stmt = @stmt + ' AND   c.date_created <= ''' + convert(varchar(10), @ToDate,   101) + ' 23:59:59'''

if isnull(@Office,0) > 0
	select	@stmt = @stmt + ' AND c.office = ' + convert(varchar, @office)

select @stmt = @stmt + '
	group by	prb.description, c.cause_fin_problem1
	order by	prb.description;'

exec ( @stmt )
return ( 0 )
GO
