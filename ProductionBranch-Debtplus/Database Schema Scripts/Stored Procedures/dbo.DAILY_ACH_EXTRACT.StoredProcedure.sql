USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_ACH_EXTRACT]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_ACH_EXTRACT] ( @PrenoteDate as datetime = null ) AS
-- Special procedure for Richmond to extract ACH prenotes for their bank.

-- Update the clients to remove any prenote date that falls in the date range specified
if @prenotedate is null
	select	@prenotedate = getdate();
select	@PrenoteDate = convert(varchar(10), @Prenotedate, 101)

update  client_ach
set		prenotedate = null
where	prenotedate >= @PrenoteDate and prenotedate < dateadd(d, 1, @PrenoteDate)

declare	@program	varchar(800)
select	@program	= 'C:\PROGRA~1\DEBTPLUS\ACHEXT~1\DEBTPL~1.EXE C:\PROGRA~1\DEBTPLUS\ACHEXT~1\Clearpoint.ACH.Extract.config'  -- .NET version

-- Find the output directory
declare	@output_dir	varchar(800)
select	@output_dir	= 'D:\ONLY_T~1\shared\ACHPRE~1' -- d:\only_this_gets_backed_up\shared\ACHPrenotes

-- Create the output file name
declare	@output_fname	varchar(800)
select	@output_fname = right('0000' + convert(varchar, datepart(year, @PrenoteDate)), 4) + '' + 
			right('00'   + convert(varchar, datepart(month, @PrenoteDate)), 2) + '' + 
			right('00'   + convert(varchar, datepart(day, @PrenoteDate)), 2) + '.txt'

select	@output_dir = @output_dir + '\' + @output_fname

-- The first parameter is the output file.
select	@program	= @program + ' ' + @output_dir

-- Execute the command
execute master..xp_cmdshell @program, no_output

-- Terminate normally
return ( 0 )
GO
