USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AppointmentActivity_ByType]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_AppointmentActivity_ByType] ( @FromDate AS DateTime = NULL, @office as int = null ) AS
-- ===========================================================================================
-- ==           Information for the appointment activity report                             ==
-- ===========================================================================================

-- This is the cutoff date
DECLARE @CutoffDate DATETIME

-- Default the date to today if it is not supplied
IF @FromDate IS NULL
	SELECT @FromDate = getdate()

-- Remove the time from the date value
SELECT	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')

-- Split the input date into the appropriate items
DECLARE @Month as INT
DECLARE @Year  as INT
DECLARE @Day   as INT
SELECT	@Month = datepart ( month, @FromDate ),
	@Day = datepart ( day, @FromDate ),
	@Year = datepart ( year, @FromDate )

-- Set the limits for the month operation
DECLARE @Lower_Month    DATETIME
SELECT  @Lower_Month  = CONVERT ( DATETIME, convert(varchar,@Month) + '/01/' + convert(varchar,@Year) )

-- Set the limits for the year operation
DECLARE @Lower_Year     DATETIME
SELECT  @Lower_Year   = CONVERT ( DATETIME, '01/01/' + convert(varchar,@Year) )

-- Set the limits for the quarter
DECLARE @Lower_Quarter  DATETIME
SELECT	@Month = (((@Month - 1) / 3) * 3) + 1
SELECT  @Lower_Quarter = CONVERT ( DATETIME, convert(varchar, @Month ) + '/01/' + convert(varchar,@Year) )

-- Define the cutoff at the end of the desired month
SELECT	@CutoffDate	= dateadd(s, -1, dateadd(m, 1, @Lower_Month))
IF @CutoffDate > getdate()
	SELECT	@CutoffDate	= getdate()

-- Disable the counts as a resulting recordset
SET NOCOUNT ON

-- Create a temporary table to hold the results so that it may be summarized
CREATE TABLE #t_Appointments(
                RecType Int,
                Description VarChar(50),
                MTDCount Int,
                QTDCount Int,
                YTDCount Int)

-- Populate the table with the labels but no counts
INSERT INTO #t_Appointments (RecType, Description, MTDCount, QTDCount, YTDCount)
SELECT
	appt_type,
	appt_name,
	0 as 'MTDCount',
	0 as 'QTDCount',
	0 as 'YTDCount'
FROM
	appt_types

-- Calculate the appointments by the month to date
INSERT INTO #t_Appointments (RecType, Description, MTDCount, QTDCount, YTDCount)
SELECT		ca.appt_type,
		t.appt_name,
		COUNT(ca.appt_type) AS 'MTDCount',
		0 as 'QTDCount',
		0 as 'YTDCount'
FROM		client_appointments ca
LEFT OUTER JOIN	appt_types t ON ca.appt_type = t.appt_type
WHERE		ca.start_time BETWEEN @Lower_Month AND @CutoffDate
AND		ca.workshop IS NULL
AND		((@office = -1) or (ca.office = @office))
GROUP BY	ca.appt_type, t.appt_name

-- Calculate the appointments by the quarter to date
INSERT INTO #t_Appointments (RecType, Description, MTDCount, QTDCount, YTDCount)
SELECT		ca.appt_type,
		t.appt_name,
		0 as 'MTDCount',
		COUNT(ca.appt_type) AS 'QTDCount',
		0 as 'YTDCount'
FROM		client_appointments ca
LEFT OUTER JOIN	appt_types t ON ca.appt_type=t.appt_type
WHERE		ca.start_time BETWEEN @Lower_Quarter AND @CutoffDate
AND		ca.workshop IS NULL
AND		((@office = -1) or (ca.office = @office))
GROUP BY	ca.appt_type, t.appt_name

-- Calculate the appointments by the year to date
INSERT INTO #t_Appointments (RecType, Description, MTDCount, QTDCount, YTDCount)
SELECT		ca.appt_type,
		t.appt_name,
		0 as 'MTDCount',
		0 as 'QTDCount',
		COUNT(ca.appt_type) AS 'YTDCount'
FROM		client_appointments ca
LEFT OUTER JOIN	appt_types t ON ca.appt_type=t.appt_type
WHERE		ca.start_time BETWEEN @Lower_Year AND @CutoffDate
AND		ca.workshop IS NULL
AND		((@office = -1) or (ca.office = @office))
GROUP BY	ca.appt_type, t.appt_name

-- Disable the counts as a resulting recordset
SET NOCOUNT OFF

-- Return the resulting recordset which is the sum of the temporary table
SELECT		isnull(RecType,0) as RecType,
		Description,
		SUM(MTDCount) as 'MTDCount',
		SUM(QTDCount) as 'QTDCount',
		SUM(YTDCount) as 'YTDCount'
FROM		#t_Appointments
GROUP BY	RecType, Description
ORDER BY	RecType, Description

-- Drop the working table
DROP TABLE #t_Appointments
GO
