USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_statement_notes]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_statement_notes] ( @client as int, @client_statement_batch as int ) as

-- Return the notes for the client statement
select	note
from	client_statement_batches with (nolock)
where	note is not null
and	client_statement_batch	= @client_statement_batch

union all

select	note
from	client_statement_notes with (nolock)
where	client			= @client
and	client_statement_batch	= @client_statement_batch
and	note is not null
GO
