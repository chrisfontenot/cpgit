USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_job_descriptions]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_job_descriptions] ( @description as varchar(50), @nfcc as varchar(4) = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the job descriptions table               ==
-- ========================================================================================
	insert into job_descriptions ( [description], [nfcc] ) values ( @description, @nfcc )
	return ( scope_identity() )
GO
