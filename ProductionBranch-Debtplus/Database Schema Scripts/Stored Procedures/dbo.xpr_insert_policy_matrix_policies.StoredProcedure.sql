USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_policy_matrix_policies]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_policy_matrix_policies] ( @creditor as Varchar(10), @policy_matrix_policy_type as Varchar(50), @item_value as VarChar(1024), @message as varchar(1024), @use_sic as integer = 0 ) AS

-- Suppress intermediate results
set nocount on

-- Start a transaction for the work
begin transaction

-- Correct the parameters
select	@creditor					= ltrim(rtrim(isnull(@creditor,''))),
		@policy_matrix_policy_type	= ltrim(rtrim(isnull(@policy_matrix_policy_type,''))),
		@item_value					= ltrim(rtrim(isnull(@item_value,''))),
		@message					= ltrim(rtrim(isnull(@message,'')))

if @item_value = ''
	select	@item_value = null

if @message = ''
	select	@message = null

-- If we are doing multiple creditors then define the creditor list
create table #updates (creditor varchar(10), PolicyMatrixPolicy int null);
create index ix1_temp_updates on #updates ( PolicyMatrixPolicy );

declare	@sic	varchar(50)

-- Determine if the creditors of this sic code are being updated.
if @use_sic <> 0
begin
	select	@sic = ltrim(rtrim(sic))
	from	creditors with (nolock)
	where	creditor = @creditor;

end else
	select	@sic = ''

if @sic = ''
	select	@sic = null

-- If doing multiple creditors then find the creditors to be changed
if (@sic is not null) and (@use_sic <> 0)
begin
	insert into #updates (creditor)
	select	creditor
	from	creditors with (nolock)
	where	sic = @sic
	and	sic is not null

	union

	select	@creditor;

end else begin

	-- The single creditor is being updated.
	insert into #updates (creditor) values (@creditor)
end

-- Find the existing records for the policy information
update		#updates
set			PolicyMatrixPolicy = p.policy_matrix_policy
from		#updates u
inner join	policy_matrix_policies p on u.creditor = p.creditor and @policy_matrix_policy_type = p.policy_matrix_policy_type;

-- Write them to the creditor notes when they are changed
insert into creditor_notes (creditor, type, is_text, dont_delete, dont_edit, dont_print, subject, note)
select		u.creditor, 3, 1, 1, 1, 0, 'Changed policy ' + @policy_matrix_policy_type, 'Change of the policy information' + isnull(' for ' + t.description,'') + char(13) + char(10) + isnull(char(13) + char(10) + 'Old message: ' + p.message,'') + isnull(char(13) + char(10) + 'new message: ' + @message,'')
from		#updates u
inner join	policy_matrix_policies p on p.policy_matrix_policy = u.PolicyMatrixPolicy
inner join  policy_matrix_policy_types t on @policy_matrix_policy_type = t.policy_matrix_policy_type
where		isnull(@message,'') <> isnull(p.message,'')

-- Add the items that are not currently defined
insert into policy_matrix_policies (creditor, policy_matrix_policy_type, item_value, message, changed_date)
select		creditor, @policy_matrix_policy_type, @item_value, @message, GETDATE()
from		#updates
where		PolicyMatrixPolicy is null;

-- Update the rows with the new pointers
update		#updates
set			PolicyMatrixPolicy = p.policy_matrix_policy
from		#updates u
inner join	policy_matrix_policies p on u.creditor = p.creditor and @policy_matrix_policy_type = p.policy_matrix_policy_type
where	    PolicyMatrixPolicy is null;

-- Correct the record with the new values
update		policy_matrix_policies
set			message	= @message,
			item_value = @item_value,
			changed_date = getdate()
from		policy_matrix_policies p
inner join  #updates u on p.policy_matrix_policy = u.PolicyMatrixPolicy;

-- Commit the transaction
commit transaction

-- Cleanup
drop table #updates

RETURN ( 1 )
GO
