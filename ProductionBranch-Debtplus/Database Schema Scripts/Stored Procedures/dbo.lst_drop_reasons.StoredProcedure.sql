USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_drop_reasons]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_drop_reasons] AS

-- =================================================================================================
-- ==            Reasons for dropping the client                                                  ==
-- =================================================================================================

-- Disable intermediate result sets
SET NOCOUNT ON

-- Fetch the list of drop reasons
SELECT	drop_reason	as 'item_key',
	[description]	as 'description'
FROM	drop_reasons

RETURN ( @@rowcount )
GO
