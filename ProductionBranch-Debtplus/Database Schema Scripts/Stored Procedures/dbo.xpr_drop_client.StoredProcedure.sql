USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_drop_client]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_drop_client] ( @client as int, @drop_reason as int, @drop_reason_other varchar(50) = null ) as
-- =====================================================================================================
-- ==    Called by the trigger to the update_clients view when the active status is set to "I"        ==
-- =====================================================================================================

-- ChangeLog
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

set nocount on

-- Create the drop notices
select 'DR'					as  'type',
		cc.client			as  'client',
		cc.creditor			as  'creditor',
		cc.client_creditor	as  'client_creditor',
		convert(varchar(1024), 'We have discontinued the client' + isnull(' because ' + dr.description, '')) as 'note_text'
into	#drop_client
from	client_creditor cc with (nolock)
inner join creditors cr with (nolock)					on cc.creditor = cr.creditor
inner join client_creditor_balances bal with (nolock)	on cc.client_creditor_balance = bal.client_creditor_balance and bal.client_creditor = cc.client_creditor
inner join clients c with (nolock)						on cc.client = c.client
left outer join drop_reasons dr with (nolock)			on cc.drop_reason = dr.drop_reason
where	cc.client		   = @client
and	    cc.reassigned_debt = 0
and		cc.drop_date is null;

-- Set the information into the debts to indicate that the debt is now dropped
update	client_creditor
set		drop_reason			= @drop_reason,
		drop_date			= getdate()
from	client_creditor cc
inner join #drop_client x on cc.client_creditor = x.client_creditor;

-- Set the information into the client
-- This should have been set already, but we must make sure....
update	clients
set		drop_reason			= @drop_reason,
		drop_date			= getdate()
where	client				= @client;

-- Load the debt notes for later processing
insert into debt_notes ([type], [client], [creditor], [client_creditor], [note_text], [drop_reason])
select	[type], [client], [creditor], [client_creditor], [note_text], @drop_reason
from	#drop_client;

drop table #drop_client;
return ( 0 )
GO
