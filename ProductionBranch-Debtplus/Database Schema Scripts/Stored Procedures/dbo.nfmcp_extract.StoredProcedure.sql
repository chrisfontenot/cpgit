USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfmcp_extract]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[nfmcp_extract]  (@from_date as datetime = null, @to_date as datetime = null ) as

-- ChangeLog
--  5/14/08
--    Used codes in the NFCC column of the 'Financial Problems' table of LOI (Loss of Income), IEX (Increase in Expense),
--	  BVF (Business Venture Failed), and ILP (Increase in Loan Payment).
--    Added test to ensure that resulted clients are for the proper hud_grant type
--  6/4/08 
--	  modified session length to decimal format
--  6/19/08
--    Corrected loan status
--  7/9/08
--    Added hud_interview ID to the selection table
--  8/4/08
--    Ensured that only priority 1 loans were processed
--  8/5/08
--    Changed date format to MM/DD/YYYY
--	03/06/12
--	  Added field for most recent HUD purpose of visit
--	03/08/12
--	  Added field for MSA

-- Suppress intermediate results
set nocount on

-- Build the resulting table using the common logic
-- If there is no table then just quit here.
declare @result		int
execute @result = nfmcp_extract_select @from_date, @to_date
if @result = 0
	return;

-- Create the table for the results and populate it
create table #results (location varchar(80), value varchar(80));

-- Discard the clients who have indicated that they have NOT signed the privacy policy.
-- This only applies to phone counseling.
delete
from	##tmp_nfcc_nfmc_extract
where	CounselingMode = 1
and	indicator_date is null
and	datediff(d, CounselingIntakeDate, getdate()) < 30;

-- Toss the clients who inhibit data.
/*
-- Count them first.
insert into #results (location, value)
select	'B4', convert(varchar,count(*))
from	##tmp_nfcc_nfmc_extract
where	inhibit_date is not null;
*/

delete
from	##tmp_nfcc_nfmc_extract
where	inhibit_date is not null;

-- Serialize the line numbers in the table. We need to include the row number, starting at 2 for each client.
create table #lines (sequenceID int, clientid int, BranchName varchar(50) null, line int identity(2,1));

insert into #lines (sequenceID, ClientID, BranchName)
select sequenceID, ClientID, BranchName
from ##tmp_nfcc_nfmc_extract
order by clientID, BranchName

update ##tmp_nfcc_nfmc_extract
set    line = l.line
from   ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.SequenceID = l.SequenceID;

drop table #lines

-- Branch ID
insert into #results (location, value)
select 'A' + convert(varchar,line), BranchName
from ##tmp_nfcc_nfmc_extract

-- Client ID
insert into #results (location, value)
select 'B' + convert(varchar,line), ClientID
from ##tmp_nfcc_nfmc_extract

-- Counseling Level
insert into #results (location, value)
select 'C' + convert(varchar,line), CounselingLevel
from ##tmp_nfcc_nfmc_extract

-- Intake date
insert into #results (location, value)
select 'D' + convert(varchar,line), convert(varchar(10), CounselingIntakeDate, 101)
from ##tmp_nfcc_nfmc_extract

-- Counseling Mode
insert into #results (location, value)
select 'E' + convert(varchar,line), CounselingMode
from ##tmp_nfcc_nfmc_extract

-- Identity
insert into #results (location, value)
select 'F' + convert(varchar,line), FirstName
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'G' + convert(varchar,line), LastName
from ##tmp_nfcc_nfmc_extract

-- Age
insert into #results (location, value)
select 'H' + convert(varchar,line), Age
from ##tmp_nfcc_nfmc_extract

-- Race
insert into #results (location, value)
select 'I' + convert(varchar,line), Race
from ##tmp_nfcc_nfmc_extract

-- Ethnicicty
insert into #results (location, value)
select 'J' + convert(varchar,line), Ethnicity
from ##tmp_nfcc_nfmc_extract

-- Gender
insert into #results (location, value)
select 'K' + convert(varchar,line), Gender
from ##tmp_nfcc_nfmc_extract

-- Household type
insert into #results (location, value)
select 'L' + convert(varchar,line), HouseholdType
from ##tmp_nfcc_nfmc_extract

-- Household income
insert into #results (location, value)
select 'M' + convert(varchar,line), HouseholdIncome
from ##tmp_nfcc_nfmc_extract

-- Income category
insert into #results (location, value)
select 'N' + convert(varchar,line), IncomeCategory
from ##tmp_nfcc_nfmc_extract

-- Address
insert into #results (location, value)
select 'O' + convert(varchar,line), HouseNo
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'P' + convert(varchar,line), Street
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'Q' + convert(varchar,line), City
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'R' + convert(varchar,line), [State]
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'S' + convert(varchar,line), Zip
from ##tmp_nfcc_nfmc_extract

-- Total individual foreclosure hours received
insert into #results (location, value)
select 'T' + convert(varchar,line), isnull(Total_Individual_foreclosure_hours_received,'0')
from ##tmp_nfcc_nfmc_extract

-- Total group foreclosure hours received
insert into #results (location, value)
select 'U' + convert(varchar,line), isnull(Total_group_foreclosure_hours_received,'0')
from ##tmp_nfcc_nfmc_extract

-- Name of originating lender
insert into #results (location, value)
select 'V' + convert(varchar,line), NameofOriginatingLender
from ##tmp_nfcc_nfmc_extract

-- FDIC of originating lender
insert into #results (location, value)
select 'W' + convert(varchar,line), FDICofOriginalLender
from ##tmp_nfcc_nfmc_extract

-- Original Loan Number
insert into #results (location, value)
select 'X' + convert(varchar,line), OriginalLoanNumber
from ##tmp_nfcc_nfmc_extract

-- Current loan servicer
insert into #results (location, value)
select 'Y' + convert(varchar,line), CurrentLoanServicer
from ##tmp_nfcc_nfmc_extract

-- FDIC of current servicer
insert into #results (location, value)
select 'Z' + convert(varchar,line), Current_Servicer_FDIC
from ##tmp_nfcc_nfmc_extract

-- Current service loan number
insert into #results (location, value)
select 'AA' + convert(varchar,line), CurrentServicerLoanNo
from ##tmp_nfcc_nfmc_extract

-- Credit score
insert into #results (location, value)
select 'AB' + convert(varchar,line), CreditScore
from ##tmp_nfcc_nfmc_extract

-- Why no credit score
insert into #results (location, value)
select 'AC' + convert(varchar,line), WhyNoCreditScore
from ##tmp_nfcc_nfmc_extract

-- Credit score type
insert into #results (location, value)
select 'AD' + convert(varchar,line), ScoreType
from ##tmp_nfcc_nfmc_extract

-- Total Monthly PITI as intake
insert into #results (location, value)
select 'AE' + convert(varchar,line), PITIatIntake
from ##tmp_nfcc_nfmc_extract

-- First or second loan
insert into #results (location, value)
select 'AF' + convert(varchar,line), FirstOrSecondLoan
from ##tmp_nfcc_nfmc_extract

-- Has second loan
insert into #results (location, value)
select 'AG' + convert(varchar,line), HasSecondLoan
from ##tmp_nfcc_nfmc_extract

-- Loan Product Type
insert into #results (location, value)
select 'AH' + convert(varchar,line), LoanProductType
from ##tmp_nfcc_nfmc_extract

-- Interest only
insert into #results (location, value)
select 'AI' + convert(varchar,line), InterestOnly
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AJ' + convert(varchar,line), Hybrid
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AK' + convert(varchar,line), OptionARM
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AL' + convert(varchar,line), VAorHFAInsured
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AM' + convert(varchar,line), PrivatelyHeld
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AN' + convert(varchar,line), ARMReset
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AO' + convert(varchar,line), DefaultReasonCode
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AP' + convert(varchar,line), LoanStatusAtContact
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AQ' + convert(varchar,line), CounselingOutcomeCode
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AR' + convert(varchar,line), convert(varchar(10), CounselingOutcomeDate, 101)
from ##tmp_nfcc_nfmc_extract

-- ========================= ADDITIONAL FIELDS FOR CLEARPOINT ==============================

insert into #results (location, value)
select 'AS' + convert (varchar,line), PurposeOfVisit
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AT' + convert (varchar,line), pov_created_by
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AU' + convert (varchar,line), MSA_Name
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AV' + convert (varchar,line), MHA_screening_note
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AW' + convert (varchar,line), GLB_verbal_note
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AX' + convert (varchar,line), NFMCP_verbal_note
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AY' + convert (varchar,line), Level_2_note
from ##tmp_nfcc_nfmc_extract

-- Return the results
select 'NFMC Reporting Template Ver. 2' as sheet, location, value
from #results
where value is not null
order by location;

drop table #results;
-- drop table ##tmp_nfcc_nfmc_extract;
return ( 0 );
GO
