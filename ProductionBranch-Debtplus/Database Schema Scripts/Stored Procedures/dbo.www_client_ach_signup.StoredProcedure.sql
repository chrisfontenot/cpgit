USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_ach_signup]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_ach_signup] (@client as int, @aba as varchar(9), @account as varchar(22), @type as varchar(1)) as

-- =================================================================================================
-- ==            Perform an ACH signup operation on the client                                    ==
-- =================================================================================================

set nocount on

if @account is null
	select @account = ''

if @aba is null
	select @aba = ''

if @type is null
	select @type = ''

select @account = ltrim(rtrim(@account)),
	@aba	= ltrim(rtrim(@aba)),
	@type	= ltrim(rtrim(@type))

if len(@aba) != 9
begin
	RaisError ('The length of the ABA value must be 9 digits. Please correct it.', 16, 1)
	return ( 0 )
end

if len(@account) = 0
begin
	RaisError ('The account number is required and was not supplied. Please give an account number', 16, 1)
	return ( 0 )
end

if @type not in ('S', 'C')
begin
	RaisError ('The type of the account is neither a checking nor savings account. Please correct it.', 16, 1)
	return ( 0 )
end

-- Ensure that the client is valid
if not exists (select client from clients where client = @client)
begin
	RaisError ('The client is not valid. Please correct the client ID', 16, 1)
	return ( 0 )
end

-- Configure the client to use ACH
update	client_ach
set		isActive		= 1,
		StartDate		= getdate(),
		CheckingSavings	= @type,
		ABA				= @aba,
		AccountNumber	= @account,
		PrenoteDate		= null
where	client			= @client

if @@rowcount < 1
begin
	RaisError ('The client could not be modified at this time. Please try again later.', 16, 1)
	return ( 0 )
end

insert into client_notes (client, type, subject, is_text, created_by, date_created, note)
values (@client, 3, 'ACH status updated from WWW interface', 1, 'WWW User', getdate(),
'The client has updated the ACH status to start ACH as soon as possible.' + char(13) + char(10) +
'The ABA number entered was: ' + @aba + char(13) + char(10) +
'The bank account number entered was: ' + @account + char(13) + char(10) +
'The type of the account entered was: ' + case @type when 'S' then 'Savings' else 'Checking' end +
char(13) + char(10) +
char(13) + char(10) +
'The prenote date was reset due to the change in the account status')

return ( 1 )
GO
