USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_letter_mark_printed]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_letter_mark_printed] ( @letter_queue as int ) as
update	letter_queue
set	date_printed	= getdate(),
	death_date	= dateadd(d, 4, getdate())
where	letter_queue	= @letter_queue
return ( @@rowcount )
GO
