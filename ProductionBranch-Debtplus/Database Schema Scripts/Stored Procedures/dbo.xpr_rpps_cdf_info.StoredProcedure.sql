USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdf_info]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdf_info] ( @rpps_transaction AS INT ) AS
-- ===========================================================================================
-- ==            Generate an RPPS CDF information for a specific proposal                   ==
-- ===========================================================================================

-- ChangeLog
--    8/11/2003
--       Initial creation
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate result sets
SET NOCOUNT ON

-- Fetch the information from the current proposal information
SELECT	cc.client							as 'client',
	''								as 'company_identifier',
	left(coalesce (p.ssn, '000000000') + '000000000', 9)		as 'ssn',
	isnull(t.new_account_number,cc.account_number)			as 'new_account_number',
	t.return_code							as 'exception_code',
	isnull(cc.orig_dmp_payment, cc.disbursement_factor)		as 'monthly_payment_amount',
	isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0) as 'balance'

FROM		rpps_transactions t	WITH (NOLOCK)

INNER JOIN	client_creditor cc	WITH (NOLOCK) ON t.client_creditor = cc.client_creditor
inner join	client_creditor_balances bal on cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN people p		WITH (NOLOCK) ON cc.client = p.client AND dbo.DebtOwner(cc.client_creditor) = p.person

WHERE					t.rpps_transaction = @rpps_transaction

-- Return success to the caller
RETURN ( 1 )
GO
