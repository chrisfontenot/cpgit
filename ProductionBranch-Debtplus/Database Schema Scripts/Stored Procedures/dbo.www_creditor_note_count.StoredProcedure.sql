USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_note_count]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_note_count] ( @creditor as typ_creditor ) as

-- Determine the number of notes possible for the creditor
declare	@note_count	int
select	@note_count	= count(*)
from	creditor_www_notes
where	creditor	= @creditor
and	date_viewed is null

if @note_count is null
	select @note_count = 0

-- Return the creditor names/addresses to the page
select		creditor,
		creditor_name,
		division
from		creditors
where		creditor = @creditor

return ( 0 )
GO
