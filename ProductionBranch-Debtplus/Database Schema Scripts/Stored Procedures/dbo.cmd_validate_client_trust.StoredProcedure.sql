USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_validate_client_trust]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cmd_validate_client_trust] ( @period_start as datetime = null, @period_stop as datetime = null) as
-- ===================================================================================================
-- ==           Look at the client transactions in the CURRENT month only. Match that against the   ==
-- ==           client trust balance. Report all clients who have a trust balance that does not     ==
-- ==           match in the current month, it starts with the BB transaction.                      ==
-- ===================================================================================================

select	client, held_in_trust
into	#clients
from	clients

if @period_stop is null
	select	@period_stop	= getdate()

if @period_start is null
	select	@period_start	= convert(datetime, convert(varchar,month ( @period_stop )) + '/01/' +  convert(varchar,year ( @period_stop )))

select	client, sum(isnull(credit_amt,0) - isnull(debit_amt,0)) as held_in_trust
into	#trans
from	registers_client
where	tran_type NOT IN ('LD','EB')
and	date_created between @period_start and @period_stop
group by client

select	isnull(a.client, b.client) as client, isnull(a.held_in_trust,0) as 'client trust', isnull(b.held_in_trust,0) as 'calculated'
from	#clients a
full outer join #trans b on a.client = b.client
where	isnull(a.held_in_trust,0) <> isnull(b.held_in_trust,0)
and	isnull(a.client, b.client) <> 0;

drop table #clients
drop table #trans
GO
