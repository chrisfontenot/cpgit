SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_client_refund_CR] ( @Client AS typ_client, @Amount AS Money = 0, @message AS typ_message = NULL, @tran_type AS typ_transaction = NULL ) AS

-- =========================================================================================================================
-- ==            Generate a client refund check where the check is hand-written                                           ==
-- =========================================================================================================================

DECLARE	@trust_register	INT
DECLARE	@TrustBalance	MONEY
DECLARE	@p1		VarChar(80)
DECLARE	@cleared	Char(1)

-- Suppress result sets
SET NOCOUNT ON

-- Generate a transaction to hold the money transfer
BEGIN TRANSACTION

IF @Amount <= 0
BEGIN
	RaisError(50019, 16, 1)
	Rollback Transaction
	Return ( 0 )
END

-- If the type is not indicated then determine the proper transaction type
IF @tran_type IS NULL
	SET @tran_type = 'CR'

IF @tran_type NOT IN ('AR', 'CR', 'MR')
BEGIN
	RaisError(50080, 16, 1)
	Rollback Transaction
	Return ( 0 )
END

-- Ensure that there is a valid trust balance
SELECT	@TrustBalance	= isnull(held_in_trust,0)
FROM	clients
WHERE	client		= @Client

IF @TrustBalance IS NULL
BEGIN
	RaisError(50014, 16, 1, @Client)
	Rollback Transaction
	Return ( 0 )
END

-- If the trust balance is negative then complain here
IF @TrustBalance < 0
BEGIN
	SET @p1 = '-$' + convert(varchar, 0 - @TrustBalance, 1)
	RaisError(50063, 16, 1, @p1 )
	RollBack Transaction
	Return ( 0 )
END	

-- Ensure that the trust balance is valid
IF @TrustBalance < @Amount
BEGIN
	SET @p1 = '$' + convert(varchar, @TrustBalance, 1)
	RaisError(50064, 16, 1, @p1 )
	RollBack Transaction
	Return ( 0 )
END

-- Take the money from the client
UPDATE	clients
SET	held_in_trust	= held_in_trust - @Amount
WHERE	client		= @Client

SELECT	@TrustBalance	= held_in_trust
FROM	clients
WHERE	client		= @Client

-- The new client balance must be zero or posative
IF @TrustBalance < 0
BEGIN
	RaisError(50064, 16, 1, @p1 )
	RollBack Transaction
	Return ( 0 )
END

-- Insert the trust register item
insert into registers_trust	(tran_type,	client,		amount,		cleared,	checknum,	check_order)
values				(@tran_type,	@client,	@amount,	'P',		NULL,		0)

SELECT @trust_register = SCOPE_IDENTITY()

-- Insert the client item
insert into registers_client	(tran_type,	client,		debit_amt,	trust_register,		message)
values				(@tran_type,	@client,	@amount,	@trust_register,	@message)

-- Commit the transaction and return the trust register for the transaction
COMMIT TRANSACTION
RETURN ( @trust_register )
GO
