USE [DebtPlus]
GO
CREATE PROCEDURE [dbo].[UPDATE_add_column] ( @table as varchar(80), @column as varchar(80), @type as varchar(80) = 'varchar(40)') as

-- Take an early abort if the table does not exist
IF NOT EXISTS(SELECT * FROM [sysobjects] WHERE [type] = 'U' AND [name] = @table)
	return

-- Used in the update scripts to ensure that a column is defined in the tables prior to the conversion of the data.
-- This allows us to have some tables which have been upgraded and others which have not.
declare @stmt	varchar(8000)
select @stmt = 'if not exists (select * from INFORMATION_SCHEMA.COLUMNS where table_name = N''' + @table + ''' AND column_name = N''' + @column + ''') alter table [' + @table + '] add [' + @column + '] ' + @type + ' null;'
exec ( @stmt )
GO
