USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_creditor_invoice]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_creditor_invoice] ( @creditor AS typ_creditor = NULL ) AS
-- ===========================================================================================
-- ==               Generate the list of creditors which have pending invoices. This is the ==
-- ==               recordset for the top-level invoice report. It returns the creditors    ==
-- ==               that should be printed.                                                 ==
-- ===========================================================================================

-- ChangeLog
--   11/03/2002
--     Suppressed selecting creditors which have "suppress_invoice" set.
--   3/28/2011
--     Allowed supressed invoice creditors to come through if they have already been
--     selected for an invoice. Otherwise, the names are not shown and it looks strange.

-- Suppress intermedite results
set nocount on

if @creditor is not null
begin
	select	@creditor = ltrim(rtrim(@creditor))
	if @creditor = ''
		select @creditor = null
end

-- If the creditor is supplied then just return that creditor
IF @creditor is not null
BEGIN
	declare	@Address			int
	
	select	@Address			= AddressID
	from	creditor_addresses with (NOLOCK)
	Where	Creditor			= @creditor
	And		Type				= 'I';

	if @Address is null
		Select	@Address			= AddressID
		From	creditor_addresses
		Where	Creditor			= @creditor
		And		Type				= 'P';

	Select	Creditor			as 'creditor',
			Creditor_name		as 'creditor_name',
			po_number			as 'po_number',
			
			@Address			as 'Address',
			a.PostalCode		as 'PostalCode'

	From	creditors cr with (nolock)
	Left outer join addresses a WITH (NOLOCK) ON @Address = a.Address
	Where	Creditor			= @creditor
--	And		suppress_invoice	= 0

	return ( 1 )
END

-- Select the creditors from the system which have pending invoices
Select	cr.creditor					as 'creditor',
		cr.creditor_name			as 'creditor_name',
		cr.po_number				as 'po_number',
		convert(int,null)			as 'address',
		convert(varchar(10),null)	as 'postalcode'
Into	#list
From	creditors cr with (nolock)
Where	cr.creditor in (
	Select	i.creditor
	From	registers_invoices i with (nolock)
	Where	isnull(i.inv_amount,0) > (isnull(i.pmt_amount,0) + isnull(i.adj_amount,0))
	And		cr.suppress_invoice = 0
)

-- Prefer the invoice address record
Update	#list
Set		address		= ca.addressid
From	#list l
Inner join creditor_addresses ca on l.creditor = ca.creditor AND 'I' = ca.type;

-- Use the payment address if there is no invoice address
Update	#list
Set		address		= ca.addressid
From	#list l
Inner join creditor_addresses ca on l.creditor = ca.creditor AND 'P' = ca.type
Where	l.address is null;

-- Return the information to the caller
declare	@rowcount		int

select	l.creditor					as 'creditor',
		l.creditor_name				as 'creditor_name',
		l.po_number					as 'po_number',
		
		-- SQL 2005 needs these columns
		a.Address					as 'address',
		a.PostalCode				as 'PostalCode'
from	#list l
left outer join addresses a with (nolock) on l.addressid = a.address
order by PostalCode;

-- Find the list of rows returned
select	@rowcount = @@rowcount;

-- Drop the table for the results
drop table #list

return	( @rowcount )
GO
