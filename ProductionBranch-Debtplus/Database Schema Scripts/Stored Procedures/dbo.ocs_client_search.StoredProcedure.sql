USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[ocs_client_Search]    Script Date: 01/22/2016 12:53:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
DROP PROCEDURE [dbo].[ocs_client_Search] 
GO 

-- =============================================
-- Author:		Kevin Wilkie
-- Create date: 12/4/2015
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[ocs_client_Search] 
	-- Add the parameters for the stored procedure here
	@Field varchar(20) = NULL, 
	@Operator varchar(10) = 'EQ',
	@Value1 varchar(50) = NULL
AS
BEGIN
	DECLARE @PhoneFun varchar(30)
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM dbo.OCSClientSearch WHERE UserName = SUSER_NAME()
	
	-- Start building out the sql we'll use later as much as we can now
	DECLARE @SQLString nvarchar(max);
	SET @SQLString = 'WITH OCS AS (SELECT DISTINCT OCS.ID, ClientID, ActiveFlag, Archive, InvestorNumber, SearchServicerId, UploadRecord, BatchName, [Description] '
	SET @SQLString =  @SQLString + '	FROM OCS_Client OCS WITH (NOLOCK) '
	SET @SQLString =  @SQLString + '	LEFT JOIN OCS_PartnerProgram Program WITH (NOLOCK) ON ocs.Program = program.Id '
	SET @SQLString =  @SQLString + '	LEFT JOIN OCS_UploadReport rpt WITH (NOLOCK) ON ocs.UploadAttempt = rpt.AttemptId),'
	SET @SQLString =  @SQLString + 'OCSUpload AS (SELECT DISTINCT Id, ClientStreet, ClientCity, ClientState, ClientZipcode, SSN1, '
	SET @SQLString =  @SQLString + '	FirstName1, LastName1, Phone1, Phone2, Phone3, Phone4, Phone5, Servicer, LoanNumber '
	SET @SQLString =  @SQLString + '	FROM OCS_UploadRecord WITH (NOLOCK)) ' 
	SET @SQLString =  @SQLString + 'INSERT INTO OCSClientSearch '
	SET @SQLString =  @SQLString + 'SELECT '
    SET @SQLString =  @SQLString + 'ocs.Id [Id], '
    SET @SQLString =  @SQLString + 'ocs.ClientId [ClientId], '
    SET @SQLString =  @SQLString + 'ocs.ActiveFlag, '
    SET @SQLString =  @SQLString + 'ocs.Archive, '
    SET @SQLString =  @SQLString + 'ocs.BatchName, '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(ocs.InvestorNumber)) [InvestorNo],  '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.ClientStreet)) [StreetAddress], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.ClientCity)) [City], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.ClientState)) [StateAbbreviation], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.ClientZipcode)) [ZipCode], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.SSN1)) [AppSSN], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(ocs.Description)) [Program], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.FirstName1)) [AppFirstName], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.LastName1)) [AppLastName], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.Phone1)) [Phone1], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.Phone2)) [Phone2], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.Phone3)) [Phone3], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.Phone4)) [Phone4], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.Phone5)) [Phone5], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.Servicer)) [Servicer], '
    SET @SQLString =  @SQLString + 'LTRIM(RTRIM(upload.LoanNumber)) [ServicerLoanNum], '
    SET @SQLString =  @SQLString + 'ocs.SearchServicerId [ServicerId], ''' + SUSER_NAME() + ''''
	SET @SQLString =  @SQLString + 'FROM OCS [ocs] '
	SET @SQLString =  @SQLString + 'LEFT JOIN OCSUpload [upload] ON upload.Id = ocs.UploadRecord WHERE 1=1 '

	-- Figure out what field(s) we're going to work with
	IF @Field != 'Phone' 
	BEGIN
		SELECT @SQLString = CASE 
								WHEN @Field = 'DebtPlusID' THEN @SQLString + 'AND ocs.ClientId '
								WHEN @Field = 'LastName' THEN @SQLString + 'AND upload.LastName1 '
								WHEN @Field = 'Street' THEN @SQLString + 'AND upload.ClientStreet '
								WHEN @Field = 'InvestorLoanNo' THEN @SQLString + 'AND ocs.InvestorNumber '
								WHEN @Field = 'ServicerLoanNo' THEN @SQLString + 'AND upload.LoanNumber '
								WHEN @Field = 'ServicerNo' THEN @SQLString + 'AND upload.Servicer '
								WHEN @Field = 'BatchName' THEN @SQLString + 'AND ocs.BatchName '
							END

		-- Now set the operator and values we'll be playing with
		SELECT @SQLString = CASE 
								WHEN @Operator = 'EQ'  THEN @SQLString + ' = ''' + @Value1 + ''''
								WHEN @Operator = 'NEQ' THEN @SQLString + ' >= ''' + @Value1 + ''''
								WHEN @Operator = '>'  THEN @SQLString + ' > ' + @Value1
								WHEN @Operator = '<'  THEN @SQLString + ' < ' + @Value1
								WHEN @Operator = '<=' THEN @SQLString + ' <= ' + @Value1
								WHEN @Operator = 'not =' THEN @SQLString + ' != ' + @Value1
								WHEN @Operator = 'between' THEN @SQLString + ' BETWEEN ' + @Value1
								WHEN @Operator = 'not between' THEN @SQLString + ' NOT BETWEEN ' + @Value1
								WHEN @Operator = 'Contains' THEN @SQLString + ' LIKE ''%' + @Value1 + '%'''
								WHEN @Operator = 'NotContains' THEN @SQLString + ' NOT LIKE ''%' + @Value1 + '%'''
								WHEN @Operator = 'EndsWith' THEN @SQLString + ' LIKE ''%' + @Value1 + ''''
								WHEN @Operator = 'StartsWith' THEN @SQLString + ' LIKE ''' + @Value1 + '%'''
								WHEN @Operator = 'NotStartsWith' THEN @SQLString + ' NOT LIKE ''' + @Value1 + '%'''
							END
	END
	ELSE	-- Phones just have to be different
	BEGIN
		SELECT @SQLString = @SQLString + 'AND (upload.Phone1{Phone} OR upload.Phone2{Phone} OR upload.Phone3{Phone} OR upload.Phone4{Phone} OR upload.Phone5{Phone})'
		
		SELECT @PhoneFun = CASE 
								WHEN @Operator = 'EQ'  THEN ' = ''' + @Value1 + ''''
								WHEN @Operator = 'NEQ' THEN ' >= ''' + @Value1 + ''''
								WHEN @Operator = '>'  THEN ' > ' + @Value1
								WHEN @Operator = '<'  THEN ' < ' + @Value1
								WHEN @Operator = '<=' THEN ' <= ' + @Value1
								WHEN @Operator = 'not =' THEN ' != ' + @Value1
								WHEN @Operator = 'Contains' THEN ' LIKE ''%' + @Value1 + '%'''
								WHEN @Operator = 'NotContains' THEN ' NOT LIKE ''%' + @Value1 + '%'''
								WHEN @Operator = 'EndsWith' THEN ' LIKE ''%' + @Value1 + ''''
								WHEN @Operator = 'StartsWith' THEN ' LIKE ''' + @Value1 + '%'''
								WHEN @Operator = 'NotStartsWith' THEN ' NOT LIKE ''' + @Value1 + '%'''
							END
		SELECT @SQLString = REPLACE(@SQLString, '{Phone}', @PhoneFun)
	END

	--PRINT @SQLString
	EXEC sp_executesql @SQLString

		SELECT 		Id					AS 		Id					
	,ClientID			AS      ClientID			
	,ActiveFlag			AS      ActiveFlag			
	,Archive			AS 	    Archive			
	,BatchName			AS      BatchName			
	,InvestorNo			AS      InvestorNo			
	,StreetAddress		AS      StreetAddress		
	,City				AS      City				
	,StateAbbreviation 	AS      StateAbbreviation
	,ZipCode			AS      ZipCode			
	,AppSSN			    AS      AppSSN			  
	,Program			AS      Program			
	,AppFirstName	    AS      AppFirstName	  
	,AppLastName		AS      AppLastName		
	,Phone1			    AS      Phone1			  
	,Phone2			    AS      Phone2			  
	,Phone3			    AS      Phone3			  
	,Phone4			    AS      Phone4			  
	,Phone5			    AS      Phone5			  
	,Servicer		    AS      Servicer		  
	,ServicerLoanNum    AS      ServicerLoanNum  
	,ServicerID		    AS      ServicerID	
	FROM OCSClientSearch WITH (NOLOCK)
	WHERE Username = SUSER_NAME()

END