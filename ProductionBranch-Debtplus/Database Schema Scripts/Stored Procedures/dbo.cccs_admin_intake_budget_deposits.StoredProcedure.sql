USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_budget_deposits]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_budget_deposits] ( @application_id as int, @client as int = NULL ) as

-- If there is no client then find it from the re-import tables
if @client is null
	select	@client = ForeignDatabaseID
	from	cccs_admin..applications
	where	application_id = @application_id

-- If there still is no client then do not continue	
if @client is null
begin
	RaisError('Client is NULL and can not be determined', 16, 1)
	return ( -1 )
end

declare	@deposit_amount		money
declare	@deposit_date		datetime

-- Find the amount to be deposited on the indicated date
select	@deposit_amount = sum(disbursement_factor)
from	client_creditor with (nolock)
where	client		= @client;

-- It is not allowed to be null.
if @deposit_amount is null
	select @deposit_amount = 0

-- Find the cushion factor for the deposits
declare	@dmp_payment	money
select	@dmp_payment	= dmp_payment
from	cccs_admin..budgets
where	application_id = @application_id

-- If there is a payment figure then use it rather than the simple sum of the disbursement factors
if isnull(@dmp_payment,0) > 0
	select	@deposit_amount = @dmp_payment

-- Update the existing record on the match for the item
declare @client_deposit int
select  @client_deposit = client_deposit
from    client_deposits
where   client = @client;

if @client_deposit is null
begin

	-- Remove the relative time, but start with 10 days in the future
	select	@deposit_date	= convert(varchar(10), dateadd(d, 10, getdate()), 101)

	insert into client_deposits (client, deposit_amount, deposit_date, ach_pull) values (@client, @deposit_amount, @deposit_date, 1);
	select @client_deposit = SCOPE_IDENTITY()
end

-- Update the existing row with the new deposit amount
update client_deposits
set    deposit_amount = isnull(@deposit_amount, 0)
where  client_deposit = @client_deposit;

return ( @client_deposit )
GO
