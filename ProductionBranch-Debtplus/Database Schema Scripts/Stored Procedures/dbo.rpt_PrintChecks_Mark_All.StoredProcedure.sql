USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintChecks_Mark_All]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintChecks_Mark_All] ( @bank as int = -1 ) AS

-- Suppress intermediate result sets
set nocount on

if isnull(@bank,-1) < 0
	select	@bank	= min(bank)
	from	banks with (nolock)
	where	type	= 'C'

-- Generate a distinct ID for the pending checks
declare	@id_string	Varchar(50)
select	@id_string = newid()

-- Mark the pending checks to be printed
update	registers_trust
set	sequence_number		= @ID_String
from	registers_trust
where	isnull(cleared,' ')	= 'P'
and	((creditor IS NOT NULL) OR (client is not null))
and	amount			> 0.0

-- Until we are absolutely certain that this will work, just say that it works with any bank account.
--and	bank			= @bank

-- Return the ID string for the marked checks
select	@id_string	as [id],
	count(*)	as items
from	registers_trust
where	isnull(cleared,' ')	= 'P'
and	sequence_number		= @id_string

-- Return the number of rows in the result
return ( 1 )
GO
