USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_Housing_CounselorAttribute]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_Housing_CounselorAttribute] ( @Counselor as int, @HousingAttribute as int ) AS
-- ======================================================================================
-- ==        Insert a row into the Housing_CounselorAttributes table for .NET          ==
-- ======================================================================================
insert into Housing_CounselorAttributes ( [Counselor], [HousingAttribute] ) values ( @Counselor, @HousingAttribute )
return ( scope_identity() )
GO
