USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_AddressInformation]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_AddressInformation] (@client as int) AS
-- =========================================================================================
-- ==                             Fetch the Client Address information                    ==
-- ==                                                                                     ==
-- ==  ** DO NOT CHANGE THIS STORED PROCEDURE. DO NOT ADD COLUMNS. DO NOT DELETE COLUMNS  ==
-- ==     IN SHORT, KEEP YOUR HANDS OFF THIS PROCEDURE OR YOU WILL HAVE TO RE-VERIFY      ==
-- ==     MANY REPORTS; AND SOME OF THEM ARE EMBEDDED IN APPLICATIONS !!! **              ==
-- ==                                                                                     ==
-- ==  If you wish to augment this function, update the view and select items from the    ==
-- ==  view directly. The 'newer' reports do this rather than call this procedure, but    ==
-- ==  some of the old ones still use this proc. So, keep your hands off this procedure.  ==
-- ==                                                                                     ==
-- ==  You have been warned.                                                              ==
-- =========================================================================================

-- Retrieve the standard information from the total view of formatted items
select	[zipcode], [name], [addr1], [addr2], [addr3], [salutation]
from	view_client_address
where	client = @client

RETURN ( @@rowcount )
GO
