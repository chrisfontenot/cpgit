USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_Client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_Client] ( @Client AS INT ) AS

SELECT		c.salutation,
		dbo.format_address_line_1(ca.house,ca.direction,ca.street,ca.suffix,ca.modifier,ca.modifier_value) as address1,
		ca.address_line_2 as address2,
		dbo.format_city_state_zip ( ca.city, ca.state, ca.postalcode ) as 'address3',

		dbo.format_TelephoneNumber ( c.HomeTelephoneID ) AS 'home_ph',
		dbo.format_TelephoneNumber ( c.MsgTelephoneID ) AS 'message_ph',

		case isnull(c.active_status, 'I')
			when 'I'   then 'Inactive'
			when 'A'   then 'Active'
			when 'AR'  then 'Active Restart'
			when 'SC'  then 'Successful Completion'
			when 'SA'  then 'Self-Administered'
			when 'PND' then 'Pending Further Information'
			when 'WKS' then 'Attended Workshop only'
			when 'FCO' then 'Financial Councelling'
			when 'NP'  then 'Terminated, Non-Payment'
			when 'B'   then 'Terminated, Bankruptcy'
			else active_status
		end as 'active_status',

	case ch.housing_status
		when 0 then 'Homeless'
		when 1 then 'Lease Purchase'
		when 2 then 'Friend/Relative'
		when 3 then 'Military Housing'
		when 4 then 'Other'
		when 5 then 'Own'
		when 6 then 'Own/Mortgage'
		when 7 then 'Owner Financed'
		when 8 then 'Rent'
	end as 'housing_status',

	start_date,
	disbursement_date,
	drop_date,

	isnull(drp.description, drop_reason_other) as 'drop_reason',
	isnull(referred_by.description,'') as 'referred_by',

	fin1.description as 'cause_fin_problem1',
	fin2.description as 'cause_fin_problem2',
	fin3.description as 'cause_fin_problem3',
	fin4.description as 'cause_fin_problem4',

	o.name as 'office',

	convert(money,0) as other_debt,
	hold_disbursements,
	personal_checks,
	convert(bit,isnull(ach.isActive,0)) as ach_active,
	stack_proration,
	mortgage_problems,
	fed_tax_owed,		fed_tax_months,
	state_tax_owed,		state_tax_months,
	local_tax_owed,		local_tax_months,

	held_in_trust,

	case isnull(marital_status, 1)
		when 1 then 'Single'
		when 2 then 'Married'
		when 3 then 'Divorced'
		when 4 then 'Widowed'
		when 5 then 'Seperated'
	end as 'marital_status',

	c.date_created,

	isnull(l.Attribute, 'English') as 'language',

	ach.CheckingSavings as ach_account,
	ach.ABA as ach_routing_number,
	ach.AccountNumber as ach_bank_number,

	isnull('Prenoted on ' + convert(varchar(10), ach.PrenoteDate, 1), 'Not Performed') as 'prenote_status',

	ach.ErrorDate as ach_error_date,

	fee.description
FROM			clients c
LEFT OUTER JOIN client_housing ch WITH (nolock) ON c.client = ch.client
LEFT OUTER JOIN client_ach ach with (nolock) on c.client = ach.client
LEFT OUTER JOIN	AttributeTypes l ON c.language = l.oID
LEFT OUTER JOIN	offices o ON c.office = o.office
LEFT OUTER JOIN	financial_problems fin1 ON c.cause_fin_problem1 = fin1.financial_problem
LEFT OUTER JOIN	financial_problems fin2 ON c.cause_fin_problem2 = fin2.financial_problem
LEFT OUTER JOIN	financial_problems fin3 ON c.cause_fin_problem3 = fin3.financial_problem
LEFT OUTER JOIN	financial_problems fin4 ON c.cause_fin_problem4 = fin4.financial_problem
LEFT OUTER JOIN	referred_by ON c.referred_by = referred_by.referred_by
LEFT OUTER JOIN	drop_reasons drp ON c.drop_reason = drp.drop_reason
LEFT OUTER JOIN	config_fees fee ON c.config_fee = fee.config_fee
LEFT OUTER JOIN	addresses ca ON c.addressid = ca.Address
WHERE			c.client = @client

RETURN ( @@rowcount )
GO
