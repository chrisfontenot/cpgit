SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_housing_arm_v4_agencies] as

-- ======================================================================================
-- ==         Return the information needed for the agency profiles in the hud extract ==
-- ======================================================================================

-- ChangeLog
--   9/11/2010
--     Initial creation

-- Supress intermediate results
set nocount on

-- Create the rsult table to hold the office information
create table #results (
		office						int null,
		agc_hcs_id					int,
		Description					varchar(50) null,
		UserName					varchar(100) null,
		Password					varchar(100) null,
		[Default]					bit null,
		[ActiveFlag]				bit null,

		agc_name					varchar(100) null,
		agc_cms_type				int null,
		agc_ein						varchar(100) null,
		agc_dun_nbr					varchar(100) null,
		agc_address1				varchar(100) null,
		agc_address2				varchar(100) null,
		agc_address3				varchar(100) null,
		agc_address4				varchar(100) null,
		agc_city					varchar(100) null,
		agc_state					int null,
		agc_zip						varchar(100) null,
		agc_web_site				varchar(100) null,
		agc_phone_nbr				varchar(100) null,
		alternate_phone_nbr			varchar(100) null,
		agc_fax_nbr					varchar(100) null,
		
		agc_email					varchar(100) null,
		agc_faith_based_ind			bit null,
		agc_colonias_ind			bit null,
		agc_migrfarm_worker_ind		bit null,
		agc_counselin_budget_amt	int null,
		Send9902					bit null,
		SendCounselors				bit null,
		SendClients					bit null,
		SendAttendees				bit null,
		SendClientSSN				bit null,
		SendClientCount				int null,

		-- Information from the office record
		office_addressID				int null,
		office_emailID					int null,
		office_TelephoneID				int null,
		office_AltTelephoneID			int null,
		office_FAXID					int null
);

insert into #results (agc_hcs_id, Description, UserName, Password, agc_faith_based_ind, agc_colonias_ind, agc_migrfarm_worker_ind, agc_counselin_budget_amt)
select	hcs_id, Description, username, password, case faith_based when 0 then 'False' else 'True' end, case colonias when 0 then 'False' else 'True' end, case migrant_farm_worker when 0 then 'False' else 'True' end, convert(int, counseling_amount)
from	housing_arm_hcs_ids
where	ActiveFlag <> 0
order by 1;

-- Define the office information for the agencies
select	office, hud_hcs_id, convert(int,[default]) as 'default', convert(int,[ActiveFlag]) as 'ActiveFlag'
into	#ids
from	offices;

-- Default the HUD id if needed
declare	@default_hcs_id	int
select	@default_hcs_id	 = min(hcs_id)
from	housing_arm_hcs_ids
where	activeflag <> 0
and		[default] <> 0;

if @default_hcs_id is null
	select	@default_hcs_id	 = min(hcs_id)
	from	housing_arm_hcs_ids
	where	activeflag <> 0

if @default_hcs_id is null
	select	@default_hcs_id	 = min(hcs_id)
	from	housing_arm_hcs_ids

if @default_hcs_id is null
	select	@default_hcs_id = 0
	
update	#ids
set		hud_hcs_id = @default_hcs_id
where	hud_hcs_id is null
or		hud_hcs_id not in (select hcs_id from housing_arm_hcs_ids where activeflag <> 0)

-- Find the appropriate office to use for address information
select	hud_hcs_id, min(office) as office
into	#ids2
from	#ids
where	ActiveFlag <> 0
group by hud_hcs_id

update	#results
set		office = b.office
from	#results a
inner join #ids2 b on a.agc_hcs_id = b.hud_hcs_id

-- Always choose the default office over others in the list
update	#results
set		office = o.office
from	#results r
inner join offices o on o.hud_hcs_id = r.agc_hcs_id
where	o.[activeflag] <> 0
and		o.[default] <> 0;

-- Do the same thing but without the ActiveFlag test
select	hud_hcs_id, min(office) as office
into	#ids3
from	#ids
where	ActiveFlag = 0
group by hud_hcs_id

update	#results
set		office = b.office
from	#results a
inner join #ids3 b on a.agc_hcs_id = b.hud_hcs_id
where   a.office is null

-- Always choose the default office over others in the list
update	#results
set		office = o.office
from	#results r
inner join offices o on o.hud_hcs_id = r.agc_hcs_id
where	o.[activeflag] = 0
and		o.[default] <> 0
and     r.office is null;

drop table #ids
drop table #ids2
drop table #ids3

update	#results
set		[default]					= o.[default],
		[activeflag]				= o.[activeflag],
		[agc_faith_based_ind]		= o.faith_based,
		[agc_colonias_ind]			= o.colonias,
		[agc_migrfarm_worker_ind]	= o.migrant_farm_worker,
		[agc_counselin_budget_amt]  = o.counseling_amount
from	#results r
inner join housing_arm_hcs_ids o on r.agc_hcs_id = o.hcs_id

-- Finally, choose the default office if there is no corresponding item for the HCS id
update	#results
set		office = (select min(office) from offices where [activeflag] <> 0 and [default] <> 0)
where	office is null

-- Set the office information from the offices table
update	#results
set		office_addressid		= o.addressid,
		office_emailid			= o.emailid,
		office_telephoneid		= o.telephoneid,
		office_alttelephoneid	= o.alttelephoneid,
		office_faxid			= o.faxid
from	#results r
inner join offices o on r.office = o.office

-- Find the information from the system configuration file
declare		@cnf_name				varchar(80)
declare		@cnf_telephoneid		int
declare		@cnf_alt_telephoneid	int
declare		@cnf_address			int
declare		@cnf_alt_address		int
declare		@cnf_faxid				int
declare		@cnf_ein_number			varchar(80)
declare		@cnf_dun_nbr			varchar(80)
declare		@cnf_web_site			varchar(100)

select		@cnf_name				= name,
			@cnf_telephoneid		= TelephoneID,
			@cnf_alt_telephoneid	= AltTelephoneID,
			@cnf_address			= AddressID,
			@cnf_alt_address		= AltAddressID,
			@cnf_faxid				= FAXID,
			@cnf_ein_number			= fed_tax_id,
			@cnf_dun_nbr			= dun_nbr,
			@cnf_web_site			= web_site
from		config

-- Correct the website address
if ltrim(rtrim(isnull(@cnf_web_site,''))) = ''
	select	@cnf_web_site = ''
if @cnf_web_site like 'https://%' and @cnf_web_site not like 'http://%'
	select	@cnf_web_site	= 'http://' + @cnf_web_site

-- Correct the federal tax ID number
if ltrim(rtrim(isnull(@cnf_ein_number,''))) = ''
	select	@cnf_ein_number	= '00-0000000'
select	@cnf_ein_number	= dbo.numbers_only ( @cnf_ein_number )
select	@cnf_ein_number	= right('000000000' + @cnf_ein_number, 9)
select	@cnf_ein_number	= left(@cnf_ein_number,2) + '-' + right(@cnf_ein_number, 7)

-- Correct the dunn-bradstreet number
select	@cnf_dun_nbr	= dbo.numbers_only ( isnull(@cnf_dun_nbr, '') )
select	@cnf_dun_nbr	= right('000000000' + @cnf_dun_nbr, 9)

-- Set additional fields to control what is transmitted to HUD for each ID
update	#results
set		Send9902				= 1,		-- Send summary 9902 data
		SendCounselors			= 1,		-- Send counselor data (always sent if clients are sent)
		SendClients				= 1,		-- Send clients
		SendAttendees			= 1,		-- Send workshop attendees
		SendClientSSN			= 1,		-- Not used since it is controlled by the client_select procedure
		SendClientCount			= 1			-- Not used

-- Set the email addresses for the offices
update	#results
set		agc_email		= e.address
from	#results r
inner join emailaddresses e on e.email = r.office_emailID
where	ltrim(rtrim(isnull(e.address,''))) <> ''

-- Supply the other defaults from the config entries
update	#results set agc_web_site          = @cnf_web_site
update	#results set office_faxid          = @cnf_faxid where office_faxid is null;
update	#results set office_telephoneid    = @cnf_telephoneid where office_telephoneid is null;
update	#results set office_alttelephoneid = @cnf_alt_telephoneid where office_alttelephoneid is null;

-- Convert the items from the telephone numbers
update	#results
set		agc_phone_nbr = dbo.format_hud_9902_telephonenumber ( office_telephoneid ),
		alternate_phone_nbr = dbo.format_hud_9902_telephonenumber ( office_alttelephoneid ),
		agc_fax_nbr = dbo.format_hud_9902_telephonenumber ( office_faxid ),
		agc_web_site = @cnf_web_site,
		agc_name	 = @cnf_name,
		agc_cms_type = 29,				-- 29 = DebtPlus. Not used. Moved to config file.
		agc_ein		 = @cnf_ein_number,
		agc_dun_nbr  = @cnf_dun_nbr
		
-- Set the address information
update	#results
set		agc_address1	= dbo.format_address_line_1 ( a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value ),
 		agc_address2	= a.address_line_2,
		agc_address3	= a.address_line_3,
		agc_city		= a.city,
		agc_state		= dbo.map_hud_9902_state ( a.state ),
		agc_zip		    = dbo.format_hud_9902_postalcode ( a.postalcode )
from	#results r
inner join addresses a on r.office_addressid = a.address

-- Supply suitable defaults for the fields that need values
update	#results set agc_name            = '-' where ltrim(rtrim(isnull(agc_name,''))) = ''
update	#results set agc_address1        = '-' where ltrim(rtrim(isnull(agc_address1,''))) = ''
update	#results set agc_address2        = '-' where ltrim(rtrim(isnull(agc_address2,''))) = ''
update	#results set agc_address3        = '-' where ltrim(rtrim(isnull(agc_address3,''))) = ''
update	#results set agc_address4        = '-' where ltrim(rtrim(isnull(agc_address4,''))) = ''
update	#results set agc_city            = '-' where ltrim(rtrim(isnull(agc_city,''))) = ''
update	#results set agc_state	         = 61 where isnull(agc_state,0) <= 0
update	#results set agc_zip	         = '00000' where ltrim(rtrim(isnull(agc_zip,''))) = ''
update	#results set alternate_phone_nbr = '000-000-0000' where ltrim(rtrim(isnull(alternate_phone_nbr,''))) = ''
update	#results set agc_phone_nbr       = '000-000-0000' where ltrim(rtrim(isnull(agc_phone_nbr,''))) = ''
update	#results set agc_fax_nbr         = '000-000-0000' where ltrim(rtrim(isnull(agc_fax_nbr,''))) = ''
update	#results set agc_ein             = '00-0000000'   where ltrim(rtrim(isnull(agc_ein,''))) = ''
update	#results set agc_dun_nbr         = '000000000'   where ltrim(rtrim(isnull(agc_dun_nbr,''))) = ''

-- Allow for the last result to go through
set nocount off

-- Return the list of agenices to report upon for the HUD 9902 reports
select		office,
			agc_name,
			agc_cms_type,
			agc_ein,
			agc_dun_nbr,
			agc_address1,
			agc_address2,
			agc_address3,
			agc_address4,
			agc_city,
			agc_state,
			agc_zip,
			agc_web_site,
			agc_phone_nbr,
			alternate_phone_nbr,
			agc_fax_nbr,
			agc_email,
			agc_faith_based_ind,
			agc_colonias_ind,
			agc_migrfarm_worker_ind,
			agc_counselin_budget_amt,
			Send9902,
			SendCounselors,
			SendClients,
			SendAttendees,
			SendClientSSN,
			SendClientCount,

			UserName,
			Password,
			agc_hcs_id,

--			convert(varchar(100),'MX3525') as UserName,
--			convert(varchar(100),'Passwd09') as Password,
--			convert(int,83525) as agc_hcs_id,

			Description,
			[Default],
			[ActiveFlag]
from		#results
order by description, agc_hcs_id, office

-- Discard working table and exit
drop table #results
return ( @@rowcount )
GO
