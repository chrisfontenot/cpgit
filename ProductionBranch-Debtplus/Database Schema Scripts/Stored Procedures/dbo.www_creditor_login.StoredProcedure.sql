USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_login]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_creditor_login] ( @creditor as typ_creditor, @password as varchar(32) = NULL ) AS
-- =========================================================================================================
-- ==            Return the list of creditors which are acceptable for this login                         ==
-- =========================================================================================================

-- Ensure that the extra recordsets are not generated
set nocount on

-- Determine if the item is valid
if not exists (select * from creditor_www where creditor = @creditor and password = @password)
begin
	RaisError ('The account or password is not valid.', 16, 1)
	return ( 0 )
end

-- Retrieve the creditors in the list
/*
select	creditor as 'item_key', creditor, creditor_name, division
from	creditors
where	creditor = @creditor
*/
execute xpr_creditor_addresses @creditor, 'P'

return ( 1 )
GO
