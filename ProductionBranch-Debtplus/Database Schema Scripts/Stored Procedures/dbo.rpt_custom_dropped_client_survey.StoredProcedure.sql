USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_dropped_client_survey]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_custom_dropped_client_survey] ( @from_date datetime = null, @to_date datetime = null ) as

-- ================================================================================================
-- ==     custom report to show email addresses for dropped clients                              ==
-- ================================================================================================

set nocount on

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

select	@from_date	= convert(varchar(10), @from_date, 101),
	@to_date	= convert(varchar(10), @to_date, 101) + ' 23:59:59'

select	c.client			as 'client',
	dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)			as 'name',
	pe.address				as 'email',
	c.drop_date,
	pn.first as first_name,
	pn.last as last_name
from	clients c with (nolock)
left outer join people p with (nolock) on c.client = p.client and 1 = p.relation
left outer join Names pn with (nolock) on p.NameID = pn.name
left outer join EmailAddresses pe with (nolock) on p.EmailID = pe.Email
where	c.active_status = 'I' and c.drop_date between @from_date and @to_date
order by 1

return ( @@rowcount )
GO
