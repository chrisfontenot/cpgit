USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_event_update]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_debt_event_update] ( @client_creditor_event as int = null, @client_creditor as int, @effective_date as datetime, @config_fee as int, @value as money ) as

-- ===================================================================================================================
-- ==            Update or create the event for the debt record                                                     ==
-- ===================================================================================================================

set nocount on

declare	@fee_type		int
declare	@config_fee_default	int
declare	@paf_creditor		varchar(10)

if @client_creditor_event is null
	select @client_creditor_event = 0

if @client_creditor_event <= 0
begin
	if @effective_date < getdate()
	begin
		RaisError ('The effective date must be greater than today', 16, 1)
		rollback transaction
		return ( 0 )
	end

	if @client_creditor <= 0
	begin
		RaisError ('The debt record could not be determined during the creation event', 16, 1)
		rollback transaction
		return ( 0 )
	end

	declare	@creditor	varchar(10)
	select	@creditor = creditor
	from	client_creditor
	where	client_creditor = @client_creditor

	if @@rowcount < 1
	begin
		RaisError ('The debt record could not be determined during the creation event', 16, 1)
		rollback transaction
		return ( 0 )
	end

	select	@paf_creditor		= paf_creditor
	from	config

	if @paf_creditor = @creditor
	begin
		if @config_fee is null
			select	@config_fee	= min(config_fee)
			from	config_fees
			where	[default] = 1
			
		if @config_fee is null
			select	@config_fee	= min(config_fee)
			from	config_fees
			
		if @config_fee is null
			select	@config_fee	= 1

		select	@fee_type = fee_type
		from	config_fees
		where	config_fee	= @config_fee

		if @@rowcount < 1
		begin
			RaisError ('The config fee for the client is not valid', 16, 1)
			rollback transaction
			return ( 0 )
		end

		if @fee_type = 1 and @value = 0
		begin
			RaisError ('The P.A.F. fee is fixed and may no be set to $0.00. Please enter a non-zero value for the new fee.', 16, 1)
			rollback transaction
			return ( 0 )
		end
	end

	-- Create the new record
	insert into client_creditor_events (client_creditor, effective_date, config_fee, [value])
	values ( @client_creditor, @effective_date, @config_fee, @value )

	select	@client_creditor_event = SCOPE_IDENTITY()

end else begin

	-- Complain if the record has been "used"
	if exists (select * from client_creditor_events where date_updated is not null and client_creditor_event = @client_creditor_event)
	begin
		RaisError ('The record has been updated in the database and may not be edited.', 16, 1)
		rollback transaction
		return ( 0 )
	end

	update	client_creditor_events
	set		effective_date = @effective_date,
			config_fee	= @config_fee,
			[value]		= @value
	where	client_creditor_event = @client_creditor_event

	if @@rowcount < 1
	begin
		RaisError ('The event for the debt record could not be updated because it no longer exists', 16, 1)
		rollback transaction
		return ( 0 )
	end
end

return ( @client_creditor_event )
GO
