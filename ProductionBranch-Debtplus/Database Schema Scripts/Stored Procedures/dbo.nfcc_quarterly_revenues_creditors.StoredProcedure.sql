USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfcc_quarterly_revenues_creditors]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[nfcc_quarterly_revenues_creditors] ( @from_date as datetime = null, @to_date as datetime = null ) as

-- This is the name of the resulting sheet in the spreadsheet output
declare	@SheetName	varchar(80)
select	@SheetName	= 'Revenues - Creditors'

-- Find last quarter if required
if (@to_date is null) and (@from_date is null)
begin
	select	@from_date	= min(dt) from calendar where y = datepart(year, getdate()) and q = (select q from calendar where dt = convert(varchar(10), getdate(), 101))
	select	@from_date	= dateadd(m, -3, @from_date)
	select	@to_date	= max(dt) from calendar where y = datepart(year, @from_date) and q = (select q from calendar where dt = convert(varchar(10), @from_date, 101))
end

select	@from_date	= convert(varchar(10), @from_date, 101) + ' 00:00:00',
	@to_date	= convert(varchar(10), @to_date, 101)   + ' 23:59:59';

-- Build a map of the creditors to determine where to place the answers
select	[type], creditor, creditor_id, 19 as line, convert(money,0) as deduct, convert(money,0) as billed, convert(money,0) as non_dmp_revenues, convert(money,0) as disbursements
into	#map_creditors
from	creditors with (nolock);

create index temp_ix_map_creditors_1 on #map_creditors ( creditor_id );
create index temp_ix_map_creditors_2 on #map_creditors ( creditor );

-- Default medical creditors to line 19
update	#map_creditors
set	line	= 18
from	#map_creditors x
inner join creditor_types t on x.type = t.type
where	t.description like '%medical%';

-- For the specific creditors, update their locations. Change fairshare_line to C if want to show in grant column.
update	#map_creditors
set	line	= 8	-- American Express
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('6300019401','6300019402')

update	#map_creditors
set	line	= 9	-- B of A
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('3000000003','9061799597','9000000003','9000000004','0311001609','0311001607','0311001610','0311001608','0311001606','0311001617',
'0311001622','0311001623','0311001624')

update	#map_creditors
set	line		= 10	--Capital One
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0801000002','2020622581','0702090004')

update	#map_creditors
set	line		= 11	--Chase
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('1130000057','2125280001','4226000001',
'5200000003','5200000002','0092236480','0090753463','9133357362','0006609200')

update	#map_creditors
set	line		= 12	--Citigroup
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('9000000034','0090893755','0090893751',
'0090893767','0090813817','0090056681','0090056682','0090749154','0090056670','0090056667',
'0090893758','0090056672','0090893768','0090893760','0090056669','0090186655','0090529674',
'0090893757','0090825595','0090893765','0090862303','0090770337','0090926339','0090875615',
'0090002442','0090880254','0090001004','0090893764','0090474092','0090172290',
'0090827123','0090827842','0090827601','0090827792','0090451690','0090449157','0090452512',
'0088539007','0090004936','0090036384','0090264344','0090264439','0090264517','0090264758',
'0090264928','0090426822','0090551806','0090563121','0090617986','0090785651','0090948569',
'0090991466','0091003128','0091412279','0092075436','0092585680','0092648392','0092693767',
'0092693786','0090389940','7000000002','7000000003','7000000009','7000000004','7000000006',
'7000000007','7000000008','7000000010','7000000011','0090005004','0090892820','0091130670',
'0090615372','0090611584','0090327249','0090233908','0090990027','0091019364','0090005921',
'1500000009','0090710444','0090710837','0091138408','0091128682','0077584001','0077584002',
'0077584003','0077584004','2000060601','2000060602','2000060603','2000060603','3188500001',
'7100000001','7200000002','7300000003','7400000004','7600000006','7700000008','7751800009',
'7900000009','9030799296','9500070399','9500071109','9500077999','9734697019','9902847009',
'0091170715','0090618155','0090613103','0090427095')

update	#map_creditors
set	line		= 13	--Discover
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0090074123','0090074124')

update	#map_creditors
set	line		= 14	--GE Capital
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0090000297','0090005013','0090005074','0090005077','0090005078',
'0090005079','0090005086','0090005090','0090005091','0090005280','0090006001','0090185019','0090185040',
'0090018971','0090520030','0090520050','0090520060','0090520090','0090604412','0090635646',
'0090888281','0090948570','0090464597','0090047180','0090036240','0090524933','0090830642','0090888697',
'0090838951','0090673414','0090587931','0090790280','0090148384','0090059000','0090948568','0090165176',
'0090584381','0090439905','0090023525','0980001001','1030000350','1030000351','8138821221','8410215170',
'0090480617','0090057713','0090658378','0090005043','0090018951','0090185016','0090750274','0090673411',
'0090005003','0090005011','0090185042','0090185041')

update	#map_creditors
set	line		= 15	--HSBC
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0001510000','0001540000','0001560000','0001650000','0001670000','0001680000',
'0001690000','0001710000','0001860000','0002030000','0002220000','0002230000','0002290000','0002330000',
'0002340000','0002350000','0002420000','0002440000','0002450000','0002456110','0002490000','0002550000',
'0002640000','0002750000','0002950000','0002960000','0003050000','0003100000','0003110000','0003420000',
'0003700000','0003710000','0004000000','0004060000','0004130000','0004220000','0004260000','0004310000',
'0004650000','0004990000','0005200000','0005310000','0005500000','0005890000','0005930000','0006070000',
'0007100000','0007200000','0007610000','0007670000','0007750000','0008010000','0008020000','0009910920',
'0009920020','0009920030','0009920040','0009920060','0009920100','0009920130','0009920140','0009920170',
'0009920180','0009920360','0009920370','0009920380','0009930010','0009930020','0009930030','0009930040',
'0009930050','0009930060','0009930070','0009930080','0009930090','0009930110','0009930150','0009930220',
'0009930260','0009930390','0009930400','0009930420','0009930430','0009930440','0009967000','0078599004',
'0078599012','0078599014','2000040001','3429901001','9051999675','9051999687','9051999776','9051999796',
'4000040001')

update	#map_creditors
set	line		= 16	-- Wells Fargo
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('2363464672','730002286','730002287','730002284',
'2121526107','4121380780','2896462180')

update	#map_creditors
set	line		= 17	--US Bank	
from	#map_creditors m
inner join creditor_methods met on m.creditor_id = met.creditor
inner join banks b on met.bank = b.bank
where	b.type	= 'R'
and	met.rpps_biller_id in ('0000473522')

-- Find the disbursements in the period
select	x.creditor,
	x.line,
	sum(isnull(rcc.debit_amt,0) - isnull(rcc.credit_amt,0)) as disbursements,
	sum (case when rcc.creditor_type <> 'D' then 0
	          when rcc.tran_type in ('AD','BW','MD','CM') then fairshare_amt
	          else 0 - rcc.fairshare_amt end) as deduct
into	#disbursements
from	registers_client_creditor rcc with (nolock)
inner join #map_creditors x on rcc.creditor = x.creditor
where	rcc.tran_type in ('AD','BW','MD','CM','RR','VD','RF')
and	rcc.date_created between @from_date and @to_date
group by x.creditor, x.line;

update	#map_creditors
set	disbursements	= d.disbursements,
	deduct		= d.deduct
from	#map_creditors m
inner join #disbursements d on m.creditor = d.creditor

drop table #disbursements

-- Find the amount received on invoices
select	creditor, sum(isnull(credit_amt,0)) as billed
into	#billed
from	registers_creditor rc with (nolock)
where	rc.tran_type = 'RC'
and	rc.date_created between @from_date and @to_date
group by creditor;

update	#map_creditors
set	billed		= d.billed
from	#map_creditors m
inner join #billed d on m.creditor = d.creditor

drop table #billed

-- Add the non-ar transactions to the list
select	creditor, sum(credit_amt) as non_dmp_revenues
into	#registers_non_ar
from	registers_non_ar
where	tran_type = 'NA'
and	date_created between @from_date and @to_date
and	creditor is not null
group by creditor;

update	#map_creditors
set	non_dmp_revenues = x.non_dmp_revenues
from	#map_creditors m
inner join #registers_non_ar x on m.creditor = x.creditor;

drop table #registers_non_ar

-- Merge the information together to form the columns and lines
select	line, sum(deduct + billed) as fairshare, sum(non_dmp_revenues) as non_dmp_revenues, sum(disbursements) as disbursements
into	#map_creditors_2
from	#map_creditors
group by line;

-- Build a list of the disbursement information
create table #results (sheet varchar(80), location varchar(80), value varchar(80))

insert into #results (sheet, location, value)
select	@SheetName, 'B' + convert(varchar, line) as location, convert(varchar, fairshare) as value
from	#map_creditors_2;

insert into #results (sheet, location, value)
select	@SheetName, 'C' + convert(varchar, line) as location, convert(varchar, non_dmp_revenues) as value
from	#map_creditors_2;

insert into #results (sheet, location, value)
select	@SheetName, 'D' + convert(varchar, line) as location, convert(varchar, disbursements) as value
from	#map_creditors_2;

drop table #map_creditors_2;
drop table #map_creditors;

-- Return the results
select * from #results where value is not null order by location;
drop table #results;
GO
