USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_InvoiceStatus]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_InvoiceStatus] ( @FromDate as DateTime = NULL, @ToDate as DateTime = NULL, @ShowAll as INT = 0 ) AS

-- =================================================================================================
-- ==                   Invoice Status Report                                                     ==
-- =================================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class

-- Suppress intermediate results
set nocount on

-- Correct the date ranges
IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

IF @ShowAll <> 0

	-- Retrieve the information from the database
	SELECT		i.invoice_register,
			i.inv_date,
			i.inv_amount,
			i.adj_date,
			i.adj_amount,
			i.pmt_date,
			i.pmt_amount,
			isnull(i.inv_amount,0) - isnull(i.adj_amount,0) - isnull(i.pmt_amount,0) as 'balance',

			i.date_created as 'generated',
			i.date_printed as 'printed',

			i.creditor,
			cr.creditor_name,

			case cr.contrib_cycle
				when 'N' then 'None'
				when 'W' then 'Weekly'
				when 'M' then 'Monthly'
				when 'B' then 'Bi-Monthly'
				when 'Q' then 'Quarterly'
				when 'S' then 'Semi-Annually'
				when 'A' then 'Annually'
			end as contrib_cycle
	FROM		registers_invoices i with (nolock)
	LEFT OUTER JOIN	creditors cr with (nolock) ON i.creditor = cr.creditor
	WHERE		inv_date BETWEEN @FromDate AND @ToDate
	ORDER BY	cr.type, cr.creditor_id, i.creditor, i.invoice_register

ELSE

	-- Retrieve the information from the database
	SELECT		i.invoice_register,
			i.inv_date,
			i.inv_amount,
			i.adj_date,
			i.adj_amount,
			i.pmt_date,
			i.pmt_amount,
			isnull(i.inv_amount,0) - isnull(i.adj_amount,0) - isnull(i.pmt_amount,0) as 'balance',

			i.date_created as 'generated',
			i.date_printed as 'printed',

			i.creditor,
			cr.creditor_name,

			case cr.contrib_cycle
				when 'N' then 'None'
				when 'W' then 'Weekly'
				when 'M' then 'Monthly'
				when 'B' then 'Bi-Monthly'
				when 'Q' then 'Quarterly'
				when 'S' then 'Semi-Annually'
				when 'A' then 'Annually'
			end as contrib_cycle
	FROM		registers_invoices i with (nolock)
	LEFT OUTER JOIN	creditors cr with (nolock) ON i.creditor = cr.creditor
	WHERE		inv_date BETWEEN @FromDate AND @ToDate
	AND		isnull(inv_amount,0) > isnull(pmt_amount,0) + isnull(adj_amount,0)
	ORDER BY	cr.type, cr.creditor_id, i.creditor, i.invoice_register

RETURN ( @@rowcount )
GO
