USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_custom_budget_surplus_deficit_byclient]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_custom_budget_surplus_deficit_byclient] ( @client as int ) as

-- Custom routine to return the surplus/deficit funds for a client. Net Income, plus other income less
-- budget, less dmp payments

declare	@budget_id	int
select	@budget_id = dbo.map_client_to_budget ( @client );

declare	@budget_amount	money
if @budget_id is not null
	select	@budget_amount	= sum(suggested_amount)
	from	budget_detail with (nolock)
	where	budget		= @budget_id;

if @budget_amount is null
	select	@budget_amount	= 0

declare	@net_income	money
select	@net_income	= sum(net_income)
from	people with (nolock)
where	client		= @client

if @net_income is null
	select	@net_income	= 0

declare	@disbursement_factor	money
-- Limit the disbursement factors to the balance of the debts to eliminate "paid off debts"
select	@disbursement_factor	= sum(case when isnull(ccl.zero_balance,1) > 0 then disbursement_factor
					   when b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments > cc.disbursement_factor then cc.disbursement_factor
					   else b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments
				      end)
from	client_creditor cc with (nolock)
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and cc.client_creditor = b.client_creditor
left outer join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
where	cc.reassigned_debt	= 0
and	cc.client		= @client

if @disbursement_factor is null
	select	@disbursement_factor	= 0

select	@net_income						as net_income,
	@budget_amount						as budget_amount,
	@disbursement_factor					as dmp_payments,
	@net_income - @budget_amount - @disbursement_factor	as balance

return ( 1 )
GO
