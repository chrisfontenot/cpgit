USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_client_ticklers]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_client_ticklers]  ( @client as int = NULL, @show_all as int = 0 ) AS
-- ==========================================================================================================
-- ==            List the tickler items for the client                                                     ==
-- ==========================================================================================================

set nocount on

if @show_all = 0
begin

	-- Find the items that are pending for this client
	select		x.tickler		as 'tickler',		-- 1 Primary key 
			dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor',		-- 2 Counselor ID
			t.description		as 'description',	-- 3 Description of the event
			x.date_effective	as 'date_effective',	-- 4 Effective date
			x.date_created		as 'date_created',	-- 5 Date item was created
			x.created_by		as 'created_by',	-- 6 Person who created the item
			x.priority			as 'priority'		-- 7 Priority of the item

	from		ticklers x with (nolock)
	left outer join TicklerTypes t on x.tickler_type = t.oID
	left outer join	counselors co with (nolock) on x.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name
	where	date_deleted is null
	AND		client = @client
	order by	7, 4, 2 -- priority, date_effective, counselor

end else begin

	-- Find the items that are pending for this client
	select	x.tickler			as 'tickler',		-- Primary key
			dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor',		-- 2 Counselor ID
			t.description		as 'description',	-- Description of the event
			x.date_effective	as 'date_effective',	-- Effective date
			x.date_created		as 'date_created',	-- Date item was created
			x.created_by		as 'created_by',	-- Person who created the item
			x.priority		as 'priority'		-- Priority of the item

	from		ticklers x with (nolock)
	left outer join TicklerTypes t on x.tickler_type = t.oID
	left outer join	counselors co with (nolock) on x.counselor = co.counselor
	left outer join names cox with (nolock) on co.NameID = cox.name
	where		client = @client
	order by	7, 4, 2 -- priority, date_effective, counselor
end

return ( @@rowcount )
GO
