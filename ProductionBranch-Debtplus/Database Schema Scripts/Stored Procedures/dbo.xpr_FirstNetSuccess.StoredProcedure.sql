USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_FirstNetSuccess]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[xpr_FirstNetSuccess] ( @BatchID as UniqueIdentifier, @Status as varchar(80) = null, @Client as varchar(80) = null ) as
-- =================================================================================================================
-- ==         Process sucessful merge status for clients                                                          ==
-- =================================================================================================================
	set nocount on

	-- Convert the client to an integer value
	declare @clientID		int
	if isnumeric(@Client) <> 0
		select @clientID = convert(int, @client)

	-- If the client is still missing, make it negative
	if @clientID is null
		select @clientID = -1

	-- For valid clients, generate the status message.
	if @client > 0
	begin
		insert into client_notes (client, type, subject, dont_print, dont_edit, dont_delete, note)
		select client, 1, 'ACH set up confirmed', 0, 1, 1, isnull('Status: ' + @Status,'') from clients where client = @ClientID
	end			

	return ( 0 )
GO
