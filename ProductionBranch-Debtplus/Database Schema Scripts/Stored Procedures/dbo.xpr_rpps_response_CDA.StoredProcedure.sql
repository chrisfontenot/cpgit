USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_CDA]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_CDA] ( @response_file as int, @trace_number as varchar(15), @biller_id as varchar(10), @return_code as varchar(3), @name as varchar(5) = null, @account_number as varchar(22) = null, @net as money = 0, @client as int, @ssn as varchar(9) = null, @company_identification as varchar(10) = null, @last_communication as varchar(15) = null, @balance as money = 0.0, @apr as float = 0.0, @payment_amount as money = null, @start_date as datetime = null ) as

-- =======================================================================================================================
-- ==            Generate the response for a FBD request                                                                ==
-- =======================================================================================================================

-- ChangeLog
--   5/11/2012
--      Added "code cleanup" fields from Mastercard

-- Generate the base response row
declare	@rpps_response_detail	int
execute @rpps_response_detail = xpr_rpps_response @response_file, @trace_number, @biller_id, @return_code, 'CDA', @name, @account_number, @net

-- Insert the addendum information for proposal acceptances
if @rpps_response_detail > 0
begin
	-- Convert the SSN field
	if @ssn is not null
		select @ssn = ltrim(rtrim(@ssn))

	if @ssn not like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		select @ssn = null

	-- Convert the last communication field
	if @last_communication is not null
		select @last_communication = ltrim(rtrim(@last_communication))

	if @last_communication = ''
		select @last_communication = null

	-- Convert the company identification field
	if @company_identification is not null
		select @company_identification = ltrim(rtrim(@company_identification))

	if @company_identification = ''
		select @company_identification = null

	-- Update the addendum information for a rejected proposal
	insert into rpps_response_details_cda ([rpps_response_detail], [last_communication], [company_identification], [ssn], [client], [balance], [apr], [payment_amount], [start_date])
	values (@rpps_response_detail, @last_communication, @company_identification, @ssn, @client, @balance, @apr, @payment_amount, @start_date)
end

return ( @rpps_response_detail )
GO
