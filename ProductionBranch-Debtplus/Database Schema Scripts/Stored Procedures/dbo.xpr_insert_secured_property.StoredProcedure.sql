USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_secured_property]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_insert_secured_property] ( @client as typ_client,@class AS INT = 0,@secured_type AS INT = 0,@school_name AS VARCHAR(50) = '',@studentLoan_type_id AS INT = 0,@general_outcome_id AS INT = 0,@type_of_workout_id AS INT = 0,@borrower_id AS INT = 0, @description AS VARCHAR(80) = '', @sub_description AS VARCHAR(80) = '', @original_price AS MONEY = 0, @current_value AS MONEY = 0, @year_acquired AS INT = 0, @year_mfg AS INT = 0, @housing_type AS INT = 0, @primary_residence AS BIT = 0 ) AS
-- ===================================================================================================================
-- ==         Insert a row into the secured_properties table                                                        ==
-- ===================================================================================================================
SET NOCOUNT ON

insert into secured_properties ( [client],[class],[secured_type],[school_name],[studentLoan_type_id],[general_outcome_id],[type_of_workout_id],[borrower_id],[description],[sub_description],[original_price],[current_value],[year_acquired],[year_mfg],[housing_type],[primary_residence] ) VALUES ( @client,@class,@secured_type,@school_name,@studentLoan_type_id,@general_outcome_id,@type_of_workout_id,@borrower_id,@description,@sub_description,@original_price,@current_value,@year_acquired,@year_mfg,@housing_type,@primary_residence )
return ( scope_identity() )
GO
