USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_housing_custom_events]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_housing_custom_events] ( @hud_interview as int ) as
-- ========================================================================================
-- ==             Do special processing when housing events are created                  ==
-- ========================================================================================

-- Find the client information
declare	@client					int
declare	@date_created			datetime
select	@client					= client,
		@date_created			= interview_date
from	hud_interviews with (nolock)
where	hud_interview			= @hud_interview;

-- Terminate if there is no client
if @client is null
	return ( 1 )

declare	@retention_event		int	
declare @description			varchar(50)
declare @priority				int
declare	@expire_type			int
declare @event_followup_period	int

-- Event 128 is 45 days
select	@retention_event		= retention_event,
		@description			= '45 Day Followup Housing Event',
		@priority				= priority,
		@expire_type			= expire_type,
		@event_followup_period	= 45
from	retention_events with (nolock)
where	retention_event			= 128;

-- Create event 128 for 45 days out
if @retention_event IS NOT NULL
	insert into client_retention_events (client, retention_event, amount, message, expire_type, expire_date, priority, effective_date, hud_interview)
	select @client, @retention_event, 0, @description, @expire_type, dateadd(d, @event_followup_period + 30, @date_created), @priority, dateadd(d, @event_followup_period, @date_created), @hud_interview

-- Event 129 is 60 days
select	@retention_event		= retention_event,
		@description			= '60 Day Followup Housing Event',
		@priority				= priority,
		@expire_type			= expire_type,
		@event_followup_period	= 60
from	retention_events with (nolock)
where	retention_event			= 129;

-- Create event 129 for 60 days out
if @retention_event IS NOT NULL
	insert into client_retention_events (client, retention_event, amount, message, expire_type, expire_date, priority, effective_date, hud_interview)
	select @client, @retention_event, 0, @description, @expire_type, dateadd(d, @event_followup_period + 30, @date_created), @priority, dateadd(d, @event_followup_period, @date_created), @hud_interview

-- Event 130 is 90 days
select	@retention_event		= retention_event,
		@description			= '90 Day Followup Housing Event',
		@priority				= priority,
		@expire_type			= expire_type,
		@event_followup_period	= 90
from	retention_events with (nolock)
where	retention_event			= 130;

-- Create event 130 for 90 days out
if @retention_event IS NOT NULL
	insert into client_retention_events (client, retention_event, amount, message, expire_type, expire_date, priority, effective_date, hud_interview)
	select @client, @retention_event, 0, @description, @expire_type, dateadd(d, @event_followup_period + 30, @date_created), @priority, dateadd(d, @event_followup_period, @date_created), @hud_interview

-- Return success to the caller
return ( 0 )
GO
