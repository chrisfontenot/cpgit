USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_housing_training_course]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[xpr_insert_housing_training_course] (@TrainingTitle as varchar(50) = null,@TrainingDuration as int = null,@TrainingOrganization as varchar(10) = null,@TrainingOrganizationOther as varchar(50) = null,@TrainingSponsor as varchar(10) = null,@TrainingSponsorOther as varchar(50) = null,@Certificate as bit = 0,@ActiveFlag as bit = 1) as
-- ===============================================================================================
-- ==           Insert a row into the housing_training_courses table                            == 
-- ===============================================================================================
insert into housing_training_courses ([TrainingTitle],[TrainingDuration],[TrainingOrganization],[TrainingOrganizationOther],[TrainingSponsor],[TrainingSponsorOther],[Certificate],[ActiveFlag]) values (@TrainingTitle,@TrainingDuration,@TrainingOrganization,@TrainingOrganizationOther,@TrainingSponsor,@TrainingSponsorOther,isnull(@Certificate,0),isnull(@ActiveFlag,1))
return (scope_identity())
GO
