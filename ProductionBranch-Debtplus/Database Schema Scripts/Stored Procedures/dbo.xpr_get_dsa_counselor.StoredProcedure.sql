SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_get_dsa_counselor](
	@program int,
	@language int
)
as
begin
select top(1) c.counselor as CounselorID, n.First + ' ' + n.Last + case when t.Ext is null then ' ' else 'x' end + isnull(t.Ext, '') as 'Counselor', c.CaseCounter
		from (select	ca.counselor 
						, max(case when at.grouping = 'DSAPARTNER' and at.oID = 301 then 301 else null end) as FreddieMac
						, max(case when at.grouping = 'DSAPARTNER' and at.oID = 302 then 302 else null end) as FannieMaeMhn
						, max(case when at.grouping = 'DSAPARTNER' and at.oID = 303 then 303 else null end) as FannieMaeHpf
						, max(case when at.grouping = 'DSAPARTNER' and at.oID = 304 then 304 else null end) as WellsFargo
						, max(case when at.grouping = 'DSAPARTNER' and at.oID = 305 then 305 else null end) as HPFFMAC
						, max(case when at.grouping = 'LANGUAGE' and at.oID = 1 then 1 else null end) as English
						, max(case when at.grouping = 'LANGUAGE' and at.oID = 2 then 2 else null end) as Spanish
						, max(case when at.grouping = 'ROLE' and at.oID = 104 then 1 else 0 end) as IsDSA
				from counselor_attributes as ca 
				left outer join AttributeTypes as at on ca.Attribute = at.oID
				group by ca.Counselor) as dc
		join counselors as c on c.Counselor = dc.Counselor
		join names as n on c.NameID = n.Name
		left outer join TelephoneNumbers as t on c.TelephoneID = t.TelephoneNumber
	where c.ActiveFlag = 1 and (dc.FreddieMac = @program or dc.FannieMaeMhn = @program or dc.FannieMaeHpf = @program or dc.WellsFargo = @program or dc.HPFFMAC = @program ) and (dc.English = @language or dc.Spanish = @language) and dc.IsDSA = 1
	order by c.CaseCounter, c.counselor

end
GO
