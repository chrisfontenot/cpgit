USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_counties]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_counties] ( @state as int = 0,@name as varchar(50),@median_income as money = 0,@bankruptcy_district as int = 1,@default as bit = 0,@ActiveFlag as bit = 1,@fips as varchar(50) = '' ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the counties table                       ==
-- ========================================================================================
	insert into counties ( [state],[name],[median_income],[bankruptcy_district],[default],[ActiveFlag],[fips] ) values ( @state,@name,@median_income,@bankruptcy_district,@default,@ActiveFlag,@fips )
	return ( scope_identity() )
GO
