USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_workshop_pending_clients]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_workshop_pending_clients] ( @workshop as int ) as

-- ==================================================================================================
-- ==            Return the information for the workshop client list                               ==
-- ==================================================================================================

select	a.client_appointment			as 'item_key',
		a.client						as 'client',
		dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as 'name',
		a.workshop_people				as 'people_attending'
from	client_appointments a with (nolock)
left outer join people p on a.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.NameID = pn.Name
where	workshop = @workshop
and		workshop is not null
and		status = 'P'

-- Return the second recordset with the heading information for the workshop
select	wt.workshop_type				as 'workshop_type',
		wt.description					as 'description',
		w.start_time					as 'start_time',
		isnull(wl.name,'')			as 'location'
from	workshops w with (nolock)
left outer join workshop_types wt with (nolock) on w.workshop_type = wt.workshop_type
left outer join workshop_locations wl with (nolock) on w.workshop_location = wl.workshop_location
where	workshop = @workshop

return ( @@rowcount )
GO
