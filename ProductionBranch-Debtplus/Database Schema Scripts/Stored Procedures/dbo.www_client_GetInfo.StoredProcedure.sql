USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_GetInfo]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_GetInfo] ( @client int, @birthdate datetime = null, @ssn varchar(80) = null ) as

-- Suppress intermediate results so that we don't confuse the result builder
set nocount on

-- Find the person from the selection criteria
declare	@person		int

if @birthdate is null and @ssn is null
	select	@person = person from people where client = @client and Relation = 1
else if @birthdate is null
	select	@person = person from people where client = @client and dbo.numbers_only(ssn) = dbo.numbers_only(@ssn)
else if @ssn is null
	select	@person = person from people where client = @client and dbo.date_only(birthdate) = dbo.date_only(@birthdate)
else
	select	@person = person from people where client = @client and dbo.date_only(birthdate) = dbo.date_only(@birthdate) and dbo.numbers_only(ssn) = dbo.numbers_only(@ssn)

-- If we found a match then return the result set	
if @person is not null
	select	c.client				as Client,
			c.active_status			as ActiveStatus,
			dbo.format_normal_name(n.prefix, n.first, n.middle, n.last, n.suffix) as ClientName,
			p.birthdate				as BirthDate,
			dbo.format_ssn(p.ssn)	as SSN,
			dbo.format_address_block(c.AddressID) as 'Address'

	from	clients c
	inner join people p on p.Person = @person
	left outer join Names n on p.NameID = n.Name
	where	c.client = @client

return ( @@rowcount )
GO
