USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statements_Quarterly]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statements_Quarterly] ( @client_statement_batch_1 as int = null, @client_statement_batch_2 as int = null, @client_statement_batch_3 as int = null ) as

-- ===========================================================================================
-- ==        Client statement information                                                   ==
-- ===========================================================================================

set nocount on

if @client_statement_batch_1 is null
	select top 1
		@client_statement_batch_1	= client_statement_batch
	from	client_statement_batches with (nolock)
	order by client_statement_batch desc;

if @client_statement_batch_1 is null
	select	@client_statement_batch_1 = 1;

if @client_statement_batch_2 is null
	select	@client_statement_batch_2 = @client_statement_batch_1;

if @client_statement_batch_3 is null
	select	@client_statement_batch_3 = @client_statement_batch_1;

-- Build a list of the clients to be printed
select	distinct c.client, a.postalcode, convert(int,0) as items_1, convert(int,0) as items_2, convert(int,0) as items_3
into	#t
from	client_statement_clients v with (nolock)
inner join clients c with (nolock) on v.client = c.client
left outer join addresses a with (nolock) on c.addressid = a.address
where	v.client_statement_batch in (@client_statement_batch_1, @client_statement_batch_2, @client_statement_batch_3)
and	c.mail_error_date is null
and	v.active_debts > 0
--and	v.delivery_method <> 'E'
--and	v.active_status in ('A','AR')

-- Create an index for the client id
create index ix1_temp_t on #t ( client );

-- Indicate that there are some details on the batches
update	#t
set	items_1 = 1
from	#t t
inner join client_statement_details d on t.client = d.client and @client_statement_batch_1 = d.client_statement_batch
where	d.debits <> d.credits;

update	#t
set	items_2 = 1
from	#t t
inner join client_statement_details d on t.client = d.client and @client_statement_batch_2 = d.client_statement_batch
where	d.debits <> d.credits;

update	#t
set	items_3 = 1
from	#t t
inner join client_statement_details d on t.client = d.client and @client_statement_batch_3 = d.client_statement_batch
where	d.debits <> d.credits;

-- Return the clients to the report
select	c.client					as client,
	convert(varchar(50), coalesce(v3.expected_deposit_date, v2.expected_deposit_date, v1.expected_deposit_date,''))		as expected_deposit_date,
	convert(varchar(50), coalesce(v3.expected_deposit_amt, v2.expected_deposit_amt, v1.expected_deposit_amt,''))		as expected_deposit_amt,
	convert(varchar(50), isnull(dbo.format_normal_name(default,cox.first,default,cox.last,default),''))	as counselor_name,
	convert(varchar(50), '')			as company_id,

	convert(datetime, b3.disbursement_date)		as disbursement_date,
	convert(datetime, b3.statement_date)		as statement_date,
	convert(datetime, b1.period_start)		as period_start,
	convert(datetime, b3.period_end)		as period_end,

	convert(money,isnull(v1.deposit_amt,0))		as v1_deposit_amt,
	convert(money,isnull(v2.deposit_amt,0))		as v2_deposit_amt,
	convert(money,isnull(v3.deposit_amt,0))		as v3_deposit_amt,

	convert(money,isnull(v1.refund_amt,0))		as v1_refund_amt,
	convert(money,isnull(v2.refund_amt,0))		as v2_refund_amt,
	convert(money,isnull(v3.refund_amt,0))		as v3_refund_amt,

	convert(money,isnull(v1.disbursement_amt,0))	as v1_disbursement_amt,
	convert(money,isnull(v2.disbursement_amt,0))	as v2_disbursement_amt,
	convert(money,isnull(v3.disbursement_amt,0))	as v3_disbursement_amt,

	convert(money,isnull(v3.held_in_trust,0))	as v3_held_in_trust

from	#t c

left outer join client_statement_clients v1		with (nolock) on c.client = v1.client and @client_statement_batch_1 = v1.client_statement_batch
left outer join client_statement_clients v2		with (nolock) on c.client = v2.client and @client_statement_batch_2 = v2.client_statement_batch
left outer join client_statement_clients v3		with (nolock) on c.client = v3.client and @client_statement_batch_3 = v3.client_statement_batch
left outer join counselors co				with (nolock) on coalesce(v3.counselor,v2.counselor,v1.counselor,-1) = co.counselor
left outer join names cox						with (nolock) on co.nameid = cox.name
left outer join	client_statement_batches b1		with (nolock) on @client_statement_batch_1 = b1.client_statement_batch
left outer join	client_statement_batches b2		with (nolock) on @client_statement_batch_2 = b2.client_statement_batch
left outer join	client_statement_batches b3		with (nolock) on @client_statement_batch_3 = b3.client_statement_batch

-- If you want only items that have something to print, include this line and uncomment the
-- block above that adds these items to the table.
where	(c.items_1 <> 0 or c.items_2 <> 0 or c.items_3 <> 0)
order by c.postalcode, c.client;

drop table #t

return ( 0 )
GO
