USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_Summary]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_Summary] ( @client_creditor_proposal AS INT = null) AS
-- ===========================================================================================
-- ==                    Fetch the client_creditor from the proposal table                  ==
-- ===========================================================================================

-- ChangeLog
--   4/11/2002
--     Used saved payment from proposals as proposed payment amount when possible.
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2002
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   9/2/2003
--     Changed for CR 9.0 to call with "NULL" as a parameter value.

DECLARE		@client_creditor	INT
DECLARE		@proposal_income_info	BIT

-- Sometimes, CR9 calls this procedure with "0" for the value. Handle it here as a "null".
if @client_creditor_proposal = 0
	select @client_creditor_proposal = null

-- If the value is null then choose a suitable value. It's not important that the information exist, only that we don't "die".
if @client_creditor_proposal IS NULL
	SELECT	@client_creditor	= 1,
		@proposal_income_info	= 0

ELSE BEGIN

	SELECT		@client_creditor	= prop.client_creditor,
			@proposal_income_info	= cr.proposal_income_info
	FROM		client_creditor_proposals prop	WITH (NOLOCK)
	INNER JOIN	client_creditor cc		WITH (NOLOCK) ON prop.client_creditor = cc.client_creditor
	LEFT OUTER JOIN	creditors cr			WITH (NOLOCK) ON cc.creditor = cr.creditor
	WHERE		prop.client_creditor_proposal = @client_creditor_proposal

	IF @client_creditor IS NULL
	BEGIN
		RAISERROR (50039, 16, 1, @client_creditor_proposal)
		RETURN ( 0 )
	END
END

-- Default the flag to send income information
IF @proposal_income_info IS NULL
	SELECT @proposal_income_info = 0

DECLARE	@asset_liabilities	MONEY
DECLARE	@asset_info		MONEY

-- ===========================================================================================
-- ==                    Fetch the client from the client_creditor table                    ==
-- ===========================================================================================

DECLARE @BalanceOwed		MONEY
DECLARE @client			INT

-- This is for the single debt being requested. It is always shown as a blance, even if the creditor class says not to include it.
-- (If the information is not valid, then you should not be printing this proposal.)

if @client_creditor_proposal IS NULL
	SELECT	@client		= 0,
		@BalanceOwed	= 0

ELSE BEGIN

	SELECT	@client		= client,
		@BalanceOwed	= isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) - isnull(bal.total_payments,0)
	FROM	client_creditor cc WITH (NOLOCK)
	INNER JOIN client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
	WHERE	cc.client_creditor = @client_creditor

	IF @client IS NULL
	BEGIN
		RAISERROR (50040, 16, 1, @client_creditor)
		RETURN ( 0 )
	END
END

-- ===========================================================================================
-- ==             Retrieve the list of debts for this client                                ==
-- ===========================================================================================
DECLARE	@Orig_Bal	MONEY
DECLARE @Orig_Adj	MONEY
DECLARE @Interest       MONEY
DECLARE @Payments	MONEY

SELECT		@Orig_bal = sum(bal.orig_balance),
		@Orig_adj = sum(bal.orig_balance_adjustment),
		@Interest = sum(bal.total_interest),
		@Payments = sum(bal.total_payments)
FROM		client_creditor cc WITH (NOLOCK)
inner join	client_creditor_balances bal WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
LEFT OUTER JOIN	creditors cr WITH (NOLOCK) ON cc.creditor = cr.creditor
LEFT OUTER JOIN creditor_classes ccl WITH (NOLOCK) ON cr.creditor_class = ccl.creditor_class
WHERE		client = @client
AND		isnull(ccl.proposal_balance,1) <> 0
AND		cc.reassigned_debt = 0
AND		isnull(bal.orig_balance,0) + isnull(bal.orig_balance_adjustment,0) + isnull(bal.total_interest,0) > isnull(bal.total_payments,0)

-- ===========================================================================================
-- ==             Retrieve the propposed payment for this debt                              ==
-- ===========================================================================================
DECLARE @Proposed_Payment	MONEY

SELECT		@Proposed_Payment = proposed_amount
FROM		client_creditor_proposals WITH (NOLOCK)
WHERE		client_creditor_proposal = @client_creditor_proposal

IF @Proposed_Payment IS NULL
	SELECT		@Proposed_Payment = disbursement_factor
	FROM		client_creditor cc WITH (NOLOCK)
	WHERE		client_creditor = @client_creditor

-- ===========================================================================================
-- ==             Retrieve the start date from the client                                   ==
-- ===========================================================================================
DECLARE @StartDate		DateTime
DECLARE @DisbDate       	smallint
DECLARE @PeopleInHousehold	INT
DECLARE @ProposedDate		DateTime

SELECT		@StartDate		= coalesce (p.proposed_start_date, cc.start_date, c.start_date, getdate()),
		@ProposedDate		= coalesce (p.proposed_start_date, cc.start_date, c.start_date, getdate()),
		@PeopleInHousehold	= c.dependents + (case c.marital_status when 2 then 2 else 1 end),
		@DisbDate		= isnull(c.disbursement_date,10)
FROM		client_creditor_proposals p WITH (NOLOCK)
INNER JOIN	client_creditor cc WITH (NOLOCK) ON p.client_creditor = cc.client_creditor
INNER JOIN	clients c WITH (NOLOCK) ON cc.client = c.client
WHERE		p.client_creditor_proposal = @client_creditor_proposal

-- ===========================================================================================
-- ==             Calculate the net income for the client                                   ==
-- ===========================================================================================
DECLARE @Net_Income   MONEY
DECLARE @People_Net   MONEY
SELECT	@People_Net = SUM(net_income)
FROM	people WITH (NOLOCK)
WHERE	client = @client

-- Add to that any additional income sources
SELECT		@Net_Income = SUM(asset_amount)
FROM		assets WITH (NOLOCK)
WHERE		client = @client

SELECT		@Net_Income = isnull(@Net_Income,0) + isnull(@People_Net,0)

-- ===========================================================================================
-- ==             Find the latest budget ID for this client                                 ==
-- ===========================================================================================
DECLARE @Budget   INT
SELECT	@Budget = dbo.map_client_to_budget ( @client )

-- ===========================================================================================
-- ==             Calculate the living expenses for the client                              ==
-- ===========================================================================================

DECLARE @LivingExpenses   MONEY

IF @Budget IS NOT NULL
	SELECT		@LivingExpenses = SUM(suggested_amount)
	FROM		budget_detail d		WITH (NOLOCK)
	INNER JOIN	budget_categories c	WITH (NOLOCK) ON c.budget_category=d.budget_category AND c.living_expense>0
	WHERE		d.budget = @Budget

-- ===========================================================================================
-- ==             Calculate the housing expenses for the client                             ==
-- ===========================================================================================

DECLARE @HousingExpenses   MONEY

IF @Budget IS NOT NULL
	SELECT		@HousingExpenses = SUM(suggested_amount)
	FROM		budget_detail d		WITH (NOLOCK)
	INNER JOIN	budget_categories c	WITH (NOLOCK) ON c.budget_category=d.budget_category AND c.housing_expense>0
	WHERE		d.budget = @Budget

-- ===========================================================================================
-- ==             Retrieve the income information                                           ==
-- ===========================================================================================

IF @proposal_income_info > 0
BEGIN
	SELECT	@asset_info		= sum(current_value)
	FROM	secured_properties WITH (NOLOCK)
	WHERE	client			= @client

	SELECT	@asset_liabilities	= sum(l.balance)
	FROM	secured_properties p WITH (NOLOCK)
	INNER JOIN secured_loans l WITH (NOLOCK) ON p.secured_property = l.secured_property
	WHERE	p.client			= @client
END

-- ===========================================================================================
-- ==             Return the resulting information to the caller                            ==
-- ===========================================================================================

SELECT	convert(money, isnull(@BalanceOwed,0))		as balance_owed,
	convert(money, isnull(@Proposed_Payment,0))	as proposed_payment,
	convert(datetime, @ProposedDate)		as start_date,
	convert(money, isnull(@Net_Income,0))		as net_income,
	convert(money, isnull(@LivingExpenses,0))	as living_expense,
	convert(money, isnull(@HousingExpenses,0))	as housing_expense,
	convert(int, isnull(@PeopleInHousehold,1))	as people_in_household,
	convert(money, isnull(@asset_info,0))		as asset_info,
	convert(money, isnull(@asset_liabilities,0))	as asset_liabilities

RETURN ( 1 )
GO
