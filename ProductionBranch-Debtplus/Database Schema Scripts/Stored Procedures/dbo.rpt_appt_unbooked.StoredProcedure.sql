USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_appt_unbooked]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_appt_unbooked]  (@from_date as datetime = null, @to_date as datetime = null) as

-- =========================================================================================
-- ==           Show the appointments that are available for a given client/office        ==
-- =========================================================================================

-- ChangeLog
--   7/29/08
--     Initial creation

if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date	= @to_date

select	@from_date	= convert(varchar(10), @from_date, 101),
		@to_date	= convert(varchar(10), @to_date, 101) + ' 23:59:59';

select	t.appt_time,
		t.office,
		o.name as office_name,

		ac.counselor,
		dbo.format_normal_name(default,cox.first,default,cox.last,default) as counselor_name,

		t.start_time,
		t.appt_type,
		apt.appt_duration,
		isnull(apt.appt_name,'Any Appointment Type') as appt_name,
		isnull(apt.contact_type,0) as contact_type,
		m.description,
		m.nfcc,

		ac.inactive,
		ac.inactive_reason,

		convert(varchar(10), 'A') as status,
		convert(int,null) as client,
		convert(varchar(80), null) as client_name,

		convert(int,0) as appt_count,
		convert(datetime, convert(varchar(10), t.start_time, 101)) as appt_date

into	#pending_appointments
from	appt_times t
inner join appt_types apt on t.appt_type = apt.appt_type 
inner join appt_counselors ac on t.appt_time = ac.appt_time
left outer join counselors co on ac.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name
left outer join FirstContactTypes m on apt.contact_type = m.oID
left outer join offices o on t.office = o.office
where	t.start_time between @from_date and @to_date;

-- Find the number of appointments for each day
select	counselor, office, nfcc, count(*) as appt_count
into	#p_appointments
from	#pending_appointments
group by counselor, office, nfcc;

update	#pending_appointments
set		appt_count = b.appt_count
from	#pending_appointments a
inner join #p_appointments b on a.counselor = b.counselor and a.office = b.office and a.nfcc = b.nfcc;

drop table #p_appointments;

-- Set cancelled appointments
update	#pending_appointments
set		client = ca.client,
		status = ca.status
from	#pending_appointments x
inner join client_appointments ca on x.appt_time = ca.appt_time
where	ca.status = 'C';

-- Set rescheduled appointments
update	#pending_appointments
set		client = ca.client,
		status = ca.status
from	#pending_appointments x
inner join client_appointments ca on x.appt_time = ca.appt_time
where	ca.status = 'R';

-- Set pending appointments
update	#pending_appointments
set		client = ca.client,
		status = ca.status
from	#pending_appointments x
inner join client_appointments ca on x.appt_time = ca.appt_time
where	ca.status = 'P';

-- Set counseled appointments
update	#pending_appointments
set		client = ca.client,
		status = ca.status
from	#pending_appointments x
inner join client_appointments ca on x.appt_time = ca.appt_time
where	ca.status in ('K','W');

-- Set the corresponding client name
update	#pending_appointments
set		client_name = dbo.format_normal_name(default, pn.first, pn.middle, pn.last, pn.suffix)
from	#pending_appointments x
inner join people p on x.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name;

-- Return the results to the report
-- It will generate the grouings needed.
select	*
from	#pending_appointments
where	status in ('C','R','A')
order by office_name, office, counselor_name, counselor, nfcc, start_time, client;

drop table #pending_appointments
return ( 0 )
GO
