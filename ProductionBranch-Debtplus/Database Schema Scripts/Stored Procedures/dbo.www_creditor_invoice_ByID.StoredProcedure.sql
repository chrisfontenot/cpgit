SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [www_creditor_invoice_ByID] ( @creditor as typ_creditor, @invoice_register as typ_key, @invoice_date as varchar(10) OUTPUT, @invoice_amount as money OUTPUT ) AS
-- =========================================================================================================================
-- ==            Return the information for the invoice, given the invoice number                                         ==
-- =========================================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Suppress intermediate results
set nocount on

-- Ensure that the creditor is valid for the invoice
if not exists ( select * from registers_invoices where creditor = @creditor and invoice_register = @invoice_register )
begin
	RaisError ('Either the creditor or the invoice number is not valid', 16, 1)
	return ( 0 )
end

-- Find the invoice amount and date of the invoice
select	@invoice_date	= convert(varchar(10), inv_date, 1),
		@invoice_amount	= inv_amount
from	registers_invoices
where	invoice_register = @invoice_register

-- Return the invoice address to the caller
declare	@address_type	varchar(10)
if exists ( select * from creditor_addresses where type = 'I' and creditor = @creditor)
	select	@address_type = 'I'
else
	select	@address_type = 'P'

declare	@attn		varchar(80)
DECLARE @Addr1		varchar(256)
DECLARE @Addr2		varchar(256)
DECLARE @Addr3		varchar(256)
DECLARE @Addr4		varchar(256)
DECLARE @Addr5		varchar(256)
DECLARE @Addr6		varchar(256)

-- Use the creditor address if this is a creditor check
create table #www_creditor_address (line int, address varchar(80) null)

IF @creditor IS NOT NULL
BEGIN
	select	@attn		= attn,
			@addr1		= addr1,
			@addr2		= addr2,
			@addr3		= addr3,
			@addr4		= addr4,
			@addr5		= addr5,
			@addr6		= addr6
	from	view_creditor_addresses
	where	creditor	= @creditor
	and	type		= 'P'

	insert into #www_creditor_address (line, address) values (1, 'ATTN: ' + @attn)
	insert into #www_creditor_address (line, address) values (2, @addr1)
	insert into #www_creditor_address (line, address) values (3, @addr2)
	insert into #www_creditor_address (line, address) values (4, @addr3)
	insert into #www_creditor_address (line, address) values (5, @addr4)
	insert into #www_creditor_address (line, address) values (6, @addr5)
	insert into #www_creditor_address (line, address) values (7, @addr6)
END

select		upper(address) as address, @invoice_amount as 'invoice_amount', @invoice_date as 'invoice_date'
from		#www_creditor_address
where		address is not null
order by	line

drop table #www_creditor_address

-- Return the transactions for this invoice
select	convert(varchar(10), date_created, 1)		as 'date_created',
		tran_type					as 'tran_type',
		credit_amt					as 'credit_amt'
from	registers_creditor
where	tran_type in ('RC', 'RA')
and		creditor = @creditor
and		invoice_register = @invoice_register
order by date_created

-- Return the disbursement information for the invoice
select	convert(varchar,tr.checknum)				as 'check_number',
		convert(varchar(10), tr.date_created, 1)	as 'date_created',
		tr.trust_register							as 'trust_register',
		d.client									as 'client',
		dbo.format_reverse_name (default, pn.first, pn.middle, pn.last, default)	as 'client_name',
		isnull(d.account_number,cc.account_number)	as 'account_number',
		d.debit_amt									as 'debit_amt',
		d.fairshare_amt								as 'fairshare_amt',
		cc.client_creditor							as 'client_creditor'
from	registers_client_creditor d
inner join registers_trust tr on d.trust_register = tr.trust_register
left outer join people p on d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join client_creditor cc on d.client_creditor = cc.client_creditor
where	d.creditor = @creditor
and		d.invoice_register = @invoice_register
and		d.tran_type in ('AD', 'MD', 'BW', 'CM')
order by 1, 5, 2

return ( 1 )
GO
