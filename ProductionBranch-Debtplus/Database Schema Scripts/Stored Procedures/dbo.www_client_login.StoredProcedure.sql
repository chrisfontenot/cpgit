USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_login]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_client_login] ( @client as int, @password as varchar(80)) as
-- =============================================================================================
-- ==            Perform the client logon function                                            ==
-- =============================================================================================

-- Disable intermediate results
set nocount on

select	@password = lower(ltrim(rtrim(@password)))
if @password = ''
	select @password = null

-- If there is no password then ensure that the client does not exist in the database.
if @password is null
begin
	if exists (select UserName from client_www where UserName = convert(varchar,@client) AND ApplicationName = '/')
	begin
		RaisError ('The client ID is not valid', 16, 1)
		return ( 0 )
	end
end else begin
	-- Validate the logon parameters for the client in the database
	if not exists ( select * from client_www where username = convert(varchar,@client) and applicationname = '/' and [password] = @password )
	begin
		RaisError ('The client ID is not valid', 16, 1)
		return ( 0 )
	end
end

-- Determine the parameters for the logon once the information is correct
select	dbo.format_normal_name (default, pn.first, default, pn.last, pn.suffix)	as 'name',
	isnull(dbo.format_address_line_1 (a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),'')							as 'address1',
	isnull(a.address_line_2,'')							as 'address2',
	dbo.format_city_state_zip(a.city, a.state, a.postalcode)		as 'address3',

	case c.active_status
		when 'CRE' then 'N'
		when 'APT' then 'N'
		when 'WKS' then 'N'
		when 'PND' then 'N'
		when 'RDY' then 'N'
		when 'PRO' then 'N'
		when 'A'   then 'Y'
		when 'AR'  then 'Y'
		when 'EX'  then 'Y'
					when 'I'   then case
						when drop_date is null   then 'N'
						when drop_date < dateadd(m, -6, getdate()) then 'N'
						else 'Y'
						end
		else		'N'
		end								as active_status

from		people p	with (nolock)
inner join	clients c	with (nolock) on p.client = c.client
left outer join addresses a with (nolock) on c.AddressID = a.Address
left outer join Names pn with (nolock) on p.NameID = pn.Name
where		p.client = @client
and		p.relation = 1

if @@rowcount < 1
begin
	RaisError ('The client ID is not valid', 16, 1)
	return ( 0 )
end

return ( 1 )
GO
