USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_ResourceGuide]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_ResourceGuide] (@office as int = NULL, @types as varchar(256) = NULL) AS
-- =================================================================================
-- ==    Select the information needed to generate the Resource Guide Report      ==
-- =================================================================================
declare	@office_select	varchar(512)
declare	@type_select	varchar(512)
declare	@stmt		varchar(1024)

-- Process the office selection
select	@office_select  = ''
if @office is not null
begin
	if @office > 0
		select	@office_select	= 'office=' + convert(varchar, @office)
end

-- Process the resource type selection
select	@type_select	= ''
if @types is not null
begin
	select	@types = ltrim(rtrim(@types))
	if @types != ''
		select	@type_select	= 'resource_type in (' + @types + ')'
end

-- Generate the proper select statement based upon the criteria
if @office_select <> '' and @type_select <> ''
	select	@stmt = @office_select + ' and ' + @type_select
else
	select	@stmt = @office_select + @type_select

-- Include the clause the do the selection if there is one
if @stmt <> ''
	select	@stmt = ' where ' + @stmt

-- The resulting statement is now to retrieve the data from the database view
select	@stmt = 'select distinct description, [name], address1, address2, address3, telephone, fax, email, www FROM view_resource_detail WHERE resource in (select resource FROM view_resource_detail ' + @stmt + ') order by 1,2'
exec ( @stmt )
return ( @@rowcount )
GO
