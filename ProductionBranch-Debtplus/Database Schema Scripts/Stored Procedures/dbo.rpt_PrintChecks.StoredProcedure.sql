SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_PrintChecks] ( @Marker AS VarChar(50) ) AS
-- ====================================================================================
-- ==               Print the indicated check or all checks pending printing         ==
-- ====================================================================================

-- ChangeLog
--   9/8/2002
--     Assign the check numbers in postalcode sort order. This will cause the checks to be printed in zipcode
--     sort sequence order (since they are printed in check number order).
--   12/10/2002
--     Re-Added support for double-spaced creditor vouchers which was somehow dropped earlier.
--   11/13/2003
--     Added support for the marker to be null because crystal reports 9 calls it with null when loading the report
--     Added support for the banks table to hold the check number. It is no longer in the config table.
--   2/23/2012
--     Fully support the possibility of the procedure being called twice with the same marker string.
--     We need to handle the case where the marker points to a series of checks that have already been "printed".
--     The report procedure calls this procedure twice with the same marker for both so the first time, the numbers
--     are assigned and the checks marked "printed". The second call needs to get the data from the first procedure
--     so it calls it again with the same marker. We don't want to change the check number. We just want to return the
--     data.

-- Do not cause problems later
SET NOCOUNT ON
SET ANSI_WARNINGS OFF

-- Allow crystal reprots to verify the procedure even when we don't want them to do this.
if len(isnull(@marker,'')) < 3
begin
	SELECT top 1	t.trust_register,

		case
			when t.creditor is not null then cr.creditor_name
			when t.client   is not null then dbo.format_normal_name (pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)
			else ''
		end as payee,

		case
			when t.client is not null	then 'CL'	-- This is a client check
			when t.creditor is null		then 'UN'	-- This is a strange check with no payee
			when cr.voucher_spacing = 2	then 'NV'	-- Creditor check with no voucher
			when cr.voucher_spacing = 1	then 'CR2'	-- Double-spaced creditor check
							else 'CR1'	-- Single-spaced creditor check
		end	as 'tran_type',

		t.client,
		t.creditor,
		t.date_created,
		t.checknum,

		t.amount

	FROM		registers_trust t
	LEFT OUTER JOIN creditors cr with (nolock) ON t.creditor = cr.creditor
	left outer join people p with (nolock) on t.client = p.client and 1 = p.relation
	left outer join names pn with (nolock) on p.nameid = pn.name

	return ( @@rowcount )
end

BEGIN TRANSACTION
SET XACT_ABORT ON

-- Find a trust register which has this marker sequence. We need only the bank account number from it.
declare @trust_register	int
declare	@bank		int

select	@trust_register = trust_register,
		@bank		= bank
from	registers_trust with (nolock)
where	cleared		= 'P'
and	tran_type	in ('AD','MD','CM','CR')
and	sequence_number	= @marker

if @trust_register is null
	select	@trust_register = trust_register,
			@bank			= bank
	from	registers_trust with (nolock)
	where	tran_type	in ('AD','MD','CM','CR')
	and		sequence_number	= @marker

if @trust_register is null
begin
	raiserror('Unable to locate trust register for the indicated marker string', 16, 1)
	return ( 0 )
end

-- Find the check number from the banks table
DECLARE	@CheckNumber	BigInt
select	@CheckNumber	= checknum
from	banks
where	bank		= @bank
and		type		= 'C'

if @CheckNumber is null
begin
	raiserror ('The bank account associated with the check batch is not a checking account', 16, 1)
	return ( 0 )
end

-- Generate a list of the check numbers to be processed so that the check numbers may
-- be assigned by this procedure.
CREATE TABLE	#t_CheckNumbers (
	trust_register	int,
	check_order		int,
	postalcode		varchar(50) null,
	checknum		BigInt NULL
)
CREATE INDEX ix1_temp_checknums ON #t_CheckNumbers ( trust_register );

-- Include the creditor disbursement checks
-- These may or may not have check numbers
INSERT INTO	#t_CheckNumbers (trust_register, check_order, postalcode, checknum)
SELECT		tr.trust_register, isnull(tr.check_order,9), left(isnull(ad.postalcode,''),50), tr.checknum
FROM		registers_trust tr
LEFT OUTER JOIN	view_creditor_addresses ad with (nolock) on tr.creditor = ad.creditor and 'P' = ad.type
WHERE		sequence_number = @Marker
AND			tran_type in ('AD', 'CM', 'MD')

-- Include the client refund operations
-- These may or may not have check numbers
INSERT INTO	#t_CheckNumbers (trust_register, check_order, postalcode, checknum)
SELECT		tr.trust_register, isnull(tr.check_order,9), left(isnull(a.postalcode,''),50), tr.checknum
FROM		registers_trust tr
LEFT OUTER JOIN	clients ad with (nolock) on tr.client = ad.client
left outer join addresses a with (nolock) on ad.AddressID = a.address
WHERE		sequence_number = @Marker
AND			tran_type in ('CR', 'AR')

-- Update the check numbers for the indicated list
-- We do this only for the items that have no check number, ergo the "where" clause below.
DECLARE check_cursor CURSOR FOR
	SELECT		trust_register
	FROM		#t_CheckNumbers
	WHERE		checknum IS NULL
	ORDER BY	check_order, postalcode, trust_register

OPEN check_cursor

FETCH check_cursor INTO @trust_register
WHILE @@fetch_status = 0
BEGIN
	UPDATE	#t_CheckNumbers
	SET		checknum = @CheckNumber
	WHERE	trust_register = @trust_register

	SELECT	@CheckNumber = @CheckNumber + 1

	FETCH	check_cursor INTO @trust_register
END

CLOSE check_cursor
DEALLOCATE check_cursor

-- Update the banks table with the next check number
update	banks
set		checknum	= @CheckNumber
where	bank		= @bank

-- Merge the temporary table back into the trust register to actually set the check number
-- The check numbers is extracted if it is not-null. Only the null values are assigned new check
-- numbers so if the check number was previously specified, it is updated with the same value and
-- there is no change.
UPDATE	registers_trust
SET		checknum = x.checknum,
		cleared	= ' '
FROM	registers_trust t
INNER JOIN #t_CheckNumbers x ON t.trust_register = x.trust_register

-- Select the checks from the result set in the order that we have chosen
SELECT		t.trust_register,

		case
			when t.creditor is not null then cr.creditor_name
			when t.client   is not null then dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix)
			else ''
		end as payee,

		case
			when t.client is not null	then 'CL'	-- This is a client check
			when t.creditor is null		then 'UN'	-- This is a strange check with no payee
			when cr.voucher_spacing = 2	then 'NV'	-- Creditor check with no voucher
			when cr.voucher_spacing = 1	then 'CR2'	-- Double-spaced creditor check
							else 'CR1'	-- Single-spaced creditor check
		end	as 'tran_type',

		t.client,
		t.creditor,
		t.date_created,
		t.checknum,

		t.amount

FROM		registers_trust t
INNER JOIN	#t_CheckNumbers x ON x.trust_register = t.trust_register
LEFT OUTER JOIN creditors cr	with (nolock) ON t.creditor = cr.creditor
left outer join people p	with (nolock) on t.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

ORDER BY	x.checknum

-- Discard the working table that we used to build the list
DROP TABLE	#t_CheckNumbers

-- Make the changes permanent
COMMIT TRANSACTION

-- Return success to the caller
RETURN ( @@rowcount )
GO
