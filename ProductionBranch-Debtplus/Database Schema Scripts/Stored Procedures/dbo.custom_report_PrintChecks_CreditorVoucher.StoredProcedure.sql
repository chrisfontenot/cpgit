USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[custom_report_PrintChecks_CreditorVoucher]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[custom_report_PrintChecks_CreditorVoucher] ( @Check AS INT, @creditor as varchar(10) = null ) AS
-- ====================================================================================
-- ==               Retrieve the information for the creditor voucher                ==
-- ====================================================================================

-- ChangeLog
--   5/8/2002
--     Use cc.client_name if one is supplied.
--   5/23/2003
--     Use client refund information if applicable
--   06/09/2009
--      Remove references to debt_number and use client_creditor

set nocount on

declare	@tran_type	varchar(2)
select	@tran_type = tran_type
from	registers_trust with (nolock)
where	trust_register = @check

if @tran_type = 'CR'
begin

SELECT		c.active_status				as 'active_status',
		c.client				as 'client',
		convert(varchar(22),null)		as 'account_number',
		convert(varchar(80),dbo.format_reverse_name (default, pn.first, pn.middle, pn.last, pn.suffix))	as 'client_name',
		convert(money,isnull(tr.amount,0))	as 'gross',
		convert(money,0)			as 'deducted',
		convert(money,0)			as 'billed',
		convert(money,isnull(tr.amount,0))	as 'net'

FROM		registers_trust tr
LEFT OUTER JOIN people p WITH (NOLOCK) ON tr.client=p.client AND 1=p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN clients c WITH (NOLOCK) ON tr.client = c.client
WHERE		tr.trust_register = @Check

end else if @tran_type = 'BW'
begin

SELECT		c.active_status as active_status,
		t.client as 'client',
		convert(varchar(22),isnull(t.account_number, cc.account_number)) as 'account_number',

		convert(varchar(80), case
			when cc.client_name is null		then dbo.format_reverse_name (default, pn.first, pn.middle, pn.last, pn.suffix)
			when ltrim(rtrim(cc.client_name)) = '' then dbo.format_reverse_name (default, pn.first, pn.middle, pn.last, pn.suffix)
								else ltrim(rtrim(cc.client_name))
		end) as 'client_name',

		convert(money,isnull(t.debit_amt,0))							as 'gross',
		convert(money,case t.creditor_type when 'D' then isnull(t.fairshare_amt,0) else 0 end)	as 'deducted',
		convert(money,case t.creditor_type when 'N' then 0 when 'D' then 0 else isnull(t.fairshare_amt,0) end)	as 'billed',
		convert(money,case t.creditor_type when 'D' then isnull(t.debit_amt,0) - isnull(t.fairshare_amt,0) else isnull(t.debit_amt,0) end) as 'net'

FROM		registers_client_creditor t
INNER JOIN	client_creditor cc WITH (NOLOCK) ON t.client_creditor = cc.client_creditor
INNER JOIN clients c WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN people p WITH (NOLOCK) ON t.client=p.client AND 1=p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		t.trust_register = @Check
AND			t.creditor = @creditor
ORDER BY	t.client

END ELSE BEGIN

SELECT		c.active_status as active_status,
		t.client as 'client',
		convert(varchar(22),isnull(t.account_number, cc.account_number)) as 'account_number',

		convert(varchar(80), case
			when cc.client_name is null		then dbo.format_reverse_name (default, pn.first, pn.middle, pn.last, pn.suffix)
			when ltrim(rtrim(cc.client_name)) = '' then dbo.format_reverse_name (default, pn.first, pn.middle, pn.last, pn.suffix)
								else ltrim(rtrim(cc.client_name))
		end) as 'client_name',

		convert(money,isnull(t.debit_amt,0))							as 'gross',
		convert(money,case t.creditor_type when 'D' then isnull(t.fairshare_amt,0) else 0 end)	as 'deducted',
		convert(money,case t.creditor_type when 'N' then 0 when 'D' then 0 else isnull(t.fairshare_amt,0) end)	as 'billed',
		convert(money,case t.creditor_type when 'D' then isnull(t.debit_amt,0) - isnull(t.fairshare_amt,0) else isnull(t.debit_amt,0) end) as 'net'

FROM		registers_client_creditor t
INNER JOIN	client_creditor cc WITH (NOLOCK) ON t.client_creditor = cc.client_creditor
INNER JOIN clients c WITH (NOLOCK) ON cc.client = c.client
LEFT OUTER JOIN people p WITH (NOLOCK) ON t.client=p.client AND 1=p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		t.trust_register = @Check
ORDER BY	t.client

end

-- Return success to the caller
RETURN ( @@rowcount )
GO
