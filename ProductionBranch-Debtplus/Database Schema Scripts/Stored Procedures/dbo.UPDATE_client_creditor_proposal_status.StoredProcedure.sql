USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_client_creditor_proposal_status]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_client_creditor_proposal_status] as

-- ===================================================================================================
-- ==          Force all debt records to point to the "correct" proposal record and status          ==
-- ===================================================================================================

-- alter table client_creditor add client_creditor_proposal int null
select	client_creditor, max(date_created) as date_created, convert(int,null) as client_creditor_proposal
into	#latest
from	client_creditor_proposals
group by client_creditor

update	#latest
set	client_creditor_proposal = p.client_creditor_proposal
from	#latest l
inner join client_creditor_proposals p on l.client_creditor = p.client_creditor and l.date_created = p.date_created

update	client_creditor
set	client_creditor_proposal = null

update	client_creditor
set	client_creditor_proposal = l.client_creditor_proposal
from	client_creditor cc
inner join #latest l on cc.client_creditor = l.client_creditor

drop table #latest
GO
