USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdf_generate]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdf_generate] ( @rpps_file as int = null ) AS
-- ===================================================================================================
-- ==            Generate transactions for the RPPS CDF record                                     ==
-- ===================================================================================================

-- ChangeLog
--   8/12/2003
--     Initial creation

RETURN ( 1 )
GO
