USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_response_fr]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_response_fr] (
	@epay_response_file as int,
	@transaction_id as varchar(27),
	@transaction_type as varchar(2),
	@sending_id as varchar(12),
	@receiving_id as varchar(12),
	@message as varchar(300),
	@local_biller_id as varchar(12)

) as

-- Remove the extra spaces around the fields. ADODB does not like empty strings so we don't trim them there.
select	@sending_id		= ltrim(rtrim(@sending_id)),
	@receiving_id		= ltrim(rtrim(@receiving_id)),
	@transaction_type	= ltrim(rtrim(@transaction_type)),
	@message		= ltrim(rtrim(@message)),
	@local_biller_id	= ltrim(rtrim(@local_biller_id))

if @sending_id = ''
	select	@sending_id	= null;

if @receiving_id = ''
	select	@receiving_id	= null;

if @message = ''
	select	@message	= null;

if @local_biller_id = ''
	select	@local_biller_id = null;

if not (@message is null)
begin
	insert into epay_responses_fr (
		epay_response_file,
		transaction_id,
		sending_id,
		receiving_id,
		message,
		local_biller_id

	) values (

		@epay_response_file,
		@transaction_id,
		@sending_id,
		@receiving_id,
		@message,
		@local_biller_id
	)

	return ( @@identity )
end

-- An empty message is not recorded. Just return an invalid pointer.
return ( 0 )
GO
