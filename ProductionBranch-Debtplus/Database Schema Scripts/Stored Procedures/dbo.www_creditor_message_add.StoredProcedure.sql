USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_message_add]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_message_add] ( @creditor as typ_creditor, @message as varchar(80) = NULL ) as
-- ===================================================================================
-- ==            Add the message to the creditor list of messages                   ==
-- ===================================================================================
set nocount on

-- Remove the extra spaces around the message.
if @message is null
	select	@message = ''
select	@message = ltrim(rtrim(@message))

-- If there is a message, insert it
if @message <> ''
begin
	insert into creditor_www_notes (creditor, message) values (@creditor, @message)
	return ( SCOPE_IDENTITY() )
end

return	( 0 )
GO
