USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_trace_update]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_rpps_trace_update] ( @batch_id as INT, @transaction_id as INT, @rpps_file as int) AS

-- ======================================================================================================================
-- ==            Update the batch and transaction sequence number for the next generation routine                      ==
-- ======================================================================================================================

set nocount on
begin transaction

declare	@bank		int
select	@bank		= bank
from	rpps_files with (nolock)
where	rpps_file	= @rpps_file

if @bank is null
begin
	rollback transaction
	raiserror ('There is no bank account configured for RPPS. Do not send this file!', 16, 1)
	return ( 0 )
end

-- Ensure that the trace numbers are within the proper range
select	@batch_id	= @batch_id       % 10000000,
	@transaction_id	= @transaction_id % 10000000

-- Save the last batch and transaction ID for the requests
update	banks
set	batch_number		= @batch_id,
	transaction_number	= @transaction_id
where	bank			= @bank

if @@rowcount <> 1
begin
	rollback transaction
	raiserror ('There is no bank account configured for RPPS. Do not send this file!', 16, 1)
	return ( 0 )
end

commit transaction
return ( 1 )
GO
