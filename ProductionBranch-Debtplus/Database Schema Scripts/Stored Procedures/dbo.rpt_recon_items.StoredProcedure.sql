SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_recon_items] ( @recon_batch as int = null ) as

-- ================================================================================================
-- ==            Given the reconcilation batch, return the list of items                         ==
-- ==            which have been reconciled in the same batch.                                   ==
-- ================================================================================================

-- ChangeLog
--   3/22/2002
--     Added RR to the list of deposit transaction types
--   4/29/2002
--     Do not include un-reconciled items which have no error.

if @recon_batch is null
	select top 1
		@recon_batch	= recon_batch
	from	recon_batches
	order by date_created desc

-- Return the list of checks and deposits which are marked with the proper sequence identifier
select
		case
			when x.tran_type in ('AD', 'CM', 'MD', 'CR') then 'AD'
			else x.tran_type
		end					as 'tran_type',

		isnull(tr.checknum,x.checknum)	as 'checknum',
		isnull(x.cleared,tr.cleared)		as 'cleared',
		tr.date_created				as 'date_created',
		tr.amount				as 'check_amount',
		x.recon_date				as 'reconciled_date',
		x.amount				as 'reconciled_amount',

		case
			when tr.client is not null then dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)
			when tr.creditor is not null then cr.creditor_name
			else isnull(tt.description,'')
		end				as 'payee',

		convert(varchar(80),isnull(x.message,''))		as 'message',
		@recon_batch						as 'recon_batch'

from		recon_details x		with (nolock)
left outer join	registers_trust tr	with (nolock) on x.trust_register = tr.trust_register
left outer join	people p		with (nolock) on tr.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join creditors cr		with (nolock) on tr.creditor = cr.creditor
left outer join	tran_types tt		with (nolock) on tr.tran_type = tt.tran_type

where		x.recon_batch	= @recon_batch

-- Do not include un-reconciled items which have no error.
and		(x.message is not null or x.cleared != ' ')

order by	1, 2, 4		-- tran_type, checknum, date_created

return ( @@rowcount )
GO
