USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_employers]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_employers] ( @Name as varchar(50), @AddressID as int = null, @TelephoneID as int = null, @FAXID as int = null, @Industry as int = 1 ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the employers table                      ==
-- ========================================================================================
	insert into Employers( Name, AddressID, TelephoneID, FAXID, Industry ) values ( @Name, @AddressID, @TelephoneID, @FAXID, @Industry )
	return ( scope_identity() )
GO
