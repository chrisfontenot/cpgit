USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_info_ffmsg]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_info_ffmsg] ( @epay_transaction as int ) AS

-- ===============================================================================================================
-- ==            Generate the information for the Free Form Message                                             ==
-- ===============================================================================================================

-- ChangeLog
--    4/15/2003
--     Initial version
--   10/1/2003
--     Expanded the proposal_message field to "text" format.

-- Suppress intermediate results
set nocount on

-- Return the information for the proposal information
select	convert(varchar(256),dn.note_text)	as 'note_text',
	t.epay_biller_id			as 'creditor_id'
from	epay_transactions t
inner join client_creditor_proposals pr with (nolock) on t.client_creditor_proposal = pr.client_creditor_proposal
left outer join debt_notes dn on pr.client_creditor_proposal = dn.client_creditor_proposal and 'PR' = dn.type
where	t.epay_transaction		= @epay_transaction

return ( @@rowcount )
GO
