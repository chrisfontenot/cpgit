USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_emailaddress]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_emailaddress] ( @Email as int, @Address as varchar(80) = NULL, @Client as int = null, @Person as int = null, @Validation as int = 1 ) as
-- ===========================================================================================================================
-- ==             Update an entry in the EmailAddresses table for .NET                                                      ==
-- ===========================================================================================================================

declare @NoteText	varchar(800)
declare	@Subject	varchar(80)
declare	@OldAddress	varchar(800)
declare	@RowCount	int

set nocount on

select	@OldAddress	= [Address]
from	EmailAddresses
where	[Email]		= @Email;
select	@RowCount	= @@ROWCOUNT

if @RowCount > 0
begin
	UPDATE	EmailAddresses
	set		[Address]		= @Address,
			[Validation]	= @Validation
	WHERE	[Email]			= @Email;
	select	@RowCount		= @@ROWCOUNT

	-- Create a system note if one is given
	if @Client is not null AND isnull(@OldAddress,'') <> isnull(@Address,'')
	begin
		select	@Subject = 'Changed '
		if @Person is not null
		begin
			declare	@relation	int
			select	@relation = [relation]
			from	people with (nolock)
			where	[Client]	= @Client
			and		[Person]	= @Person;
		
			if @relation <> 1
				select	@Subject = @Subject + 'Co-Applicant '
			else
				select	@Subject = @Subject + 'Applicant '
		end
		select @Subject = @Subject + 'Email address'
		insert into client_notes (client, type, subject, dont_delete, dont_edit, note)
		select	@Client, 3, @Subject, 1, 1, 'Changed Email address' + ISNULL(' from ''''' + replace(@OldAddress, '''', '''''') + '''', '') + ' to ' + ISNULL('''' + replace(@Address, '''', '''''') + '''', 'NOTHING')
	end
end

return ( @RowCount )
GO
