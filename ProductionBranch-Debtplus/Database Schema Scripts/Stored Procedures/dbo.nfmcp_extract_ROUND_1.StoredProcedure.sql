USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[nfmcp_extract_ROUND_1]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[nfmcp_extract_ROUND_1]  (@from_date as datetime = null, @to_date as datetime = null ) as

-- ChangeLog
--  5/14/08
--    Used codes in the NFCC column of the 'Financial Problems' table of LOI (Loss of Income), IEX (Increase in Expense),
--	  BVF (Business Venture Failed), and ILP (Increase in Loan Payment).
--    Added test to ensure that resulted clients are for the proper hud_grant type
--  6/4/08 
--	  modified session length to decimal format
--  6/19/08
--    Corrected loan status
--  7/9/08
--    Added hud_interview ID to the selection table
--  8/4/08
--    Ensured that only priority 1 loans were processed
--  8/5/08
--    Changed date format to MM/DD/YYYY
--  8/19/08
--    Ensured that the primary residence was reported if multiple real property homes were entered
--    Added new values for chose not to respond for Race and Ethnicity
--  1/13/09
--    Modified AMI values B = > ami, C= 1.60, D=2.0

-- Suppress intermediate results
set nocount on

-- Default the values
if @to_date is null
	select	@to_date = getdate()

if @from_date is null
	select	@from_date = @to_date

-- Convert the dates to suitable form
select	@from_date = convert(varchar(10), @from_date, 101),
	@to_date = convert(varchar(10), @to_date, 101) + ' 23:59:59'

-- Delete the partial table from the previous execution
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##tmp_nfcc_nfmc_extract'' and type = ''U'') drop table ##tmp_nfcc_nfmc_extract' )

-- Find the grant type
declare	@hud_grant_type		int
select	@hud_grant_type	= oID
from	Housing_GrantTypes
where	description = 'Pending Housing'

-- If there is no grant for this type then just ignore the results.
if @hud_grant_type is null
	return;

-- Create the selection table for the clients to be reported
create table ##tmp_nfcc_nfmc_extract (
OrgID varchar(256) null,
BranchName varchar(256) null,
ClientID int not null,
CounselingLevel varchar(10) null,
CounselingIntakeDate datetime null,
CounselingMode varchar(10) null,
FirstName varchar(50) null,
LastName varchar(50) null,
Age varchar(10) null,
Race varchar(10) null,
Ethnicity varchar(10) null,
Gender varchar(10) null,
HouseholdType varchar(10) null,
HouseholdIncome varchar(50) null,
IncomeCategory varchar(10) null,
HouseNo varchar(50) null,
Street varchar(128) null,
City varchar(128) null,
[State] varchar(10) null,
Zip varchar(20) null,
Total_Individual_foreclosure_hours_received varchar(30) null,
Total_group_foreclosure_hours_received varchar(10) null,
NameofOriginatingLender varchar(256) null,
FDICofOriginalLender varchar(32) null,
OriginalLoanNumber varchar(50) null,
CurrentLoanServicer varchar(256) null,
Current_Servicer_FDIC varchar(50) null,
CurrentServicerLoanNo varchar(50) null,
CreditScore varchar(10) null,
ScoreType varchar(30) null,
PITIatIntake varchar(20) null,
LoanProductType varchar(10) null,
InterestOnly varchar(10) null,
Hybrid varchar(10) null,
OptionARM varchar(10) null,
VAorHFAInsured varchar(10) null,
PrivatelyHeld varchar(10) null,
ARMReset varchar(10) null,
DefaultReasonCode varchar(10) null,
LoanStatusAtContact varchar(10) null,
CounselingOutcomeCode varchar(30) null,
CounselingOutcomeDate datetime null,
indicator_date datetime null,
inhibit_date datetime null,
secured_property int null,
office int null,
hud_interview int null);

-- Load the client list into the table.
insert into ##tmp_nfcc_nfmc_extract (clientid)
select	c.client
from	clients c
inner join hud_ids id on c.client = id.client
where	c.client in (
	select	client
	from	hud_interviews
	where	interview_date between @from_date and @to_date
)
and	id.hud_type = @hud_grant_type;

-- Set the hud interview pointer
update ##tmp_nfcc_nfmc_extract
set hud_interview = i.hud_interview
from ##tmp_nfcc_nfmc_extract x
inner join hud_interviews i on x.clientid = i.client and i.interview_date between @from_date and @to_date;

-- Find the indicator to say that the privacy policy was signed
declare	@privacy_ind		int
-- select	@privacy_ind = indicator
-- from	indicators
-- where	description like 'NFMCP Privacy Policy%';

if @privacy_ind is not null
	update	##tmp_nfcc_nfmc_extract
	set	indicator_date = i.date_created
	from	##tmp_nfcc_nfmc_extract x
	inner join client_indicators i on x.clientid = i.client and @privacy_ind = i.indicator;

-- Find the indicator to decline authorization
declare	@inhibit_ind		int
select	@inhibit_ind = indicator
from	indicators
where	description like 'NFMCP decline Authorization%';

if @inhibit_ind is not null
	update	##tmp_nfcc_nfmc_extract
	set	inhibit_date = i.date_created
	from	##tmp_nfcc_nfmc_extract x
	inner join client_indicators i on x.clientid = i.client and @inhibit_ind = i.indicator;

-- Default the other columns to suitable values
update	##tmp_nfcc_nfmc_extract
set OrgID = '0000',
	CounselingLevel = '',
	CounselingMode = '2',
	FirstName = '',
	LastName = '',
	Age = '',
	Race = 10,
	Ethnicity = 2,
	Gender = 1,
	HouseholdType = 1,
	HouseholdIncome = '',
	IncomeCategory = '',
	HouseNo = '',
	Street = '',
	City = '',
	[State] = '',
	Zip = '',
	Total_Individual_foreclosure_hours_received = 0,
	Total_group_foreclosure_hours_received = 0,
	NameofOriginatingLender = '',
	FDICofOriginalLender = '',
	OriginalLoanNumber = '',
	CurrentLoanServicer = '',
	Current_Servicer_FDIC = '',
	CurrentServicerLoanNo = '',
	CreditScore = '',
	ScoreType = '',
	PITIatIntake = '',
	LoanProductType = 6,
	InterestOnly = 0,
	Hybrid = 0,
	OptionARM = 0,
	VAorHFAInsured = 0,
	PrivatelyHeld = 0,
	ARMReset = 0,
	DefaultReasonCode = 0,
	LoanStatusAtContact = 1,
	CounselingOutcomeCode = '';

-- Find the property field
update	##tmp_nfcc_nfmc_extract
set	secured_property = p.secured_property
from	##tmp_nfcc_nfmc_extract x
inner join secured_properties p on x.clientid = p.client
inner join secured_types t on p.secured_type = t.secured_type
where	t.auto_home_other	= 'H';

-- Prefer the primary residence
update	##tmp_nfcc_nfmc_extract
set	secured_property = p.secured_property
from	##tmp_nfcc_nfmc_extract x
inner join secured_properties p on x.clientid = p.client
inner join secured_types t on p.secured_type = t.secured_type
where	t.auto_home_other	= 'H'
and		p.primary_residence = 1;

-- Fill in the name and lender information
update	##tmp_nfcc_nfmc_extract
set	NameOfOriginatingLender	= l.case_number,
	OriginalLoanNumber = '',
	CurrentLoanServicer = l.lender,
	Current_Servicer_FDIC = '',
	CurrentServicerLoanNo = l.account_number
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority

-- Intererst rate reset
update	##tmp_nfcc_nfmc_extract
set	ARMReset = 1
from	##tmp_nfcc_nfmc_extract x
inner join client_indicators i on x.clientid = i.client
inner join indicators ix on i.indicator = ix.indicator
where	ix.description = 'NFMCP if ARM-interest rate reset?';

-- Level 1 counseling
update	##tmp_nfcc_nfmc_extract
set	CounselingLevel = CounselingLevel + '1'
from	##tmp_nfcc_nfmc_extract x
inner join client_indicators i on x.clientid = i.client
inner join indicators ix on i.indicator = ix.indicator
where	ix.description = 'NFMCP Level 1';

-- Level 2 counseling
update	##tmp_nfcc_nfmc_extract
set	CounselingLevel = CounselingLevel + '2'
from	##tmp_nfcc_nfmc_extract x
inner join client_indicators i on x.clientid = i.client
inner join indicators ix on i.indicator = ix.indicator
where	ix.description = 'NFMCP Level 2';

-- Level 3 counseling
update	##tmp_nfcc_nfmc_extract
set	CounselingLevel = CounselingLevel + '3'
from	##tmp_nfcc_nfmc_extract x
inner join client_indicators i on x.clientid = i.client
inner join indicators ix on i.indicator = ix.indicator
where	ix.description = 'NFMCP Level 3';

-- Update the counseling levels correctly
update	##tmp_nfcc_nfmc_extract
set	CounselingLevel = ''
where	CounselingLevel not in ('1', '2', '3');

-- Fill in the client information where possible
update ##tmp_nfcc_nfmc_extract
set	Street	= dbo.street_name ( c.address1 ),
	HouseNo	= dbo.house_number ( c.address1 ),
	City	= c.city,
	[State] = substring(st.mailingcode, 1, 2),
	Zip     = c.postalcode
from	##tmp_nfcc_nfmc_extract x
inner join clients c on c.client = x.clientid
left outer join states st on c.state = st.state;

-- Fill in the people information
update ##tmp_nfcc_nfmc_extract
set	CreditScore	= case when p.fico_score is null then '' when p.fico_score = 0 then '' else p.fico_score end,
	FirstName	= p.first,
	LastName	= p.last,
	Age		= datediff(year, p.birthdate, getdate()),
	race		= case p.race
				when 1 then 2		-- black
				when 2 then 1		-- asian
				when 3 then 4		-- white
				when 4 then 4		-- hispanic
				when 5 then 9		-- other multiracial
				when 6 then 9		-- east indian
				when 7 then 8		-- indian and black
				when 8 then 9		-- middle eastern
				when 9 then 0		-- american indian
				when 10 then 10		-- unspecified
				when 11 then 3		-- pacific islander
				when 12 then 5		-- american indian and white
				when 13 then 6		-- asian and white
				when 14 then 7		-- black and white
				else 9				-- all others
			end,
	Ethnicity	= p.hispanic,
	gender		= case p.gender
				when 1 then 1
				else 0
			  end
from	##tmp_nfcc_nfmc_extract x
inner join people p on p.client = x.clientid
where	p.relation = 1;

update	##tmp_nfcc_nfmc_extract
set	Ethnicity	= 2
where	race	= 10;

-- Set the hispanic marker if the race is hispanic
update	##tmp_nfcc_nfmc_extract
set	ethnicity	= 1
from	##tmp_nfcc_nfmc_extract x
inner join people p on p.client = x.clientid
where	p.relation = 1
and	p.race = 4;

-- Set the race to white if hispanic
update	##tmp_nfcc_nfmc_extract
set	race	= 4
where	ethnicity	= 1;

-- Mark the items that have no second applicant listed
update	##tmp_nfcc_nfmc_extract
set	HouseholdType	= 20
from	##tmp_nfcc_nfmc_extract x
left outer join people p on x.clientid = p.client and 2 = p.relation
where	p.client is null;

-- Update those to show more than one dependant
update	##tmp_nfcc_nfmc_extract
set	HouseholdType	= 21
from	##tmp_nfcc_nfmc_extract x
inner join clients c on x.clientid = c.client
where	x.HouseholdType	= 20
and	c.dependents > 0;

-- Fold the other types to married
update	##tmp_nfcc_nfmc_extract
set	HouseholdType	= 4
where	HouseholdType	= 1;

-- Update the items that are married to show children
update	##tmp_nfcc_nfmc_extract
set	HouseholdType	= 5
from	##tmp_nfcc_nfmc_extract x
inner join clients c on x.clientid = c.client
where	x.HouseholdType	= 4
and	c.dependents > 0;

-- Update the items to show single people
update	##tmp_nfcc_nfmc_extract
set	HouseholdType	= 2
where	HouseholdType	= 21
and	Gender		= 0;

update	##tmp_nfcc_nfmc_extract
set	HouseholdType	= 3
where	HouseholdType	= 21;

update	##tmp_nfcc_nfmc_extract
set	HouseholdType	= 1
where	HouseholdType	= 20;

-- Clear the undefined credit scores
update	##tmp_nfcc_nfmc_extract
set	CreditScore	 = ''
where	CreditScore = 0;

-- Set the source of the credit score
update	##tmp_nfcc_nfmc_extract
set	ScoreType	= 'Experian'
where	CreditScore	> 0;

-- Look at the first kept appointment for the client
update	##tmp_nfcc_nfmc_extract
set	CounselingMode	= case
		when m.nfcc = 'F' then 2
		when m.nfcc = 'P' then 1
		when m.nfcc = 'I' then 3
		when m.nfcc = 'E' then 3
		else 5
	end
from	##tmp_nfcc_nfmc_extract x
inner join clients c on x.clientid = c.client
inner join client_appointments ca on c.first_kept_appt = ca.client_appointment
inner join appt_types apt on ca.appt_type = apt.appt_type
inner join FirstContactTypes m on apt.contact_type = m.oID

-- Set the office from the client
update	##tmp_nfcc_nfmc_extract
set	office = c.office
from	##tmp_nfcc_nfmc_extract x
inner join clients c on x.clientid = c.client

-- Update the branch ID from the office field
update	##tmp_nfcc_nfmc_extract
set	BranchName = o.nfcc
from	##tmp_nfcc_nfmc_extract x
inner join offices o on x.office = o.office;

-- Household income
select	clientid, dbo.client_income (clientid) as income, dbo.ami(clientid) as ami, 'A' as householdlevel
into	#ami
from	##tmp_nfcc_nfmc_extract

update      #ami
set   householdlevel = 'B'
where income > ami;
 
update      #ami
set   householdlevel = 'C'
where income > ami * 1.60;

update      #ami
set   householdlevel = 'D'
where income > ami * 2.0;

update      #ami
set   householdlevel = ''
where ami = 0;


update	##tmp_nfcc_nfmc_extract
set	HouseholdIncome	= convert(varchar, convert(decimal(10,2), a.income)),
	IncomeCategory = a.householdlevel
from	##tmp_nfcc_nfmc_extract x
inner join #ami a on x.clientid = a.clientid;

drop table #ami

-- Split the lender name from the fdic and account number
update	##tmp_nfcc_nfmc_extract
set	FDICOfOriginalLender	= ltrim(rtrim(substring(NameOfOriginatingLender, charindex('/', NameOfOriginatingLender) + 1, 80)))
where	NameOfOriginatingLender like '[^/]%/%'


update	##tmp_nfcc_nfmc_extract
set	NameOfOriginatingLender = ltrim(rtrim(substring(NameOfOriginatingLender, 1, charindex('/', NameOfOriginatingLender) - 1)))
where	NameOfOriginatingLender like '[^/]%/%'

-- Split the account number from the fdic
update	##tmp_nfcc_nfmc_extract

set	OriginalLoanNumber	= ltrim(rtrim(substring(FDICofOriginalLender, charindex('/', FDICofOriginalLender) + 1, 80)))
where	FDICofOriginalLender like '[^/]%/%'

update	##tmp_nfcc_nfmc_extract
set	FDICofOriginalLender = ltrim(rtrim(substring(FDICofOriginalLender, 1, charindex('/', FDICofOriginalLender) - 1)))
where	FDICofOriginalLender like '[^/]%/%'

-- Try to split the FDIC number from the lender name
update	##tmp_nfcc_nfmc_extract
set	Current_Servicer_FDIC	= ltrim(rtrim(substring(CurrentLoanServicer, charindex('/', CurrentLoanServicer) + 1, 80)))
where	CurrentLoanServicer like '[^/]%/%'

update	##tmp_nfcc_nfmc_extract
set	CurrentLoanServicer = ltrim(rtrim(substring(CurrentLoanServicer, 1, charindex('/', CurrentLoanServicer) - 1)))
where	CurrentLoanServicer like '[^/]%/%'

-- PITI at intake
select	x.clientid as client,
	sum(l.payment) as PITIatIntake
into	#piti
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
group by x.clientid;

update	##tmp_nfcc_nfmc_extract
set	PITIatIntake = convert(varchar, convert(decimal(10,2), b.PITIatIntake))
from	##tmp_nfcc_nfmc_extract x
inner join #piti b on x.clientid = b.client;

drop table #piti
/*
-- Interest only
update	##tmp_nfcc_nfmc_extract
set	InterestOnly = 1
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
inner join Housing_LoanTypes m on l.loan_type = m.oID
where	m.description like '%interest%';

-- Option-ARM
update	##tmp_nfcc_nfmc_extract
set	OptionARM = 1
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
inner join Housing_LoanTypes m on l.loan_type = m.oID
where	' ' + m.description + ' ' like '% Option ARM %';

-- Hybrid
update	##tmp_nfcc_nfmc_extract
set	Hybrid = 1
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
inner join Housing_LoanTypes m on l.loan_type = m.oID
where	' ' + m.description + ' ' like '% Hybrid ARM %';

-- VAorFHAInsured
update	##tmp_nfcc_nfmc_extract
set	VAorHFAInsured = 1
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
inner join Housing_LoanTypes m on l.loan_type = m.oID
where	(' ' + m.description + ' ' like '% VA %' or ' ' + m.description + ' ' like '% FHA %');
*/
-- LoanProductType
update	##tmp_nfcc_nfmc_extract
set	LoanProductType = '6';
/*
update	##tmp_nfcc_nfmc_extract
set	LoanProductType = '1'
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
inner join Housing_LoanTypes m on l.loan_type = m.oID
where	' ' + m.description + ' ' like '% Fixed Rate %';

update	##tmp_nfcc_nfmc_extract
set	LoanProductType = '3'
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
inner join Housing_LoanTypes m on l.loan_type = m.oID
where	' ' + m.description + ' ' like '% ARM %';

-- Look at the rate and modify the types accordingly
update	##tmp_nfcc_nfmc_extract
set	LoanProductType = convert(int,LoanProductType) + 1
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority
where	l.interest_rate >= 0.08
*/
-- Change the invalid values to "other"
update	##tmp_nfcc_nfmc_extract
set	LoanProductType = '6'
where	LoanProductType not in ('1', '2', '3', '4');

-- Privately held is all non-va or fha loans
update	##tmp_nfcc_nfmc_extract
set	PrivatelyHeld = 1
where	VAorHFAInsured = 0;

-- DefaultReasonCode
update	##tmp_nfcc_nfmc_extract
set	DefaultReasonCode	= case
		when fin.nfcc = 'LOI' then 3				-- Loss of Income
		when fin.nfcc = 'IEX' then 5				-- Increase in Expense 
		when fin.nfcc = 'BVF' then 8				-- Business Venture Failed
		when fin.nfcc = 'ILP' then 9				-- Increase in Loan Payment
		
		when fin.rpps_code = 2 then 1				-- reduction in income
		when fin.rpps_code = 1 then 2				-- poor management skills
		when fin.rpps_code = 4 then 4				-- medical
		when fin.rpps_code = 3 then 6				-- divorce
		when fin.rpps_code = 5 then 7				-- death
		else 10
	end
from	##tmp_nfcc_nfmc_extract x
inner join clients c on x.clientid = c.client
inner join financial_problems fin on c.cause_fin_problem1 = fin.financial_problem;

-- LoanStatusAtContact
update	##tmp_nfcc_nfmc_extract
set	LoanStatusAtContact = case
		when l.past_due_periods <= 0 then 1
		when l.past_due_periods > 4 then 5
		else l.past_due_periods + 1
	end
from	##tmp_nfcc_nfmc_extract x
inner join secured_loans l on x.secured_property = l.secured_property and 1 = l.priority

-- Find the total amount of time spent so far on the client case
select	x.clientid,
	sum(t.minutes) as minutes
into	#hours
from	##tmp_nfcc_nfmc_extract x
inner join hud_transactions t on x.hud_interview = t.hud_interview
group by x.clientid;

update	##tmp_nfcc_nfmc_extract
set	Total_Individual_foreclosure_hours_received = round(convert(float, t.minutes) / 60.0, 3)
from	##tmp_nfcc_nfmc_extract x
inner join #hours t on x.clientid = t.clientid;

drop table #hours;

-- Find the total amount of time spent so far on the client case
select	x.clientid, sum(t.duration) as minutes
into	#workshop
from	##tmp_nfcc_nfmc_extract x
inner join client_appointments ca on x.ClientID = ca.client AND ca.start_time between @from_date and @to_date and ca.workshop is not null and ca.office is null and ca.status in ('K','W')
inner join workshops w on ca.workshop = w.workshop
inner join workshop_types t on w.workshop_type = t.workshop_type
where	t.workshop_type in ( -- Make sure that we count workshops only once.
	select	wc.workshop_type
	from	workshop_contents wc
	inner join workshop_content_types wct on wc.content_type = wct.content_type
	where	wct.hud_9902_section = '6.c'
)
group by x.clientid;

update	##tmp_nfcc_nfmc_extract
set	Total_group_foreclosure_hours_received = round(convert(float, t.minutes) / 60.0, 3)
from	##tmp_nfcc_nfmc_extract x
inner join #workshop t on x.clientid = t.clientid;

drop table #workshop;

-- CounselingIntakeDate
update	##tmp_nfcc_nfmc_extract
set	CounselingIntakeDate = i.interview_date
from	##tmp_nfcc_nfmc_extract x
inner join hud_interviews i on x.hud_interview = i.hud_interview

-- Add clients who have results but are not in the list
insert into ##tmp_nfcc_nfmc_extract (clientid, hud_interview)
select	iv.client, iv.hud_interview
from	hud_interviews iv
inner join hud_ids id on iv.client = id.client
where	id.hud_type = @hud_grant_type
and	iv.client not in (
	select	ClientID
	from	##tmp_nfcc_nfmc_extract)
and	iv.result_date between @from_date and @to_date;

-- CounselingOutcomeDate
update	##tmp_nfcc_nfmc_extract
set	CounselingOutcomeDate	= i.result_date
from	##tmp_nfcc_nfmc_extract x
inner join hud_interviews i on x.hud_interview = i.hud_interview;

-- CounselingOutcomeCode
update	##tmp_nfcc_nfmc_extract
set	CounselingOutcomeCode	= case h.section_label
			when '7.b.01' then 1
			when '7.b.02' then 15
			when '7.b.03' then 16
			when '7.b.04' then 17
			when '7.b.05' then 2
			when '7.b.06' then 3
			when '7.b.07' then 4
			when '7.b.08' then 51
			when '7.b.09' then 5
			when '7.b.10' then 52
			when '7.b.11' then 53
			when '7.b.12' then 54
			when '7.b.13' then 55
			when '7.b.14' then 56
			when '7.b.15' then 18
			when '7.b.16' then 57
			else 20
		end
		
from	##tmp_nfcc_nfmc_extract x
inner join hud_interviews i on x.hud_interview = i.hud_interview
inner join hud_cars_9902_summary h on h.hud_interview = i.interview_type and h.hud_result = i.hud_result;

-- Create the table for the results and populate it
create table #results (location varchar(80), value varchar(80));

-- Discard the clients who have indicated that they have NOT signed the privacy policy.
-- This only applies to phone counseling.
delete
from	##tmp_nfcc_nfmc_extract
where	CounselingMode = 1
and	indicator_date is null
and	datediff(d, CounselingIntakeDate, getdate()) < 30;

-- Toss the clients who inhibit data. But count them first.
insert into #results (location, value)
select	'B4', convert(varchar,count(*))
from	##tmp_nfcc_nfmc_extract
where	inhibit_date is not null;

delete
from	##tmp_nfcc_nfmc_extract
where	inhibit_date is not null;

-- Serialize the line numbers in the table
create table #lines (clientid int, BranchName varchar(50) null, line int identity(7,1));

insert into #lines (clientid, BranchName)
select clientid, BranchName
from ##tmp_nfcc_nfmc_extract
order by 2, 1;

insert into #results (location, value)
select 'A' + convert(varchar,line), convert(varchar, line - 6)
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'B' + convert(varchar,line), OrgID
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'C' + convert(varchar,line), x.BranchName
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'D' + convert(varchar,line), x.ClientID
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'E' + convert(varchar,line), CounselingLevel
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'F' + convert(varchar,line), convert(varchar(10), CounselingIntakeDate, 101)
-- select 'F' + convert(varchar,line), dbo.date_dd_mm_yyyy(CounselingIntakeDate)
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'G' + convert(varchar,line), CounselingMode
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'H' + convert(varchar,line), FirstName
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'I' + convert(varchar,line), LastName
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'J' + convert(varchar,line), Age
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'K' + convert(varchar,line), Race
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'L' + convert(varchar,line), Ethnicity
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'M' + convert(varchar,line), Gender
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'N' + convert(varchar,line), HouseholdType
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'O' + convert(varchar,line), HouseholdIncome
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'P' + convert(varchar,line), IncomeCategory
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'Q' + convert(varchar,line), HouseNo
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'R' + convert(varchar,line), Street
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'S' + convert(varchar,line), City
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'T' + convert(varchar,line), [State]
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'U' + convert(varchar,line), Zip
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'V' + convert(varchar,line), isnull(Total_Individual_foreclosure_hours_received,'0')
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'W' + convert(varchar,line), isnull(Total_group_foreclosure_hours_received,'0')
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'X' + convert(varchar,line), NameofOriginatingLender
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'Y' + convert(varchar,line), FDICofOriginalLender
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'Z' + convert(varchar,line), OriginalLoanNumber
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AA' + convert(varchar,line), CurrentLoanServicer
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AB' + convert(varchar,line), Current_Servicer_FDIC
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AC' + convert(varchar,line), CurrentServicerLoanNo
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AD' + convert(varchar,line), CreditScore
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AE' + convert(varchar,line), ScoreType
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AF' + convert(varchar,line), PITIatIntake
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AG' + convert(varchar,line), LoanProductType
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AH' + convert(varchar,line), InterestOnly
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AI' + convert(varchar,line), Hybrid
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AJ' + convert(varchar,line), OptionARM
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AK' + convert(varchar,line), VAorHFAInsured
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AL' + convert(varchar,line), PrivatelyHeld
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AM' + convert(varchar,line), ARMReset
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AN' + convert(varchar,line), DefaultReasonCode
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AO' + convert(varchar,line), LoanStatusAtContact
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AP' + convert(varchar,line), CounselingOutcomeCode
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

insert into #results (location, value)
select 'AQ' + convert(varchar,line), convert(varchar(10), CounselingOutcomeDate, 101)
-- select 'AQ' + convert(varchar,line), dbo.date_dd_mm_yyyy(CounselingOutcomeDate)
from ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.clientid = l.clientid;

-- Return the results
select 'Sheet1' as sheet, location, value
from #results
where value is not null
order by location;

drop table #results;
-- drop table ##tmp_nfcc_nfmc_extract;
return ( 0 );
GO
