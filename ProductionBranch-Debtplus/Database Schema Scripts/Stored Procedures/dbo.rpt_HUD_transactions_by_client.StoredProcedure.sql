USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_HUD_transactions_by_client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_HUD_transactions_by_client]  ( @client as int, @FromDate as datetime = null, @ToDate as datetime = null ) as

-- =============================================================================================================
-- ==           Select the transactions for the client                                                        ==
-- =============================================================================================================

set nocount on

-- ChangeLog
--   1/5/2003
--     Suppressed descriptions if the date for the interview or description is out of the range

if @FromDate is not null
begin
	if @ToDate is null
		select	@toDate = getdate()

	select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
		@ToDate = convert(datetime, convert(varchar(10), @ToDate, 101) + ' 23:59:59')

	select		i.client,
			case isnull(d.client_appointment,0) when 0 then 'CL' else 'AP' end as source,

			case
				when i.interview_date between @FromDate and @ToDate then i.interview_date
				else null
			end as 'item_date',

			0 as units,

			d.minutes as duration,

			case
				when i.interview_date between @FromDate and @ToDate then isnull(iv.description,'UNKNOWN')
				else null
			end as interview_type,

			case
				when i.result_date between @FromDate and @ToDate then isnull(r.description,'UNKNOWN')
				else null
			end as results,

			trm.description as termination_reason,

			d.created_by,
			d.date_created

	from		hud_transactions d
	inner join	hud_interviews i on d.hud_interview = i.hud_interview
	LEFT OUTER JOIN housing_PurposeOfVisitTypes iv	WITH (NOLOCK) ON i.interview_type = iv.oID
	LEFT OUTER JOIN housing_ResultTypes r			WITH (NOLOCK) ON i.hud_result = r.oID
	LEFT OUTER JOIN housing_TerminationReasonTypes trm WITH (NOLOCK) ON i.termination_reason = trm.oID
	where		i.client	= @client
	and			d.date_created between @fromDate and @toDate
	order by	d.date_created

end else

	select		i.client,
			case isnull(d.client_appointment,0) when 0 then 'CL' else 'AP' end as source,
			i.interview_date as item_date,
			0 as units, d.minutes as duration,
			trm.description as interview_type,
			r.description as results,

			trm.description as termination_reason,

			d.created_by,
			d.date_created

	from		hud_transactions d
	inner join	hud_interviews i on d.hud_interview = i.hud_interview
	LEFT OUTER JOIN housing_PurposeOfVisitTypes iv	WITH (NOLOCK) ON i.interview_type = iv.oID
	LEFT OUTER JOIN housing_ResultTypes r			WITH (NOLOCK) ON i.hud_result = r.oID
	LEFT OUTER JOIN housing_TerminationReasonTypes trm WITH (NOLOCK) ON i.termination_reason = trm.oID
	where		i.client	= @client
	order by	d.date_created

return ( @@rowcount )
GO
