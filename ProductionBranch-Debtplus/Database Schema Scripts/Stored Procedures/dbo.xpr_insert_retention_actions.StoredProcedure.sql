USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_retention_actions]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_retention_actions] ( @description as varchar(50), @letter_code as int = null ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the retention_actions table               ==
-- ========================================================================================
	insert into retention_actions ( [description], [letter_code] ) values ( @description, @letter_code )
	return ( scope_identity() )
GO
