USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_zipcodes]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_zipcodes] ( @Office as int, @Zipcode as varchar(5) ) as
    insert into zipcodes ( [office], [zip_lower], [zip_upper] ) values ( @Office, @Zipcode + '0000', @Zipcode + '9999' )
    return ( scope_identity() )
GO
