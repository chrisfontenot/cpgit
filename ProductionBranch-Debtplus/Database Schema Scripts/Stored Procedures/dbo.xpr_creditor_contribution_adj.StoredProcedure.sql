USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_adj]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_adj] ( @invoice_register AS INT = NULL, @amount AS Money = 0, @reference AS VarChar(256) = NULL ) AS

-- ======================================================================================================
-- ==             Adjust this invoice the indicated amount                                             ==
-- ======================================================================================================

--   2/20/2002
--     Changed to xpr_creditor_contribution_adj to make similar to the other items in the database.

SET NOCOUNT ON

-- Fetch the creditor from the invoice
DECLARE	@creditor	VarChar(10)

SELECT	@creditor		= creditor
FROM	registers_invoices
WHERE	invoice_register	= @invoice_register

if @reference is not null
begin
	select @reference = ltrim(rtrim(@reference))
	if @reference = ''
		select @reference = null
end

IF @creditor IS NOT NULL
BEGIN
	-- Log the payment information
	INSERT INTO registers_creditor	(tran_type,	creditor,	invoice_register,	credit_amt,	message)
	VALUES				('RA',		@creditor,	@invoice_register,	@amount,	@reference)

	-- Adjust the invoice to indicate the payment
	UPDATE	registers_invoices
	SET	adj_amount		= adj_amount + @amount,
		adj_date		= getdate()
	WHERE	invoice_register	= @invoice_register
END

-- Return success to the caller
RETURN ( 1 )
GO
