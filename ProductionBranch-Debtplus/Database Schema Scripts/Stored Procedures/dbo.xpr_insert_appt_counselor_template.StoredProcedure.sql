USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_appt_counselor_template]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_appt_counselor_template] ( @appt_time_template as int, @counselor as int ) as
	declare	@answer	int
	select	@answer = appt_counselor_template
	from	appt_counselor_templates
	where	appt_time_template = @appt_time_template
	and		counselor = @counselor
	
	if @answer is null
	begin
		insert into appt_counselor_templates([appt_time_template], [counselor]) values (@appt_time_template, @counselor)
		select	@answer = SCOPE_IDENTITY()
	end
	
	return ( @answer )
GO
