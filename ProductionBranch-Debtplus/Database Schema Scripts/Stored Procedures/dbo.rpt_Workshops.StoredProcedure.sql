USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Workshops]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Workshops] ( @Workshop AS INT = NULL ) AS
-- ======================================================================================================
-- ==                   Retrieve a list of pending workshops                                           ==
-- ======================================================================================================
IF @Workshop IS NULL
	SELECT @Workshop = 0

IF @Workshop <= 0
BEGIN
	SELECT  w.workshop,
		w.start_time,
		w.seats_available,
		t.description as 'workshop_description',
		t.duration as 'workshop_duration',
		w.workshop_location as 'workshop_location',
		dbo.format_normal_name(default,con.first,default,con.last,default) as 'counselor',

		-- Guest speakers
		g.name      as Guest_Name,
		g.Email     as Guest_Email,
		g.Telephone as Guest_Telephone

	FROM    workshops w
	LEFT OUTER JOIN workshop_types t ON w.workshop_type = t.workshop_type
	LEFT OUTER JOIN counselors co ON w.counselor = co.person
    left outer join names con with (nolock) on co.NameID = con.Name
	left outer join workshop_guests g WITH (NOLOCK) ON w.Guest = g.oID

	WHERE w.start_time >= getdate()
END

ELSE

BEGIN
	SELECT  w.workshop,
		w.start_time,
		w.seats_available,
        t.description as 'workshop_description',
		t.duration as 'workshop_duration',
		w.workshop_location as 'workshop_location',
		dbo.format_normal_name(default,con.first,default,con.last,default) as 'counselor',
		
		-- Guest speakers
		g.name      as Guest_Name,
		g.Email     as Guest_Email,
		g.Telephone as Guest_Telephone

	FROM    workshops w
	LEFT OUTER JOIN workshop_types t ON w.workshop_type = t.workshop_type
	LEFT OUTER JOIN counselors co ON w.counselor = co.person
    left outer join names con with (nolock) on co.NameID = con.name
	left outer join workshop_guests g WITH (NOLOCK) ON w.Guest = g.oID
	WHERE	w.workshop = @Workshop
END

RETURN ( @@rowcount )
GO
