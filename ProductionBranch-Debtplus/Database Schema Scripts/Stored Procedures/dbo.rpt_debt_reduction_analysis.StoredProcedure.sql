USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_debt_reduction_analysis]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_debt_reduction_analysis] ( @From_Date as datetime = null, @To_Date as datetime = null ) as

set nocount on

if @To_date is null
	select	@To_date = getdate()

if @From_Date is null
	select	@From_date = @To_date

select	@From_date = convert(varchar(10), @From_date, 101) + ' 00:00:00',
	@To_date   = convert(varchar(10), @To_date, 101)   + ' 23:59:59'

select	convert(varchar(80), isnull(o.name,convert(varchar, c.office))) as office,
	cc.client, dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix ) as client_name,

	c.start_date, c.active_status, c.last_deposit_date,

	convert(datetime,
	case
		when c.active_status = 'I' then c.drop_date
		else null
	end) as drop_date,

	convert(varchar(80), case
		when c.active_status = 'I' then isnull (dr.description, c.drop_reason_other)
		else null
	end) as drop_reason,

	sum ( b.orig_balance ) as orig_balance, sum ( b.orig_balance + b.orig_balance_adjustment ) as orig_balance_adjustment,
	sum ( b.total_payments ) as total_payments,
	sum ( b.total_interest ) as total_interest,
	sum ( b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments ) as current_balance

from	client_creditor cc with (nolock)
inner join client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
inner join creditors cr with (nolock) on cc.creditor = cr.creditor
left outer join creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class
inner join clients c with (nolock) on cc.client = c.client
left outer join drop_reasons dr with (nolock) on c.drop_reason = dr.drop_reason
left outer join people p with (nolock) on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join offices o with (nolock) on c.office = o.office

where	isnull(ccl.agency_account,0) = 0
and	cc.reassigned_debt = 0
and	c.start_date between @From_Date and @To_Date

group by o.name, c.office, c.last_deposit_date, c.start_date, c.active_status, c.drop_reason, dr.description, c.drop_reason_other, c.drop_date, cc.client, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix

order by o.name, c.start_date, cc.client

return ( @@rowcount )
GO
