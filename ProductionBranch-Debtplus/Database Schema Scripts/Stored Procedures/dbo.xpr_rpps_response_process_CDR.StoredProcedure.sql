USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_response_process_CDR]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_response_process_CDR] ( @rpps_response_file as int ) AS
-- ====================================================================================================================
-- ==            Mark the proposal information as being rejected                                                     ==
-- ====================================================================================================================

-- Find the proposal from the trace table
begin transaction
set nocount on
set xact_abort on

-- Find the proposals that are being rejected
select	d.rpps_response_detail,
		d.rpps_transaction,
		t.trace_number_first as trace_number,
		t.client_creditor_proposal,

		pr.client_creditor,
		pr.proposal_batch_id,
		pr.proposed_amount,
		pr.proposed_balance,
		pr.proposed_start_date,

		d.return_code,
		convert(int, null) as proposal_reject_reason,
		x.ssn,
		x.client,
		x.reject_reason,
		x.counter_offer,
		x.additional_info_required,
		x.resubmit_date,
		x.cycle_months_remaining,
		x.forgiven_pct,
		x.first_payment_date,
		x.good_thru_date,
		x.ineligible_reason,
		x.internal_program_ends_date,
		x.interest_rate,
		x.third_party_detail,
		x.third_party_contact

into	#rpps_response_cdr

from	rpps_response_details d
inner join rpps_response_details_cdr x	on d.rpps_response_detail = x.rpps_response_detail
inner join rpps_transactions t		on d.rpps_transaction = t.rpps_transaction
inner join client_creditor_proposals pr	on t.client_creditor_proposal = pr.client_creditor_proposal

where	d.rpps_response_file		= @rpps_response_file
and	d.processing_error		is null
and	d.service_class_or_purpose	= 'CDR'
and	t.service_class_or_purpose	= 'CDP'

-- Mark all other proposals as "unknown"
update	rpps_response_details
set		processing_error		= 'UNKNOWN PROPOSAL'
where	rpps_response_file		= @rpps_response_file
and		service_class_or_purpose	= 'CDR'
and		processing_error is null
and		rpps_response_detail not in (
		select	rpps_response_detail
		from	#rpps_response_cdr
);

-- Do nothing more if there are no proposals to be processed
if not exists (select * from #rpps_response_cdr)
begin
	drop table #rpps_response_cdr
	commit transaction
	return ( 1 )
end

-- Attempt to translate the response code to our value
update	#rpps_response_cdr
set		proposal_reject_reason	= r.proposal_result_reason
from	#rpps_response_cdr x
inner join proposal_result_reasons r on ltrim(rtrim(r.rpps_code)) = ltrim(rtrim(x.reject_reason))
where	x.proposal_reject_reason is null;

declare	@default_reason_code	int
select	@default_reason_code = min(proposal_result_reason)
from	proposal_result_reasons;

update	#rpps_response_cdr
set		proposal_reject_reason		= isnull(@default_reason_code, 1)
where	proposal_reject_reason is null;

-- Mark the transactions as being rejected
update	rpps_transactions
set		return_code = 'CDR'
where	rpps_transaction in (
		select	rpps_transaction
		from	#rpps_response_cdr
);

-- Mark the proposals as rejected
update	client_creditor_proposals
set		proposal_status		= 4,
		proposal_status_date	= getdate()
where	client_creditor_proposal in (
		select	client_creditor_proposal
		from	#rpps_response_cdr
);

-- Mark the proposal as being rejected
update	client_creditor_proposals
set		proposal_status					= 4,
		proposal_status_date			= getdate(),
		proposal_reject_reason			= x.proposal_reject_reason,
		counter_amount					= x.counter_offer,
		missing_item					= x.additional_info_required,
		rpps_resubmit_date				= x.resubmit_date,
		rpps_cycle_months_remaining		= x.cycle_months_remaining,
		rpps_forgiven_pct				= x.forgiven_pct,
		rpps_first_payment_date			= x.first_payment_date,
		rpps_good_thru_date				= x.good_thru_date,
		rpps_ineligible_reason			= x.ineligible_reason,
		rpps_internal_program_ends_date	= x.internal_program_ends_date,
		rpps_interest_rate				= x.interest_rate,
		rpps_third_party_detail			= x.third_party_detail,
		rpps_third_party_contact		= x.third_party_contact
from	client_creditor_proposals p
inner join #rpps_response_cdr x on p.client_creditor_proposal = x.client_creditor_proposal

-- Insert a system note for the transactions
insert into client_notes (client, client_creditor, is_text, type, dont_edit, dont_delete, dont_print, subject, note)
select		cc.client,
			cc.client_creditor,
			1 as 'is_text',
			3 as 'type',
			1 as 'dont_edit',
			1 as 'dont_delete',
			0 as 'dont_print',
			'Proposal Rejected by RPPS System' as 'subject',

			'The proposal was rejected by the creditor.' + char(13) + char(10) +
			'The proposed amount was $' + convert(varchar, p.proposed_amount) + char(13) + char(10) +
			'The proposed balance was $' + convert(varchar, p.proposed_balance) + char(13) + char(10) +
			'The proposed start date was ' + convert(varchar, p.proposed_start_date) + char(13) + char(10) +
			char(13) + char(10) +
			'The reject reason was ' + coalesce(x.reject_reason,'') + char(13) + char(10) +
			'The counter offer was $' + isnull(convert(varchar, x.counter_offer, 1), '0.00') +
			char(13) + char(10) +
			'The proposal was sent out in batch number ' + convert(varchar, x.proposal_batch_id) + ' dated ' + convert(varchar(10), b.date_created, 101) + char(13) + char(10) +
			'It was rejected in the batch identified as ' + convert(varchar, @rpps_response_file) + char(13) + char(10) +
			'The batch was processed by ' + suser_sname() + char(13) + char(10)

from		client_creditor_proposals p	with (nolock)
inner join client_creditor cc			with (nolock) on p.client_creditor = cc.client_creditor
inner join proposal_batch_ids b			with (nolock) on p.proposal_batch_id = b.proposal_batch_id
inner join #rpps_response_cdr x			on p.client_creditor_proposal = x.client_creditor_proposal
	
-- Cleanup
drop table #rpps_response_cdr
commit transaction
return ( 1 )
GO
