USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_response_cr]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_epay_response_cr] (
	@epay_response_file as int,
	@transaction_id as varchar(27),
	@transaction_type as varchar(2),
	@response_date as datetime,
	@agency_name as varchar(35),
	@agency_id as  varchar(12),
	@creditor_id as varchar(12),
	@creditor_name as varchar(35),
	@client_number as int,
	@client_name as varchar(35),
	@response_originator as varchar(1),
	@customer_biller_account_number as varchar(32),
	@current_client_balance as money,
	@date_of_balance  as datetime,
	@date_of_last_payment as datetime
) as
	insert into epay_responses_cr (
		epay_response_file,
		transaction_id,
		response_date,
		agency_name,
		agency_id,
		creditor_id,
		creditor_name,
		client_number,
		client_name,
		response_originator,
		customer_biller_account_number,
		current_client_balance,
		date_of_balance,
		date_of_last_payment
	) values (
		@epay_response_file,
		@transaction_id,
		@response_date,
		@agency_name,
		@agency_id,
		@creditor_id,
		@creditor_name,
		@client_number,
		@client_name,
		@response_originator,
		@customer_biller_account_number,
		@current_client_balance,
		@date_of_balance,
		@date_of_last_payment
	)
return ( @@identity )
GO
