USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_bankruptcy]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_bankruptcy] AS

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
from	BankruptcyClassTypes with (nolock)
order by 2
GO
