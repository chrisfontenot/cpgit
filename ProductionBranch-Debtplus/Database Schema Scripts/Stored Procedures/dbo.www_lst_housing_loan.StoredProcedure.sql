USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_lst_housing_loan]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_lst_housing_loan] AS

-- ============================================================================================================
-- ==            Provide access to the resource types for the www_user that does not have access to lst_*    ==
-- ============================================================================================================

select	oID								as item_key,
		description						as description,
		[default]						as [default],
		ActiveFlag						as ActiveFlag
from	housing_mortgagetypes with (nolock)

-- Put fixed loans first, then ARM loans, then everything else
order by case when description like '% year ARM' then 2
			  when description like '% year fixed' then 1
			  else 3 end,

-- Secondly, put the loans in order by the term years
		 case when description like '[0-9] %'      then '0' + substring(description, 1, 1)
		      when description like '[0-9][0-9] %' then substring(description, 1, 2)
			  else '00' end,

-- Lastly, use the description and then the ID
		 description, oID

return ( @@rowcount )
GO
