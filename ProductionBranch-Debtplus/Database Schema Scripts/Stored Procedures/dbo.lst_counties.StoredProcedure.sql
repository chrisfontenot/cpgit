USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_counties]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_counties] ( @state as int = null ) AS

-- ========================================================================================================
-- ==            Return the list of counties                                                             ==
-- ========================================================================================================

-- ChangeLog
--    6/20/2007
--       Added @state parameter

if isnull(@state,0) <= 0
	select	county			as item_key,
		name			as description,
		[default]		as [default],
		ActiveFlag		as ActiveFlag
	FROM	counties with (nolock)
	order by 2

else

	select	county			as item_key,
		name			as description,
		[default]		as [default],
		ActiveFlag		as ActiveFlag
	FROM	counties with (nolock)
	WHERE	state			= @state
	AND	state			<> 0

	UNION ALL

	select	county			as item_key,
		name			as description,
		[default]		as [default],
		ActiveFlag		as ActiveFlag
	FROM	counties with (nolock)
	WHERE	state			= 0

	order by 2

return ( @@rowcount )
GO
