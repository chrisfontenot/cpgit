USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_create_client]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_create_client] as
-- ============================================================================================================
-- ==            Create the intake session for the client                                                    ==
-- ============================================================================================================

-- Initialize to process the request
set nocount on

declare	@intake_id		varchar(20)
declare	@intake_client	int
declare	@date_start		datetime
declare	@date_stop		datetime
declare	@id_value		int

select	@date_start = convert(varchar(10), getdate(), 101),
		@date_stop  = dateadd(d, 1, @date_start)

-- Create the client information
insert into intake_clients	(housing_status,  housing_type,  referred_by,  cause_fin_problem1,  marital_status, people, bankruptcy) values (4, 10, null, null, 1, 1, 0)
select @intake_client = SCOPE_IDENTITY()

-- Update the id for the client
select	@intake_id = max(intake_id)
from	intake_clients
where	date_created >= @date_start
and		date_created <  @date_stop;

-- If there is no number then use 0000
if @intake_id is null
	select	@id_value = 0
else
	select	@id_value = convert(int, right(@intake_id, 4)) + 1

-- Create the session ID value from the current date and ID
select	@intake_id = right('0000' + convert(varchar, year(getdate())), 4) +
					 right('00'   + convert(varchar, month(getdate())), 2) +
				     right('00'   + convert(varchar, day(getdate())), 2) +
				     right('0000' + convert(varchar, @id_value), 4)

-- Set the session ID string as appropriate
update	intake_clients
set		intake_id		= @intake_id
where	intake_client	= @intake_client;

-- Return the result to the caller
select	@intake_client		as 'intake_client',
		@intake_id			as 'intake_id'
return ( @intake_client )
GO
