USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintChecks_Mark_Selected]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_PrintChecks_Mark_Selected] ( @bank as int = -1, @Marker AS VarChar(50) = null, @TrustRegister AS INT = -1 ) AS

-- ================================================================================================
-- ==              Generate a specific check from the list of pending checks                     ==
-- ================================================================================================

-- Do not generate interemediate results
SET NOCOUNT ON

if isnull(@bank,-1) <= 0
	select	@bank	= min(bank)
	from	banks with (nolock)
	where	type	= 'C'

-- Update the trust register to indicate the proper sequence
UPDATE	registers_trust
SET	sequence_number	= @Marker
WHERE	trust_register	= @TrustRegister
AND	cleared = 'P'
AND	((creditor is not null) OR (client is not null))
AND	amount > 0.0
--and	bank	= @bank

-- Return the number of rows in the result set
RETURN ( @@rowcount )
GO
