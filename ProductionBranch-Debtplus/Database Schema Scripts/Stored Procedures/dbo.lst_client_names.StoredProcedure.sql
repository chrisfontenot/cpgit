USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_client_names]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_client_names] ( @client as int ) as

-- ==============================================================================
-- ==            Return the list of client names                               ==
-- ==============================================================================

set nocount on

select	person							as 'item_key',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'description'
from	people p with (nolock)
left outer join names pn with (nolock) on p.NameID = pn.Name
where	p.client	= @client

return ( @@rowcount )
GO
