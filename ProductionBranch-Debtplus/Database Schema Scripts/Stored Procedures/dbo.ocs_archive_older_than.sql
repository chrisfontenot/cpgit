USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[ocs_archive_older_than]    Script Date: 06/18/2015 12:25:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[ocs_archive_older_than] 
	@UserName varchar(50) = 'System',
	@Program int = 0,
	@BeforeDate datetime = '12-1-2000',
	@NumberArchived int OUTPUT,
	@Success bit OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	begin transaction

	begin try

		declare @report int 
		set @report = null

		insert into OCS_ArchiveReport ([User],ArchiveDate,RecordsPriorTo,Program)
		VALUES (@UserName,getdate(),@BeforeDate,@Program)

		set @report = SCOPE_IDENTITY()

		update OCS_Client
		set Archive = 1,
		ActiveFlag = 0,
		ArchiveAttempt = @report
		where ClientId in (
			select ocs.ClientId from OCS_Client [ocs]
			left join OCS_UploadReport [report] on report.AttemptId = ocs.UploadAttempt
			where ocs.Archive = 0
			and report.UploadDate < @BeforeDate
			and ocs.Program = @Program
		)

		set @NumberArchived = @@ROWCOUNT
		set @Success = 1

		update OCS_ArchiveReport
		set RecordsArchived = @NumberArchived
		where ID = @report
				
        commit transaction

	end try

	begin catch
	  rollback transaction
	  set @Success = 0
	end catch

END


