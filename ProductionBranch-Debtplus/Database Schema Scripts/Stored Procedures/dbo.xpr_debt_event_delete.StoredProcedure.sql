USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_event_delete]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_debt_event_delete] ( @client_creditor_event as int ) AS

-- ============================================================================================================
-- ==            Delete the event from the system                                                            ==
-- ============================================================================================================

-- Complain if the record has been "used"
if exists (select * from client_creditor_events where date_updated is not null and client_creditor_event = @client_creditor_event)
begin
	RaisError ('The record has been updated in the database and may not be edited.', 16, 1)
	rollback transaction
	return ( 0 )
end

delete
from	client_creditor_events
where	client_creditor_event	= @client_creditor_event

if @@rowcount < 0
begin
	RaisError ('The indicated event could not be located in the system at the present time.', 16, 1)
	rollback transaction
	return ( 0 )
end

return ( 1 )
GO
