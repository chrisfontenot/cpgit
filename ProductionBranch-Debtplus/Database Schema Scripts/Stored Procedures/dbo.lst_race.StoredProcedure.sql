USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_race]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_race] AS

-- ===================================================================================================
-- ==                Return the list of ethnic backgrounds                                          ==
-- ===================================================================================================

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
FROM	RaceTypes
order by 2

return ( @@rowcount )
GO
