SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_trust_register_create_RR] ( @creditor as typ_creditor, @amount AS Money = 0, @item_date AS DateTime = NULL, @cleared AS VarChar(1) = NULL, @checknum as BigInt = NULL, @bank as int = 1 ) AS

-- ChangeLog
--   11/20/2003
--     Added bank number to parameter list

-- ====================================================================================================
-- ==            Create a row in the trust register for RPS Reject deposits                          ==
-- ====================================================================================================

SET NOCOUNT ON

-- Ensure that the amount is valid
IF @Amount < 0
BEGIN
	RaisError (50019, 16, 1)
	Return ( 0 )
END

-- Default the item date
IF @Item_Date IS NULL
	SET @Item_Date = getdate()

if @cleared is null
	set @cleared = ' '

DECLARE	@trust_register		INT

-- Insert the item into the trust register
INSERT INTO	registers_trust (tran_type,	date_created,	amount,		cleared,	bank)
VALUES				('RR',		@Item_Date,	@Amount,	@cleared,	@bank)

SELECT @trust_register = SCOPE_IDENTITY()

-- Return the trust register ID to the caller
RETURN ( @trust_register )
GO
