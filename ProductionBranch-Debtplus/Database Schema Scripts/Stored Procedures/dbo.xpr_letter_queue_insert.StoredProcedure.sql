USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_letter_queue_insert]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_letter_queue_insert] (@queue_name as varchar(50), @letter_type as varchar(10), @client as int = -1, @creditor as varchar(10) = null, @top_margin as float = 0.0, @bottom_margin as float = 0.0, @left_margin as float = 0.0, @right_margin as float = 0.0) as
-- =======================================================================================================================
-- ==       Create an entry in the letter_queue table for queueing a letter to be printed                               ==
-- =======================================================================================================================

declare	@priority	int
declare	@sort_order	varchar(15)
declare	@creditor_id int

-- Find the sort order key for the item
if @client >= 0
begin

	-- If this is a debt note, the client is present as well as the creditor. Use the client's zipcode.
	-- Find the information from the client
	select	@priority	= 9,
			@sort_order	= a.PostalCode
	from	clients c with (nolock)
	left outer join addresses a on c.addressid = a.address
	where	c.client		= @client

end else begin

	-- Find the creditor label from the creditor id. The creditor_address table is still in creditor_label order.
	select	@priority	= mail_priority,
			@creditor_id	= creditor_id
	from	creditors with (nolock)
	where	creditor	= @creditor

	-- Try the zipcode for the letter address
	select	@sort_order	= PostalCode
	from	view_creditor_addresses with (nolock)
	where	type		= 'L'
	and		creditor	= @creditor

	-- If there is no zipcode for letters, try the payment address
	if @sort_order is null
		select	@sort_order	= PostalCode
		from	view_creditor_addresses with (nolock)
		where	type		= 'P'
		and		creditor	= @creditor
end

-- The priority must be between 0 and 9
if @priority is null
	select	@priority	= 9

if @priority < 0 or @priority > 9
	select	@priority	= 9

-- The sort order should not be null
if @sort_order is null
	select	@sort_order	= '00000'

-- Ensure that the sort order is the first five digits of the zipcode
select	@sort_order = left(@sort_order, 5)

-- Insert the item into the queue.
insert into letter_queue (queue_name,	letter_type,	client,		creditor,	sort_order,	priority,	top_margin,	bottom_margin,	left_margin,	right_margin)
values			 (@queue_name,	@letter_type,	@client,	@creditor_id,	@sort_order,	@priority,	@top_margin,	@bottom_margin,	@left_margin,	@right_margin)

return ( scope_identity() )
GO
