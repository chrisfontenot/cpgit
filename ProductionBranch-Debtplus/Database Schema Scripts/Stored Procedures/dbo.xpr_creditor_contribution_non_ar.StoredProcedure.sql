USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_non_ar]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_non_ar] ( @creditor AS typ_creditor, @amount as money = 0.0, @Account AS typ_glaccount, @Message AS typ_description = NULL ) AS

-- ======================================================================================================
-- ==            Make a non-ar ledger entry for the creditor contribtuions                             ==
-- ======================================================================================================

--   2/20/2002
--     Changed to xpr_creditor_contribution_non_ar to make similar to the other items in the database.
--   8/05/2008
--     Added the amount to the creditor contribution MTD received field. More creditors are paying by grant
--     so this is a "good" idea.

SET NOCOUNT ON

declare	@result	int
select	@result	= 1

-- Transfer money from the creditor to the agency
if @amount > 0
BEGIN
	-- Transfer the money as a credit to the non-ar ledger
	insert into registers_non_ar	(tran_type,	creditor,	credit_amt,	dst_ledger_account,	message)
	values				('NA',		@creditor,	@amount,	@account,		@message)
	select @result = scope_identity()

	-- Record the NA transaction into the creditor register as well
	insert into registers_creditor	(tran_type,	creditor,	credit_amt,	message)
	values				('NA',		@creditor,	@amount,	@message)

	-- Add the creditor contribution to the creditor MTD field
	update	creditors
	set	contrib_mtd_received = isnull(contrib_mtd_received,0) + @amount
	where	creditor	= @creditor;
END

RETURN ( @result )
GO
