USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_income_read]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_income_read] ( @intake_client int ) as

-- ======================================================================================================
-- ==            Retrieve the information needed to process the income page of the intake              ==
-- ======================================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

set nocount on

select
		-- Retrieve the client information
		c.housing_status,
		c.housing_type,
		c.referred_by,
		c.cause_fin_problem1,
		c.marital_status,
		c.people,

		-- Retrieve the applicant information
		p1.gross_income,
		p1.net_income,
		p1.birthdate,
		p1.gender,
		p1.race,

		-- Retrieve the co-applicant information
		p2.gross_income,
		p2.net_income,
		p2.birthdate,
		p2.gender,
		p2.race

from		intake_clients c with (nolock)
left outer join	intake_people p1 with (nolock) on c.intake_client = p1.intake_client and 1 = p1.person
left outer join	intake_people p2 with (nolock) on c.intake_client = p2.intake_client and 2 = p2.person
where		c.intake_client		= @intake_client

-- Return the information for the income
select		d.asset_id          			as 'asset_id',
		isnull(d.description,'')		as 'description',
		isnull(a.asset_amount,0)		as 'asset_amount'
from            asset_ids d with (nolock)
left outer join intake_assets a with (nolock) on d.asset_id = a.asset_id and @intake_client = a.intake_client
ORDER BY        1;

return ( @@rowcount )
GO
