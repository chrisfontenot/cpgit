USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_client_creditor_payments]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_client_creditor_payments] as

declare	@month_0	datetime
declare	@month_1	datetime

select	@month_0	= getdate()
select	@month_0	= convert(datetime, convert(varchar,month(@month_0)) + '/01/' + convert(varchar,year(@month_0)) + ' 00:00:00')
select	@month_1	= dateadd(m, -1, @month_0)

-- THIS MONTH
select	cc.client_creditor_balance,
	sum(isnull(debit_amt,0) - isnull(credit_amt,0)) as payment
into	#temp_0
from	registers_client_creditor rcc
inner join client_creditor cc on rcc.client_creditor = cc.client_creditor
where	rcc.date_created >= @month_0
and	rcc.tran_type in ('AD', 'BW', 'MD', 'CM', 'RF', 'VD', 'RR')
group by cc.client_creditor_balance

update	client_creditor_balances
set	payments_month_0 = 0;

update	client_creditor_balances
set	payments_month_0 = x.payment
from	client_creditor_balances bal
inner join #temp_0 x on bal.client_creditor_balance = x.client_creditor_balance

drop table #temp_0

-- LAST MONTH
select	cc.client_creditor_balance,
	sum(isnull(debit_amt,0) - isnull(credit_amt,0)) as payment
into	#temp_1
from	registers_client_creditor rcc
inner join client_creditor cc on rcc.client_creditor = cc.client_creditor
where	rcc.date_created between @month_1 and @month_0
and	rcc.tran_type in ('AD', 'BW', 'MD', 'CM', 'RF', 'VD', 'RR')
group by cc.client_creditor_balance

update	client_creditor_balances
set	payments_month_1 = 0;

update	client_creditor_balances
set	payments_month_1 = x.payment
from	client_creditor_balances bal
inner join #temp_1 x on bal.client_creditor_balance = x.client_creditor_balance

drop table #temp_1
GO
