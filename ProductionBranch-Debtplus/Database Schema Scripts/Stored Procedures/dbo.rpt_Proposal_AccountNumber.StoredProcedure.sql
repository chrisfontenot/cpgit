USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Proposal_AccountNumber]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Proposal_AccountNumber] ( @ClientCreditor AS INT ) AS
-- ============================================================================================
-- ==              This is the information for the full disclosure proposal letter           ==
-- ============================================================================================

-- ChangeLog
--   7/18/2002
--     Added support for the "person" field in the client_creditor table. This is somewhat of a "hack" for
--     the co-applicant's ssn in that it assumes that the person is either 1 or 2. This violates our rule
--     about the database having support for more than two people, but since the co-applicant's ssn was
--     dropped from most proposals, it is of little significance.

SELECT		cc.client_creditor,
			cc.account_number,
			p1.ssn as ssn,
			r.description as 'referred_by',
			p2.ssn as ssn2
FROM		client_creditor cc
INNER JOIN	clients c ON c.client = cc.client
LEFT OUTER JOIN	people p1 ON cc.client = p1.client AND dbo.DebtOwner(cc.client_creditor) = p1.person
LEFT OUTER JOIN	people p2 ON cc.client = p2.client AND dbo.DebtOwner(cc.client_creditor) <> p2.person
LEFT OUTER JOIN	referred_by r ON r.referred_by = c.referred_by
WHERE		cc.client_creditor = @clientCreditor

RETURN ( @@rowcount )
GO
