ALTER PROCEDURE [dbo].[xpr_hecm_update_client] (
	@clientID INT
	,@referralCode INT
	,@servicerID INT
	,@loanNumber VARCHAR(20)
	,@language INT
	,@applicantName INT
	,@coApplicantName INT
	,@homePhone INT
	,@applicantCell INT
	,@coapplicantCell INT
	,@messagePhone INT
	,@applicantWorkPhone INT
	,@coapplicantWorkPhone INT
	,@address INT
	,@applicantEmail INT
	,@coapplicantEmail INT
	,@InvestorAccountNumber VARCHAR(50)
	,@Corporate_Advance MONEY = NULL
	,@delinq_Type INT = NULL
	,@SPOC_ID INT = NULL
	)
AS
BEGIN
	DECLARE @Housing_Lender_ID INT
	DECLARE @Housing_Loand_ID INT
	DECLARE @Housing_Propertities_ID INT

	BEGIN TRANSACTION

	BEGIN TRY
		UPDATE clients
		SET AddressID = @address
			,MsgTelephoneID = @messagePhone
			,LANGUAGE = @language
			,referred_by = @referralCode
		WHERE client = @clientID

		UPDATE PEOPLE
		SET NameID = @applicantName
			,WorkTelephoneID = @applicantWorkPhone
			,CellTelephoneID = @applicantCell
			,EmailID = @applicantEmail
			,SPOC_ID = @SPOC_ID
		WHERE client = @clientid AND Relation = 1

		UPDATE PEOPLE
		SET NameID = @coApplicantName
			,WorkTelephoneID = @coapplicantWorkPhone
			,CellTelephoneID = @coapplicantCell
			,EmailID = @coapplicantEmail
		WHERE client = @clientid AND Relation = 2

		DECLARE @ins_delinq_state INT
		DECLARE @tax_delinq_state INT

		SET @ins_delinq_state = NULL
		SET @tax_delinq_state = NULL

		SELECT @Housing_Lender_ID = HLR.oID
			,@Housing_Loand_ID = HLN.oID
			,@Housing_Propertities_ID = HP.oID
			,@ins_delinq_state = HP.ins_delinq_state
			,@tax_delinq_state = HP.tax_delinq_state
		FROM [Housing_lenders] HLR
		INNER JOIN [Housing_loans] HLN ON HLR.oID = HLN.CurrentLenderID
		INNER JOIN [Housing_properties] HP ON HP.oID = HLN.PropertyID
		WHERE HousingID = @clientID

		IF (@delinq_Type = 3)
		BEGIN
			SET @ins_delinq_state = 2
			SET @tax_delinq_state = 2
		END

		IF (@delinq_Type = 2)
			SET @ins_delinq_state = 2

		IF (@delinq_Type = 1)
			SET @tax_delinq_state = 2

		UPDATE Housing_properties
		SET tax_delinq_state = @tax_delinq_state
			,ins_delinq_state = @ins_delinq_state
		WHERE HousingID = @clientID

		UPDATE Housing_lenders
		SET ServicerID = @servicerID
			,AcctNum = @loanNumber
			,InvestorAccountNumber = @InvestorAccountNumber
			,Corporate_Advance = @Corporate_Advance
		WHERE oID = @Housing_Lender_ID

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		ROLLBACK TRANSACTION
	END CATCH

	SELECT @clientid AS ClientID
END
