SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_recon_detail_insert] (
@Batch AS INT,
@checknum AS BigInt = NULL,
@Cleared AS VarChar(1) = 'R',
@ReconDate AS DateTime = NULL,
@ReconAmount AS Money = NULL,
@tran_type AS VarChar(2) = NULL,
@Comments as varchar(80) = NULL,
@Bank as int = -1,
@ErrorMessage as varchar(80) = ''
) AS

-- =======================================================================================================================
-- ==            Attempt to reconcile the item against the check register                                               ==
-- =======================================================================================================================

-- ChangeLog
--   2/15/2002
--     Added support for other transaction types
--   7/22/2003
--     Add test to determine if the item is reconciled earlier in the same batch.
--   7/28/2004
--     Do not call it "DUPLICATE ITEM" if the bank is confirming that the check is now void.

-- Disable intermediate results
SET NOCOUNT ON

-- Trust register pointer
DECLARE	@TrustRegister	INT
DECLARE	@TrustTranType	VarChar(5)
DECLARE	@TrustAmount	Money
DECLARE	@TrustCleared	VarChar(2)
DECLARE	@Item		INT

DECLARE	@FromDate	datetime
DECLARE	@ToDate		datetime
SELECT	@FromDate	= convert(datetime, convert(varchar(10), @ReconDate, 101) + ' 00:00:00'),
		@ToDate		= convert(datetime, convert(varchar(10), @ReconDate, 101) + ' 23:59:59')

-- Correct the message to the proper null value if passed as blank		
if @ErrorMessage is not null
begin
	select @ErrorMessage = LTRIM(RTRIM(@ErrorMessage))
	if @ErrorMessage = ''
		select	@ErrorMessage = null
end

-- do the same to the comments field
if @comments is not null
begin
	select @comments = LTRIM(RTRIM(@comments))
	if @comments = ''
		select	@comments = null
end

-- If there is no check number then clear the check number
if @checknum is not null
begin
	SELECT	@checknum = ltrim(rtrim(@checknum))
	IF @checknum = ''
		SELECT	@checknum = null
end
	
-- Start a transaction for these requests
BEGIN TRANSACTION

-- Attempt to find the item in the trust register
IF @tran_type IS NULL
	SELECT @tran_type = 'AD'
ELSE BEGIN
	IF @tran_type NOT IN ('AD', 'BW', 'SC', 'BI', 'CR', 'MD', 'CM', 'DP', 'RR', 'RF')
	BEGIN
		RaisError (50021, 16, 1, @tran_type)
		Rollback Transaction
		Return ( 0 )
	END
END

-- The status must be valid
IF @Cleared NOT IN ('R', 'C', 'V')
BEGIN
	RaisError (50022, 16, 1, @Cleared)
	Rollback Transaction
	Return ( 0 )
END

-- Transactions other than checks do not have a check number
IF @tran_type NOT IN ('AD', 'CM', 'CR', 'MD', 'RF')
	SELECT @checknum = NULL

-- There must be a check number for a check. If there is no check number then this must be a bank error.
IF @checknum is null and @tran_type = 'AD'
	SELECT	@ErrorMessage = 'NO NUMBER'

-- If there is a comment and no trust reigster, then try to find the item in the reconciled list
if @TrustRegister IS NULL and @Comments IS NOT NULL and @tran_type in ('SC', 'BE', 'BI', 'DP', 'RF') and @ErrorMessage is null
	SELECT	@TrustRegister	= trust_register
	FROM	recon_details
	WHERE	comments	= @Comments
	AND		recon_date	= @ReconDate
	AND		amount		= @ReconAmount

-- Find the check or deposit in the trust register
if @TrustRegister IS NULL and @checknum is not null and @ErrorMessage is null
	SELECT	TOP 1
		@TrustRegister	= trust_register,
		@tran_type	= tran_type
	FROM	registers_trust WITH (NOLOCK)
	WHERE	tran_type		IN ('AD', 'CM', 'MD', 'CR', 'AR')
	AND		checknum		= @checknum
	AND		isnull(cleared,' ')	<> 'D'
	AND		bank				= @bank
	ORDER BY	date_created desc;

-- Try to match the transaction against the trust register for the item type and its amount
if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
	SELECT	@TrustRegister	= trust_register
	FROM	registers_trust with (nolock)
	WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
	AND		amount		= @ReconAmount
	AND		isnull(cleared,' ')	!= 'D'
	AND		date_created	between @FromDate and @ToDate

-- Try one day earlier
if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
	SELECT	@TrustRegister	= trust_register
	FROM	registers_trust with (nolock)
	WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
	AND		amount		= @ReconAmount
	AND		isnull(cleared,' ')	!= 'D'
	AND		date_created	between dateadd( d, -1, @FromDate ) AND @ToDate;

-- Try two days earlier
if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
	SELECT	@TrustRegister	= trust_register
	FROM	registers_trust with (nolock)
	WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
	AND		amount		= @ReconAmount
	AND		isnull(cleared,' ')	!= 'D'
	AND		date_created	between dateadd( d, -2, @FromDate ) AND @ToDate;

-- Try three days earlier
if @TrustRegister IS NULL and @checknum is null and @ErrorMessage is null
	SELECT	@TrustRegister	= trust_register
	FROM	registers_trust with (nolock)
	WHERE	tran_type	in ('DP', 'RR', 'VD', 'RF')
	AND		amount		= @ReconAmount
	AND		isnull(cleared,' ')	!= 'D'
	AND		date_created	between dateadd( d, -3, @FromDate ) AND @ToDate;
		
-- There should be a trust register if this is valid
IF @TrustRegister IS NULL AND @ErrorMessage IS NULL
	SELECT @ErrorMessage = 'NOT FOUND'

-- Fetch the information from the trust register
IF @ErrorMessage IS NULL
BEGIN
	SELECT	@TrustTranType	= tran_type,
			@TrustAmount	= amount,
			@TrustCleared	= cleared
	FROM	registers_trust
	WHERE	trust_register	= @TrustRegister

	-- Change the deposit type to the appropriate refund type as needed
	IF @TrustTranType != @tran_type
	begin
		IF @tran_type = 'DP' AND @TrustTranType in ('RR', 'RF', 'VD')
			SELECT @tran_type = @TrustTranType
	end

	IF @TrustTranType != @tran_type
		SELECT @ErrorMessage = 'WRONG TYPE'
END

IF @ReconAmount IS NULL
	SELECT @ReconAmount = @TrustAmount
ELSE
	IF @ErrorMessage IS NULL AND @TrustAmount != @ReconAmount
		SELECT @ErrorMessage = 'WRONG AMOUNT'

-- If the trust register is not pending then look for a void status being confirmed as void. Accept that as being OK.
-- All others are duplicated reconcilation information.
IF @ErrorMessage IS NULL AND isnull(@TrustCleared,' ') != ' '
BEGIN
	IF (@TrustCleared <> 'V') OR (@Cleared <> 'V')
		SELECT @ErrorMessage = 'DUPLICATE ITEM'
END

-- Look for duplicate items in the same batch.
-- This would mean that the bank cashed the same check for the same amount in the current list of checks. It is
-- definately "not good".
IF (@ErrorMessage IS NULL) AND (@checknum IS NOT NULL)
begin
	IF exists (SELECT * from recon_details WHERE trust_register = @TrustRegister AND recon_batch = @Batch)
		SELECT @ErrorMessage = 'DUPLICATE ITEM'
end

-- Default the reconcilation date
IF @ReconDate IS NULL
	SELECT @ReconDate = convert(datetime, convert(varchar(10), getdate(), 101) + ' 00:00:00')

if @ErrorMessage IS NOT NULL
	SELECT	@Cleared = ' '

-- Insert the item into the table
INSERT INTO recon_details	(bank, recon_batch,	tran_type,	checknum,	amount,		cleared,	recon_date,	trust_register,	message,	comments)
VALUES				(@bank, @Batch,	@tran_type,	@checknum,	@ReconAmount,	@Cleared,	@ReconDate,	@TrustRegister,	@ErrorMessage,	@Comments)

SELECT @Item = SCOPE_IDENTITY()

-- Return the status to the caller
COMMIT TRANSACTION
RETURN ( @Item )
GO
