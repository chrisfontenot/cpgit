USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_creditor_contribution_analysis]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_creditor_contribution_analysis] as

-- Find the current date
declare @item_date	datetime
select	@item_date = getdate()

-- Find the date ranges for the periods
declare	@month_start	datetime
declare	@month_end	datetime
declare	@quarter_start	datetime
declare	@quarter_end	datetime
declare	@year_start	datetime
declare	@year_end	datetime

select	@month_start	= month_start,
	@month_end	= month_end,
	@quarter_start	= quarter_start,
	@quarter_end	= quarter_end,
	@year_start	= year_start,
	@year_end	= year_end
from	dbo.quarter_dates ( @item_date )

-- Find the transactions for the creditors
select	creditor,
	sum(isnull(debit_amt,0) - isnull(credit_amt,0)) as debit_amt,
	year(date_created) as 'year',
	month(date_created) as 'month',
	convert(datetime, null) as date_created
into	#all_transactions
from	registers_client_creditor with (nolock)
where	tran_type in ('AD','BW','MD','CM','RF','RR','VD')
group by creditor, year(date_created), month(date_created);

update	#all_transactions
set	date_created = convert(datetime, convert(varchar, [month]) + '/01/' + convert(varchar, [year]))

-- Find the month to date transactions
select	creditor, sum(debit_amt) as debit_amt
into	#mtd_payments
from	#all_transactions
where	date_created between @month_start and @month_end
group by creditor;

-- Find the quarter to date transactions
select	creditor, sum(debit_amt) as debit_amt
into	#qtd_payments
from	#all_transactions
where	date_created between @quarter_start and @quarter_end
group by creditor;

-- Find the year to date transactions
select	creditor, sum(debit_amt) as debit_amt
into	#ytd_payments
from	#all_transactions
where	date_created between @year_start and @year_end
group by creditor;

-- Find all transactions
select	creditor, sum(debit_amt) as debit_amt
into	#all_payments
from	#all_transactions
group by creditor;

-- Count of all clients
select	creditor, count(*) as clients
into	#all_clients
from	client_creditor with (nolock)
where	reassigned_debt = 0
group by creditor;

-- Count of mtd clients
select	creditor, count(*) as clients
into	#mtd_clients
from	client_creditor with (nolock)
where	reassigned_debt = 0
and	date_created <= @month_end
group by creditor;

-- Count of qtd clients
select	creditor, count(*) as clients
into	#qtd_clients
from	client_creditor with (nolock)
where	reassigned_debt = 0
and	date_created <= @quarter_end
group by creditor;

-- Count of ytd clients
select	creditor, count(*) as clients
into	#ytd_clients
from	client_creditor with (nolock)
where	reassigned_debt = 0
and	date_created <= @year_end
group by creditor;

select	creditor, avg(fairshare_pct) as fairshare_pct
into	#last_year_pct
from	registers_client_creditor with (nolock)
where	tran_type in ('AD','BW')
and	dateadd(yy, 1, date_created) between @year_start and @year_end
group by creditor;

-- Current year percentage
select	creditor, avg(fairshare_pct) as fairshare_pct
into	#current_pct
from	registers_client_creditor with (nolock)
where	tran_type in ('AD','BW')
and	date_created between @year_start and @year_end
group by creditor;

-- Find the payment creditor contact information
declare	@creditor_contact_type	int
select	@creditor_contact_type	= min(creditor_contact_type)
from	creditor_contact_types with (nolock)
where	contact_type like '%P%'

if @creditor_contact_type is null
	select	@creditor_contact_type	= min(creditor_contact_type)
	from	creditor_contact_types with (nolock)
	where	contact_type like '%O%'

if @creditor_contact_type is null
	select	@creditor_contact_type	= min(creditor_contact_type)
	from	creditor_contact_types with (nolock)
	where	[name] = 'original'

if @creditor_contact_type is null
	select	@creditor_contact_type	= min(creditor_contact_type)
	from	creditor_contact_types with (nolock)

if @creditor_contact_type is null
	select	@creditor_contact_type	= 1

select	cr.creditor, aa.postalcode as zipcode, cr.creditor_name, dbo.format_normal_name (ccn.prefix, ccn.first, ccn.middle, ccn.last, ccn.suffix) as contact_name, dbo.format_TelephoneNumber ( cc.TelephoneID ) as phone
into	#creditors
from	creditors cr with (nolock)
left outer join creditor_addresses a with (nolock) on cr.creditor = a.creditor and 'P' = a.type
left outer join addresses aa with (nolock) on a.AddressID = aa.Address
left outer join creditor_contacts cc with (nolock) on cr.creditor = cc.creditor and @creditor_contact_type = cc.creditor_contact_type
left outer join names ccn with (nolock) on cc.NameID = ccn.Name

-- Return the results
select	a.creditor, a.zipcode, a.creditor_name, a.contact_name, a.phone,

	b.debit_amt as mtd_payments,
	c.debit_amt as qtd_payments,
	d.debit_amt as ytd_payments,
	e.debit_amt as all_payments,

	f.clients as mtd_clients,
	g.clients as qtd_clients,
	h.clients as ytd_clients,
	i.clients as all_clients,

	round(isnull(j.fairshare_pct,0) * 100.0,3) as last_year_fairshare,
	round(isnull(k.fairshare_pct,0) * 100.0,3) as this_year_fairshare

from		#creditors a

left outer join	#mtd_payments b		on a.creditor = b.creditor
left outer join	#qtd_payments c		on a.creditor = c.creditor
left outer join	#ytd_payments d		on a.creditor = d.creditor
left outer join	#all_payments e		on a.creditor = e.creditor

left outer join	#mtd_clients f		on a.creditor = f.creditor
left outer join	#qtd_clients g		on a.creditor = g.creditor
left outer join	#ytd_clients h		on a.creditor = h.creditor
left outer join	#all_clients i		on a.creditor = i.creditor

left outer join	#last_year_pct j	on a.creditor = j.creditor
left outer join	#current_pct k		on a.creditor = k.creditor

-- where	isnull(k.fairshare_pct,0) <= 0.10
order by 2,1

-- Discard the working tables
drop table #all_transactions
drop table #mtd_payments
drop table #qtd_payments
drop table #ytd_payments
drop table #all_payments
drop table #all_clients
drop table #mtd_clients
drop table #qtd_clients
drop table #ytd_clients
drop table #last_year_pct
drop table #current_pct
drop table #creditors

return ( @@rowcount )
GO
