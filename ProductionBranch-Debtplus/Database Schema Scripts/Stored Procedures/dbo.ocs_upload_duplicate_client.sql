USE [DebtPlus]
GO

/****** Object:  StoredProcedure [dbo].[ocs_upload_duplicate_client]    Script Date: 6/22/2015 10:47:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:        Brandon Wilhite
-- Create date: 
-- Description:   Add a new ocs client record to replace one that has been purged
-- =============================================

ALTER PROCEDURE [dbo].[ocs_upload_duplicate_client] 
    @program int,
    @archive bit,                                   --bool Archive, 
      @activeFlag bit,                            --bool ActiveFlag, 
    @isDuplicate bit,                           --bool IsDuplicate, 
    @clientId int,
    @uploadedRecordId int,                      --int UploadRecordId, 
    @uploadAttempt varchar(50) = NULL,    --actually a guid
            
    @lastChanceList datetime = NULL,        --DateTime LastChanceList, 
    @actualSaleDate datetime = NULL,        --DateTime ActualSaleDAte, 
    @dueDate datetime = NULL,                     --DateTime DueDate, 
      
    @queueCode varchar(10) = NULL,        --string QueueCode
      
    @p1area varchar(50) = NULL, @p1num varchar(50) = NULL, --home
    @p2area varchar(50) = NULL, @p2num varchar(50) = NULL, --msg
    @p3area varchar(50) = NULL, @p3num varchar(50) = NULL, --app cell
    @p4area varchar(50) = NULL, @p4num varchar(50) = NULL, --app work
    @p5area varchar(50) = NULL, @p5num varchar(50) = NULL, --coapp cell
    @p6area varchar(50) = NULL, @p6num varchar(50) = NULL,    

    @servicer varchar(50) = NULL,                 --string Servicer,
    @zipcode varchar(50),                     --string Zipcode,
            
    @Inserted int OUTPUT
AS
BEGIN

      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      begin transaction

      begin try

            declare @prevUpload int
            set @prevUpload = 
                  (select top 1 UploadRecord from OCS_Client 
                  where ClientId = @clientId and Program = @program 
                  order by Id desc)
            
            update OCS_Client
            set Archive = 1,
            ActiveFlag = 0,
            IsDuplicate = 1
            where Id = (select top 1 Id from OCS_Client where ClientId = @clientId and Program = @program order by Id desc)

            /*"forget" phone numbers, per OCSTEST-57*/
            update clients 
            set HomeTelephoneID = null,
            MsgTelephoneID = null
            where client = @clientId

            update people
            set WorkTelephoneID = null,
            CellTelephoneID = null
            where Client = @clientId
            
            --INSERT PHONE NUMBERS
            declare @p1 int
            set @p1 = NULL
            if @p1area is not null
            begin
                  insert into TelephoneNumbers (Country,Acode,Number)
                  values (1,@p1area,@p1num)
                  set @p1 = SCOPE_IDENTITY()                
            end
            declare @p2 int
            set @p2 = NULL
            if @p2area is not null
            begin
                  insert into TelephoneNumbers (Country,Acode,Number)
                  values (1,@p2area,@p2num)
                  set @p2 = SCOPE_IDENTITY()                
            end
            declare @p3 int
            set @p3 = NULL
            if @p3area is not null
            begin
                  insert into TelephoneNumbers (Country,Acode,Number)
                  values (1,@p3area,@p3num)
                  set @p3 = SCOPE_IDENTITY()                
            end
            declare @p4 int
            set @p4 = NULL
            if @p4area is not null
            begin
                  insert into TelephoneNumbers (Country,Acode,Number)
                  values (1,@p4area,@p4num)
                  set @p4 = SCOPE_IDENTITY()                
            end
            declare @p5 int
            set @p5 = NULL
            if @p5area is not null
            begin
                  insert into TelephoneNumbers (Country,Acode,Number)
                  values (1,@p5area,@p5num)
                  set @p5 = SCOPE_IDENTITY()                
            end
            declare @p6 int
            set @p6 = NULL
            if @p6area is not null
            begin
                  insert into TelephoneNumbers (Country,Acode,Number)
                  values (1,@p6area,@p6num)
                  set @p6 = SCOPE_IDENTITY()                
            end
            
            update clients
            set HomeTelephoneID = @p1,
            MsgTelephoneID = @p2
            where client = @clientId

            --applicant phone number references
            if @p3 is not null
            begin
            
                  if (select count(*) from people where Client = @clientId and Relation = 1) = 1
                  begin
                        update people 
                        set CellTelephoneID = @p3,
                              WorkTelephoneID = @p4
                        where Client = @clientId and Relation = 1
                  end
                  else              
                        insert into people(Client,Relation,CellTelephoneID,WorkTelephoneID)
                        values(@clientId,1,@p3,@p4)
            end

            /*INSERT/UPDATE PERSON2 IF NEEDED*/
            declare @needsPerson2 int
            declare @fname2 varchar(50)
            set @fname2 = (select FirstName2 from OCS_UploadRecord where Id = @uploadedRecordId)
            declare @lname2 varchar(50)
            set @lname2 = (select LastName2 from OCS_UploadRecord where Id = @uploadedRecordId)
            
            declare @oldfname2 varchar(50)
            set @oldfname2 = (select FirstName2 from OCS_UploadRecord where Id = @prevUpload)
            declare @oldlname2 varchar(50)
            set @oldlname2 = (select LastName2 from OCS_UploadRecord where Id = @prevUpload)

            set @needsPerson2 = 
                  case 
                        when @p5 is not null OR 
                              @p6 is not null OR 
                              ((@fname2 is not null) AND (@fname2 <> '')) OR
                              ((@lname2 is not null) AND (@lname2 <> ''))
                        then 1 
                        else 0
                  end
                  
            declare @person2 int
            set @person2 = (select top 1 person from people where client = @clientId and Relation <> 1)

            declare @name2 int
            set @name2 = case when @person2 is null then null else (select NameID from people where person = @person2) end

            --we have no name or person, but need both
            if @needsPerson2 = 1 and (@name2 is null or @person2 is null)
            begin
                  insert into Names(First,Last)
                  values (@fname2,@lname2)
                  set @name2 = SCOPE_IDENTITY()
            end
            --we just need a new name
            else if (@needsPerson2 = 1 and @name2 is not null) AND                  
                  (--names do not match
                        (@fname2 <> @oldfname2 and NOT(@fname2 is null and @oldfname2 is null)) OR
                        (@lname2 <> @oldlname2 and NOT(@lname2 is null and @oldlname2 is null))
                  )
            begin             
                  insert into Names(First,Last)
                  values (@fname2,@lname2)
                  set @name2 = SCOPE_IDENTITY()

                  update people
                  set NameID = @name2
                  where Person = @person2
            end

            --we need a new person
            if @needsPerson2 = 1 and @person2 is null
            begin
                  insert into people(Client,NameID,Relation,CellTelephoneID,WorkTelephoneID,Gender,Race,Ethnicity,Education,CreditAgency,no_fico_score_reason)
                  values(@clientId,@name2,2,@p5,@p6,2,10,2,1,'EQUIFAX',4)
                  set @person2 = SCOPE_IDENTITY()
            end

            --coapplicant phone number references
            if @p5 is not null OR @p6 is not null
            begin
                  update people 
                  set CellTelephoneID = @p5,
                  WorkTelephoneID = @p6
                  where person = @person2
            end
            /*INSERT/UPDATE PERSON2 IF NEEDED*/

            /*UPDATE PERSON 1 NAME IF NEEDED*/
            declare @fname1 varchar(50)
            set @fname1 = (select FirstName1 from OCS_UploadRecord where Id = @uploadedRecordId)
            declare @lname1 varchar(50)
            set @lname1 = (select LastName1 from OCS_UploadRecord where Id = @uploadedRecordId)
            
            declare @oldfname1 varchar(50)
            set @oldfname1 = (select FirstName1 from OCS_UploadRecord where Id = @prevUpload)
            declare @oldlname1 varchar(50)
            set @oldlname1 = (select LastName1 from OCS_UploadRecord where Id = @prevUpload)

            declare @person1 int
            set @person1 = (select top 1 person from people where client = @clientId and Relation = 1)

            declare @name1 int
            set @name1 = (select NameID from people where person = @person1)

            --names do not match
            if    (@fname2 <> @oldfname2 and NOT(@fname2 is null and @oldfname2 is null)) OR
                  (@lname2 <> @oldlname2 and NOT(@lname2 is null and @oldlname2 is null))
            begin             
                  insert into Names(First,Last)
                  values (@fname1,@lname1)
                  set @name1 = SCOPE_IDENTITY()

                  update people
                  set NameID = @name1
                  where Person = @person1
            end
            /*END UPDATE PERSON 1 NAME IF NEEDED*/


            /*UPDATE ADDRESS IF NEEDED*/
            declare @address int
            set @address = (select AddressID from clients where client = @clientId)

            declare @zip varchar(50)
                  set @zip = (select ClientZipcode from OCS_UploadRecord where Id = @uploadedRecordId)
            declare @street varchar(50)
                  set @street = (select ClientStreet from OCS_UploadRecord where Id = @uploadedRecordId)
            declare @city varchar(50)
                  set @city = (select ClientCity from OCS_UploadRecord where Id = @uploadedRecordId)
            declare @state varchar(50)
                  set @state = (select ClientState from OCS_UploadRecord where Id = @uploadedRecordId)

            
            declare @oldzip varchar(50)
                  set @oldzip = (select ClientZipcode from OCS_UploadRecord where Id = @prevUpload)
            declare @oldstreet varchar(50)
                  set @oldstreet = (select ClientStreet from OCS_UploadRecord where Id = @prevUpload)
            declare @oldcity varchar(50)
                  set @city = (select ClientCity from OCS_UploadRecord where Id = @prevUpload)
            declare @oldstate varchar(50)
                  set @state = (select ClientState from OCS_UploadRecord where Id = @prevUpload)

                  declare @stateId int
                  set @stateId = 
                        (select top 1 state from states where MailingCode = @state)

            --something in the address changed
            if    (@zip <> @oldzip and NOT(@zip is null and @zip is null)) OR
                  (@street <> @oldstreet and NOT(@street is null and @street is null)) OR
                  (@city <> @oldcity and NOT(@city is null and @city is null)) OR
                  (@state <> @oldstate and NOT(@state is null and @state is null))
            begin       
                  INSERT INTO addresses (PostalCode,street,address_line_2,city,state)
                  VALUES (@zipcode,@street,'',@city,@stateId)

                  set @address = SCOPE_IDENTITY()

                  update clients 
                  set AddressID = @address
                  where client = @clientId
            end
            /*end UPDATE ADDRESS IF NEEDED*/

                  
            declare @investorNo varchar(20)
            set @investorNo = (select InvestorNumber from OCS_UploadRecord where Id = @uploadedRecordId)
                              
            declare @calcServicerId int 
            set @calcServicerId =   COALESCE((select top 1 oID from Housing_lender_servicers [servicers] 
                                                where servicers.[description] = @servicer),442)
                                                                  
            declare @foundTz varchar(50)
                  set @foundTz = 
                  (select top 1 map.time_zone from zip_to_timezone [map] where map.zip like (@zipcode + '%'))

                  if @foundTz is null
                  begin
                        set @foundTz = 
                              (select top 1 zipInfo.TimeZone 
                              from ZipCodeSearch [zipInfo]
                              inner join states [state]
                              on state.MailingCode = zipInfo.StateAbbr
                              where state.state = @state)
                  end

            --INSERT OCS CLIENT
            declare @ocs int
            set @ocs = NULL
            insert into OCS_Client(ClientId,Program,StatusCode,Archive,ActiveFlag,IsDuplicate,UploadRecord,UploadAttempt,InvestorNumber,InvestorLastChanceList,InvestorActualSaleDate,InvestorDueDate,SearchTimezone,SearchServicerId,QueueCode)
            values(@clientId,@program,0,@archive,@activeFlag,@isDuplicate,@uploadedRecordId,@uploadAttempt,@investorNo,@lastChanceList,@actualSaleDate,@dueDate,COALESCE(@foundTz,'Indeterminate'),@calcServicerId,@queueCode)
            set @ocs = SCOPE_IDENTITY()

            commit transaction
      end try

      begin catch
        rollback transaction
      end catch
      
      set @Inserted = @clientId
END

GO


