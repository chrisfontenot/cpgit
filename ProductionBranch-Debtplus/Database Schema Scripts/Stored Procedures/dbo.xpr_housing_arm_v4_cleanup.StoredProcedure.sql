SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[xpr_housing_arm_v4_cleanup] as

-- Delete the table from the previous execution
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##hud_9902_clients'' and type = ''U'') drop table ##hud_9902_clients' )
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##hud_9902_workshops'' and type = ''U'') drop table ##hud_9902_workshops' )
exec ( 'if exists (select * from tempdb..sysobjects where name = ''##hud_9902_counselors'' and type = ''U'') drop table ##hud_9902_counselors' )

return ( 1 )


GO
