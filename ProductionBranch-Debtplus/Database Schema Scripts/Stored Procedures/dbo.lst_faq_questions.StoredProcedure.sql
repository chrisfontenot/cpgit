USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_faq_questions]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_faq_questions] AS

-- ===========================================================================================================
-- ==            Return the list of frequently asked questions                                              ==
-- ===========================================================================================================

select	faq_question				as 'item_key',
	description				as 'description'
from	faq_questions with (nolock)

return ( @@rowcount )
GO
