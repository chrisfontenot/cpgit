USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_FindBankFromAccount]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_FindBankFromAccount](@aba as varchar(9) = null, @AccountNumber as varchar(50)) as
-- ================================================================================
-- ==       Given an account number, find the corresponding bank ID for recon    ==
-- ================================================================================

	declare	@answer		int

	-- Reduce the aba to a valid item	
	if @aba is not null
	begin
		select @aba = LTRIM(rtrim(@aba))
		if @aba = ''
			select @aba = null
	end
	
	-- Reduce the account number to a valid item
	if @AccountNumber is not null
	begin
		select @AccountNumber = LTRIM(rtrim(@accountnumber))
		if @AccountNumber = ''
			select @AccountNumber = null
	end

	-- Try to find the item in the banks table
	if @AccountNumber is not null
		select	@answer			= MIN(bank)
		from	banks with (nolock)
		where	[type]			= 'C'
		and		[ActiveFlag]	= 1
		and		account_number = @AccountNumber
		and		(@aba is null or @aba = aba)
		
	if @answer is null and @aba is not null
		select	@answer			= MIN(bank)
		from	banks with (nolock)
		where	[type]			= 'C'
		and		[ActiveFlag]	= 1
		and		(@AccountNumber is null or account_number = @AccountNumber)
		and		@aba = aba
	
	-- There is no match, look for the default checking account
	if @answer is null
		select	@answer			= MIN(bank)
		from	banks with (nolock)
		where	[type]			= 'C'
		and		[ActiveFlag]	= 1
		and		[default]		= 1

	-- Cast wider nets to find the checking acocunt		
	if @answer is null
		select	@answer			= MIN(bank)
		from	banks with (nolock)
		where	[type]			= 'C'
		and		[ActiveFlag]	= 1
	
	if @answer is null
		select	@answer			= MIN(bank)
		from	banks with (nolock)
		where	[type]			= 'C'
		and		[default]		= 1

	if @answer is null
		select	@answer			= MIN(bank)
		from	banks with (nolock)
		where	[type]			= 'C'

	if @answer is null
		select	@answer			= MIN(bank)
		from	banks with (nolock)

	if @answer is null
		select	@answer			= 1
		
	return ( @answer )
GO
