USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cie_update]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cie_update](@rpps_transaction as int, @trace_number_first as varchar(15), @trace_number_last as varchar(15), @death_date as datetime, @rpps_batch as int, @rpps_file as int) as
update	rpps_transactions
set	trace_number_first	= @trace_number_first,
	trace_number_last	= @trace_number_last,
	rpps_batch		= @rpps_batch,
	rpps_file		= @rpps_file,
	death_date		= @death_date
where	rpps_transaction	= @rpps_transaction

return ( @@rowcount )
GO
