USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_HeldDebts]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_HeldDebts] AS
-- =============================================================================================
-- ==             List of client debts on hold for the disbursements                          ==
-- =============================================================================================

-- ChangeLog
--   3/14/2003
--     Allow for more than 10,000 creditors in a class
--   9/21/2006
--     Fixup conversion of name to use format_name
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Allocate a table to hold the debts that are on hold status
create table #results (
	client_creditor		int
);
CREATE index ix_temp_results on #results ( client_creditor );

-- Include all debts for any client which is on hold
insert into #results (client_creditor)
select		cc.client_creditor
FROM		clients c
INNER JOIN	client_creditor cc ON cc.client=c.client
WHERE		c.hold_disbursements > 0
AND		cc.reassigned_debt = 0
AND		c.active_status IN ('A', 'AR');

-- Add debt hold conditions
insert into #results (client_creditor)
select		cc.client_creditor
FROM		clients c
INNER JOIN	client_creditor cc ON cc.client=c.client
WHERE		cc.hold_disbursements > 0
AND		cc.reassigned_debt = 0
AND		c.active_status IN ('A', 'AR');

-- Find the distinct debt list
select distinct client_creditor
into		#results_2
from		#results;
CREATE index ix_results_2 on #results_2 ( client_creditor );

-- Return the results
SELECT		c.disbursement_date,
		cc.creditor,
		cr.type,
		cr.creditor_id,
		cr.creditor_name,
		cc.client,
		cc.client_creditor,
		cc.disbursement_factor,
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as 'name',
		cc.account_number

from		#results_2 x
inner join	client_creditor cc on x.client_creditor = cc.client_creditor
inner join	client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
inner join	clients c with (nolock) on cc.client = c.client
LEFT OUTER JOIN	people p with (nolock) ON p.client = c.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
INNER JOIN	creditors cr ON cc.creditor = cr.creditor
order by	cc.client, cr.type, cr.creditor_id, cc.client_creditor

drop table	#results
drop table	#results_2

RETURN ( @@rowcount )
GO
