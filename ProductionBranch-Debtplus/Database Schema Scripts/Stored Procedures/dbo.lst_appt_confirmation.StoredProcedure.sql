USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_appt_confirmation]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[lst_appt_confirmation] AS

select	oID				as item_key,
		description		as description,
		[default]		as [default],
		ActiveFlag		as ActiveFlag
from	AppointmentConfirmationTypes
order by 1
return ( @@rowcount )
GO
