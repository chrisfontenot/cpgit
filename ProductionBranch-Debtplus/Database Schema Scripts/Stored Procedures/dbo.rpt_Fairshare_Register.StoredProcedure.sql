SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Fairshare_Register] ( @FromDate as datetime = null, @ToDate as datetime = null ) as

-- =================================================================================================
-- ==                   Generate the fairshare transaction requests                               ==
-- =================================================================================================

-- Initialize to process the request
set nocount on

if @ToDate is null
	set @ToDate = getdate()

if @FromDate is null
	set @FromDate = @ToDate

select	@FromDate = convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate   = convert(datetime, convert(varchar(10), @ToDate,   101) + ' 23:59:59')

-- Create the table to hold the transactions
create table #transactions ( tran_type varchar(2), date_created datetime, created_by varchar(80) null, credit_amt money null, debit_amt money null, [grouping] int, checknum BigInt null );

-- Determine the deduct creditor. It is used for transactions involving client 0
declare	@deduct_creditor	varchar(10)
select	@deduct_creditor	= deduct_creditor
from	config;

-- This must be present. Assume the default if there is no one specified
if @deduct_creditor is null
	select	@deduct_creditor = 'Z9999';

-- Obtain the starting trust balance for the date from the statistics table
insert into #transactions (tran_type, date_created, credit_amt, [grouping])
select	'BB'					as 'tran_type',
	convert(varchar(10), dateadd(d, 1, period_start), 101)	as 'date_created',
	client_0					as 'credit_amt',
	1							as 'grouping'
from	[statistics] with (nolock)
where	Period_Start between @FromDate and dateadd(d, -1, @ToDate)

-- Ensure that there is a starting balanace transaction
if not exists (select * from #transactions)
	insert into #transactions (tran_type, date_created, credit_amt, [grouping])
	select	'BB'					as 'tran_type',
	convert(varchar(10), @FromDate, 101)	as 'date_created',
	0					as 'credit_amt',
	1					as 'grouping'

-- Insert the disbursement information as a credit
insert into #transactions (tran_type, date_created, created_by, credit_amt, [grouping])
select	case tran_type
		when 'BW' then 'AD'
		else tran_type
	end						as 'tran_type',
	convert(varchar(10), date_created,101)	as 'date_created',
	created_by				as 'created_by',
	fairshare_amt			as 'credit_amt',
	2						as 'grouping'
from	registers_client_creditor with (nolock)
where	date_created between @FromDate and @ToDate
and	creditor != @deduct_creditor
and	tran_type in ('AD', 'BW', 'MD', 'CM')
and	creditor_type = 'D';

-- Insert the refund information
insert into #transactions (tran_type, date_created, created_by, debit_amt, [grouping])
select	tran_type				as 'tran_type',
	convert(varchar(10), date_created,101)	as 'date_created',
	created_by					as 'created_by',
	fairshare_amt				as 'debit_amt',
	2							as 'grouping'
from	registers_client_creditor with (nolock)
where	date_created between @FromDate and @ToDate
and	creditor != @deduct_creditor
and	((tran_type = 'RF' and fairshare_amt > 0)
	or
	(tran_type in ('RR', 'ER', 'VD') and fairshare_amt > 0 and creditor_type = 'D')
	)

-- Insert the disbursement information into the transaction
insert into #transactions (tran_type, date_created, created_by, debit_amt, [grouping], checknum)
select	'MD'					as 'tran_type',
	convert(varchar(10), rcc.date_created,101)	as 'date_created',
	rcc.created_by				as 'created_by',
	rcc.debit_amt				as 'debit_amt',
	3							as 'grouping',
	tr.checknum					as 'checknum'
from	registers_client_creditor rcc with (nolock)
left outer join registers_trust tr on rcc.trust_register = tr.trust_register
where	rcc.date_created between @FromDate and @ToDate
and	rcc.creditor = @deduct_creditor
and	rcc.tran_type in ('AD', 'MD', 'CM')

-- The result is returned to the caller once the operation is completed
select	x.tran_type			as 'tran_type',
	x.date_created			as 'date_created',
	isnull(co.name, x.created_by)	as 'created_by',
	checknum				as 'checknum',
	sum(isnull(x.credit_amt,0))	as 'credit_amt',
	sum(isnull(x.debit_amt,0))	as 'debit_amt',
	x.[grouping]			as 'grouping'
from	#transactions x
left outer join view_counselors co with (nolock) on x.created_by = co.person
group by x.tran_type, x.date_created, x.created_by, x.[grouping], x.checknum, co.name
order by x.date_created, x.[grouping], x.tran_type

drop table #transactions;

return ( @@rowcount )
GO
