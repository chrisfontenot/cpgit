USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_districts]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_districts] ( @description as varchar(50) ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the districts table                      ==
-- ========================================================================================
	insert into districts ( [description] ) values ( @description )
	return ( scope_identity() )
GO
