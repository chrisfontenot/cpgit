USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_close_to_payoff]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_close_to_payoff] ( @counselor as int = null ) AS
-- =============================================================================================
-- ==           Report information for the "close to disbursement" report                     ==
-- =============================================================================================

-- ChangeLog
--   9/31/2002
--     Added support for client_creditor_balances table
--   01/15/2003
--     Added creditor_classes table references and dropped "creditors.fee_creditor"
--   1/17/2003
--     Limited disbursement factor to the debt balance
--   2/29/2004
--     Added counselor parameter

-- Ignore intermediate result sets
SET NOCOUNT ON
CREATE table #t_close_to_payoff_balances ( client int, balance money, disbursement money, verified int, debt int );

-- Generate a list of the client balances
if @counselor is null
	insert into #t_close_to_payoff_balances (client, balance, disbursement, verified, debt )

	SELECT		cc.client as 'client',
			bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as 'balance',

			case
				when cc.reassigned_debt = 1 then 0
				when isnull(ccl.zero_balance,0) = 1 then cc.disbursement_factor
				when cc.disbursement_factor < bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments then cc.disbursement_factor
				else bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments
			end as 'disbursement',

			case
				when cc.balance_verify_date is null then 0
				else 1
			end as 'verified',
			1 as debt
	FROM		client_creditor cc		with (nolock)
	inner join	client_creditor_balances bal	WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
	INNER JOIN	clients c			with (nolock) ON cc.client = c.client
	INNER JOIN	creditors cr			with (nolock) ON cc.creditor = cr.creditor
	LEFT OUTER JOIN	creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class

	WHERE		cc.reassigned_debt = 0
--	AND		isnull(ccl.zero_balance,0) = 0
	AND		c.active_status in ('A','AR')

ELSE

	insert into #t_close_to_payoff_balances (client, balance, disbursement, verified, debt )

	SELECT		cc.client as 'client',
			bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments as 'balance',

			case
				when cc.reassigned_debt = 1 then 0
				when isnull(ccl.zero_balance,0) = 1 then cc.disbursement_factor
				when cc.disbursement_factor < bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments then cc.disbursement_factor
				else bal.orig_balance + bal.orig_balance_adjustment + bal.total_interest - bal.total_payments
			end as 'disbursement',

			case
				when cc.balance_verify_date is null then 0
				else 1
			end as 'verified',
			1 as debt
	FROM		client_creditor cc		with (nolock)
	inner join	client_creditor_balances bal	WITH (NOLOCK) ON cc.client_creditor_balance = bal.client_creditor_balance
	INNER JOIN	clients c			with (nolock) ON cc.client = c.client
	INNER JOIN	creditors cr			with (nolock) ON cc.creditor = cr.creditor
	LEFT OUTER JOIN	creditor_classes ccl		with (nolock) on cr.creditor_class = ccl.creditor_class

	WHERE		cc.reassigned_debt = 0
--	AND		isnull(ccl.zero_balance,0) = 0
	AND		c.active_status in ('A','AR')
	and		c.counselor = @counselor

-- Generate the total amount owed by each client
SELECT		client,
		sum(balance)		as 'balance',
		sum(disbursement)	as 'disbursement',
		sum(verified)		as 'verified',
		sum(debt)		as 'debt'
INTO		#t_close_to_payoff_debt
FROM		#t_close_to_payoff_balances
GROUP BY	client

-- Generate the appropriate slot for each of the records
SELECT		t.client,
		t.balance,
		t.disbursement,
		isnull(t.debt,0) - isnull(t.verified,0) as 'unverified',
	
		case
			when t.balance < 100                  then 0
			when t.balance < t.disbursement       then 1
			when t.balance < (2 * t.disbursement) then 2
			when t.balance < (3 * t.disbursement) then 3
			when t.balance < (4 * t.disbursement) then 4
			when t.balance < (5 * t.disbursement) then 5
							      else 6
		end as 'months_to_payoff',

		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name'

INTO		#t_close_to_payoff_result
FROM		#t_close_to_payoff_debt t
LEFT OUTER JOIN	people p with (nolock) ON t.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name

-- Finally return the result set order by the grouping factor
SELECT		*
FROM		#t_close_to_payoff_result
ORDER BY	months_to_payoff, client

-- Discard the working tables
DROP TABLE #t_close_to_payoff_debt
DROP TABLE #t_close_to_payoff_balances
DROP TABLE #t_close_to_payoff_result

-- Return the number of rows in the result
return ( @@rowcount )
GO
