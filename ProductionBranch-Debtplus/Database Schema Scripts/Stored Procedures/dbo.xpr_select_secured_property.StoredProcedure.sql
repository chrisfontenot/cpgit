USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_select_secured_property]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_select_secured_property] ( @client as int ) as

-- =====================================================================================================================
-- ==        Find the list of secured properties for the client                                                       ==
-- =====================================================================================================================

create table #results ( oID int identity(1,1), secured_property int null, client int null, class int null, secured_type int null, description varchar(50) null, sub_description varchar(50) null, original_price money null, current_value money null, year_acquired int null, year_mfg int null, housing_type int null, primary_residence bit null, secured_type_description varchar(50) null, secured_type_grouping varchar(50) null, secured_type_auto_home_other varchar(10) null)

-- Include the properties for the client
insert into #results (secured_property, client, class, secured_type, description, sub_description, original_price, current_value, year_acquired, year_mfg, housing_type, primary_residence, secured_type_description, secured_type_grouping, secured_type_auto_home_other)
select p.secured_property, p.client, p.class, p.secured_type, p.description, p.sub_description, p.original_price, p.current_value, p.year_acquired, p.year_mfg, p.housing_type, p.primary_residence, t.description, t.grouping, t.auto_home_other
from secured_types t
inner join secured_properties p on t.secured_type = p.secured_type
where	p.client = @client

-- Include the standard list of types
insert into #results (secured_property, client, class, secured_type, description, sub_description, original_price, current_value, year_acquired, year_mfg, housing_type, primary_residence, secured_type_description, secured_type_grouping, secured_type_auto_home_other)
select null, null, null, t.secured_type, null, null, null, null, null, null, null, 0, t.description, t.grouping, t.auto_home_other
from secured_types t
where t.secured_type not in (
	select	secured_type
	from	#results
);

-- Find the loans outstanding on the properties
select	p.secured_property,
		sum(l.balance) as balance
into	#loans
from	secured_loans l with (nolock)
inner join #results p on l.secured_property = p.secured_property
where	p.secured_property is not null
group by p.secured_property

-- Return the results to the caller
select	r.*, l.balance as loan_balance
from	#results r
left outer join #loans l on r.secured_property = l.secured_property
order by secured_type_grouping, secured_type_description;

drop table #results
drop table #loans

return ( @@rowcount )
GO
