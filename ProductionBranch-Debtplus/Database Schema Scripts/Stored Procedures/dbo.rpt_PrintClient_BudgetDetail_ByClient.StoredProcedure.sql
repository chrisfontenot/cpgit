USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_PrintClient_BudgetDetail_ByClient]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_PrintClient_BudgetDetail_ByClient] ( @Client AS INT ) AS

-- =====================================================================================================
-- ==             Retrieve the most current budget for the client                                     ==
-- =====================================================================================================

-- ChangeLog
--   12/19/2008
--      Revisions for SQL2008

set nocount on

DECLARE @BudgetID	INT
select  @BudgetID = dbo.map_client_to_budget ( @client )

-- Removed the error about a missing budget. Now, we use a totally "invalid" budget id to generate an empty result set.
IF @BudgetID IS NULL
    select  @BudgetID = -1

EXECUTE rpt_PrintClient_BudgetDetail_ByBudget @BudgetID
GO
