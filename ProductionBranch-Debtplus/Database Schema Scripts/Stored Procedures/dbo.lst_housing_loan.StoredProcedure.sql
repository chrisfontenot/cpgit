USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[lst_housing_loan]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[lst_housing_loan] AS
-- ========================================================================================
-- ==            Return the list of housing loan types                                   ==
-- ========================================================================================

select	oID	as item_key,
		description	as description,
		[default],
		ActiveFlag
FROM	Housing_LoanTypes
order by 2

return ( @@rowcount )
GO
