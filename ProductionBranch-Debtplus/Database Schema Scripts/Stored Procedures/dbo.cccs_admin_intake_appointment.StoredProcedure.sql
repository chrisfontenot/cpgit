USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cccs_admin_intake_appointment]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[cccs_admin_intake_appointment] ( @application_id int, @client int ) as

-- Find the information for the appointment
declare	@appt_result		varchar(5)
declare	@appt_referred_by	int
declare	@appt_referred_to	int
declare	@appt_type			int
declare	@appt_office		int
declare	@appt_counselor		int
declare	@appt_duration		int

select	@appt_result		= 'IS',	-- Appointment Result = Incomplete Session
		@appt_referred_by	= 412,	-- Appointment Referral Source = Internet
		@appt_referred_to	= 17,	-- Where did you send the client = ClearPoint ? Needs Additional Appt
		@appt_type			= 3,	-- Appointment Type = Internet Submission
		@appt_office		= 49,	-- Office Location = Internet Counseling ? E
		@appt_counselor		= 612,	-- Counselor = Internet
		@appt_duration		= 30	-- Appointment Duration = 30

-- Create the appointment for the client
insert into client_appointments (client, appt_time, counselor, start_time, end_time, office, workshop, workshop_people,
					 appt_type, status, result, previous_appointment, referred_to,
					 referred_by, bankruptcy_class, priority, housing, post_purchase, credit,
					 callback_ph, confirmation_status, date_confirmed, date_updated)

values				(@client, null, @appt_counselor, getdate(), dateadd(n, @appt_duration, getdate()), @appt_office, null, 1,
					 @appt_type, 'W', @appt_result, NULL, @appt_referred_to,
					 @appt_referred_by, 0, 0, 0, 0, 0,
					 NULL, 0, NULL, getdate());

declare	@appt					int
select	@appt	= SCOPE_IDENTITY();

-- Update the client's appointment information	
update	clients
set		first_appt			= @appt,
		first_kept_appt		= @appt,
		active_status		= 'APT',
		client_status		= 'IS',
		office				= @appt_office,
		counselor			= @appt_counselor,
		bankruptcy_class	= 0
where	client				= @client

return ( @appt )
GO
