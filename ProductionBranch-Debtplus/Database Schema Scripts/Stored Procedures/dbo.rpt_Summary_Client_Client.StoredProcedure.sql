USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Summary_Client_Client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_Summary_Client_Client] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for client to client trnasfers                           ==
-- ======================================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SELECT @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the transactions
SELECT		d.tran_type				as 'tran_type',
		d.client				as 'client',
		d.credit_amt				as 'credit_amt',
		d.debit_amt				as 'debit_amt',
		d.created_by				as 'created_by',
		d.date_created				as 'date_created',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name'
FROM		registers_client d
LEFT OUTER JOIN	people p on d.client=p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		d.tran_type IN ('CD', 'CW')
AND		d.client >= 0
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created

RETURN ( @@rowcount )
GO
