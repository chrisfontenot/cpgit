USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_update_debt]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_intake_update_debt] ( @intake_client as int, @intake_id as varchar(20), @creditor_name as varchar(80) = null, @account_number as varchar(80) = null, @balance as money = 0, @months_delinquent as smallint = 0, @non_dmp_interest as float = 0) as 

-- ==============================================================================================================
-- ==            Start the processing on the new debt information                                              ==
-- ==============================================================================================================

-- Set the proper indicator flags
set nocount	on
set xact_abort	on

if not exists (select	*
		from	intake_clients
		where	intake_client	= @intake_client
		and	intake_id	= @intake_id)
begin
	RaisError ('The client is not in the system', 16, 1, @intake_client)
	return ( 0 )
end

-- Remove leading/trailing blanks from the description.
select	@creditor_name		= ltrim(rtrim(@creditor_name)),
	@account_number		= ltrim(rtrim(@account_number))

if @creditor_name = ''
	select @creditor_name = null

if @account_number = ''
	select @account_number = null

-- Update the client with the information
insert into intake_debts ( intake_client,  creditor_name,  account_number,  non_dmp_interest,  balance,  months_delinquent)
values			 (@intake_client, @creditor_name, @account_number, @non_dmp_interest, @balance, @months_delinquent)

return ( 1 )
GO
