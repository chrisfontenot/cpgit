USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Summary_Oper_To_Client]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Summary_Oper_To_Client] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for operating account to client transfers                ==
-- ======================================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00')
SELECT @ToDate	= convert(datetime, convert(varchar(10), @ToDate, 101)   + ' 23:59:59')

-- Fetch the transactions
SELECT		d.client					as 'client',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix) as 'name',
		d.debit_amt					as 'gross',
		d.src_ledger_account				as 'dest_account',
		rd.description					as 'dest_description',
		d.message					as 'reason',
		d.date_created					as 'item_date',
		d.created_by					as 'counselor'
FROM		registers_non_ar d
LEFT OUTER JOIN	ledger_codes rd ON d.src_ledger_account = rd.ledger_code
INNER JOIN	clients c ON d.client = c.client
LEFT OUTER JOIN	people p on c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
WHERE		d.tran_type = 'MF'
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created, d.client

RETURN ( @@rowcount )
GO
