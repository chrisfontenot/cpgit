USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_update_housing_interview]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_update_housing_interview](@housing_interview int, @interview_type int, @hud_result int = null, @termination_reason int = null, @duration int = 0, @GrantAmountUsed as money = 0) as
-- ========================================================================================
-- ==             Update a housing interview record                                      ==
-- ========================================================================================

-- Keep intermediate results clean
set nocount on
declare @housing_transaction int

-- Update the result information with the current transaction
if @hud_result is null
	update	[hud_interviews] set [hud_result] = null, [result_counselor] = null, [result_date] = null where [hud_interview] = @housing_interview
else
	update	[hud_interviews] set [hud_result] = @hud_result, [result_counselor] = SUSER_SNAME(), [result_date] = getdate() where [hud_interview] = @housing_interview
	
-- Record the termination reason
if @termination_reason is null
	update [hud_interviews] set [termination_reason] = null, [termination_date] = null, [termination_counselor] = null WHERE [hud_interview] = @housing_interview
else
	update [hud_interviews] set [termination_reason] = @termination_reason, [termination_date] = GETDATE(),[termination_counselor] = SUSER_SNAME() WHERE [hud_interview] = @housing_interview

-- If there is a duration then set the minutes for the transaction
if ISNULL(@duration,0) > 0
begin
	insert into hud_transactions ([hud_interview], [minutes], [GrantAmountUsed]) values ( @housing_interview, @duration, isnull(@GrantAmountUsed,0) )
	select @housing_transaction = SCOPE_IDENTITY()
end

-- The result is the pointer to the transaction record.	
return ( @housing_transaction )
GO
