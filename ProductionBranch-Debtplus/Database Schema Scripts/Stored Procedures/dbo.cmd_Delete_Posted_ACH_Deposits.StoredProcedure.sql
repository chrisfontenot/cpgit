USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_Delete_Posted_ACH_Deposits]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_Delete_Posted_ACH_Deposits] ( @deposit_batch_id as int = null ) as

-- ==================================================================================================
-- ==            Delete an ACH deposit batch once the item has been posted                         ==
-- ==================================================================================================

-- ChangeLog
--   Note: This cmd un-posts the transactions and leaves the batch unposted. Remember to run the 
--   cmd update_deposits_in_trust after this to fix up the unposted deposit field. 
     

if exists (select * from registers_disbursement where date_posted is null and tran_type = 'AD')
begin
	raiserror ('This procedure may not be executed with an open disbursement batch. Please post or delete any open disbursement batches before removing accidental ACH deposits.', 16, 1)
	return ( 0 )
end

-- Ensure that the batch ID is valid
if @deposit_batch_id is null
	set @deposit_batch_id = 0

if @deposit_batch_id <= 0
begin
	print 'You must enter the batch ID (not the name) for the batch that you wish to remove.'
	print 'The last 10 ACH files are as follows:'

	select top 10
		deposit_batch_id			as 'batch',
		convert(varchar, date_created)		as 'date',
		created_by				as 'created by',
		note					as 'Note'
	from	deposit_batch_ids
	where	date_posted is not null
	and	batch_type = 'AC'
	order by 2 desc, 1 desc;

end else begin

	declare	@trust_register		int
	declare	@date_posted		datetime

	select	@trust_register		= trust_register,
		@date_posted		= date_posted
	from	deposit_batch_ids
	where	deposit_batch_id	= @deposit_batch_id;

	if @trust_register is null
	begin
		print 'The ACH deposit batch does not exist.'

	end else begin

		if @date_posted is null
		begin
			select	@trust_register = null
			print 'The ACH deposit file has not been posted.'
		end
	end

	if @trust_register is null
		select	@deposit_batch_id = 0
end

if @deposit_batch_id > 0
begin
	print	'The batch was posted on ' + convert(varchar(10), @date_posted, 101)

	-- Keep things consistent
	begin transaction
	set xact_abort on

	-- Determine the total amount of the deposit for each client. Allow for multiple entries.
	select	client, sum(credit_amt) as credit_amt
	into	#t_deposits
	from	registers_client
	where	tran_type	= 'DP'
	and	tran_subtype	= 'AC'
	and	trust_register	= @trust_register
	group by client;

	-- Adjust the client balances to remove the extra money
	update	clients
	set	held_in_trust = held_in_trust - t.credit_amt
	from	clients c
	inner join #t_deposits t on c.client = t.client

	-- Discard the working table
	drop table #t_deposits;

	-- Remove the client deposit records from the system
	delete
	from	registers_client
	where	tran_type		= 'DP'
	and	tran_subtype		= 'AC'
	and	trust_register		= @trust_register

	-- Remove the trust register entry for the deposit from the system
	delete
	from	registers_trust
	where	trust_register		= @trust_register

	-- "Un-Post" the transactions
	update	deposit_batch_details
	set	date_posted		= null
	where	deposit_batch_id	= @deposit_batch_id;

	-- "Un-Post" the file
	update	deposit_batch_ids
	set	date_posted		= null,
		trust_register		= null
	where	deposit_batch_id	= @deposit_batch_id

	-- Commit the transaction to the database once this is complete
	commit transaction
end
GO
