USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[MONTHLY_disbursement_cleanup]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[MONTHLY_disbursement_cleanup] AS

-- ==================================================================================================
-- ==            Remove obsolete disbursement batches from the system                              ==
-- ==================================================================================================

begin transaction
set xact_abort on

-- Discard the disbursement transactions from the files which have been posted over 35 days ago
-- The "registers_client" has all of this information. The disbursement_* tables are only "working" values that hold the
-- current in-process disbursement data. Once the batch has been posted, there is no need for the duplicate items other
-- than a possibility of history. The "real" information is in the other tables, including everything that is in these
-- items.

delete from disbursement_creditors where disbursement_register in (select disbursement_register from registers_disbursement where date_posted < dateadd(d, -35, getdate()))
delete from disbursement_clients   where disbursement_register in (select disbursement_register from registers_disbursement where date_posted < dateadd(d, -35, getdate()))

-- -- Discard *ALL* batches over 65 days, including unposted ones. These are just assumed to be junk and would never be valid if they are that old.
-- commented out until it is resolved for the case that someone took some money from the client's trust and never disbursed it.
-- delete from disbursement_creditors where disbursement_register in (select disbursement_register from registers_disbursement where date_created < dateadd(d, -65, getdate()))
-- delete from disbursement_clients   where disbursement_register in (select disbursement_register from registers_disbursement where date_created < dateadd(d, -65, getdate()))

commit transaction

-- Return success
return ( 1 )
GO
