USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_intake_import_people]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
if not exists(select * from sysobjects where type = 'p' and name = 'xpr_intake_import_people')
begin
	exec ('create procedure xpr_intake_import_people as return 0;')
	exec ('grant execute on xpr_intake_import_people to public as dbo')
	exec ('deny execute on xpr_intake_import_people to www_role')
end
GO
ALTER PROCEDURE [dbo].[xpr_intake_import_people] ( @intake_client as int, @client as int, @person as int ) as
-- =================================================================================
-- ==              Create the person items                                        ==
-- =================================================================================
declare	@prefix		varchar(80)
declare	@first		varchar(80)
declare	@middle		varchar(80)
declare	@last		varchar(80)
declare	@suffix		varchar(80)
declare @email		varchar(800)
declare	@work_ph	varchar(80)
declare	@work_ext	varchar(80)

select	@prefix			= UPPER(prefix),
		@first			= UPPER(first),
		@middle			= UPPER(middle),
		@last			= UPPER(last),
		@suffix			= UPPER(suffix),
		@email			= email,
		@work_ph		= work_ph,
		@work_ext		= work_ext
from	intake_people
where	intake_client	= @intake_client
and		person			= @person;

-- Create a name record for the item
declare	@NameID		int
execute	@NameID	= xpr_insert_names @prefix, @first, @middle, @last, @suffix

-- Create an email record for the item
declare	@EmailID	int
if @email is not null
	execute	@EmailID	= xpr_insert_emailaddress @Email
		
-- Create a work telephone number for the item
declare	@WorkTelephoneID	int
if @work_ph is not null
begin
	declare	@Acode		varchar(4)
	declare	@Number		varchar(80)
	select	@Acode		= LEFT(@work_ph,3),
			@Number		= SUBSTRING(@work_ph, 4, 3) + '-' + SUBSTRING(@work_ph, 7, 4)
	execute	@WorkTelephoneID = xpr_insert_TelephoneNumbers 1, @Acode, @Number, @work_ext
end

declare	@person_id		int
if @person = 1
	select	@person_id		= person
	from	people
	where	client			= @client
	and		relation		= 1
else
	select	@person_id		= person
	from	people
	where	client			= @client
	and		relation		<> 1
	
if @person_id is null
	select	@person_id		= -1

-- Create the DebtPlus applicant id
update	people
set		NameID			= @NameID,
		former			= i.former,
		EmailID			= @EmailID,
		ssn				= i.ssn,
		gross_income	= i.gross_income,
		net_income		= i.net_income,
		job				= i.job,
		other_job		= i.other_job,
		birthdate		= i.birthdate,
		gender			= i.gender,
		race			= i.race,
		employer		= i.employer,
		WorkTelephoneID	= @WorkTelephoneID,
		relation		= case when i.person = 1 then 1 when i.person = 2 then 2 else 3 end,
		education		= i.education
from	people p
inner join intake_people i on i.intake_client = @intake_client and i.person = @person
where	p.person = @person_id

return ( @person )
GO
