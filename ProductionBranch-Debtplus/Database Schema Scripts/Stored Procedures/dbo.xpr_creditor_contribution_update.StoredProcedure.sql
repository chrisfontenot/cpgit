USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_creditor_contribution_update]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_creditor_contribution_update] (@creditor_contribution_pct typ_key, @creditor as typ_creditor,
						   @effective_date datetime,
						   @creditor_type_eft   typ_creditor_type, @fairshare_pct_eft   typ_fairshare_rate,
						   @creditor_type_check typ_creditor_type, @fairshare_pct_check typ_fairshare_rate) AS
-- ==================================================================================================
-- ==            						                                   ==
-- ==================================================================================================
-- Disable intermediate results
set nocount on
BEGIN TRANSACTION

DECLARE @check_msg					VARCHAR(300)
DECLARE @eft_msg					VARCHAR(300)
DECLARE	@input_fairshare_pct_eft	float
DECLARE	@input_fairshare_pct_check	float
DECLARE	@input_creditor_type_eft	varchar(1)
DECLARE	@input_creditor_type_check	varchar(1)
DECLARE	@input_creditor_contribution_pct	int

-- Make the effective date midnight on the indicated date
select	@effective_date = convert(datetime, convert(varchar(10), @effective_date, 101) + ' 00:00:00')

-- Ensure that the EFT type is defined
select  @creditor_type_eft = coalesce(@creditor_type_eft, @creditor_type_check, 'N')

-- Ensure that the creditor type is defined
if @creditor_type_check is null
	select	@creditor_type_check = @creditor_type_eft

-- Update the record with the new values
UPDATE	creditor_contribution_pcts
SET	effective_date			= @effective_date,
	fairshare_pct_check		= @fairshare_pct_check,
	fairshare_pct_eft		= @fairshare_pct_eft,
	creditor_type_check		= @creditor_type_check,
	creditor_type_eft		= @creditor_type_eft
WHERE	creditor_contribution_pct	= @creditor_contribution_pct
AND		date_updated is null

if @@rowcount < 1
	execute @creditor_contribution_pct = xpr_insert_creditor_contribution_pcts @creditor, @effective_date, @creditor_type_eft, @creditor_type_check, @fairshare_pct_eft, @fairshare_pct_check

-- Generate the message text for check percentages
IF @creditor_type_check = 'B'
	SELECT @check_msg = 'BILL at ' + convert(varchar,isnull(@fairshare_pct_check,0) * 100.0) + '%'
ELSE BEGIN
	IF @creditor_type_check = 'D'
		SELECT @check_msg = 'DEDUCT at ' + convert(varchar,isnull(@fairshare_pct_check,0) * 100.0) + '%'
	ELSE
		SELECT @check_msg = 'NO CONTRIBUTIONS'
END

-- Generate the message text for EFT percentages
IF @creditor_type_eft = 'B'
	SELECT @eft_msg = 'BILL at ' + convert(varchar,isnull(@fairshare_pct_eft,0) * 100.0) + '%'
ELSE BEGIN
	IF @creditor_type_eft = 'D'
		SELECT @eft_msg = 'DEDUCT at ' + convert(varchar,isnull(@fairshare_pct_eft,0) * 100.0) + '%'
	ELSE
		SELECT @eft_msg = 'NO CONTRIBUTIONS'
END

-- Update the message
if @check_msg = @eft_msg
	select @check_msg = @check_msg + ' for both check/eft'
else
	select @check_msg = @check_msg + ' for checks and ' + @eft_msg + ' for eft'

-- Insert the system note that the percentage was changed
insert into creditor_notes (creditor, is_text, subject, type, dont_edit, dont_delete, dont_print, note)
values (@creditor, 1, 'Contribution percentage scheduled', 3, 1, 1, 0,
'The creditor contribution percentage for the creditor was scheduled to be changed on ' + convert(varchar(10), @effective_date, 101) + ' to ' + @check_msg)

-- Make the change to the contribution percentage if the value has "expired"
if @effective_date < getdate()
BEGIN
	-- Process the most current update on the contribution fees for this creditor
	DECLARE contrib_pct CURSOR FOR

		SELECT		creditor_contribution_pct,
				fairshare_pct_check,
				fairshare_pct_eft,
				creditor_type_check,
				creditor_type_eft
		FROM		creditor_contribution_pcts
		WHERE		date_updated is null
		AND		(effective_date <= getdate())
		AND		(creditor = @creditor)
		ORDER BY	effective_date

	OPEN contrib_pct

	-- Fetch the pending items from the system
	FETCH contrib_pct INTO	@input_creditor_contribution_pct,
				@input_fairshare_pct_check,
				@input_fairshare_pct_eft,
				@input_creditor_type_check,
				@input_creditor_type_eft

	WHILE @@fetch_status = 0
	BEGIN
		-- Indicate that we have processed this specific record for the next time
		UPDATE	creditor_contribution_pcts
		SET	date_updated				= getdate()
		WHERE	creditor_contribution_pct	= @input_creditor_contribution_pct

		-- Update the creditor record with the new values
		UPDATE	creditors
		SET	creditor_contribution_pct	= @creditor_contribution_pct
		WHERE	creditor			= @creditor

		-- Include a system note that the percentage was changed
		IF @@rowcount > 0
		BEGIN
			-- Generate the message text for check percentages
			IF @input_creditor_type_check = 'B'
				SELECT @check_msg = 'BILL at ' + convert(varchar,isnull(@input_fairshare_pct_check,0) * 100.0) + '%'
			ELSE BEGIN
				IF @input_creditor_type_check = 'D'
					SELECT @check_msg = 'DEDUCT at ' + convert(varchar,isnull(@input_fairshare_pct_check,0) * 100.0) + '%'
				ELSE
					SELECT @check_msg = 'NO CONTRIBUTIONS'
			END

			-- Generate the message text for EFT percentages
			IF @input_creditor_type_eft = 'B'
				SELECT @eft_msg = 'BILL at ' + convert(varchar,isnull(@input_fairshare_pct_eft,0) * 100.0) + '%'
			ELSE BEGIN
				IF @input_creditor_type_eft = 'D'
					SELECT @eft_msg = 'DEDUCT at ' + convert(varchar,isnull(@input_fairshare_pct_eft,0) * 100.0) + '%'
				ELSE
					SELECT @eft_msg = 'NO CONTRIBUTIONS'
			END

			-- Update the message
			if @check_msg = @eft_msg
				select @check_msg = @check_msg + ' for both check/eft'
			else
				select @check_msg = @check_msg + ' for checks and ' + @eft_msg + ' for eft'

			-- Add a creditor system note that the contribution status was changed
			INSERT INTO creditor_notes (creditor,is_text,type,dont_delete,dont_edit,subject,note)
			VALUES (@creditor,
				1,3,1,1,
				'Contribution information was set',
				'Contribution information was set due to the scheduled request to the following infomation:' + char(13) + char(10) + @check_msg)
		END

		-- Obtain the next row from the recordset
		FETCH contrib_pct INTO	@input_creditor_contribution_pct,
					@input_fairshare_pct_check,
					@input_fairshare_pct_eft,
					@input_creditor_type_check,
					@input_creditor_type_eft
	END

	-- Cleanup
	CLOSE		contrib_pct
	DEALLOCATE	contrib_pct
END

COMMIT TRANSACTION
return ( @creditor_contribution_pct )
GO
