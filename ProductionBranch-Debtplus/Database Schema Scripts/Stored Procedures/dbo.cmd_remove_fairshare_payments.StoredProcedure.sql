USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[cmd_remove_fairshare_payments]    Script Date: 09/15/2014 13:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[cmd_remove_fairshare_payments] ( @creditor as varchar(10), @date as datetime ) as

-- ====================================================================================================
-- ==            Remove all of the payments to fairshare invoices for the creditor on the date       ==
-- ====================================================================================================

declare	@from_date	datetime
declare	@to_date	datetime

select	@from_date = convert(datetime, convert(varchar(10), @date, 101) + ' 00:00:00'),
	@to_date   = convert(datetime, convert(varchar(10), @date, 101) + ' 23:59:59')

-- Do this or fail
begin transaction
set xact_abort on
set nocount on

-- Find the list of the invoices being adjusted
select	invoice_register, sum(isnull(credit_amt,0)) as credit_amt
into	#invoice_register
from	registers_creditor
where	tran_type	= 'RC'
and	creditor	= @creditor
and	date_created	between @from_date and @to_date
group by invoice_register;

-- Remove the payments
delete
from	registers_creditor
where	tran_type	= 'RC'
and	creditor	= @creditor
and	date_created	between @from_date and @to_date;

-- Remove the payments for the invoices
select	rc.invoice_register, sum(isnull(rc.credit_amt,0)) as pmt_amt
into	#payments
from	registers_creditor rc
inner join #invoice_register x on rc.invoice_register = x.invoice_register
where	tran_type	= 'RC'
group by rc.invoice_register;

-- Find the date of the last payment
select	rc.invoice_register, max(rc.date_created) as pmt_date
into	#last_payment
from	registers_creditor rc
inner join #invoice_register x on rc.invoice_register = x.invoice_register
where	tran_type	= 'RC'
group by rc.invoice_register;

-- Update the invoice information
update	registers_invoices
set	pmt_amount	= 0,
	pmt_date	= null
where	invoice_register in (
	select	invoice_register
	from	#invoice_register
);

update	registers_invoices
set	pmt_amount	= x.pmt_amt
from	registers_invoices i
inner join #payments x on i.invoice_register = x.invoice_register;

update	registers_invoices
set	pmt_date	= x.pmt_date
from	registers_invoices i
inner join #last_payment x on i.invoice_register = x.invoice_register;

-- Discard the working tables
drop table #last_payment
drop table #payments
drop table #invoice_register

-- Commit the changes to the database
commit transaction
GO
