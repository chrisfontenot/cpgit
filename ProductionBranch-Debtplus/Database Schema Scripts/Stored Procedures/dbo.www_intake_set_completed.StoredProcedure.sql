USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_set_completed]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_set_completed] ( @intake_client as int, @set_identity as bit = 0) as
-- ============================================================================================================
-- ==            Set the completed status for the current client                                             ==
-- ============================================================================================================

-- Initialize to process the request
set nocount on

if isnull(@set_identity,0) <> 0
	update intake_clients
	set    completed_date = getdate(),
		   identity_date  = getdate()
	where  intake_client = @intake_client;
else
	update intake_clients
	set    completed_date = getdate()
	where  intake_client = @intake_client;

return ( @@rowcount )
GO
