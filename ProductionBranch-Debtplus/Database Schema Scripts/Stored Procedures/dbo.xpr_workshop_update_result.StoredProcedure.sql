USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_workshop_update_result]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_workshop_update_result] ( @client_appointment as int, @status as varchar(1) = 'P', @result as varchar(3) = null, @referred_to as int = null ) as

-- ==================================================================================================
-- ==            Update the result status for an appointment                                       ==
-- ==================================================================================================

set nocount on

declare @workshop	int
declare	@old_status	varchar(1)
declare	@client		int
declare	@old_result	varchar(3)

select	@workshop	= workshop,
	@old_status	= status,
	@client		= client
from	client_appointments with (nolock)
where	client_appointment = @client_appointment

if @workshop is null
begin
	rollback transaction
	raiserror ('The appointment being updated is not a workshop appointment.', 16, 1)
	return ( 0 )
end

if @old_status <> 'P'
begin
	rollback transaction
	raiserror ('The appointment being updated is no longer pending.', 16, 1)
	return ( 0 )
end

update	client_appointments
set	status = @status,
	result = @result,
	referred_to = @referred_to
where	client_appointment = @client_appointment

-- If there is a new client status then change the client status accordingly
if @result is not null
begin
	select	@old_result = client_status
	from	clients with (nolock)
	where	client = @client

	if isnull(@old_status,'CRE') <> @status
	begin
		update	clients
		set	client_status = @status
		where	client = @client

		insert into client_notes (client, is_text, subject, note)
		values (@client, 1, 'Client status changed', 'The client status was changed from ' + isnull(@old_status,'CRE') + ' to ' + @status + ' as a result of a workshop appointment.')
	end
end

return ( 1 )
GO
