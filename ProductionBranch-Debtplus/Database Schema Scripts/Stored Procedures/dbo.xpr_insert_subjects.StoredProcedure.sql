USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_subjects]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_subjects] ( @description as varchar(50) ) as
-- ========================================================================================
-- ==   Procedure for .NET to insert items into the subjects table                       ==
-- ========================================================================================
	insert into subjects ( [description] ) values ( @description )
	return ( scope_identity() )
GO
