USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[DAILY_disbursement_retention_events]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[DAILY_disbursement_retention_events] as

declare @from_date		datetime
declare	@to_date		datetime
declare	@todays_date		datetime

declare	@retention_event_1	int
declare	@retention_event_2	int
declare	@retention_event_3	int
declare	@retention_event_4	int

select	@retention_event_1	= 90,
	@retention_event_2	= 91,
	@retention_event_3	= 92,
	@retention_event_4	= 93

-- Find the period date
select	@todays_date	= getdate()

-- Translate the relative day of the month to the desired period.
if day ( @todays_date ) = 9 -- am of the 9th
	select	@from_date = convert(varchar, month(@todays_date)) + '/01/' + convert(varchar, year(@todays_date)) + ' 00:00:00',
		@to_date   = convert(varchar, month(@todays_date)) + '/08/' + convert(varchar, year(@todays_date)) + ' 23:59:59'
else
	if day ( @todays_date ) = 16 -- am of the 16th
		select	@from_date = convert(varchar, month(@todays_date)) + '/09/' + convert(varchar, year(@todays_date)) + ' 00:00:00',
			@to_date   = convert(varchar, month(@todays_date)) + '/15/' + convert(varchar, year(@todays_date)) + ' 23:59:59'
else
	if day ( @todays_date ) = 23 -- am of the 23
		select	@from_date = convert(varchar, month(@todays_date)) + '/16/' + convert(varchar, year(@todays_date)) + ' 00:00:00',
			@to_date   = convert(varchar, month(@todays_date)) + '/22/' + convert(varchar, year(@todays_date)) + ' 23:59:59';
else
	if day ( @todays_date ) = 1 -- am of the 1st of a new month
	begin
		select	@from_date = dateadd(d, -1, @todays_date);
		select	@from_date = convert(varchar, month(@from_date)) + '/23/' + convert(varchar, year(@from_date)) + ' 00:00:00';
		select	@to_date   = convert(varchar(10), dateadd(d, -1, @todays_date), 101) + ' 23:59:59';
	end
	else
		return

-- Find the last disbursement date for the clients
select	rc.client, max(rc.date_created) as last_disbursement_date, convert(int,null) as retention_event
into	#last_disb
from	registers_client rc
inner join clients c on rc.client = c.client
where	rc.tran_type in ('AD','MD')
and	rc.client > 0
and	rc.debit_amt > 0
and	c.active_status in ('A','AR')
and	rc.disbursement_register <> 10130 -- ignore the special disb on 1/31/07 
group by rc.client;

-- Look one month back
if day ( @todays_date ) = 1 -- am of the 1st of a new month
begin
	select	@from_date = dateadd(m, -1, @todays_date);
	select	@from_date = dateadd(d, -1, @from_date);
	select	@to_date   = convert(varchar(10), @from_date, 101) + ' 23:59:59';
	select	@from_date = convert(varchar, month(@from_date)) + '/23/' + convert(varchar, year(@from_date)) + ' 00:00:00';
end else
	select	@from_date = dateadd(m, -1, @from_date),
		@to_date   = dateadd(m, -1, @to_date)

update #last_disb
set    retention_event = @retention_event_1
where  last_disbursement_date between @from_date and @to_date

-- Look two months back
if day ( @todays_date ) = 1 -- am of the 1st of a new month
begin
	select	@from_date = dateadd(m, -2, @todays_date);
	select	@from_date = dateadd(d, -1, @from_date);
	select	@to_date   = convert(varchar(10), @from_date, 101) + ' 23:59:59';
	select	@from_date = convert(varchar, month(@from_date)) + '/23/' + convert(varchar, year(@from_date)) + ' 00:00:00';
end else
	select	@from_date = dateadd(m, -1, @from_date),
		@to_date   = dateadd(m, -1, @to_date)

update #last_disb
set    retention_event = @retention_event_2
where  last_disbursement_date between @from_date and @to_date

-- Look three months back
if day ( @todays_date ) = 1 -- am of the 1st of a new month
begin
	select	@from_date = dateadd(m, -3, @todays_date);
	select	@from_date = dateadd(d, -1, @from_date);
	select	@to_date   = convert(varchar(10), @from_date, 101) + ' 23:59:59';
	select	@from_date = convert(varchar, month(@from_date)) + '/23/' + convert(varchar, year(@from_date)) + ' 00:00:00';
end else
	select	@from_date = dateadd(m, -1, @from_date),
		@to_date   = dateadd(m, -1, @to_date)

update #last_disb
set    retention_event = @retention_event_3
where  last_disbursement_date between @from_date and @to_date

-- Look four months back
if day ( @todays_date ) = 1 -- am of the 1st of a new month
begin
	select	@from_date = dateadd(m, -4, @todays_date);
	select	@from_date = dateadd(d, -1, @from_date);
	select	@to_date   = convert(varchar(10), @from_date, 101) + ' 23:59:59';
	select	@from_date = convert(varchar, month(@from_date)) + '/23/' + convert(varchar, year(@from_date)) + ' 00:00:00';
end else
	select	@from_date = dateadd(m, -1, @from_date),
		@to_date   = dateadd(m, -1, @to_date)

update #last_disb
set    retention_event = @retention_event_4
where  last_disbursement_date between @from_date and @to_date

-- Find the expected deposit amount for the client
select	client, sum(deposit_amount) as deposit_amount
into	#expected_deposit
from	client_deposits
where	isnull(one_time,0) = 0
group by client;

-- Toss clients who have made a deposit in the last 20 days
delete	#last_disb
from	#last_disb d
inner join clients c on d.client = c.client
where	c.last_deposit_date is not null
and	c.last_deposit_date >= dateadd(d, -20, @todays_date);

-- Insert the retention events into the system at this point.
insert into client_retention_events (client, retention_event, amount, message, expire_type, priority, date_expired)
select d.client, e.retention_event, isnull(dep.deposit_amount,0) as amount, 'Client missed disbursement cycle. The last date for the client disbursement was ' + convert(varchar(10), d.last_disbursement_date, 101), e.expire_type, e.priority, null as date_expired
from #last_disb d
inner join retention_events e on e.retention_event = d.retention_event
left outer join #expected_deposit dep on d.client = dep.client
where d.retention_event is not null

drop table #expected_deposit
drop table #last_disb
return ( 1 )
GO
