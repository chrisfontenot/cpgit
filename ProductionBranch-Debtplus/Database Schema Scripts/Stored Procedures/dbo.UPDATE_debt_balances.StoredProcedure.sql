USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[UPDATE_debt_balances]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UPDATE_debt_balances] as

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor

-- Find the payments for this creditor
select client_creditor, sum(isnull(debit_amt,0)) as debit_amt
into #payments
from registers_client_creditor
where tran_type in ('AD', 'BW', 'MD', 'CM')
group by client_creditor;

-- Find the refunds received
select client_creditor, sum(isnull(credit_amt,0)) as credit_amt
into #returns
from registers_client_creditor
where tran_type in ('RR', 'VD', 'RF')
group by client_creditor;

-- Find the interest charged
select client_creditor, sum(isnull(credit_amt,0)) as credit_amt
into #interest
from registers_client_creditor
where tran_type = 'IN'
group by client_creditor;

-- Empty the debt information
update client_creditor
set payments_this_creditor = 0,
    returns_this_creditor = 0,
    interest_this_creditor = 0;

-- Correct the payments to this creditor
update client_creditor
set payments_this_creditor = x.debit_amt
from client_creditor cc
inner join #payments x on cc.client_creditor = x.client_creditor

drop table #payments;

-- Correct the refunds received by the creditor
update client_creditor
set returns_this_creditor = x.credit_amt
from client_creditor cc
inner join #returns x on cc.client_creditor = x.client_creditor

drop table #returns;

-- Correct the interest charged by this creditor
update client_creditor
set interest_this_creditor = x.credit_amt
from client_creditor cc
inner join #interest x on cc.client_creditor = x.client_creditor

-- Make the interest figure the same as the payments for "zero_balance" (fee) creditors.
update	client_creditor
set	interest_this_creditor = payments_this_creditor - returns_this_creditor
where	creditor in (
	select	creditor
	from	creditors
	where	creditor_class in (
		select	creditor_class
		from	creditor_classes
		where	zero_balance = 1
	)
)

drop table #interest;

-- Find the total amount for each of the various fields
select	client_creditor_balance, sum(isnull(payments_this_creditor,0) - isnull(returns_this_creditor,0)) as payments, sum(isnull(interest_this_creditor,0)) as interest
into	#debt_info
from	client_creditor
group by client_creditor_balance;

update	client_creditor_balances
set	total_payments	= 0,
	total_interest	= 0;

update	client_creditor_balances
set	total_payments	= x.payments,
	total_interest	= x.interest
from	client_creditor_balances b
inner join #debt_info x on b.client_creditor_balance = x.client_creditor_balance;

drop table #debt_info;

-- Find the payments for the current and prior months
declare	@month_start	datetime
declare	@month_end	datetime

select	@month_start	= convert(datetime, convert(varchar,month(getdate())) + '/01/' + convert(varchar, year(getdate())) + ' 00:00:00')
select	@month_end	= dateadd(month, 1, @month_start)
select	@month_end	= dateadd(second, -1, @month_end)

select	cc.client_creditor_balance, sum(isnull(debit_amt,0) - isnull(credit_amt,0)) as payment
into	#month_0
from	registers_client_creditor rcc
inner join client_creditor cc on rcc.client_creditor = cc.client_creditor
where	rcc.tran_type in ('AD', 'BW', 'MD', 'CM', 'VD', 'RF', 'RR')
and	rcc.date_created between @month_start and @month_end
group by cc.client_creditor_balance;

update	client_creditor_balances
set	payments_month_0	= 0;

update	client_creditor_balances
set	payments_month_0	= x.payment
from	client_creditor_balances b
inner join #month_0 x on b.client_creditor_balance = x.client_creditor_balance;

drop table #month_0;

select	@month_end	= @month_start
select	@month_start	= dateadd(month, -1, @month_start)
select	@month_end	= dateadd(second, -1, @month_end)

select	cc.client_creditor_balance, sum(isnull(debit_amt,0) - isnull(credit_amt,0)) as payment
into	#month_1
from	registers_client_creditor rcc
inner join client_creditor cc on rcc.client_creditor = cc.client_creditor
where	rcc.tran_type in ('AD', 'BW', 'MD', 'CM', 'VD', 'RF', 'RR')
and	rcc.date_created between @month_start and @month_end
group by cc.client_creditor_balance;

update	client_creditor_balances
set	payments_month_1	= 0;

update	client_creditor_balances
set	payments_month_1	= x.payment
from	client_creditor_balances b
inner join #month_1 x on b.client_creditor_balance = x.client_creditor_balance;

drop table #month_1;
GO
