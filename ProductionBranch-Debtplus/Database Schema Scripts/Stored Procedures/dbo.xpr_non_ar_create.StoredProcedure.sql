USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_non_ar_create]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_non_ar_create] ( @Source AS INT, @LedgerCode AS VarChar(80), @Amount AS Money, @EffectiveDate AS DateTime = NULL, @Description AS VarChar(256) = NULL ) AS

-- ===============================================================================================
-- ==            Transfer money into the non-ar system. Such transfer never leaves.             ==
-- ===============================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

IF @Amount < 0
BEGIN
	RaisError(50019, 16, 1)
	RETURN ( 0 )
END

-- Insert the item into the non-AR register
INSERT INTO registers_non_ar	(tran_type,	non_ar_source,	credit_amt,	dst_ledger_account,	message)
VALUES				('NA',		@Source,	@Amount,	@LedgerCode,		@Description)

-- Terminate normally
RETURN ( SCOPE_IDENTITY() )
GO
