USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_epay_generate_cloout]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_epay_generate_cloout] ( @epay_file as int ) as

-- ==================================================================================================
-- ==            Generate the Close Out messages                                                   ==
-- ==================================================================================================

-- ChangeLog
--    4/15/2003
--     Created but not defined.


-- Find the bank number for the creditors
declare	@bank		int

select	@bank		= bank
from	epay_files with (nolock)
where	epay_file	= @epay_file

if isnull(@bank,0) <= 0
begin
	RaisError ('The file is not associated with a bank number. Please use the new procedures.', 16, 1)
	return ( 0 )
end

-- Find a list of pending dropped debts which have not been sent the notices
select		cc.client_creditor,
			cm.epay_biller_id
into		#pending_epay_drops
from		client_creditor cc with (nolock)
inner join clients c with (nolock) on cc.client = c.client
inner join creditors cr with (nolock)					on cc.creditor = cr.creditor
inner join creditor_methods cm with (nolock)			on cr.creditor_id = cm.creditor and cm.type = 'CLO' and cm.region in (0, c.region)
inner join banks b with (nolock)						on cm.bank = b.bank and 'E' = b.type
where		cc.reassigned_debt = 0
and			cc.drop_reason is not null
and			cc.drop_reason_sent is null
and			cm.bank = @bank;

-- Include the notices into the debt notes table for future use
insert into debt_notes (type, client, creditor, client_creditor, note_text, creditor_method, drop_reason, rpps_biller_id, bank)
select		'DR'				as type,
			cc.client			as client,
			cc.creditor			as creditor,
			cc.client_creditor	as client_creditor,
			'We have discontinued the client' + isnull(' because ' + dr.description, '') as note_text,
			cm.creditor_method	as creditor_method,
			cc.drop_reason		as drop_reason,
			cm.rpps_biller_id	as rpps_biller_id,
			@bank				as bank
from		#pending_epay_drops x
inner join  client_creditor cc on x.client_creditor = cc.client_creditor
inner join creditors cr with (nolock)					on cc.creditor = cr.creditor
inner join creditor_methods cm with (nolock)			on cr.creditor_id = cm.creditor and cm.type = 'CLO' and cm.region in (0, c.region)
left outer join drop_reasons dr with (nolock)			on cc.drop_reason = dr.drop_reason

-- Indicate that we have sent the drop notices to those items that are still pending
update		client_creditor
set			drop_reason_sent	= GETDATE()
from		client_creditor cc
inner join #pending_epay_drops x on cc.client_creditor = x.client_creditor;

return ( 0 )
GO
