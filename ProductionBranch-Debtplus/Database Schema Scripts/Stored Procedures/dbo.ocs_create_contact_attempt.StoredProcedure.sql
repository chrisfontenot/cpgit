SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
StatusCode
ContactAttempts
ActiveFlag
*/
ALTER procedure [ocs_create_contact_attempt] 

      @clientId int = null,
      @user varchar(50) = null,
      @contactType int = null,
      @beginTime datetime = null,
      @endTime datetime = null,
      @resultCode int = null,
      @specificReason varchar(100) = null,
      @statusCode int = null,
      @contactAttempts int = null,
      @activeFlag bit = null,
      
      @p1area varchar(50) = NULL, @p1num varchar(50) = NULL, @p1res int, @p1type varchar(50) = null,
      @p2area varchar(50) = NULL, @p2num varchar(50) = NULL, @p2res int, @p2type varchar(50) = null,
      @p3area varchar(50) = NULL, @p3num varchar(50) = NULL, @p3res int, @p3type varchar(50) = null,
      @p4area varchar(50) = NULL, @p4num varchar(50) = NULL, @p4res int, @p4type varchar(50) = null,
      @p5area varchar(50) = NULL, @p5num varchar(50) = NULL, @p5res int, @p5type varchar(50) = null,
      @p6area varchar(50) = NULL, @p6num varchar(50) = NULL, @p6res int, @p6type varchar(50) = null,

      @Inserted int OUTPUT
AS
BEGIN

-- DO NOT MESS WITH THE COUNTRY FIELD IN THE TELEPHONE NUMBERS OR YOU WILL CRASH THE DATABASE!!!!
-- ==============================================================================================

      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      begin transaction

      begin try

			-- This is the only valid country code for all telephone numbers.
			declare @only_valid_country	int
			select @only_valid_country = min(country) from countries where [default] = 1
			if @only_valid_country is null
				select @only_valid_country = min(country) from countries

            declare @counselorId int
            set @counselorId = (select top 1 Counselor from counselors where Person = @user)

            declare @ocsId int
            set @ocsId = (select top 1 Id from OCS_Client 
                  where ClientId = @clientId and ActiveFlag = 1 and Archive = 0
                  order by Id desc)

            --contact attempt itself
            declare @contactAttempt int
            set @contactAttempt = NULL
            insert into OCS_ContactAttempt(ClientId,[User],ContactType,[Begin],[End],ResultCode,
                  SpecificReason,StatusCode,ContactAttempts,ActiveFlag,CounselorId,OcsId)
            values(@clientId,@user,@contactType,@beginTime,@endTime,@resultCode,
                  @specificReason,@statusCode,@contactAttempts,@activeFlag,@counselorId,@ocsId)
            set @contactAttempt = SCOPE_IDENTITY()

            --update duplicate data on ocs_client
            update OCS_Client
            set StatusCode = @statusCode,
            ContactAttempts = @contactAttempts,
            ActiveFlag = @activeFlag
            where ClientId = @clientId
            and Archive = 0

            --Add Applicant person entry if we need it
            declare @applicant int
            set @applicant = (select Person from people where client = @clientId and relation = 1)
            if @applicant is null and (@p3area is not null or @p4area is not null)
            begin
                  declare @name1 int
                  insert into Names(First,Last)
                  values ('','')
                  set @name1 = SCOPE_IDENTITY()

                  insert into people(Client,NameID,Relation)
                  values(@clientId,@name1,1)
                  set @applicant = SCOPE_IDENTITY()                     
            end

            --Add CoApplicant person entry if we need it
            declare @coapplicant int
            set @coapplicant = (select top 1 Person from people where client = @clientId and relation <> 1)
            if @coapplicant is null and (@p5area is not null or @p6area is not null)
            begin
                  declare @name2 int
                  insert into Names(First,Last)
                  values ('','')
                  set @name2 = SCOPE_IDENTITY()

                  insert into people(Client,NameID,Relation)
                  values(@clientId,@name2,1)
                  set @coapplicant = SCOPE_IDENTITY()                   
            end

            --home telephone
            declare @p1 int
            set @p1 = (select HomeTelephoneID from clients where client = @clientId)
            if @p1area is not null
            begin
                  if @p1 is null
                  begin
                        insert into TelephoneNumbers (Country,Acode,Number,user_selected_type)
                        values (@only_valid_country,@p1area,@p1num,@p1type)
                        set @p1 = SCOPE_IDENTITY()    
                  end
                  else
                  begin
                        update TelephoneNumbers
                        set user_selected_type = @p1type
                        where TelephoneNumber = @p1                           
                  end

                  insert into OCS_CallOutcome(ContactAttempt,PhoneNumber,ResultCode)
                  values(@contactAttempt,(@p1area + @p1num),@p1res)
            end

            --message phone
            declare @p2 int
            set @p2 = (select MsgTelephoneID from clients where client = @clientId)
            if @p2area is not null
            begin
                  if @p2 is null
                  begin
                        insert into TelephoneNumbers (Country,Acode,Number,user_selected_type)
                        values (@only_valid_country,@p2area,@p2num,@p2type)
                        set @p2 = SCOPE_IDENTITY()    
                  end
                  else
                  begin
                        update TelephoneNumbers
                        set user_selected_type = @p2type
                        where TelephoneNumber = @p2                           
                  end

                  insert into OCS_CallOutcome(ContactAttempt,PhoneNumber,ResultCode)
                  values(@contactAttempt,(@p2area + @p2num),@p2res)
            end

            --applicant cell phone
            declare @p3 int
            set @p3 = (select CellTelephoneID from people 
                              where client = @clientId and relation = 1)
            if @p3area is not null
            begin
                  if @p3 is null
                  begin
                        insert into TelephoneNumbers (Country,Acode,Number,user_selected_type)
                        values (@only_valid_country,@p3area,@p3num,@p3type)
                        set @p3 = SCOPE_IDENTITY()
                  end
                  else begin
                        update TelephoneNumbers
                        set user_selected_type = @p3type
                        where TelephoneNumber = @p3         
                  end   
                  
                  update people
                  set CellTelephoneID = @p3
                  where Person = @applicant     

                  insert into OCS_CallOutcome(ContactAttempt,PhoneNumber,ResultCode)
                  values(@contactAttempt,(@p3area + @p3num),@p3res)
            end

            --applicant work phone
            declare @p4 int
            set @p4 = (select WorkTelephoneID from people 
                              where client = @clientId and relation = 1)
            if @p4area is not null
            begin
                  if @p4 is null
                  begin
                        insert into TelephoneNumbers (Country,Acode,Number,user_selected_type)
                        values (@only_valid_country,@p4area,@p4num,@p4type)
                        set @p4 = SCOPE_IDENTITY()
                  end
                  else begin
                        update TelephoneNumbers
                        set user_selected_type = @p4type
                        where TelephoneNumber = @p4         
                  end   
                  
                  update people
                  set WorkTelephoneID = @p4
                  where Person = @applicant     

                  insert into OCS_CallOutcome(ContactAttempt,PhoneNumber,ResultCode)
                  values(@contactAttempt,(@p4area + @p4num),@p4res)
            end

            --CoApplicant Phones
            --coapplicant cell phone
            declare @p5 int
            set @p5 = (select CellTelephoneID from people 
                              where client = @clientId and relation <> 1)
            if @p5area is not null
            begin
                  if @p5 is null
                  begin
                        insert into TelephoneNumbers (Country,Acode,Number,user_selected_type)
                        values (@only_valid_country,@p5area,@p5num,@p5type)
                        set @p5 = SCOPE_IDENTITY()
                  end
                  else begin
                        update TelephoneNumbers
                        set user_selected_type = @p5type
                        where TelephoneNumber = @p5         
                  end   
                  
                  update people
                  set CellTelephoneID = @p5
                  where Person = @coapplicant

                  insert into OCS_CallOutcome(ContactAttempt,PhoneNumber,ResultCode)
                  values(@contactAttempt,(@p5area + @p5num),@p5res)
            end

            --coapplicant work phone
            declare @p6 int
            set @p6 = (select WorkTelephoneID from people 
                              where client = @clientId and relation = 1)
            if @p6area is not null
            begin
                  if @p6 is null
                  begin
                        insert into TelephoneNumbers (Country,Acode,Number,user_selected_type)
                        values (@only_valid_country,@p6area,@p6num,@p6type)
                        set @p6 = SCOPE_IDENTITY()
                  end
                  else begin
                        update TelephoneNumbers
                        set user_selected_type = @p6type
                        where TelephoneNumber = @p6         
                  end   
                  
                  update people
                  set WorkTelephoneID = @p6
                  where Person = @coapplicant     

                  insert into OCS_CallOutcome(ContactAttempt,PhoneNumber,ResultCode)
                  values(@contactAttempt,(@p6area + @p6num),@p6res)
            end

            commit transaction
      end try

      begin catch
        rollback transaction
      end catch
      
      set @Inserted = @contactAttempt
END
GO
