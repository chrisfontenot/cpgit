USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_debt_creditor]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_debt_creditor] ( @client_creditor as int, @creditor as typ_creditor, @clear_msg as int = 1, @account_number as varchar(80) = null) as

-- ====================================================================================================
-- ==            Interface to the "code creditor" function.                                          ==
-- ====================================================================================================

-- ChangeLog
--   06/09/2009
--      Remove references to debt_number and use client_creditor
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

declare	@client			int
declare	@priority		int
declare	@default_priority	int
declare	@prohibit_use		int

-- Suppress intermediate result sets
set nocount on
begin transaction
set xact_abort on

-- Determine the creditor parameters
if @creditor is not null
	select	@prohibit_use		= prohibit_use,
		@default_priority	= usual_priority,
		@creditor		= creditor
	from	creditors
	where	creditor		= @creditor

if @creditor is null
begin
	commit transaction
	RaisError ('The creditor does not exist.', 16, 1)
	return ( 0 )
end

-- Determine the parameters from the config table
declare	@deduct_creditor	varchar(10)
select	@deduct_creditor	= deduct_creditor
from	config

-- If there is no deduct creditor, assume one.
if @deduct_creditor is null
	select	@deduct_creditor = 'Z9999'

-- The creditor must not be a system creditor
if @creditor = @deduct_creditor
begin
	commit transaction
	RaisError ('The creditor is a system creditor and may not be used for debts.', 16, 1)
	return ( 0 )
end

-- Ensure that the parameters are valid
if @default_priority is null
	select	@default_priority	= 9

if @prohibit_use is null
	select	@prohibit_use		= 0

if @prohibit_use > 0
begin
	commit transaction
	RaisError ('The creditor is marked PROHIBIT USE and may not be used.', 16, 1)
	return ( 0 )
end

-- Find the client and priority from the debt number
select	@client		= client,
	@priority	= priority
from	client_creditor
where	client_creditor = @client_creditor

-- Adjust the priority to match the creditor value
if @priority > @default_priority
begin
	update	client_creditor
	set	priority	= @default_priority
	where	client_creditor	= @client_creditor
end

-- Update the debt record with the parameters
update	client_creditor
set	creditor	= @creditor,
	creditor_name	= null
where	client_creditor	= @client_creditor

-- If an account number was specified then update the account number
if not (@account_number is null)
begin
	select	@account_number = ltrim(rtrim(@account_number))
	if @account_number = ''
		select	@account_number = null

	update	client_creditor
	set	account_number	= @account_number
	where	client_creditor	= @client_creditor
end

-- Remove the message parameter from the debt record if needed
if @clear_msg <> 0
	update	client_creditor
	set	message		= null
	where	client_creditor	= @client_creditor

-- Insert the system note that the debt has been assigned
declare	@sysnote	varchar(256)
select	@sysnote = 'The debt has been assigned to ' + dbo.format_client_id ( @client ) + '*' + @creditor
insert into client_notes (client, client_creditor, type, is_text, subject, note) values (@client, @client_creditor, 3, 1, 'Creditor ID assigned', @sysnote)

-- Find the account number
if @account_number is null
	select	@account_number	= account_number
	from	client_creditor with (nolock)
	where	client_creditor	= @client_creditor

-- If the account number is being changed then update the pointer to the payment mask table
if (@account_number is not null)
BEGIN
	declare		@creditor_id	int
	declare		@client_region	int

	select		@creditor_id	= cr.creditor_id,
			@client_region	= c.region
	FROM		client_creditor cc	WITH (NOLOCK)
	inner join	clients c		with (nolock) on cc.client	= c.client
	left outer join creditors cr		WITH (NOLOCK) ON cc.creditor	= cr.creditor
	WHERE		client_creditor	= @client_creditor

	declare	@payment_rpps_mask		int
	declare	@payment_rpps_mask_timestamp	binary(8)

	select		@payment_rpps_mask		= m.creditor_method,
			@payment_rpps_mask_timestamp	= m.[timestamp]
	from		rpps_masks rm
	inner join	rpps_biller_ids ids	with (nolock) on rm.rpps_biller_id = ids.rpps_biller_id
	inner join	creditor_methods m	with (nolock) on ids.rpps_biller_id = m.rpps_biller_id
	inner join	banks b			with (nolock) on b.bank = m.bank

	where		m.region in (0, @client_region)
	and		@account_number like dbo.map_rpps_masks ( rm.mask )
	and		m.creditor	= @creditor_id
	and		m.type		= 'EFT'
	and		b.type		= 'R'

	-- If the mask is not found then generate an error if there is any EFT payment for this creditor. There should be a match.
	if @payment_rpps_mask is null
	begin
		if exists (select * from creditor_methods m with (nolock) inner join banks b with (nolock) on m.bank = b.bank and 'R' = b.type where m.creditor = @creditor_id and m.region in (0, @client_region) AND m.type = 'EFT')
		begin
			RaisError ('The account number is not valid for this creditor. Please correct it.', 16, 1)
			rollback transaction
			return ( 0 )
		end
	end

	-- Update the record with the change
	update	client_creditor
	set	payment_rpps_mask		= @payment_rpps_mask,
		payment_rpps_mask_timestamp	= @payment_rpps_mask_timestamp
	where	client_creditor			= @client_creditor
end

-- Cleanup for the successful operation
commit transaction
return ( 1 )
GO
