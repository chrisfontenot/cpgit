USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_intake_agreement_for_services]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_intake_agreement_for_services] ( @intake_client as int, @intake_id as varchar(20) ) AS
-- =============================================================================================================
-- ==            Indicate that the client agreed to the statement for counseling services.                    ==
-- =============================================================================================================

-- Just update the flag. It will be imported into the clients table when we do the import function.
-- default is zero. When created, the system will make it 0. We need only set it to "1".
update	intake_clients
set	intake_agreement = 1
where	intake_client	= @intake_client
and	intake_id	= @intake_id

return ( @@rowcount )
GO
