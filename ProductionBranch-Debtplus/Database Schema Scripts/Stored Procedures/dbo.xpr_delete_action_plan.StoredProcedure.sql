USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_delete_action_plan]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_delete_action_plan] ( @action_plan as int ) as

-- Discard the plan itself
delete
from		action_plans
where		action_plan	= @action_plan;

return ( @@rowcount )
GO
