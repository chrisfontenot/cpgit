USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_StudentLoanOtherConcerns]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[xpr_insert_StudentLoanOtherConcerns] ( @JobLoss bit = 0, @CreditCards bit = 0, @MedicalDebt bit = 0, @Rent bit = 0, @Other bit = 0, @OtherNotes varchar(2048) = '' ) as
-- ==============================================================================================
-- ==         Insert a row into the StudentLoanOtherConcerns table                             ==
-- ==============================================================================================
insert into StudentLoanOtherConcerns ( JobLoss, CreditCards, MedicalDebt, Rent, Other, OtherNotes ) values ( @JobLoss, @CreditCards, @MedicalDebt, @Rent, @Other, @OtherNotes )
return ( scope_identity() )
GO
