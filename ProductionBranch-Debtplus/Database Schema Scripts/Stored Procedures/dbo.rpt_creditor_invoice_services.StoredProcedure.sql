USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_creditor_invoice_services]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_creditor_invoice_services] ( @from_date as datetime = null, @to_date as datetime = null ) as
-- =======================================================================================================
-- ==            Version of invoicing for creditors that perform specific services                      ==
-- =======================================================================================================

-- ChangeLog
--    6/9/2005
--       Initial draft
--    7/1/2013
--       Do not include creditors 'V68295' or 'V68300'

set nocount on

-- Correct the date ranges
if @to_date is null
	select	@to_date	= getdate()

if @From_date is null
	select	@from_date	= @to_date

select	@from_date	= convert(datetime, convert(varchar(10), @from_date, 101) + ' 00:00:00'),
	@to_date	= convert(datetime, convert(varchar(10), @to_date, 101) + ' 23:59:59')

-- Return the information for the "invoice"
select	cr.creditor,
		cr.creditor_name,
		cc.client,
		convert(varchar(80),isnull(cc.message, cc.account_number)) as account_number,
		dbo.format_normal_name (pn.prefix, pn.first, pn.middle, pn.last, pn.suffix) as client_name,
		sum(b.total_payments) as paid_to_date,
		sum(b.orig_balance + b.orig_balance_adjustment + b.total_interest - b.total_payments) as current_balance

from		client_creditor cc with (nolock)
inner join	clients c with (nolock) on cc.client = c.client
left outer join	people p with (nolock) on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
inner join	client_creditor_balances b with (nolock) on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
inner join	creditors cr with (nolock) on cc.creditor = cr.creditor
inner join	creditor_classes ccl with (nolock) on cr.creditor_class = ccl.creditor_class

where	ccl.creditor_statement = 1
and		cc.reassigned_debt = 0
and		cc.date_created between @from_date and @to_date
and		cr.creditor not in ('V68295', 'V68300')

group by cc.message, cc.account_number, cr.creditor, cr.creditor_name, cc.client, pn.prefix, pn.first, pn.middle, pn.last, pn.suffix

order by 1, 3

return ( @@rowcount )
GO
