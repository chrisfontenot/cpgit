USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_statements_stamp]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_statements_stamp](@batch as int, @client as int) as

-- Update the client statement to show that it was delivered to the web
update	client_statement_clients
set

-- Remove the comment to NOT print statements that are read electronically
--	delivery_method		= 'E',

	delivery_date		= getdate()
where	client_statement_batch	= @batch
and	client			= @client

return ( @@rowcount )
GO
