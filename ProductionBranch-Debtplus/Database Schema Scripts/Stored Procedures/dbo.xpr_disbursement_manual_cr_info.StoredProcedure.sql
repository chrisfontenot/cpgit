SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [xpr_disbursement_manual_cr_info] ( @creditor AS typ_creditor ) AS

-- ====================================================================================================================
-- ==            Fetch the information for the creditor in a manual disbursement process                             ==
-- ====================================================================================================================

-- ChangeLog
--   1/5/2002
--     Support for check/eft deduction type. Use the "check" info.

declare	@bank	int

select	@bank		= check_bank
from	creditors with (nolock)
where	creditor	= @creditor

-- This must be a checking account
if @bank is not null
begin
	if not exists (select bank from banks where type = 'C' and bank = @bank)
		select	@bank = 0
end

-- Find the default checking account if one was not specified
if isnull(@bank,0) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'
	and	[default]	= 1

if isnull(@bank,0) <= 0
	select	@bank		= min(bank)
	from	banks with (nolock)
	where	type		= 'C'

if isnull(@bank,0) <= 0
	select	@bank		= 1

-- Find the parameters for the current bank information
SELECT		isnull(pct.creditor_type_check,'N')				as 'creditor_type',
		isnull(cr.prohibit_use,0)					as 'prohibit_use',
		isnull(cr.voucher_spacing,0)					as 'voucher_spacing',
		coalesce(cr.max_clients_per_check,b.max_clients_per_check,40)	as 'max_clients_per_check',
		coalesce(cr.max_amt_per_check,b.max_amt_per_check,0)	as 'max_amt_per_check',
		isnull(fairshare_pct_check,0)					as 'fairshare_pct_check'

FROM		creditors cr with (nolock)
LEFT OUTER JOIN	banks b with (nolock) ON b.bank = @bank
LEFT OUTER JOIN creditor_contribution_pcts pct with (nolock) on cr.creditor_contribution_pct = pct.creditor_contribution_pct
WHERE		cr.creditor = @creditor

RETURN ( @@rowcount )
GO
