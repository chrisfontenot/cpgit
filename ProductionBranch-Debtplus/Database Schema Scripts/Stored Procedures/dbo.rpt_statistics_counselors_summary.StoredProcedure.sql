USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_statistics_counselors_summary]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_statistics_counselors_summary] as

-- ======================================================================================================
-- ==            Retrieve the last 12 dates for the office statistics                                  ==
-- ======================================================================================================

set nocount on

-- Retrieve the distinctive dates from the items
select distinct period_start
into		#raw_dates
from		statistics_counselors with (nolock);

-- Take the most recient 12 dates
select top 12	period_start
into		#date_selections
from		#raw_dates
order by	1 desc;

-- Return the information from the statistics
select	s.period_start,
	coalesce(co.name, 'counselor #' + convert(varchar, s.counselor), 'UNSPECIFIED') as 'counselor_name',
	sum(s.active_clients) as clients_active,
	sum(s.clients_disbursed) as clients_disbursed,
	sum(s.dropped_clients) as clients_dropped,
	sum(s.new_dmp_clients) as clients_new_dmp,
	sum(s.new_dmp_debt) as debt_new_dmp,
	sum(s.expected_disbursement) as disbursement_expected,
	sum(s.actual_disbursement) as disbursement_actual,

	case
		when sum(s.expected_disbursement) <= 0 then 0
		else convert(float,sum(s.actual_disbursement)) / convert(float, sum(s.expected_disbursement)) * 100.0
	end as disbursement_percentage,

	sum(paf_fees) as fees_paf
	
from	statistics_counselors s with (nolock)
left outer join offices o with (nolock) on s.office = o.office
left outer join counselors co with (nolock) on s.counselor = co.counselor

where	period_start in (select period_start from #date_selections)
and	s.actual_disbursement <> 0

group by s.period_start, co.name, s.counselor
order by 1, 2;

-- Discard the working tables
drop table #date_selections
drop table #raw_dates

return ( @@rowcount );
GO
