USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_creditor_client_info]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[www_creditor_client_info] ( @creditor as varchar(10), @client_creditor as int ) AS
select	c.client
from	client_creditor cc with (nolock)
inner join clients c with (nolock) on cc.client = c.client
where	cc.creditor = @creditor
and	cc.client_creditor = @client_creditor
return ( @@rowcount )
GO
