USE [Debtplus]
GO
if NOT EXISTS(SELECT * FROM sysobjects WHERE type = 'P' AND name = 'nfmcp_text_extract')
BEGIN
	EXEC ('CREATE PROCEDURE nfmcp_text_extract AS RETURN 0')
	EXEC ('GRANT EXECUTE ON nfmcp_text_extract TO public AS dbo')
	EXEC ('DENY EXECUTE ON nfmcp_text_extract TO www_role')
END
GO
ALTER PROCEDURE [dbo].[nfmcp_text_extract]  (@from_date as datetime = null, @to_date as datetime = null ) as

--  ChangeLog
--    5/14/08
--    - Used codes in the NFCC column of the 'Financial Problems' table of LOI (Loss of Income), IEX (Increase in Expense),
--	    BVF (Business Venture Failed), and ILP (Increase in Loan Payment).
--    - Added test to ensure that resulted clients are for the proper hud_grant type
--    6/4/08 
--    - modified session length to decimal format
--    6/19/08
--    - Corrected loan status
--    7/7/08
--    - Added counselor
--    7/9/08
--    - Added hud_interview ID to the selection table
--    8/4/08
--    - Ensured that only priority 1 loans were processed
--    8/5/08
--    - Changed date format to MM/DD/YYYY.
--      It is not needed since it was always MM/DD/YYYY.

-- Suppress intermediate results
set nocount on

-- Build the resulting table using the common logic
-- If there is no table then just quit here.
declare @result		int
execute @result = nfmcp_extract_select @from_date, @to_date
if @result = 0
	return ( 0 );

-- Race
update	##tmp_nfcc_nfmc_extract
set		Race = rt.description
from	##tmp_nfcc_nfmc_extract x
left outer join RaceTypes rt on x.Race = rt.nfmcp_section

-- Gender
update	##tmp_nfcc_nfmc_extract
set		Gender = rt.description
from	##tmp_nfcc_nfmc_extract x
left outer join GenderTypes rt on x.Gender = rt.nfmcp_section

-- Ethnicity
update	##tmp_nfcc_nfmc_extract
set		Ethnicity = rt.description
from	##tmp_nfcc_nfmc_extract x
left outer join EthnicityTypes rt on x.Ethnicity = rt.nfmcp_section

-- Household Type
update	##tmp_nfcc_nfmc_extract
set		HouseholdType = rt.description
from	##tmp_nfcc_nfmc_extract x
left outer join HouseholdHeadTypes rt on x.HouseholdType = rt.nfmcp_section

-- Income Category. This is not in a table as it is simple values so just use a case statement.
update	##tmp_nfcc_nfmc_extract
set		IncomeCategory = case IncomeCategory
							when 'A' then 'less than 50%'
							when 'B' then 'between 50 and 80%'
							when 'C' then 'between 80 and 100%'
							when 'D' then 'more than 100%'
							when ''  then 'not specified'
							else IncomeCategory
						  end

-- 1st or 2nd loan
update	##tmp_nfcc_nfmc_extract set FirstOrSecondLoan = case FirstOrSecondLoan when '2' then 'Second' else 'First' end

-- Loan Product type
update	##tmp_nfcc_nfmc_extract
set		LoanProductType = t.description
from	##tmp_nfcc_nfmc_extract x
inner join housing_loantypes t on x.LoanProductType = t.nfmcp_section

-- Counseling Mode
update	##tmp_nfcc_nfmc_extract
set		CounselingMode = t.description
from	##tmp_nfcc_nfmc_extract x
inner join FirstContactTypes t on x.CounselingMode = t.nfmcp_section

-- Indicators for Y and N
update	##tmp_nfcc_nfmc_extract set InterestOnly	= case InterestOnly when 0 then 'N' else 'Y' end
update	##tmp_nfcc_nfmc_extract set Hybrid			= case Hybrid when 0 then 'N' else 'Y' end
update	##tmp_nfcc_nfmc_extract set OptionARM		= case OptionARM when 0 then 'N' else 'Y' end
update	##tmp_nfcc_nfmc_extract set VAorHFAInsured	= case VAorHFAInsured when 0 then 'N' else 'Y' end
update	##tmp_nfcc_nfmc_extract set PrivatelyHeld	= case PrivatelyHeld when 0 then 'N' else 'Y' end
update	##tmp_nfcc_nfmc_extract set ARMReset		= case ARMReset when 0 then 'N' else 'Y' end
update	##tmp_nfcc_nfmc_extract set HasSecondLoan	= case HasSecondLoan when 0 then 'N' else 'Y' end

-- Default Reason
update	##tmp_nfcc_nfmc_extract
set		DefaultReasonCode = t.description
from	##tmp_nfcc_nfmc_extract x
inner join financial_problems t on x.DefaultReasonCode = t.nfmcp_section

-- LoanStatusAtContact
update	##tmp_nfcc_nfmc_extract
set		LoanStatusAtContact = case isnull(LoanStatusAtContact,-1)
								when -1 then 'Unknown'
								when  1 then 'Current'
								when  2 then '30 days'
								when  3 then '60 days'
								when  4 then '90 days'
								when  5 then '120+ days'
								        else 'Invalid'
							  end
from	##tmp_nfcc_nfmc_extract

-- CounselingOutcomeCode
update	##tmp_nfcc_nfmc_extract
set		CounselingOutcomeCode	= r.description
from	##tmp_nfcc_nfmc_extract x
inner join [housing_AllowedVisitOutcomeTypes] h ON x.CounselingOutcomeCode = h.nfmcp_section
inner join [housing_ResultTypes] r ON h.Outcome = r.oID

-- WhyNoCreditScore
update	##tmp_nfcc_nfmc_extract
set		WhyNoCreditScore	= t.description
from	##tmp_nfcc_nfmc_extract x
inner join Housing_FICONotIncludedReasons t on x.WhyNoCreditScore = t.nfmcp_section

-- Branch name
update	##tmp_nfcc_nfmc_extract
set		BranchName			= o.name
from	##tmp_nfcc_nfmc_extract x
inner join offices o on x.office = o.office;

-- Create the table for the results and populate it
create table #results (location varchar(80), value varchar(1024));

-- Discard the clients who have indicated that they have NOT signed the privacy policy.
-- This only applies to phone counseling.
update	##tmp_nfcc_nfmc_extract
set		discard_reason = 'Not signed privacy policy'
where	CounselingMode = 'Phone'
and		indicator_date is null
and		datediff(d, CounselingIntakeDate, getdate()) < 30;

-- Toss the clients who inhibit data. But count them first.
/*
insert into #results (location, value)
select	'B4', convert(varchar,count(*))
from	##tmp_nfcc_nfmc_extract
where	inhibit_date is not null;
*/

update	##tmp_nfcc_nfmc_extract
set		discard_reason = 'Refused to share information'
where	inhibit_date is not null;

-- Serialize the line numbers in the table now that the items have been deleted.
-- what is left, we report.
create table #lines (sequenceID int, clientid int, BranchName varchar(50) null, line int identity(2,1));

insert into #lines (sequenceID, ClientID, BranchName)
select sequenceID, ClientID, BranchName
from ##tmp_nfcc_nfmc_extract
order by clientID, BranchName

update ##tmp_nfcc_nfmc_extract
set    line = l.line
from   ##tmp_nfcc_nfmc_extract x
inner join #lines l on x.SequenceID = l.SequenceID;

drop table #lines

-- Branch ID
insert into #results (location, value)
select 'A' + convert(varchar,line), BranchName
from ##tmp_nfcc_nfmc_extract

-- Client ID
insert into #results (location, value)
select 'B' + convert(varchar,line), ClientID
from ##tmp_nfcc_nfmc_extract

-- Counseling Level
insert into #results (location, value)
select 'C' + convert(varchar,line), CounselingLevel
from ##tmp_nfcc_nfmc_extract

-- Intake date
insert into #results (location, value)
select 'D' + convert(varchar,line), convert(varchar(10), CounselingIntakeDate, 101)
from ##tmp_nfcc_nfmc_extract

-- Counseling Mode
insert into #results (location, value)
select 'E' + convert(varchar,line), CounselingMode
from ##tmp_nfcc_nfmc_extract

-- Identity
insert into #results (location, value)
select 'F' + convert(varchar,line), FirstName
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'G' + convert(varchar,line), LastName
from ##tmp_nfcc_nfmc_extract

-- Age
insert into #results (location, value)
select 'H' + convert(varchar,line), Age
from ##tmp_nfcc_nfmc_extract

-- Race
insert into #results (location, value)
select 'I' + convert(varchar,line), Race
from ##tmp_nfcc_nfmc_extract

-- Ethnicicty
insert into #results (location, value)
select 'J' + convert(varchar,line), Ethnicity
from ##tmp_nfcc_nfmc_extract

-- Gender
insert into #results (location, value)
select 'K' + convert(varchar,line), Gender
from ##tmp_nfcc_nfmc_extract

-- Household type
insert into #results (location, value)
select 'L' + convert(varchar,line), HouseholdType
from ##tmp_nfcc_nfmc_extract

-- Household income
insert into #results (location, value)
select 'M' + convert(varchar,line), HouseholdIncome
from ##tmp_nfcc_nfmc_extract

-- Income category
insert into #results (location, value)
select 'N' + convert(varchar,line), IncomeCategory
from ##tmp_nfcc_nfmc_extract

-- Address
insert into #results (location, value)
select 'O' + convert(varchar,line), HouseNo
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'P' + convert(varchar,line), Street
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'Q' + convert(varchar,line), City
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'R' + convert(varchar,line), [State]
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'S' + convert(varchar,line), Zip
from ##tmp_nfcc_nfmc_extract

-- Total individual foreclosure hours received
insert into #results (location, value)
select 'T' + convert(varchar,line), isnull(Total_Individual_foreclosure_hours_received,'0')
from ##tmp_nfcc_nfmc_extract

-- Total group foreclosure hours received
insert into #results (location, value)
select 'U' + convert(varchar,line), isnull(Total_group_foreclosure_hours_received,'0')
from ##tmp_nfcc_nfmc_extract

-- Name of originating lender
insert into #results (location, value)
select 'V' + convert(varchar,line), NameofOriginatingLender
from ##tmp_nfcc_nfmc_extract

-- FDIC of originating lender
insert into #results (location, value)
select 'W' + convert(varchar,line), FDICofOriginalLender
from ##tmp_nfcc_nfmc_extract

-- Original Loan Number
insert into #results (location, value)
select 'X' + convert(varchar,line), OriginalLoanNumber
from ##tmp_nfcc_nfmc_extract

-- Current loan servicer
insert into #results (location, value)
select 'Y' + convert(varchar,line), CurrentLoanServicer
from ##tmp_nfcc_nfmc_extract

-- FDIC of current servicer
insert into #results (location, value)
select 'Z' + convert(varchar,line), Current_Servicer_FDIC
from ##tmp_nfcc_nfmc_extract

-- Current service loan number
insert into #results (location, value)
select 'AA' + convert(varchar,line), CurrentServicerLoanNo
from ##tmp_nfcc_nfmc_extract

-- Credit score
insert into #results (location, value)
select 'AB' + convert(varchar,line), CreditScore
from ##tmp_nfcc_nfmc_extract

-- Why no credit score
insert into #results (location, value)
select 'AC' + convert(varchar,line), WhyNoCreditScore
from ##tmp_nfcc_nfmc_extract

-- Credit score type
insert into #results (location, value)
select 'AD' + convert(varchar,line), ScoreType
from ##tmp_nfcc_nfmc_extract

-- Total Monthly PITI as intake
insert into #results (location, value)
select 'AE' + convert(varchar,line), PITIatIntake
from ##tmp_nfcc_nfmc_extract

-- First or second loan
insert into #results (location, value)
select 'AF' + convert(varchar,line), FirstOrSecondLoan
from ##tmp_nfcc_nfmc_extract

-- Has second loan
insert into #results (location, value)
select 'AG' + convert(varchar,line), HasSecondLoan
from ##tmp_nfcc_nfmc_extract

-- Loan Product Type
insert into #results (location, value)
select 'AH' + convert(varchar,line), LoanProductType
from ##tmp_nfcc_nfmc_extract

-- Interest only
insert into #results (location, value)
select 'AI' + convert(varchar,line), InterestOnly
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AJ' + convert(varchar,line), Hybrid
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AK' + convert(varchar,line), OptionARM
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AL' + convert(varchar,line), VAorHFAInsured
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AM' + convert(varchar,line), PrivatelyHeld
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AN' + convert(varchar,line), ARMReset
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AO' + convert(varchar,line), DefaultReasonCode
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AP' + convert(varchar,line), LoanStatusAtContact
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AQ' + convert(varchar,line), CounselingOutcomeCode
from ##tmp_nfcc_nfmc_extract

insert into #results (location, value)
select 'AR' + convert(varchar,line), convert(varchar(10), CounselingOutcomeDate, 101)
from ##tmp_nfcc_nfmc_extract

-- Text version includes a reason why it is not included in the non-text version
insert into #results (location, value)
select 'AS' + convert(varchar,line), discard_reason
from ##tmp_nfcc_nfmc_extract

-- Return the results
select 'NFMC Reporting Template Ver. 2' as sheet, location, value
from #results
where value is not null
order by location;

drop table #results;
-- drop table ##tmp_nfcc_nfmc_extract;
return ( 1 );
GO
