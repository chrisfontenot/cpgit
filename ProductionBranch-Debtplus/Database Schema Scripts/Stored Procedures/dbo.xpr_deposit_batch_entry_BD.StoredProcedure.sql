USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_deposit_batch_entry_BD]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_deposit_batch_entry_BD] ( @batch as INT, @scanned_client as varchar(20), @scanned_date as datetime, @amount as Money, @reference as varchar(50) = NULL, @ErrorMessage as varchar(50) = NULL, @tran_type as varchar(10) = 'BD' )  AS 
-- =====================================================================================================
-- ==                  Create a entry into the system for the deposit                                 ==
-- =====================================================================================================

-- ChangeLog
--   5/16/2011
--      Copied from xpr_deposit_batch_entry_LB to do additional checking on reference being the zipcode
--		Used to Process deposits from Vanco, VB = Vanco BK or VM = Vanco DMP  
-- Prevent intermediate result sets
SET NOCOUNT ON

-- Try to find the client information
DECLARE	@Client		int
DECLARE	@ActiveStatus	varchar(10)

-- Remove extra spaces from the input
select	@scanned_client	= ltrim(rtrim(@scanned_client)),
		@reference		= ltrim(rtrim(@reference)),
		@ErrorMessage	= ltrim(rtrim(ISNULL(@ErrorMessage,'')))

-- Do not attempt to load the hash total client even if the config file does not contain
-- the proper reference to the hash total.
if @scanned_client = '9999999'
	return 0

if @ErrorMessage = ''
	select	@ErrorMessage = null

IF @scanned_client = ''
	SELECT @scanned_client = null

IF @ErrorMessage IS NULL
	IF @scanned_client IS NULL
		SELECT @ErrorMessage = 'Invalid ID'

IF @ErrorMessage IS NULL
	IF len(@scanned_client) > 10
		SELECT @ErrorMessage = 'Invalid ID'

IF @ErrorMessage IS NULL
	IF right('000000000000000000000' + @scanned_client, 10) > '2147483647'
		SELECT @ErrorMessage = 'Invalid ID'

IF @ErrorMessage IS NULL
	if isnumeric(@scanned_client) = 0
		SELECT @ErrorMessage = 'Invalid ID'

IF @ErrorMessage IS NULL
BEGIN
	SELECT @Client = convert(int, @scanned_client)
	IF @Client <= 0
		SELECT @ErrorMessage = 'Invalid ID'
END

IF @ErrorMessage IS NULL
	IF NOT EXISTS (select * FROM clients WHERE client = @client)
		SELECT @ErrorMessage = 'Not on file'

IF @ErrorMessage IS NULL
BEGIN
	SELECT	@ActiveStatus = isnull(active_status,'CRE')
	FROM	clients WITH (NOLOCK)
	WHERE	client = @Client

	IF @ActiveStatus = 'CRE'
		SELECT @ErrorMessage = 'Not Active'
END

-- Check the zipcode for the client
if @ErrorMessage is null
begin
	declare	@valid_zipcode	int
	declare	@addressid		int
	
	select	@addressid = addressid
	from	clients with (nolock)
	where	client = @client;
	
	if @addressid is not null
		select	@valid_zipcode	= count(*)
		from	addresses with (nolock)
		where	[address] = @addressid
		and		left(isnull([postalcode],'')+'00000', 5) = left(isnull(@reference,'')+'00000',5)

	if isnull(@valid_zipcode,0) < 1
		select	@ErrorMessage = 'Invalid Zipcode'
end

-- Is the transaction valid to be posted?
declare	@ok		int
SELECT	@OK	= 0

IF @ErrorMessage IS NULL
	SELECT	@OK = 1

-- Insert the item into the batch
INSERT INTO deposit_batch_details	(deposit_batch_id,	ok_to_post,	tran_subtype,	scanned_client,		client,		amount,		item_date,		reference,	message,		created_by,		date_created,	date_posted)
VALUES								(@batch,			@ok,		@tran_type,		@scanned_client,	@client,	@amount,	@scanned_date,	@reference,	@ErrorMessage,	suser_sname(),	getdate(),		null)

if @ok = 1
	SELECT @ok = SCOPE_IDENTITY()

-- Return the record number or 0 to indicate a failure
RETURN ( @ok )
GO
