USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[www_client_geninfo_appointments]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[www_client_geninfo_appointments] ( @client as int ) as
-- =================================================================================
-- ==            Return the appointment information                               ==
-- =================================================================================

-- Suppress intermediate results
set nocount on

-- =================================================================================
-- ==           The first recordset is for the future appointments                ==
-- =================================================================================
select
	apt.start_time					as 'start_time',
	o.name						as 'office',
	dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor',
	convert(varchar(10), apt.date_confirmed,1)	as 'date_confirmed',
	case apt.status
		when 'P' then 'Pending'
		when 'M' then 'No show'
		when 'C' then 'Cancelled'
		when 'R' then 'Re-Scheduled'
		when 'K' then 'Kept - Thank you'
	end						as 'status'
from	client_appointments apt with (nolock)
left outer join offices o with (nolock) on apt.office = o.office
left outer join counselors co with (nolock) on apt.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

where	apt.client = @client
and	apt.start_time >= getdate()
order by 1

-- =================================================================================
-- ==           The second recordset is for the previous appointments             ==
-- =================================================================================
select
	apt.start_time					as 'start_time',
	o.name						as 'office',
	dbo.format_normal_name(default,cox.first,default,cox.last,default) as 'counselor',
	convert(varchar(10), apt.date_confirmed,1)	as 'date_confirmed',
	case apt.status
		when 'P' then 'No show'
		when 'M' then 'No show'
		when 'C' then 'Cancelled'
		when 'R' then 'Re-Scheduled'
		when 'K' then 'Kept - Thank you'
	end						as 'status'
from	client_appointments apt with (nolock)
left outer join offices o with (nolock) on apt.office = o.office
left outer join counselors co with (nolock) on apt.counselor = co.counselor
left outer join names cox with (nolock) on co.NameID = cox.name

where	apt.client = @client
and	apt.start_time < getdate()
order by 1 desc
GO
