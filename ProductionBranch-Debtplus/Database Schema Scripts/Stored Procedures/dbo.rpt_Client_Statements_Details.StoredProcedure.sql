USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_Client_Statements_Details]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_Client_Statements_Details] ( @client_statement_batch as int = null, @client as int = null ) as
-- ===========================================================================================
-- ==        Creditor Payment Information                                                   ==
-- ===========================================================================================

select	v.client_statement_detail		as client_statement_detail,
	v.current_balance			as current_balance,
	v.credits				as credits,
	isnull(v.interest_rate,0) * 100.0	as interest_rate,
	v.sched_payment				as sched_payment,
	v.payments_to_date			as payments_to_date,
	v.original_balance			as original_balance,
	v.creditor				as creditor,
	v.formatted_account_number		as account_number,
	v.creditor_name				as creditor_name,
	v.net_debits				as current_payment

from	view_client_statement_details v with (nolock)
where	client_statement_batch	= @client_statement_batch
and	client			= @client
order by v.creditor, v.client_statement_detail

return ( @@rowcount )
GO
