USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_insert_secured_loan]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_insert_secured_loan] ( @secured_property as typ_key,@lender as Varchar(50) = '',@account_number as Varchar(50) = '',@interest_rate as Float = 0.0,@payment as Money = 0,@original_amount as Money = 0,@balance as Money = 0,@periods as Int = 0,@past_due_amount as Money = 0,@past_due_periods as Int = 0 ) AS
-- =========================================================================================================================================
-- ==            Create the secured loan for .NET                                                                                         ==
-- =========================================================================================================================================
SET NOCOUNT ON

-- Add the record to the system
insert into secured_loans ( [secured_property],[lender],[account_number],[interest_rate],[payment],[original_amount],[balance],[periods],[past_due_amount],[past_due_periods] ) VALUES ( @secured_property,@lender,@account_number,@interest_rate,@payment,@original_amount,@balance,@periods,@past_due_amount,@past_due_periods )
return ( scope_identity() )
GO
