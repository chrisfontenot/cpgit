USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_client_status_summary]    Script Date: 09/15/2014 13:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[rpt_client_status_summary] ( @Client AS INT ) AS
-- ==============================================================================================
-- ==              Generate the client status summary information for CMS reports              ==
-- ==============================================================================================

-- Suppress intermediate resuls
set nocount on

-- Determine the counselor for the client
declare	@counselor	varchar(256)
select	@counselor	= dbo.format_normal_name(default,n.first,default,n.last,default)
from	counselors co WITH (NOLOCK)
INNER JOIN clients c with (nolock) on co.counselor = c.counselor
LEFT OUTER JOIN names n with (nolock) on co.NameID = n.name
where   c.client = @client

if @counselor is null
	select	@counselor = suser_sname()

-- Determine the agency location information
declare @name varchar(256)
declare @addr1 varchar(256)
declare @addr2 varchar(256)
declare	@addr3 varchar(256)
declare @phone varchar(20)

SELECT	@name  = upper(c.name),
		@addr1 = isnull(dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value),''),
		@addr2 = isnull(a.address_line_2,''),
		@addr3 = isnull(dbo.format_city_state_zip(a.city, a.state, a.postalcode),''),
		@phone = dbo.format_TelephoneNumber ( c.TelephoneID )
FROM	config c
left outer join addresses a on c.addressid = a.address

if @addr2 = ''
	SELECT	@addr2 = @addr3,
		@addr3 = ''

if @addr1 = ''
	SELECT	@addr1 = @addr2,
		@addr2 = @addr3,
		@addr3 = ''

-- Return the information for the client
SELECT		c.client					as 'client',
			c.held_in_trust				as 'available_escrow',
			@counselor					as 'counselor',
			c.last_deposit_amount		as 'last_payment',
			c.last_deposit_date			as 'payment_date',

			isnull(dbo.format_normal_name(pn.prefix, pn.first, pn.middle, pn.last, pn.suffix),'') as client_name,
			dbo.format_address_line_1(a.house, a.direction, a.street, a.suffix, a.modifier, a.modifier_value)	as 'client_addr1',
			a.address_line_2			as 'client_addr2',
			dbo.format_city_state_zip(a.city, a.state, a.postalcode) as 'client_addr3',

			@name						as 'agency_name',
			case when @addr1 = '' then null else @addr1 end	as 'agency_addr1',
			case when @addr2 = '' then null else @addr2 end	as 'agency_addr2',
			case when @addr3 = '' then null else @addr3 end	as 'agency_addr3',
			@phone						as 'agency_phone'

FROM		clients c WITH (NOLOCK)
LEFT OUTER JOIN people p WITH (NOLOCK) ON c.client = p.client AND 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
left outer join addresses a with (nolock) on c.addressid = a.address
WHERE		c.client = @client

RETURN ( @@rowcount )
GO
