USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[TEMP add_timestamp]    Script Date: 09/15/2014 13:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[TEMP add_timestamp] as

-- Add timestamp columns
declare stmt_cursor cursor for
	select	'alter table [' + t.name + '] add [ts] timestamp not null;'
	from	sysobjects t
	where	t.name not in ('numbers', 'dtproperties', 'sysdiagrams', 'CitiCreditors', 'AMI_hh_adjustment', 'obsolete hud_cars_9902_summary', 'round8_low_income_zips', 'round8_minority_zips', 'ZipCodeSearch-new', 'zip_msa', 'zip_to_timezone')
	and		t.type = 'U'
	and		t.id not in
			(
				select id
				from	syscolumns
				where	xtype = 189
			)
	order by 1

declare	@stmt		varchar(800)
open stmt_cursor
fetch stmt_cursor into @stmt

while @@fetch_status = 0
begin
	print @stmt
-- 	exec ( @stmt )

	fetch stmt_cursor into @stmt
end

close stmt_cursor
deallocate stmt_cursor
GO
