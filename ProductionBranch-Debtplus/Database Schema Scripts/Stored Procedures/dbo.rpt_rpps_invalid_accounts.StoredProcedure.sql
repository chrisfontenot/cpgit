USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[rpt_rpps_invalid_accounts]    Script Date: 09/15/2014 13:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[rpt_rpps_invalid_accounts] as

-- ======================================================================================================
-- ==          List the accounts which are invalid for the RPPS system                                 ==
-- ======================================================================================================

-- ChangeLog
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id

-- Surpress intermediate results
set nocount on

-- Create a table to hold the suspect information
create table #suspect_accounts ( region int, creditor int, client_creditor int, account_number varchar(80), rpps_mask int null, rpps_mask_timestamp binary(8) null, payment_rpps_mask int null, payment_rpps_mask_timestamp binary(8) null, new_rpps_mask int null, new_rpps_mask_timestamp binary(8) null, new_payment_rpps_mask int null, new_payment_rpps_mask_timestamp binary(8) null);

-- Build a list of the accounts which have been changed since the last time
insert into #suspect_accounts (region, creditor, client_creditor, account_number, rpps_mask, rpps_mask_timestamp, payment_rpps_mask, payment_rpps_mask_timestamp )

select		c.region,
		cr.creditor_id,
		cc.client_creditor,
		cc.account_number,
		cc.rpps_mask,
		convert(binary(8), null) as rpps_mask_timestamp,
		cc.payment_rpps_mask,
		convert(binary(8), null) as payment_rpps_mask_timestamp

from		client_creditor cc
inner join	clients c on cc.client = c.client
inner join	creditors cr on cc.creditor = cr.creditor
left outer join	rpps_masks mk on cc.rpps_mask = mk.rpps_mask
where		(mk.rpps_mask is null or mk.[timestamp] <> cc.rpps_mask_timestamp)
and		cc.rpps_mask is not null
and		cc.rpps_mask_timestamp is not null

union

select		c.client,
		cr.creditor_id,
		cc.client_creditor,
		cc.account_number,
		cc.rpps_mask,
		convert(binary(8), null) as rpps_mask_timestamp,
		cc.payment_rpps_mask,
		convert(binary(8), null) as payment_rpps_mask_timestamp
from		client_creditor cc
inner join	clients c on cc.client = c.client
inner join	creditors cr on cc.creditor = cr.creditor
left outer join	creditor_methods mt on cc.payment_rpps_mask = mt.creditor_method
where		(mt.creditor_method is null or mt.[timestamp] <> cc.payment_rpps_mask_timestamp)
and		cc.payment_rpps_mask is not null
and		cc.payment_rpps_mask_timestamp is not null

-- Set the new settings for the accounts
update		#suspect_accounts
set		new_rpps_mask			= mk.rpps_mask,
		new_rpps_mask_timestamp		= mk.[timestamp],
		new_payment_rpps_mask		= mt.creditor_method,
		new_payment_rpps_mask_timestamp	= mt.[timestamp]
from		#suspect_accounts a
inner join	creditor_methods mt on a.creditor = mt.creditor and 'EFT' = mt.type and mt.region in (0, a.region)
inner join	banks b on mt.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids ids on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks mk on ids.rpps_biller_id = mk.rpps_biller_id
where		a.account_number like dbo.map_rpps_masks ( mk.mask )

-- Reset the items which have invalid checksums
update		#suspect_accounts
set		new_rpps_mask			= null,
		new_rpps_mask_timestamp		= null
from		#suspect_accounts a
inner join	rpps_masks mk on a.new_rpps_mask = mk.rpps_mask
where		mk.checksum = 3
and		dbo.valid_checksum_luhn ( a.account_number ) = 0

-- Update the debt record with the new information
update		client_creditor
set		rpps_mask			= new_rpps_mask,
		rpps_mask_timestamp		= new_rpps_mask_timestamp,
		payment_rpps_mask		= new_payment_rpps_mask,
		payment_rpps_mask_timestamp	= new_payment_rpps_mask_timestamp

from		client_creditor cc
inner join	#suspect_accounts a on cc.client_creditor = a.client_creditor;

-- Build a list of the possible error items
select	cc.client,
		cc.creditor,
		isnull (cc.client_name, dbo.format_normal_name ( pn.prefix, pn.first, pn.middle, pn.last, pn.suffix )) as client_name,
		cr.creditor_name,
		cc.account_number,
		convert(varchar(80), null) as account_mask,
		convert(int,null) as account_length,
		convert(int,null) as checksum,
		convert(varchar(80), null) as error_condition,

		case when cc.rpps_mask is null or cc.rpps_mask_timestamp is null then null else cc.payment_rpps_mask end		as payment_rpps_mask,
		case when cc.rpps_mask is null or cc.rpps_mask_timestamp is null then null else cc.payment_rpps_mask_timestamp end	as payment_rpps_mask_timestamp,
		cc.rpps_mask,
		cc.rpps_mask_timestamp,
		cr.creditor_id,
		convert(int,0) as to_be_checked,
		isnull(c.region,0) as region,
		cc.client_creditor,
		convert(varchar(80), null) as rpps_biller_id

into		#invalid_accounts

from		client_creditor cc

left outer join	clients c on cc.client = c.client
left outer join people p on cc.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
inner join	client_creditor_balances b on cc.client_creditor_balance = b.client_creditor_balance and b.client_creditor = cc.client_creditor
inner join	creditors cr on cc.creditor = cr.creditor
inner join	creditor_classes cl on cr.creditor_class = cl.creditor_class

where		(cc.payment_rpps_mask_timestamp is null or cc.rpps_mask_timestamp is null)
and		c.active_status in ('A','AR')
and		cc.reassigned_debt = 0
and		((b.orig_balance + b.orig_balance_adjustment + b.total_interest > b.total_payments) or (cl.zero_balance > 0))

-- Find the items which have the possibility of being incorrect
update		#invalid_accounts
set		to_be_checked = -1
from		#invalid_accounts i
inner join	creditor_methods mt with (nolock) on i.creditor_id = mt.creditor and 'EFT' = mt.type and mt.region in (0, i.region)

-- Discard the items which are payments other than RPPS banks
update		#invalid_accounts
set		to_be_checked = 0
from		#invalid_accounts i
inner join	creditor_methods mt with (nolock) on i.creditor_id = mt.creditor and 'EFT' = mt.type and mt.region in (0, i.region)
inner join	banks b with (nolock) on mt.bank = b.bank
where		b.type <> 'R'

-- Discard the remainder of the items since these are by checks only
delete
from		#invalid_accounts
where		to_be_checked = 0

-- If the item is now correct then just update the information
update		#invalid_accounts
set		payment_rpps_mask = mt.creditor_method,
		payment_rpps_mask_timestamp = mt.[timestamp],
		rpps_mask = mk.rpps_mask,
		rpps_mask_timestamp = mk.[timestamp]

from		#invalid_accounts i
inner join	creditor_methods mt on i.creditor_id = mt.creditor and mt.region in (0, i.region) and 'EFT' = mt.type
inner join	banks b with (nolock) on mt.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids ids with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks mk with (nolock) on mk.rpps_biller_id = ids.rpps_biller_id
where		i.account_number like dbo.map_rpps_masks ( mk.mask )

-- If there is no checksum then do not validate the checksum item
update		#invalid_accounts
set		rpps_mask		= null,
		rpps_mask_timestamp	= null,
		error_condition		= 'CHECKSUM'

from		#invalid_accounts i
inner join	rpps_masks mk with (nolock) on i.rpps_mask = mk.rpps_mask
where		mk.checksum = 3
and		dbo.valid_checksum_luhn ( i.account_number ) = 0

-- Correct the item
update		client_creditor
set		payment_rpps_mask = i.payment_rpps_mask,
		payment_rpps_mask_timestamp = i.payment_rpps_mask_timestamp,
		rpps_mask = i.rpps_mask,
		rpps_mask_timestamp = i.rpps_mask_timestamp

from		client_creditor cc
inner join	#invalid_accounts i on cc.client_creditor = i.client_creditor

-- If there is no method at all then mark the items as being "valid". These are checks only creditors.
update			#invalid_accounts
set			payment_rpps_mask_timestamp = 0
from			#invalid_accounts i
left outer join		creditor_methods m on i.creditor_id = m.creditor and 'EFT' = m.type
where			m.creditor_method is null

-- If there is a method but it points to a checking account then accept the item
update			#invalid_accounts
set			payment_rpps_mask_timestamp = 0
from			#invalid_accounts i
inner join		creditor_methods m on i.creditor_id = m.creditor and 'EFT' = m.type and m.region in (0, i.region)
left outer join		banks b on m.bank = b.bank
where			isnull(b.type,'C') <> 'R'

-- Remove the matched records from the error status
delete
from		#invalid_accounts
where		payment_rpps_mask_timestamp is not null

-- Mark the creditors which have invalid biller ids
update		#invalid_accounts
set		error_condition	= 'BILLER'
from		#invalid_accounts i
inner join	creditor_methods mt on i.creditor_id = mt.creditor and mt.region in (0, i.region) and 'EFT' = mt.type
left outer join	rpps_biller_ids ids on mt.rpps_biller_id = ids.rpps_biller_id
where		ids.rpps_biller_id is null
and		error_condition is null

-- Find the items which have a valid mask pointer
update		#invalid_accounts
set		payment_rpps_mask = mt.creditor_method
from		#invalid_accounts i
inner join	creditor_methods mt with (nolock) on mt.creditor = i.creditor_id and mt.type = 'EFT' and mt.region in (0, i.region)
inner join	rpps_biller_ids ids with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks mk with (nolock) on ids.rpps_biller_id = mk.rpps_biller_id and i.account_number like dbo.map_rpps_masks ( mk.rpps_mask )
where		i.payment_rpps_mask is null;

-- Find any mask for the item
update		#invalid_accounts
set		payment_rpps_mask = mt.creditor_method
from		#invalid_accounts i
inner join	creditor_methods mt with (nolock) on mt.creditor = i.creditor_id and mt.type = 'EFT' and mt.region in (0, i.region)
inner join	rpps_biller_ids ids with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks mk with (nolock) on ids.rpps_biller_id = mk.rpps_biller_id
where		i.payment_rpps_mask is null;

-- Find the closest account mask
update		#invalid_accounts
set		payment_rpps_mask = mt.creditor_method,
		payment_rpps_mask_timestamp = mt.[timestamp],
		error_condition = 'MASK'
from		#invalid_accounts i
inner join	creditor_methods mt on i.creditor_id = mt.creditor and mt.region in (0, i.region) and 'EFT' = mt.type
inner join	banks b with (nolock) on mt.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids ids with (nolock) on mt.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks mk with (nolock) on mk.rpps_biller_id = ids.rpps_biller_id

where		i.account_number like dbo.map_rpps_masks ( mk.rpps_mask )
and		i.error_condition is null

-- Finally report all others as just being "invalid"
update		#invalid_accounts
set		error_condition	= 'BILLER'
where		error_condition is null;

-- Return the results
select	client, creditor, client_creditor, client_name, creditor_name, account_number, account_mask, account_length, checksum, error_condition
from	#invalid_accounts
order by creditor, client, client_creditor

-- Discard working table
drop table #invalid_accounts
return ( @@rowcount )
GO
