USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_disbursement_manual_cm_client]    Script Date: 09/15/2014 13:13:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE PROCEDURE [dbo].[xpr_disbursement_manual_cm_client] ( @client AS typ_client ) AS

-- Fetch the information about the indicated client
SELECT	isnull(active_status,'PND')				as 'active_status',
	isnull(held_in_trust,0)					as 'held_in_trust'
FROM	clients WITH (NOLOCK)
WHERE	client = @client

return ( @@rowcount )
GO
