SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Transactions_CL] (@Client AS INT, @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ==============================================================================================================
-- ==               Retrieve the information for the given client                                              ==
-- ==============================================================================================================

-- Suppress intermediate result sets
SET NOCOUNT ON

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT	@FromDate	= convert(datetime, convert(varchar(10), @FromDate, 101) + ' 00:00:00'),
	@ToDate		= convert(datetime, convert(varchar(10), dateadd(d,1,@ToDate),   101) + ' 00:00:00')

-- Return the transactions
SELECT		d.client_register								as 'item_key',

		case d.tran_type
			when 'DP'	then d.tran_type + isnull('/' + d.tran_subtype,'')
			else d.tran_type
		end										as 'tran_type',

		convert (datetime, convert(varchar(10), d.date_created, 101) + ' 00:00:00')	as 'item_date',
		tr.checknum									as 'checknum',
		convert (datetime, convert(varchar(10), reconciled_date, 101) + ' 00:00:00')	as 'item_reconciled',

		convert(money, case d.tran_type
			when 'LD' then 0
			else d.credit_amt
		end) 										as 'credit_amt',

		convert(money, case d.tran_type
			when 'LD' then 0
			else d.debit_amt
		end) 										as 'debit_amt',

		d.message									as 'message'

FROM	registers_client d WITH (NOLOCK)
LEFT OUTER JOIN	registers_trust tr WITH (NOLOCK) ON d.trust_register = tr.trust_register
WHERE		d.client = @client
AND		d.tran_type IS NOT NULL
AND		d.date_created >= @FromDate
AND		d.date_created < @ToDate

ORDER BY	d.date_created, d.tran_type

RETURN ( @@rowcount )
GO
