SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [rpt_Summary_Check_CL_Voids] ( @FromDate AS DateTime = NULL, @ToDate AS DateTime = NULL ) AS
-- ======================================================================================================
-- ==                   Fetch the information for voided client checks                                 ==
-- ======================================================================================================

IF @ToDate IS NULL
	SELECT @ToDate = getdate()

IF @FromDate IS NULL
	SELECT @FromDate = @ToDate

-- Adjust the dates for the proper settings
SELECT @FromDate	= cast (convert(varchar(10), @FromDate, 101) + ' 00:00:00' as datetime)
SELECT @ToDate	= cast (convert(varchar(10), @ToDate, 101)   + ' 23:59:59' as datetime)

-- Fetch the transactions
SELECT		d.client		as 'client',
		d.credit_amt		as 'amount',
		dbo.format_normal_name(pn.prefix,pn.first,pn.middle,pn.last,pn.suffix)	as 'client_name',
		d.date_created		as 'date_created',
		tr.checknum		as 'checknum',
		d.created_by		as 'created_by',
		d.trust_register	as 'trust_register',
		tr.date_created		as 'date_written'

FROM		registers_client d	WITH (NOLOCK)
LEFT OUTER JOIN	people p		WITH (NOLOCK) ON d.client = p.client and 1 = p.relation
left outer join names pn with (nolock) on p.nameid = pn.name
LEFT OUTER JOIN	registers_trust tr	WITH (NOLOCK) ON d.trust_register = tr.trust_register
WHERE		d.tran_type = 'VR'
AND		d.client >= 0	-- Use client only to force the index scan to be performed
AND		d.date_created BETWEEN @FromDate AND @ToDate
ORDER BY	d.date_created, d.trust_register

RETURN ( @@rowcount )
GO
