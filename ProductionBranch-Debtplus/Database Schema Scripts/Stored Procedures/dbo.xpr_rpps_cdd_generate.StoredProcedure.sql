USE [DebtPlus]
GO
/****** Object:  StoredProcedure [dbo].[xpr_rpps_cdd_generate]    Script Date: 09/15/2014 13:13:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[xpr_rpps_cdd_generate] ( @rpps_file as int ) AS

-- ===================================================================================================
-- ==            Generate transactions for the RPPS drop record                                     ==
-- ===================================================================================================

-- ChangeLog
--   2/1/2002
--     Switch tables to the rpps_biller_ids table
--   11/20/2002
--     Changed to use RPPS' biller_aba number rather than rpps_biller_id
--   12/07/2002
--     Added support for drop_notice.
--   6/2/2003
--     Change to support "dual" (both proposals and payments) billers
--   11/24/2003
--     Added "@bank" as a parameter for the time being. It is optional.
--   1/5/2009
--      Changed from using biller_aba to using rpps_biller_id
--   3/21/2011
--      Revised to work with xrp_drop_debt.
--      Used information from the debt_notes rather than look up items again.
--   6/1/2012
--      Set the death_date when creating the transaction
--   12/10/2012
--      Only generate drops for valid types as specified in the creditor_drops table

-- Disable intermediate result sets
SET NOCOUNT ON

-- Find the bank number for the creditors
declare	@bank		int
select	@bank		= bank
from	rpps_files with (nolock)
where	rpps_file	= @rpps_file

if isnull(@bank,0) <= 0
begin
	RaisError ('The file is not associated with a bank number. Please use the new procedures.', 16, 1)
	return ( 0 )
end

-- Start a transaction for the operation
BEGIN TRANSACTION

-- Build a list of the drop notices to go out via RPPS
select		dn.debt_note, cc.client_creditor, cc.account_number, cc.client, cc.creditor, cr.creditor_id, cc.drop_reason, convert(varchar(10),null) as rpps_biller_id
into		#pending_rpps_drops
from		debt_notes dn with (nolock)
inner join	client_creditor cc with (nolock) on				dn.client_creditor = cc.client_creditor
inner join  drop_reasons dr with (nolock) on				cc.drop_reason = dr.drop_reason
inner join  creditor_drop_reasons drr with (nolock) on      dn.creditor = drr.creditor and dr.rpps_code = drr.rpps_code
inner join	clients c with (nolock) on						cc.client = c.client
inner join	creditors cr with (nolock) on					cc.creditor = cr.creditor
where		dn.type = 'DR'
and			dn.rpps_transaction is null
and			dn.output_batch is null
and			dn.drop_reason is not null

-- Fill in the creditor methods and the masks for the items
update		#pending_rpps_drops
set			rpps_biller_id		= ids.rpps_biller_id
from		#pending_rpps_drops d
inner join	creditor_methods cm on d.creditor_id = cm.creditor and 'CLO' = cm.type
inner join	banks b on cm.bank = b.bank and 'R' = b.type
inner join	rpps_biller_ids ids on cm.rpps_biller_id = ids.rpps_biller_id
inner join	rpps_masks m on ids.rpps_biller_id = m.rpps_biller_id
where		d.account_number like dbo.map_rpps_masks ( m.mask )
			
-- Mark these as having been sent the notice
update		client_creditor
set			drop_reason_sent = GETDATE()
from		client_creditor cc with (nolock)
inner join	#pending_rpps_drops x on cc.client_creditor = x.client_creditor
where		rpps_biller_id is not null;

-- Date when the transactions are to expire
declare		@death_date		datetime
select		@death_date	= dateadd(d, 180, getdate())

-- Build the RPPS transactions
insert into	rpps_transactions (trace_number_first, trace_number_last, client, creditor, client_creditor, biller_id, transaction_code, service_class_or_purpose, death_date, bank, rpps_file, debt_note)
select		null						as trace_number_first,
			null						as trace_number_last,
			client						as client,
			creditor					as creditor,
			client_creditor				as client_creditor,
			rpps_biller_id				as rpps_biller_id,
			23							as transaction_code,
			'CDD'						as service_class_or_purpose,
			@death_date					as death_date,
			@bank						as bank,
			@rpps_file					as rpps_file,
			debt_note					as debt_note
from		#pending_rpps_drops
where		rpps_biller_id is not null;

-- Generate the automated batch label
declare	@batch_label		varchar(50)
select	@batch_label = 'RPPS ' + convert(varchar, @rpps_file)

-- Update the transactions back
update		debt_notes
set			rpps_transaction	= tr.rpps_transaction,
			output_batch		= @batch_label
from		debt_notes dn with (nolock)
inner join  #pending_rpps_drops x on dn.debt_note = x.debt_note
inner join	rpps_transactions tr with (nolock) on dn.debt_note = tr.debt_note
where		tr.rpps_file		= @rpps_file
and			x.rpps_biller_id is not null;

-- Discard the working table
drop table	#pending_rpps_drops

-- Make the changes permanent
COMMIT TRANSACTION
RETURN ( 1 )
GO
